unit uPrimitiveClassHelper;

interface

Uses SysUtils;

type
  TStringClassHelper = class helper for String
    function ToIntDef(AValorDefault: Integer = 0): Integer;
end;

implementation

{ TStringClassHelper }

function TStringClassHelper.ToIntDef(AValorDefault: Integer = 0): Integer;
begin
  result := StrtoIntDef(Self, AValorDefault);
end;

end.
