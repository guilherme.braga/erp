unit uJSONUtils;

interface

uses
  Data.DBXJSON, Data.DBXJSONReflect, Generics.Collections, System.JSON, Rest.JSON;

type
  TJsonUtil = class
  private
    class var JSONMarshal: TJSONMarshal;
    class var JSONUnMarshal: TJSONUnMarshal;
  public
    class function ObjectToJSON<T: class>
    (AObject: T): TJSONValue;
    class function JSONToObject<T: class>
    (AJSON: TJSONValue): T;
    class function ListToJSONArray<T: class>
    (AList: TList<T>): TJSONArray;
    class function JSONArrayToList<T: class>
    (AJSONArray: TJSONArray): TList<T>;
  end;

implementation

class function TJsonUtil.ObjectToJSON<T>(AObject: T): TJSONValue;
begin
  if Assigned(AObject) then
  begin
    if not Assigned(JSONMarshal) then
      JSONMarshal := TJSONMarshal.Create;
    Result := JSONMarshal.Marshal(AObject);
  end
  else
    Exit(TJSONNull.Create);
end;

class function TJsonUtil.JSONToObject<T>(AJSON: TJSONValue): T;
begin
  if AJSON is TJSONNull then
    Exit(nil);
  if not Assigned(JSONUnMarshal) then
    JSONUnMarshal := TJSONUnMarshal.Create;
  Result := T(JSONUnMarshal.UnMarshal(AJSON));
end;

class function TJsonUtil.ListToJSONArray<T>
  (AList: TList<T>): TJSONArray;
var i: Integer;
begin
  Result := TJSONArray.Create;
  for i := 0 to AList.Count - 1 do
   Result.AddElement(Self.ObjectToJSON(AList[i]));
end;

class function TJsonUtil.JSONArrayToList<T>
  (AJSONArray: TJSONArray): TList<T>;
var i: Integer;
begin
  Result := TList<T>.Create;
  for i := 0 to AJSONArray.Size - 1 do
   Result.Add(Self.JSONToObject<T>(AJSONArray.Get(i)));
end;

end.
