unit uTJSON;

interface

Uses System.JSON, Classes, Contnrs, SysUtils;

type TJSON = class
  class function ArquivoParaJSON(AStringStream: TStringStream): TJSONArray;
  class function JSONParaArquivo(pArquivoJSON: TJSONArray): TStringStream;
end;

implementation

class function TJSON.ArquivoParaJSON(AStringStream: TStringStream) : TJSONArray;
var
  sBytesArquivo: String;
  iTamanhoArquivo, iCont: Integer;
const
  sNomeArquivo: string = 'JSONFile';
begin
  Result := TJSONArray.Create; // Instanciando o objeto JSON que conter� o arquivo serializado

  iTamanhoArquivo := AStringStream.Size; // pegando o tamanho do arquivo

  sBytesArquivo := '';

  // Fazendo um lan�o no arquivo que est� na memoria para pegar os bytes do mesmo
  for iCont := 0 to iTamanhoArquivo - 1 do
  begin
    // A medida que est� fazendo o la�o para pegar os bytes, os mesmos s�o jogados para
    // uma vari�vel do tipo string separado por ","
    sBytesArquivo := sBytesArquivo + IntToStr(AStringStream.Bytes[iCont]) + ', ';
  end;

  // Como � colocado uma v�rgula ap�s o byte, fica sempre sobrando uma v�gugula, que � deletada
  Delete(sBytesArquivo, Length(sBytesArquivo)-1, 2);

  // Adiciona a string que cont�m os bytes para o array JSON
  Result.Add(sBytesArquivo);

  // Adiciona para o array JSON o tamanho do arquivo
  Result.AddElement(TJSONNumber.Create(iTamanhoArquivo));

  // Adiciona na terceira posi��o do array JSON o nome do arquivo
  Result.AddElement(TJSONString.Create(sNomeArquivo));
end;


class function TJSON.JSONParaArquivo(pArquivoJSON: TJSONArray): TStringStream;
var
  sArquivoString, sNomeArquivo: String;
  iTamanhoArquivo, iCont: Integer;
  SLArrayStringsArquivo: TStringList;
  byArquivoBytes: Tbytes;
begin
  try
    sArquivoString := pArquivoJSON.Get(0).ToString;  // Pega a posi��o 0 do array que contem os bytes do arquivo
    Delete(sArquivoString, Length(sArquivoString), 1); // Deleta a �ltima aspas da string
    Delete(sArquivoString, 1, 1); // Deleta a primeira aspas da string

    sNomeArquivo := pArquivoJSON.Get(2).ToString;  // Pega o nome do arquivo que est� na posi��o 2
    Delete(sNomeArquivo, Length(sNomeArquivo), 1); // Deleta a �ltima aspas da string
    Delete(sNomeArquivo, 1, 1); // Deleta a primeira aspas da string

    iTamanhoArquivo := TJSONNumber(pArquivoJSON.Get(1)).AsInt; // Pega na posi��o 1 o tamanho do arquivo

    SLArrayStringsArquivo := TStringList.Create; // Cria um obje do tipo TStringList para emparelhar os bytes
    ExtractStrings([','], [' '], PChar(sArquivoString), SLArrayStringsArquivo); // Coloca cada byte em uma linha no objeto TStringList

    SetLength(byArquivoBytes, iTamanhoArquivo); // Seta o tamanho do array de bytes igual ao tamanho do arquivo

    // Faz um la�o para pegar os bytes do objeto TStringList
    for iCont := 0 to iTamanhoArquivo - 1 do
    begin
      //Pega os bytes do TStringList e adiciona no array de bytes
      byArquivoBytes[iCont] := StrToInt(SLArrayStringsArquivo[iCont]);
    end;
    result := TStringStream.Create(byArquivoBytes); // Instancia o objeto TStringStream para salvar o arquivo
  finally
    SLArrayStringsArquivo.Free;
  end;
end;

end.
