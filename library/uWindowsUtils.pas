unit uWindowsUtils;

interface

uses Windows, SysUtils, Forms, Types, Shellapi;

type TWindowsUtils = class
  class function DiretorioAplicacao: String;
  class function Sto_ShellExecute(const FileName, Parameters: string; var ExitCode: DWORD; const Wait: DWORD = 0; const Hide: Boolean = False): Boolean;
  class procedure ExecutaProcesso(lAguarda: Boolean; cmd: string; FCod_Funcionario : Integer);
  class procedure ExecutePrograma(Nome, Parametros: string);
  class function CreateProcessSimple(lAguarda: boolean; cmd: string): Boolean;
end;

implementation

{ TWindowsUtils }

class function TWindowsUtils.DiretorioAplicacao: String;
begin
  result := ExtractFilePath(Application.ExeName);
end;

class procedure TWindowsUtils.ExecutePrograma(Nome, Parametros: string);
var
  Comando: array[0..1024] of Char;
  Parms: array[0..1024] of Char;
begin
  StrPCopy(Comando, Nome);
  StrPCopy(Parms, Parametros);
  ShellExecute(0, nil, Comando, Parms, nil, SW_ShowMaximized);
end;

class procedure TWindowsUtils.ExecutaProcesso(lAguarda: Boolean; cmd: string; FCod_Funcionario : Integer);
begin
  CreateProcessSimple(lAguarda, PChar(cmd + ' ' + IntToStr(FCod_Funcionario)));
end;

class  function TWindowsUtils.CreateProcessSimple(lAguarda: boolean; cmd: string): Boolean;
var SUInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
begin
  FillChar(SUInfo, SizeOf(SUInfo), #0);
  SUInfo.cb := SizeOf(SUInfo);
  SUInfo.dwFlags := STARTF_USESHOWWINDOW;
  SUInfo.wShowWindow := SW_NORMAL;
  Result := CreateProcess(nil, PChar(cmd), nil, nil, false, CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil, nil, SUInfo, ProcInfo);

  if (Result) and lAguarda then
    begin
      WaitForSingleObject(ProcInfo.hProcess, INFINITE);
      CloseHandle(ProcInfo.hProcess);
      CloseHandle(ProcInfo.hThread);
    end;
end;

class function TWindowsUtils.Sto_ShellExecute(const FileName, Parameters: string; var ExitCode: DWORD; const Wait: DWORD = 0; const Hide: Boolean = False): Boolean;
var
  myInfo: SHELLEXECUTEINFO;
  iWaitRes: DWORD;
begin
  // prepare SHELLEXECUTEINFO structure
  ZeroMemory(@myInfo, SizeOf(SHELLEXECUTEINFO));
  myInfo.cbSize := SizeOf(SHELLEXECUTEINFO);
  myInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI;
  myInfo.lpFile := PChar(FileName);
  myInfo.lpParameters := PChar(Parameters);
  myInfo.lpDirectory := PChar(ExtractFileDrive(FileName));
  if Hide then
    myInfo.nShow := SW_HIDE
  else
    myInfo.nShow := SW_SHOWNORMAL;
  // start file
  ExitCode := 0;
  Result := ShellExecuteEx(@myInfo);
  // if process could be started
  if Result then
    begin
    // wait on process ?
      if (Wait > 0) then
        begin
          iWaitRes := WaitForSingleObject(myInfo.hProcess, Wait);
      // timeout reached ?
          if (iWaitRes = WAIT_TIMEOUT) then
            begin
              Result := False;
              TerminateProcess(myInfo.hProcess, 0);
            end;
      // get the exitcode
          GetExitCodeProcess(myInfo.hProcess, ExitCode);
        end;
    // close handle, because SEE_MASK_NOCLOSEPROCESS was set
      CloseHandle(myInfo.hProcess);
    end;
end;


end.
