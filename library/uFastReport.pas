unit uFastReport;

interface

uses
  //Project
  SysUtils,
  StrUtils,
  Classes,
  windows,
  forms,
  fs_iinterpreter,
  Variants,
  Math,
  DateUtils,
  Controls,
  DB,
  ShlObj,
  ActiveX;

const
  FR_GROUP = 'Obliviate - Fun��es';

type
TFastReportSysUtils = class(TfsRTTIModule)
private
function CallMethod(Instance: TObject; ClassType: TClass; const MethodName: String; var Params: Variant): Variant;
public
constructor Create(AScript: TfsScript); override;
end;

function Extenso(Numero: Real): String; //retorna o numero por extenso
function Divide(X, Y: Double): Double; //retorna a divis�o de X por Y.
//Considera se Y for nulo ou zero.
function Nz(Value: Variant; ValueIfNull: Variant): Variant; //Retorna
//ValueIfNull se o valor for nulo sen�o retorna Value
function Ceil(const X: Extended): Integer; //valor arredondado para baixo
function PriMaiuscula(Texto: String): String; //Primeira em Mai�scula
function FormataCPF(CPF : string): string; // Formata uma cadeia de strings
//referente ao CPF
function FormataCGC(CGC : string): string; // Formata uma cadeia de strings
//referente ao CGC
function Sobrenome(S : string) : string; // Retorna o Sobrenome de um nome
//especificado
function RemoveAcentos(Str:String): String; {Remove caracteres acentuados de
uma string}
function PrimeiroNome (Nome : String) : String; // Para pegar o primeiro nome
//de uma pessoa
function NumeroRomano(num:integer):string; // Converte um valor integer em
//Romano
function StrToBoolean(s: string): boolean; // Converte um valor String em
//Booleano
function AbreviaNome(Nome: String): String; // Abrevia nomes
function DifHora(Inicio,Fim : String):String; {Retorna a diferen�a entre duas
horas}
function DifDias(DataVenc:TDateTime; DataAtual:TDateTime): String; {Retorna a
diferenca de dias entre duas datas}
function NomedoMes(dData:TDatetime):string; {Retorna o nome do mee, em
extenso, de uma determinada data}
function IdadeN(Nascimento:TDateTime) : String; // Retorna a idade de uma
//pessoa a partir da data de nascimento
function ProximoDiaUtil(dData : TDateTime) : TDateTime; // Retorna o pr�ximo
//dia �til caso a data informada caia em um fim de semana
function Replicate( Caracter:String; Quant:Integer ): String; {Repete o mesmo
caractere v�rias vezes}
function DataExtenso(Data:TDateTime): String; {Para exibir uma data por
extenso no Delphi, coloque a seguinte rotina}
function IdadeAtual(Nasc : TDateTime): Integer; // Retorna a idade Atual de
///uma pessoa a partir da data de nascimento
function roundx(dValIni: Double; iCasas:Integer): Double; // Faz o
//arredondamento conforme o n�mero de casas decimais
function Preenche(X, Y: String; Z:Integer):String; // Preenche a vari�vel Y
//com Z vezes com o caractere X
function idade(Nascimento,DataAtual:string):Integer; // Retorna a idade de uma pessoa
function FormataTel(Tel : string ; FormataMesmoAssim : boolean = true): string; //Retorna um telefone com sua respectiva m�scara
function getDataExtenso(dDate: TDate): String;  //Retorna uma data por extenso
function FileExists(AFilePath: String):Boolean; //Checar se o Arquivo Existe
function GetWindowsDirectory: String; //Pegar o Diret�rio Default do Windows (Windows)
function GetMyDocumentsDirectory: String;// Pegar o Diret�rio Default do Meus Documentos;
function GetProgramDirectory: String;//Diret�rio do Programa
function CompleteCharLeft(AChar: String; AString: String; ALength: Integer): String;
function CompleteCharRight(AChar: String; AString: String; ALength: Integer): String;
function StringReplace(AString, OldString, NewString: String): String;
function StrtoIntDef(AString: String; ADefaultValue: Integer): Integer;
function StrtoFloatDef(AString: String; ADefaultValue: Real): Real;
function ClearRichTextFormat(AValue: String): String;

implementation

constructor TFastReportSysUtils.Create;
begin
  inherited Create(AScript);

  //Registra as Fun��es...
  with AScript do
    begin
      AddMethod('function Extenso(Numero: Real): String', CallMethod, FR_GROUP,'Retorna o valor por extenso');
      AddMethod('function Divide(X, Y: Real): Real', CallMethod, FR_GROUP, 'Divide X por Y, retorna 0 se Y for nulo ou 0.');
      AddMethod('function Nz(Value: Variant; ValueIfNull: Variant): Variant', CallMethod, FR_GROUP, 'Retorna Value se este n�o � nulo, se for retornaValueIfNull');
      AddMethod('function Ceil(const X: Extended): Integer', CallMethod, FR_GROUP,'Retorna a parte inteira do valor X arredondado para cima se houver decimais');
      AddMethod('function PriMaiuscula(Texto: String): String', CallMethod,FR_GROUP, 'Converte a primeira letra do texto para mai�scula e as restantespara minuscula');

      AddMethod('function FormataCPF(CPF : string): string', CallMethod, FR_GROUP,'Formata uma cadeia de strings referente ao CPF');
      AddMethod('function FormataCGC(CGC : string): string', CallMethod, FR_GROUP,'Formata uma cadeia de strings referente ao CGC');
      AddMethod('function Sobrenome(S : string) : string', CallMethod, FR_GROUP,'Retorna o Sobrenome de um nome especificado');
      AddMethod('function RemoveAcentos(Str:String): String', CallMethod,FR_GROUP, 'Remove caracteres acentuados de uma string');
      AddMethod('function PrimeiroNome (Nome : String) : String', CallMethod,FR_GROUP, 'Retorna o primeiro nome de uma pessoa');
      AddMethod('function RomanNum(num:integer):string', CallMethod, FR_GROUP,'Converte um valor integer em Romano');
      AddMethod('function StrToBoolean(s: string): boolean', CallMethod, FR_GROUP,'Converte um valor String em Booleano. Se a String for TRUE, T, YES, Y, ON, O ou 1; ent�o retorna True. Sen�o retorna False.');
      AddMethod('function AbreviaNome(Nome: String): String', CallMethod, FR_GROUP, 'Abrevia nomes. Ex: Jos� Francisco Manoel da Siva vira Jos� F. M. daSilva.');
      AddMethod('function DifHora(Inicio,Fim : String):String', CallMethod,FR_GROUP, 'Retorna a diferen�a entre duas horas');
      AddMethod('function DifDias(DataVenc:TDateTime, CallMethod, FR_GROUP,DataAtual:TDateTime): String', CallMethod, FR_GROUP, 'Retorna a diferenca dedias entre duas datas');
      AddMethod('function NomedoMes(dData:TDatetime):string', CallMethod,FR_GROUP, 'Retorna o nome do mes em extenso, de uma determinada data');
      AddMethod('function IdadeN(Nascimento:TDateTime) : String', CallMethod,FR_GROUP, 'Retorna a idade de uma pessoa a partir da data de nascimento');
      AddMethod('function ProximoDiaUtil(dData : TDateTime) : TDateTime',CallMethod, FR_GROUP, 'Retorna o pr�ximo dia �til caso a data informada caiaem um fim de semana');
      AddMethod('function Replicate(Caracter:String; Quant:Integer): String',CallMethod, FR_GROUP, 'Repete o mesmo caractere v�rias vezes');
      AddMethod('function DataExtenso(Data:TDateTime): String', CallMethod,FR_GROUP, 'Para exibir uma data por extenso.');
      AddMethod('function IdadeAtual(Nasc : TDate): Integer', CallMethod,FR_GROUP, 'Retorna a idade atual de uma pessoa a partir da data de nascimento.Esta fun��o retorna a idade em formato integer');
      AddMethod('function roundx(dValIni: Double, CallMethod, FR_GROUP,iCasas:Integer): Double', CallMethod, FR_GROUP, 'Faz o arredondamento conforme on�mero de casas decimais');
      AddMethod('function Preenche(X, Y: String, CallMethod, FR_GROUP,Z:Integer):String', CallMethod, FR_GROUP, 'Preenche a vari�vel Y com Z vezescom o caractere X');
      AddMethod('function idade(Nascimento,DataAtual:string):Integer', CallMethod,FR_GROUP, 'Retorna a idade de uma pessoa');
      AddMethod('function FormataTel(Tel : string ; FormataMesmoAssim : boolean = true): string', CallMethod,FR_GROUP,
                'Insere m�scara de telefone. Ex.: (xx) xxxx-xxxx. Caso a variavel "FormataMesmoAssim" for informada como "false", n�o ir� inserir a m�scara caso esteja faltando caracteres.');
      AddMethod('function getDataExtenso(dDate: TDate): String', CallMethod,FR_GROUP,
                'Formata data por extenso. Recebe uma TDATE e retorna uma string com a data por extenso. Ex.: 15 de Novembro de 2011');
      AddMethod('function FileExists(AFilePath: String):Boolean', CallMethod,FR_GROUP,'Retorna se o arquivo por parametro existe ou n�o. Ex.: FileExists("c:\Teste.txt")');
      AddMethod('function GetWindowsDirectory: String', CallMethod,FR_GROUP,'Retorna o diret�rio do Windows');
      AddMethod('function GetMyDocumentsDirectory: String', CallMethod,FR_GROUP,'Pegar o Diret�rio Default do Meus Documentos');
      AddMethod('function GetProgramDirectory: String', CallMethod,FR_GROUP,'Diret�rio do Programa');
      AddMethod('function StrtoIntDef(AString: String; ADefaultValue: Integer): Integer', CallMethod,FR_GROUP,'Converte um valor String para Inteiro passando um valor default em caso de exception');
      AddMethod('function StrtoFloatDef(AString: String; ADefaultValue: Real): Real', CallMethod,FR_GROUP,'Converte um valor String para flutuando passando um valor default em caso de exception');
      AddMethod('function CompleteCharLeft(AChar: Char; AString: String; ALength: Integer): String', CallMethod,FR_GROUP,'Completa uma String com a String desejado.');
      AddMethod('function CompleteCharRight(AChar: Char; AString: String; ALength: Integer): String', CallMethod,FR_GROUP,'Completa uma String com a String desejado.');
      AddMethod('function StringReplace(AString, OldString, NewString: String): String', CallMethod,FR_GROUP,'Substitui uma String por outra String em um Array de Strings');
      AddMethod('function ClearRichTextFormat(AValue: String): String', CallMethod,FR_GROUP,'Limpa caracteres de formatacao encontrados em RichText');
    end;
end;

function TFastReportSysUtils.CallMethod(Instance: TObject; ClassType: TClass; const MethodName: String; var Params: Variant): Variant;
var
MethodName_Upper: String;
begin
MethodName_Upper := UpperCase(MethodName);

  //Retorno de Fun��es no FAST-REPORT
  if MethodName_Upper = 'EXTENSO' then
    Result := Extenso(Params[0])
  else
  if MethodName_Upper = 'DIVIDE' then
    Result := divide(Params[0], Params[1])
  else
  if MethodName_Upper = 'NZ' then
    Result := Nz(Params[0], Params[1])
  else
  if MethodName_Upper = 'CEIL' then
    Result := Ceil(Params[0])
  else
  if MethodName_Upper = 'PRIMAIUSCULA' then
    Result := PriMaiuscula(Params[0])
  else
  if MethodName_Upper = 'FORMATACPF' then
    Result := FormataCPF(Params[0])
  else
  if MethodName_Upper = 'FORMATACGC' then
    Result := FormataCGC(Params[0])
  else
  if MethodName_Upper = 'SOBRENOME' then
    Result := Sobrenome(Params[0])
  else
  if MethodName_Upper = 'REMOVEACENTOS' then
    Result := RemoveAcentos(Params[0])
  else
  if MethodName_Upper = 'PRIMEIRONOME' then
    Result := PrimeiroNome(Params[0])
  else
  if MethodName_Upper = 'NUMEROROMANO' then
    Result := NumeroRomano(Params[0])
  else
  if MethodName_Upper = 'STRTOBOOLEAN' then
    Result := StrToBoolean(Params[0])
  else
  if MethodName_Upper = 'ABREVIANOME' then
    Result := AbreviaNome(Params[0])
  else
  if MethodName_Upper = 'DIFHORA' then
    Result := DifHora(Params[0], Params[1])
  else
  if MethodName_Upper = 'DIFDIAS' then
    Result := DifDias(Params[0], Params[1])
  else
  if MethodName_Upper = 'NOMEDOMES' then
    Result := NomeDoMes(Params[0])
  else
  if MethodName_Upper = 'IDADEN' then
    Result := IdadeN(Params[0])
  else
  if MethodName_Upper = 'PROXIMODIAUTIL' then
    Result := ProximoDiaUtil(Params[0])
  else
  if MethodName_Upper = 'REPLICATE' then
    Result := Replicate(Params[0], Params[1])
  else
  if MethodName_Upper = 'DATAEXTENSO' then
    Result := DataExtenso(Params[0])
  else
  if MethodName_Upper = 'IDADE' then
    Result := Idade(Params[0], Params[1])
  else
  if MethodName_Upper = 'IDADEATUAL' then
    Result := IdadeAtual(Params[0])
  else
  if MethodName_Upper = 'ROUNDX' then
    Result := RoundX(Params[0], Params[1])
  else
  if MethodName_Upper = 'PREENCHE' then
    Result := Preenche(Params[0], Params[1], Params[2])
  else
  if MethodName_Upper = 'FORMATATEL' then
    Result := FormataTel(Params[0], Params[1])
  else
  if MethodName_Upper = 'GETDATAEXTENSO' then
    Result := getDataExtenso(Params[0])
  else
  if MethodName_Upper = 'FILEEXISTS' then
    Result := FileExists(Params[0])
  else
  if MethodName_Upper = 'GETWINDOWSDIRECTORY' then
    Result := GetWindowsDirectory
  else
  if MethodName_Upper = 'GETPROGRAMDIRECTORY' then
    Result := GetProgramDirectory
  else
  if MethodName_Upper = 'GETMYDOCUMENTSDIRECTORY' then
    Result := GetMyDocumentsDirectory
  else
  if MethodName_Upper = 'STRTOINTDEF' then
    Result := uFastReport.StrtoIntDef(Params[0], Params[1])
  else
  if MethodName_Upper = 'COMPLETECHARRIGHT' then
    Result := uFastReport.CompleteCharRight(Params[0], Params[1], Params[2])
  else
  if MethodName_Upper = 'COMPLETECHARLEFT' then
    Result := uFastReport.CompleteCharLeft(Params[0], Params[1], Params[2])
  else
  if MethodName_Upper = 'STRINGREPLACE' then
    Result := uFastReport.StringReplace(Params[0], Params[1], Params[2])
  else
  if MethodName_Upper = 'CLEARRICHTEXTFORMAT' then
    Result := uFastReport.ClearRichTextFormat(Params[0]);
end;

{ ================ fun��es de usu�rio para o FastReport ================= }

function ClearRichTextFormat(AValue: String): String;
var cString: String;
begin
  cString := AValue;
  cString := SysUtils.StringReplace(cString, '{\rtf1', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\ansi', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\ansicpg1252', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\deff0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '{\fonttbl', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '{\f0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fnil', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fcharset0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New;}}', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\viewkind4', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\uc1', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\pard', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\lang1046', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\f0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fs20', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\par }', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\par', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\par  }', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\rtf1', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\ansi', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\dff0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\viewkind4', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\uc1', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\pard', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\lang1046', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\f0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fs20', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '{cpg1252', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\cpg1252', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, 'cpg1252', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\deff0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '{\fonttbl', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fonttbl', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fnil', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, '\fcharset0', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New; }}', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New;}}', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New;}', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New;', '', [rfReplaceAll]);
  cString := SysUtils.StringReplace(cString, ' Courier New', '', [rfReplaceAll]);
  result := cString;
end;

function StringReplace(AString, OldString, NewString: String): String;
begin
  result := SysUtils.StringReplace(AString, OldString, NewString, [rfReplaceAll]);
end;

function CompleteCharLeft(AChar: String; AString: String; ALength: Integer): String;
begin
  result := DupeString(AChar, ALength - Length(AString)) + AString;
end;

function CompleteCharRight(AChar: String; AString: String; ALength: Integer): String;
begin
  result := AString + DupeString(AChar, ALength - Length(AString));
end;

function GetMyDocumentsDirectory: String;// Pegar o Diret�rio Default do Meus Documentos;
var
  PIDL: PItemIDList;
  Path: LPSTR;
  AMalloc: IMalloc;
begin
{  Path := StrAlloc(MAX_PATH);
  SHGetSpecialFolderLocation(Application.Handle, CSIDL_PERSONAL, PIDL);

  if SHGetPathFromIDList(PIDL, Path) then
    Result := Path;

  SHGetMalloc(AMalloc);
  AMalloc.Free(PIDL);
  StrDispose(Path);      }

{  Now you can call this function with different parameters. For example:
CSIDL_DESKTOP for WINDOWS\Desktop
CSIDL_DESKTOPDIRECTORY for WINDOWS\Desktop
CSIDL_FONTS for WINDOWS\FONTS
CSIDL_NETHOOD for WINDOWS\NetHood
CSIDL_PERSONAL for X:\My Documents
CSIDL_PROGRAMS for WINDOWS\StartMenu\Programs
CSIDL_RECENT for WINDOWS\Recent
CSIDL_SENDTO for WINDOWS\SendTo
CSIDL_STARTMENU for WINDOWS\Start Menu
CSIDL_STARTUP for WINDOWS\Start Menu\Programs\StartUp
CSIDL_TEMPLATES for WINDOWS\ShellNew}

end;

function StrtoIntDef(AString: String; ADefaultValue: Integer): Integer;
begin
  result := SysUtils.StrtoIntDef(AString, ADefaultValue);
end;

function StrtoFloatDef(AString: String; ADefaultValue: Real): Real;
begin
  result := SysUtils.StrtoFloatDef(AString, ADefaultValue);
end;

function GetProgramDirectory: String;  //
begin
  result := ExtractFilePath(Application.ExeName);
end;

function FileExists(AFilePath: String):Boolean; //Checar se o Arquivo Existe
begin
  result := SysUtils.FileExists(AFilePath);
end;

function GetWindowsDirectory: String; //Pegar o Diret�rio Default do Windows (Meus Documentos)
begin
  SetLength(Result, MAX_PATH);
  Windows.GetWindowsDirectory(PChar(Result), MAX_PATH);
  Result := string(PChar(Result)+'\');
end;

function Extenso(Numero: Real): String;
{ retorna o extenso de um n�mero }
type
TNumeroStr = String; { tipo para a fun��o "Extenso" }

const
aUnidades: array[1..19] of TNumeroStr =
('um', 'dois', 'tr�s', 'quatro','cinco', 'seis', 'sete', 'oito',
'nove', 'dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze',
'dezesseis', 'dezessete', 'dezoito','dezenove');
aDezenas: array[1..9] of TNumeroStr =
('dez', 'vinte', 'trinta', 'quarenta','cinq�enta', 'sessenta',
'setenta', 'oitenta', 'noventa');
aCentenas: array[1..9] of TNumeroStr =
('cem', 'duzentos', 'trezentos','quatrocentos', 'quinhentos',
'seiscentos', 'setecentos', 'oitocentos','novecentos');

ERRORSTRING = 'Valor fora da faixa';
MIN = 0.01;
MAX = 4294967295.99;
MOEDA = ' real';
MOEDAS = ' reais';
CENTESIMO = ' centavo';
CENTESIMOS = ' centavos';

{ fun��o auxiliar para a extenso retornar as unidades por extenso }
function ConversaoRecursiva(N: LongWord): String;
begin
case N of
1..19:
Result := aUnidades[N];
20, 30, 40, 50, 60, 70, 80, 90:
Result := aDezenas[N div 10] + ' ';
21..29, 31..39, 41..49, 51..59, 61..69, 71..79, 81..89, 91..99:
Result := aDezenas[N div 10] + ' e ' + ConversaoRecursiva(N mod 10);
100, 200, 300, 400, 500, 600, 700, 800, 900:
Result := aCentenas[N div 100] + ' ';
101..199:
Result := ' cento e ' + ConversaoRecursiva(N mod 100);
201..299, 301..399, 401..499, 501..599, 601..699, 701..799, 801..899,
901..999:
Result := aCentenas[N div 100] + ' e ' + ConversaoRecursiva(N mod 100);
1000..999999:
Result := ConversaoRecursiva(N div 1000) + ' mil ' +
ConversaoRecursiva(N mod 1000);
1000000..1999999:
Result := ConversaoRecursiva(N div 1000000) + ' milh�o ' +
ConversaoRecursiva(N mod 1000000);
2000000..999999999:
Result := ConversaoRecursiva(N div 1000000) + ' milh�es ' +
ConversaoRecursiva(N mod 1000000);
1000000000..1999999999:
Result := ConversaoRecursiva(N div 1000000000) + ' bilh�o ' +
ConversaoRecursiva(N mod 1000000000);
2000000000..4294967295:
Result := ConversaoRecursiva(N div 1000000000) + ' bilh�es ' +
ConversaoRecursiva(N mod 1000000000);
end;
end;

begin
if (Numero=null) or (Numero=0) then
Result := ''
else
if (Numero >= MIN) and (Numero <= MAX) then
begin
{ tratar parte inteira }
Result := ConversaoRecursiva(Round(Int(Numero)));

if Round(Int(Numero)) = 1 then
Result := Result + MOEDA
else
if Round(Int(Numero)) <> 0 then
Result := Result + MOEDAS;

{ tratar parte decimal }
if not(Frac(Numero) = 0.00) then
begin
if Round(Int(Numero)) <> 0 then
Result := Result + ' e ';
Result := Result + ConversaoRecursiva(Round(Frac(Numero) * 100));

if (Round(Frac(Numero) * 100) = 1) then
Result := Result + CENTESIMO
else
Result := Result + CENTESIMOS;
end;

Result := SysUtils.StringReplace(Result, ' ', ' ', []);
end
else
Raise
ERangeError.CreateFmt('%g ' + ERRORSTRING + ' %g..%g', [Numero, MIN,
MAX]);
end;

function Divide(X, Y: Double): Double;
{ retorna o valor X divido por B, so que se Y for nulo ou zero,
sempre retorna zero, evitando erros }
begin
if (Y=null) or (Y=0) then
result := 0
else
result := X / Y;
end;

function Nz(Value: Variant; ValueIfNull: Variant): Variant;
{ retorna V se n�o � nulo, se � retorna ValueIfNull }
begin
if (Value=null) then
begin
Result := ValueIfNull
end
else
Result := Value;
end;

function Ceil(const X: Extended): Integer;
{Retorna a parte inteira do valor X arredondado para cima se houver decimais}
begin
Result := Math.Ceil(X);
end;

function PriMaiuscula(Texto: String): String;
{Converte a primeira letra do texto para maiuscula e as restantes para
minuscula}
begin
if (Texto=null) or (Result = '') then
Result := ''
else
begin
Texto := UpperCase(Copy(Texto,1,1))+LowerCase(Copy(Texto,2,Length(Texto)));
Result := Texto;
end;
end;


function FormataCPF(CPF : string): string;
// Formata uma cadeia de strings referente ao CPF
begin
Result :=
Copy(CPF,1,3)+'.'+Copy(CPF,4,3)+'.'+Copy(CPF,7,3)+'-'+Copy(CPF,10,2);
end;

function FormataCGC(CGC : string): string;
// Formata uma cadeia de strings referente ao CGC
begin
Result :=
Copy(CGC,1,2)+'.'+Copy(CGC,3,3)+'.'+Copy(CGC,6,3)+'/'+Copy(CGC,9,4)+'-'+Copy(CGC,13,2);
end;

function Sobrenome(S : string) : string;
// Retorna o Sobrenome de um nome especificado
var
i, Size: Integer;
begin
i := Pos(#32, S);
if i = 0 then
begin
Result := S;
Exit;
end
else
begin
Size := (Length(S) - i);
Move(S[i + 1], S[1], Size);
SetLength(S, Size);
Result := S;
end;
end;

function RemoveAcentos(Str:String): String;
{Remove caracteres acentuados de uma string}
Const ComAcento = '����������������������������';
SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
Var
x : Integer;
Begin
For x := 1 to Length(Str) do
Begin
if Pos(Str[x],ComAcento)<>0 Then
begin
Str[x] := SemAcento[Pos(Str[x],ComAcento)];
end;
end;
Result := Str;
end;

function PrimeiroNome (Nome : String) : String;
// Para pegar o primeiro nome de uma pessoa
var
PNome : String;
begin
PNome := '';
if pos (' ', Nome) <> 0 then
begin
PNome := copy (Nome, 1, pos (' ', Nome) - 1);
end;
Result := PNome;
end;

function NumeroRomano(num:integer):string;
// Converte um valor integer em Romano
var
i,j,n: integer;
digit,pivot:string;
begin
if (num<1) or (num>9999) then
begin
result:='Error!';
exit;
end;
result := '';
digit := 'IXCM';
pivot := 'VLD';
for i:=1 to 3 do
begin
n := num MOD 10;
num := num Div 10;
case n of
1..3: begin
for j := 1 to n do
begin
result := digit[i]+result;
end;
end;
4: result := digit[i]+pivot[i]+result;
5..8: begin
for j := 6 to n do
begin
result:=digit[i]+result;
end;
result:=pivot[i]+result;
end;
9: result:=Copy(digit,i,2)+result;
end;
end;
for i:=1 to num do
begin
result:='M'+result;
end;
end;

function StrToBoolean(s: string): boolean;
// Converte um valor String em Booleano
begin
result := ((uppercase(s) = 'TRUE') or
(uppercase(s) = 'T') or
(uppercase(s) = 'YES') or
(uppercase(s) = 'Y') or
(uppercase(s) = 'ON') or
(uppercase(s) = 'O') or
(uppercase(s) = '1'));
end;

function AbreviaNome(Nome: String): String;
// Abrevia nomes que, em alguns casos, n�o
// cabem em um determinado componente na tela
// ou ficam muito grandes para serem impresso
// em um relat�rio.
// Ex: O nome Jos� Francisco Manoel da Siva �
// abreviado, por esta fun��o, para Jos� F. M. da Silva.
var
Nomes: array[1..20] of string;
i, TotalNomes: Integer;
begin
Nome := Trim(Nome);
Result := Nome;
{Insere um espa�o para garantir que todas as letras sejam testadas}
Nome := Nome + #32;
{Pega a posi��o do primeiro espa�o}
i := Pos(#32, Nome);
if i > 0 then
begin
TotalNomes := 0;
{Separa todos os nomes}
while i > 0 do
begin
Inc(TotalNomes);
Nomes[TotalNomes] := Copy(Nome, 1, i - 1);
Delete(Nome, 1, i);
i := Pos(#32, Nome);
end;
if TotalNomes > 2 then
begin
{Abreviar a partir do segundo nome, exceto o �ltimo.}
for i := 2 to TotalNomes - 1 do
begin
{Cont�m mais de 3 letras? (ignorar de, da, das, do, dos, etc.)}
if Length(Nomes[i]) > 3 then
{Pega apenas a primeira letra do nome e coloca um ponto ap�s.}
Nomes[i] := Nomes[i][1] + '.';
end;
Result := '';
for i := 1 to TotalNomes do
Result := Result + Trim(Nomes[i]) + #32;
Result := Trim(Result);
end;
end;
end;

function DifHora(Inicio,Fim : String):String;
{Retorna a diferen�a entre duas horas}
var
FIni,FFim : TDateTime;
begin
Fini := StrTotime(Inicio);
FFim := StrToTime(Fim);
If (Inicio > Fim) then
begin
Result := TimeToStr((StrTotime('23:59:59')-Fini)+FFim)
end
else
begin
Result := TimeToStr(FFim-Fini);
end;
end;

function DifDias(DataVenc:TDateTime; DataAtual:TDateTime): String;
{Retorna a diferenca de dias entre duas datas}
Var Data: TDateTime;
dia, mes, ano: Word;
begin
if DataAtual < DataVenc then
begin
Result := 'erro';
end
else
begin
Data := DataAtual - DataVenc;
DecodeDate( Data, ano, mes, dia);
Result := FloatToStr(Data);
end;
end;

function NomedoMes(dData:TDatetime):string;
{Retorna o nome do mee, em extenso, de uma determinada data}
var
nAno,nMes,nDia:word;
cMes:array[1..12] of string;
begin
cMes[01] := 'Janeiro';
cMes[02] := 'Fevereiro';
cMes[03] := 'Mar�o';
cMes[04] := 'Abril';
cMes[05] := 'Maio';
cMes[06] := 'Junho';
cMes[07] := 'Julho';
cMes[08] := 'Agosto';
cMes[09] := 'Setembro';
cMes[10] := 'Outubro';
cMes[11] := 'Novembro';
cMes[12] := 'Dezembro';
decodedate(dData,nAno,nMes,nDia);
if (nMes>=1) and (nMes<=13)then
begin
Result:=cMes[nMes];
end
else
begin
Result:='';
end;
end;

function IdadeN(Nascimento:TDateTime) : String;
// Retorna a idade de uma pessoa a partir da data de nascimento
Type
Data = Record
Ano : Word;
Mes : Word;
Dia : Word;
End;
Const
Qdm:String = '312831303130313130313031'; // Qtde dia no mes
Var
Dth : Data; // Data de hoje
Dtn : Data; // Data de nascimento
anos, meses, dias, nrd : Shortint; // Usadas para calculo da idade
begin
DecodeDate(Date,Dth.Ano,Dth.Mes,Dth.Dia);
DecodeDate(Nascimento,Dtn.Ano,Dtn.Mes,Dtn.Dia);
anos := Dth.Ano - Dtn.Ano;
meses := Dth.Mes - Dtn.Mes;
if meses < 0 then
begin
Dec(anos);
meses := meses+12;
end;
dias := Dth.Dia - Dtn.Dia;
if dias < 0 then
begin
nrd := StrToInt(Copy(Qdm,(Dth.Mes-1)*2-1,2));
if ((Dth.Mes-1)=2) and ((Dth.Ano Div 4)=0) then
begin
Inc(nrd);
end;
dias := dias+nrd;
meses := meses-1;
end;
Result := IntToStr(anos)+' Anos '+IntToStr(meses)+' Meses '+IntToStr(dias)+'Dias';
end;

function ProximoDiaUtil(dData : TDateTime) : TDateTime;
// Retorna o pr�ximo dia �til caso a data informada caia em um fim de semana
begin
if DayOfWeek(dData) = 7 then
begin
dData := dData + 2
end
else if DayOfWeek(dData) = 1 then
begin
dData := dData + 1;
end;
Result := dData;
end;

function Replicate( Caracter:String; Quant:Integer ): String;
{Repete o mesmo caractere v�rias vezes}
var I : Integer;
begin
Result := '';
for I := 1 to Quant do
Result := Result + Caracter;
end;

function DataExtenso(Data:TDateTime): String;
{Para exibir uma data por extenso no Delphi, coloque a seguinte rotina}
var
NoDia : Integer;
// Now: TdateTime;
DiaDaSemana : array [1..7] of String;
Meses : array [1..12] of String;
Dia, Mes, Ano : Word;
begin
{ Dias da Semana }
DiaDasemana [1]:= 'Domingo';
DiaDasemana [2]:= 'Segunda-feira';
DiaDasemana [3]:= 'Ter�a-feira';
DiaDasemana [4]:= 'Quarta-feira';
DiaDasemana [5]:= 'Quinta-feira';
DiaDasemana [6]:= 'Sexta-feira';
DiaDasemana [7]:= 'S�bado';

{ Meses do ano }
Meses [1]:= 'Janeiro';
Meses [2]:= 'Fevereiro';
Meses [3]:= 'Mar�o';
Meses [4]:= 'Abril';
Meses [5]:= 'Maio';
Meses [6]:= 'Junho';
Meses [7]:= 'Julho';
Meses [8]:= 'Agosto';
Meses [9]:= 'Setembro';
Meses [10]:= 'Outubro';
Meses [11]:= 'Novembro';
Meses [12]:= 'Dezembro';

DecodeDate (Data, Ano, Mes, Dia);
NoDia := DayOfWeek (Data);

Result := DiaDaSemana[NoDia] + ', ' + InttoStr (Dia) + ' de ' + Meses [Mes]+ ' de ' + inttostr (Ano);
end;

function IdadeAtual(Nasc : TDateTime): Integer;
// Retorna a idade Atual de uma pessoa a partir da data de nascimento
// Esta fun��o retorna a idade em formato integer
Var AuxIdade, Meses : String;
MesesFloat : Real;
IdadeInc, IdadeReal : Integer;
begin
AuxIdade := Format('%0.2f', [(Date - Nasc) / 365.6]);
Meses := FloatToStr(Frac(StrToFloat(AuxIdade)));
if AuxIdade = '0' then
begin
Result := 0;
Exit;
end;
if Meses[1] = '-' then
begin
Meses := FloatToStr(StrToFloat(Meses) * -1);
end;
Delete(Meses, 1, 2);
if Length(Meses) = 1 then
begin
Meses := Meses + '0';
end;
if (Meses <> '0') And (Meses <> '') then
begin
MesesFloat := Round(((365.6 * StrToInt(Meses)) / 100) / 30.47)
end
else
begin
MesesFloat := 0;
end;
if MesesFloat <> 12 then
begin
IdadeReal := Trunc(StrToFloat(AuxIdade)); // + MesesFloat;
end
else
begin
IdadeInc := Trunc(StrToFloat(AuxIdade));
Inc(IdadeInc);
IdadeReal := IdadeInc;
end;
Result := IdadeReal;
end;

function roundx(dValIni: Double; iCasas:Integer): Double;
// Faz o arredondamento conforme o n�mero de casas decimais
var
dDNum:Double;
begin
if dValIni >=0 then
dDNum:=((dValIni * power(10,iCasas))+0.5)
else
dDNum:=((dValIni * power(10,iCasas))-0.5);
dDNum:=Int(dDNum);
roundx:=dDNum/Power(10,iCasas);
end;

function Preenche(X, Y: String; Z:Integer):String;
// Preenche a vari�vel Y com Z vezes com o caractere X
var
Cont: Integer;
begin
for Cont := Length(X) to z do
X := X + Y;
Result := Copy(X,1,Z)
end;

function idade(Nascimento,DataAtual:string):Integer;
// Retorna a idade de uma pessoa
var
idade,dian,mesn,anon,diaa,mesa,anoa:word;
begin
decodedate(StrToDate(DataAtual),anoa,mesa,diaa);
decodedate(StrToDate(Nascimento),anon,mesn,dian);
idade := anoa - anon;
if mesn > mesa then
begin
idade := idade;
end;
if(mesn > mesa) and (dian > diaa)then
begin
idade := idade;
end;
result := idade;
end;

function FormataTel(Tel : string ; FormataMesmoAssim : boolean = true): string;
// Formata uma cadeia de strings referente ao TELEFONE
begin
 Result := Tel;
 {if formataMesmoAssim = true then
    begin
       Result :=  '(' + Copy(Tel, 1, 2) + ')' + Copy(Tel, 3, 4) + '-' + Copy(Tel, 7, 4);
    end; }
end;

function getDataExtenso(dDate: TDate): String;
var dd,mm,yy: word;
    cMonth: String;
    cDay: String;
begin
  DecodeDate(dDate,yy,mm,dd);

  Case mm of
    1: cMonth := 'Janeiro';
    2: cMonth := 'Fevereiro';
    3: cMonth := 'Mar�o';
    4: cMonth := 'Abril';
    5: cMonth := 'Maio';
    6: cMonth := 'Junho';
    7: cMonth := 'Julho';
    8: cMonth := 'Agosto';
    9: cMonth := 'Setembro';
    10: cMonth := 'Outubro';
    11: cMonth := 'Novembro';
    12: cMonth := 'Dezembro';
  end;

  Case dd of
    1: cDay := '01';
    2: cDay := '02';
    3: cDay := '03';
    4: cDay := '04';
    5: cDay := '05';
    6: cDay := '06';
    7: cDay := '07';
    8: cDay := '08';
    9: cDay := '09';
    10: cDay := '10';
    11: cDay := '11';
    12: cDay := '12';
    13: cDay := '13';
    14: cDay := '14';
    15: cDay := '15';
    16: cDay := '16';
    17: cDay := '17';
    18: cDay := '18';
    19: cDay := '19';
    20: cDay := '20';
    21: cDay := '21';
    22: cDay := '22';
    23: cDay := '23';
    24: cDay := '24';
    25: cDay := '25';
    26: cDay := '26';
    27: cDay := '27';
    28: cDay := '28';
    29: cDay := '29';
    30: cDay := '30';
  end;

  result := cDay + ' de ' + cMonth + ' de ' + InttoStr(yy);
end;


initialization
  fsRTTIModules.Add(TFastReportSysUtils);

end.
