unit uVersao;

interface

Uses Windows, SysUtils;

type TVersao = class
  class function GetExeVersion(sFileName:string): string;
end;

implementation


{ TVersao }

class function TVersao.GetExeVersion(sFileName: string): string;
var
  VerInfoSize  : DWORD;
  VerInfo      : Pointer;
  VerValueSize : DWORD;
  VerValue     : PVSFixedFileInfo;
  Dummy        : DWORD;
begin
  try
    VerInfoSize := GetFileVersionInfoSize(PChar(sFileName), Dummy);
    GetMem(VerInfo, VerInfoSize);
    GetFileVersionInfo(PChar(sFileName), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
    with VerValue^ do
    begin
      Result := IntToStr(dwFileVersionMS shr 16);
      Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
      Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
      Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
    end;
    FreeMem(VerInfo, VerInfoSize);
  except //se ocorrer um erro retorna a vers�o 1.0.0.0, vai ocoorer um erro se o arquivo n�o tiver vers�o
    Result:='Sem Vers�o';
  end;
end;

end.
