unit uMotorCalculoNotaFiscal;

interface

Uses Data.DB, SysUtils, uMotorCalculoNotaFiscalICMS, uFilialProxy, uImpostoICMSProxy, uImpostoIPIProxy,
  uImpostoPISCofinsProxy;

type TMotorCalculoNotaFiscal = class
private
  FTipoDocumentoFiscal: String;
  FTipoNotaFiscal: String;
  FDatasetNotaFiscal: TDataset;
  FDatasetProduto: TDataset;
  FFilial: TFilialProxy;

  procedure CalcularValorTotalItemGeral;
  procedure CalcularValorTotalItemICMS(const AImpostoICMS: TImpostoICMSProxy);
  procedure CalcularValorTotalItemIPI(const AImpostoIPI: TImpostoIPIProxy);
  procedure CalcularValorTotalItemPISCofins(const AImpostoPisCofins: TImpostoPisCofinsProxy);
  procedure CalcularValorTotalItemGeralEntrada;
  procedure CalcularValorTotalItemGeralSaida;
public
  procedure CalcularValorTotalItem(const AImpostoICMS: TImpostoICMSProxy; AImpostoIPI: TImpostoIPIProxy;
    AImpostoPisCofins: TImpostoPisCofinsProxy);

  procedure CalcularValorNotaFiscal;

  constructor Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String; ADatasetNotaFiscal,
      ADatasetProduto: TDataset; AFilial: TFilialProxy) overload;
end;

implementation

Uses uNotaFiscalProxy, uMathUtils, uMotorCalculoNotaFiscalCOFINS,
  uMotorCalculoNotaFiscalPIS;

constructor TMotorCalculoNotaFiscal.Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String;
  ADatasetNotaFiscal, ADatasetProduto: TDataset; AFilial: TFilialProxy);
begin
  inherited Create;

  FTipoDocumentoFiscal := ATipoDocumentoFiscal;
  FTipoNotaFiscal := ATipoNotaFiscal;
  FDatasetNotaFiscal := ADatasetNotaFiscal;
  FDatasetProduto := ADatasetProduto;
  FFilial := AFilial;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemGeral;
begin
  if FDatasetNotaFiscal.FieldByName('TIPO_NF').AsString.Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    CalcularValorTotalItemGeralSaida;
  end
  else
  begin
    CalcularValorTotalItemGeralEntrada;
  end;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemGeralEntrada;
begin
  FDatasetProduto.FieldByName('I_QTRIB').AsFloat := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
  FDatasetProduto.FieldByName('I_VUNTRIB').AsFloat := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;

  FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat :=
   (FDatasetProduto.FieldByName('I_VUNCOM').AsFloat * FDatasetProduto.FieldByName('I_QCOM').AsFloat) +
    FDatasetProduto.FieldByName('I_VSEG').AsFloat +
    FDatasetProduto.FieldByName('I_VOUTRO').AsFloat + FDatasetProduto.FieldByName('I_VFRETE').AsFloat -
    FDatasetProduto.FieldByName('I_VDESC').AsFloat + FDatasetProduto.FieldByName('O_VIPI').AsFloat +
    FDatasetProduto.FieldByName('N_VICMSST').AsFloat;

  FDatasetProduto.FieldByName('VL_CUSTO_IMPOSTO').AsFloat :=
    FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemGeralSaida;
begin
  FDatasetProduto.FieldByName('I_QTRIB').AsFloat := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
  FDatasetProduto.FieldByName('I_VUNTRIB').AsFloat := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;

  FDatasetProduto.FieldByName('VL_CUSTO_IMPOSTO').AsFloat :=
    FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat;
    //FDatasetProduto.FieldByName('I_VFRETE').AsFloat +
    //FDatasetProduto.FieldByName('N_VICMS').AsFloat +
    //FDatasetProduto.FieldByName('O_VIPI').AsFloat;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemICMS(const AImpostoICMS: TImpostoICMSProxy);
var
  motorCalculoICMS: TMotorCalculoNotaFiscalICMS;
begin
  motorCalculoICMS := TMotorCalculoNotaFiscalICMS.Create(FTipoDocumentoFiscal, FTipoNotaFiscal,
      FDatasetNotaFiscal, FDatasetProduto, FFilial, AImpostoICMS);
  try
    motorCalculoICMS.CalcularImpostoICMS;
  finally
    FreeAndNil(motorCalculoICMS);
  end;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemIPI(const AImpostoIPI: TImpostoIPIProxy);
begin
{var
  motorCalculoIPI: TMotorCalculoNotaFiscalIPI;
begin
  motorCalculoIPI := TMotorCalculoNotaFiscalIPI.Create(FTipoDocumentoFiscal, FTipoNotaFiscal,
      FDatasetNotaFiscal, FDatasetProduto, FFilial, AImpostoIPI);
  try
    motorCalculoIPI.CalcularImpostoIPI;
  finally
    FreeAndNil(motorCalculoIPI);
  end;}
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItemPISCofins(
  const AImpostoPisCofins: TImpostoPisCofinsProxy);
var
  motorCalculoPIS: TMotorCalculoNotaFiscalPIS;
  motorCalculoCofins: TMotorCalculoNotaFiscalCofins;
begin
  motorCalculoPIS := TMotorCalculoNotaFiscalPIS.Create(FTipoDocumentoFiscal, FTipoNotaFiscal,
      FDatasetNotaFiscal, FDatasetProduto, FFilial, AImpostoPISCofins);
  try
    motorCalculoPIS.CalcularImpostoPIS;
  finally
    FreeAndNil(motorCalculoPIS);
  end;

  motorCalculoCofins := TMotorCalculoNotaFiscalCofins.Create(FTipoDocumentoFiscal, FTipoNotaFiscal,
      FDatasetNotaFiscal, FDatasetProduto, FFilial, AImpostoPISCofins);
  try
    motorCalculoCofins.CalcularImpostoCofins;
  finally
    FreeAndNil(motorCalculoCofins);
  end;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorTotalItem(const AImpostoICMS: TImpostoICMSProxy;
  AImpostoIPI: TImpostoIPIProxy; AImpostoPisCofins: TImpostoPisCofinsProxy);
begin
  if Assigned(AImpostoICMS) then
  begin
    CalcularValorTotalItemICMS(AImpostoICMS);
  end;

  if Assigned(AImpostoIPI) then
  begin
    CalcularValorTotalItemIPI(AImpostoIPI);
  end;

  if Assigned(AImpostoPisCofins) then
  begin
    CalcularValorTotalItemPisCofins(AImpostoPisCofins);
  end;

  CalcularValorTotalItemGeral;
end;

procedure TMotorCalculoNotaFiscal.CalcularValorNotaFiscal;
var
   w_vprod
  ,w_vst
  ,w_vseg
  ,w_vdesc
  ,w_voutro
  ,w_vipi
  ,w_vfrete
  ,w_vicms
  ,w_vbc
  ,w_vbcst
  ,w_vnf
  ,perc_vst
  ,perc_vicms
  ,perc_vseg
  ,perc_vdesc
  ,perc_voutro
  ,perc_vipi
  ,perc_vfrete
    :Double;
begin
  w_vprod := 0;
  w_vst := 0;
  w_vseg := 0;
  w_vdesc := 0;
  w_voutro := 0;
  w_vipi := 0;
  w_vfrete := 0;
  w_vicms := 0;
  w_vbc := 0;
  w_vbcst := 0;
  w_vnf := 0;
  perc_vst := 0;
  perc_vicms := 0;
  perc_vseg := 0;
  perc_vdesc := 0;
  perc_voutro := 0;
  perc_vipi := 0;
  perc_vfrete := 0;

  FDatasetProduto.First;
  while not FDatasetProduto.Eof do
  begin
    w_vprod := w_vprod + (FDatasetProduto.FieldByName('I_VUNCOM').AsFloat *
      FDatasetProduto.FieldByName('I_QCOM').AsFloat);
    w_vst := w_vst + TMathUtils.Arredondar(FDatasetProduto.FieldByName('N_VICMSST').AsFloat);
    w_vseg := w_vseg + FDatasetProduto.FieldByName('I_VSEG').AsFloat;
    w_vdesc := w_vdesc + FDatasetProduto.FieldByName('I_VDESC').AsFloat;
    w_voutro := w_voutro + FDatasetProduto.FieldByName('I_VOUTRO').AsFloat;
    w_vipi := w_vipi + FDatasetProduto.FieldByName('O_VIPI').AsFloat;
    w_vfrete := w_vfrete + FDatasetProduto.FieldByName('I_VFRETE').AsFloat;
    w_vicms := w_vicms  + FDatasetProduto.FieldByName('N_VICMS').AsFloat;
    w_vbc := w_vbc + FDatasetProduto.FieldByName('N_VBC').AsFloat;
    w_vbcst := w_vbcst + FDatasetProduto.FieldByName('N_VBCST').AsFloat;
    w_vnf := w_vnf + FDatasetProduto.FieldByName('VL_CUSTO_IMPOSTO').AsFloat;

    FDatasetProduto.Next;
  end;

  perc_vst := TMathUtils.PercentualSobreValor(w_vst, w_vprod);
  perc_vicms := TMathUtils.PercentualSobreValor(w_vicms, w_vprod);
  perc_vseg := TMathUtils.PercentualSobreValor(w_vseg, w_vprod);
  perc_vdesc := TMathUtils.PercentualSobreValor(w_vdesc, w_vprod);
  perc_voutro := TMathUtils.PercentualSobreValor(w_voutro, w_vprod);
  perc_vipi := TMathUtils.PercentualSobreValor(w_vipi, w_vprod);
  perc_vfrete := TMathUtils.PercentualSobreValor(w_vfrete, w_vprod);

  FDatasetNotaFiscal.FieldByName('W_VBC').AsFloat := w_vbc;
  FDatasetNotaFiscal.FieldByName('W_VBCST').AsFloat := w_vbcst;
  FDatasetNotaFiscal.FieldByName('W_VPROD').AsFloat := w_vprod;
  FDatasetNotaFiscal.FieldByName('W_VST').AsFloat := w_vst;
  FDatasetNotaFiscal.FieldByName('W_VICMS').AsFloat := w_vicms;
  FDatasetNotaFiscal.FieldByName('W_VSEG').AsFloat := w_vseg;
  FDatasetNotaFiscal.FieldByName('W_VDESC').AsFloat := w_vdesc;
  FDatasetNotaFiscal.FieldByName('W_VOUTRO').AsFloat := w_voutro;
  FDatasetNotaFiscal.FieldByName('W_VIPI').AsFloat := w_vipi;
  FDatasetNotaFiscal.FieldByName('W_VFRETE').AsFloat := w_vfrete;
  FDatasetNotaFiscal.FieldByName('W_VNF').AsFloat := w_vnf + w_vst;

  FDatasetNotaFiscal.FieldByName('PERC_VST').AsFloat := perc_vst;
  //FDatasetNotaFiscal.FieldByName('PERC_VICMS').AsFloat := perc_vst;
  FDatasetNotaFiscal.FieldByName('PERC_VSEG').AsFloat := perc_vseg;
  FDatasetNotaFiscal.FieldByName('PERC_VDESC').AsFloat := perc_vdesc;
  FDatasetNotaFiscal.FieldByName('PERC_VOUTRO').AsFloat := perc_voutro;
  FDatasetNotaFiscal.FieldByName('PERC_VIPI').AsFloat := perc_vipi;
  FDatasetNotaFiscal.FieldByName('PERC_VFRETE').AsFloat := perc_vfrete;
end;

end.
