﻿unit uMotorCalculoNotaFiscalCofins;

interface

Uses Data.DB, SysUtils, uFilialProxy, uImpostoPISCofinsProxy, Math;

type TMotorCalculoNotaFiscalCofins = class
  private
    FTipoDocumentoFiscal: String;
    FTipoNotaFiscal: String;
    FDatasetNotaFiscal: TDataset;
    FDatasetProduto: TDataset;
    FFilial: TFilialProxy;
    FImpostoCofins: TImpostoPisCofinsProxy;

    function CalcularCofins(ACalularImpostoCofins: TEntradaCalculoImpostoCofinsProxy): TSaidaCalculoImpostoCofinsProxy;
  public
    procedure CalcularImpostoCofins;

    constructor Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String; ADatasetNotaFiscal,
        ADatasetProduto: TDataset; AFilialProxy: TFilialProxy; AImpostoCofins: TImpostoPisCofinsProxy) overload;
end;

implementation

uses uMathUtils;

constructor TMotorCalculoNotaFiscalCofins.Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String;
  ADatasetNotaFiscal, ADatasetProduto: TDataset; AFilialProxy: TFilialProxy;
  AImpostoCofins: TImpostoPisCofinsProxy);
begin
  inherited Create;
  FTipoDocumentoFiscal := ATipoDocumentoFiscal;
  FTipoNotaFiscal := ATipoNotaFiscal;
  FDatasetNotaFiscal := ADatasetNotaFiscal;
  FDatasetProduto := ADatasetProduto;

  FFilial := AFilialProxy;
  FImpostoCofins := AImpostoCofins;
end;

procedure TMotorCalculoNotaFiscalCofins.CalcularImpostoCofins;
var
  calcularImpostoCofinsEnvio: TEntradaCalculoImpostoCofinsProxy;
  calcularImpostoCofinsResposta: TSaidaCalculoImpostoCofinsProxy;
begin
  calcularImpostoCofinsEnvio := TEntradaCalculoImpostoCofinsProxy.Create;

  with calcularImpostoCofinsEnvio do
  begin
    FCRTEmitente := FFilial.FCrt;
    FCST := FDatasetProduto.FieldByName('N_CST').AsString;
    FImpostoCofins := Self.FImpostoCofins;
    FQuantidadeItem := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
    FValorUnitario := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;
    FValorSeguro := FDatasetProduto.FieldByName('I_VSEG').AsFloat;
    FValorOutrasDespesas := FDatasetProduto.FieldByName('I_VOUTRO').AsFloat;
    FValorDesconto := FDatasetProduto.FieldByName('I_VDESC').AsFloat;
  end;

  calcularImpostoCofinsResposta := TSaidaCalculoImpostoCofinsProxy.Create;
  try
    calcularImpostoCofinsResposta := CalcularCofins(calcularImpostoCofinsEnvio);

    FDatasetProduto.FieldByName('S_VBC').AsFloat := calcularImpostoCofinsResposta.FValorBaseCalculoCofinsCalculado;
    FDatasetProduto.FieldByName('S_VICMS').AsFloat := calcularImpostoCofinsResposta.FValorCofinsCalculado;
    FDatasetProduto.FieldByName('S_PICMS').AsFloat := calcularImpostoCofinsResposta.FPercentualCofinsCalculado;
  finally
    FreeAndNil(calcularImpostoCofinsResposta);
  end;
end;

function TMotorCalculoNotaFiscalCofins.CalcularCofins(
  ACalularImpostoCofins: TEntradaCalculoImpostoCofinsProxy): TSaidaCalculoImpostoCofinsProxy;
begin
  result := TSaidaCalculoImpostoCofinsProxy.Create;

  with ACalularImpostoCofins do
  begin
    case StrtoIntDef(ACalularImpostoCofins.FCST, 0) of
      01,
      02:
      begin
        result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

        result.FPercentualCofinsCalculado := FImpostoCofins.FPercAliquotaCofins;
        result.FValorBaseCalculoCofinsCalculado := result.FValorBaseOperacaoCalculado;
        result.FValorCofinsCalculado := {ceil}(result.FPercentualBaseCalculoCofinsCalculado * result.FPercentualCofinsCalculado) / 100;

        result.FPercentualBaseCalculoCofinsCalculado := TMathUtils.PercentualSobreValor(
          result.FValorBaseCalculoCofinsCalculado, result.FValorBaseOperacaoCalculado);

        result.FPercentualCofinsCalculado := TMathUtils.PercentualSobreValor(
          result.FValorCofinsCalculado, result.FValorBaseOperacaoCalculado);
      end;      
      03:
      begin
        { $item->imposto->PIS->CST = $tributacaoPIS->CST;
        $item->imposto->PIS->qBCProd = $produto->qTrib;
        $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS;  COMENTADO POR CAUSA DESSE CAMPO AQUI
        $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
      end;  
      04,
      05,
      06,
      07,
      08,
      09:
      begin
      end;
      else
      begin
        if (FTipoTributacaoCofins = 0) then
        begin
          result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

          result.FPercentualCofinsCalculado := FImpostoCofins.FPercAliquotaCofins;
          result.FValorBaseCalculoCofinsCalculado := result.FValorBaseOperacaoCalculado;
          result.FValorCofinsCalculado := {ceil}(result.FPercentualBaseCalculoCofinsCalculado * result.FPercentualCofinsCalculado) / 100;

          result.FPercentualBaseCalculoCofinsCalculado := TMathUtils.PercentualSobreValor(
            result.FValorBaseCalculoCofinsCalculado, result.FValorBaseOperacaoCalculado);

          result.FPercentualCofinsCalculado := TMathUtils.PercentualSobreValor(
            result.FValorCofinsCalculado, result.FValorBaseOperacaoCalculado);
        end
        else 
        begin          
          { $item->imposto->PIS->qBCProd = $produto->qTrib;
          $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS; COMENTADO POR CAUSA DESSE CAMPO AQUI
          $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
        end;  
      end;                    
    end;    
  end;
end;

end.