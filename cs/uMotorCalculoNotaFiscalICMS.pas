﻿unit uMotorCalculoNotaFiscalICMS;

interface

Uses Data.DB, SysUtils, uFilialProxy, uImpostoICMSProxy;

type TMotorCalculoNotaFiscalICMS = class
  private
    FTipoDocumentoFiscal: String;
    FTipoNotaFiscal: String;
    FDatasetNotaFiscal: TDataset;
    FDatasetProduto: TDataset;
    FFilial: TFilialProxy;
    FImpostoICMS: TImpostoICMSProxy;

    function CalcularICMS(ACalularImpostoIcms: TEntradaCalculoImpostoIcmsProxy): TSaidaCalculoImpostoIcmsProxy;
  public
    procedure CalcularImpostoICMS;

    constructor Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String; ADatasetNotaFiscal,
        ADatasetProduto: TDataset; AFilialProxy: TFilialProxy; AImpostoICMS: TImpostoICMSProxy) overload;
end;

implementation

uses uMathUtils;

constructor TMotorCalculoNotaFiscalICMS.Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String;
  ADatasetNotaFiscal, ADatasetProduto: TDataset; AFilialProxy: TFilialProxy;
  AImpostoICMS: TImpostoICMSProxy);
begin
  inherited Create;
  FTipoDocumentoFiscal := ATipoDocumentoFiscal;
  FTipoNotaFiscal := ATipoNotaFiscal;
  FDatasetNotaFiscal := ADatasetNotaFiscal;
  FDatasetProduto := ADatasetProduto;

  FFilial := AFilialProxy;
  FImpostoICMS := AImpostoICMS;
end;

procedure TMotorCalculoNotaFiscalICMS.CalcularImpostoICMS;
var
  calcularImpostoICMSEnvio: TEntradaCalculoImpostoIcmsProxy;
  calcularImpostoICMSResposta: TSaidaCalculoImpostoIcmsProxy;
begin
  calcularImpostoICMSEnvio := TEntradaCalculoImpostoIcmsProxy.Create;

  with calcularImpostoICMSEnvio do
  begin
    FCRTEmitente := FFilial.FCrt;
    FCST := FDatasetProduto.FieldByName('N_CST').AsString;
    FImpostoIcms := Self.FImpostoIcms;
    FQuantidadeItem := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
    FValorUnitario := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;
    FValorSeguro := FDatasetProduto.FieldByName('I_VSEG').AsFloat;
    FValorOutrasDespesas := FDatasetProduto.FieldByName('I_VOUTRO').AsFloat;
    FValorDesconto := FDatasetProduto.FieldByName('I_VDESC').AsFloat;
  end;

  calcularImpostoICMSResposta := TSaidaCalculoImpostoIcmsProxy.Create;
  try
    calcularImpostoICMSResposta := CalcularICMS(calcularImpostoICMSEnvio);

    FDatasetProduto.FieldByName('N_VBC').AsFloat := calcularImpostoICMSResposta.FValorBaseCalculoICMSCalculado;
    FDatasetProduto.FieldByName('N_VICMS').AsFloat := calcularImpostoICMSResposta.FValorICMSCalculado;
    FDatasetProduto.FieldByName('N_PICMS').AsFloat := calcularImpostoICMSResposta.FPercentualICMSCalculado;
    FDatasetProduto.FieldByName('N_VBCST').AsFloat := calcularImpostoICMSResposta.FValorBaseCalculoICMSSTCalculado;
    FDatasetProduto.FieldByName('N_VICMSST').AsFloat := calcularImpostoICMSResposta.FValorICMSSTCalculado;
    FDatasetProduto.FieldByName('N_PICMSST').AsFloat := calcularImpostoICMSResposta.FPercentualICMSSTCalculado;
    FDatasetProduto.FieldByName('N_PMVAST').AsFloat := calcularImpostoICMSEnvio.FImpostoIcms.FPercMvaProprio;

    //MUDAR URGENTE VAI DAR PAU QDO NAO TIVER ICMS - ISSO É MUITO PROVISORIO
    FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat := calcularImpostoICMSResposta.FValorBaseOperacaoCalculado;
    FDatasetProduto.FieldByName('I_VPROD').AsFloat := FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat;
  finally
    FreeAndNil(calcularImpostoICMSResposta);
  end;
end;

function TMotorCalculoNotaFiscalICMS.CalcularICMS(
  ACalularImpostoIcms: TEntradaCalculoImpostoIcmsProxy): TSaidaCalculoImpostoIcmsProxy;
var
  FValorBaseCalculoICMSSTMVa: Double;
begin
  result := TSaidaCalculoImpostoIcmsProxy.Create;

  with ACalularImpostoIcms do
  begin
    result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) + FValorSeguro +
      FValorOutrasDespesas - FValorDesconto;

    Case ACalularImpostoIcms.FCrtEmitente Of
      2..3 :
      Begin
        {ICMS}
        result.FValorBaseCalculoICMSCalculado := result.FValorBaseOperacaoCalculado;

        //Se tiver redução do icms, tira o percentual da base ja calculada.
        if FImpostoIcms.FPercReducaoIcms > 0 then
        begin
          result.FValorReducaoBaseCalculoICMSCalculado :=
            (result.FValorBaseOperacaoCalculado * (FImpostoIcms.FPercReducaoIcms / 100));

          result.FValorBaseCalculoICMSCalculado := result.FValorBaseCalculoICMSCalculado -
            result.FValorReducaoBaseCalculoICMSCalculado;
        end;

        result.FPercentualBaseCalculoICMSCalculado := TMathUtils.PercentualSobreValor(
          result.FValorBaseCalculoICMSCalculado, result.FValorBaseOperacaoCalculado);

        result.FValorICMSCalculado := result.FValorBaseCalculoICMSCalculado *
          (FImpostoIcms.FPercAliquotaIcms / 100);

        result.FPercentualICMSCalculado := FImpostoIcms.FPercAliquotaIcms;

        {ICMS ST}
        result.FValorBaseCalculoICMSSTCalculado := result.FValorBaseOperacaoCalculado; // + Vl_IPI

        if StrtoIntDef(ACalularImpostoIcms.FCST, 0) in [10, 30, 70, 90 {,Partilha}] then
        begin
          //Se tiver redução do icms, tira o percentual da base ja calculada.
          if FImpostoIcms.FPercReducaoIcmsST > 0 then
          begin
            result.FValorReducaoBaseCalculoICMSSTCalculado :=
              (result.FValorBaseOperacaoCalculado * (FImpostoIcms.FPercReducaoIcmsST / 100));

            result.FValorBaseCalculoICMSSTCalculado := result.FValorBaseCalculoICMSSTCalculado - result.FValorReducaoBaseCalculoICMSSTCalculado;
          end;
        end;

        if FImpostoIcms.FPercMvaProprio > 0 then
        begin
          FValorBaseCalculoICMSSTMVa :=
            (result.FValorBaseOperacaoCalculado *
            (FImpostoIcms.FPercMvaProprio / 100));
        end;

        result.FPercentualICMSSTCalculado := FImpostoIcms.FPercAliquotaIcmsST;

        result.FValorICMSSTCalculado := (result.FValorBaseCalculoICMSSTCalculado + FValorBaseCalculoICMSSTMVa) *
          (FImpostoIcms.FPercAliquotaIcmsST / 100);

        result.FValorICMSSTCalculado := result.FValorICMSSTCalculado - result.FValorICMSCalculado;

        result.FValorBaseCalculoICMSSTCalculado := result.FValorBaseCalculoICMSSTCalculado +
          (result.FValorBaseCalculoICMSSTCalculado * (FImpostoIcms.FPercMvaProprio / 100));

        //Segundo conversa com fischer, qdo CSOSN não destaca ICMS
        result.FValorBaseCalculoICMSCalculado := 0;
        result.FPercentualBaseCalculoICMSCalculado := 0;
        result.FValorICMSCalculado := 0;
        result.FPercentualICMSCalculado := 0;
      End;
    1 :
      Begin
        {ICMS}
        result.FValorBaseCalculoICMSCalculado := result.FValorBaseOperacaoCalculado;

        //Se tiver redução do icms, tira o percentual da base ja calculada.
        if FImpostoIcms.FPercReducaoIcms > 0 then
        begin
          result.FValorReducaoBaseCalculoICMSCalculado :=
            (result.FValorBaseOperacaoCalculado * (FImpostoIcms.FPercReducaoIcms / 100));

          result.FValorBaseCalculoICMSCalculado := result.FValorBaseCalculoICMSCalculado -
            result.FValorReducaoBaseCalculoICMSCalculado;
        end;

        result.FPercentualBaseCalculoICMSCalculado := TMathUtils.PercentualSobreValor(
          result.FValorBaseCalculoICMSCalculado, result.FValorBaseOperacaoCalculado);

        result.FValorICMSCalculado := result.FValorBaseCalculoICMSCalculado *
          (FImpostoIcms.FPercAliquotaIcms / 100);

        result.FPercentualICMSCalculado := FImpostoIcms.FPercAliquotaIcms;

        if FImpostoIcms.FModalidadeBaseIcmsSt >= 0 then
        begin
          {ICMS ST}
          result.FValorBaseCalculoICMSSTCalculado := result.FValorBaseOperacaoCalculado; // + Vl_IPI

          result.FPercentualICMSSTCalculado := FImpostoIcms.FPercAliquotaIcmsST;

          if FImpostoIcms.FPercMvaAjustadoSt > 0 then
          begin
            FValorBaseCalculoICMSSTMVa :=
              (result.FValorBaseCalculoICMSCalculado *
              ((100 + FImpostoIcms.FPercMvaAjustadoSt) / 100));

            result.FValorBaseCalculoICMSSTCalculado :=
              FValorBaseCalculoICMSSTMVa;
          end;

          //Se tiver redução do icms, tira o percentual da base ja calculada.
          if FImpostoIcms.FPercReducaoIcmsST > 0 then
          begin
            result.FValorReducaoBaseCalculoICMSSTCalculado :=
              (result.FValorBaseCalculoICMSSTCalculado * (FImpostoIcms.FPercReducaoIcmsST / 100));

            result.FValorBaseCalculoICMSSTCalculado :=
              result.FValorBaseCalculoICMSSTCalculado -
              result.FValorReducaoBaseCalculoICMSSTCalculado;
          end;

          result.FValorICMSSTCalculado := (result.FValorBaseCalculoICMSSTCalculado *
            (FImpostoIcms.FPercAliquotaIcmsST / 100)) -
            (result.FValorBaseCalculoICMSCalculado *
            (FImpostoIcms.FPercAliquotaIcmsProprioParaSt / 100));
        end;

        //Segundo conversa com fischer, qdo CSOSN não destaca ICMS
        result.FValorBaseCalculoICMSCalculado := 0;
        result.FPercentualBaseCalculoICMSCalculado := 0;
        result.FValorICMSCalculado := 0;
        result.FPercentualICMSCalculado := 0;
      End
    End;
  end;
end;


{

Se N13 = 0 então : BASE OPERACAO¹ * (1 + ConfigICMS.PercMVAProprio)

Se N13 = 1 então : Mercadoria.BaseCalcICMS

Se N13 = 2 então : Mercadoria.BaseCalcICMS

Se N13 = 3 então : BASE OPERACAO¹ 
_______________________________________________________
BASE OPERACAO¹: Inicialmente a base de cálculo do ICMS se dá pela seguinte soma:
(Valor Unitário * Quantidade) + Seguro + Outras Despesas – Desconto.

As situações abaixo podem adicionar valores na base de cálculo.
Venda para consumidor final (ConfigICMS.IncluirIPIBaseICMS = 1): Acréscimo do IPI na base de cálculo.
Frete fornecido pelo emitente do documento fiscal (ConfigICMS.IncluirFreteBaseICMS = 1): Acréscimo do frete na base de cálculo.

As seguintes condições podem reduzir valor da base de ICMS:
Quando N14 > 0(ICMS20): BASE OPERAÇÃO = BASE OPERAÇÃO * (1 – N14)
Quando N25 > 0(ICMSPart) :  BASE OPERAÇÃO = BASE OPERAÇÃO * (1 – N25)

}


end.