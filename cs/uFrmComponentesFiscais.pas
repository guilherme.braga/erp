unit uFrmComponentesFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ACBrCTe, ACBrNFeDANFEClass, ACBrNFeDANFEFR, ACBrNFe, ACBrNFeDANFEFRDM,
  ACBrBase, ACBrDFe, ACBrSocket, ACBrIBPTax, frxClass, uConfiguracaoFiscalProxy, uUsuarioProxy, uFIlialProxy,
  pcnConversao, uStringUtils, pcnConversaoNFe, ACBrDFeSSL, vcl.Printers, ACBrMail;

type
  TFrmComponentesFiscais = class(TForm)
    ACBrNFe: TACBrNFe;
    ACBrCTe: TACBrCTe;
    ACBrIBPTax: TACBrIBPTax;
    ACBrMail: TACBrMail;
  private

  public
    procedure AtualizarTabelaIBPT;
    procedure ValidarConfiguracoesNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
    procedure ValidarConfiguracoesNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
    procedure ConfigurarACBrNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure SalvarCopiaPDFJuntoComXML;

    class procedure InutilizarDocumentoFiscal(const ACNPJEmissor, AJustificativa: String;
      const AANo, AModelo, ASerie, ANumeroInicial, ANumeroFinal: Integer; ATipoDocumentoFiscal: String;
      AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
  end;

var
  FrmComponentesFiscais: TFrmComponentesFiscais;

implementation

{$R *.dfm}

Uses ACBrUtil, uNotaFiscalProxy;

{ TFrmComponentesFiscais }

procedure TFrmComponentesFiscais.AtualizarTabelaIBPT;
{ar
  I: Integer;
  qIBPT, qIBPTVersao: IFastQuery; }
begin
{
  // configurar a URL do arquivo para ser baixado
  // Voc� configurar como quiser, para o seu site ou servidor proprios
  // deixarei a tabela atualizada sempre no endere�o
  // https://regys.com.br/arquivos/AcspDeOlhoNoImpostoIbptV.0.0.1.csv
  {if Trim(URL) <> '' then
    ACBrIBPTax1.URLDownload := Trim(URL);}

  // se o path do arquivo n�o for passado o componente tenta baixar da URL
  // informada na propriedade URLDownload
  // Se o path for passado ele abre diretamente o arquivo informado
{  if ACBrIBPTax.DownloadTabela then
  begin
    // vers�o do arquivo
    lVersao.Caption := 'Vers�o: ' + ACBrIBPTax.VersaoArquivo;

    // exemplo de como popular uma tabela com os dados baixados
    tmpCadastro.Close;
    tmpCadastro.DisableControls;
    try
      for I := 0 to ACBrIBPTax1.Itens.Count - 1 do
      begin
        tmpCadastro.Append;
        tmpCadastroNCM.AsString              := ACBrIBPTax.Itens[I].NCM;
        tmpCadastroEx.AsString               := ACBrIBPTax.Itens[I].Excecao;
        tmpCadastroTabela.AsInteger          := Integer(ACBrIBPTax.Itens[I].Tabela);
        tmpCadastroAliqNacional.AsFloat      := ACBrIBPTax.Itens[I].AliqNacional;
        tmpCadastroAliqInternacional.AsFloat := ACBrIBPTax.Itens[I].AliqImportado;
        tmpCadastro.Post;
      end;
    finally
      tmpCadastro.First;
      tmpCadastro.EnableControls;
    end;
  end; }
end;


procedure TFrmComponentesFiscais.ConfigurarACBrNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  with ACBrNFe do
  begin
    Configuracoes.Geral.ModeloDF := moNFCe;
    Configuracoes.Geral.VersaoDF := ve310;
    Configuracoes.Geral.IncluirQRCodeXMLNFCe := true;
    Configuracoes.Geral.ExibirErroSchema := true;
    Configuracoes.Geral.FormaEmissao := teNormal;
    Configuracoes.Geral.FormatoAlerta := 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.';
    Configuracoes.Geral.IdCSC := AConfiguracaoFiscal.FNFCEIdCSC;
    Configuracoes.Geral.CSC := AConfiguracaoFiscal.FNFCECSC;
    Configuracoes.Geral.Salvar := true;
    Configuracoes.Geral.SSLLib := libCapicom;

    //ACBrNFe1.Configuracoes.Certificados.ArquivoPFX  := edtCaminho.Text;
    //ACBrNFe1.Configuracoes.Certificados.Senha       := edtSenha.Text;
    Configuracoes.Certificados.NumeroSerie := AConfiguracaoFiscal.FCertificadoNumeroSerie;

    Configuracoes.WebServices.UF := AConfiguracaoFiscal.FNFCeWebserviceUf;

    if AConfiguracaoFiscal.FNFCeWebserviceAmbiente.Equals(
      TConfiguracaoFiscalProxy.NFCE_WEBSERVICE_AMBIENTE_PRODUCAO) then
    begin
      Configuracoes.WebServices.Ambiente := taProducao;
    end
    else
    begin
      Configuracoes.WebServices.Ambiente := taHomologacao;
    end;

    Configuracoes.WebServices.Visualizar :=
      AConfiguracaoFiscal.FNFCeWebserviceVisualizarMensagem.Equals('S');

    ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal, AFilial, AUsuario);

    Configuracoes.WebServices.ProxyHost := AConfiguracaoFiscal.FNFCeProxyHost;
    Configuracoes.WebServices.ProxyPort := InttoStr(AConfiguracaoFiscal.FNFCeProxyPorta);
    Configuracoes.WebServices.ProxyUser := AConfiguracaoFiscal.FNFCeProxyUsuario;
    Configuracoes.WebServices.ProxyPass := AConfiguracaoFiscal.FNFCeProxySenha;

    with Configuracoes.Arquivos do
    begin
      Salvar             := true;
      SepararPorMes      := true;
      AdicionarLiteral   := true;
      EmissaoPathNFe     := true;
      SalvarEvento       := true;
      SepararPorCNPJ     := true;
      SepararPorModelo   := true;
      PathSalvar         := AConfiguracaoFiscal.FArquivoPathLogs;
      PathSchemas        := AConfiguracaoFiscal.FArquivoPathSchemas;
      PathNFe            := AConfiguracaoFiscal.FArquivoPathNfe;
      PathInu            := AConfiguracaoFiscal.FArquivoPathInutilizacao;
      PathEvento         := AConfiguracaoFiscal.FArquivoPathEvento;
    end;
  end;
end;

procedure TFrmComponentesFiscais.ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  ACBrNFe.DANFE := TACBrNFeDANFEFR.Create(ACBrNFe);
  with TACBrNFeDANFEFR(ACBrNFe.DANFE) do
  begin
    PathPDF := AConfiguracaoFiscal.FArquivoPathPDF;

    TipoDANFE := tiNFCe;
    //dmDanfe := TACBrNFeFRClass.Create(FACBrNFE.DANFE);

    FastFile := AConfiguracaoFiscal.FArquivoRelatorioNFCEFR;
    FastFileEvento := AConfiguracaoFiscal.FArquivoEventoNFCEFR;
    Impressora := AConfiguracaoFiscal.FImpressoraNFCE;

    imprimirtotalliquido := true;

    CasasDecimais._vUnCom := 2;
    CasasDecimais._qCom := 2;

    mostrarpreview := AConfiguracaoFiscal.FNFCEPreview.Equals('S');

    MargemInferior := 0;
    MargemDireita := 0;

    NumCopias := 1;

    Usuario := AUsuario.nome;
  end
end;

procedure TFrmComponentesFiscais.ConfigurarACBrNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  with ACBrNFe do
  begin
    Configuracoes.Geral.ExibirErroSchema := true;
    Configuracoes.Geral.FormatoAlerta := 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.';
    Configuracoes.Geral.SSLLib := libCapicom;

    Configuracoes.Geral.ModeloDF := moNFe;
    Configuracoes.Geral.VersaoDF := ve310;

    Configuracoes.Certificados.NumeroSerie := AConfiguracaoFiscal.FCertificadoNumeroSerie;

    if AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_NORMAL) then
    begin
      Configuracoes.Geral.FormaEmissao := teNormal;
    end
    else if AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_CONTINGENCIA) then
    begin
      Configuracoes.Geral.FormaEmissao := teContingencia;
    end
    else if AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_DPEC) then
    begin
      Configuracoes.Geral.FormaEmissao := teDPEC;
    end
    else if AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_SCAN) then
    begin
      Configuracoes.Geral.FormaEmissao := teSCAN;
    end
    else if AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_FSDA) then
    begin
      Configuracoes.Geral.FormaEmissao := teFSDA;
    end;

    Configuracoes.Geral.Salvar := true;

    //Configuracoes.Geral.PathSalvar := AConfiguracaoFiscal.FNfeArquivoEnvioResposta;
    //Configuracoes.Geral.PathSchemas := AConfiguracaoFiscal.FNFePathSchemas;

    Configuracoes.WebServices.UF := AConfiguracaoFiscal.FNfeWebserviceUf;

    if AConfiguracaoFiscal.FNfeWebserviceAmbiente.Equals(
      TConfiguracaoFiscalProxy.NFE_WEBSERVICE_AMBIENTE_PRODUCAO) then
    begin
      Configuracoes.WebServices.Ambiente := taProducao;
    end
    else
    begin
      Configuracoes.WebServices.Ambiente := taHomologacao;
    end;

    Configuracoes.WebServices.Visualizar :=
      AConfiguracaoFiscal.FNfeWebserviceVisualizarMensagem.Equals('S');

    ConfigurarACBrNFEDanfe(AConfiguracaoFiscal, AFilial, AUsuario);

    Configuracoes.WebServices.ProxyHost := AConfiguracaoFiscal.FNfeProxyHost;
    Configuracoes.WebServices.ProxyPort := InttoStr(AConfiguracaoFiscal.FNfeProxyPorta);
    Configuracoes.WebServices.ProxyUser := AConfiguracaoFiscal.FNfeProxyUsuario;
    Configuracoes.WebServices.ProxyPass := AConfiguracaoFiscal.FNfeProxySenha;

    with Configuracoes.Arquivos do
    begin
      Salvar             := true;
      SepararPorMes      := true;
      AdicionarLiteral   := true;
      EmissaoPathNFe     := true;
      SalvarEvento       := true;
      SepararPorCNPJ     := true;
      SepararPorModelo   := true;
      PathSalvar         := AConfiguracaoFiscal.FArquivoPathLogs;
      PathSchemas        := AConfiguracaoFiscal.FArquivoPathSchemas;
      PathNFe            := AConfiguracaoFiscal.FArquivoPathNfe;
      PathInu            := AConfiguracaoFiscal.FArquivoPathInutilizacao;
      PathEvento         := AConfiguracaoFiscal.FArquivoPathEvento;
    end;
  end;
end;

procedure TFrmComponentesFiscais.ConfigurarACBrNFEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin

  ACBrNFe.DANFE := TACBrNFeDANFEFR.Create(ACBrNFe);
  with TACBrNFeDANFEFR(ACBrNFe.DANFE) do
  begin
    PathPDF := AConfiguracaoFiscal.FArquivoPathPDF;

    FastFile := AConfiguracaoFiscal.FArquivoRelatorioNFEFR;
    FastFileEvento := AConfiguracaoFiscal.FArquivoEventoNFEFR;
    Impressora := AConfiguracaoFiscal.FImpressoraNFE;

    if AConfiguracaoFiscal.FNfeModoImpressao.Equals(
      TConfiguracaoFiscalProxy.NFE_MODO_IMPRESSAO_RETRATO) then
    begin
      TipoDANFE := tiRetrato;
    end
    else
    begin
      TipoDANFE := tiPaisagem;
    end;

    //Ajustar l�gica de logomarca na danfe depois
    {If AConfiguracaoFiscal.FLogomarca Then
    Begin
      DANFE.Logo := cDiretorioPrograma+'\Logo_nfe.jpeg';
      DANFE.ExpandirLogoMarca := AConfiguracaoFiscal.FExpandirLogomarca.Equals('S');
    End;}

    {  try
        DmGeral.DatabaseFR.Disconnect;
        DmGeral.Conectar_Base_FR;

        qAuxiliar := FactoryQuery('SELECT * FROM tab_formulario_fr WHERE cod_formulario_fr = '+qConfig2.FieldbyName('nfe_caminho_arq_fast').AsString, DmGeral.DatabaseFR);
        TFormulario_Fr.SaveTempFile(qAuxiliar);
        FastFile := TFormulario_Fr.GetTempFileName(qAuxiliar);
      finally
        FreeAndNil(qAuxiliar);
      end;}

    imprimirtotalliquido := true;

    CasasDecimais._vUnCom := 2;
    CasasDecimais._qCom := 2;

    mostrarpreview := AConfiguracaoFiscal.FNFEPreview.Equals('S');

    MargemInferior := 1.0;
    MargemDireita := 0.6;

    Fax := AFilial.FTelefone;
    Sistema := 'Gest�o Empresarial | Kratos Tecnologia';
    site := AFilial.FSite;
    email := AFilial.FEmail;

    {if qConfig.FieldByName('nfe_geral_danfe').AsInteger = 0 then
      begin
        MargemInferior := qConfig.FieldByName('nfe_margem_inferior').AsFloat;
        MargemDireita := qConfig.FieldByName('nfe_margem_direita').AsFloat;
      end
    else
    begin
      MargemInferior := iif(qConfig.FieldByName('nfe_margem_inferior').AsFloat > 0, qConfig.FieldByName('nfe_margem_inferior').AsFloat, 1.0);
      MargemDireita := iif(qConfig.FieldByName('nfe_margem_direita').AsFloat > 0, qConfig.FieldByName('nfe_margem_direita').AsFloat, 0.6);
    end;}

    NumCopias := 1;

    Usuario := AUsuario.nome;
  end
end;

class procedure TFrmComponentesFiscais.InutilizarDocumentoFiscal(const ACNPJEmissor, AJustificativa: String;
  const AANo, AModelo, ASerie, ANumeroInicial, ANumeroFinal: Integer; ATipoDocumentoFiscal: String;
  AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
var
  componentesFiscais: TFrmComponentesFiscais;
begin
  Application.CreateForm(TFrmComponentesFiscais, componentesFiscais);
  try
    if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
    begin
      componentesFiscais.ConfigurarACBrNFE(AConfiguracaoFiscal, AFilial, AUsuario);
    end
    else if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
    begin
      componentesFiscais.ConfigurarACBrNFCE(AConfiguracaoFiscal, AFilial, AUsuario);
    end;

    componentesFiscais.ACBrNFE.WebServices.Inutiliza(ACNPJEmissor, AJustificativa, AANo, AModelo,
      ASerie, ANumeroInicial, ANumeroFinal);
  finally
    FreeAndNil(componentesFiscais);
  end;
end;

procedure TFrmComponentesFiscais.SalvarCopiaPDFJuntoComXML;
var
  diretorio: String;
begin
  { ==========   ATEN��O   ===============
    Esse trecho foi extraido e adaptado do m�todo ACBrNFeNotasFiscais.NotaFiscal.CalcularPathArquivo: String;
    para que seja possivel salvar o PDF na mesma pasta em q � salvo a danfe.xml
  }

  diretorio := PathWithDelim(ACBrNFE.Configuracoes.Arquivos.GetPathNFe(ACBrNFE.NotasFiscais[0].NFe.Ide.dEmi,
    ACBrNFE.NotasFiscais[0].NFe.Emit.CNPJCPF, ACBrNFE.NotasFiscais[0].NFe.Ide.modelo));

  if not DirectoryExists(diretorio) then
  begin
    exit;
  end;

  ACbrNFE.DANFE.PathPDF := diretorio;
  ACbrNFE.NotasFiscais.ImprimirPDF;
end;

procedure TFrmComponentesFiscais.ValidarConfiguracoesNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
begin
  //Validar Configura��es NFCE
end;

procedure TFrmComponentesFiscais.ValidarConfiguracoesNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
begin
  //Validar Configura��es NFE
end;

end.
