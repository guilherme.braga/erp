unit uLoteRecebimentoServ;

interface

uses
  Data.FireDACJSONReflect, uFactoryQuery;

type TLoteRecebimentoServ = class
  class function RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
  class function GerarLancamentosAgrupados(AIdLoteRecebimento: Integer): string;
  class function RemoverLancamentosAgrupados(AIdLoteRecebimento: Integer;
    AGerarContraPartida: Boolean): string;
  class function GetIdLoteRecebimentoPelaChaveProcesso(AChaveProcesso : Integer): Integer;
  class function GetContasAReceber(AIdsContaReceber : string): TFDJSONDataSets;
  class procedure AtualizarLoteRecebimento(const AIdLoteRecebimento: Integer; const AStatus: String);
  private
  class function GerarMovimentacaoContaCorrente(ALote, ALoteQuitacao: TFastQuery): Boolean; static;
  class function GerarQuitacaoDesconto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlDesconto: Double): Boolean; static;
  class function GerarQuitacaoJuros(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlJuros: Double): Boolean; static;
  class function GerarQuitacaoMulta(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlMulta: Double): Boolean; static;
  class function GerarQuitacaoValorAberto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlQuitacao: Double): Boolean; static;
end;

implementation

{ TLoteRecebimentoServ }

uses
  uLoteRecebimentoProxy,
  uContaReceberServ,
  uContaReceberProxy,
  uContaCorrenteProxy,
  uMathUtils,
  uContaCorrenteServ,
  REST.Json,
  System.SysUtils, uTipoQuitacaoProxy, uTipoQuitacaoServ, uDatasetUtilsServ;

class procedure TLoteRecebimentoServ.AtualizarLoteRecebimento(
  const AIdLoteRecebimento: Integer; const AStatus: String);
begin
  if AStatus = TLoteRecebimentoProxy.STATUS_ABERTO then
  begin
    TFastQuery.ExecutarScript('UPDATE lote_recebimento SET status = :status,'+
    ' DH_EFETIVACAO = null WHERE id = :id_lote_recebimento',
    TArray<Variant>.Create(AStatus, AIdLoteRecebimento));
  end
  else if AStatus = TLoteRecebimentoProxy.STATUS_EFETIVADO then
  begin
    TFastQuery.ExecutarScript('UPDATE lote_recebimento SET status = :status,'+
    ' DH_EFETIVACAO = CURRENT_TIMESTAMP WHERE id = :id_lote_recebimento',
    TArray<Variant>.Create(AStatus, AIdLoteRecebimento));
  end;
end;

class function TLoteRecebimentoServ.GerarLancamentosAgrupados(
  AIdLoteRecebimento: Integer): string;
const

  SQLContaReceber = ' SELECT cr.*                  ' +
                  '   FROM conta_receber cr        ' +
                  '  WHERE cr.id = :IdContaReceber ';

  SQLLoteRecebimento = ' SELECT lr.*                      ' +
                     '   FROM lote_recebimento lr         ' +
                     '  WHERE lr.id = :PIdLoteRecebimento ';

  SQLLoteRecebimentoTitulo = ' SELECT lrt.*                                             ' +
                           '   FROM lote_recebimento_titulo lrt                         ' +
                           '  INNER JOIN conta_receber cr ON lrt.id_conta_receber = cr.id ' +
                           '  WHERE lrt.id_lote_recebimento = :PIdLoteRecebimento         ' +
                           '  ORDER BY cr.dt_vencimento ';

  SQLLoteRecebimentoQuitacao = ' SELECT lrq.*                       ' +
                             '   FROM lote_recebimento_quitacao lrq ' +
                             '  WHERE lrq.id_lote_Recebimento = :PIdLoteRecebimento  ';
var
  fqLote         : TFastQuery;
  fqLoteTitulos  : TFastQuery;
  fqLoteQuitacao : TFastQuery;

  valorJuros: Double;
  valorMulta: Double;
  valorDesconto: Double;
  percentualDesconto: Double;
  valorQuitacao: Double;

  valorJurosLR: Double;
  valorMultaLR: Double;
  ValorDescontoLR: Double;
  ValorTotalLR: Double;
  ValorQuitacaoLR: Double;
  ValorTotalLRAberto: Double;

  tipoQuitacao: String;
begin
   Result := TLoteRecebimentoProxy.RETORNO_SUCESSO;

  fqLote := TFastQuery.ModoDeConsulta(SQLLoteRecebimento, TArray<Variant>.Create(AIdLoteRecebimento));
  try
    fqLoteTitulos := TFastQuery.ModoDeConsulta(SQLLoteRecebimentoTitulo, TArray<Variant>.Create(AIdLoteRecebimento));
    try
      fqLoteQuitacao := TFastQuery.ModoDeConsulta(SQLLoteRecebimentoQuitacao, TArray<Variant>.Create(AIdLoteRecebimento));
      try
        ValorQuitacaoLR := fqLote.GetAsFloat('VL_QUITADO'); //Bruto
        valorJurosLR := TDatasetUtilsServ.SomarColuna(fqLoteQuitacao.FieldByName('VL_JUROS'));
        valorMultaLR := TDatasetUtilsServ.SomarColuna(fqLoteQuitacao.FieldByName('VL_MULTA'));
        ValorDescontoLR := fqLote.GetAsFloat('VL_DESCONTO');
        ValorTotalLR := fqLote.GetAsFloat('VL_QUITADO_LIQUIDO'); //liquido
        ValorTotalLRAberto := ValorTotalLR - valorJurosLR - valorMultaLR + ValorDescontoLR;

        if ValorQuitacaoLR <= 0 then
        begin
          exit;
        end;

        {Realizar o recebimento do Multa}
        if valorMultaLR > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorMulta := TContaReceberServ.GetValorMulta(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
            if (valorMulta > 0) then
            begin
              if (valorMulta > valorMultaLR) then
              begin
                valorMulta := valorMultaLR;
              end;

              GerarQuitacaoMulta(fqLote, fqLoteQuitacao, fqLoteTitulos, valorMulta);
              valorMultaLR := valorMultaLR - valorMulta;
            end;

            if valorMultaLR <= 0 then
            begin
              break;
            end;

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o recebimento do Juros}
        if valorJurosLR > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorJuros := TContaReceberServ.GetValorJuros(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
            if (valorJuros > 0) then
            begin
              if (valorJuros > valorJurosLR) then
              begin
                valorJuros := valorJurosLR;
              end;

              GerarQuitacaoJuros(fqLote, fqLoteQuitacao, fqLoteTitulos, valorJuros);
              valorJurosLR := valorJurosLR - valorJuros;
            end;

            if valorJurosLR <= 0 then
            begin
              break;
            end;

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o recebimento do valor do desconto}
        if valorDescontoLR > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorQuitacao := TContaReceberServ.GetValorAberto(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
            percentualDesconto := ((valorDescontoLR * 100) / ValorQuitacaoLR); //proporcional
            valorDesconto := TMathUtils.ValorSobrePercentual(percentualDesconto, valorQuitacao); //valor do percentual

            GerarQuitacaoDesconto(fqLote, fqLoteQuitacao, fqLoteTitulos, valorDesconto);

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o recebimento do valor em aberto}
        fqLoteTitulos.First;
        while not fqLoteTitulos.Eof do
        begin
          valorQuitacao := TContaReceberServ.GetValorAberto(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
          if (valorQuitacao > 0) then
          begin
            if (valorQuitacao > ValorTotalLRAberto) then
            begin
              valorQuitacao := ValorTotalLRAberto;
            end;

            GerarQuitacaoValorAberto(fqLote, fqLoteQuitacao, fqLoteTitulos, valorQuitacao);
            ValorTotalLRAberto := ValorTotalLRAberto - valorQuitacao;
          end;

          if ValorTotalLRAberto <= 0 then
          begin
            break;
          end;

          fqLoteTitulos.Next;
        end;

        {Realiza a gera��o da movimenta��o do conta corrente}
        fqLoteQuitacao.First;
        while not fqLoteQuitacao.Eof do
        begin
          GerarMovimentacaoContaCorrente(fqLote, fqLoteQuitacao);
          fqLoteQuitacao.Proximo;
        end;

        AtualizarLoteRecebimento(AIdLoteRecebimento, TLoteRecebimentoProxy.STATUS_EFETIVADO);
      finally
        FreeAndNil(fqLoteQuitacao);
      end;
    finally
      FreeAndNil(fqLoteTitulos);
    end;
  finally
    FreeAndNil(fqLote);
  end;
end;

class function TLoteRecebimentoServ.GerarQuitacaoMulta(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlMulta: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
begin
  Quitacao  := TContaReceberQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaReceber       := ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := AVlMulta;
    Quitacao.FVlMulta            := AVlMulta;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_LOTE_RECEBIMENTO;
    Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente a multa informada no lote de recebimento';

    TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaReceberServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
  finally
    Quitacao.Free;
  end;
end;

class function TLoteRecebimentoServ.GerarQuitacaoJuros(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlJuros: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
begin
  Quitacao  := TContaReceberQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaReceber       := ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := AVlJuros;
    Quitacao.FVlJuros            := AVlJuros;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_LOTE_RECEBIMENTO;
    Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao juros informado no lote de recebimento';

    TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaReceberServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
  finally
    Quitacao.Free;
  end;
end;

class function TLoteRecebimentoServ.GerarQuitacaoDesconto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlDesconto: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
begin
  Quitacao  := TContaReceberQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaReceber       := ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := AVlDesconto;
    Quitacao.FVlAcrescimo        := 0;
    Quitacao.FVlJuros            := 0;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_LOTE_RECEBIMENTO;
    Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao desconto informado no lote de recebimento';

    TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaReceberServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
  finally
    Quitacao.Free;
  end;
end;

class function TLoteRecebimentoServ.GerarQuitacaoValorAberto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlQuitacao: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
begin
  Quitacao  := TContaReceberQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaReceber       := ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := 0;
    Quitacao.FVlJuros            := 0;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := AVlQuitacao;
    Quitacao.FVlTotal            := AVlQuitacao;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_LOTE_RECEBIMENTO;
    Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao valor em aberto informado no lote de recebimento';

    TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaReceberServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));
  finally
    Quitacao.Free;
  end;
end;

class function TLoteRecebimentoServ.GerarMovimentacaoContaCorrente(ALote, ALoteQuitacao: TFastQuery): Boolean;
var
  Movimento: TContaCorrenteMovimento;
  tipoQuitacao: String;
begin
  try
    Movimento := TContaCorrenteMovimento.Create;

    Movimento.FDocumento         := ALoteQuitacao.GetAsString('DOCUMENTO');
    Movimento.FDescricao         := ALoteQuitacao.GetAsString('DESCRICAO');

    if Movimento.FDocumento.IsEmpty then
    begin
      Movimento.FDocumento := 'LP '+ALote.GetAsString('ID');
    end;

    if Movimento.FDescricao.IsEmpty then
    begin
      Movimento.FDescricao := 'LOTE DE RECEBIMENTO N�'+ALote.GetAsString('ID');
    end;

    Movimento.FObservacao        := ALoteQuitacao.GetAsString('OBSERVACAO');
    Movimento.FParcelamento      := '1/1';
    Movimento.FDhCadastro        := DateTimeToStr(Now);
    Movimento.FDtEmissao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtMovimento       := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtCompetencia     := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FVlMovimento       := ALoteQuitacao.GetAsFloat('VL_TOTAL');
    Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
    Movimento.FIdContaCorrente   := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Movimento.FIdContaAnalise    := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Movimento.FIdCentroResultado := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Movimento.FIdChaveProcesso   := ALote.GetAsInteger('ID_CHAVE_PROCESSO');
    Movimento.FIdFilial          := ALote.GetAsInteger('ID_FILIAL');
    Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_LOTE_RECEBIMENTO;
    Movimento.FIdDocumentoOrigem := ALote.GetAsInteger('ID');

    //Se for cheque a concilia��o � falsa
    tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO'));
    if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
      (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
    begin
      Movimento.FBoCheque := 'S';
      Movimento.FBoConciliado := 'N';

      //Dados do Cheque
      Movimento.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
      Movimento.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
      Movimento.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
      Movimento.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
      Movimento.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
      Movimento.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
      Movimento.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
      Movimento.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');
    end
    else
    begin
      Movimento.FBoConciliado := 'N';
    end;

    //Dados do Cart�o
    Movimento.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
  finally
    Movimento.Free;
  end;
end;

class function TLoteRecebimentoServ.RemoverLancamentosAgrupados(AIdLoteRecebimento: Integer;
  AGerarContraPartida: Boolean): string;
const
  SQLMovimentos = ' SELECT ccm.id                                  ' +
                  '   FROM conta_corrente_movimento ccm            ' +
                  '  WHERE ccm.id_documento_origem = :id_origem    ' +
                  '    and ccm.bo_processo_amortizado = ''N''      '+
                  '    AND ccm.tp_documento_origem = :tp_documento ';

  SQLQuitacoes  = ' SELECT crq.id                                  ' +
                  '       ,crq.id_conta_receber                    ' +
                  '   FROM conta_receber_quitacao crq              ' +
                  '  WHERE crq.id_documento_origem = :id_origem    ' +
                  '    AND crq.tp_documento_origem = :tp_documento ';

var
  fqMovimentos : TFastQuery;
  fqQuitacoes  : TFastQuery;
begin
  fqMovimentos := TFastQuery.ModoDeConsulta(SQLMovimentos
  ,TArray<Variant>.Create(AIdLoteRecebimento,TContaCorrenteMovimento.ORIGEM_LOTE_RECEBIMENTO));

  while not fqMovimentos.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqMovimentos.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqMovimentos.GetAsInteger('ID'));
    end;

    fqMovimentos.Proximo;
  end;

  fqQuitacoes  := TFastQuery.ModoDeConsulta(SQLQuitacoes
                                           ,TArray<Variant>.Create(AIdLoteRecebimento
                                                                  ,TContaReceberQuitacaoMovimento.ORIGEM_LOTE_Recebimento));
  while not fqQuitacoes.Eof do
  begin
    TContaReceberServ.RemoverContaReceberQuitacaoPorID(fqQuitacoes.GetAsInteger('ID'));
    TContaReceberServ.AtualizarValores(fqQuitacoes.GetAsInteger('ID_CONTA_RECEBER'));
    fqQuitacoes.Proximo;
  end;

  AtualizarLoteRecebimento(AIdLoteRecebimento, TLoteRecebimentoProxy.STATUS_ABERTO);

  Result := TLoteRecebimentoProxy.RETORNO_SUCESSO;
end;

{class function TLoteRecebimentoServ.GerarLancamentosIndividuais(
  AIdLoteRecebimento: Integer): string;
const

  SQLContaReceber = 'SELECT cr.*                  ' +
                    '  FROM conta_receber cr        ' +
                    ' WHERE cr.id = :IdContaReceber ';

  SQLLoteRecebimento = 'SELECT lr.*                      ' +
                       '  FROM lote_recebimento lr         ' +
                       ' WHERE lr.id = :PIdLoteRecebimento ';

  SQLLoteRecebimentoTitulo = ' SELECT lrt.*                                             ' +
                             '   FROM lote_recebimento_titulo lrt                         ' +
                             '  INNER JOIN conta_receber cr ON lrt.id_conta_receber = cr.id ' +
                             '  WHERE lrt.id_lote_Recebimento = :PIdLoteRecebimento         ' +
                             '  ORDER BY cr.dt_vencimento ';

  SQLLoteRecebimentoQuitacao = ' SELECT lrq.*                       ' +
                               '   FROM lote_recebimento_quitacao lrq ' +
                               '  WHERE lrq.id = :PIdLoteRecebimento  ';

var
  fqLote         : TFastQuery;
  fqLoteTitulos  : TFastQuery;
  fqLoteQuitacao : TFastQuery;
  fqContaReceber   : TFastQuery;

  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;

  QuitacaoJSON   : string;
  MovimentoJSON  : string;

  ValorQuitacaoLR  : Double;
  ValorQuitacaoCR  : Double;
  tipoQuitacao: String;
begin

  Result := TLoteRecebimentoProxy.RETORNO_SUCESSO;

  fqLoteTitulos  := TFastQuery.ModoDeConsulta(SQLLoteRecebimentoTitulo, TArray<Variant>.Create(AIdLoteRecebimento));
  fqLote         := TFastQuery.ModoDeConsulta(SQLLoteRecebimento, TArray<Variant>.Create(AIdLoteRecebimento));
  fqLoteQuitacao := TFastQuery.ModoDeConsulta(SQLLoteRecebimentoQuitacao, TArray<Variant>.Create(AIdLoteRecebimento));

  while not fqLoteQuitacao.Eof do
  begin
    ValorQuitacaoLR := fqLoteQuitacao.GetAsFloat('VL_QUITACAO');
    while not fqLoteTitulos.Eof do
    begin
      fqContaReceber := TFastQuery.ModoDeConsulta(SQLContaReceber, TArray<Variant>.Create(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER')));
      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      Movimento := TContaCorrenteMovimento.Create;
      try

        Quitacao.FIdContaReceber  := fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER');
        Quitacao.FDhCadastro      := fqLote.GetAsString('DH_EFETIVACAO');
        Quitacao.FDtQuitacao      := fqLote.GetAsString('DH_EFETIVACAO');
        Quitacao.FObservacao      := fqLote.GetAsString('OBSERVACAO');
        Quitacao.FIdFilial        := fqLote.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := fqLoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
        Quitacao.FIdContaCorrente := fqLoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
        Quitacao.FIdCentroResultado  := fqContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
        Quitacao.FIdContaAnalise  := fqContaReceber.GetAsInteger('ID_CONTA_ANALISE');
        Quitacao.FVlDesconto      := fqLoteTitulos.GetAsFloat('VL_DESCONTO');
        Quitacao.FVlAcrescimo     := fqLoteTitulos.GetAsFloat('VL_ACRESCIMO');

        if ValorQuitacaoLR >= (fqContaReceber.GetAsFloat('VL_ABERTO') - fqLoteTitulos.GetAsFloat('VL_DESCONTO'))then
        begin
          ValorQuitacaoCR := (fqContaReceber.GetAsFloat('VL_ABERTO') - fqLoteTitulos.GetAsFloat('VL_DESCONTO'));
          ValorQuitacaoLR := ValorQuitacaoLR - ValorQuitacaoCR;
        end
        else
        begin
          ValorQuitacaoCR := ValorQuitacaoLR;
          ValorQuitacaoLR := 0;
        end;

        Quitacao.FVlQuitacao        := ValorQuitacaoCR;
        Quitacao.FVlTotal           := ValorQuitacaoCR + fqLoteTitulos.GetAsFloat('VL_ACRESCIMO');
        Quitacao.FPercAcrescimo     := TMathUtils.PercentualSobreValor(fqLoteTitulos.GetAsFloat('VL_ACRESCIMO'),
                                                                        fqContaReceber.GetAsFloat('VL_ABERTO'));
        Quitacao.FPercDesconto      := TMathUtils.PercentualSobreValor(fqLoteTitulos.GetAsFloat('VL_DESCONTO'),
                                                                        fqContaReceber.GetAsFloat('VL_ABERTO'));
        Quitacao.FIdDocumentoOrigem := fqLote.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_LOTE_Recebimento;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := fqLoteQuitacao.GetAsString('CHEQUE_SACADO');
        Quitacao.FChequeDocFederal    := fqLoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
        Quitacao.FChequeBanco         := fqLoteQuitacao.GetAsString('CHEQUE_BANCO');
        Quitacao.FChequeDtEmissao     := fqLoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
        Quitacao.FChequeAgencia       := fqLoteQuitacao.GetAsString('CHEQUE_AGENCIA');
        Quitacao.FChequeContaCorrente := fqLoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
        Quitacao.FChequeNumero        := fqLoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

        //Dados do Cart�o
        Quitacao.FIdOperadoraCartao   := fqLoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

        QuitacaoJSON := TJson.ObjectToJsonString(Quitacao);
        TContaReceberServ.GerarContaReceberQuitacao(QuitacaoJSON);
        TContaReceberServ.AtualizarValores(fqLoteTitulos.GetAsInteger('ID_CONTA_RECEBER'));

        Movimento.FDocumento         := fqLoteQuitacao.GetAsString('DOCUMENTO');
        Movimento.FDescricao         := fqLoteQuitacao.GetAsString('DESCRICAO');
        Movimento.FObservacao        := Quitacao.FObservacao;
        Movimento.FDhCadastro        := Quitacao.FDtQuitacao;
        Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
        Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
        Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
        Movimento.FVlMovimento       := Quitacao.FVlTotal;
        Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
        Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
        Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
        Movimento.FIdCentroResultado := fqContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
        Movimento.FIdChaveProcesso   := fqLote.GetAsInteger('ID_CHAVE_PROCESSO');
        Movimento.FIdFilial          := fqLote.GetAsInteger('ID_FILIAL');
        Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_LOTE_RECEBIMENTO;
        Movimento.FIdDocumentoOrigem := fqLote.GetAsInteger('ID');

        //Se for cheque a concilia��o � falsa
        tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(Quitacao.FIdTipoQuitacao);
        if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
          (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
        begin
          Movimento.FBoCheque := 'S';
          Movimento.FBoConciliado := 'N';

          //Dados do Cheque
          Movimento.FChequeSacado        := Quitacao.FChequeSacado;
          Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
          Movimento.FChequeBanco         := Quitacao.FChequeBanco;
          Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
          Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
          Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
          Movimento.FChequeNumero        := Quitacao.FChequeNumero;
        end
        else
        begin
          Movimento.FBoConciliado := 'S';
        end;

        //Dados do Cart�o
        Movimento.FIdOperadoraCartao   := Quitacao.FIdOperadoraCartao;

        MovimentoJSON := TJson.ObjectToJsonString(Movimento);
        TContaCorrenteServ.GerarMovimentoContaCorrente(MovimentoJSON);
      finally
        Movimento.Free;
        Quitacao.Free;
      end;

      if ValorQuitacaoLR > 0 then
      begin
        fqLoteTitulos.Proximo;
      end
      else
      begin
        Break;
      end;
    end;
    fqLoteQuitacao.Proximo;
  end;
end;        }

class function TLoteRecebimentoServ.GetContasAReceber(
  AIdsContaReceber: string): TFDJSONDataSets;
const
  SQL = 'SELECT * FROM conta_receber cr WHERE cr.id IN (:ids)';
var
  fqContaReceber : IFastQuery;
begin

  Result := TFDJSONDataSets.Create;

  fqContaReceber := TFastQuery.ModoDeConsulta(SQL,
                   TArray<Variant>.Create(AIdsContaReceber));

  TFDJSONDataSetsWriter.ListAdd(Result, (fqContaReceber as TFastQuery));

end;

class function TLoteRecebimentoServ.GetIdLoteRecebimentoPelaChaveProcesso(
  AChaveProcesso: Integer): Integer;
const
  SqlLoteRecebimento = 'SELECT lr.id FROM lote_recebimento lr WHERE lr.id_chave_processo = :idChave ';
var
  fqLoteRecebimento : TFastQuery;
begin
  fqLoteRecebimento := TFastQuery.ModoDeConsulta(SqlLoteRecebimento
                                              ,TArray<Variant>.Create(AChaveProcesso));

  Result := fqLoteRecebimento.GetAsInteger('ID');

end;

class function TLoteRecebimentoServ.RemoverQuitacoesIndividuais(
  AIdLoteRecebimento: Integer): string;
const
  SQLQuitacoes  = ' SELECT crq.id                                  ' +
                  '       ,crq.id_conta_receber                      ' +
                  '   FROM conta_receber_quitacao crq                ' +
                  '  WHERE crq.id_documento_origem = :id_origem    ' +
                  '    AND crq.tp_documento_origem = :tp_documento ';
var
  fqQuitacoes  : TFastQuery;
begin

  Result := TLoteRecebimentoProxy.RETORNO_SUCESSO;

  fqQuitacoes  := TFastQuery.ModoDeConsulta(SQLQuitacoes
                                           ,TArray<Variant>.Create(AIdLoteRecebimento
                                                                  ,TContaReceberQuitacaoMovimento.ORIGEM_LOTE_RECEBIMENTO));
  while not fqQuitacoes.Eof do
  begin
    TContaReceberServ.RemoverContaReceberQuitacaoPorID(fqQuitacoes.GetAsInteger('ID'));
    TContaReceberServ.AtualizarValores(fqQuitacoes.GetAsInteger('ID_CONTA_RECEBER'));
  end;

end;

end.
