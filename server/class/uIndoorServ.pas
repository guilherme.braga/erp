unit uIndoorServ;

interface

uses
  System.SysUtils,
  System.JSON,
  REST.Json;

  type TIndoorServ = class
    private
      // private stuff goes here if you need some
    public
      class function ObterListaArquivos(AIdDispositivo : string) : string;
  end;

//  type TMidia = class
//    FNome : string;
//    FNomeArquivo : string;
//  end;

implementation

uses
  uFactoryQuery;

{ TIndoorServ }

class function TIndoorServ.ObterListaArquivos(AIdDispositivo: string): string;
var
  fqLista    : TFastQuery;
  jsobj, jso : TJsonObject;
  jsa        : TJsonArray;
  jsp        : TJsonPair;
  jsonString : string;

begin

  jsobj := TJSONObject.Create;
  jsa   := TJSONArray.Create;
  jsp   := TJSONPair.Create('Arquivos', jsa);
  jsobj.AddPair(jsp);

  try

    fqLista := TFastQuery.ModoDeConsulta(
      '  SELECT m.nome AS nome                             ' +
      '        ,m.ENDERECO_FTP AS nome_arquivo             ' +
      '    FROM gerenciamento_estacao ge                   ' +
      '   INNER JOIN gerenciamento_estacao_midia gem       ' +
      '      ON ge.ID = gem.ID_GERENCIAMENTO_ESTACAO       ' +
      '   INNER JOIN estacao e ON ge.ID_ESTACAO = e.ID     ' +
      '   INNER JOIN midia m	 ON gem.ID_MIDIA = m.ID      ' +
      '   WHERE e.tv = :IdDispositivo',
      TArray<Variant>.Create(AIdDispositivo));

    while not fqLista.Eof do
    begin

      jso := TJSONObject.Create;

      jso.AddPair(TJSONPair.Create('Nome',
                                   fqLista.GetAsString('NOME')));

      jso.AddPair(TJSONPair.Create('NomeArquivo',
                                   fqLista.GetAsString('NOME_ARQUIVO')));

      jsa.AddElement(jso);

      fqLista.Proximo;

    end;

    jsonString := jsobj.ToString;

  finally
    jsobj.Free;
  end;

  result := jsonString;

end;

end.
