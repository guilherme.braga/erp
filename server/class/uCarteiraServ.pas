unit uCarteiraServ;

Interface

Uses uCarteiraProxy, SysUtils;

type TCarteiraServ = class
  class function GetCarteira(const AIdCarteira: Integer): TCarteiraProxy;
  class function GerarCarteira(const ACarteira: TCarteiraProxy): Integer;
  class procedure IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
  class procedure AtualizarBoletoNossoNumeroProximo(const AIdCarteira, ABoletoNossoNumeroProximo: Integer);
  class function BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
  class function BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
end;

implementation

{ TCarteiraServ }

uses uFactoryQuery, uContaReceberServ;

class function TCarteiraServ.GetCarteira(
    const AIdCarteira: Integer): TCarteiraProxy;
const
  SQL = 'SELECT * FROM CARTEIRA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCarteiraProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCarteira));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FPercJuros := qConsulta.GetAsFloat('PERC_JUROS');
  result.FPercMulta := qConsulta.GetAsFloat('PERC_MULTA');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FCarencia := qConsulta.GetAsInteger('CARENCIA');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FTipoBoleto := qConsulta.GetAsString('TIPO_BOLETO');
  result.FCbxArquivoLicenca := qConsulta.GetAsString('CBX_ARQUIVO_LICENCA');
  result.FCedenteNome := qConsulta.GetAsString('CEDENTE_NOME');
  result.FCedenteDocFederal := qConsulta.GetAsString('CEDENTE_DOC_FEDERAL');
  result.FCedenteBanco := qConsulta.GetAsString('CEDENTE_BANCO');
  result.FCedenteCarteira := qConsulta.GetAsString('CEDENTE_CARTEIRA');
  result.FCedenteNomePersonalizado := qConsulta.GetAsString('CEDENTE_NOME_PERSONALIZADO');
  result.FCedenteAgencia := qConsulta.GetAsString('CEDENTE_AGENCIA');
  result.FCedenteContaCorrente := qConsulta.GetAsString('CEDENTE_CONTA_CORRENTE');
  result.FCedenteCodigo := qConsulta.GetAsString('CEDENTE_CODIGO');
  result.FBoletoNossoNumeroInicial := qConsulta.GetAsInteger('BOLETO_NOSSO_NUMERO_INICIAL');
  result.FBoletoNossoNumeroFinal := qConsulta.GetAsInteger('BOLETO_NOSSO_NUMERO_FINAL');
  result.FBoletoNossoNumeroProximo := qConsulta.GetAsInteger('BOLETO_NOSSO_NUMERO_PROXIMO');
  result.FBoletoModalidade := qConsulta.GetAsInteger('BOLETO_MODALIDADE');
  result.FBoletoCodigoTransmissao := qConsulta.GetAsInteger('BOLETO_CODIGO_TRANSMISSAO');
  result.FBoletoLocalPagamento := qConsulta.GetAsString('BOLETO_LOCAL_PAGAMENTO');
  result.FBoletoInstrucoes := qConsulta.GetAsString('BOLETO_INSTRUCOES');
  result.FBoletoDemonstrativo := qConsulta.GetAsString('BOLETO_DEMONSTRATIVO');
  result.FBoletoLayoutImpressao := qConsulta.GetAsString('BOLETO_LAYOUT_IMPRESSAO');
  result.FBoletoLayoutRemessa := qConsulta.GetAsString('BOLETO_LAYOUT_REMESSA');
  result.FBoletoLayoutRetorno := qConsulta.GetAsString('BOLETO_LAYOUT_RETORNO');
  result.FBoletoPathRemessa := qConsulta.GetAsString('BOLETO_PATH_REMESSA');
  result.FBoletoPathRetorno := qConsulta.GetAsString('BOLETO_PATH_RETORNO');
  result.FBoletoEspecieDocumento := qConsulta.GetAsString('BOLETO_ESPECIE_DOCUMENTO');
  result.FBoletoSequenciaRemessa := qConsulta.GetAsInteger('BOLETO_SEQUENCIA_REMESSA');
  result.FBoletoAmbiente := qConsulta.GetAsString('BOLETO_AMBIENTE');
  result.FBoletoDiasProtesto := qConsulta.GetAsInteger('BOLETO_DIAS_PROTESTO');
  result.FBoAceiteBoleto := qConsulta.GetAsString('BO_ACEITE_BOLETO');
  result.FBoletoVlAcrescimo := qConsulta.GetAsFloat('BOLETO_VL_ACRESCIMO');
  result.FBoletoVlTarifaBancaria := qConsulta.GetAsFloat('BOLETO_VL_TARIFA_BANCARIA');
  result.FBoletoPercMulta := qConsulta.GetAsFloat('BOLETO_PERC_MULTA');
  result.FBoletoPercMoraDiaria := qConsulta.GetAsFloat('BOLETO_PERC_MORA_DIARIA');
  result.FBoletoPercDesconto := qConsulta.GetAsFloat('BOLETO_PERC_DESCONTO');
  result.FBoletoMoeda := qConsulta.GetAsString('BOLETO_MOEDA');
  result.FBoEnviaBoletoEmail := qConsulta.GetAsString('BO_ENVIA_BOLETO_EMAIL');
  result.FBoletoAssuntoEmail := qConsulta.GetAsString('BOLETO_ASSUNTO_EMAIL');
  result.FBoletoLayoutBoleto := qConsulta.GetAsString('BOLETO_LAYOUT_BOLETO');
  result.FBoletoIdReciboEmailPersonalizado := qConsulta.GetAsInteger('BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO');
  result.FBoletoIdReciboBoletoPersonalizado := qConsulta.GetAsInteger('BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO');
  result.FBoletoPrefixoNossoNumero := qConsulta.GetAsInteger('BOLETO_PREFIXO_NOSSO NUMERO');
  result.FBoletoTipoDocumento := qConsulta.GetAsString('BOLETO_TIPO_DOCUMENTO');
  result.FBoletoLayoutEmail := qConsulta.GetAsString('BOLETO_LAYOUT_EMAIL');
  result.FIdContaCorrente := qConsulta.GetAsInteger('ID_CONTA_CORRENTE');
end;

class procedure TCarteiraServ.IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE carteira '+
    '    SET boleto_nosso_numero_proximo = boleto_nosso_numero_proximo + 1 '+
    '  WHERE id = :id', TArray<Variant>.Create(AIdCarteira));
end;

class procedure TCarteiraServ.AtualizarBoletoNossoNumeroProximo(const AIdCarteira,
  ABoletoNossoNumeroProximo: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE carteira '+
    '    SET boleto_nosso_numero_proximo = :boleto_nosso_numero_proximo '+
    '  WHERE id = :id', TArray<Variant>.Create(ABoletoNossoNumeroProximo, AIdCarteira));
end;

class function TCarteiraServ.BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
begin
  result := TContaReceberServ.BuscarMaiorNossoNumero(AIdCarteira) + 1;
  TCarteiraServ.AtualizarBoletoNossoNumeroProximo(AIdCarteira, result);
end;

class function TCarteiraServ.BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
begin
  result := 1; //implementar
end;

class function TCarteiraServ.GerarCarteira(
    const ACarteira: TCarteiraProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('CARTEIRA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ACarteira.FDescricao);
    qEntidade.SetAsFloat('PERC_JUROS', ACarteira.FPercJuros);
    qEntidade.SetAsFloat('PERC_MULTA', ACarteira.FPercMulta);
    qEntidade.SetAsString('BO_ATIVO', ACarteira.FBoAtivo);
    qEntidade.SetAsInteger('CARENCIA', ACarteira.FCarencia);
    qEntidade.SetAsString('OBSERVACAO', ACarteira.FObservacao);
    qEntidade.SetAsString('TIPO_BOLETO', ACarteira.FTipoBoleto);
    qEntidade.SetAsString('CBX_ARQUIVO_LICENCA', ACarteira.FCbxArquivoLicenca);
    qEntidade.SetAsString('CEDENTE_NOME', ACarteira.FCedenteNome);
    qEntidade.SetAsString('CEDENTE_DOC_FEDERAL', ACarteira.FCedenteDocFederal);
    qEntidade.SetAsString('CEDENTE_BANCO', ACarteira.FCedenteBanco);
    qEntidade.SetAsString('CEDENTE_CARTEIRA', ACarteira.FCedenteCarteira);
    qEntidade.SetAsString('CEDENTE_NOME_PERSONALIZADO', ACarteira.FCedenteNomePersonalizado);
    qEntidade.SetAsString('CEDENTE_AGENCIA', ACarteira.FCedenteAgencia);
    qEntidade.SetAsString('CEDENTE_CONTA_CORRENTE', ACarteira.FCedenteContaCorrente);
    qEntidade.SetAsString('CEDENTE_CODIGO', ACarteira.FCedenteCodigo);
    qEntidade.SetAsInteger('BOLETO_NOSSO_NUMERO_INICIAL', ACarteira.FBoletoNossoNumeroInicial);
    qEntidade.SetAsInteger('BOLETO_NOSSO_NUMERO_FINAL', ACarteira.FBoletoNossoNumeroFinal);
    qEntidade.SetAsInteger('BOLETO_NOSSO_NUMERO_PROXIMO', ACarteira.FBoletoNossoNumeroProximo);
    qEntidade.SetAsInteger('BOLETO_MODALIDADE', ACarteira.FBoletoModalidade);
    qEntidade.SetAsInteger('BOLETO_CODIGO_TRANSMISSAO', ACarteira.FBoletoCodigoTransmissao);
    qEntidade.SetAsString('BOLETO_LOCAL_PAGAMENTO', ACarteira.FBoletoLocalPagamento);
    qEntidade.SetAsString('BOLETO_INSTRUCOES', ACarteira.FBoletoInstrucoes);
    qEntidade.SetAsString('BOLETO_DEMONSTRATIVO', ACarteira.FBoletoDemonstrativo);
    qEntidade.SetAsString('BOLETO_LAYOUT_IMPRESSAO', ACarteira.FBoletoLayoutImpressao);
    qEntidade.SetAsString('BOLETO_LAYOUT_REMESSA', ACarteira.FBoletoLayoutRemessa);
    qEntidade.SetAsString('BOLETO_LAYOUT_RETORNO', ACarteira.FBoletoLayoutRetorno);
    qEntidade.SetAsString('BOLETO_PATH_REMESSA', ACarteira.FBoletoPathRemessa);
    qEntidade.SetAsString('BOLETO_PATH_RETORNO', ACarteira.FBoletoPathRetorno);
    qEntidade.SetAsString('BOLETO_ESPECIE_DOCUMENTO', ACarteira.FBoletoEspecieDocumento);
    qEntidade.SetAsInteger('BOLETO_SEQUENCIA_REMESSA', ACarteira.FBoletoSequenciaRemessa);
    qEntidade.SetAsString('BOLETO_AMBIENTE', ACarteira.FBoletoAmbiente);
    qEntidade.SetAsInteger('BOLETO_DIAS_PROTESTO', ACarteira.FBoletoDiasProtesto);
    qEntidade.SetAsString('BO_ACEITE_BOLETO', ACarteira.FBoAceiteBoleto);
    qEntidade.SetAsFloat('BOLETO_VL_ACRESCIMO', ACarteira.FBoletoVlAcrescimo);
    qEntidade.SetAsFloat('BOLETO_VL_TARIFA_BANCARIA', ACarteira.FBoletoVlTarifaBancaria);
    qEntidade.SetAsFloat('BOLETO_PERC_MULTA', ACarteira.FBoletoPercMulta);
    qEntidade.SetAsFloat('BOLETO_PERC_MORA_DIARIA', ACarteira.FBoletoPercMoraDiaria);
    qEntidade.SetAsFloat('BOLETO_PERC_DESCONTO', ACarteira.FBoletoPercDesconto);
    qEntidade.SetAsString('BOLETO_MOEDA', ACarteira.FBoletoMoeda);
    qEntidade.SetAsString('BO_ENVIA_BOLETO_EMAIL', ACarteira.FBoEnviaBoletoEmail);
    qEntidade.SetAsString('BOLETO_ASSUNTO_EMAIL', ACarteira.FBoletoAssuntoEmail);
    qEntidade.SetAsString('BOLETO_LAYOUT_BOLETO', ACarteira.FBoletoLayoutBoleto);
    qEntidade.SetAsInteger('BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO', ACarteira.FBoletoIdReciboEmailPersonalizado);
    qEntidade.SetAsInteger('BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO', ACarteira.FBoletoIdReciboBoletoPersonalizado);
    qEntidade.SetAsInteger('BOLETO_PREFIXO_NOSSO NUMERO', ACarteira.FBoletoPrefixoNossoNumero);
    qEntidade.SetAsString('BOLETO_TIPO_DOCUMENTO', ACarteira.FBoletoTipoDocumento);
    qEntidade.SetAsString('BOLETO_LAYOUT_EMAIL', ACarteira.FBoletoLayoutEmail);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ACarteira.FID := qEntidade.GetAsInteger('ID');
    result :=     ACarteira.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Carteira.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.



end.

