﻿unit uIntegracaoFiscalVendaAgrupadaServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery, uFactoryQuery, 
  uIntegracaoFiscalServ, uVendaServ, uVendaProxy;

type TIntegracaoFiscalVendaAgrupadaServ = class(TIntegracaoFiscalServ)
  private
    FIdsVendas: String;
  public
    procedure EstornarContasReceber; override;
    procedure EstornarContaCorrenteMovimento; override;
    procedure GerarContasReceber; override;

    constructor CreateIntegracaoNFE(const AIdsVendas: String; AVendaProxy: TVendaProxy); overload;
    constructor CreateIntegracaoNFCE(const AIdsVendas: String; const ACPFNaNota: String; AVendaProxy: TVendaProxy); overload;
  protected
    function ValidarDocumentoFiscalJaEmitido: Boolean; override;    
    procedure InstanciarEntidadeOrigem; override;  
    procedure AtualizarCodigoNaEntidadeBase; override;
end;  

implementation

Uses uNotaFiscalProxy, uNotaFiscalServ;

function TIntegracaoFiscalVendaAgrupadaServ.ValidarDocumentoFiscalJaEmitido: Boolean;
var
  idNotaFiscal: Integer;
  statusNotaFiscalEletronica: String;  
begin
  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    idNotaFiscal := TVendaServ.GetIdNotaFiscalNFE(FIdEntidadeBase);    
  end
  else if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    idNotaFiscal := TVendaServ.GetIdNotaFiscalNFCE(FIdEntidadeBase);
  end;

  if idNotaFiscal > 0 then
  begin
    statusNotaFiscalEletronica := TNotaFiscalServ.BuscarStatusNotaFiscalEletronica(idNotaFiscal);

    if statusNotaFiscalEletronica.Equals(TNotaFiscalProxy.STATUS_NFE_ENVIADA) then
    begin
      result := true;
      exit;
    end;

    RemoverNotaFiscalIntegracaoFiscal;
    result := false;
  end;
  begin
    result := false;
  end;  
end;	

procedure TIntegracaoFiscalVendaAgrupadaServ.InstanciarEntidadeOrigem;
begin
  FEntidadeBase := TFastQuery.ModoDeConsulta(
	'   SELECT ID'+
	'         ,DH_CADASTRO'+
	'         ,SUM(IFNULL(VL_DESCONTO, 0)) AS VL_DESCONTO'+
	'         ,SUM(IFNULL(VL_ACRESCIMO, 0)) AS VL_ACRESCIMO'+
	'         ,SUM(IFNULL(VL_TOTAL_PRODUTO, 0)) AS VL_TOTAL_PRODUTO'+
	'         ,SUM(IFNULL(VL_VENDA, 0)) AS VL_VENDA'+
	'         ,STATUS'+
	'         ,ID_PESSOA'+
	'         ,ID_FILIAL'+
	'         ,ID_OPERACAO'+
	'         ,ID_CHAVE_PROCESSO'+
	'         ,ID_CENTRO_RESULTADO'+
	'         ,ID_CONTA_ANALISE'+
	'         ,ID_PLANO_PAGAMENTO'+
	'         ,ID_FORMA_PAGAMENTO'+
	'         ,SUM(IFNULL(PERC_DESCONTO, 0)) AS PERC_DESCONTO'+
	'         ,SUM(IFNULL(PERC_ACRESCIMO, 0)) AS PERC_ACRESCIMO'+
	'         ,ID_TABELA_PRECO'+
	'         ,TIPO'+
	'         ,DH_FECHAMENTO'+
	'         ,SUM(IFNULL(VL_PAGAMENTO, 0)) AS VL_PAGAMENTO'+
	'         ,SUM(IFNULL(VL_PESSOA_CREDITO_UTILIZADO, 0)) AS VL_PESSOA_CREDITO_UTILIZADO'+
	'         ,ID_VENDEDOR'+
	'         ,ID_NOTA_FISCAL_NFE'+
	'         ,ID_OPERACAO_FISCAL'+
	'         ,ID_NOTA_FISCAL_NFSE'+
	'         ,ID_NOTA_FISCAL_NFCE'+
	'     FROM VENDA'+
	'    WHERE ID IN ('+FIdsVendas+')'+
	' GROUP BY ID_FILIAL');

  FEntidadeBaseItem := TFastQuery.ModoDeConsulta(
	'     SELECT ID'+
	'           ,ID_VENDA'+
	'           ,ID_PRODUTO'+
	'           ,NR_ITEM'+
	'           ,SUM(QUANTIDADE) AS QUANTIDADE'+
	'           ,SUM(VL_DESCONTO) AS VL_DESCONTO'+
	'           ,SUM(VL_ACRESCIMO) AS VL_ACRESCIMO'+
	'           ,SUM(VL_BRUTO) AS VL_BRUTO'+
	'           ,SUM(VL_LIQUIDO) AS VL_LIQUIDO'+
	'           ,SUM(PERC_DESCONTO) AS PERC_DESCONTO'+
	'           ,SUM(PERC_ACRESCIMO) AS PERC_ACRESCIMO'+
	'           ,DESCRICAO_PRODUTO'+
	'           ,ID_OPERACAO_FISCAL'+
	'       FROM VENDA_ITEM'+
	'      WHERE ID_VENDA IN ('+FIdsVendas+')'+
	'   GROUP BY ID_PRODUTO');

  FEntidadeBaseParcela := TFastQuery.ModoDeConsulta(
	'SELECT * FROM VENDA_PARCELA WHERE 1=2');
end;

constructor TIntegracaoFiscalVendaAgrupadaServ.CreateIntegracaoNFE(const AIdsVendas: String;
  AVendaProxy: TVendaProxy);
begin
  CreateIntegracaoNFE(0);
  FIdsVendas := AIdsVendas;
  FNotaFiscalTransiente.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
  FNotaFiscalTransiente.FIdFormaPagamentoDinheiro := AVendaProxy.FIdFormaPagamentoDinheiro;
  FNotaFiscalTransiente.FIdCarteira := AVendaProxy.FIdCarteira;
end;

procedure TIntegracaoFiscalVendaAgrupadaServ.EstornarContaCorrenteMovimento;
begin
  {Não estorna a movimentação pois as venda ja estão fechadas}
  //inherited;
end;

procedure TIntegracaoFiscalVendaAgrupadaServ.EstornarContasReceber;
begin
  {Não estorna a movimentação pois as venda ja estão fechadas}
  //inherited;
end;

procedure TIntegracaoFiscalVendaAgrupadaServ.GerarContasReceber;
begin
  {Não gera a movimentação pois as venda ja estão fechadas}
  //inherited;
end;

constructor TIntegracaoFiscalVendaAgrupadaServ.CreateIntegracaoNFCE(const AIdsVendas: String;
  const ACPFNaNota: String; AVendaProxy: TVendaProxy);
begin
  CreateIntegracaoNFCE(0, ACPFNaNota);
  FIdsVendas := AIdsVendas;
  FNotaFiscalTransiente.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
  FNotaFiscalTransiente.FIdFormaPagamentoDinheiro := AVendaProxy.FIdFormaPagamentoDinheiro;
  FNotaFiscalTransiente.FIdCarteira := AVendaProxy.FIdCarteira;
end;

procedure TIntegracaoFiscalVendaAgrupadaServ.AtualizarCodigoNaEntidadeBase;
begin	   
  TVendaServ.AtualizarCodigoNotaFiscalNasVendas(FIdsVendas, FIdNotaFiscal, FTipoDocumentoFiscal);
end;

end.