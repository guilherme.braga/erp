unit uNotaFiscalServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery,
  uFactoryQuery, uNotaFiscalProxy, Data.FireDACJSONReflect, uVendaProxy;

type TNotaFiscalServ = class
  public
    class function GerarEntradaQuitacao(ANotaFiscal: TFastQuery;
      ANotaFiscalProxy: TNotaFiscalTransienteProxy): Boolean;

    class function GerarContaReceber(AIdChaveProcesso: Integer;
      ANotaFiscalTransiente: TNotaFiscalTransienteProxy): Boolean;

    class function GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
    class function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
    class function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
    class procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
    class procedure AtualizarStatusDocumentoFiscal(const AIdNotaFiscal: Integer; const AStatus: String);
    class procedure AtualizarNovaChaveProcesso(const AIdChaveProcesso: Integer);
    class function GetNotaFiscalProxy(const AIdNotaFiscal: Integer): TNotaFiscalProxy;
    class function GetNotaFiscal(AIdChaveProcesso: Integer): TFastQuery;
    class function GetNotaFiscalPorId(AId: Integer): TFastQuery;
    class function GetNotaFiscalItem(AIdNotaFiscal: Integer): TFastQuery;
    class function GetNotaFiscalParcela(AIdNotaFiscal: Integer): TFastQuery;
    class function GerarMovimentacaoEstoque(ANotaFiscal, ANotaFiscalItem: TFastQuery; AAcao: String): boolean;
    class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    class function Efetivar(AIdChaveProcesso: Integer;
      ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
    class function Cancelar(AIdChaveProcesso: Integer): Boolean;
    class function GerarNotaFiscal(const ANotaFiscal: TNotaFiscalProxy): Integer;
    class function GetNotaFiscalItemProxy(const AIdNotaFiscalItem: Integer): TNotaFiscalItemProxy;
    class function GerarNotaFiscalItem(const ANotaFiscalItem: TNotaFiscalItemProxy): Integer;
    class function GetId(AIdChaveProcesso: Integer): Integer;
    class procedure AtribuirChaveNFE(const AIdNotaFiscal: Integer; AChaveNFE: String);
    class function ProtocolarDPEC(const AIdNotaFiscal: Integer; const ANrRegistro: String): String;
    class function GerarNotaFiscalDaVenda(const AId: Integer; const ACPFNaNota: String;
      ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
    class function GerarNotaFiscalDeVendaAgrupada(const AIdsVendas: String; const ACPFNaNota: String;
      ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
    class function GerarNFEDaOrdemServico(const AIdOrdemServico: Integer; const ACPFNaNota: String): Boolean;
    class procedure AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
    class function GetNumeroDocumentoNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
    class function GetSerieNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
    class procedure GravarXMLDanfe(const AIdNotaFiscal: Integer; AXML: String);
    class function BuscarXML(const AIdNotaFiscal: Integer): String;
    class procedure GravarRetornoSefaz(const AIdNotaFiscal: Integer; const ATipoAmbiente, AVersaoAplicativo: String;
      const ACodigoStatus: String; const AMotivo: String; const AUF: String; const ADhRecebimento: TDateTime;
      const ATempoMedio: Integer; const ADhRetorno: TDateTime; const AObservacao: String);
    class function BuscarStatusNotaFiscalEletronica(const AIdNotaFiscal: Integer): String;
    class procedure RemoverNotaFiscal(const AIdNotaFiscal: Integer);
    class procedure InutilizarNotaFiscal(const AIdNotaFiscal: Integer);
    class function BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes, AAno: Integer): TFDJSONDataSets;
end;

const STATUS_NOTA_CONFIRMADA: String = 'EFETIVADO';
const STATUS_NOTA_CANCELADA: String = 'CANCELADO';
const STATUS_NOTA_ABERTA: String = 'ABERTA';

implementation

uses uContaReceberProxy, uContaReceberServ, uContaPagarProxy,
  uContaPagarServ, uOperacaoServ, uAcaoProxy, uProdutoProxy, uProdutoServ,
  uDateUtils, uSMContaReceber, uChaveProcessoServ, uChaveProcessoProxy, uVendaServ, uOrdemServicoServ,
  uModeloNotaServ, uPessoaServ, uTabelaPrecoServ, uImpostoICMSProxy, uImpostoICMSServ, uFilialServ,
  uMathUtils, uIntegracaoFiscalVendaServ, DateUtils, uContaCorrenteProxy, uContaCorrenteServ,
  uFormaPagamentoServ, uIntegracaoFiscalVendaAgrupadaServ;

{ TNotaFiscalServ }

class procedure TNotaFiscalServ.AtribuirChaveNFE(const AIdNotaFiscal: Integer; AChaveNFE: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE nota_fiscal SET nfe_chave = :nfe_chave'+
    ' where id = :id_nota_fiscal',
    TArray<Variant>.Create(AChaveNFE, AIdNotaFiscal));
end;

class procedure TNotaFiscalServ.AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE nota_fiscal SET doc_federal_pessoa = :ACPF'+
    ' where id = :id_nota_fiscal',
    TArray<Variant>.Create(ACPF, AIdNotaFiscal));
end;

class procedure TNotaFiscalServ.AtualizarNovaChaveProcesso(const AIdChaveProcesso: Integer);
var
  idNovaChaveProcesso: Integer;
begin
  idNovaChaveProcesso := TChaveProcessoServ.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_NOTA_FISCAL,
    TNotaFiscalServ.GetId(AIdChaveProcesso));

  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE nota_fiscal SET id_chave_processo = :id_chave_processo_nova'+
    ' where id_chave_processo = :id_chave_processo_antiga',
    TArray<Variant>.Create( idNovaChaveProcesso, AidChaveProcesso));
end;

class procedure TNotaFiscalServ.AtualizarStatus(
  AIdChaveProcesso: Integer; AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE nota_fiscal SET status = :status where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class procedure TNotaFiscalServ.AtualizarStatusDocumentoFiscal(const AIdNotaFiscal: Integer;
  const AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE nota_fiscal SET ar_cstat = :status where id = :id',
    TArray<Variant>.Create(AStatus, AIdNotaFiscal));
end;

class function TNotaFiscalServ.BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes,
  AAno: Integer): TFDJSONDataSets;
var qNotas: IFastQuery;
    SQL: String;
    dataInicial, dataFinal: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  '    select nf.*,'+
  '   cast(''S'' as char(1)) AS BO_CHECKED'+
  '      from nota_fiscal nf'+
  '     where id_modelo_nota in (select id'+
  '                                from modelo_nota'+
  '                               where modelo in (''55'',''65''))'+
  '      and ar_cstat = ''A ENVIAR'''+
  '      and b_nnf < (select max(b_nnf)'+
  '                     from nota_fiscal'+
  '                    where id_modelo_nota in (select id'+
  '                                               from modelo_nota'+
  '                                              where modelo in (''55'',''65''))'+
  '                      and ar_cstat <> ''A ENVIAR'''+
  '                      and b_serie = nf.b_serie'+
  '                      and id_modelo_nota = nf.id_modelo_nota)'+
  '      and b_demi between :dt_emissao_inicial and :dt_emissao_final'+
  '      and id_filial = :id_filial';

  dataInicial := InttoStr(AAno)+FormatFloat('00',AMes)+'01';
  dataFinal := InttoStr(AAno)+FormatFloat('00',AMes)+InttoStr(DayOf(EndOfTheMonth(EncodeDate(AAno, AMes, 01))));

  qNotas := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(dataInicial, dataFinal, AIdFIlial));

  TFDJSONDataSetsWriter.ListAdd(result, (qNotas as TFastQuery));
end;

class function TNotaFiscalServ.BuscarStatusNotaFiscalEletronica(const AIdNotaFiscal: Integer): String;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT ar_cstat FROM nota_fiscal WHERE id = :id',
    TArray<Variant>.Create(AIdNotaFiscal));

  result := consulta.GetAsString('ar_cstat');
end;

class function TNotaFiscalServ.BuscarXML(const AIdNotaFiscal: Integer): String;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT documento_fiscal_xml FROM nota_fiscal WHERE id = :id',
    TArray<Variant>.Create(AIdNotaFiscal));

  result := consulta.GetAsString('documento_fiscal_xml');
end;

class function TNotaFiscalServ.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  if TContaReceberServ.ExisteContaReceber(AIdChaveProcesso) then
    TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);

  if TContaPagarServ.ExisteContaContaPagar(AIdChaveProcesso) then
    TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);

  TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);

  TNotaFiscalServ.AtualizarStatus(AIdChaveProcesso, STATUS_NOTA_ABERTA);
  TNotaFiscalServ.AtualizarNovaChaveProcesso(AIdChaveProcesso);
end;

class function TNotaFiscalServ.Efetivar(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
var
  qNotaFiscal, qNotaFiscalItem, qNotaFiscalParcela: TFastQuery;
begin
  result := true;

  qNotaFiscal := TNotaFiscalServ.GetNotaFiscal(AIdChaveProcesso);
  qNotaFiscalItem := TNotaFiscalServ.GetNotaFiscalItem(qNotaFiscal.GetAsInteger('ID'));
  qNotaFiscalParcela := TNotaFiscalServ.GetNotaFiscalParcela(qNotaFiscal.GetAsInteger('ID'));

  if TOperacaoServ.ExisteAcao(GERAR_CONTA_RECEBER,qNotaFiscal.GetAsInteger('ID_OPERACAO')) then
  begin
    TNotaFiscalServ.GerarContaReceber(AIdChaveProcesso, ANotaFiscalTransienteProxy);
  end;

  if TOperacaoServ.ExisteAcao(GERAR_CONTA_PAGAR,qNotaFiscal.GetAsInteger('ID_OPERACAO')) then
  begin
    TNotaFiscalServ.GerarContaPagar(AIdChaveProcesso);
  end;

  if TOperacaoServ.ExisteAcao(GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA,qNotaFiscal.GetAsInteger('ID_OPERACAO')) then
  begin
    TNotaFiscalServ.GerarMovimentacaoEstoque(qNotaFiscal, qNotaFiscalItem, GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA);
  end;

  if TOperacaoServ.ExisteAcao(GERAR_MOVIMENTACAO_ESTOQUE_SAIDA,qNotaFiscal.GetAsInteger('ID_OPERACAO')) then
  begin
    TNotaFiscalServ.GerarMovimentacaoEstoque(qNotaFiscal, qNotaFiscalItem, GERAR_MOVIMENTACAO_ESTOQUE_SAIDA);
  end;

  AtualizarStatus(AIdChaveProcesso, STATUS_NOTA_CONFIRMADA);
end;

class function TNotaFiscalServ.EstornarContaReceber(
  AIdChaveProcesso: Integer): Boolean;
var
  qGeracaoDocumento, qGeracaoDocumentoParcela: IFastQuery;
begin
  result := true;
  TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);
end;

class function TNotaFiscalServ.EstornarContaPagar(
  AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);
end;

class function TNotaFiscalServ.GerarContaPagar(
  AIdChaveProcesso: Integer): Boolean;
var contaPagar: TContaPagarMovimento;
    objectContaPagarJSON: String;
    NotaFiscal, NotaFiscalParcela: IFastQuery;
begin
  result := true;

  NotaFiscal := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  NotaFiscalParcela := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal_parcela where id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(NotaFiscal.GetAsInteger('id')));

  while not NotaFiscalParcela.Eof do
  begin
    contaPagar := TContaPagarMovimento.Create;
    try
      with contaPagar do
      begin
        FIdPessoa :=            NotaFiscal.GetAsInteger('Id_Pessoa');

        FDocumento  :=          NotaFiscal.GetAsString('B_NNF');
        FDescricao  :=          NotaFiscalParcela.GetAsString('DOCUMENTO');
        FStatus     :=          ABERTO;

        FDtVencimento :=        NotaFiscalParcela.GetAsString('Dt_Vencimento');
        FDtDocumento  :=        NotaFiscalParcela.GetAsString('Dt_Vencimento');
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           NotaFiscalParcela.GetAsFloat('VL_TITULO');
        FVlQuitado :=           0;
        FVlAberto  :=           NotaFiscalParcela.GetAsFloat('VL_TITULO');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           NotaFiscalParcela.GetAsInteger('qt_parcela');
        FNrParcela :=           NotaFiscalParcela.GetAsInteger('Nr_Parcela');

        FSequencia  :=          NotaFiscalParcela.GetAsString('Nr_Parcela')+'/'+
          NotaFiscalParcela.GetAsString('qt_parcela');

        FBoVencido  :=          'N';
        FObservacao :=          '';

        FIdContaAnalise    :=   NotaFiscal.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   NotaFiscal.GetAsInteger('Id_Centro_Resultado');

        FIdChaveProcesso  :=    NotaFiscal.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    NotaFiscal.GetAsInteger('Id_Forma_Pagamento');

        FIdCarteira :=   NotaFiscalParcela.GetAsInteger('Id_Carteira');

        FIdFilial :=    NotaFiscal.GetAsInteger('Id_Filial');

        FIdDocumentoOrigem := NotaFiscal.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_NOTA_FISCAL;
      end;
      objectContaPagarJSON := TJson.ObjectToJsonString(contaPagar);
      TContaPagarServ.GerarContaPagar(objectContaPagarJSON);
    finally
      contaPagar.Free;
    end;

    NotaFiscalParcela.Proximo;
  end;
end;

class function TNotaFiscalServ.GerarContaReceber(
  AIdChaveProcesso: Integer; ANotaFiscalTransiente: TNotaFiscalTransienteProxy): Boolean;
var
  contaReceber: TContaReceberMovimento;
  objectContaReceberJSON: String;
  qNotaFiscal, qNotaFiscalParcela: IFastQuery;
begin
  result := true;

  qNotaFiscal := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  qNotaFiscalParcela := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal_parcela where id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(qNotaFiscal.GetAsInteger('id')));

  if qNotaFiscal.GetAsFloat('VL_QUITACAO_DINHEIRO') > 0 then
  begin
    TNotaFiscalServ.GerarEntradaQuitacao(qNotaFiscal as TFastQuery, ANotaFiscalTransiente);
  end;

  while not qNotaFiscalParcela.Eof do
  begin
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            qNotaFiscal.GetAsInteger('Id_Pessoa');

        FDocumento  :=          qNotaFiscal.GetAsString('B_NNF');
        FDescricao  :=          'Nota Fiscal N� '+qNotaFiscal.GetAsString('B_NNF')+' - '+
            TPessoaServ.GetNomePessoa(qNotaFiscal.GetAsInteger('Id_Pessoa'));
        FStatus     :=          ABERTO;

        FDtVencimento :=        qNotaFiscalParcela.GetAsString('Dt_Vencimento');
        FDtDocumento  :=        qNotaFiscalParcela.GetAsString('Dt_Vencimento');
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           qNotaFiscalParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado :=           0;
        FVlAberto  :=           qNotaFiscalParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           qNotaFiscalParcela.GetAsInteger('qt_parcela');
        FNrParcela :=           qNotaFiscalParcela.GetAsInteger('Nr_Parcela');

        FSequencia  :=          qNotaFiscalParcela.GetAsString('Nr_Parcela')+'/'+
          qNotaFiscalParcela.GetAsString('qt_parcela');

        FBoVencido  :=          'N';
        FObservacao :=          '';

        FIdContaAnalise    :=   qNotaFiscal.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   qNotaFiscal.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   qNotaFiscal.GetAsInteger('Id_Filial');
        FIdCarteira :=   qNotaFiscalParcela.GetAsInteger('Id_Carteira');

        FIdChaveProcesso  :=    qNotaFiscal.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    qNotaFiscalParcela.GetAsInteger('Id_Forma_Pagamento');

        FIdDocumentoOrigem := qNotaFiscal.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_NOTA_FISCAL;
      end;
      objectContaReceberJSON := TJson.ObjectToJsonString(contaReceber);
      TContaReceberServ.GerarContaReceber(objectContaReceberJSON);
    finally
      contaReceber.Free;
    end;

    qNotaFiscalParcela.Proximo;
  end;
end;

class function TNotaFiscalServ.GerarEntradaQuitacao(ANotaFiscal: TFastQuery;
  ANotaFiscalProxy: TNotaFiscalTransienteProxy): Boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
begin
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            ANotaFiscal.GetAsInteger('Id_Pessoa');

        FDocumento  :=          ANotaFiscal.GetAsString('B_NNF');
        FDescricao  :=          'Nota Fiscal: '+ANotaFiscal.GetAsString('Id')+' - '+
            TPessoaServ.GetNomePessoa(ANotaFiscal.GetAsInteger('Id_Pessoa'));
        FStatus     :=          ABERTO;

        FDtVencimento :=        DatetoStr(Date);
        FDtDocumento  :=        DatetoStr(ANotaFiscal.GetAsDate('dh_cadastro'));
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           ANotaFiscal.GetAsFloat('VL_QUITACAO_DINHEIRO');
        FVlQuitado :=           0;
        FVlAberto  :=           ANotaFiscal.GetAsFloat('VL_QUITACAO_DINHEIRO');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           1;
        FNrParcela :=           1;

        FSequencia  :=          InttoStr(FNrParcela)+'/'+InttoStr(FQtParcela);

        FBoVencido  :=          'N';
        FObservacao :=          'Entrada/Quita��o da Nota Fiscal';

        FIdContaAnalise    :=   ANotaFiscal.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   ANotaFiscal.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   ANotaFiscal.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    ANotaFiscal.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    ANotaFiscalProxy.FIdFormaPagamentoDinheiro;
        FIdContaCorrente :=     ANotaFiscalProxy.FIdContaCorrente;
        FIdCarteira := ANotaFiscalProxy.FIdCarteira;

        FIdDocumentoOrigem := ANotaFiscal.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_VENDA;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(ANotaFiscal.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := ANotaFiscal.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          ANotaFiscalProxy.FIdFormaPagamentoDinheiro);
        Quitacao.FIdContaCorrente := ANotaFiscalProxy.FIdContaCorrente;
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := contaReceber.FVlAberto;
        Quitacao.FVlTotal           := contaReceber.FVlAberto;
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := ANotaFiscal.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := ANotaFiscal.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;

          Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;

          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
          Movimento.FIdDocumentoOrigem := ANotaFiscal.GetAsInteger('ID');
          Movimento.FBoConciliado      := 'S';

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TNotaFiscalServ.GerarMovimentacaoEstoque(
  ANotaFiscal, ANotaFiscalItem: TFastQuery; AAcao: String): boolean;
var tipoMovimentacao: String;
  estoque: TProdutoMovimento;
  objectMovimentoEstoqueJSON: String;
  qtdeProduto: Double;
begin
  result := true;

  estoque := TProdutoMovimento.Create;
  try
    if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA then
    begin
      tipoMovimentacao := MOVIMENTO_ENTRADA;
    end
    else if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_SAIDA then
    begin
      tipoMovimentacao := MOVIMENTO_SAIDA;
    end;

    ANotaFiscalItem.First;
    while not ANotaFiscalItem.Eof do
    begin
      with estoque do
      begin
        FIdProduto         := ANotaFiscalItem.GetAsInteger('ID_PRODUTO');
        FIdFilial          := ANotaFiscal.GetAsInteger('ID_FILIAL');
        FIdChaveProcesso   := ANotaFiscal.GetAsInteger('ID_CHAVE_PROCESSO');
        FTipo              := tipoMovimentacao;
        FDocumentoOrigem   := DOCUMENTO_NOTA_FISCAL;
        FDtMovimento       := DateToStr(Date);
        FQtEstoqueAnterior := TProdutoServ.GetEstoqueAtual(FIdProduto, FIdFilial);
        FQtMovimento       := ANotaFiscalItem.GetAsFloat('I_QCOM');

        if FQtMovimento > 0 then
        begin
          qtdeProduto := FQtMovimento;
        end
        else
        begin
          qtdeProduto := 1;
        end;

        FVlUltimoCusto     := ANotaFiscalItem.GetAsFloat('I_VUNCOM');
        FVlUltimoFrete     := ANotaFiscalItem.GetAsFloat('I_VFRETE') / qtdeProduto;
        FVlUltimoSeguro := ANotaFiscalItem.GetAsFloat('I_VSEG') / qtdeProduto;
        FVlUltimoIcmsSt := ANotaFiscalItem.GetAsFloat('N_NVICMSST') / qtdeProduto;
        FVlUltimoIpi := ANotaFiscalItem.GetAsFloat('O_VIPI') / qtdeProduto;

        FVlUltimoDesconto := ANotaFiscalItem.GetAsFloat('I_VDESC');
        FVlUltimaDespesaAcessoria := ANotaFiscalItem.GetAsFloat('I_VOUTRO');

        FVlCustoOperacional := ANotaFiscalItem.GetAsFloat('VL_CUSTO_OPERACIONAL');

        FPercUltimoFrete := ANotaFiscalItem.GetAsFloat('PERC_FRETE');
        FPercUltimoSeguro := ANotaFiscalItem.GetAsFloat('PERC_SEGURO');
        FPercUltimoIcmsSt := ANotaFiscalItem.GetAsFloat('PERC_ICMS_ST');
        FPercUltimoIpi := ANotaFiscalItem.GetAsFloat('PERC_IPI');
        FPercUltimoDesconto := ANotaFiscalItem.GetAsFloat('PERC_DESCONTO');
        FPercUltimaDespesaAcessoria := ANotaFiscalItem.GetAsFloat('PERC_ACRESCIMO');
        FPercCustoOperacional := ANotaFiscalItem.GetAsFloat('PERC_CUSTO_OPERACIONAL');
      end;
      objectMovimentoEstoqueJSON := TJson.ObjectToJsonString(estoque);
      TProdutoMovimentoServ.GerarMovimentacaoProduto(objectMovimentoEstoqueJSON);
      ANotaFiscalItem.Next;
    end;
  finally
    FreeAndNil(estoque);
  end;
end;

class function TNotaFiscalServ.GerarNotaFiscalDaVenda(const AId: Integer; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
var
  integracaoFiscalVenda: TIntegracaoFiscalVendaServ;
begin
  try
    result := false;

    if ATipoDocumentoFiscal.Equals(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE) then
    begin
      integracaoFiscalVenda := TIntegracaoFiscalVendaServ.CreateIntegracaoNFE(AId, AVendaProxy);
    end
    else
    begin
      integracaoFiscalVenda := TIntegracaoFiscalVendaServ.CreateIntegracaoNFCE(AId, ACPFNaNota, AVendaProxy);
    end;

    integracaoFiscalVenda.RealizarIntegracaoFiscal;
    result := true;
  finally
    FreeAndNil(integracaoFiscalVenda);
  end;
end;

class function TNotaFiscalServ.GerarNotaFiscalDeVendaAgrupada(const AIdsVendas: String; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
var
  integracaoFiscalVenda: TIntegracaoFiscalVendaAgrupadaServ;
begin
  try
    result := false;

    if ATipoDocumentoFiscal.Equals(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE) then
    begin
      integracaoFiscalVenda := TIntegracaoFiscalVendaAgrupadaServ.CreateIntegracaoNFE(AIdsVendas, AVendaProxy);
    end
    else
    begin
      integracaoFiscalVenda := TIntegracaoFiscalVendaAgrupadaServ.CreateIntegracaoNFCE(AIdsVendas, ACPFNaNota, AVendaProxy);
    end;

    integracaoFiscalVenda.RealizarIntegracaoFiscal;
    result := true;
  finally
    FreeAndNil(integracaoFiscalVenda);
  end;
end;

class function TNotaFiscalServ.GerarNFEDaOrdemServico(const AIdOrdemServico: Integer;
  const ACPFNaNota: String): Boolean;
begin

end;

class function TNotaFiscalServ.GerarNotaFiscal(
    const ANotaFiscal: TNotaFiscalProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('NOTA_FISCAL');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', ANotaFiscal.FDhCadastro);
    qEntidade.SetAsInteger('B_NNF', ANotaFiscal.FBNnf);
    qEntidade.SetAsInteger('B_SERIE', ANotaFiscal.FBSerie);

    if ANotaFiscal.FIdModeloNota > 0 then
    begin
      qEntidade.SetAsInteger('ID_MODELO_NOTA', ANotaFiscal.FIdModeloNota);
    end;
    qEntidade.SetAsString('B_DEMI', ANotaFiscal.FBDemi);
    qEntidade.SetAsString('B_DSAIENT', ANotaFiscal.FBDsaient);
    qEntidade.SetAsString('B_HSAIENT', ANotaFiscal.FBHsaient);
    qEntidade.SetAsFloat('W_VPROD', ANotaFiscal.FWVprod);
    qEntidade.SetAsFloat('W_VST', ANotaFiscal.FWVst);
    qEntidade.SetAsFloat('W_VSEG', ANotaFiscal.FWVseg);
    qEntidade.SetAsFloat('W_VDESC', ANotaFiscal.FWVdesc);
    qEntidade.SetAsFloat('W_VOUTRO', ANotaFiscal.FWVoutro);
    qEntidade.SetAsFloat('W_VIPI', ANotaFiscal.FWVipi);
    qEntidade.SetAsFloat('W_VFRETE', ANotaFiscal.FWVfrete);
    qEntidade.SetAsFloat('W_VNF', ANotaFiscal.FWVnf);
    qEntidade.SetAsString('X_XNOME', ANotaFiscal.FXXnome);
    qEntidade.SetAsFloat('VL_FRETE_TRANSPORTADORA', ANotaFiscal.FVlFreteTransportadora);
    qEntidade.SetAsString('DT_VENCIMENTO', ANotaFiscal.FDtVencimento);

    if ANotaFiscal.FIdCentroResultado > 0 then
    begin
      qEntidade.SetAsInteger('ID_CENTRO_RESULTADO', ANotaFiscal.FIdCentroResultado);
    end;

    if ANotaFiscal.FIdContaAnalise > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_ANALISE', ANotaFiscal.FIdContaAnalise);
    end;

    if ANotaFiscal.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', ANotaFiscal.FIdChaveProcesso);
    end;

    if ANotaFiscal.FIdOperacao > 0 then
    begin
      qEntidade.SetAsInteger('ID_OPERACAO', ANotaFiscal.FIdOperacao);
    end;

    if ANotaFiscal.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', ANotaFiscal.FIdFilial);
    end;

    if ANotaFiscal.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', ANotaFiscal.FIdPessoa);
    end;
    qEntidade.SetAsFloat('PERC_VST', ANotaFiscal.FPercVst);
    qEntidade.SetAsFloat('PERC_VSEG', ANotaFiscal.FPercVseg);
    qEntidade.SetAsFloat('PERC_VDESC', ANotaFiscal.FPercVdesc);
    qEntidade.SetAsFloat('PERC_VOUTRO', ANotaFiscal.FPercVoutro);
    qEntidade.SetAsFloat('PERC_VIPI', ANotaFiscal.FPercVipi);
    qEntidade.SetAsFloat('PERC_VFRETE', ANotaFiscal.FPercVfrete);
    qEntidade.SetAsString('STATUS', ANotaFiscal.FStatus);

    if ANotaFiscal.FIdPlanoPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_PLANO_PAGAMENTO', ANotaFiscal.FIdPlanoPagamento);
    end;

    if ANotaFiscal.FIdFormaPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_FORMA_PAGAMENTO', ANotaFiscal.FIdFormaPagamento);
    end;
    qEntidade.SetAsString('DT_COMPETENCIA', ANotaFiscal.FDtCompetencia);

    if ANotaFiscal.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', ANotaFiscal.FIdPessoaUsuario);
    end;
    qEntidade.SetAsString('TIPO_NF', ANotaFiscal.FTipoNf);

    qEntidade.SetAsString('NFE_CHAVE', ANotaFiscal.FNfeChave);
    qEntidade.SetAsString('AR_VERSAO', ANotaFiscal.FArVersao);
    qEntidade.SetAsString('AR_TP_AMB', ANotaFiscal.FArTpAmb);
    qEntidade.SetAsString('AR_VERAPLIC', ANotaFiscal.FArVeraplic);
    qEntidade.SetAsInteger('AR_CSTAT', ANotaFiscal.FArCstat);
    qEntidade.SetAsString('AR_XMOTIVO', ANotaFiscal.FArXmotivo);
    qEntidade.SetAsString('AR_CUF', ANotaFiscal.FArCuf);
    qEntidade.SetAsString('AR_DHRECBTO', ANotaFiscal.FArDhrecbto);
    qEntidade.SetAsString('AR_INFREC', ANotaFiscal.FArInfrec);
    qEntidade.SetAsString('AR_NREC', ANotaFiscal.FArNrec);
    qEntidade.SetAsInteger('AR_TMED', ANotaFiscal.FArTmed);
    qEntidade.SetAsString('AR_PROTNFE', ANotaFiscal.FArProtnfe);

    qEntidade.Salvar;
    qEntidade.Persistir;

    ANotaFiscal.FID := qEntidade.GetAsInteger('ID');
    result := ANotaFiscal.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar NotaFiscal.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TNotaFiscalServ.GetId(AIdChaveProcesso: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id FROM nota_fiscal WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := consulta.GetAsInteger('ID');
end;

class function TNotaFiscalServ.GetNotaFiscal(AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TNotaFiscalServ.GetNotaFiscalItem(AIdNotaFiscal: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal_item WHERE id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(AIdNotaFiscal));
end;

class function TNotaFiscalServ.GetNotaFiscalParcela(
  AIdNotaFiscal: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM nota_fiscal_parcela WHERE id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(AIdNotaFiscal));
end;

class function TNotaFiscalServ.GetNotaFiscalPorId(AId: Integer): TFastQuery;
const
  SQL = 'SELECT * FROM NOTA_FISCAL WHERE id = :id ';
begin
  result := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AId));
end;

class function TNotaFiscalServ.GetNotaFiscalProxy(const AIdNotaFiscal: Integer): TNotaFiscalProxy;
const
  SQL = 'SELECT * FROM NOTA_FISCAL WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TNotaFiscalProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdNotaFiscal));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FBNnf := qConsulta.GetAsInteger('B_NNF');
  result.FBSerie := qConsulta.GetAsInteger('B_SERIE');
  result.FIdModeloNota := qConsulta.GetAsInteger('ID_MODELO_NOTA');
  result.FBDemi := qConsulta.GetAsString('B_DEMI');
  result.FBDsaient := qConsulta.GetAsString('B_DSAIENT');
  result.FBHsaient := qConsulta.GetAsString('B_HSAIENT');
  result.FWVprod := qConsulta.GetAsFloat('W_VPROD');
  result.FWVst := qConsulta.GetAsFloat('W_VST');
  result.FWVseg := qConsulta.GetAsFloat('W_VSEG');
  result.FWVdesc := qConsulta.GetAsFloat('W_VDESC');
  result.FWVoutro := qConsulta.GetAsFloat('W_VOUTRO');
  result.FWVipi := qConsulta.GetAsFloat('W_VIPI');
  result.FWVfrete := qConsulta.GetAsFloat('W_VFRETE');
  result.FWVnf := qConsulta.GetAsFloat('W_VNF');
  result.FXXnome := qConsulta.GetAsString('X_XNOME');
  result.FVlFreteTransportadora := qConsulta.GetAsFloat('VL_FRETE_TRANSPORTADORA');
  result.FDtVencimento := qConsulta.GetAsString('DT_VENCIMENTO');
  result.FIdCentroResultado := qConsulta.GetAsInteger('ID_CENTRO_RESULTADO');
  result.FIdContaAnalise := qConsulta.GetAsInteger('ID_CONTA_ANALISE');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FIdOperacao := qConsulta.GetAsInteger('ID_OPERACAO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FPercVst := qConsulta.GetAsFloat('PERC_VST');
  result.FPercVseg := qConsulta.GetAsFloat('PERC_VSEG');
  result.FPercVdesc := qConsulta.GetAsFloat('PERC_VDESC');
  result.FPercVoutro := qConsulta.GetAsFloat('PERC_VOUTRO');
  result.FPercVipi := qConsulta.GetAsFloat('PERC_VIPI');
  result.FPercVfrete := qConsulta.GetAsFloat('PERC_VFRETE');
  result.FStatus := qConsulta.GetAsString('STATUS');
  result.FIdPlanoPagamento := qConsulta.GetAsInteger('ID_PLANO_PAGAMENTO');
  result.FIdFormaPagamento := qConsulta.GetAsInteger('ID_FORMA_PAGAMENTO');
  result.FDtCompetencia := qConsulta.GetAsString('DT_COMPETENCIA');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FTipoNf := qConsulta.GetAsString('TIPO_NF');
  result.FNfeChave := qConsulta.GetAsString('NFE_CHAVE');
  result.FArVersao := qConsulta.GetAsString('AR_VERSAO');
  result.FArTpAmb := qConsulta.GetAsString('AR_TP_AMB');
  result.FArVeraplic := qConsulta.GetAsString('AR_VERAPLIC');
  result.FArCstat := qConsulta.GetAsInteger('AR_CSTAT');
  result.FArXmotivo := qConsulta.GetAsString('AR_XMOTIVO');
  result.FArCuf := qConsulta.GetAsString('AR_CUF');
  result.FArDhrecbto := qConsulta.GetAsString('AR_DHRECBTO');
  result.FArInfrec := qConsulta.GetAsString('AR_INFREC');
  result.FArNrec := qConsulta.GetAsString('AR_NREC');
  result.FArTmed := qConsulta.GetAsInteger('AR_TMED');
  result.FArProtnfe := qConsulta.GetAsString('AR_PROTNFE');
  result.FIdOperacaoFiscal := qConsulta.GetAsInteger('ID_OPERACAO_FISCAL');
  result.FFormaPagamento := qConsulta.GetAsString('FORMA_PAGAMENTO');
end;

class function TNotaFiscalServ.GetNumeroDocumentoNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
const
  SQL =
  '  SELECT IFNULL(MAX(b_nnf), 0)+1 as numeracao'+
  '  FROM nota_fiscal'+
  ' WHERE b_serie = (SELECT MAX(b_serie)'+
  '                    FROM nota_fiscal'+
  '                   WHERE tipo_nf = ''SAIDA'''+
  '                     AND id_modelo_nota = (SELECT id'+
  '                                             FROM modelo_nota'+
  '                                            WHERE modelo = :modelo'+
  '                                            LIMIT 1))'+
  '   AND tipo_nf = ''SAIDA'''+
  '   AND id_modelo_nota = (select id'+
  '                           from modelo_nota'+
  '                          where modelo = :modelo'+
  '                          limit 1)';
var
   qConsulta: IFastQuery;
   modelo: String;
begin
  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    modelo := TNotaFiscalProxy.MODELO_NFE;
  end
  else
  begin
    modelo := TNotaFiscalProxy.MODELO_NFCE;
  end;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(modelo));

  result := qConsulta.GetAsInteger('numeracao');
end;

class function TNotaFiscalServ.GetSerieNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
const
  SQL =
  '  SELECT IFNULL(MAX(b_serie),0) as serie'+
  '  FROM nota_fiscal'+
  ' WHERE tipo_nf = ''SAIDA'''+
  '   AND id_modelo_nota = (SELECT id'+
  '                           FROM modelo_nota'+
  '                          WHERE modelo = :modelo'+
  '                          LIMIT 1)';
var
   qConsulta: IFastQuery;
   modelo: String;
begin
  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    modelo := TNotaFiscalProxy.MODELO_NFE;
  end
  else
  begin
    modelo := TNotaFiscalProxy.MODELO_NFCE;
  end;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(modelo));

  if qConsulta.GetAsInteger('serie') > 0 then
  begin
    result := qConsulta.GetAsInteger('serie');
  end
  else
  begin
    result := 1;
  end;
end;

class procedure TNotaFiscalServ.GravarRetornoSefaz(const AIdNotaFiscal: Integer; const ATipoAmbiente,
  AVersaoAplicativo: String; const ACodigoStatus: String; const AMotivo, AUF: String;
  const ADhRecebimento: TDateTime; const ATempoMedio: Integer; const ADhRetorno: TDateTime;
  const AObservacao: String);
var
  qNotaFiscal: IFastQuery;
begin
  qNotaFiscal := TFastQuery.ModoDeAlteracao('SELECT * FROM nota_fiscal WHERE id = :id',
    TArray<Variant>.Create(AIdNotaFiscal));

  qNotaFiscal.Alterar;
  qNotaFiscal.SetAsString('AR_TP_AMB',ATipoAmbiente);
  qNotaFiscal.SetAsString('AR_VERAPLIC',AVersaoAplicativo);
  qNotaFiscal.SetAsString('AR_CSTAT',ACodigoStatus);
  qNotaFiscal.SetAsString('AR_XMOTIVO',AMotivo);
  qNotaFiscal.SetAsString('AR_CUF',AUF);
  qNotaFiscal.SetAsDateTime('AR_DHRECBTO',ADhRecebimento);
  qNotaFiscal.SetAsInteger('AR_TMED',ATempoMedio);
  qNotaFiscal.Persistir;

  //qNotaFiscal.SetAsString('xxx',ADhRetorno);
  //qNotaFiscal.SetAsString('xxx',AObservacao);
end;

class procedure TNotaFiscalServ.GravarXMLDanfe(const AIdNotaFiscal: Integer; AXML: String);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE nota_fiscal SET documento_fiscal_xml = :documento_fiscal_xml '+
    ' where id = :id_nota_fiscal',
    TArray<Variant>.Create(AXML, AIdNotaFiscal));
end;

class procedure TNotaFiscalServ.InutilizarNotaFiscal(const AIdNotaFiscal: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE nota_fiscal SET STATUS = :STATUS, ar_cstat = :AR_STATUS '+
    ' where id = :id_nota_fiscal',
    TArray<Variant>.Create(STATUS_NOTA_CANCELADA,  TNotaFiscalProxy.STATUS_NFE_INUTILIZADA, AIdNotaFiscal));
end;

class function TNotaFiscalServ.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := not(TContaReceberServ.ExisteQuitacao(AIdChaveProcesso)) and
    not(TContaPagarServ.ExisteQuitacao(AIdChaveProcesso));
end;

class function TNotaFiscalServ.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  result := not(TContaReceberServ.ExisteContaReceber(AIdChaveProcesso)) and
    not(TContaPagarServ.ExisteContaContaPagar(AIdChaveProcesso));
end;

class function TNotaFiscalServ.ProtocolarDPEC(const AIdNotaFiscal: Integer; const ANrRegistro: String): String;
var
  dataHoraEnvio: TDateTime;
begin
  dataHoraEnvio := Now;

  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE nota_fiscal SET bo_envio_dpec = :bo_envio_dpec, nr_reg_dpec = :nr_reg_dpec, '+
    ' dh_reg_dpec = :dh_reg_dpec where id = :id_nota_fiscal',
    TArray<Variant>.Create('S', ANrRegistro, dataHoraEnvio, AIdNotaFiscal));

  result := ANrRegistro + ' ' + DateTimeToStr(dataHoraEnvio);
end;

class procedure TNotaFiscalServ.RemoverNotaFiscal(const AIdNotaFiscal: Integer);
begin
  TFastQuery.ExecutarScript(
    ' DELETE FROM nota_fiscal_parcela'+
    ' where id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(AIdNotaFiscal));

  TFastQuery.ExecutarScript(
    ' DELETE FROM nota_fiscal_item'+
    ' where id_nota_fiscal = :id_nota_fiscal',
    TArray<Variant>.Create(AIdNotaFiscal));

  TFastQuery.ExecutarScript(
    ' DELETE FROM nota_fiscal'+
    ' where id = :id_nota_fiscal',
    TArray<Variant>.Create(AIdNotaFiscal));
end;

class function TNotaFiscalServ.GetNotaFiscalItemProxy(
    const AIdNotaFiscalItem: Integer): TNotaFiscalItemProxy;
const
  SQL = 'SELECT * FROM nota_fiscal_item WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TNotaFiscalItemProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdNotaFiscalItem));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FHNitem := qConsulta.GetAsInteger('H_NITEM');
  result.FIQcom := qConsulta.GetAsFloat('I_QCOM');
  result.FIVuncom := qConsulta.GetAsFloat('I_VUNCOM');
  result.FIVdesc := qConsulta.GetAsFloat('I_VDESC');
  result.FVlLiquido := qConsulta.GetAsFloat('VL_LIQUIDO');
  result.FIVfrete := qConsulta.GetAsFloat('I_VFRETE');
  result.FIVseg := qConsulta.GetAsFloat('I_VSEG');
  result.FIVoutro := qConsulta.GetAsFloat('I_VOUTRO');
  result.FNNvicmsst := qConsulta.GetAsFloat('N_NVICMSST');
  result.FOVipi := qConsulta.GetAsFloat('O_VIPI');
  result.FVlCustoImposto := qConsulta.GetAsFloat('VL_CUSTO_IMPOSTO');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdNotaFiscal := qConsulta.GetAsInteger('ID_NOTA_FISCAL');
  result.FPercDesconto := qConsulta.GetAsFloat('PERC_DESCONTO');
  result.FPercAcrescimo := qConsulta.GetAsFloat('PERC_ACRESCIMO');
  result.FPercFrete := qConsulta.GetAsFloat('PERC_FRETE');
  result.FPercSeguro := qConsulta.GetAsFloat('PERC_SEGURO');
  result.FPercIcmsSt := qConsulta.GetAsFloat('PERC_ICMS_ST');
  result.FPercIpi := qConsulta.GetAsFloat('PERC_IPI');
  result.FVlCusto := qConsulta.GetAsFloat('VL_CUSTO');
  result.FVlCustoOperacional := qConsulta.GetAsFloat('VL_CUSTO_OPERACIONAL');
  result.FPercCustoOperacional := qConsulta.GetAsFloat('PERC_CUSTO_OPERACIONAL');
  result.FIXprod := qConsulta.GetAsString('I_XPROD');
  result.FNVicms := qConsulta.GetAsFloat('N_VICMS');
  result.FNVbcst := qConsulta.GetAsFloat('N_VBCST');
  result.FNPredbcst := qConsulta.GetAsFloat('N_PREDBCST');
  result.FNMotdesicms := qConsulta.GetAsInteger('N_MOTDESICMS');
  result.FNPmvast := qConsulta.GetAsFloat('N_PMVAST');
  result.FNModbcst := qConsulta.GetAsInteger('N_MODBCST');
  result.FINcm := qConsulta.GetAsInteger('I_NCM');
  result.FIExtipi := qConsulta.GetAsString('I_EXTIPI');
  result.FICfop := qConsulta.GetAsInteger('I_CFOP');
  result.FICeantrib := qConsulta.GetAsString('I_CEANTRIB');
  result.FIUtrib := qConsulta.GetAsString('I_UTRIB');
  result.FIQtrib := qConsulta.GetAsFloat('I_QTRIB');
  result.FIVuntrib := qConsulta.GetAsFloat('I_VUNTRIB');
  result.FIIndtot := qConsulta.GetAsInteger('I_INDTOT');
  result.FINve := qConsulta.GetAsString('I_NVE');
  result.FICean := qConsulta.GetAsString('I_CEAN');
  result.FNOrig := qConsulta.GetAsInteger('N_ORIG');
  result.FNCst := qConsulta.GetAsInteger('N_CST');
  result.FNModbc := qConsulta.GetAsInteger('N_MODBC');
  result.FNPredbc := qConsulta.GetAsFloat('N_PREDBC');
  result.FNVbc := qConsulta.GetAsFloat('N_VBC');
  result.FNPicms := qConsulta.GetAsFloat('N_PICMS');
  result.FNVicmsop := qConsulta.GetAsFloat('N_VICMSOP');
  result.FNUfst := qConsulta.GetAsString('N_UFST');
  result.FNPdif := qConsulta.GetAsFloat('N_PDIF');
  result.FNVlicmsdif := qConsulta.GetAsFloat('N_VLICMSDIF');
  result.FNVicmsdeson := qConsulta.GetAsFloat('N_VICMSDESON');
  result.FNPbcop := qConsulta.GetAsFloat('N_PBCOP');
  result.FNBcstret := qConsulta.GetAsFloat('N_BCSTRET');
  result.FNPcredsn := qConsulta.GetAsFloat('N_PCREDSN');
  result.FNVbcstdest := qConsulta.GetAsFloat('N_VBCSTDEST');
  result.FNVicmsstdest := qConsulta.GetAsFloat('N_VICMSSTDEST');
  result.FNVcredicmssn := qConsulta.GetAsFloat('N_VCREDICMSSN');
  result.FOClenq := qConsulta.GetAsString('O_CLENQ');
  result.FOCnpjprod := qConsulta.GetAsString('O_CNPJPROD');
  result.FOCselo := qConsulta.GetAsString('O_CSELO');
  result.FOQselo := qConsulta.GetAsFloat('O_QSELO');
  result.FOCenq := qConsulta.GetAsString('O_CENQ');
  result.FOCst := qConsulta.GetAsInteger('O_CST');
  result.FOVbc := qConsulta.GetAsFloat('O_VBC');
  result.FOQunid := qConsulta.GetAsFloat('O_QUNID');
  result.FOVunid := qConsulta.GetAsFloat('O_VUNID');
  result.FOPipi := qConsulta.GetAsFloat('O_PIPI');
  result.FQCst := qConsulta.GetAsInteger('Q_CST');
  result.FQVbc := qConsulta.GetAsFloat('Q_VBC');
  result.FQPpis := qConsulta.GetAsFloat('Q_PPIS');
  result.FQVpis := qConsulta.GetAsFloat('Q_VPIS');
  result.FQQbcprod := qConsulta.GetAsFloat('Q_QBCPROD');
  result.FQValiqprod := qConsulta.GetAsFloat('Q_VALIQPROD');
  result.FSCst := qConsulta.GetAsInteger('S_CST');
  result.FSVbc := qConsulta.GetAsFloat('S_VBC');
  result.FSPcofins := qConsulta.GetAsFloat('S_PCOFINS');
  result.FSQbcprod := qConsulta.GetAsFloat('S_QBCPROD');
  result.FSValiqprod := qConsulta.GetAsFloat('S_VALIQPROD');
  result.FSVcofins := qConsulta.GetAsFloat('S_VCOFINS');
end;

class function TNotaFiscalServ.GerarNotaFiscalItem(
    const ANotaFiscalItem: TNotaFiscalItemProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('nota_fiscal_item');
    qEntidade.Incluir;

    qEntidade.SetAsInteger('H_NITEM', ANotaFiscalItem.FHNitem);
    qEntidade.SetAsFloat('I_QCOM', ANotaFiscalItem.FIQcom);
    qEntidade.SetAsFloat('I_VUNCOM', ANotaFiscalItem.FIVuncom);
    qEntidade.SetAsFloat('I_VDESC', ANotaFiscalItem.FIVdesc);
    qEntidade.SetAsFloat('VL_LIQUIDO', ANotaFiscalItem.FVlLiquido);
    qEntidade.SetAsFloat('I_VFRETE', ANotaFiscalItem.FIVfrete);
    qEntidade.SetAsFloat('I_VSEG', ANotaFiscalItem.FIVseg);
    qEntidade.SetAsFloat('I_VOUTRO', ANotaFiscalItem.FIVoutro);
    qEntidade.SetAsFloat('N_NVICMSST', ANotaFiscalItem.FNNvicmsst);
    qEntidade.SetAsFloat('O_VIPI', ANotaFiscalItem.FOVipi);
    qEntidade.SetAsFloat('VL_CUSTO_IMPOSTO', ANotaFiscalItem.FVlCustoImposto);

    if ANotaFiscalItem.FIdProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_PRODUTO', ANotaFiscalItem.FIdProduto);
    end;

    if ANotaFiscalItem.FIdNotaFiscal > 0 then
    begin
      qEntidade.SetAsInteger('ID_NOTA_FISCAL', ANotaFiscalItem.FIdNotaFiscal);
    end;
    qEntidade.SetAsFloat('PERC_DESCONTO', ANotaFiscalItem.FPercDesconto);
    qEntidade.SetAsFloat('PERC_ACRESCIMO', ANotaFiscalItem.FPercAcrescimo);
    qEntidade.SetAsFloat('PERC_FRETE', ANotaFiscalItem.FPercFrete);
    qEntidade.SetAsFloat('PERC_SEGURO', ANotaFiscalItem.FPercSeguro);
    qEntidade.SetAsFloat('PERC_ICMS_ST', ANotaFiscalItem.FPercIcmsSt);
    qEntidade.SetAsFloat('PERC_IPI', ANotaFiscalItem.FPercIpi);
    qEntidade.SetAsFloat('VL_CUSTO', ANotaFiscalItem.FVlCusto);
    qEntidade.SetAsFloat('VL_CUSTO_OPERACIONAL', ANotaFiscalItem.FVlCustoOperacional);
    qEntidade.SetAsFloat('PERC_CUSTO_OPERACIONAL', ANotaFiscalItem.FPercCustoOperacional);
    qEntidade.SetAsString('I_XPROD', ANotaFiscalItem.FIXprod);
    qEntidade.SetAsFloat('N_VICMS', ANotaFiscalItem.FNVicms);
    qEntidade.SetAsFloat('N_VBCST', ANotaFiscalItem.FNVbcst);
    qEntidade.SetAsFloat('N_PREDBCST', ANotaFiscalItem.FNPredbcst);
    qEntidade.SetAsInteger('N_MOTDESICMS', ANotaFiscalItem.FNMotdesicms);
    qEntidade.SetAsFloat('N_PMVAST', ANotaFiscalItem.FNPmvast);
    qEntidade.SetAsInteger('N_MODBCST', ANotaFiscalItem.FNModbcst);
    qEntidade.SetAsInteger('I_NCM', ANotaFiscalItem.FINcm);
    qEntidade.SetAsString('I_EXTIPI', ANotaFiscalItem.FIExtipi);
    qEntidade.SetAsInteger('I_CFOP', ANotaFiscalItem.FICfop);
    qEntidade.SetAsString('I_CEANTRIB', ANotaFiscalItem.FICeantrib);
    qEntidade.SetAsString('I_UTRIB', ANotaFiscalItem.FIUtrib);
    qEntidade.SetAsFloat('I_QTRIB', ANotaFiscalItem.FIQtrib);
    qEntidade.SetAsFloat('I_VUNTRIB', ANotaFiscalItem.FIVuntrib);
    qEntidade.SetAsInteger('I_INDTOT', ANotaFiscalItem.FIIndtot);
    qEntidade.SetAsString('I_NVE', ANotaFiscalItem.FINve);
    qEntidade.SetAsString('I_CEAN', ANotaFiscalItem.FICean);
    qEntidade.SetAsInteger('N_ORIG', ANotaFiscalItem.FNOrig);
    qEntidade.SetAsInteger('N_CST', ANotaFiscalItem.FNCst);
    qEntidade.SetAsInteger('N_MODBC', ANotaFiscalItem.FNModbc);
    qEntidade.SetAsFloat('N_PREDBC', ANotaFiscalItem.FNPredbc);
    qEntidade.SetAsFloat('N_VBC', ANotaFiscalItem.FNVbc);
    qEntidade.SetAsFloat('N_PICMS', ANotaFiscalItem.FNPicms);
    qEntidade.SetAsFloat('N_VICMSOP', ANotaFiscalItem.FNVicmsop);
    qEntidade.SetAsString('N_UFST', ANotaFiscalItem.FNUfst);
    qEntidade.SetAsFloat('N_PDIF', ANotaFiscalItem.FNPdif);
    qEntidade.SetAsFloat('N_VLICMSDIF', ANotaFiscalItem.FNVlicmsdif);
    qEntidade.SetAsFloat('N_VICMSDESON', ANotaFiscalItem.FNVicmsdeson);
    qEntidade.SetAsFloat('N_PBCOP', ANotaFiscalItem.FNPbcop);
    qEntidade.SetAsFloat('N_BCSTRET', ANotaFiscalItem.FNBcstret);
    qEntidade.SetAsFloat('N_PCREDSN', ANotaFiscalItem.FNPcredsn);
    qEntidade.SetAsFloat('N_VBCSTDEST', ANotaFiscalItem.FNVbcstdest);
    qEntidade.SetAsFloat('N_VICMSSTDEST', ANotaFiscalItem.FNVicmsstdest);
    qEntidade.SetAsFloat('N_VCREDICMSSN', ANotaFiscalItem.FNVcredicmssn);
    qEntidade.SetAsString('O_CLENQ', ANotaFiscalItem.FOClenq);
    qEntidade.SetAsString('O_CNPJPROD', ANotaFiscalItem.FOCnpjprod);
    qEntidade.SetAsString('O_CSELO', ANotaFiscalItem.FOCselo);
    qEntidade.SetAsFloat('O_QSELO', ANotaFiscalItem.FOQselo);
    qEntidade.SetAsString('O_CENQ', ANotaFiscalItem.FOCenq);
    qEntidade.SetAsInteger('O_CST', ANotaFiscalItem.FOCst);
    qEntidade.SetAsFloat('O_VBC', ANotaFiscalItem.FOVbc);
    qEntidade.SetAsFloat('O_QUNID', ANotaFiscalItem.FOQunid);
    qEntidade.SetAsFloat('O_VUNID', ANotaFiscalItem.FOVunid);
    qEntidade.SetAsFloat('O_PIPI', ANotaFiscalItem.FOPipi);
    qEntidade.SetAsInteger('Q_CST', ANotaFiscalItem.FQCst);
    qEntidade.SetAsFloat('Q_VBC', ANotaFiscalItem.FQVbc);
    qEntidade.SetAsFloat('Q_PPIS', ANotaFiscalItem.FQPpis);
    qEntidade.SetAsFloat('Q_VPIS', ANotaFiscalItem.FQVpis);
    qEntidade.SetAsFloat('Q_QBCPROD', ANotaFiscalItem.FQQbcprod);
    qEntidade.SetAsFloat('Q_VALIQPROD', ANotaFiscalItem.FQValiqprod);
    qEntidade.SetAsInteger('S_CST', ANotaFiscalItem.FSCst);
    qEntidade.SetAsFloat('S_VBC', ANotaFiscalItem.FSVbc);
    qEntidade.SetAsFloat('S_PCOFINS', ANotaFiscalItem.FSPcofins);
    qEntidade.SetAsFloat('S_QBCPROD', ANotaFiscalItem.FSQbcprod);
    qEntidade.SetAsFloat('S_VALIQPROD', ANotaFiscalItem.FSValiqprod);
    qEntidade.SetAsFloat('S_VCOFINS', ANotaFiscalItem.FSVcofins);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ANotaFiscalItem.FID := qEntidade.GetAsInteger('ID');
    result :=     ANotaFiscalItem.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar NotaFiscalItem.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
