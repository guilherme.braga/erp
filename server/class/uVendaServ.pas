unit uVendaServ;

interface

uses System.SysUtils, FireDAC.Comp.Client, Data.FireDACJSONReflect, REST.Json,
  uFactoryQuery, uVendaProxy, uContaReceberProxy, DateUtils;

type TVendaServ = class
  class function GetVenda(AIdChaveProcesso: Integer): TFastQuery;
  class function GetVendaItem(AIdVenda: Integer): TFastQuery;
  class function GetVendaParcela(AIdVenda: Integer): TFastQuery;
  class function GetVendaReceitaOtica(AIdVenda: Integer): TFastQuery;
  class function GerarMovimentacaoEstoque(AVenda, AVendaItem: TFastQuery; AAcao: String): boolean;
  class function GetVendaProxy(const AIdVenda: Integer): TVendaProxy;
  class function GerarVenda(const AVenda: TVendaProxy): Integer;

  class function GerarDevolucaoEmDinheiroParaCliente(
    const AVendaProxy: TVendaProxy; const AValorDevolucao: Double): Boolean;

  class function GerarDevolucaoEmCreditoParaCliente(
    const AIdChaveProcesso: Integer; const AValorDevolucao: Double): Boolean;

  class function GerarDevolucaoCreditandoNoContasAReceber(const AIdChaveProcesso: Integer;
    const AValorDevolucao: Double; AIdsContasReceber: String): Boolean;

  class function GerarQuitacaoMultaNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
    AVlMulta: Double): Boolean;

  class function GerarQuitacaoJurosNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
    AVlJuros: Double): Boolean;

  class function GerarQuitacaoValorAbertoNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
    AVlQuitacao: Double): Boolean;

  //DEPRECIADO - SUBSTITUIDO PELO METODO GERARCONTARECEBER
  //class function GerarMovimentacaoContaCorrente(AVenda, AVendaParcela: TFastQuery): boolean;

  class function GerarContaReceber(
    AVenda, AVendaParcela: TFastQuery; AVendaProxy: TVendaProxy): boolean;
  class function GerarEntradaQuitacao(
    AVenda: TFastQuery; AVendaProxy: TVendaProxy): Boolean;
  class function GerarContaReceberCartaoCredito(AContaReceber:
    TContaReceberMovimento; AVendaProxy: TVendaProxy; AVenda: TFastQuery;
    AIdOperadoraCartao: Integer): boolean;
  class function GerarContaReceberCartaoDebito(AContaReceber:
    TContaReceberMovimento; AVendaProxy: TVendaProxy; AVenda: TFastQuery;
    AIdOperadoraCartao: Integer): boolean;
  class function GerarContaReceberCheque(AContaReceber: TContaReceberMovimento;
    AVenda, AVendaParcela: TFastQuery; AVendaProxy: TVendaProxy): boolean;
  class procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
  class procedure AtualizarTipoDocumento(const AIdChaveProcesso: Integer; const ATipoDocumento: String);
  class procedure AtualizarNovaChaveProcesso(const AIdChaveProcesso: Integer);

  class procedure AtualizarReceitaOtica(AVendaReceitaOtica: TFastQuery);

  class function Cancelar(AIdChaveProcesso: Integer): Boolean;

  class function Efetivar(AIdChaveProcesso: Integer;
    AVendaProxy: String): Boolean;

  class function EfetivarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
  class function CancelarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;

  class function GetId(AIdChaveProcesso: Integer): Integer;
  class function GetChaveProcesso(AId: Integer): Integer;
  class function GetIdNotaFiscalNFCE(AId: Integer): Integer;
  class function GetIdNotaFiscalNFE(AId: Integer): Integer;

  class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
  class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
  class function ValorEntradaQuitacao(AIdChaveProcesso: Integer): Double;
  class procedure ExcluirVenda(const AIdChaveProcesso: Integer);
  class procedure AtualizarCodigoNotaFiscalNaVenda(const AIdVenda, AIdNotaFiscal: Integer;
    ATipoDocumentoFiscal: String);
  class procedure AtualizarCodigoNotaFiscalNasVendas(const AIdsVendas: String;const AIdNotaFiscal: Integer;
    ATipoDocumentoFiscal: String);
  class function GetTotaisPorTipoVenda(const AIdsVenda: String): TFDJSONDatasets;
  class function BuscarBloqueiosVenda(const AIdVenda: Integer): TFDJSONDatasets;
end;

const STATUS_VENDA_CONFIRMADA: String = 'FECHADO';
const STATUS_VENDA_CANCELADA: String = 'CANCELADO';
const STATUS_VENDA_ABERTA: String = 'ABERTO';

  //Valores dos Campos TIPO
const TIPO_DOCUMENTO_PEDIDO: String = 'PEDIDO';
const TIPO_DOCUMENTO_VENDA: String = 'VENDA';
const TIPO_DOCUMENTO_ORCAMENTO: String = 'OR�AMENTO';
const TIPO_DOCUMENTO_CONDICIONAL: String = 'CONDICIONAL';
const TIPO_DOCUMENTO_DEVOLUCAO: String = 'DEVOLU��O';

implementation

{ TVendaServ }

uses uAcaoProxy, uProdutoProxy, uProdutoServ, uContaCorrenteProxy,
  uContaCorrenteServ, uContaReceberServ, uFormaPagamentoServ,
  uOperadoraCartaoServ, uTipoQuitacaoServ, uTipoQuitacaoProxy, uContaPagarServ, uReceitaOticaServ,
  uProdutoReservaEstoqueMovimentoServ, uContaPagarProxy, uPessoaCreditoProxy, uPessoaCreditoServ,
  uChaveProcessoProxy, uChaveProcessoServ, uNotaFiscalProxy, uOperacaoServ,
  uPessoaServ, uBloqueioPersonalizadoServ;

class procedure TVendaServ.AtualizarCodigoNotaFiscalNasVendas(const AIdsVendas: String;
  const AIdNotaFiscal: Integer; ATipoDocumentoFiscal: String);
begin
  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE venda SET id_nota_fiscal_nfce = :id_nota_fiscal_nfce where id in ('+AIdsVendas+')',
      TArray<Variant>.Create(AIdNotaFiscal));

    exit;
  end;

  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE venda SET id_nota_fiscal_nfe = :id_nota_fiscal_nfe where id in ('+AIdsVendas+')',
      TArray<Variant>.Create(AIdNotaFiscal));
  end;
end;

class procedure TVendaServ.AtualizarCodigoNotaFiscalNaVenda(const AIdVenda, AIdNotaFiscal: Integer;
  ATipoDocumentoFiscal: String);
begin
  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE venda SET id_nota_fiscal_nfce = :id_nota_fiscal_nfce where id = :id_venda',
      TArray<Variant>.Create(AIdNotaFiscal, AIdVenda));

    exit;
  end;

  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE venda SET id_nota_fiscal_nfe = :id_nota_fiscal_nfe where id = :id_venda',
      TArray<Variant>.Create(AIdNotaFiscal, AIdVenda));
  end;
end;

class procedure TVendaServ.AtualizarNovaChaveProcesso(const AIdChaveProcesso: Integer);
var
  idNovaChaveProcesso: Integer;
begin
  idNovaChaveProcesso := TChaveProcessoServ.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_VENDA_VAREJO,
    TVendaServ.GetId(AIdChaveProcesso));

  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE venda SET id_chave_processo = :id_chave_processo_nova'+
    ' where id_chave_processo = :id_chave_processo_antiga',
    TArray<Variant>.Create(idNovaChaveProcesso, AidChaveProcesso));
end;

class procedure TVendaServ.AtualizarReceitaOtica(AVendaReceitaOtica: TFastQuery);
begin
  AVendaReceitaOtica.First;
  while not AVendaReceitaOtica.Eof do
  begin
    TReceitaOticaServ.VincularVendaNaReceita(AVendaReceitaOtica.GetAsInteger('ID_RECEITA_OTICA'),
      AVendaReceitaOtica.GetAsInteger('ID_VENDA'));
    AVendaReceitaOtica.Next;
  end;
end;

class procedure TVendaServ.AtualizarStatus(AIdChaveProcesso: Integer;
  AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE venda SET status = :status where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class procedure TVendaServ.AtualizarTipoDocumento(const AIdChaveProcesso: Integer;
  const ATipoDocumento: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE venda SET tipo = IF(tipo = '+QuotedStr(TIPO_DOCUMENTO_DEVOLUCAO)+', tipo, :tipo)'+
    ' where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(ATipoDocumento, AidChaveProcesso));
end;

class function TVendaServ.BuscarBloqueiosVenda(const AIdVenda: Integer): TFDJSONDatasets;
var

  qBloqueiosAtivosVenda: IFastQuery;
  qExecutarBloqueio: IFastQuery;
  SQL: String;
  qVenda: IFastQuery;
  mBloqueiosAtivosVenda: TFDMemTable;
begin
  result := TFDJSONDataSets.Create;
  qVenda := TVendaServ.GetVenda(TVendaServ.GetChaveProcesso(AIdVenda));
  qBloqueiosAtivosVenda := TBloqueioPersonalizadoServ.BuscarBloqueiosVenda;

  mBloqueiosAtivosVenda := TFDMemTable.Create(nil);
  try
    mBloqueiosAtivosVenda.Data := (qBloqueiosAtivosVenda as TFastQuery).Data;
    mBloqueiosAtivosVenda.First;

    while not mBloqueiosAtivosVenda.Eof do
    begin
      try
        SQL := TBloqueioPersonalizadoServ.PrepararSQLBloqueioVenda(mBloqueiosAtivosVenda.FieldByName('SQL').AsString,
          qVenda as TFastQuery);

        qExecutarBloqueio := TFastQuery.ModoDeConsulta(SQL);

        if qExecutarBloqueio.IsEmpty then
        begin
          mBloqueiosAtivosVenda.Delete;
        end;
      except
        mBloqueiosAtivosVenda.Delete;
      end;

      mBloqueiosAtivosVenda.Next;
    end;

    TFDJSONDataSetsWriter.ListAdd(Result, mBloqueiosAtivosVenda);
  finally
    //FreeAndNil(mBloqueiosAtivosVenda);
  end;
end;

class function TVendaServ.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  try
    if TContaReceberServ.ExisteContaReceber(AIdChaveProcesso) then
      TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);

    if TContaPagarServ.ExisteContaContaPagar(AIdChaveProcesso) then
      TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);

    if TPessoaCreditoServ.ExisteCreditoPessoa(AIdChaveProcesso) then
      TPessoaCreditoMovimentoServ.RealizarEstornoDaMovimentacaoCredito(AIdChaveProcesso);

    TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);

    TProdutoReservaEstoqueMovimentoServ.RemoverReservaDeEstoque(AIdChaveProcesso);

    TContaCorrenteServ.GerarContraPartidaContaCorrente(AIdChaveProcesso);

    TVendaServ.AtualizarStatus(AIdChaveProcesso, STATUS_VENDA_ABERTA);
    TVendaServ.AtualizarTipoDocumento(AIdChaveProcesso, TIPO_DOCUMENTO_PEDIDO);
    TVendaServ.AtualizarNovaChaveProcesso(AIdChaveProcesso);
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao cancelar venda.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.CancelarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
begin
  try
    TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);
    TProdutoReservaEstoqueMovimentoServ.RemoverReservaDeEstoque(AIdChaveProcesso);
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao cancelar venda condicional.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.Efetivar(AIdChaveProcesso: Integer; AVendaProxy: String): Boolean;
var
  qVenda,
  qVendaItem,
  qVendaParcela,
  qVendaReceitaOtica: TFastQuery;
  vendaProxy: TVendaProxy;
begin
  Result := False;
  try
    qVenda := TVendaServ.GetVenda(AIdChaveProcesso);
    try
      qVendaItem := TVendaServ.GetVendaItem(qVenda.GetAsInteger('ID'));
      try
        qVendaParcela := TVendaServ.GetVendaParcela(qVenda.GetAsInteger('ID'));
        try
          vendaProxy := TJson.JsonToObject<TVendaProxy>(AVendaProxy);
          try
            //N�O FUNCIONA OPERA��O E A��O NA VENDA AINDA, N�O VIMOS A NECESSIDADE,
            //ENT�O SEMPRE GERA ESTOQUE E SEMPRE ATUALIZA ESTOQUE

            if qVenda.GetAsString('TIPO').Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
            begin
              TVendaServ.GerarMovimentacaoEstoque(qVenda, qVendaItem, GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA)
            end
            else
            begin
              TVendaServ.GerarMovimentacaoEstoque(qVenda, qVendaItem, GERAR_MOVIMENTACAO_ESTOQUE_SAIDA);

              if ((qVenda.GetAsInteger('ID_OPERACAO') > 0) and
                TOperacaoServ.ExisteAcao(GERAR_CONTA_RECEBER,qVenda.GetAsInteger('ID_OPERACAO'))) or
                (qVenda.GetAsInteger('ID_OPERACAO') = 0) then
              begin
                TVendaServ.GerarContaReceber(qVenda, qVendaParcela, vendaProxy);
              end;
            end;

            AtualizarStatus(AIdChaveProcesso, STATUS_VENDA_CONFIRMADA);
          finally
            vendaProxy.Free;
          end;

          qVendaReceitaOtica := TVendaServ.GetVendaReceitaOtica(qVenda.GetAsInteger('ID'));
          try
            if not qVendaReceitaOtica.IsEmpty then
            begin
              TVendaServ.AtualizarReceitaOtica(qVendaReceitaOtica);
            end;
          finally
            FreeAndNil(qVendaReceitaOtica);
          end;
        finally
          FreeAndNil(qVendaParcela);
        end;
      finally
        FreeAndNil(qVendaItem);
      end;
    finally
      FreeAndNil(qVenda);
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao efetivar venda.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.EfetivarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
var
  qVenda,
  qVendaItem: TFastQuery;
  vendaProxy: TVendaProxy;
begin
  Result := False;
  try
    qVenda := TVendaServ.GetVenda(AIdChaveProcesso);
    try
      qVendaItem := TVendaServ.GetVendaItem(qVenda.GetAsInteger('ID'));
      try
        TVendaServ.GerarMovimentacaoEstoque(qVenda, qVendaItem, GERAR_MOVIMENTACAO_ESTOQUE_SAIDA);
      finally
        FreeAndNil(qVendaItem);
      end;
    finally
      FreeAndNil(qVenda);
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar a movimenta��o de venda condicional.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class procedure TVendaServ.ExcluirVenda(const AIdChaveProcesso: Integer);
var
  idVenda: Integer;
begin
  idVenda := TVendaServ.GetId(AIdChaveProcesso);

  TVendaServ.Cancelar(AIdChaveProcesso);

  TFastQuery.ExecutarScript('DELETE FROM venda_item_devolucao where id_venda = :id_venda',
    TArray<Variant>.Create(idVenda));

  TFastQuery.ExecutarScript('DELETE FROM venda_item where id_venda = :id_venda',
    TArray<Variant>.Create(idVenda));

  TFastQuery.ExecutarScript('DELETE FROM venda_parcela where id_venda = :id_venda',
    TArray<Variant>.Create(idVenda));

  TFastQuery.ExecutarScript('DELETE FROM venda where id = :id_venda',
    TArray<Variant>.Create(idVenda));
end;

class function TVendaServ.GerarContaReceber(AVenda,
  AVendaParcela: TFastQuery; AVendaProxy: TVendaProxy): boolean;
var
  contaReceber: TContaReceberMovimento;
begin
  result := false;
  try
    if AVenda.GetAsFloat('VL_PAGAMENTO') > 0 then
    begin
      TVendaServ.GerarEntradaQuitacao(AVenda, AVendaProxy);
    end;

    AVendaParcela.First;
    while not AVendaParcela.Eof do
    begin
      contaReceber := TContaReceberMovimento.Create;
      try
        with contaReceber do
        begin
          FIdPessoa :=            AVenda.GetAsInteger('Id_Pessoa');

          FDocumento  :=          AVenda.GetAsString('ID');

          FDescricao  :=          'Venda: '+AVenda.GetAsString('Id')+' - '+
            TPessoaServ.GetNomePessoa(AVenda.GetAsInteger('Id_Pessoa'));

          FStatus     :=          ABERTO;

          FDtVencimento :=        AVendaParcela.GetAsString('Dt_Vencimento');
          FDtDocumento  :=        DatetoStr(Date);
          FDhCadastro   :=        DateTimeToStr(Now);

          FVlTitulo  :=           AVendaParcela.GetAsFloat('Vl_Titulo');
          FVlQuitado :=           0;
          FVlAberto  :=           AVendaParcela.GetAsFloat('Vl_Titulo');

          FVlAcrescimo := 0;
          FVlDecrescimo := 0;
          FVlQuitadoLiquido := 0;

          FQtParcela :=           AVendaParcela.GetAsInteger('qt_parcela');
          FNrParcela :=           AVendaParcela.GetAsInteger('Nr_Parcela');

          FSequencia  :=          AVendaParcela.GetAsString('Nr_Parcela')+'/'+
            AVendaParcela.GetAsString('qt_parcela');

          FBoVencido  :=          'N';
          FObservacao :=          '';

          FIdContaAnalise    :=   AVenda.GetAsInteger('Id_Conta_Analise');
          FIdCentroResultado :=   AVenda.GetAsInteger('Id_Centro_Resultado');

          FIdFilial          :=    AVenda.GetAsInteger('Id_Filial');

          FIdChaveProcesso  :=    AVenda.GetAsInteger('Id_Chave_Processo');
          FIdFormaPagamento :=    AVendaParcela.GetAsInteger('Id_Forma_Pagamento');
          FIdContaCorrente :=     AVendaProxy.FIdContaCorrente;
          FIdCarteira := AVendaParcela.GetAsInteger('Id_Carteira');

          FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
          FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_VENDA;
        end;

        if TFormaPagamentoServ.FormaPagamentoCartaoDebito(contaReceber.FIdFormaPagamento) then
        begin
          TVendaServ.GerarContaReceberCartaoDebito(contaReceber, AVendaProxy,
            AVenda, AVendaParcela.GetAsInteger('ID_OPERADORA_CARTAO'));
        end
        else if TFormaPagamentoServ.FormaPagamentoCartaoCredito(contaReceber.FIdFormaPagamento) then
        begin
          TVendaServ.GerarContaReceberCartaoCredito(contaReceber, AVendaProxy,
            AVenda, AVendaParcela.GetAsInteger('ID_OPERADORA_CARTAO'));
        end
        else if TFormaPagamentoServ.FormaPagamentoCheque(contaReceber.FIdFormaPagamento) then
        begin
          TVendaServ.GerarContaReceberCheque(contaReceber, AVenda, AVendaParcela, AVendaProxy);
        end
        else if TFormaPagamentoServ.FormaPagamentoDinheiro(contaReceber.FIdFormaPagamento) then
        begin
          TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));
        end;
      finally
        contaReceber.Free;
      end;
      AVendaParcela.Proximo;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarContaReceberCartaoCredito(AContaReceber:
    TContaReceberMovimento; AVendaProxy: TVendaProxy; AVenda: TFastQuery;
    AIdOperadoraCartao: Integer): boolean;
var
  IdContaReceber: Integer;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
begin
  result := true;
  try
    //Gera��o do conta a receber do cliente
    IdContaReceber := TContaReceberServ.GerarContaReceber(
      TJson.ObjectToJsonString(AContaReceber));

    //Gera��o da baixa do conta a receber do cliente
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber  := IdContaReceber;
      Quitacao.FDhCadastro      := DateTimeToStr(Now);
      Quitacao.FDtQuitacao      := DatetoStr(AVenda.GetAsDate('dh_cadastro'));
      Quitacao.FObservacao      := AContaReceber.FObservacao;
      Quitacao.FIdFilial        := AVenda.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
        AContaReceber.FIdFormaPagamento);
      Quitacao.FIdOperadoraCartao := AIdOperadoraCartao;
      Quitacao.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
      Quitacao.FIdCentroResultado  := AContaReceber.FIdCentroResultado;
      Quitacao.FIdContaAnalise  := AContaReceber.FIdContaAnalise;
      Quitacao.FVlDesconto      := 0;
      Quitacao.FVlAcrescimo     := 0;

      Quitacao.FVlQuitacao        := AContaReceber.FVlTitulo;
      Quitacao.FVlTotal           := AContaReceber.FVlTitulo;
      Quitacao.FPercAcrescimo     := 0;
      Quitacao.FPercDesconto      := 0;
      Quitacao.FIdDocumentoOrigem := AContaReceber.FIdDocumentoOrigem;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(IdContaReceber);
    finally
      Quitacao.Free;
    end;

    {Gera��o do conta a receber da operadora para 30 dias ap�s a data original
      pois � cart�o de d�bito}
    AContaReceber.FIdPessoa :=
      TOperadoraCartaoServ.GetIdPessoaOperadoraCartao(AIdOperadoraCartao);
    AContaReceber.FDtVencimento := DatetoStr(IncMonth(StrtoDate(AContaReceber.FDtVencimento)));
    AContaReceber.FDtDocumento  := DatetoStr(IncMonth(StrtoDate(AContaReceber.FDtDocumento)));
    TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(AContaReceber));
  except
    on e : Exception do
    begin
      Result := False;
      raise Exception.Create('Erro ao gerar conta a receber com a forma de pagamento de cart�o de d�bito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarContaReceberCartaoDebito(AContaReceber:
  TContaReceberMovimento; AVendaProxy: TVendaProxy; AVenda: TFastQuery;
  AIdOperadoraCartao: Integer): boolean;
var
  IdContaReceber: Integer;
  Quitacao       : TContaReceberQuitacaoMovimento;
begin
  result := false;
  try
    //Gera��o do conta a receber do cliente
    IdContaReceber := TContaReceberServ.GerarContaReceber(
      TJson.ObjectToJsonString(AContaReceber));

    //Gera��o da baixa do conta a receber do cliente
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber  := IdContaReceber;
      Quitacao.FDhCadastro      := DateTimeToStr(Now);
      Quitacao.FDtQuitacao      := DatetoStr(AVenda.GetAsDate('dh_cadastro'));
      Quitacao.FObservacao      := AContaReceber.FObservacao;
      Quitacao.FIdFilial        := AVenda.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
        AContaReceber.FIdFormaPagamento);
      Quitacao.FIdOperadoraCartao := AIdOperadoraCartao;
      Quitacao.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
      Quitacao.FIdCentroResultado  := AContaReceber.FIdCentroResultado;
      Quitacao.FIdContaAnalise  := AContaReceber.FIdContaAnalise;
      Quitacao.FVlDesconto      := 0;
      Quitacao.FVlAcrescimo     := 0;

      Quitacao.FVlQuitacao        := AContaReceber.FVlTitulo;
      Quitacao.FVlTotal           := AContaReceber.FVlTitulo;
      Quitacao.FPercAcrescimo     := 0;
      Quitacao.FPercDesconto      := 0;
      Quitacao.FIdDocumentoOrigem := AContaReceber.FIdDocumentoOrigem;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(IdContaReceber);
    finally
      Quitacao.Free;
    end;

    {Gera��o do conta a receber da operadora para 1 dia ap�s a data original
      pois � cart�o de d�bito}
    AContaReceber.FIdPessoa :=
      TOperadoraCartaoServ.GetIdPessoaOperadoraCartao(AIdOperadoraCartao);
    AContaReceber.FDtVencimento := DatetoStr(IncDay(StrtoDate(AContaReceber.FDtVencimento)));
    AContaReceber.FDtDocumento  := DatetoStr(IncDay(StrtoDate(AContaReceber.FDtDocumento)));
    TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(AContaReceber));
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber com a forma de pagamento de cart�o de d�bito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarContaReceberCheque(
  AContaReceber: TContaReceberMovimento; AVenda, AVendaParcela: TFastQuery;
  AVendaProxy: TVendaProxy): boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
  tipoQuitacao: String;
begin
  result := false;
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            AVenda.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AVenda.GetAsString('ID');
        FDescricao  :=          'Venda: '+AVenda.GetAsString('Id')+' - '+
            TPessoaServ.GetNomePessoa(AVenda.GetAsInteger('Id_Pessoa'));
        FStatus     :=          ABERTO;

        FDtVencimento :=        AVendaParcela.GetAsString('Dt_Vencimento');
        FDtDocumento  :=        DatetoStr(Date);
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AVendaParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado :=           0;
        FVlAberto  :=           AVendaParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           AVendaParcela.GetAsInteger('qt_parcela');
        FNrParcela :=           AVendaParcela.GetAsInteger('Nr_Parcela');

        FSequencia  :=          AVendaParcela.GetAsString('Nr_Parcela')+'/'+
          AVendaParcela.GetAsString('qt_parcela');

        FBoVencido  :=          'N';
        FObservacao :=          '';

        FIdContaAnalise    :=   AVenda.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AVenda.GetAsInteger('Id_Centro_Resultado');

        FIdFilial          :=    AVenda.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AVenda.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AVendaParcela.GetAsInteger('Id_Forma_Pagamento');
        FIdContaCorrente :=     AVendaProxy.FIdContaCorrente;
        FIdCarteira := AVendaParcela.GetAsInteger('Id_Carteira');

        FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AVenda.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := contaReceber.FIdFilial;
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          contaReceber.FIdFormaPagamento);
        Quitacao.FIdContaCorrente := contaReceber.FIdContaCorrente;
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := contaReceber.FVlAberto;
        Quitacao.FVlTotal           := contaReceber.FVlAberto;
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := AVendaParcela.GetAsString('CHEQUE_SACADO');
        Quitacao.FChequeDocFederal    := AVendaParcela.GetAsString('CHEQUE_DOC_FEDERAL');
        Quitacao.FChequeBanco         := AVendaParcela.GetAsString('CHEQUE_BANCO');
        Quitacao.FChequeDtEmissao     := AVendaParcela.GetAsString('CHEQUE_DT_EMISSAO');
        Quitacao.FChequeDtVencimento  := AVendaParcela.GetAsString('CHEQUE_DT_VENCIMENTO');
        Quitacao.FChequeAgencia       := AVendaParcela.GetAsString('CHEQUE_AGENCIA');
        Quitacao.FChequeContaCorrente := AVendaParcela.GetAsString('CHEQUE_CONTA_CORRENTE');
        Quitacao.FChequeNumero        := AVendaParcela.GetAsInteger('CHEQUE_NUMERO');

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AVenda.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;
          Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
          Movimento.FIdDocumentoOrigem := AVenda.GetAsInteger('ID');

          //Se for cheque a concilia��o � falsa
          tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(Quitacao.FIdTipoQuitacao);
          if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
            (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
          begin
            Movimento.FBoCheque := 'S';
            Movimento.FBoConciliado := 'N';

            Movimento.FChequeSacado        := Quitacao.FChequeSacado;
            Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
            Movimento.FChequeBanco         := Quitacao.FChequeBanco;
            Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
            Movimento.FChequeDtVencimento  := Quitacao.FChequeDtVencimento;
            Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
            Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
            Movimento.FChequeNumero        := Quitacao.FChequeNumero;
          end
          else
          begin
            Movimento.FBoConciliado := 'S';
          end;
          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarDevolucaoEmCreditoParaCliente(const AIdChaveProcesso: Integer;
  const AValorDevolucao: Double): Boolean;
var
  creditoPessoa: TPessoaCreditoMovimentoProxy;
  venda: TVendaProxy;
begin
  try
    venda := TVendaProxy.Create;
    try
      venda := TVendaServ.GetVendaProxy(TVendaServ.GetId(AIdChaveProcesso));
      creditoPessoa := TPessoaCreditoMovimentoProxy.Create;
      try
        creditoPessoa.FVlCreditoMovimento := AValorDevolucao;
        creditoPessoa.FDhMovimento := DatetimeToStr(Now);
        creditoPessoa.FIdDocumentoOrigem := venda.FId;
        creditoPessoa.FDocumentoOrigem := TPessoaCreditoMovimentoProxy.DOCUMENTO_ORIGEM_VENDA;
        creditoPessoa.FIdPessoa := venda.FIdPessoa;
        creditoPessoa.FIdPessoaUsuario := venda.FIdVendedor;
        creditoPessoa.FIdChaveProcesso := venda.FIdChaveProcesso;
        creditoPessoa.FTipo := TPessoaCreditoMovimentoProxy.TIPO_CREDITO;
        TPessoaCreditoServ.RealizarTransacaoCredito(TJSON.ObjectToJSONString(creditoPessoa));
      finally
        FreeAndNil(creditoPessoa);
      end;
    finally
      FreeAndNil(venda);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarDevolucaoEmDinheiroParaCliente(const AVendaProxy: TVendaProxy;
  const AValorDevolucao: Double): Boolean;
var
  venda: TVendaProxy;
  contaPagar: TContaPagarMovimento;
  Quitacao       : TContaPagarQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
begin
  venda := TVendaProxy.Create;
  try
    venda := TVendaServ.GetVendaProxy(TVendaServ.GetId(AVendaProxy.FIdChaveProcesso));
    try
      contaPagar := TContaPagarMovimento.Create;
      try
        with contaPagar do
        begin
          FIdPessoa :=            venda.FIdPessoa;

          FDocumento  :=          InttoStr(venda.FID);
          FDescricao  :=          'Venda: '+InttoStr(venda.FID)+' - '+
            TPessoaServ.GetNomePessoa(venda.FIdPessoa);
          FStatus     :=          ABERTO;

          FDtVencimento :=        DatetoStr(Date);
          FDtDocumento  :=        DatetoStr(Date);
          FDhCadastro   :=        DatetoStr(Date);

          FVlTitulo  :=           AValorDevolucao;
          FVlQuitado :=           0;
          FVlAberto  :=           AValorDevolucao;

          FVlAcrescimo := 0;
          FVlDecrescimo := 0;
          FVlQuitadoLiquido := 0;

          FQtParcela :=           1;
          FNrParcela :=           1;

          FSequencia  :=          '1/1';

          FBoVencido  :=          'N';
          FObservacao :=          'ESTORNO FINANCEIRO - DEVOLU��O DE VENDA N� '+InttoStr(venda.FId);

          FIdContaAnalise    :=   venda.FIdContaAnalise;
          FIdCentroResultado :=   venda.FIdCentroResultado;

          FIdFilial          :=   venda.FIdFilial;

          FIdChaveProcesso  :=    venda.FIdChaveProcesso;
          FIdFormaPagamento :=    AVendaProxy.FIdFormaPagamentoDinheiro;
          FIdContaCorrente :=     AVendaProxy.FIdContaCorrente;
          FIdCarteira := AVendaProxy.FIdCarteira;

          FIdDocumentoOrigem := venda.FID;
          FTPDocumentoOrigem := TContaPagarMovimento.ORIGEM_VENDA;
        end;
        contaPagar.FId := TContaPagarServ.GerarContaPagar(TJson.ObjectToJsonString(contaPagar));

        Quitacao  := TContaPagarQuitacaoMovimento.Create;
        try
          Quitacao.FIdContaPagar  := contaPagar.FId;
          Quitacao.FDhCadastro      := DateTimeToStr(Now);
          Quitacao.FDtQuitacao      := contaPagar.FDhCadastro;
          Quitacao.FObservacao      := contaPagar.FObservacao;
          Quitacao.FIdFilial        := contaPagar.FIdFilial;
          Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
           contaPagar.FIdFormaPagamento);
          Quitacao.FIdContaCorrente := contaPagar.FIdContaCorrente;
          Quitacao.FIdCentroResultado  := contaPagar.FIdCentroResultado;
          Quitacao.FIdContaAnalise  := contaPagar.FIdContaAnalise;
          Quitacao.FVlDesconto      := 0;
          Quitacao.FVlAcrescimo     := 0;

          Quitacao.FVlQuitacao        := contaPagar.FVlTitulo;
          Quitacao.FVlTotal           := contaPagar.FVlTitulo;
          Quitacao.FPercAcrescimo     := 0;
          Quitacao.FPercDesconto      := 0;
          Quitacao.FIdDocumentoOrigem := contaPagar.FIdDocumentoOrigem;
          Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_VENDA;
          Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

          TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
          TContaPagarServ.AtualizarValores(contaPagar.FId);

          Movimento := TContaCorrenteMovimento.Create;
          try
            Movimento.FDocumento         := InttoStr(Quitacao.FIdDocumentoOrigem);
            Movimento.FDescricao         := contaPagar.FDescricao;
            Movimento.FObservacao        := Quitacao.FObservacao;
            Movimento.FDhCadastro        := DateTimeToStr(Now);
            Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
            Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
            Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
            Movimento.FVlMovimento       := Quitacao.FVlTotal;

            Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

            Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
            Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
            Movimento.FIdCentroResultado := contaPagar.FIdCentroResultado;
            Movimento.FIdChaveProcesso   := contaPagar.FIdChaveProcesso;
            Movimento.FIdFilial          := contaPagar.FIdFilial;
            Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
            Movimento.FIdDocumentoOrigem := Quitacao.FIdDocumentoOrigem;
            Movimento.FBoConciliado      := 'S';

            TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
          finally
            Movimento.Free;
          end;
        finally
          Quitacao.Free;
        end;
      finally
        contaPagar.Free;
      end;
    finally
      FreeAndNil(venda);
    end;
    result := true;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar conta a pagar referente ao estorno financeiro.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarEntradaQuitacao(AVenda: TFastQuery;
  AVendaProxy: TVendaProxy): Boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
begin
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            AVenda.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AVenda.GetAsString('ID');
        FDescricao  :=          'Venda: '+AVenda.GetAsString('Id')+' - '+
            TPessoaServ.GetNomePessoa(AVenda.GetAsInteger('Id_Pessoa'));
        FStatus     :=          ABERTO;

        FDtVencimento :=        DatetoStr(Date);
        FDtDocumento  :=        DatetoStr(AVenda.GetAsDate('dh_cadastro'));
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AVenda.GetAsFloat('Vl_Pagamento');
        FVlQuitado :=           0;
        FVlAberto  :=           AVenda.GetAsFloat('Vl_Pagamento');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           1;
        FNrParcela :=           1;

        FSequencia  :=          InttoStr(FNrParcela)+'/'+InttoStr(FQtParcela);

        FBoVencido  :=          'N';
        FObservacao :=          'Entrada/Quita��o da Venda';

        FIdContaAnalise    :=   AVenda.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AVenda.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   AVenda.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AVenda.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AVendaProxy.FIdFormaPagamentoDinheiro;
        FIdContaCorrente :=     AVendaProxy.FIdContaCorrente;
        FIdCarteira := AVendaProxy.FIdCarteira;

        FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_VENDA;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AVenda.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := AVenda.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          AVendaProxy.FIdFormaPagamentoDinheiro);
        Quitacao.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := AVenda.GetAsFloat('Vl_Pagamento');
        Quitacao.FVlTotal           := AVenda.GetAsFloat('Vl_Pagamento');
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AVenda.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;

          if AVenda.GetAsString('TIPO').Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
          begin
            Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;
          end
          else
          begin
            Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
          end;

          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
          Movimento.FIdDocumentoOrigem := AVenda.GetAsInteger('ID');
          Movimento.FBoConciliado      := 'S';

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

//DEPRECIADO - SUBSTITUIDO PELO METODO GERARCONTARECEBER
{class function TVendaServ.GerarMovimentacaoContaCorrente(AVenda,
  AVendaParcela: TFastQuery): boolean;
var
  Movimento :TContaCorrenteMovimento;
  fqContaCorrente :IFastQuery;
  MovimentoJSON :String;
const
  CREDITO: String = 'CREDITO';
  DEBITO: String = 'DEBITO';
  VENDA_VAREJO: String = 'VENDA';
begin
  Result := True;

  fqContaCorrente := TFastQuery.ModoDeConsulta('SELECT ID FROM CONTA_CORRENTE LIMIT 1');

  AVendaParcela.First;
  while not AVendaParcela.Eof do
  begin
    Movimento := TContaCorrenteMovimento.Create;
    try
      Movimento.FDocumento              := AVenda.GetAsString('ID');
      Movimento.FDescricao              := VENDA_VAREJO + ' ' + AVenda.GetAsString('ID');
      Movimento.FObservacao             := '';
      Movimento.FDhCadastro             := AVenda.GetAsString('DH_CADASTRO');
      Movimento.FDtEmissao              := AVendaParcela.GetAsString('DT_VENCIMENTO');
      Movimento.FDtMovimento            := AVendaParcela.GetAsString('DT_VENCIMENTO');
      Movimento.FVlMovimento            := AVendaParcela.GetAsFloat('VL_TITULO');

      if AVenda.GetAsString('TIPO').Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
        Movimento.FTpMovimento          := DEBITO
      else
        Movimento.FTpMovimento          := CREDITO;

      Movimento.FBoConciliado           := 'S';
      Movimento.FDhConciliado           := AVendaParcela.GetAsString('DT_VENCIMENTO');
      Movimento.FVlSaldoConciliado      := 0;
      Movimento.FVlSaldoGeral           := 0;
      Movimento.FIdContaCorrente        := fqContaCorrente.GetAsInteger('ID');
      Movimento.FIdPessoa               := AVenda.GetAsInteger('ID_PESSOA');
      Movimento.FIdContaAnalise         := AVenda.GetAsInteger('ID_CONTA_ANALISE');
      Movimento.FIdCentroResultado      := AVenda.GetAsInteger('ID_CENTRO_RESULTADO');
      Movimento.FBoMovimentoOrigem      := 'N';
      Movimento.FIdMovimentoOrigem      := 0;
      Movimento.FBoMovimentoDestino     := 'N';
      Movimento.FIdMovimentoDestino     := 0;
      Movimento.FBoTransferencia        := 'N';
      Movimento.FIdContaCorrenteDestino := 0;
      Movimento.FTPDocumentoOrigem      := VENDA_VAREJO;
      Movimento.FIdDocumentoOrigem      := AVendaParcela.GetAsInteger('ID');
      Movimento.FIdChaveProcesso        := AVenda.GetAsInteger('ID_CHAVE_PROCESSO');

      MovimentoJSON := TJson.ObjectToJsonString(Movimento);

      result := TContaCorrenteServ.GerarMovimentoContaCorrente(MovimentoJSON);
    finally
      Movimento.Free;
    end;
    AVendaParcela.Proximo;
  end;
end;}

class function TVendaServ.GerarMovimentacaoEstoque(AVenda,
  AVendaItem: TFastQuery; AAcao: String): boolean;
var tipoMovimentacao: String;
  estoque: TProdutoMovimento;
  objectMovimentoEstoqueJSON: String;
  realizarReservaEstoque: Boolean;
begin
  try
    estoque := TProdutoMovimento.Create;
    try
      if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA then
      begin
        tipoMovimentacao := MOVIMENTO_ENTRADA;
      end
      else if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_SAIDA then
      begin
        tipoMovimentacao := MOVIMENTO_SAIDA;
      end;

      realizarReservaEstoque := AVenda.GetAsString('TIPO').Equals(TIPO_DOCUMENTO_CONDICIONAL);

      AVendaItem.First;
      while not AVendaItem.Eof do
      begin
        with estoque do
        begin
          FIdProduto         := AVendaItem.GetAsInteger('ID_PRODUTO');
          FIdFilial          := AVenda.GetAsInteger('ID_FILIAL');
          FIdChaveProcesso   := AVenda.GetAsInteger('ID_CHAVE_PROCESSO');
          FTipo              := tipoMovimentacao;
          FDocumentoOrigem   := DOCUMENTO_VENDA;
          FDtMovimento       := DateToStr(Date);
          FQtEstoqueAnterior := TProdutoServ.GetEstoqueAtual(FIdProduto, FIdFilial);
          FQtMovimento       := AVendaItem.GetAsFloat('QUANTIDADE');
          FQtEstoqueAtual    := FQtEstoqueAnterior - FQtMovimento;
        end;
        objectMovimentoEstoqueJSON := TJson.ObjectToJsonString(estoque);
        TProdutoMovimentoServ.GerarMovimentacaoProduto(objectMovimentoEstoqueJSON);

        if realizarReservaEstoque then
        begin
          TProdutoReservaEstoqueMovimentoServ.RealizarReservaDeEstoque(estoque, condicional);
        end;

        AVendaItem.Next;
      end;
      result := true;
    finally
      FreeAndNil(estoque);
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar a movimenta��o de Estoque.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GetChaveProcesso(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_chave_processo FROM venda WHERE id = :id_',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_chave_processo');
end;

class function TVendaServ.GetId(AIdChaveProcesso: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id FROM venda WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := consulta.GetAsInteger('ID');
end;

class function TVendaServ.GetIdNotaFiscalNFCE(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_nota_fiscal_nfce FROM venda WHERE id = :id',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_nota_fiscal_nfce');
end;

class function TVendaServ.GetIdNotaFiscalNFE(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_nota_fiscal_nfe FROM venda WHERE id = :id',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_nota_fiscal_nfe');
end;

class function TVendaServ.GetTotaisPorTipoVenda(
  const AIdsVenda: String): TFDJSONDatasets;
var
  qConsulta: IFastQuery;
  SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select vendas, condicionais, pedidos, orcamentos, devolucoes'+
  '   from ('+
  ' select (select sum(vl_venda) from venda where tipo = ''VENDA'' and status <> '''+STATUS_VENDA_CANCELADA+''' and id in ('+AIdsVenda+')) AS vendas'+
  '       ,(select sum(vl_venda) from venda where tipo = ''PEDIDO'' and status <> '''+STATUS_VENDA_CANCELADA+''' and id in ('+AIdsVenda+')) AS pedidos'+
  '       ,(select sum(vl_venda) from venda where tipo = ''OR�AMENTO'' and status <> '''+STATUS_VENDA_CANCELADA+''' and id in ('+AIdsVenda+')) AS orcamentos'+
  '       ,(select sum(vl_venda) from venda where tipo = ''CONDICIONAL'' and status <> '''+STATUS_VENDA_CANCELADA+''' and id in ('+AIdsVenda+')) AS condicionais'+
  '       ,(select sum(vl_venda) from venda where tipo = ''DEVOLU��O'' and status <> '''+STATUS_VENDA_CANCELADA+''' and id in ('+AIdsVenda+')) AS devolucoes'+
  ' from'+
  '   filial limit 1) totais_titulo';

  qConsulta := TFastQuery.ModoDeConsulta(SQL);
  TFDJSONDataSetsWriter.ListAdd(Result, (qConsulta as TFastQuery));
end;

class function TVendaServ.GetVenda(AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM venda WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TVendaServ.GetVendaItem(AIdVenda: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM venda_item WHERE id_venda = :id_venda',
    TArray<Variant>.Create(AIdVenda));
end;

class function TVendaServ.GetVendaParcela(
  AIdVenda: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM venda_parcela WHERE id_venda = :id_venda',
    TArray<Variant>.Create(AIdVenda));
end;


class function TVendaServ.GetVendaReceitaOtica(AIdVenda: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM venda_receita_otica WHERE id_venda = :id_venda',
    TArray<Variant>.Create(AIdVenda));
end;

class function TVendaServ.GetVendaProxy(
    const AIdVenda: Integer): TVendaProxy;
const
  SQL = 'SELECT * FROM VENDA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TVendaProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdVenda));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FVlDesconto := qConsulta.GetAsFloat('VL_DESCONTO');
  result.FVlAcrescimo := qConsulta.GetAsFloat('VL_ACRESCIMO');
  result.FVlTotalProduto := qConsulta.GetAsFloat('VL_TOTAL_PRODUTO');
  result.FVlVenda := qConsulta.GetAsFloat('VL_VENDA');
  result.FStatus := qConsulta.GetAsString('STATUS');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdOperacao := qConsulta.GetAsInteger('ID_OPERACAO');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FIdCentroResultado := qConsulta.GetAsInteger('ID_CENTRO_RESULTADO');
  result.FIdContaAnalise := qConsulta.GetAsInteger('ID_CONTA_ANALISE');
  result.FIdPlanoPagamento := qConsulta.GetAsInteger('ID_PLANO_PAGAMENTO');
  result.FIdFormaPagamento := qConsulta.GetAsInteger('ID_FORMA_PAGAMENTO');
  result.FPercDesconto := qConsulta.GetAsFloat('PERC_DESCONTO');
  result.FPercAcrescimo := qConsulta.GetAsFloat('PERC_ACRESCIMO');
  result.FIdTabelaPreco := qConsulta.GetAsInteger('ID_TABELA_PRECO');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FDhFechamento := qConsulta.GetAsString('DH_FECHAMENTO');
  result.FVlPagamento := qConsulta.GetAsFloat('VL_PAGAMENTO');
  result.FVlPessoaCreditoUtilizado := qConsulta.GetAsFloat('VL_PESSOA_CREDITO_UTILIZADO');
  result.FIdVendedor := qConsulta.GetAsInteger('ID_VENDEDOR');
  result.FIdNotaFiscalNFE := qConsulta.GetAsInteger('ID_NOTA_FISCAL_NFE');
  result.FIdOperacaoFiscal := qConsulta.GetAsInteger('ID_OPERACAO_FISCAL');
  result.FIdNotaFiscalNFSE := qConsulta.GetAsInteger('ID_NOTA_FISCAL_NFSE');
  result.FIdNotaFiscalNFCE := qConsulta.GetAsInteger('ID_NOTA_FISCAL_NFCE');
end;

class function TVendaServ.GerarVenda(
    const AVenda: TVendaProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('VENDA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', AVenda.FDhCadastro);
    qEntidade.SetAsFloat('VL_DESCONTO', AVenda.FVlDesconto);
    qEntidade.SetAsFloat('VL_ACRESCIMO', AVenda.FVlAcrescimo);
    qEntidade.SetAsFloat('VL_TOTAL_PRODUTO', AVenda.FVlTotalProduto);
    qEntidade.SetAsFloat('VL_VENDA', AVenda.FVlVenda);
    qEntidade.SetAsString('STATUS', AVenda.FStatus);

    if AVenda.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', AVenda.FIdPessoa);
    end;

    if AVenda.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AVenda.FIdFilial);
    end;

    if AVenda.FIdOperacao > 0 then
    begin
      qEntidade.SetAsInteger('ID_OPERACAO', AVenda.FIdOperacao);
    end;

    if AVenda.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AVenda.FIdChaveProcesso);
    end;

    if AVenda.FIdCentroResultado > 0 then
    begin
      qEntidade.SetAsInteger('ID_CENTRO_RESULTADO', AVenda.FIdCentroResultado);
    end;

    if AVenda.FIdContaAnalise > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_ANALISE', AVenda.FIdContaAnalise);
    end;

    if AVenda.FIdPlanoPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_PLANO_PAGAMENTO', AVenda.FIdPlanoPagamento);
    end;

    if AVenda.FIdFormaPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_FORMA_PAGAMENTO', AVenda.FIdFormaPagamento);
    end;
    qEntidade.SetAsFloat('PERC_DESCONTO', AVenda.FPercDesconto);
    qEntidade.SetAsFloat('PERC_ACRESCIMO', AVenda.FPercAcrescimo);

    if AVenda.FIdTabelaPreco > 0 then
    begin
      qEntidade.SetAsInteger('ID_TABELA_PRECO', AVenda.FIdTabelaPreco);
    end;
    qEntidade.SetAsString('TIPO', AVenda.FTipo);
    qEntidade.SetAsString('DH_FECHAMENTO', AVenda.FDhFechamento);
    qEntidade.SetAsFloat('VL_PAGAMENTO', AVenda.FVlPagamento);
    qEntidade.SetAsFloat('VL_PESSOA_CREDITO_UTILIZADO', AVenda.FVlPessoaCreditoUtilizado);

    if AVenda.FIdNotaFiscalNFE > 0 then
    begin
      qEntidade.SetAsInteger('ID_NOTA_FISCAL_NFE', AVenda.FIdNotaFiscalNFE);
    end;

    if AVenda.FIdOperacaoFiscal > 0 then
    begin
      qEntidade.SetAsInteger('ID_OPERACAO_FISCAL', AVenda.FIdOperacaoFiscal);
    end;

    if AVenda.FIdNotaFiscalNFSE > 0 then
    begin
      qEntidade.SetAsInteger('ID_NOTA_FISCAL_NFSE', AVenda.FIdNotaFiscalNFSE);
    end;

    if AVenda.FIdNotaFiscalNFCE > 0 then
    begin
      qEntidade.SetAsInteger('ID_NOTA_FISCAL_NFCE', AVenda.FIdNotaFiscalNFCE);
    end;

    qEntidade.Salvar;
    qEntidade.Persistir;

    AVenda.FID := qEntidade.GetAsInteger('ID');
    result :=     AVenda.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Venda.' + 'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarDevolucaoCreditandoNoContasAReceber(const AIdChaveProcesso: Integer;
  const AValorDevolucao: Double; AIdsContasReceber: String): Boolean;
var
  valorJuros: Double;
  valorMulta: Double;
  valorQuitacao: Double;

  saldoParaAbatimento: Double;
  qContasReceber: IFastQuery;
  venda: TVendaProxy;
begin
  if AValorDevolucao <= 0 then
  begin
    exit;
  end;

  saldoParaAbatimento := AValorDevolucao;

  venda := TVendaServ.GetVendaProxy(TVendaServ.GetId(AIdChaveProcesso));

  qContasReceber := TFastQuery.ModoDeConsulta(
    'SELECT cr.* FROM conta_receber cr WHERE cr.id in ('+AIdsContasReceber+')',
    TArray<Variant>.Create(venda.FIdPessoa));

  {Realizar o recebimento da multa}
{  qContasReceber.First;
  while not qContasReceber.Eof do
  begin
    valorMulta := TContaReceberServ.GetValorMulta(qContasReceber.GetAsInteger('ID'));
    if (valorMulta > 0) then
    begin
      if (valorMulta > saldoParaAbatimento) then
      begin
        valorMulta := saldoParaAbatimento;
      end;

      GerarQuitacaoMultaNoContaReceber(venda, (qContasReceber as TFastQuery), valorMulta);
      saldoParaAbatimento := saldoParaAbatimento - valorMulta;
    end;

    if saldoParaAbatimento <= 0 then
    begin
      break;
    end;

    qContasReceber.Next;
  end;  }

  {Realizar o recebimento do Juros}
{  qContasReceber.First;
  while not qContasReceber.Eof do
  begin
    valorJuros := TContaReceberServ.GetValorJuros(qContasReceber.GetAsInteger('ID'));
    if (valorJuros > 0) then
    begin
      if (valorJuros > saldoParaAbatimento) then
      begin
        valorJuros := saldoParaAbatimento;
      end;

      GerarQuitacaoJurosNoContaReceber(venda, (qContasReceber as TFastQuery), valorJuros);
      saldoParaAbatimento := saldoParaAbatimento - valorJuros;
    end;

    if saldoParaAbatimento <= 0 then
    begin
      break;
    end;

    qContasReceber.Next;
  end;     }

  {Realizar o recebimento do valor em aberto}
  qContasReceber.First;
  while not qContasReceber.Eof do
  begin
    valorQuitacao := TContaReceberServ.GetValorAberto(qContasReceber.GetAsInteger('ID'));
    if (valorQuitacao > 0) then
    begin
      if (valorQuitacao > saldoParaAbatimento) then
      begin
        valorQuitacao := saldoParaAbatimento;
      end;

      GerarQuitacaoValorAbertoNoContaReceber(venda, (qContasReceber as TFastQuery), valorQuitacao);
      saldoParaAbatimento := saldoParaAbatimento - valorQuitacao;
    end;

    if saldoParaAbatimento <= 0 then
    begin
      break;
    end;

    qContasReceber.Next;
  end;
end;

class function TVendaServ.GerarQuitacaoMultaNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
  AVlMulta: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
  Movimento: TContaCorrenteMovimento;
begin
  try
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber     := AContaReceber.GetAsInteger('ID');
      Quitacao.FDhCadastro         := DateTimeToStr(Now);
      Quitacao.FDtQuitacao         := DatetoStr(Date);
      Quitacao.FIdFilial           := AContaReceber.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao     := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
           AContaReceber.GetAsInteger('ID_FORMA_PAGAMENTO'));
      Quitacao.FIdContaCorrente    := AContaReceber.GetAsInteger('ID_CONTA_CORRENTE');
      Quitacao.FIdContaAnalise     := AContaReceber.GetAsInteger('ID_CONTA_ANALISE');
      Quitacao.FIdCentroResultado  := AContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
      Quitacao.FVlDesconto         := 0;
      Quitacao.FVlAcrescimo        := AVlMulta;
      Quitacao.FVlMulta            := AVlMulta;
      Quitacao.FPercMulta          := 0;
      Quitacao.FPercAcrescimo      := 0;
      Quitacao.FPercDesconto       := 0;
      Quitacao.FVlQuitacao         := 0;
      Quitacao.FVlTotal            := 0;

      Quitacao.FIdDocumentoOrigem := AVenda.FId;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      Quitacao.FObservacao := 'ESTORNO FINANCEIRO - DEVOLU��O DE VENDA N� '+InttoStr(AVenda.FId);

      Quitacao.FId := TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(AContaReceber.GetAsInteger('ID'));

      Movimento := TContaCorrenteMovimento.Create;
      try
        Movimento.FDocumento         := InttoStr(Quitacao.FIdDocumentoOrigem);
        Movimento.FDescricao         := AContaReceber.GetAsString('DESCRICAO');
        Movimento.FObservacao        := Quitacao.FObservacao;
        Movimento.FDhCadastro        := DateTimeToStr(Now);
        Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
        Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
        Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
        Movimento.FVlMovimento       := AVlMulta;

        Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

        Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
        Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
        Movimento.FIdCentroResultado := Quitacao.FIdCentroResultado;
        Movimento.FIdChaveProcesso   := AContaReceber.GetAsInteger('ID_CHAVE_PROCESSO');
        Movimento.FIdFilial          := Quitacao.FIdFilial;
        Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
        Movimento.FIdDocumentoOrigem := Quitacao.FIdDocumentoOrigem;
        Movimento.FBoConciliado      := 'S';

        TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
      finally
        Movimento.Free;
      end;
    finally
      Quitacao.Free;
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar quita��o de multa no contas a receber referente ao estorno financeiro.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarQuitacaoJurosNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
  AVlJuros: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
  Movimento: TContaCorrenteMovimento;
begin
  try
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber     := AContaReceber.GetAsInteger('ID');
      Quitacao.FDhCadastro         := DateTimeToStr(Now);
      Quitacao.FDtQuitacao         := DateToStr(Date);
      Quitacao.FIdFilial           := AContaReceber.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao     := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
           AContaReceber.GetAsInteger('ID_FORMA_PAGAMENTO'));
      Quitacao.FIdContaCorrente    := AContaReceber.GetAsInteger('ID_CONTA_CORRENTE');
      Quitacao.FIdContaAnalise     := AContaReceber.GetAsInteger('ID_CONTA_ANALISE');
      Quitacao.FIdCentroResultado  := AContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
      Quitacao.FVlDesconto         := 0;
      Quitacao.FVlAcrescimo        := AVlJuros;
      Quitacao.FVlMulta            := AVlJuros;
      Quitacao.FPercMulta          := 0;
      Quitacao.FPercAcrescimo      := 0;
      Quitacao.FPercDesconto       := 0;
      Quitacao.FVlQuitacao         := 0;
      Quitacao.FVlTotal            := 0;

      Quitacao.FIdDocumentoOrigem := AVenda.FId;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      Quitacao.FObservacao := 'ESTORNO FINANCEIRO - DEVOLU��O DE VENDA N� '+InttoStr(AVenda.FId);

      Quitacao.FId := TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(AContaReceber.GetAsInteger('ID'));

      Movimento := TContaCorrenteMovimento.Create;
      try
        Movimento.FDocumento         := InttoStr(Quitacao.FIdDocumentoOrigem);
        Movimento.FDescricao         := AContaReceber.GetAsString('DESCRICAO');
        Movimento.FObservacao        := Quitacao.FObservacao;
        Movimento.FDhCadastro        := DateTimeToStr(Now);
        Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
        Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
        Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
        Movimento.FVlMovimento       := AVlJuros;

        Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

        Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
        Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
        Movimento.FIdCentroResultado := Quitacao.FIdCentroResultado;
        Movimento.FIdChaveProcesso   := AContaReceber.GetAsInteger('ID_CHAVE_PROCESSO');
        Movimento.FIdFilial          := Quitacao.FIdFilial;
        Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
        Movimento.FIdDocumentoOrigem := Quitacao.FIdDocumentoOrigem;
        Movimento.FBoConciliado      := 'S';

        TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
      finally
        Movimento.Free;
      end;
    finally
      Quitacao.Free;
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar quita��o de juros no contas a receber referente ao estorno financeiro.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.GerarQuitacaoValorAbertoNoContaReceber(AVenda: TVendaProxy; AContaReceber: TFastQuery;
  AVlQuitacao: Double): Boolean;
var
  Quitacao  : TContaReceberQuitacaoMovimento;
  Movimento: TContaCorrenteMovimento;
begin
  try
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber     := AContaReceber.GetAsInteger('ID');
      Quitacao.FDhCadastro         := DateTimeToStr(Now);
      Quitacao.FDtQuitacao         := DateToStr(Date);
      Quitacao.FIdFilial           := AContaReceber.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao     := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
           AContaReceber.GetAsInteger('ID_FORMA_PAGAMENTO'));
      Quitacao.FIdContaCorrente    := AContaReceber.GetAsInteger('ID_CONTA_CORRENTE');
      Quitacao.FIdContaAnalise     := AContaReceber.GetAsInteger('ID_CONTA_ANALISE');
      Quitacao.FIdCentroResultado  := AContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
      Quitacao.FVlDesconto         := 0;
      Quitacao.FVlAcrescimo        := 0;
      Quitacao.FVlMulta            := 0;
      Quitacao.FPercMulta          := 0;
      Quitacao.FPercAcrescimo      := 0;
      Quitacao.FPercDesconto       := 0;
      Quitacao.FVlQuitacao         := AVlQuitacao;
      Quitacao.FVlTotal            := AVlQuitacao;

      Quitacao.FIdDocumentoOrigem := AVenda.FId;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_VENDA;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      Quitacao.FObservacao := 'ESTORNO FINANCEIRO - DEVOLU��O DE VENDA N� '+InttoStr(AVenda.FId);

      Quitacao.FId := TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(AContaReceber.GetAsInteger('ID'));

      Movimento := TContaCorrenteMovimento.Create;
      try
        Movimento.FDocumento         := InttoStr(Quitacao.FIdDocumentoOrigem);
        Movimento.FDescricao         := AContaReceber.GetAsString('DESCRICAO');
        Movimento.FObservacao        := Quitacao.FObservacao;
        Movimento.FDhCadastro        := DateTimeToStr(Now);
        Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
        Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
        Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
        Movimento.FVlMovimento       := AVlQuitacao;

        Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

        Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
        Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
        Movimento.FIdCentroResultado := Quitacao.FIdCentroResultado;
        Movimento.FIdChaveProcesso   := AContaReceber.GetAsInteger('ID_CHAVE_PROCESSO');
        Movimento.FIdFilial          := Quitacao.FIdFilial;
        Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
        Movimento.FIdDocumentoOrigem := Quitacao.FIdDocumentoOrigem;
        Movimento.FBoConciliado      := 'S';

        TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
      finally
        Movimento.Free;
      end;
    finally
      Quitacao.Free;
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar quita��o no contas a receber referente ao estorno financeiro.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TVendaServ.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TVendaServ.PodeExcluir(AIdChaveProcesso);
end;

class function TVendaServ.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var
  existeCRQuitado: boolean;
  CRReferenteEntradaQuitacao: Boolean;
  CRReferenteCartaoCredito: Boolean;
  valorEntrada: Double;
  valorQuitacaoTotal: Double;
  valorQuitacaoCartao: Double;
begin
  result := false;

  existeCRQuitado := TContaReceberServ.ExisteQuitacao(AIdChaveProcesso);

  if existeCRQuitado then
  begin
    valorEntrada := TVendaServ.ValorEntradaQuitacao(AIdChaveProcesso);
    valorQuitacaoTotal := TContaReceberServ.TotalQuitacaoRealizada(AIdChaveProcesso);
    valorQuitacaoCartao := TContaReceberServ.TotalQuitacaoRealizadaPorCartao(AIdChaveProcesso);

    if valorEntrada > 0 then
    begin
      CRReferenteEntradaQuitacao := (valorQuitacaoTotal = valorEntrada);

      if CRReferenteEntradaQuitacao then
      begin
        result := true;
        exit;
      end;
    end;

    if valorQuitacaoCartao > 0 then
    begin
      CRReferenteCartaoCredito := ((valorQuitacaoCartao + valorEntrada) = valorQuitacaoTotal);

      if CRReferenteCartaoCredito then
      begin
        result := true;
        exit;
      end;
    end;

    result := false;
  end
  else
  begin
    result := true;
  end;
end;

class function TVendaServ.ValorEntradaQuitacao(
  AIdChaveProcesso: Integer): Double;
var
  qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT vl_pagamento'+
    ' FROM venda'+
    ' WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qContaReceber.getAsFloat('vl_pagamento');
end;

end.
