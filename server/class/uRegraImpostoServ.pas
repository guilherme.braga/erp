unit uRegraImpostoServ;

Interface

Uses uRegraImpostoProxy, uFactoryQuery, SysUtils;

type TRegraImpostoServ = class
  class function GetRegraImposto(const AIdRegraImposto: Integer): TRegraImpostoProxy;
end;

implementation

{ TRegraImpostoServ }

class function TRegraImpostoServ.GetRegraImposto(
    const AIdRegraImposto: Integer): TRegraImpostoProxy;
const
  SQL = 'SELECT * FROM regra_imposto WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TRegraImpostoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdRegraImposto));

  result.FId := qConsulta.GetAsString('ID');
  result.FIdNcm := qConsulta.GetAsInteger('ID_NCM');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FExcecaoIpi := qConsulta.GetAsInteger('EXCECAO_IPI');
  result.FIdNcmEspecializado := qConsulta.GetAsInteger('ID_NCM_ESPECIALIZADO');
end;

end.


