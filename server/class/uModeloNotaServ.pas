unit uModeloNotaServ;

Interface

Uses uModeloNotaProxy, SysUtils;

type TModeloNotaServ = class
  class function GetModeloNota(const AIdModeloNota: Integer): TModeloNotaProxy;
  class function GerarModeloNota(const AModeloNota: TModeloNotaProxy): Integer;
  class function BuscarIdModelo(const AModeloNota: String): Integer;
  class function GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
end;

implementation

{ TModeloNotaServ }

uses uFactoryQuery;

class function TModeloNotaServ.GetModeloNota(
    const AIdModeloNota: Integer): TModeloNotaProxy;
const
  SQL = 'SELECT * FROM MODELO_NOTA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TModeloNotaProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdModeloNota));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FModelo := qConsulta.GetAsString('MODELO');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
end;

class function TModeloNotaServ.GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
const
  SQL = 'SELECT modelo FROM MODELO_NOTA WHERE id = :id';
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(AIdModeloNota));
  result := qConsulta.GetAsString('modelo');
end;

class function TModeloNotaServ.BuscarIdModelo(const AModeloNota: String): Integer;
const
  SQL = 'SELECT id FROM MODELO_NOTA WHERE modelo = :modelo';
var
  qConsulta: IFastQuery;
  qInclusao: TFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(AModeloNota));

  if not qConsulta.IsEmpty then
  begin
    result := qConsulta.GetAsInteger('id');
  end
  else
  begin
    qInclusao := TFastQuery.ModoDeInclusao('MODELO_NOTA');
    try
      qInclusao.Incluir;
      qInclusao.SetAsString('modelo', AModeloNota);
      qInclusao.SetAsString('descricao', AModeloNota);
      qInclusao.Salvar;
      qInclusao.Persistir;

      result := qInclusao.GetAsInteger('id');
    finally
      FreeAndNil(qInclusao);
    end;
  end;
end;

class function TModeloNotaServ.GerarModeloNota(
    const AModeloNota: TModeloNotaProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('MODELO_NOTA');
    qEntidade.Incluir;

    qEntidade.SetAsString('MODELO', AModeloNota.FModelo);
    qEntidade.SetAsString('DESCRICAO', AModeloNota.FDescricao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AModeloNota.FID := qEntidade.GetAsInteger('ID');
    result :=     AModeloNota.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ModeloNota.'+'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
