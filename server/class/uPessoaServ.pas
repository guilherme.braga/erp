unit uPessoaServ;

interface

uses System.SysUtils, FireDAC.Comp.Client, Data.FireDACJSONReflect, REST.Json, uPessoaProxy, uFactoryQuery;

type TPessoaServ = class
  class function GetNomePessoa(const AIDPessoa: Integer): String;
  class function GetPessoa(const AIdPessoa: Integer): TPessoaProxy;
  class function GetPessoaQuery(const AIdPessoa: Integer): TFastQuery;
  class function GerarPessoa(const APessoa: TPessoaProxy): Integer;
  class procedure SetClienteConsumidorFinal(const AIdPessoa: Integer);
  class function VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
  class function ValidaCnpjCeiCpf(Numero: String): Boolean;
  class function ValidarInscricaoEstadual(const AInscricaoEstadual, AUf: String): Boolean;
  class function GetInformacoesFiltroImposto(const AIdPessoa: Integer): TFastQuery;
  class function PossuiCPFValido(const AIdPessoa: Integer): Boolean;
end;

type TPessoaEnderecoServ = class
  class function GetPessoaEndereco(const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
  class function GerarPessoaEndereco(const APessoaEndereco: TPessoaEnderecoProxy): Integer;
  class function GetPessoaEnderecoParaNotaFiscal(const AIdPessoa: Integer): TPessoaEnderecoProxy;
  class function GetIDEstadoParaNotaFiscal(const AIdPessoa: Integer): Integer;
end;

type TPessoaContatoServ = class
  class function GetPessoaContato(
    const AIdPessoaContato: Integer): TPessoaContatoProxy;
  class function GerarPessoaContato(
    const APessoaContato: TPessoaContatoProxy): Integer;
  class function GetPessoaContatoPrincipal(const AIdPessoa: Integer): TPessoaContatoPrincipalProxy;
  class function GetEmailDocumentoFiscal(const AIdPessoa: Integer): String;
end;


implementation

{ TPessoaServ }

uses ACBrValidador, uCEPServ;

class function TPessoaServ.GetInformacoesFiltroImposto(const AIdPessoa: Integer): TFastQuery;
const
  SQL_PESSOA_REGRA_IMPOSTO_FILTRO =
  ' SELECT tipo_empresa'+
  '       ,regime_tributario'+
  '       ,bo_contribuinte_icms'+
  '       ,bo_contribuinte_ipi'+
  '       ,id_zoneamento'+
  '       ,id_regime_especial'+
  '       ,id_estado'+
  '   FROM pessoa'+
  '  WHERE id = :id ';
begin
  result := TFastQuery.ModoDeConsulta(SQL_PESSOA_REGRA_IMPOSTO_FILTRO, TArray<Variant>.Create(AIdPessoa));
end;

class function TPessoaServ.GetNomePessoa(const AIDPessoa: Integer): String;
var qPessoaItem: IFastQuery;
begin
  qPessoaItem := TFastQuery.ModoDeConsulta(
    'SELECT nome FROM pessoa WHERE id = :id',
    TArray<Variant>.Create(AIDPessoa));

  result := qPessoaItem.GetAsString('nome');
end;

class function TPessoaServ.GetPessoa(
    const AIdPessoa: Integer): TPessoaProxy;
const
  SQL = 'SELECT * FROM PESSOA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoa));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FTpPessoa := qConsulta.GetAsString('TP_PESSOA');
  result.FBoCliente := qConsulta.GetAsString('BO_CLIENTE');
  result.FBoFornecedor := qConsulta.GetAsString('BO_FORNECEDOR');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FBoTransportadora := qConsulta.GetAsString('BO_TRANSPORTADORA');
  result.FIdGrupoPessoa := qConsulta.GetAsInteger('ID_GRUPO_PESSOA');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FDtAssociacao := qConsulta.GetAsString('DT_ASSOCIACAO');
  result.FRazaoSocial := qConsulta.GetAsString('RAZAO_SOCIAL');
  result.FNome := qConsulta.GetAsString('NOME');
  result.FDocFederal := qConsulta.GetAsString('DOC_FEDERAL');
  result.FDocEstadual := qConsulta.GetAsString('DOC_ESTADUAL');
  result.FDtNascimento := qConsulta.GetAsString('DT_NASCIMENTO');
  result.FNomePai := qConsulta.GetAsString('NOME_PAI');
  result.FNomeMae := qConsulta.GetAsString('NOME_MAE');
  result.FOrgaoEmissor := qConsulta.GetAsString('ORGAO_EMISSOR');
  result.FSexo := qConsulta.GetAsString('SEXO');
  result.FEmpresa := qConsulta.GetAsString('EMPRESA');
  result.FEnderecoEmpresa := qConsulta.GetAsString('ENDERECO_EMPRESA');
  result.FNumeroEmpresa := qConsulta.GetAsInteger('NUMERO_EMPRESA');
  result.FCepEmpresa := qConsulta.GetAsString('CEP_EMPRESA');
  result.FCnpjEmpresa := qConsulta.GetAsString('CNPJ_EMPRESA');
  result.FBairroEmpresa := qConsulta.GetAsString('BAIRRO_EMPRESA');
  result.FDtAdmissao := qConsulta.GetAsString('DT_ADMISSAO');
  result.FTelefoneEmpresa := qConsulta.GetAsString('TELEFONE_EMPRESA');
  result.FRamalEmpresa := qConsulta.GetAsString('RAMAL_EMPRESA');
  result.FRendaMensal := qConsulta.GetAsFloat('RENDA_MENSAL');
  result.FSuframa := qConsulta.GetAsString('SUFRAMA');
  result.FDocMunicipal := qConsulta.GetAsString('DOC_MUNICIPAL');
  result.FIdNaturalidade := qConsulta.GetAsInteger('ID_NATURALIDADE');
  result.FIdCidadeEmpresa := qConsulta.GetAsInteger('ID_CIDADE_EMPRESA');
  result.FIdUfEmissor := qConsulta.GetAsInteger('ID_UF_EMISSOR');
  result.FIdAreaAtuacao := qConsulta.GetAsInteger('ID_AREA_ATUACAO');
  result.FIdClassificacaoEmpresa := qConsulta.GetAsInteger('ID_CLASSIFICACAO_EMPRESA');
  result.FIdAtividadePrincipal := qConsulta.GetAsInteger('ID_ATIVIDADE_PRINCIPAL');
  result.FEstadoCivil := qConsulta.GetAsString('ESTADO_CIVIL');
  result.FTempoTrabalhoEmpresa := qConsulta.GetAsString('TEMPO_TRABALHO_EMPRESA');
  result.FQuantidadeFilhos := qConsulta.GetAsInteger('QUANTIDADE_FILHOS');
  result.FTipoResidencia := qConsulta.GetAsString('TIPO_RESIDENCIA');
  result.FRendaExtra := qConsulta.GetAsString('RENDA_EXTRA');
  result.FVlRendaExtra := qConsulta.GetAsFloat('VL_RENDA_EXTRA');
  result.FVlLimite := qConsulta.GetAsFloat('VL_LIMITE');
  result.FOcupacao := qConsulta.GetAsString('OCUPACAO');
  result.FIdZoneamento := qConsulta.GetAsInteger('ID_ZONEAMENTO');
  result.FCrt := qConsulta.GetAsInteger('CRT');
  result.FRegimeTributario := qConsulta.GetAsInteger('REGIME_TRIBUTARIO');
  result.FBoContribuinteIcms := qConsulta.GetAsString('BO_CONTRIBUINTE_ICMS');
  result.FBoContribuinteIpi := qConsulta.GetAsString('BO_CONTRIBUINTE_IPI');
  result.FBoContribuinte := qConsulta.GetAsString('BO_CONTRIBUINTE');
  result.FIdRegimeEspecial := qConsulta.GetAsInteger('ID_REGIME_ESPECIAL');
  result.FTipoEmpresa := qConsulta.GetAsString('TIPO_EMPRESA');
  result.FIdPessoaImportacao := qConsulta.GetAsInteger('ID_PESSOA_IMPORTACAO');
  result.FBoClienteConsumidorFinal := qConsulta.GetAsString('BO_CLIENTE_CONSUMIDOR_FINAL');
end;

class function TPessoaServ.GetPessoaQuery(const AIdPessoa: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM pessoa WHERE id = :id', TArray<Variant>.Create(AIdPessoa));
end;

class function TPessoaServ.PossuiCPFValido(const AIdPessoa: Integer): Boolean;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta('SELECT doc_federal FROM pessoa WHERE id = :id',
    TArray<Variant>.Create(AIdPessoa));

  result := TPessoaServ.ValidaCnpjCeiCpf(qConsulta.GetAsString('doc_federal'));
end;

class procedure TPessoaServ.SetClienteConsumidorFinal(const AIdPessoa: Integer);
begin
  try
    TFastQuery.ExecutarScript(
      'UPDATE pessoa SET bo_cliente_consumidor_final = ''N'' WHERE bo_cliente_consumidor_final = ''S''', nil);

    TFastQuery.ExecutarScript(
      'UPDATE pessoa SET bo_cliente_consumidor_final = ''S'' WHERE id = :id',
      TArray<Variant>.Create(AIdPessoa));
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao configurar cliente como consumidor final.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaServ.VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta('SELECT bo_cliente_consumidor_final FROM pessoa WHERE id = :id',
    TArray<Variant>.Create(AIdPessoa));

  result := qConsulta.GetAsString('bo_cliente_consumidor_final').Equals('S');
end;

class function TPessoaServ.GerarPessoa(
    const APessoa: TPessoaProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('PESSOA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', APessoa.FDhCadastro);
    qEntidade.SetAsString('TP_PESSOA', APessoa.FTpPessoa);
    qEntidade.SetAsString('BO_CLIENTE', APessoa.FBoCliente);
    qEntidade.SetAsString('BO_FORNECEDOR', APessoa.FBoFornecedor);
    qEntidade.SetAsString('BO_ATIVO', APessoa.FBoAtivo);
    qEntidade.SetAsString('BO_TRANSPORTADORA', APessoa.FBoTransportadora);

    if APessoa.FIdGrupoPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_GRUPO_PESSOA', APessoa.FIdGrupoPessoa);
    end;
    qEntidade.SetAsString('OBSERVACAO', APessoa.FObservacao);
    qEntidade.SetAsString('DT_ASSOCIACAO', APessoa.FDtAssociacao);
    qEntidade.SetAsString('RAZAO_SOCIAL', APessoa.FRazaoSocial);
    qEntidade.SetAsString('NOME', APessoa.FNome);
    qEntidade.SetAsString('DOC_FEDERAL', APessoa.FDocFederal);
    qEntidade.SetAsString('DOC_ESTADUAL', APessoa.FDocEstadual);
    qEntidade.SetAsString('DT_NASCIMENTO', APessoa.FDtNascimento);
    qEntidade.SetAsString('NOME_PAI', APessoa.FNomePai);
    qEntidade.SetAsString('NOME_MAE', APessoa.FNomeMae);
    qEntidade.SetAsString('ORGAO_EMISSOR', APessoa.FOrgaoEmissor);
    qEntidade.SetAsString('SEXO', APessoa.FSexo);
    qEntidade.SetAsString('EMPRESA', APessoa.FEmpresa);
    qEntidade.SetAsString('ENDERECO_EMPRESA', APessoa.FEnderecoEmpresa);
    qEntidade.SetAsInteger('NUMERO_EMPRESA', APessoa.FNumeroEmpresa);
    qEntidade.SetAsString('CEP_EMPRESA', APessoa.FCepEmpresa);
    qEntidade.SetAsString('CNPJ_EMPRESA', APessoa.FCnpjEmpresa);
    qEntidade.SetAsString('BAIRRO_EMPRESA', APessoa.FBairroEmpresa);
    qEntidade.SetAsString('DT_ADMISSAO', APessoa.FDtAdmissao);
    qEntidade.SetAsString('TELEFONE_EMPRESA', APessoa.FTelefoneEmpresa);
    qEntidade.SetAsString('RAMAL_EMPRESA', APessoa.FRamalEmpresa);
    qEntidade.SetAsFloat('RENDA_MENSAL', APessoa.FRendaMensal);
    qEntidade.SetAsString('SUFRAMA', APessoa.FSuframa);
    qEntidade.SetAsString('DOC_MUNICIPAL', APessoa.FDocMunicipal);

    if APessoa.FIdNaturalidade > 0 then
    begin
      qEntidade.SetAsInteger('ID_NATURALIDADE', APessoa.FIdNaturalidade);
    end;

    if APessoa.FIdCidadeEmpresa > 0 then
    begin
      qEntidade.SetAsInteger('ID_CIDADE_EMPRESA', APessoa.FIdCidadeEmpresa);
    end;

    if APessoa.FIdUfEmissor > 0 then
    begin
      qEntidade.SetAsInteger('ID_UF_EMISSOR', APessoa.FIdUfEmissor);
    end;

    if APessoa.FIdAreaAtuacao > 0 then
    begin
      qEntidade.SetAsInteger('ID_AREA_ATUACAO', APessoa.FIdAreaAtuacao);
    end;

    if APessoa.FIdClassificacaoEmpresa > 0 then
    begin
      qEntidade.SetAsInteger('ID_CLASSIFICACAO_EMPRESA', APessoa.FIdClassificacaoEmpresa);
    end;

    if APessoa.FIdAtividadePrincipal > 0 then
    begin
      qEntidade.SetAsInteger('ID_ATIVIDADE_PRINCIPAL', APessoa.FIdAtividadePrincipal);
    end;
    qEntidade.SetAsString('ESTADO_CIVIL', APessoa.FEstadoCivil);
    qEntidade.SetAsString('TEMPO_TRABALHO_EMPRESA', APessoa.FTempoTrabalhoEmpresa);
    qEntidade.SetAsInteger('QUANTIDADE_FILHOS', APessoa.FQuantidadeFilhos);
    qEntidade.SetAsString('TIPO_RESIDENCIA', APessoa.FTipoResidencia);
    qEntidade.SetAsString('RENDA_EXTRA', APessoa.FRendaExtra);
    qEntidade.SetAsFloat('VL_RENDA_EXTRA', APessoa.FVlRendaExtra);
    qEntidade.SetAsFloat('VL_LIMITE', APessoa.FVlLimite);
    qEntidade.SetAsString('OCUPACAO', APessoa.FOcupacao);

    if APessoa.FIdZoneamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_ZONEAMENTO', APessoa.FIdZoneamento);
    end;
    qEntidade.SetAsInteger('CRT', APessoa.FCrt);
    qEntidade.SetAsInteger('REGIME_TRIBUTARIO', APessoa.FRegimeTributario);
    qEntidade.SetAsString('BO_CONTRIBUINTE_ICMS', APessoa.FBoContribuinteIcms);
    qEntidade.SetAsString('BO_CONTRIBUINTE_IPI', APessoa.FBoContribuinteIpi);
    qEntidade.SetAsString('BO_CONTRIBUINTE', APessoa.FBoContribuinte);

    if APessoa.FIdRegimeEspecial > 0 then
    begin
      qEntidade.SetAsInteger('ID_REGIME_ESPECIAL', APessoa.FIdRegimeEspecial);
    end;
    qEntidade.SetAsString('TIPO_EMPRESA', APessoa.FTipoEmpresa);

    if APessoa.FIdPessoaImportacao > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_IMPORTACAO', APessoa.FIdPessoaImportacao);
    end;

    qEntidade.SetAsString('BO_CLIENTE_CONSUMIDOR_FINAL', APessoa.FBoClienteConsumidorFinal);

    qEntidade.Salvar;
    qEntidade.Persistir;

    APessoa.FID := qEntidade.GetAsInteger('ID');
    result :=     APessoa.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Pessoa.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaServ.ValidaCnpjCeiCpf(Numero: String): Boolean;
//Verifica se o numero passado no parametro � CNPJ/CPF ou CEI e valida o mesmo. Se nao for v�lido e o parametro ExibeMSGErro for true, exibe um messagebox co icone de erro.
var
  i,d,b,
  Digito : Byte;
  Soma : Integer;
  CNPJ,CPF,CEI : Boolean;
  DgPass,
  DgCalc,DocMsg, MsgDialogo : String;

  function IIf(pCond:Boolean;pTrue,pFalse:Variant): Variant;
    begin
      If pCond Then
        Result := pTrue
      else
        Result := pFalse;
    end;

  function ValidaCEI(StrCEI:String):Boolean;
  const
    PESO = '74185216374';
  var
    Numero,DV_DIG,StrSoma :String;
    soma,i,valor1,valor2,
    resto,PRIDIG          :integer;
  begin
    Result := True;

    if Length(Trim(StrCEI)) = 0 then Exit;

    Numero := Copy(StrCEI,Length(StrCEI)-12+1,12);
    DV_DIG := Copy(Numero,12,1);
    soma   := 0;

    for i:= 1 to 11 do
      soma := soma + (StrToInt(Copy(Numero,i,1)) * StrToInt(Copy(PESO,i,1)));

    StrSoma:= FormatFloat('0000',soma);
    valor1 := StrToInt(Copy(StrSoma,4,1));
    valor2 := StrToInt(Copy(StrSoma,3,1));
    resto  := (valor1+valor2) Mod 10;

    if resto <> 0 then PRIDIG := 10 - resto;

    if PRIDIG <> StrToInt(DV_DIG) then
      Result := False;
  end;

begin
  Result := False;
  Numero := (Numero);
  // Caso o n�mero n�o seja 11 (CPF) ou 14 (CNPJ), aborta

  CPF := Length(Numero)=11;
  CNPJ:= Length(Numero)=14;
  CEI := Length(Numero)=12;

  if (Length(Numero) in [11,12,14]) then
  begin
    if CPF or CNPJ then
    begin
      // Separa o n�mero do d�gito
      DgCalc := '';
      DgPass := Copy(Numero,Length(Numero)-1,2);
      Numero := Copy(Numero,1,Length(Numero)-2);
      // Calcula o digito 1 e 2
      for d := 1 to 2 do
      begin
        B := IIF(D=1,2,3); // BYTE
        SOMA := IIF(D=1,0,STRTOINTDEF(DGCALC,0)*2);
        for i := Length(Numero) downto 1 do
          begin
            Soma := Soma + (Ord(Numero[I])-Ord('0'))*b;
            Inc(b);
            if (b > 9) And CNPJ Then
              b := 2;
          end;
        Digito := 11 - Soma mod 11;
        if Digito >= 10 then
          Digito := 0;
        DgCalc := DgCalc + Chr(Digito + Ord('0'));
      end;
      Result := DgCalc = DgPass;
    end
    else
      Result := ValidaCEI(Numero);
  end;
end;

class function TPessoaServ.ValidarInscricaoEstadual(const AInscricaoEstadual, AUf: String): Boolean;
var
  acbrValidador: TACbrValidador;
begin
  acbrValidador := TACBrValidador.Create(nil);
  try
    ACBrValidador.TipoDocto := docInscEst;
    ACBrValidador.Documento := AInscricaoEstadual;
    ACBrValidador.Complemento := AUf;
    result := ACBrValidador.Validar;
  finally
    FreeAndNil(acbrValidador);
  end;
end;

class function TPessoaEnderecoServ.GetIDEstadoParaNotaFiscal(const AIdPessoa: Integer): Integer;
var
  endereco: TPessoaEnderecoProxy;
begin
  endereco := TPessoaEnderecoServ.GetPessoaEnderecoParaNotaFiscal(AIdPessoa);
  try
    result := TCEPServ.GetIdEstadoPelaCidade(endereco.FIdCidade);
  finally
    FreeAndNil(endereco);
  end;
end;

class function TPessoaEnderecoServ.GetPessoaEndereco(
    const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
const
  SQL =
  '   SELECT PESSOA_ENDERECO.*'+
  '       ,CIDADE.IBGE AS JOIN_IBGE_CIDADE'+
  '       ,CIDADE.DESCRICAO AS JOIN_DESCRICAO_CIDADE'+
  '       ,ESTADO.UF  AS JOIN_UF_ESTADO'+
  '       ,ESTADO.IBGE AS JOIN_IBGE_ESTADO'+
  '       ,ESTADO.DESCRICAO AS JOIN_DESCRICAO_ESTADO'+
  '       ,PAIS.IBGE AS JOIN_IBGE_PAIS'+
  '       ,PAIS.DESCRICAO AS JOIN_DESCRICAO_PAIS'+
  ' FROM PESSOA_ENDERECO'+
  ' INNER JOIN CIDADE ON PESSOA_ENDERECO.ID_CIDADE = CIDADE.ID'+
  ' INNER JOIN ESTADO ON CIDADE.ID_ESTADO = ESTADO.ID'+
  ' INNER JOIN PAIS ON ESTADO.ID_PAIS = PAIS.ID'+
  ' WHERE PESSOA_ENDERECO.id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaEnderecoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoaEndereco));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FLogradouro := qConsulta.GetAsString('LOGRADOURO');
  result.FNumero := qConsulta.GetAsString('NUMERO');
  result.FComplemento := qConsulta.GetAsString('COMPLEMENTO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FBairro := qConsulta.GetAsString('BAIRRO');
  result.FIdCidade := qConsulta.GetAsInteger('ID_CIDADE');
  result.FCep := qConsulta.GetAsString('CEP');

  //Dados Transientes
  result.FCodigoIGBECidade := qConsulta.GetAsInteger('JOIN_IBGE_CIDADE');
  result.FCodigoIGBEEstado := qConsulta.GetAsInteger('JOIN_IBGE_ESTADO');
  result.FCodigoIGBEPais := qConsulta.GetAsInteger('JOIN_IBGE_PAIS');
  result.FNomeCidade := qConsulta.GetAsString('JOIN_DESCRICAO_CIDADE');
  result.FUF := qConsulta.GetAsString('JOIN_UF_ESTADO');
  result.FNomeEstado := qConsulta.GetAsString('JOIN_DESCRICAO_ESTADO');
  result.FNomePais := qConsulta.GetAsString('JOIN_DESCRICAO_PAIS');
end;

class function TPessoaEnderecoServ.GetPessoaEnderecoParaNotaFiscal(
  const AIdPessoa: Integer): TPessoaEnderecoProxy;
var
  qConsultaEndereco: IFastQuery;
begin
  result := TPessoaEnderecoProxy.Create;

  qConsultaEndereco := TFastQuery.ModoDeConsulta(
    'SELECT * FROM pessoa_endereco WHERE id_pessoa = :id limit 1', TArray<Variant>.Create(AIdPessoa));

  result := TPessoaEnderecoServ.GetPessoaEndereco(qConsultaEndereco.GetAsInteger('id'));
end;

class function TPessoaEnderecoServ.GerarPessoaEndereco(
    const APessoaEndereco: TPessoaEnderecoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('PESSOA_ENDERECO');
    qEntidade.Incluir;

    qEntidade.SetAsString('TIPO', APessoaEndereco.FTipo);
    qEntidade.SetAsString('LOGRADOURO', APessoaEndereco.FLogradouro);
    qEntidade.SetAsString('NUMERO', APessoaEndereco.FNumero);
    qEntidade.SetAsString('COMPLEMENTO', APessoaEndereco.FComplemento);
    qEntidade.SetAsString('OBSERVACAO', APessoaEndereco.FObservacao);

    if APessoaEndereco.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', APessoaEndereco.FIdPessoa);
    end;
    qEntidade.SetAsString('BAIRRO', APessoaEndereco.FBairro);

    if APessoaEndereco.FIdCidade > 0 then
    begin
      qEntidade.SetAsInteger('ID_CIDADE', APessoaEndereco.FIdCidade);
    end;
    qEntidade.SetAsString('CEP', APessoaEndereco.FCep);
    qEntidade.Salvar;
    qEntidade.Persistir;

    APessoaEndereco.FID := qEntidade.GetAsInteger('ID');
    result :=     APessoaEndereco.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar PessoaEndereco.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;


class function TPessoaContatoServ.GetEmailDocumentoFiscal(const AIdPessoa: Integer): String;
const
  SQL =
    ' SELECT contato'+
    '   FROM pessoa_contato'+
    '  WHERE tipo = ''EMAIL''' +
    '    AND BO_EMAIL_ENVIO_DOCUMENTO_FISCAL = ''S'''+
    '    AND id_pessoa = :id_pessoa'+
    '  LIMIT 1';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(AIdPessoa));
  result := qConsulta.GetAsString('contato');
end;

class function TPessoaContatoServ.GetPessoaContato(
    const AIdPessoaContato: Integer): TPessoaContatoProxy;
const
  SQL = 'SELECT * FROM PESSOA_CONTATO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaContatoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoaContato));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FContato := qConsulta.GetAsString('CONTATO');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
end;

class function TPessoaContatoServ.GetPessoaContatoPrincipal(
  const AIdPessoa: Integer): TPessoaContatoPrincipalProxy;
const
  SQL =
    ' SELECT ID '+
    '       ,(SELECT contato FROM pessoa_contato where id_pessoa = pessoa.id and tipo = ''TELEFONE'' limit 1) as TELEFONE'+
    '       ,(SELECT contato FROM pessoa_contato where id_pessoa = pessoa.id and tipo = ''EMAIL'' limit 1) as EMAIL'+
    '   FROM pessoa'+
    ' WHERE id = :id';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaContatoPrincipalProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(AIdPessoa));

  result.FIdPessoa := qConsulta.GetAsInteger('ID');
  result.FTelefone := qConsulta.GetAsString('TELEFONE');
  result.FEmail := qConsulta.GetAsString('EMAIL');
end;

class function TPessoaContatoServ.GerarPessoaContato(
    const APessoaContato: TPessoaContatoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('PESSOA_CONTATO');
    qEntidade.Incluir;

    qEntidade.SetAsString('CONTATO', APessoaContato.FContato);
    qEntidade.SetAsString('TIPO', APessoaContato.FTipo);
    qEntidade.SetAsString('OBSERVACAO', APessoaContato.FObservacao);

    if APessoaContato.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', APessoaContato.FIdPessoa);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    APessoaContato.FID := qEntidade.GetAsInteger('ID');
    result :=     APessoaContato.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar PessoaContato.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
