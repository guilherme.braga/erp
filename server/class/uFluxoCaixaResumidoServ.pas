unit uFluxoCaixaResumidoServ;

interface

uses FireDAC.Comp.Client, Data.FireDACJSONReflect, SysUtils, StrUtils, Controls,
  System.Classes, uFactoryQuery, System.Variants, DateUtils, uFluxoCaixaResumidoProxy;

type TFluxoCaixaResumidoServ = class
  public
    class function GetContasCorrentes: TFDJSONDataSets;

    class function GetFluxoCaixaResumido(const AContasCorrentes: String;
      const AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;

    class function GetTotalPorContaCorrente(const AContasCorrentes,
      ADtInicial, ADtFinal, ATipoPeriodo: String): TFDJSONDataSets;

    class function GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;

    class function GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;

    class function GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;

    class function GerarFluxoCaixaResumido(
      const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;

  private
  class function GetString_AGGParticionado(const AFieldParam: String; AStringAGG: String): String;
  class procedure ReplaceMacro(var ASQL: String; const AMacro, AValue: String);
  class function GetFieldNameFromPeriodoColumn(const AMes, ATipoPeriodo: String): String;
  class function RetornarCamposDoPeriodoPersonalizado(const AListaCamposPeriodo, ATipoPeriodo: String): String;
end;

implementation

{ TPlanoContaServ }

uses uStringUtils, uDateUtils, uSMFinanceiro;

{ TFluxoCaixaResumido }

class function TFluxoCaixaResumidoServ.GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
const
  SQL = 'SELECT * FROM FLUXO_CAIXA_RESUMIDO';
var
   qConsulta: IFastQuery;
begin
  result := TFluxoCaixaResumidoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL);

  result.FOrdemContaCorrente := qConsulta.GetAsString('ORDEM_CONTA_CORRENTE');
end;

class function TFluxoCaixaResumidoServ.GerarFluxoCaixaResumido(
    const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    TFastQuery.ExecutarScript('DELETE FROM FLUXO_CAIXA_RESUMIDO');

    qEntidade := TFastQuery.ModoDeInclusao('FLUXO_CAIXA_RESUMIDO');

    qEntidade.Incluir;
    qEntidade.SetAsString('ORDEM_CONTA_CORRENTE', AFluxoCaixaResumido.FOrdemContaCorrente);
    qEntidade.Salvar;
    qEntidade.Persistir;

    //AFluxoCaixaResumido.FID := qEntidade.GetAsInteger('ID');
    //result :=     AFluxoCaixaResumido.FID;
    result := 1;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Fluxo Caixa Resumido.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TFluxoCaixaResumidoServ.GetContasCorrentes: TFDJSONDataSets;
var qContaCorrente: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qContaCorrente := TFastQuery.ModoDeConsulta(
    ' SELECT'+
    '   cc.id as id_conta_corrente,'+
    '   cc.descricao,'+
    '   cast(999 as SIGNED) AS ORDENACAO,'+
    '   cast(''S'' as char(1)) AS BO_CHECKED'+
    ' FROM'+
    '   conta_corrente cc'+
    ' WHERE bo_ativo = ''S'' AND bo_visualiza_fluxo_caixa = ''S'''+
    ' ORDER BY'+
    '   cc.id');

  TFDJSONDataSetsWriter.ListAdd(Result, (qContaCorrente as TFastQuery));
end;

class function TFluxoCaixaResumidoServ.GetFluxoCaixaResumido(
  const AContasCorrentes: String; const AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;
var
  qFluxoCaixaResumido: IFastQuery;
  SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
    '   SELECT cast(id as char(11)) as id_conta_corrente'+
    '         ,concat(cc.id,'' - '', cc.descricao) as descricao'+
    //'   cast(''S'' as char(1)) AS BO_CHECKED'+
    RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo, ATipoPeriodo)+
    '     FROM conta_corrente cc'+
    '    WHERE cc.id in ('+AContasCorrentes+')'+
    ' ORDER BY cc.id';

  qFluxoCaixaResumido := TFastQuery.ModoDeAlteracao(SQL, nil);

  TFDJSONDataSetsWriter.ListAdd(result, (qFluxoCaixaResumido as TFastQuery));
end;

class function TFluxoCaixaResumidoServ.GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;
var
  qTotal: IFastQuery;
  SQL: String;
begin
  SQL :=
    '   select sum(conta_pagar.vl_aberto) as vl_total_pagar'+
    '     from conta_pagar'+
    '    where conta_pagar.dt_vencimento BETWEEN :dt_vencimento AND :dt_vencimento';

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  result := qTotal.getAsFloat('vl_total_pagar');
end;

class function TFluxoCaixaResumidoServ.GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;
var
  qTotal: IFastQuery;
  SQL: String;
begin
  SQL :=
    '   select sum(conta_receber.vl_aberto) as vl_total_receber'+
    '     from conta_receber'+
    '    where conta_receber.dt_vencimento BETWEEN :dt_vencimento AND :dt_vencimento';

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  result := qTotal.getAsFloat('vl_total_receber');
end;

class function TFluxoCaixaResumidoServ.GetTotalPorContaCorrente(
  const AContasCorrentes, ADtInicial, ADtFinal, ATipoPeriodo: String): TFDJSONDataSets;
var
  qTotal: IFastQuery;
  SQL: String;
  whereIDPlanoConta, whereFieldIDPlanoConta: String;
begin
  result := TFDJSONDataSets.Create;
  SQL := ' select';

{  if ATipoPeriodo.Equals('MENSAL') then
  begin
    SQL := SQL+' '+'   ,concat(extract(month from ccm.dt_movimento), ''/'', extract(year from ccm.dt_movimento)) as periodo';
  end
  else if ATipoPeriodo.Equals('SEMANAL') then
  begin
    SQL := SQL+' '+QuotedStr(StringReplace(ADtInicial, '/', '', [rfReplaceAll]))+' as periodo';
  end
  else if ATipoPeriodo.Equals('DIARIO') then
  begin}
  {  SQL := SQL+' '+QuotedStr(StringReplace(ADtInicial, '/', '', [rfReplaceAll]))+' as periodo';
  {end;

 { SQL := SQL +
    '   ,id_conta_corrente'+
    '   ,vl_previsto_entrada'+
    '   ,vl_previsto_saida'+
    '   ,(vl_previsto_entrada - vl_previsto_saida) as vl_saldo_previsto'+
    '   ,vl_realizado_entrada'+
    '   ,vl_realizado_saida'+
    '  ,(vl_realizado_entrada - vl_realizado_saida) as vl_saldo_realizado'+
    '  ,((vl_previsto_entrada - vl_previsto_saida) - (vl_realizado_entrada - vl_realizado_saida)) as vl_total'+
    ' from'+
    '   (select'+
    '    conta_corrente.id as id_conta_corrente'+
    '  ,IFNULL((select sum(conta_receber.vl_aberto)'+
    '     from conta_receber'+
    '    where conta_receber.id_conta_corrente = conta_corrente.id'+
    '          and conta_receber.dt_competencia BETWEEN :dt_competencia_inicial AND :dt_competencia_final),0)'+
    '                                                                                                  as vl_previsto_entrada'+
    '  ,IFNULL((select sum(conta_pagar.vl_aberto)'+
    '     from conta_pagar'+
    '    where conta_pagar.id_conta_corrente = conta_corrente.id'+
    '      and conta_pagar.dt_competencia BETWEEN :dt_competencia_inicial AND :dt_competencia_final),0)'+
    '                                                                                                  as vl_previsto_saida'+
    '  ,IFNULL((select sum(conta_receber_quitacao.vl_total) from conta_receber_quitacao'+
    '     inner join conta_receber on conta_receber_quitacao.id_conta_receber = conta_receber.id'+
    '     where conta_receber_quitacao.id_conta_corrente = conta_corrente.id'+
    '       and conta_receber.dt_competencia BETWEEN :dt_competencia_inicial AND :dt_competencia_final),0) '+
    '                                                                                                  as vl_realizado_entrada'+
    '  ,IFNULL((select sum(conta_pagar_quitacao.vl_total) from conta_pagar_quitacao'+
    '     inner join conta_pagar on conta_pagar_quitacao.id_conta_pagar = conta_pagar.id'+
    '     where conta_pagar_quitacao.id_conta_corrente = conta_corrente.id'+
    '       and conta_pagar.dt_competencia BETWEEN :dt_competencia_inicial AND :dt_competencia_final),0) '+
    '                                                                                                  as vl_realizado_saida'+

    ' FROM conta_corrente) fluxocaixa'+
    ' where fluxocaixa.id_conta_corrente in ('+AContasCorrentes+')'+
    ' group by'+
    '   fluxocaixa.id_conta_corrente';
  }

  SQL := SQL +
    '  id as id_conta_corrente'+
    '  , (select vl_saldo_geral from conta_corrente_movimento'+
    '      where id = IFNULL((select id from conta_corrente_movimento where'+
    '            dt_movimento BETWEEN :dt_competencia_inicial AND :dt_competencia_final'+
    '        and id_conta_corrente = :id_conta_corrente'+
    '     order by dt_movimento desc, id desc, documento desc limit 1), '+
    ' (select id from conta_corrente_movimento'+
    ' where conta_corrente_movimento.id_conta_corrente = :id_conta_corrente '+
    '       and dt_movimento between 20150101 and :dt_competencia_inicial'+
    ' order by dt_movimento desc, id desc, documento desc limit 1))) as vl_total'+
    ' FROM conta_corrente'+
    ' where conta_corrente.id = :id_conta_corrente';
{   ' group by'+
    '   conta_corrente.id, periodo';}

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal)),
    AContasCorrentes));

  TFDJSONDataSetsWriter.ListAdd(result, (qTotal as TFastQuery));
end;

class procedure TFluxoCaixaResumidoServ.ReplaceMacro(var ASQL: String; const AMacro, AValue: String);
begin
   ASQL := StringReplace(UpperCase(ASQL), UpperCase(AMacro), AValue, [rfReplaceAll]);
end;

class function TFluxoCaixaResumidoServ.RetornarCamposDoPeriodoPersonalizado(
  const AListaCamposPeriodo, ATipoPeriodo: String): String;
var fieldName: String;
    i: Integer;
    lista: TStringList;
begin
  lista := TStringList.Create();
  lista.DelimitedText := AListaCamposPeriodo;
  try
    for i := 0 to Pred(lista.Count) do
    begin
      fieldName := GetFieldNameFromPeriodoColumn(lista[i], ATipoPeriodo);
      result := result+',cast(0 as Decimal(15,2)) as '+QuotedStr(fieldName);
    end;
  finally
    lista.Free;
  end;
end;

class function TFluxoCaixaResumidoServ.GetString_AGGParticionado(const AFieldParam: String;AStringAGG: String): String;
var i: Integer;
    caracterCorreto: boolean;
begin
  while not AStringAGG.IsEmpty do
  begin
    if not(result.IsEmpty) and not(AStringAGG.IsEmpty) then
      result := result + ' OR ';

    i := 200;

    if i > Length(AStringAGG) then
      i := Length(AStringAGG)
    else
    begin
      caracterCorreto := false;
      while not (caracterCorreto) do
      begin
        if (AStringAGG[i] <> '') and (AStringAGG[i] = ',') then
        begin
          dec(i);
          break
        end
        else
          inc(i);

        if i > Length(AStringAGG) then break;
      end;
    end;

    result := result + ' ' + AFieldParam + ' IN ('+Copy(AStringAGG, 1, i)+')';
    delete(AStringAGG,1,i+1);
  end;
end;

class function TFluxoCaixaResumidoServ.GetFieldNameFromPeriodoColumn(const AMes, ATipoPeriodo: String): String;
begin
  if ATipoPeriodo.Equals('MENSAL') then
  begin
    result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes)+1, 4);
  end
  else if ATipoPeriodo.Equals('SEMANAL') then
  begin
    result := AMes;
  end
  else if ATipoPeriodo.Equals('DIARIO') then
  begin
    result := AMes;
  end;
end;

end.
