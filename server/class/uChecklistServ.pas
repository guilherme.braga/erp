unit uChecklistServ;

interface

Uses Data.FireDACJSONReflect, uFactoryQuery;

type TChecklistServ = class
  class function GetTodosItensAtivosDoChecklist(const AIdChecklist: Integer): TFDJSONDataSets;
end;

implementation

{ TChecklistServ }

class function TChecklistServ.GetTodosItensAtivosDoChecklist(
  const AIdChecklist: Integer): TFDJSONDataSets;
var qChecklist: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qChecklist := TFastQuery.ModoDeConsulta(
    ' SELECT *'+
    ' FROM checklist_item'+
    ' WHERE bo_ativo = ''S'' and id_checklist = :id_checklist'+
    ' ORDER BY nr_item',
    TArray<Variant>.Create(AIdChecklist));

  TFDJSONDataSetsWriter.ListAdd(Result, (qChecklist as TFastQuery));
end;

end.
