unit uSeguranca;

interface

Uses windows, SysUtils, Winsock, classes, Registry;

type TSeguranca = class
  class function MaquinaLiberada: Boolean;
  class function NomedoComputador: String;
  class function NomedoUsuario: String;
  class function SerialHD: String;
  class function EnderecoMAC: String;
  class function SetDadosFilial(var AStrings: TStringList): String;
  class function IPLocal: String;
  class procedure CriarRegistroWindowsAutorizacao(AAutorizacao: String);
  class procedure CriarRegistroWindowsOffline(AAcessos: String);
  class function MaquinaAutorizadaOffline: Boolean;
  class function AcessosOffline: Integer;
  class function AcessoOfflineValido: Boolean;
  class procedure IncrementarAcessosOffline;
  class procedure AutorizarMaquinaOffline;
  class procedure DesautorizarMaquinaOffline;

  const ARQUIVO_REG_EDIT = 'Kt';
  const SESSAO_REG_EDIT = 'Ge';
  const REGISTRO_AUTORIZACAO_REG_EDIT = 'Aut';
  const VALOR_DESAUTORIZADO_REG_EDIT = '0';
  const VALOR_AUTORIZADO_REG_EDIT = '1313';
  const REGISTRO_ACESSO_OFFLINE = 'Off';
  const VALOR_ACESSO_OFFLINE_REG_EDIT = '1701';
  const VALOR_ACESSO_OFFLINE_MAXIMO_REG_EDIT = '1799';
  const VALOR_ACESSO_OFFLINE_INVALIDO_REG_EDIT = '0';
end;

implementation

uses uFactoryQuery, uSauronClient;

class function TSeguranca.NomeDoComputador: String;
var
  buffer: Array[0..255] of char;
  size: DWord;
begin
  size := 256;
  if GetComputerName(buffer,size) then
   Result := Buffer
  else
   Result := '';
end;

class function TSeguranca.NomeDoUsuario: String;
var
  UserName: String;
  UserNameLen: DWord;
begin
  UserNameLen := 255;
  SetLength (UserName, UserNameLen);
  if GetUserName (PChar(UserName), UserNameLen) Then
    Result := Copy (UserName,1,UserNameLen - 1)
  else
    Result := 'Desconhecido';
end;

class function TSeguranca.MaquinaAutorizadaOffline: Boolean;
var
  ArqReg: TRegIniFile;
  Reg: TRegistry;
begin
  ArqReg := TRegIniFile.Create(ARQUIVO_REG_EDIT);
  Try
    Reg := TRegistry.Create;
    try
      Reg.RootKey := HKEY_CURRENT_USER;
      Reg.OpenKey(ARQUIVO_REG_EDIT+'\'+SESSAO_REG_EDIT, false);
      if not Reg.ValueExists(REGISTRO_AUTORIZACAO_REG_EDIT)  then
      begin
        CriarRegistroWindowsAutorizacao(VALOR_DESAUTORIZADO_REG_EDIT);
        result := false;
        exit;
      end;
    finally
      FreeAndNil(Reg);
    end;

    result :=
      ArqReg.ReadString(SESSAO_REG_EDIT, REGISTRO_AUTORIZACAO_REG_EDIT, VALOR_DESAUTORIZADO_REG_EDIT) =
      VALOR_AUTORIZADO_REG_EDIT;
  Finally
    ArqReg.Free;
  end;
end;

class procedure TSeguranca.AutorizarMaquinaOffline;
begin
  if not MaquinaAutorizadaOffline then
  begin
    CriarRegistroWindowsAutorizacao(VALOR_AUTORIZADO_REG_EDIT);
  end;

  CriarRegistroWindowsOffline(VALOR_ACESSO_OFFLINE_REG_EDIT);
end;

class procedure TSeguranca.DesautorizarMaquinaOffline;
begin
  CriarRegistroWindowsAutorizacao(VALOR_DESAUTORIZADO_REG_EDIT);
  CriarRegistroWindowsOffline(VALOR_ACESSO_OFFLINE_INVALIDO_REG_EDIT);
end;

class procedure TSeguranca.CriarRegistroWindowsAutorizacao(AAutorizacao: String);
var
  ArqReg: TRegIniFile;
begin
  ArqReg := TRegIniFile.Create(ARQUIVO_REG_EDIT);
  Try
    ArqReg.WriteString(SESSAO_REG_EDIT, REGISTRO_AUTORIZACAO_REG_EDIT, AAutorizacao);
  Finally
    ArqReg.Free;
  end;
end;

class function TSeguranca.AcessoOfflineValido: Boolean;
var
  acessos: Integer;
begin
  acessos := AcessosOffline;
  result := (acessos >= StrtoInt(VALOR_ACESSO_OFFLINE_REG_EDIT)) and
    (acessos <= StrtoInt(VALOR_ACESSO_OFFLINE_MAXIMO_REG_EDIT));
end;

class function TSeguranca.AcessosOffline: Integer;
var
  ArqReg: TRegIniFile;
  Reg: TRegistry;
begin
  ArqReg := TRegIniFile.Create(ARQUIVO_REG_EDIT);
  Try
    Reg := TRegistry.Create;
    try
      Reg.RootKey := HKEY_CURRENT_USER;
      Reg.OpenKey(ARQUIVO_REG_EDIT+'\'+SESSAO_REG_EDIT, false);
      if not Reg.ValueExists(REGISTRO_ACESSO_OFFLINE)  then
      begin
        CriarRegistroWindowsOffline(VALOR_ACESSO_OFFLINE_REG_EDIT);
      end;
    finally
      FreeAndNil(Reg);
    end;

    result := StrtoIntDef(ArqReg.ReadString(
      SESSAO_REG_EDIT, REGISTRO_ACESSO_OFFLINE, VALOR_ACESSO_OFFLINE_REG_EDIT), 0);
  Finally
    ArqReg.Free;
  end;
end;

class procedure TSeguranca.CriarRegistroWindowsOffline(AAcessos: String);
var
  ArqReg: TRegIniFile;
begin
  ArqReg := TRegIniFile.Create(ARQUIVO_REG_EDIT);
  Try
    ArqReg.WriteString(SESSAO_REG_EDIT, REGISTRO_ACESSO_OFFLINE, AAcessos);
  Finally
    ArqReg.Free;
  end;
end;

class procedure TSeguranca.IncrementarAcessosOffline;
begin
  CriarRegistroWindowsOffline(InttoStr(TSeguranca.AcessosOffline+1));
end;

class function TSeguranca.EnderecoMAC: string;
var
 Lib: Cardinal;
 Func: function(GUID: PGUID): Longint; stdcall;
 GUID1, GUID2: TGUID;
begin
  Result := '';
  Lib := LoadLibrary('rpcrt4.dll');
  if Lib <> 0 then
  begin
   @Func := GetProcAddress(Lib, 'UuidCreateSequential');
   if Assigned(Func) then
   begin
     if (Func(@GUID1) = 0) and
        (Func(@GUID2) = 0) and
        (GUID1.D4[2] = GUID2.D4[2]) and
        (GUID1.D4[3] = GUID2.D4[3]) and
        (GUID1.D4[4] = GUID2.D4[4]) and
        (GUID1.D4[5] = GUID2.D4[5]) and
        (GUID1.D4[6] = GUID2.D4[6]) and
        (GUID1.D4[7] = GUID2.D4[7]) then
     begin
       Result :=
         IntToHex(GUID1.D4[2], 2) + '-' +
         IntToHex(GUID1.D4[3], 2) + '-' +
         IntToHex(GUID1.D4[4], 2) + '-' +
         IntToHex(GUID1.D4[5], 2) + '-' +
         IntToHex(GUID1.D4[6], 2) + '-' +
         IntToHex(GUID1.D4[7], 2);
     end;
   end;
  end;
end;

class function TSeguranca.SerialHD;
Var
  Serial:DWord;
  DirLen,Flags: DWord;
  DLabel : Array[0..11] of Char;
begin
  Try
    GetVolumeInformation(PChar('C:\'),dLabel,12,@Serial,DirLen,Flags,nil,0);
    Result := IntToHex(Serial,8);
  Except
    Result := '';
  end;
end;

class function TSeguranca.MaquinaLiberada: Boolean;
var
  requisicao: TStringList;
begin
  requisicao := TStringList.Create;
  try
    requisicao.Add(NomeDoComputador);
    requisicao.Add(NomedoUsuario);
    requisicao.Add(SerialHD);
    requisicao.Add(EnderecoMAC);
    requisicao.Add(IPLocal);
    TSeguranca.SetDadosFilial(requisicao);
    requisicao.Delimiter := ';';
    try
      result := DmSauronClient.SMClient.AutorizarGestaoEmpresarial(requisicao.DelimitedText);
    except
      result := true;
    end;
  finally
    FreeAndNil(requisicao);
  end;
end;

class function TSeguranca.SetDadosFilial(var AStrings: TStringList): String;
var
  filiais: IFastQuery;
  sql: String;
const
  CONCATENADOR = '''  |  '',';
  VAZIO = ''' ''';
begin
  sql :=
    '  select CONCAT('+
    ' ''Fantasia: '',IFNULL(FANTASIA, '+VAZIO+'),'+CONCATENADOR+
    ' ''Razao Social: '',IFNULL(RAZAO_SOCIAL, '+VAZIO+'),'+CONCATENADOR+
    ' ''CNPJ: '',IFNULL(CNPJ, '+VAZIO+'),'+CONCATENADOR+
    ' ''IE: '',IFNULL(IE, '+VAZIO+'),'+CONCATENADOR+
    ' ''CEP: '',IFNULL(CEP, '+VAZIO+'),'+CONCATENADOR+
    ' ''Email: '',IFNULL(EMAIL, '+VAZIO+')) as descricao_filial'+
    ' FROM FILIAL';

  filiais := TFastQuery.ModoDeConsulta(sql);

  if filiais.IsEmpty then
  begin
    AStrings.Add('');
    exit;
  end;

  filiais.First;
  while not filiais.Eof do
  begin
    AStrings.Add(filiais.GetAsString('descricao_filial'));
    filiais.Next;
  end;
end;

class function TSeguranca.IPLocal : string;
type
  TaPInAddr = array [0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe  : PHostEnt;
  pptr : PaPInAddr;
  Buffer : array [0..63] of Ansichar;
  I    : Integer;
  GInitData      : TWSADATA;
begin
  WSAStartup($101, GInitData);
  Result := '';
  GetHostName(Buffer, SizeOf(Buffer));
  phe :=GetHostByName(buffer);

  if phe = nil then
  begin
    Exit;
  end;

  pptr := PaPInAddr(Phe^.h_addr_list);
  I := 0;

  while pptr^[I] <> nil do
  begin
    result:=StrPas(inet_ntoa(pptr^[I]^));
    Inc(I);
  end;
  WSACleanup;
end;

end.
