unit uEmissaoEtiquetaServ;

interface

type TEmissaoEtiquetaServ = class
  class procedure InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
  class procedure LimparRegistrosDaEmissaoEtiqueta;
end;

implementation

{ TEmissaoEtiquetaServ }

uses uFactoryQuery;

class procedure TEmissaoEtiquetaServ.InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
begin
  TFastQuery.ExecutarScript('INSERT INTO emissao_etiqueta_produto (id_produto) VALUES (:id_produto)',
    TArray<Variant>.Create(AIdProduto));
end;

class procedure TEmissaoEtiquetaServ.LimparRegistrosDaEmissaoEtiqueta;
begin
  TFastQuery.ExecutarScript('DELETE FROM emissao_etiqueta_produto');
end;

end.
