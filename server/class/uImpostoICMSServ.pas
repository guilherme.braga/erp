unit uImpostoIcmsServ;

Interface

Uses uImpostoIcmsProxy;

type TImpostoIcmsServ = class
  class function GetImpostoIcms(const AIdImpostoIcms: Integer): TImpostoIcmsProxy;
  class function PossuiMVAAjustadoST(const AIdImpostoICMS: Integer): Boolean;
end;

implementation

{ TImpostoIcmsServ }

uses uFactoryQuery, uMathUtils, Rest.JSON;

class function TImpostoIcmsServ.GetImpostoIcms(
    const AIdImpostoIcms: Integer): TImpostoIcmsProxy;
const
  SQL = 'SELECT * FROM imposto_icms WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TImpostoIcmsProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdImpostoIcms));

  result.FId := qConsulta.GetAsString('ID');
  result.FBoIncluirFreteNaBaseIpi := qConsulta.GetAsString('BO_INCLUIR_FRETE_NA_BASE_IPI');
  result.FBoIncluirFreteNaBaseIcms := qConsulta.GetAsString('BO_INCLUIR_FRETE_NA_BASE_ICMS');
  result.FBoIncluirIpiNaBaseIcms := qConsulta.GetAsString('BO_INCLUIR_IPI_NA_BASE_ICMS');
  result.FPercReducaoIcmsSt := qConsulta.GetAsFloat('PERC_REDUCAO_ICMS_ST');
  result.FModalidadeBaseIcms := qConsulta.GetAsInteger('MODALIDADE_BASE_ICMS');
  result.FModalidadeBaseIcmsSt := qConsulta.GetAsInteger('MODALIDADE_BASE_ICMS_ST');
  result.FPercReducaoIcms := qConsulta.GetAsFloat('PERC_REDUCAO_ICMS');
  result.FPercAliquotaIcms := qConsulta.GetAsFloat('PERC_ALIQUOTA_ICMS');
  result.FPercAliquotaIcmsSt := qConsulta.GetAsFloat('PERC_ALIQUOTA_ICMS_ST');
  result.FPercMvaAjustadoSt := qConsulta.GetAsFloat('PERC_MVA_AJUSTADO_ST');
  result.FPercMvaProprio := qConsulta.GetAsFloat('PERC_MVA_PROPRIO');
  result.FMotivoDesoneracaoIcms := qConsulta.GetAsInteger('MOTIVO_DESONERACAO_ICMS');
  result.FPercBaseIcmsProprio := qConsulta.GetAsFloat('PERC_BASE_ICMS_PROPRIO');
  result.FPercAliquotaIcmsProprioParaSt := qConsulta.GetAsFloat('PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST');
  result.FBoDestacarValorIcms := qConsulta.GetAsString('BO_DESTACAR_VALOR_ICMS');
  result.FIdCstCsosn := qConsulta.GetAsInteger('ID_CST_CSOSN');
end;

class function TImpostoIcmsServ.PossuiMVAAjustadoST(const AIdImpostoICMS: Integer): Boolean;
const
  SQL = 'SELECT perc_mva_ajustado_st FROM imposto_icms WHERE id = :id';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdImpostoIcms));

  result := qConsulta.GetAsFloat('perc_mva_ajustado_st') > 0;
end;

end.


