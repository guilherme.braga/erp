unit uContaReceberServ;

interface

Uses FireDAC.Comp.Client, uGBFDQuery, System.Generics.Collections, SysUtils, DateUtils, uFactoryQuery,
  Data.FireDACJSONReflect;

type TContaReceberServ = class
  class function GetQueryContaReceber(AIdContaReceber: Integer): TFastQuery;
  class function GetQueryVariosContasReceber(AIdsContasReceber: String): TFastQuery;
  class function GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: String): TFDJSONDatasets;
  class function GetQueryQuitacoesContaReceber(AIdContaReceber: Integer): TFastQuery;
  class function GetQueryContaReceberQuitacao(AIdContaReceberQuitacao: Integer): TFastQuery;
  class function PodeAlterar(AChaveProcesso: Integer): Boolean;
  class function PodeCancelar(AChaveProcesso: Integer): Boolean;
  class function PodeReabrir(AChaveProcesso: Integer): Boolean;
  class function GerarContaReceber(AContaReceber: String): Integer;
  class function PodeExcluir(AChaveProcesso: Integer): Boolean;
  class function RemoverContaReceber(AChaveProcesso : Integer): Boolean;
  class function ExisteContaReceber(AIdChaveProcesso: Integer): Boolean;
  class function PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Integer;
  class function ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Integer;
  class function ExisteQuitacao(AIdChaveProcesso : Integer): Boolean;
  class function TotalQuitacaoRealizada(AIdChaveProcesso : Integer): Double;
  class function TotalQuitacaoRealizadaPorCartao(AIdChaveProcesso: Integer): Double;
  class function AtualizarMovimentosContaCorrente(AIdContaReceber, AIdChaveProcesso: Integer;
    AGerarContraPartida: Boolean): Boolean;
  class function GerarMovimentacaoTroco(AIdQuitacao: Integer): Boolean;
  class function RemoverMovimentosContaCorrente(
      AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean; static;
  class function GerarContaReceberQuitacao(
      AContaReceberQuitacao: String): Integer;
  class function AtualizarValores(AIdContaReceber: Integer): Boolean; static;
  class function RemoverContaReceberQuitacaoPorID(AId: Integer): string;
  class function GetIdContaReceberPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
  class function GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber: Integer): TList<Integer>;
  class function GetValorMulta(const AIdContaReceber: Integer): Double;
  class function GetValorJuros(const AIdContaReceber: Integer): Double;
  class function GetValorAberto(const AIdContaReceber: Integer): Double;
  class function GetTotaisContaReceber(const AIdsContaReceber: String): TFDJSONDatasets;

  {Realiza l�gica de negocia��o no titulo do conta a receber}
  class procedure RealizarNegociacao(const AIdContaReceber: Integer);

  {Remove l�gica de negocia��o no titulo do conta a receber}
  class procedure RemoverNegociacao(const AIdContaReceber: Integer);

  class procedure AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
    const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);

  class function BuscarMaiorNossoNumero(const AIdCarteira: Integer): Integer;

  class procedure CancelarBoletoEmitido(const AIdsContaReceber: String);
end;

implementation

uses
  uDatasetUtilsServ,
  uContaReceberProxy,
  uContaCorrenteProxy,
  uContaCorrenteServ,
  REST.Json, uFormaPagamentoProxy, uTipoQuitacaoProxy, uFormaPagamentoServ,
  uTipoQuitacaoServ, uCarteiraProxy, uCarteiraServ, uMathUtils;

{ TContaReceberServ }

class function TContaReceberServ.PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Integer;
var qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT count(*) as qtde'+
    '   from conta_receber'+
    '  where id_pessoa = :id_pessoa and status = :status',
    TArray<Variant>.Create(AIdPessoa, TContaReceberMovimento.ABERTO));

  result := qContaReceber.GetAsInteger('qtde');
end;

class function TContaReceberServ.PodeAlterar(AChaveProcesso: Integer): Boolean;
begin

end;

class function TContaReceberServ.PodeCancelar(
  AChaveProcesso: Integer): Boolean;
begin

end;

class function TContaReceberServ.PodeReabrir(AChaveProcesso: Integer): Boolean;
begin

end;

class function TContaReceberServ.ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Integer;
var qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT count(*) as qtde'+
    '   from conta_receber'+
    '  where id_chave_processo = :id_chave_processo and status = :status',
    TArray<Variant>.Create(AIdChaveProcesso, TContaReceberMovimento.ABERTO));

  result := qContaReceber.GetAsInteger('qtde');
end;

class function TContaReceberServ.ExisteContaReceber(
  AIdChaveProcesso: Integer): Boolean;
var qContaReceber: IFastQuery;
begin
  result := true;

  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   from conta_receber'+
    '  where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := not qContaReceber.IsEmpty;
end;

class function TContaReceberServ.ExisteQuitacao(
  AIdChaveProcesso: Integer): Boolean;
var
  qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   from conta_receber'+
    '  where id_chave_processo = :id_chave_processo'+
    '    and exists(select 1'+
    '                     from conta_receber_quitacao'+
    '                    where id_conta_receber = conta_receber.id)',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := not qContaReceber.IsEmpty;
end;

class function TContaReceberServ.GerarContaReceber(AContaReceber: String): Integer;
var
  fqContaReceber : IFastQuery;
  ContaReceber   : TContaReceberMovimento;
begin
  result := 0;

  ContaReceber := TJson.JsonToObject<TContaReceberMovimento>(AContaReceber);

  fqContaReceber := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_RECEBER WHERE 1 = 2' , nil);
  fqContaReceber.Incluir;

  fqContaReceber.SetAsString('DH_CADASTRO', ContaReceber.FDhCadastro);
  fqContaReceber.SetAsString('DOCUMENTO', ContaReceber.FDocumento);
  fqContaReceber.SetAsString('DESCRICAO', ContaReceber.FDescricao);
  fqContaReceber.SetAsFloat('VL_TITULO', ContaReceber.FVlTitulo);
  fqContaReceber.SetAsFloat('VL_TITULO_ORIGINAL_HISTORICO', ContaReceber.FVlTituloOriginalHistorico);
  fqContaReceber.SetAsFloat('VL_QUITADO', ContaReceber.FVlQuitado);
  fqContaReceber.SetAsFloat('VL_ABERTO', ContaReceber.FVlAberto);
  fqContaReceber.SetAsInteger('QT_PARCELA', ContaReceber.FQtParcela);
  fqContaReceber.SetAsInteger('NR_PARCELA', ContaReceber.FNrParcela);
  fqContaReceber.SetAsString('DT_VENCIMENTO', ContaReceber.FDtVencimento);
  fqContaReceber.SetAsString('SEQUENCIA', ContaReceber.FSequencia);
  fqContaReceber.SetAsString('BO_VENCIDO', ContaReceber.FBoVencido);
  fqContaReceber.SetAsString('OBSERVACAO', ContaReceber.FObservacao);

  if ContaReceber.FIdPessoa > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_PESSOA', ContaReceber.FIdPessoa);
  end;

  if ContaReceber.FIdContaAnalise > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_CONTA_ANALISE', ContaReceber.FIdContaAnalise);
  end;

  if ContaReceber.FIdCentroResultado > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_CENTRO_RESULTADO', ContaReceber.FIdCentroResultado);
  end;
  fqContaReceber.SetAsString('STATUS', ContaReceber.FStatus);

  if ContaReceber.FIdChaveProcesso > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_CHAVE_PROCESSO', ContaReceber.FIdChaveProcesso);
  end;

  if ContaReceber.FIdFormaPagamento > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_FORMA_PAGAMENTO', ContaReceber.FIdFormaPagamento);
  end;
  fqContaReceber.SetAsFloat('VL_ACRESCIMO', ContaReceber.FVlAcrescimo);
  fqContaReceber.SetAsFloat('VL_DECRESCIMO', ContaReceber.FVlDecrescimo);
  fqContaReceber.SetAsFloat('VL_QUITADO_LIQUIDO', ContaReceber.FVlQuitadoLiquido);
  fqContaReceber.SetAsString('DT_COMPETENCIA', ContaReceber.FDtCompetencia);

  if ContaReceber.FIdDocumentoOrigem > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_DOCUMENTO_ORIGEM', ContaReceber.FIdDocumentoOrigem);
  end;
  fqContaReceber.SetAsString('TP_DOCUMENTO_ORIGEM', ContaReceber.FTpDocumentoOrigem);
  fqContaReceber.SetAsString('DT_DOCUMENTO', ContaReceber.FDtDocumento);

  if ContaReceber.FIdContaCorrente > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_CONTA_CORRENTE', ContaReceber.FIdContaCorrente);
  end;

  if ContaReceber.FIdFilial > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_FILIAL', ContaReceber.FIdFilial);
  end;

  if ContaReceber.FIdCarteira > 0 then
  begin
    fqContaReceber.SetAsInteger('ID_CARTEIRA', ContaReceber.FIdCarteira);
  end;
  fqContaReceber.SetAsFloat('VL_JUROS', ContaReceber.FVlJuros);
  fqContaReceber.SetAsFloat('PERC_JUROS', ContaReceber.FPercJuros);
  fqContaReceber.SetAsFloat('VL_MULTA', ContaReceber.FVlMulta);
  fqContaReceber.SetAsFloat('PERC_MULTA', ContaReceber.FPercMulta);
  fqContaReceber.SetAsString('BO_NEGOCIADO', ContaReceber.FBoNegociado);
  fqContaReceber.SetAsString('BO_GERADO_POR_NEGOCIACAO', ContaReceber.FBoGeradoPorNegociacao);
  fqContaReceber.SetAsFloat('VL_NEGOCIADO', ContaReceber.FVlNegociado);
  fqContaReceber.SetAsFloat('VL_TITULO_ANTES_NEGOCIACAO', ContaReceber.FVlTituloAntesNegociacao);
  fqContaReceber.SetAsString('BO_BOLETO_EMITIDO', ContaReceber.FBoBoletoEmitido);
  fqContaReceber.SetAsInteger('BOLETO_NOSSO_NUMERO', ContaReceber.FBoletoNossoNumero);
  fqContaReceber.SetAsString('DH_EMISSAO_BOLETO', ContaReceber.FDhEmissaoBoleto);
  fqContaReceber.SetAsString('BOLETO_LINHA_DIGITAVEL', ContaReceber.FBoletoLinhaDigitavel);
  fqContaReceber.SetAsString('BOLETO_CODIGO_BARRAS', ContaReceber.FBoletoCodigoBarras);

  fqContaReceber.Salvar;
  fqContaReceber.Persistir;

  result := fqContaReceber.GetAsInteger('ID');
end;

class function TContaReceberServ.PodeExcluir(AChaveProcesso: Integer): Boolean;
var
  fqContaReceber : IFastQuery;
begin

  fqContaReceber := TFastQuery.ModoDeConsulta(
  '   SELECT cr.id                                          ' +
  '     FROM conta_receber cr                               ' +
  '    WHERE cr.id_chave_processo = :id_chave_processo      ' +
  '      AND NOT EXISTS(SELECT 1                            ' +
  '                       FROM conta_receber_quitacao crq   ' +
  '                      WHERE crq.id_conta_receber = cr.id)',
  TArray<Variant>.Create(AChaveProcesso)
  );

  result := fqContaReceber.IsEmpty;

end;

class procedure TContaReceberServ.RealizarNegociacao(const AIdContaReceber: Integer);
begin
//
end;

class function TContaReceberServ.RemoverContaReceber(
  AChaveProcesso: Integer): Boolean;
begin
  try
    Result := True;

    TFastQuery.ExecutarScriptIndependente(
          ' DELETE FROM conta_receber_quitacao WHERE id_conta_receber in '+
          ' (select id from conta_receber WHERE id_chave_processo = :id_chave_processo)',
          TArray<Variant>.Create(AChaveProcesso));

    TFastQuery.ExecutarScriptIndependente(
          'DELETE FROM conta_receber WHERE id_chave_processo = :id_chave_processo',
          TArray<Variant>.Create(AChaveProcesso));
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaReceberServ.RemoverContaReceberQuitacaoPorID(
  AId: Integer): string;
begin
  try
    Result := TContaReceberMovimento.RETORNO_SUCESSO;
    TFastQuery.ExecutarScriptIndependente(
          'DELETE FROM conta_receber_quitacao WHERE id = :id',
          TArray<Variant>.Create(AId));
  except
    on e : Exception do
    begin
      result := e.Message;
    end;
  end;
end;

class procedure TContaReceberServ.AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
  const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE conta_receber SET boleto_nosso_numero = :boleto_nosso_numero, '+
    ' boleto_linha_digitavel = :boleto_linha_digitavel, boleto_codigo_barras = :boleto_codigo_barras, dh_emissao_boleto = :dh_emissao_boleto'+
    ' WHERE id = :id',
    TArray<Variant>.Create(ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras, Now, AIdContaReceber));
end;

class function TContaReceberServ.AtualizarMovimentosContaCorrente(
  AIdContaReceber, AIdChaveProcesso : Integer; AGerarContraPartida: Boolean): Boolean;
var
  Movimento              : TContaCorrenteMovimento;
  fqContaReceber         : IFastQuery;
  fqAtualizacao          : IFastQuery;
  fqRemocao              : IFastQuery;
  fqInsercao             : IFastQuery;
  MovimentoJSON          : string;
  formaPagamento         : TFormaPagamentoProxy;
  tipoQuitacao           : String;
const
  CREDITO      : String = 'CREDITO';
  CONTARECEBER : string = 'CONTARECEBER';
  NAO          : string = 'N';
  SIM          : String = 'S';
begin
  Result := True;

  if AIdContaReceber <= 0 then
  begin
    fqContaReceber := TFastQuery.ModoDeConsulta(
      'SELECT * FROM conta_receber where id_chave_processo = :id_chave_processo',
      TArray<Variant>.Create(AIdChaveProcesso)
      );
  end
  else
  begin
    fqContaReceber := TFastQuery.ModoDeConsulta(
      'SELECT * FROM conta_receber where id = :id_conta_receber',
      TArray<Variant>.Create(AIdContaReceber)
      );
  end;

  fqAtualizacao := TFastQuery.ModoDeConsulta(
  'SELECT ccm.id as id                    ' +
  '      ,crq.dh_cadastro as dh_cadastro  ' +
  '      ,crq.dt_quitacao as dt_quitacao  ' +
  '      ,crq.vl_total  as vl_total       ' +
  '      ,crq.id_conta_corrente as id_conta_corrente  ' +
  '  FROM conta_corrente_movimento ccm                ' +
  ' INNER JOIN conta_receber_quitacao crq ON ccm.id_documento_origem = crq.id ' +
  '   AND ccm.tp_documento_origem = ''CONTARECEBER''  ' +
  '   AND ccm.id_chave_processo = :id_chave_processo  ' +
  '   AND crq.id_conta_receber  = :id_conta_receber   ',
  TArray<Variant>.Create(fqContaReceber.GetAsInteger('ID_CHAVE_PROCESSO')
                        ,fqContaReceber.GetAsInteger('ID'))
  );

  while not fqAtualizacao.Eof do
  begin
    Movimento := TContaCorrenteMovimento.Create;
    try

      Movimento.Fid                := fqAtualizacao.GetAsInteger('id');
      Movimento.FDhCadastro        := fqAtualizacao.GetAsString('dh_cadastro');
      Movimento.FDtEmissao         := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FDtMovimento       := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FDtCompetencia     := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FVlMovimento       := fqAtualizacao.GetAsFloat('vl_total');
      Movimento.FIdContaCorrente   := fqAtualizacao.GetAsInteger('id_conta_corrente');

      result := TContaCorrenteServ.AtualizarMovimento(Movimento);

    finally
      Movimento.Free;
    end;
    fqAtualizacao.Proximo;
  end;

  fqInsercao := TFastQuery.ModoDeConsulta(
  'SELECT crq.*                                                         ' +
  '  FROM conta_receber_quitacao crq                                    ' +
  ' WHERE crq.id_conta_receber = :id_conta_receber                      ' +
  '   AND NOT EXISTS (SELECT 1                                          ' +
  '                     FROM conta_corrente_movimento ccm               ' +
  '                    WHERE ccm.id_chave_processo = :id_chave_processo ' +
  '                      AND ccm.id_documento_origem = crq.id           ' +
  '                      AND ccm.tp_documento_origem = ''CONTARECEBER'')'
  ,TArray<Variant>.Create(fqContaReceber.GetAsInteger('ID')
                         ,fqContaReceber.GetAsInteger('ID_CHAVE_PROCESSO'))
  );

  while not fqInsercao.Eof do
  begin
    Movimento := TContaCorrenteMovimento.Create;
    try
      Movimento.FDocumento              := fqContaReceber.GetAsString('DOCUMENTO');
      Movimento.FDescricao              := fqContaReceber.GetAsString('DESCRICAO');
      Movimento.FObservacao             := fqContaReceber.GetAsString('OBSERVACAO');
      Movimento.FDhCadastro             := fqInsercao.GetAsString('DH_CADASTRO');
      Movimento.FDtEmissao              := fqInsercao.GetAsString('DT_QUITACAO');
      Movimento.FDtMovimento            := fqInsercao.GetAsString('DT_QUITACAO');
      Movimento.FDtCompetencia          := fqInsercao.GetAsString('DT_QUITACAO');

      //Se for cheque a concilia��o � falsa
      tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(fqInsercao.GetAsInteger('ID_TIPO_QUITACAO'));
      if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
        (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
      begin
        Movimento.FBoCheque := SIM;
        Movimento.FBoChequeTerceiro := SIM;//Sempre � de terceiro qdo � conta a receber

        Movimento.FBoConciliado := NAO;

        //Dados do Cheque
        Movimento.FChequeSacado        := fqInsercao.GetAsString('CHEQUE_SACADO');
        Movimento.FChequeDocFederal    := fqInsercao.GetAsString('CHEQUE_DOC_FEDERAL');
        Movimento.FChequeBanco         := fqInsercao.GetAsString('CHEQUE_BANCO');
        Movimento.FChequeDtEmissao     := fqInsercao.GetAsString('CHEQUE_DT_EMISSAO');
        Movimento.FChequeDtVencimento  := fqInsercao.GetAsString('CHEQUE_DT_VENCIMENTO');
        Movimento.FChequeAgencia       := fqInsercao.GetAsString('CHEQUE_AGENCIA');
        Movimento.FChequeContaCorrente := fqInsercao.GetAsString('CHEQUE_CONTA_CORRENTE');
        Movimento.FChequeNumero        := fqInsercao.GetAsInteger('CHEQUE_NUMERO');
      end
      else
      begin
        Movimento.FBoConciliado := NAO;
      end;

      //Quando for cheque, deve gerar a movimenta��o com o valor integral do cheque (VL_TOTAL + VL_TROCO)
      if (Movimento.FBoCheque = SIM) and (fqInsercao.GetAsFloat('VL_TROCO') > 0) then
      begin
        Movimento.FVlMovimento := fqInsercao.GetAsFloat('VL_TOTAL')+fqInsercao.GetAsFloat('VL_TROCO');
      end
      else
      begin
        Movimento.FVlMovimento := fqInsercao.GetAsFloat('VL_TOTAL');
      end;

      Movimento.FTpMovimento            := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;

      Movimento.FDhConciliado           := '';
      Movimento.FVlSaldoConciliado      := 0;
      Movimento.FVlSaldoGeral           := fqInsercao.GetAsFloat('VL_TOTAL');
      Movimento.FIdContaCorrente        := fqInsercao.GetAsInteger('ID_CONTA_CORRENTE');
      Movimento.FIdContaAnalise         := fqContaReceber.GetAsInteger('ID_CONTA_ANALISE');
      Movimento.FIdCentroResultado      := fqContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
      Movimento.FBoMovimentoOrigem      := NAO;
      Movimento.FIdMovimentoOrigem      := 0;
      Movimento.FBoMovimentoDestino     := NAO;
      Movimento.FIdMovimentoDestino     := 0;
      Movimento.FBoTransferencia        := NAO;
      Movimento.FIdContaCorrenteDestino := 0;
      Movimento.FTPDocumentoOrigem      := TContaCorrenteMovimento.ORIGEM_CONTA_RECEBER;//+ '('+fqInsercao.GetAsString('ID_CONTA_RECEBER')+')';
      Movimento.FIdDocumentoOrigem      := fqInsercao.GetAsInteger('ID');
      Movimento.FIdChaveProcesso        := fqContaReceber.GetAsInteger('ID_CHAVE_PROCESSO');
      Movimento.FIdFilial               := fqContaReceber.GetAsInteger('ID_FILIAL');
      Movimento.FParcelamento           := fqContaReceber.GetAsString('SEQUENCIA');

      //Dados do Cart�o
      Movimento.FIdOperadoraCartao   := fqInsercao.GetAsInteger('ID_OPERADORA_CARTAO');

      MovimentoJSON := TJson.ObjectToJsonString(Movimento);

      result := TContaCorrenteServ.GerarMovimentoContaCorrente(MovimentoJSON);

      //Quando for cheque, deve gerar uma movimenta��o referente ao troco
      if (Movimento.FBoCheque = SIM) and (fqInsercao.GetAsFloat('VL_TROCO') > 0) then
      begin
        GerarMovimentacaoTroco(fqInsercao.GetAsInteger('ID'));
      end;
    finally
      Movimento.Free;
    end;

    fqInsercao.Proximo;
  end;

  fqRemocao := TFastQuery.ModoDeConsulta(
  '  SELECT ccm.*                                                                ' +
  '    FROM conta_corrente_movimento ccm                                         ' +
  '   WHERE ccm.id_chave_processo   = :id_chave_processo                         ' +
  '     AND ccm.tp_documento_origem = ''CONTARECEBER'' and ccm.bo_processo_amortizado = ''N'' ' +
  '     AND NOT EXISTS ( SELECT 1                                                ' +
  '                        FROM conta_receber_quitacao crq                       ' +
  '                  INNER JOIN conta_receber cr ON crq.id_conta_receber = cr.id ' +
  '                       WHERE crq.id = ccm.id_documento_origem                 ' +
  '                         AND cr.id_chave_processo = ccm.id_chave_processo )   '
  ,TArray<Variant>.Create(fqContaReceber.GetAsInteger('ID_CHAVE_PROCESSO'))
  );

  while not fqRemocao.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqRemocao.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqRemocao.GetAsInteger('ID'));
    end;

    fqRemocao.Proximo;
  end;

end;

class function TContaReceberServ.RemoverMovimentosContaCorrente(
  AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean;
var
  fqQuitacao : IFastQuery;
begin
  fqQuitacao := TFastQuery.ModoDeConsulta(
    '     SELECT ccm.id ' +
    '       FROM conta_corrente_movimento ccm ' +
    ' INNER JOIN conta_receber_quitacao crq ON ccm.id_documento_origem = crq.id  ' +
    '      WHERE ccm.tp_documento_origem = ''CONTARECEBER''  ' +
    '        AND crq.id_conta_receber    = :id_conta_receber '
    ,TArray<Variant>.Create(AIdContaReceber));

  while not fqQuitacao.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqQuitacao.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqQuitacao.GetAsInteger('ID'));
    end;

    fqQuitacao.Proximo;
  end;

  Result := True;
end;

class procedure TContaReceberServ.RemoverNegociacao(const AIdContaReceber: Integer);
begin
//
end;

class function TContaReceberServ.TotalQuitacaoRealizada(
  AIdChaveProcesso: Integer): Double;
var
  qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT SUM(vl_quitado) AS vl_quitado'+
    ' FROM conta_receber'+
    ' WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qContaReceber.getAsFloat('vl_quitado');
end;

class function TContaReceberServ.TotalQuitacaoRealizadaPorCartao(AIdChaveProcesso: Integer): Double;
var
  qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT SUM(vl_quitado) AS vl_quitado'+
    ' FROM conta_receber'+
    ' WHERE id_chave_processo = :id_chave_processo'+
    '   AND id_forma_pagamento in ((select forma_pagamento.id from forma_pagamento'+
                              ' inner join tipo_quitacao on forma_pagamento.id_tipo_quitacao = tipo_quitacao.id'+
                                      ' and tipo_quitacao.tipo in (''DEBITO'', ''CREDITO'')))',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qContaReceber.getAsFloat('vl_quitado');
end;

class function TContaReceberServ.GerarContaReceberQuitacao(AContaReceberQuitacao: String) : Integer;
var
  fqQuitacao : IFastQuery;
  Quitacao   : TContaReceberQuitacaoMovimento;
begin

  Quitacao := TJson.JsonToObject<TContaReceberQuitacaoMovimento>(AContaReceberQuitacao);

  fqQuitacao := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_RECEBER_QUITACAO WHERE 1 = 2' , nil);

  fqQuitacao.Incluir;

  fqQuitacao.SetAsInteger('NR_ITEM', Quitacao.FNrItem);
  fqQuitacao.SetAsInteger('ID_CONTA_RECEBER', Quitacao.FIdContaReceber);

  fqQuitacao.SetAsString('DH_CADASTRO', Quitacao.FDhCadastro);
  fqQuitacao.SetAsString('DT_QUITACAO', Quitacao.FDtQuitacao);
  fqQuitacao.SetAsString('OBSERVACAO', Quitacao.FObservacao);

  fqQuitacao.SetAsInteger('ID_DOCUMENTO_ORIGEM', Quitacao.FIdDocumentoOrigem);
  fqQuitacao.SetAsString('TP_DOCUMENTO_ORIGEM', Quitacao.FTPDocumentoOrigem);
  fqQuitacao.SetAsString('STATUS', Quitacao.FStatus);

  fqQuitacao.SetAsInteger('ID_FILIAL', Quitacao.FIdFilial);
  fqQuitacao.SetAsInteger('ID_USUARIO_BAIXA', Quitacao.FIdUsuarioBaixa);
  fqQuitacao.SetAsInteger('ID_TIPO_QUITACAO', Quitacao.FIdTipoQuitacao);
  fqQuitacao.SetAsInteger('ID_CONTA_CORRENTE', Quitacao.FIdContaCorrente);
  fqQuitacao.SetAsInteger('ID_CENTRO_RESULTADO', Quitacao.FIdCentroResultado);
  fqQuitacao.SetAsInteger('ID_CONTA_ANALISE', Quitacao.FIdContaAnalise);

  fqQuitacao.SetAsFloat('VL_DESCONTO', Quitacao.FVlDesconto);
  fqQuitacao.SetAsFloat('VL_ACRESCIMO', Quitacao.FVlAcrescimo);
  fqQuitacao.SetAsFloat('VL_QUITACAO', Quitacao.FVlQuitacao);
  fqQuitacao.SetAsFloat('VL_TOTAL', Quitacao.FVlTotal);
  fqQuitacao.SetAsFloat('PERC_ACRESCIMO', Quitacao.FPercAcrescimo);
  fqQuitacao.SetAsFloat('PERC_DESCONTO', Quitacao.FPercDesconto);

  fqQuitacao.SetAsFloat('VL_MULTA', Quitacao.FVlMulta);
  fqQuitacao.SetAsFloat('VL_JUROS', Quitacao.FVlJuros);
  fqQuitacao.SetAsFloat('PERC_MULTA', Quitacao.FPercMulta);
  fqQuitacao.SetAsFloat('PERC_JUROS', Quitacao.FPercJuros);

  fqQuitacao.SetAsString('CHEQUE_SACADO',Quitacao.FChequeSacado);
  fqQuitacao.SetAsString('CHEQUE_DOC_FEDERAL',Quitacao.FChequeDocFederal);
  fqQuitacao.SetAsString('CHEQUE_BANCO',Quitacao.FChequeBanco);
  fqQuitacao.SetAsString('CHEQUE_DT_EMISSAO',Quitacao.FChequeDtEmissao);
  fqQuitacao.SetAsString('CHEQUE_DT_VENCIMENTO',Quitacao.FChequeDtVencimento);
  fqQuitacao.SetAsString('CHEQUE_AGENCIA',Quitacao.FChequeAgencia);
  fqQuitacao.SetAsString('CHEQUE_CONTA_CORRENTE',Quitacao.FChequeContaCorrente);
  fqQuitacao.SetAsInteger('CHEQUE_NUMERO',Quitacao.FChequeNumero);

  if Quitacao.FIdOperadoraCartao > 0 then
  begin
    fqQuitacao.SetAsInteger('ID_OPERADORA_CARTAO', Quitacao.FIdOperadoraCartao);
  end;

  fqQuitacao.Salvar;
  fqQuitacao.Persistir;

  Result := fqQuitacao.getAsInteger('ID');
end;

class function TContaReceberServ.GerarMovimentacaoTroco(AIdQuitacao: Integer): Boolean;
var
  qContaReceber: IFastQuery;
  qContaReceberQuitacao: IFastQuery;
  Movimento: TContaCorrenteMovimento;
begin
  qContaReceberQuitacao := TContaReceberServ.GetQueryContaReceberQuitacao(AIdQuitacao);
  qContaReceber := TContaReceberServ.GetQueryContaReceber(
    qContaReceberQuitacao.GetAsInteger('ID_CONTA_RECEBER'));

  Movimento := TContaCorrenteMovimento.Create;
  try
    Movimento.FDocumento              := qContaReceber.GetAsString('DOCUMENTO');
    Movimento.FDescricao              := qContaReceber.GetAsString('DESCRICAO');

    Movimento.FObservacao             := 'TROCO REFERENTE A QUITA��O DO CONTA A RECEBER N� '+
      qContaReceberQuitacao.GetAsString('ID_CONTA_RECEBER')+' QUITACAO N� '+InttoStr(AIdQuitacao);

    Movimento.FDhCadastro             := qContaReceberQuitacao.GetAsString('DH_CADASTRO');
    Movimento.FDtEmissao              := qContaReceberQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtMovimento            := qContaReceberQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtCompetencia          := qContaReceberQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FVlMovimento            := qContaReceberQuitacao.GetAsFloat('VL_TROCO');
    Movimento.FTpMovimento            := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;
    Movimento.FIdContaCorrente        := qContaReceberQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Movimento.FIdContaAnalise         := qContaReceber.GetAsInteger('ID_CONTA_ANALISE');
    Movimento.FIdCentroResultado      := qContaReceber.GetAsInteger('ID_CENTRO_RESULTADO');
    Movimento.FTPDocumentoOrigem      := TContaCorrenteMovimento.ORIGEM_CONTA_RECEBER;
    Movimento.FIdDocumentoOrigem      := qContaReceberQuitacao.GetAsInteger('ID');
    Movimento.FIdChaveProcesso        := qContaReceber.GetAsInteger('ID_CHAVE_PROCESSO');
    Movimento.FIdFilial               := qContaReceber.GetAsInteger('ID_FILIAL');
    Movimento.FParcelamento           := qContaReceber.GetAsString('SEQUENCIA');

    result := TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
  finally
    Movimento.Free;
  end;
end;

class function TContaReceberServ.GetIdContaReceberPeloIdDaQuitacao(
  AIdQuitacao: Integer): Integer;
var qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    'SELECT id_conta_receber FROM conta_receber_quitacao WHERE id = :id_quitacao',
    TArray<Variant>.Create(AIdQuitacao));
  result := qContaReceber.GetAsInteger('id_conta_receber');
end;

class function TContaReceberServ.GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber: Integer): TList<Integer>;
var
  qConsulta: IFastQuery;
begin
  result := TList<Integer>.Create;

  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT id FROM conta_receber_quitacao WHERE id > :id and id_conta_receber = :id_conta_receber',
    TArray<Variant>.Create(AIdUltimaQuitacao, AIdContaReceber));

  qConsulta.Primeiro;
  while not qConsulta.Eof do
  begin
    result.Add(qConsulta.GetAsInteger('ID'));
    qConsulta.Proximo;
  end;
end;

class function TContaReceberServ.GetQueryContaReceber(AIdContaReceber: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM conta_receber WHERE id = :id',
    TArray<Variant>.Create(AIdContaReceber));
end;

class function TContaReceberServ.GetQueryContaReceberQuitacao(AIdContaReceberQuitacao: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM conta_receber_quitacao WHERE id = :id',
    TArray<Variant>.Create(AIdContaReceberQuitacao));
end;

class function TContaReceberServ.GetQueryQuitacoesContaReceber(AIdContaReceber: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM conta_receber_quitacao WHERE id_conta_receber = :id',
    TArray<Variant>.Create(AIdContaReceber));
end;

class function TContaReceberServ.GetQueryVariosContasReceber(AIdsContasReceber: String): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM CONTA_RECEBER WHERE ID IN ('+AIdsContasReceber+')');
end;

class function TContaReceberServ.GetQueryVariosContasReceberJSONDatasets(
  AIdsContasReceber: String): TFDJSONDatasets;
begin
  result := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Result, (TContaReceberServ.GetQueryVariosContasReceber(AIdsContasReceber)));
end;

class function TContaReceberServ.GetTotaisContaReceber(const AIdsContaReceber: String): TFDJSONDatasets;
var
  qConsulta: IFastQuery;
  SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select vl_aberto'+
  '       ,vl_acrescimo_aberto'+
  '       ,(vl_aberto + vl_acrescimo_aberto) as vl_aberto_liquido'+
  '       ,vl_aberto_vencido'+
  '       ,vl_aberto_vencer'+
  '       ,vl_recebido'+
  '       ,vl_acrescimo_recebido'+
  '       ,vl_recebido_liquido FROM'+
  ' (select (SELECT SUM(conta_receber.vl_aberto) FROM conta_receber WHERE id IN ('+AIdsContaReceber+') ) AS vl_aberto'+
  '       ,(SELECT SUM(if(DATEDIFF(curdate(),conta_receber.dt_vencimento) > 0 and conta_receber.vl_aberto > 0,'+
  '          round(((conta_receber.vl_titulo*(DATEDIFF(curdate(),conta_receber.dt_vencimento) *'+
  '          (carteira.perc_juros/100)))+(conta_receber.vl_titulo*(carteira.perc_multa/100)))-'+
  '          conta_receber.vl_acrescimo,2),0)) FROM conta_receber inner join carteira on conta_receber.id_carteira = carteira.id'+
  '          WHERE conta_receber.id IN ('+AIdsContaReceber+')) AS vl_acrescimo_aberto'+
  '       ,(SELECT SUM(conta_receber.vl_aberto) FROM conta_receber WHERE dt_vencimento < CURDATE() AND id IN ('+AIdsContaReceber+') ) AS vl_aberto_vencido'+
  '       ,(SELECT SUM(conta_receber.vl_aberto) FROM conta_receber WHERE dt_vencimento >= CURDATE() AND id IN ('+AIdsContaReceber+') ) AS vl_aberto_vencer'+
  '       ,(SELECT SUM(conta_receber.vl_quitado) FROM conta_receber WHERE id IN ('+AIdsContaReceber+')) AS vl_recebido'+
  '       ,(SELECT SUM(conta_receber.vl_acrescimo) FROM conta_receber WHERE id IN ('+AIdsContaReceber+')) AS vl_acrescimo_recebido'+
  '       ,(SELECT SUM(conta_receber.vl_quitado_liquido) FROM conta_receber WHERE id IN ('+AIdsContaReceber+')) AS vl_recebido_liquido'+
  ' from'+
  '   filial limit 1) totais_titulo';

  qConsulta := TFastQuery.ModoDeConsulta(SQL);
  TFDJSONDataSetsWriter.ListAdd(Result, (qConsulta as TFastQuery));
end;

class function TContaReceberServ.AtualizarValores(AIdContaReceber : Integer) : Boolean;
const
  SQLConsulta = 'SELECT sum(crq.vl_quitacao)  AS VL_QUITACAO     ' +
                '      ,sum(crq.vl_total)     AS VL_TOTAL        ' +
                '      ,sum(crq.vl_acrescimo) AS VL_ACRESCIMO    ' +
                '      ,sum(crq.vl_multa) AS VL_MULTA    ' +
                '      ,sum(crq.vl_juros) AS VL_JUROS    ' +
                '      ,sum(crq.perc_acrescimo) AS PERC_ACRESCIMO    ' +
                '      ,sum(crq.perc_multa) AS PERC_MULTA    ' +
                '      ,sum(crq.perc_juros) AS PERC_JUROS    ' +
                '      ,sum(crq.vl_desconto)  AS VL_DESCONTO     ' +
                '      ,sum(crq.perc_desconto)  AS PERC_DESCONTO     ' +
                '  FROM conta_receber_quitacao crq               ' +
                ' WHERE crq.id_conta_receber = :id_conta_receber ';
var
  fqReceber   : TFastQuery;
  fqQuitacao  : TFastQuery;
  VlTitulo    : Double;
  status      : string;
  VlQuitacao  : Double;
  VlAcrescimo : Double;
  VlMulta : Double;
  VlJuros : Double;
  PercAcrescimo : Double;
  PercMulta : Double;
  PercJuros : Double;
  PercDesconto : Double;
  VlDesconto  : Double;
  VlTotal     : Double;
begin

  fqQuitacao := TFastQuery.ModoDeConsulta(SQLConsulta
                                         ,TArray<Variant>.Create(AIdContaReceber));

  fqReceber := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_RECEBER WHERE ID = :ID'
                                         ,TArray<Variant>.Create(AIdContaReceber));

  VlQuitacao  := fqQuitacao.GetAsFloat('VL_QUITACAO');
  VlAcrescimo := fqQuitacao.GetAsFloat('VL_ACRESCIMO');
  VlDesconto  := fqQuitacao.GetAsFloat('VL_DESCONTO');
  VlMulta := fqQuitacao.GetAsFloat('VL_MULTA');
  VlJuros := fqQuitacao.GetAsFloat('VL_JUROS');
  PercMulta := fqQuitacao.GetAsFloat('PERC_MULTA');
  PercJuros := fqQuitacao.GetAsFloat('PERC_JUROS');
  VlTotal     := fqQuitacao.GetAsFloat('VL_TOTAL');
  VlTitulo    := fqReceber.GetAsFloat('VL_TITULO');

  if VlQuitacao >= VlTitulo then
  begin
    status := TContaReceberMovimento.QUITADO;
  end
  else
  begin
    status := TContaReceberMovimento.ABERTO;
  end;

  fqReceber.Alterar;
  fqReceber.SetAsString('STATUS', status);
  fqReceber.SetAsFloat('VL_QUITADO', VlQuitacao);
  fqReceber.SetAsFloat('VL_ABERTO', (VlTitulo - VlTotal));
  fqReceber.SetAsFloat('VL_ACRESCIMO', VlAcrescimo);
  fqReceber.SetAsFloat('VL_DECRESCIMO', VlDesconto);
  fqReceber.SetAsFloat('VL_MULTA', VlMulta);
  fqReceber.SetAsFloat('VL_JUROS', VlJuros);
  fqReceber.SetAsFloat('PERC_MULTA', PercMulta);
  fqReceber.SetAsFloat('PERC_JUROS', PercJuros);
  fqReceber.SetAsFloat('VL_QUITADO_LIQUIDO', VlTotal + VlMulta + VlJuros - VlDesconto);
  fqReceber.Salvar;
  fqReceber.Persistir;
end;

class function TContaReceberServ.BuscarMaiorNossoNumero(const AIdCarteira: Integer): Integer;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    ' SELECT MAX(boleto_nosso_numero) AS boleto_nosso_numero FROM'+
    ' conta_receber where id_carteira = :id_carteira',
    TArray<Variant>.Create(AIdCarteira));

  result := qConsulta.GetAsInteger('boleto_nosso_numero');
end;

class procedure TContaReceberServ.CancelarBoletoEmitido(const AIdsContaReceber: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    ' UPDATE conta_receber SET boleto_nosso_numero = 0, '+
    ' boleto_linha_digitavel = null, boleto_codigo_barras = null, dh_emissao_boleto = null'+
    ' WHERE id in ('+AIdsContaReceber+')');
end;

class function TContaReceberServ.GetValorAberto(const AIdContaReceber: Integer): Double;
var
  fqConsulta : TFastQuery;
begin
  fqConsulta := TFastQuery.ModoDeConsulta('SELECT vl_aberto FROM conta_receber WHERE id = :id'
                                           ,TArray<Variant>.Create(AIdContaReceber));

  result := fqConsulta.GetAsFloat('vl_aberto');
end;

class function TContaReceberServ.GetValorJuros(const AIdContaReceber: Integer): Double;
var
  carteira: TCarteiraProxy;
  estaVencido: Boolean;
  estaAberto: Boolean;
  diasEmAtraso: Integer;
  AContaReceber: IFastQuery;
  percJuros: Double;
begin
  result := 0;
  percJuros := 0;

  AContaReceber := TFastQuery.ModoDeConsulta('SELECT * FROM CONTA_RECEBER WHERE ID = :ID',
    TArray<Variant>.Create(AIdContaReceber));

  estaVencido := (AContaReceber.GetAsDateTime('DT_VENCIMENTO') < Date);
  estaAberto := AContaReceber.GetAsString('STATUS').Equals(
    TContaReceberMovimento.ABERTO);

  if not(estaVencido) or not(estaAberto) then
  begin
    Exit;
  end;

  carteira := TCarteiraServ.GetCarteira(
    AContaReceber.GetAsInteger('ID_CARTEIRA'));
  try
    estaVencido :=(AContaReceber.GetAsDateTime('DT_VENCIMENTO') + carteira.FCarencia) < (Date);

    diasEmAtraso := DaysBetween(AContaReceber.GetAsDateTime('DT_VENCIMENTO') + carteira.FCarencia, Date);

    if not estaVencido  then
    begin
      Exit;
    end;

    if AContaReceber.GetAsFloat('PERC_JUROS') < (carteira.FPercJuros * diasEmAtraso) then
    begin
      percJuros :=
        (carteira.FPercJuros * diasEmAtraso) - AContaReceber.GetAsFloat('PERC_JUROS');
    end;

    if percJuros > 0 then
    begin
      result := TMathUtils.ValorSobrePercentual(percJuros,
        AContaReceber.GetAsFloat('VL_ABERTO'));
    end;
  finally
    FreeAndNil(carteira);
  end;
end;

class function TContaReceberServ.GetValorMulta(const AIdContaReceber: Integer): Double;
var
  carteira: TCarteiraProxy;
  estaVencido: Boolean;
  estaAberto: Boolean;
  diasEmAtraso: Integer;
  AContaReceber: IFastQuery;
  percMulta: Double;
begin
  result := 0;
  percMulta := 0;

  AContaReceber := TFastQuery.ModoDeConsulta('SELECT * FROM CONTA_RECEBER WHERE ID = :ID',
    TArray<Variant>.Create(AIdContaReceber));

  estaVencido := (AContaReceber.GetAsDateTime('DT_VENCIMENTO') < Date);
  estaAberto := AContaReceber.GetAsString('STATUS').Equals(
    TContaReceberMovimento.ABERTO);

  if not(estaVencido) or not(estaAberto) then
  begin
    Exit;
  end;

  carteira := TCarteiraServ.GetCarteira(
    AContaReceber.GetAsInteger('ID_CARTEIRA'));
  try
    estaVencido :=(AContaReceber.GetAsDateTime('DT_VENCIMENTO')
      + carteira.FCarencia) < (Date);

    diasEmAtraso := DaysBetween(AContaReceber.GetAsDateTime('DT_VENCIMENTO')
      + carteira.FCarencia, Date);

    if not estaVencido  then
    begin
      Exit;
    end;

    if AContaReceber.GetAsFloat('PERC_MULTA') < carteira.FPercMulta then
    begin
      percMulta :=
        carteira.FPercMulta - AContaReceber.GetAsFloat('PERC_MULTA');
    end;

    if percMulta > 0 then
    begin
      result :=
        TMathUtils.ValorSobrePercentual(
        percMulta,
        AContaReceber.GetAsFloat('VL_ABERTO'));
    end;
  finally
    FreeAndNil(carteira);
  end;
end;

end.
