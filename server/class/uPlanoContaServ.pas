unit uPlanoContaServ;

interface

uses FireDAC.Comp.Client, Data.FireDACJSONReflect, SysUtils, StrUtils, Controls,
  System.Classes, uFactoryQuery, System.Variants;

type TPlanoContaServ = class
  class function GetIDNivel(const ANivel: Integer; ASequencia: String): Integer;
  class procedure SetPlanoContaCompleto(AQuery: TFDQuery);
  class procedure SetPlanoContaPorCentroResultado(AQuery: TFDQuery; AIdCentroResultado: Integer);
  class procedure SetIDPlanoContaAssociado;
  class function GetDescricaoContaAnalise(const AID: Integer): String;
  class function GetDescricaoCentroResultado(const AID: Integer): String;
end;

type TExtratoPlanoContaServ = class
  public
  class function GetCentroResultados: TFDJSONDataSets;
  class function GetPrimeiroNivel(const ANivel: Integer; const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
  class function GetNivelAninhado(const ANivel: Integer; const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
  class function GetMovimentacao(const ACentrosResultados, APlanosContas, AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal: String): TFDJSONDataSets;
  class function GetContasCorrentes(const ACentrosResultados, APlanosContas,
    AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
  class function GetTotalPorCentroResultado(const ACentrosResultados, ADtInicial, ADtFinal: String): TFDJSONDataSets;
  class function GetTotalPorNivel(const ANivel: Integer; const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
  class function GetTotalPorContaCorrente(const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
  class procedure SalvarPrevisao(AIdCentroResultado: Integer; AIdContaAnalise: Integer; APeriodo: String; AValor: Double);
  class function BuscarValorPrevisao(const ANivel: Integer;
    const APeriodoPrevisaoInicial, APeriodoPrevisaoFinal: String;
    const AIdCentroResultado: Integer; const AIdContaAnalise: Integer): Double;

  private
  class function GetString_AGGParticionado(const AFieldParam: String; AStringAGG: String): String;
  class procedure ReplaceMacro(var ASQL: String; const AMacro, AValue: String);
  class function GetFieldNameFromPeriodoColumn(const AMes: String): String;
  class function RetornarCamposDoPeriodoPersonalizado(const AListaCamposPeriodo: String): String;
end;

implementation

{ TPlanoContaServ }

uses uStringUtils, uDateUtils, uSMFinanceiro;

class function TPlanoContaServ.GetDescricaoContaAnalise(const AID: Integer): String;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT descricao FROM conta_analise WHERE id = :id',
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsString('descricao');
end;

class function TPlanoContaServ.GetDescricaoCentroResultado(const AID: Integer): String;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT descricao FROM centro_resultado WHERE id = :id',
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsString('descricao');
end;

class function TPlanoContaServ.GetIDNivel(const ANivel: Integer; ASequencia: String): Integer;
var
  countPontos: Integer;
const
  CARACTER_PONTO: Char = '.';

  function GetIDPeloNivelESequencia: Integer;
  var query: IFastQuery;
  begin
    query := TFastQuery.ModoDeConsulta(
      ' select'+
      '   id'+
      ' from'+
      '   conta_analise'+
      ' where nivel = :nivel'+
      '   and sequencia = :sequencia',
    TARRAY<Variant>.Create(ANivel, ASequencia));

    result := query.GetAsInteger('id');
  end;

begin
  result := GetIDPeloNivelESequencia;

  if result > 0 then exit;

  countPontos := TStringUtils.OcorrenciaDeTexto(ASequencia,CARACTER_PONTO);

  if ((ANivel = 1) and (countPontos >= 0)) then
  begin
    ASequencia := Copy(ASequencia, 1, Pred(Pos(CARACTER_PONTO, ASequencia)));
  end
  else if (countPontos >= 0) then
  begin
    while (Pred(ANivel) < TStringUtils.OcorrenciaDeTexto(ASequencia,CARACTER_PONTO)) do
      ASequencia := Copy(ASequencia, 1, Length(ASequencia)-1);
  end;

  result := GetIDPeloNivelESequencia;
end;

class procedure TPlanoContaServ.SetIDPlanoContaAssociado;
var qPlanoConta: IFastQuery;
  procedure SetIDAssociado;
  begin
    qPlanoConta.SetAsInteger('ID_PLANO_CONTA_ASSOCIADO',
      TPlanoContaServ.GetIDNivel(TStringUtils.Iif(qPlanoConta.GetAsInteger('NIVEL') = 1, 1,
      qPlanoConta.GetAsInteger('NIVEL') - 1), qPlanoConta.GetAsString('SEQUENCIA')));
  end;
begin
  qPlanoConta := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_ANALISE WHERE BO_ATIVO = ''S'' and NIVEL > 0');
  while not qPlanoConta.Eof do
  begin
    qPlanoConta.Alterar;
    SetIDAssociado;
    qPlanoConta.Salvar;

    qPlanoConta.Proximo;
  end;

  qPlanoConta.Persistir;
end;

class procedure TPlanoContaServ.SetPlanoContaCompleto(AQuery: TFDQuery);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    ' select'+
    '   id, sequencia, descricao, nivel, id as id_associacao, id AS bo_habilitado'+//esse id_associacao ser� alterado no client
    ' from'+
    '   conta_analise cc'+
    ' where bo_ativo = ''S''');
  AQuery.Open;
end;

class procedure TPlanoContaServ.SetPlanoContaPorCentroResultado(
  AQuery: TFDQuery; AIdCentroResultado: integer);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    ' select'+
    '   cc.id, cc.sequencia, cc.descricao, cc.nivel, cc.id as id_associacao, cc.id AS bo_habilitado'+//esse id_associacao ser� alterado no client
    ' from'+
    '   plano_conta_item pci'+
    ' inner join plano_conta pc on pci.id_plano_conta = pc.id'+
    ' inner join conta_analise cc on pci.id_conta_analise = cc.id'+    
    ' where pc.id_centro_resultado = :id_centro_resultado and cc.bo_ativo = ''S''');
  AQuery.Params[0].AsInteger := AIdCentroResultado;
  AQuery.Open;

end;

{ TExtratoPlanoConta }

class function TExtratoPlanoContaServ.BuscarValorPrevisao(const ANivel: Integer; const
  APeriodoPrevisaoInicial, APeriodoPrevisaoFinal: String;
  const AIdCentroResultado: Integer; const AIdContaAnalise: Integer): Double;
var
  qConsulta: IFastQuery;
  whereSQL: String;
begin
  whereSQL := ' and case ';
  if ANivel = 1 then
  begin
    whereSQL := whereSQL+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 1 then'+
    '        id_conta_analise = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 2 then'+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise) = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 3 then'+
    '        (select id_plano_conta_associado from conta_analise where id ='+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise)) = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 4 then'+
    '        (select id_plano_conta_associado from conta_analise where id ='+
    '        (select id_plano_conta_associado from conta_analise where id ='+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise))) = :id_conta_analise end';
  end
  else if ANivel = 2 then
  begin
    whereSQL := whereSQL+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 2 then'+
    '        id_conta_analise = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 3 then'+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise) = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 4 then'+
    '        (select id_plano_conta_associado from conta_analise where id ='+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise)) = :id_conta_analise end';
  end
  else if ANivel = 3 then
  begin
    whereSQL := whereSQL+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 3 then'+
    '        id_conta_analise  = :id_conta_analise'+
    '      when (select nivel from conta_analise where id = id_conta_analise) = 4 then'+
    '        (select id_plano_conta_associado from conta_analise where id = id_conta_analise)  = :id_conta_analise end';
  end
  else if ANivel = 4 then
  begin
    whereSQL := whereSQL +
    '      when (select nivel from conta_analise where id = id_conta_analise) = 4 then'+
    '        id_conta_analise = :id_conta_analise end';
  end;

  qConsulta := TFastQuery.ModoDeConsulta(
  '   select'+
  '      ('+
  '        IFNULL((select sum(VL_TITULO_ORIGINAL_HISTORICO)'+
  '           from conta_receber'+
  '          where dt_vencimento between :dt_inicio and :dt_fim'+
  '            and id_centro_resultado = :id_centro_resultado'+
  '            '+whereSQL+'), 0) -'+
  ''+
  '        IFNULL((select sum(VL_TITULO_ORIGINAL_HISTORICO)'+
  '           from conta_pagar'+
  '          where dt_vencimento between :dt_inicio and :dt_fim'+
  '            and id_centro_resultado = :id_centro_resultado'+
  '            '+whereSQL+'), 0)) as valor'+
  '    from filial limit 1',
  TArray<Variant>.Create(APeriodoPrevisaoInicial, APeriodoPrevisaoFinal,
    AIdCentroResultado, AIdContaAnalise));

  result := qConsulta.GetAsFloat('valor');
end;

class function TExtratoPlanoContaServ.GetCentroResultados: TFDJSONDataSets;
var qCentroResultado: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qCentroResultado := TFastQuery.ModoDeConsulta(
    ' SELECT'+
    '   pc.id_centro_resultado,'+
    '   cr.descricao,'+
    '   cr.id_grupo_centro_resultado,'+
    '   gr.descricao AS grupo_centro_resultado,'+
    '   cast(''S'' as char(1)) AS BO_CHECKED'+
    ' FROM'+
    '   plano_conta pc'+
    '   INNER JOIN centro_resultado cr ON (pc.id_centro_resultado = cr.id)'+
    '   INNER JOIN grupo_centro_resultado gr ON (cr.id_grupo_centro_resultado = gr.id)'+
    ' WHERE BO_EXIBIR_EXTRATO_PLANO_CONTA = ''S'''+
    ' ORDER BY'+
    '   cr.id');

  TFDJSONDataSetsWriter.ListAdd(Result, (qCentroResultado as TFastQuery));
end;

class function TExtratoPlanoContaServ.GetNivelAninhado(const ANivel: Integer; const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
var
  qNivelAninhado: IFastQuery;
const
  niveisSuperioresHierarquicos3 =
    ',(select concat(sequencia, '' - '', descricao)'+
    '    from conta_analise'+
    '   where id = (select id_plano_conta_associado'+
    '                  from conta_analise'+
    '                 where id = ca.id_plano_conta_associado)) as desc_plano_conta_associado_1_nivel';

  niveisSuperioresHierarquicos4 =
    ',(select concat(sequencia, '' - '', descricao)'+
    '    from conta_analise'+
    '   where id = (select id_plano_conta_associado'+
    '                 from conta_analise'+
    '                where id = (select id_plano_conta_associado' +
    '                              from conta_analise'+
    '                             where id = ca.id_plano_conta_associado))) as desc_plano_conta_associado_1_nivel';
begin
  result := TFDJSONDataSets.Create;

  case ANivel of
    1,2:
    qNivelAninhado := TFastQuery.ModoDeConsulta(
      '         select ca.id'+
      '           ,ca.sequencia'+
      '           ,ca.descricao'+
      '           ,cr.id as id_centro_resultado'+
      '           ,CONCAT(cr.id, '' - '', cr.descricao) as desc_centro_resultado'+
      '           ,gr.id as id_grupo_centro_resultado'+
      '           ,gr.descricao as desc_grupo_centro_resultado'+
      '           ,ca.id_plano_conta_associado'+
      '           ,(select concat(sequencia, '' - '', descricao) from conta_analise where id = ca.id_plano_conta_associado) as desc_plano_conta_associado'+
      '           ,cast(''S'' as char(1)) AS BO_CHECKED'+
      RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo)+
      '       from plano_conta_item pci'+
      ' inner join conta_analise ca on pci.id_conta_analise = ca.id and ca.bo_exibir_resumo_conta = ''S'' and ca.nivel = :nivel'+
      ' inner join plano_conta pc on pci.id_plano_conta = pc.id and pc.id_centro_resultado in ('+ACentrosResultados+')'+
      ' inner join centro_resultado cr on pc.id_centro_resultado = cr.id'+
      ' inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
      '      where id_plano_conta_associado in ('+APlanosContas+')'+
      '   order by cr.id', TArray<Variant>.Create(ANivel));

    3,4:
    qNivelAninhado := TFastQuery.ModoDeConsulta(
      'select * from ('+
      '         select ca.id'+
      '           ,ca.sequencia'+
      '           ,ca.descricao'+
      '           ,cr.id as id_centro_resultado'+
      '           ,CONCAT(cr.id, '' - '', cr.descricao) as desc_centro_resultado'+
      '           ,gr.id as id_grupo_centro_resultado'+
      '           ,gr.descricao as desc_grupo_centro_resultado'+
      '           ,ca.id_plano_conta_associado'+
      niveisSuperioresHierarquicos3+
      '           ,(select concat(sequencia, '' - '', descricao) from conta_analise where id = ca.id_plano_conta_associado) as desc_plano_conta_associado'+
      '           ,cast(''S'' as char(1)) AS BO_CHECKED'+
      RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo)+
      '       from plano_conta_item pci'+
      ' inner join conta_analise ca on pci.id_conta_analise = ca.id and ca.bo_exibir_resumo_conta = ''S'' and ca.nivel = 3'+
      ' inner join plano_conta pc on pci.id_plano_conta = pc.id and pc.id_centro_resultado in ('+ACentrosResultados+')'+
      ' inner join centro_resultado cr on pc.id_centro_resultado = cr.id'+
      ' inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
      '      where id_plano_conta_associado in ('+APlanosContas+')'+
      ''+
      ' union all'+
      ''+
      '         select ca.id'+
      '           ,ca.sequencia'+
      '           ,ca.descricao'+
      '           ,cr.id as id_centro_resultado'+
      '           ,CONCAT(cr.id, '' - '', cr.descricao) as desc_centro_resultado'+
      '           ,gr.id as id_grupo_centro_resultado'+
      '           ,gr.descricao as desc_grupo_centro_resultado'+
      '           ,ca.id_plano_conta_associado'+
      niveisSuperioresHierarquicos4+
      '           ,(select concat(sequencia, '' - '', descricao) from conta_analise'+
      '             where id = (select id_plano_conta_associado from conta_analise'+
      '                         where id = ca.id_plano_conta_associado)) as desc_plano_conta_associado'+
      '           ,cast(''S'' as char(1)) AS BO_CHECKED'+
      RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo)+
      '       from plano_conta_item pci'+
      ' inner join conta_analise ca on pci.id_conta_analise = ca.id and ca.bo_exibir_resumo_conta = ''S'' and ca.nivel = 4'+
      ' inner join plano_conta pc on pci.id_plano_conta = pc.id and pc.id_centro_resultado in ('+ACentrosResultados+')'+
      ' inner join centro_resultado cr on pc.id_centro_resultado = cr.id'+
      ' inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
      '   ) contasAnalise order by id');
  end;

  TFDJSONDataSetsWriter.ListAdd(result, qNivelAninhado as TFastQuery);
end;

class function TExtratoPlanoContaServ.GetPrimeiroNivel(const ANivel: Integer; const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
var qPrimeiroNivel: IFastQuery;
  SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
    '       select ca.id'+
    '             ,ca.sequencia'+
    '             ,ca.descricao'+
    '             ,cr.id as id_centro_resultado'+
    '             ,concat(cr.id, '' - '', cr.descricao) as desc_centro_resultado'+
    '             ,gr.id as id_grupo_centro_resultado'+
    '             ,gr.descricao as desc_grupo_centro_resultado'+
    '             ,cast(''S'' as char(1)) AS BO_CHECKED'+
    RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo)+
    '         from plano_conta_item pci'+
    '   inner join conta_analise ca on pci.id_conta_analise = ca.id and ca.bo_exibir_resumo_conta = ''S'' and ca.nivel = :nivel'+
    '   inner join plano_conta pc on pci.id_plano_conta = pc.id and pc.id_centro_resultado in ('+ACentrosResultados+')'+
    '   inner join centro_resultado cr on pc.id_centro_resultado = cr.id'+
    '   inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
    '     order by gr.id, cr.id, sequencia';

  qPrimeiroNivel := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(ANivel));

  TFDJSONDataSetsWriter.ListAdd(result, (qPrimeiroNivel as TFastQuery));
end;

class function TExtratoPlanoContaServ.GetTotalPorCentroResultado(const ACentrosResultados, ADtInicial, ADtFinal: String): TFDJSONDataSets;
var qTotal: IFastQuery;
    SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
    ' select '+
    '  sum(Iif(ccm.tp_movimento=''CREDITO'', ccm.vl_movimento,0)) - sum(Iif(ccm.tp_movimento=''DEBITO'', ccm.vl_movimento,0)) as vl_total '+
    ' ,ccm.id_centro_resultado '+
    ' ,concat(extract(month from ccm.dt_movimento), ''/'', extract(year from ccm.dt_movimento))) as periodo '+
    ' from conta_corrente_movimento ccm '+
    ' where ccm.id_centro_resultado in ('+ACentrosResultados+')'+
    ' and ccm.dt_movimento between :dt_inicio and :dt_termino '+
    ' GROUP BY  ccm.id_centro_resultado,  periodo ';

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  TFDJSONDataSetsWriter.ListAdd(result, (qTotal as TFastQuery));
end;

class function TExtratoPlanoContaServ.GetTotalPorContaCorrente(const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
var qTotal: IFastQuery;
    SQL: String;
    whereIDPlanoConta, whereFieldIDPlanoConta: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select '+
  '  sum(If(ccm.tp_movimento=''CREDITO'', ccm.vl_movimento,0)) - sum(if(ccm.tp_movimento=''DEBITO'', ccm.vl_movimento,0)) as vl_total '+
  ' ,ccm.id_conta_corrente '+
  ' ,ccm.id_centro_resultado'+
  ' ,concat(extract(month from ccm.dt_movimento), ''/'', extract(year from ccm.dt_movimento)) as periodo '+
  ' from conta_corrente_movimento ccm '+
  ' where ccm.id_centro_resultado in ('+ACentrosResultados+')'+
  ' and ccm.dt_movimento between :dt_inicio and :dt_termino '+
  ' GROUP BY  ccm.id_centro_resultado, ccm.id_conta_corrente, periodo ';

  whereIDPlanoConta := 'ccm.id_plano_conta in (:id_plano_conta)';
  whereFieldIDPlanoConta := 'ccm.id_plano_conta';

  ReplaceMacro(
    SQL,
    whereIDPlanoConta,
    GetString_AGGParticionado(whereFieldIDPlanoConta, AContasAnalises));

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial))
    , TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  TFDJSONDataSetsWriter.ListAdd(result, (qTotal as TFastQuery));
end;

class function TExtratoPlanoContaServ.GetTotalPorNivel(const ANivel: Integer; const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
var
  qTotal: IFastQuery;
  SQL: String;
  whereIDPlanoConta, whereFieldIDPlanoConta: String;
const
  FIELD_WHERE_NIVEL_3 =
  '  (SELECT ID_PLANO_CONTA_ASSOCIADO'+
  '     FROM CONTA_ANALISE'+
  '    WHERE ID = ID_CONTA_ANALISE) ';

  FIELD_WHERE_NIVEL_4 =
  '  (SELECT ID_PLANO_CONTA_ASSOCIADO'+
  '     FROM CONTA_ANALISE'+
  '    WHERE ID = (SELECT ID_PLANO_CONTA_ASSOCIADO'+
  '                  FROM CONTA_ANALISE'+
  '                 WHERE ID = ID_CONTA_ANALISE)) ';
begin
  result := TFDJSONDataSets.Create;

  if ANivel in [1,2] then
  begin
    SQL :=
      'select sum(vl_total) as vl_total, id_conta_analise, id_centro_resultado, periodo from ('+
      '   select'+
      '    sum(If(ccm.tp_movimento=''CREDITO'', ccm.vl_movimento,0)) - sum(If(ccm.tp_movimento=''DEBITO'', ccm.vl_movimento,0)) as vl_total'+

      '   ,case';

    if ANivel = 1 then
    begin
      SQL := SQL+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 1 then'+
      '        ccm.id_conta_analise'+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 2 then'+
      '        (select id_plano_conta_associado from conta_analise where id = ccm.id_conta_analise)'+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 3 then'+
      '        (select id_plano_conta_associado from conta_analise where id ='+
      '        (select id_plano_conta_associado from conta_analise where id = ccm.id_conta_analise))'+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 4 then'+
      '        (select id_plano_conta_associado from conta_analise where id ='+
      '        (select id_plano_conta_associado from conta_analise where id ='+
      '        (select id_plano_conta_associado from conta_analise where id = ccm.id_conta_analise)))';
    end
    else if ANivel = 2 then
    begin
      SQL := SQL+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 2 then'+
      '        ccm.id_conta_analise'+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 3 then'+
      '        (select id_plano_conta_associado from conta_analise where id = ccm.id_conta_analise)'+
      '      when (select nivel from conta_analise where id = ccm.id_conta_analise) = 4 then'+
      '        (select id_plano_conta_associado from conta_analise where id ='+
      '        (select id_plano_conta_associado from conta_analise where id = ccm.id_conta_analise))';
    end;

    SQL := SQL +
    '    end as id_conta_analise'+

    '   ,ccm.id_centro_resultado'+
    '   ,concat(extract(month from ccm.dt_movimento), ''/'', extract(year from ccm.dt_movimento)) as periodo'+
    '   from conta_corrente_movimento ccm'+
    '   where ccm.id_centro_resultado in ('+ACentrosResultados+')'+
    '     and ccm.dt_movimento between :dt_inicio and :dt_termino'+
    '   GROUP BY  ccm.id_centro_resultado, ccm.id_conta_analise, periodo) conta_corrente';

    SQL := SQL +'   WHERE (SELECT ID_PLANO_CONTA_ASSOCIADO FROM CONTA_ANALISE WHERE ID = ID_CONTA_ANALISE) in (:id_conta_analise)';
    whereIDPlanoConta := '(SELECT ID_PLANO_CONTA_ASSOCIADO FROM CONTA_ANALISE WHERE ID = ID_CONTA_ANALISE) in (:id_conta_analise)';
    whereFieldIDPlanoConta := '(SELECT ID_PLANO_CONTA_ASSOCIADO FROM CONTA_ANALISE WHERE ID = ID_CONTA_ANALISE)';

    SQL := SQL +'  group by id_centro_resultado, id_conta_analise, periodo';

    if ANivel = 1 then
      ReplaceMacro(
        SQL,
        whereIDPlanoConta,
        '1=1')
    else
      ReplaceMacro(
        SQL,
        whereIDPlanoConta,
        GetString_AGGParticionado(whereFieldIDPlanoConta,AContasAnalises));

    qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
      TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
      TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

    TFDJSONDataSetsWriter.ListAdd(result, (qTotal as TFastQuery));
  end
  else if ANivel in [3,4] then
  begin
    SQL :=
    ' select sum(vl_total) as vl_total, id_conta_analise, id_centro_resultado, periodo from ('+
    '   select'+
    '    sum(If(ccm.tp_movimento=''CREDITO'', ccm.vl_movimento,0)) - sum(If(ccm.tp_movimento=''DEBITO'', '+
    ' ccm.vl_movimento,0)) as vl_total';

    SQL := SQL+
    '   ,ccm.id_conta_analise'+

    '   ,ccm.id_centro_resultado'+
    '   ,concat(extract(month from ccm.dt_movimento), ''/'', extract(year from ccm.dt_movimento)) as periodo'+
    '   from conta_corrente_movimento ccm'+
    '   where ccm.id_centro_resultado in ('+ACentrosResultados+')'+
    '     and ccm.dt_movimento between :dt_inicio and :dt_termino'+
    '   GROUP BY  ccm.id_centro_resultado, ccm.id_conta_analise, periodo) conta_corrente';

    if ANivel in [3,4] then
    begin
      SQL := SQL +' WHERE '+FIELD_WHERE_NIVEL_3+' in (:id_conta_analise_3)';
      whereIDPlanoConta := FIELD_WHERE_NIVEL_3+' in (:id_conta_analise_3)';
      whereFieldIDPlanoConta := FIELD_WHERE_NIVEL_3;

      ReplaceMacro(
      SQL,
      whereIDPlanoConta,
      GetString_AGGParticionado(whereFieldIDPlanoConta,AContasAnalises));

      SQL := SQL +' OR '+FIELD_WHERE_NIVEL_4+' in (:id_conta_analise_4)';
      whereIDPlanoConta := FIELD_WHERE_NIVEL_4+' in (:id_conta_analise_4)';
      whereFieldIDPlanoConta := FIELD_WHERE_NIVEL_4;

      ReplaceMacro(
      SQL,
      whereIDPlanoConta,
      GetString_AGGParticionado(whereFieldIDPlanoConta,AContasAnalises));
    end;

    SQL := SQL +'  group by id_centro_resultado, id_conta_analise, periodo';

    qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
      TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
      TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

    TFDJSONDataSetsWriter.ListAdd(result, (qTotal as TFastQuery));
  end;
end;

class procedure TExtratoPlanoContaServ.ReplaceMacro(var ASQL: String; const AMacro, AValue: String);
begin
   ASQL := StringReplace(UpperCase(ASQL), UpperCase(AMacro), AValue, [rfReplaceAll]);
end;

class function TExtratoPlanoContaServ.RetornarCamposDoPeriodoPersonalizado(
  const AListaCamposPeriodo: String): String;
var fieldName, fieldNamePrevisao: String;
    i: Integer;
    lista: TStringList;
begin
  lista := TStringList.Create();
  lista.DelimitedText := AListaCamposPeriodo;
  try
    for i := 0 to Pred(lista.Count) do
    begin
      fieldName := GetFieldNameFromPeriodoColumn(lista[i]);
      fieldNamePrevisao := GetFieldNameFromPeriodoColumn(lista[i])+'PREV';
      result := result+',cast(0 as Decimal(15,2)) as '+fieldNamePrevisao;
      result := result+',cast(0 as Decimal(15,2)) as '+fieldName;
    end;
  finally
    lista.Free;
  end;
end;

class procedure TExtratoPlanoContaServ.SalvarPrevisao(AIdCentroResultado: Integer;
  AIdContaAnalise: Integer; APeriodo: String; AValor: Double);
var
  qExtratoPlanoContaPrevisao: TFastQuery;
  fdmPrevisao: TFDMemTable;
  i: Integer;
begin
  exit;

  qExtratoPlanoContaPrevisao := TFastQuery.ModoDeAlteracao(
    ' SELECT * FROM EXTRATO_PLANO_CONTA_PREVISAO'+
    ' WHERE periodo = :periodo and id_centro_resultado = :id_centro_resultado'+
    ' and id_conta_analise = :id_conta_analise',
    TArray<Variant>.Create(APeriodo, AIdCentroResultado, AIdContaAnalise));

  if qExtratoPlanoContaPrevisao.IsEmpty then
  begin
    qExtratoPlanoContaPrevisao.Incluir;
    qExtratoPlanoContaPrevisao.SetAsInteger('ID_CONTA_ANALISE', AIdContaAnalise);
    qExtratoPlanoContaPrevisao.SetAsInteger('ID_CENTRO_RESULTADO', AIdCentroResultado);
    qExtratoPlanoContaPrevisao.SetAsString('PERIODO', APeriodo);
    qExtratoPlanoContaPrevisao.SetAsFloat('VALOR', AValor);
    qExtratoPlanoContaPrevisao.Persistir;
  end
  else
  begin
    qExtratoPlanoContaPrevisao.Alterar;
    qExtratoPlanoContaPrevisao.SetAsFloat('VALOR', AValor);
    qExtratoPlanoContaPrevisao.Persistir;
  end;

{  fdmPrevisao.First;
  while fdmPrevisao.Eof do
  begin
    for i := 0 to fdmPrevisao.FieldCount -1 do
    begin
      if Copy(fdmPrevisao.Fields[i].FieldName, 1, 4) = 'PREV' then
      begin
        if qExtratoPlanoContaPrevisao.Locate('PERIODO;ID_CONTA_ANALISE',
          VarArrayOf([fdmPrevisao.Fields[i].FieldName, fdmPrevisao.FieldByName('ID').AsInteger]))then
        begin
          qExtratoPlanoContaPrevisao.Alterar;
          qExtratoPlanoContaPrevisao.SetAsFloat('VALOR', fdmPrevisao.Fields[i].AsFloat);
          qExtratoPlanoContaPrevisao.Persistir;
        end
        else
        begin
          qExtratoPlanoContaPrevisao.Incluir;
          qExtratoPlanoContaPrevisao.SetAsInteger('ID_CONTA_ANALISE', fdmPrevisao.FieldByName('ID').AsInteger);
          qExtratoPlanoContaPrevisao.SetAsString('PERIODO', fdmPrevisao.Fields[i].FieldName);
          qExtratoPlanoContaPrevisao.SetAsFloat('VALOR', fdmPrevisao.Fields[i].AsFloat);
          qExtratoPlanoContaPrevisao.Persistir;
        end;
      end;
    end;
    fdmPrevisao.Next;
  end;  }
end;

class function TExtratoPlanoContaServ.GetString_AGGParticionado(const AFieldParam: String;AStringAGG: String): String;
var i: Integer;
    caracterCorreto: boolean;
begin
  while not AStringAGG.IsEmpty do
  begin
    if not(result.IsEmpty) and not(AStringAGG.IsEmpty) then
      result := result + ' OR ';

    i := 200;

    if i > Length(AStringAGG) then
      i := Length(AStringAGG)
    else
    begin
      caracterCorreto := false;
      while not (caracterCorreto) do
      begin
        if (AStringAGG[i] <> '') and (AStringAGG[i] = ',') then
        begin
          dec(i);
          break
        end
        else
          inc(i);

        if i > Length(AStringAGG) then break;
      end;
    end;

    result := result + ' ' + AFieldParam + ' IN ('+Copy(AStringAGG, 1, i)+')';
    delete(AStringAGG,1,i+1);
  end;
end;

class function TExtratoPlanoContaServ.GetContasCorrentes(const ACentrosResultados,
  APlanosContas, AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
var qContaCorrente: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qContaCorrente := TFastQuery.ModoDeConsulta(
    '     select cc.id'+
    '           ,cc.descricao'+
    '           ,cr.id as id_centro_resultado'+
    '           ,cr.id || '' - '' || cr.descricao as desc_centro_resultado'+
    '           ,gr.id as id_grupo_centro_resultado'+
    '           ,gr.descricao as desc_grupo_centro_resultado'+
    ' ,cast(''S'' as char(1)) AS BO_CHECKED'+
    RetornarCamposDoPeriodoPersonalizado(AListaCamposPeriodo)+
    '       from conta_corrente cc'+
    ' inner join conta_corrente_movimento ccm on cc.id = ccm.id_conta_corrente'+
    '  and ccm.id_conta_analise IN ('+APlanosContas+')'+
    '  and ccm.dt_movimento between :dt_inicio and :dt_termino '+
    ' inner join centro_resultado cr on ccm.id_centro_resultado = cr.id'+
    ' inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
    '      where cr.id in ('+ACentrosResultados+')'+
    '   group by cc.id, cc.descricao, cr.id, cr.descricao, gr.id, gr.descricao'+
    '   order by cc.id', TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  TFDJSONDataSetsWriter.ListAdd(Result, (qContaCorrente as TFastQuery));
end;

class function TExtratoPlanoContaServ.GetFieldNameFromPeriodoColumn(const AMes: String): String;
begin
  result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes)+1, 4);
end;

class function TExtratoPlanoContaServ.GetMovimentacao(const ACentrosResultados,
  APlanosContas, AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal: String): TFDJSONDataSets;
var qMovimentacao: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qMovimentacao := TFastQuery.ModoDeConsulta(
  '      select ccm.*'+
  '        ,cc.descricao as desc_conta_corrente'+
  '        ,ca.sequencia'+
  '        ,ca.descricao as desc_plano_conta'+
  '        ,substring(tp_movimento from 1 for 1) as tp_movimento_sigla'+
  '        ,concat(cr.id, '' - '', cr.descricao) as desc_centro_resultado'+
  '        ,gr.id as id_grupo_centro_resultado'+
  '        ,gr.descricao as desc_grupo_centro_resultado'+
  '         from conta_corrente_movimento ccm'+
  '  inner join conta_corrente cc on ccm.id_conta_corrente = cc.id'+
  '  inner join conta_analise ca    on ccm.id_conta_analise = ca.id'+
  '  inner join centro_resultado cr on ccm.id_centro_resultado = cr.id'+
  '  inner join grupo_centro_resultado gr on cr.id_grupo_centro_resultado = gr.id'+
{  '  where ccm.id_conta_corrente IN ('+AContasCorrentes+')'+   }
  '    where ccm.id_conta_analise IN ('+APlanosContas+')'+
  '    and cr.id in ('+ACentrosResultados+')'+
  '    and ccm.dt_movimento between :dt_inicio and :dt_termino'+
  '  order by ca.sequencia, ccm.dt_movimento, observacao', TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtInicial)),
    TStringUtils.RetornarDataNoFormatoMYSQL(StrToDate(ADtFinal))));

  TFDJSONDataSetsWriter.ListAdd(Result, (qMovimentacao as TFastQuery));
end;

end.

