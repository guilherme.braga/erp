unit uAjusteEstoqueServ;

interface

uses uFactoryQuery, uAjusteEstoqueProxy, Generics.Collections;

Type  TAjusteEstoqueServ = class
  private
    class function GetAjusteEstoque(AIdChaveProcesso: Integer): TFastQuery;
    class function GetAjusteEstoqueItem(AIdAjusteEstoque: Integer): TFastQuery;

    //Gerar a Movimentação de Estoque
    class function GerarMovimentacaoEstoque(AAjusteEstoque,
      AAjusteEstoqueItem: TFastQuery; ATipoMovimento: String): boolean;

    //Atualizar o Ajuste de Estoque
    class procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
  public
    //Efetivar o ajuste de estoque
    class function Efetivar(AIdChaveProcesso: Integer): Boolean;

    //Cancelar o Ajuste de Estoque
    class function Cancelar(AIdChaveProcesso: Integer): Boolean;

    //Reabrir o Ajuste de Estoque
    class function Reabrir(AIdChaveProcesso: Integer): Boolean;

    //Gera um movimento de ajuste de estoque
    class function GerarAjusteEstoque(
      const AAjusteEstoque: TAjusteEstoqueProxy): Integer;

    //Gera os itens do ajuste de estoque
    class function GerarAjusteEstoqueItem(
      const FIdAjusteEstoque: Integer; const AAjusteEstoqueItem: TList<TAjusteEstoqueItemProxy>): Boolean;

    //Remove o ajuste de estoque
    class procedure RemoverAjusteEstoque(AIdAjusteEstoque: Integer);
    class procedure RemoverAjusteEstoquePelaChaveProcesso(AChaveProcesso: Integer);
end;

implementation

{ TAjusteEstoqueServ }

uses uProdutoServ, SysUtils, uProdutoProxy, REST.Json;

class procedure TAjusteEstoqueServ.AtualizarStatus(AIdChaveProcesso: Integer;
  AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE ajuste_estoque SET status = :status, dh_fechamento = null where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class function TAjusteEstoqueServ.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  try
    TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);

    TAjusteEstoqueServ.AtualizarStatus(AIdChaveProcesso,TAjusteEstoqueProxy.STATUS_ABERTO);
  Except
    result := false;
    raise Exception.Create('Falha ao tentar Cancelar o ajuste de estoque');
  end;
end;

class function TAjusteEstoqueServ.Efetivar(AIdChaveProcesso: Integer): Boolean;
var qAjusteEstoque, qAjusteEstoqueItem: TFastQuery;
begin
  result := true;

  qAjusteEstoque := TAjusteEstoqueServ.GetAjusteEstoque(AIdChaveProcesso);

  if qAjusteEstoque.IsEmpty then
    exit;

  qAjusteEstoqueItem := TAjusteEstoqueServ.GetAjusteEstoqueItem(qAjusteEstoque.GetAsInteger('ID'));

  qAjusteEstoqueItem.Primeiro;
  while not qAjusteEstoqueItem.Eof do
  begin
    if qAjusteEstoqueItem.GetAsString('TIPO').Equals(TAjusteEstoqueProxy.TIPO_ENTRADA) then
    begin
      TAjusteEstoqueServ.GerarMovimentacaoEstoque(qAjusteEstoque, qAjusteEstoqueItem, TAjusteEstoqueProxy.TIPO_ENTRADA);
    end;

    if qAjusteEstoqueItem.GetAsString('TIPO').Equals(TAjusteEstoqueProxy.TIPO_SAIDA) then
    begin
      TAjusteEstoqueServ.GerarMovimentacaoEstoque(qAjusteEstoque, qAjusteEstoqueItem, TAjusteEstoqueProxy.TIPO_SAIDA);
    end;

    qAjusteEstoqueItem.Proximo;
  end;

  AtualizarStatus(AIdChaveProcesso, TAjusteEstoqueProxy.STATUS_CONFIRMADO);
end;

class function TAjusteEstoqueServ.GerarAjusteEstoque(
  const AAjusteEstoque: TAjusteEstoqueProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('ajuste_estoque');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', AAjusteEstoque.FDhCadastro);
    qEntidade.SetAsString('DH_FECHAMENTO', AAjusteEstoque.FDhFechamento);
    qEntidade.SetAsString('STATUS', AAjusteEstoque.FStatus);

    if AAjusteEstoque.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AAjusteEstoque.FIdFilial);
    end;

    if AAjusteEstoque.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', AAjusteEstoque.FIdPessoaUsuario);
    end;

    if AAjusteEstoque.FIdMotivo > 0 then
    begin
      qEntidade.SetAsInteger('ID_MOTIVO', AAjusteEstoque.FIdMotivo);
    end;

    if AAjusteEstoque.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AAjusteEstoque.FIdChaveProcesso);
    end;

    qEntidade.Salvar;
    qEntidade.Persistir;
    AAjusteEstoque.FID := qEntidade.GetAsInteger('ID');

    GerarAjusteEstoqueItem(AAjusteEstoque.FID, AAjusteEstoque.FAjusteEstoqueItem);

    TAjusteEstoqueServ.Efetivar(AAjusteEstoque.FIdChaveProcesso);

    result := AAjusteEstoque.FID;
  except
    on e : Exception do
    begin
      if AAjusteEstoque.FID > 0 then
      begin
        RemoverAjusteEstoque(AAjusteEstoque.FID);
      end;

      raise Exception.Create('Erro ao gerar AjusteEstoque.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TAjusteEstoqueServ.GerarAjusteEstoqueItem(
  const FIdAjusteEstoque: Integer;
  const AAjusteEstoqueItem: TList<TAjusteEstoqueItemProxy>): Boolean;
var
  qEntidade: IFastQuery;
  item: TAjusteEstoqueItemProxy;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('ajuste_estoque_item');

    for item in AAjusteEstoqueItem do
    begin
      qEntidade.Incluir;

      qEntidade.SetAsFloat('QUANTIDADE', item.FQuantidade);

      if item.FIdProduto > 0 then
      begin
        qEntidade.SetAsInteger('ID_PRODUTO', item.FIdProduto);
      end;

      if FIdAjusteEstoque > 0 then
      begin
        qEntidade.SetAsInteger('ID_AJUSTE_ESTOQUE', FIdAjusteEstoque);
      end;

      qEntidade.SetAsInteger('NR_ITEM', item.FNrItem);
      qEntidade.SetAsString('TIPO', item.FTipo);
      qEntidade.Salvar;
    end;

    qEntidade.Persistir;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar produtos no Ajuste de Estoque.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TAjusteEstoqueServ.GerarMovimentacaoEstoque(AAjusteEstoque,
  AAjusteEstoqueItem: TFastQuery; ATipoMovimento: String): boolean;
var
  estoque: TProdutoMovimento;
  objectMovimentoEstoqueJSON: String;
begin
  result := true;

  estoque := TProdutoMovimento.Create;
  try
    with estoque do
    begin
      FIdProduto         := AAjusteEstoqueItem.GetAsInteger('ID_PRODUTO');
      FIdFilial          := AAjusteEstoque.GetAsInteger('ID_FILIAL');
      FIdChaveProcesso   := AAjusteEstoque.GetAsInteger('ID_CHAVE_PROCESSO');
      FTipo              := ATipoMovimento;
      FDocumentoOrigem   := DOCUMENTO_AJUSTE_ESTOQUE;
      FDtMovimento       := DateToStr(Date);
      FQtMovimento       := AAjusteEstoqueItem.GetAsFloat('QUANTIDADE');
    end;
    objectMovimentoEstoqueJSON := TJson.ObjectToJsonString(estoque);
    TProdutoMovimentoServ.GerarMovimentacaoProduto(objectMovimentoEstoqueJSON);
  finally
    FreeAndNil(estoque);
  end;
end;

class function TAjusteEstoqueServ.GetAjusteEstoque(
  AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM ajuste_estoque WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

end;

class function TAjusteEstoqueServ.GetAjusteEstoqueItem(
  AIdAjusteEstoque: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM ajuste_estoque_item WHERE id_ajuste_estoque = :id_ajuste_estoque',
    TArray<Variant>.Create(AIdAjusteEstoque));
end;

class function TAjusteEstoqueServ.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  try
    TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);

    TAjusteEstoqueServ.AtualizarStatus(AIdChaveProcesso,TAjusteEstoqueProxy.STATUS_ABERTO);
  Except
    result := false;
    raise Exception.Create('Falha ao tentar Reabrir o ajuste de estoque');
  end;
end;

class procedure TAjusteEstoqueServ.RemoverAjusteEstoque(
  AIdAjusteEstoque: Integer);
begin
  TFastQuery.ExecutarScript(
    'DELETE FROM ajuste_estoque_item WHERE id_ajuste_estoque = :id',
    TArray<Variant>.Create(AIdAjusteEstoque));

  TFastQuery.ExecutarScript('DELETE FROM ajuste_estoque WHERE id = :id',
    TArray<Variant>.Create(AIdAjusteEstoque));
end;

class procedure TAjusteEstoqueServ.RemoverAjusteEstoquePelaChaveProcesso(
  AChaveProcesso: Integer);
var qAjusteEstoque: IFastQuery;
begin
  qAjusteEstoque := TFastQuery.ModoDeConsulta(
    'SELECT id FROM ajuste_estoque WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AChaveProcesso));

  TAjusteEstoqueServ.RemoverAjusteEstoque(qAjusteEstoque.GetAsInteger('id'));
end;

end.
