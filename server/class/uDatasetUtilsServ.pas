unit uDatasetUtilsServ;

interface

uses uGBFDQuery, System.Generics.Collections, SysUtils, db, Variants, Classes;

type TDatasetUtilsServ = class
  public
    class procedure DuplicarRegistro(AQueryOrigem, AQueryDestino: TgbFDQuery;
      const ACamposQueNaoSeraoCopiados, ACamposQueSeraoCopiadosMasTeraoOutrosValores: Array of String;
      ANovosValores: Array of Variant);
    class function SomarColuna(AField: TField): Double;
    class function Concatenar(AField: TField): string;
end;

implementation

class function TDatasetUtilsServ.Concatenar(AField: TField): string;
var
  Concatenacao: TStringList;
begin
  Concatenacao := TStringList.Create;
  try
    AField.Dataset.First;
    while not AField.Dataset.Eof do
    try
      Concatenacao.add(AField.AsString);
      AField.Dataset.Next;
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(Concatenacao);
  end;
end;

class procedure TDatasetUtilsServ.DuplicarRegistro(AQueryOrigem, AQueryDestino: TgbFDQuery;
  const ACamposQueNaoSeraoCopiados, ACamposQueSeraoCopiadosMasTeraoOutrosValores: Array of String;
  ANovosValores: Array of Variant);
var
  i,x, c: Integer;
  lAdiciona: Boolean;
begin
  AQueryDestino.Append;
  for i:=0 to Pred(AQueryOrigem.FieldCount) do
  begin
    lAdiciona := True;

    for x:= 0 to Pred(Length(ACamposQueNaoSeraoCopiados)) do
      if (UPPERCASE(AQueryOrigem.Fields[i].FullName) = UPPERCASE(ACamposQueNaoSeraoCopiados[x]))
        or (AQueryOrigem.Fields[i] is TDataSetField) then
        lAdiciona := False;

    if lAdiciona then
    begin
      c:= -1;
      for x:= 0 to Pred(Length(ACamposQueSeraoCopiadosMasTeraoOutrosValores)) do
        if UPPERCASE(AQueryOrigem.Fields[i].FullName) = UPPERCASE(ACamposQueSeraoCopiadosMasTeraoOutrosValores[x]) then
          c:= x;
      if (c > -1) then
      begin
        if AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName) is TDateTimeField then
          AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsDateTime := ANovosValores[c]
        else if AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName) is TIntegerField then
          AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsInteger := ANovosValores[c]
        else
          AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsString := ANovosValores[c];
      end
      else
        AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsString := AQueryOrigem.FieldByName(AQueryOrigem.Fields[i].FullName).AsString;
    end;
  end;

  AQueryDestino.Salvar;
end;

class function TDatasetUtilsServ.SomarColuna(AField: TField): Double;
begin
  Result := 0;

  AField.Dataset.First;
  while not AField.Dataset.Eof do
  try
    Result := Result + AField.AsFloat;
    AField.Dataset.Next;
  Except
    break;
  end;
end;

end.
