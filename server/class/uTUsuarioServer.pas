unit uTUsuarioServer;

interface

type TUsuario = class

end;

type TUsuarioPesquisaPadrao = class
  procedure SetConsultaAutomatica(AQuery: TFDQuery; const ACodigoUsuario: Integer; const AFormName: String; const AHabilitar: Boolean);
  function GetConsultaAutomatica(AQuery: TFDQuery; const ACodigoUsuario: Integer; const AFormName: String): Boolean;
end;

implementation

{ TUsuarioPesquisaPadrao }

function TUsuarioPesquisaPadrao.GetConsultaAutomatica(AQuery: TFDQuery;
  const ACodigoUsuario: Integer; const AFormName: String): Boolean;
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    ' SELECT '+
    '   bo_consulta_automatica'+
    ' FROM '+
    '   pesquisa_padrao'+
    ' WHERE'+
    '   id_pessoa_usuario = :id_pessoa_usuario'
    '   and descricao = :descricao');
  AQuery.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  AQuery.Params.ParamByName('descricao').AsString := AFormName;
  AQuery.Open;

  result := AQuery.FieldByName('bo_consulta_automatica').AsString = 'S';
end;

procedure TUsuarioPesquisaPadrao.SetConsultaAutomatica(AQuery: TFDQuery;
  const ACodigoUsuario: Integer; const AFormName: String;
  const AHabilitar: Boolean);
var status: String;
begin
  if AHabilitar then
    status := 'S'
  else
    status := 'N';

  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    ' UPDATE '+
    '   pesquisa_padrao'+
    ' SET '+
    '   bo_consulta_automatica = :bo_consulta_automatica,'+
    ' WHERE'+
    '   id_pessoa_usuario = :id_pessoa_usuario'
    '   and descricao = :descricao');
  AQuery.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  AQuery.Params.ParamByName('descricao').AsString := AFormName;
  AQuery.Params.ParamByName('id_pessoa_usuario').AsString := status;
  AQuery.ExecSQL;
end;

end.

