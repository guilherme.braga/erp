﻿unit uIntegracaoFiscalServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery, uFactoryQuery,
  uNotaFiscalProxy, uMotorCalculoNotaFiscal, Classes, uProdutoProxy, uFilialProxy;

type TIntegracaoFiscalServ = class
  private
    procedure InstanciarListaInconsistencias;
    procedure InstanciarMotorCalculo;
    procedure DestruirMotorCalculo;
    procedure DestruirListaInconsistencias;
  public
    procedure EstornarContasReceber; virtual;
    procedure EstornarContaCorrenteMovimento; virtual;
    procedure GerarContasReceber; virtual;

    constructor CreateIntegracaoNFE(const AIdEntidadeBase: Integer); overload;
    constructor CreateIntegracaoNFCE(const AIdEntidadeBase: Integer; const ACPFNaNota: String); overload;

    destructor Destroy;

    function RealizarIntegracaoFiscal: TRetornoDocumentoFiscalProxy;
  protected
    FTipoDocumentoFiscal: String;
    FIdNotaFiscal: Integer;
    FIdEntidadeBase: Integer;
    FCpfNaNota: String;
    FNotaFiscal: IFastQuery;
    FNotaFiscalItem: IFastQuery;
    FNotaFiscalParcela: IFastQuery;
    FEntidadeBase: IFastQuery;
    FEntidadeBaseItem: IFastQuery;
    FEntidadeBaseParcela: IFastQuery;
    FInconsistencias: TStringList;
    FMotorCalculoNotaFiscal: TMotorCalculoNotaFiscal;

    FNotaFiscalTransiente: TNotaFiscalTransienteProxy;

    function ValidarDocumentoFiscalJaEmitido: Boolean; virtual; abstract;
    procedure InstanciarEntidadeOrigem; virtual; abstract;
    procedure InstanciarNotaFiscal; virtual;
    procedure IncluirNotaFiscal; virtual;
    procedure IniciarInclusaoNotaFiscal; virtual;
    procedure AtribuirCabecalhoNotaFiscal; virtual;
    procedure AtribuirChavesEstrangeiras; virtual;
    procedure ValidarChavesEstrangeiras; virtual;
    procedure AtribuirNumeroNotaFiscal; virtual;
    procedure AtribuirSerieNotaFiscal; virtual;
    procedure AtribuirModeloNotaFiscal; virtual;
    procedure AtribuirFreteNotaFiscal; virtual;
    procedure AtribuirFinanceiroNotaFiscal; virtual;
    procedure AtribuirTotais; virtual;
    procedure AtribuirRetornoSefaz; virtual;
    procedure ConcluirInclusaoNotaFiscal; virtual;
    procedure IncluirNotaFiscalItem; virtual;
    procedure IniciarInclusaoItens; virtual;
    procedure AtribuirInformacaoGeralItem(const AProdutoFiscal: TProdutoFiscalProxy); virtual;
    procedure AtribuirValoresItem(const AProdutoFiscal: TProdutoFiscalProxy); virtual;
    procedure AtribuirICMSItem(const AProdutoFiscal: TProdutoFiscalProxy); virtual;
    procedure AtribuirPISCofinsItem(const AProdutoFiscal: TProdutoFiscalProxy); virtual;
    procedure AtribuirIPIItem(const AProdutoFiscal: TProdutoFiscalProxy); virtual;
    procedure CalcularValorTotalItem; virtual;
    procedure SalvarInclusaoItens; virtual;
    procedure PersistirInclusaoItens; virtual;
    procedure CalcularValorTotalNotaFiscal; virtual;
    procedure IncluirNotaFiscalParcela; virtual;
    procedure IniciarInclusaoParcelas; virtual;
    procedure AtribuirParcela; virtual;
    procedure SalvarInclusaoParcelas; virtual;
    procedure PersistirInclusaoParcelas; virtual;
    procedure AtualizarCodigoNaEntidadeBase; virtual; abstract;
    procedure RemoverNotaFiscalIntegracaoFiscal; virtual;
end;

implementation

uses uPessoaServ, uNotaFiscalServ, uModeloNotaServ, uProdutoServ, uFilialServ, uImpostoICMSProxy,
  uImpostoIPIProxy, uImpostoPisCofinsProxy, uImpostoICMSServ, uImpostoIPIServ, uImpostoPisCofinsServ,
  uContaReceberServ, uContaCorrenteServ;

constructor TIntegracaoFiscalServ.CreateIntegracaoNFE(const AIdEntidadeBase: Integer);
begin
  inherited Create;
  FNotaFiscalTransiente := TNotaFiscalTransienteProxy.Create;
  FTipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE;
  FIdEntidadeBase := AIdEntidadeBase;
  FCpfNaNota := '';
  InstanciarListaInconsistencias;
end;

constructor TIntegracaoFiscalServ.CreateIntegracaoNFCE(const AIdEntidadeBase: Integer; const ACPFNaNota: String);
begin
  inherited Create;
  FNotaFiscalTransiente := TNotaFiscalTransienteProxy.Create;
  FTipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE;
  FIdEntidadeBase := AIdEntidadeBase;
  FCpfNaNota := ACPFNaNota;
  InstanciarListaInconsistencias;
end;

procedure TIntegracaoFiscalServ.InstanciarListaInconsistencias;
begin
  FInconsistencias := TStringList.Create;
end;

procedure TIntegracaoFiscalServ.DestruirListaInconsistencias;
begin
  if Assigned(FInconsistencias) then
  begin
    FreeAndNil(FInconsistencias);
  end;

  if Assigned(FNotaFiscalTransiente) then
  begin
    FreeAndNil(FNotaFiscalTransiente);
  end;
end;

procedure TIntegracaoFiscalServ.InstanciarMotorCalculo;
var
  filial: TFilialProxy;
begin
  filial := TFilialServ.GetFilial(FEntidadeBase.GetAsInteger('ID_FILIAL'));
  try
    FMotorCalculoNotaFiscal := TMotorCalculoNotaFiscal.Create(FTipoDocumentoFiscal,
      TNotaFiscalProxy.TIPO_NF_SAIDA, FNotaFiscal as TDataset, FNotaFiscalItem as TDataset, filial);
  finally
    //FreeAndNil(filial);
  end;
end;

procedure TIntegracaoFiscalServ.DestruirMotorCalculo;
begin
  if Assigned(FMotorCalculoNotaFiscal) then
  begin
    FreeAndNil(FMotorCalculoNotaFiscal);
  end;
end;

procedure TIntegracaoFiscalServ.EstornarContaCorrenteMovimento;
begin
  TContaCorrenteServ.RemoverMovimentoContaCorrente(FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO'));
end;

procedure TIntegracaoFiscalServ.EstornarContasReceber;
begin
  TContaReceberServ.RemoverContaReceber(FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO'));
end;

procedure TIntegracaoFiscalServ.GerarContasReceber;
begin
  EstornarContaCorrenteMovimento;
  EstornarContasReceber;
  TNotaFiscalServ.GerarContaReceber(FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO'), FNotaFiscalTransiente);
end;

destructor TIntegracaoFiscalServ.Destroy;
begin
  DestruirListaInconsistencias;
  DestruirMotorCalculo;
end;

procedure TIntegracaoFiscalServ.InstanciarNotaFiscal;
begin
  FNotaFiscal := TFastQuery.ModoDeInclusao('nota_fiscal');
  FNotaFiscalItem := TFastQuery.ModoDeInclusao('nota_fiscal_item');
  FNotaFiscalParcela := TFastQuery.ModoDeInclusao('nota_fiscal_parcela');
end;  

procedure TIntegracaoFiscalServ.IncluirNotaFiscal;
begin
  IniciarInclusaoNotaFiscal;
  AtribuirCabecalhoNotaFiscal;
  AtribuirChavesEstrangeiras;	 
  ValidarChavesEstrangeiras;
  AtribuirNumeroNotaFiscal;
  AtribuirSerieNotaFiscal;
  AtribuirModeloNotaFiscal;    
  AtribuirFreteNotaFiscal;
  AtribuirFinanceiroNotaFiscal;
  AtribuirTotais;
  AtribuirRetornoSefaz;
  ConcluirInclusaoNotaFiscal;
end;

procedure TIntegracaoFiscalServ.IniciarInclusaoNotaFiscal;
begin
  FNotaFiscal.Incluir;
end;		

procedure TIntegracaoFiscalServ.AtribuirCabecalhoNotaFiscal;
begin
  FNotaFiscal.SetAsDateTime('DH_CADASTRO', FEntidadeBase.GetAsDateTime('DH_CADASTRO'));
  FNotaFiscal.SetAsDateTime('B_DEMI', FEntidadeBase.GetAsDateTime('DH_CADASTRO'));
  FNotaFiscal.SetAsDateTime('B_DSAIENT', FEntidadeBase.GetAsDateTime('DH_CADASTRO'));
  FNotaFiscal.SetAsDateTime('B_HSAIENT', FEntidadeBase.GetAsDateTime('DH_CADASTRO'));
  FNotaFiscal.SetAsString('TIPO_NF', TNotaFiscalProxy.TIPO_NF_SAIDA);    

  FNotaFiscal.SetAsString('X_XNOME', TPessoaServ.GetNomePessoa(FEntidadeBase.GetAsInteger('ID_PESSOA')));
  FNotaFiscal.SetAsString('DOC_FEDERAL_PESSOA', FCPFNaNota);

  FNotaFiscal.SetAsString('STATUS', STATUS_NOTA_CONFIRMADA);
end;

procedure TIntegracaoFiscalServ.AtribuirFreteNotaFiscal;
begin
  FNotaFiscal.SetAsFloat('VL_FRETE_TRANSPORTADORA', 0);
  FNotaFiscal.SetAsInteger('X_MODFRETE', TNotaFiscalProxy.MODALIDADE_FRETE_SEM_FRETE);
end;

procedure TIntegracaoFiscalServ.AtribuirNumeroNotaFiscal;
begin
  FNotaFiscal.SetAsInteger('B_NNF', TNotaFiscalServ.GetNumeroDocumentoNotaFiscalSaida(FTipoDocumentoFiscal));
end;

procedure TIntegracaoFiscalServ.AtribuirSerieNotaFiscal;
begin
  FNotaFiscal.SetAsInteger('B_SERIE', TNotaFiscalServ.GetSerieNotaFiscalSaida(FTipoDocumentoFiscal));
end;

procedure TIntegracaoFiscalServ.AtribuirModeloNotaFiscal;
var
  modeloNota: String;
  idModeloNota: Integer;
begin
  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    modeloNota := TNotaFiscalProxy.MODELO_NFE;
  end
  else if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    modeloNota := TNotaFiscalProxy.MODELO_NFCE;
  end;

  idModeloNota := TModeloNotaServ.BuscarIdModelo(modeloNota);
  FNotaFiscal.SetAsInteger('ID_MODELO_NOTA', idModeloNota);
end;	

procedure TIntegracaoFiscalServ.AtribuirChavesEstrangeiras;
begin
  if FEntidadeBase.GetAsInteger('ID_CENTRO_RESULTADO') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_CENTRO_RESULTADO', FEntidadeBase.GetAsInteger('ID_CENTRO_RESULTADO'));
  end;

  if FEntidadeBase.GetAsInteger('ID_CONTA_ANALISE') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_CONTA_ANALISE', FEntidadeBase.GetAsInteger('ID_CONTA_ANALISE'));
  end;

  if FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_CHAVE_PROCESSO', FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO'));
  end;

  if FEntidadeBase.GetAsInteger('ID_OPERACAO') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_OPERACAO', FEntidadeBase.GetAsInteger('ID_OPERACAO'));
  end;

  if FEntidadeBase.GetAsInteger('ID_FILIAL') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_FILIAL', FEntidadeBase.GetAsInteger('ID_FILIAL'));
  end;

  if FEntidadeBase.GetAsInteger('ID_PESSOA') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_PESSOA', FEntidadeBase.GetAsInteger('ID_PESSOA'));
  end;

  if FEntidadeBase.GetAsInteger('ID_PLANO_PAGAMENTO') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_PLANO_PAGAMENTO', FEntidadeBase.GetAsInteger('ID_PLANO_PAGAMENTO'));
  end;

  if FEntidadeBase.GetAsInteger('ID_FORMA_PAGAMENTO') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_FORMA_PAGAMENTO', FEntidadeBase.GetAsInteger('ID_FORMA_PAGAMENTO'));
  end;    

  if FEntidadeBase.GetAsInteger('ID_VENDEDOR') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_PESSOA_USUARIO', FEntidadeBase.GetAsInteger('ID_VENDEDOR'));
  end;

  if FEntidadeBase.GetAsInteger('ID_OPERACAO_FISCAL') > 0 then
  begin
    FNotaFiscal.SetAsInteger('ID_OPERACAO_FISCAL', FEntidadeBase.GetAsInteger('ID_OPERACAO_FISCAL'));
  end;
end;

procedure TIntegracaoFiscalServ.ValidarChavesEstrangeiras;
begin
  if FEntidadeBase.GetAsInteger('ID_CENTRO_RESULTADO') <= 0 then
  begin
    FInconsistencias.Add('Centro de Resultado não informado.');
  end;

  if FEntidadeBase.GetAsInteger('ID_CONTA_ANALISE') <= 0 then
  begin
    FInconsistencias.Add('Conta de Análise não informada.');
  end;

  if FEntidadeBase.GetAsInteger('ID_CHAVE_PROCESSO') <= 0 then
  begin
    FInconsistencias.Add('Chave de Processo não informada.');
  end;

  if FEntidadeBase.GetAsInteger('ID_OPERACAO') <= 0 then
  begin
    FInconsistencias.Add('Operação não informada.');
  end;

  if FEntidadeBase.GetAsInteger('ID_FILIAL') <= 0 then
  begin
    FInconsistencias.Add('Filial não informada.');
  end;

  if FEntidadeBase.GetAsInteger('ID_PESSOA') <= 0 then
  begin
    FInconsistencias.Add('Pessoa não informada.');
  end;

  if FEntidadeBase.GetAsInteger('ID_PLANO_PAGAMENTO') <= 0 then
  begin
    FInconsistencias.Add('Plano de Pagamento não informado.');
  end;

  if FEntidadeBase.GetAsInteger('ID_FORMA_PAGAMENTO') <= 0 then
  begin
    FInconsistencias.Add('Forma de Pagamento não informada.');
  end;    

  if FEntidadeBase.GetAsInteger('ID_VENDEDOR') <= 0 then
  begin
    FInconsistencias.Add('Funcionário não informado.');
  end;

  if FEntidadeBase.GetAsInteger('ID_OPERACAO_FISCAL') <= 0 then
  begin
    FInconsistencias.Add('Operação Fiscal não informada.');
  end;
end;

procedure TIntegracaoFiscalServ.AtribuirFinanceiroNotaFiscal;
begin
  FNotaFiscal.SetAsDateTime('DT_COMPETENCIA', FEntidadeBase.GetAsDateTime('DH_FECHAMENTO'));

  if FEntidadeBaseParcela.IsEmpty then
  begin
    FNotaFiscal.SetAsString('FORMA_PAGAMENTO', TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA);
  end
  else
  begin
    FNotaFiscal.SetAsString('FORMA_PAGAMENTO', TNotaFiscalProxy.FORMA_PAGAMENTO_PRAZO);
  end;
end;

procedure TIntegracaoFiscalServ.AtribuirTotais;
begin
  FNotaFiscal.SetAsFloat('W_VPROD', 0);
  FNotaFiscal.SetAsFloat('W_VST', 0);
  FNotaFiscal.SetAsFloat('W_VSEG', 0);
  FNotaFiscal.SetAsFloat('W_VDESC', 0);
  FNotaFiscal.SetAsFloat('W_VOUTRO', 0);
  FNotaFiscal.SetAsFloat('W_VIPI', 0);
  FNotaFiscal.SetAsFloat('W_VFRETE', 0);
  FNotaFiscal.SetAsFloat('W_VNF', 0);
  FNotaFiscal.SetAsFloat('W_VBC', 0);
  FNotaFiscal.SetAsFloat('W_VICMS', 0);
  FNotaFiscal.SetAsFloat('W_VBCST', 0);
  FNotaFiscal.SetAsFloat('W_VII', 0);
  FNotaFiscal.SetAsFloat('W_VPIS', 0);
  FNotaFiscal.SetAsFloat('W_VCOFINS', 0);
  FNotaFiscal.SetAsFloat('W_VRETPIS', 0);
  FNotaFiscal.SetAsFloat('W_VRETCOFINS', 0);
  FNotaFiscal.SetAsFloat('W_VRETCSLL', 0);
  FNotaFiscal.SetAsFloat('W_VBCIRRF', 0);
  FNotaFiscal.SetAsFloat('W_VIRRF', 0);
  FNotaFiscal.SetAsFloat('W_VBCRETPREV', 0);
  FNotaFiscal.SetAsFloat('W_VRETPREV', 0);
  FNotaFiscal.SetAsFloat('PERC_ICMS', 0);
  FNotaFiscal.SetAsFloat('PERC_PIS', 0);
  FNotaFiscal.SetAsFloat('PERC_COFINS', 0);
  FNotaFiscal.SetAsFloat('PERC_VST', 0);
  FNotaFiscal.SetAsFloat('PERC_VSEG', 0);
  FNotaFiscal.SetAsFloat('PERC_VDESC', 0);
  FNotaFiscal.SetAsFloat('PERC_VOUTRO', 0);
  FNotaFiscal.SetAsFloat('PERC_VIPI', 0);
  FNotaFiscal.SetAsFloat('PERC_VFRETE', 0);
end;

procedure TIntegracaoFiscalServ.AtribuirRetornoSefaz;
begin
  FNotaFiscal.SetAsString('NFE_CHAVE', '');
  FNotaFiscal.SetAsString('AR_VERSAO', '');
  FNotaFiscal.SetAsString('AR_TP_AMB', '');
  FNotaFiscal.SetAsString('AR_VERAPLIC', '');
  FNotaFiscal.SetAsString('AR_CSTAT', TNotaFiscalProxy.STATUS_NFE_AENVIAR);
  FNotaFiscal.SetAsString('AR_XMOTIVO', '');
  FNotaFiscal.SetAsString('AR_CUF', '');
  FNotaFiscal.SetAsString('AR_DHRECBTO', '');
  FNotaFiscal.SetAsString('AR_INFREC', '');
  FNotaFiscal.SetAsString('AR_NREC', '');
  FNotaFiscal.SetAsString('AR_TMED', '');
  FNotaFiscal.SetAsString('AR_PROTNFE', '');	
  FNotaFiscal.SetAsString('BO_ENVIO_DPEC', 'N');
  FNotaFiscal.SetAsString('NR_REG_DPEC', '');
  FNotaFiscal.SetAsString('DH_REG_DPEC', '');
end;	

procedure TIntegracaoFiscalServ.ConcluirInclusaoNotaFiscal;
begin
  FNotaFiscal.Persistir;
  FIdNotaFiscal := FNotaFiscal.GetAsInteger('ID');
end;	

procedure TIntegracaoFiscalServ.IncluirNotaFiscalItem;
var
  produtoFiscal: TProdutoFiscalProxy;
begin
  FEntidadeBaseItem.First;
  while not FEntidadeBaseItem.Eof do
  begin
    try
      produtoFiscal := TProdutoServ.GetProdutoFiscal(FEntidadeBaseItem.GetAsinteger('ID_PRODUTO'),
        FNotaFiscal.GetAsinteger('ID_PESSOA'), FNotaFiscal.GetAsinteger('ID_FILIAL'));
      try
        IniciarInclusaoItens;
        AtribuirInformacaoGeralItem(produtoFiscal);
        AtribuirValoresItem(produtoFiscal);
        AtribuirICMSItem(produtoFiscal);
        AtribuirPISCofinsItem(produtoFiscal);
        AtribuirIPIItem(produtoFiscal);
      finally
        FreeAndNil(produtoFiscal);
      end;

      CalcularValorTotalItem;

      SalvarInclusaoItens;
      FEntidadeBaseItem.Next;
    except
      on e : Exception do
      begin
        raise Exception.Create('Erro ao gerar Itens da Nota Fiscal.'+
          'Mensagem Original: '+e.message);
      end;
    end;
  end;
  PersistirInclusaoItens;
end;

procedure TIntegracaoFiscalServ.IniciarInclusaoItens;
begin	
  FNotaFiscalItem.Incluir;
end;

procedure TIntegracaoFiscalServ.AtribuirInformacaoGeralItem(const AProdutoFiscal: TProdutoFiscalProxy);
begin
	FNotaFiscalItem.SetAsInteger('H_NITEM', FNotaFiscalItem.RecordCount+1);

	if FEntidadeBaseItem.GetAsInteger('ID_PRODUTO') > 0 then
	begin
	  FNotaFiscalItem.SetAsInteger('ID_PRODUTO', FEntidadeBaseItem.GetAsInteger('ID_PRODUTO'));
	end;

	if FNotaFiscal.GetAsInteger('ID') > 0 then
	begin
	  FNotaFiscalItem.SetAsInteger('ID_NOTA_FISCAL', FNotaFiscal.GetAsInteger('ID'));
	end;

	FNotaFiscalItem.SetAsString('I_XPROD', AProdutoFiscal.I_XPROD);
	FNotaFiscalItem.SetAsString('I_NCM', AProdutoFiscal.I_NCM);
	FNotaFiscalItem.SetAsString('I_EXTIPI', '');
	FNotaFiscalItem.SetAsString('I_CFOP', AProdutoFiscal.I_CFOP);
	FNotaFiscalItem.SetAsString('I_CEANTRIB', AProdutoFiscal.I_CEANTRIB);
	FNotaFiscalItem.SetAsString('I_UTRIB', AProdutoFiscal.I_UTRIB);
	FNotaFiscalItem.SetAsInteger('I_INDTOT', AProdutoFiscal.I_INDTOT);
	FNotaFiscalItem.SetAsString('I_NVE', '');
	FNotaFiscalItem.SetAsString('I_CEAN', AProdutoFiscal.I_CEAN);
	FNotaFiscalItem.SetAsString('I_UCOM', AProdutoFiscal.I_UCOM);
	FNotaFiscalItem.SetAsFloat('I_VPROD', AProdutoFiscal.I_VPROD);
	FNotaFiscalItem.SetAsString('I_XPED', AProdutoFiscal.I_XPED);
	FNotaFiscalItem.SetAsInteger('I_NITEMPED', AProdutoFiscal.I_NITEMPED);
end;

procedure TIntegracaoFiscalServ.AtribuirValoresItem(const AProdutoFiscal: TProdutoFiscalProxy);
begin
	FNotaFiscalItem.SetAsFloat('I_QTRIB', AProdutoFiscal.I_QTRIB);
	FNotaFiscalItem.SetAsFloat('I_VUNTRIB', AProdutoFiscal.I_VUNTRIB);
	FNotaFiscalItem.SetAsFloat('I_QCOM', FEntidadeBaseItem.GetAsFloat('QUANTIDADE'));
	FNotaFiscalItem.SetAsFloat('I_VUNCOM', FEntidadeBaseItem.GetAsFloat('VL_BRUTO'));
	FNotaFiscalItem.SetAsFloat('I_VDESC', FEntidadeBaseItem.GetAsFloat('QUANTIDADE') *
    FEntidadeBaseItem.GetAsFloat('VL_DESCONTO'));
	FNotaFiscalItem.SetAsFloat('I_VFRETE', 0);
	FNotaFiscalItem.SetAsFloat('I_VSEG', 0);  //Muita atenção com o desconto e acrescimo tem q ver se estão unitarios os totalizados
	FNotaFiscalItem.SetAsFloat('I_VOUTRO', FEntidadeBaseItem.GetAsFloat('VL_ACRESCIMO'));
	FNotaFiscalItem.SetAsFloat('VL_LIQUIDO', FEntidadeBaseItem.GetAsFloat('VL_LIQUIDO'));
	FNotaFiscalItem.SetAsFloat('VL_CUSTO_IMPOSTO', 0);
	FNotaFiscalItem.SetAsFloat('PERC_DESCONTO', FEntidadeBaseItem.GetAsFloat('PERC_DESCONTO'));
	FNotaFiscalItem.SetAsFloat('PERC_ACRESCIMO', FEntidadeBaseItem.GetAsFloat('PERC_ACRESCIMO'));
	FNotaFiscalItem.SetAsFloat('PERC_FRETE', 0);
	FNotaFiscalItem.SetAsFloat('PERC_SEGURO', 0);
	FNotaFiscalItem.SetAsFloat('VL_CUSTO', TProdutoFilialServ.BuscarValorCusto(
	  FEntidadeBaseItem.GetAsinteger('ID_PRODUTO'), FNotaFiscal.GetAsinteger('ID_FILIAL')));
	FNotaFiscalItem.SetAsFloat('VL_CUSTO_OPERACIONAL', FNotaFiscalItem.GetAsFloat('VL_CUSTO'));
	FNotaFiscalItem.SetAsFloat('PERC_CUSTO_OPERACIONAL', 0);
end;

procedure TIntegracaoFiscalServ.AtribuirICMSItem(const AProdutoFiscal: TProdutoFiscalProxy);
begin
	if AProdutoFiscal.ID_IMPOSTO_ICMS > 0 then
	begin
	  FNotaFiscalItem.SetAsInteger('ID_IMPOSTO_ICMS', AProdutoFiscal.ID_IMPOSTO_ICMS);
	end;

	FNotaFiscalItem.SetAsString('N_ORIG', AProdutoFiscal.N_ORIG);
	FNotaFiscalItem.SetAsString('N_CST', Trim(AProdutoFiscal.N_CST));
	FNotaFiscalItem.SetAsString('N_MODBC', AProdutoFiscal.N_MODBC);
	FNotaFiscalItem.SetAsFloat('N_PREDBC', AProdutoFiscal.N_PREDBC);
	FNotaFiscalItem.SetAsFloat('N_VBC', AProdutoFiscal.N_VBC);
	FNotaFiscalItem.SetAsFloat('N_PICMS', AProdutoFiscal.N_PICMS);
	FNotaFiscalItem.SetAsFloat('N_VICMSOP', AProdutoFiscal.N_VICMSOP);
	FNotaFiscalItem.SetAsString('N_UFST', AProdutoFiscal.N_UFST);
	FNotaFiscalItem.SetAsFloat('N_PDIF', AProdutoFiscal.N_PDIF);
	FNotaFiscalItem.SetAsFloat('N_VLICMSDIF', AProdutoFiscal.N_VLICMSDIF);
	FNotaFiscalItem.SetAsFloat('N_VICMSDESON', AProdutoFiscal.N_VICMSDESON);
	FNotaFiscalItem.SetAsFloat('N_PBCOP', AProdutoFiscal.N_PBCOP);
	FNotaFiscalItem.SetAsFloat('N_BCSTRET', AProdutoFiscal.N_BCSTRET);
	FNotaFiscalItem.SetAsFloat('N_PCREDSN', AProdutoFiscal.N_PCREDSN);
	FNotaFiscalItem.SetAsFloat('N_VBCSTDEST', AProdutoFiscal.N_VBCSTDEST);
	FNotaFiscalItem.SetAsFloat('N_VICMSSTDEST', AProdutoFiscal.N_VICMSSTDEST);
	FNotaFiscalItem.SetAsFloat('N_VCREDICMSSN', AProdutoFiscal.N_VCREDICMSSN);
	FNotaFiscalItem.SetAsFloat('N_VICMS', 0);
	FNotaFiscalItem.SetAsFloat('N_VBCST', 0);
	FNotaFiscalItem.SetAsFloat('N_PREDBCST', 0);
	FNotaFiscalItem.SetAsString('N_MOTDESICMS', '');
	FNotaFiscalItem.SetAsFloat('N_PMVAST', 0);
	FNotaFiscalItem.SetAsString('N_MODBCST', AProdutoFiscal.N_MODBCST);
	FNotaFiscalItem.SetAsFloat('N_VICMSSTRET', AProdutoFiscal.N_VICMSSTRET);
	FNotaFiscalItem.SetAsFloat('N_NVICMSST', 0);
	FNotaFiscalItem.SetAsFloat('PERC_ICMS_ST', 0);     
end;

procedure TIntegracaoFiscalServ.AtribuirPISCofinsItem(const AProdutoFiscal: TProdutoFiscalProxy);
begin
	FNotaFiscalItem.SetAsString('Q_CST', AProdutoFiscal.Q_CST);
	FNotaFiscalItem.SetAsFloat('Q_VBC', AProdutoFiscal.Q_VBC);
	FNotaFiscalItem.SetAsFloat('Q_PPIS', AProdutoFiscal.Q_PPIS);
	FNotaFiscalItem.SetAsFloat('Q_VPIS', AProdutoFiscal.Q_VPIS);
	FNotaFiscalItem.SetAsFloat('Q_QBCPROD', AProdutoFiscal.Q_BCPROD);
	FNotaFiscalItem.SetAsFloat('Q_VALIQPROD', AProdutoFiscal.Q_VALIQPROD);

	FNotaFiscalItem.SetAsString('S_CST', AProdutoFiscal.S_CST);
	FNotaFiscalItem.SetAsFloat('S_VBC', AProdutoFiscal.S_VBC);
	FNotaFiscalItem.SetAsFloat('S_PCOFINS', AProdutoFiscal.S_PCOFINS);
	FNotaFiscalItem.SetAsFloat('S_QBCPROD', AProdutoFiscal.S_QBCPROD);
	FNotaFiscalItem.SetAsFloat('S_VALIQPROD', AProdutoFiscal.S_VALIQPROD);
	FNotaFiscalItem.SetAsFloat('S_VCOFINS', AProdutoFiscal.S_VCOFINS);
end;

procedure TIntegracaoFiscalServ.AtribuirIPIItem(const AProdutoFiscal: TProdutoFiscalProxy);
begin
	FNotaFiscalItem.SetAsString('O_CLENQ', AProdutoFiscal.O_CLENQ);
	FNotaFiscalItem.SetAsString('O_CNPJPROD', AProdutoFiscal.O_CNPJPROD);
	FNotaFiscalItem.SetAsString('O_CSELO', AProdutoFiscal.O_CSELO);
	FNotaFiscalItem.SetAsFloat('O_QSELO', AProdutoFiscal.O_QSELO);
	FNotaFiscalItem.SetAsString('O_CENQ', AProdutoFiscal.O_CENQ);
	FNotaFiscalItem.SetAsString('O_CST', AProdutoFiscal.O_CST);
	FNotaFiscalItem.SetAsFloat('O_VBC', AProdutoFiscal.O_VBC);
	FNotaFiscalItem.SetAsFloat('O_QUNID', AProdutoFiscal.O_QUNID);
	FNotaFiscalItem.SetAsFloat('O_VUNID', AProdutoFiscal.O_VUNID);
	FNotaFiscalItem.SetAsFloat('O_PIPI', AProdutoFiscal.O_PIPI);
	FNotaFiscalItem.SetAsFloat('O_VIPI', 0);        
	FNotaFiscalItem.SetAsFloat('PERC_IPI', 0);
end;

procedure TIntegracaoFiscalServ.CalcularValorTotalItem;
var
  impostoICMS: TImpostoICMSProxy;
  impostoIPI: TImpostoIPIProxy;
  impostoPisCofins: TImpostoPisCofinsProxy;
begin
  impostoICMS := nil;
  impostoIPI := nil;
  impostoPisCofins := nil;

  if FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_ICMS') > 0 then
  begin
    impostoICMS := TImpostoICMSServ.GetImpostoIcms(FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_ICMS'));
  end;

  if FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_IPI') > 0 then
  begin
    impostoIPI := TImpostoIPIServ.GetImpostoIPI(FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_IPI'));
  end;

  if FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_PIS_COFINS') > 0 then
  begin
    impostoPisCofins := TImpostoPisCofinsServ.GetImpostoPISCofins(
      FNotaFiscalItem.GetAsInteger('ID_IMPOSTO_PIS_COFINS'));
  end;

  try
    FMotorCalculoNotaFiscal.CalcularValorTotalItem(impostoICMS, impostoIPI, impostoPisCofins);
  finally
    if Assigned(impostoICMS) then
    begin
      FreeAndNil(impostoICMS);
    end;

    if Assigned(impostoIPI) then
    begin
      FreeAndNil(impostoIPI);
    end;

    if Assigned(impostoPisCofins) then
    begin
      FreeAndNil(impostoPisCofins);
    end;
  end;
end;

procedure TIntegracaoFiscalServ.SalvarInclusaoItens;
begin
  FNotaFiscalItem.Salvar;
end;

procedure TIntegracaoFiscalServ.PersistirInclusaoItens;
begin
  FNotaFiscalItem.Persistir;
end;	

procedure TIntegracaoFiscalServ.CalcularValorTotalNotaFiscal;
var
  idNotaFiscal: Integer;
begin
  idNotaFiscal := FNotaFiscal.GetAsInteger('ID');

  FNotaFiscal.AtualizarConsultaSQL('select * from nota_fiscal where id = :id',
    TArray<Variant>.Create(idNotaFiscal));

  FNotaFiscal.Alterar;
  FMotorCalculoNotaFiscal.CalcularValorNotaFiscal;

  if (FEntidadeBase.GetAsFloat('VL_PAGAMENTO') > 0) and (FEntidadeBaseParcela.IsEmpty) then
  begin
    FNotaFiscal.SetAsFloat('VL_QUITACAO_DINHEIRO',FNotaFiscal.GetAsFloat('W_VNF'));
  end
  else
  begin
    FNotaFiscal.SetAsFloat('VL_QUITACAO_DINHEIRO', FEntidadeBase.GetAsFloat('VL_PAGAMENTO'));
  end;

  FNotaFiscal.Persistir;
end;

procedure TIntegracaoFiscalServ.IncluirNotaFiscalParcela;
begin
if not FEntidadeBaseParcela.IsEmpty then
  begin
    FEntidadeBaseParcela.First;
    while not FEntidadeBaseParcela.Eof do
    begin
      try
        IniciarInclusaoParcelas;
        AtribuirParcela;
        SalvarInclusaoParcelas;
        FEntidadeBaseParcela.Next;
      except
        on e : Exception do
        begin
          raise Exception.Create('Erro ao gerar as parcelas da nota fiscal.'+
            'Mensagem Original: '+e.message);
        end;
      end;
    end;

    PersistirInclusaoParcelas;
  end;
end;

procedure TIntegracaoFiscalServ.IniciarInclusaoParcelas;
begin
  FNotaFiscalParcela.Incluir; 
end;

procedure TIntegracaoFiscalServ.AtribuirParcela;
begin
	FNotaFiscalParcela.SetAsString('DOCUMENTO', FEntidadeBaseParcela.GetAsString('DOCUMENTO'));
	FNotaFiscalParcela.SetAsFloat('VL_TITULO', FEntidadeBaseParcela.GetAsFloat('VL_TITULO'));
	FNotaFiscalParcela.SetAsInteger('QT_PARCELA', FEntidadeBaseParcela.GetAsInteger('QT_PARCELA'));
	FNotaFiscalParcela.SetAsInteger('NR_PARCELA', FEntidadeBaseParcela.GetAsInteger('NR_PARCELA'));
	FNotaFiscalParcela.SetAsDate('DT_VENCIMENTO', FEntidadeBaseParcela.GetAsDate('DT_VENCIMENTO'));

	if FNotaFiscal.GetAsInteger('ID') > 0 then
	begin
	  FNotaFiscalParcela.SetAsInteger('ID_NOTA_FISCAL', FNotaFiscal.GetAsInteger('ID'));
	end;

	FNotaFiscalParcela.SetAsString('OBSERVACAO', FEntidadeBaseParcela.GetAsString('OBSERVACAO'));
	FNotaFiscalParcela.SetAsString('CHEQUE_SACADO', FEntidadeBaseParcela.GetAsString('CHEQUE_SACADO'));
	FNotaFiscalParcela.SetAsString('CHEQUE_DOC_FEDERAL', FEntidadeBaseParcela.GetAsString('CHEQUE_DOC_FEDERAL'));
	FNotaFiscalParcela.SetAsString('CHEQUE_BANCO', FEntidadeBaseParcela.GetAsString('CHEQUE_BANCO'));
	FNotaFiscalParcela.SetAsString('CHEQUE_DT_EMISSAO', FEntidadeBaseParcela.GetAsString('CHEQUE_DT_EMISSAO'));
	FNotaFiscalParcela.SetAsString('CHEQUE_AGENCIA', FEntidadeBaseParcela.GetAsString('CHEQUE_AGENCIA'));
	FNotaFiscalParcela.SetAsString('CHEQUE_CONTA_CORRENTE', FEntidadeBaseParcela.GetAsString('CHEQUE_CONTA_CORRENTE'));
	FNotaFiscalParcela.SetAsInteger('CHEQUE_NUMERO', FEntidadeBaseParcela.GetAsInteger('CHEQUE_NUMERO'));
	FNotaFiscalParcela.SetAsString('CHEQUE_DT_VENCIMENTO', FEntidadeBaseParcela.GetAsString('CHEQUE_DT_VENCIMENTO'));

	if FEntidadeBaseParcela.GetAsInteger('ID_OPERADORA_CARTAO') > 0 then
	begin
	  FNotaFiscalParcela.SetAsInteger('ID_OPERADORA_CARTAO', FEntidadeBaseParcela.GetAsInteger('ID_OPERADORA_CARTAO'));
	end;

	if FEntidadeBaseParcela.GetAsInteger('ID_FORMA_PAGAMENTO') > 0 then
	begin
	  FNotaFiscalParcela.SetAsInteger('ID_FORMA_PAGAMENTO', FEntidadeBaseParcela.GetAsInteger('ID_FORMA_PAGAMENTO'));
	end;

	if FEntidadeBaseParcela.GetAsInteger('ID_CARTEIRA') > 0 then
	begin
	  FNotaFiscalParcela.SetAsInteger('ID_CARTEIRA', FEntidadeBaseParcela.GetAsInteger('ID_CARTEIRA'));
	end;
end;

procedure TIntegracaoFiscalServ.SalvarInclusaoParcelas;
begin
  FNotaFiscalParcela.Salvar;
end;

procedure TIntegracaoFiscalServ.PersistirInclusaoParcelas;
begin
  FNotaFiscalParcela.Persistir;
end;	

function TIntegracaoFiscalServ.RealizarIntegracaoFiscal: TRetornoDocumentoFiscalProxy;
begin
  result := TRetornoDocumentoFiscalProxy.Create;
  try
    result.FRetorno := false;

    if not ValidarDocumentoFiscalJaEmitido then
    begin
      InstanciarEntidadeOrigem;
      InstanciarNotaFiscal;
      InstanciarMotorCalculo;
      IncluirNotaFiscal;
      IncluirNotaFiscalItem;
      IncluirNotaFiscalParcela;
      CalcularValorTotalNotaFiscal;
      AtualizarCodigoNaEntidadeBase;
      GerarContasReceber;
    end;
    result.FRetorno := true;
  finally
    result.FMensagensCommaText := FInconsistencias.CommaText;
  end;  
end;

procedure TIntegracaoFiscalServ.RemoverNotaFiscalIntegracaoFiscal;
begin
  TNotaFiscalServ.RemoverNotaFiscal(FIdNotaFiscal);
end;


end.