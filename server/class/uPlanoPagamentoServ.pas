unit uPlanoPagamentoServ;

interface

Uses FireDAC.Comp.Client, Data.FireDACJSONReflect, SysUtils, StrUtils, Controls, System.Classes, uFactoryQuery, System.Variants;

type TPlanoPagamentoServ = class
  public
  class function GetPlanoPagamento(AIdPlanoPagamento: Integer): TFDJSONDataSets;
  class function GetDescricaoPlanoPagamento(const AID: Integer): String;
end;

implementation

{ TPlanoPagamentoServ }

class function TPlanoPagamentoServ.GetDescricaoPlanoPagamento(const AID: Integer): String;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT descricao FROM PLANO_PAGAMENTO WHERE id = :id',
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsString('descricao');
end;

class function TPlanoPagamentoServ.GetPlanoPagamento(
  AIdPlanoPagamento: Integer): TFDJSONDataSets;
var qPlanoPagamento: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qPlanoPagamento := TFastQuery.ModoDeConsulta(
    'SELECT * FROM PLANO_PAGAMENTO WHERE id = :id', TArray<Variant>.Create(AIdPlanoPagamento));

  TFDJSONDataSetsWriter.ListAdd(result, qPlanoPagamento as TFastQuery);
end;

end.
