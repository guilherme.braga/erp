unit uImpostoIpiServ;

Interface

Uses uImpostoIpiProxy;

type TImpostoIpiServ = class
  class function GetImpostoIpi(
    const AIdImpostoIpi: Integer): TImpostoIpiProxy;
end;

implementation

{ TImpostoIpiServ }

uses uFactoryQuery;

class function TImpostoIpiServ.GetImpostoIpi(
    const AIdImpostoIpi: Integer): TImpostoIpiProxy;
const
  SQL = 'SELECT * FROM imposto_ipi WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TImpostoIpiProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdImpostoIpi));

  result.FId := qConsulta.GetAsString('ID');
  result.FPercAliquota := qConsulta.GetAsFloat('PERC_ALIQUOTA');
  result.FIdCstIpi := qConsulta.GetAsInteger('ID_CST_IPI');
end;

end.
