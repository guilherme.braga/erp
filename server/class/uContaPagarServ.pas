unit uContaPagarServ;

interface

Uses
  FireDAC.Comp.Client,
  uGBFDQuery,
  System.Generics.Collections,
  SysUtils,
  Data.FireDACJSONReflect;

type TContaPagarServ = class
  class function GerarContaPagar(AContaPagar: String): Integer;
  class function PodeRemover(AChaveProcesso: Integer): Boolean;
  class function RemoverContaPagar(AChaveProcesso : Integer): Boolean;
  class function ExisteContaContaPagar(AIdChaveProcesso: Integer): Boolean;
  class function ExisteQuitacao(AIdChaveProcesso : Integer): Boolean;
  class function AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso: Integer;
    AGerarContraPartida: Boolean): Boolean; static;
  class function RemoverMovimentosContaCorrente(
      AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean; static;
  class function GerarContaPagarQuitacao(AContaPagarQuitacao: String): Integer;
  class function AtualizarValores(AIdContaPagar: Integer): Boolean;
  class function AtualizarContaPagar(AContaPagar: String): Boolean; static;
  class function PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Integer;
  class function ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Integer;
  class function AtualizarContaPagarQuitacao(
    AContaPagarQuitacao: String): Boolean; static;
  class function GetIdContaPagar(AIdBaixa: Integer): Integer;
  class function RemoverContaPagarQuitacao(AChaveProcesso: Integer): Boolean; static;
  class function RemoverContaPagarQuitacaoPorID(AId: Integer): Boolean; static;
  class function GetIdContaPagarPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
  class function GetValorMulta(const AIdContaPagar: Integer): Double;
  class function GetValorJuros(const AIdContaPagar: Integer): Double;
  class function GetValorAberto(const AIdContaPagar: Integer): Double;
  class function TotalQuitacaoRealizada(AIdChaveProcesso : Integer): Double;

  class function GetTotaisContaPagar(const AIdsContaPagar: String): TFDJSONDatasets;
end;


implementation

uses
  uDatasetUtilsServ,
  uFactoryQuery,
  uContaPagarProxy,
  REST.Json,
  uContaCorrenteProxy,
  uContaCorrenteServ, uFormaPagamentoServ, uFormaPagamentoProxy,
  uTipoQuitacaoProxy, uTipoQuitacaoServ;

{ TContaPagarServ }

class function TContaPagarServ.ExisteContaContaPagar(
  AIdChaveProcesso: Integer): Boolean;
var qContaPagar: IFastQuery;
begin
  result := true;

  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   from conta_pagar'+
    '  where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := not qContaPagar.IsEmpty;
end;

class function TContaPagarServ.ExisteQuitacao(
  AIdChaveProcesso: Integer): Boolean;
var
  qContaPagar: IFastQuery;
begin
  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   from conta_pagar'+
    '  where id_chave_processo = :id_chave_processo'+
    '    and exists(select 1'+
    '                     from conta_pagar_quitacao'+
    '                    where id_conta_pagar = conta_pagar.id)',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := not qContaPagar.IsEmpty;
end;

class function TContaPagarServ.GerarContaPagar(AContaPagar: String): Integer;
var
  fqContaPagar : IFastQuery;
  ContaPagar   : TContaPagarMovimento;
begin
  result := 0;

  ContaPagar := TJson.JsonToObject<TContaPagarMovimento>(AContaPagar);

  fqContaPagar := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_PAGAR WHERE 1 = 2' , nil);
  fqContaPagar.Incluir;

  fqContaPagar.SetAsInteger('ID_PESSOA', ContaPagar.FIdPessoa);

  fqContaPagar.SetAsString('DOCUMENTO', ContaPagar.FDocumento);
  fqContaPagar.SetAsString('DESCRICAO', ContaPagar.FDescricao);
  fqContaPagar.SetAsString('STATUS', ContaPagar.FStatus);

  fqContaPagar.SetAsDateTime('DH_CADASTRO', StrToDateTime(ContaPagar.FDhCadastro));
  fqContaPagar.SetAsDateTime('DT_VENCIMENTO', StrToDate(ContaPagar.FDtVencimento));
  fqContaPagar.SetAsDateTime('DT_COMPETENCIA',
    StrToDateDef(ContaPagar.FDtCompetencia, StrToDate(ContaPagar.FDtVencimento)));
  fqContaPagar.SetAsDateTime('DT_DOCUMENTO', StrToDate(ContaPagar.FDtDocumento));

  fqContaPagar.SetAsFloat('QT_PARCELA', ContaPagar.FQtParcela);
  fqContaPagar.SetAsFloat('NR_PARCELA', ContaPagar.FNrParcela);

  fqContaPagar.SetAsFloat('VL_TITULO', ContaPagar.FVlTitulo);
  fqContaPagar.SetAsFloat('VL_TITULO_ORIGINAL_HISTORICO', ContaPagar.FVlTituloOriginalHistorico);
  fqContaPagar.SetAsFloat('VL_QUITADO', ContaPagar.FVlQuitado);
  fqContaPagar.SetAsFloat('VL_ABERTO', ContaPagar.FVlAberto);

  fqContaPagar.SetAsFloat('VL_ACRESCIMO', ContaPagar.FVlAcrescimo);
  fqContaPagar.SetAsFloat('VL_DECRESCIMO', ContaPagar.FVlDecrescimo);
  fqContaPagar.SetAsFloat('VL_QUITADO_LIQUIDO', ContaPagar.FVlQuitadoLiquido);

  fqContaPagar.SetAsFloat('VL_MULTA', ContaPagar.FVlMulta);
  fqContaPagar.SetAsFloat('VL_JUROS', ContaPagar.FVlJuros);
  fqContaPagar.SetAsFloat('PERC_MULTA', ContaPagar.FPercMulta);
  fqContaPagar.SetAsFloat('PERC_JUROS', ContaPagar.FPercJuros);

  fqContaPagar.SetAsString('SEQUENCIA', ContaPagar.FSequencia);
  fqContaPagar.SetAsString('BO_VENCIDO', ContaPagar.FBoVencido);
  fqContaPagar.SetAsString('OBSERVACAO', ContaPagar.FObservacao);

  if ContaPagar.FIdContaAnalise > 0 then
    fqContaPagar.SetAsInteger('ID_CONTA_ANALISE', ContaPagar.FIdContaAnalise);

  if ContaPagar.FIdCentroResultado > 0 then
    fqContaPagar.SetAsInteger('ID_CENTRO_RESULTADO', ContaPagar.FIdCentroResultado);

  if ContaPagar.FIdContaCorrente > 0 then
    fqContaPagar.SetAsInteger('ID_CONTA_CORRENTE', ContaPagar.FIdContaCorrente);

  if ContaPagar.FIdChaveProcesso > 0 then
    fqContaPagar.SetAsInteger('ID_CHAVE_PROCESSO', ContaPagar.FIdChaveProcesso);

  if ContaPagar.FIdFormaPagamento > 0 then
    fqContaPagar.SetAsInteger('ID_FORMA_PAGAMENTO', ContaPagar.FIdFormaPagamento);

  if ContaPagar.FIdFilial > 0 then
    fqContaPagar.SetAsInteger('ID_FILIAL', ContaPagar.FIdFilial);

  if ContaPagar.FIdDocumentoOrigem > 0 then
    fqContaPagar.SetAsInteger('ID_DOCUMENTO_ORIGEM', ContaPagar.FIdDocumentoOrigem);

  if ContaPagar.FIdCarteira > 0 then
    fqContaPagar.SetAsInteger('ID_CARTEIRA', ContaPagar.FIdCarteira);

  fqContaPagar.SetAsString('TP_DOCUMENTO_ORIGEM', ContaPagar.FTPDocumentoOrigem);

  fqContaPagar.Salvar;
  fqContaPagar.Persistir;

  result := fqContaPagar.GetAsInteger('ID');
end;

class function TContaPagarServ.PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Integer;
var qContaPagar: IFastQuery;
begin
  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT count(*) as qtde'+
    '   from conta_pagar'+
    '  where id_pessoa = :id_pessoa and status = :status',
    TArray<Variant>.Create(AIdPessoa, TContaPagarMovimento.ABERTO));

  result := qContaPagar.GetAsInteger('qtde');
end;

class function TContaPagarServ.PodeRemover(AChaveProcesso: Integer): Boolean;
var
  fqContaPagar : IFastQuery;
begin

  fqContaPagar := TFastQuery.ModoDeConsulta(
  '   SELECT cp.id                                        ' +
  '     FROM conta_pagar cp                               ' +
  '    WHERE cp.id_chave_processo = :id_chave_processo    ' +
  '      AND NOT EXISTS(SELECT 1                          ' +
  '                       FROM conta_pagar_quitacao cpq   ' +
  '                      WHERE cpq.id_conta_pagar = cp.id)',
  TArray<Variant>.Create(AChaveProcesso)
  );

  result := fqContaPagar.IsEmpty;

end;

class function TContaPagarServ.ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Integer;
var qContaPagar: IFastQuery;
begin
  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT count(*) as qtde'+
    '   from conta_pagar'+
    '  where id_chave_processo = :id_chave_processo and status = :status',
    TArray<Variant>.Create(AIdChaveProcesso, TContaPagarMovimento.ABERTO));

  result := qContaPagar.GetAsInteger('qtde');
end;

class function TContaPagarServ.RemoverContaPagar(
  AChaveProcesso: Integer): Boolean;
begin
  try
    Result := True;

    TFastQuery.ExecutarScriptIndependente(
          ' DELETE FROM conta_pagar_quitacao WHERE id_conta_pagar in '+
          ' (select id from conta_pagar WHERE id_chave_processo = :id_chave_processo)',
          TArray<Variant>.Create(AChaveProcesso));

    TFastQuery.ExecutarScript(
          'DELETE FROM conta_pagar WHERE id_chave_processo = :id_chave_processo',
          TArray<Variant>.Create(AChaveProcesso));
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaPagarServ.RemoverContaPagarQuitacao(
  AChaveProcesso: Integer): Boolean;
begin
  try
    Result := True;
    TFastQuery.ExecutarScript(
          'DELETE FROM conta_pagar_quitacao WHERE id_chave_processo = :id_chave_processo',
          TArray<Variant>.Create(AChaveProcesso));
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaPagarServ.RemoverContaPagarQuitacaoPorID(
  AId: Integer): Boolean;
begin
  try
    Result := True;
    TFastQuery.ExecutarScript(
          'DELETE FROM conta_pagar_quitacao WHERE id = :id',
          TArray<Variant>.Create(AId));
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaPagarServ.RemoverMovimentosContaCorrente(
  AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean;
var
  fqQuitacao : IFastQuery;
begin
  fqQuitacao := TFastQuery.ModoDeConsulta(
    '     SELECT ccm.id ' +
    '       FROM conta_corrente_movimento ccm ' +
    ' INNER JOIN conta_pagar_quitacao cpq ON ccm.id_documento_origem = cpq.id  ' +
    '      WHERE ccm.tp_documento_origem = ''CONTAPAGAR''  ' +
    '        AND cpq.id_conta_pagar    = :id_conta_pagar '
    ,TArray<Variant>.Create(AIdContaPagar));

  while not fqQuitacao.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqQuitacao.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqQuitacao.GetAsInteger('ID'));
    end;
    fqQuitacao.Proximo;
  end;

  Result := True;
end;

class function TContaPagarServ.AtualizarMovimentosContaCorrente(
  AIdContaPagar, AIdChaveProcesso: Integer; AGerarContraPartida: Boolean): Boolean;
var
  Movimento              : TContaCorrenteMovimento;
  fqContaPagar           : IFastQuery;
  fqContaPagarQuitacao   : IFastQuery;
  fqAtualizacao          : IFastQuery;
  fqRemocao              : IFastQuery;
  fqInsercao             : IFastQuery;
  MovimentoJSON          : string;
  tipoQuitacao           : String;
const
  DEBITO      : String = 'DEBITO';
  CONTAPAGAR  : string = 'CONTAPAGAR';
  NAO         : string = 'N';
  SIM         : string = 'S';
begin
  Result := True;

  if AIdContaPagar <= 0 then
  begin
    fqContaPagar := TFastQuery.ModoDeConsulta(
    'SELECT * FROM conta_pagar where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso)
    );
  end
  else
  begin
    fqContaPagar := TFastQuery.ModoDeConsulta(
    'SELECT * FROM conta_pagar where id = :id_conta_pagar',
    TArray<Variant>.Create(AIdContaPagar)
    );
  end;

  fqAtualizacao := TFastQuery.ModoDeConsulta(
  'SELECT ccm.id as id                    ' +
  '      ,cpq.dh_cadastro as dh_cadastro  ' +
  '      ,cpq.dt_quitacao as dt_quitacao  ' +
  '      ,cpq.vl_total  as vl_total       ' +
  '      ,cpq.id_conta_corrente as id_conta_corrente  ' +
  '  FROM conta_corrente_movimento ccm                ' +
  ' INNER JOIN conta_pagar_quitacao cpq ON ccm.id_documento_origem = cpq.id ' +
  '   AND ccm.tp_documento_origem = ''CONTAPAGAR''   ' +
  '   AND ccm.id_chave_processo = :id_chave_processo ' +
  '   AND cpq.id_conta_pagar = :id_conta_pagar       ' ,
  TArray<Variant>.Create(fqContaPagar.GetAsInteger('ID_CHAVE_PROCESSO')
                        ,fqContaPagar.GetAsInteger('ID'))
  );

  while not fqAtualizacao.Eof do
  begin
    Movimento := TContaCorrenteMovimento.Create;
    try

      Movimento.Fid                := fqAtualizacao.GetAsInteger('id');
      Movimento.FDhCadastro        := fqAtualizacao.GetAsString('dh_cadastro');
      Movimento.FDtEmissao         := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FDtMovimento       := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FDtCompetencia     := fqAtualizacao.GetAsString('dt_quitacao');
      Movimento.FVlMovimento       := fqAtualizacao.GetAsFloat('vl_total');
      Movimento.FIdContaCorrente   := fqAtualizacao.GetAsInteger('id_conta_corrente');

      result := TContaCorrenteServ.AtualizarMovimento(Movimento);

    finally
      Movimento.Free;
    end;
    fqAtualizacao.Proximo;
  end;

  fqInsercao := TFastQuery.ModoDeConsulta(
  'SELECT cpq.*                                                         ' +
  '  FROM conta_pagar_quitacao cpq                                    ' +
  ' WHERE cpq.id_conta_pagar = :id_conta_pagar                      ' +
  '   AND NOT EXISTS (SELECT 1                                          ' +
  '                     FROM conta_corrente_movimento ccm               ' +
  '                    WHERE ccm.id_chave_processo = :id_chave_processo ' +
  '                      AND ccm.id_documento_origem = cpq.id           ' +
  '                      AND ccm.tp_documento_origem = ''CONTAPAGAR'')'
  ,TArray<Variant>.Create(fqContaPagar.GetAsInteger('ID')
                         ,fqContaPagar.GetAsInteger('ID_CHAVE_PROCESSO'))
  );

  while not fqInsercao.Eof do
  begin
    Movimento := TContaCorrenteMovimento.Create;
    try
      Movimento.FDocumento              := fqContaPagar.GetAsString('DOCUMENTO');
      Movimento.FDescricao              := fqContaPagar.GetAsString('DESCRICAO');
      Movimento.FObservacao             := fqContaPagar.GetAsString('OBSERVACAO');
      Movimento.FDhCadastro             := fqInsercao.GetAsString('DH_CADASTRO');
      Movimento.FDtEmissao              := fqInsercao.GetAsString('DT_QUITACAO');
      Movimento.FDtMovimento            := fqInsercao.GetAsString('DT_QUITACAO');
      Movimento.FDtCompetencia          := fqInsercao.GetAsString('DT_QUITACAO');
      Movimento.FVlMovimento            := fqInsercao.GetAsFloat('VL_TOTAL');
      Movimento.FTpMovimento            := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

      //Se for cheque a concilia��o � falsa
      tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(fqInsercao.GetAsInteger('ID_TIPO_QUITACAO'));
      if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
        (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
      begin
        Movimento.FBoCheque := SIM;

        if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO then
        begin
          Movimento.FBoChequeTerceiro := SIM;
        end;

        if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO then
        begin
          Movimento.FBoChequeTerceiro := SIM;//Sempre � de terceiro qdo � conta a receber
        end;

        Movimento.FBoConciliado := NAO;

        //Dados do Cheque
        Movimento.FChequeSacado        := fqInsercao.GetAsString('CHEQUE_SACADO');
        Movimento.FChequeDocFederal    := fqInsercao.GetAsString('CHEQUE_DOC_FEDERAL');
        Movimento.FChequeBanco         := fqInsercao.GetAsString('CHEQUE_BANCO');
        Movimento.FChequeDtEmissao     := fqInsercao.GetAsString('CHEQUE_DT_EMISSAO');
        Movimento.FChequeDtVencimento  := fqInsercao.GetAsString('CHEQUE_DT_VENCIMENTO');
        Movimento.FChequeAgencia       := fqInsercao.GetAsString('CHEQUE_AGENCIA');
        Movimento.FChequeContaCorrente := fqInsercao.GetAsString('CHEQUE_CONTA_CORRENTE');
        Movimento.FChequeNumero        := fqInsercao.GetAsInteger('CHEQUE_NUMERO');
      end
      else
      begin
        Movimento.FBoConciliado := NAO;
      end;

      Movimento.FDhConciliado           := '';
      Movimento.FVlSaldoConciliado      := 0;
      Movimento.FVlSaldoGeral           := fqInsercao.GetAsFloat('VL_TOTAL');
      Movimento.FIdContaCorrente        := fqInsercao.GetAsInteger('ID_CONTA_CORRENTE');
      Movimento.FIdContaAnalise         := fqContaPagar.GetAsInteger('ID_CONTA_ANALISE');
      Movimento.FIdCentroResultado      := fqContaPagar.GetAsInteger('ID_CENTRO_RESULTADO');
      Movimento.FBoMovimentoOrigem      := NAO;
      Movimento.FIdMovimentoOrigem      := 0;
      Movimento.FBoMovimentoDestino     := NAO;
      Movimento.FIdMovimentoDestino     := 0;
      Movimento.FBoTransferencia        := NAO;
      Movimento.FIdContaCorrenteDestino := 0;
      Movimento.FTPDocumentoOrigem      := TContaCorrenteMovimento.ORIGEM_CONTA_PAGAR; //+ '('+fqInsercao.GetAsString('ID_CONTA_PAGAR')+')';
      Movimento.FIdDocumentoOrigem      := fqInsercao.GetAsInteger('ID');
      Movimento.FIdChaveProcesso        := fqContaPagar.GetAsInteger('ID_CHAVE_PROCESSO');
      Movimento.FIdFilial               := fqContaPagar.GetAsInteger('ID_FILIAL');
      Movimento.FParcelamento           := fqContaPagar.GetAsString('SEQUENCIA');

      //Dados do Cart�o
      Movimento.FIdOperadoraCartao   := fqInsercao.GetAsInteger('ID_OPERADORA_CARTAO');

      MovimentoJSON := TJson.ObjectToJsonString(Movimento);

      result := TContaCorrenteServ.GerarMovimentoContaCorrente(MovimentoJSON);
    finally
      Movimento.Free;
    end;
    fqInsercao.Proximo;
  end;

  fqRemocao := TFastQuery.ModoDeConsulta(
    '  SELECT ccm.*                                                            ' +
    '    FROM conta_corrente_movimento ccm                                     ' +
    '   WHERE ccm.id_chave_processo   = :id_chave_processo                     ' +
    '     AND ccm.tp_documento_origem = ''CONTAPAGAR'' and ccm.bo_processo_amortizado = ''N'' ' +
    '     AND NOT EXISTS ( SELECT 1                                            ' +
    '                        FROM conta_pagar_quitacao cpq                     ' +
    '                  INNER JOIN conta_pagar cp ON cpq.id_conta_pagar = cp.id ' +
    '                       WHERE cpq.id = ccm.id_documento_origem             ' +
    '                         AND cp.id_chave_processo = ccm.id_chave_processo ) '
    ,TArray<Variant>.Create(fqContaPagar.GetAsInteger('ID_CHAVE_PROCESSO'))
    );

  while not fqRemocao.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqRemocao.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqRemocao.GetAsInteger('ID'));
    end;

    fqRemocao.Proximo;
  end;
end;

class function TContaPagarServ.TotalQuitacaoRealizada(
  AIdChaveProcesso: Integer): Double;
var
  qContaPagar: IFastQuery;
begin
  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT SUM(vl_quitado) AS vl_quitado'+
    ' FROM conta_pagar'+
    ' WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qContaPagar.getAsFloat('vl_quitado');
end;

class function TContaPagarServ.GerarContaPagarQuitacao(AContaPagarQuitacao: String) : Integer;
var
  fqQuitacao : IFastQuery;
  Quitacao   : TContaPagarQuitacaoMovimento;
begin

  Quitacao := TJson.JsonToObject<TContaPagarQuitacaoMovimento>(AContaPagarQuitacao);

  fqQuitacao := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_PAGAR_QUITACAO WHERE 1 = 2' , nil);

  fqQuitacao.Incluir;

  fqQuitacao.SetAsInteger('NR_ITEM', Quitacao.FNrItem);
  fqQuitacao.SetAsInteger('ID_CONTA_PAGAR', Quitacao.FIdContaPagar);

  fqQuitacao.SetAsString('DH_CADASTRO', Quitacao.FDhCadastro);
  fqQuitacao.SetAsString('DT_QUITACAO', Quitacao.FDtQuitacao);
  fqQuitacao.SetAsString('OBSERVACAO', Quitacao.FObservacao);

  fqQuitacao.SetAsInteger('ID_DOCUMENTO_ORIGEM', Quitacao.FIdDocumentoOrigem);
  fqQuitacao.SetAsString('TP_DOCUMENTO_ORIGEM', Quitacao.FTPDocumentoOrigem);
  fqQuitacao.SetAsString('STATUS', Quitacao.FStatus);

  fqQuitacao.SetAsInteger('ID_FILIAL', Quitacao.FIdFilial);
  fqQuitacao.SetAsInteger('ID_USUARIO_BAIXA', Quitacao.FIdUsuarioBaixa);
  fqQuitacao.SetAsInteger('ID_TIPO_QUITACAO', Quitacao.FIdTipoQuitacao);
  fqQuitacao.SetAsInteger('ID_CONTA_CORRENTE', Quitacao.FIdContaCorrente);
  fqQuitacao.SetAsInteger('ID_CONTA_ANALISE', Quitacao.FIdContaAnalise);
  fqQuitacao.SetAsInteger('ID_CENTRO_RESULTADO', Quitacao.FIdCentroResultado);

  fqQuitacao.SetAsFloat('VL_DESCONTO', Quitacao.FVlDesconto);
  fqQuitacao.SetAsFloat('VL_ACRESCIMO', Quitacao.FVlAcrescimo);
  fqQuitacao.SetAsFloat('VL_QUITACAO', Quitacao.FVlQuitacao);
  fqQuitacao.SetAsFloat('VL_TOTAL', Quitacao.FVlTotal);
  fqQuitacao.SetAsFloat('PERC_ACRESCIMO', Quitacao.FPercAcrescimo);
  fqQuitacao.SetAsFloat('PERC_DESCONTO', Quitacao.FPercDesconto);

  fqQuitacao.SetAsString('CHEQUE_SACADO',Quitacao.FChequeSacado);
  fqQuitacao.SetAsString('CHEQUE_DOC_FEDERAL',Quitacao.FChequeDocFederal);
  fqQuitacao.SetAsString('CHEQUE_BANCO',Quitacao.FChequeBanco);
  fqQuitacao.SetAsString('CHEQUE_DT_EMISSAO',Quitacao.FChequeDtEmissao);
  fqQuitacao.SetAsString('CHEQUE_DT_VENCIMENTO',Quitacao.FChequeDtVencimento);
  fqQuitacao.SetAsString('CHEQUE_AGENCIA',Quitacao.FChequeAgencia);
  fqQuitacao.SetAsString('CHEQUE_CONTA_CORRENTE',Quitacao.FChequeContaCorrente);
  fqQuitacao.SetAsInteger('CHEQUE_NUMERO',Quitacao.FChequeNumero);

  if Quitacao.FIdOperadoraCartao > 0 then
  begin
    fqQuitacao.SetAsInteger('ID_OPERADORA_CARTAO', Quitacao.FIdOperadoraCartao);
  end;

  fqQuitacao.Salvar;
  fqQuitacao.Persistir;

  Result := fqQuitacao.getAsInteger('ID');
end;

class function TContaPagarServ.AtualizarContaPagarQuitacao(AContaPagarQuitacao: String) : Boolean;
var
  fqQuitacao : IFastQuery;
  Quitacao   : TContaPagarQuitacaoMovimento;
begin

  Quitacao := TJson.JsonToObject<TContaPagarQuitacaoMovimento>(AContaPagarQuitacao);

  fqQuitacao := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_PAGAR_QUITACAO WHERE ID = :ID'
                                          ,TArray<Variant>.Create(Quitacao.FId));

  fqQuitacao.Alterar;

  fqQuitacao.SetAsInteger('NR_ITEM', Quitacao.FNrItem);
  fqQuitacao.SetAsInteger('ID_CONTA_PAGAR', Quitacao.FIdContaPagar);

  fqQuitacao.SetAsString('DH_CADASTRO', Quitacao.FDhCadastro);
  fqQuitacao.SetAsString('DT_QUITACAO', Quitacao.FDtQuitacao);
  fqQuitacao.SetAsString('OBSERVACAO', Quitacao.FObservacao);

  fqQuitacao.SetAsInteger('ID_DOCUMENTO_ORIGEM', Quitacao.FIdDocumentoOrigem);
  fqQuitacao.SetAsString('TP_DOCUMENTO_ORIGEM', Quitacao.FTPDocumentoOrigem);
  fqQuitacao.SetAsString('STATUS', Quitacao.FStatus);

  fqQuitacao.SetAsInteger('ID_FILIAL', Quitacao.FIdFilial);
  fqQuitacao.SetAsInteger('ID_USUARIO_BAIXA', Quitacao.FIdUsuarioBaixa);
  fqQuitacao.SetAsInteger('ID_TIPO_QUITACAO', Quitacao.FIdTipoQuitacao);
  fqQuitacao.SetAsInteger('ID_CONTA_CORRENTE', Quitacao.FIdContaCorrente);
  fqQuitacao.SetAsInteger('ID_CONTA_ANALISE', Quitacao.FIdContaAnalise);

  fqQuitacao.SetAsFloat('VL_DESCONTO', Quitacao.FVlDesconto);
  fqQuitacao.SetAsFloat('VL_ACRESCIMO', Quitacao.FVlAcrescimo);
  fqQuitacao.SetAsFloat('VL_QUITACAO', Quitacao.FVlQuitacao);
  fqQuitacao.SetAsFloat('VL_TOTAL', Quitacao.FVlTotal);
  fqQuitacao.SetAsFloat('PERC_ACRESCIMO', Quitacao.FPercAcrescimo);
  fqQuitacao.SetAsFloat('PERC_DESCONTO', Quitacao.FPercDesconto);

  fqQuitacao.SetAsFloat('VL_MULTA', Quitacao.FVlMulta);
  fqQuitacao.SetAsFloat('VL_JUROS', Quitacao.FVlJuros);
  fqQuitacao.SetAsFloat('PERC_MULTA', Quitacao.FPercMulta);
  fqQuitacao.SetAsFloat('PERC_JUROS', Quitacao.FPercJuros);

  fqQuitacao.Salvar;
  fqQuitacao.Persistir;

  Result := True;

end;

class function TContaPagarServ.AtualizarContaPagar(AContaPagar: String): Boolean;
var
  fqContaPagar : IFastQuery;
  ContaPagar   : TContaPagarMovimento;
begin

  ContaPagar := TJson.JsonToObject<TContaPagarMovimento>(AContaPagar);

  fqContaPagar := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_PAGAR WHERE ID = :ID'
                                             ,TArray<Variant>.Create(ContaPagar.FId));
  fqContaPagar.Alterar;
  fqContaPagar.SetAsInteger('ID_PESSOA', ContaPagar.FIdPessoa);

  fqContaPagar.SetAsString('DOCUMENTO', ContaPagar.FDocumento);
  fqContaPagar.SetAsString('DESCRICAO', ContaPagar.FDescricao);
  fqContaPagar.SetAsString('STATUS', ContaPagar.FStatus);

  fqContaPagar.SetAsDateTime('DH_CADASTRO', StrToDateTime(ContaPagar.FDhCadastro));
  fqContaPagar.SetAsDateTime('DT_VENCIMENTO', StrToDate(ContaPagar.FDtVencimento));
  fqContaPagar.SetAsDateTime('DT_COMPETENCIA', StrToDate(ContaPagar.FDtCompetencia));

  fqContaPagar.SetAsFloat('QT_PARCELA', ContaPagar.FQtParcela);
  fqContaPagar.SetAsFloat('NR_PARCELA', ContaPagar.FNrParcela);

  fqContaPagar.SetAsFloat('VL_TITULO', ContaPagar.FVlTitulo);
  fqContaPagar.SetAsFloat('VL_QUITADO', ContaPagar.FVlQuitado);
  fqContaPagar.SetAsFloat('VL_ABERTO', ContaPagar.FVlAberto);

  fqContaPagar.SetAsFloat('VL_ACRESCIMO', ContaPagar.FVlAcrescimo);
  fqContaPagar.SetAsFloat('VL_DECRESCIMO', ContaPagar.FVlDecrescimo);
  fqContaPagar.SetAsFloat('VL_QUITADO_LIQUIDO', ContaPagar.FVlQuitadoLiquido);

  fqContaPagar.SetAsFloat('VL_MULTA', ContaPagar.FVlMulta);
  fqContaPagar.SetAsFloat('VL_JUROS', ContaPagar.FVlJuros);
  fqContaPagar.SetAsFloat('PERC_MULTA', ContaPagar.FPercMulta);
  fqContaPagar.SetAsFloat('PERC_JUROS', ContaPagar.FPercJuros);

  fqContaPagar.SetAsString('SEQUENCIA', ContaPagar.FSequencia);
  fqContaPagar.SetAsString('BO_VENCIDO', ContaPagar.FBoVencido);
  fqContaPagar.SetAsString('OBSERVACAO', ContaPagar.FObservacao);

  fqContaPagar.SetAsInteger('ID_CONTA_ANALISE', ContaPagar.FIdContaAnalise);
  fqContaPagar.SetAsInteger('ID_CENTRO_RESULTADO', ContaPagar.FIdCentroResultado);
  fqContaPagar.SetAsInteger('ID_CONTA_CORRENTE', ContaPagar.FIdContaCorrente);

  fqContaPagar.SetAsInteger('ID_CHAVE_PROCESSO', ContaPagar.FIdChaveProcesso);
  fqContaPagar.SetAsInteger('ID_FORMA_PAGAMENTO', ContaPagar.FIdFormaPagamento);

  fqContaPagar.Salvar;
  fqContaPagar.Persistir;

  Result := True;

end;


class function TContaPagarServ.AtualizarValores(AIdContaPagar : Integer) : Boolean;
const
  SQLConsulta = 'SELECT sum(crq.vl_quitacao)  AS VL_QUITACAO     ' +
                '      ,sum(crq.vl_total)     AS VL_TOTAL        ' +
                '      ,sum(crq.vl_acrescimo) AS VL_ACRESCIMO    ' +
                '      ,sum(crq.vl_multa) AS VL_MULTA    ' +
                '      ,sum(crq.vl_juros) AS VL_JUROS    ' +
                '      ,sum(crq.perc_acrescimo) AS PERC_ACRESCIMO    ' +
                '      ,sum(crq.perc_multa) AS PERC_MULTA    ' +
                '      ,sum(crq.perc_juros) AS PERC_JUROS    ' +
                '      ,sum(crq.vl_desconto)  AS VL_DESCONTO     ' +
                '      ,sum(crq.perc_desconto)  AS PERC_DESCONTO     ' +
                '  FROM conta_pagar_quitacao crq               ' +
                ' WHERE crq.id_conta_pagar = :id_conta_pagar ';
var
  fqPagar   : TFastQuery;
  fqQuitacao  : TFastQuery;
  VlTitulo    : Double;
  status      : string;
  VlQuitacao  : Double;
  VlAcrescimo : Double;
  VlMulta : Double;
  VlJuros : Double;
  PercAcrescimo : Double;
  PercMulta : Double;
  PercJuros : Double;
  PercDesconto : Double;
  VlDesconto  : Double;
  VlTotal     : Double;
begin

  fqQuitacao := TFastQuery.ModoDeConsulta(SQLConsulta
                                         ,TArray<Variant>.Create(AIdContaPagar));

  fqPagar := TFastQuery.ModoDeAlteracao('SELECT * FROM CONTA_PAGAR WHERE ID = :ID'
                                         ,TArray<Variant>.Create(AIdContaPagar));

  VlQuitacao  := fqQuitacao.GetAsFloat('VL_QUITACAO');
  VlAcrescimo := fqQuitacao.GetAsFloat('VL_ACRESCIMO');
  VlDesconto  := fqQuitacao.GetAsFloat('VL_DESCONTO');
  VlMulta := fqQuitacao.GetAsFloat('VL_MULTA');
  VlJuros := fqQuitacao.GetAsFloat('VL_JUROS');
  PercAcrescimo := fqQuitacao.GetAsFloat('PERC_ACRESCIMO');
  PercMulta := fqQuitacao.GetAsFloat('PERC_MULTA');
  PercJuros := fqQuitacao.GetAsFloat('PERC_JUROS');
  PercDesconto := fqQuitacao.GetAsFloat('PERC_DESCONTO');
  VlTotal     := fqQuitacao.GetAsFloat('VL_TOTAL');
  VlTitulo    := fqPagar.GetAsFloat('VL_TITULO');

  if VlQuitacao >= VlTitulo then
  begin
    status := TContaPagarMovimento.QUITADO;
  end
  else
  begin
    status := TContaPagarMovimento.ABERTO;
  end;

  fqPagar.Alterar;
  fqPagar.SetAsString('STATUS', status);
  fqPagar.SetAsFloat('VL_QUITADO', VlQuitacao );
  fqPagar.SetAsFloat('VL_ABERTO', (VlTitulo - VlTotal));
  fqPagar.SetAsFloat('VL_ACRESCIMO', VlAcrescimo);
  fqPagar.SetAsFloat('VL_DECRESCIMO', VlDesconto);
  fqPagar.SetAsFloat('VL_MULTA', VlMulta);
  fqPagar.SetAsFloat('VL_JUROS', VlJuros);
  fqPagar.SetAsFloat('PERC_MULTA', PercMulta);
  fqPagar.SetAsFloat('PERC_JUROS', PercJuros);
  fqPagar.SetAsFloat('VL_QUITADO_LIQUIDO', VlTotal + VlMulta + VlJuros - VlDesconto);
  fqPagar.Salvar;
  fqPagar.Persistir;
end;

class function TContaPagarServ.GetIdContaPagar(AIdBaixa : Integer) : Integer;
var
  fqConsulta : TFastQuery;
begin
  fqConsulta := TFastQuery.ModoDeConsulta('SELECT * FROM conta_pagar_quitacao WHERE id = :id'
                                           ,TArray<Variant>.Create(AIdBaixa));

  result := fqConsulta.GetAsInteger('id_conta_pagar');
end;

class function TContaPagarServ.GetIdContaPagarPeloIdDaQuitacao(
  AIdQuitacao: Integer): Integer;
var qContaPagar: IFastQuery;
begin
  qContaPagar := TFastQuery.ModoDeConsulta(
    'SELECT id_conta_pagar FROM conta_pagar_quitacao WHERE id = :id_quitacao',
    TArray<Variant>.Create(AIdQuitacao));
  result := qContaPagar.GetAsInteger('id_conta_pagar');
end;

class function TContaPagarServ.GetTotaisContaPagar(const AIdsContaPagar: String): TFDJSONDatasets;
var
  qConsulta: IFastQuery;
  SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select vl_aberto'+
  '       ,vl_acrescimo_aberto'+
  '       ,(vl_aberto + vl_acrescimo_aberto) as vl_aberto_liquido'+
  '       ,vl_aberto_vencido'+
  '       ,vl_aberto_vencer'+
  '       ,vl_recebido'+
  '       ,vl_acrescimo_recebido'+
  '       ,vl_recebido_liquido FROM'+
  ' (select (SELECT SUM(conta_pagar.vl_aberto) FROM conta_pagar WHERE id IN ('+AIdsContaPagar+') ) AS vl_aberto'+
  '       ,0 AS vl_acrescimo_aberto'+
  '       ,(SELECT SUM(conta_pagar.vl_aberto) FROM conta_pagar WHERE dt_vencimento < CURDATE() AND id IN ('+AIdsContaPagar+') ) AS vl_aberto_vencido'+
  '       ,(SELECT SUM(conta_pagar.vl_aberto) FROM conta_pagar WHERE dt_vencimento >= CURDATE() AND id IN ('+AIdsContaPagar+') ) AS vl_aberto_vencer'+
  '       ,(SELECT SUM(conta_pagar.vl_quitado) FROM conta_pagar WHERE id IN ('+AIdsContaPagar+')) AS vl_recebido'+
  '       ,(SELECT SUM(conta_pagar.vl_acrescimo) FROM conta_pagar WHERE id IN ('+AIdsContaPagar+')) AS vl_acrescimo_recebido'+
  '       ,(SELECT SUM(conta_pagar.vl_quitado_liquido) FROM conta_pagar WHERE id IN ('+AIdsContaPagar+')) AS vl_recebido_liquido'+
  ' from'+
  '   filial limit 1) totais_titulo';

  qConsulta := TFastQuery.ModoDeConsulta(SQL);
  TFDJSONDataSetsWriter.ListAdd(Result, (qConsulta as TFastQuery));
end;

class function TContaPagarServ.GetValorAberto(const AIdContaPagar: Integer): Double;
var
  fqConsulta : TFastQuery;
begin
  fqConsulta := TFastQuery.ModoDeConsulta('SELECT vl_aberto FROM conta_pagar WHERE id = :id'
                                           ,TArray<Variant>.Create(AIdContaPagar));

  result := fqConsulta.GetAsFloat('vl_aberto');
end;

class function TContaPagarServ.GetValorJuros(const AIdContaPagar: Integer): Double;
begin
  result := 0;
end;

class function TContaPagarServ.GetValorMulta(const AIdContaPagar: Integer): Double;
begin
  result := 0;
end;

end.
