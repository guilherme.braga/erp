unit uTabelaPrecoServ;

interface

Uses Data.FireDACJSONReflect, System.Generics.Collections, uProdutoProxy, uTabelaPrecoProxy, SysUtils;

type TTabelaPrecoServ = class
  class procedure AtualizarRegistro(AIdTabelaPreco, AIdProduto: Integer;
    AVlCusto, APercLucro, AVlVenda: Double);
  class function GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
  class function GetProdutosTabelaPreco(
    const AIdTabelaPreco: Integer): TFDJSONDataSets;
  class function GetDescricaoTabelaPreco(const AID: Integer): String;
  class function GetProdutoTabelaPreco(const AIdTabelaPreco,
    AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
  class function BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
  class function ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
  class procedure AtualizarValorCustoImpostoOperacional(const AIdProduto: Integer;
    const AValorCustoImpostoOperacional: Double);
end;

type TTabelaPrecoItemServ = class
  class function GetTabelaPrecoItem(
    const AIdTabelaPreco, AIdProduto: Integer): TTabelaPrecoItemProxy;
  class function GerarTabelaPrecoItem(
    const ATabelaPrecoItem: TTabelaPrecoItemProxy): Integer;
end;

implementation

{ TTabelaPrecoServ }

uses uFactoryQuery, uMathUtils;

class procedure TTabelaPrecoServ.AtualizarValorCustoImpostoOperacional(
  const AIdProduto: Integer; const AValorCustoImpostoOperacional: Double);
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM TABELA_PRECO_ITEM WHERE id = :id',
    TArray<Variant>.Create(AIdProduto));

  qConsulta.First;
  while not qConsulta.Eof do
  begin
    qConsulta.Alterar;
    qConsulta.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', AValorCustoImpostoOperacional);
    qConsulta.SetAsFloat('PERC_LUCRO',
      TMathUtils.PercentualSobreValor(qConsulta.GetAsFloat('VL_VENDA'),
        AValorCustoImpostoOperacional));
    qConsulta.Salvar;
    qConsulta.Next;
  end;

  qConsulta.Persistir;
end;

class function TTabelaPrecoServ.BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
var qTabelaPreco: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qTabelaPreco := TFastQuery.ModoDeConsulta(
     ' SELECT vl_custo_imposto_operacional as vl_custo'+
     '     ,tabela_preco_item.perc_lucro'+
     '     ,tabela_preco_item.vl_venda'+
     '     ,tabela_preco.id as id_tabela_preco'+
     '     ,tabela_preco.DESCRICAO as descricao_tabela_preco'+
     ' FROM tabela_preco'+
     ' LEFT JOIN tabela_preco_item on tabela_preco.id = tabela_preco_item.ID_TABELA_PRECO'+
     ' AND tabela_preco_item.id_produto = :id_produto',
    TArray<Variant>.Create(AIdProduto));

  TFDJSONDataSetsWriter.ListAdd(result, (qTabelaPreco as TFastQuery));
end;

class function TTabelaPrecoServ.ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
begin
  try
    TFastQuery.ExecutarScript('DELETE FROM TABELA_PRECO_ITEM WHERE ID_TABELA_PRECO = :ID',
      TArray<Variant>.Create(AIdTabelaPreco));

    TFastQuery.ExecutarScript('DELETE FROM TABELA_PRECO WHERE ID = :ID',
      TArray<Variant>.Create(AIdTabelaPreco));

    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao excluir a tabela de pre�o.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TTabelaPrecoServ.GetDescricaoTabelaPreco(const AID: Integer): String;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT descricao FROM TABELA_PRECO WHERE id = :id',
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsString('descricao');
end;

class procedure TTabelaPrecoServ.AtualizarRegistro(AIdTabelaPreco,
  AIdProduto: Integer; AVlCusto, APercLucro, AVlVenda: Double);
var qTabelaPrecoItem: IFastQuery;
begin
  qTabelaPrecoItem := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM TABELA_PRECO_ITEM WHERE ID_PRODUTO = :ID_PRODUTO AND ID_TABELA_PRECO = :ID_TABELA_PRECO',
    TArray<Variant>.Create(AIdProduto, AIdTabelaPreco));

  if qTabelaPrecoItem.IsEmpty then
  begin
    qTabelaPrecoItem.Incluir;
  end
  else
  begin
    qTabelaPrecoItem.Alterar;
  end;

  qTabelaPrecoItem.SetAsInteger('ID_PRODUTO', AIdProduto);
  qTabelaPrecoItem.SetAsInteger('ID_TABELA_PRECO', AIdTabelaPreco);
  qTabelaPrecoItem.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', AVlCusto);
  qTabelaPrecoItem.SetAsFloat('PERC_LUCRO', APercLucro);
  qTabelaPrecoItem.SetAsFloat('VL_VENDA', AVlVenda);
  qTabelaPrecoItem.Salvar;

  qTabelaPrecoItem.Persistir;
end;

class function TTabelaPrecoServ.GetProdutosTabelaPreco(const AIdTabelaPreco: Integer): TFDJSONDataSets;
var qTabelaPrecoItem: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qTabelaPrecoItem := TFastQuery.ModoDeConsulta(
    'SELECT * FROM tabela_preco_item WHERE id_tabela_preco = :id_tabela_preco',
    TArray<Variant>.Create(AIdTabelaPreco));

  TFDJSONDataSetsWriter.ListAdd(result, (qTabelaPrecoItem as TFastQuery));
end;

class function TTabelaPrecoServ.GetProdutoTabelaPreco(const AIdTabelaPreco,
  AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
const
  SQL =
  ' SELECT'+
  '   produto.id'+
  '  ,produto.descricao'+
  '  ,produto.codigo_barra'+
  '  ,produto_filial.qt_estoque'+
  '  ,tabela_preco_item.vl_custo_imposto_operacional'+
  '  ,tabela_preco_item.vl_venda'+
  '  ,unidade_estoque.sigla'+
  ' FROM PRODUTO'+
  ' LEFT JOIN unidade_estoque ON produto.id_unidade_estoque = unidade_estoque.id'+
  ' INNER JOIN produto_filial ON produto.id = produto_filial.id_produto'+
  ' INNER JOIN tabela_preco_item ON produto.id = tabela_preco_item.id_produto'+
  ' INNER JOIN tabela_preco ON tabela_preco_item.id_tabela_preco = tabela_preco.id'+
  ' WHERE tabela_preco_item.id_tabela_preco = :id_tabela_preco'+
  '   and tabela_preco_item.id_produto = :id_produto'+
  '   and produto_filial.id_filial = :id_filial';
var
  consultaProduto: IFastQuery;
begin
  result := TTabelaPrecoProdutoProxy.Create;
  consultaProduto := TFastQuery.ModoDeConsulta(SQL,TArray<Variant>.Create(
    AIdTabelaPreco, AIdProduto, AIdFilial));

  result.FId := consultaProduto.GetAsInteger('ID');
  result.FDescricao := consultaProduto.GetAsString('descricao');
  result.FCodigoBarra := consultaProduto.GetAsString('codigo_barra');
  result.FQtdeEstoque := consultaProduto.GetAsFloat('qt_estoque');
  result.FVlCusto := consultaProduto.GetAsFloat('vl_custo_imposto_operacional');
  result.FVlVenda := consultaProduto.GetAsFloat('vl_venda');
  result.FUN := consultaProduto.GetAsString('sigla');
end;

class function TTabelaPrecoServ.GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
var qTabelaPreco: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qTabelaPreco := TFastQuery.ModoDeConsulta(
    'SELECT * FROM tabela_preco LIMIT 1',
    nil);

  TFDJSONDataSetsWriter.ListAdd(result, (qTabelaPreco as TFastQuery));
end;

class function TTabelaPrecoItemServ.GetTabelaPrecoItem(
  const AIdTabelaPreco, AIdProduto: Integer): TTabelaPrecoItemProxy;
const
  SQL = 'SELECT * FROM TABELA_PRECO_ITEM WHERE id_tabela_preco = :AIdTabelaPreco and id_produto = :id_produto';
var
   qConsulta: IFastQuery;
begin
  result := TTabelaPrecoItemProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdTabelaPreco, AIdProduto));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdTabelaPreco := qConsulta.GetAsInteger('ID_TABELA_PRECO');
  result.FPercLucro := qConsulta.GetAsFloat('PERC_LUCRO');
  result.FVlVenda := qConsulta.GetAsFloat('VL_VENDA');
  result.FVlCustoImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');
end;

class function TTabelaPrecoItemServ.GerarTabelaPrecoItem(
    const ATabelaPrecoItem: TTabelaPrecoItemProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('TABELA_PRECO_ITEM');
    qEntidade.Incluir;


    if ATabelaPrecoItem.FIdProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_PRODUTO', ATabelaPrecoItem.FIdProduto);
    end;

    if ATabelaPrecoItem.FIdTabelaPreco > 0 then
    begin
      qEntidade.SetAsInteger('ID_TABELA_PRECO', ATabelaPrecoItem.FIdTabelaPreco);
    end;
    qEntidade.SetAsFloat('PERC_LUCRO', ATabelaPrecoItem.FPercLucro);
    qEntidade.SetAsFloat('VL_VENDA', ATabelaPrecoItem.FVlVenda);
    qEntidade.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', ATabelaPrecoItem.FVlCustoImpostoOperacional);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ATabelaPrecoItem.FID := qEntidade.GetAsInteger('ID');
    result :=     ATabelaPrecoItem.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar TabelaPrecoItem.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;


end.
