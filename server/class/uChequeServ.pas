unit uChequeServ;

interface

uses uChequeProxy, uFactoryQuery;

type TChequeServ = class
  private
    class function BuscarCheque(const AIdCheque: Integer): TFastQuery;
  public
    class function CadastrarCheque(ACheque: TChequeProxy): Integer;
    class function ExcluirCheque(AIdCheque: Integer): Boolean;
    class function CancelarCheque(AIdCheque: Integer): Boolean;
    class procedure ConciliarCheque(const AIdCheque: Integer;
      AConciliar: Boolean);
    class procedure GerarUtilizacaoCheque(const AIdCheque: Integer);
    class procedure EstornarUtilizacaoCheque(const AIdCheque: Integer);
    class function GetCheque(const AIdCheque: Integer): TChequeProxy;
    class function RealizarTransferencia(const AIdCheque: Integer;
      const AIdContaCorrenteDestino: Integer): Boolean;
    class function RealizarRetirada(const AIdCheque: Integer): Boolean;

    //Reentrada do cheque na conta corrente
    class function RealizarEstornoDaRetirada(const AIdCheque: Integer): Boolean;

    class function ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
end;

type TChequeHistoricoServ = class
  class function GetChequeHistorico(
    const AIdChequeHistorico: Integer): TChequeHistoricoProxy;

  class function GerarChequeHistorico(
    const AChequeHistorico: TChequeHistoricoProxy): Integer;

  class function PegarOcorrenciaTipoLancamentoChequeProprio(const AIdCheque, AIdContaPagar: Integer;
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrente: Integer): String;

  class function PegarOcorrenciaTipoEstornoChequeProprio(const AIdCheque, AIdContaPagar: Integer;
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrente: Integer): String;

  class function PegarOcorrenciaTipoTransferenciaChequeTerceiro(const AIdCheque,
    AIdContaCorrenteOrigem: Integer; const ADescricaoContaCorrenteOrigem: String;
    const AIdMovimentacaoContaCorrenteOrigem, AIdContaCorrenteDestino: Integer;
    const ADescricaoContaCorrenteDestino: String;
    const AIdMovimentacaoContaCorrenteDestino: Integer): String;

  class function PegarOcorrenciaTipoDebitoChequeTerceiro(const AIdCheque,
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrenteOrigem, AIdMovimentacaoContaCorrenteEstorno: Integer): String;

  class function PegarOcorrenciaTipoCreditoChequeTerceiro(const AIdCheque,
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrente: Integer): String;

  class function PegarOcorrenciaTipoUtilizadoChequeTerceiroParaPagamento(const AIdCheque,
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrente, AIdContaPagar,
    AIdMovimentacaoContaCorrenteEstorno: Integer): String;

  class function PegarOcorrenciaTipoEstornoUtilizacaoChequeTerceiroEmPagamento(const AIdCheque,
    AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
    const AIdMovimentacaoContaCorrente, AIdContaPagar,
    AIdMovimentacaoContaCorrenteEstorno: Integer): String;
end;

implementation

{ TChequeServ }

uses SysUtils, uContaCorrenteServ;

class function TChequeServ.GetCheque(
    const AIdCheque: Integer): TChequeProxy;
const
  SQL = 'SELECT * FROM CHEQUE WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TChequeProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCheque));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FSacado := qConsulta.GetAsString('SACADO');
  result.FDocFederal := qConsulta.GetAsString('DOC_FEDERAL');
  result.FVlCheque := qConsulta.GetAsFloat('VL_CHEQUE');
  result.FNumero := qConsulta.GetAsInteger('NUMERO');
  result.FBanco := qConsulta.GetAsString('BANCO');
  result.FAgencia := qConsulta.GetAsString('AGENCIA');
  result.FContaCorrente := qConsulta.GetAsString('CONTA_CORRENTE');
  result.FDtEmissao := qConsulta.GetAsString('DT_EMISSAO');
  result.FDtVencimento := qConsulta.GetAsString('DT_VENCIMENTO');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FIdContaCorrente := qConsulta.GetAsInteger('ID_CONTA_CORRENTE');
  result.FBoConciliado := qConsulta.GetAsString('BO_CONCILIADO');
  result.FDhConciliado := qConsulta.GetAsString('DH_CONCILIADO');
  result.FIdDocumentoOrigem := qConsulta.GetAsInteger('ID_DOCUMENTO_ORIGEM');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FBoChequeUtilizado := qConsulta.GetAsString('BO_CHEQUE_UTILIZADO');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FIdContaCorrenteMovimento := qConsulta.GetAsInteger('ID_CONTA_CORRENTE_MOVIMENTO');
end;

class function TChequeServ.RealizarEstornoDaRetirada(const AIdCheque: Integer): Boolean;
var
  cheque: TChequeProxy;
  qCheque: TFastQuery;
  idContaCorrenteMovimentoDestino: Integer;
begin
  result := false;

  if (AIdCheque <= 0) then
  begin
    exit;
  end;

  cheque := TChequeServ.GetCheque(AIdCheque);
  try
    if cheque.FIdContaCorrenteMovimento <= 0 then
    begin
      exit;
    end;

    //Gera a movimentação contraria na mesma conta corrente
    idContaCorrenteMovimentoDestino := TContaCorrenteServ.RealizarTransferencia(
      cheque.FIdContaCorrenteMovimento, cheque.FIdContaCorrente);

    qCheque := TChequeServ.BuscarCheque(AIdCheque);
    try
      qCheque.Alterar;
      qCheque.SetAsInteger('ID_CONTA_CORRENTE_MOVIMENTO', idContaCorrenteMovimentoDestino);
      qCheque.Salvar;
      qCheque.Persistir;
    finally
      FreeAndNil(qCheque);
    end;
  finally
    FreeAndNil(cheque);
  end;
end;

class function TChequeServ.RealizarRetirada(const AIdCheque: Integer): Boolean;
var
  cheque: TChequeProxy;
  qCheque: TFastQuery;
  idContaCorrenteMovimentoDestino: Integer;
begin
  result := false;

  if (AIdCheque <= 0) then
  begin
    exit;
  end;

  cheque := TChequeServ.GetCheque(AIdCheque);
  try
    if cheque.FIdContaCorrenteMovimento <= 0 then
    begin
      exit;
    end;

    //Gera a movimentação contraria na mesma conta corrente
    idContaCorrenteMovimentoDestino := TContaCorrenteServ.RealizarTransferencia(
      cheque.FIdContaCorrenteMovimento, cheque.FIdContaCorrente);

    qCheque := TChequeServ.BuscarCheque(AIdCheque);
    try
      qCheque.Alterar;
      qCheque.SetAsInteger('ID_CONTA_CORRENTE_MOVIMENTO', idContaCorrenteMovimentoDestino);
      qCheque.Salvar;
      qCheque.Persistir;
    finally
      FreeAndNil(qCheque);
    end;
  finally
    FreeAndNil(cheque);
  end;
end;

class function TChequeServ.RealizarTransferencia(const AIdCheque, AIdContaCorrenteDestino: Integer): Boolean;
var
  cheque: TChequeProxy;
  qCheque: TFastQuery;
  idContaCorrenteMovimentoDestino: Integer;
begin
  result := false;

  if (AIdCheque <= 0) or (AIdContaCorrenteDestino <= 0) then
  begin
    exit;
  end;

  cheque := TChequeServ.GetCheque(AIdCheque);
  try
    if cheque.FIdContaCorrenteMovimento <= 0 then
    begin
      exit;
    end;

    //Gera a movimentação contraria na mesma conta corrente
    idContaCorrenteMovimentoDestino := TContaCorrenteServ.RealizarTransferencia(
      cheque.FIdContaCorrenteMovimento, cheque.FIdContaCorrente);

    //Gera a movimentação destino na conta corrente de destino
    idContaCorrenteMovimentoDestino := TContaCorrenteServ.RealizarTransferencia(
      idContaCorrenteMovimentoDestino, AIdContaCorrenteDestino);

    qCheque := TChequeServ.BuscarCheque(AIdCheque);
    try
      qCheque.Alterar;
      qCheque.SetAsInteger('ID_CONTA_CORRENTE', AIdContaCorrenteDestino);
      qCheque.SetAsInteger('ID_CONTA_CORRENTE_MOVIMENTO', idContaCorrenteMovimentoDestino);
      qCheque.Salvar;
      qCheque.Persistir;
    finally
      FreeAndNil(qCheque);
    end;
  finally
    FreeAndNil(cheque);
  end;
end;

class function TChequeServ.BuscarCheque(const AIdCheque: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM cheque WHERE id = :id',
    TArray<Variant>.Create(AIdCheque));
end;

class function TChequeServ.CadastrarCheque(ACheque: TChequeProxy): Integer;
var
  qCheque: IFastQuery;
begin
  try
    qCheque := TFastQuery.ModoDeInclusao('CHEQUE');

    qCheque.Incluir;
    qCheque.SetAsString('SACADO',ACheque.FSacado);
    qCheque.SetAsString('DOC_FEDERAL',ACheque.FDocFederal);
    qCheque.SetAsFloat('VL_CHEQUE',ACheque.FVlCheque);

    if ACheque.FNumero > 0 then
    begin
      qCheque.SetAsInteger('NUMERO',ACheque.FNumero);
    end;

    qCheque.SetAsString('BANCO',ACheque.FBanco);
    qCheque.SetAsString('AGENCIA',ACheque.FAgencia);
    qCheque.SetAsString('CONTA_CORRENTE',ACheque.FContaCorrente);

    if not ACheque.FDtEmissao.IsEmpty then
    begin
      qCheque.SetAsString('DT_EMISSAO',ACheque.FDtEmissao);
    end;

    if not ACheque.FDtVencimento.IsEmpty then
    begin
      qCheque.SetAsString('DT_VENCIMENTO',ACheque.FDtVencimento);
    end;

    qCheque.SetAsInteger('ID_CHAVE_PROCESSO',ACheque.FIdChaveProcesso);
    qCheque.SetAsInteger('ID_CONTA_CORRENTE',ACheque.FIdContaCorrente);

    qCheque.SetAsString('BO_CONCILIADO',ACheque.FBoConciliado);

    if not ACheque.FDhConciliado.IsEmpty then
    begin
      qCheque.SetAsString('DH_CONCILIADO',ACheque.FDhConciliado);
    end;

    qCheque.SetAsString('BO_CHEQUE_UTILIZADO',ACheque.FBoChequeUtilizado);
    qCheque.SetAsString('TIPO',ACheque.FTipo);

    if ACheque.FIdDocumentoOrigem > 0 then
    begin
      qCheque.SetAsInteger('ID_DOCUMENTO_ORIGEM',ACheque.FIdDocumentoOrigem);
      qCheque.SetAsString('DOCUMENTO_ORIGEM',ACheque.FDocumentoOrigem);
    end;

    if ACheque.FIdContaCorrenteMovimento > 0 then
    begin
      qCheque.SetAsInteger('ID_CONTA_CORRENTE_MOVIMENTO',ACheque.FIdContaCorrenteMovimento);
    end;

    qCheque.Salvar;
    qCheque.Persistir;

    ACheque.FId := qCheque.GetAsInteger('ID');
    result := ACheque.FId;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao cadastrar cheque.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TChequeServ.CancelarCheque(AIdCheque: Integer): Boolean;
var
  qCheque: IFastQuery;
begin
  qCheque := TFastQuery.ModoDeAlteracao('SELECT * FROM CHEQUE WHERE ID = :id',
    TArray<Variant>.Create(AIdCheque));

  qCheque.Alterar;
  qCheque.SetAsString('BO_CHEQUE_UTILIZADO', 'N');
  qCheque.SetAsString('STATUS', TChequeProxy.STATUS_CANCELADO);
  qCheque.Salvar;
  qCheque.Persistir;
end;

class function TChequeServ.ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
var
  cheque: IFastQuery;
begin
  cheque := TFastQuery.ModoDeConsulta(
    'SELECT * FROM cheque WHERE id = :id',
    TArray<Variant>.Create(AIdCheque));

  result := cheque.GetAsString('BO_CHEQUE_UTILIZADO') = 'N';
end;

class procedure TChequeServ.ConciliarCheque(const AIdCheque: Integer;
  AConciliar: Boolean);
var
  qCheque: IFastQuery;
begin
  qCheque := TFastQuery.ModoDeAlteracao('SELECT * FROM CHEQUE WHERE ID = :id',
    TArray<Variant>.Create(AIdCheque));
  qCheque.Alterar;

  if AConciliar then
  begin
    qCheque.SetAsString('BO_CONCILIADO', 'S');
    qCheque.SetAsDateTime('dh_conciliado',Now);
  end
  else
  begin
    qCheque.SetAsString('BO_CONCILIADO', 'N');
    TFastQuery(qCheque).FieldByName('dh_conciliado').Clear;
  end;

  qCheque.Salvar;
  qCheque.Persistir;
end;

class procedure TChequeServ.EstornarUtilizacaoCheque(const AIdCheque: Integer);
begin
  TFastQuery.ExecutarScript(
    'UPDATE cheque SET bo_cheque_utilizado = :bo_cheque_utilizado and id = :id',
      TArray<Variant>.Create('N', AIdCheque));
end;

class function TChequeServ.ExcluirCheque(AIdCheque: Integer): Boolean;
var
  qCheque: IFastQuery;
begin
  qCheque := TFastQuery.ModoDeAlteracao('SELECT * FROM CHEQUE WHERE ID = :id',
    TArray<Variant>.Create(AIdCheque));

  qCheque.Excluir;
  qCheque.Persistir;
end;

class procedure TChequeServ.GerarUtilizacaoCheque(const AIdCheque: Integer);
begin
  TFastQuery.ExecutarScript(
    'UPDATE cheque SET bo_cheque_utilizado = :bo_cheque_utilizado and id = :id',
      TArray<Variant>.Create('S', AIdCheque));
end;

{ TChequeHistoricoServ }

class function TChequeHistoricoServ.GetChequeHistorico(
    const AIdChequeHistorico: Integer): TChequeHistoricoProxy;
const
  SQL = 'SELECT * FROM CHEQUE_HISTORICO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TChequeHistoricoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdChequeHistorico));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhOcorrencia := qConsulta.GetAsString('DH_OCORRENCIA');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FOcorrencia := qConsulta.GetAsString('OCORRENCIA');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdDocumentoOrigem := qConsulta.GetAsInteger('ID_DOCUMENTO_ORIGEM');
  result.FSequencia := qConsulta.GetAsInteger('SEQUENCIA');
  result.FIdCheque := qConsulta.GetAsInteger('ID_CHEQUE');
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoCreditoChequeTerceiro(const AIdCheque,
  AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
  const AIdMovimentacaoContaCorrente: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_CREDITO_CHEQUE_TERCEIRO,
    [AIdCheque, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrente]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoDebitoChequeTerceiro(const AIdCheque,
  AIdContaCorrente: Integer; const ADescricaoContaCorrente: String; const AIdMovimentacaoContaCorrenteOrigem,
  AIdMovimentacaoContaCorrenteEstorno: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_DEBITO_CHEQUE_TERCEIRO,
    [AIdCheque, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrenteOrigem,
    AIdMovimentacaoContaCorrenteEstorno]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoEstornoChequeProprio(const AIdCheque,
  AIdContaPagar: Integer; AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
  const AIdMovimentacaoContaCorrente: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_ESTORNO_CHEQUE_PROPRIO,
    [AIdCheque, AIdContaPagar, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrente]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoEstornoUtilizacaoChequeTerceiroEmPagamento(
  const AIdCheque, AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
  const AIdMovimentacaoContaCorrente, AIdContaPagar, AIdMovimentacaoContaCorrenteEstorno: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_ESTORNO_UTILIZACAO_CHEQUE_TERCEIRO_EM_PAGAMENTO,
    [AIdCheque, AIdContaPagar, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrente,
    AIdContaPagar, AIdMovimentacaoContaCorrenteEstorno]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoLancamentoChequeProprio(const AIdCheque,
  AIdContaPagar: Integer; AIdContaCorrente: Integer; const ADescricaoContaCorrente: String;
  const AIdMovimentacaoContaCorrente: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_LANCAMENTO_CHEQUE_PROPRIO,
    [AIdCheque, AIdContaPagar, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrente]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoTransferenciaChequeTerceiro(const AIdCheque,
  AIdContaCorrenteOrigem: Integer; const ADescricaoContaCorrenteOrigem: String;
  const AIdMovimentacaoContaCorrenteOrigem, AIdContaCorrenteDestino: Integer;
  const ADescricaoContaCorrenteDestino: String; const AIdMovimentacaoContaCorrenteDestino: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_TRANSFERENCIA_CHEQUE_TERCEIRO,
    [AIdCheque, AIdContaCorrenteOrigem, ADescricaoContaCorrenteOrigem, AIdMovimentacaoContaCorrenteOrigem,
    AIdContaCorrenteDestino, ADescricaoContaCorrenteDestino, AIdMovimentacaoContaCorrenteDestino]);
end;

class function TChequeHistoricoServ.PegarOcorrenciaTipoUtilizadoChequeTerceiroParaPagamento(const AIdCheque,
  AIdContaCorrente: Integer; const ADescricaoContaCorrente: String; const AIdMovimentacaoContaCorrente,
  AIdContaPagar, AIdMovimentacaoContaCorrenteEstorno: Integer): String;
begin
  result := Format(TChequeHistoricoProxy.OCORRENCIA_TIPO_UTILIZADO_CHEQUE_TERCEIRO_PARA_PAGAMENTO,
    [AIdCheque, AIdContaCorrente, ADescricaoContaCorrente, AIdMovimentacaoContaCorrente,
    AIdContaPagar, AIdMovimentacaoContaCorrenteEstorno]);
end;

class function TChequeHistoricoServ.GerarChequeHistorico(
    const AChequeHistorico: TChequeHistoricoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('CHEQUE_HISTORICO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_OCORRENCIA', AChequeHistorico.FDhOcorrencia);
    qEntidade.SetAsString('TIPO', AChequeHistorico.FTipo);
    qEntidade.SetAsString('OCORRENCIA', AChequeHistorico.FOcorrencia);

    if AChequeHistorico.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', AChequeHistorico.FIdPessoaUsuario);
    end;

    if AChequeHistorico.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AChequeHistorico.FIdFilial);
    end;

    if AChequeHistorico.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AChequeHistorico.FIdChaveProcesso);
    end;
    qEntidade.SetAsString('DOCUMENTO_ORIGEM', AChequeHistorico.FDocumentoOrigem);

    if AChequeHistorico.FIdDocumentoOrigem > 0 then
    begin
      qEntidade.SetAsInteger('ID_DOCUMENTO_ORIGEM', AChequeHistorico.FIdDocumentoOrigem);
    end;
    qEntidade.SetAsInteger('SEQUENCIA', AChequeHistorico.FSequencia);

    if AChequeHistorico.FIdCheque > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHEQUE', AChequeHistorico.FIdCheque);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    AChequeHistorico.FID := qEntidade.GetAsInteger('ID');
    result :=     AChequeHistorico.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Cheque Historico.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.



