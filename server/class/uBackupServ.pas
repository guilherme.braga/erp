unit uBackupServ;

interface

Uses classes, SysUtils, System.Types, uBackupProxy;

type TBackupServ = class
  private
    class function DiretorioExiste: Boolean;
    class function MysqlDumpEncontrado: Boolean;
    class function NomeArquivoBackup: String;
    const DIRETORIO_BACKUP = 'Backup';
  public
    class function ValidarConfiguracoesParaBackup(): Boolean;
    class function RealizarBackup(const AIdUsuario: Integer): Boolean;
    class function GetBackup(const AIdBackup: Integer): TBackupProxy;
    class function BackupDiarioRealizado: Boolean;
    class function BuscarArquivoUltimoBackupGerado: String;
    class function GerarRegistroBackup(const ABackup: TBackupProxy): Integer;
    class function DownloadBackup(out Size: Int64): TStream;
end;

implementation

{ TBackupServ }

uses uWindowsUtils, uIniFileServ, ShellApi, DateUtils, uFactoryQuery;

class function TBackupServ.BuscarArquivoUltimoBackupGerado: String;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT arquivo_backup_servidor FROM backup WHERE id = (select max(id) from backup)');

  if qConsulta.IsEmpty then
  begin
    result := '';
  end
  else
  begin
    result := qConsulta.GetAsString('arquivo_backup_servidor');
  end;
end;

class function TBackupServ.DiretorioExiste: Boolean;
begin
  ForceDirectories(TWindowsUtils.DiretorioAplicacao + DIRETORIO_BACKUP);

  result := DirectoryExists(TWindowsUtils.DiretorioAplicacao + DIRETORIO_BACKUP);
end;

class function TBackupServ.DownloadBackup(out Size: Int64): TStream;
var
  arquivo: String;
begin
  arquivo := BuscarArquivoUltimoBackupGerado;

  if not(arquivo.IsEmpty) and FileExists(arquivo) then
  begin
    Result := TFileStream.Create(arquivo, fmOpenRead or fmShareDenyNone);
    Size := Result.Size;

    Result.Position := 0;
  end
  else
  begin
    result := nil;
  end;
end;

class function TBackupServ.MysqlDumpEncontrado: Boolean;
begin
  result := FileExists(TWindowsUtils.DiretorioAplicacao + '\mysqldump.exe');
end;

class function TBackupServ.NomeArquivoBackup: String;
var
  DataHoraTexto: String;
begin
  DataHoraTexto := DateTimeToStr(now);

  DataHoraTexto := StringReplace(DataHoraTexto, '/', '', [rfReplaceAll]);

  DataHoraTexto := StringReplace(DataHoraTexto, ' ', '_', [rfReplaceAll]);

  DataHoraTexto := StringReplace(DataHoraTexto, ':', '', [rfReplaceAll]);

  result := TWindowsUtils.DiretorioAplicacao + DIRETORIO_BACKUP+
    '\Backup_'+IniSystemFile.databaseERP+'_'+DataHoraTexto+'.sql';
end;

class function TBackupServ.RealizarBackup(const AIdUsuario: Integer): Boolean;
var
  sComando_Backup: String;
  ExitCode: DWORD;
  backup: TBackupProxy;
  nomeArquivo: String;
begin
  ValidarConfiguracoesParaBackup;
  nomeArquivo := NomeArquivoBackup;

   sComando_Backup := '/c ' + TWindowsUtils.DiretorioAplicacao + '\mysqldump -h ' +
     IniSystemFile.server + ' -P ' + IniSystemFile.porta + ' -u root --password=t23mhcr247 ' +
     IniSystemFile.databaseERP + ' > ' + nomeArquivo;

  if TWindowsUtils.Sto_ShellExecute('cmd.exe', sComando_Backup, ExitCode, 1000000000, true) then

  if ExitCode <> 0 then
  begin
    result := false;
    raise Exception.Create('Erro ao gerar o script. C�digo de sa�da: ' + IntToStr(ExitCode));
  end
  else
  begin
    backup := TBackupProxy.Create;
    try
      backup.FIdPessoaUsuario := AIdUsuario;
      backup.FDhBackup := DateTimeToStr(Now);
      backup.FArquivoBackupServidor := nomeArquivo;
      TBackupServ.GerarRegistroBackup(backup);
      result := true;
    finally
      FreeAndNil(backup);
    end;
  end;
end;

class function TBackupServ.ValidarConfiguracoesParaBackup(): Boolean;
var
  existeDiretorioBackup: Boolean;
  arquivoDumpEncontrado: Boolean;
begin
  existeDiretorioBackup := DiretorioExiste;
  arquivoDumpEncontrado := MysqlDumpEncontrado;

  result := existeDiretorioBackup and arquivoDumpEncontrado;

  if not existeDiretorioBackup then
  begin
    raise Exception.Create('N�o foi poss�vel criar diret�rio de backup, verifique as permiss�es de escrita do usu�rio.');
  end;

  if not arquivoDumpEncontrado then
  begin
    raise Exception.Create('N�o foi encontrado o arquivo mysqldump.exe no diret�rio do servidor de aplica��o.');
  end;
end;

class function TBackupServ.GetBackup(
    const AIdBackup: Integer): TBackupProxy;
const
  SQL = 'SELECT * FROM BACKUP WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TBackupProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdBackup));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FDhBackup := qConsulta.GetAsString('DH_BACKUP');
  result.FArquivoBackupServidor := qConsulta.GetAsString('ARQUIVO_BACKUP_SERVIDOR');
end;

class function TBackupServ.GerarRegistroBackup(
    const ABackup: TBackupProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('BACKUP');
    qEntidade.Incluir;

    if ABackup.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', ABackup.FIdPessoaUsuario);
    end;

    qEntidade.SetAsString('DH_BACKUP', ABackup.FDhBackup);
    qEntidade.SetAsString('ARQUIVO_BACKUP_SERVIDOR', ABackup.FArquivoBackupServidor);

    qEntidade.Salvar;
    qEntidade.Persistir;

    ABackup.FID := qEntidade.GetAsInteger('ID');
    result :=     ABackup.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Backup.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TBackupServ.BackupDiarioRealizado: Boolean;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT dh_backup FROM backup WHERE id = (select max(id) from backup)');

  if qConsulta.IsEmpty then
  begin
    result := false;
  end
  else
  begin
    result := DaysBetween(qConsulta.GetAsDate('dh_backup'), Now) <= 1;
  end;
end;

end.
