unit uFormaPagamentoServ;

interface

Uses uFactoryQuery, SysUtils;

type TFormaPagamentoServ = class
  const
    SQL_FORMA_PAGAMENTO_COM_QUITACAO =
    ' select fp.*'+
    '       ,tq.descricao AS JOIN_DESCRICAO_TIPO_QUITACAO'+
    '       ,tq.tipo AS JOIN_TIPO_TIPO_QUITACAO'+
    ' from forma_pagamento fp'+
    ' inner join tipo_quitacao tq on fp.id_tipo_quitacao = tq.id'+
    ' where fp.id = :id';

  class function GetDescricaoFormaPagamento(const AID: Integer): String;
  class function GetTipoQuitacaoFormaPagamento(const AID: Integer): Integer;
  class function GetFormaPagamento(const AId: Integer): String; //FormaPagamentoProxy
  class function GetCodigosFormaPagamentoCartao: String;
  class function FormaPagamentoCartaoDebito(const AIdFormaPagamento: Integer): Boolean;
  class function FormaPagamentoCartaoCredito(const AIdFormaPagamento: Integer): Boolean;
  class function FormaPagamentoDinheiro(const AIdFormaPagamento: Integer): Boolean;
  class function FormaPagamentoCheque(const AIdFormaPagamento: Integer): Boolean;
end;

implementation

Uses uFormaPagamentoProxy, REST.JSON, uTipoQuitacaoProxy, uDatasetUtilsServ;

class function TFormaPagamentoServ.FormaPagamentoCartaoCredito(
  const AIdFormaPagamento: Integer): Boolean;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AIdFormaPagamento));

  result := qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO').Equals(
    TTipoQuitacaoProxy.TIPO_CREDITO);
end;

class function TFormaPagamentoServ.FormaPagamentoCartaoDebito(
  const AIdFormaPagamento: Integer): Boolean;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AIdFormaPagamento));

  result := qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO').Equals(
    TTipoQuitacaoProxy.TIPO_DEBITO);
end;

class function TFormaPagamentoServ.FormaPagamentoCheque(
  const AIdFormaPagamento: Integer): Boolean;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AIdFormaPagamento));

  result := qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO').Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO').Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO);
end;

class function TFormaPagamentoServ.FormaPagamentoDinheiro(
  const AIdFormaPagamento: Integer): Boolean;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AIdFormaPagamento));

  result := qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO').Equals(
    TTipoQuitacaoProxy.TIPO_DINHEIRO);
end;

class function TFormaPagamentoServ.GetCodigosFormaPagamentoCartao: String;
const
  SQL =
  '  select id'+
  '  from forma_pagamento'+
  ' where exists(select 1 from tipo_quitacao'+
  '                      where id = forma_pagamento.id_tipo_quitacao'+
  '                        and tipo_quitacao.tipo IN (''CREDITO'', ''DEBITO''))';
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL, nil);
  result := TDatasetUtilsServ.Concatenar(TFastQuery(qConsulta).FieldByName('ID'));
end;

class function TFormaPagamentoServ.GetDescricaoFormaPagamento(const AID: Integer): String;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsString('descricao');
end;

class function TFormaPagamentoServ.GetFormaPagamento(
  const AId: Integer): String;
var
  formaPagamentoProxy: TFormaPagamentoProxy;
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AID));

  formaPagamentoProxy := TFormaPagamentoProxy.Create;
  try
    formaPagamentoProxy.Fid := qConsulta.GetAsInteger('ID');
    formaPagamentoProxy.Fdescricao := qConsulta.GetAsString('DESCRICAO');
    formaPagamentoProxy.FIdTipoQuitacao :=
      qConsulta.GetAsInteger('ID_TIPO_QUITACAO');
    formaPagamentoProxy.FDescricaoTipoQuitacao :=
      qConsulta.GetAsString('JOIN_DESCRICAO_TIPO_QUITACAO');
    formaPagamentoProxy.FTipoTipoQuitacao :=
      qConsulta.GetAsString('JOIN_TIPO_TIPO_QUITACAO');
    result := TJSON.ObjectToJsonString(formaPagamentoProxy);
  finally
    FreeAndNil(formaPagamentoProxy);
  end;
end;

class function TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
  const AID: Integer): Integer;
var qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    SQL_FORMA_PAGAMENTO_COM_QUITACAO,
    TArray<Variant>.Create(AID));

  result := qConsulta.GetAsInteger('ID_TIPO_QUITACAO');

end;

end.
