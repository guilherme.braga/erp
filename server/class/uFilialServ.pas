unit uFilialServ;

interface

Uses uFilialProxy, uFactoryQuery;

type TFilialServ = class
  class function GetIdFilial: Integer;
  class function GetCNPJ(const AIdFilial: Integer): String;
  class function GetCRT(AIdFilial: Integer): Integer;
  class function GetFantasia(AIdFilial: Integer): String;
  class function GetFilial(const AIdFilial: Integer): TFilialProxy;
  class function GetFilialQuery(const AIdFilial: Integer): TFastQuery;
  class function GetInformacoesFiltroImposto(const AIdFilial: Integer): TFastQuery;
  class function OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
end;

implementation

{ TFilial }

uses uCNAEServ;

class function TFilialServ.GetIdFilial: Integer;
begin
  result := 1;
end;

class function TFilialServ.GetInformacoesFiltroImposto(const AIdFilial: Integer): TFastQuery;
const
  SQL_PESSOA_REGRA_IMPOSTO_FILTRO =
  ' SELECT crt'+
  '       ,regime_tributario'+
  '       ,bo_contribuinte_icms'+
  '       ,bo_contribuinte_ipi'+
  '       ,id_zoneamento'+
  '       ,id_regime_especial'+
  '       ,id_estado'+
  '   FROM filial'+
  '  WHERE id = :id ';
begin
  result := TFastQuery.ModoDeConsulta(SQL_PESSOA_REGRA_IMPOSTO_FILTRO, TArray<Variant>.Create(AIdFilial));
end;

class function TFilialServ.OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
var
  qfilial: IFastQuery;
begin
  qfilial := TFastQuery.ModoDeConsulta(
    'SELECT REGIME_TRIBUTARIO, CRT FROM filial WHERE id = :id',
    TArray<Variant>.Create(AIdFilial));

  result := (qFilial.GetAsString('REGIME_TRIBUTARIO') = 'SIMPLES') and
    (qFilial.GetAsString('CRT') = '1');
end;

class function TFilialServ.GetCNPJ(const AIdFilial: Integer): String;
var qfilial: IFastQuery;
begin
  qfilial := TFastQuery.ModoDeConsulta(
    'SELECT cnpj FROM filial WHERE id = :id',
    TArray<Variant>.Create(AIdFilial));

  result := qFilial.GetAsString('cnpj');
end;

class function TFilialServ.GetCRT(AIdFilial: Integer): Integer;
var qfilial: IFastQuery;
begin
  qfilial := TFastQuery.ModoDeConsulta(
    'SELECT crt FROM filial WHERE id = :id',
    TArray<Variant>.Create(AIdFilial));

  result := qFilial.GetAsInteger('crt');
end;

class function TFilialServ.GetFantasia(AIdFilial: Integer): String;
var qfilial: IFastQuery;
begin
  qfilial := TFastQuery.ModoDeConsulta(
    'SELECT fantasia FROM filial WHERE id = :id',
    TArray<Variant>.Create(AIdFilial));

  result := qFilial.GetAsString('fantasia');
end;

class function TFilialServ.GetFilial(
    const AIdFilial: Integer): TFilialProxy;
const
  SQL =
  '   SELECT filial.*'+
  '       ,CIDADE.IBGE AS JOIN_IBGE_CIDADE'+
  '       ,CIDADE.DESCRICAO AS JOIN_DESCRICAO_CIDADE'+
  '       ,ESTADO.UF  AS JOIN_UF_ESTADO'+
  '       ,ESTADO.IBGE AS JOIN_IBGE_ESTADO'+
  '       ,ESTADO.DESCRICAO AS JOIN_DESCRICAO_ESTADO'+
  '       ,PAIS.IBGE AS JOIN_IBGE_PAIS'+
  '       ,PAIS.DESCRICAO AS JOIN_DESCRICAO_PAIS'+
  ' FROM filial'+
  ' LEFT JOIN CIDADE ON filial.ID_CIDADE = CIDADE.ID'+
  ' LEFT JOIN ESTADO ON CIDADE.ID_ESTADO = ESTADO.ID'+
  ' LEFT JOIN PAIS ON ESTADO.ID_PAIS = PAIS.ID'+
  ' WHERE filial.id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TFilialProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdFilial));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FNrItem := qConsulta.GetAsInteger('NR_ITEM');
  result.FFantasia := qConsulta.GetAsString('FANTASIA');
  result.FRazaoSocial := qConsulta.GetAsString('RAZAO_SOCIAL');
  result.FCnpj := qConsulta.GetAsString('CNPJ');
  result.FIe := qConsulta.GetAsString('IE');
  result.FDtFundacao := qConsulta.GetAsString('DT_FUNDACAO');
  result.FIdEmpresa := qConsulta.GetAsInteger('ID_EMPRESA');
  result.FIdZoneamento := qConsulta.GetAsInteger('ID_ZONEAMENTO');
  result.FRegimeTributario := qConsulta.GetAsString('REGIME_TRIBUTARIO');
  result.FBoMatriz := qConsulta.GetAsString('BO_MATRIZ');
  result.FIdCidade := qConsulta.GetAsInteger('ID_CIDADE');
  result.FLogradouro := qConsulta.GetAsString('LOGRADOURO');
  result.FNumero := qConsulta.GetAsString('NUMERO');
  result.FComplemento := qConsulta.GetAsString('COMPLEMENTO');
  result.FBairro := qConsulta.GetAsString('BAIRRO');
  result.FCep := qConsulta.GetAsString('CEP');
  result.FTelefone := qConsulta.GetAsString('TELEFONE');
  result.FSite := qConsulta.GetAsString('SITE');
  result.FEmail := qConsulta.GetAsString('EMAIL');
  result.FTipoEmpresa := qConsulta.GetAsString('TIPO_EMPRESA');
  result.FCrt := qConsulta.GetAsInteger('CRT');
  result.FBoContribuinteIcms := qConsulta.GetAsString('BO_CONTRIBUINTE_ICMS');
  result.FBoContribuinteIpi := qConsulta.GetAsString('BO_CONTRIBUINTE_IPI');
  result.FIM := qConsulta.GetAsString('IM');
  result.FIdCNAE := qConsulta.GetAsInteger('ID_CNAE');

  //Transientes
  result.FCodigoIGBECidade := qConsulta.GetAsInteger('JOIN_IBGE_CIDADE');
  result.FCodigoIGBEEstado := qConsulta.GetAsInteger('JOIN_IBGE_ESTADO');
  result.FCodigoIGBEPais := qConsulta.GetAsInteger('JOIN_IBGE_PAIS');
  result.FNomeCidade := qConsulta.GetAsString('JOIN_DESCRICAO_CIDADE');
  result.FUF := qConsulta.GetAsString('JOIN_UF_ESTADO');
  result.FNomeEstado := qConsulta.GetAsString('JOIN_DESCRICAO_ESTADO');
  result.FNomePais := qConsulta.GetAsString('JOIN_DESCRICAO_PAIS');
end;

class function TFilialServ.GetFilialQuery(const AIdFilial: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM filial WHERE id = :id',
    TArray<Variant>.Create(AIdFilial));
end;

end.
