unit uUsuarioServer;

interface

uses FireDAC.Comp.Client, SysUtils, uGBFDQuery, Data.FireDACJSONReflect,
  uFactoryQuery, uUsuarioProxy;

type TUsuarioPesquisaPadrao = class
  private
    class function GerarSQLPesquisaPadrao(AQuery: TgbFDQuery;
      const AIdentifier: String; const AIdUsuario: Integer): String;

  public
  class procedure HabilitarConsultaAutomatica(AQuery: TgbFDQuery; const ACodigoUsuario: Integer; const AFormName: String; const AHabilitar: Boolean);
  class function EstaConsultandoAutomatico(AQuery: TgbFDQuery; const ACodigoUsuario: Integer; const AFormName: String): Boolean;
  class function GetSQLPesquisaPadrao(AQuery: TgbFDQuery; const AIdentifier: String; const AIdUsuario: Integer): String;
  class procedure SetSQLPesquisaPadrao(AQuery: TgbFDQuery; const AIdentifier: String; const AIdUsuario: Integer; ASQL: String);
  class function ExistePesquisaPadrao(AQuery: TgbFDQuery; const AIdentifier: String; const AIdUsuario: Integer): Boolean;
end;

type TUsuarioServ = class
  class function GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
  class function GetPessoaUsuario(const AIdPessoaUsuario: Integer): TUsuarioProxy;
  class function GetIdUsuario(const AUsuario: String): Integer;
end;

implementation

{ TUsuarioPesquisaPadrao }

class function TUsuarioServ.GetIdUsuario(const AUsuario: String): Integer;
const
  SQL = 'SELECT id FROM PESSOA_USUARIO WHERE usuario = :usuario ';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(UpperCase(AUsuario)));

  result := qConsulta.GetAsInteger('id');
end;

class function TUsuarioServ.GetPessoaUsuario(
    const AIdPessoaUsuario: Integer): TUsuarioProxy;
const
  SQL = 'SELECT * FROM PESSOA_USUARIO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TUsuarioProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoaUsuario));

  result.Id := qConsulta.GetAsInteger('ID');
  result.Usuario := qConsulta.GetAsString('USUARIO');
  result.IdUsuarioPerfil := qConsulta.GetAsInteger('ID_USUARIO_PERFIL');
  result.Nome := qConsulta.GetAsString('NOME');
end;

class function TUsuarioServ.GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
var qUsuario: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qUsuario := TFastQuery.ModoDeConsulta(
    'SELECT * FROM pessoa_usuario WHERE id = :id',
    TArray<Variant>.Create(AIdUsuario));

  TFDJSONDataSetsWriter.ListAdd(result, (qUsuario as TFastQuery));
end;

class function TUsuarioPesquisaPadrao.EstaConsultandoAutomatico(AQuery: TgbFDQuery;
  const ACodigoUsuario: Integer; const AFormName: String): Boolean;
begin
  AQuery.ModoDeConsulta(
    ' SELECT '+
    '   bo_consulta_automatica'+
    ' FROM '+
    '   pesquisa_padrao'+
    ' WHERE'+
    '   id_pessoa_usuario = :id_pessoa_usuario'+
    '   and descricao = :descricao', TArray<Variant>.Create(ACodigoUsuario, UpperCase(AFormName)));

  result := (AQuery.IsEmpty) or
    (AQuery.FieldByName('bo_consulta_automatica').AsString = 'S');
end;

class function TUsuarioPesquisaPadrao.ExistePesquisaPadrao(AQuery: TgbFDQuery;
  const AIdentifier: String; const AIdUsuario: Integer): Boolean;
begin
  AQuery.ModoDeConsulta(
    ' SELECT sql_pesquisa '+
    ' FROM pesquisa_padrao'+
    ' WHERE UPPER(descricao) = :descricao AND id_pessoa_usuario = :id_pessoa_usuario',
    TArray<Variant>.Create(UpperCase(AIdentifier), InttoStr(AIdUsuario)));

  result := not AQuery.IsEmpty;
end;

class function TUsuarioPesquisaPadrao.GerarSQLPesquisaPadrao(AQuery: TgbFDQuery;
  const AIdentifier: String; const AIdUsuario: Integer): String;
var
  SQLGerado: String;
  qTestarSQL: IFastQuery;
begin
  SQLGerado := 'SELECT * FROM '+AIdentifier;
  try
    qTestarSQL := TFastQuery.ModoDeConsulta(SQLGerado);
  Except
    result := EmptyStr;
    Exit;
  end;

  TUsuarioPesquisaPadrao.SetSQLPesquisaPadrao(AQuery, AIdentifier, AIdUsuario, SQLGerado);

  result := SQLGerado;
end;

class function TUsuarioPesquisaPadrao.GetSQLPesquisaPadrao(AQuery: TgbFDQuery;
  const AIdentifier: String; const AIdUsuario: Integer): String;
begin
  //CAST(sql_pesquisa AS CHAR(50000) CHARACTER SET utf8)
  AQuery.ModoDeConsulta(
    ' SELECT sql_pesquisa as sql_pesquisa '+
    ' FROM pesquisa_padrao'+
    ' WHERE UPPER(descricao) = :descricao AND id_pessoa_usuario = :id_pessoa_usuario',
    TArray<Variant>.Create(UpperCase(AIdentifier), InttoStr(AIdUsuario)));

  if not AQuery.FieldByName('sql_pesquisa').AsString.IsEmpty then
  begin
    result := AQuery.FieldByName('sql_pesquisa').AsString;
    exit;
  end;

  AQuery.ModoDeConsulta(
    ' SELECT sql_pesquisa as sql_pesquisa '+
    ' FROM pesquisa_padrao'+
    ' WHERE UPPER(descricao) = :descricao LIMIT 1',
    TArray<Variant>.Create(UpperCase(AIdentifier), InttoStr(AIdUsuario)));

  if not AQuery.FieldByName('sql_pesquisa').AsString.IsEmpty then
  begin
    result := AQuery.FieldByName('sql_pesquisa').AsString;
    exit;
  end;

  Result := TUsuarioPesquisaPadrao.GerarSQLPesquisaPadrao(AQuery, AIdentifier, AIdUsuario);
end;

class procedure TUsuarioPesquisaPadrao.SetSQLPesquisaPadrao(AQuery: TgbFDQuery;
  const AIdentifier: String; const AIdUsuario: Integer; ASQL: String);
begin
  if ExistePesquisaPadrao(AQuery, AIdentifier, AIdUsuario) then
  begin
    AQuery.ExecutarScript(
      ' UPDATE'+
      ' pesquisa_padrao'+
      ' SET sql_pesquisa = :sql_pesquisa'+
      ' WHERE UPPER(descricao) = :descricao AND id_pessoa_usuario = :id_pessoa_usuario',
      TArray<Variant>.Create(ASQL, UpperCase(AIdentifier), AIdUsuario));
      //' AND id_pessoa_usuario = '+InttoStr(AIdUsuario));
  end
  else
  begin
    AQuery.ModoDeAlteracao(
      ' SELECT * '+
      ' FROM pesquisa_padrao'+
      ' WHERE 1=2', nil);

    AQuery.Append;
    AQuery.FieldByName('SQL_PESQUISA').AsString := ASQL;
    AQuery.FieldByName('DESCRICAO').AsString := UpperCase(AIdentifier);
    AQuery.FieldByName('BO_ATIVO').AsString := 'S';
    AQuery.FieldByName('ID_PESSOA_USUARIO').AsInteger := AIdUsuario;
    AQuery.FieldByName('BO_CONSULTA_AUTOMATICA').AsString := 'N';
    AQuery.Post;

    AQuery.Persistir;
    AQuery.Commit;
  end;
end;

class procedure TUsuarioPesquisaPadrao.HabilitarConsultaAutomatica(AQuery: TgbFDQuery;
  const ACodigoUsuario: Integer; const AFormName: String;
  const AHabilitar: Boolean);
var status: String;
begin
  if AHabilitar then
    status := 'S'
  else
    status := 'N';

  AQuery.ExecutarScript(
    ' UPDATE '+
    '   pesquisa_padrao'+
    ' SET '+
    '   bo_consulta_automatica = :bo_consulta_automatica'+
    ' WHERE'+
    '   id_pessoa_usuario = :id_pessoa_usuario'+
    '   and descricao = :descricao', TArray<Variant>.Create(status, ACodigoUsuario, UpperCase(AFormName)));
  AQuery.ExecSQL;
end;

end.

