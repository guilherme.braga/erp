unit uMontadoraServ;

Interface

Uses uMontadoraProxy, db, SysUtils;

type TMontadoraServ = class
  class function GetMontadora(
    const AIdMontadora: Integer): TMontadoraProxy;
  class function GerarMontadora(
    const AMontadora: TMontadoraProxy): Integer;
  class function ExisteMontadora(const ADescricaoMontadora: String): Boolean;
  class function GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
end;

type TModeloMontadoraServ = class
  class function GetModeloMontadora(
    const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
  class function GerarModeloMontadora(
    const AModeloMontadora: TModeloMontadoraProxy): Integer;
  class function ExisteModelo(const ADescricaoModelo: String; const ADescricaoMontadora: String): Boolean;
end;

implementation

{ TMontadoraServ }

uses uFactoryQuery;

class function TMontadoraServ.GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from montadora where descricao = :montadora',
    TArray<Variant>.Create(ADescricaoMontadora));

  result := qEntidade.GetAsInteger('id');
end;

class function TMontadoraServ.GetMontadora(
    const AIdMontadora: Integer): TMontadoraProxy;
const
  SQL = 'SELECT * FROM MONTADORA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TMontadoraProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdMontadora));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
end;

class function TMontadoraServ.ExisteMontadora(const ADescricaoMontadora: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from montadora where descricao = :montadora',
    TArray<Variant>.Create(ADescricaoMontadora));

  result := not qEntidade.IsEmpty;
end;

class function TMontadoraServ.GerarMontadora(
    const AMontadora: TMontadoraProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('MONTADORA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AMontadora.FDescricao);
    qEntidade.SetAsString('BO_ATIVO', AMontadora.FBoAtivo);
    qEntidade.SetAsString('OBSERVACAO', AMontadora.FObservacao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AMontadora.FID := qEntidade.GetAsInteger('ID');
    result :=     AMontadora.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Montadora.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

{ TModeloMontadoraServ }

class function TModeloMontadoraServ.GetModeloMontadora(
    const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
const
  SQL = 'SELECT * FROM MODELO_MONTADORA WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TModeloMontadoraProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdModeloMontadora));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FIdMontadora := qConsulta.GetAsInteger('ID_MONTADORA');
end;

class function TModeloMontadoraServ.ExisteModelo(const ADescricaoModelo,
  ADescricaoMontadora: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' SELECT id FROM MODELO_MONTADORA WHERE descricao = :modelo and'+
    ' id_montadora = (select id from montadora where descricao = :montadora)',
    TArray<Variant>.Create(ADescricaoModelo, ADescricaoMontadora));

  result := not qEntidade.IsEmpty;
end;

class function TModeloMontadoraServ.GerarModeloMontadora(
    const AModeloMontadora: TModeloMontadoraProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('MODELO_MONTADORA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AModeloMontadora.FDescricao);
    qEntidade.SetAsString('BO_ATIVO', AModeloMontadora.FBoAtivo);
    qEntidade.SetAsString('OBSERVACAO', AModeloMontadora.FObservacao);

    if AModeloMontadora.FIdMontadora > 0 then
    begin
      qEntidade.SetAsInteger('ID_MONTADORA', AModeloMontadora.FIdMontadora);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    AModeloMontadora.FID := qEntidade.GetAsInteger('ID');
    result :=     AModeloMontadora.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ModeloMontadora.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.

