unit uIntegracaoFiscalVendaServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery, uFactoryQuery,
  uIntegracaoFiscalServ, uVendaProxy;

type TIntegracaoFiscalVendaServ = class(TIntegracaoFiscalServ)
  public
    constructor CreateIntegracaoNFE(const AIdEntidadeBase: Integer; AVendaProxy: TVendaProxy); overload;
    constructor CreateIntegracaoNFCE(const AIdEntidadeBase: Integer; const ACPFNaNota: String;
      AVendaProxy: TVendaProxy); overload;
  protected
    function ValidarDocumentoFiscalJaEmitido: Boolean; override;
    procedure InstanciarEntidadeOrigem; override;  
    procedure AtualizarCodigoNaEntidadeBase; override;
end;

implementation

uses uNotaFiscalProxy, uNotaFiscalServ, uVendaServ, uContaReceberServ;

function TIntegracaoFiscalVendaServ.ValidarDocumentoFiscalJaEmitido: Boolean;
var
  idNotaFiscal: Integer;
  statusNotaFiscalEletronica: String;  
begin
  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    idNotaFiscal := TVendaServ.GetIdNotaFiscalNFE(FIdEntidadeBase);    
  end
  else if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    idNotaFiscal := TVendaServ.GetIdNotaFiscalNFCE(FIdEntidadeBase);
  end;

  if idNotaFiscal > 0 then
  begin
    statusNotaFiscalEletronica := TNotaFiscalServ.BuscarStatusNotaFiscalEletronica(idNotaFiscal);

    if statusNotaFiscalEletronica.Equals(TNotaFiscalProxy.STATUS_NFE_ENVIADA) then
    begin
      result := true;
      exit; 
    end;

    RemoverNotaFiscalIntegracaoFiscal;
    result := false;
  end;
  begin
    result := false;
  end;  
end;

constructor TIntegracaoFiscalVendaServ.CreateIntegracaoNFCE(const AIdEntidadeBase: Integer;
  const ACPFNaNota: String; AVendaProxy: TVendaProxy);
begin
  CreateIntegracaoNFCE(AIdEntidadeBase, ACPFNaNota);

  FNotaFiscalTransiente.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
  FNotaFiscalTransiente.FIdFormaPagamentoDinheiro := AVendaProxy.FIdFormaPagamentoDinheiro;
  FNotaFiscalTransiente.FIdCarteira := AVendaProxy.FIdCarteira;
end;

constructor TIntegracaoFiscalVendaServ.CreateIntegracaoNFE(const AIdEntidadeBase: Integer;
  AVendaProxy: TVendaProxy);
begin
  CreateIntegracaoNFE(AIdEntidadeBase);

  FNotaFiscalTransiente.FIdContaCorrente := AVendaProxy.FIdContaCorrente;
  FNotaFiscalTransiente.FIdFormaPagamentoDinheiro := AVendaProxy.FIdFormaPagamentoDinheiro;
  FNotaFiscalTransiente.FIdCarteira := AVendaProxy.FIdCarteira;
end;

procedure TIntegracaoFiscalVendaServ.InstanciarEntidadeOrigem;
begin
  FEntidadeBase := TVendaServ.GetVenda(TVendaServ.GetChaveProcesso(FIdEntidadeBase));
  FEntidadeBaseItem := TVendaServ.GetVendaItem(FIdEntidadeBase);
  FEntidadeBaseParcela := TVendaServ.GetVendaParcela(FIdEntidadeBase);
end;

procedure TIntegracaoFiscalVendaServ.AtualizarCodigoNaEntidadeBase;
begin
  TVendaServ.AtualizarCodigoNotaFiscalNaVenda(FIdEntidadeBase,
    FIdNotaFiscal, FTipoDocumentoFiscal);	
end;

end.
