unit uMovimentacaoChequeServ;

interface

Uses uFactoryQuery, uMovimentacaoChequeProxy, SysUtils;

type TMovimentacaoChequeServ = class
private
  class function BuscarMovimentacao(const AIdChaveProcesso: Integer): TFastQuery;
  class function BuscarChequesDaMovimentacao(const AIdMovimentacaoCheque: Integer): TFastQuery;
  class function BuscarMovimentacoesContaCorrente(const AIdsContaCorrenteMovimento: String): TFastQuery;
  class function BuscarCheques(const AIdsCheques: String): TFastQuery;
  class procedure AtualizarStatus(const AIdMovimentacaoCheque: Integer; const AStatus: String);
  class function GerarTransferencia(AListaCheques: TFastQuery; AContaCorrenteDestino: Integer): Boolean;
  class function GerarDebito(AListaCheques: TFastQuery): Boolean;
  class function EstornarDebito(AListaCheques: TFastQuery): Boolean;
public
  class function Efetivar(const AIdChaveProcesso: Integer): Boolean;
  class function Estornar(const AIdChaveProcesso: Integer): Boolean;
  class function PodeReabrir(const AIdMovimentacaoCheque: Integer): Boolean;
  class function PodeExcluir(const AIdMovimentacaoCheque: Integer): Boolean;

  class function GetMovimentacaoCheque(const AIdMovimentacaoCheque: Integer): TMovimentacaoChequeProxy;
  class function GerarMovimentacaoCheque(const AMovimentacaoCheque: TMovimentacaoChequeProxy): Integer;

  class function BuscarIDPelaChaveProcesso(const AIDChaveProcesso: Integer): Integer;
end;

implementation

{ TMovimentacaoChequeServ }

uses uContaCorrenteProxy, uContaCorrenteServ, uChequeServ;

class function TMovimentacaoChequeServ.BuscarMovimentacao(const
  AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
  'SELECT * FROM movimentacao_cheque WHERE id_chave_processo = :id',
  TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TMovimentacaoChequeServ.BuscarChequesDaMovimentacao(const
  AIdMovimentacaoCheque: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    ' SELECT * FROM movimentacao_cheque_cheque WHERE'+
    ' id_movimentacao_cheque = :id_movimentacao_cheque',
    TArray<Variant>.Create(AIdMovimentacaoCheque));
end;

class function TMovimentacaoChequeServ.BuscarIDPelaChaveProcesso(const AIDChaveProcesso: Integer): Integer;
var
  qMovimentacao: IFastQuery;
begin
  qMovimentacao := TFastQuery.ModoDeConsulta(
    'SELECT ID FROM movimentacao_cheque WHERE id_chave_processo = :id',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qMovimentacao.GetAsInteger('ID');
end;

class function TMovimentacaoChequeServ.BuscarMovimentacoesContaCorrente(
  const AIdsContaCorrenteMovimento: String): TFastQuery;
begin
  result := TContaCorrenteServ.GetContaCorrenteMovimentos(AIdsContaCorrenteMovimento);
end;

class procedure TMovimentacaoChequeServ.AtualizarStatus(
  const AIdMovimentacaoCheque: Integer; const AStatus: String);
begin
  if AStatus.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO) then
  begin
    TFastQuery.ExecutarScriptIndependente(
      'UPDATE movimentacao_cheque SET status = :status, dh_fechamento = null where id = :id',
      TArray<Variant>.Create(AStatus, AIdMovimentacaoCheque));
  end
  else
  begin
    TFastQuery.ExecutarScriptIndependente(
      'UPDATE movimentacao_cheque SET status = :status, dh_fechamento = CURRENT_TIMESTAMP() where id = :id',
      TArray<Variant>.Create(AStatus, AIdMovimentacaoCheque));
  end;
end;

class function TMovimentacaoChequeServ.BuscarCheques(const AIdsCheques: String): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM cheque WHERE id in ('+AIdsCheques+')');
end;

class function TMovimentacaoChequeServ.Efetivar(const AIdChaveProcesso: Integer): Boolean;
var
  qMovimentacaoCheque: TFastQuery;
  qMovimentacaoChequeCheque: TFastQuery;
  qCheques: TFastQuery;
  tipoMovimentacao: String;
  idsCheques: String;
begin
  try
    qMovimentacaoCheque := TMovimentacaoChequeServ.BuscarMovimentacao(AIdChaveProcesso);
    try
      qMovimentacaoChequeCheque :=
      TMovimentacaoChequeServ.BuscarChequesDaMovimentacao(qMovimentacaoCheque.GetAsInteger('ID'));
      try
        idsCheques := qMovimentacaoChequeCheque.Concatenar('ID_CHEQUE');
        qCheques := TMovimentacaoChequeServ.BuscarCheques(idsCheques);
        try
          tipoMovimentacao := qMovimentacaoCheque.GetAsString('TIPO_MOVIMENTO');

          if tipoMovimentacao.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA) then
          begin
            TMovimentacaoChequeServ.GerarTransferencia(qCheques,
              qMovimentacaoCheque.GetAsInteger('ID_CONTA_CORRENTE_DESTINO'));
          end
          else if tipoMovimentacao.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_DEBITO) then
          begin
            TMovimentacaoChequeServ.GerarDebito(qCheques);
          end;
        finally
          FreeAndNil(qCheques);
        end
      finally
        FreeAndNil(qMovimentacaoChequeCheque);
      end;

      TMovimentacaoChequeServ.AtualizarStatus(
        qMovimentacaoCheque.GetAsInteger('ID'), TMovimentacaoChequeProxy.STATUS_FECHADO);

      result := true;
    finally
      FreeAndNil(qMovimentacaoCheque);
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar efetivar a movimenta��o de cheque.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TMovimentacaoChequeServ.Estornar(const AIdChaveProcesso: Integer): Boolean;
var
  qMovimentacaoCheque: TFastQuery;
  qMovimentacaoChequeCheque: TFastQuery;
  qCheques: TFastQuery;
  tipoMovimentacao: String;
  idsCheques: String;
begin
  try
    qMovimentacaoCheque := TMovimentacaoChequeServ.BuscarMovimentacao(AIdChaveProcesso);
    try
      qMovimentacaoChequeCheque :=
      TMovimentacaoChequeServ.BuscarChequesDaMovimentacao(qMovimentacaoCheque.GetAsInteger('ID'));
      try
        idsCheques := qMovimentacaoChequeCheque.Concatenar('ID_CHEQUE');
        qCheques := TMovimentacaoChequeServ.BuscarCheques(idsCheques);
        try
          tipoMovimentacao := qMovimentacaoCheque.GetAsString('TIPO_MOVIMENTO');

          {if tipoMovimentacao.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA) then
          begin
            TMovimentacaoChequeServ.GerarTransferencia(qCheques,
              qMovimentacaoCheque.GetAsInteger('ID_CONTA_CORRENTE_DESTINO'));
          end}//N�o implementa estorno de transferencia

          if tipoMovimentacao.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_DEBITO) then
          begin
            TMovimentacaoChequeServ.EstornarDebito(qCheques);
          end;
        finally
          FreeAndNil(qCheques);
        end
      finally
        FreeAndNil(qMovimentacaoChequeCheque);
      end;

      TMovimentacaoChequeServ.AtualizarStatus(
        qMovimentacaoCheque.GetAsInteger('ID'), TMovimentacaoChequeProxy.STATUS_CANCELADO);

      result := true;
    finally
      FreeAndNil(qMovimentacaoCheque);
    end;
  except
    on e : Exception do
    begin
      result := false;
      raise Exception.Create('Erro ao gerar estorno da movimenta��o de cheque.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TMovimentacaoChequeServ.EstornarDebito(AListaCheques: TFastQuery): Boolean;
begin
  AListaCheques.First;
  while not AListaCheques.Eof do
  begin
    TChequeServ.RealizarEstornoDaRetirada(AListaCheques.GetAsInteger('ID'));
    AListaCheques.Next;
  end;

  result := true;
end;

class function TMovimentacaoChequeServ.GerarDebito(AListaCheques: TFastQuery): Boolean;
begin
  AListaCheques.First;
  while not AListaCheques.Eof do
  begin
    TChequeServ.RealizarRetirada(AListaCheques.GetAsInteger('ID'));
    AListaCheques.Next;
  end;

  result := true;
end;

class function TMovimentacaoChequeServ.GerarTransferencia(AListaCheques: TFastQuery;
  AContaCorrenteDestino: Integer): Boolean;
begin
  AListaCheques.First;
  while not AListaCheques.Eof do
  begin
    TChequeServ.RealizarTransferencia(AListaCheques.GetAsInteger('ID'), AContaCorrenteDestino);
    AListaCheques.Next;
  end;

  result := true;
end;

class function TMovimentacaoChequeServ.PodeReabrir(const
  AIdMovimentacaoCheque: Integer): Boolean;
begin
  result := TMovimentacaoChequeServ.PodeExcluir(AIdMovimentacaoCheque);
end;

class function TMovimentacaoChequeServ.PodeExcluir(const
  AIdMovimentacaoCheque: Integer): Boolean;
begin
  result := true;
end;

class function TMovimentacaoChequeServ.GetMovimentacaoCheque(
    const AIdMovimentacaoCheque: Integer): TMovimentacaoChequeProxy;
const
  SQL = 'SELECT * FROM MOVIMENTACAO_CHEQUE WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TMovimentacaoChequeProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdMovimentacaoCheque));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipoMovimento := qConsulta.GetAsString('TIPO_MOVIMENTO');
  result.FMovimentacaoChequecol := qConsulta.GetAsString('MOVIMENTACAO_CHEQUEcol');
  result.FIdPessoaUsuarioCadastro := qConsulta.GetAsInteger('ID_PESSOA_USUARIO_CADASTRO');
  result.FIdPessoaUsuarioEfetivacao := qConsulta.GetAsInteger('ID_PESSOA_USUARIO_EFETIVACAO');
  result.FIdPessoaUsuarioReabertura := qConsulta.GetAsInteger('ID_PESSOA_USUARIO_REABERTURA');
  result.FIdContaCorrente := qConsulta.GetAsInteger('ID_CONTA_CORRENTE');
  result.FIdContaCorrenteDestino := qConsulta.GetAsInteger('ID_CONTA_CORRENTE_DESTINO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FDhFechamento := qConsulta.GetAsString('DH_FECHAMENTO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
end;

class function TMovimentacaoChequeServ.GerarMovimentacaoCheque(
    const AMovimentacaoCheque: TMovimentacaoChequeProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('MOVIMENTACAO_CHEQUE');
    qEntidade.Incluir;

    qEntidade.SetAsString('TIPO_MOVIMENTO', AMovimentacaoCheque.FTipoMovimento);
    qEntidade.SetAsString('MOVIMENTACAO_CHEQUEcol', AMovimentacaoCheque.FMovimentacaoChequecol);

    if AMovimentacaoCheque.FIdPessoaUsuarioCadastro > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO_CADASTRO', AMovimentacaoCheque.FIdPessoaUsuarioCadastro);
    end;

    if AMovimentacaoCheque.FIdPessoaUsuarioEfetivacao > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO_EFETIVACAO', AMovimentacaoCheque.FIdPessoaUsuarioEfetivacao);
    end;

    if AMovimentacaoCheque.FIdPessoaUsuarioReabertura > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO_REABERTURA', AMovimentacaoCheque.FIdPessoaUsuarioReabertura);
    end;

    if AMovimentacaoCheque.FIdContaCorrente > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_CORRENTE', AMovimentacaoCheque.FIdContaCorrente);
    end;

    if AMovimentacaoCheque.FIdContaCorrenteDestino > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_CORRENTE_DESTINO', AMovimentacaoCheque.FIdContaCorrenteDestino);
    end;

    if AMovimentacaoCheque.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AMovimentacaoCheque.FIdFilial);
    end;

    if AMovimentacaoCheque.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AMovimentacaoCheque.FIdChaveProcesso);
    end;
    qEntidade.SetAsString('DH_CADASTRO', AMovimentacaoCheque.FDhCadastro);
    qEntidade.SetAsString('DH_FECHAMENTO', AMovimentacaoCheque.FDhFechamento);
    qEntidade.SetAsString('OBSERVACAO', AMovimentacaoCheque.FObservacao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AMovimentacaoCheque.FID := qEntidade.GetAsInteger('ID');
    result :=     AMovimentacaoCheque.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar MovimentacaoCheque.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.

