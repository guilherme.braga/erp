unit uNaturezaOperacaoServ;

Interface

Uses uNaturezaOperacaoProxy, db, SysUtils;

type TNaturezaOperacaoServ = class
  class function GetNaturezaOperacao(const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
  class function GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
  class function GerarNaturezaOperacao(const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
  class function ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
  class function GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
end;

implementation

{ TNaturezaOperacaoServ }

uses uFactoryQuery;

class function TNaturezaOperacaoServ.GetNaturezaOperacao(
    const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
const
  SQL = 'SELECT * FROM NATUREZA_OPERACAO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TNaturezaOperacaoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdNaturezaOperacao));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
end;

class function TNaturezaOperacaoServ.GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select descricao from cor where id = :id',
    TArray<Variant>.Create(AIdNaturezaOperacao));

  result := qEntidade.GetAsString('descricao');
end;

class function TNaturezaOperacaoServ.ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from cor where descricao = :cor',
    TArray<Variant>.Create(ADescricaoNaturezaOperacao));

  result := not qEntidade.IsEmpty;
end;

class function TNaturezaOperacaoServ.GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from cor where descricao = :cor',
    TArray<Variant>.Create(ADescricaoNaturezaOperacao));

  result := qEntidade.GetAsInteger('id');
end;

class function TNaturezaOperacaoServ.GerarNaturezaOperacao(
    const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('NATUREZA_OPERACAO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ANaturezaOperacao.FDescricao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ANaturezaOperacao.FID := qEntidade.GetAsInteger('ID');
    result := ANaturezaOperacao.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Natureza Operacao.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


