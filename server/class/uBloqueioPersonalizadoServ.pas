unit uBloqueioPersonalizadoServ;

Interface

Uses uBloqueioPersonalizadoProxy, SysUtils, uFactoryQuery;

type TBloqueioPersonalizadoServ = class
  class function GetBloqueioPersonalizado(
    const AIdBloqueioPersonalizado: Integer): TBloqueioPersonalizadoProxy;
  class function GerarBloqueioPersonalizado(
    const ABloqueioPersonalizado: TBloqueioPersonalizadoProxy): Integer;
  class function UsuarioAutorizado(const AIdBloqueioPersonalizado, AIdUsuario: Integer): Boolean;
  class function BuscarBloqueiosVenda: TFastQuery;
  class function PrepararSQLBloqueioVenda(const ASQL: String; AVenda: TFastQuery): String;
end;

type TBloqueioPersonalizadoUsuarioLiberacaoServ = class
  class function GetBloqueioPersonalizadoUsuarioLiberacao(
    const AIdBloqueioPersonalizadoUsuarioLiberacao: Integer): TBloqueioPersonalizadoUsuarioLiberacaoProxy;
  class function GerarBloqueioPersonalizadoUsuarioLiberacao(
    const ABloqueioPersonalizadoUsuarioLiberacao: TBloqueioPersonalizadoUsuarioLiberacaoProxy): Integer;
end;

implementation

Uses db, uDateUtils, uStringUtils;

{ TBloqueioPersonalizadoServ }

class function TBloqueioPersonalizadoServ.UsuarioAutorizado(const AIdBloqueioPersonalizado,
  AIdUsuario: Integer): Boolean;
const
  SQL = 'SELECT * FROM BLOQUEIO_PERSONALIZADO_USUARIO_LIBERACAO WHERE '+
  '  id_bloqueio_personalizado = :id_bloqueio_personalizado and id_pessoa_usuario = :id_pessoa_usuario';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdBloqueioPersonalizado, AIdUsuario));

  result := not qConsulta.IsEmpty;
end;

class function TBloqueioPersonalizadoServ.GetBloqueioPersonalizado(
    const AIdBloqueioPersonalizado: Integer): TBloqueioPersonalizadoProxy;
const
  SQL = 'SELECT * FROM BLOQUEIO_PERSONALIZADO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TBloqueioPersonalizadoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdBloqueioPersonalizado));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FSql := qConsulta.GetAsString('SQL');
  result.FBoVenda := qConsulta.GetAsString('BO_VENDA');
  result.FBoNotaFiscal := qConsulta.GetAsString('BO_NOTA_FISCAL');
  result.FBoOrdemServico := qConsulta.GetAsString('BO_ORDEM_SERVICO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

class function TBloqueioPersonalizadoServ.PrepararSQLBloqueioVenda(const ASQL: String;
  AVenda: TFastQuery): String;
var
  i: Integer;
  valor: String;
begin
  result := UpperCase(ASQL);
  for i := 0 to AVenda.Fields.Count -1 do
  begin
    case AVenda.Fields[i].DataType of
      ftSmallint, ftInteger, ftWord, ftLargeint, ftShortint:
        valor := AVenda.Fields[i].AsString;

      ftFloat, ftFMTBcd, ftCurrency, ftBCD, ftExtended:
        valor := TStringUtils.RetornarDoubleNoFormatoMYSQL(AVenda.Fields[i].AsFloat);

      ftDate, ftTime, ftDateTime:
        valor := TStringUtils.RetornarDataNoFormatoMYSQL(AVenda.Fields[i].AsDateTime);
    else
      valor := QuotedStr(AVenda.Fields[i].AsString);
    end;

    result := StringReplace(result, '['+UpperCase(AVenda.Fields[i].FieldName)+']', valor, [rfReplaceAll]);
  end;
end;

class function TBloqueioPersonalizadoServ.BuscarBloqueiosVenda: TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM bloqueio_personalizado WHERE bo_venda = ''S'' AND bo_ativo = ''S''');
end;

class function TBloqueioPersonalizadoServ.GerarBloqueioPersonalizado(
    const ABloqueioPersonalizado: TBloqueioPersonalizadoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('BLOQUEIO_PERSONALIZADO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ABloqueioPersonalizado.FDescricao);
    qEntidade.SetAsString('SQL', ABloqueioPersonalizado.FSql);
    qEntidade.SetAsString('BO_VENDA', ABloqueioPersonalizado.FBoVenda);
    qEntidade.SetAsString('BO_NOTA_FISCAL', ABloqueioPersonalizado.FBoNotaFiscal);
    qEntidade.SetAsString('BO_ORDEM_SERVICO', ABloqueioPersonalizado.FBoOrdemServico);
    qEntidade.SetAsString('BO_ATIVO', ABloqueioPersonalizado.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ABloqueioPersonalizado.FID := qEntidade.GetAsInteger('ID');
    result :=     ABloqueioPersonalizado.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar BloqueioPersonalizado.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

{ TBloqueioPersonalizadoUsuarioLiberacaoServ }

class function TBloqueioPersonalizadoUsuarioLiberacaoServ.GetBloqueioPersonalizadoUsuarioLiberacao(
    const AIdBloqueioPersonalizadoUsuarioLiberacao: Integer): TBloqueioPersonalizadoUsuarioLiberacaoProxy;
const
  SQL = 'SELECT * FROM BLOQUEIO_PERSONALIZADO_USUARIO_LIBERACAO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TBloqueioPersonalizadoUsuarioLiberacaoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdBloqueioPersonalizadoUsuarioLiberacao));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FIdBloqueioPersonalizado := qConsulta.GetAsInteger('ID_BLOQUEIO_PERSONALIZADO');
end;

class function TBloqueioPersonalizadoUsuarioLiberacaoServ.GerarBloqueioPersonalizadoUsuarioLiberacao(
    const ABloqueioPersonalizadoUsuarioLiberacao: TBloqueioPersonalizadoUsuarioLiberacaoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('BLOQUEIO_PERSONALIZADO_USUARIO_LIBERACAO');
    qEntidade.Incluir;


    if ABloqueioPersonalizadoUsuarioLiberacao.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', ABloqueioPersonalizadoUsuarioLiberacao.FIdPessoaUsuario);
    end;

    if ABloqueioPersonalizadoUsuarioLiberacao.FIdBloqueioPersonalizado > 0 then
    begin
      qEntidade.SetAsInteger('ID_BLOQUEIO_PERSONALIZADO', ABloqueioPersonalizadoUsuarioLiberacao.FIdBloqueioPersonalizado);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    ABloqueioPersonalizadoUsuarioLiberacao.FID := qEntidade.GetAsInteger('ID');
    result :=     ABloqueioPersonalizadoUsuarioLiberacao.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar BloqueioPersonalizadoUsuarioLiberacao.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
