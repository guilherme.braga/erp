unit uStringUtils;

interface

uses Classes, System.SysUtils, DateUtils, StrUtils;

type TStringUtils = class
  class function IIF(AValue: Boolean; const ATrue, AFalse: Variant): Variant;
  class function OcorrenciaDeTexto(S: String; C: Char): Integer;
  class function RetornarDataNoFormatoMYSQL(AData: TDate): String;
  class function RetornarDoubleNoFormatoMYSQL(AValor: Double): String;
  class function LPad(AString: string; caractere: char; tamanho: integer): String;
  class function RPad(AString: string; caractere: char; tamanho: integer): String;
end;

implementation

class function TStringUtils.LPad(AString: string; caractere: char; tamanho: integer): string;
var i : integer;
begin
  Result := AString;
  if(Length(AString) > tamanho) then exit;
  for i := 1 to (tamanho - Length(AString)) do
    Result := Result + caractere;
end;

class function TStringUtils.RPad(AString: string; caractere: char; tamanho: integer): string;
var i : integer;
begin
  Result := AString;
  if (Length(AString) > tamanho) then exit;

  for i := 1 to (tamanho - Length(AString)) do
    Result := caractere + Result;
end;

class function TStringUtils.IIF(AValue: Boolean; const ATrue, AFalse: Variant): Variant;
begin
  if AValue then
    result := ATrue
  else
    result := AFalse;
end;

class function TStringUtils.OcorrenciaDeTexto(S: String; C: Char): Integer;
var
  N: Integer;
begin
  Result := 0;
  N := 0;
  repeat
    N := PosEx(C, S, N + 1);
    if N <> 0 then
      Inc(Result);
  until N = 0;
end;


class function TStringUtils.RetornarDataNoFormatoMYSQL(AData: TDate): String;
var dia, mes, ano: word;
begin
  DecodeDate(AData, ano, mes, dia);
  result :=  InttoStr(ano)+FormatFloat('00',mes)+FormatFloat('00',dia);

end;

class function TStringUtils.RetornarDoubleNoFormatoMYSQL(
  AValor: Double): String;
begin
  result := StringReplace(FloattoStr(AValor), ',', '.', [rfReplaceAll]);
end;

end.
