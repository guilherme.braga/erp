unit uGeracaoDocumentoServ;

interface

Uses
  Data.DB,
  System.Generics.Collections,
  REST.Json,
  SysUtils,
  uGBFDQuery,Data.FireDACJSONReflect,
  uGeracaoDocumentoProxy,
  uFactoryQuery;

type TGeracaoDocumentoServ = class
  private
    class function GerarContaReceber(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function GerarContaPagar(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
    class function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
    class procedure AtualizarStatusGeracaoDocumento(AIdChaveProcesso: Integer; AStatus: String);
    class function GerarEntradaQuitacaoContaReceber(AGeracaoDocumento: TFastQuery;
      AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function GerarEntradaQuitacaoContaPagar(AGeracaoDocumento: TFastQuery;
      AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function DuplicarRegistroGeracaoDocumento(const AIdGeracaoDocumento: Integer): Integer;
    class procedure DuplicarRegistroGeracaoDocumentoParcela(const AIdGeracaoDocumentoOrigem,
      AIdGeracaoDocumentoDestino: Integer);
  public
    class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    class function GerarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function EstornarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
    class function GetContasReceberAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
    class function GetContasPagarAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
    class function Duplicar(const AIdGeracaoDocumento: Integer): Integer;
end;

implementation

{ TGeracaoDocumentoServ }

uses
  uContaReceberProxy,
  uContaReceberServ,
  uContaPagarProxy,
  uContaPagarServ,
  uChaveProcessoServ,
  uChaveProcessoProxy, uContaCorrenteProxy, uFormaPagamentoServ, uContaCorrenteServ, uDatasetUtilsServ,
  uTipoQuitacaoServ, uTipoQuitacaoProxy;

class function TGeracaoDocumentoServ.Duplicar(const AIdGeracaoDocumento: Integer): Integer;
var
  idGeracaoDocumento: Integer;
begin
  idGeracaoDocumento := 0;
  try
    idGeracaoDocumento := DuplicarRegistroGeracaoDocumento(AIdGeracaoDocumento);
    DuplicarRegistroGeracaoDocumentoParcela(AIdGeracaoDocumento, idGeracaoDocumento);
    result := idGeracaoDocumento;
  except
    on e : Exception do
    begin
      if idGeracaoDocumento < 0 then
      begin
        result := 0;
      end;

      raise Exception.Create('Erro ao duplicar geracao de documento.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TGeracaoDocumentoServ.DuplicarRegistroGeracaoDocumento(
  const AIdGeracaoDocumento: Integer): Integer;
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';

  const CAMPOS_MODIFICAR: Array [0..2] of string = ('dh_cadastro', 'status', 'id_chave_processo');
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM GERACAO_DOCUMENTO WHERE ID = :ID',
    TArray<Variant>.Create(AIdGeracaoDocumento));

  queryDestino := TFastQuery.ModoDeInclusao('GERACAO_DOCUMENTO');

  //Novo Registro em outra conta corrente
  TDatasetUtilsServ.DuplicarRegistro(
    (queryOrigem as TFastQuery), //Origem
    (queryDestino as TFastQuery), //Destino
    [CAMPOS_NAO_COPIAR], //No Copy
    CAMPOS_MODIFICAR, //Campos para mudar
    TArray<Variant>.Create(now, TGeracaoDocumentoProxy.STATUS_ABERTO,
      TChaveProcessoServ.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_GERACAO_DOCUMENTO, 0)) //Valores para Mudar
  );

  queryDestino.Persistir;

  TChaveProcessoServ.AtualizarChaveProcesso(queryDestino.GetAsInteger('ID_CHAVE_PROCESSO'),
    TChaveProcessoProxy.ORIGEM_GERACAO_DOCUMENTO, queryDestino.GetAsInteger('ID'));

  result := queryDestino.GetAsInteger('ID');
end;

class procedure TGeracaoDocumentoServ.DuplicarRegistroGeracaoDocumentoParcela(
  const AIdGeracaoDocumentoOrigem, AIdGeracaoDocumentoDestino: Integer);
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';

  const CAMPOS_MODIFICAR: String = 'id_geracao_documento';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM GERACAO_DOCUMENTO_PARCELA WHERE ID_GERACAO_DOCUMENTO = :ID',
    TArray<Variant>.Create(AIdGeracaoDocumentoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('GERACAO_DOCUMENTO_PARCELA');

  queryOrigem.First;
  while not queryOrigem.Eof do
  begin
    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      [CAMPOS_NAO_COPIAR], //No Copy
      [CAMPOS_MODIFICAR], //Campos para mudar
      TArray<Variant>.Create(AIdGeracaoDocumentoDestino) //Valores para Mudar
    );

    queryOrigem.Next;
  end;

  queryDestino.Persistir;
end;

class function TGeracaoDocumentoServ.GerarDocumento(
  AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
begin
  result := true;

  if AGeracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos = TGeracaoDocumentoProxy.TIPO_CONTARECEBER then
    TGeracaoDocumentoServ.GerarContaReceber(AIdChaveProcesso, AGeracaoDocumentoProxy)
  else if AGeracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos = TGeracaoDocumentoProxy.TIPO_CONTAPAGAR then
    TGeracaoDocumentoServ.GerarContaPagar(AIdChaveProcesso, AGeracaoDocumentoProxy);

  AtualizarStatusGeracaoDocumento(AIdChaveProcesso, TGeracaoDocumentoProxy.STATUS_GERADO);
end;

class function TGeracaoDocumentoServ.GerarEntradaQuitacaoContaPagar(AGeracaoDocumento: TFastQuery;
  AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
var
  contaPagar: TContaPagarMovimento;
  Quitacao       : TContaPagarQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaPagar: Integer;
  tipoQuitacao : String;
begin
  result := false;
  try
    contaPagar := TContaPagarMovimento.Create;
    try
      with contaPagar do
      begin
        FIdPessoa :=            AGeracaoDocumento.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AGeracaoDocumento.GetAsString('ID');
        FDescricao  :=          'Geracao de Documento: '+AGeracaoDocumento.GetAsString('ID');
        FStatus     :=          ABERTO;

        FDtVencimento :=        DatetoStr(Date);
        FDtDocumento  :=        DatetoStr(AGeracaoDocumento.GetAsDate('dh_cadastro'));
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        FVlQuitado :=           0;
        FVlAberto  :=           AGeracaoDocumento.GetAsFloat('Vl_Entrada');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           1;
        FNrParcela :=           1;

        FSequencia  :=          InttoStr(FNrParcela)+'/'+InttoStr(FQtParcela);

        FBoVencido  :=          'N';
        FObservacao :=          'Entrada/Quita��o da Geracao de Documento';

        FIdContaAnalise    :=   AGeracaoDocumento.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AGeracaoDocumento.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   AGeracaoDocumento.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AGeracaoDocumento.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AGeracaoDocumento.GetAsInteger('Id_Forma_Pagamento_Entrada');
        FIdContaCorrente :=     AGeracaoDocumento.GetAsInteger('Id_Conta_Corrente_Entrada');
        FIdCarteira := AGeracaoDocumento.GetAsInteger('Id_Carteira');

        FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaPagarMovimento.ORIGEM_GERACAO_DOCUMENTO;
      end;
      IdContaPagar := TContaPagarServ.GerarContaPagar(TJson.ObjectToJsonString(contaPagar));

      Quitacao  := TContaPagarQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaPagar  := IdContaPagar;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AGeracaoDocumento.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaPagar.FObservacao;
        Quitacao.FIdFilial        := AGeracaoDocumento.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          AGeracaoDocumento.GetAsInteger('ID_FORMA_PAGAMENTO_ENTRADA'));
        Quitacao.FIdContaCorrente := AGeracaoDocumento.GetAsInteger('Id_Conta_Corrente');
        Quitacao.FIdCentroResultado  := contaPagar.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaPagar.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        Quitacao.FVlTotal           := AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_GERACAO_DOCUMENTO;
        Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := AGeracaoDocumento.GetAsString('CHEQUE_SACADO_ENTRADA');
        Quitacao.FChequeDocFederal    := AGeracaoDocumento.GetAsString('CHEQUE_DOC_FEDERAL_ENTRADA');
        Quitacao.FChequeBanco         := AGeracaoDocumento.GetAsString('CHEQUE_BANCO_ENTRADA');
        Quitacao.FChequeDtEmissao     := AGeracaoDocumento.GetAsString('CHEQUE_DT_EMISSAO_ENTRADA');
        Quitacao.FChequeDtVencimento  := AGeracaoDocumento.GetAsString('CHEQUE_DT_VENCIMENTO_ENTRADA');
        Quitacao.FChequeAgencia       := AGeracaoDocumento.GetAsString('CHEQUE_AGENCIA_ENTRADA');
        Quitacao.FChequeContaCorrente := AGeracaoDocumento.GetAsString('CHEQUE_CONTA_CORRENTE_ENTRADA');
        Quitacao.FChequeNumero        := AGeracaoDocumento.GetAsInteger('CHEQUE_NUMERO_ENTRADA');

        //Dados do Cart�o
        if AGeracaoDocumento.GetAsInteger('ID_OPERADORA_CARTAO_ENTRADA') > 0 then
        begin
          Quitacao.FIdOperadoraCartao := AGeracaoDocumento.GetAsInteger('ID_OPERADORA_CARTAO_ENTRADA');
        end;

        TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaPagarServ.AtualizarValores(IdContaPagar);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AGeracaoDocumento.GetAsString('ID');
          Movimento.FDescricao         := contaPagar.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;

          //Cr�dito - Porque � originado de um conta a pagar
          Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;

          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaPagar.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaPagar.FIdChaveProcesso;
          Movimento.FIdFilial          := contaPagar.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
          Movimento.FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');

          //Se for cheque a concilia��o � falsa
          tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(
            Quitacao.FIdTipoQuitacao);
          if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
            (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
          begin
            Movimento.FBoCheque := 'S';

            if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO then
            begin
              Movimento.FBoChequeTerceiro := 'S';
            end;

            if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO then
            begin
              Movimento.FBoChequeTerceiro := 'S';//Sempre � de terceiro qdo � conta a receber
            end;

            Movimento.FBoConciliado := 'N';

            //Dados do Cheque
            Movimento.FChequeSacado        := Quitacao.FChequeSacado;
            Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
            Movimento.FChequeBanco         := Quitacao.FChequeBanco;
            Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
            Movimento.FChequeDtVencimento  := Quitacao.FChequeDtVencimento;
            Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
            Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
            Movimento.FChequeNumero        := Quitacao.FChequeNumero;
          end
          else
          begin
            Movimento.FBoConciliado := 'N';
          end;

          //Dados do Cart�o
          if Quitacao.FIdOperadoraCartao > 0 then
          begin
            Movimento.FIdOperadoraCartao := Quitacao.FIdOperadoraCartao;
          end;

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaPagar.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a pagar.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TGeracaoDocumentoServ.GerarEntradaQuitacaoContaReceber(AGeracaoDocumento: TFastQuery;
  AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
  tipoQuitacao: String;
begin
  result := false;
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            AGeracaoDocumento.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AGeracaoDocumento.GetAsString('ID');
        FDescricao  :=          'Geracao de Documento: '+AGeracaoDocumento.GetAsString('ID');
        FStatus     :=          ABERTO;

        FDtVencimento :=        DatetoStr(Date);
        FDtDocumento  :=        DatetoStr(AGeracaoDocumento.GetAsDate('dh_cadastro'));
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        FVlQuitado :=           0;
        FVlAberto  :=           AGeracaoDocumento.GetAsFloat('Vl_Entrada');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           1;
        FNrParcela :=           1;

        FSequencia  :=          InttoStr(FNrParcela)+'/'+InttoStr(FQtParcela);

        FBoVencido  :=          'N';
        FObservacao :=          'Entrada/Quita��o da Geracao de Documento';

        FIdContaAnalise    :=   AGeracaoDocumento.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AGeracaoDocumento.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   AGeracaoDocumento.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AGeracaoDocumento.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AGeracaoDocumento.GetAsInteger('Id_Forma_Pagamento_Entrada');
        FIdContaCorrente :=     AGeracaoDocumento.GetAsInteger('Id_Conta_Corrente_Entrada');
        FIdCarteira := AGeracaoDocumento.GetAsInteger('Id_Carteira');

        FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_GERACAO_DOCUMENTO;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AGeracaoDocumento.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := AGeracaoDocumento.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          AGeracaoDocumento.GetAsInteger('ID_FORMA_PAGAMENTO_ENTRADA'));
        Quitacao.FIdContaCorrente := AGeracaoDocumento.GetAsInteger('Id_Conta_Corrente');
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        Quitacao.FVlTotal           := AGeracaoDocumento.GetAsFloat('Vl_Entrada');
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_GERACAO_DOCUMENTO;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := AGeracaoDocumento.GetAsString('CHEQUE_SACADO_ENTRADA');
        Quitacao.FChequeDocFederal    := AGeracaoDocumento.GetAsString('CHEQUE_DOC_FEDERAL_ENTRADA');
        Quitacao.FChequeBanco         := AGeracaoDocumento.GetAsString('CHEQUE_BANCO_ENTRADA');
        Quitacao.FChequeDtEmissao     := AGeracaoDocumento.GetAsString('CHEQUE_DT_EMISSAO_ENTRADA');
        Quitacao.FChequeDtVencimento  := AGeracaoDocumento.GetAsString('CHEQUE_DT_VENCIMENTO_ENTRADA');
        Quitacao.FChequeAgencia       := AGeracaoDocumento.GetAsString('CHEQUE_AGENCIA_ENTRADA');
        Quitacao.FChequeContaCorrente := AGeracaoDocumento.GetAsString('CHEQUE_CONTA_CORRENTE_ENTRADA');
        Quitacao.FChequeNumero        := AGeracaoDocumento.GetAsInteger('CHEQUE_NUMERO_ENTRADA');

        //Dados do Cart�o
        if AGeracaoDocumento.GetAsInteger('ID_OPERADORA_CARTAO_ENTRADA') > 0 then
        begin
          Quitacao.FIdOperadoraCartao := AGeracaoDocumento.GetAsInteger('ID_OPERADORA_CARTAO_ENTRADA');
        end;

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AGeracaoDocumento.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;

          //Cr�dito - Porque � originado de um conta a receber
          Movimento.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;

          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_VENDA;
          Movimento.FIdDocumentoOrigem := AGeracaoDocumento.GetAsInteger('ID');

           //Se for cheque a concilia��o � falsa
          tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(
            Quitacao.FIdTipoQuitacao);
          if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
            (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
          begin
            Movimento.FBoCheque := 'S';

            if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO then
            begin
              Movimento.FBoChequeTerceiro := 'S';
            end;

            if tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO then
            begin
              Movimento.FBoChequeTerceiro := 'S';//Sempre � de terceiro qdo � conta a receber
            end;

            Movimento.FBoConciliado := 'N';

            //Dados do Cheque
            Movimento.FChequeSacado        := Quitacao.FChequeSacado;
            Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
            Movimento.FChequeBanco         := Quitacao.FChequeBanco;
            Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
            Movimento.FChequeDtVencimento  := Quitacao.FChequeDtVencimento;
            Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
            Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
            Movimento.FChequeNumero        := Quitacao.FChequeNumero;
          end
          else
          begin
            Movimento.FBoConciliado := 'N';
          end;

          //Dados do Cart�o
          if Quitacao.FIdOperadoraCartao > 0 then
          begin
            Movimento.FIdOperadoraCartao := Quitacao.FIdOperadoraCartao;
          end;

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TGeracaoDocumentoServ.GetContasPagarAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
var qConsulta: IFastQuery;
    SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select *'+
  ' from conta_pagar'+
  ' where status = :status'+
  ' and id_chave_processo = :id_chave_processo';

  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
  TContaPagarMovimento.ABERTO, AIdChaveProcesso));

  TFDJSONDataSetsWriter.ListAdd(result, (qConsulta as TFastQuery));
end;

class function TGeracaoDocumentoServ.GetContasReceberAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
var qConsulta: IFastQuery;
    SQL: String;
begin
  result := TFDJSONDataSets.Create;

  SQL :=
  ' select *'+
  ' from conta_receber'+
  ' where status = :status'+
  ' and id_chave_processo = :id_chave_processo';

  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
  TContaReceberMovimento.ABERTO, AIdChaveProcesso));

  TFDJSONDataSetsWriter.ListAdd(result, (qConsulta as TFastQuery));
end;

class procedure TGeracaoDocumentoServ.AtualizarStatusGeracaoDocumento(
  AIdChaveProcesso: Integer; AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE geracao_documento SET status = :status where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class function TGeracaoDocumentoServ.EstornarContaPagar(
  AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);
end;

class function TGeracaoDocumentoServ.EstornarContaReceber(
  AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);
end;

class function TGeracaoDocumentoServ.EstornarDocumento(
  AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
begin
  result := true;

  if AGeracaoDocumentoProxy.FGerarContraPartidaContaCorrenteMovimento then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrente(AIdChaveProcesso);
    end
  else
  begin
    TContaCorrenteServ.RemoverMovimentoContaCorrente(AIdChaveProcesso);
  end;

  if AGeracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos = TGeracaoDocumentoProxy.TIPO_CONTARECEBER then
    TGeracaoDocumentoServ.EstornarContaReceber(AIdChaveProcesso)
  else if AGeracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos = TGeracaoDocumentoProxy.TIPO_CONTAPAGAR then
    TGeracaoDocumentoServ.EstornarContaPagar(AIdChaveProcesso);

  AtualizarStatusGeracaoDocumento(AIdChaveProcesso, TGeracaoDocumentoProxy.STATUS_CANCELADO);
end;

class function TGeracaoDocumentoServ.GerarContaPagar(
  AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
var contaPagar: TContaPagarMovimento;
    objectContaPagarJSON: String;
    qGeracaoDocumento, qGeracaoDocumentoParcela: IFastQuery;
begin
  result := true;

  qGeracaoDocumento := TFastQuery.ModoDeConsulta(
    'SELECT * FROM geracao_documento where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  qGeracaoDocumentoParcela := TFastQuery.ModoDeConsulta(
    'SELECT * FROM geracao_documento_parcela where id_geracao_documento = :id_geracao_documento',
    TArray<Variant>.Create(qGeracaoDocumento.GetAsInteger('id')));

  if qGeracaoDocumento.GetAsFloat('Vl_Entrada') > 0 then
  begin
    TGeracaoDocumentoServ.GerarEntradaQuitacaoContaPagar((qGeracaoDocumento as TFastQuery), AGeracaoDocumentoProxy);
  end;

  while not qGeracaoDocumentoParcela.Eof do
  begin
    contaPagar := TContaPagarMovimento.Create;
    try
      with contaPagar do
      begin
        FIdPessoa   := qGeracaoDocumento.GetAsInteger('Id_Pessoa');
        FDocumento  := qGeracaoDocumento.GetAsString('Documento');
        FDescricao  := qGeracaoDocumento.GetAsString('Descricao');
        FObservacao := qGeracaoDocumento.GetAsString('Observacao');
        FStatus     := ABERTO;

        FDtVencimento  := qGeracaoDocumentoParcela.GetAsString('Dt_Vencimento');
        FDtCompetencia := qGeracaoDocumentoParcela.GetAsString('dt_competencia');
        FDtDocumento   := qGeracaoDocumentoParcela.GetAsString('dt_documento');
        FDhCadastro    := DateTimeToStr(Now);

        FVlTitulo  := qGeracaoDocumentoParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado := 0;
        FVlAberto  := qGeracaoDocumentoParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo      := 0;
        FVlDecrescimo     := 0;
        FVlQuitadoLiquido := 0;


        FQtParcela :=           qGeracaoDocumento.GetAsInteger('QT_PARCELAS');
        FNrParcela :=           qGeracaoDocumentoParcela.GetAsInteger('Nr_Parcela');

        FSequencia := qGeracaoDocumentoParcela.GetAsString('Nr_Parcela')+'/'+
          qGeracaoDocumentoParcela.GetAsString('total_parcelas');

        FBoVencido :='N';

        FIdContaAnalise    := qGeracaoDocumentoParcela.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado := qGeracaoDocumentoParcela.GetAsInteger('Id_Centro_Resultado');
        FIdContaCorrente   := qGeracaoDocumentoParcela.GetAsInteger('ID_CONTA_CORRENTE');
        FIdCarteira   := qGeracaoDocumento.GetAsInteger('ID_CARTEIRA');

        FIdChaveProcesso  := qGeracaoDocumento.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento := qGeracaoDocumento.GetAsInteger('Id_Forma_Pagamento');

        FIdFilial := qGeracaoDocumento.GetAsInteger('Id_Filial');

        FIdDocumentoOrigem := qGeracaoDocumento.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaPagarMovimento.ORIGEM_GERACAO_DOCUMENTO;

      end;
      objectContaPagarJSON := TJson.ObjectToJsonString(contaPagar);
      TContaPagarServ.GerarContaPagar(objectContaPagarJSON);

      TChaveProcessoServ.AtualizarChaveProcesso(contaPagar.FIdChaveProcesso,
                                                TChaveProcessoProxy.ORIGEM_GERACAO_DOCUMENTO,
                                                qGeracaoDocumentoParcela.GetAsInteger('ID'));

    finally
      contaPagar.Free;
    end;

    qGeracaoDocumentoParcela.Proximo;
  end;
end;

class function TGeracaoDocumentoServ.GerarContaReceber(
  AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy): Boolean;
var contaReceber: TContaReceberMovimento;
    objectContaReceberJSON: String;
    qGeracaoDocumento, qGeracaoDocumentoParcela: IFastQuery;
begin
  result := true;

  qGeracaoDocumento := TFastQuery.ModoDeConsulta(
    'SELECT * FROM geracao_documento where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  qGeracaoDocumentoParcela := TFastQuery.ModoDeConsulta(
    'SELECT * FROM geracao_documento_parcela where id_geracao_documento = :id_geracao_documento',
    TArray<Variant>.Create(qGeracaoDocumento.GetAsInteger('id')));

  if qGeracaoDocumento.GetAsFloat('Vl_Entrada') > 0 then
  begin
    TGeracaoDocumentoServ.GerarEntradaQuitacaoContaReceber((qGeracaoDocumento as TFastQuery), AGeracaoDocumentoProxy);
  end;

  while not qGeracaoDocumentoParcela.Eof do
  begin
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa   := qGeracaoDocumento.GetAsInteger('Id_Pessoa');

        FDocumento  := qGeracaoDocumento.GetAsString('Documento');
        FDescricao  := qGeracaoDocumento.GetAsString('Descricao');
        FObservacao := qGeracaoDocumento.GetAsString('Observacao');
        FStatus     := ABERTO;

        FDtVencimento  := qGeracaoDocumentoParcela.GetAsString('Dt_Vencimento');
        FDtCompetencia := qGeracaoDocumentoParcela.GetAsString('dt_competencia');
        FDtDocumento   := qGeracaoDocumentoParcela.GetAsString('dt_documento');
        FDhCadastro    := DateTimeToStr(Now);

        FVlTitulo  := qGeracaoDocumentoParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado := 0;
        FVlAberto  := qGeracaoDocumentoParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo      := 0;
        FVlDecrescimo     := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela := qGeracaoDocumento.GetAsInteger('QT_PARCELAS');
        FNrParcela := qGeracaoDocumentoParcela.GetAsInteger('Nr_Parcela');

        FSequencia := qGeracaoDocumentoParcela.GetAsString('Nr_Parcela')+'/'+
          qGeracaoDocumentoParcela.GetAsString('total_parcelas');

        FBoVencido := 'N';

        FIdContaAnalise    := qGeracaoDocumentoParcela.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado := qGeracaoDocumentoParcela.GetAsInteger('Id_Centro_Resultado');
        FIdContaCorrente   := qGeracaoDocumentoParcela.GetAsInteger('ID_CONTA_CORRENTE');

        FIdCarteira   := qGeracaoDocumento.GetAsInteger('ID_CARTEIRA');

        FIdChaveProcesso  := qGeracaoDocumento.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento := qGeracaoDocumento.GetAsInteger('Id_Forma_Pagamento');

        FIdFilial := qGeracaoDocumento.GetAsInteger('Id_Filial');

        FIdDocumentoOrigem := qGeracaoDocumento.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_GERACAO_DOCUMENTO;

      end;

      objectContaReceberJSON := TJson.ObjectToJsonString(contaReceber);
      TContaReceberServ.GerarContaReceber(objectContaReceberJSON);

      TChaveProcessoServ.AtualizarChaveProcesso(contaReceber.FIdChaveProcesso,
                                                TChaveProcessoProxy.ORIGEM_GERACAO_DOCUMENTO,
                                                qGeracaoDocumentoParcela.GetAsInteger('ID'));

    finally
      contaReceber.Free;
    end;

    qGeracaoDocumentoParcela.Proximo;
  end;
end;

class function TGeracaoDocumentoServ.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
var
  existeQuitacao: boolean;
  QuitacaoReferenteEntradaQuitacao: Boolean;
  qDocumento: IFastQuery;
begin
  result := true;

  qDocumento := TFastQuery.ModoDeConsulta(
    ' SELECT vl_entrada, tipo FROM geracao_documento where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  if (qDocumento.GetAsString('tipo') = TGeracaoDocumentoProxy.TIPO_CONTARECEBER) then
  begin
    existeQuitacao := TContaReceberServ.ExisteQuitacao(AIdChaveProcesso);

    if existeQuitacao then
    begin
      QuitacaoReferenteEntradaQuitacao :=
        TContaReceberServ.TotalQuitacaoRealizada(AIdChaveProcesso) =
        qDocumento.GetAsFloat('vl_entrada');

      result := QuitacaoReferenteEntradaQuitacao;
    end
  end
  else if (qDocumento.GetAsString('tipo') = TGeracaoDocumentoProxy.TIPO_CONTAPAGAR) then
  begin
    existeQuitacao := TContaPagarServ.ExisteQuitacao(AIdChaveProcesso);

    if existeQuitacao then
    begin
      QuitacaoReferenteEntradaQuitacao :=
        TContaPagarServ.TotalQuitacaoRealizada(AIdChaveProcesso) =
        qDocumento.GetAsFloat('vl_entrada');

      result := QuitacaoReferenteEntradaQuitacao;
    end
  end;
end;

class function TGeracaoDocumentoServ.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var qDocumento: IFastQuery;
    qContaReceber: IFastQuery;
    qContaPagar: IFastQuery;
begin
  result := TGeracaoDocumentoServ.PodeEstornar(AIdChaveProcesso);
end;

end.
