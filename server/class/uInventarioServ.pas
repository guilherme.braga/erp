unit uInventarioServ;

Interface

Uses uInventarioProxy, Generics.Collections, uFactoryQuery;

type TInventarioServ = class
  class function GetInventario(
    const AIdInventario: Integer): TInventarioProxy;

  class function GetListaProdutos(const AIdInventario: Integer):
    TList<TInventarioProdutoProxy>;

  class function GetQueryInventario(AIdChaveProcesso: Integer): TFastQuery;
  class function GetQueryInventarioItem(AIdInventario: Integer): TFastQuery;

  class function GerarInventario(
    const AInventario: TInventarioProxy): Integer;

  //Efetivar o Iventario
  class function Efetivar(AIdChaveProcesso: Integer): Boolean;

  //Reabrir o Iventario
  class function Reabrir(AIdChaveProcesso: Integer): Boolean;

  //Atualizar o Ajuste do Inventario
  class procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
end;

implementation

{ TInventarioServ }

uses SysUtils, uProdutoServ, uAjusteEstoqueProxy, uAjusteEstoqueServ;

class function TInventarioServ.GetInventario(
    const AIdInventario: Integer): TInventarioProxy;
const
  SQL = 'SELECT * FROM inventario WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TInventarioProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdInventario));

  if not qConsulta.IsEmpty then
  begin
    result.FId := qConsulta.GetAsInteger('ID');
    result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
    result.FTipo := qConsulta.GetAsString('TIPO');
    result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
    result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
    result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
    result.FDhInicio := qConsulta.GetAsString('DH_INICIO');
    result.FDhFim := qConsulta.GetAsString('DH_FIM');
    result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
    result.FInventarioProduto := GetListaProdutos(result.FId);
  end;
end;

class function TInventarioServ.GetListaProdutos(
  const AIdInventario: Integer): TList<TInventarioProdutoProxy>;
const
  SQL = 'SELECT * FROM inventario_produto WHERE id_inventario = :id ';
var
   qConsulta: IFastQuery;
   itemInventario: TInventarioProdutoProxy;
begin
  result := TList<TInventarioProdutoProxy>.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdInventario));

  qConsulta.Primeiro;
  if not qConsulta.IsEmpty then
  begin
    itemInventario := TInventarioProdutoProxy.Create;

    itemInventario.FId := qConsulta.GetAsInteger('ID');
    itemInventario.FIdInventario := qConsulta.GetAsInteger('ID_INVENTARIO');
    itemInventario.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');

    result.Add(itemInventario);

    qConsulta.Proximo;
  end;
end;

class function TInventarioServ.GetQueryInventario(
  AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM inventario WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TInventarioServ.GetQueryInventarioItem(
  AIdInventario: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM inventario_produto WHERE id_inventario = :id_inventario',
    TArray<Variant>.Create(AIdInventario));
end;

class procedure TInventarioServ.AtualizarStatus(AIdChaveProcesso: Integer;
  AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE inventario SET status = :status, dh_fechamento = null where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class function TInventarioServ.Efetivar(AIdChaveProcesso: Integer): Boolean;
var
  qInventario,
  qInventarioItem: TFastQuery;
  ajusteEstoque: TAjusteEstoqueProxy;
  itemAjusteEstoque: TAjusteEstoqueItemProxy;
  tipoAjuste: String;
  qtdeAjuste: Double;
begin
  result := true;

  qInventario := TInventarioServ.GetQueryInventario(AIdChaveProcesso);
  try
    if qInventario.IsEmpty then
      exit;

    qInventarioItem := TInventarioServ.GetQueryInventarioItem(
      qInventario.GetAsInteger('ID'));
    try
      ajusteEstoque := TAjusteEstoqueProxy.Create;
      try
        ajusteEstoque.FDhCadastro := qInventario.GetAsString('DH_CADASTRO');
        ajusteEstoque.FDhFechamento := qInventario.GetAsString('DH_FECHAMENTO');
        ajusteEstoque.FStatus := qInventario.GetAsString('STATUS');
        ajusteEstoque.FIdFilial := qInventario.GetAsInteger('ID_FILIAL');
        ajusteEstoque.FIdPessoaUsuario := qInventario.GetAsInteger('ID_PESSOA_USUARIO');
        ajusteEstoque.FIdMotivo := qInventario.GetAsInteger('ID_MOTIVO');;
        ajusteEstoque.FIdChaveProcesso := qInventario.GetAsInteger('ID_CHAVE_PROCESSO');

        qInventarioItem.Primeiro;
        while not qInventarioItem.Eof do
        begin
          qtdeAjuste := 0;
          case qInventario.GetAsInteger('CONTAGEM_ESCOLHIDA') of
            1:
            begin
              if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') >
                qInventarioItem.GetAsFloat('PRIMEIRA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('PRIMEIRA_CONTAGEM');
                tipoAjuste := TAjusteEstoqueProxy.TIPO_SAIDA;
              end
              else  if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') <
                qInventarioItem.GetAsFloat('PRIMEIRA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('PRIMEIRA_CONTAGEM');

                qtdeAjuste := -1 * qtdeAjuste;
                tipoAjuste := TAjusteEstoqueProxy.TIPO_ENTRADA;
              end;
            end;
            2:
            begin
              if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') >
                qInventarioItem.GetAsFloat('SEGUNDA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('SEGUNDA_CONTAGEM');
                tipoAjuste := TAjusteEstoqueProxy.TIPO_SAIDA;
              end
              else  if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') <
                qInventarioItem.GetAsFloat('SEGUNDA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('SEGUNDA_CONTAGEM');

                qtdeAjuste := -1 * qtdeAjuste;
                tipoAjuste := TAjusteEstoqueProxy.TIPO_ENTRADA;
              end;
            end;
            3:
            begin
              if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') >
                qInventarioItem.GetAsFloat('TERCEIRA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('TERCEIRA_CONTAGEM');
                tipoAjuste := TAjusteEstoqueProxy.TIPO_SAIDA;
              end
              else  if qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') <
                qInventarioItem.GetAsFloat('TERCEIRA_CONTAGEM') then
              begin
                qtdeAjuste := qInventarioItem.GetAsFloat('QUANTIDADE_ESTOQUE') -
                  qInventarioItem.GetAsFloat('TERCEIRA_CONTAGEM');

                qtdeAjuste := -1 * qtdeAjuste;
                tipoAjuste := TAjusteEstoqueProxy.TIPO_ENTRADA;
              end;
            end;
          end;

          if qtdeAjuste > 0 then
          begin
            itemAjusteEstoque := TAjusteEstoqueItemProxy.Create;
            itemAjusteEstoque.FQuantidade := qtdeAjuste;
            itemAjusteEstoque.FIdProduto := qInventarioItem.GetAsInteger('ID_PRODUTO');
            itemAjusteEstoque.FNrItem := ajusteEstoque.FAjusteEstoqueItem.Count+1;
            itemAjusteEstoque.FTipo := tipoAjuste;
            ajusteEstoque.FAjusteEstoqueItem.Add(itemAjusteEstoque);
          end;

          qInventarioItem.Proximo;
        end;
        TAjusteEstoqueServ.GerarAjusteEstoque(ajusteEstoque);
      finally
        FreeAndNil(ajusteEstoque);
      end;
    finally
      FreeAndNil(qInventarioItem);
    end;
  finally
    FreeAndNil(qInventario);
  end;
end;

class function TInventarioServ.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  try
    TAjusteEstoqueServ.Cancelar(AIdChaveProcesso);

    TInventarioServ.AtualizarStatus(AIdChaveProcesso,TInventarioProxy.STATUS_ABERTO);
    TAjusteEstoqueServ.RemoverAjusteEstoquePelaChaveProcesso(AIdChaveProcesso);
  Except
    result := false;
    raise Exception.Create('Falha ao tentar Reabrir o inventário');
  end;
end;

class function TInventarioServ.GerarInventario(
    const AInventario: TInventarioProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('inventario');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', AInventario.FDhCadastro);
    qEntidade.SetAsString('TIPO', AInventario.FTipo);

    if AInventario.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AInventario.FIdFilial);
    end;

    if AInventario.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', AInventario.FIdPessoaUsuario);
    end;

    if AInventario.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AInventario.FIdChaveProcesso);
    end;
    qEntidade.SetAsString('DH_INICIO', AInventario.FDhInicio);
    qEntidade.SetAsString('DH_FIM', AInventario.FDhFim);
    qEntidade.SetAsString('OBSERVACAO', AInventario.FObservacao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AInventario.FID := qEntidade.GetAsInteger('ID');
    result :=     AInventario.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Inventario.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


