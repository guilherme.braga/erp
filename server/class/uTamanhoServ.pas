unit uTamanhoServ;

Interface

Uses uTamanhoProxy, db, SysUtils;

type TTamanhoServ = class
  class function GetTamanho(const AIdTamanho: Integer): TTamanhoProxy;
  class function GerarTamanho(const ATamanho: TTamanhoProxy): Integer;
  class function ExisteTamanho(const ADescricaoTamanho: String): Boolean;
  class function GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
end;

implementation

{ TTamanhoServ }

uses uFactoryQuery;

class function TTamanhoServ.GetTamanho(
    const AIdTamanho: Integer): TTamanhoProxy;
const
  SQL = 'SELECT * FROM TAMANHO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TTamanhoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdTamanho));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

class function TTamanhoServ.ExisteTamanho(const ADescricaoTamanho: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from tamanho where descricao = :tamanho',
    TArray<Variant>.Create(ADescricaoTamanho));

  result := not qEntidade.IsEmpty;
end;

class function TTamanhoServ.GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from tamanho where descricao = :tamanho',
    TArray<Variant>.Create(ADescricaoTamanho));

  result := qEntidade.GetAsInteger('id');
end;

class function TTamanhoServ.GerarTamanho(
    const ATamanho: TTamanhoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('TAMANHO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ATamanho.FDescricao);
    qEntidade.SetAsString('BO_ATIVO', ATamanho.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ATamanho.FID := qEntidade.GetAsInteger('ID');
    result := ATamanho.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Tamanho.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


