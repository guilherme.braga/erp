unit uCfopServ;

Interface

Uses uCfopProxy, SysUtils;

type TCfopServ = class
  class function GetCfop(const AIdCfop: Integer): TCfopProxy;
  class function GerarCfop(const ACfop: TCfopProxy): Integer;
end;

implementation

{ TCfopServ }

uses uFactoryQuery;

class function TCfopServ.GetCfop(
    const AIdCfop: Integer): TCfopProxy;
const
  SQL = 'SELECT * FROM CFOP WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCfopProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCfop));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FCodigo := qConsulta.GetAsInteger('CODIGO');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
end;

class function TCfopServ.GerarCfop(
    const ACfop: TCfopProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('CFOP');
    qEntidade.Incluir;

    qEntidade.SetAsInteger('CODIGO', ACfop.FCodigo);
    qEntidade.SetAsString('DESCRICAO', ACfop.FDescricao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ACfop.FID := qEntidade.GetAsInteger('ID');
    result :=     ACfop.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Cfop.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
