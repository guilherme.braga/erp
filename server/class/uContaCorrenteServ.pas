unit uContaCorrenteServ;

interface

uses
  FireDAC.Comp.Client,
  uGBFDQuery,
  System.Generics.Collections,
  uContaCorrenteProxy,
  uContaPagarProxy,
  uContaReceberProxy,
  SysUtils,
  uFactoryQuery;

type TContaCorrenteServ = class
  private
    class function GetContaCorrenteMovimentoPorChaveProcesso(AIdChaveProcesso: Integer): TFastQuery;
    class function BuscarCadastroChequeReferenteMovimentacao(AIdMovimentacao: Integer): TFastQuery;
    class procedure ProcessarChequeDeUmaMovimentacaoEstornada(
      const AIdContaCorrenteMovimento: Integer; const AChequeProprio: Boolean; ATipoMovimentacao: String);
  public
    class function GetContaCorrenteMovimento(AIdContaCorrenteMovimento: Integer): TFastQuery;
    class function GetContaCorrenteMovimentos(AIdsContaCorrenteMovimento: String): TFastQuery;
    class procedure SetContaCorrente(AQuery: TgbFDQuery; ASomenteAtivo: Boolean);
    class procedure AtualizarSaldoMovimentacao(AIdContaCorrente: Integer);
    class procedure AtualizarIDDestinoNaOrigemDaTransferencia(const AIdMovimentoOrigem, AIdMovimentoDestino: Integer);
    class function RealizarTransferencia(const AIdMovimentoOrigem, AIdContaCorrenteDestino: Integer): integer;
    class function RegistroEstaConciliado(const AIdMovimentacao: Integer): boolean;
    class procedure ConciliarRegistro(const AIdMovimentacao: Integer);
    class procedure DesconciliarRegistro(const AIdMovimentacao: Integer);
    class function IsTransferencia(ADataset: TFDQuery): Boolean;
    class function RemoverMovimentoContaCorrente(AChaveProcesso: Integer): Boolean;
    class function RemoverMovimentoContaCorrentePorID(AId: Integer): Boolean;
    class function GerarContrapartidaContaCorrentePorID(AId: Integer; AExcluirCheque: Boolean = true): Boolean;
    class function GerarContraPartidaContaCorrente(AChaveProcesso: Integer; AExcluirCheque: Boolean = true): Boolean;
    class function GerarMovimentoContaCorrente(AMovimento: String): Boolean;
    class function AtualizarMovimento(AMovimento : TContaCorrenteMovimento) : Boolean;
    class function GerarDocumento(AMovimento: String): Boolean;
    class function CadastrarCheque(AMovimento: TContaCorrenteMovimento): Integer;
    class function GetContaCorrenteMovimentoProxy(
      const AIdContaCorrenteMovimento: Integer): TContaCorrenteMovimento;
    class function GerarContaCorrenteMovimento(
      const AContaCorrenteMovimento: TContaCorrenteMovimento): Integer;
    class procedure AmortizarProcessos(const AIdChaveProcesso: Integer);
    class function GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
      const ADtMovimento: String): Double;
end;

implementation

{ TContaCorrenteServ }

uses
  uDatasetUtilsServ,
  uContaPagarServ,
  uContaReceberServ,
  REST.Json, uFilialServ, uChaveProcessoProxy, uChequeProxy, uChequeServ, uStringUtils;

class function TContaCorrenteServ.AtualizarMovimento(AMovimento : TContaCorrenteMovimento): Boolean;
var
  query : IFastQuery;
begin
  Result := True;
  try
    query := TFastQuery.ModoDeAlteracao(
    '      SELECT ccm.*'+
    '        FROM conta_corrente_movimento ccm'+
    '       WHERE id = :id'
    , TArray<Variant>.Create(AMovimento.Fid));
    query.Alterar;
    query.SetAsString('dh_cadastro', AMovimento.FDhCadastro);
    query.SetAsString('dt_emissao', AMovimento.FDtEmissao);
    query.SetAsString('dt_movimento', AMovimento.FDtMovimento);
    query.SetAsString('dt_competencia', AMovimento.FDtCompetencia);
    query.SetAsFloat('vl_movimento', AMovimento.FVlMovimento);
    query.SetAsInteger('id_conta_corrente', AMovimento.FIdContaCorrente);
    query.Salvar;
    query.Persistir;
  except
    Result := False;
  end;
end;

class procedure TContaCorrenteServ.AtualizarSaldoMovimentacao(AIdContaCorrente: Integer);
var
  ID_PLANO_CONTA_ARQUIVAMENTO: Integer;
  VL_MOVIMENTO: Double;
  BO_CONCILIADO: String;
  SALDO_CONCILIADO: Double;
  SALDO_GERAL: Double;
  TP_MOVIMENTO: String;
  VL_SALDO_CONCILIADO_MOVIMENTO: Double;
  VL_SALDO_GERAL_MOVIMENTO: Double;
  queryProcessamento: IFastQuery;
begin
  saldo_geral := 0;
  SALDO_CONCILIADO := 0;

  queryProcessamento := TFastQuery.ModoDeAlteracao(
    '      select ccm.id'+
    '           , ccm.vl_movimento'+
    '           , ccm.bo_conciliado'+
    '           , ccm.tp_movimento'+
    '           , ccm.id_conta_analise'+
    '           , ccm.vl_saldo_conciliado'+
    '           , ccm.vl_saldo_geral'+
    '        from conta_corrente_movimento ccm'+
    '       where id_conta_corrente = :id_conta_corrente'+
    '             and ccm.dt_movimento >= 20140101'+
    '    order by ccm.dt_movimento, ccm.id, ccm.documento', TArray<Variant>.Create(AIdContaCorrente));

  while not queryProcessamento.Eof do
  begin
    vl_movimento := queryProcessamento.GetAsFloat('vl_movimento');
    bo_conciliado := queryProcessamento.GetAsString('bo_conciliado');
    tp_movimento := queryProcessamento.GetAsString('tp_movimento');
    ID_PLANO_CONTA_ARQUIVAMENTO := queryProcessamento.GetAsInteger('id_conta_analise');
    VL_SALDO_CONCILIADO_MOVIMENTO := queryProcessamento.GetAsFloat('vl_saldo_conciliado');
    VL_SALDO_GERAL_MOVIMENTO := queryProcessamento.GetAsFloat('vl_saldo_geral');

    if (id_plano_conta_arquivamento = 167) then
    begin
      saldo_conciliado := vl_saldo_conciliado_movimento;
      saldo_geral := vl_saldo_geral_movimento;
    end
    else
    begin
        if (tp_movimento = 'CREDITO') then
        begin
          if (bo_conciliado = 'S') then
            saldo_conciliado := saldo_conciliado + vl_movimento;

          saldo_geral := saldo_geral + vl_movimento;
        end
        else
        begin
          if (bo_conciliado = 'S') then
            saldo_conciliado := saldo_conciliado - vl_movimento;

          saldo_geral := saldo_geral - vl_movimento;
        end
    end;

    queryProcessamento.Alterar;
    queryProcessamento.SetAsFloat('vl_saldo_geral', saldo_geral);
    queryProcessamento.SetAsFloat('vl_saldo_conciliado', saldo_conciliado);
    queryProcessamento.Salvar;

    queryProcessamento.Proximo;
  end;
  queryProcessamento.Persistir;
end;

class function TContaCorrenteServ.CadastrarCheque(
  AMovimento: TContaCorrenteMovimento): Integer;
var
  qCheque: TFastQuery;
  cheque: TChequeProxy;
  chequeProprioUtilizadoParaPagamento: Boolean;
  chequeDeTerceiroUtilizadoParaPagamento: Boolean;
  chequeDeRecebimento: Boolean;
  novoCheque: boolean;
  chequeExistente: boolean;
begin
  try
    chequeProprioUtilizadoParaPagamento := AMovimento.FBoChequeProprio.Equals('S') and
        AMovimento.FTPMovimento.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO);

    chequeDeTerceiroUtilizadoParaPagamento := AMovimento.FBoChequeTerceiro.Equals('S') and
      AMovimento.FTPMovimento.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO);

    chequeDeRecebimento := AMovimento.FTPMovimento.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO);

    novoCheque := chequeProprioUtilizadoParaPagamento or chequeDeRecebimento;

    chequeExistente := chequeDeTerceiroUtilizadoParaPagamento;

    if novoCheque then
    begin
      cheque := TChequeProxy.Create;
      try
        cheque.FIdContaCorrenteMovimento := AMovimento.FId;
        cheque.FSacado := AMovimento.FChequeSacado;
        cheque.FDocFederal := AMovimento.FChequeDocFederal;
        cheque.FVlCheque := AMovimento.FVlMovimento;
        cheque.FNumero := AMovimento.FChequeNumero;
        cheque.FBanco := AMovimento.FChequeBanco;
        cheque.FAgencia := AMovimento.FChequeAgencia;
        cheque.FContaCorrente := AMovimento.FChequeContaCorrente;
        cheque.FDtEmissao := AMovimento.FChequeDtEmissao;
        cheque.FDtVencimento := AMovimento.FChequeDtVencimento;
        cheque.FIdChaveProcesso := AMovimento.FIdChaveProcesso;
        cheque.FIdContaCorrente := AMovimento.FIdContaCorrente;
        cheque.FBoConciliado := AMovimento.FBoConciliado;
        cheque.FDhConciliado := AMovimento.FDhConciliado;
        cheque.FIdDocumentoOrigem := AMovimento.FIdDocumentoOrigem;
        cheque.FDocumentoOrigem := AMovimento.FTPDocumentoOrigem;

        if chequeProprioUtilizadoParaPagamento then
        begin
          cheque.FTipo := TChequeProxy.TIPO_CHEQUE_PROPRIO;
          cheque.FBoChequeUtilizado := 'S';
        end
        else if chequeDeRecebimento then
        begin
          cheque.FTipo := TChequeProxy.TIPO_CHEQUE_TERCEIRO;
          cheque.FBoChequeUtilizado := 'N';
        end;

        result := TChequeServ.CadastrarCheque(cheque);
      finally
        FreeAndNil(cheque);
      end;

      exit;
    end;

    if chequeExistente then
    begin
      qCheque := BuscarCadastroChequeReferenteMovimentacao(AMovimento.FId);
      try
        if qCheque.GetAsInteger('ID') <= 0 then
        begin
          exit;
        end;

        TChequeServ.GerarUtilizacaoCheque(qCheque.GetAsInteger('ID'));
      finally
        FreeAndNil(qCheque);
      end;
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao cadastrar cheque.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class procedure TContaCorrenteServ.ConciliarRegistro(
  const AIdMovimentacao: Integer);
var
  qCheque: TFastQuery;
  qContaCorrenteMovimento: TFastQuery;
begin
  TFastQuery.ExecutarScriptIndependente(
    'update conta_corrente_movimento set bo_conciliado = ''S'' where id = :id',
    TArray<Variant>.Create(AIdMovimentacao));

  qContaCorrenteMovimento := GetContaCorrenteMovimento(AIdMovimentacao);
  try
    if qContaCorrenteMovimento.GetAsString('BO_CHEQUE').Equals('S') then
    begin
      qCheque := BuscarCadastroChequeReferenteMovimentacao(AIdMovimentacao);
      try
        TChequeServ.ConciliarCheque(qCheque.GetAsInteger('ID'), true);
      finally
        FreeAndNil(qCheque);
      end;
    end;
  finally
    FreeAndNil(qContaCorrenteMovimento);
  end;
end;

class function TContaCorrenteServ.BuscarCadastroChequeReferenteMovimentacao(
  AIdMovimentacao: Integer): TFastQuery;
var
  qContaCorrenteMovimento: TFastQuery;
begin
  qContaCorrenteMovimento := GetContaCorrenteMovimento(AIdMovimentacao);
  try
    if not qContaCorrenteMovimento.IsEmpty then
    begin
      result := TFastQuery.ModoDeAlteracao(
        'SELECT * FROM CHEQUE WHERE ID_CONTA_CORRENTE_MOVIMENTO = :ID',
        TArray<Variant>.Create(AIdMovimentacao));
    end;
  finally
    FreeAndNil(qContaCorrenteMovimento);
  end;
end;

class procedure TContaCorrenteServ.DesconciliarRegistro(
  const AIdMovimentacao: Integer);
var
  qCheque: TFastQuery;
  qContaCorrenteMovimento: TFastQuery;
begin
  TFastQuery.ExecutarScriptIndependente(
    'update conta_corrente_movimento set bo_conciliado = ''N'' where id = :id',
    TArray<Variant>.Create(AIdMovimentacao));

  qContaCorrenteMovimento := GetContaCorrenteMovimento(AIdMovimentacao);
  try
    if qContaCorrenteMovimento.GetAsString('BO_CHEQUE').Equals('S') then
    begin
      qCheque := BuscarCadastroChequeReferenteMovimentacao(AIdMovimentacao);
      try
        TChequeServ.ConciliarCheque(qCheque.GetAsInteger('ID'), false);
      finally
        FreeAndNil(qCheque);
      end;
    end;
  finally
    FreeAndNil(qContaCorrenteMovimento);
  end;
end;

class function TContaCorrenteServ.IsTransferencia(ADataset: TFDQuery): Boolean;
begin
  result := ADataset.FieldByName('BO_TRANSFERENCIA').AsString.Equals('S');
end;

class function TContaCorrenteServ.RealizarTransferencia(const AIdMovimentoOrigem, AIdContaCorrenteDestino: Integer): Integer;
var idContaCorrenteMovimento: Integer;
    idContaCorrenteMovimentoDestino: Integer;
    tipoMovimento: String;
    queryVerificacao, queryDestino, queryOrigem: IFastQuery;
    tranferenciaJaRealizada: Boolean;

const CAMPOS_NAO_COPIAR: Array [0..7] of string =
  ('id',
   'vl_saldo_geral',
   'vl_saldo_conciliado',
   'nome_conta_corrente',
   'nome_plano_conta',
   'tp_movimento_sigla',
   'id_conta_corrente_origem',
   'id_conta_corrente_destino');

const CAMPOS_MODIFICAR: Array [0..3] of string =
  ('id_movimento_origem',
   'tp_movimento',
   'dh_cadastro',
   'id_conta_corrente');

const CREDITO: String = 'CREDITO';
const DEBITO: String = 'DEBITO';
begin
  result := 0;

  queryVerificacao := TFastQuery.ModoDeConsulta(
    'SELECT id FROM conta_corrente_movimento where ID_MOVIMENTO_ORIGEM = :ID_MOVIMENTO_ORIGEM',
    TArray<Variant>.Create(AIdMovimentoOrigem));

  tranferenciaJaRealizada := not queryVerificacao.IsEmpty;

  if not tranferenciaJaRealizada then
  begin
    queryOrigem := TFastQuery.ModoDeConsulta('select * from conta_corrente_movimento where id = :id', TArray<Variant>.Create(AIdMovimentoOrigem));
    queryDestino := TFastQuery.ModoDeAlteracao('select * from conta_corrente_movimento where 1=2', nil);

    if queryOrigem.GetAsString('TP_MOVIMENTO').Equals(DEBITO) then
      tipoMovimento := CREDITO
    else
      tipoMovimento := DEBITO;

    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      CAMPOS_NAO_COPIAR, //No Copy
      CAMPOS_MODIFICAR, //Campos para mudar
      TArray<Variant>.Create(AIdMovimentoOrigem, tipoMovimento, Now, AIdContaCorrenteDestino) //Valores para Mudar
    );

    queryDestino.Persistir;

    TContaCorrenteServ.AtualizarIDDestinoNaOrigemDaTransferencia(AIdMovimentoOrigem, queryDestino.GetAsInteger('ID'));

    result := (queryDestino as TFastQuery).FgbPKGeradaPeloDB;
  end
  else
  begin
    result := queryVerificacao.GetAsInteger('id');
  end;
end;

class procedure TContaCorrenteServ.AmortizarProcessos(const AIdChaveProcesso: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE conta_corrente_movimento set bo_processo_amortizado = ''S'' '+
    ' where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class procedure TContaCorrenteServ.AtualizarIDDestinoNaOrigemDaTransferencia(const AIdMovimentoOrigem, AIdMovimentoDestino: Integer);
begin
  TFastQuery.ExecutarScriptIndependente('update conta_corrente_movimento set id_movimento_destino = :id_movimento_destino where id = :id',
    TArray<Variant>.Create(AIdMovimentoDestino, AIdMovimentoOrigem));
end;

class function TContaCorrenteServ.RegistroEstaConciliado(
  const AIdMovimentacao: Integer): boolean;
var qRegistro: IFastQuery;
begin
  qRegistro := TFastQuery.ModoDeConsulta('select bo_conciliado from conta_corrente_movimento where id = :id'
    , TArray<Variant>.Create(AidMovimentacao));
  result := qRegistro.GetAsString('bo_conciliado').Equals('S');
end;

class procedure TContaCorrenteServ.SetContaCorrente(AQuery: TgbFDQuery; ASomenteAtivo: Boolean);
var whereAtivo: String;
begin
  if ASomenteAtivo then
    whereAtivo := ' where bo_ativo = ''S'''
  else
    whereAtivo := '';

  AQuery.ModoDeConsulta(
    ' select'+
    '   id, descricao'+//esse id_associacao ser� alterado no client
    ' from'+
    '   conta_corrente'+whereAtivo);
end;

class function TContaCorrenteServ.RemoverMovimentoContaCorrente(
  AChaveProcesso: Integer): Boolean;
begin
  try
    Result := True;
    TFastQuery.ExecutarScriptIndependente(
          'DELETE FROM conta_corrente_movimento WHERE id_chave_processo = :id_chave_processo',
          TArray<Variant>.Create(AChaveProcesso));
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(
  AId: Integer): Boolean;
var
  qContaCorrenteMovimento: IFastQuery;
begin
  try
    qContaCorrenteMovimento := TFastQuery.ModoDeAlteracao(
          'SELECT * FROM conta_corrente_movimento WHERE id = :id',
          TArray<Variant>.Create(AId));

    if qContaCorrenteMovimento.GetAsString('BO_CHEQUE').Equals('S') then
    begin
      TContaCorrenteServ.ProcessarChequeDeUmaMovimentacaoEstornada(
        AId,
        qContaCorrenteMovimento.GetAsString('BO_CHEQUE_PROPRIO').Equals('S'),
        qContaCorrenteMovimento.GetAsString('TP_MOVIMENTO'));
    end;

    TFastQuery.ExecutarScriptIndependente('DELETE FROM conta_corrente_movimento WHERE id = :id',
      TArray<Variant>.Create(AId));

    Result := True;
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class procedure TContaCorrenteServ.ProcessarChequeDeUmaMovimentacaoEstornada(
  const AIdContaCorrenteMovimento: Integer; const AChequeProprio: Boolean; ATipoMovimentacao: String);
var
  qCheque: TFastQuery;
  chequeProprioUtilizadoParaPagamento: Boolean;
  chequeDeTerceiroUtilizadoParaPagamento: Boolean;
  chequeDeRecebimento: Boolean;
begin
  chequeProprioUtilizadoParaPagamento := AChequeProprio and
      ATipoMovimentacao.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO);

  chequeDeTerceiroUtilizadoParaPagamento := (not AChequeProprio) and
      ATipoMovimentacao.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO);

  chequeDeRecebimento := ATipoMovimentacao.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO);

  qCheque := BuscarCadastroChequeReferenteMovimentacao(AIdContaCorrenteMovimento);
  try
    if qCheque.GetAsInteger('ID') <= 0 then
    begin
      exit;
    end;

    if chequeProprioUtilizadoParaPagamento or chequeDeRecebimento then
    begin
      TChequeServ.CancelarCheque(qCheque.GetAsInteger('ID'));
    end
    else if chequeDeTerceiroUtilizadoParaPagamento then
    begin
      TChequeServ.EstornarUtilizacaoCheque(qCheque.GetAsInteger('ID'));
    end;
  finally
    FreeAndNil(qCheque);
  end;
end;

class function TContaCorrenteServ.GerarMovimentoContaCorrente(AMovimento : String) : Boolean;
var
  MovimentoCC : TContaCorrenteMovimento;
  fqMovimento : TFastQuery;
  cadastrarCheque: Boolean;
begin
  cadastrarCheque := false;
  Result := True;
  try
    MovimentoCC := TJson.JsonToObject<TContaCorrenteMovimento>(AMovimento);

    fqMovimento := TFastQuery.ModoDeAlteracao('SELECT * FROM conta_corrente_movimento WHERE 1 = 2' , nil);

    fqMovimento.Incluir;

    fqMovimento.SetAsString('DOCUMENTO', MovimentoCC.FDocumento);
    fqMovimento.SetAsString('DESCRICAO', MovimentoCC.FDescricao);
    fqMovimento.SetAsString('OBSERVACAO', MovimentoCC.FObservacao);

    fqMovimento.SetAsString('DH_CADASTRO', MovimentoCC.FDhCadastro);
    fqMovimento.SetAsString('DT_EMISSAO', MovimentoCC.FDtEmissao);
    fqMovimento.SetAsString('DT_MOVIMENTO', MovimentoCC.FDtMovimento);
    fqMovimento.SetAsString('DT_COMPETENCIA', MovimentoCC.FDtCompetencia);

    fqMovimento.SetAsFloat('VL_MOVIMENTO', MovimentoCC.FVlMovimento);
    fqMovimento.SetAsString('TP_MOVIMENTO', MovimentoCC.FTpMovimento);

    fqMovimento.SetAsString('BO_CONCILIADO', MovimentoCC.FBoConciliado);
    fqMovimento.SetAsString('DH_CONCILIADO', MovimentoCC.FDhConciliado);
    fqMovimento.SetAsFloat('VL_SALDO_CONCILIADO', MovimentoCC.FVlSaldoConciliado);
    fqMovimento.SetAsFloat('VL_SALDO_GERAL', MovimentoCC.FVlSaldoGeral);

    if MovimentoCC.FIdContaCorrente > 0 then
    begin
      fqMovimento.SetAsInteger('ID_CONTA_CORRENTE', MovimentoCC.FIdContaCorrente);
    end;

    if MovimentoCC.FIdContaAnalise > 0 then
    begin
      fqMovimento.SetAsInteger('ID_CONTA_ANALISE', MovimentoCC.FIdContaAnalise);
    end;

    if MovimentoCC.FIdCentroResultado > 0 then
    begin
      fqMovimento.SetAsInteger('ID_CENTRO_RESULTADO', MovimentoCC.FIdCentroResultado);
    end;

    fqMovimento.SetAsString('BO_MOVIMENTO_ORIGEM', MovimentoCC.FBoMovimentoOrigem);
    fqMovimento.SetAsInteger('ID_MOVIMENTO_ORIGEM', MovimentoCC.FIdMovimentoOrigem);

    fqMovimento.SetAsString('BO_MOVIMENTO_DESTINO', MovimentoCC.FBoMovimentoOrigem);
    fqMovimento.SetAsInteger('ID_MOVIMENTO_DESTINO', MovimentoCC.FIdMovimentoDestino);

    fqMovimento.SetAsString('BO_TRANSFERENCIA', MovimentoCC.FBoMovimentoOrigem);
    fqMovimento.SetAsInteger('ID_CONTA_CORRENTE_DESTINO', MovimentoCC.FIdContaCorrenteDestino);

    fqMovimento.SetAsString('TP_DOCUMENTO_ORIGEM', MovimentoCC.FTPDocumentoOrigem);
    fqMovimento.SetAsInteger('ID_DOCUMENTO_ORIGEM', MovimentoCC.FIdDocumentoOrigem);

    if MovimentoCC.FIdChaveProcesso > 0 then
    begin
      fqMovimento.SetAsInteger('ID_CHAVE_PROCESSO', MovimentoCC.FIdChaveProcesso);
    end;

    if MovimentoCC.FIdFilial > 0 then
    begin
      fqMovimento.SetAsInteger('ID_FILIAL', MovimentoCC.FIdFilial);
    end;

    fqMovimento.SetAsString('PARCELAMENTO', MovimentoCC.FParcelamento);

    if MovimentoCC.FBoCheque = 'S' then
    begin
      fqMovimento.SetAsString('BO_CHEQUE',MovimentoCC.FBoCheque);
      fqMovimento.SetAsString('BO_CHEQUE_TERCEIRO',MovimentoCC.FBoChequeTerceiro);
      fqMovimento.SetAsString('BO_CHEQUE_PROPRIO',MovimentoCC.FBoChequeProprio);

      fqMovimento.SetAsInteger('CHEQUE_NUMERO',MovimentoCC.FChequeNumero);
      fqMovimento.SetAsString('CHEQUE_SACADO',MovimentoCC.FChequeSacado);
      fqMovimento.SetAsString('CHEQUE_DOC_FEDERAL',MovimentoCC.FChequeDocFederal);
      fqMovimento.SetAsString('CHEQUE_BANCO',MovimentoCC.FChequeBanco);
      fqMovimento.SetAsString('CHEQUE_DT_EMISSAO',MovimentoCC.FChequeDtEmissao);
      fqMovimento.SetAsString('CHEQUE_DT_VENCIMENTO',MovimentoCC.FChequeDtVencimento);
      fqMovimento.SetAsString('CHEQUE_AGENCIA',MovimentoCC.FChequeAgencia);
      fqMovimento.SetAsString('CHEQUE_CONTA_CORRENTE',MovimentoCC.FChequeContaCorrente);
      cadastrarCheque := true;
    end;

    if MovimentoCC.FIdOperadoraCartao > 0 then
    begin
      fqMovimento.SetAsInteger('ID_OPERADORA_CARTAO', MovimentoCC.FIdOperadoraCartao);
    end;

    fqMovimento.Salvar;
    fqMovimento.Persistir;

    MovimentoCC.FId := fqMovimento.GetAsInteger('ID');

    if cadastrarCheque then
    begin
      TContaCorrenteServ.CadastrarCheque(MovimentoCC);
    end;
  except
    on e : Exception do
    begin
      Result := False;
      raise Exception.Create('Erro ao gerar a movimenta��o de conta corrente.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TContaCorrenteServ.GetContaCorrenteMovimento(
  AIdContaCorrenteMovimento: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeAlteracao(
    'select * FROM conta_corrente_movimento where id = :id',
    TArray<Variant>.Create(AIdContaCorrenteMovimento));
end;

class function TContaCorrenteServ.GerarContraPartidaContaCorrente(AChaveProcesso: Integer;
  AExcluirCheque: Boolean = true): Boolean;
var
  qContaCorrenteMovimento: TFastQuery;
begin
  try
    Result := True;
    qContaCorrenteMovimento := GetContaCorrenteMovimentoPorChaveProcesso(AChaveProcesso);
    try
      qContaCorrenteMovimento.First;
      while not qContaCorrenteMovimento.Eof do
      begin
        TContaCorrenteServ.GerarContraPartidaContaCorrentePorID(
          qContaCorrenteMovimento.FieldByName('ID').AsInteger, AExcluirCheque);

        qContaCorrenteMovimento.Next;
      end;
    Except
      FreeAndNil(qContaCorrenteMovimento);
    end;
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaCorrenteServ.GerarContraPartidaContaCorrentePorID(
  AId: Integer; AExcluirCheque: Boolean = true): Boolean;
var
  movimentacao: TContaCorrenteMovimento;
  qCheque: TFastQuery;
begin
  try
    Result := True;
    movimentacao := TContaCorrenteServ.GetContaCorrenteMovimentoProxy(AId);
    movimentacao.FDhCadastro := DateTimeToStr(Now);
    movimentacao.FDtMovimento := DateToStr(Date);

    if movimentacao.FTpMovimento.Equals(TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO) then
    begin
      movimentacao.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
    end
    else
    begin
      movimentacao.FTpMovimento := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;
    end;

    if movimentacao.FBoConciliado = 'S' then
    begin
      movimentacao.FDhConciliado := DateTimeToStr(Now);
    end
    else
    begin
      movimentacao.FDhConciliado := '';
    end;

    TContaCorrenteServ.GerarContaCorrenteMovimento(movimentacao);

    TContaCorrenteServ.AmortizarProcessos(movimentacao.FIdChaveProcesso);

    if AExcluirCheque then
    begin
      if movimentacao.FBoCheque.Equals('S') then
      begin
        TContaCorrenteServ.ProcessarChequeDeUmaMovimentacaoEstornada(
          AId,
          movimentacao.FBoChequeProprio.Equals('S'),
          movimentacao.FTpMovimento);
      end;
    end;
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class function TContaCorrenteServ.GerarDocumento(AMovimento : String) : Boolean;
var
  MovimentoCC : TContaCorrenteMovimento;

  function VincularOrigem(AIdChaveProcesso : Integer;
                          AIdDocOrigem     : Integer;
                          ATpDocOrigem     : string ) : Boolean;
  begin
    TFastQuery.ExecutarScriptIndependente(' update conta_corrente_movimento                 ' +
                                          '    set id_documento_origem = :idDocOrigem       ' +
                                          '       ,tp_documento_origem = :tpDocumentoOrigem ' +
                                          '  where id_chave_processo   = :id_chave_processo ',
      TArray<Variant>.Create(AIdDocOrigem, ATpDocOrigem, AIdChaveProcesso));

  end;

  function GerarContaPagar(AMovimento : TContaCorrenteMovimento) : Boolean;
  var
    ContaPagar             : TContaPagarMovimento;
    ContaPagarQuitacao     : TContaPagarQuitacaoMovimento;
    ContaPagarJson         : string;
    ContaPagarQuitacaoJson : string;
  begin

    Result := False;

    ContaPagar         := TContaPagarMovimento.Create;
    ContaPagarQuitacao := TContaPagarQuitacaoMovimento.Create;

    try
      ContaPagar.FDocumento         := AMovimento.FDocumento;
      ContaPagar.FDescricao         := AMovimento.FDescricao;
      ContaPagar.FStatus            := ContaPagar.ABERTO;
      ContaPagar.FDtVencimento      := AMovimento.FDtMovimento;
      ContaPagar.FDhCadastro        := AMovimento.FDhCadastro;
      ContaPagar.FDtCompetencia     := AMovimento.FDtCompetencia;
      ContaPagar.FDtDocumento       := AMovimento.FDtEmissao;
      ContaPagar.FVlTitulo          := AMovimento.FVlMovimento;
      ContaPagar.FVlQuitado         := 0;
      ContaPagar.FVlAberto          := AMovimento.FVlMovimento;
      ContaPagar.FVlAcrescimo       := 0;
      ContaPagar.FVlDecrescimo      := 0;
      ContaPagar.FVlQuitadoLiquido  := 0;
      ContaPagar.FQtParcela         := 1;
      ContaPagar.FNrParcela         := 1;
      ContaPagar.FSequencia         := '1';
      ContaPagar.FObservacao        := AMovimento.FObservacao;
      ContaPagar.FIdContaAnalise    := AMovimento.FIdContaAnalise;
      ContaPagar.FIdCentroResultado := AMovimento.FIdCentroResultado;
      ContaPagar.FIdChaveProcesso   := AMovimento.FIdChaveProcesso;
      ContaPagar.FIdFormaPagamento  := 1;

      ContaPagar.FIdFilial  := AMovimento.FIdFilial;

      ContaPagar.FBoVencido         := ContaPagar.NAO;

      ContaPagarJson := TJson.ObjectToJsonString(ContaPagar);

      ContaPagar.FId := TContaPagarServ.GerarContaPagar(ContaPagarJson);

      ContaPagarQuitacao.FIdContaPagar    := ContaPagar.FId;
      ContaPagarQuitacao.FNrItem          := 1;
      ContaPagarQuitacao.FDhCadastro      := AMovimento.FDhCadastro;
      ContaPagarQuitacao.FDtQuitacao      := AMovimento.FDtMovimento;
      ContaPagarQuitacao.FObservacao      := AMovimento.FObservacao;
      ContaPagarQuitacao.FIdFilial        := TFilialServ.GetIdFilial;
      ContaPagarQuitacao.FIdTipoQuitacao  := 1;
      ContaPagarQuitacao.FIdContaCorrente := AMovimento.FIdContaCorrente;
      ContaPagarQuitacao.FIdContaAnalise  := AMovimento.FIdContaAnalise;
      ContaPagarQuitacao.FVlDesconto      := 0;
      ContaPagarQuitacao.FVlAcrescimo     := 0;
      ContaPagarQuitacao.FVlQuitacao      := AMovimento.FVlMovimento;
      ContaPagarQuitacao.FVlTotal         := AMovimento.FVlMovimento;
      ContaPagarQuitacao.FPercAcrescimo   := 0;
      ContaPagarQuitacao.FPercDesconto    := 0;

      ContaPagarQuitacaoJson := TJson.ObjectToJsonString(ContaPagarQuitacao);

      ContaPagarQuitacao.FId := TContaPagarServ.GerarContaPagarQuitacao(ContaPagarQuitacaoJson);

      TContaPagarServ.AtualizarValores(ContaPagar.FId);

      VincularOrigem(AMovimento.FIdChaveProcesso,
                     ContaPagarQuitacao.FId,
                     TContaCorrenteMovimento.TIPO_DOCUMENTO_CONTAPAGAR)
    finally
      ContaPagar.Free;
      ContaPagarQuitacao.Free;
    end;

  end;

  function AtualizarContaPagar(AMovimento : TContaCorrenteMovimento) : Boolean;
  var
    ContaPagar             : TContaPagarMovimento;
    ContaPagarQuitacao     : TContaPagarQuitacaoMovimento;
    ContaPagarJson         : string;
    ContaPagarQuitacaoJson : string;
  begin

    Result := False;

    ContaPagar         := TContaPagarMovimento.Create;
    ContaPagarQuitacao := TContaPagarQuitacaoMovimento.Create;

    try
      ContaPagar.FId                := TContaPagarServ.GetIdContaPagar(AMovimento.FIdDocumentoOrigem);
      ContaPagar.FDocumento         := AMovimento.FDocumento;
      ContaPagar.FDescricao         := AMovimento.FDescricao;
      ContaPagar.FStatus            := ContaPagar.ABERTO;
      ContaPagar.FDtVencimento      := AMovimento.FDtMovimento;
      ContaPagar.FDhCadastro        := AMovimento.FDhCadastro;
      ContaPagar.FDtCompetencia     := AMovimento.FDtCompetencia;
      ContaPagar.FVlTitulo          := AMovimento.FVlMovimento;
      ContaPagar.FVlQuitado         := 0;
      ContaPagar.FVlAberto          := AMovimento.FVlMovimento;
      ContaPagar.FVlAcrescimo       := 0;
      ContaPagar.FVlDecrescimo      := 0;
      ContaPagar.FVlQuitadoLiquido  := 0;
      ContaPagar.FQtParcela         := 1;
      ContaPagar.FNrParcela         := 1;
      ContaPagar.FSequencia         := '1';
      ContaPagar.FObservacao        := AMovimento.FObservacao;
      ContaPagar.FIdContaAnalise    := AMovimento.FIdContaAnalise;
      ContaPagar.FIdCentroResultado := AMovimento.FIdCentroResultado;
      ContaPagar.FIdChaveProcesso   := AMovimento.FIdChaveProcesso;
      ContaPagar.FIdFormaPagamento  := 1;
      ContaPagar.FBoVencido         := ContaPagar.NAO;

      ContaPagarJson := TJson.ObjectToJsonString(ContaPagar);

      TContaPagarServ.AtualizarContaPagar(ContaPagarJson);

      ContaPagarQuitacao.FId              := Amovimento.FIdDocumentoOrigem;
      ContaPagarQuitacao.FIdContaPagar    := ContaPagar.FId;
      ContaPagarQuitacao.FNrItem          := 1;
      ContaPagarQuitacao.FDhCadastro      := AMovimento.FDhCadastro;
      ContaPagarQuitacao.FDtQuitacao      := AMovimento.FDtMovimento;
      ContaPagarQuitacao.FObservacao      := AMovimento.FObservacao;
      ContaPagarQuitacao.FIdFilial        := TFilialServ.GetIdFilial;
      ContaPagarQuitacao.FIdTipoQuitacao  := 1;
      ContaPagarQuitacao.FIdContaCorrente := AMovimento.FIdContaCorrente;
      ContaPagarQuitacao.FIdContaAnalise  := AMovimento.FIdContaAnalise;
      ContaPagarQuitacao.FVlDesconto      := 0;
      ContaPagarQuitacao.FVlAcrescimo     := 0;
      ContaPagarQuitacao.FVlQuitacao      := AMovimento.FVlMovimento;
      ContaPagarQuitacao.FVlTotal         := AMovimento.FVlMovimento;
      ContaPagarQuitacao.FPercAcrescimo   := 0;
      ContaPagarQuitacao.FPercDesconto    := 0;

      ContaPagarQuitacaoJson := TJson.ObjectToJsonString(ContaPagarQuitacao);

      TContaPagarServ.AtualizarContaPagarQuitacao(ContaPagarQuitacaoJson);

      TContaPagarServ.AtualizarValores(ContaPagar.FId);

    finally
      ContaPagar.Free;
      ContaPagarQuitacao.Free;
    end;

  end;

  function GerarContaReceber(AMovimento : TContaCorrenteMovimento) : Boolean;
  var
    ContaReceber             : TContaReceberMovimento;
    ContaReceberQuitacao     : TContaReceberQuitacaoMovimento;
    ContaReceberJson         : string;
    ContaReceberQuitacaoJson : string;
  begin

    Result := True;

    ContaReceber         := TContaReceberMovimento.Create;
    ContaReceberQuitacao := TContaReceberQuitacaoMovimento.Create;

    try
      ContaReceber.FDocumento         := AMovimento.FDocumento;
      ContaReceber.FDescricao         := AMovimento.FDescricao;
      ContaReceber.FStatus            := ContaReceber.ABERTO;
      ContaReceber.FDtVencimento      := AMovimento.FDtMovimento;
      ContaReceber.FDtDocumento       := AMovimento.FDtEmissao;
      ContaReceber.FDtCompetencia     := AMovimento.FDtCompetencia;
      ContaReceber.FDhCadastro        := AMovimento.FDhCadastro;
      ContaReceber.FVlTitulo          := AMovimento.FVlMovimento;
      ContaReceber.FVlQuitado         := 0;
      ContaReceber.FVlAberto          := AMovimento.FVlMovimento;
      ContaReceber.FVlAcrescimo       := 0;
      ContaReceber.FVlDecrescimo      := 0;
      ContaReceber.FVlQuitadoLiquido  := 0;
      ContaReceber.FQtParcela         := 1;
      ContaReceber.FNrParcela         := 1;
      ContaReceber.FSequencia         := '1';
      ContaReceber.FObservacao        := AMovimento.FObservacao;
      ContaReceber.FIdContaAnalise    := AMovimento.FIdContaAnalise;
      ContaReceber.FIdCentroResultado := AMovimento.FIdCentroResultado;
      ContaReceber.FIdChaveProcesso   := AMovimento.FIdChaveProcesso;
      ContaReceber.FIdFormaPagamento  := 1;
      ContaReceber.FIdFilial          := AMovimento.FIdFilial;
      ContaReceber.FBoVencido         := ContaReceber.NAO;

      ContaReceberJson := TJson.ObjectToJsonString(ContaReceber);

      ContaReceber.FId := TContaReceberServ.GerarContaReceber(ContaReceberJson);

      ContaReceberQuitacao.FIdContaReceber  := ContaReceber.FId;
      ContaReceberQuitacao.FNrItem          := 1;
      ContaReceberQuitacao.FDhCadastro      := AMovimento.FDhCadastro;
      ContaReceberQuitacao.FDtQuitacao      := AMovimento.FDtMovimento;
      ContaReceberQuitacao.FObservacao      := AMovimento.FObservacao;
      ContaReceberQuitacao.FIdFilial        := TFilialServ.GetIdFilial;
      ContaReceberQuitacao.FIdTipoQuitacao  := 1;
      ContaReceberQuitacao.FIdContaCorrente := AMovimento.FIdContaCorrente;
      ContaReceberQuitacao.FIdCentroResultado := ContaReceber.FIdCentroResultado;
      ContaReceberQuitacao.FIdContaAnalise  := AMovimento.FIdContaAnalise;
      ContaReceberQuitacao.FVlDesconto      := 0;
      ContaReceberQuitacao.FVlAcrescimo     := 0;
      ContaReceberQuitacao.FVlQuitacao      := AMovimento.FVlMovimento;
      ContaReceberQuitacao.FVlTotal         := AMovimento.FVlMovimento;
      ContaReceberQuitacao.FPercAcrescimo   := 0;
      ContaReceberQuitacao.FPercDesconto    := 0;

      ContaReceberQuitacaoJson := TJson.ObjectToJsonString(ContaReceberQuitacao);

      ContaReceberQuitacao.FId := TContaReceberServ.GerarContaReceberQuitacao(ContaReceberQuitacaoJson);

      TContaReceberServ.AtualizarValores(ContaReceber.FId);

      VincularOrigem(AMovimento.FIdChaveProcesso,
                     ContaReceberQuitacao.FId,
                     TContaCorrenteMovimento.TIPO_DOCUMENTO_CONTARECEBER)

    finally
      ContaReceber.Free;
      ContaReceberQuitacao.Free;
    end;

  end;

begin

  Result := True;

  try

    MovimentoCC := TJson.JsonToObject<TContaCorrenteMovimento>(AMovimento);

    if MovimentoCC.Fid < 0 then
    begin
      if MovimentoCC.FTpMovimento = TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO then
      begin
        Result := GerarContaPagar(MovimentoCC);
      end
      else
      if MovimentoCC.FTpMovimento = TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO then
      begin
        Result := GerarContaReceber(MovimentoCC);
      end;
    end
    else
    begin
      if MovimentoCC.FTpMovimento = TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO then
      begin
        Result := AtualizarContaPagar(MovimentoCC);
      end
      else
      if MovimentoCC.FTpMovimento = TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO then
      begin
        //Result := GerarContaReceber(MovimentoCC);
      end;
    end;

  except
    on e : Exception do
    begin
      Result := False;
    end;
  end;
end;

class function TContaCorrenteServ.GetContaCorrenteMovimentoPorChaveProcesso(
  AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeAlteracao(
    'select * FROM conta_corrente_movimento where id_chave_processo = :id',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TContaCorrenteServ.GetContaCorrenteMovimentoProxy(
    const AIdContaCorrenteMovimento: Integer): TContaCorrenteMovimento;
const
  SQL = 'SELECT * FROM conta_corrente_movimento WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TContaCorrenteMovimento.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdContaCorrenteMovimento));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FVlMovimento := qConsulta.GetAsFloat('VL_MOVIMENTO');
  result.FTpMovimento := qConsulta.GetAsString('TP_MOVIMENTO');
  result.FBoConciliado := qConsulta.GetAsString('BO_CONCILIADO');
  result.FDhConciliado := qConsulta.GetAsString('DH_CONCILIADO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FDtEmissao := qConsulta.GetAsString('DT_EMISSAO');
  result.FDtMovimento := qConsulta.GetAsString('DT_MOVIMENTO');
  result.FVlSaldoConciliado := qConsulta.GetAsFloat('VL_SALDO_CONCILIADO');
  result.FVlSaldoGeral := qConsulta.GetAsFloat('VL_SALDO_GERAL');
  result.FDocumento := qConsulta.GetAsString('DOCUMENTO');
  result.FIdContaCorrente := qConsulta.GetAsInteger('ID_CONTA_CORRENTE');
  result.FIdContaAnalise := qConsulta.GetAsInteger('ID_CONTA_ANALISE');
  result.FIdCentroResultado := qConsulta.GetAsInteger('ID_CENTRO_RESULTADO');
  result.FIdMovimentoOrigem := qConsulta.GetAsInteger('ID_MOVIMENTO_ORIGEM');
  result.FIdMovimentoDestino := qConsulta.GetAsInteger('ID_MOVIMENTO_DESTINO');
  result.FBoMovimentoOrigem := qConsulta.GetAsString('BO_MOVIMENTO_ORIGEM');
  result.FBoMovimentoDestino := qConsulta.GetAsString('BO_MOVIMENTO_DESTINO');
  result.FBoTransferencia := qConsulta.GetAsString('BO_TRANSFERENCIA');
  result.FIdContaCorrenteDestino := qConsulta.GetAsInteger('ID_CONTA_CORRENTE_DESTINO');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FIdDocumentoOrigem := qConsulta.GetAsInteger('ID_DOCUMENTO_ORIGEM');
  result.FTpDocumentoOrigem := qConsulta.GetAsString('TP_DOCUMENTO_ORIGEM');
  result.FParcelamento := qConsulta.GetAsString('PARCELAMENTO');
  result.FDtCompetencia := qConsulta.GetAsString('DT_COMPETENCIA');
  result.FChequeSacado := qConsulta.GetAsString('CHEQUE_SACADO');
  result.FChequeDocFederal := qConsulta.GetAsString('CHEQUE_DOC_FEDERAL');
  result.FChequeBanco := qConsulta.GetAsString('CHEQUE_BANCO');
  result.FChequeDtEmissao := qConsulta.GetAsString('CHEQUE_DT_EMISSAO');
  result.FChequeDtVencimento := qConsulta.GetAsString('CHEQUE_DT_VENCIMENTO');
  result.FChequeAgencia := qConsulta.GetAsString('CHEQUE_AGENCIA');
  result.FChequeContaCorrente := qConsulta.GetAsString('CHEQUE_CONTA_CORRENTE');
  result.FChequeNumero := qConsulta.GetAsInteger('CHEQUE_NUMERO');
  result.FIdOperadoraCartao := qConsulta.GetAsInteger('ID_OPERADORA_CARTAO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FBoCheque := qConsulta.GetAsString('BO_CHEQUE');
  result.FBoChequeTerceiro := qConsulta.GetAsString('BO_CHEQUE_TERCEIRO');
  result.FBoChequeProprio := qConsulta.GetAsString('BO_CHEQUE_PROPRIO');
end;

class function TContaCorrenteServ.GetContaCorrenteMovimentos(AIdsContaCorrenteMovimento: String): TFastQuery;
begin
  result := TFastQuery.ModoDeAlteracao(
    'select * FROM conta_corrente_movimento where id in ('+AIdsContaCorrenteMovimento+')');
end;

class function TContaCorrenteServ.GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
  const ADtMovimento: String): Double;
var
  qTotal: IFastQuery;
  SQL: String;
begin
  SQL :=
    ' select'+
    '  (select vl_saldo_geral from conta_corrente_movimento'+
    '      where id = IFNULL((select id from conta_corrente_movimento where'+
    '            dt_movimento = :dt_competencia_inicial'+
    '        and id_conta_corrente = :id_conta_corrente'+
    '     order by dt_movimento desc, id desc, documento desc limit 1), '+
    ' (select id from conta_corrente_movimento'+
    ' where conta_corrente_movimento.id_conta_corrente = :id_conta_corrente '+
    '       and dt_movimento between 20150101 and :dt_competencia_inicial'+
    ' order by dt_movimento desc, id desc, documento desc limit 1))) as vl_total'+
    ' FROM conta_corrente'+
    ' where conta_corrente.id = :id_conta_corrente';

  qTotal := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    TStringUtils.RetornarDataNoFormatoMYSQL(StrtoDate(ADtMovimento)-1), AIdContaCorrente));

  result := qTotal.GetAsFloat('vl_total');
end;

class function TContaCorrenteServ.GerarContaCorrenteMovimento(
    const AContaCorrenteMovimento: TContaCorrenteMovimento): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('conta_corrente_movimento');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AContaCorrenteMovimento.FDescricao);
    qEntidade.SetAsString('DH_CADASTRO', AContaCorrenteMovimento.FDhCadastro);
    qEntidade.SetAsFloat('VL_MOVIMENTO', AContaCorrenteMovimento.FVlMovimento);
    qEntidade.SetAsString('TP_MOVIMENTO', AContaCorrenteMovimento.FTpMovimento);
    qEntidade.SetAsString('BO_CONCILIADO', AContaCorrenteMovimento.FBoConciliado);
    qEntidade.SetAsString('DH_CONCILIADO', AContaCorrenteMovimento.FDhConciliado);
    qEntidade.SetAsString('OBSERVACAO', AContaCorrenteMovimento.FObservacao);
    qEntidade.SetAsString('DT_EMISSAO', AContaCorrenteMovimento.FDtEmissao);
    qEntidade.SetAsString('DT_MOVIMENTO', AContaCorrenteMovimento.FDtMovimento);
    qEntidade.SetAsFloat('VL_SALDO_CONCILIADO', AContaCorrenteMovimento.FVlSaldoConciliado);
    qEntidade.SetAsFloat('VL_SALDO_GERAL', AContaCorrenteMovimento.FVlSaldoGeral);
    qEntidade.SetAsString('DOCUMENTO', AContaCorrenteMovimento.FDocumento);

    if AContaCorrenteMovimento.FIdContaCorrente > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_CORRENTE', AContaCorrenteMovimento.FIdContaCorrente);
    end;

    if AContaCorrenteMovimento.FIdContaAnalise > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_ANALISE', AContaCorrenteMovimento.FIdContaAnalise);
    end;

    if AContaCorrenteMovimento.FIdCentroResultado > 0 then
    begin
      qEntidade.SetAsInteger('ID_CENTRO_RESULTADO', AContaCorrenteMovimento.FIdCentroResultado);
    end;

    if AContaCorrenteMovimento.FIdMovimentoOrigem > 0 then
    begin
      qEntidade.SetAsInteger('ID_MOVIMENTO_ORIGEM', AContaCorrenteMovimento.FIdMovimentoOrigem);
    end;

    if AContaCorrenteMovimento.FIdMovimentoDestino > 0 then
    begin
      qEntidade.SetAsInteger('ID_MOVIMENTO_DESTINO', AContaCorrenteMovimento.FIdMovimentoDestino);
    end;
    qEntidade.SetAsString('BO_MOVIMENTO_ORIGEM', AContaCorrenteMovimento.FBoMovimentoOrigem);
    qEntidade.SetAsString('BO_MOVIMENTO_DESTINO', AContaCorrenteMovimento.FBoMovimentoDestino);
    qEntidade.SetAsString('BO_TRANSFERENCIA', AContaCorrenteMovimento.FBoTransferencia);

    if AContaCorrenteMovimento.FIdContaCorrenteDestino > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_CORRENTE_DESTINO', AContaCorrenteMovimento.FIdContaCorrenteDestino);
    end;

    if AContaCorrenteMovimento.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AContaCorrenteMovimento.FIdChaveProcesso);
    end;

    if AContaCorrenteMovimento.FIdDocumentoOrigem > 0 then
    begin
      qEntidade.SetAsInteger('ID_DOCUMENTO_ORIGEM', AContaCorrenteMovimento.FIdDocumentoOrigem);
    end;
    qEntidade.SetAsString('TP_DOCUMENTO_ORIGEM', AContaCorrenteMovimento.FTpDocumentoOrigem);
    qEntidade.SetAsString('PARCELAMENTO', AContaCorrenteMovimento.FParcelamento);
    qEntidade.SetAsString('DT_COMPETENCIA', AContaCorrenteMovimento.FDtCompetencia);
    qEntidade.SetAsString('CHEQUE_SACADO', AContaCorrenteMovimento.FChequeSacado);
    qEntidade.SetAsString('CHEQUE_DOC_FEDERAL', AContaCorrenteMovimento.FChequeDocFederal);
    qEntidade.SetAsString('CHEQUE_BANCO', AContaCorrenteMovimento.FChequeBanco);
    qEntidade.SetAsString('CHEQUE_DT_EMISSAO', AContaCorrenteMovimento.FChequeDtEmissao);
    qEntidade.SetAsString('CHEQUE_DT_VENCIMENTO', AContaCorrenteMovimento.FChequeDtVencimento);
    qEntidade.SetAsString('CHEQUE_AGENCIA', AContaCorrenteMovimento.FChequeAgencia);
    qEntidade.SetAsString('CHEQUE_CONTA_CORRENTE', AContaCorrenteMovimento.FChequeContaCorrente);
    qEntidade.SetAsInteger('CHEQUE_NUMERO', AContaCorrenteMovimento.FChequeNumero);

    if AContaCorrenteMovimento.FIdOperadoraCartao > 0 then
    begin
      qEntidade.SetAsInteger('ID_OPERADORA_CARTAO', AContaCorrenteMovimento.FIdOperadoraCartao);
    end;

    if AContaCorrenteMovimento.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AContaCorrenteMovimento.FIdFilial);
    end;
    qEntidade.SetAsString('BO_CHEQUE', AContaCorrenteMovimento.FBoCheque);
    qEntidade.SetAsString('BO_CHEQUE', AContaCorrenteMovimento.FBoChequeTerceiro);
    qEntidade.SetAsString('BO_CHEQUE', AContaCorrenteMovimento.FBoChequeProprio);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AContaCorrenteMovimento.FID := qEntidade.GetAsInteger('ID');
    result :=     AContaCorrenteMovimento.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ContaCorrenteMovimento.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
