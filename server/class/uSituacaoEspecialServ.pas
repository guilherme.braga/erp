unit uSituacaoEspecialServ;

Interface

Uses uSituacaoEspecialProxy;

type TSituacaoEspecialServ = class
  class function GetSituacaoEspecial(
    const AIdSituacaoEspecial: Integer): TSituacaoEspecialProxy;
end;

implementation

{ TSituacaoEspecialServ }

uses uFactoryQuery;

class function TSituacaoEspecialServ.GetSituacaoEspecial(
    const AIdSituacaoEspecial: Integer): TSituacaoEspecialProxy;
const
  SQL = 'SELECT * FROM situacao_especial WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TSituacaoEspecialProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdSituacaoEspecial));

  result.FId := qConsulta.GetAsString('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

end.


