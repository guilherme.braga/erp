unit uRegraImpostoFiltroServ;

Interface

Uses uRegraImpostoFiltroProxy, SysUtils, Generics.Collections;

type TRegraImpostoFiltroServ = class
  class function GetRegraImpostoFiltro(const AIdRegraImpostoFiltro: Integer): TRegraImpostoFiltroProxy;
  class function GerarRegraImpostoFiltro(const ARegraImpostoFiltro: TRegraImpostoFiltroProxy): Integer;
  class function BuscarImpostoAdequado(const AIdProduto, AIdPessoa, AIdFilial,
    AIdRegraImposto: Integer): TRegraImpostoFiltroProxy;
end;

implementation

{ TRegraImpostoFiltroServ }

uses uFactoryQuery, uProdutoServ, uFilialServ, uPessoaServ, uRegraImpostoServ, uOperacaoServ,
  uConstantesFiscaisProxy, uFilialProxy, uCEPServ;

class function TRegraImpostoFiltroServ.GetRegraImpostoFiltro(
    const AIdRegraImpostoFiltro: Integer): TRegraImpostoFiltroProxy;
const
  SQL = 'SELECT * FROM REGRA_IMPOSTO_FILTRO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TRegraImpostoFiltroProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdRegraImpostoFiltro));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipoEmpresaPessoa := qConsulta.GetAsString('TIPO_EMPRESA_PESSOA');
  result.FFormaAquisicao := qConsulta.GetAsString('FORMA_AQUISICAO');
  result.FOrigemDaMercadoria := qConsulta.GetAsInteger('ORIGEM_DA_MERCADORIA');
  result.FRegimeTributarioDestinatario := qConsulta.GetAsInteger('REGIME_TRIBUTARIO_DESTINATARIO');
  result.FRegimeTributarioEmitente := qConsulta.GetAsInteger('REGIME_TRIBUTARIO_EMITENTE');
  result.FCrtEmitente := qConsulta.GetAsInteger('CRT_EMITENTE');
  result.FBoContribuinteIcmsEmitente := qConsulta.GetAsString('BO_CONTRIBUINTE_ICMS_EMITENTE');
  result.FBoContribuinteIpiEmitente := qConsulta.GetAsString('BO_CONTRIBUINTE_IPI_EMITENTE');
  result.FBoContribuinteIcmsDestinatario := qConsulta.GetAsString('BO_CONTRIBUINTE_ICMS_DESTINATARIO');
  result.FBoContribuinteIpiDestinatario := qConsulta.GetAsString('BO_CONTRIBUINTE_IPI_DESTINATARIO');
  result.FIdZoneamentoEmitente := qConsulta.GetAsInteger('ID_ZONEAMENTO_EMITENTE');
  result.FIdZoneamentoDestinatario := qConsulta.GetAsInteger('ID_ZONEAMENTO_DESTINATARIO');
  result.FIdRegimeEspecialEmitente := qConsulta.GetAsInteger('ID_REGIME_ESPECIAL_EMITENTE');
  result.FIdRegimeEspecialDestinatario := qConsulta.GetAsInteger('ID_REGIME_ESPECIAL_DESTINATARIO');
  result.FIdSituacaoEspecial := qConsulta.GetAsInteger('ID_SITUACAO_ESPECIAL');
  result.FIdOperacaoFiscal := qConsulta.GetAsInteger('ID_OPERACAO_FISCAL');
  result.FIdRegraImposto := qConsulta.GetAsInteger('ID_REGRA_IMPOSTO');
  result.FIdEstadoOrigem := qConsulta.GetAsInteger('ID_ESTADO_ORIGEM');
  result.FIdEstadoDestino := qConsulta.GetAsInteger('ID_ESTADO_DESTINO');
  result.FDhInicioVigencia := qConsulta.GetAsString('DH_INICIO_VIGENCIA');
  result.FDhFimVigencia := qConsulta.GetAsString('DH_FIM_VIGENCIA');
  result.FIdImpostoPisCofins := qConsulta.GetAsInteger('ID_IMPOSTO_PIS_COFINS');
  result.FIdImpostoIcms := qConsulta.GetAsInteger('ID_IMPOSTO_ICMS');
  result.FIdImpostoIpi := qConsulta.GetAsInteger('ID_IMPOSTO_IPI');
end;

class function TRegraImpostoFiltroServ.GerarRegraImpostoFiltro(
    const ARegraImpostoFiltro: TRegraImpostoFiltroProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('REGRA_IMPOSTO_FILTRO');
    qEntidade.Incluir;

    qEntidade.SetAsString('TIPO_EMPRESA_PESSOA', ARegraImpostoFiltro.FTipoEmpresaPessoa);
    qEntidade.SetAsString('FORMA_AQUISICAO', ARegraImpostoFiltro.FFormaAquisicao);
    qEntidade.SetAsInteger('ORIGEM_DA_MERCADORIA', ARegraImpostoFiltro.FOrigemDaMercadoria);
    qEntidade.SetAsInteger('REGIME_TRIBUTARIO_DESTINATARIO', ARegraImpostoFiltro.FRegimeTributarioDestinatario);
    qEntidade.SetAsInteger('REGIME_TRIBUTARIO_EMITENTE', ARegraImpostoFiltro.FRegimeTributarioEmitente);
    qEntidade.SetAsInteger('CRT_EMITENTE', ARegraImpostoFiltro.FCrtEmitente);
    qEntidade.SetAsString('BO_CONTRIBUINTE_ICMS_EMITENTE', ARegraImpostoFiltro.FBoContribuinteIcmsEmitente);
    qEntidade.SetAsString('BO_CONTRIBUINTE_IPI_EMITENTE', ARegraImpostoFiltro.FBoContribuinteIpiEmitente);
    qEntidade.SetAsString('BO_CONTRIBUINTE_ICMS_DESTINATARIO', ARegraImpostoFiltro.FBoContribuinteIcmsDestinatario);
    qEntidade.SetAsString('BO_CONTRIBUINTE_IPI_DESTINATARIO', ARegraImpostoFiltro.FBoContribuinteIpiDestinatario);

    if ARegraImpostoFiltro.FIdZoneamentoEmitente > 0 then
    begin
      qEntidade.SetAsInteger('ID_ZONEAMENTO_EMITENTE', ARegraImpostoFiltro.FIdZoneamentoEmitente);
    end;

    if ARegraImpostoFiltro.FIdZoneamentoDestinatario > 0 then
    begin
      qEntidade.SetAsInteger('ID_ZONEAMENTO_DESTINATARIO', ARegraImpostoFiltro.FIdZoneamentoDestinatario);
    end;

    if ARegraImpostoFiltro.FIdRegimeEspecialEmitente > 0 then
    begin
      qEntidade.SetAsInteger('ID_REGIME_ESPECIAL_EMITENTE', ARegraImpostoFiltro.FIdRegimeEspecialEmitente);
    end;

    if ARegraImpostoFiltro.FIdRegimeEspecialDestinatario > 0 then
    begin
      qEntidade.SetAsInteger('ID_REGIME_ESPECIAL_DESTINATARIO', ARegraImpostoFiltro.FIdRegimeEspecialDestinatario);
    end;

    if ARegraImpostoFiltro.FIdSituacaoEspecial > 0 then
    begin
      qEntidade.SetAsInteger('ID_SITUACAO_ESPECIAL', ARegraImpostoFiltro.FIdSituacaoEspecial);
    end;

    if ARegraImpostoFiltro.FIdOperacaoFiscal > 0 then
    begin
      qEntidade.SetAsInteger('ID_OPERACAO_FISCAL', ARegraImpostoFiltro.FIdOperacaoFiscal);
    end;

    if ARegraImpostoFiltro.FIdRegraImposto > 0 then
    begin
      qEntidade.SetAsInteger('ID_REGRA_IMPOSTO', ARegraImpostoFiltro.FIdRegraImposto);
    end;

    if ARegraImpostoFiltro.FIdEstadoOrigem > 0 then
    begin
      qEntidade.SetAsInteger('ID_ESTADO_ORIGEM', ARegraImpostoFiltro.FIdEstadoOrigem);
    end;

    if ARegraImpostoFiltro.FIdEstadoDestino > 0 then
    begin
      qEntidade.SetAsInteger('ID_ESTADO_DESTINO', ARegraImpostoFiltro.FIdEstadoDestino);
    end;
    qEntidade.SetAsString('DH_INICIO_VIGENCIA', ARegraImpostoFiltro.FDhInicioVigencia);
    qEntidade.SetAsString('DH_FIM_VIGENCIA', ARegraImpostoFiltro.FDhFimVigencia);

    if ARegraImpostoFiltro.FIdImpostoPisCofins > 0 then
    begin
      qEntidade.SetAsInteger('ID_IMPOSTO_PIS_COFINS', ARegraImpostoFiltro.FIdImpostoPisCofins);
    end;

    if ARegraImpostoFiltro.FIdImpostoIcms > 0 then
    begin
      qEntidade.SetAsInteger('ID_IMPOSTO_ICMS', ARegraImpostoFiltro.FIdImpostoIcms);
    end;

    if ARegraImpostoFiltro.FIdImpostoIpi > 0 then
    begin
      qEntidade.SetAsInteger('ID_IMPOSTO_IPI', ARegraImpostoFiltro.FIdImpostoIpi);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    ARegraImpostoFiltro.FID := qEntidade.GetAsInteger('ID');
    result :=     ARegraImpostoFiltro.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar RegraImpostoFiltro.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TRegraImpostoFiltroServ.BuscarImpostoAdequado(const AIdProduto, AIdPessoa, AIdFilial,
  AIdRegraImposto: Integer): TRegraImpostoFiltroProxy;
var
  SQL_FILTRO_IMPOSTO: String;
  qProduto: IFastQuery;
  qFilial: IFastQuery;
  qPessoa: IFastQuery;
  parametrosConsulta: TList<Variant>;
  qConsultaRegraImpostoFiltro: IFastQuery;
  idEstadoPessoa: Integer;
begin
  result := TRegraImpostoFiltroProxy.Create;

  parametrosConsulta := TList<Variant>.Create;
  try
    qProduto := TProdutoServ.GetProdutoQuery(AIdProduto);
    qFilial := TFilialServ.GetFilialQuery(AIdFilial);
    qPessoa := TPessoaServ.GetPessoaQuery(AIdPessoa);

    SQL_FILTRO_IMPOSTO :=
    ' SELECT id'+
    '   FROM regra_imposto_filtro'+
    '  WHERE id_regra_imposto = :id_regra_imposto';


    parametrosConsulta.Add(AIdRegraImposto);

    if not qPessoa.GetAsString('tipo_empresa').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((tipo_empresa_pessoa = :tipo_empresa_pessoa) OR (tipo_empresa_pessoa IS NULL))';
      parametrosConsulta.Add(qPessoa.GetAsString('tipo_empresa'));
    end;

    if not qProduto.GetAsString('forma_aquisicao').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((forma_aquisicao = :forma_aquisicao) OR (forma_aquisicao IS NULL))';
      parametrosConsulta.Add(qProduto.GetAsString('forma_aquisicao'));
    end;

    if not qProduto.GetAsString('origem_da_mercadoria').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((origem_da_mercadoria = :origem_da_mercadoria) OR (origem_da_mercadoria IS NULL))';
      parametrosConsulta.Add(qProduto.GetAsString('origem_da_mercadoria'));
    end;

    if not qPessoa.GetAsString('regime_tributario').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((regime_tributario_destinatario = :regime_tributario_destinatario) OR (regime_tributario_destinatario IS NULL))';
      parametrosConsulta.Add(qPessoa.GetAsString('regime_tributario'));
    end;

    if not qFilial.GetAsString('regime_tributario').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((regime_tributario_emitente = :regime_tributario_emitente) OR (regime_tributario_emitente IS NULL))';

      if qFilial.GetAsString('regime_tributario').Equals(TFilialProxy.REGIME_TRIBUTARIO_SIMPLES) then
      begin
        parametrosConsulta.Add(TConstantesFiscais.REGIME_TRIBUTARIO_SIMPLES_NACIONAL);
      end
      else
      begin
        parametrosConsulta.Add(TConstantesFiscais.REGIME_TRIBUTARIO_RPA);
      end;
    end;

    if not qFilial.GetAsString('crt').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((crt_emitente = :crt_emitente) OR (crt_emitente IS NULL))';
      parametrosConsulta.Add(qPessoa.GetAsString('crt'));
    end;

    if not qFilial.GetAsString('bo_contribuinte_icms').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((bo_contribuinte_icms_emitente = :bo_contribuinte_icms_emitente) OR (bo_contribuinte_ipi_emitente = ''I''))';
      parametrosConsulta.Add(qFilial.GetAsString('bo_contribuinte_icms'));
    end;

    if not qFilial.GetAsString('bo_contribuinte_ipi').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((bo_contribuinte_ipi_emitente = :bo_contribuinte_ipi_emitente) OR (bo_contribuinte_ipi_emitente = ''I''))';
      parametrosConsulta.Add(qFilial.GetAsString('bo_contribuinte_ipi'));
    end;

    if not qPessoa.GetAsString('bo_contribuinte_icms').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((bo_contribuinte_icms_destinatario = :bo_contribuinte_icms_destinatario) OR (bo_contribuinte_ipi_emitente = ''I''))';
      parametrosConsulta.Add(qPessoa.GetAsString('bo_contribuinte_icms'));
    end;

    if not qPessoa.GetAsString('bo_contribuinte_ipi').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((bo_contribuinte_ipi_destinatario = :bo_contribuinte_ipi_destinatario) OR (bo_contribuinte_ipi_emitente = ''I''))';
      parametrosConsulta.Add(qPessoa.GetAsString('bo_contribuinte_ipi'));
    end;

    {if not qFilial.GetAsString('id_zoneamento').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_zoneamento_emitente = :id_zoneamento_emitente) OR (id_zoneamento_emitente IS NULL))';
      parametrosConsulta.Add(qFilial.GetAsString('id_zoneamento'));
    end;

    if not qPessoa.GetAsString('id_zoneamento').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_zoneamento_destinatario = :id_zoneamento_destinatario) OR (id_zoneamento_destinatario IS NULL))';
      parametrosConsulta.Add(qPessoa.GetAsString('id_zoneamento'));
    end;

    if not qFilial.GetAsString('id_regime_especial').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_regime_especial_emitente = :id_regime_especial_emitente) OR (id_regime_especial_emitente IS NULL))';
      parametrosConsulta.Add(qFilial.GetAsString('id_regime_especial'));
    end;

    if not qPessoa.GetAsString('id_regime_especial').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_regime_especial_destinatario = :id_regime_especial_destinatario) OR (id_regime_especial_destinatario IS NULL))';
      parametrosConsulta.Add(qPessoa.GetAsString('id_regime_especial'));
    end;

    //N�o Implementado
    //'    AND id_situacao_especial                = :id_situacao_especial';

   { if AIdOperacaoFiscal > 0 then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_operacao_fiscal = :id_operacao_fiscal) OR (id_operacao_fiscal IS NULL))';
      //
      parametrosConsulta.Add(AIdOperacaoFiscal);
    end;}

    if not qFilial.GetAsString('id_cidade').IsEmpty then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_estado_origem = :id_estado_origem) OR (id_estado_origem IS NULL))';
      //
      parametrosConsulta.Add(TCEPServ.GetIdEstadoPelaCidade(qFilial.GetAsInteger('id_cidade')));
    end;

    idEstadoPessoa := TPessoaEnderecoServ.GetIDEstadoParaNotaFiscal(qPessoa.GetAsInteger('id'));
    if idEstadoPessoa > 0 then
    begin
      SQL_FILTRO_IMPOSTO := SQL_FILTRO_IMPOSTO + '    AND ((id_estado_destino = :id_estado_destino) OR (id_estado_destino IS NULL))';
      //
      parametrosConsulta.Add(idEstadoPessoa);
    end;

    //filtro estado de orgiem, estado de destino desc, opera��o fiscal

    qConsultaRegraImpostoFiltro := TFastQuery.ModoDeConsulta(SQL_FILTRO_IMPOSTO, parametrosConsulta.ToArray);

    if qConsultaRegraImpostoFiltro.GetAsInteger('id') > 0 then
    begin
      result := TRegraImpostoFiltroServ.GetRegraImpostoFiltro(qConsultaRegraImpostoFiltro.GetAsInteger('id'));
    end;
  finally
    FreeAndNil(parametrosConsulta);
  end;
end;

end.

