unit uServicoServ;

interface

Uses uServicoProxy, SysUtils;

type TServicoServ = class
  class function GetServico(const AIdServico: Integer): TServicoProxy;
  class function GetDescricao(const AIdServico: Integer): String;
  class function GerarServico(const AServico: TServicoProxy): Integer;
  class function ExisteServico(const ADescricaoServico: String): Boolean;
  class function ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
  class function GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
  class function GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
end;

implementation

{ TServicoServ }

uses uFactoryQuery;

class function TServicoServ.GetServico(const AIdServico: Integer): TServicoProxy;
const
  SQL =
  ' SELECT'+
  '   servico.id'+
  '  ,servico.descricao'+
  '  ,servico.duracao'+
  '  ,servico.vl_custo'+
  '  ,servico.vl_venda'+
  '  ,servico.perc_lucro'+
  '  ,servico.bo_servico_terceiro'+
  '  ,servico.bo_ativo'+
  ' FROM SERVICO WHERE id = :id';
var
  consultaServico: IFastQuery;
begin
  result := TServicoProxy.Create;

  consultaServico := TFastQuery.ModoDeConsulta(
    SQL, TArray<Variant>.Create(AIdServico));

  result.FId := consultaServico.GetAsInteger('ID');
  result.FDescricao := consultaServico.GetAsString('DESCRICAO');
  result.FVlCusto := consultaServico.GetAsFloat('VL_CUSTO');
  result.FPercLucro := consultaServico.GetAsFloat('PERC_LUCRO');
  result.FVlVenda := consultaServico.GetAsFloat('VL_VENDA');
  result.FDuracao := consultaServico.GetAsString('DURACAO');
  result.FBoServicoTerceiro := consultaServico.GetAsString('BO_SERVICO_TERCEIRO');
  result.FBoAtivo := consultaServico.GetAsString('BO_ATIVO');
end;

class function TServicoServ.GetDescricao(const AIdServico: Integer): String;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select descricao from servico where id = :id',
    TArray<Variant>.Create(AIdServico));

  result := qEntidade.GetAsString('descricao');
end;

class function TServicoServ.ExisteServico(const ADescricaoServico: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from servico where descricao = :servico and bo_servico_terceiro = ''N''',
    TArray<Variant>.Create(ADescricaoServico));

  result := not qEntidade.IsEmpty;
end;

class function TServicoServ.GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from servico where descricao = :servico and bo_servico_terceiro = ''N''',
    TArray<Variant>.Create(ADescricaoServico));

  result := qEntidade.GetAsInteger('id');
end;

class function TServicoServ.ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from servico where descricao = :servico and bo_servico_terceiro = ''S''',
    TArray<Variant>.Create(ADescricaoServico));

  result := not qEntidade.IsEmpty;
end;

class function TServicoServ.GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from servico where descricao = :servico and bo_servico_terceiro = ''S''',
    TArray<Variant>.Create(ADescricaoServico));

  result := qEntidade.GetAsInteger('id');
end;

class function TServicoServ.GerarServico(
    const AServico: TServicoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('SERVICO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AServico.FDescricao);
    qEntidade.SetAsFloat('VL_CUSTO', AServico.FVlCusto);
    qEntidade.SetAsFloat('PERC_LUCRO', AServico.FPercLucro);
    qEntidade.SetAsFloat('VL_VENDA', AServico.FVlVenda);
    qEntidade.SetAsString('DURACAO', AServico.FDuracao);
    qEntidade.SetAsString('BO_SERVICO_TERCEIRO', AServico.FBoServicoTerceiro);
    qEntidade.SetAsString('BO_ATIVO', AServico.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AServico.FID := qEntidade.GetAsInteger('ID');
    result :=     AServico.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Servico.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
