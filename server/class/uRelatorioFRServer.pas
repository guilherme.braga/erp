unit uRelatorioFRServer;

interface

Uses frxClass, frxVariables, frxFDComponents, System.Classes, System.SysUtils,
  uFactoryQuery, db, Data.FireDACJSONReflect, frxBarcode;

Type TRelatorioFRServer = class
  private
    class procedure CriarParametrosNoRelatorio(ARelatorio: TFrxReport;
      const AIdMaster: Integer; const AWhere: String);

    class procedure CarregarRelatorioDoBanco(ARelatorio: TFrxReport;
      AIdRelatorio: Integer);

    class procedure ConfigurarConexaoNoRelatorio(ARelatorio: TFrxReport);
  public
    class function BuscarRelatorio(const AIdMaster, AIdRelatorio: Integer;
      const AWhere: String): TStream;

    class function BuscarRelatoriosAssociados(const AClasseFormulario: String;
      AIdUsuario: Integer): TFDJSONDataSets;
end;

implementation

{ TRelatorioFRServer }

uses uIniFileServ;

class function TRelatorioFRServer.BuscarRelatorio(const AIdMaster,
  AIdRelatorio: Integer; const AWhere: String): TStream;
var Relatorio: TFrxReport;
begin
  try
    Relatorio := TFrxReport.Create(nil);
    try
      result := TMemoryStream.Create;
      result.Position := 0;

      TRelatorioFRServer.CarregarRelatorioDoBanco(Relatorio, AIdRelatorio);

      TRelatorioFRServer.CriarParametrosNoRelatorio(Relatorio, AIdMaster, AWhere);

      Relatorio.PrepareReport();
      Relatorio.SaveToStream(result);
      result.Position := 0;
    finally
      FreeAndNil(Relatorio);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Relatorio.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TRelatorioFRServer.BuscarRelatoriosAssociados(
  const AClasseFormulario: String; AIdUsuario: Integer): TFDJSONDataSets;
var
  qRelatorio: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qRelatorio := TFastQuery.ModoDeConsulta(
    ' SELECT * FROM relatorio_fr '+
    ' WHERE id = :id and '+
    ' Exists('+
    '          select 1'+
    '          from relatorio_fr_formulario'+
    '          where id_relatorio_fr = relatorio_fr.id_relatorio_fr and'+
    '          id_pessoa_usuario = :id_pessoa_usuario)',
    TArray<Variant>.Create(AIdUsuario));

  TFDJSONDataSetsWriter.ListAdd(result, (qRelatorio as TFastQuery));
end;

class procedure TRelatorioFRServer.CarregarRelatorioDoBanco(
  ARelatorio: TFrxReport; AIdRelatorio: Integer);
var
  qRelatorio: IFastQuery;
  ArquivoStream: TStream;
begin
  ArquivoStream := TMemoryStream.Create;
  try
    ArquivoStream.Position := 0;

    qRelatorio := TFastQuery.ModoDeConsulta('SELECT arquivo FROM relatorio_fr '+
      ' WHERE id = :id', TArray<Variant>.Create(AIdRelatorio));

    TBlobField((qRelatorio as TFastQuery).FieldByName('ARQUIVO')).SaveToStream(
      ArquivoStream);

    ArquivoStream.Position := 0;

    ARelatorio.LoadFromStream(ArquivoStream);

    ConfigurarConexaoNoRelatorio(ARelatorio);
  finally
    FreeAndNil(ArquivoStream);
  end;
end;

class procedure TRelatorioFRServer.ConfigurarConexaoNoRelatorio(ARelatorio: TFrxReport);
var
  i: Integer;
  conexao: TfrxFDDatabase;
begin
  for i := 0 to Pred(ARelatorio.ComponentCount) do
  begin
    if ARelatorio.Components[i] is TfrxFDDatabase then
    begin
      conexao := TfrxFDDatabase(ARelatorio.Components[i]);
      conexao.Connected := False;
      conexao.Params.Clear;
      conexao.Params.Add('Database='+IniSystemFile.databaseERP);
      conexao.Params.Add('User_Name='+TIniSystemFile.usuarioMYSQL);
      conexao.Params.Add('Password='+TIniSystemFile.senhaMYSQL);
      conexao.Params.Add('Server='+IniSystemFile.server);
      conexao.Params.Add('Port='+IniSystemFile.porta);
      conexao.Params.Add('DriverID=MySQL');
      conexao.DatabaseName := IniSystemFile.databaseERP;
      conexao.DriverName := 'MYSQL';

      break;
    end;
  end;
end;

class procedure TRelatorioFRServer.CriarParametrosNoRelatorio(
  ARelatorio: TFrxReport; const AIdMaster: Integer; const AWhere: String);
var
  variavel: TFrxVariable;
  indiceDaBusca: Integer;
begin
  indiceDaBusca := ARelatorio.Variables.IndexOf('ID_MASTER');
  if indiceDaBusca >= 0 then
  begin
    ARelatorio.Variables.Items[indiceDaBusca].Value := AIdMaster;
  end
  else
  begin
    variavel := ARelatorio.Variables.Add;
    variavel.Name := 'ID_MASTER';
    variavel.Value := AIdMaster;
  end;

  indiceDaBusca := ARelatorio.Variables.IndexOf('WHERE');
  if indiceDaBusca >= 0 then
  begin
    ARelatorio.Variables.Items[indiceDaBusca].Value := QuotedStr(AWhere);
  end
  else
  begin
    variavel := ARelatorio.Variables.Add;
    variavel.Name := 'WHERE';
    variavel.Value := '''';
  end;
end;

end.
