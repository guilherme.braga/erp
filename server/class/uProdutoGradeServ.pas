unit uProdutoGradeServ;

interface

uses uProdutoGradeProxy, Generics.Collections, SysUtils, Data.FireDACJSONReflect, Classes;

type TProdutoGradeServ = class
  private
    class function GetFieldsReferenteAsCores(const AIdGradeProduto: Integer): String;
  public
    class function GerarProdutos(const AProdutosEmGrade: TList<TProdutoGradeProxy>): Boolean;
    class function VisaoProdutoGrade(const AIdGradeProduto, AIdProduto: Integer): TFDJsonDatasets;
end;

implementation

{ TProdutoGradeServ }

uses uProdutoServ, uFactoryQuery, uGradeProdutoServ, uCorServ;

class function TProdutoGradeServ.GerarProdutos(const AProdutosEmGrade: TList<TProdutoGradeProxy>): Boolean;
var
  i: Integer;
  produtoGrade: TProdutoGradeProxy;
  idNovoProduto: Integer;
  produtoJaIncluido: Boolean;
  produtoAgrupadoJaIncluidoEConfigurado: Boolean;
begin
  result := false;
  try
    for i := 0 to Pred(AProdutosEmGrade.Count) do
    begin
      produtoAgrupadoJaIncluidoEConfigurado := false;
      produtoGrade := AProdutosEmGrade[i];

      if (produtoGrade.FIdProduto <= 0) and (not produtoGrade.FCodigoBarra.IsEmpty) then
      begin
        produtoGrade.FIdProduto := TProdutoServ.CodigoDeBarraJaCadastrado(produtoGrade.FCodigoBarra,
          produtoGrade.FIdProduto);
      end
      else if (produtoGrade.FIdProduto <= 0) and (produtoGrade.FCodigoBarra.IsEmpty) then
      begin
        produtoGrade.FIdProduto := TProdutoServ.BuscarIdProdutoAgrupadoPorCorTamanho(
          produtoGrade.FIdProdutoAgrupador, produtoGrade.FIdCor, produtoGrade.FIdTamanho);

        produtoAgrupadoJaIncluidoEConfigurado := produtoGrade.FIdProduto > 0;
      end;

      produtoJaIncluido := produtoGrade.FIdProduto > 0;

      if not produtoJaIncluido then
      begin
        idNovoProduto := TProdutoServ.DuplicarProduto(produtoGrade.FIdProdutoAgrupador);
      end
      else
      begin
        idNovoProduto := produtoGrade.FIdProduto;
      end;

      if (idNovoProduto > 0) and not(produtoAgrupadoJaIncluidoEConfigurado) then
      begin
        TProdutoServ.SetProdutoAgrupado(produtoGrade.FIdProdutoAgrupador, idNovoProduto);
        TProdutoServ.AlterarTamanhoCorProduto(idNovoProduto, produtoGrade.FIdCor, produtoGrade.FIdTamanho);

        if not produtoGrade.FCodigoBarra.IsEmpty then
        begin
          TProdutoServ.AlterarCodigoBarraProduto(idNovoProduto, produtoGrade.FCodigoBarra);
        end;
      end;
    end;

    TProdutoServ.SetProdutoAgrupador(produtoGrade.FIdProdutoAgrupador);

    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar produtos em grade.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TProdutoGradeServ.VisaoProdutoGrade(const AIdGradeProduto, AIdProduto: Integer): TFDJsonDatasets;
var qConsulta: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

{ qConsulta := TFastQuery.ModoDeConsulta(
    ' SELECT cast(0 as signed int) as ID_PRODUTO_AGRUPADOR'+
    TProdutoGradeServ.GetFieldsReferenteAsCores(AIdGradeProduto)+
    '       ,grade_produto_cor_tamanho.ID_TAMANHO'+
    '       ,cast('''' as CHAR(20)) as CODIGO_BARRA'+
    '       ,(select descricao from tamanho where id = grade_produto_cor_tamanho.ID_TAMANHO) as JOIN_DESCRICAO_TAMANHO'+
    '   FROM grade_produto_cor_tamanho'+
    '  WHERE id_grade_produto = :id_grade_produto',
    TArray<Variant>.Create(AIdProduto, AIdGradeProduto));}

  qConsulta := TFastQuery.ModoDeConsulta(
    ' select * from (SELECT grade_produto_cor_tamanho.*' +
    '        ,(select descricao from cor where id = grade_produto_cor_tamanho.id_cor) AS JOIN_DESCRICAO_COR'+
    '        ,(select descricao from tamanho where id = grade_produto_cor_tamanho.id_tamanho) AS JOIN_DESCRICAO_TAMANHO'+
    '        ,ifNull((select id from produto where id_cor = grade_produto_cor_tamanho.id_cor and'+
    ' id_tamanho = grade_produto_cor_tamanho.id_tamanho and id_produto_agrupador = :id_produto_agrupador limit 1),'+
    ' 0) as ID_PRODUTO'+
    ' ,cast(0 as Decimal(24,9)) AS QUANTIDADE'+
    '   FROM grade_produto_cor_tamanho'+
    '  WHERE id_grade_produto = :id_grade_produto) grade order by JOIN_DESCRICAO_COR, JOIN_DESCRICAO_TAMANHO',
    TArray<Variant>.Create(AIdProduto, AIdGradeProduto));

  TFDJSONDataSetsWriter.ListAdd(Result, (qConsulta as TFastQuery));
end;

class function TProdutoGradeServ.GetFieldsReferenteAsCores(const AIdGradeProduto: Integer): String;
var
  listaCampos: TStringList;
  qGradeProdutoCor: TFastQuery;
  qGradeProdutoTamanhoCor: TFastQuery;
  idCor: Integer;
  idTamanho: Integer;
const
  //FIELD_ID_COR = ' ,%d AS ID_COR_%d ';
  FIELD_DESCRICAO_COR = ' ,cast(0 as Decimal(24,9)) AS DESCRICAO_COR_%d ';
  FIELD_ID_PRODUTO =    ' ,ifNull((select id from produto where id_cor = %d and'+
    ' id_tamanho = grade_produto_cor_tamanho.ID_TAMANHO and id_produto_agrupador = :id_produto_agrupador limit 1),'+
    ' 0) as ID_PRODUTO_COR_%d_TAMANHO_%d';
begin
  listaCampos :=  TStringList.Create;
  try
    qGradeProdutoCor := TGradeProdutoServ.GetGradeProdutoCor(AIdGradeProduto);
    try
      qGradeProdutoCor.First;
      while not qGradeProdutoCor.Eof do
      begin
        idCor := qGradeProdutoCor.GetAsInteger('ID_COR');

        //listaCampos.Add(Format(FIELD_ID_COR, [idCor, idCor]));
        listaCampos.Add(Format(FIELD_DESCRICAO_COR,[idCor]));
        qGradeProdutoCor.Next;
      end;
    finally
      FreeAndNil(qGradeProdutoCor);
    end;

    qGradeProdutoTamanhoCor := TGradeProdutoServ.GetGradeProdutoCorTamanho(AIdGradeProduto);
    try
      qGradeProdutoTamanhoCor.First;
      while not qGradeProdutoTamanhoCor.Eof do
      begin
        idCor := qGradeProdutoTamanhoCor.GetAsInteger('ID_COR');
        idTamanho := qGradeProdutoTamanhoCor.GetAsInteger('ID_TAMANHO');

        listaCampos.Add(Format(FIELD_ID_PRODUTO,[idCor, idCor, idTamanho]));
        qGradeProdutoTamanhoCor.Next;
      end;
    finally
      FreeAndNil(qGradeProdutoTamanhoCor);
    end;

    result := listaCampos.Text;
  finally
    FreeAndNil(listaCampos);
  end;
end;

end.
