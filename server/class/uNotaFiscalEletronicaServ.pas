unit uNotaFiscalEletronicaServ;

interface

Uses ACBrNFE, uFactoryQuery, uPessoaProxy, uFilialProxy, SysUtils, Classes, uConfiguracaoFiscalProxy,
  uUsuarioProxy, pcnNFE, uFrmComponentesFiscais, Windows, Forms, uRetornoProxy, Dialogs, ACBrNFeDANFEFRDM,
  frxClass;

type TNotaFiscalEletronicaServ = class
  private
    FComponentesFiscais: TFrmComponentesFiscais;
    FACbrNFE: TACbrNFE;
    FInconsistencias: TStringList;
    FTipoDocumentoFiscal: String;
    procedure ValidarPessoa(APessoa: TPessoaProxy; APessoaEndereco: TPessoaEnderecoProxy);
    procedure ValidarFilial(AFilial: TFilialProxy);
    procedure ValidarConfiguracoesNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
    procedure ValidarConfiguracoesNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
    procedure InstanciarComponentesACBr(const AFilial: TFilialProxy; AUsuario: TUsuarioProxy;
      AConfiguracoesFiscais: TConfiguracaoFiscalProxy);
    procedure DestruirComponentesACBr;
    procedure ConfigurarACBrNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy; AFilial: TFilialProxy;
      AUsuario: TUsuarioProxy);
    procedure AdicionarNotaFiscal(ANotaFiscal, ANotaFiscalItem, ANotaFiscalParcela: TFastQuery;
      APessoa: TPessoaProxy; APessoaEndereco: TPessoaEnderecoProxy; AFilial: TFilialProxy);
    procedure ValidarServidorAtivo(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);

    procedure PreencherNotaFiscalGrupoInformacoesNFe(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoIdentificacaoNFe(ANFe: TNFe; ANotaFiscal: TFastQuery;
      AFilial: TFilialProxy; APessoaEndereco: TPessoaEnderecoProxy);
    procedure PreencherNotaFiscalGrupoIdentificacaoEmitente(ANFe: TNFe; AFilial: TFilialProxy);
    procedure PreencherNotaFiscalGrupoIdentificacaoDestinatario(ANFe: TNFe; APessoa: TPessoaProxy;
      APessoaEndereco: TPessoaEnderecoProxy);
    procedure PreencherNotaFiscalGrupoProdutos(ANFe: TNFe; ANotaFiscalItem: TFastQuery; AFilial: TFilialProxy);
    procedure PreencherNotaFiscalGrupoProdutosInformacaoProduto(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosDI(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosMedicamento(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosCombustivel(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosICMS(AProdutoNFe: TDetCollectionItem;
      ANotaFiscalItem: TFastQuery; AFilial: TFilialProxy);
    procedure PreencherNotaFiscalGrupoProdutosIPI(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosII(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosPIS(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosCOFINS(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoProdutosISSQN(AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoFrete(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoVolume(ANFe: TNFe; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesAdicionais(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesAdicionaisIBPT(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesAdicionaisObservacaoFisco(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesAdicionaisProcesso(ANFe: TNFe; ANotaFiscal: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesParcelas(ANFe: TNFe; ANotaFiscal,
      ANotaFiscalParcela: TFastQuery);
    procedure PreencherNotaFiscalGrupoTotais(ANFe: TNFe; ANotaFiscal, ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoTotaisICMS(ANFe: TNFe; ANotaFiscal, ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesComercioExterior(ANFe: TNFe; ANotaFiscalItem: TFastQuery);
    procedure PreencherNotaFiscalGrupoInformacoesCompra(ANFe: TNFe; ANotaFiscalItem: TFastQuery);

    procedure GerarNFE;
    procedure AssinarNFE;
    procedure ValidarNFEGerada;
    procedure AtribuirChaveNFENaNotaFiscal(const AIdNotaFiscal: Integer);
    procedure EnviarNFE(const AConfiguracaoFiscal: TConfiguracaoFiscalProxy; const ANotaFiscal: TFastQuery);
    procedure EnviarEmail(const AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
    function ValidarConfiguracoesEmail(const AConfiguracaoFiscal: TConfiguracaoFiscalProxy): Boolean;
    function BuscarMensagemRetornoNFE(const ACodigoStatus: Integer; const AMotivo: String): String;

    procedure AtribuirDocumentoFederalPessoa(const APessoa: TPessoaProxy; ANotaFiscal: TFastQuery);

    function ExisteInconsistencias: Boolean;

    function BuscarRelatorioStream: TStream;
  public
    class function EmitirNFE(const AIdNotaFiscal: Integer; const ATipoDocumentoFiscal: String): String;
    class function CancelarDocumentoFiscal(const AIdNotaFiscal: Integer; const AMotivoCancelamento: String;
      const ATipoDocumentoFiscal: String): Boolean;
    class function ImprimirDocumentoFiscal(const AIdNotaFiscal: Integer;
      const ATipoDocumentoFiscal: String): String;
    class function ImprimirNFE(const AXML: String; const AIdFilial, AIdUsuario: Integer): String;
    class function ImprimirNFCE(const AXML: String; const AIdFilial, AIdUsuario: Integer): String;
    class function NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
    class function ChecarServidorAtivo(const AIdFilial, AIdUsuario: Integer): String;
    class procedure InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);

    constructor Create;
    destructor Destroy; override;
end;

implementation

{ TNotaFiscalEletronicaServ }

uses uNotaFiscalServ, uPessoaServ, uFilialServ, Rest.JSON,
  uConfiguracaoFiscalServ, pcnConversao, ACBrNFeDANFEFR, uUsuarioServer, uNotaFiscalProxy, StrUtils,
  uOperacaoServ, uCNAEServ, uModeloNotaServ, uStringUtils, pcnConversaoNFe, uMathUtils, ACBrDFeSSL,
  vcl.Printers, uCESTServ, DateUtils, uGBFDQuery;

procedure TNotaFiscalEletronicaServ.AdicionarNotaFiscal(ANotaFiscal, ANotaFiscalItem,
  ANotaFiscalParcela: TFastQuery; APessoa: TPessoaProxy; APessoaEndereco: TPessoaEnderecoProxy;
  AFilial: TFilialProxy);
var
  notaFiscal: TNFe;
begin
  notaFiscal := FACBrNFe.NotasFiscais.Add.NFe;

  PreencherNotaFiscalGrupoInformacoesNFe(notaFiscal, ANotaFiscal);
  PreencherNotaFiscalGrupoIdentificacaoNFe(notaFiscal, ANotaFiscal, AFilial, APessoaEndereco);
  PreencherNotaFiscalGrupoIdentificacaoEmitente(notaFiscal, AFilial);
  PreencherNotaFiscalGrupoIdentificacaoDestinatario(notaFiscal, APessoa, APessoaEndereco);
  PreencherNotaFiscalGrupoProdutos(notaFiscal, ANotaFiscalItem, AFilial);
  PreencherNotaFiscalGrupoFrete(notaFiscal, ANotaFiscal);
  PreencherNotaFiscalGrupoVolume(notaFiscal, ANotaFiscal); //Fora de Uso
  PreencherNotaFiscalGrupoInformacoesAdicionais(notaFiscal, ANotaFiscal);
  PreencherNotaFiscalGrupoInformacoesParcelas(notaFiscal, ANotaFiscal, ANotaFiscalParcela);
  PreencherNotaFiscalGrupoTotais(notaFiscal, ANotaFiscal, ANotaFiscalItem);
  PreencherNotaFiscalGrupoInformacoesComercioExterior(notaFiscal, ANotaFiscal); //Fora de Uso
  PreencherNotaFiscalGrupoInformacoesCompra(notaFiscal, ANotaFiscal); //Fora de Uso
end;

procedure TNotaFiscalEletronicaServ.AssinarNFE;
begin
  try
    FACbrNFE.NotasFiscais.Assinar;
  except
    on e: exception do
    begin
      FInconsistencias.Add(e.message);
      exit;
    end;
  end;
end;

procedure TNotaFiscalEletronicaServ.AtribuirChaveNFENaNotaFiscal(const AIdNotaFiscal: Integer);
begin
  TNotaFiscalServ.AtribuirChaveNFE(AIdNotaFiscal, copy(FACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID, 4,
    length(FACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID)));
end;

procedure TNotaFiscalEletronicaServ.AtribuirDocumentoFederalPessoa(const APessoa: TPessoaProxy;
  ANotaFiscal: TFastQuery);
begin
  if FTipoDocumentoFiscal <> TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    exit;
  end;

  if APessoa.FDocFederal.IsEmpty and (not ANotaFiscal.GetAsString('DOC_FEDERAL_PESSOA').IsEmpty) then
  begin
    APessoa.FDocFederal := ANotaFiscal.GetAsString('DOC_FEDERAL_PESSOA');
  end;
end;

function TNotaFiscalEletronicaServ.BuscarMensagemRetornoNFE(const ACodigoStatus: Integer;
  const AMotivo: String): String;
begin
  //Implementar quando o thiago conseguir os dados da sc.tab_msg_nfe
  result := InttoStr(ACodigoStatus) + ' - ' +AMotivo
end;

function TNotaFiscalEletronicaServ.BuscarRelatorioStream: TStream;
begin
  try
    result := TMemoryStream.Create;
    result.Position := 0;

    TACBrNFeDANFEFR(FACbrNFE.DANFE).dmDanfe.frxReport.SaveToStream(result);
    result.Position := 0;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar processar relat�rio para retornar.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

function TNotaFiscalEletronicaServ.ExisteInconsistencias: Boolean;
begin
  result := FInconsistencias.Count > 0;
end;

class function TNotaFiscalEletronicaServ.CancelarDocumentoFiscal(const AIdNotaFiscal: Integer;
  const AMotivoCancelamento: String; const ATipoDocumentoFiscal: String): Boolean;
var vAux, cNomeArquivo: string;
  nfe: TNotaFiscalEletronicaServ;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
  qNotaFiscal: TFastQuery;
begin
  try
    nfe := TNotaFiscalEletronicaServ.Create;
    try
      nfe.FTipoDocumentoFiscal := ATipoDocumentoFiscal;
      qNotaFiscal := TNotaFiscalServ.GetNotaFiscalPorId(AIdNotaFiscal);

      configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(qNotaFiscal.GetAsInteger('ID_FILIAL'));
      filial := TFilialServ.GetFilial(qNotaFiscal.GetAsInteger('ID_FILIAL'));
      usuario := TUsuarioServ.GetPessoaUsuario(qNotaFiscal.GetAsInteger('ID_PESSOA_USUARIO'));

      nfe.InstanciarComponentesACBr(filial, usuario, configuracoesFiscais);
      nfe.FACbrNFE.NotasFiscais.Clear;
      nfe.FACbrNFE.NotasFiscais.LoadFromString(qNotaFiscal.GetAsString('DOCUMENTO_FISCAL_XML'));
      nfe.FACbrNFE.Cancelamento(AMotivoCancelamento);

      TNotaFiscalServ.AtualizarStatusDocumentoFiscal(AIdNotaFiscal, TNotaFiscalProxy.STATUS_NFE_CANCELADA);

      result := true;
    finally
      FreeAndNil(nfe);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao cancelar documento fiscal.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TNotaFiscalEletronicaServ.ChecarServidorAtivo(const AIdFilial, AIdUsuario: Integer): String;
var
  nfe: TNotaFiscalEletronicaServ;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  retornoProxy: TRetornoDocumentoFiscalProxy;
begin
  retornoProxy := TRetornoDocumentoFiscalProxy.Create;
  retornoProxy.FRetorno := false;

  nfe := TNotaFiscalEletronicaServ.Create;
  try
    nfe.FTipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE;
    configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(AIdFilial);

    filial := TFilialServ.GetFilial(AIdFilial);
    usuario := TUsuarioServ.GetPessoaUsuario(AIdUsuario);
    try
      nfe.InstanciarComponentesACBr(filial, usuario, configuracoesFiscais);
      nfe.ValidarServidorAtivo(configuracoesFiscais);
    finally
      FreeAndNil(filial);
      FreeAndNil(usuario);
    end;
    retornoProxy.FRetorno := true;
  finally
    retornoProxy.FMensagensCommaText := nfe.FInconsistencias.CommaText;
    result := TJSON.ObjectToJsonString(retornoProxy);
    FreeAndNil(nfe);
  end;
end;

procedure TNotaFiscalEletronicaServ.ConfigurarACBrNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  FComponentesFiscais.ConfigurarACBrNFCE(AConfiguracaoFiscal, AFilial, AUsuario);
end;

procedure TNotaFiscalEletronicaServ.ConfigurarACBrNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  FComponentesFiscais.ConfigurarACBrNFE(AConfiguracaoFiscal, AFilial, AUsuario);
end;

procedure TNotaFiscalEletronicaServ.ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  FComponentesFiscais.ConfigurarACBrNFCEDanfe(AConfiguracaoFiscal, AFilial, AUsuario);
end;

procedure TNotaFiscalEletronicaServ.ConfigurarACBrNFEDanfe(AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  AFilial: TFilialProxy; AUsuario: TUsuarioProxy);
begin
  FComponentesFiscais.ConfigurarACBrNFEDanfe(AConfiguracaoFiscal, AFilial, AUsuario);
end;

constructor TNotaFiscalEletronicaServ.Create;
begin
  FInconsistencias := TStringList.Create;
end;

destructor TNotaFiscalEletronicaServ.Destroy;
begin
  if Assigned(FInconsistencias) then
  begin
    FreeAndNil(FInconsistencias);
  end;

  DestruirComponentesACBr;

  inherited;
end;

procedure TNotaFiscalEletronicaServ.DestruirComponentesACBr;
begin
  if Assigned(FComponentesFiscais) then
  begin
    FreeAndNil(FComponentesFiscais);
  end;
end;

function TNotaFiscalEletronicaServ.ValidarConfiguracoesEmail(
  const AConfiguracaoFiscal: TConfiguracaoFiscalProxy): Boolean;
begin
  if AConfiguracaoFiscal.FEmailEmailDestino.IsEmpty then
  begin
    result := false;
    exit;
  end;

  if (FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE) and
    (not AConfiguracaoFiscal.FEmailEnviarNfe.Equals('S')) then
  begin
    result := false;
    exit;
  end;

  if (FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE) and
    (not AConfiguracaoFiscal.FEmailEnviarNfe.Equals('S')) then
  begin
    result := false;
    exit;
  end;
end;

procedure TNotaFiscalEletronicaServ.EnviarEmail(const AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
var
  listaCC, listaCCO, mensagem: TStringList;
const
  ENVIAR_PDF = true;
begin
  if not ValidarConfiguracoesEmail(AConfiguracaoFiscal) then
  begin
    exit;
  end;

  listaCC := TstringList.Create;
  mensagem := TstringList.Create;
  try
    listaCC.Delimiter := ';';
    listaCC.DelimitedText := AConfiguracaoFiscal.FEmailEmailCc;

    mensagem.Add(AConfiguracaoFiscal.FEmailMensagem);

    FComponentesFiscais.ACBrMail.Host := AConfiguracaoFiscal.FEmailEmailHost;
    FComponentesFiscais.ACBrMail.Port := AConfiguracaoFiscal.FEmailPorta;
    FComponentesFiscais.ACBrMail.Username := AConfiguracaoFiscal.FEmailUsuario;
    FComponentesFiscais.ACBrMail.Password := AConfiguracaoFiscal.FEmailSenha;
    FComponentesFiscais.ACBrMail.From := AConfiguracaoFiscal.FEmailSmtp;
    FComponentesFiscais.ACBrMail.SetSSL := AConfiguracaoFiscal.FEmailSSL.Equals('S'); // SSL - Conexão Segura
    FComponentesFiscais.ACBrMail.SetTLS := AConfiguracaoFiscal.FEmailSSL.Equals('S');
    FComponentesFiscais.ACBrMail.ReadingConfirmation := AConfiguracaoFiscal.FEmailSolicitaConfirmacao.Equals('S');
    FComponentesFiscais.ACBrMail.UseThread := AConfiguracaoFiscal.FEmailUsarThread.Equals('S');
    FComponentesFiscais.ACBrMail.FromName := AConfiguracaoFiscal.FEmailNomeOrigem;

    FACBrNFe.NotasFiscais.Items[0].EnviarEmail(
       AConfiguracaoFiscal.FEmailEmailDestino
      ,AConfiguracaoFiscal.FEmailAssunto
      ,mensagem
      ,ENVIAR_PDF // Enviar PDF junto
      ,listaCC // Lista com emails que serão enviado cópias - TStrings
      , nil); // Lista de anexos - TStrings
  finally
    FreeAndNil(listaCC);
    FreeAndNil(mensagem);
  end;
end;

procedure TNotaFiscalEletronicaServ.EnviarNFE(const AConfiguracaoFiscal: TConfiguracaoFiscalProxy;
  const ANotaFiscal: TFastQuery);
var
  protocoloDPEC: String;
begin
  if AnsiIndexStr(AConfiguracaoFiscal.FNfeFormaEmissao,
    [TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_NORMAL
    ,TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_SCAN]) >= 0 then
  begin
    FACBrNFe.WebServices.Envia(0);
    exit;
  end;

  if AnsiIndexStr(AConfiguracaoFiscal.FNfeFormaEmissao,
    [TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_DPEC]) >= 0 then
  begin
{    FACBrNFe.WebServices.EnviarDPEC.Executar;

    protocoloDPEC := TNotaFiscalServ.ProtocolarDPEC(ANotaFiscal.GetAsInteger('id'),
      FACBrNFe.WebServices.EnviarDPEC.nRegDPEC);

    FACBrNFe.DANFE.ProtocoloNFe := protocoloDPEC;}
  end;
end;

procedure TNotaFiscalEletronicaServ.GerarNFE;
begin
  try
    FACbrNFE.NotasFiscais.GerarNFe;
  except
    on e: exception do
    begin
      FInconsistencias.Add(e.message);
      exit;
    end;
  end;
end;

class function TNotaFiscalEletronicaServ.EmitirNFE(const AIdNotaFiscal: Integer;
  const ATipoDocumentoFiscal: String): String;
var
  qNotaFiscal: TFastQuery;
  qNotaFiscalItem: TFastQuery;
  qNotaFiscalParcela: TFastQuery;
  nfe: TNotaFiscalEletronicaServ;
  pessoa: TPessoaProxy;
  pessoaEndereco: TPessoaEnderecoProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  retornoProxy: TRetornoDocumentoFiscalProxy;
  statusNFE: String;
begin
  retornoProxy := TRetornoDocumentoFiscalProxy.Create;
  retornoProxy.FRetorno := false;
  nfe := TNotaFiscalEletronicaServ.Create;
  try
    if TNotaFiscalEletronicaServ.NFEEmitida(AIdNotaFiscal) then
    begin
      retornoProxy.FRetorno := true;
      exit;
    end;

    nfe.FTipoDocumentoFiscal := ATipoDocumentoFiscal;
    qNotaFiscal := TNotaFiscalServ.GetNotaFiscalPorId(AIdNotaFiscal);
    qNotaFiscalItem := TNotaFiscalServ.GetNotaFiscalItem(AIdNotaFiscal);
    qNotaFiscalParcela := TNotaFiscalServ.GetNotaFiscalParcela(AIdNotaFiscal);
    try
      configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(
        qNotaFiscal.GetAsInteger('id_filial'));

      pessoa := TPessoaServ.GetPessoa(qNotaFiscal.GetAsInteger('id_pessoa'));
      nfe.AtribuirDocumentoFederalPessoa(pessoa, qNotaFiscal);

      filial := TFilialServ.GetFilial(qNotaFiscal.GetAsInteger('id_filial'));
      usuario := TUsuarioServ.GetPessoaUsuario(qNotaFiscal.GetAsInteger('id_pessoa_usuario'));
      pessoaEndereco := TPessoaEnderecoServ.GetPessoaEnderecoParaNotaFiscal(qNotaFiscal.GetAsInteger('id_pessoa'));
      try
        nfe.ValidarPessoa(pessoa, pessoaEndereco);
        nfe.ValidarFilial(filial);

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.InstanciarComponentesACBr(filial, usuario, configuracoesFiscais);

        nfe.ValidarServidorAtivo(configuracoesFiscais);

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.AdicionarNotaFiscal(qNotaFiscal, qNotaFiscalItem, qNotaFiscalParcela, pessoa,
          pessoaEndereco, filial);

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.GerarNFE;

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.AssinarNFE;

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.ValidarNFEGerada;

        if nfe.ExisteInconsistencias then
        begin
          Exit;
        end;

        nfe.AtribuirChaveNFENaNotaFiscal(AIdNotaFiscal);

        try
          try
            //ShowMessage('Vai enviar');
            nfe.EnviarNFE(configuracoesFiscais, qNotaFiscal);
            //ShowMessage('enviou');
            TNotaFiscalServ.GravarXMLDanfe(AIdNotaFiscal, nfe.FACbrNfe.NotasFiscais[0].XML);
          Except
            on e: Exception do
            begin
              nfe.FInconsistencias.Add('Rejei��o: '+
                InttoStr(nfe.FACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].cStat)+' - '+
                nfe.FACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].xMotivo);

              exit;
            end;
          end;

          configuracoesFiscais.FEmailEmailDestino := TPessoaContatoServ.GetEmailDocumentoFiscal(pessoa.FId);
          nfe.EnviarEmail(configuracoesFiscais);
        finally
          if nfe.FACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].cStat = 100 then
          begin
            statusNFE := TNotaFiscalProxy.STATUS_NFE_ENVIADA;
          end
          else
          begin
            statusNFE := TNotaFiscalProxy.STATUS_NFE_AENVIAR;
          end;

          TNotaFiscalServ.GravarRetornoSefaz(AIdNotaFiscal,
           TpAmbToStr(nfe.FACBrNFe.WebServices.StatusServico.tpAmb),
           nfe.FACBrNFe.WebServices.StatusServico.verAplic,
           statusNFE,
           nfe.FACBrNFe.WebServices.StatusServico.xMotivo,
           IntToStr(nfe.FACBrNFe.WebServices.StatusServico.cUF),
           nfe.FACBrNFe.WebServices.StatusServico.dhRecbto,
           nfe.FACBrNFe.WebServices.StatusServico.TMed,
           nfe.FACBrNFe.WebServices.StatusServico.dhRetorno,
           nfe.FACBrNFe.WebServices.StatusServico.xObs);
        end;
      finally
        FreeAndNil(pessoa);
        FreeAndNil(filial);
        FreeAndNil(pessoaEndereco);
        FreeAndNil(usuario);
        FreeAndNil(configuracoesFiscais);
      end;
    finally
      FreeAndNil(qNotaFiscal);
      FreeAndNil(qNotaFiscalItem);
      FreeAndNil(qNotaFiscalParcela);
    end;
    retornoProxy.FRetorno := true;
  finally
    retornoProxy.FMensagensCommaText := nfe.FInconsistencias.CommaText;
    result :=  TJSON.ObjectToJsonString(retornoProxy);
    FreeAndNil(nfe);
  end;
end;

class function TNotaFiscalEletronicaServ.ImprimirDocumentoFiscal(const AIdNotaFiscal: Integer;
  const ATipoDocumentoFiscal: String): String;
var
  xml: String;
  qNotaFiscal: IFastQuery;
  retornoProxy: TRetornoDocumentoFiscalProxy;
begin
  retornoProxy := TRetornoDocumentoFiscalProxy.Create;
  try
    qNotaFiscal := TFastQuery.ModoDeConsulta('select documento_fiscal_xml, id_pessoa_usuario, id_filial'+
    ' from nota_fiscal where id = :id', TArray<Variant>.Create(AIdNotaFiscal));

    if qNotaFiscal.GetAsString('documento_fiscal_xml').IsEmpty then
    begin
      retornoProxy.FRetorno := false;
      retornoProxy.FMensagensCommaText := 'N�o foi encontrado o xml da nota informada,'+
        ' a impress�o n�o pode ser conclu�da.';
      exit;
    end;

    if ATipoDocumentoFiscal = 'NFE' then
    begin
      result := TNotaFiscalEletronicaServ.ImprimirNFE(qNotaFiscal.GetAsString('documento_fiscal_xml'),
        qNotaFiscal.GetAsInteger('id_filial'), qNotaFiscal.GetAsInteger('id_pessoa_usuario'));
      exit;
    end;

    if ATipoDocumentoFiscal = 'NFCE' then
    begin
      result := TNotaFiscalEletronicaServ.ImprimirNFCE(qNotaFiscal.GetAsString('documento_fiscal_xml'),
        qNotaFiscal.GetAsInteger('id_filial'), qNotaFiscal.GetAsInteger('id_pessoa_usuario'));
    end;
    retornoProxy.FRetorno := true;
  finally
    FreeAndNil(retornoProxy);
  end;
end;

class function TNotaFiscalEletronicaServ.ImprimirNFCE(const AXML: String;
  const AIdFilial, AIdUsuario: Integer): String;
var
  nfe: TNotaFiscalEletronicaServ;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
begin
  nfe := TNotaFiscalEletronicaServ.Create;
  try
    nfe.FTipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE;

    configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(AIdFilial);
    filial := TFilialServ.GetFilial(AIdFilial);
    usuario := TUsuarioServ.GetPessoaUsuario(AIdUsuario);

    nfe.InstanciarComponentesACBr(filial, usuario, configuracoesFiscais);
    nfe.FACbrNFE.NotasFiscais.Clear;
    nfe.FACbrNFE.NotasFiscais.LoadFromString(AXML);
    nfe.FACbrNFE.NotasFiscais.Imprimir;

    if nfe.FACbrNFE.DANFE.PathPDF <> '' then
      nfe.FACbrNFE.NotasFiscais.ImprimirPDF;

    //result := TACBrNFeDANFEFR(nfe.FACbrNFE.DANFE).PreparedReport;

    //result := nfe.BuscarRelatorioStream;
  finally
    FreeAndNil(nfe);
  end;
end;

class function TNotaFiscalEletronicaServ.ImprimirNFE(const AXML: String; const AIdFilial,
  AIdUsuario: Integer): String;
var
  nfe: TNotaFiscalEletronicaServ;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
begin
  nfe := TNotaFiscalEletronicaServ.Create;
  try
    nfe.FTipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE;

    configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(AIdFilial);
    filial := TFilialServ.GetFilial(AIdFilial);
    usuario := TUsuarioServ.GetPessoaUsuario(AIdUsuario);

    nfe.InstanciarComponentesACBr(filial, usuario, configuracoesFiscais);
    nfe.FACbrNFE.NotasFiscais.Clear;
    nfe.FACbrNFE.NotasFiscais.LoadFromString(AXML);
    nfe.FACbrNFE.NotasFiscais.Imprimir;

    if nfe.FACbrNFE.DANFE.PathPDF <> '' then
      nfe.FACbrNFE.NotasFiscais.ImprimirPDF;
  finally
    FreeAndNil(nfe);
  end;
end;

procedure TNotaFiscalEletronicaServ.InstanciarComponentesACBr(const AFilial: TFilialProxy;
  AUsuario: TUsuarioProxy; AConfiguracoesFiscais: TConfiguracaoFiscalProxy);
begin
  Application.CreateForm(TFrmComponentesFiscais,FComponentesFiscais);
  FACbrNFE := FComponentesFiscais.ACBrNFe;

  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    ValidarConfiguracoesNFE(AConfiguracoesFiscais);
    ConfigurarACBrNFE(AConfiguracoesFiscais, AFilial, AUsuario);
  end
  else if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    ValidarConfiguracoesNFCE(AConfiguracoesFiscais);
    ConfigurarACBrNFCE(AConfiguracoesFiscais, AFilial, AUsuario);
  end;
end;

class procedure TNotaFiscalEletronicaServ.InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);
var
  Modelo, Serie, Ano, NumeroInicial, NumeroFinal: Integer;
  Justificativa, CNPJEmissor, tipoDocumentoFiscal : String;
  qNotaFiscal: IFastQuery;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
begin
  qNotaFiscal := TFastQuery.ModoDeAlteracao('select * from nota_fiscal where id in ('+AIdsNotaFiscal+')');

  while not qNotaFiscal.Eof do
  begin
    CNPJEmissor := TFilialServ.GetCNPJ(qNotaFiscal.GetAsInteger('id_filial'));
    Ano := YearOf(qNotaFiscal.GetAsDateTime('B_DEMI'));
    Serie := qNotaFiscal.GetAsInteger('B_SERIE');
    modelo := StrtoInt(TModeloNotaServ.GetModeloNotaFiscal(qNotaFiscal.GetAsInteger('ID_MODELO_NOTA')));
    NumeroInicial := qNotaFiscal.GetAsInteger('B_NNF');
    NumeroFinal := NumeroInicial;
    Justificativa := 'Inutiliza��o por n�o efetiva��o da nota fiscal - '+
    'Essa nota ser� cancelada e inutilizada perante a SEFAZ.';

    if InttoStr(Modelo) = TNotaFiscalProxy.MODELO_NFE then
    begin
      tipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE;
    end
    else
    begin
      tipoDocumentoFiscal := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE;
    end;

    configuracoesFiscais := TConfiguracaoFiscalServ.GetConfiguracaoFiscal(qNotaFiscal.GetAsInteger('id_filial'));
    filial := TFilialServ.GetFilial(qNotaFiscal.GetAsInteger('id_filial'));
    usuario := TUsuarioServ.GetPessoaUsuario(qNotaFiscal.GetAsInteger('id_pessoa_usuario'));

    TFrmComponentesFiscais.InutilizarDocumentoFiscal(CNPJEmissor, Justificativa, Ano, Modelo,
      Serie, NumeroInicial, NumeroFinal, tipoDocumentoFiscal, configuracoesFiscais, filial, usuario);

    TNotaFiscalServ.InutilizarNotaFiscal(qNotaFiscal.GetAsInteger('ID'));

    qNotaFiscal.Next;
  end;
end;

class function TNotaFiscalEletronicaServ.NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
var
  qNotaFiscal: IFastQuery;
begin
  qNotaFiscal := TFastQuery.ModoDeConsulta('SELECT AR_CSTAT, DOCUMENTO_FISCAL_XML FROM NOTA_FISCAL WHERE id = :id',
    TArray<Variant>.Create(AIdNotaFiscal));

  result := qNotaFiscal.GetAsString('AR_CSTAT').Equals(TNotaFiscalProxy.STATUS_NFE_ENVIADA) and
    (not qNotaFiscal.GetAsString('DOCUMENTO_FISCAL_XML').IsEmpty);
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoIdentificacaoNFe(ANFe: TNFe;
  ANotaFiscal: TFastQuery; AFilial: TFilialProxy; APessoaEndereco: TPessoaEnderecoProxy);
var
  dataHoraSaida: TDateTime;
begin
  ANFe.ide.cUF := AFilial.FCodigoIGBEEstado; // B02
  ANFe.ide.cNF := ANotaFiscal.FieldByName('B_NNF').AsInteger; // B03

  ANFe.ide.natOp := TOperacaoServ.BuscarNaturezaOperacao(ANotaFiscal.FieldByName('ID_OPERACAO').AsInteger);

  //#426 Definir com thiago como ficar� a tela para esse caso
  {case qVenda.FieldByName('forma_pagamento').AsInteger of
    0: Ide.indPag := ipVista; // B05   // TpcnIndicadorPagamento = (ipVista, ipPrazo, ipOutras);
    1: Ide.indPag := ipPrazo; // B05
    2: Ide.indPag := ipOutras; // B05
  end;}

  ANFe.Ide.modelo := StrtoIntDef(TModeloNotaServ.GetModeloNotaFiscal(
    ANotaFiscal.FieldByName('ID_MODELO_NOTA').AsInteger), 0); // B06

  dataHoraSaida := Now;

  ANFe.Ide.serie := ANotaFiscal.FieldByName('B_SERIE').AsInteger; // B07
  ANFe.Ide.nNF := ANotaFiscal.FieldByName('B_NNF').AsInteger; // B08
  ANFe.Ide.dEmi := dataHoraSaida;//ANotaFiscal.FieldByName('B_DEMI').AsDateTime; // B09
  ANFe.Ide.dSaiEnt := dataHoraSaida; // B10
  ANFe.Ide.hSaiEnt := dataHoraSaida; // B10a

  if ANotaFiscal.FieldByName('TIPO_NF').AsString.Equals(TNotaFiscalProxy.TIPO_NF_ENTRADA) then
  begin
    ANFe.Ide.tpNF := tnEntrada; // B11
  end
  else
  begin
    ANFe.Ide.tpNF := tnSaida; // B11
  end;

  ANFe.Ide.cMunFG := AFilial.FCodigoIGBECidade; // B12
  ANFe.Ide.tpAmb := FACBrNFE.Configuracoes.WebServices.Ambiente;

  //#432 Desenvolver l�gica de Finalidade da NFE
  ANFe.Ide.finNFe := fnNormal; // B25

  //#434 Desenvolver Processo de Emissao (AplicativoContribuinte, AvulsaFisco, AvulsaContribuinte, ContribuinteAplicativoFisco)
  ANFe.Ide.procEmi := peAplicativoContribuinte; // B26
  ANFe.Ide.verProc := '3.0.0.0'; // B27

  if FTipoDocumentoFiscal = 'NFCE' then
  begin
    ANFe.Ide.tpImp := tiNFCe;
    ANFe.Ide.indFinal := cfConsumidorFinal;
    ANFe.Ide.indPres := pcPresencial;
    ANFe.Ide.tpEmis := teNormal;
    //ANFe.Ide.tpEmis := teOffline;
    //ANFe.Ide.dhCont := Now;
    //ANFe.Ide.xJust := 'ENVIO DE NFCE EM CONTING�NCIA DEVIDO A FORMA DE TRABALHO.';
  end
  else
  begin
    ANFe.Ide.tpEmis := teNormal;
    ANFe.Ide.tpImp := tiRetrato;
  end;

  if (Afilial.FUF = APessoaEndereco.FUF) or (APessoaEndereco.FUF.IsEmpty) THEN
  begin
    ANFe.IDE.idDest          := doInterna;           // dentro do estado
  end
  else
  begin
    ANFe.IDE.idDest          := doInterestadual   // fora do estado
  end;

  // Informacoes da Contingencia
  (*  StrToTpEmis( Ok, IntToStr( nForma_Emissao + 1 ) );
      ( qVenda.FieldByName( 'forma_emissao' ).AsInteger + 1 )
      1 � Normal � emiss�o normal;
      2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
      3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
      4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
      5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
  *)

  //Implementar o retorno
  {if not AConfiguracaoFiscal.FNfeFormaEmissao.Equals(TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_NORMAL) then
  begin
    ANFe.ide.dhCont := Now; // B28
    ANFe.ide.xJust := 'Servidor da receita federal est� inoperante devido a isso torna-se necess�rio '+
    ' a emiss�o de conting�nica'; // B29
  end;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesAdicionais(ANFe: TNFe;
  ANotaFiscal: TFastQuery);
begin
  //
  // Dados Adicionais
  //
  //infAdic.infCPl := '';
  //10/04/2010 Rodrigo, texto para as ceramicas
{  if (filial_.aliquota_credito_icms > 0) and (cliente_.status_credito_icms) then
    infAdic.infCPl := '"Permite o aproveitamento do cr�dito de ICMS no valor de R$ ' + FormatFloat('###,###,##0.00', ((Total.ICMSTot.vProd + Total.ICMSTot.vFrete + Total.ICMSTot.vOutro + Total.ICMSTot.vSeg - Total.ICMSTot.vDesc + Total.ICMSTot.vST) * (filial_.aliquota_credito_icms / 100))) +
      ', correspondente ao percentual de ' + FormatFloat('##0.00%', filial_.aliquota_credito_icms) + ', nos termos do Artigo 23 da LC n� 123/2006" (Resolu��es CGSN n�s 10/2007 e 53/2008).';

  //#442 Implementar Funrural
  {if nVr_FunRural > 0 then
    infAdic.infCPl := infAdic.infCPl + iif(infAdic.infCPl <> '', sLineBreak, '') + cTextoFunRural +' '+ FormatFloat('##0.0#%', nPercFunRural) + ' R$ ' + FormatFloat('###,###,##0.00', nVr_FunRural);}

{  if nStMva > 0 then
    infAdic.infCPl := infAdic.infCPl + iif(infAdic.infCPl <> '', sLineBreak, '') + 'BASE CALCULO ST R$ ' + FormatFloat('###,###,##0.00', nBaseStMva) + ' ICMS ST = R$ ' + FormatFloat('###,###,##0.00', nStMva);

  infAdic.infCPl := infAdic.infCPl +
    iif(qVenda.FieldByName('dados_adicionais').AsString <> '', sLineBreak + qVenda.FieldByName('dados_adicionais').AsString, '');

  if (qVenda.FieldByName('placa_veiculo_2').AsString <> '') then
    infAdic.infCPl := infAdic.infCPl +
      iif(qVenda.FieldByName('nf_placa_veiculo').AsString <> '', sLineBreak + 'Placa(s): ' + ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_placa_veiculo').AsString, '.', ''), '-', '') + ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_uf_veiculo').AsString, '.', ''), '-', ''), '') +
      iif(qVenda.FieldByName('placa_veiculo_2').AsString <> '', ' ' + ReplaceStr(ReplaceStr(qVenda.FieldByName('placa_veiculo_2').AsString, '.', ''), '-', '') + ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_uf_veiculo').AsString, '.', ''), '-', ''), '') +
      iif(qVenda.FieldByName('placa_veiculo_3').AsString <> '', ' ' + ReplaceStr(ReplaceStr(qVenda.FieldByName('placa_veiculo_3').AsString, '.', ''), '-', '') + ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_uf_veiculo').AsString, '.', ''), '-', ''), '');
  //
  // Dados Adicionais
  //

  InfAdic.infAdFisco := qConfig.FieldByName('inf_ad_fisco').AsString; // Z02
  }

  PreencherNotaFiscalGrupoInformacoesAdicionaisIBPT(ANFe, ANotaFiscal);
  PreencherNotaFiscalGrupoInformacoesAdicionaisObservacaoFisco(ANFe, ANotaFiscal); //Fora de Uso
  PreencherNotaFiscalGrupoInformacoesAdicionaisProcesso(ANFe, ANotaFiscal); //Fora de Uso
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesAdicionaisIBPT(ANFe: TNFe;
  ANotaFiscal: TFastQuery);
var
  valorTributosIncidentesIBPT: Double;
begin
  valorTributosIncidentesIBPT :=  TMathUtils.ValorSobrePercentual(28, ANFe.Total.ICMSTot.vTotTrib); //Ajustar para buscar da tabela IBPT

  if valorTributosIncidentesIBPT > 0 then
  begin
    ANFe.infAdic.infCPl := ANFe.infAdic.infCPl + 'Tributos Totais Incidentes R$ '+
      FormatFloat('###,###,###,##0.00', (valorTributosIncidentesIBPT));
  end;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesAdicionaisObservacaoFisco(ANFe: TNFe;
  ANotaFiscal: TFastQuery);
begin
  {
   Try
      qObsFisco := FactoryQuery(
        'select * from tab_venda_obs_fisco where cod_venda = '+IntToStr( qVenda.FieldByName('cod_venda').AsInteger )+' order by cod_venda_obs_fisco' );
      while Not qObsFisco.Eof do
        Begin
          With InfAdic.obsFisco.Add Do
            Begin
              xCampo := qParcelas.FieldByName('x_campo').AsString;
              xTexto := qParcelas.FieldByName('x_texto').AsString;
            End;
          qObsFisco.Next;
        End;
    Finally
      FreeAndNil( qObsFisco );
    End;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesAdicionaisProcesso(ANFe: TNFe;
  ANotaFiscal: TFastQuery);
begin
 //N�O PRECISA AINDA
{ Try
  qProcRef := FactoryQuery(
  'select * from tab_venda_proc_ref where cod_venda = '+IntToStr( qVenda.FieldByName('cod_venda').AsInteger )+' order by cod_venda_proc_ref' );
  while Not qProcRef.Eof do
  Begin
    //With
    With InfAdic.procRef.Add Do
      Begin
        nProc := qProcRef.FieldByName( 'n_proc' ).AsString; // Z11
        case qProcRef.FieldByName( 'ind_proc' ).AsInteger of // Z12
          0 : indProc := ipSEFAZ;
          1 : indProc := ipJusticaFederal;
          2 : indProc := ipJusticaEstadual;
          3 : indProc := ipSecexRFB;
          9 : indProc := ipOutros;
        end;
      End;

    qProcRef.Next;
  End;
  Finally
  FreeAndNil( qProcRef );
  End;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesComercioExterior(ANFe: TNFe;
  ANotaFiscalItem: TFastQuery);
begin
  //N�O PRECISA AINDA
  {
  exporta.UFembarq := qVenda.FieldByName( 'exporta_uf_embarq' ).AsString;
  exporta.xLocEmbarq := qVenda.FieldByName( 'exporta_x_local_embarq' ).AsString;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesCompra(ANFe: TNFe;
  ANotaFiscalItem: TFastQuery);
begin
  //N�O PRECISA AINDA
  {
  compra.xNEmp := qVenda.FieldByName( 'compra_x_n_emp' ).AsString;
  compra.xPed := qVenda.FieldByName( 'compra_x_ped' ).AsString;
  compra.xCont := qVenda.FieldByName( 'compra_x_cont' ).AsString;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesNFe(ANFe: TNFe; ANotaFiscal: TFastQuery);
begin
  ANFe.infNFe.Versao := 3.1;
  ANFe.infNFe.ID := ANotaFiscal.FieldByName('B_NNF').AsString;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoInformacoesParcelas(ANFe: TNFe; ANotaFiscal,
  ANotaFiscalParcela: TFastQuery);
var rotuloEntradaQuitacao: String;
begin
  if ANotaFiscal.FieldByName('vl_quitacao_dinheiro').AsFloat > 0 then
  begin
    if ANotaFiscalParcela.IsEmpty then
    begin
      rotuloEntradaQuitacao := 'A Vista';
    end
    else
    begin
      rotuloEntradaQuitacao := 'Entrada';
    end;

    //Analisar como vai ficar isso na integra��o da venda para a nota
    if FTipoDocumentoFiscal = 'NFCE' then
    begin
      with ANFe.pag.Add do
      begin
        tPag := fpDinheiro; //dinheiro
        vPag := ANotaFiscal.FieldByName('vl_quitacao_dinheiro').AsFloat;
      end;
    end
    else
    begin
      with ANFe.cobr.dup.Add do
      begin
        nDup := rotuloEntradaQuitacao;
        dVenc := Date;
        vDup := ANotaFiscal.FieldByName('vl_quitacao_dinheiro').AsFloat;
      end;
    end;
  end;

  ANotaFiscalParcela.First;
  while not ANotaFiscalParcela.Eof do
  begin
    with ANFe.cobr.dup.Add do
    begin
      nDup := ANotaFiscalParcela.FieldByName('nr_parcela').AsString;
      dVenc := ANotaFiscalParcela.FieldByName('dt_vencimento').Value;
      vDup := ANotaFiscalParcela.FieldByName('vl_titulo').AsFloat;
    end;

    ANotaFiscalParcela.Next;
  end;

{  Try
    qObsCont := FactoryQuery(
      'select * from tab_venda_obs_cont where cod_venda = '+IntToStr( ANotaFiscal.FieldByName('cod_venda').AsInteger )+' order by cod_venda_obs_cont' );
    while Not qObsCont.Eof do
      Begin
        With InfAdic.obsCont.Add Do
          Begin
            xCampo := ANotaFiscalParcela.FieldByName('x_campo').AsString;
            xTexto := ANotaFiscalParcela.FieldByName('x_texto').AsString;
          End;

        qObsCont.Next;
      End;
  Finally
    FreeAndNil( qObsCont );
  End;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutos(ANFe: TNFe; ANotaFiscalItem: TFastQuery;
  AFilial: TFilialProxy);
var
  produtoNFE: TDetCollectionItem;
begin
  ANotaFiscalItem.First;
  while not ANotaFiscalItem.Eof do
  begin
    produtoNFE := ANFe.Det.Add;

    PreencherNotaFiscalGrupoProdutosInformacaoProduto(produtoNFE, ANotaFiscalItem);
    PreencherNotaFiscalGrupoProdutosDI(produtoNFE, ANotaFiscalItem); //N�o Utilizado
    PreencherNotaFiscalGrupoProdutosMedicamento(produtoNFE, ANotaFiscalItem); //N�o Utilizado
    PreencherNotaFiscalGrupoProdutosCombustivel(produtoNFE, ANotaFiscalItem); //N�o Utilizado

    PreencherNotaFiscalGrupoProdutosICMS(produtoNFE, ANotaFiscalItem, AFilial);
    PreencherNotaFiscalGrupoProdutosIPI(produtoNFE, ANotaFiscalItem);
    PreencherNotaFiscalGrupoProdutosII(produtoNFE, ANotaFiscalItem); //N�o Utilizado Imposto de Importa��o (II)
    PreencherNotaFiscalGrupoProdutosPIS(produtoNFE, ANotaFiscalItem);
    PreencherNotaFiscalGrupoProdutosCOFINS(produtoNFE, ANotaFiscalItem);
    PreencherNotaFiscalGrupoProdutosISSQN(produtoNFE, ANotaFiscalItem); //N�o Utilizado

    ANotaFiscalItem.Next;
  end;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosCOFINS(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
var
  OK: Boolean;
begin
  case StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))) of
    cof01, cof02 :
      Begin
        AProdutoNFe.Imposto.COFINS.CST := StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))); // TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof99);
        AProdutoNFe.Imposto.COFINS.vBC := ANotaFiscalItem.FieldByName('S_vBC').AsFloat;
        AProdutoNFe.Imposto.COFINS.pCOFINS := ANotaFiscalItem.FieldByName('S_pCOFINS').AsFloat;
        AProdutoNFe.Imposto.COFINS.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
      End;
    cof03 :
      Begin
        AProdutoNFe.Imposto.COFINS.CST := StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))); // TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof99);
        AProdutoNFe.Imposto.COFINS.qBCProd := ANotaFiscalItem.FieldByName('S_QBCProd').AsFloat;
        AProdutoNFe.Imposto.COFINS.vAliqProd := ANotaFiscalItem.FieldByName('S_vAliqProd').AsFloat;
        AProdutoNFe.Imposto.COFINS.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
      End;
    cof04, cof06, cof07, cof08, cof09:
      Begin
        AProdutoNFe.Imposto.COFINS.CST := StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))); // TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof99);
      End;
    cof49, cof50, cof51, cof52, cof53, cof54, cof55, cof56, cof60, cof61, cof62, cof63, cof64, cof65, cof66, cof67, cof70, cof71, cof72, cof73, cof74, cof75, cof98, cof99 :
      Begin
        if ( AProdutoNFe.Imposto.COFINS.qBCProd + AProdutoNFe.Imposto.COFINS.vAliqProd > 0 ) then
          begin
            AProdutoNFe.Imposto.COFINS.CST := StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))); // TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof99);
            AProdutoNFe.Imposto.COFINS.qBCProd := ANotaFiscalItem.FieldByName('S_SBCProd').AsFloat;
            AProdutoNFe.Imposto.COFINS.vAliqProd := ANotaFiscalItem.FieldByName('S_vAliqProd').AsFloat;
            AProdutoNFe.Imposto.COFINS.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
          end
        else
          begin
            AProdutoNFe.Imposto.COFINS.CST := StrToCSTCOFINS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('S_CST').AsString, 0))); // TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof99);
            AProdutoNFe.Imposto.COFINS.vBC := ANotaFiscalItem.FieldByName('S_vBC').AsFloat;
            AProdutoNFe.Imposto.COFINS.pCOFINS := ANotaFiscalItem.FieldByName('S_pCOFINS').AsFloat;
            AProdutoNFe.Imposto.COFINS.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
          end;
      End;
  end;

  if ( AProdutoNFe.Imposto.COFINSST.qBCProd + AProdutoNFe.Imposto.COFINSST.vAliqProd > 0 ) then
    begin
      AProdutoNFe.Imposto.COFINSST.qBCProd := ANotaFiscalItem.FieldByName('S_qBCProd').AsFloat;
      AProdutoNFe.Imposto.COFINSST.vAliqProd := ANotaFiscalItem.FieldByName('S_vAliqProd').AsFloat;
      AProdutoNFe.Imposto.COFINSST.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
    end
  else
    begin
      AProdutoNFe.Imposto.COFINSST.vBC := ANotaFiscalItem.FieldByName('S_vBC').AsFloat;
      AProdutoNFe.Imposto.COFINSST.pCOFINS := ANotaFiscalItem.FieldByName('S_pCOFINS').AsFloat;
      AProdutoNFe.Imposto.COFINSST.vCOFINS := ANotaFiscalItem.FieldByName('S_vCOFINS').AsFloat;
    end;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosCombustivel(
  AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
begin
{
 If ANotaFiscalItem.FieldByName( 'comb_c_prod_anp' ).AsInteger > 0 Then
Begin
  prod.comb.cProdANP := ANotaFiscalItem.FieldByName( 'comb_c_prod_anp' ).AsInteger; // L102
  prod.comb.CODIF := ANotaFiscalItem.FieldByName( 'comb_codif' ).AsString; // L103
  prod.comb.qTemp := ANotaFiscalItem.FieldByName( 'comb_q_temp' ).AsFloat; // L104
  qEstado.Locate( 'cod_estado', ANotaFiscalItem.FieldByName( 'comb_us_cons' ).AsInteger, [] );
  prod.comb.UFcons := qEstado.FieldByName( 'codigo_ibge' ).AsString; // L120
  prod.comb.CIDE.qBCProd := ANotaFiscalItem.FieldByName( 'comb_cide_q_bc_prod' ).AsFloat; // L106
  prod.comb.CIDE.vAliqProd := ANotaFiscalItem.FieldByName( 'comb_cide_v_aliq_prod' ).AsFloat; // L107
  prod.comb.CIDE.vCIDE := ANotaFiscalItem.FieldByName( 'comb_cide_v_cide' ).AsFloat; // L108
End; }
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosDI(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
begin
{
 Try
        qItem_DI := FactoryQuery(
          ' select * from tab_venda_item_di'+
          ' where cod_venda_item = '+IntToStr( ANotaFiscalItem.FieldByName( 'cod_venda_item' ).AsInteger )+
          ' order by ndi' );
        while Not qItem_DI.Eof do
          Begin
            With Prod.DI.Add Do
              Begin

                nDi := qItem_DI.FieldByName( 'nDi' ).AsString; // I19
                dDi := qItem_DI.FieldByName( 'dDi' ).AsDateTime; // I20
                xLocDesemb := qItem_DI.FieldByName( 'xLocDesemb' ).AsString; // I21
                UFDesemb := qItem_DI.FieldByName( 'ufdesemb' ).AsString; // I22
                dDesemb := qItem_DI.FieldByName( 'dDesemb' ).AsDateTime; // I23
                cExportador := qItem_DI.FieldByName( 'cExportador' ).AsString; // I24

                Try
                  qItem_DI_Adicoes := FactoryQuery(
                    ' select * from tab_venda_item_di_adicoes'+
                    ' where cod_venda_item_di = '+IntToStr( qItem_DI.FieldByName( 'cod_venda_item_di' ).AsInteger )+
                    ' order by nAdicao' );
                  while Not qItem_DI_Adicoes.Eof do
                    Begin
                      With adi.Add Do
                        Begin
                          nAdicao := qItem_DI_Adicoes.FieldByName( 'nAdicao' ).AsInteger; // I26
                          nSeqAdi := qItem_DI_Adicoes.FieldByName( 'nSeqAdic' ).AsInteger; // I27
                          cFabricante := qItem_DI_Adicoes.FieldByName( 'cFabricante' ).AsString; // I28
                          vDescDI := qItem_DI_Adicoes.FieldByName( 'vDescDi' ).AsFloat; // I29
                        End;
                      qItem_DI_Adicoes.Next;
                    End;
                Finally
                  FreeAndNil( qItem_DI_Adicoes );
                End;
              end;
            qItem_DI.Next;
          End;
      Finally
        FreeAndNil( qItem_DI );
      End;
}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosICMS(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery; AFilial: TFilialProxy);
var
  OK: Boolean;
begin
   Case AFilial.FCrt Of
    2..3 :
      Begin
        case StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2)) of
          cst00 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_vBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
            end;
          cst10 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_vBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString);
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_vBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            end;
          cst20 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_vBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
            end;
          cst30 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_vBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            end;
          cst40, cst41, cst50 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, Copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.motDesICMS := StrTomotDesICMS(ok, ANotaFiscalItem.FieldByName('N_motDesICMS').AsString );
            end;
          cst51 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_VBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
            end;
          cst60 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.vBCSTRet := ANotaFiscalItem.FieldByName('N_vBCSTRet').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSSTRet := ANotaFiscalItem.FieldByName('N_vICMSSTRet').AsFloat;
            end;
          cst70 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_VBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            end;
          cst90 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_VBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            end;
          cstPart10, cstPart90 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CST := StrToCSTICMS(OK, copy(ANotaFiscalItem.FieldByName('N_CST').AsString, 2, 2));
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_VBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            end;
        end;
      End;
    1 :
      Begin
        Case StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString) Of
          csosn101 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
              AProdutoNFe.Imposto.ICMS.pCredSN := ANotaFiscalItem.FieldByName('N_pCredSN').AsFloat;
              AProdutoNFe.Imposto.ICMS.vCredICMSSN := ANotaFiscalItem.FieldByName('N_vCredICMSSN').AsFloat;
            End;
          csosn102,
          csosn103,
          csosn300,
          csosn400 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
            End;
          csosn201 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pCredSN := ANotaFiscalItem.FieldByName('N_pCredSN').AsFloat;
              AProdutoNFe.Imposto.ICMS.vCredICMSSN := ANotaFiscalItem.FieldByName('N_vCredICMSSN').AsFloat;
            End;
          csosn202, csosn203 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
            End;
          csosn500 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
              AProdutoNFe.Imposto.ICMS.vBCSTRet := ANotaFiscalItem.FieldByName('N_vBCSTRet').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSSTRet := ANotaFiscalItem.FieldByName('N_vICMSSTRet').AsFloat;
            End;
          csosn900 :
            Begin
              AProdutoNFe.Imposto.ICMS.orig := StrToOrig(OK, ANotaFiscalItem.FieldByName('N_ORIG').AsString);
              AProdutoNFe.Imposto.ICMS.CSOSN := StrToCSOSNIcms(OK, ANotaFiscalItem.FieldByName('N_CST').AsString); // TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
              AProdutoNFe.Imposto.ICMS.modBC := StrTomodBC(ok, ANotaFiscalItem.FieldByName('N_MODBC').AsString );
              AProdutoNFe.Imposto.ICMS.vBC := ANotaFiscalItem.FieldByName('N_VBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBC := ANotaFiscalItem.FieldByName('N_pRedBC').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMS := ANotaFiscalItem.FieldByName('N_pICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMS := ANotaFiscalItem.FieldByName('N_vICMS').AsFloat;
              AProdutoNFe.Imposto.ICMS.modBCST := StrTomodBCST(ok, ANotaFiscalItem.FieldByName('N_MODBCST').AsString );
              AProdutoNFe.Imposto.ICMS.pMVAST := ANotaFiscalItem.FieldByName('N_pMVAST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pRedBCST := ANotaFiscalItem.FieldByName('N_pRedBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vBCST := ANotaFiscalItem.FieldByName('N_VBCST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pICMSST := ANotaFiscalItem.FieldByName('N_pICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.vICMSST := ANotaFiscalItem.FieldByName('N_vICMSST').AsFloat;
              AProdutoNFe.Imposto.ICMS.pCredSN := ANotaFiscalItem.FieldByName('N_pCredSN').AsFloat;
              AProdutoNFe.Imposto.ICMS.vCredICMSSN := ANotaFiscalItem.FieldByName('N_vCredICMSSN').AsFloat;
            End;
        End;
      End
   End;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosII(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
begin
{#462 NFE: [MANUAL] Substituir o nome dos campos no mapeamento dos impostos}
// Imposto de Importacao
{
     AProdutoNFE.Imposto.II.vBc := ANotaFiscalItem.FieldByName('nf_ii_v_bc').AsFloat;
     AProdutoNFE.Imposto.II.vDespAdu := ANotaFiscalItem.FieldByName('nf_ii_v_desp_adu').AsFloat;
     AProdutoNFE.Imposto.II.vII := ANotaFiscalItem.FieldByName('nf_ii_v_ii').AsFloat;
     AProdutoNFE.Imposto.II.vIOF := ANotaFiscalItem.FieldByName('nf_ii_v_iof').AsFloat;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosInformacaoProduto(
  AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
begin
  AProdutoNFe.Prod.nItem := TgbFDQuery(ANotaFiscalItem).Recno; // H02
  AProdutoNFe.Prod.cProd := ANotaFiscalItem.FieldByName('ID_PRODUTO').AsString; // I02;
  AProdutoNFe.Prod.cEAN := ANotaFiscalItem.FieldByName('I_CEAN').AsString; // I03
  AProdutoNFe.Prod.cEANTrib := ANotaFiscalItem.FieldByName('I_CEANTRIB').AsString; // I12

  if (FTipoDocumentoFiscal.Equals('NFCE')) and (FACBrNFE.Configuracoes.WebServices.Ambiente = taHomologacao) then
  begin
    AProdutoNFe.Prod.xProd := 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
  end
  else
  begin
    AProdutoNFe.Prod.xProd := ANotaFiscalItem.FieldByName('I_XPROD').AsString; // I04
  end;

  AProdutoNFe.Prod.NCM := ANotaFiscalItem.FieldByName('I_NCM').AsString; // I05
  AProdutoNFe.Prod.CEST := TCestServ.BuscarCodigoCEST(ANotaFiscalItem.FieldByName('I_NCM').AsString); //CEST
  AProdutoNFe.Prod.CFOP := ANotaFiscalItem.FieldByName('I_CFOP').AsString; // I08
  AProdutoNFe.Prod.uCom := ANotaFiscalItem.FieldByName('I_UCOM').AsString; // I09 // Unidade Comercial - UF
  AProdutoNFe.Prod.qCom := ANotaFiscalItem.FieldByName('I_QCOM').AsFloat; // I10
  AProdutoNFe.Prod.vUnCom := ANotaFiscalItem.FieldByName('I_VUNCOM').AsFloat; // I10a
  AProdutoNFe.Prod.vProd := AProdutoNFe.Prod.qCom * AProdutoNFe.Prod.vUnCom;//ANotaFiscalItem.FieldByName('I_VPROD').AsFloat; // I11
  AProdutoNFe.Prod.uTrib := ANotaFiscalItem.FieldByName('I_UTRIB').AsString; // I13
  AProdutoNFe.Prod.qTrib := ANotaFiscalItem.FieldByName('I_QTRIB').AsFloat; // I14
  AProdutoNFe.Prod.vUnTrib := ANotaFiscalItem.FieldByName('I_VUNTRIB').AsFloat; // I14a
  AProdutoNFe.Prod.vFrete := ANotaFiscalItem.FieldByName('I_VFRETE').AsFloat; // I15
  AProdutoNFe.Prod.vSeg := ANotaFiscalItem.FieldByName('I_VSEG').AsFloat; // I16
  AProdutoNFe.Prod.vDesc := ANotaFiscalItem.FieldByName('I_VDESC').AsFloat; // I17
  AProdutoNFe.Prod.vOutro := ANotaFiscalItem.FieldByName('I_VOUTRO').AsFloat; // I17a
  AProdutoNFe.Prod.IndTot := TpcnIndicadorTotal(ANotaFiscalItem.FieldByName('I_INDTOT').AsInteger); // I17b
  AProdutoNFe.Prod.xPed := ANotaFiscalItem.FieldByName( 'I_XPED' ).AsString;
  AProdutoNFe.Prod.nItemPed := ANotaFiscalItem.FieldByName( 'I_NITEMPED' ).AsString;

  AProdutoNFe.infAdProd := '';
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosIPI(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
var
  OK: Boolean;
begin
  AProdutoNFE.Imposto.IPI.clEnq := ''; //ANotaFiscalItem.FieldByName('O_clEnq').AsString
  AProdutoNFE.Imposto.IPI.CNPJProd := ANotaFiscalItem.FieldByName('O_CNPJProd').AsString;
  AProdutoNFE.Imposto.IPI.cSelo := ANotaFiscalItem.FieldByName('O_cSelo').AsString;
  AProdutoNFE.Imposto.IPI.qSelo := ANotaFiscalItem.FieldByName('O_qSelo').AsInteger;
  AProdutoNFE.Imposto.IPI.cEnq := ANotaFiscalItem.FieldByName('O_clEnq').AsString;//ANotaFiscalItem.FieldByName('O_cEnq').AsString;

  Case StrToCSTIPI(OK, ANotaFiscalItem.FieldByName('O_CST').AsString) Of
    ipi00, ipi49, ipi50, ipi99 :
      Begin
        if (AProdutoNFE.Imposto.IPI.qUnid +AProdutoNFE.Imposto.IPI.vUnid > 0 ) Then
          Begin
           AProdutoNFE.Imposto.IPI.CST := StrToCSTIPI(OK, ANotaFiscalItem.FieldByName('O_CST').AsString); // TpcnCstIpi = (ipi00, ipi49, ipi50, ipi99, ipi01, ipi02, ipi03, ipi04, ipi05, ipi51, ipi52, ipi53, ipi54, ipi55);
           AProdutoNFE.Imposto.IPI.qUnid := ANotaFiscalItem.FieldByName('O_qUnid').AsFloat;
           AProdutoNFE.Imposto.IPI.vUnid := ANotaFiscalItem.FieldByName('O_vUnid').AsFloat;
           AProdutoNFE.Imposto.IPI.vIPI := ANotaFiscalItem.FieldByName('O_vIPI').AsFloat;
          End
        else
          Begin
           AProdutoNFE.Imposto.IPI.CST := StrToCSTIPI(OK, ANotaFiscalItem.FieldByName('O_CST').AsString); // TpcnCstIpi = (ipi00, ipi49, ipi50, ipi99, ipi01, ipi02, ipi03, ipi04, ipi05, ipi51, ipi52, ipi53, ipi54, ipi55);
           AProdutoNFE.Imposto.IPI.vBC := ANotaFiscalItem.FieldByName('O_vBC').AsFloat;
           AProdutoNFE.Imposto.IPI.pIPI := ANotaFiscalItem.FieldByName('O_pIPI').AsFloat;
           AProdutoNFE.Imposto.IPI.vIPI := ANotaFiscalItem.FieldByName('O_vIPI').AsFloat;
          End;
      End;
    ipi01, ipi02, ipi03, ipi04, ipi51, ipi52, ipi53, ipi54, ipi55 :
      Begin
       AProdutoNFE.Imposto.IPI.CST := StrToCSTIPI(OK, ANotaFiscalItem.FieldByName('O_CST').AsString); // TpcnCstIpi = (ipi00, ipi49, ipi50, ipi99, ipi01, ipi02, ipi03, ipi04, ipi05, ipi51, ipi52, ipi53, ipi54, ipi55);
      end;
  End;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosISSQN(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
begin
{#462 NFE: [MANUAL] Substituir o nome dos campos no mapeamento dos impostos}
  // ISSQN
{ AProdutoNFE.Imposto.ISSQN.vBC := ANotaFiscalItem.FieldByName('issqn_v_bc').AsFloat;
 AProdutoNFE.Imposto.ISSQN.vAliq := ANotaFiscalItem.FieldByName('issqn_v_aliq').AsFloat;
 AProdutoNFE.Imposto.ISSQN.vISSQN := ANotaFiscalItem.FieldByName('issqn_v_issqn').AsFloat;

  qCidade.Locate( 'cod_cidade', ANotaFiscalItem.FieldByName('issqn_cod_cidade').AsInteger, []);
 AProdutoNFE.Imposto.ISSQN.cMunFG := qCidade.FieldByName('codigo_ibge').AsInteger;

 AProdutoNFE.Imposto.ISSQN.cListServ := ANotaFiscalItem.FieldByName('issqn_c_list_serv').AsInteger;
 AProdutoNFE.Imposto.ISSQN.cSitTrib := ISSQNcSitTribVazio;
  case ANotaFiscalItem.FieldByName('issqn_c_sit_trib').AsInteger of // TpcnISSQNcSitTrib  = ( ISSQNcSitTribVazio , ISSQNcSitTribNORMAL, ISSQNcSitTribRETIDA, ISSQNcSitTribSUBSTITUTA,ISSQNcSitTribISENTA);
    1 :AProdutoNFE.Imposto.ISSQN.cSitTrib := ISSQNcSitTribNORMAL;
    2 :AProdutoNFE.Imposto.ISSQN.cSitTrib := ISSQNcSitTribRETIDA;
    3 :AProdutoNFE.Imposto.ISSQN.cSitTrib := ISSQNcSitTribSUBSTITUTA;
    4 :AProdutoNFE.Imposto.ISSQN.cSitTrib := ISSQNcSitTribISENTA;
  end;
            }
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosMedicamento(
  AProdutoNFe: TDetCollectionItem; ANotaFiscalItem: TFastQuery);
begin
{try
        qItem_Med := FactoryQuery(
          ' select * from tab_venda_item_med'+
          ' where cod_venda_item = '+IntToStr( ANotaFiscalItem.FieldByName( 'cod_venda_item' ).AsInteger )+
          ' order by med_n_lote' );
        while Not qItem_Med.Eof do
          Begin
            With prod.med.Add Do
              Begin
                nLote := qItem_Med.FieldByName( 'med_n_lote' ).AsString; // K02
                qLote := qItem_Med.FieldByName( 'med_q_lote' ).AsInteger; // K03
                dFab := qItem_Med.FieldByName( 'med_d_fab' ).AsDateTime; // K04
                dVal := qItem_Med.FieldByName( 'med_d_val' ).AsDateTime; // K05
                vPMC := qItem_Med.FieldByName( 'med_v_pmc' ).AsFloat; // K06
              End;

            qItem_Med.Next;
          End;
      finally
        FreeAndNil( qItem_Med );
      end;}
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoProdutosPIS(AProdutoNFe: TDetCollectionItem;
  ANotaFiscalItem: TFastQuery);
var
  OK: Boolean;
begin
  AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, '99');
  Case StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))) Of
    pis01, pis02 :
      Begin
        AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))); // TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis99);
        AProdutoNFE.Imposto.PIS.vBC := ANotaFiscalItem.FieldByName('Q_VBC').AsFloat;
        AProdutoNFE.Imposto.PIS.pPIS := ANotaFiscalItem.FieldByName('Q_PPIS').AsFloat;
        AProdutoNFE.Imposto.PIS.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
      End;
    pis03 :
      Begin
        AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))); // TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis99);
        AProdutoNFE.Imposto.PIS.qBCProd := ANotaFiscalItem.FieldByName('Q_QBCPROD').AsFloat;
        AProdutoNFE.Imposto.PIS.vAliqProd := ANotaFiscalItem.FieldByName('Q_vAliqProd').AsFloat;
        AProdutoNFE.Imposto.PIS.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
      End;
    pis04, pis06, pis07, pis08, pis09 :
      Begin
       AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))); // TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis99);
      End;
    pis49, pis50, pis51, pis52, pis53, pis54, pis55, pis56, pis60, pis61, pis62, pis63, pis64, pis65, pis66, pis67, pis70, pis71, pis72, pis73, pis74, pis75, pis98, pis99 :
      Begin
        if (AProdutoNFE.Imposto.PIS.qBCProd +AProdutoNFE.Imposto.PIS.vAliqProd > 0 ) then
          begin
           AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))); // TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis99);
           AProdutoNFE.Imposto.PIS.qBCProd := ANotaFiscalItem.FieldByName('Q_QBCPROD').AsFloat;
           AProdutoNFE.Imposto.PIS.vAliqProd := ANotaFiscalItem.FieldByName('Q_vAliqProd').AsFloat;
           AProdutoNFE.Imposto.PIS.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
          end
        else
          begin
           AProdutoNFE.Imposto.PIS.CST := StrToCSTPIS(OK, FormatFloat('00', StrtoIntDef(ANotaFiscalItem.FieldByName('Q_CST').AsString, 0))); // TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis99);
           AProdutoNFE.Imposto.PIS.vBC := ANotaFiscalItem.FieldByName('Q_VBC').AsFloat;
           AProdutoNFE.Imposto.PIS.pPIS := ANotaFiscalItem.FieldByName('Q_PPIS').AsFloat;
           AProdutoNFE.Imposto.PIS.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
          end;
      End;
  End;

  if (AProdutoNFE.Imposto.PISST.qBCProd +AProdutoNFE.Imposto.PISST.vAliqProd > 0 ) then
    begin
     AProdutoNFE.Imposto.PISST.qBCProd := ANotaFiscalItem.FieldByName('Q_QBCPROD').AsFloat;
     AProdutoNFE.Imposto.PISST.vAliqProd := ANotaFiscalItem.FieldByName('Q_VALIQPROD').AsFloat;
     AProdutoNFE.Imposto.PISST.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
    end
  else
  begin
   AProdutoNFE.Imposto.PISST.vBc := ANotaFiscalItem.FieldByName('Q_VBC').AsFloat;
   AProdutoNFE.Imposto.PISST.pPis := ANotaFiscalItem.FieldByName('Q_PPIS').AsFloat;
   AProdutoNFE.Imposto.PISST.vPIS := ANotaFiscalItem.FieldByName('Q_VPIS').AsFloat;
  end;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoFrete(ANFe: TNFe; ANotaFiscal: TFastQuery);
begin
  if FTipoDocumentoFiscal = 'NFCE' then
  begin
    ANFe.transp.modFrete := mfSemFrete;
  end;

{
  case qVenda.FieldByName('cod_tipo_frete').AsInteger of // X02  //  (mfContaEmitente, mfContaDestinatario, mfContaTerceiros, mfSemFrete)
    0: transp.modFrete := mfContaEmitente; // EMITENTE
    1: transp.modFrete := mfContaDestinatario; // DEST/REM
    2: transp.modFrete := mfContaTerceiros; // TERCEIROS
    9: transp.modFrete := mfSemFrete; // SEM FRETE
  end;

  if (transp_.codigo > 0) or (qVenda.FieldByName('cod_tipo_frete').AsInteger = 9) then
    with Transp do
      begin
        //modFrete := StrTomodFrete( OK, iif( qVenda.FieldByName( 'nr_tipo_frete' ).AsString = 'P', '0', '1' ) );

        transporta.CNPJCPF := transp_.doc_federal_; // X04 e X06
        transporta.xNome := transp_.nome; // X06
        transporta.IE := iif( cliente_.tp_pessoa = 'J', transp_.doc_estadual, '' ); // X07
        transporta.xEnder := transp_.endereco_ + ' - ' + transp_.numero; // X08
        transporta.xMun := transp_.cidade; // X09
        transporta.UF := transp_.sigla; // X10

        If qVenda.FieldByName('transp_ret_transp_v_serv').AsFloat > 0 Then
          Begin
            retTransp.vServ := qVenda.FieldByName('transp_ret_transp_v_serv').AsFloat;
            retTransp.vBCRet := qVenda.FieldByName('transp_ret_transp_v_bc_ret').AsFloat;
            retTransp.pICMSRet := qVenda.FieldByName('transp_ret_transp_p_icms_ret').AsFloat;
            retTransp.vICMSRet := qVenda.FieldByName('transp_ret_transp_v_icms_ret').AsFloat;

            cfop := carregaCFOP_Nf(qVenda.FieldByName('transp_ret_transp_cfop').AsString, cConnection_);
            retTransp.CFOP := ReplaceStr(cfop.codigo, '.', '');

            qCidade.Locate( 'cod_cidade', qVenda.FieldByName('transp_ret_transp_cod_cidade').AsInteger, []);
            retTransp.cMunFG := qCidade.FieldByName('codigo_ibge').AsInteger; // F07
          End;

        veicTransp.placa := ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_placa_veiculo').AsString, '.', ''), '-', ''); // X19
        veicTransp.UF := ReplaceStr(ReplaceStr(qVenda.FieldByName('nf_uf_veiculo').AsString, '.', ''), '-', ''); // X20
        veicTransp.RNTC := ReplaceStr(ReplaceStr(qVenda.FieldByName('transp_veic_transp_rntc').AsString, '.', ''), '-', ''); // X21

        qVeicTransp := FactoryQuery(
          'select * from tab_venda_reboque where cod_venda = '+IntToStr( qVenda.FieldByName('cod_venda').AsInteger )+' order by reboque_placa' );
        while Not qVeicTransp.Eof do
          Begin
            with reboque.Add do
              begin
                placa := ReplaceStr(ReplaceStr(qVeicTransp.FieldByName('reboque_placa').AsString, '.', ''), '-', ''); // X23
                UF := ReplaceStr(ReplaceStr(qVeicTransp.FieldByName('reboque_uf').AsString, '.', ''), '-', ''); // X24
                RNTC := ReplaceStr(ReplaceStr(qVeicTransp.FieldByName('reboque_rntc').AsString, '.', ''), '-', ''); // X25
              end;
            qVeicTransp.Next;
          End;

         vagao := qVenda.FieldByName('transp_balsa').AsString;
         balsa := qVenda.FieldByName('transp_vagao').AsString;
      end; }
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoTotais(ANFe: TNFe; ANotaFiscal,
  ANotaFiscalItem: TFastQuery);
begin
  PreencherNotaFiscalGrupoTotaisICMS(ANFe, ANotaFiscal, ANotaFiscalItem);

  ANFe.total.ISSQNtot.vServ := 0; // W18
  ANFe.total.ISSQNtot.vBC := 0;
  ANFe.total.ISSQNtot.vISS := 0;
  ANFe.total.ISSQNtot.vPIS := 0;
  ANFe.total.ISSQNtot.vCOFINS := 0;

  ANFe.total.retTrib.vRetPIS := 0;
  ANFe.total.retTrib.vRetCOFINS := 0;
  ANFe.total.retTrib.vRetCSLL := 0;
  ANFe.total.retTrib.vBCIRRF := 0;
  ANFe.total.retTrib.vIRRF := 0;
  ANFe.total.retTrib.vBCRetPrev := 0;
  ANFe.total.retTrib.vRetPrev := 0;
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoTotaisICMS(ANFe: TNFe; ANotaFiscal,
  ANotaFiscalItem: TFastQuery);
begin
  ANFe.Total.ICMSTot.vBC := ANotaFiscal.FieldByName('W_VBC').AsFloat; // W03
  ANFe.Total.ICMSTot.vICMS := ANotaFiscal.FieldByName('W_VICMS').AsFloat; // W04
  ANFe.Total.ICMSTot.vBCST := ANotaFiscal.FieldByName('W_VBCST').AsFloat; // W05
  ANFe.Total.ICMSTot.vST := ANotaFiscal.FieldByName('W_VST').AsFloat; // W06
  ANFe.Total.ICMSTot.vProd := ANotaFiscal.FieldByName('W_VPROD').AsFloat; // W07
  ANFe.Total.ICMSTot.vFrete := ANotaFiscal.FieldByName('W_VFRETE').AsFloat; // W08
  ANFe.Total.ICMSTot.vSeg := ANotaFiscal.FieldByName('W_VSEG').AsFloat; // W09
  ANFe.Total.ICMSTot.vDesc := ANotaFiscal.FieldByName('W_VDESC').AsFloat; // W10
  ANFe.Total.ICMSTot.vII := ANotaFiscal.FieldByName('W_VII').AsFloat; // W11
  ANFe.Total.ICMSTot.vIPI := ANotaFiscal.FieldByName('W_VIPI').AsFloat; // W12
  ANFe.Total.ICMSTot.vPIS := ANotaFiscal.FieldByName('W_VPIS').AsFloat; // W13
  ANFe.Total.ICMSTot.vCOFINS := ANotaFiscal.FieldByName('W_VCOFINS').AsFloat; // W14
  ANFe.Total.ICMSTot.vOutro := ANotaFiscal.FieldByName('W_VOUTRO').AsFloat; // W15
  ANFe.Total.ICMSTot.vNF := ANotaFiscal.FieldByName('W_VNF').AsFloat; // W16
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoVolume(ANFe: TNFe; ANotaFiscalItem: TFastQuery);
begin
{  qVenda_Volumes := FactoryQuery( 'select * from tab_venda_volumes where cod_venda = '+IntToStr( qVenda.FieldByName('cod_venda').AsInteger )+' order by cod_venda_volumes' );
  while Not qVenda_Volumes.Eof do
  Begin
    with Transp.vol.Add do
      begin
        qVol := qVenda_Volumes.FieldByName('vol_q_vol').AsInteger; // X27

        if qVenda_Volumes.FieldByName('vol_cod_especie').AsInteger > 0 then
          try
            qAuxiliar := FactoryQuery('select nome_especie from tab_especie where cod_especie = ' + QuotedStr(qVenda_Volumes.FieldByName('vol_cod_especie').AsString), cConnection);
            esp := qAuxiliar.FieldByName('nome_especie').AsString; // X28
          finally
            FreeAndNil(qAuxiliar);
          end;

        if qVenda_Volumes.FieldByName('vol_cod_marca').AsInteger > 0 then
          try
            qAuxiliar := FactoryQuery('select nome_marca from tab_marca where cod_marca = ' + QuotedStr(qVenda_Volumes.FieldByName('vol_cod_marca').AsString), cConnection);
            marca := qAuxiliar.FieldByName('nome_marca').AsString; // X29
          finally
            FreeAndNil(qAuxiliar);
          end;

        nVol := qVenda_Volumes.FieldByName('vol_n_vol').AsString; // X30

        pesoL := qVenda_Volumes.FieldByName('vol_peso_l').AsFloat; // X31
        pesoB := qVenda_Volumes.FieldByName('vol_peso_b').AsFloat; // X32

        qVenda_Volumes_Lacres := FactoryQuery(
          'select * from tab_venda_volumes_lacres where cod_venda_volumes = '+IntToStr( qVenda_Volumes.FieldByName( 'cod_venda_volumes' ).AsInteger )+' order by cod_venda_volumes_lacres' );
        while not qVenda_Volumes_Lacres.Eof do
          Begin
            With Lacres.Add Do
              Begin
                nLacre := qVenda_Volumes_Lacres.FieldByName( 'lacres_n_lacre' ).AsString; // X34
              End;

            qVenda_Volumes_Lacres.Next;
          End;
      end;

    qVenda_Volumes.Next;
  End; }
end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoIdentificacaoDestinatario(ANFe: TNFe;
  APessoa: TPessoaProxy; APessoaEndereco: TPessoaEnderecoProxy);
var
  pessoaContato: TPessoaContatoPrincipalProxy;
begin
// D01 Avulsa - Informacoes do Fisco Emitente, Grupo de Uso Exclusivo do Fiscal
    (*
    Avulsa.CNPJ := ''; // D02
    Avulsa.xOrgao := ''; // D03
    Avulsa.matr := ''; // D04
    Avulsa.xAgente := ''; // D05
    Avulsa.fone := ''; // D06
    Avulsa.UF := ''; // D07
    Avulsa.nDAR := ''; // D08
    Avulsa.dEmi := Now; // D09
    Avulsa.vDAR := 0; // D10
    Avulsa.repEmi := ''; // D11
    Avulsa.dPag := Now; // D12  *)

    // Dest E01 - Identificacao do Destinat�rio da NF-e
    pessoaContato := TPessoaContatoPrincipalProxy.Create;
    try
      pessoaContato := TPessoaContatoServ.GetPessoaContatoPrincipal(APessoa.FId);
      ANFe.Dest.CNPJCPF := APessoa.FDocFederal;//TStringUtils.RPAD(APessoa.FDocFederal, '0', 14); // E02 e E03
      //ANFe.Dest.idEstrangeiro := '0'; // E03a

      if APessoa.FTpPessoa.Equals(TPessoaProxy.TIPO_PESSOA_JURIDICA) then
      begin
        ANFe.Dest.xNome := copy(APessoa.FRazaoSocial, 1, 60); // E04
      end
      else
      begin
        ANFe.Dest.xNome := copy(APessoa.FNome, 1, 60); // E04
      end;

      ANFe.Dest.EnderDest.xLgr := APessoaEndereco.FLogradouro; // E06
      ANFe.Dest.EnderDest.nro := APessoaEndereco.FNumero; // E07
      ANFe.Dest.EnderDest.xCpl := APessoaEndereco.FComplemento; // E08
      ANFe.Dest.EnderDest.xBairro := APessoaEndereco.FBairro; // E09
      ANFe.Dest.EnderDest.cMun := APessoaEndereco.FCodigoIGBECidade; // E10
      ANFe.Dest.EnderDest.xMun := APessoaEndereco.FNomeCidade; // E11
      ANFe.Dest.EnderDest.UF := APessoaEndereco.FUF; // E12
      ANFe.Dest.EnderDest.CEP := StrtoIntDef(APessoaEndereco.FCep, 0); // E13
      ANFe.Dest.EnderDest.cPais := APessoaEndereco.FCodigoIGBEPais; // E14
      ANFe.Dest.EnderDest.xPais := APessoaEndereco.FNomePais; // E15
      ANFe.Dest.EnderDest.Fone := pessoaContato.FTelefone; // E16
      ANFe.Dest.email := pessoaContato.FEmail; // E19

      if FTipoDocumentoFiscal = 'NFCE' then
      begin
        ANFe.Dest.indIEDest := inNaoContribuinte;
      end
      else
      begin
        if (Length(APessoa.FDocFederal) = 14) and (not APessoa.FDocEstadual.IsEmpty) then
        begin
          ANFe.Dest.IE := UpperCase(APessoa.FDocEstadual); // E17
        end
        else
        begin
          ANFe.Dest.indIEDest := inNaoContribuinte;
        end;
      end;
    Finally
      FreeAndNil(pessoaContato);
    end;

    {Criar esse campo na pessoa para preparar para suframa}
    //Dest.ISUF := APessoa.isuf; // E18 -
       (* Obrigat�rio, nas opera��es que se beneficiam de incentivos fiscais existentes nas �reas sob controle da SUFRAMA.
          A omiss�o da Inscri��o SUFRAMA impede o processamento da opera��o pelo Sistema de Mercadoria Nacional da SUFRAMA e a
          libera��o da Declara��o de Ingresso, prejudicando a comprova��o do ingresso/internamento da mercadoria nas �reas sob
          controle da SUFRAMA. (v2.0) *)


{Jonatas Fischer
 retirada e entrega?
De:
Jonatas Fischer
vc somente preenche retirada se o endere�o for diferente do endere�o de remetende (tipo, o cliente vai retirar a mercadoria em outro ligar)
De:
Jonatas Fischer
a entrega somente se o endere�o for diferente do endere�o do destinat�rio, tipo uma empresa comprou e mandou entregar na casa do s�cio
:D

    if qVenda.FieldByName('status_local_retirada').AsString = 'S' then
      begin
        with Retirada do
          begin
            //CNPJCPF := PADR(APessoa.doc_federal_, 14, '0'); // F02 e F02a
            //CNPJCPF := APessoa.doc_federal_; // F02 e F02a // Removiso o PADL  id #3395
            CNPJCPF := qVenda.FieldByName('local_retirada_cnpj_cpf').AsString;
            If CNPJCPF = '' Then CNPJCPF := APessoa.doc_federal_; // Para garantir que as nfes antigas funciones

            xLgr := qVenda.FieldByName('local_retirada_endereco').AsString; // F03
            nro := qVenda.FieldByName('local_retirada_numero').AsString; // F04
            xCpl := qVenda.FieldByName('local_retirada_complemento').AsString; // F05

            qBairro.Locate( 'cod_bairro', qVenda.FieldByName('local_retirada_cod_bairro').AsInteger, []);
            xBairro := qBairro.FieldByName('bairro').AsString; // F06

            qCidade.Locate( 'cod_cidade', qVenda.FieldByName('local_retirada_cod_cidade').AsInteger, []);
            cMun := qCidade.FieldByName('codigo_ibge').AsInteger; // F07
            xMun := qCidade.FieldByName('cidade').AsString; // F08

            UF := qCidade.FieldByName('sigla').AsString; // F09
          end;
      end;

    if qVenda.FieldByName('status_local_entrega').AsString = 'S' then
      begin
        with Entrega do
          begin
           // CNPJCPF := PADR(APessoa.doc_federal_, 14, '0'); // G02 e G02a
            //CNPJCPF := APessoa.doc_federal_; // F02 e F02a // Removiso o PADL  id #3395
            CNPJCPF := qVenda.FieldByName('local_entrega_cnpj_cpf').AsString;
            If CNPJCPF = '' Then CNPJCPF := APessoa.doc_federal_; // Para garantir que as nfes antigas funciones

            xLgr := qVenda.FieldByName('local_entrega_endereco').AsString; // G03
            nro := qVenda.FieldByName('local_entrega_numero').AsString; // G04
            xCpl := qVenda.FieldByName('local_entrega_complemento').AsString; // G05

            qBairro.Locate( 'cod_bairro', qVenda.FieldByName('local_entrega_cod_bairro').AsInteger, []);
            xBairro := qBairro.FieldByName('bairro').AsString; // G06

            qCidade.Locate( 'cod_cidade', qVenda.FieldByName('local_entrega_cod_cidade').AsInteger, []);
            cMun := qCidade.FieldByName('codigo_ibge').AsInteger; // G07
            xMun := qCidade.FieldByName('cidade').AsString; // G08

            UF := qCidade.FieldByName('sigla').AsString; // G09
          end;
      end;}

end;

procedure TNotaFiscalEletronicaServ.PreencherNotaFiscalGrupoIdentificacaoEmitente(ANFe: TNFe;
  AFilial: TFilialProxy);
var
  ok: boolean;
begin
  ANFe.Emit.CNPJCPF := AFilial.FCnpj; // C02 e C02a
  ANFe.Emit.xNome := AFilial.FRazaoSocial; // C03 -  Razao Social ou Nome do Emitente
  ANFe.Emit.xFant := AFilial.FFantasia; // C04 - Nome Fantasia

  ANFe.Emit.EnderEmit.xLgr := AFilial.FLogradouro; // C06
  ANFe.Emit.EnderEmit.nro := AFilial.FNumero; // C07
  ANFe.Emit.EnderEmit.xCpl := AFilial.FComplemento; // C08
  ANFe.Emit.EnderEmit.xBairro := AFilial.FBairro; // C09
  ANFe.Emit.EnderEmit.cMun := AFilial.FCodigoIGBECidade; // C10
  ANFe.Emit.EnderEmit.xMun := AFilial.FNomeCidade; // C11
  ANFe.Emit.EnderEmit.UF := AFilial.FUF; // C12
  ANFe.Emit.EnderEmit.CEP := StrtoIntDef(AFilial.FCep,0); // C13
  ANFe.Emit.enderEmit.cPais := AFilial.FCodigoIGBEPais; // C14
  ANFe.Emit.enderEmit.xPais := AFilial.FNomePais; // C15
  ANFe.Emit.EnderEmit.fone := AFilial.FTelefone; // C16

  if not (AFilial.FIE.IsEmpty) then
  begin
    ANFe.Emit.IE := AFilial.FIE; // C17
  end;

  // -Verifico a sigla do estado do cliente para retornar a IE ST da filial para o estado do cliente, se a mesma existir se n�o vai vazio
  //ANFe.Emit.IEST := getInsc_Est_Filial_ST(AFilial.codigo, cliente_.sigla, cConnection); // C18 - IE do Substituto Tribut�rio

  ANFe.Emit.IM := AFilial.FIM; // C19 - Inscricao Municipal
  ANFe.Emit.CNAE := InttoStr(TCnaeServ.BuscarCodigoCNAE(AFilial.FIdCNAE)); // C20 - CNAE Fiscal
  ANFe.Emit.crt := StrToCRT(ok, IntToStr(AFilial.FCrt)); // C21 - C�digo de Regime Tribut�rio
end;

procedure TNotaFiscalEletronicaServ.ValidarConfiguracoesNFCE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
begin
  FComponentesFiscais.ValidarConfiguracoesNFCE(AConfiguracaoFiscal);
end;

procedure TNotaFiscalEletronicaServ.ValidarConfiguracoesNFE(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
begin
  FComponentesFiscais.ValidarConfiguracoesNFCE(AConfiguracaoFiscal);
end;

procedure TNotaFiscalEletronicaServ.ValidarFilial(AFilial: TFilialProxy);
var
  descricaoCliente: String;
begin
  descricaoCliente := InttoStr(AFilial.FId) +' - '+ AFilial.FFantasia;

  if AFilial.FCnpj.IsEmpty then
  begin
    FInconsistencias.Add('CPF/CNPJ da filial '+descricaoCliente+' n�o encontrado.');
  end
  else if not TPessoaServ.ValidaCnpjCeiCpf(AFilial.FCnpj) then
  begin
    FInconsistencias.Add('CPF/CNPJ da filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FIE.IsEmpty then
  begin
    FInconsistencias.Add('Inscri��o Estadual da filial '+descricaoCliente+' n�o encontrada.');
  end
  else if not TPessoaServ.ValidarInscricaoEstadual(AFilial.FIE, AFilial.FUf) then
  begin
    FInconsistencias.Add('Inscri��o Estadual da filial '+descricaoCliente+' � invalida.');
  end;

  if AFilial.FIdCNAE <= 0 then
  begin
    FInconsistencias.Add('CNAE da filial '+descricaoCliente+' � inv�lida.');
  end;

  if AFilial.FLogradouro.IsEmpty then
  begin
    FInconsistencias.Add('Logradouro da filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FBairro.IsEmpty then
  begin
    FInconsistencias.Add('Bairro da filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FIdCidade <= 0 then
  begin
    FInconsistencias.Add('Cidade da filial '+descricaoCliente+' � inv�lida.');
  end;

  if AFilial.FCep.IsEmpty then
  begin
    FInconsistencias.Add('CEP da filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FNumero.IsEmpty then
  begin
    FInconsistencias.Add('N�mero da logradouro do filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FCodigoIGBECidade <= 0 then
  begin
    FInconsistencias.Add('C�digo do IBGE da cidade '+AFilial.FNomeCidade+
      ' do filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FCodigoIGBEEstado <= 0 then
  begin
    FInconsistencias.Add('C�digo do IBGE do estado '+AFilial.FNomeEstado+
      ' do filial '+descricaoCliente+' � inv�lido.');
  end;

  if AFilial.FCodigoIGBEPais <= 0 then
  begin
    FInconsistencias.Add('C�digo do IBGE do pa�s '+AFilial.FNomePais+
      ' do filial '+descricaoCliente+' � inv�lido.');
  end;
end;

procedure TNotaFiscalEletronicaServ.ValidarNFEGerada;
begin
  try
    FACBrNFe.NotasFiscais.Validar;
  except
    on E: Exception do
    begin
      FInconsistencias.Add('Falha na valida��o do XML: '+E.Message);
    end;
  end;
end;

procedure TNotaFiscalEletronicaServ.ValidarPessoa(APessoa: TPessoaProxy; APessoaEndereco: TPessoaEnderecoProxy);
var
  descricaoCliente: String;
begin
  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    exit;
  end;

  descricaoCliente := InttoStr(APessoa.FId) +' - '+ APessoa.FNome;

  if APessoa.FDocFederal.IsEmpty then
  begin
    FInconsistencias.Add('CPF/CNPJ do cliente '+descricaoCliente+' n�o encontrado.');
  end
  else if not TPessoaServ.ValidaCnpjCeiCpf(APessoa.FDocFederal) then
  begin
    FInconsistencias.Add('CPF/CNPJ do cliente '+descricaoCliente+' � inv�lido.');
  end;

{  if APessoa.FTpPessoa = TPessoaProxy.TIPO_PESSOA_JURIDICA then
  begin
    if APessoa.FDocEstadual.IsEmpty then
    begin
      FInconsistencias.Add('Inscri��o Estadual do cliente '+descricaoCliente+' n�o encontrada.');
    end
    else if not TPessoaServ.ValidarInscricaoEstadual(APessoa.FDocEstadual, APessoaEndereco.FUF) then
    begin
      FInconsistencias.Add('Inscri��o Estadual do cliente '+descricaoCliente+' � invalida.');
    end;
  end; }

  if APessoaEndereco.FId <= 0 then
  begin
    FInconsistencias.Add('Dados do endere�o do cliente '+descricaoCliente+' n�o encontrado.');
  end
  else
  begin
    if APessoaEndereco.FLogradouro.IsEmpty then
    begin
      FInconsistencias.Add('Logradouro do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FBairro.IsEmpty then
    begin
      FInconsistencias.Add('Bairro do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FIdCidade <= 0 then
    begin
      FInconsistencias.Add('Cidade do cliente '+descricaoCliente+' � inv�lida.');
    end;

    if APessoaEndereco.FCep.IsEmpty then
    begin
      FInconsistencias.Add('CEP do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FNumero.IsEmpty then
    begin
      FInconsistencias.Add('N�mero do logradouro do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FCodigoIGBECidade <= 0 then
    begin
      FInconsistencias.Add('C�digo do IBGE da cidade '+APessoaEndereco.FNomeCidade+
        ' do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FCodigoIGBEEstado <= 0 then
    begin
      FInconsistencias.Add('C�digo do IBGE do estado '+APessoaEndereco.FNomeEstado+
        ' do cliente '+descricaoCliente+' � inv�lido.');
    end;

    if APessoaEndereco.FCodigoIGBEPais <= 0 then
    begin
      FInconsistencias.Add('C�digo do IBGE do pa�s '+APessoaEndereco.FNomePais+
        ' do cliente '+descricaoCliente+' � inv�lido.');
    end;
  end;
end;

procedure TNotaFiscalEletronicaServ.ValidarServidorAtivo(AConfiguracaoFiscal: TConfiguracaoFiscalProxy);
begin
  // Executa quando nao FS ou FS-DA
  if AnsiIndexStr(AConfiguracaoFiscal.FNfeFormaEmissao,
    [TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_NORMAL
    ,TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_DPEC
    ,TConfiguracaoFiscalProxy.NFE_FORMA_EMISSAO_CONTINGENCIA]) >= 0 then
  begin
    try
      FACBrNFe.WebServices.StatusServico.Executar;
    except
      on e: exception do
      begin
        FInconsistencias.Add(e.message);
        exit;
      end;
    end;

    if (FACBrNFe.WebServices.StatusServico.cStat <> 107) then
    begin
      FInconsistencias.Add('Status: ' + BuscarMensagemRetornoNFE(FACBrNFe.WebServices.StatusServico.cStat,
        FACBrNFe.WebServices.StatusServico.xMotivo));

      //abort;
    end;

    FACBrNFe.NotasFiscais.Clear;
  end;
end;

end.
