unit uIniFileServ;

interface

uses
  Classes, SysUtils, IniFiles, Forms, Windows;

type TIniSystemFile = record
  server, rotuloServidor, databaseERP, porta, caminho_gbak, caminho_relatorios: String;
  portadatasnap: Integer;
  IndoorPort : Integer; //8081
  const usuarioMYSQL = 'root';
  const senhaMYSQL = 't23mhcr247';
end;

type TIniOptions = class(TObject)
  private
    function FileName: String;
  public
    constructor Create;
    procedure LoadSettings(Ini: TIniFile);
    procedure SaveSettings(Ini: TIniFile);

    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);

  end;

var
  IniOptions: TIniOptions = nil;
  IniSystemFile: TIniSystemFile;

implementation

procedure TIniOptions.LoadSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin
      IniSystemFile.rotuloServidor := ReadString('GERAL','RotuloServidor','Servidor - Gest�o Empresarial');
      IniSystemFile.server := ReadString('MYSQL','server','127.0.0.1');
      IniSystemFile.porta := ReadString('MYSQL','porta','3306');
      IniSystemFile.databaseERP := ReadString('MYSQL','databaseERP','gestao_empresarial');
      IniSystemFile.portaDatasnap := ReadInteger('DATASNAP','porta',211);
      IniSystemFile.IndoorPort := ReadInteger('INDOOR', 'IndoorPort', 0);
    end;
  end;
end;

procedure TIniOptions.SaveSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin
      WriteString('GERAL','RotuloServidor','Servidor - Gest�o Empresarial');
      WriteString('MYSQL','server','127.0.0.1');
      WriteString('MYSQL','porta','3306');
      WriteString('MYSQL','databaseERP','gestao_empresarial');
      WriteInteger('DATASNAP','porta',211);
      WriteString('INDOOR','IndoorPort','');
    end;
  end;
end;

constructor TIniOptions.Create;
begin
  inherited Create;
  LoadFromFile(FileName);
end;

function TIniOptions.FileName: String;
begin
  result :=
    ExtractFilePath(Application.ExeName)+
    StringReplace(ExtractFileName(Application.ExeName)+'.ini',
                  ExtractFileExt(ExtractFileName(Application.ExeName)),
                  '', [rfReplaceAll]);
end;

procedure TIniOptions.LoadFromFile(const FileName: string);
var
  Ini: TIniFile;
begin
  if not FileExists(FileName) then
    SaveToFile(FileName);

  Ini := TIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    Ini.Free;
  end;
end;

procedure TIniOptions.SaveToFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    SaveSettings(Ini);
  finally
    Ini.Free;
  end;
end;

initialization
  IniOptions := TIniOptions.Create;

finalization
  IniOptions.Free;

end.

