unit uCESTServ;

Interface

Uses uCestProxy, SysUtils;

type TCestServ = class
  class function GetCest(const AIdCest: Integer): TCestProxy;
  class function GerarCest(const ACest: TCestProxy): Integer;
  class function BuscarCodigoCEST(const ACodigoNCM: String): String;
end;

implementation

{ TCestServ }

uses uFactoryQuery;

class function TCestServ.GetCest(
    const AIdCest: Integer): TCestProxy;
const
  SQL = 'SELECT * FROM CEST WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCestProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCest));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FCest := qConsulta.GetAsString('CEST');
  result.FNcm := qConsulta.GetAsString('NCM');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
end;

class function TCestServ.BuscarCodigoCEST(const ACodigoNCM: String): String;
const
  SQL = 'SELECT CEST FROM CEST WHERE ncm = :ncm ';
var
  qConsulta: IFastQuery;
  cest: String;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(ACodigoNCM));

  cest := qConsulta.GetAsString('CEST');

  if cest.IsEmpty then
  begin
    qConsulta.AtualizarConsultaSQL(SQL,
    TArray<Variant>.Create(Copy(ACodigoNCM,1,4)));

    cest := qConsulta.GetAsString('CEST');
  end;

  result := cest;
end;

class function TCestServ.GerarCest(
    const ACest: TCestProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('CEST');
    qEntidade.Incluir;

    qEntidade.SetAsString('CEST', ACest.FCest);
    qEntidade.SetAsString('NCM', ACest.FNcm);
    qEntidade.SetAsString('DESCRICAO', ACest.FDescricao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ACest.FID := qEntidade.GetAsInteger('ID');
    result :=     ACest.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Cest.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
