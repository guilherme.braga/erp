unit uReceitaOticaServ;

Interface

Uses uReceitaOticaProxy, SysUtils;

type TReceitaOticaServ = class
  class function GetReceitaOtica(
    const AIdReceitaOtica: Integer): TReceitaOticaProxy;
  class function GerarReceitaOtica(
    const AReceitaOtica: TReceitaOticaProxy): Integer;
  class procedure VincularVendaNaReceita(const AIdReceitaOtica, AIdVenda: Integer);
end;

implementation

{ TReceitaOticaServ }

uses uFactoryQuery;

class function TReceitaOticaServ.GetReceitaOtica(
    const AIdReceitaOtica: Integer): TReceitaOticaProxy;
const
  SQL =
    ' SELECT *'+
    ' ,(select nome from medico where id = receita_otica.id_medico) as medico'+
    ' ,(select nome from pessoa where id = receita_otica.id_pessoa) as pessoa'+
    ' FROM RECEITA_OTICA'+
    ' WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TReceitaOticaProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdReceitaOtica));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FDhPrevisaoEntrega := qConsulta.GetAsString('DH_PREVISAO_ENTREGA');
  result.FIdMedico := qConsulta.GetAsInteger('ID_MEDICO');
  result.FEspecificacao := qConsulta.GetAsString('ESPECIFICACAO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FIdVenda := qConsulta.GetAsInteger('ID_VENDA');
  result.FLongeDireitoEsferico := qConsulta.GetAsString('LONGE_DIREITO_ESFERICO');
  result.FLongeDireitoCilindrico := qConsulta.GetAsString('LONGE_DIREITO_CILINDRICO');
  result.FLongeDireitoEixo := qConsulta.GetAsString('LONGE_DIREITO_EIXO');
  result.FLongeEsquerdoEsferico := qConsulta.GetAsString('LONGE_ESQUERDO_ESFERICO');
  result.FLongeEsquerdoCilindrico := qConsulta.GetAsString('LONGE_ESQUERDO_CILINDRICO');
  result.FLongeEsquerdoEixo := qConsulta.GetAsString('LONGE_ESQUERDO_EIXO');
  result.FPertoDireitoEsferico := qConsulta.GetAsString('PERTO_DIREITO_ESFERICO');
  result.FPertoDireitoCilindrico := qConsulta.GetAsString('PERTO_DIREITO_CILINDRICO');
  result.FPertoDireitoEixo := qConsulta.GetAsString('PERTO_DIREITO_EIXO');
  result.FPertoEsquerdoEsferico := qConsulta.GetAsString('PERTO_ESQUERDO_ESFERICO');
  result.FPertoEsquerdoCilindrico := qConsulta.GetAsString('PERTO_ESQUERDO_CILINDRICO');
  result.FPertoEsquerdoEixo := qConsulta.GetAsString('PERTO_ESQUERDO_EIXO');
  result.FPertoDnpDireito := qConsulta.GetAsFloat('PERTO_DNP_DIREITO');
  result.FPertoDnpEsquerdo := qConsulta.GetAsFloat('PERTO_DNP_ESQUERDO');
  result.FPertoDnpGeral := qConsulta.GetAsFloat('PERTO_DNP_GERAL');
  result.FLongeDnpDireito := qConsulta.GetAsFloat('LONGE_DNP_DIREITO');
  result.FLongeDnpEsquerdo := qConsulta.GetAsFloat('LONGE_DNP_ESQUERDO');
  result.FLongeDnpGeral := qConsulta.GetAsFloat('LONGE_DNP_GERAL');
  result.FAltura := qConsulta.GetAsString('ALTURA');
  result.FAdicao := qConsulta.GetAsString('ADICAO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');

  {JOIN}
  result.FNomePessoa := qConsulta.GetAsString('PESSOA');
  result.FNomeMedico := qConsulta.GetAsString('MEDICO');
end;

class procedure TReceitaOticaServ.VincularVendaNaReceita(const AIdReceitaOtica, AIdVenda: Integer);
begin
  TFastQuery.ExecutarScript('UPDATE receita_otica set id_venda = :id_venda where id = :id_receita_otica',
    TArray<Variant>.Create(AIdVenda, AIdReceitaOtica));
end;

class function TReceitaOticaServ.GerarReceitaOtica(
    const AReceitaOtica: TReceitaOticaProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('RECEITA_OTICA');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO', AReceitaOtica.FDhCadastro);

    if AReceitaOtica.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', AReceitaOtica.FIdPessoaUsuario);
    end;

    if AReceitaOtica.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', AReceitaOtica.FIdPessoa);
    end;
    qEntidade.SetAsString('DH_PREVISAO_ENTREGA', AReceitaOtica.FDhPrevisaoEntrega);

    if AReceitaOtica.FIdMedico > 0 then
    begin
      qEntidade.SetAsInteger('ID_MEDICO', AReceitaOtica.FIdMedico);
    end;
    qEntidade.SetAsString('ESPECIFICACAO', AReceitaOtica.FEspecificacao);
    qEntidade.SetAsString('OBSERVACAO', AReceitaOtica.FObservacao);

    if AReceitaOtica.FIdVenda > 0 then
    begin
      qEntidade.SetAsInteger('ID_VENDA', AReceitaOtica.FIdVenda);
    end;
    qEntidade.SetAsString('LONGE_DIREITO_ESFERICO', AReceitaOtica.FLongeDireitoEsferico);
    qEntidade.SetAsString('LONGE_DIREITO_CILINDRICO', AReceitaOtica.FLongeDireitoCilindrico);
    qEntidade.SetAsString('LONGE_DIREITO_EIXO', AReceitaOtica.FLongeDireitoEixo);
    qEntidade.SetAsString('LONGE_ESQUERDO_ESFERICO', AReceitaOtica.FLongeEsquerdoEsferico);
    qEntidade.SetAsString('LONGE_ESQUERDO_CILINDRICO', AReceitaOtica.FLongeEsquerdoCilindrico);
    qEntidade.SetAsString('LONGE_ESQUERDO_EIXO', AReceitaOtica.FLongeEsquerdoEixo);
    qEntidade.SetAsString('PERTO_DIREITO_ESFERICO', AReceitaOtica.FPertoDireitoEsferico);
    qEntidade.SetAsString('PERTO_DIREITO_CILINDRICO', AReceitaOtica.FPertoDireitoCilindrico);
    qEntidade.SetAsString('PERTO_DIREITO_EIXO', AReceitaOtica.FPertoDireitoEixo);
    qEntidade.SetAsString('PERTO_ESQUERDO_ESFERICO', AReceitaOtica.FPertoEsquerdoEsferico);
    qEntidade.SetAsString('PERTO_ESQUERDO_CILINDRICO', AReceitaOtica.FPertoEsquerdoCilindrico);
    qEntidade.SetAsString('PERTO_ESQUERDO_EIXO', AReceitaOtica.FPertoEsquerdoEixo);
    qEntidade.SetAsFloat('PERTO_DNP_DIREITO', AReceitaOtica.FPertoDnpDireito);
    qEntidade.SetAsFloat('PERTO_DNP_ESQUERDO', AReceitaOtica.FPertoDnpEsquerdo);
    qEntidade.SetAsFloat('PERTO_DNP_GERAL', AReceitaOtica.FPertoDnpGeral);
    qEntidade.SetAsFloat('LONGE_DNP_DIREITO', AReceitaOtica.FLongeDnpDireito);
    qEntidade.SetAsFloat('LONGE_DNP_ESQUERDO', AReceitaOtica.FLongeDnpEsquerdo);
    qEntidade.SetAsFloat('LONGE_DNP_GERAL', AReceitaOtica.FLongeDnpGeral);
    qEntidade.SetAsString('ALTURA', AReceitaOtica.FAltura);
    qEntidade.SetAsString('ADICAO', AReceitaOtica.FAdicao);

    if AReceitaOtica.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AReceitaOtica.FIdFilial);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    AReceitaOtica.FID := qEntidade.GetAsInteger('ID');
    result :=     AReceitaOtica.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ReceitaOtica.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
