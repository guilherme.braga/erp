unit uCEPServ;

interface

Uses uCEPProxy;

type TCEPServ = class
  class function GetLogradouroPeloCEP(const AIdCEP: String): TCEPProxy;
  class function GetIdEstadoPelaCidade(const AIdCidade: Integer): Integer;
end;

implementation

{ TCEPServ }

uses uFactoryQuery;

class function TCEPServ.GetIdEstadoPelaCidade(const AIdCidade: Integer): Integer;
const
  SQL =
  ' SELECT'+
  ' id_estado'+
  ' FROM cidade'+
  ' WHERE'+
  '  id= :id';
var
  consultaEstado: IFastQuery;
begin;
  consultaEstado := TFastQuery.ModoDeConsulta(
    SQL, TArray<Variant>.Create(AIdCidade));

  result := consultaEstado.GetAsInteger('id_estado');
end;

class function TCEPServ.GetLogradouroPeloCEP(const AIdCEP: String): TCEPProxy;
const
  SQL =
  ' SELECT'+
  ' cep.id as id_cep,'+
  ' cep.cep,'+
  ' bairro.descricao as bairro,'+
  ' cep.id_cidade,'+
  ' cep.rua,'+
  ' cidade.descricao as cidade,'+
  ' estado.id as id_estado,'+
  ' estado.descricao as estado,'+
  ' estado.uf as uf,'+
  ' pais.descricao as pais,'+
  ' pais.id as id_pais'+
  ' FROM CEP'+
  ' inner join cidade on cidade.id = cep.id_cidade'+
  ' inner join bairro on bairro.id = cep.id_bairro'+
  ' inner join estado on estado.id = cidade.id_estado'+
  ' inner join pais on estado.id_pais = pais.id'+
  ' WHERE'+
  ' cep.cep = :cep';
var
  consultaCEP: IFastQuery;
begin
  result := TCEPProxy.Create;
  consultaCEP := TFastQuery.ModoDeConsulta(
    SQL, TArray<Variant>.Create(AIdCEP));

  result.FIdCep := consultaCEP.GetAsInteger('id_cep');
  result.FCep :=  consultaCEP.GetAsString('cep');
  result.FBairro := consultaCEP.GetAsString('bairro');
  result.FIdCidade := consultaCEP.GetAsInteger('id_cidade');
  result.FRua := consultaCEP.GetAsString('rua');
  result.FCidade := consultaCEP.GetAsString('cidade');
  result.FIdEstado := consultaCEP.GetAsInteger('id_estado');
  result.FEstado := consultaCEP.GetAsString('estado');
  result.FUf := consultaCEP.GetAsString('uf');
  result.FPais := consultaCEP.GetAsString('pais');
  result.FIdPais := consultaCEP.GetAsInteger('id_pais');
end;

end.
