unit uImpostoPisCofinsServ;

Interface

Uses uImpostoPisCofinsProxy;

type TImpostoPisCofinsServ = class
  class function GetImpostoPisCofins(
    const AIdImpostoPisCofins: Integer): TImpostoPisCofinsProxy;
end;

implementation

{ TImpostoPisCofinsServ }

uses uFactoryQuery;

class function TImpostoPisCofinsServ.GetImpostoPisCofins(
    const AIdImpostoPisCofins: Integer): TImpostoPisCofinsProxy;
const
  SQL = 'SELECT * FROM imposto_pis_cofins WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TImpostoPisCofinsProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdImpostoPisCofins));

  result.FId := qConsulta.GetAsString('ID');
  result.FPercAliquotaPis := qConsulta.GetAsFloat('PERC_ALIQUOTA_PIS');
  result.FPercAliquotaCofins := qConsulta.GetAsFloat('PERC_ALIQUOTA_COFINS');
  result.FPercAliquotaPisSt := qConsulta.GetAsFloat('PERC_ALIQUOTA_PIS_ST');
  result.FPercAliquotaCofinsSt := qConsulta.GetAsFloat('PERC_ALIQUOTA_COFINS_ST');
  result.FIdCstPisCofins := qConsulta.GetAsInteger('ID_CST_PIS_COFINS');
end;

end.


