unit uGradeProdutoServ;

Interface

Uses uGradeProdutoProxy, SysUtils, uFactoryQuery;

type TGradeProdutoServ = class
  class function GetGradeProduto(const AIdGradeProduto: Integer): TGradeProdutoProxy;
  class function GerarGradeProduto(const AGradeProduto: TGradeProdutoProxy): Integer;
  class function GetGradeProdutoCor(const AIdGradeProduto: Integer): TFastQuery;
  class function GetGradeProdutoCorTamanho(const AIdGradeProduto: Integer): TFastQuery;
end;

implementation

{ TGradeProdutoServ }

class function TGradeProdutoServ.GetGradeProduto(
    const AIdGradeProduto: Integer): TGradeProdutoProxy;
const
  SQL = 'SELECT * FROM GRADE_PRODUTO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TGradeProdutoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdGradeProduto));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

class function TGradeProdutoServ.GetGradeProdutoCor(const AIdGradeProduto: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM grade_produto_cor WHERE id_grade_produto = :id',
    TArray<Variant>.Create(AIdGradeProduto));
end;

class function TGradeProdutoServ.GetGradeProdutoCorTamanho(const AIdGradeProduto: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM grade_produto_cor_tamanho WHERE id_grade_produto = :id',
    TArray<Variant>.Create(AIdGradeProduto));
end;

class function TGradeProdutoServ.GerarGradeProduto(
    const AGradeProduto: TGradeProdutoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('GRADE_PRODUTO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AGradeProduto.FDescricao);
    qEntidade.SetAsString('OBSERVACAO', AGradeProduto.FObservacao);
    qEntidade.SetAsString('BO_ATIVO', AGradeProduto.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AGradeProduto.FID := qEntidade.GetAsInteger('ID');
    result :=     AGradeProduto.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar GradeProduto.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.
