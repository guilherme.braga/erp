unit uCstPisCofinsServ;

Interface

Uses uCstPisCofinsProxy;

type TCstPisCofinsServ = class
  class function GetCstPisCofins(
    const AIdCstPisCofins: Integer): TCstPisCofinsProxy;
end;

implementation

{ TCstPisCofinsServ }

uses uFactoryQuery;

class function TCstPisCofinsServ.GetCstPisCofins(
    const AIdCstPisCofins: Integer): TCstPisCofinsProxy;
const
  SQL = 'SELECT * FROM cst_pis_cofins WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCstPisCofinsProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCstPisCofins));

  result.FId := qConsulta.GetAsString('ID');
  result.FCodigoCst := qConsulta.GetAsString('CODIGO_CST');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
end;

end.


