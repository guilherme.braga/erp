unit uOperadoraCartaoServ;

interface

Uses uOperadoraCartaoProxy;

type TOperadoraCartaoServ = class
  class function GetOperadoraCartao(AIdOperadoraCartao: Integer): TOperadoraCartaoProxy;
  class function GetIdPessoaOperadoraCartao(AIdOperadoraCartao: Integer): Integer;
end;

implementation

{ TOperadoraCartaoServ }

Uses uFactoryQuery, SysUtils;

class function TOperadoraCartaoServ.GetIdPessoaOperadoraCartao(
  AIdOperadoraCartao: Integer): Integer;
var
  operadoraCartaoProxy: TOperadoraCartaoProxy;
begin
  operadoraCartaoProxy := TOperadoraCartaoServ.GetOperadoraCartao(
    AIdOperadoraCartao);
  try
    result := operadoraCartaoProxy.FIdPessoa;
  finally
    FreeAndNil(operadoraCartaoProxy);
  end;
end;

class function TOperadoraCartaoServ.GetOperadoraCartao(
  AIdOperadoraCartao: Integer): TOperadoraCartaoProxy;
var
  qOperadoraCartao: IFastQuery;
begin
  qOperadoraCartao := TFastQuery.ModoDeConsulta(
    'SELECT * FROM OPERADORA_CARTAO WHERE id = :id_operadora_cartao',
    TArray<Variant>.Create(AIdOperadoraCartao));
  result := TOperadoraCartaoProxy.Create;

  result.Fid := qOperadoraCartao.GetAsInteger('ID');
  result.FDescricao := qOperadoraCartao.GetAsString('DESCRICAO');
  result.FidPessoa := qOperadoraCartao.GetAsInteger('ID_PESSOA');
end;

end.
