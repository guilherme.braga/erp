unit uNegociacaoContaReceberServ;

Interface

Uses uNegociacaoContaReceberProxy, SysUtils;

type TNegociacaoContaReceberServ = class
  private
    class function BaixarTitulosNegociados(const AIdChaveProcesso: Integer): Boolean;
    class function EstornarTitulosNegociados(const AIdChaveProcesso: Integer): Boolean;
    class function GerarContaReceberDaNegociacao(const AIdChaveProcesso: Integer): Boolean;
    class function EstornarContaReceberDaNegociacao(const AIdChaveProcesso: Integer): Boolean;
    class function AtualizarStatus(const AIdNegociacaoContaReceber: Integer; AStatus: String): Boolean;
  public
    class function GetNegociacaoContaReceber(
      const AIdNegociacaoContaReceber: Integer):TNegociacaoContaReceberProxy;
    class function GerarNegociacaoContaReceber(
      const ANegociacaoContaReceber: TNegociacaoContaReceberProxy): Integer;
    class function Efetivar(const AIdChaveProcesso: Integer): Boolean;
    class function Estornar(const AIdChaveProcesso: Integer): Boolean;
    class function PodeEstornar(const AIdChaveProcesso: Integer): Boolean;
end;

implementation

{ TNegociacaoContaReceberServ }

uses uFactoryQuery, uContaReceberServ, uContaReceberProxy, uChaveProcessoProxy, uChaveProcessoServ, rest.JSON;

class function TNegociacaoContaReceberServ.GetNegociacaoContaReceber(
const AIdNegociacaoContaReceber: Integer):
TNegociacaoContaReceberProxy;
const
  SQL = 'SELECT * FROM negociacao_conta_receber WHERE id = :id ';
var
  qConsulta: IFastQuery;
begin
  result := TNegociacaoContaReceberProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
  TArray<Variant>.Create(AIdNegociacaoContaReceber));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FVlTotal := qConsulta.GetAsFloat('VL_TOTAL');
  result.FVlAcrescimo := qConsulta.GetAsFloat('VL_ACRESCIMO');
  result.FVlDesconto := qConsulta.GetAsFloat('VL_DESCONTO');
  result.FVlLiquido := qConsulta.GetAsFloat('VL_LIQUIDO');
  result.FVlEntrada := qConsulta.GetAsFloat('VL_ENTRADA');
  result.FDtParcelaInicial := qConsulta.GetAsString('DT_PARCELA_INICIAL');
  result.FDtCompetenciaInicial :=
  qConsulta.GetAsString('DT_COMPETENCIA_INICIAL');
  result.FStatus := qConsulta.GetAsString('STATUS');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FIdFormaPagamento := qConsulta.GetAsInteger('ID_FORMA_PAGAMENTO');
  result.FIdPlanoPagamento := qConsulta.GetAsInteger('ID_PLANO_PAGAMENTO');
  result.FIdContaAnalise := qConsulta.GetAsInteger('ID_CONTA_ANALISE');
  result.FIdCentroResultado :=
  qConsulta.GetAsInteger('ID_CENTRO_RESULTADO');
  result.FIdContaCorrente := qConsulta.GetAsInteger('ID_CONTA_CORRENTE');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
end;

class function TNegociacaoContaReceberServ.GerarNegociacaoContaReceber(
const ANegociacaoContaReceber: TNegociacaoContaReceberProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('negociacao_conta_receber');
    qEntidade.Incluir;

    qEntidade.SetAsString('DH_CADASTRO',
    ANegociacaoContaReceber.FDhCadastro);
    qEntidade.SetAsFloat('VL_TOTAL', ANegociacaoContaReceber.FVlTotal);
    qEntidade.SetAsFloat('VL_ACRESCIMO',
    ANegociacaoContaReceber.FVlAcrescimo);
    qEntidade.SetAsFloat('VL_DESCONTO',
    ANegociacaoContaReceber.FVlDesconto);
    qEntidade.SetAsFloat('VL_LIQUIDO', ANegociacaoContaReceber.FVlLiquido);
    qEntidade.SetAsFloat('VL_ENTRADA', ANegociacaoContaReceber.FVlEntrada);
    qEntidade.SetAsString('DT_PARCELA_INICIAL',
    ANegociacaoContaReceber.FDtParcelaInicial);
    qEntidade.SetAsString('DT_COMPETENCIA_INICIAL',
    ANegociacaoContaReceber.FDtCompetenciaInicial);
    qEntidade.SetAsString('STATUS', ANegociacaoContaReceber.FStatus);

    if ANegociacaoContaReceber.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA',
      ANegociacaoContaReceber.FIdPessoa);
    end;

    if ANegociacaoContaReceber.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL',
      ANegociacaoContaReceber.FIdFilial);
    end;

    if ANegociacaoContaReceber.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO',
      ANegociacaoContaReceber.FIdChaveProcesso);
    end;

    if ANegociacaoContaReceber.FIdFormaPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_FORMA_PAGAMENTO',
      ANegociacaoContaReceber.FIdFormaPagamento);
    end;

    if ANegociacaoContaReceber.FIdPlanoPagamento > 0 then
    begin
      qEntidade.SetAsInteger('ID_PLANO_PAGAMENTO',
      ANegociacaoContaReceber.FIdPlanoPagamento);
    end;

    if ANegociacaoContaReceber.FIdContaAnalise > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_ANALISE',
      ANegociacaoContaReceber.FIdContaAnalise);
    end;

    if ANegociacaoContaReceber.FIdCentroResultado > 0 then
    begin
      qEntidade.SetAsInteger('ID_CENTRO_RESULTADO',
      ANegociacaoContaReceber.FIdCentroResultado);
    end;

    if ANegociacaoContaReceber.FIdContaCorrente > 0 then
    begin
      qEntidade.SetAsInteger('ID_CONTA_CORRENTE',
      ANegociacaoContaReceber.FIdContaCorrente);
    end;
    qEntidade.SetAsString('OBSERVACAO',
    ANegociacaoContaReceber.FObservacao);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ANegociacaoContaReceber.FID := qEntidade.GetAsInteger('ID');
    result := ANegociacaoContaReceber.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar NegociacaoContaReceber.'+
      'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TNegociacaoContaReceberServ.Efetivar(const AIdChaveProcesso: Integer): Boolean;
begin
  try
    BaixarTitulosNegociados(AIdChaveProcesso);
    GerarContaReceberDaNegociacao(AIdChaveProcesso);
    AtualizarStatus(AIdChaveProcesso, TNegociacaoContaReceberProxy.STATUS_EFETIVADO);
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao efetivar Negociacao de Contas a Receber. '+e.message);
    end;
  end;
end;

class function TNegociacaoContaReceberServ.Estornar(const AIdChaveProcesso: Integer): Boolean;
begin
  try
    EstornarTitulosNegociados(AIdChaveProcesso);
    EstornarContaReceberDaNegociacao(AIdChaveProcesso);
    AtualizarStatus(AIdChaveProcesso, TNegociacaoContaReceberProxy.STATUS_ABERTO);
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao estornar Negociacao de Contas a Receber. '+e.message);
    end;
  end;
end;

class function TNegociacaoContaReceberServ.PodeEstornar(const AIdChaveProcesso: Integer): Boolean;
begin
  try
    result := TContaReceberServ.ExisteQuitacao(AIdChaveProcesso);
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao verificar possibilidade de estorno da Negociacao de Contas a Receber. '
        +e.message);
    end;
  end;
end;

class function TNegociacaoContaReceberServ.BaixarTitulosNegociados(const AIdChaveProcesso: Integer): Boolean;
var
  qNegociacaoContaReceber: IFastQuery;
  qNegociacaoContaReceberParcela: IFastQuery;
begin
  qNegociacaoContaReceber := TFastQuery.ModoDeConsulta(
    'SELECT * FROM negociacao_conta_receber where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  qNegociacaoContaReceberParcela := TFastQuery.ModoDeConsulta(
    ' SELECT * FROM negociacao_conta_receber_parcela where'+
    ' id_negociacao_conta_receber = :id_negociacao_conta_receber',
    TArray<Variant>.Create(qNegociacaoContaReceber.GetAsInteger('id')));

  qNegociacaoContaReceberParcela.First;
  while not qNegociacaoContaReceberParcela.Eof do
  begin
    TContaReceberServ.RealizarNegociacao(qNegociacaoContaReceberParcela.GetAsInteger('id_conta_receber'));
    qNegociacaoContaReceberParcela.Next;
  end;
end;

class function TNegociacaoContaReceberServ.EstornarTitulosNegociados(const AIdChaveProcesso: Integer): Boolean;
var
  qNegociacaoContaReceber: IFastQuery;
  qNegociacaoContaReceberParcela: IFastQuery;
begin
  qNegociacaoContaReceber := TFastQuery.ModoDeConsulta(
      'SELECT * FROM negociacao_conta_receber where id_chave_processo = :id_chave_processo',
      TArray<Variant>.Create(AIdChaveProcesso));

  qNegociacaoContaReceberParcela := TFastQuery.ModoDeConsulta(
    ' SELECT * FROM negociacao_conta_receber_parcela where '+
    ' id_negociacao_conta_receber = :id_negociacao_conta_receber',
    TArray<Variant>.Create(qNegociacaoContaReceber.GetAsInteger('id')));

  qNegociacaoContaReceberParcela.First;
  while not qNegociacaoContaReceberParcela.Eof do
  begin
    TContaReceberServ.RemoverNegociacao(qNegociacaoContaReceberParcela.GetAsInteger('id_conta_receber'));
    qNegociacaoContaReceberParcela.Next;
  end;
end;

class function TNegociacaoContaReceberServ.GerarContaReceberDaNegociacao(
  const AIdChaveProcesso:Integer): Boolean;
var
  contaReceber: TContaReceberMovimento;
  objectContaReceberJSON: String;
  qNegociacaoContaReceber, qNegociacaoContaReceberParcela: IFastQuery;
begin
  result := true;
  qNegociacaoContaReceber := TFastQuery.ModoDeConsulta(
    'SELECT * FROM negociacao_conta_receber where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  qNegociacaoContaReceberParcela := TFastQuery.ModoDeConsulta(
    ' SELECT * FROM negociacao_conta_receber_parcela '+
    ' where id_negociacao_conta_receber = :id_negociacao_conta_receber',
    TArray<Variant>.Create(qNegociacaoContaReceber.GetAsInteger('id')));

  qNegociacaoContaReceberParcela.First;
  while not qNegociacaoContaReceberParcela.Eof do
  begin
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa := qNegociacaoContaReceber.GetAsInteger('Id_Pessoa');

        FDocumento := qNegociacaoContaReceber.GetAsString('Documento');
        FDescricao := qNegociacaoContaReceber.GetAsString('Descricao');
        FObservacao := qNegociacaoContaReceber.GetAsString('Observacao');
        FStatus := ABERTO;

        FDtVencimento :=
        qNegociacaoContaReceberParcela.GetAsString('Dt_Vencimento');
        FDtCompetencia :=
        qNegociacaoContaReceberParcela.GetAsString('dt_competencia');
        FDtDocumento :=
        qNegociacaoContaReceberParcela.GetAsString('dt_documento');
        FDhCadastro := DateTimeToStr(Now);

        FVlTitulo :=
        qNegociacaoContaReceberParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado := 0;
        FVlAberto :=
        qNegociacaoContaReceberParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=
        qNegociacaoContaReceber.GetAsInteger('QT_PARCELAS');
        FNrParcela :=
        qNegociacaoContaReceberParcela.GetAsInteger('Nr_Parcela');

        FSequencia :=
        qNegociacaoContaReceberParcela.GetAsString('Nr_Parcela')+'/'+
        qNegociacaoContaReceberParcela.GetAsString('total_parcelas');

        FBoVencido := 'N';

        FIdContaAnalise :=
        qNegociacaoContaReceberParcela.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=
        qNegociacaoContaReceberParcela.GetAsInteger('Id_Centro_Resultado');
        FIdContaCorrente :=
        qNegociacaoContaReceberParcela.GetAsInteger('ID_CONTA_CORRENTE');

        FIdCarteira :=
        qNegociacaoContaReceber.GetAsInteger('ID_CARTEIRA');

        FIdChaveProcesso :=
        qNegociacaoContaReceber.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=
        qNegociacaoContaReceber.GetAsInteger('Id_Forma_Pagamento');

        FIdFilial := qNegociacaoContaReceber.GetAsInteger('Id_Filial');

        FIdDocumentoOrigem :=
        qNegociacaoContaReceberParcela.GetAsInteger('ID');
        FTPDocumentoOrigem :=
        TContaReceberMovimento.ORIGEM_GERACAO_DOCUMENTO;
      end;

      objectContaReceberJSON := TJson.ObjectToJsonString(contaReceber);
      TContaReceberServ.GerarContaReceber(objectContaReceberJSON);

      TChaveProcessoServ.AtualizarChaveProcesso(contaReceber.FIdChaveProcesso,
      TChaveProcessoProxy.ORIGEM_NEGOCIACAO_CONTA_RECEBER,
      qNegociacaoContaReceberParcela.GetAsInteger('ID'));
    finally
      contaReceber.Free;
    end;


    qNegociacaoContaReceberParcela.Proximo;
  end;
end;

class function TNegociacaoContaReceberServ.EstornarContaReceberDaNegociacao(const AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
    TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);
end;

class function TNegociacaoContaReceberServ.AtualizarStatus(const AIdNegociacaoContaReceber: Integer;
  AStatus: String): Boolean;
begin
  TFastQuery.ExecutarScriptIndependente(
  'UPDATE negociacao_conta_receber SET status = :status where id = :id_negociacao_conta_receber',
  TArray<Variant>.Create(AStatus, AIdNegociacaoContaReceber));
end;

end.

