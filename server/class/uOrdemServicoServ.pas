unit uOrdemServicoServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery,
  uFactoryQuery, uContaReceberProxy,uOrdemServicoProxy, DateUtils;

type TOrdemServicoServ = class
  private
    class function ValorEntradaQuitacao(AIdChaveProcesso: Integer): Double;

  public
    class function GetOrdemServico(AIdChaveProcesso: Integer): TFastQuery;
    class function GetOrdemServicoProduto(AIdOrdemServico: Integer): TFastQuery;
    class function GetOrdemServicoParcela(AIdOrdemServico: Integer): TFastQuery;

    class function GerarContaReceber(
      AOrdemServico, AOrdemServicoParcela: TFastQuery; AOrdemServicoProxy: TOrdemServicoProxy): boolean;
    class function GerarEntradaQuitacao(
      AOrdemServico: TFastQuery; AOrdemServicoProxy: TOrdemServicoProxy): Boolean;
    class function GerarContaReceberCartaoCredito(AContaReceber:
      TContaReceberMovimento; AOrdemServicoProxy: TOrdemServicoProxy; AOrdemServico: TFastQuery;
      AIdOperadoraCartao: Integer): boolean;
    class function GerarContaReceberCartaoDebito(AContaReceber:
      TContaReceberMovimento; AOrdemServicoProxy: TOrdemServicoProxy; AOrdemServico: TFastQuery;
      AIdOperadoraCartao: Integer): boolean;
    class function GerarContaReceberCheque(AContaReceber: TContaReceberMovimento;
      AOrdemServico, AOrdemServicoParcela: TFastQuery; AOrdemServicoProxy: TOrdemServicoProxy): boolean;
    class function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
      class function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;

    class function GerarMovimentacaoEstoque(
      AOrdemServico, AOrdemServicoItem: TFastQuery; AAcao: String): boolean;

    class function Efetivar(AIdChaveProcesso: Integer;
      AOrdemServicoProxy: String): Boolean;

    class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    class function Cancelar(AIdChaveProcesso: Integer): Boolean;
    class procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);

    class function GetId(AIdChaveProcesso: Integer): Integer;

    class function GetIdNotaFiscalNFCE(AId: Integer): Integer;
    class function GetIdNotaFiscalNFE(AId: Integer): Integer;
    class function GetChaveProcesso(AId: Integer): Integer;
    class procedure AtualizarCodigoNotaFiscalNaOrdemDeServico(const AIdOrdemServico, AIdNotaFiscal: Integer;
      ATipoDocumentoFiscal: String);
end;

implementation

uses uContaReceberServ, uContaPagarProxy,
  uContaPagarServ, uOperacaoServ, uAcaoProxy, uProdutoProxy, uProdutoServ,
  uDateUtils, uFormaPagamentoServ, uContaCorrenteProxy,
  uOperadoraCartaoServ, uContaCorrenteServ, uTipoQuitacaoServ,
  uTipoQuitacaoProxy, uNotaFiscalProxy;

{ TOrdemServicoServ }

class procedure TOrdemServicoServ.AtualizarCodigoNotaFiscalNaOrdemDeServico(const AIdOrdemServico,
  AIdNotaFiscal: Integer; ATipoDocumentoFiscal: String);
begin
  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE ordem_servico SET id_nota_fiscal_nfce = :id_nota_fiscal_nfce where id = :id_ordem_servico',
      TArray<Variant>.Create(AIdNotaFiscal, AIdOrdemServico));

    exit;
  end;

  if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    TFastQuery.ExecutarScript(
      'UPDATE ordem_servico SET id_nota_fiscal_nfe = :id_nota_fiscal_nfe where id = :id_ordem_servico',
      TArray<Variant>.Create(AIdNotaFiscal, AIdOrdemServico));
  end;
end;

class procedure TOrdemServicoServ.AtualizarStatus(
  AIdChaveProcesso: Integer; AStatus: String);
begin
  TFastQuery.ExecutarScriptIndependente(
    'UPDATE ordem_servico SET status = :status where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AStatus, AidChaveProcesso));
end;

class function TOrdemServicoServ.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  if TContaReceberServ.ExisteContaReceber(AIdChaveProcesso) then
    TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);

  if TContaPagarServ.ExisteContaContaPagar(AIdChaveProcesso) then
    TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);

  TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso);

  TContaCorrenteServ.GerarContraPartidaContaCorrente(AIdChaveProcesso);

  TOrdemServicoServ.AtualizarStatus(AIdChaveProcesso,TOrdemServicoProxy.STATUS_CANCELADO);
end;

class function TOrdemServicoServ.EstornarContaReceber(
  AIdChaveProcesso: Integer): Boolean;
var
  qOrdemServico, qOrdemServicoParcela: IFastQuery;
begin
  result := true;
  TContaReceberServ.RemoverContaReceber(AIdChaveProcesso);
end;

class function TOrdemServicoServ.Efetivar(AIdChaveProcesso: Integer; AOrdemServicoProxy: String): Boolean;
var
  qVenda,
  qVendaItem,
  qVendaParcela: TFastQuery;
  vendaProxy: TOrdemServicoProxy;
begin
  Result := False;
  try
    qVenda := TOrdemServicoServ.GetOrdemServico(AIdChaveProcesso);
    try
      qVendaItem := TOrdemServicoServ.GetOrdemServicoProduto(qVenda.GetAsInteger('ID'));
      try
        qVendaParcela := TOrdemServicoServ.GetOrdemServicoParcela(qVenda.GetAsInteger('ID'));
        try
          vendaProxy := TJson.JsonToObject<TOrdemServicoProxy>(AOrdemServicoProxy);
          try
            //N�O FUNCIONA OPERA��O E A��O NA ORDEM DE SERVI�O AINDA, N�O VIMOS A NECESSIDADE,
            //ENT�O SEMPRE GERA ESTOQUE E SEMPRE ATUALIZA ESTOQUE

            {Caso venha a gerar entrada de estoque no futuro}
            {if qVenda.GetAsString('TIPO').Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
              TOrdemServicoServ.GerarMovimentacaoEstoque(qVenda, qVendaItem, GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA)
            else}

            TOrdemServicoServ.GerarMovimentacaoEstoque(qVenda, qVendaItem, GERAR_MOVIMENTACAO_ESTOQUE_SAIDA);

            TOrdemServicoServ.GerarContaReceber(qVenda, qVendaParcela, vendaProxy);
            AtualizarStatus(AIdChaveProcesso, TOrdemServicoProxy.STATUS_FECHADO);
          finally
            vendaProxy.Free;
          end;
        finally
          FreeAndNil(qVendaParcela);
        end;
      finally
        FreeAndNil(qVendaItem);
      end;
    finally
      FreeAndNil(qVenda);
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar a movimenta��o de conta corrente.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.EstornarContaPagar(
  AIdChaveProcesso: Integer): Boolean;
begin
  result := true;
  TContaPagarServ.RemoverContaPagar(AIdChaveProcesso);
end;

class function TOrdemServicoServ.GerarContaReceber(AOrdemServico,
  AOrdemServicoParcela: TFastQuery; AOrdemServicoProxy: TOrdemServicoProxy): boolean;
var
  contaReceber: TContaReceberMovimento;
begin
  result := false;
  try
    if AOrdemServico.GetAsFloat('VL_PAGAMENTO') > 0 then
    begin
      TOrdemServicoServ.GerarEntradaQuitacao(AOrdemServico, AOrdemServicoProxy);
    end;

    AOrdemServicoParcela.First;
    while not AOrdemServicoParcela.Eof do
    begin
      contaReceber := TContaReceberMovimento.Create;
      try
        with contaReceber do
        begin
          FIdPessoa :=            AOrdemServico.GetAsInteger('Id_Pessoa');

          FDocumento  :=          AOrdemServico.GetAsString('ID');
          FDescricao  :=          'Ordem de Servi�o: '+AOrdemServico.GetAsString('ID');
          FStatus     :=          ABERTO;

          FDtVencimento :=        AOrdemServicoParcela.GetAsString('Dt_Vencimento');
          FDtDocumento  :=        AOrdemServicoParcela.GetAsString('Dt_Vencimento');
          FDhCadastro   :=        DateTimeToStr(Now);

          FVlTitulo  :=           AOrdemServicoParcela.GetAsFloat('Vl_Titulo');
          FVlQuitado :=           0;
          FVlAberto  :=           AOrdemServicoParcela.GetAsFloat('Vl_Titulo');

          FVlAcrescimo := 0;
          FVlDecrescimo := 0;
          FVlQuitadoLiquido := 0;

          FQtParcela :=           AOrdemServicoParcela.GetAsInteger('qt_parcela');
          FNrParcela :=           AOrdemServicoParcela.GetAsInteger('Nr_Parcela');

          FSequencia  :=          AOrdemServicoParcela.GetAsString('Nr_Parcela')+'/'+
            AOrdemServicoParcela.GetAsString('qt_parcela');

          FBoVencido  :=          'N';
          FObservacao :=          '';

          FIdContaAnalise    :=   AOrdemServico.GetAsInteger('Id_Conta_Analise');
          FIdCentroResultado :=   AOrdemServico.GetAsInteger('Id_Centro_Resultado');

          FIdFilial          :=    AOrdemServico.GetAsInteger('Id_Filial');

          FIdChaveProcesso  :=    AOrdemServico.GetAsInteger('Id_Chave_Processo');
          FIdFormaPagamento :=    AOrdemServicoParcela.GetAsInteger('Id_Forma_Pagamento');
          FIdContaCorrente :=     AOrdemServicoProxy.FIdContaCorrente;
          FIdCarteira := AOrdemServicoParcela.GetAsInteger('Id_Carteira');

          FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
          FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_ORDEM_SERVICO;
        end;

        if TFormaPagamentoServ.FormaPagamentoCartaoDebito(contaReceber.FIdFormaPagamento) then
        begin
          TOrdemServicoServ.GerarContaReceberCartaoDebito(contaReceber, AOrdemServicoProxy,
            AOrdemServico, AOrdemServicoParcela.GetAsInteger('ID_OPERADORA_CARTAO'));
        end
        else if TFormaPagamentoServ.FormaPagamentoCartaoCredito(contaReceber.FIdFormaPagamento) then
        begin
          TOrdemServicoServ.GerarContaReceberCartaoCredito(contaReceber, AOrdemServicoProxy,
            AOrdemServico, AOrdemServicoParcela.GetAsInteger('ID_OPERADORA_CARTAO'));
        end
        else if TFormaPagamentoServ.FormaPagamentoCheque(contaReceber.FIdFormaPagamento) then
        begin
          TOrdemServicoServ.GerarContaReceberCheque(contaReceber, AOrdemServico, AOrdemServicoParcela, AOrdemServicoProxy);
        end
        else if TFormaPagamentoServ.FormaPagamentoDinheiro(contaReceber.FIdFormaPagamento) then
        begin
          TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));
        end;
      finally
        contaReceber.Free;
      end;
      AOrdemServicoParcela.Proximo;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.GerarContaReceberCartaoCredito(AContaReceber:
    TContaReceberMovimento; AOrdemServicoProxy: TOrdemServicoProxy; AOrdemServico: TFastQuery;
    AIdOperadoraCartao: Integer): boolean;
var
  IdContaReceber: Integer;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
begin
  result := true;
  try
    //Gera��o do conta a receber do cliente
    IdContaReceber := TContaReceberServ.GerarContaReceber(
      TJson.ObjectToJsonString(AContaReceber));

    //Gera��o da baixa do conta a receber do cliente
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber  := IdContaReceber;
      Quitacao.FDhCadastro      := DateTimeToStr(Now);
      Quitacao.FDtQuitacao      := DatetoStr(AOrdemServico.GetAsDate('dh_cadastro'));
      Quitacao.FObservacao      := AContaReceber.FObservacao;
      Quitacao.FIdFilial        := AOrdemServico.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao  :=
        TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
        AOrdemServico.GetAsInteger('ID_FORMA_PAGAMENTO'));
      Quitacao.FIdOperadoraCartao := AIdOperadoraCartao;
      Quitacao.FIdContaCorrente := AOrdemServicoProxy.FIdContaCorrente;
      Quitacao.FIdCentroResultado  := AContaReceber.FIdCentroResultado;
      Quitacao.FIdContaAnalise  := AContaReceber.FIdContaAnalise;
      Quitacao.FVlDesconto      := 0;
      Quitacao.FVlAcrescimo     := 0;

      Quitacao.FVlQuitacao        := AContaReceber.FVlTitulo;
      Quitacao.FVlTotal           := AContaReceber.FVlTitulo;
      Quitacao.FPercAcrescimo     := 0;
      Quitacao.FPercDesconto      := 0;
      Quitacao.FIdDocumentoOrigem := AContaReceber.FIdDocumentoOrigem;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_ORDEM_SERVICO;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(IdContaReceber);
    finally
      Quitacao.Free;
    end;

    {Gera��o do conta a receber da operadora para 30 dias ap�s a data original
      pois � cart�o de d�bito}
    AContaReceber.FIdPessoa :=
      TOperadoraCartaoServ.GetIdPessoaOperadoraCartao(AIdOperadoraCartao);
    AContaReceber.FDtVencimento := DatetoStr(IncMonth(StrtoDate(AContaReceber.FDtVencimento)));
    AContaReceber.FDtDocumento  := DatetoStr(IncMonth(StrtoDate(AContaReceber.FDtDocumento)));
    TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(AContaReceber));
  except
    on e : Exception do
    begin
      Result := False;
      raise Exception.Create('Erro ao gerar conta a receber com a forma de pagamento de cart�o de d�bito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.GerarContaReceberCartaoDebito(AContaReceber:
  TContaReceberMovimento; AOrdemServicoProxy: TOrdemServicoProxy; AOrdemServico: TFastQuery;
  AIdOperadoraCartao: Integer): boolean;
var
  IdContaReceber: Integer;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
begin
  result := false;
  try
    //Gera��o do conta a receber do cliente
    IdContaReceber := TContaReceberServ.GerarContaReceber(
      TJson.ObjectToJsonString(AContaReceber));

    //Gera��o da baixa do conta a receber do cliente
    Quitacao  := TContaReceberQuitacaoMovimento.Create;
    try
      Quitacao.FIdContaReceber  := IdContaReceber;
      Quitacao.FDhCadastro      := DateTimeToStr(Now);
      Quitacao.FDtQuitacao      := DatetoStr(AOrdemServico.GetAsDate('dh_cadastro'));
      Quitacao.FObservacao      := AContaReceber.FObservacao;
      Quitacao.FIdFilial        := AOrdemServico.GetAsInteger('ID_FILIAL');
      Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
        AOrdemServico.GetAsInteger('ID_FORMA_PAGAMENTO'));
      Quitacao.FIdOperadoraCartao := AIdOperadoraCartao;
      Quitacao.FIdContaCorrente := AOrdemServicoProxy.FIdContaCorrente;
      Quitacao.FIdCentroResultado  := AContaReceber.FIdCentroResultado;
      Quitacao.FIdContaAnalise  := AContaReceber.FIdContaAnalise;
      Quitacao.FVlDesconto      := 0;
      Quitacao.FVlAcrescimo     := 0;

      Quitacao.FVlQuitacao        := AContaReceber.FVlTitulo;
      Quitacao.FVlTotal           := AContaReceber.FVlTitulo;
      Quitacao.FPercAcrescimo     := 0;
      Quitacao.FPercDesconto      := 0;
      Quitacao.FIdDocumentoOrigem := AContaReceber.FIdDocumentoOrigem;
      Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_ORDEM_SERVICO;
      Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

      TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
      TContaReceberServ.AtualizarValores(IdContaReceber);
    finally
      Quitacao.Free;
    end;

    {Gera��o do conta a receber da operadora para 1 dia ap�s a data original
      pois � cart�o de d�bito}
    AContaReceber.FIdPessoa :=
      TOperadoraCartaoServ.GetIdPessoaOperadoraCartao(AIdOperadoraCartao);
    AContaReceber.FDtVencimento := DatetoStr(IncDay(StrtoDate(AContaReceber.FDtVencimento)));
    AContaReceber.FDtDocumento  := DatetoStr(IncDay(StrtoDate(AContaReceber.FDtDocumento)));
    TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(AContaReceber));
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber com a forma de pagamento de cart�o de d�bito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.GerarContaReceberCheque(
  AContaReceber: TContaReceberMovimento; AOrdemServico, AOrdemServicoParcela: TFastQuery;
  AOrdemServicoProxy: TOrdemServicoProxy): boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
  tipoQuitacao: String;
begin
  result := false;
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            AOrdemServico.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AOrdemServico.GetAsString('ID');
        FDescricao  :=          'Ordem de Servi�o: '+AOrdemServico.GetAsString('ID');
        FStatus     :=          ABERTO;

        FDtVencimento :=        AOrdemServicoParcela.GetAsString('Dt_Vencimento');
        FDtDocumento  :=        AOrdemServicoParcela.GetAsString('Dt_Vencimento');
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AOrdemServicoParcela.GetAsFloat('Vl_Titulo');
        FVlQuitado :=           0;
        FVlAberto  :=           AOrdemServicoParcela.GetAsFloat('Vl_Titulo');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           AOrdemServicoParcela.GetAsInteger('qt_parcela');
        FNrParcela :=           AOrdemServicoParcela.GetAsInteger('Nr_Parcela');

        FSequencia  :=          AOrdemServicoParcela.GetAsString('Nr_Parcela')+'/'+
          AOrdemServicoParcela.GetAsString('qt_parcela');

        FBoVencido  :=          'N';
        FObservacao :=          '';

        FIdContaAnalise    :=   AOrdemServico.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AOrdemServico.GetAsInteger('Id_Centro_Resultado');

        FIdFilial          :=    AOrdemServico.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AOrdemServico.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AOrdemServicoParcela.GetAsInteger('Id_Forma_Pagamento');
        FIdContaCorrente :=     AOrdemServicoProxy.FIdContaCorrente;
        FIdCarteira := AOrdemServicoParcela.GetAsInteger('Id_Carteira');

        FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_ORDEM_SERVICO;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AOrdemServico.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := AOrdemServico.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          contaReceber.FIdFormaPagamento);
        Quitacao.FIdContaCorrente := AOrdemServicoProxy.FIdContaCorrente;
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := AOrdemServico.GetAsFloat('Vl_Pagamento');
        Quitacao.FVlTotal           := AOrdemServico.GetAsFloat('Vl_Pagamento');
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_ORDEM_SERVICO;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := AOrdemServicoParcela.GetAsString('CHEQUE_SACADO');
        Quitacao.FChequeDocFederal    := AOrdemServicoParcela.GetAsString('CHEQUE_DOC_FEDERAL');
        Quitacao.FChequeBanco         := AOrdemServicoParcela.GetAsString('CHEQUE_BANCO');
        Quitacao.FChequeDtEmissao     := AOrdemServicoParcela.GetAsString('CHEQUE_DT_EMISSAO');
        Quitacao.FChequeDtVencimento  := AOrdemServicoParcela.GetAsString('CHEQUE_DT_VENCIMENTO');
        Quitacao.FChequeAgencia       := AOrdemServicoParcela.GetAsString('CHEQUE_AGENCIA');
        Quitacao.FChequeContaCorrente := AOrdemServicoParcela.GetAsString('CHEQUE_CONTA_CORRENTE');
        Quitacao.FChequeNumero        := AOrdemServicoParcela.GetAsInteger('CHEQUE_NUMERO');

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AOrdemServico.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;
          Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_ORDEM_SERVICO;
          Movimento.FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');

          //Se for cheque a concilia��o � falsa
          tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(Quitacao.FIdTipoQuitacao);
          if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
            (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
          begin
            Movimento.FBoCheque := 'S';
            Movimento.FBoConciliado := 'N';

            Movimento.FChequeSacado        := Quitacao.FChequeSacado;
            Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
            Movimento.FChequeBanco         := Quitacao.FChequeBanco;
            Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
            Movimento.FChequeDtVencimento  := Quitacao.FChequeDtVencimento;
            Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
            Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
            Movimento.FChequeNumero        := Quitacao.FChequeNumero;
          end
          else
          begin
            Movimento.FBoConciliado := 'S';
          end;

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.GerarEntradaQuitacao(AOrdemServico: TFastQuery;
  AOrdemServicoProxy: TOrdemServicoProxy): Boolean;
var
  contaReceber: TContaReceberMovimento;
  Quitacao       : TContaReceberQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;
  IdContaReceber: Integer;
begin
  try
    contaReceber := TContaReceberMovimento.Create;
    try
      with contaReceber do
      begin
        FIdPessoa :=            AOrdemServico.GetAsInteger('Id_Pessoa');

        FDocumento  :=          AOrdemServico.GetAsString('ID');
        FDescricao  :=          'Ordem de Servi�o: '+AOrdemServico.GetAsString('ID');
        FStatus     :=          ABERTO;

        FDtVencimento :=        DatetoStr(Date);
        FDtDocumento  :=        DatetoStr(AOrdemServico.GetAsDate('dh_cadastro'));
        FDhCadastro   :=        DateTimeToStr(Now);

        FVlTitulo  :=           AOrdemServico.GetAsFloat('Vl_Pagamento');
        FVlQuitado :=           0;
        FVlAberto  :=           AOrdemServico.GetAsFloat('Vl_Pagamento');

        FVlAcrescimo := 0;
        FVlDecrescimo := 0;
        FVlQuitadoLiquido := 0;

        FQtParcela :=           1;
        FNrParcela :=           1;

        FSequencia  :=          InttoStr(FNrParcela)+'/'+InttoStr(FQtParcela);

        FBoVencido  :=          'N';
        FObservacao :=          'Entrada/Quita��o de Ordem de Servi�o';

        FIdContaAnalise    :=   AOrdemServico.GetAsInteger('Id_Conta_Analise');
        FIdCentroResultado :=   AOrdemServico.GetAsInteger('Id_Centro_Resultado');

        FIdFilial :=   AOrdemServico.GetAsInteger('Id_Filial');

        FIdChaveProcesso  :=    AOrdemServico.GetAsInteger('Id_Chave_Processo');
        FIdFormaPagamento :=    AOrdemServicoProxy.FIdFormaPagamentoDinheiro;
        FIdContaCorrente :=     AOrdemServicoProxy.FIdContaCorrente;
        FIdCarteira := AOrdemServicoProxy.FIdCarteira;

        FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
        FTPDocumentoOrigem := TContaReceberMovimento.ORIGEM_ORDEM_SERVICO;
      end;
      IdContaReceber := TContaReceberServ.GerarContaReceber(TJson.ObjectToJsonString(contaReceber));

      Quitacao  := TContaReceberQuitacaoMovimento.Create;
      try
        Quitacao.FIdContaReceber  := IdContaReceber;
        Quitacao.FDhCadastro      := DateTimeToStr(Now);
        Quitacao.FDtQuitacao      := DatetoStr(AOrdemServico.GetAsDate('dh_cadastro'));
        Quitacao.FObservacao      := contaReceber.FObservacao;
        Quitacao.FIdFilial        := AOrdemServico.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := TFormaPagamentoServ.GetTipoQuitacaoFormaPagamento(
          AOrdemServicoProxy.FIdFormaPagamentoDinheiro);
        Quitacao.FIdContaCorrente := AOrdemServicoProxy.FIdContaCorrente;
        Quitacao.FIdCentroResultado  := contaReceber.FIdCentroResultado;
        Quitacao.FIdContaAnalise  := contaReceber.FIdContaAnalise;
        Quitacao.FVlDesconto      := 0;
        Quitacao.FVlAcrescimo     := 0;

        Quitacao.FVlQuitacao        := AOrdemServico.GetAsFloat('Vl_Pagamento');
        Quitacao.FVlTotal           := AOrdemServico.GetAsFloat('Vl_Pagamento');
        Quitacao.FPercAcrescimo     := 0;
        Quitacao.FPercDesconto      := 0;
        Quitacao.FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaReceberQuitacaoMovimento.ORIGEM_ORDEM_SERVICO;
        Quitacao.FStatus            := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

        TContaReceberServ.GerarContaReceberQuitacao(TJson.ObjectToJsonString(Quitacao));
        TContaReceberServ.AtualizarValores(IdContaReceber);

        Movimento := TContaCorrenteMovimento.Create;
        try
          Movimento.FDocumento         := AOrdemServico.GetAsString('ID');
          Movimento.FDescricao         := contaReceber.FDescricao;
          Movimento.FObservacao        := Quitacao.FObservacao;
          Movimento.FDhCadastro        := DateTimeToStr(Now);
          Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
          Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
          Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
          Movimento.FVlMovimento       := Quitacao.FVlTotal;
          Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO;
          Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
          Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
          Movimento.FIdCentroResultado := contaReceber.FIdCentroResultado;
          Movimento.FIdChaveProcesso   := contaReceber.FIdChaveProcesso;
          Movimento.FIdFilial          := contaReceber.FIdFilial;
          Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_ORDEM_SERVICO;
          Movimento.FIdDocumentoOrigem := AOrdemServico.GetAsInteger('ID');
          Movimento.FBoConciliado      := 'S';

          TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
        finally
          Movimento.Free;
        end;
      finally
        Quitacao.Free;
      end;
    finally
      contaReceber.Free;
    end;
    result := true;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar conta a receber.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TOrdemServicoServ.GerarMovimentacaoEstoque(AOrdemServico,
  AOrdemServicoItem: TFastQuery; AAcao: String): boolean;
var tipoMovimentacao: String;
  estoque: TProdutoMovimento;
  objectMovimentoEstoqueJSON: String;
begin
  estoque := TProdutoMovimento.Create;
  try
    if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_ENTRADA then
    begin
      tipoMovimentacao := MOVIMENTO_ENTRADA;
    end
    else if AAcao = GERAR_MOVIMENTACAO_ESTOQUE_SAIDA then
    begin
      tipoMovimentacao := MOVIMENTO_SAIDA;
    end;

    AOrdemServicoItem.First;
    while not AOrdemServicoItem.Eof do
    begin
      with estoque do
      begin
        FIdProduto         := AOrdemServicoItem.GetAsInteger('ID_PRODUTO');
        FIdFilial          := AOrdemServico.GetAsInteger('ID_FILIAL');
        FIdChaveProcesso   := AOrdemServico.GetAsInteger('ID_CHAVE_PROCESSO');
        FTipo              := tipoMovimentacao;
        FDocumentoOrigem   := DOCUMENTO_VENDA;
        FDtMovimento       := DateToStr(Date);
        FQtEstoqueAnterior := TProdutoServ.GetEstoqueAtual(FIdProduto, FIdFilial);
        FQtMovimento       := AOrdemServicoItem.GetAsFloat('QUANTIDADE');
        FQtEstoqueAtual    := FQtEstoqueAnterior - FQtMovimento;
      end;
      objectMovimentoEstoqueJSON := TJson.ObjectToJsonString(estoque);
      TProdutoMovimentoServ.GerarMovimentacaoProduto(objectMovimentoEstoqueJSON);
      AOrdemServicoItem.Next;
    end;
    result := true;
  finally
    FreeAndNil(estoque);
  end;
end;

class function TOrdemServicoServ.GetChaveProcesso(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_chave_processo FROM ordem_servico WHERE id = :id',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_chave_processo');
end;

class function TOrdemServicoServ.GetId(AIdChaveProcesso: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id FROM venda WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := consulta.GetAsInteger('ID');
end;

class function TOrdemServicoServ.GetIdNotaFiscalNFCE(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_nota_fiscal_nfce FROM ordem_servico WHERE id = :id',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_nota_fiscal_nfce');
end;

class function TOrdemServicoServ.GetIdNotaFiscalNFE(AId: Integer): Integer;
var
  consulta: IFastQuery;
begin
  consulta := TFastQuery.ModoDeConsulta(
    'SELECT id_nota_fiscal_nfe FROM ordem_servico WHERE id = :id',
    TArray<Variant>.Create(AId));

  result := consulta.GetAsInteger('id_nota_fiscal_nfe');
end;

class function TOrdemServicoServ.GetOrdemServico(AIdChaveProcesso: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM ordem_servico WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TOrdemServicoServ.GetOrdemServicoProduto(AIdOrdemServico: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM ordem_servico_produto WHERE id_ordem_servico = :id_ordem_servico',
    TArray<Variant>.Create(AIdOrdemServico));
end;

class function TOrdemServicoServ.GetOrdemServicoParcela(
  AIdOrdemServico: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM ordem_servico_parcela WHERE id_ordem_servico = :id_ordem_servico',
    TArray<Variant>.Create(AIdOrdemServico));
end;

class function TOrdemServicoServ.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TOrdemServicoServ.PodeExcluir(AIdChaveProcesso);
end;

class function TOrdemServicoServ.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var
  existeCRQuitado: boolean;
  CRReferenteEntradaQuitacao: Boolean;
  CRReferenteCartaoCredito: Boolean;
  valorEntrada: Double;
  valorQuitacaoTotal: Double;
  valorQuitacaoCartao: Double;
begin
  result := false;

  existeCRQuitado := TContaReceberServ.ExisteQuitacao(AIdChaveProcesso);

  if existeCRQuitado then
  begin
    valorEntrada := TOrdemServicoServ.ValorEntradaQuitacao(AIdChaveProcesso);
    valorQuitacaoTotal := TContaReceberServ.TotalQuitacaoRealizada(AIdChaveProcesso);
    valorQuitacaoCartao := TContaReceberServ.TotalQuitacaoRealizadaPorCartao(AIdChaveProcesso);

    if valorEntrada > 0 then
    begin
      CRReferenteEntradaQuitacao := (valorQuitacaoTotal = valorEntrada);

      if CRReferenteEntradaQuitacao then
      begin
        result := true;
        exit;
      end;
    end;

    if valorQuitacaoCartao > 0 then
    begin
      CRReferenteCartaoCredito := ((valorQuitacaoCartao + valorEntrada) = valorQuitacaoTotal);

      if CRReferenteCartaoCredito then
      begin
        result := true;
        exit;
      end;
    end;

    result := false;
  end
  else
  begin
    result := true;
  end;
end;

class function TOrdemServicoServ.ValorEntradaQuitacao(
  AIdChaveProcesso: Integer): Double;
var
  qContaReceber: IFastQuery;
begin
  qContaReceber := TFastQuery.ModoDeConsulta(
    ' SELECT vl_pagamento'+
    ' FROM ordem_servico'+
    ' WHERE id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := qContaReceber.getAsFloat('vl_pagamento');
end;

end.
