unit uIntegracaoFiscalOrdemServicoServ;

interface

Uses Data.DB, System.Generics.Collections, REST.Json, SysUtils, uGBFDQuery, uFactoryQuery, 
  uIntegracaoFiscalServ;

type TIntegracaoFiscalOrdemServicoServ = class(TIntegracaoFiscalServ)
  protected
    function ValidarDocumentoFiscalJaEmitido: Boolean; override;
    procedure InstanciarEntidadeOrigem; override;
    procedure AtualizarCodigoNaEntidadeBase; override;
end;

implementation

uses uNotaFiscalProxy, uOrdemServicoServ;

function TIntegracaoFiscalOrdemServicoServ.ValidarDocumentoFiscalJaEmitido: Boolean;
begin
  if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
  begin
    result := TOrdemServicoServ.GetIdNotaFiscalNFE(FIdEntidadeBase) > 0;
  end
  else if FTipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
  begin
    result := TOrdemServicoServ.GetIdNotaFiscalNFCE(FIdEntidadeBase) > 0;
  end;
end;	

procedure TIntegracaoFiscalOrdemServicoServ.InstanciarEntidadeOrigem;
begin
  FEntidadeBase := TOrdemServicoServ.GetOrdemServico(TOrdemServicoServ.GetChaveProcesso(FIdEntidadeBase));
  FEntidadeBaseItem := TOrdemServicoServ.GetOrdemServicoProduto(FIdEntidadeBase);
  FEntidadeBaseParcela := TOrdemServicoServ.GetOrdemServicoParcela(FIdEntidadeBase);
end;

procedure TIntegracaoFiscalOrdemServicoServ.AtualizarCodigoNaEntidadeBase;
begin
  TOrdemServicoServ.AtualizarCodigoNotaFiscalNaOrdemDeServico(FIdEntidadeBase,
    FIdNotaFiscal, FTipoDocumentoFiscal);	
end;

end.