unit uZoneamentoServ;

Interface

Uses uZoneamentoProxy;

type TZoneamentoServ = class
  class function GetZoneamento(
    const AIdZoneamento: Integer): TZoneamentoProxy;
end;

implementation

{ TZoneamentoServ }

uses uFactoryQuery;

class function TZoneamentoServ.GetZoneamento(
    const AIdZoneamento: Integer): TZoneamentoProxy;
const
  SQL = 'SELECT * FROM zoneamento WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TZoneamentoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdZoneamento));

  result.FId := qConsulta.GetAsString('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

end.


