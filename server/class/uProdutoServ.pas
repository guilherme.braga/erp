unit uProdutoServ;

interface

Uses
  System.SysUtils, FireDAC.Comp.Client, Data.FireDACJSONReflect, REST.Json,
  uProdutoProxy, Classes, Generics.Collections, uProdutoReservaEstoqueMovimentoProxy,
  uProdutoReservaEstoqueMovimentoServ, uFactoryQuery;

{ REGRAS GERAIS - CONTEXTO
  Sempre ir� gerar a sa�da do estoque real em uma sa�da de estoque, caso queira enviar essa
  movimenta��o para uma posi��o, dever� ser invocado o m�todo RealizarReservaDeEstoque passando
  o proxy da movimenta��o e informar qual a posi��o de estoque que ser� utilizada. A reserva �
  feita de acordo com o tipo da movimenta��o de origem, ou seja, se a movimenta��o � de SAIDA
  do estoque REAL, ent�o a reserva ser� de ENTRADA, se a movimenta��o � de ENTRADA no estoque REAL
  ent�o a reserva ser� de SAIDA
}

type TProdutoMovimentoServ = class
  class procedure GerarMovimentacaoProduto(AProdutoEstoque: String);
  class function GetChaveProcessoMovimentacaoInicial(const AIdProduto: Integer): Integer;
  class function RemoverMovimentacaoProduto(AIdChaveProcesso: Integer): Boolean;
  class function PegarUltimaMovimentacaoProduto(AIdProduto, AIdFilial: Integer;
    AConsiderarMovimentosEstornados: Boolean = true; AIdChaveProcesso: Integer = 0): TProdutoMovimento;
  class function GetProdutoMovimento(const AIdProdutoMovimento: Integer): TProdutoMovimento;
  class procedure AmortizarProcessos(const AIdChaveProcesso: Integer);
  class procedure ExcluirRegistroDasMovimentacoesProduto(AIdChaveProcesso: Integer);
end;

type TProdutoServ = class
    private
      class function DuplicarRegistroProduto(const AIdProdutoOrigem: Integer): Integer;
      class procedure DuplicarRegistroProdutoFornecedor(
        const AIdProdutoOrigem, AIdProdutoDestino: Integer);
      class procedure DuplicarRegistroProdutoModeloMontadora(
        const AIdProdutoOrigem, AIdProdutoDestino: Integer);
      class procedure DuplicarRegistroProdutoMontadora(
        const AIdProdutoOrigem, AIdProdutoDestino: Integer);
      class procedure DuplicarRegistroProdutoTributacaoPorUnidade(
        const AIdProdutoOrigem, AIdProdutoDestino: Integer);
      class procedure SetProdutoFiscalInformacoesProduto(AProdutoFiscal: TProdutoFiscalProxy;
        const AIdProduto: Integer);
      class procedure SetProdutoFiscalTributacao(AProdutoFiscal: TProdutoFiscalProxy;
        const AIdProduto, AIdFilial, AIdPessoa: Integer);
      class procedure SetProdutoFiscalTributacaoCFOP(AProdutoFiscal: TProdutoFiscalProxy;
        AIdProduto, AIdOperacaoFiscal, AIdImpostoICMS: Integer);
      class procedure SetProdutoFiscalTributacaoICMS(AProdutoFiscal: TProdutoFiscalProxy;
        AIdImpostoICMS: Integer);
      class procedure SetProdutoFiscalTributacaoIPI(AProdutoFiscal: TProdutoFiscalProxy;
        AIdImpostoIPI: Integer);
      class procedure SetProdutoFiscalTributacaoPISConfins(AProdutoFiscal: TProdutoFiscalProxy;
        AIdImpostoPisCofins: Integer);
      class function GetInformacoesFiltroImposto(const AIdProduto: Integer): TFastQuery;
      class function GetIdRegraImposto(const AIdProduto: Integer): Integer;

    public
      class function GetEstoqueAtual(AIdProduto, AIdFilial: Integer): Double;
      class function ExisteFilialParaProduto(AIdProduto, AIdFilial: Integer): Boolean; static;
      class function CriarFilialParaProduto(AIdProduto, AIdFilial: Integer): Boolean;
      class function GetProximoID: Integer;
      class function CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
        const AIdProduto: Integer): Integer;
      class function GetProduto(AIdProduto: Integer): TFDJSONDataSets;
      class function GetProdutoQuery(AIdProduto: Integer): TFastQuery;
      class function GetTodosGruposProduto(): TFDJSONDataSets;
      class function GetProdutoPeloCodigoBarra(ACodigoBarraProduto: String): TFDJSONDataSets;
      class function GetProdutos(AIdsProdutos: string): TFDJSONDataSets; static;
      class function GetProdutosCompleto(AIdsProdutos: string): TFDJSONDataSets; static;
      class function GetProdutoFilial(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
      class function GetLitaProdutos(AIdsProdutos: String; AIdFilial: Integer): TList<TProdutoProxy>;
      class function ExisteMovimentacoesDiferentesDaMovimentacaoInicial(
        const AIdProduto, AIdFilial: Integer): Boolean;
      class function GerarProduto(const AProduto: TProdutoProxy): Integer;
      class function DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
      class function GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
      class procedure SetProdutoAgrupado(const AIdProdutoAgrupador, AIdProduto: Integer);
      class procedure SetProdutoAgrupador(const AIdProduto: Integer);
      class procedure AlterarTamanhoCorProduto(const AIdProduto, AIdCor, AIdTamanho: Integer);
      class procedure AlterarCodigoBarraProduto(const AIdProduto: Integer; const ACodigoBarra: String);
      class function BuscarIdProdutoAgrupadoPorCorTamanho(const AIdProdutoAgrupador,
        AIdCor, AIdTamanho: Integer): Integer;
      class procedure AtualizarCodigoBarra(const ACodigoBarra: String; const AIdProduto: Integer);
      class function GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
      class function VerificarTipoMercadoria(const AIdProduto: Integer): Boolean;
      class function VerificarTipoProduto(const AIdProduto: Integer): Boolean;
      class function CodigoEANValido(const ACodigoBarra: String): Boolean;
      class procedure RefazerEAN13TodosProdutos;
end;

type TProdutoFilialServ = class
  class function GetProdutoFilial(
    const AIdProduto, AIdFilial: Integer): TProdutoFilialProxy;
  class function GerarProdutoFilial(
    const AProdutoMovimento: TProdutoMovimento): Integer;
  class function AtualizarReservaDeEstoque(AIdProduto, AIdFilial: Integer;
    AQuantidadeEstoqueAtual: Double; APosicaoDestino: TPosicoesEstoque): Boolean;
  class function BuscarValorCusto(const AIdProduto, AIdFilial: Integer): Double;
end;

{PRODUTO_MOVIMENTO.TIPO - TIpo de Movimentacao}
const MOVIMENTO_ENTRADA: String = 'ENTRADA';
const MOVIMENTO_SAIDA: String = 'SAIDA';

{PRODUTO_MOVIMENTO.DOUMENTO_ORIGEM - Movimento de Origem}
const DOCUMENTO_NOTA_FISCAL: String = 'NOTA_FISCAL';
const DOCUMENTO_VENDA: String = 'VENDA';
const DOCUMENTO_AJUSTE_ESTOQUE: String = 'AJUSTE_ESTOQUE';


implementation

{ TProdutoServ }

uses uChaveProcessoProxy, uDatasetUtilsServ, uOperacaoServ, uRegraImpostoServ, uRegraImpostoFiltroProxy,
  uRegraImpostoFiltroServ, uImpostoICMSProxy, uImpostoICMSServ, uImpostoIPIProxy, uImpostoIPIServ,
  uImpostoPisCofinsProxy, uImpostoPisCofinsServ, uCSTCSOSNProxy, uCSTCSOSNServ, uCSTIPIProxy, uCSTIPIServ,
  uCSTPisCofinsProxy, uCSTPisCofinsServ, ACBrUtil, uConstantesFiscaisProxy,
  uTabelaPrecoServ;

class function TProdutoServ.GerarProduto(
    const AProduto: TProdutoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('produto');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AProduto.FDescricao);
    qEntidade.SetAsString('BO_ATIVO', AProduto.FBoAtivo);

    if AProduto.FIdGrupoProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_GRUPO_PRODUTO', AProduto.FIdGrupoProduto);
    end;

    if AProduto.FIdSubGrupoProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_SUB_GRUPO_PRODUTO', AProduto.FIdSubGrupoProduto);
    end;

    if AProduto.FIdMarca > 0 then
    begin
      qEntidade.SetAsInteger('ID_MARCA', AProduto.FIdMarca);
    end;

    if AProduto.FIdCategoria > 0 then
    begin
      qEntidade.SetAsInteger('ID_CATEGORIA', AProduto.FIdCategoria);
    end;

    if AProduto.FIdSubCategoria > 0 then
    begin
      qEntidade.SetAsInteger('ID_SUB_CATEGORIA', AProduto.FIdSubCategoria);
    end;

    if AProduto.FIdLinha > 0 then
    begin
      qEntidade.SetAsInteger('ID_LINHA', AProduto.FIdLinha);
    end;

    if AProduto.FIdModelo > 0 then
    begin
      qEntidade.SetAsInteger('ID_MODELO', AProduto.FIdModelo);
    end;

    if AProduto.FIdUnidadeEstoque > 0 then
    begin
      qEntidade.SetAsInteger('ID_UNIDADE_ESTOQUE', AProduto.FIdUnidadeEstoque);
    end;
    qEntidade.SetAsString('TIPO', AProduto.FTipo);
    qEntidade.SetAsString('CODIGO_BARRA', AProduto.FCodigoBarra);

    if AProduto.FIdCor > 0 then
    begin
      qEntidade.SetAsInteger('ID_COR', AProduto.FIdCor);
    end;
    qEntidade.SetAsString('FORMA_AQUISICAO', AProduto.FFormaAquisicao);
    qEntidade.SetAsInteger('ORIGEM_DA_MERCADORIA', AProduto.FOrigemDaMercadoria);

    if AProduto.FIdNcm > 0 then
    begin
      qEntidade.SetAsInteger('ID_NCM', AProduto.FIdNcm);
    end;

    if AProduto.FIdNcmEspecializado > 0 then
    begin
      qEntidade.SetAsInteger('ID_NCM_ESPECIALIZADO', AProduto.FIdNcmEspecializado);
    end;
    qEntidade.SetAsInteger('TIPO_TRIBUTACAO_PIS_COFINS_ST', AProduto.FTipoTributacaoPisCofinsSt);
    qEntidade.SetAsInteger('TIPO_TRIBUTACAO_IPI', AProduto.FTipoTributacaoIpi);
    qEntidade.SetAsInteger('EXCECAO_IPI', AProduto.FExcecaoIpi);

    if AProduto.FIdRegraImposto > 0 then
    begin
      qEntidade.SetAsInteger('ID_REGRA_IMPOSTO', AProduto.FIdRegraImposto);
    end;
    qEntidade.SetAsString('DH_CADASTRO', AProduto.FDhCadastro);
    qEntidade.SetAsString('BO_PRODUTO_AGRUPADOR', AProduto.FBoProdutoAgrupador);
    qEntidade.SetAsString('BO_PRODUTO_AGRUPADO', AProduto.FBoProdutoAgrupado);

    if AProduto.FIdProdutoAgrupador > 0 then
    begin
      qEntidade.SetAsInteger('ID_PRODUTO_AGRUPADOR', AProduto.FIdProdutoAgrupador);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    AProduto.FID := qEntidade.GetAsInteger('ID');
    result :=     AProduto.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Produto.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TProdutoServ.GetProdutoFilial(const AIdProduto,
  AIdFilial: Integer): TProdutoProxy;
const
  SQL =
  ' SELECT'+
  '   produto.*'+
  '  ,unidade_estoque.sigla'+
  ' FROM PRODUTO'+
  ' LEFT JOIN unidade_estoque ON produto.id_unidade_estoque = unidade_estoque.id'+
  ' WHERE produto.id = :id_produto';
var
  qConsulta: IFastQuery;
begin
  result := TProdutoProxy.Create;
  qConsulta := TFastQuery.ModoDeConsulta(SQL,TArray<Variant>.Create(AIdProduto));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FIdGrupoProduto := qConsulta.GetAsInteger('ID_GRUPO_PRODUTO');
  result.FIdSubGrupoProduto := qConsulta.GetAsInteger('ID_SUB_GRUPO_PRODUTO');
  result.FIdMarca := qConsulta.GetAsInteger('ID_MARCA');
  result.FIdCategoria := qConsulta.GetAsInteger('ID_CATEGORIA');
  result.FIdSubCategoria := qConsulta.GetAsInteger('ID_SUB_CATEGORIA');
  result.FIdLinha := qConsulta.GetAsInteger('ID_LINHA');
  result.FIdModelo := qConsulta.GetAsInteger('ID_MODELO');
  result.FIdUnidadeEstoque := qConsulta.GetAsInteger('ID_UNIDADE_ESTOQUE');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FCodigoBarra := qConsulta.GetAsString('CODIGO_BARRA');
  result.FIdCor := qConsulta.GetAsInteger('ID_COR');
  result.FFormaAquisicao := qConsulta.GetAsString('FORMA_AQUISICAO');
  result.FOrigemDaMercadoria := qConsulta.GetAsInteger('ORIGEM_DA_MERCADORIA');
  result.FIdNcm := qConsulta.GetAsInteger('ID_NCM');
  result.FIdNcmEspecializado := qConsulta.GetAsInteger('ID_NCM_ESPECIALIZADO');
  result.FTipoTributacaoPisCofinsSt := qConsulta.GetAsInteger('TIPO_TRIBUTACAO_PIS_COFINS_ST');
  result.FTipoTributacaoIpi := qConsulta.GetAsInteger('TIPO_TRIBUTACAO_IPI');
  result.FExcecaoIpi := qConsulta.GetAsInteger('EXCECAO_IPI');
  result.FIdRegraImposto := qConsulta.GetAsInteger('ID_REGRA_IMPOSTO');
  result.FDhCadastro := qConsulta.GetAsString('DH_CADASTRO');
  result.FBoProdutoAgrupador := qConsulta.GetAsString('BO_PRODUTO_AGRUPADOR');
  result.FBoProdutoAgrupado := qConsulta.GetAsString('BO_PRODUTO_AGRUPADO');
  result.FIdProdutoAgrupador := qConsulta.GetAsInteger('ID_PRODUTO_AGRUPADOR');

  result.FUN := qConsulta.GetAsString('sigla');
  result.FProdutoFilial := TProdutoFilialServ.GetProdutoFilial(AIdProduto, AIdFilial);
  result.FQtdeEstoque := result.FProdutoFilial.FQtEstoque;
end;

class procedure TProdutoServ.SetProdutoFiscalInformacoesProduto(AProdutoFiscal: TProdutoFiscalProxy;
  const AIdProduto: Integer);
const
  SQL_PRODUTO =
  '  SELECT produto.id AS ID_PRODUTO'+
  '        ,produto.codigo_barra AS I_CEAN'+
  '        ,produto.descricao AS I_XPROD'+
  '        ,produto.origem_da_mercadoria as N_ORIG'+
  '        ,(select codigo from ncm where id = produto.id_ncm) AS I_NCM'+
  '        ,IFNULL((select sigla'+
  '            from unidade_estoque'+
  '           where id = produto.id_unidade_estoque), ''UN'') AS I_UCOM'+
  '    from produto '+
  '   where produto.id = :id_produto';

var
  qConsultaProduto: IFastQuery;
  codigoCEAN: String;
begin
  qConsultaProduto := TFastQuery.ModoDeConsulta(SQL_PRODUTO, TArray<Variant>.Create(AIdProduto));

  codigoCEAN := qConsultaProduto.GetAsString('I_CEAN');

  if not TProdutoServ.CodigoEANValido(codigoCEAN) then
  begin
    codigoCEAN := TProdutoServ.GerarCodigoBarraEAN13(qConsultaProduto.GetAsInteger('ID_PRODUTO'));
  end;

  AProdutoFiscal.ID_PRODUTO := qConsultaProduto.GetAsString('ID_PRODUTO');
  AProdutoFiscal.I_CEAN := codigoCEAN;
  AProdutoFiscal.I_CEANTRIB := codigoCEAN;
  AProdutoFiscal.I_XPROD := qConsultaProduto.GetAsString('I_XPROD');
  AProdutoFiscal.I_NCM := qConsultaProduto.GetAsString('I_NCM');
  AProdutoFiscal.I_UCOM := qConsultaProduto.GetAsString('I_UCOM');
  AProdutoFiscal.I_UTRIB := qConsultaProduto.GetAsString('I_UCOM');
  AProdutoFiscal.N_ORIG := qConsultaProduto.GetAsString('N_ORIG');
end;

class procedure TProdutoServ.SetProdutoFiscalTributacao(AProdutoFiscal: TProdutoFiscalProxy;
  const AIdProduto, AIdFilial, AIdPessoa: Integer);
var
  regraImpostoFiltro: TRegraImpostoFiltroProxy;
  impostoICMS: TImpostoICMSProxy;
  impostoIPI: TImpostoIPIProxy;
  impostoPISCofins: TImpostoPISCofinsProxy;
begin
  regraImpostoFiltro := TRegraImpostoFiltroServ.BuscarImpostoAdequado(AIdProduto, AIdPessoa, AIdFilial,
    TProdutoServ.GetIdRegraImposto(AIdProduto));

  AProdutoFiscal.ID_REGRA_IMPOSTO_FILTRO := regraImpostoFiltro.FId;
  AProdutoFiscal.ID_IMPOSTO_ICMS := regraImpostoFiltro.FIdImpostoICMS;
  AProdutoFiscal.ID_IMPOSTO_IPI := regraImpostoFiltro.FIdImpostoIPI;
  AProdutoFiscal.ID_IMPOSTO_PISCofins := regraImpostoFiltro.FIdImpostoPISCofins;

  if regraImpostoFiltro.FId <= 0 then
  begin
    exit;
  end;

  TProdutoServ.SetProdutoFiscalTributacaoCFOP(AProdutoFiscal, AIdProduto, regraImpostoFiltro.FIdOperacaoFiscal,
    regraImpostoFiltro.FIdImpostoICMS);

  if regraImpostoFiltro.FIdImpostoICMS > 0 then
  begin
    TProdutoServ.SetProdutoFiscalTributacaoICMS(AProdutoFiscal, regraImpostoFiltro.FIdImpostoICMS);
  end;

  if regraImpostoFiltro.FIdImpostoIPI > 0 then
  begin
    TProdutoServ.SetProdutoFiscalTributacaoIPI(AProdutoFiscal, regraImpostoFiltro.FIdImpostoICMS);
  end;

  if regraImpostoFiltro.FIdImpostoPISCofins > 0 then
  begin
    TProdutoServ.SetProdutoFiscalTributacaoPISConfins(AProdutoFiscal, regraImpostoFiltro.FIdImpostoICMS);
  end;
end;

class procedure TProdutoServ.SetProdutoFiscalTributacaoCFOP(AProdutoFiscal: TProdutoFiscalProxy;
  AIdProduto, AIdOperacaoFiscal, AIdImpostoICMS: Integer);
begin
  AProdutoFiscal.I_CFOP := InttoStr(TOperacaoServ.BuscarCFOP(AidOperacaoFiscal,
    VerificarTipoMercadoria(AIdProduto), AIdImpostoICMS));
end;

class procedure TProdutoServ.SetProdutoFiscalTributacaoICMS(AProdutoFiscal: TProdutoFiscalProxy;
  AIdImpostoICMS: Integer);
var
  impostoICMS: TImpostoICMSProxy;
  cst: TCstCsosnProxy;
begin
  impostoICMS := TImpostoICMSServ.GetImpostoICMS(AIdImpostoICMS);
  cst := TCstCsosnServ.GetCstCsosn(impostoICMS.FIdCstCsosn);
  try
    if cst.FCodigoCsosn.IsEmpty then
    begin
      AProdutoFiscal.N_CST := cst.FCodigoCst;
    end
    else//Caso tenha CSOSN A CST � A CSOSN (Regra do Simples nacional)
    begin
      AProdutoFiscal.N_CST := cst.FCodigoCsosn;
    end;

    AProdutoFiscal.N_MODBC := InttoStr(impostoICMS.FModalidadeBaseIcms);
    AProdutoFiscal.N_pICMS := impostoICMS.FPercAliquotaIcms;
    //AProdutoFiscal.N_vICMS :=
    AProdutoFiscal.N_MODBCST := InttoStr(impostoICMS.FModalidadeBaseIcmsSt);
    AProdutoFiscal.N_pMVAST := impostoICMS.FPercMvaAjustadoSt;
    AProdutoFiscal.N_pRedBCST := impostoICMS.FPercReducaoIcmsSt;
    //AProdutoFiscal.N_vBCST :=
    AProdutoFiscal.N_pICMSST := impostoICMS.FPercAliquotaIcmsSt;
    //AProdutoFiscal.N_vICMSST := impostoICMS.;
    AProdutoFiscal.N_pRedBC := impostoICMS.FPercReducaoIcms;
    AProdutoFiscal.N_motDesICMS := impostoICMS.FMotivoDesoneracaoIcms;
    //AProdutoFiscal.N_vBCSTRet := impostoICMS.XXX;
    //AProdutoFiscal.N_vICMSSTRet := impostoICMS.XXX;
    //AProdutoFiscal.N_pCredSN := impostoICMS.;
    //AProdutoFiscal.N_vCredICMSSN := impostoICMS.XXX;
  finally
    FreeAndNil(impostoICMS);
    FreeAndNil(cst);
  end;
end;

class procedure TProdutoServ.SetProdutoFiscalTributacaoIPI(AProdutoFiscal: TProdutoFiscalProxy;
  AIdImpostoIPI: Integer);
var
  impostoIPI: TImpostoIPIProxy;
  cst: TCstIPIProxy;
begin
  impostoIPI := TImpostoIPIServ.GetImpostoIPI(AIdImpostoIPI);
  cst := TCstIPIServ.GetCstIPI(impostoIPI.FIdCstIPI);
  try
    AProdutoFiscal.O_clEnq := cst.FCENQ;
    //AProdutoFiscal.O_CNPJProd := impostoIPI.XXX;
    //AProdutoFiscal.O_cSelo := impostoIPI.XXX;
    //AProdutoFiscal.O_qSelo := impostoIPI.XXX;
    //AProdutoFiscal.O_cEnq := impostoIPI.XXX;
    //AProdutoFiscal.O_qUnid := impostoIPI.XXX;
    //AProdutoFiscal.O_vUnid := impostoIPI.XXX;
    //AProdutoFiscal.O_vIPI := impostoIPI.XXX;
    AProdutoFiscal.O_CST := cst.FCodigoCst;
    //AProdutoFiscal.O_vBC := impostoIPI.XXX;
    AProdutoFiscal.O_pIPI := impostoIPI.FPercAliquota;
  finally
    FreeAndNil(impostoIPI);
    FreeAndNil(cst);
  end;
end;

class procedure TProdutoServ.SetProdutoFiscalTributacaoPISConfins(AProdutoFiscal: TProdutoFiscalProxy;
  AIdImpostoPisCofins: Integer);
var
  impostoPisCofins: TImpostoPisCofinsProxy;
  cst: TCstPisCofinsProxy;
begin
  impostoPisCofins := TImpostoPisCofinsServ.GetImpostoPisCofins(AIdImpostoPisCofins);
  cst := TCstPisCofinsServ.GetCstPisCofins(impostoPisCofins.FIdCstPisCofins);
  try
    //Imposto PIS
    AProdutoFiscal.Q_CST := cst.FCodigoCst;
    //Q_VBC := impostoPisCofins.;
    AProdutoFiscal.Q_PPIS := impostoPisCofins.FPercAliquotaPis;
    //Q_VPIS := impostoPisCofins.xxx;
    //Q_BCPROD := impostoPisCofins.xxx;
    //Q_vAliqProd := impostoPisCofins.xxx;

    //Imposto COFINS
    AProdutoFiscal.S_CST := cst.FCodigoCst;
    //S_vBC := impostoPisCofins.xxx;
    AProdutoFiscal.S_pCOFINS := impostoPisCofins.FPercAliquotaCofins;
    //S_vCOFINS := impostoPisCofins.xxx;
    //S_BCProd := impostoPisCofins.xxx;
    //S_vAliqProd := impostoPisCofins.xxx;
    //S_qBCProd := impostoPisCofins.xxx;
  finally
    FreeAndNil(impostoPisCofins);
    FreeAndNil(cst);
  end;
end;

class function TProdutoServ.VerificarTipoMercadoria(const AIdProduto: Integer): Boolean;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT FORMA_AQUISICAO'+
  '   FROM produto ', nil);

  result := qProduto.GetAsString('FORMA_AQUISICAO').Equals(TConstantesFiscais.FORMA_AQUISICAO_ADQUIRIDA_TERCEIRO);
end;

class function TProdutoServ.VerificarTipoProduto(const AIdProduto: Integer): Boolean;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT FORMA_AQUISICAO'+
  '   FROM produto ', nil);

  result := qProduto.GetAsString('FORMA_AQUISICAO').Equals(TConstantesFiscais.FORMA_AQUISICAO_PRODUZIDA);
end;

class function TProdutoServ.GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
const
  SQL_ICMS ='';
var
  qConsultaProdutoICMS: IFastQuery;
  qConsultaProdutoIPI: IFastQuery;
  qConsultaProdutoPISCofins: IFastQuery;
begin
  result := TProdutoFiscalProxy.Create;

  SetProdutoFiscalInformacoesProduto(result, AIdProduto);
  SetProdutoFiscalTributacao(result, AIdProduto, AFilial, AIdPessoa);
end;

class procedure TProdutoMovimentoServ.AmortizarProcessos(const AIdChaveProcesso: Integer);
begin
  TFastQuery.ExecutarScript(
    'UPDATE produto_movimento set bo_processo_amortizado = ''S'' where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class procedure TProdutoMovimentoServ.ExcluirRegistroDasMovimentacoesProduto(AIdChaveProcesso: Integer);
begin
  TFastQuery.ExecutarScript(
    'DELETE FROM produto_movimento WHERE id_chave_processo = :id',
      TArray<Variant>.Create(AIdChaveProcesso));
end;

class procedure TProdutoMovimentoServ.GerarMovimentacaoProduto(
  AProdutoEstoque : String);
var
  fqMovimentoEstoque : IFastQuery;
  produtoMovimento   : TProdutoMovimento;
  qtdeEstoqueAtual: Double;
  ultimaMovimentacao: TProdutoFilialProxy;
begin
  produtoMovimento := TJson.JsonToObject<TProdutoMovimento>(AProdutoEstoque);
  try
    ultimaMovimentacao := TProdutoFilialServ.GetProdutoFilial(
      produtoMovimento.FIdProduto, produtoMovimento.FIdFilial);

    fqMovimentoEstoque := TFastQuery.ModoDeAlteracao('select * from produto_movimento where 1 = 2', nil);
    fqMovimentoEstoque.Incluir;

    fqMovimentoEstoque.SetAsString('TIPO', produtoMovimento.FTipo);
    fqMovimentoEstoque.SetAsDateTime('DT_MOVIMENTO', StrToDate(produtoMovimento.FDtMovimento));
    fqMovimentoEstoque.SetAsFloat('QT_MOVIMENTO', produtoMovimento.FQtMovimento);
    fqMovimentoEstoque.SetAsString('DOCUMENTO_ORIGEM', produtoMovimento.FDocumentoOrigem);
    fqMovimentoEstoque.SetAsInteger('ID_PRODUTO', produtoMovimento.FIdProduto);
    fqMovimentoEstoque.SetAsInteger('ID_FILIAL', produtoMovimento.FIdFilial);
    fqMovimentoEstoque.SetAsFloat('ID_CHAVE_PROCESSO', produtoMovimento.FIdChaveProcesso);

    qtdeEstoqueAtual := ultimaMovimentacao.FQtEstoque;

    fqMovimentoEstoque.SetAsFloat('QT_ESTOQUE_ANTERIOR', qtdeEstoqueAtual);

    if produtoMovimento.FTipo = MOVIMENTO_ENTRADA then
      qtdeEstoqueAtual := qtdeEstoqueAtual + produtoMovimento.FQtMovimento
    else
      qtdeEstoqueAtual := qtdeEstoqueAtual - produtoMovimento.FQtMovimento;

    fqMovimentoEstoque.SetAsFloat('QT_ESTOQUE_ATUAL', qtdeEstoqueAtual);

    //fqMovimentoEstoque.SetAsFloat('QT_ESTOQUE_CONDICIONAL', produtoMovimento.FQtEstoqueCondicional);

    //Somente a entrada � quem deve gerar altera��o nos impostos
    if produtoMovimento.FTipo = MOVIMENTO_ENTRADA then
    begin
      //Ajuste n�o movimenta valores de custo
      if produtoMovimento.FDocumentoOrigem.Equals(DOCUMENTO_AJUSTE_ESTOQUE) then
      begin
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO', ultimaMovimentacao.FVlUltimoCusto);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_FRETE', ultimaMovimentacao.FVlUltimoFrete);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_SEGURO', ultimaMovimentacao.FVlUltimoSeguro);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_ICMS_ST', ultimaMovimentacao.FVlUltimoIcmsSt);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_IPI', ultimaMovimentacao.FVlUltimoIpi);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_DESCONTO', ultimaMovimentacao.FVlUltimoDesconto);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA', ultimaMovimentacao.FVlUltimaDespesaAcessoria);

        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO',
        ultimaMovimentacao.FVlUltimoCusto + ultimaMovimentacao.FVlUltimoFrete + ultimaMovimentacao.FVlUltimoSeguro +
        ultimaMovimentacao.FVlUltimoIcmsSt + ultimaMovimentacao.FVlUltimoIpi - ultimaMovimentacao.FVlUltimoDesconto +
        ultimaMovimentacao.FVlUltimaDespesaAcessoria);
        ultimaMovimentacao.FVlUltimoCustoImposto := fqMovimentoEstoque.GetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO');

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_OPERACIONAL', ultimaMovimentacao.FVlCustoOperacional);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL',
          ultimaMovimentacao.FVlUltimoCustoImposto  + ultimaMovimentacao.FVlCustoOperacional);
        ultimaMovimentacao.FVlCustoImpostoOperacional := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');

        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_FRETE', ultimaMovimentacao.FPercUltimoFrete);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_SEGURO', ultimaMovimentacao.FPercUltimoSeguro);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_ICMS_ST', ultimaMovimentacao.FPercUltimoIcmsSt);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_IPI', ultimaMovimentacao.FPercUltimoIpi);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_DESCONTO', ultimaMovimentacao.FPercUltimoDesconto);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA', ultimaMovimentacao.FPercUltimaDespesaAcessoria);
        fqMovimentoEstoque.SetAsFloat('PERC_CUSTO_OPERACIONAL', ultimaMovimentacao.FPercCustoOperacional);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO',ultimaMovimentacao.FVlUltimoCusto);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO', ultimaMovimentacao.FVlUltimoCustoImposto);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL', ultimaMovimentacao.FVlCustoImpostoOperacional);
      end
      else
      begin
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO', produtoMovimento.FVlUltimoCusto);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_FRETE', produtoMovimento.FVlUltimoFrete);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_SEGURO', produtoMovimento.FVlUltimoSeguro);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_ICMS_ST', produtoMovimento.FVlUltimoIcmsSt);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_IPI', produtoMovimento.FVlUltimoIpi);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_DESCONTO', produtoMovimento.FVlUltimoDesconto);
        fqMovimentoEstoque.SetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA', produtoMovimento.FVlUltimaDespesaAcessoria);

        fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO',
        produtoMovimento.FVlUltimoCusto + produtoMovimento.FVlUltimoFrete + produtoMovimento.FVlUltimoSeguro +
        produtoMovimento.FVlUltimoIcmsSt + produtoMovimento.FVlUltimoIpi - produtoMovimento.FVlUltimoDesconto +
        produtoMovimento.FVlUltimaDespesaAcessoria);
        produtoMovimento.FVlUltimoCustoImposto := fqMovimentoEstoque.GetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO');

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_OPERACIONAL', produtoMovimento.FVlCustoOperacional);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL',
          produtoMovimento.FVlUltimoCustoImposto  + produtoMovimento.FVlCustoOperacional);
        produtoMovimento.FVlCustoImpostoOperacional := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');

        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_FRETE', produtoMovimento.FPercUltimoFrete);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_SEGURO', produtoMovimento.FPercUltimoSeguro);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_ICMS_ST', produtoMovimento.FPercUltimoIcmsSt);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_IPI', produtoMovimento.FPercUltimoIpi);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_DESCONTO', produtoMovimento.FPercUltimoDesconto);
        fqMovimentoEstoque.SetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA', produtoMovimento.FPercUltimaDespesaAcessoria);
        fqMovimentoEstoque.SetAsFloat('PERC_CUSTO_OPERACIONAL', produtoMovimento.FPercCustoOperacional);
      end;

      if qtdeEstoqueAtual = 0 then //Custo Inicial
      begin
        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO',produtoMovimento.FVlUltimoCusto);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO',
          produtoMovimento.FVlUltimoCustoImposto);

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL',
          produtoMovimento.FVlCustoImpostoOperacional);
      end
      else if not produtoMovimento.FDocumentoOrigem.Equals(DOCUMENTO_AJUSTE_ESTOQUE) then //Ajuste nao altera custo
      begin
        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO',
          ((ultimaMovimentacao.FQtEstoque * ultimaMovimentacao.FVlCustoMedio)+
           (produtoMovimento.FQtMovimento * produtoMovimento.FVlUltimoCusto))/(qtdeEstoqueAtual)
        );

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO',
          ((ultimaMovimentacao.FQtEstoque * ultimaMovimentacao.FVlCustoMedioImposto)+
           (produtoMovimento.FQtMovimento * produtoMovimento.FVlUltimoCustoImposto))/(qtdeEstoqueAtual)
        );

        fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL',
          ((ultimaMovimentacao.FQtEstoque * ultimaMovimentacao.FVlCustoMedioImpostoOperacional)+
           (produtoMovimento.FQtMovimento * produtoMovimento.FVlCustoImpostoOperacional))/(qtdeEstoqueAtual)
        );
      end;

      produtoMovimento.FVlCustoMedio := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO');
      produtoMovimento.FVlCustoMedioImposto := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO');
      produtoMovimento.FVlCustoMedioImpostoOperacional := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL');
    end
    //Na sa�da deve-se repetir o ultimo custo
    else if (produtoMovimento.FTipo = MOVIMENTO_SAIDA) then
    begin
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO', ultimaMovimentacao.FVlUltimoCusto);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_FRETE', ultimaMovimentacao.FVlUltimoFrete);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_SEGURO', ultimaMovimentacao.FVlUltimoSeguro);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_ICMS_ST', ultimaMovimentacao.FVlUltimoIcmsSt);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_IPI', ultimaMovimentacao.FVlUltimoIpi);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_DESCONTO', ultimaMovimentacao.FVlUltimoDesconto);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA', ultimaMovimentacao.FVlUltimaDespesaAcessoria);
      fqMovimentoEstoque.SetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO',ultimaMovimentacao.FVlUltimoCustoImposto);
      fqMovimentoEstoque.SetAsFloat('VL_CUSTO_OPERACIONAL', ultimaMovimentacao.FVlCustoOperacional);
      fqMovimentoEstoque.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', ultimaMovimentacao.FVlCustoImpostoOperacional);

      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_FRETE', ultimaMovimentacao.FPercUltimoFrete);
      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_SEGURO', ultimaMovimentacao.FPercUltimoSeguro);
      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_ICMS_ST', ultimaMovimentacao.FPercUltimoIcmsSt);
      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_IPI', ultimaMovimentacao.FPercUltimoIpi);
      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMO_DESCONTO', ultimaMovimentacao.FPercUltimoDesconto);
      fqMovimentoEstoque.SetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA', ultimaMovimentacao.FPercUltimaDespesaAcessoria);
      fqMovimentoEstoque.SetAsFloat('PERC_CUSTO_OPERACIONAL', ultimaMovimentacao.FPercCustoOperacional);

      fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO', ultimaMovimentacao.FVlCustoMedio);
      fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO', ultimaMovimentacao.FVlCustoMedioImposto);
      fqMovimentoEstoque.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL', ultimaMovimentacao.FVlCustoMedioImposto);

      produtoMovimento.FVlCustoMedio := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO');
      produtoMovimento.FVlCustoMedioImposto := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO');
      produtoMovimento.FVlCustoMedioImpostoOperacional := fqMovimentoEstoque.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL');
    end;
    fqMovimentoEstoque.Salvar;
    fqMovimentoEstoque.Persistir;
  finally
    FreeAndNil(produtoMovimento);
  end;

  produtoMovimento := TJson.JsonToObject<TProdutoMovimento>(AProdutoEstoque);
  try
    produtoMovimento :=
      PegarUltimaMovimentacaoProduto(produtoMovimento.FIdProduto, produtoMovimento.FIdFilial);
    TProdutoFilialServ.GerarProdutoFilial(produtoMovimento);
  finally
    FreeAndNil(produtoMovimento);
  end;
end;

class function TProdutoServ.GetEstoqueAtual(AIdProduto, AIdFilial: Integer): Double;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT qt_estoque '+
  '   FROM produto_filial '+
  '  WHERE id_produto = :id_produto'+
  '    AND id_filial = :id_filial',
  TArray<Variant>.Create(AIdProduto, AIdFilial));

  result := qProduto.GetAsFloat('qt_estoque');
end;

class function TProdutoServ.GetIdRegraImposto(const AIdProduto: Integer): Integer;
const
  SQL_PESSOA_REGRA_IMPOSTO_FILTRO =
  ' SELECT id_regra_imposto'+
  '   FROM produto'+
  '  WHERE id = :id ';
var
  consultaRegrImposto: IFastQuery;
begin
  consultaRegrImposto := TFastQuery.ModoDeConsulta(SQL_PESSOA_REGRA_IMPOSTO_FILTRO,
    TArray<Variant>.Create(AIdProduto));

  result := consultaRegrImposto.GetAsInteger('id_regra_imposto');
end;

class function TProdutoServ.GetInformacoesFiltroImposto(const AIdProduto: Integer): TFastQuery;
const
  SQL_PESSOA_REGRA_IMPOSTO_FILTRO =
  ' SELECT forma_aquisicao'+
  '       ,origem_da_mercadoria'+
  '   FROM produto'+
  '  WHERE id = :id ';
begin
  result := TFastQuery.ModoDeConsulta(SQL_PESSOA_REGRA_IMPOSTO_FILTRO, TArray<Variant>.Create(AIdProduto));
end;

class function TProdutoServ.GetLitaProdutos(
  AIdsProdutos: String; AIdFilial: Integer): TList<TProdutoProxy>;
var
  listaDeCodigos: TStringList;
  i: Integer;
  produtoProxy: TProdutoProxy;
begin
  result := TList<TProdutoProxy>.Create;

  listaDeCodigos := TStringList.Create;
  try
    listaDeCodigos.DelimitedText := ',';
    listaDeCodigos.CommaText := AIdsProdutos;

    for i := 0 to Pred(listaDeCodigos.Count) do
    begin
      produtoProxy := TProdutoServ.GetProdutoFilial(StrtoIntDef(listaDeCodigos[i], 0), AIdFilial);
      if produtoProxy.FId > 0 then
      begin
        result.Add(produtoProxy);
      end;
    end;
  finally
    FreeAndNil(listaDeCodigos);
  end;
end;

class function TProdutoServ.GetProduto(AIdProduto: Integer): TFDJSONDataSets;
var qProduto: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qProduto := TFastQuery.ModoDeConsulta(
    'SELECT * FROM produto WHERE id = :id',
    TArray<Variant>.Create(AIdProduto));

  TFDJSONDataSetsWriter.ListAdd(result, (qProduto as TFastQuery));
end;

class function TProdutoServ.GetProdutoPeloCodigoBarra(
  ACodigoBarraProduto: String): TFDJSONDataSets;
var qProduto: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qProduto := TFastQuery.ModoDeConsulta(
    'SELECT * FROM produto WHERE codigo_barra = :codigo_barra',
    TArray<Variant>.Create(ACodigoBarraProduto));

  TFDJSONDataSetsWriter.ListAdd(result, (qProduto as TFastQuery));
end;

class function TProdutoServ.GetProdutoQuery(AIdProduto: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta(
    'SELECT * FROM produto WHERE id = :id',
    TArray<Variant>.Create(AIdProduto));
end;

class function TProdutoServ.GetProximoID: Integer;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT max(id)+1 as max'+
  '   FROM produto ', nil);

  result := qProduto.GetAsInteger('max');

  if result = 0 then
    result := 1;
end;

class function TProdutoServ.GetTodosGruposProduto: TFDJSONDataSets;
var qProduto: IFastQuery;
begin
  result := TFDJSONDataSets.Create;

  qProduto := TFastQuery.ModoDeConsulta(
    'SELECT * FROM grupo_produto',
    nil);

  TFDJSONDataSetsWriter.ListAdd(result, (qProduto as TFastQuery));
end;

class procedure TProdutoServ.RefazerEAN13TodosProdutos;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM produto');

  qProduto.First;
  while not qProduto.Eof do
  begin
    qProduto.Alterar;
    qProduto.SetAsString('CODIGO_BARRA', TProdutoServ.GerarCodigoBarraEAN13(qProduto.GetAsInteger('ID')));
    qProduto.Salvar;
    qProduto.Next;
  end;
  qProduto.Persistir;
end;

class procedure TProdutoServ.SetProdutoAgrupador(const AIdProduto: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto'+
    ' SET bo_produto_agrupado = :bo_produto_agrupado'+
    ',bo_produto_agrupador = :bo_produto_agrupador'+
    ',id_produto_agrupador = :id_produto_agrupador'+
    ' WHERE id = :id',
  TArray<Variant>.Create('N', 'S', 0, AIdProduto));
end;

class procedure TProdutoServ.SetProdutoAgrupado(const AIdProdutoAgrupador, AIdProduto: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto'+
    ' SET bo_produto_agrupado = :bo_produto_agrupado'+
    ',bo_produto_agrupador = :bo_produto_agrupador'+
    ',id_produto_agrupador = :id_produto_agrupador'+
    ' WHERE id = :id',
    TArray<Variant>.Create('S', 'N', AIdProdutoAgrupador, AIdProduto));
end;

class function TProdutoMovimentoServ.GetChaveProcessoMovimentacaoInicial(const AIdProduto: Integer): Integer;
var
  qConsulta: IFastQuery;
const
  SQL = ' SELECT id_chave_processo FROM produto_movimento'+
        ' WHERE id_produto = :id_produto and documento_origem = :documento_origem and bo_processo_amortizado = ''N''';
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL, TArray<Variant>.Create(
    AIdProduto, TProdutoFilialProxy.DOCUMENTO_ORIGEM_FORMACAO_PRECO_INICIAL));

  result := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
end;

class function TProdutoMovimentoServ.PegarUltimaMovimentacaoProduto(AIdProduto, AIdFilial: Integer;
  AConsiderarMovimentosEstornados: Boolean = true; AIdChaveProcesso: Integer = 0): TProdutoMovimento;
const
  SQL_CONSIDERA_ESTORNADOS =
    ' SELECT * FROM PRODUTO_MOVIMENTO WHERE id = '+
    ' (select max(id) from PRODUTO_MOVIMENTO where id_produto = :id_produto and id_filial = :id_filial)';
  SQL_NAO_CONSIDERA_ESTORNADOS =
    ' SELECT * FROM PRODUTO_MOVIMENTO WHERE id = '+
    ' (select max(id) from PRODUTO_MOVIMENTO where id_produto = :id_produto and id_filial = :id_filial'+
    '  and bo_processo_amortizado = ''N'' and id_chave_processo <> :id_chave_processo)';

var
   qConsulta: IFastQuery;
begin
  result := TProdutoMovimento.Create;

  if AConsiderarMovimentosEstornados then
  begin
    qConsulta := TFastQuery.ModoDeConsulta(SQL_CONSIDERA_ESTORNADOS,
      TArray<Variant>.Create(AIdProduto, AIdFilial));
  end
  else
  begin
    qConsulta := TFastQuery.ModoDeConsulta(SQL_NAO_CONSIDERA_ESTORNADOS,
      TArray<Variant>.Create(AIdProduto, AIdFilial, AIdChaveProcesso));
  end;

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FDtMovimento := qConsulta.GetAsString('DT_MOVIMENTO');
  result.FQtEstoqueAnterior := qConsulta.GetAsFloat('QT_ESTOQUE_ANTERIOR');
  result.FQtMovimento := qConsulta.GetAsFloat('QT_MOVIMENTO');
  result.FQtEstoqueAtual := qConsulta.GetAsFloat('QT_ESTOQUE_ATUAL');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FVlUltimoCusto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO');
  result.FVlCustoMedio := qConsulta.GetAsFloat('VL_CUSTO_MEDIO');
  //result.FQtEstoqueCondicional := qConsulta.GetAsFloat('QT_ESTOQUE_CONDICIONAL');
  result.FVlUltimoFrete := qConsulta.GetAsFloat('VL_ULTIMO_FRETE');
  result.FVlUltimoSeguro := qConsulta.GetAsFloat('VL_ULTIMO_SEGURO');
  result.FVlUltimoIcmsSt := qConsulta.GetAsFloat('VL_ULTIMO_ICMS_ST');
  result.FVlUltimoIpi := qConsulta.GetAsFloat('VL_ULTIMO_IPI');
  result.FVlUltimoDesconto := qConsulta.GetAsFloat('VL_ULTIMO_DESCONTO');
  result.FVlUltimaDespesaAcessoria := qConsulta.GetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA');
  result.FVlUltimoCustoImposto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO');
  result.FVlCustoMedioImposto := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO');
  result.FVlCustoOperacional := qConsulta.GetAsFloat('VL_CUSTO_OPERACIONAL');
  result.FVlCustoImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');
  result.FVlCustoMedioImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL');
  result.FPercUltimoFrete := qConsulta.GetAsFloat('PERC_ULTIMO_FRETE');
  result.FPercUltimoSeguro := qConsulta.GetAsFloat('PERC_ULTIMO_SEGURO');
  result.FPercUltimoIcmsSt := qConsulta.GetAsFloat('PERC_ULTIMO_ICMS_ST');
  result.FPercUltimoIpi := qConsulta.GetAsFloat('PERC_ULTIMO_IPI');
  result.FPercUltimoDesconto := qConsulta.GetAsFloat('PERC_ULTIMO_DESCONTO');
  result.FPercUltimaDespesaAcessoria := qConsulta.GetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA');
  result.FPercCustoOperacional := qConsulta.GetAsFloat('PERC_CUSTO_OPERACIONAL');
end;

class function TProdutoMovimentoServ.RemoverMovimentacaoProduto(AIdChaveProcesso: Integer): Boolean;
var
  qProdutoMovimento: IFastQuery;
  movimentacaoDeOrigem: TProdutoMovimento;
  ultimaMovimentacaoValida: TProdutoMovimento;
begin
  try
    Result := True;

    qProdutoMovimento := TFastQuery.ModoDeConsulta(
      'SELECT * FROM produto_movimento WHERE id_chave_processo = :id and bo_processo_amortizado = ''N''',
      TArray<Variant>.Create(AIdChaveProcesso));

    try
      qProdutoMovimento.Primeiro;
      while not qProdutoMovimento.Eof do
      begin
        movimentacaoDeOrigem := TProdutoMovimentoServ.GetProdutoMovimento(
          qProdutoMovimento.GetAsInteger('ID'));
        try
          with movimentacaoDeOrigem do
          begin
            if FTipo = MOVIMENTO_ENTRADA then
              FTipo := MOVIMENTO_SAIDA
            else
              FTipo := MOVIMENTO_ENTRADA;

            FDtMovimento := DateToStr(Date);

            ultimaMovimentacaoValida := TProdutoMovimento.Create;
            try
              ultimaMovimentacaoValida := TProdutoMovimentoServ.PegarUltimaMovimentacaoProduto(
                FIdProduto, FIdFilial, False, FIdChaveProcesso);

              FVlUltimoCusto     := ultimaMovimentacaoValida.FVlUltimoCusto;
              FVlUltimoFrete     := ultimaMovimentacaoValida.FVlUltimoFrete;
              FVlUltimoSeguro := ultimaMovimentacaoValida.FVlUltimoSeguro;
              FVlUltimoIcmsSt := ultimaMovimentacaoValida.FVlUltimoIcmsSt;
              FVlUltimoIpi := ultimaMovimentacaoValida.FVlUltimoIpi;

              FVlUltimoDesconto := ultimaMovimentacaoValida.FVlUltimoDesconto;
              FVlUltimaDespesaAcessoria := ultimaMovimentacaoValida.FVlUltimaDespesaAcessoria;

              FVlCustoOperacional := ultimaMovimentacaoValida.FVlCustoOperacional;

              FPercUltimoFrete := ultimaMovimentacaoValida.FPercUltimoFrete;
              FPercUltimoSeguro := ultimaMovimentacaoValida.FPercUltimoSeguro;
              FPercUltimoIcmsSt := ultimaMovimentacaoValida.FPercUltimoIcmsSt;
              FPercUltimoIpi := ultimaMovimentacaoValida.FPercUltimoIpi;
              FPercUltimoDesconto := ultimaMovimentacaoValida.FPercUltimoDesconto;
              FPercUltimaDespesaAcessoria :=ultimaMovimentacaoValida.FPercUltimaDespesaAcessoria;
              FPercCustoOperacional := ultimaMovimentacaoValida.FPercCustoOperacional;
            finally
              FreeAndNil(ultimaMovimentacaoValida);
            end;
          end;

          TProdutoMovimentoServ.GerarMovimentacaoProduto(TJson.ObjectToJsonString(movimentacaoDeOrigem));
        finally
          FreeAndNil(movimentacaoDeOrigem);
        end;
        qProdutoMovimento.Proximo;
      end;
    finally
      TProdutoMovimentoServ.AmortizarProcessos(AIdChaveProcesso);
    end;
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

class procedure TProdutoServ.AlterarCodigoBarraProduto(const AIdProduto: Integer; const ACodigoBarra: String);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto'+
    ' SET codigo_barra = :codigo_barra'+
    ' WHERE id = :id',
    TArray<Variant>.Create(ACodigoBarra, AIdProduto));
end;

class procedure TProdutoServ.AlterarTamanhoCorProduto(const AIdProduto, AIdCor, AIdTamanho: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto'+
    ' SET id_cor = :id_cor'+
    ',id_tamanho = :id_tamanho'+
    ' WHERE id = :id',
    TArray<Variant>.Create(AIdCor, AIdTamanho, AIdProduto));
end;

class procedure TProdutoServ.AtualizarCodigoBarra(const ACodigoBarra: String; const AIdProduto: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto'+
    ' SET codigo_barra = :codigo_barra'+
    ' WHERE id = :id',
  TArray<Variant>.Create(ACodigoBarra, AIdProduto));
end;

class function TProdutoServ.BuscarIdProdutoAgrupadoPorCorTamanho(const AIdProdutoAgrupador, AIdCor,
  AIdTamanho: Integer): Integer;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT id'+
  '   FROM produto'+
  '  where id_cor = :id_cor'+
  '    and id_tamanho = :id_tamanho'+
  '    and id_produto_agrupador = :id', TArray<Variant>.Create(AIdCor, AIdTamanho, AIdProdutoAgrupador));

  if not qProduto.IsEmpty then
    result := qProduto.GetAsInteger('id')
  else
    result := 0;
end;

class function TProdutoFilialServ.BuscarValorCusto(const AIdProduto, AIdFilial: Integer): Double;
const
  SQL = 'SELECT vl_custo_imposto_operacional FROM PRODUTO_FILIAL'+
    ' WHERE id_produto = :id_produto and id_filial = :id_filial';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdProduto, AIdFilial));

  result := qConsulta.GetAsFloat('vl_custo_imposto_operacional');
end;

class function TProdutoServ.CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
      const AIdProduto: Integer): Integer;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT id'+
  '   FROM produto'+
  '  where codigo_barra = :codigo_barra'+
  '    and id <> :id', TArray<Variant>.Create(ACodigoBarra, AIdProduto));

  if not qProduto.IsEmpty then
    result := qProduto.GetAsInteger('id')
  else
    result := 0;
end;

class function TProdutoServ.CodigoEANValido(const ACodigoBarra: String): Boolean;
begin
  result := EAN13Valido(ACodigoBarra);
end;

class function TProdutoServ.CriarFilialParaProduto(AIdProduto, AIdFilial: Integer): Boolean;
begin
  result := true;

  TFastQuery.ExecutarScript(
  ' INSERT INTO produto_filial '+
  '(QT_ESTOQUE'+
  ',LOCALIZACAO'+
  ',QT_ESTOQUE_MINIMO'+
  ',QT_ESTOQUE_MAXIMO'+
  ',ID_PRODUTO'+
  ',ID_FILIAL'+
  ',VL_CUSTO_MEDIO'+
  ',VL_ULTIMO_CUSTO) '+
  ' VALUES('+
  ':QT_ESTOQUE'+
  ',:LOCALIZACAO'+
  ',:QT_ESTOQUE_MINIMO'+
  ',:QT_ESTOQUE_MAXIMO'+
  ',:ID_PRODUTO'+
  ',:ID_FILIAL'+
  ',:VL_CUSTO_MEDIO'+
  ',:VL_ULTIMO_CUSTO)',
  TArray<Variant>.Create(
  0, '', 0, 0, AIdProduto, AIdFilial, 0, 0));
end;

class function TProdutoServ.DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
var
  idProduto: Integer;
begin
  idProduto := 0;

  try
    idProduto := DuplicarRegistroProduto(AIdProdutoOrigem);
    //DuplicarRegistroProdutoFornecedor(AIdProdutoOrigem, idProduto);
    DuplicarRegistroProdutoMontadora(AIdProdutoOrigem, idProduto);
    DuplicarRegistroProdutoModeloMontadora(AIdProdutoOrigem, idProduto);
    DuplicarRegistroProdutoTributacaoPorUnidade(AIdProdutoOrigem, idProduto);
    result := idProduto;
  except
    on e : Exception do
    begin
      if idProduto < 0 then
      begin
        result := 0;
      end;

      raise Exception.Create('Erro ao duplicar Produto.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TProdutoServ.DuplicarRegistroProduto(const AIdProdutoOrigem: Integer): Integer;
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: Array [0..3] of string =
  ('id',
   'bo_produto_agrupador',
   'bo_produto_agrupado',
   'id_produto_agrupador');

  const CAMPOS_MODIFICAR: String = 'dh_cadastro';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM PRODUTO WHERE ID = :ID',
    TArray<Variant>.Create(AIdProdutoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('PRODUTO');

  //Novo Registro em outra conta corrente
  TDatasetUtilsServ.DuplicarRegistro(
    (queryOrigem as TFastQuery), //Origem
    (queryDestino as TFastQuery), //Destino
    CAMPOS_NAO_COPIAR, //No Copy
    [CAMPOS_MODIFICAR], //Campos para mudar
    TArray<Variant>.Create(now) //Valores para Mudar
  );

  queryDestino.Persistir;

  TProdutoServ.AtualizarCodigoBarra(TProdutoServ.GerarCodigoBarraEAN13(queryDestino.GetAsInteger('ID')),
    queryDestino.GetAsInteger('ID'));

  result := queryDestino.GetAsInteger('ID');
end;

class procedure TProdutoServ.DuplicarRegistroProdutoFornecedor(const AIdProdutoOrigem, AIdProdutoDestino: Integer);
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';

  const CAMPOS_MODIFICAR: String = 'id_produto';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM PRODUTO_FORNECEDOR WHERE ID_PRODUTO = :ID',
    TArray<Variant>.Create(AIdProdutoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('PRODUTO_FORNECEDOR');

  queryOrigem.First;
  while not queryOrigem.Eof do
  begin
    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      [CAMPOS_NAO_COPIAR], //No Copy
      [CAMPOS_MODIFICAR], //Campos para mudar
      TArray<Variant>.Create(AIdProdutoDestino) //Valores para Mudar
    );

    queryOrigem.Next;
  end;

  queryDestino.Persistir;
end;

class procedure TProdutoServ.DuplicarRegistroProdutoModeloMontadora(
  const AIdProdutoOrigem, AIdProdutoDestino: Integer);
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';
  const CAMPOS_MODIFICAR: String = 'id_produto';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM PRODUTO_MODELO_MONTADORA WHERE ID_PRODUTO = :ID',
    TArray<Variant>.Create(AIdProdutoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('PRODUTO_MODELO_MONTADORA');

  queryOrigem.First;
  while not queryOrigem.Eof do
  begin
    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      [CAMPOS_NAO_COPIAR], //No Copy
      [CAMPOS_MODIFICAR], //Campos para mudar
      TArray<Variant>.Create(AIdProdutoDestino) //Valores para Mudar
    );

    queryOrigem.Next;
  end;

  queryDestino.Persistir;
end;

class procedure TProdutoServ.DuplicarRegistroProdutoMontadora(
  const AIdProdutoOrigem, AIdProdutoDestino: Integer);
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';
  const CAMPOS_MODIFICAR: String = 'id_produto';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM PRODUTO_MONTADORA WHERE ID_PRODUTO = :ID',
    TArray<Variant>.Create(AIdProdutoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('PRODUTO_MONTADORA');

  queryOrigem.First;
  while not queryOrigem.Eof do
  begin
    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      [CAMPOS_NAO_COPIAR], //No Copy
      [CAMPOS_MODIFICAR], //Campos para mudar
      TArray<Variant>.Create(AIdProdutoDestino) //Valores para Mudar
    );

    queryOrigem.Next;
  end;

  queryDestino.Persistir;
end;

class procedure TProdutoServ.DuplicarRegistroProdutoTributacaoPorUnidade(
  const AIdProdutoOrigem, AIdProdutoDestino: Integer);
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: String = 'id';
  const CAMPOS_MODIFICAR: String = 'id_produto';
begin
  queryOrigem := TFastQuery.ModoDeConsulta(
    'SELECT * FROM PRODUTO_TRIBUTACAO_POR_UNIDADE WHERE ID_PRODUTO = :ID',
    TArray<Variant>.Create(AIdProdutoOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('PRODUTO_TRIBUTACAO_POR_UNIDADE');

  queryOrigem.First;
  while not queryOrigem.Eof do
  begin
    //Novo Registro em outra conta corrente
    TDatasetUtilsServ.DuplicarRegistro(
      (queryOrigem as TFastQuery), //Origem
      (queryDestino as TFastQuery), //Destino
      [CAMPOS_NAO_COPIAR], //No Copy
      [CAMPOS_MODIFICAR], //Campos para mudar
      TArray<Variant>.Create(AIdProdutoDestino) //Valores para Mudar
    );

    queryOrigem.Next;
  end;

  queryDestino.Persistir;
end;

class function TProdutoServ.ExisteFilialParaProduto(AIdProduto, AIdFilial: Integer): Boolean;
var qProduto: IFastQuery;
begin
  qProduto := TFastQuery.ModoDeConsulta(
  ' SELECT qt_estoque '+
  '   FROM produto_filial '+
  '  WHERE id_produto = :id_produto'+
  '    AND id_filial = :id_filial',
  TArray<Variant>.Create(AIdProduto, AIdFilial));

  result := not qProduto.IsEmpty;
end;

class function TProdutoServ.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(const AIdProduto,
  AIdFilial: Integer): Boolean;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
  ' SELECT 1 '+
  '   FROM produto_movimento '+
  '  WHERE id_produto = :id_produto'+
  '    AND id_filial = :id_filial'+
  '    AND documento_origem <> :origem_inicial'+
  '    AND bo_processo_amortizado = ''N'' limit 1',
  TArray<Variant>.Create(AIdProduto, AIdFilial, TProdutoFilialProxy.DOCUMENTO_ORIGEM_FORMACAO_PRECO_INICIAL));

  result := not qConsulta.IsEmpty;
end;

class function TProdutoServ.GetProdutos(
  AIdsProdutos: string): TFDJSONDataSets;
var
  fqProdutos : IFastQuery;
  SQL : string;
begin

  SQL := 'SELECT * FROM produto p WHERE p.id IN (' + AIdsProdutos + ')';

  Result := TFDJSONDataSets.Create;

  fqProdutos := TFastQuery.ModoDeConsulta(SQL);

  TFDJSONDataSetsWriter.ListAdd(Result, (fqProdutos as TFastQuery));

end;

class function TProdutoServ.GetProdutosCompleto(
  AIdsProdutos: string): TFDJSONDataSets;
var
  fqProdutos : IFastQuery;
  SQL : string;
begin

  SQL :=

  ' SELECT p.* '+
  '       ,p.id AS ID_PRODUTO '+
  '       ,p.descricao As JOIN_DESCRICAO_PRODUTO                        ' +
  '       ,gp.descricao AS JOIN_DESCRICAO_GRUPO_PRODUTO                 ' +
  '       ,sgp.descricao AS JOIN_DESCRICAO_SUB_GRUPO_PRODUTO            ' +
  '       ,m.descricao AS JOIN_DESCRICAO_MARCA                          ' +
  '       ,c.descricao AS JOIN_DESCRICAO_CATEGORIA                      ' +
  '       ,sb.descricao AS JOIN_DESCRICAO_SUB_CATEGORIA                 ' +
  '       ,l.descricao AS JOIN_DESCRICAO_LINHA                          ' +
  '       ,m.descricao AS JOIN_DESCRICAO_MODELO                         ' +
  '       ,un.descricao AS JOIN_DESCRICAO_UNIDADE_ESTOQUE               ' +
  '  FROM produto p                                                     ' +
  ' LEFT JOIN grupo_produto gp ON p.id_grupo_produto = gp.id           ' +
  ' LEFT JOIN sub_grupo_produto sgp ON p.id_sub_grupo_produto = sgp.id ' +
  ' LEFT JOIN marca m ON p.id_marca = m.id                             ' +
  ' LEFT JOIN categoria c ON p.id_categoria = c.id                     ' +
  ' LEFT JOIN sub_categoria sb ON p.id_sub_categoria = sb.id           ' +
  ' LEFT JOIN linha l ON p.id_linha = l.id                             ' +
  ' LEFT JOIN modelo md ON p.id_modelo = md.id                         ' +
  ' LEFT JOIN unidade_estoque un ON p.id_categoria = un.id             ' +
  ' WHERE p.id IN (' + AIdsProdutos + ')';

  Result := TFDJSONDataSets.Create;

  fqProdutos := TFastQuery.ModoDeConsulta(SQL);

  TFDJSONDataSetsWriter.ListAdd(Result, (fqProdutos as TFastQuery));
end;

{ TProdutoFilialServ }

class function TProdutoFilialServ.GetProdutoFilial(
    const AIdProduto, AIdFilial: Integer): TProdutoFilialProxy;
const
  SQL = 'SELECT * FROM PRODUTO_FILIAL'+
    ' WHERE id_produto = :id_produto and id_filial = :id_filial';
var
   qConsulta: IFastQuery;
begin
  result := TProdutoFilialProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdProduto, AIdFilial));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FQtEstoque := qConsulta.GetAsFloat('QT_ESTOQUE');
  result.FLocalizacao := qConsulta.GetAsString('LOCALIZACAO');
  result.FQtEstoqueMinimo := qConsulta.GetAsFloat('QT_ESTOQUE_MINIMO');
  result.FQtEstoqueMaximo := qConsulta.GetAsFloat('QT_ESTOQUE_MAXIMO');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FQtEstoqueCondicional := qConsulta.GetAsFloat('QT_ESTOQUE_CONDICIONAL');
  result.FVlCustoMedio := qConsulta.GetAsFloat('VL_CUSTO_MEDIO');
  result.FVlUltimoCusto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO');
  result.FVlUltimoFrete := qConsulta.GetAsFloat('VL_ULTIMO_FRETE');
  result.FVlUltimoSeguro := qConsulta.GetAsFloat('VL_ULTIMO_SEGURO');
  result.FVlUltimoIcmsSt := qConsulta.GetAsFloat('VL_ULTIMO_ICMS_ST');
  result.FVlUltimoIpi := qConsulta.GetAsFloat('VL_ULTIMO_IPI');
  result.FVlUltimoDesconto := qConsulta.GetAsFloat('VL_ULTIMO_DESCONTO');
  result.FVlUltimaDespesaAcessoria := qConsulta.GetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA');
  result.FVlUltimoCustoImposto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO');
  result.FVlCustoMedioImposto := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO');
  result.FVlCustoMedioImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL');
  result.FVlCustoOperacional := qConsulta.GetAsFloat('VL_CUSTO_OPERACIONAL');
  result.FVlCustoImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');
  result.FPercUltimoFrete := qConsulta.GetAsFloat('PERC_ULTIMO_FRETE');
  result.FPercUltimoSeguro := qConsulta.GetAsFloat('PERC_ULTIMO_SEGURO');
  result.FPercUltimoIcmsSt := qConsulta.GetAsFloat('PERC_ULTIMO_ICMS_ST');
  result.FPercUltimoIpi := qConsulta.GetAsFloat('PERC_ULTIMO_IPI');
  result.FPercUltimoDesconto := qConsulta.GetAsFloat('PERC_ULTIMO_DESCONTO');
  result.FPercUltimaDespesaAcessoria := qConsulta.GetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA');
  result.FPercCustoOperacional := qConsulta.GetAsFloat('PERC_CUSTO_OPERACIONAL');
end;

class function TProdutoFilialServ.GerarProdutoFilial(const AProdutoMovimento: TProdutoMovimento): Integer;
var
  qEntidade: IFastQuery;
  qtdeMovimentoEstoqueFilial: Double;
begin
  {Validar se existe filial para o produto}
  if not TProdutoServ.ExisteFilialParaProduto(AProdutoMovimento.FIdProduto, AProdutoMovimento.FIdFilial) then
    TProdutoServ.CriarFilialParaProduto(AProdutoMovimento.FIdProduto, AProdutoMovimento.FIdFilial);

  try
    qEntidade := TFastQuery.ModoDeAlteracao(
      'SELECT * FROM PRODUTO_FILIAL WHERE ID_PRODUTO = :ID_PRODUTO AND ID_FILIAL = :ID_FILIAL',
      TArray<Variant>.Create(AProdutoMovimento.FIdProduto, AProdutoMovimento.FIdFilial));

    if qEntidade.IsEmpty then
    begin
      qEntidade.Incluir;
    end
    else
    begin
      qEntidade.Alterar;
    end;

    qEntidade.SetAsFloat('QT_ESTOQUE', AProdutoMovimento.FQtEstoqueAtual);
    qEntidade.SetAsString('LOCALIZACAO', ''); ///AINDA N�O ESTAMOS TRABALHANDO COM LOCALIZACAO
    qEntidade.SetAsFloat('QT_ESTOQUE_MINIMO', 0);
    qEntidade.SetAsFloat('QT_ESTOQUE_MAXIMO', 0);

    if AProdutoMovimento.FIdProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_PRODUTO', AProdutoMovimento.FIdProduto);
    end;

    if AProdutoMovimento.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AProdutoMovimento.FIdFilial);
    end;

    //qEntidade.SetAsFloat('QT_ESTOQUE_CONDICIONAL', AProdutoMovimento.FQtEstoqueCondicional);
    qEntidade.SetAsFloat('VL_ULTIMO_CUSTO', AProdutoMovimento.FVlUltimoCusto);
    qEntidade.SetAsFloat('VL_ULTIMO_FRETE', AProdutoMovimento.FVlUltimoFrete);
    qEntidade.SetAsFloat('VL_ULTIMO_SEGURO', AProdutoMovimento.FVlUltimoSeguro);
    qEntidade.SetAsFloat('VL_ULTIMO_ICMS_ST', AProdutoMovimento.FVlUltimoIcmsSt);
    qEntidade.SetAsFloat('VL_ULTIMO_IPI', AProdutoMovimento.FVlUltimoIpi);
    qEntidade.SetAsFloat('VL_ULTIMO_DESCONTO', AProdutoMovimento.FVlUltimoDesconto);
    qEntidade.SetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA', AProdutoMovimento.FVlUltimaDespesaAcessoria);
    qEntidade.SetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO', AProdutoMovimento.FVlUltimoCustoImposto);
    qEntidade.SetAsFloat('VL_CUSTO_OPERACIONAL', AProdutoMovimento.FVlCustoOperacional);
    qEntidade.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', AProdutoMovimento.FVlCustoImpostoOperacional);
    qEntidade.SetAsFloat('PERC_ULTIMO_FRETE', AProdutoMovimento.FPercUltimoFrete);
    qEntidade.SetAsFloat('PERC_ULTIMO_SEGURO', AProdutoMovimento.FPercUltimoSeguro);
    qEntidade.SetAsFloat('PERC_ULTIMO_ICMS_ST', AProdutoMovimento.FPercUltimoIcmsSt);
    qEntidade.SetAsFloat('PERC_ULTIMO_IPI', AProdutoMovimento.FPercUltimoIpi);
    qEntidade.SetAsFloat('PERC_ULTIMO_DESCONTO', AProdutoMovimento.FPercUltimoDesconto);
    qEntidade.SetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA', AProdutoMovimento.FPercUltimaDespesaAcessoria);
    qEntidade.SetAsFloat('PERC_CUSTO_OPERACIONAL', AProdutoMovimento.FPercCustoOperacional);

    qEntidade.SetAsFloat('VL_CUSTO_MEDIO', AProdutoMovimento.FVlCustoMedio);
    qEntidade.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO', AProdutoMovimento.FVlCustoMedioImposto);
    qEntidade.SetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL', AProdutoMovimento.FVlCustoMedioImpostoOperacional);

    qEntidade.Salvar;
    qEntidade.Persistir;

    AProdutoMovimento.FID := qEntidade.GetAsInteger('ID');
    result :=     AProdutoMovimento.FID;

    TTabelaPrecoServ.AtualizarValorCustoImpostoOperacional(AProdutoMovimento.FIdProduto,
      AProdutoMovimento.FVlCustoMedioImpostoOperacional);
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Produto Filial.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TProdutoMovimentoServ.GetProdutoMovimento(
    const AIdProdutoMovimento: Integer): TProdutoMovimento;
const
  SQL = 'SELECT * FROM produto_movimento WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TProdutoMovimento.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdProdutoMovimento));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FDtMovimento := qConsulta.GetAsString('DT_MOVIMENTO');
  result.FQtEstoqueAnterior := qConsulta.GetAsFloat('QT_ESTOQUE_ANTERIOR');
  result.FQtMovimento := qConsulta.GetAsFloat('QT_MOVIMENTO');
  result.FQtEstoqueAtual := qConsulta.GetAsFloat('QT_ESTOQUE_ATUAL');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FVlUltimoCusto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO');
  //result.FQtEstoqueCondicional := qConsulta.GetAsFloat('QT_ESTOQUE_CONDICIONAL');
  result.FVlUltimoFrete := qConsulta.GetAsFloat('VL_ULTIMO_FRETE');
  result.FVlUltimoSeguro := qConsulta.GetAsFloat('VL_ULTIMO_SEGURO');
  result.FVlUltimoIcmsSt := qConsulta.GetAsFloat('VL_ULTIMO_ICMS_ST');
  result.FVlUltimoIpi := qConsulta.GetAsFloat('VL_ULTIMO_IPI');
  result.FVlUltimoDesconto := qConsulta.GetAsFloat('VL_ULTIMO_DESCONTO');
  result.FVlUltimaDespesaAcessoria := qConsulta.GetAsFloat('VL_ULTIMA_DESPESA_ACESSORIA');
  result.FVlUltimoCustoImposto := qConsulta.GetAsFloat('VL_ULTIMO_CUSTO_IMPOSTO');
  result.FVlCustoOperacional := qConsulta.GetAsFloat('VL_CUSTO_OPERACIONAL');
  result.FVlCustoImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL');
  result.FPercUltimoFrete := qConsulta.GetAsFloat('PERC_ULTIMO_FRETE');
  result.FPercUltimoSeguro := qConsulta.GetAsFloat('PERC_ULTIMO_SEGURO');
  result.FPercUltimoIcmsSt := qConsulta.GetAsFloat('PERC_ULTIMO_ICMS_ST');
  result.FPercUltimoIpi := qConsulta.GetAsFloat('PERC_ULTIMO_IPI');
  result.FPercUltimoDesconto := qConsulta.GetAsFloat('PERC_ULTIMO_DESCONTO');
  result.FPercUltimaDespesaAcessoria := qConsulta.GetAsFloat('PERC_ULTIMA_DESPESA_ACESSORIA');
  result.FPercCustoOperacional := qConsulta.GetAsFloat('PERC_CUSTO_OPERACIONAL');
  result.FVlCustoMedio := qConsulta.GetAsFloat('VL_CUSTO_MEDIO');
  result.FVlCustoMedioImposto := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO');
  result.FVlCustoMedioImpostoOperacional := qConsulta.GetAsFloat('VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL');
end;

class function TProdutoFilialServ.AtualizarReservaDeEstoque(AIdProduto, AIdFilial: Integer;
  AQuantidadeEstoqueAtual: Double; APosicaoDestino: TPosicoesEstoque): Boolean;
var
  campoReservaDeEstoque: String;
begin
  case APosicaoDestino of
    condicional:
    begin
      campoReservaDeEstoque := 'QT_ESTOQUE_CONDICIONAL';
    end;
  end;

  TFastQuery.ExecutarScript(
    ' UPDATE produto_filial SET '+campoReservaDeEstoque+' = :quantidadeEstoque'+
    ' WHERE id_filial = :id_filial and id_produto = :id_produto',
    TArray<Variant>.Create(AQuantidadeEstoqueAtual, AIdFilial, AIdProduto));
end;

class function TProdutoServ.GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
const CODIGO_PAIS: String = '789';
      CODIGO_FILIAL: String = '001';
var codigoProduto: String;

  Function EAN13(const CodigoBarras : String) : String;
  var nX    : Integer;
     nPeso  : Integer;
     nSoma  : Double;
     nMaior  : Double;
     nDigito : Integer;
  Begin
     nPeso := 3;
     nSoma := 0;

     for nX := 12 downto 1 do
     begin
        nSoma := nSoma + StrToInt( CodigoBarras[ nX ] ) * nPeso;
        If nPeso = 3 Then
           nPeso := 1
        Else
           nPeso := 3;
     End;
     nMaior      := ( ( Trunc( nSoma / 10 ) + 1 ) * 10 );
     nDigito  := Trunc( nMaior ) - Trunc( nSoma );
     If nDigito = 10 Then
        nDigito := 0;
     Result := CodigoBarras + IntToStr( nDigito );
  End;

begin
  codigoProduto := FormatFloat('000000', ACodigoProduto);
  result := CODIGO_PAIS + CODIGO_FILIAL + codigoProduto;
  result := EAN13(result);
  //http://www.codigodebarrasean.com/calculadora_do_digito_verificador.php
end;

end.
