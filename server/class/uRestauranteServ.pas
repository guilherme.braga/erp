unit uRestauranteServ;

interface

uses FireDAC.Comp.Client, SysUtils;

type TRestauranteServ = class
  class procedure FecharCaixa(AQuery: TFDQuery; const AIdCaixaMovimento: Integer);
  class procedure SetCaixa(AQuery: TFDQuery);
  class function checkUsuario(AQuery: TFDQuery; const AUsername,
    APassword: string): Integer;
  class procedure imprimirCaixa(AQuery: TFDQuery; const id_caixa_movimento: Integer);
  class procedure trocoComanda(AQuery: TFDQuery; const vr_troco: Real;
    const id_comanda: Integer);
end;

implementation

{ TRestauranteServ }

class procedure TRestauranteServ.FecharCaixa(AQuery: TFDQuery; const AIdCaixaMovimento: Integer);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    ' UPDATE '+
    '   tab_caixa_movimento'+
    ' SET '+
    '   status_impressao = "S",'+
    '   caixa_movimento_impressao = "N",'+
    '   dt_fechamento = :p1,'+
    '   hr_fechamento = :p2'+
    ' WHERE'+
    '   cod_caixa_movimento = :p3');
  AQuery.Params.ParamByName('p1').AsDateTime := Date;
  AQuery.Params.ParamByName('p2').AsDateTime := Time;
  AQuery.Params.ParamByName('p3').AsInteger := AIdCaixaMovimento;
  AQuery.ExecSQL;
end;

class procedure TRestauranteServ.SetCaixa(AQuery: TFDQuery);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
      ' select u.id as id_usuario'+
      '    ,c.id as id_caixa'+
      '    ,m.id as id_caixa_movimento'+
      '    ,u.usuario'+
      '    ,m.dt_fechamento'+
      '    ,m.dt_abertura'+
      '    ,m.hr_fechamento'+
      '    ,m.hr_abertura'+
      '    ,m.saldo_inicial'+
      '    ,c.descricao'+
      '      from caixa c'+
      ' left join caixa_movimento m'+
      '        on c.id = m.id_caixa and m.dt_fechamento is null'+
      ' left join pessoa_usuario u'+
      '        on u.id = m.id_pessoa_usuario'+
      '  order by c.descricao');
  AQuery.Open;
end;

class function TRestauranteServ.checkUsuario(AQuery: TFDQuery; const AUsername, APassword: string): Integer;
var qAuxiliar: TFDMemTable;
begin
  result := 0;

  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(
    'SELECT id AS id_funcionario, usuario as chave, senha FROM pessoa_usuario WHERE usuario = :usuario and senha = :senha');
  AQuery.Params.ParamByName('usuario').AsString := AUsername;
  AQuery.Params.ParamByName('usuario').AsString := APassword;
  AQuery.Open;

  result := qAuxiliar.FieldByName('id_funcionario').AsInteger;
end;

class procedure TRestauranteServ.imprimirCaixa(AQuery: TFDQuery; const id_caixa_movimento: Integer);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('update tab_caixa_movimento set status_impressao = ''S''');
  AQuery.SQL.Add('caixa_movimento_impressao = ''N'' where id_caixa_movimento = :id_caixa_movimento');
  AQuery.Params.ParamByName('id_caixa_movimento').AsInteger := id_caixa_movimento;
  AQuery.ExecSQL;
end;

class procedure TRestauranteServ.trocoComanda(AQuery: TFDQuery; const vr_troco: Real; const id_comanda: Integer);
begin
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('UPDATE tab_comanda SET vr_troco_cliente = vr_troco_cliente WHERE id_comanda = :id_comanda');
  AQuery.Params.ParamByName('vr_troco_cliente').AsFloat := vr_troco;
  AQuery.Params.ParamByName('id_comanda').AsInteger := id_comanda;
  AQuery.ExecSQL;
end;


end.
