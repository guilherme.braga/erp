unit uChaveProcessoServ;

interface

type TChaveProcessoServ = class
  class function NovaChaveProcesso(AOrigem: string; AIdOrigem: Integer): Integer;
  class procedure AtualizarChaveProcesso(AIdChaveProcesso: Integer;
    AOrigem: String; AIdOrigem: Integer);

end;

implementation

{ TChaveProcessoServ }

uses uFactoryQuery;

class procedure TChaveProcessoServ.AtualizarChaveProcesso(
  AIdChaveProcesso: Integer; AOrigem: String;
  AIdOrigem: Integer);
begin
  TFastQuery.ExecutarScriptIndependente(
  'UPDATE CHAVE_PROCESSO SET ORIGEM = :ORIGEM, ID_ORIGEM = :ID_ORIGEM WHERE ID = :ID',
  TArray<Variant>.Create(AOrigem, AIdOrigem, AIdChaveProcesso));
end;

class function TChaveProcessoServ.NovaChaveProcesso(AOrigem: string; AIdOrigem: Integer): Integer;
var qChaveProcesso: IFastQuery;
begin

  TFastQuery.ExecutarScriptIndependente(
     'INSERT INTO CHAVE_PROCESSO (ORIGEM, ID_ORIGEM) VALUES (:AOrigem, :AIdOrigem)',
      TArray<Variant>.Create(AOrigem, AIdOrigem));

  qChaveProcesso := TFastQuery.ModoDeConsulta(
    'SELECT MAX(ID) as ID_CHAVE_PROCESSO FROM chave_processo');

  result := qChaveProcesso.GetAsInteger('ID_CHAVE_PROCESSO');
end;

end.
