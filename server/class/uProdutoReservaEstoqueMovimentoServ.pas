unit uProdutoReservaEstoqueMovimentoServ;

Interface

Uses uProdutoReservaEstoqueMovimentoProxy, System.SysUtils, uProdutoProxy;

type TPosicoesEstoque = (real, condicional);

type TProdutoReservaEstoqueMovimentoServ = class
  class function GetProdutoReservaEstoqueMovimento(
    const AIdProdutoReservaEstoqueMovimento: Integer): TProdutoReservaEstoqueMovimentoProxy;
  class function GerarProdutoReservaEstoqueMovimento(
    const AProdutoReservaEstoqueMovimento: TProdutoReservaEstoqueMovimentoProxy): Integer;
  class function RealizarReservaDeEstoque(AMovimentacao: TProdutoMovimento;
    APosicaoDestino: TPosicoesEstoque): Boolean;
  class function RemoverReservaDeEstoque(const AIdChaveProcesso: Integer): Boolean;
  class function PegarUltimaReservaProduto(AIdProduto, AIdFilial: Integer;
    APosicaoReserva: TPosicoesEstoque; AIdChaveProcesso: Integer = 0;
    ATipoMovimentacao: String = ''): TProdutoReservaEstoqueMovimentoProxy;
  class procedure AmortizarProcessos(const AIdChaveProcesso: Integer);
end;

implementation

{ TProdutoReservaEstoqueMovimentoServ }

uses uFactoryQuery, uProdutoServ, REST.JSON;

class function TProdutoReservaEstoqueMovimentoServ.GetProdutoReservaEstoqueMovimento(
    const AIdProdutoReservaEstoqueMovimento: Integer): TProdutoReservaEstoqueMovimentoProxy;
const
  SQL = 'SELECT * FROM PRODUTO_RESERVA_ESTOQUE_MOVIMENTO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TProdutoReservaEstoqueMovimentoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdProdutoReservaEstoqueMovimento));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FDtMovimento := qConsulta.GetAsString('DT_MOVIMENTO');
  result.FQtEstoqueAnterior := qConsulta.GetAsFloat('QT_ESTOQUE_ANTERIOR');
  result.FQtMovimento := qConsulta.GetAsFloat('QT_MOVIMENTO');
  result.FQtEstoqueAtual := qConsulta.GetAsFloat('QT_ESTOQUE_ATUAL');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FBoProcessoAmortizado := qConsulta.GetAsString('BO_PROCESSO_AMORTIZADO');
  result.FTipoReserva := qConsulta.GetAsString('TIPO_RESERVA');
end;

class procedure TProdutoReservaEstoqueMovimentoServ.AmortizarProcessos(const AIdChaveProcesso: Integer);
begin
  TFastQuery.ExecutarScript(
    ' UPDATE produto_reserva_estoque_movimento set bo_processo_amortizado = ''S'''+
    ' where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

class function TProdutoReservaEstoqueMovimentoServ.GerarProdutoReservaEstoqueMovimento(
    const AProdutoReservaEstoqueMovimento: TProdutoReservaEstoqueMovimentoProxy): Integer;
var
  qEntidade: IFastQuery;
  posicaoDestino: TPosicoesEstoque;
  ultimaMovimentacao: TProdutoReservaEstoqueMovimentoProxy;
  qtdeEstoqueAtual: Double;
  qtdeEstoqueAnterior: Double;
  tipoOrigem: String;
begin
  try
    if AProdutoReservaEstoqueMovimento.FTipoReserva.Equals(
      TProdutoReservaEstoqueMovimentoProxy.TIPO_RESERVA_ESTOQUE_CONDICIONAL) then
    begin
      posicaoDestino := condicional;
    end;

    if AProdutoReservaEstoqueMovimento.FTipo = MOVIMENTO_ENTRADA then
      tipoOrigem := MOVIMENTO_SAIDA
    else
      tipoOrigem := MOVIMENTO_ENTRADA;

    ultimaMovimentacao := PegarUltimaReservaProduto(AProdutoReservaEstoqueMovimento.FIdProduto,
      AProdutoReservaEstoqueMovimento.FIdFilial, posicaoDestino);
      //AProdutoReservaEstoqueMovimento.FIdChaveProcesso, tipoOrigem);
    try
      qtdeEstoqueAtual := ultimaMovimentacao.FQtEstoqueAtual;
      qtdeEstoqueAnterior := ultimaMovimentacao.FQtEstoqueAtual;
    finally
      FreeAndNil(ultimaMovimentacao);
    end;

    if AProdutoReservaEstoqueMovimento.FTipo = TProdutoReservaEstoqueMovimentoProxy.TIPO_ENTRADA then
      qtdeEstoqueAtual := qtdeEstoqueAtual + AProdutoReservaEstoqueMovimento.FQtMovimento
    else //SAIDA
      qtdeEstoqueAtual := qtdeEstoqueAtual - AProdutoReservaEstoqueMovimento.FQtMovimento;

    qEntidade := TFastQuery.ModoDeInclusao('PRODUTO_RESERVA_ESTOQUE_MOVIMENTO');
    qEntidade.Incluir;

    qEntidade.SetAsString('TIPO', AProdutoReservaEstoqueMovimento.FTipo);
    qEntidade.SetAsString('DT_MOVIMENTO', AProdutoReservaEstoqueMovimento.FDtMovimento);
    qEntidade.SetAsFloat('QT_ESTOQUE_ANTERIOR', qtdeEstoqueAnterior);
    qEntidade.SetAsFloat('QT_MOVIMENTO', AProdutoReservaEstoqueMovimento.FQtMovimento);
    qEntidade.SetAsString('DOCUMENTO_ORIGEM', AProdutoReservaEstoqueMovimento.FDocumentoOrigem);
    qEntidade.SetAsFloat('QT_ESTOQUE_ATUAL', qtdeEstoqueAtual);

    if AProdutoReservaEstoqueMovimento.FIdProduto > 0 then
    begin
      qEntidade.SetAsInteger('ID_PRODUTO', AProdutoReservaEstoqueMovimento.FIdProduto);
    end;

    if AProdutoReservaEstoqueMovimento.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AProdutoReservaEstoqueMovimento.FIdFilial);
    end;

    if AProdutoReservaEstoqueMovimento.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', AProdutoReservaEstoqueMovimento.FIdChaveProcesso);
    end;
    qEntidade.SetAsString('BO_PROCESSO_AMORTIZADO', AProdutoReservaEstoqueMovimento.FBoProcessoAmortizado);
    qEntidade.SetAsString('TIPO_RESERVA', AProdutoReservaEstoqueMovimento.FTipoReserva);
    qEntidade.Salvar;
    qEntidade.Persistir;

    TProdutoFilialServ.AtualizarReservaDeEstoque(AProdutoReservaEstoqueMovimento.FIdProduto,
      AProdutoReservaEstoqueMovimento.FIdFilial, qtdeEstoqueAtual, posicaoDestino);

    AProdutoReservaEstoqueMovimento.FID := qEntidade.GetAsInteger('ID');
    result :=     AProdutoReservaEstoqueMovimento.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ProdutoReservaEstoqueMovimento.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TProdutoReservaEstoqueMovimentoServ.PegarUltimaReservaProduto(AIdProduto, AIdFilial: Integer;
    APosicaoReserva: TPosicoesEstoque; AIdChaveProcesso: Integer = 0;
    ATipoMovimentacao: String = ''): TProdutoReservaEstoqueMovimentoProxy;
var
   qConsulta: IFastQuery;
   SQL: String;
   tipoReserva: String;
   filtrarPorChaveProcesso: Boolean;
begin
  filtrarPorChaveProcesso := (AIdChaveProcesso > 0) and (not ATipoMovimentacao.IsEmpty);

  if not filtrarPorChaveProcesso then
  begin
    SQL :=
      ' SELECT * FROM PRODUTO_RESERVA_ESTOQUE_MOVIMENTO WHERE id = '+
      ' (select max(id) from PRODUTO_RESERVA_ESTOQUE_MOVIMENTO '+
      '  where id_produto = :id_produto and id_filial = :id_filial and tipo_reserva = :tipo)';
  end
  else if filtrarPorChaveProcesso then
  begin
    SQL :=
      ' SELECT * FROM PRODUTO_RESERVA_ESTOQUE_MOVIMENTO'+
      '  WHERE id_produto = :id_produto and id_filial = :id_filial and tipo_reserva = :tipo_reserva'+
      ' and id_chave_processo = :id_chave_processo and tipo = :tipo)';
  end;

  case APosicaoReserva of
    condicional:
    begin
      tipoReserva := TProdutoReservaEstoqueMovimentoProxy.TIPO_RESERVA_ESTOQUE_CONDICIONAL;
    end;
  end;

  result := TProdutoReservaEstoqueMovimentoProxy.Create;

  if not filtrarPorChaveProcesso then
  begin
    qConsulta := TFastQuery.ModoDeConsulta(SQL,
      TArray<Variant>.Create(AIdProduto, AIdFilial, tipoReserva));
  end
  else if filtrarPorChaveProcesso then
  begin
    qConsulta := TFastQuery.ModoDeConsulta(SQL,
      TArray<Variant>.Create(AIdProduto, AIdFilial, tipoReserva, AIdChaveProcesso, ATipoMovimentacao));
  end;

  result.FId := qConsulta.GetAsInteger('ID');
  result.FTipo := qConsulta.GetAsString('TIPO');
  result.FDtMovimento := qConsulta.GetAsString('DT_MOVIMENTO');
  result.FQtEstoqueAnterior := qConsulta.GetAsFloat('QT_ESTOQUE_ANTERIOR');
  result.FQtMovimento := qConsulta.GetAsFloat('QT_MOVIMENTO');
  result.FQtEstoqueAtual := qConsulta.GetAsFloat('QT_ESTOQUE_ATUAL');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdProduto := qConsulta.GetAsInteger('ID_PRODUTO');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FTipoReserva := qConsulta.GetAsString('TIPO_RESERVA');
end;

class function TProdutoReservaEstoqueMovimentoServ.RealizarReservaDeEstoque(
  AMovimentacao: TProdutoMovimento; APosicaoDestino: TPosicoesEstoque): Boolean;
var
  fqMovimentoEstoque : IFastQuery;
  qtdeEstoqueAtual: Double;
  ultimaMovimentacao: TProdutoReservaEstoqueMovimentoProxy;
  tipoMovimentacao: String;
begin
  ultimaMovimentacao := PegarUltimaReservaProduto(
      AMovimentacao.FIdProduto, AMovimentacao.FIdFilial, APosicaoDestino);
  try
    qtdeEstoqueAtual := ultimaMovimentacao.FQtEstoqueAtual;
  finally
    FreeAndNil(ultimaMovimentacao);
  end;

  fqMovimentoEstoque := TFastQuery.ModoDeAlteracao(
    'select * from produto_reserva_estoque_movimento where 1 = 2', nil);

  fqMovimentoEstoque.Incluir;

   if AMovimentacao.FTipo = MOVIMENTO_ENTRADA then
     tipoMovimentacao := MOVIMENTO_SAIDA
   else
     tipoMovimentacao := MOVIMENTO_ENTRADA;

  fqMovimentoEstoque.SetAsString('TIPO', tipoMovimentacao);

  fqMovimentoEstoque.SetAsDateTime('DT_MOVIMENTO', StrToDate(AMovimentacao.FDtMovimento));
  fqMovimentoEstoque.SetAsFloat('QT_MOVIMENTO', AMovimentacao.FQtMovimento);
  fqMovimentoEstoque.SetAsString('DOCUMENTO_ORIGEM', AMovimentacao.FDocumentoOrigem);
  fqMovimentoEstoque.SetAsInteger('ID_PRODUTO', AMovimentacao.FIdProduto);
  fqMovimentoEstoque.SetAsInteger('ID_FILIAL', AMovimentacao.FIdFilial);
  fqMovimentoEstoque.SetAsFloat('ID_CHAVE_PROCESSO', AMovimentacao.FIdChaveProcesso);

  fqMovimentoEstoque.SetAsFloat('QT_ESTOQUE_ANTERIOR', qtdeEstoqueAtual);

  if tipoMovimentacao = MOVIMENTO_ENTRADA then
    qtdeEstoqueAtual := qtdeEstoqueAtual + AMovimentacao.FQtMovimento  //Movimentação inversa a movimentação de origem
  else
    qtdeEstoqueAtual := qtdeEstoqueAtual - AMovimentacao.FQtMovimento; //Movimentação inversa a movimentação de origem

  fqMovimentoEstoque.SetAsFloat('QT_ESTOQUE_ATUAL', qtdeEstoqueAtual);

  case APosicaoDestino of
    condicional:
    begin
      fqMovimentoEstoque.SetAsString('TIPO_RESERVA',
        TProdutoReservaEstoqueMovimentoProxy.TIPO_RESERVA_ESTOQUE_CONDICIONAL);
    end;
  end;

  fqMovimentoEstoque.Salvar;
  fqMovimentoEstoque.Persistir;

  TProdutoFilialServ.AtualizarReservaDeEstoque(AMovimentacao.FIdProduto,
    AMovimentacao.FIdFilial, qtdeEstoqueAtual, APosicaoDestino);
end;

class function TProdutoReservaEstoqueMovimentoServ.RemoverReservaDeEstoque(
  const AIdChaveProcesso: Integer): Boolean;
var
  qProdutoMovimento: IFastQuery;
  movimentacaoDeOrigem: TProdutoReservaEstoqueMovimentoProxy;
begin
  result := true;
  try
    Result := True;

    qProdutoMovimento := TFastQuery.ModoDeConsulta(
      ' SELECT * FROM produto_reserva_estoque_movimento WHERE id_chave_processo = :id '+
      ' and bo_processo_amortizado = ''N''',
      TArray<Variant>.Create(AIdChaveProcesso));

    try
      qProdutoMovimento.Primeiro;
      while not qProdutoMovimento.Eof do
      begin
        movimentacaoDeOrigem := TProdutoReservaEstoqueMovimentoServ.GetProdutoReservaEstoqueMovimento(
          qProdutoMovimento.GetAsInteger('ID'));
        try
          with movimentacaoDeOrigem do
          begin
            if FTipo = MOVIMENTO_ENTRADA then
              FTipo := MOVIMENTO_SAIDA
            else
              FTipo := MOVIMENTO_ENTRADA;

            FDtMovimento := DateToStr(Date);
          end;
          TProdutoReservaEstoqueMovimentoServ.GerarProdutoReservaEstoqueMovimento(
            movimentacaoDeOrigem);
        finally
          FreeAndNil(movimentacaoDeOrigem);
        end;
        qProdutoMovimento.Proximo;
      end;
    finally
      TProdutoReservaEstoqueMovimentoServ.AmortizarProcessos(AIdChaveProcesso);
    end;
  except
    on e : Exception do
    begin
      result := False;
    end;
  end;
end;

end.


