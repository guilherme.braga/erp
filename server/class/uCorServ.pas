unit uCorServ;

Interface

Uses uCorProxy, db, SysUtils;

type TCorServ = class
  class function GetCor(const AIdCor: Integer): TCorProxy;
  class function GetDescricaoCor(const AIdCor: Integer): String;
  class function GerarCor(const ACor: TCorProxy): Integer;
  class function ExisteCor(const ADescricaoCor: String): Boolean;
  class function GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
end;

implementation

{ TCorServ }

uses uFactoryQuery;

class function TCorServ.GetCor(
    const AIdCor: Integer): TCorProxy;
const
  SQL = 'SELECT * FROM COR WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCorProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCor));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FRgb := qConsulta.GetAsString('RGB');
  result.FCmyk := qConsulta.GetAsString('CMYK');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

class function TCorServ.GetDescricaoCor(const AIdCor: Integer): String;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select descricao from cor where id = :id',
    TArray<Variant>.Create(AIdCor));

  result := qEntidade.GetAsString('descricao');
end;

class function TCorServ.ExisteCor(const ADescricaoCor: String): Boolean;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from cor where descricao = :cor',
    TArray<Variant>.Create(ADescricaoCor));

  result := not qEntidade.IsEmpty;
end;

class function TCorServ.GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
var
  qEntidade: IFastQuery;
begin
  qEntidade := TFastQuery.ModoDeConsulta(
    ' select id from cor where descricao = :cor',
    TArray<Variant>.Create(ADescricaoCor));

  result := qEntidade.GetAsInteger('id');
end;

class function TCorServ.GerarCor(
    const ACor: TCorProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('COR');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ACor.FDescricao);
    qEntidade.SetAsString('RGB', ACor.FRgb);
    qEntidade.SetAsString('CMYK', ACor.FCmyk);
    qEntidade.SetAsString('BO_ATIVO', ACor.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ACor.FID := qEntidade.GetAsInteger('ID');
    result := ACor.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Cor.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


