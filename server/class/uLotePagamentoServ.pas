unit uLotePagamentoServ;

interface

uses
  Data.FireDACJSONReflect, uFactoryQuery;

type TLotePagamentoServ = class
  //class function GerarLancamentosIndividuais(AIdLotePagamento: Integer): string;
  class function RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
  class function RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
  class function GerarLancamentosAgrupados(AIdLotePagamento: Integer): string;
  class function RemoverLancamentosAgrupados(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
  class function GetIdLotePagamentoPelaChaveProcesso(AChaveProcesso : Integer): Integer;
  class function GetContasAPagar(AIdsContaPagar : string): TFDJSONDataSets;
  class procedure AtualizarLotePagamento(const AIdLotePagamento: Integer;
    const AStatus: String);
  private
  class function GerarMovimentacaoContaCorrente(ALote, ALoteQuitacao: TFastQuery): Boolean; static;
  class function GerarQuitacaoDesconto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlDesconto: Double): Boolean; static;
  class function GerarQuitacaoJuros(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlJuros: Double): Boolean; static;
  class function GerarQuitacaoMulta(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlMulta: Double): Boolean; static;
  class function GerarQuitacaoValorAberto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
    AVlQuitacao: Double): Boolean; static;
end;

implementation

{ TLotePagamentoServ }

uses
  uLotePagamentoProxy,
  uContaPagarServ,
  uContaPagarProxy,
  uContaCorrenteProxy,
  uMathUtils,
  uContaCorrenteServ,
  REST.Json,
  System.SysUtils, uTipoQuitacaoProxy, uTipoQuitacaoServ, uDatasetUtilsServ;

class procedure TLotePagamentoServ.AtualizarLotePagamento(
  const AIdLotePagamento: Integer; const AStatus: String);
begin
  if AStatus = TLotePagamentoProxy.STATUS_ABERTO then
  begin
    TFastQuery.ExecutarScript('UPDATE lote_pagamento SET status = :status,'+
    ' DH_EFETIVACAO = null WHERE id = :id_lote_pagamento',
    TArray<Variant>.Create(AStatus, AIdLotePagamento));
  end
  else if AStatus = TLotePagamentoProxy.STATUS_EFETIVADO then
  begin
    TFastQuery.ExecutarScript('UPDATE lote_pagamento SET status = :status,'+
    ' DH_EFETIVACAO = CURRENT_TIMESTAMP WHERE id = :id_lote_pagamento',
    TArray<Variant>.Create(AStatus, AIdLotePagamento));
  end;
end;

class function TLotePagamentoServ.GerarLancamentosAgrupados(
  AIdLotePagamento: Integer): string;
const
  SQLContaPagar = ' SELECT cp.*                  ' +
                  '   FROM conta_pagar cp        ' +
                  '  WHERE cp.id = :IdContaPagar ';

  SQLLotePagamento = ' SELECT lp.*                      ' +
                     '   FROM lote_pagamento lp         ' +
                     '  WHERE lp.id = :PIdLotePagamento ';

  SQLLotePagamentoTitulo = ' SELECT lpt.*                                             ' +
                           '   FROM lote_pagamento_titulo lpt                         ' +
                           '  INNER JOIN conta_pagar cp ON lpt.id_conta_pagar = cp.id ' +
                           '  WHERE lpt.id_lote_pagamento = :PIdLotePagamento         ' +
                           '  ORDER BY cp.dt_vencimento ';

  SQLLotePagamentoQuitacao = ' SELECT lpq.*                       ' +
                             '   FROM lote_pagamento_quitacao lpq ' +
                             '  WHERE lpq.id_lote_pagamento = :PIdLotePagamento  ';
var
  fqLote         : TFastQuery;
  fqLoteTitulos  : TFastQuery;
  fqLoteQuitacao : TFastQuery;

  valorJuros: Double;
  valorMulta: Double;
  valorDesconto: Double;
  percentualDesconto: Double;
  valorQuitacao: Double;

  valorJurosLP: Double;
  valorMultaLP: Double;
  ValorDescontoLP: Double;
  ValorTotalLP: Double;
  ValorTotalLPAberto: Double;
  ValorQuitacaoLP: Double;

  tipoQuitacao: String;
begin
  Result := TLotePagamentoProxy.RETORNO_SUCESSO;

  fqLote := TFastQuery.ModoDeConsulta(SQLLotePagamento, TArray<Variant>.Create(AIdLotePagamento));
  try
    fqLoteTitulos := TFastQuery.ModoDeConsulta(SQLLotePagamentoTitulo, TArray<Variant>.Create(AIdLotePagamento));
    try
      fqLoteQuitacao := TFastQuery.ModoDeConsulta(SQLLotePagamentoQuitacao, TArray<Variant>.Create(AIdLotePagamento));
      try
        ValorQuitacaoLP := fqLote.GetAsFloat('VL_QUITADO'); //Bruto
        valorJurosLP := TDatasetUtilsServ.SomarColuna(fqLoteQuitacao.FieldByName('VL_JUROS'));
        valorMultaLP := TDatasetUtilsServ.SomarColuna(fqLoteQuitacao.FieldByName('VL_MULTA'));
        ValorDescontoLP := fqLote.GetAsFloat('VL_DESCONTO');
        ValorTotalLP := fqLote.GetAsFloat('VL_QUITADO_LIQUIDO'); //liquido
        ValorTotalLPAberto := ValorTotalLP - valorJurosLP - valorMultaLP + ValorDescontoLP;

        if ValorQuitacaoLP <= 0 then
        begin
          exit;
        end;

        {Realizar o pagamento do Multa}
        if valorMultaLP > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorMulta := TContaPagarServ.GetValorMulta(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
            if (valorMulta > 0) then
            begin
              if (valorMulta > valorMultaLP) then
              begin
                valorMulta := valorMultaLP;
              end;

              GerarQuitacaoMulta(fqLote, fqLoteQuitacao, fqLoteTitulos, valorMulta);
              valorMultaLP := valorMultaLP - valorMulta;
            end;

            if valorMultaLP <= 0 then
            begin
              break;
            end;

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o pagamento do Juros}
        if valorJurosLP > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorJuros := TContaPagarServ.GetValorJuros(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
            if (valorJuros > 0) then
            begin
              if (valorJuros > valorJurosLP) then
              begin
                valorJuros := valorJurosLP;
              end;

              GerarQuitacaoJuros(fqLote, fqLoteQuitacao, fqLoteTitulos, valorJuros);
              valorJurosLP := valorJurosLP - valorJuros;
            end;

            if valorJurosLP <= 0 then
            begin
              break;
            end;

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o pagamento do valor do desconto}
        if valorDescontoLP > 0 then
        begin
          fqLoteTitulos.First;
          while not fqLoteTitulos.Eof do
          begin
            valorQuitacao := TContaPagarServ.GetValorAberto(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
            percentualDesconto := ((valorDescontoLP * 100) / ValorQuitacaoLP); //proporcional
            valorDesconto := TMathUtils.ValorSobrePercentual(percentualDesconto, valorQuitacao); //valor do percentual

            GerarQuitacaoDesconto(fqLote, fqLoteQuitacao, fqLoteTitulos, valorDesconto);

            fqLoteTitulos.Next;
          end;
        end;

        {Realizar o recebimento do valor em aberto}
        fqLoteTitulos.First;
        while not fqLoteTitulos.Eof do
        begin
          valorQuitacao := TContaPagarServ.GetValorAberto(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
          if (valorQuitacao > 0) then
          begin
            if (valorQuitacao > ValorTotalLPAberto) then
            begin
              valorQuitacao := ValorTotalLPAberto;
            end;

            GerarQuitacaoValorAberto(fqLote, fqLoteQuitacao, fqLoteTitulos, valorQuitacao);
            ValorTotalLPAberto := ValorTotalLPAberto - valorQuitacao;
          end;

          if ValorTotalLPAberto <= 0 then
          begin
            break;
          end;

          fqLoteTitulos.Next;
        end;

        {Realiza a gera��o da movimenta��o do conta corrente}
        fqLoteQuitacao.First;
        while not fqLoteQuitacao.Eof do
        begin
          GerarMovimentacaoContaCorrente(fqLote, fqLoteQuitacao);
          fqLoteQuitacao.Proximo;
        end;

        AtualizarLotePagamento(AIdLotePagamento, TLotePagamentoProxy.STATUS_EFETIVADO);
      finally
        FreeAndNil(fqLoteQuitacao);
      end;
    finally
      FreeAndNil(fqLoteTitulos);
    end;
  finally
    FreeAndNil(fqLote);
  end;
end;

class function TLotePagamentoServ.GerarQuitacaoMulta(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlMulta: Double): Boolean;
var
  Quitacao  : TContaPagarQuitacaoMovimento;
begin
  Quitacao  := TContaPagarQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaPagar       := ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := AVlMulta;
    Quitacao.FVlMulta            := AVlMulta;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO;
    Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente a multa informada no lote de pagamento';

    TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaPagarServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
  finally
    Quitacao.Free;
  end;
end;

class function TLotePagamentoServ.GerarQuitacaoJuros(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlJuros: Double): Boolean;
var
  Quitacao  : TContaPagarQuitacaoMovimento;
begin
  Quitacao  := TContaPagarQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaPagar       := ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := AVlJuros;
    Quitacao.FVlJuros            := AVlJuros;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO;
    Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao juros informado no lote de pagamento';

    TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaPagarServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
  finally
    Quitacao.Free;
  end;
end;

class function TLotePagamentoServ.GerarQuitacaoDesconto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlDesconto: Double): Boolean;
var
  Quitacao  : TContaPagarQuitacaoMovimento;
begin
  Quitacao  := TContaPagarQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaPagar       := ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := AVlDesconto;
    Quitacao.FVlAcrescimo        := 0;
    Quitacao.FVlJuros            := 0;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := 0;
    Quitacao.FVlTotal            := 0;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO;
    Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento     := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao desconto informado no lote de pagamento';

    TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaPagarServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
  finally
    Quitacao.Free;
  end;
end;

class function TLotePagamentoServ.GerarQuitacaoValorAberto(ALote, ALoteQuitacao, ALoteTitulos: TFastQuery;
  AVlQuitacao: Double): Boolean;
var
  Quitacao  : TContaPagarQuitacaoMovimento;
begin
  Quitacao  := TContaPagarQuitacaoMovimento.Create;
  try
    Quitacao.FIdContaPagar       := ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR');
    Quitacao.FDhCadastro         := DateTimeToStr(Now);
    Quitacao.FDtQuitacao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Quitacao.FObservacao         := ALote.GetAsString('OBSERVACAO');
    Quitacao.FIdFilial           := ALote.GetAsInteger('ID_FILIAL');
    Quitacao.FIdTipoQuitacao     := ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
    Quitacao.FIdContaCorrente    := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Quitacao.FIdContaAnalise     := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Quitacao.FIdCentroResultado  := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Quitacao.FVlDesconto         := 0;
    Quitacao.FVlAcrescimo        := 0;
    Quitacao.FVlJuros            := 0;
    Quitacao.FPercMulta          := 0;
    Quitacao.FPercAcrescimo      := 0;
    Quitacao.FPercDesconto       := 0;
    Quitacao.FVlQuitacao         := AVlQuitacao;
    Quitacao.FVlTotal            := AVlQuitacao;

    Quitacao.FIdDocumentoOrigem := ALote.GetAsInteger('ID');
    Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO;
    Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

    //Dados do Cheque
    Quitacao.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
    Quitacao.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
    Quitacao.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
    Quitacao.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
    Quitacao.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
    Quitacao.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
    Quitacao.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
    Quitacao.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

    //Dados do Cart�o
    Quitacao.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    Quitacao.FObservacao := 'Quita��o referente ao valor em aberto informado no lote de pagamento';

    TContaPagarServ.GerarContaPagarQuitacao(TJson.ObjectToJsonString(Quitacao));
    TContaPagarServ.AtualizarValores(ALoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));
  finally
    Quitacao.Free;
  end;
end;

class function TLotePagamentoServ.GerarMovimentacaoContaCorrente(ALote, ALoteQuitacao: TFastQuery): Boolean;
var
  Movimento: TContaCorrenteMovimento;
  tipoQuitacao: String;
begin
  try
    Movimento := TContaCorrenteMovimento.Create;

    Movimento.FDocumento         := ALoteQuitacao.GetAsString('DOCUMENTO');
    Movimento.FDescricao         := ALoteQuitacao.GetAsString('DESCRICAO');

    if Movimento.FDocumento.IsEmpty then
    begin
      Movimento.FDocumento := 'LP '+ALote.GetAsString('ID');
    end;

    if Movimento.FDescricao.IsEmpty then
    begin
      Movimento.FDescricao := 'LOTE DE PAGAMENTO N�'+ALote.GetAsString('ID');
    end;

    Movimento.FObservacao        := ALoteQuitacao.GetAsString('OBSERVACAO');
    Movimento.FParcelamento      := '1/1';
    Movimento.FDhCadastro        := DateTimeToStr(Now);
    Movimento.FDtEmissao         := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtMovimento       := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FDtCompetencia     := ALoteQuitacao.GetAsString('DT_QUITACAO');
    Movimento.FVlMovimento       := ALoteQuitacao.GetAsFloat('VL_TOTAL');
    Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;
    Movimento.FIdContaCorrente   := ALoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
    Movimento.FIdContaAnalise    := ALoteQuitacao.GetAsInteger('ID_CONTA_ANALISE');
    Movimento.FIdCentroResultado := ALoteQuitacao.GetAsInteger('ID_CENTRO_RESULTADO');
    Movimento.FIdChaveProcesso   := ALote.GetAsInteger('ID_CHAVE_PROCESSO');
    Movimento.FIdFilial          := ALote.GetAsInteger('ID_FILIAL');
    Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_LOTE_PAGAMENTO;
    Movimento.FIdDocumentoOrigem := ALote.GetAsInteger('ID');

    //Se for cheque a concilia��o � falsa
    tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(ALoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO'));
    if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
      (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
    begin
      Movimento.FBoCheque := 'S';
      Movimento.FBoConciliado := 'N';

      //Dados do Cheque
      Movimento.FChequeSacado        := ALoteQuitacao.GetAsString('CHEQUE_SACADO');
      Movimento.FChequeDocFederal    := ALoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
      Movimento.FChequeBanco         := ALoteQuitacao.GetAsString('CHEQUE_BANCO');
      Movimento.FChequeDtEmissao     := ALoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
      Movimento.FChequeDtVencimento  := ALoteQuitacao.GetAsString('CHEQUE_DT_VENCIMENTO');
      Movimento.FChequeAgencia       := ALoteQuitacao.GetAsString('CHEQUE_AGENCIA');
      Movimento.FChequeContaCorrente := ALoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
      Movimento.FChequeNumero        := ALoteQuitacao.GetAsInteger('CHEQUE_NUMERO');
    end
    else
    begin
      Movimento.FBoConciliado := 'N';
    end;

    //Dados do Cart�o
    Movimento.FIdOperadoraCartao   := ALoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

    TContaCorrenteServ.GerarMovimentoContaCorrente(TJson.ObjectToJsonString(Movimento));
  finally
    Movimento.Free;
  end;
end;

class function TLotePagamentoServ.RemoverLancamentosAgrupados(AIdLotePagamento: Integer;
  AGerarContraPartida: Boolean): string;
const
  SQLMovimentos = ' SELECT ccm.id                                  ' +
                  '   FROM conta_corrente_movimento ccm            ' +
                  '  WHERE ccm.id_documento_origem = :id_origem    ' +
                  '    and ccm.bo_processo_amortizado = ''N'' '+
                  '    AND ccm.tp_documento_origem = :tp_documento ';

  SQLQuitacoes  = ' SELECT cpq.id                                  ' +
                  '       ,cpq.id_conta_pagar                      ' +
                  '   FROM conta_pagar_quitacao cpq                ' +
                  '  WHERE cpq.id_documento_origem = :id_origem    ' +
                  '    AND cpq.tp_documento_origem = :tp_documento ';

var
  fqMovimentos : TFastQuery;
  fqQuitacoes  : TFastQuery;
begin

  fqMovimentos := TFastQuery.ModoDeConsulta(SQLMovimentos
                                           ,TArray<Variant>.Create(AIdLotePagamento
                                                                  ,TContaCorrenteMovimento.ORIGEM_LOTE_PAGAMENTO));

  while not fqMovimentos.Eof do
  begin
    if AGerarContraPartida then
    begin
      TContaCorrenteServ.GerarContrapartidaContaCorrentePorID(fqMovimentos.GetAsInteger('ID'));
    end
    else
    begin
      TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqMovimentos.GetAsInteger('ID'));
    end;

    fqMovimentos.Proximo;
  end;

  fqQuitacoes  := TFastQuery.ModoDeConsulta(SQLQuitacoes
                                           ,TArray<Variant>.Create(AIdLotePagamento
                                                                  ,TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO));
  while not fqQuitacoes.Eof do
  begin
    TContaPagarServ.RemoverContaPagarQuitacaoPorID(fqQuitacoes.GetAsInteger('ID'));
    TContaPagarServ.AtualizarValores(fqQuitacoes.GetAsInteger('ID_CONTA_PAGAR'));
    fqQuitacoes.Proximo;
  end;

  AtualizarLotePagamento(AIdLotePagamento, TLotePagamentoProxy.STATUS_ABERTO);
  Result := TLotePagamentoProxy.RETORNO_SUCESSO;
end;

{class function TLotePagamentoServ.GerarLancamentosIndividuais(
  AIdLotePagamento: Integer): string;
const

  SQLContaPagar = 'SELECT cp.*                  ' +
                  '  FROM conta_pagar cp        ' +
                  ' WHERE cp.id = :IdContaPagar ';

  SQLLotePagamento = 'SELECT lp.*                      ' +
                     '  FROM lote_pagamento lp         ' +
                     ' WHERE lp.id = :PIdLotePagamento ';

  SQLLotePagamentoTitulo = ' SELECT lpt.*                                             ' +
                           '   FROM lote_pagamento_titulo lpt                         ' +
                           '  INNER JOIN conta_pagar cp ON lpt.id_conta_pagar = cp.id ' +
                           '  WHERE lpt.id_lote_pagamento = :PIdLotePagamento         ' +
                           '  ORDER BY cp.dt_vencimento ';

  SQLLotePagamentoQuitacao = ' SELECT lpq.*                       ' +
                             '   FROM lote_pagamento_quitacao lpq ' +
                             '  WHERE lpq.id = :PIdLotePagamento  ';

var
  fqLote         : TFastQuery;
  fqLoteTitulos  : TFastQuery;
  fqLoteQuitacao : TFastQuery;
  fqContaPagar   : TFastQuery;

  Quitacao       : TContaPagarQuitacaoMovimento;
  Movimento      : TContaCorrenteMovimento;

  QuitacaoJSON   : string;
  MovimentoJSON  : string;

  ValorQuitacaoLP  : Double;
  ValorQuitacaoCP  : Double;

  tipoQuitacao: String;
begin

  Result := TLotePagamentoProxy.RETORNO_SUCESSO;

  fqLoteTitulos  := TFastQuery.ModoDeConsulta(SQLLotePagamentoTitulo, TArray<Variant>.Create(AIdLotePagamento));
  fqLote         := TFastQuery.ModoDeConsulta(SQLLotePagamento, TArray<Variant>.Create(AIdLotePagamento));
  fqLoteQuitacao := TFastQuery.ModoDeConsulta(SQLLotePagamentoQuitacao, TArray<Variant>.Create(AIdLotePagamento));

  while not fqLoteQuitacao.Eof do
  begin
    ValorQuitacaoLP := fqLoteQuitacao.GetAsFloat('VL_QUITACAO');
    while not fqLoteTitulos.Eof do
    begin

      fqContaPagar := TFastQuery.ModoDeConsulta(SQLContaPagar, TArray<Variant>.Create(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR')));

      Quitacao  := TContaPagarQuitacaoMovimento.Create;
      Movimento := TContaCorrenteMovimento.Create;
      try

        Quitacao.FIdContaPagar    := fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR');
        Quitacao.FDhCadastro      := fqLote.GetAsString('DH_EFETIVACAO');
        Quitacao.FDtQuitacao      := fqLote.GetAsString('DH_EFETIVACAO');
        Quitacao.FObservacao      := fqLote.GetAsString('OBSERVACAO');
        Quitacao.FIdFilial        := fqLote.GetAsInteger('ID_FILIAL');
        Quitacao.FIdTipoQuitacao  := fqLoteQuitacao.GetAsInteger('ID_TIPO_QUITACAO');
        Quitacao.FIdContaCorrente := fqLoteQuitacao.GetAsInteger('ID_CONTA_CORRENTE');
        Quitacao.FIdContaAnalise  := fqContaPagar.GetAsInteger('ID_CONTA_ANALISE');
        Quitacao.FVlDesconto      := fqLoteTitulos.GetAsFloat('VL_DESCONTO');
        Quitacao.FVlAcrescimo     := fqLoteTitulos.GetAsFloat('VL_ACRESCIMO');

        if ValorQuitacaoLP >= (fqContaPagar.GetAsFloat('VL_ABERTO') - fqLoteTitulos.GetAsFloat('VL_DESCONTO'))then
        begin
          ValorQuitacaoCP := (fqContaPagar.GetAsFloat('VL_ABERTO') - fqLoteTitulos.GetAsFloat('VL_DESCONTO'));
          ValorQuitacaoLP := ValorQuitacaoLP - ValorQuitacaoCP;
        end
        else
        begin
          ValorQuitacaoCP := ValorQuitacaoLP;
          ValorQuitacaoLP := 0;
        end;

        Quitacao.FVlQuitacao        := ValorQuitacaoCP;
        Quitacao.FVlTotal           := ValorQuitacaoCP + fqLoteTitulos.GetAsFloat('VL_ACRESCIMO');
        Quitacao.FPercAcrescimo     := TMathUtils.PercentualSobreValor(fqLoteTitulos.GetAsFloat('VL_ACRESCIMO'),
                                                                        fqContaPagar.GetAsFloat('VL_ABERTO'));
        Quitacao.FPercDesconto      := TMathUtils.PercentualSobreValor(fqLoteTitulos.GetAsFloat('VL_DESCONTO'),
                                                                        fqContaPagar.GetAsFloat('VL_ABERTO'));
        Quitacao.FIdDocumentoOrigem := fqLote.GetAsInteger('ID');
        Quitacao.FTPDocumentoOrigem := TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO;
        Quitacao.FStatus            := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

        //Dados do Cheque
        Quitacao.FChequeSacado        := fqLoteQuitacao.GetAsString('CHEQUE_SACADO');
        Quitacao.FChequeDocFederal    := fqLoteQuitacao.GetAsString('CHEQUE_DOC_FEDERAL');
        Quitacao.FChequeBanco         := fqLoteQuitacao.GetAsString('CHEQUE_BANCO');
        Quitacao.FChequeDtEmissao     := fqLoteQuitacao.GetAsString('CHEQUE_DT_EMISSAO');
        Quitacao.FChequeAgencia       := fqLoteQuitacao.GetAsString('CHEQUE_AGENCIA');
        Quitacao.FChequeContaCorrente := fqLoteQuitacao.GetAsString('CHEQUE_CONTA_CORRENTE');
        Quitacao.FChequeNumero        := fqLoteQuitacao.GetAsInteger('CHEQUE_NUMERO');

        //Dados do Cart�o
        Quitacao.FIdOperadoraCartao   := fqLoteQuitacao.GetAsInteger('ID_OPERADORA_CARTAO');

        QuitacaoJSON := TJson.ObjectToJsonString(Quitacao);
        TContaPagarServ.GerarContaPagarQuitacao(QuitacaoJSON);
        TContaPagarServ.AtualizarValores(fqLoteTitulos.GetAsInteger('ID_CONTA_PAGAR'));

        Movimento.FDocumento         := fqLoteQuitacao.GetAsString('DOCUMENTO');
        Movimento.FDescricao         := fqLoteQuitacao.GetAsString('DESCRICAO');
        Movimento.FObservacao        := Quitacao.FObservacao;
        Movimento.FDhCadastro        := Quitacao.FDtQuitacao;
        Movimento.FDtEmissao         := Quitacao.FDtQuitacao;
        Movimento.FDtMovimento       := Quitacao.FDtQuitacao;
        Movimento.FDtCompetencia     := Quitacao.FDtQuitacao;
        Movimento.FVlMovimento       := Quitacao.FVlTotal;
        Movimento.FTpMovimento       := TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO;
        Movimento.FIdContaCorrente   := Quitacao.FIdContaCorrente;
        Movimento.FIdContaAnalise    := Quitacao.FIdContaAnalise;
        Movimento.FIdCentroResultado := fqContaPagar.GetAsInteger('ID_CENTRO_RESULTADO');
        Movimento.FIdChaveProcesso   := fqLote.GetAsInteger('ID_CHAVE_PROCESSO');
        Movimento.FIdFilial          := fqLote.GetAsInteger('ID_FILIAL');
        Movimento.FTPDocumentoOrigem := TContaCorrenteMovimento.ORIGEM_LOTE_PAGAMENTO;
        Movimento.FIdDocumentoOrigem := fqLote.GetAsInteger('ID');

        //Se for cheque a concilia��o � falsa
        tipoQuitacao := TTipoQuitacaoServ.GetTipoQuitacao(Quitacao.FIdTipoQuitacao);
        if (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
          (tipoQuitacao = TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
        begin
          Movimento.FBoCheque := 'S';
          Movimento.FBoConciliado := 'N';

          //Dados do Cheque
          Movimento.FChequeSacado        := Quitacao.FChequeSacado;
          Movimento.FChequeDocFederal    := Quitacao.FChequeDocFederal;
          Movimento.FChequeBanco         := Quitacao.FChequeBanco;
          Movimento.FChequeDtEmissao     := Quitacao.FChequeDtEmissao;
          Movimento.FChequeAgencia       := Quitacao.FChequeAgencia;
          Movimento.FChequeContaCorrente := Quitacao.FChequeContaCorrente;
          Movimento.FChequeNumero        := Quitacao.FChequeNumero;
        end
        else
        begin
          Movimento.FBoConciliado := 'S';
        end;

        //Dados do Cart�o
        Movimento.FIdOperadoraCartao   := Quitacao.FIdOperadoraCartao;

        MovimentoJSON := TJson.ObjectToJsonString(Movimento);
        TContaCorrenteServ.GerarMovimentoContaCorrente(MovimentoJSON);
      finally
        Movimento.Free;
        Quitacao.Free;
      end;

      if ValorQuitacaoLP > 0 then
      begin
        fqLoteTitulos.Proximo;
      end
      else
      begin
        Break;
      end;

    end;
    fqLoteQuitacao.Proximo;
  end;

end;}

class function TLotePagamentoServ.GetContasAPagar(
  AIdsContaPagar: string): TFDJSONDataSets;
const
  SQL = 'SELECT * FROM conta_pagar cp WHERE cp.id IN (:ids)';
var
  fqContaPagar : IFastQuery;
begin

  Result := TFDJSONDataSets.Create;

  fqContaPagar := TFastQuery.ModoDeConsulta(SQL,
                   TArray<Variant>.Create(AIdsContaPagar));

  TFDJSONDataSetsWriter.ListAdd(Result, (fqContaPagar as TFastQuery));

end;

class function TLotePagamentoServ.GetIdLotePagamentoPelaChaveProcesso(
  AChaveProcesso: Integer): Integer;
const
  SqlLotePagamento = 'SELECT lp.id FROM lote_pagamento lp WHERE lp.id_chave_processo = :idChave ';
var
  fqLotePagamento : TFastQuery;
begin
  fqLotePagamento := TFastQuery.ModoDeConsulta(SqlLotePagamento
                                              ,TArray<Variant>.Create(AChaveProcesso));

  Result := fqLotePagamento.GetAsInteger('ID');

end;

class function TLotePagamentoServ.RemoverMovimentosIndividuais(
  AIdLotePagamento: Integer): string;
const
  SQLMovimentos = ' SELECT ccm.id                                  ' +
                  '   FROM conta_corrente_movimento ccm            ' +
                  '  WHERE ccm.id_documento_origem = :id_origem    ' +
                  '    AND ccm.tp_documento_origem = :tp_documento ';
var
  fqMovimentos : TFastQuery;
begin

  Result := TLotePagamentoProxy.RETORNO_SUCESSO;

  fqMovimentos := TFastQuery.ModoDeConsulta(SQLMovimentos
                                           ,TArray<Variant>.Create(AIdLotePagamento
                                                                  ,TContaCorrenteMovimento.ORIGEM_LOTE_PAGAMENTO));

  while not fqMovimentos.Eof do
  begin
    TContaCorrenteServ.RemoverMovimentoContaCorrentePorID(fqMovimentos.GetAsInteger('ID'));
  end;

end;

class function TLotePagamentoServ.RemoverQuitacoesIndividuais(
  AIdLotePagamento: Integer): string;
const
  SQLQuitacoes  = ' SELECT cpq.id                                  ' +
                  '       ,cpq.id_conta_pagar                      ' +
                  '   FROM conta_pagar_quitacao cpq                ' +
                  '  WHERE cpq.id_documento_origem = :id_origem    ' +
                  '    AND cpq.tp_documento_origem = :tp_documento ';
var
  fqQuitacoes  : TFastQuery;
begin

  Result := TLotePagamentoProxy.RETORNO_SUCESSO;

  fqQuitacoes  := TFastQuery.ModoDeConsulta(SQLQuitacoes
                                           ,TArray<Variant>.Create(AIdLotePagamento
                                                                  ,TContaPagarQuitacaoMovimento.ORIGEM_LOTE_PAGAMENTO));
  while not fqQuitacoes.Eof do
  begin
    TContaPagarServ.RemoverContaPagarQuitacaoPorID(fqQuitacoes.GetAsInteger('ID'));
    TContaPagarServ.AtualizarValores(fqQuitacoes.GetAsInteger('ID_CONTA_PAGAR'));
  end;

end;

end.
