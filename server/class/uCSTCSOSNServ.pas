unit uCstCsosnServ;

Interface

Uses uCstCsosnProxy;

type TCstCsosnServ = class
  class function GetCstCsosn(const AIdCstCsosn: Integer): TCstCsosnProxy;
end;

implementation

{ TCstCsosnServ }

uses uFactoryQuery;

class function TCstCsosnServ.GetCstCsosn(
    const AIdCstCsosn: Integer): TCstCsosnProxy;
const
  SQL = 'SELECT * FROM cst_csosn WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCstCsosnProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCstCsosn));

  result.FId := qConsulta.GetAsString('ID');
  result.FCodigoCst := qConsulta.GetAsString('CODIGO_CST');
  result.FCodigoCsosn := qConsulta.GetAsString('CODIGO_CSOSN');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FObservacao := qConsulta.GetAsString('OBSERVACAO');
end;

end.


