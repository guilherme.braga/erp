unit uTipoQuitacaoServ;

interface

Uses uFactoryQuery;

type TTipoQuitacaoServ = class
  class function GetTipoQuitacao(AIdTipoQuitacao: Integer): String;
end;

implementation

{ TTipoQuitacaoServ }

class function TTipoQuitacaoServ.GetTipoQuitacao(
  AIdTipoQuitacao: Integer): String;
var
  qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(
    'SELECT * FROM tipo_quitacao WHERE id = :id',
    TArray<Variant>.Create(AIdTipoQuitacao));

  result := qConsulta.GetAsString('TIPO');
end;

end.
