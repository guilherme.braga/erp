unit uVeiculoServ;

interface

Uses uVeiculoProxy;

type TVeiculoServ = class
  class function GetVeiculo(const AIdVeiculo: Integer): TVeiculoProxy;
end;

implementation

{ TVeiculoServ }

uses uFactoryQuery;

class function TVeiculoServ.GetVeiculo(const AIdVeiculo: Integer): TVeiculoProxy;
const
  SQL =
  ' SELECT veiculo.id'+
  '     ,veiculo.placa'+
  '     ,veiculo.ano'+
  '     ,veiculo.chassi'+
  '     ,veiculo.combustivel'+
  '     ,veiculo.renavam'+
  '     ,veiculo.ID_MODELO'+
  '     ,veiculo.id_marca'+
  '     ,veiculo.ID_COR'+
  '     ,veiculo.ID_PESSOA'+
  '     ,veiculo.ID_FILIAL'+
  '     ,veiculo.bo_ativo'+
  '     ,(select descricao from cor where id = veiculo.id_cor) as JOIN_COR_VEICULO'+
  '     ,(select descricao from modelo where id = veiculo.id_modelo) as JOIN_MODELO_VEICULO'+
  '     ,(select descricao from marca where id = veiculo.id_marca) as JOIN_MARCA_VEICULO'+
  ' FROM veiculo'+
  ' WHERE veiculo.id = :id';
var
  consultaVeiculo: IFastQuery;
begin
  result := TVeiculoProxy.Create;
  consultaVeiculo := TFastQuery.ModoDeConsulta(
    SQL, TArray<Variant>.Create(AIdVeiculo));

  result.FId := consultaVeiculo.GetAsInteger('ID');
  result.FPlaca := consultaVeiculo.GetAsString('placa');
  result.FAno := consultaVeiculo.GetAsInteger('ano');
  result.FCombustivel := consultaVeiculo.GetAsString('combustivel');
  result.FChassi := consultaVeiculo.GetAsString('chassi');
  result.FRenavam := consultaVeiculo.GetAsString('Renavam');
  result.FIdModelo := consultaVeiculo.GetAsInteger('Id_Modelo');
  result.FIdMarca := consultaVeiculo.GetAsInteger('Id_Marca');
  result.FIdCor := consultaVeiculo.GetAsInteger('Id_Cor');
  result.FIdFilial := consultaVeiculo.GetAsInteger('Id_filial');
  result.FIdPessoa := consultaVeiculo.GetAsInteger('Id_Pessoa');
  result.FBoAtivo := consultaVeiculo.GetAsString('Bo_Ativo');

  result.FDescricaoModelo := consultaVeiculo.GetAsString('JOIN_MODELO_VEICULO');
  result.FDescricaoMarca := consultaVeiculo.GetAsString('JOIN_MARCA_VEICULO');
  result.FDescricaoCor := consultaVeiculo.GetAsString('JOIN_COR_VEICULO');
end;

end.
