unit uTipoRestricaoServ;

Interface

Uses uTipoRestricaoProxy, SysUtils;

type TTipoRestricaoServ = class
  class function GetTipoRestricao(
    const AIdTipoRestricao: Integer): TTipoRestricaoProxy;
  class function GerarTipoRestricao(
    const ATipoRestricao: TTipoRestricaoProxy): Integer;
end;

implementation

{ TTipoRestricaoServ }

uses uFactoryQuery;

class function TTipoRestricaoServ.GetTipoRestricao(
    const AIdTipoRestricao: Integer): TTipoRestricaoProxy;
const
  SQL = 'SELECT * FROM TIPO_RESTRICAO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TTipoRestricaoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdTipoRestricao));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

class function TTipoRestricaoServ.GerarTipoRestricao(
    const ATipoRestricao: TTipoRestricaoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('TIPO_RESTRICAO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ATipoRestricao.FDescricao);
    qEntidade.SetAsString('BO_ATIVO', ATipoRestricao.FBoAtivo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ATipoRestricao.FID := qEntidade.GetAsInteger('ID');
    result :=     ATipoRestricao.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar TipoRestricao.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


