unit uPessoaCreditoServ;

interface

Uses uPessoaCreditoProxy;

type TPessoaCreditoServ = class
  private
  class function InserirPessoaCreditoMovimento(
    pessoaCredito: TPessoaCreditoMovimentoProxy): Boolean;
  class function AtualizarPessoaCredito(AIdPessoa: Integer; AValor: Double): Boolean;

  public
  class function CreditoDisponivel(AIdPessoa: Integer): Double;
  class function RealizarTransacaoCredito(APessoaCredito: String): Boolean;
  class function GetPessoaCredito(const AIdPessoaCredito: Integer): TPessoaCreditoProxy;
  class function GerarPessoaCredito(const APessoaCredito: TPessoaCreditoProxy): Integer;
  class function ExisteCreditoPessoa(AIdChaveProcesso: Integer): Boolean;
  class function ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
end;

type TPessoaCreditoMovimentoServ = class
  class function GetPessoaCreditoMovimento(
    const AIdPessoaCreditoMovimento: Integer): TPessoaCreditoMovimentoProxy;
  class function GerarPessoaCreditoMovimento(
    const APessoaCreditoMovimento: TPessoaCreditoMovimentoProxy): Integer;
  class function PegarUltimaTransacaoCredito(const AIdPessoa: Integer): TPessoaCreditoMovimentoProxy;
  class procedure AmortizarProcessos(const AIdChaveProcesso: Integer);
  class function RealizarEstornoDaMovimentacaoCredito(
    const AIdChaveProcesso: Integer): Boolean;
end;

implementation

{ TPessoaCreditoServ }

Uses uFactoryQuery, Rest.JSON, SysUtils;

class function TPessoaCreditoServ.ExisteCreditoPessoa(
  AIdChaveProcesso: Integer): Boolean;
var qContaPagar: IFastQuery;
begin
  result := true;

  qContaPagar := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   from pessoa_credito_movimento'+
    '  where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));

  result := not qContaPagar.IsEmpty;
end;

class function TPessoaCreditoServ.ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
var
  qPessoaCredito: IFastQuery;
begin
  qPessoaCredito := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM pessoa_credito WHERE id_pessoa = :id_pessoa',
    TArray<Variant>.Create(AIdPessoa));

  result := not qPessoaCredito.IsEmpty;
end;

class function TPessoaCreditoServ.AtualizarPessoaCredito(AIdPessoa: Integer; AValor: Double): Boolean;
var
  qPessoaCredito: IFastQuery;
begin
  result := false;

  qPessoaCredito := TFastQuery.ModoDeAlteracao(
    'SELECT * FROM pessoa_credito WHERE id_pessoa = :id_pessoa',
    TArray<Variant>.Create(AIdPessoa));

  if qPessoaCredito.IsEmpty then
    qPessoaCredito.Incluir
  else
    qPessoaCredito.Alterar;

  qPessoaCredito.SetAsInteger('ID_PESSOA', AIdPessoa);
  qPessoaCredito.SetAsFloat('VL_CREDITO', AValor);
  qPessoaCredito.Salvar;
  qPessoaCredito.Persistir;

  result := true;
end;

class function TPessoaCreditoServ.CreditoDisponivel(AIdPessoa: Integer): Double;
var
  qPessoaCredito: IFastQuery;
begin
  try
    qPessoaCredito := TFastQuery.ModoDeConsulta(
    'SELECT vl_credito FROM pessoa_credito WHERE id_pessoa = :id_pessoa',
    TArray<Variant>.Create(AIdPessoa));

    result := qPessoaCredito.GetAsFloat('vl_credito');
  except
    on e : Exception do
    begin
      Result := 0;
      raise Exception.Create('Erro ao buscar cr�dito dispon�vel do cliente '+InttoStr(AIdPessoa)+'.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaCreditoServ.InserirPessoaCreditoMovimento(
  pessoaCredito: TPessoaCreditoMovimentoProxy): Boolean;
var qPessoaCredito: IFastQuery;
begin
  result := false;

  qPessoaCredito := TFastQuery.ModoDeInclusao(
    TPessoaCreditoMovimentoProxy.TABELA_PESSOA_CREDITO_MOVIMENTO);

  qPessoaCredito.Incluir;

  qPessoaCredito.SetAsFloat('VL_CREDITO_ANTERIOR',
    pessoaCredito.FVlCreditoAnterior);
  qPessoaCredito.SetAsFloat('VL_CREDITO_MOVIMENTO',
    pessoaCredito.FVlCreditoMovimento);
  qPessoaCredito.SetAsFloat('VL_CREDITO_ATUAL',
    pessoaCredito.FVlCreditoAtual);
  qPessoaCredito.SetAsDateTime('DH_MOVIMENTO',
    StrtoDateTime(pessoaCredito.FDhMovimento));
  qPessoaCredito.SetAsInteger('ID_DOCUMENTO_ORIGEM',
    pessoaCredito.FIdDocumentoOrigem);
  qPessoaCredito.SetAsString('DOCUMENTO_ORIGEM',
    pessoaCredito.FDocumentoOrigem);
  qPessoaCredito.SetAsInteger('ID_PESSOA', pessoaCredito.FIdPessoa);
  qPessoaCredito.SetAsInteger('ID_PESSOA_USUARIO',
    pessoaCredito.FIdPessoaUsuario);
  qPessoaCredito.SetAsInteger('ID_CHAVE_PROCESSO',
    pessoaCredito.FIdChaveProcesso);
  qPessoaCredito.SetAsString('TIPO',
    pessoaCredito.FTipo);

  qPessoaCredito.Salvar;
  qPessoaCredito.Persistir;

  result := true;
end;

class function TPessoaCreditoServ.RealizarTransacaoCredito(
  APessoaCredito: String): Boolean;
var
  pessoaCredito: TPessoaCreditoMovimentoProxy;
  gerouMovimentacao: Boolean;
  atualizouCreditoPessoa: Boolean;
begin
  Result := False;
  try
    pessoaCredito := TPessoaCreditoMovimentoProxy.Create;
    try
      pessoaCredito := TJson.JsonToObject<TPessoaCreditoMovimentoProxy>(APessoaCredito);

      pessoaCredito.FVlCreditoAnterior :=
        TPessoaCreditoServ.CreditoDisponivel(pessoaCredito.FIdPessoa);

      if pessoaCredito.FTipo = TPessoaCreditoMovimentoProxy.TIPO_CREDITO then
      begin
        pessoaCredito.FVlCreditoAtual :=
          pessoaCredito.FVlCreditoAnterior + pessoaCredito.FVlCreditoMovimento;
      end
      else
      begin
        pessoaCredito.FVlCreditoAtual :=
          pessoaCredito.FVlCreditoAnterior - pessoaCredito.FVlCreditoMovimento;
      end;

      gerouMovimentacao := InserirPessoaCreditoMovimento(pessoaCredito);
      atualizouCreditoPessoa := AtualizarPessoaCredito(pessoaCredito.FIdPessoa,
        pessoaCredito.FVlCreditoAtual);

      Result := gerouMovimentacao and atualizouCreditoPessoa;
    finally
      FreeAndNil(pessoaCredito);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao realizar transa��o de cr�dito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaCreditoServ.GetPessoaCredito(
    const AIdPessoaCredito: Integer): TPessoaCreditoProxy;
const
  SQL = 'SELECT * FROM PESSOA_CREDITO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaCreditoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoaCredito));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FVlCredito := qConsulta.GetAsFloat('VL_CREDITO');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
end;

class function TPessoaCreditoServ.GerarPessoaCredito(
    const APessoaCredito: TPessoaCreditoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('PESSOA_CREDITO');
    qEntidade.Incluir;

    qEntidade.SetAsFloat('VL_CREDITO', APessoaCredito.FVlCredito);

    if APessoaCredito.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', APessoaCredito.FIdPessoa);
    end;
    qEntidade.Salvar;
    qEntidade.Persistir;

    APessoaCredito.FID := qEntidade.GetAsInteger('ID');
    result :=     APessoaCredito.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar PessoaCredito.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaCreditoMovimentoServ.GetPessoaCreditoMovimento(
    const AIdPessoaCreditoMovimento: Integer): TPessoaCreditoMovimentoProxy;
const
  SQL = 'SELECT * FROM PESSOA_CREDITO_MOVIMENTO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TPessoaCreditoMovimentoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdPessoaCreditoMovimento));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FVlCreditoAnterior := qConsulta.GetAsFloat('VL_CREDITO_ANTERIOR');
  result.FVlCreditoMovimento := qConsulta.GetAsFloat('VL_CREDITO_MOVIMENTO');
  result.FVlCreditoAtual := qConsulta.GetAsFloat('VL_CREDITO_ATUAL');
  result.FDhMovimento := qConsulta.GetAsString('DH_MOVIMENTO');
  result.FIdDocumentoOrigem := qConsulta.GetAsInteger('ID_DOCUMENTO_ORIGEM');
  result.FDocumentoOrigem := qConsulta.GetAsString('DOCUMENTO_ORIGEM');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FIdPessoaUsuario := qConsulta.GetAsInteger('ID_PESSOA_USUARIO');
  result.FIdChaveProcesso := qConsulta.GetAsInteger('ID_CHAVE_PROCESSO');
  result.FTipo := qConsulta.GetAsString('TIPO');
end;

class function TPessoaCreditoMovimentoServ.GerarPessoaCreditoMovimento(
    const APessoaCreditoMovimento: TPessoaCreditoMovimentoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('PESSOA_CREDITO_MOVIMENTO');
    qEntidade.Incluir;

    qEntidade.SetAsFloat('VL_CREDITO_ANTERIOR', APessoaCreditoMovimento.FVlCreditoAnterior);
    qEntidade.SetAsFloat('VL_CREDITO_MOVIMENTO', APessoaCreditoMovimento.FVlCreditoMovimento);
    qEntidade.SetAsFloat('VL_CREDITO_ATUAL', APessoaCreditoMovimento.FVlCreditoAtual);
    qEntidade.SetAsString('DH_MOVIMENTO', APessoaCreditoMovimento.FDhMovimento);

    if APessoaCreditoMovimento.FIdDocumentoOrigem > 0 then
    begin
      qEntidade.SetAsInteger('ID_DOCUMENTO_ORIGEM', APessoaCreditoMovimento.FIdDocumentoOrigem);
    end;
    qEntidade.SetAsString('DOCUMENTO_ORIGEM', APessoaCreditoMovimento.FDocumentoOrigem);

    if APessoaCreditoMovimento.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', APessoaCreditoMovimento.FIdPessoa);
    end;

    if APessoaCreditoMovimento.FIdPessoaUsuario > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA_USUARIO', APessoaCreditoMovimento.FIdPessoaUsuario);
    end;

    if APessoaCreditoMovimento.FIdChaveProcesso > 0 then
    begin
      qEntidade.SetAsInteger('ID_CHAVE_PROCESSO', APessoaCreditoMovimento.FIdChaveProcesso);
    end;
    qEntidade.SetAsString('TIPO', APessoaCreditoMovimento.FTipo);
    qEntidade.Salvar;
    qEntidade.Persistir;

    APessoaCreditoMovimento.FID := qEntidade.GetAsInteger('ID');
    result :=     APessoaCreditoMovimento.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar PessoaCreditoMovimento.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaCreditoMovimentoServ.PegarUltimaTransacaoCredito(
    const AIdPessoa: Integer): TPessoaCreditoMovimentoProxy;
var
  qPessoaCreditoMovimento: IFastQuery;
begin
  try
    result := TPessoaCreditoMovimentoProxy.Create;

    qPessoaCreditoMovimento := TFastQuery.ModoDeConsulta(
      'SELECT id from pessoa_credito_movimento where id_pessoa = :id_pessoa order by dh_movimento desc limit 1',
      TArray<Variant>.Create(AIdPessoa));

    result := TPessoaCreditoMovimentoServ.GetPessoaCreditoMovimento(qPessoaCreditoMovimento.GetAsInteger('id'));
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao buscar a ultima movimenta��o da transa��o de cr�dito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class function TPessoaCreditoMovimentoServ.RealizarEstornoDaMovimentacaoCredito(
    const AIdChaveProcesso: Integer): Boolean;
var
  qPessoaCreditoMovimento: IFastQuery;
  movimentacaoDeOrigem: TPessoaCreditoMovimentoProxy;
begin
  try
    qPessoaCreditoMovimento := TFastQuery.ModoDeConsulta(
      'SELECT * FROM pessoa_credito_movimento WHERE id_chave_processo = :id and bo_processo_amortizado = ''N''',
      TArray<Variant>.Create(AIdChaveProcesso));

    try
      qPessoaCreditoMovimento.Primeiro;
      while not qPessoaCreditoMovimento.Eof do
      begin
        movimentacaoDeOrigem := TPessoaCreditoMovimentoServ.GetPessoaCreditoMovimento(
          qPessoaCreditoMovimento.GetAsInteger('ID'));
        try
          with movimentacaoDeOrigem do
          begin
            if FTipo = TIPO_CREDITO then
              FTipo := TIPO_DEBITO
            else
              FTipo := TIPO_CREDITO;

            FDhMovimento := DateToStr(Now);
          end;
          TPessoaCreditoServ.RealizarTransacaoCredito(TJson.ObjectToJsonString(movimentacaoDeOrigem));
        finally
          FreeAndNil(movimentacaoDeOrigem);
        end;
        qPessoaCreditoMovimento.Proximo;
      end;
    finally
      TPessoaCreditoMovimentoServ.AmortizarProcessos(AIdChaveProcesso);
    end;
    Result := True;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao Realizar estorno da movimenta��o de cr�dito.'+
        ' Mensagem Original: '+e.message);
    end;
  end;
end;

class procedure TPessoaCreditoMovimentoServ.AmortizarProcessos(const AIdChaveProcesso: Integer);
begin
  TFastQuery.ExecutarScript(
    'UPDATE pessoa_credito_movimento set bo_processo_amortizado = ''S'' where id_chave_processo = :id_chave_processo',
    TArray<Variant>.Create(AIdChaveProcesso));
end;

end.




