unit uEquipamentoServ;

Interface

Uses uEquipamentoProxy, SysUtils;

type TEquipamentoServ = class
  class function GetEquipamento(
    const AIdEquipamento: Integer): TEquipamentoProxy;
  class function GerarEquipamento(
    const AEquipamento: TEquipamentoProxy): Integer;
  class function BuscarIdEquipamentoPelaDescricao(const ADescricaoEquipamento: String): Integer;
end;

implementation

{ TEquipamentoServ }

uses uFactoryQuery;

class function TEquipamentoServ.GetEquipamento(
    const AIdEquipamento: Integer): TEquipamentoProxy;
const
  SQL = 'SELECT * FROM EQUIPAMENTO WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TEquipamentoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdEquipamento));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FIdMarca := qConsulta.GetAsInteger('ID_MARCA');
  result.FIdModelo := qConsulta.GetAsInteger('ID_MODELO');
  result.FSerie := qConsulta.GetAsString('SERIE');
  result.FIdPessoa := qConsulta.GetAsInteger('ID_PESSOA');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FImei := qConsulta.GetAsString('IMEI');
end;

class function TEquipamentoServ.BuscarIdEquipamentoPelaDescricao(
  const ADescricaoEquipamento: String): Integer;
const
  SQL = 'SELECT id FROM EQUIPAMENTO WHERE descricao = :descricao ';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(ADescricaoEquipamento));

  result := qConsulta.GetAsInteger('id');
end;

class function TEquipamentoServ.GerarEquipamento(
    const AEquipamento: TEquipamentoProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('EQUIPAMENTO');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', AEquipamento.FDescricao);

    if AEquipamento.FIdMarca > 0 then
    begin
      qEntidade.SetAsInteger('ID_MARCA', AEquipamento.FIdMarca);
    end;

    if AEquipamento.FIdModelo > 0 then
    begin
      qEntidade.SetAsInteger('ID_MODELO', AEquipamento.FIdModelo);
    end;
    qEntidade.SetAsString('SERIE', AEquipamento.FSerie);

    if AEquipamento.FIdPessoa > 0 then
    begin
      qEntidade.SetAsInteger('ID_PESSOA', AEquipamento.FIdPessoa);
    end;
    qEntidade.SetAsString('BO_ATIVO', AEquipamento.FBoAtivo);
    qEntidade.SetAsString('IMEI', AEquipamento.FImei);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AEquipamento.FID := qEntidade.GetAsInteger('ID');
    result :=     AEquipamento.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Equipamento.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


