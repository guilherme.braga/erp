unit uNcmServ;

Interface

Uses uNcmProxy;

type TNcmServ = class
  class function GetNcm(
    const AIdNcm: Integer): TNcmProxy;
end;

implementation

{ TNcmServ }

uses uFactoryQuery;

class function TNcmServ.GetNcm(
    const AIdNcm: Integer): TNcmProxy;
const
  SQL = 'SELECT * FROM ncm WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TNcmProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdNcm));

  result.FId := qConsulta.GetAsString('ID');
  result.FCodigo := qConsulta.GetAsInteger('CODIGO');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FUn := qConsulta.GetAsString('UN');
end;

end.


