﻿unit uIBPTServ;

Interface

Uses uIBPTProxy, SysUtils, ACBrIBPTax, Forms;

type TIBPTServ = class
  class function GetIbpt(const AIdIbpt: Integer): TIbptProxy;
  class function GerarIbpt(const AIbpt: TIbptProxy): Integer;
  class procedure AtualizarTabelaIBPT;
  class procedure AtualizarDataHoraConclusaoImportacao(const AIdIbpt: Integer);
end;

implementation

{ TIbptServ }

uses uFactoryQuery, uFrmComponentesFiscais;

class function TIbptServ.GetIbpt(
    const AIdIbpt: Integer): TIbptProxy;
const
  SQL = 'SELECT * FROM IBPT WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TIbptProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdIbpt));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FNcm := qConsulta.GetAsString('NCM');
  result.FExcesaoNcm := qConsulta.GetAsString('EXCESAO_NCM');
  result.FTabela := qConsulta.GetAsInteger('TABELA');
  result.FAliquotaNacional := qConsulta.GetAsFloat('ALIQUOTA_NACIONAL');
  result.FAliquotaInternacional := qConsulta.GetAsFloat('ALIQUOTA_INTERNACIONAL');
end;

class function TIbptServ.GerarIbpt(
    const AIbpt: TIbptProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try
    qEntidade := TFastQuery.ModoDeInclusao('IBPT');
    qEntidade.Incluir;

    qEntidade.SetAsString('NCM', AIbpt.FNcm);
    qEntidade.SetAsString('EXCESAO_NCM', AIbpt.FExcesaoNcm);
    qEntidade.SetAsInteger('TABELA', AIbpt.FTabela);
    qEntidade.SetAsFloat('ALIQUOTA_NACIONAL', AIbpt.FAliquotaNacional);
    qEntidade.SetAsFloat('ALIQUOTA_INTERNACIONAL', AIbpt.FAliquotaInternacional);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AIbpt.FID := qEntidade.GetAsInteger('ID');
    result :=     AIbpt.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Ibpt.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

class procedure TIbptServ.AtualizarTabelaIBPT;
var
  ACBrIBPTax: TACBrIBPTax;
  frmComponentesFiscais: TFrmComponentesFiscais;
  versaoArquivo: String;
  qIBPT, qIBPTVersao: IFastQuery;
  I: Integer;
begin
  Application.CreateForm(TFrmComponentesFiscais,frmComponentesFiscais);
  try
    ACBrIBPTax := frmComponentesFiscais.aCBrIBPTax;

    if ACBrIBPTax.DownloadTabela then
    begin
      // versăo do arquivo
      versaoArquivo := ACBrIBPTax.VersaoArquivo;

      qIBPTVersao := TFastQuery.ModoDeAlteracao('SELECT * FROM ibpt_versao WHERE versao = :versao',
        TArray<Variant>.Create(versaoArquivo));

      if not qIBPTVersao.IsEmpty then
      begin
        exit;
      end;

      qIBPTVersao.Incluir;
      qIBPTVersao.SetAsString('VERSAO', versaoArquivo);
      qIBPTVersao.SetAsDateTime('DH_INICIO_ATUALIZACAO', Now);
      qIBPTVersao.SetAsDateTime('DH_TERMINO_ATUALIZACAO', Now);
      qIBPTVersao.Salvar;
      qIBPTVersao.Persistir;

      qIBPT := TFastQuery.ModoDeInclusao('IBPT');

      for I := 0 to ACBrIBPTax.Itens.Count - 1 do
      begin
        qIBPT.Incluir;
        qIBPT.SetAsString('NCM', ACBrIBPTax.Itens[I].NCM);
        qIBPT.SetAsString('EXCESAO_NCM', ACBrIBPTax.Itens[I].Excecao);
        qIBPT.SetAsInteger('TABELA', 1{ACBrIBPTax.Itens[I].Tabela.tabNCM});
        qIBPT.SetAsFloat('ALIQUOTA_NACIONAL', ACBrIBPTax.Itens[I].FederalNacional);
        qIBPT.SetAsFloat('ALIQUOTA_INTERNACIONAL', ACBrIBPTax.Itens[I].FederalImportado);
        qIBPT.Salvar;
      end;
      qIBPT.Persistir;

      AtualizarDataHoraConclusaoImportacao(qIBPTVersao.GetAsInteger('ID'));
    end;
  finally
    FreeAndNil(frmComponentesFiscais);
  end;

  {     // configurar a URL do arquivo para ser baixado
      // Vocę configurar como quiser, para o seu site ou servidor proprios
      // deixarei a tabela atualizada sempre no endereço
      // https://regys.com.br/arquivos/AcspDeOlhoNoImpostoIbptV.0.0.1.csv
      {if Trim(URL) <> '' then
        ACBrIBPTax1.URLDownload := Trim(URL);}

      // se o path do arquivo năo for passado o componente tenta baixar da URL
      // informada na propriedade URLDownload
      // Se o path for passado ele abre diretamente o arquivo informado}
end;

class procedure TIbptServ.AtualizarDataHoraConclusaoImportacao(const AIdIbpt: Integer);
begin
  TFastQuery.ExecutarScript('UPDATE ibpt_versao SET DH_TERMINO_ATUALIZACAO = :termino'+
  ' where id = :id', TArray<Variant>.Create(Now, AIdIbpt));
end;

end.
