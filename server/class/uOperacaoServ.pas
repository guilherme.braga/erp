unit uOperacaoServ;

interface

Uses uFactoryQuery, SysUtils;

type TOperacaoServ = class
  class function BuscarOperacaoFiscal(const AIdOperacaoFiscal: Integer): TFastQuery;
  class function BuscarOperacaoComercialPelaOperacaoComercial(const AIdOperacaoFiscal: Integer): Integer;
  class function ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
  class function BuscarNaturezaOperacao(const AIdOperacao: Integer): String;
  class function BuscarCFOP(const AIdOperacaoFiscal: Integer; const AMercadoria: Boolean;
    const AIdImpostoICMS: Integer): Integer;
end;

implementation

{ TOperacaoServ }

uses uImpostoICMSServ, uImpostoICMSProxy, uCSTCSOSNServ, uCSTCSOSNProxy;

class function TOperacaoServ.ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
var qAcao: IFastQuery;
begin
  qAcao := TFastQuery.ModoDeConsulta(
    ' SELECT 1'+
    '   FROM operacao_item'+
    '  WHERE id_operacao = :id_operacao'+
    '    AND id_acao = (select id from acao where Upper(acao) = Upper(:acao))',
    TArray<Variant>.Create(AIdOperacao, AAcao));

  result := not qAcao.IsEmpty;
end;

class function TOperacaoServ.BuscarCFOP(const AIdOperacaoFiscal: Integer; const AMercadoria: Boolean;
  const AIdImpostoICMS: Integer): Integer;
var
  CFOPST: boolean;
  qOperacaoFiscal: IFastQuery;
  impostoICMS: TImpostoICMSProxy;
  cstICMS: TCstCsosnProxy;
  cst: String;
begin
  impostoICMS := TImpostoIcmsServ.GetImpostoIcms(AIdImpostoICMS);
  try
    cstICMS := TCstCsosnServ.GetCstCsosn(impostoICMS.FIdCstCsosn);
    try
      if not cstICMS.FCodigoCst.IsEmpty then
      begin
        cst := cstICMS.FCodigoCst;
      end
      else
      begin
        cst := cstICMS.FCodigoCsosn;
      end;

      CFOPST := (StrToIntDef(cst,0) in [10, 30, 60, 70, 201, 202, 203]) or
        ((impostoICMS.FPercMvaAjustadoSt > 0) and (StrToIntDef(cst, 0) = 90));

      qOperacaoFiscal := TOperacaoServ.BuscarOperacaoFiscal(AIdOperacaoFiscal);

      {
        CST ICMS
        10, 30, 60, 70, 90(mva) => CFOP ST

        OU

        CSOSN
        201, 202, 203 => CFOP ST

        CASO CONTRARIO PEGA A CFOP
      }


      if AMercadoria then
      begin
        if CFOPST then
        begin
          Result := qOperacaoFiscal.GetAsInteger('CFOP_MERCADORIA_ST');
        end
        else
        begin
          Result := qOperacaoFiscal.GetAsInteger('CFOP_MERCADORIA');
        end;
      end
      else
      begin
        if CFOPST then
        begin
          Result := qOperacaoFiscal.GetAsInteger('CFOP_PRODUTO_ST');
        end
        else
        begin
          Result := qOperacaoFiscal.GetAsInteger('CFOP_PRODUTO');
        end;
      end;
    finally
      FreeAndNil(cstICMS);
    end;
  finally
    FreeAndNil(impostoICMS);
  end;
end;

class function TOperacaoServ.BuscarNaturezaOperacao(const AIdOperacao: Integer): String;
var
  qOperacao: IFastQuery;
begin
  qOperacao := TFastQuery.ModoDeConsulta(
    ' SELECT natureza_operacao'+
    '   FROM operacao'+
    '  WHERE id = :id_operacao',
    TArray<Variant>.Create(AIdOperacao));

  result := qOperacao.GetAsString('natureza_operacao');
end;

class function TOperacaoServ.BuscarOperacaoComercialPelaOperacaoComercial(
  const AIdOperacaoFiscal: Integer): Integer;
var
  qOperacao: IFastQuery;
begin
  qOperacao := TFastQuery.ModoDeConsulta(
    ' SELECT id_operacao'+
    '   FROM operacao_fiscal'+
    '  WHERE id = :id_operacao',
    TArray<Variant>.Create(AIdOperacaoFiscal));

  result := qOperacao.GetAsInteger('id_operacao');
end;

class function TOperacaoServ.BuscarOperacaoFiscal(const AIdOperacaoFiscal: Integer): TFastQuery;
begin
  result := TFastQuery.ModoDeConsulta('SELECT * FROM OPERACAO_FISCAL WHERE ID = :ID',
    TArray<Variant>.Create(AIdOperacaoFiscal));
end;

end.
