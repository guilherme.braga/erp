unit uNcmEspecializadoServ;

Interface

Uses uNcmEspecializadoProxy;

type TNcmEspecializadoServ = class
  class function GetNcmEspecializado(
    const AIdNcmEspecializado: Integer): TNcmEspecializadoProxy;
end;

implementation

{ TNcmEspecializadoServ }

uses uFactoryQuery;

class function TNcmEspecializadoServ.GetNcmEspecializado(
    const AIdNcmEspecializado: Integer): TNcmEspecializadoProxy;
const
  SQL = 'SELECT * FROM ncm_especializado WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TNcmEspecializadoProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdNcmEspecializado));

  result.FId := qConsulta.GetAsString('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FIdNcm := qConsulta.GetAsInteger('ID_NCM');
end;

end.


