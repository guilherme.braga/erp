unit uConfiguracaoFiscalServ;

Interface

Uses uConfiguracaoFiscalProxy;

type TConfiguracaoFiscalServ = class
  class function GetConfiguracaoFiscal(
    const AIdConfiguracaoFiscal: Integer): TConfiguracaoFiscalProxy;
  class function GerarConfiguracaoFiscal(
    const AConfiguracaoFiscal: TConfiguracaoFiscalProxy): Integer;
  class function DuplicarConfiguracaoFiscal(const AIdConfiguracaoFiscalOrigem: Integer): Integer;
end;

implementation

{ TConfiguracaoFiscalServ }

uses uFactoryQuery, SysUtils, uDatasetUtilsServ;

class function TConfiguracaoFiscalServ.GetConfiguracaoFiscal(
    const AIdConfiguracaoFiscal: Integer): TConfiguracaoFiscalProxy;
const
  SQL = 'SELECT * FROM CONFIGURACAO_FISCAL WHERE id_filial = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TConfiguracaoFiscalProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdConfiguracaoFiscal));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
  result.FLogomarca := qConsulta.GetAsString('LOGOMARCA');
  result.FCertificadoCaminho := qConsulta.GetAsString('CERTIFICADO_CAMINHO');
  result.FCertificadoSenha := qConsulta.GetAsString('CERTIFICADO_SENHA');
  result.FCertificadoNumeroSerie := qConsulta.GetAsString('CERTIFICADO_NUMERO_SERIE');
  result.FNfeModoImpressao := qConsulta.GetAsString('NFE_MODO_IMPRESSAO');
  result.FNfeFormaEmissao := qConsulta.GetAsString('NFE_FORMA_EMISSAO');
  result.FNfeWebserviceUf := qConsulta.GetAsString('NFE_WEBSERVICE_UF');
  result.FNfeWebserviceAmbiente := qConsulta.GetAsString('NFE_WEBSERVICE_AMBIENTE');
  result.FNfeWebserviceVisualizarMensagem := qConsulta.GetAsString('NFE_WEBSERVICE_VISUALIZAR_MENSAGEM');
  result.FNfeProxyHost := qConsulta.GetAsString('NFE_PROXY_HOST');
  result.FNfeProxyPorta := qConsulta.GetAsInteger('NFE_PROXY_PORTA');
  result.FNfeProxyUsuario := qConsulta.GetAsString('NFE_PROXY_USUARIO');
  result.FNfeProxySenha := qConsulta.GetAsString('NFE_PROXY_SENHA');
  result.FEmailSmtp := qConsulta.GetAsString('EMAIL_SMTP');
  result.FEmailPorta := qConsulta.GetAsString('EMAIL_PORTA');
  result.FEmailUsuario := qConsulta.GetAsString('EMAIL_USUARIO');
  result.FEmailSenha := qConsulta.GetAsString('EMAIL_SENHA');
  result.FEmailAssunto := qConsulta.GetAsString('EMAIL_ASSUNTO');
  result.FEmailMensagem := qConsulta.GetAsString('EMAIL_MENSAGEM');
  result.FNfceModoImpressao := qConsulta.GetAsString('NFCE_MODO_IMPRESSAO');
  result.FNfceWebserviceUf := qConsulta.GetAsString('NFCE_WEBSERVICE_UF');
  result.FNfceWebserviceAmbiente := qConsulta.GetAsString('NFCE_WEBSERVICE_AMBIENTE');
  result.FNfceWebserviceVisualizarMensagem := qConsulta.GetAsString('NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM');
  result.FNfceProxyHost := qConsulta.GetAsString('NFCE_PROXY_HOST');
  result.FNfceProxyPorta := qConsulta.GetAsInteger('NFCE_PROXY_PORTA');
  result.FNfceProxyUsuario := qConsulta.GetAsString('NFCE_PROXY_USUARIO');
  result.FNfceProxySenha := qConsulta.GetAsString('NFCE_PROXY_SENHA');
  result.FIdFilial := qConsulta.GetAsInteger('ID_FILIAL');
  result.FNfeIdRelatorioFr := qConsulta.GetAsInteger('NFE_ID_RELATORIO_FR');
  result.FExpandirLogomarcar := qConsulta.GetAsString('EXPANDIR_LOGOMARCAR');
  result.FNfceIdCsc := qConsulta.GetAsString('NFCE_ID_CSC');
  result.FNfceCsc := qConsulta.GetAsString('NFCE_CSC');
  result.FArquivoPathLogs := qConsulta.GetAsString('ARQUIVO_PATH_LOGS');
  result.FArquivoPathSchemas := qConsulta.GetAsString('ARQUIVO_PATH_SCHEMAS');
  result.FArquivoPathNfe := qConsulta.GetAsString('ARQUIVO_PATH_NFE');
  result.FArquivoPathCancelamento := qConsulta.GetAsString('ARQUIVO_PATH_CANCELAMENTO');
  result.FArquivoPathInutilizacao := qConsulta.GetAsString('ARQUIVO_PATH_INUTILIZACAO');
  result.FArquivoPathDpec := qConsulta.GetAsString('ARQUIVO_PATH_DPEC');
  result.FArquivoPathEvento := qConsulta.GetAsString('ARQUIVO_PATH_EVENTO');
  result.FArquivoRelatorioNfceFr := qConsulta.GetAsString('ARQUIVO_RELATORIO_NFCE_FR');
  result.FArquivoEventoNfceFr := qConsulta.GetAsString('ARQUIVO_EVENTO_NFCE_FR');
  result.FArquivoRelatorioNfeFr := qConsulta.GetAsString('ARQUIVO_RELATORIO_NFE_FR');
  result.FArquivoEventoNfeFr := qConsulta.GetAsString('ARQUIVO_EVENTO_NFE_FR');
  result.FImpressoraNfe := qConsulta.GetAsString('IMPRESSORA_NFE');
  result.FImpressoraNfce := qConsulta.GetAsString('IMPRESSORA_NFCE');
  result.FArquivoPathPdf := qConsulta.GetAsString('ARQUIVO_PATH_PDF');
  result.FNfcePreview := qConsulta.GetAsString('NFCE_PREVIEW');
  result.FNfePreview := qConsulta.GetAsString('NFE_PREVIEW');
  result.FSalvarPdfJuntoXml := qConsulta.GetAsString('SALVAR_PDF_JUNTO_XML');
  result.FEmailEnviarNfe := qConsulta.GetAsString('EMAIL_ENVIAR_NFE');
  result.FEmailEnviarNfce := qConsulta.GetAsString('EMAIL_ENVIAR_NFCE');
  result.FEmailEmailHost := qConsulta.GetAsString('EMAIL_EMAIL_HOST');
  result.FEmailEmailDestino := qConsulta.GetAsString('EMAIL_EMAIL_DESTINO');
  result.FEmailEmailCc := qConsulta.GetAsString('EMAIL_EMAIL_CC');
  result.FEmailEmailCco := qConsulta.GetAsString('EMAIL_EMAIL_CCO');
  result.FEmailSsl := qConsulta.GetAsString('EMAIL_SSL');
  result.FEmailSolicitaConfirmacao := qConsulta.GetAsString('EMAIL_SOLICITA_CONFIRMACAO');
  result.FEmailUsarThread := qConsulta.GetAsString('EMAIL_USAR_THREAD');
  result.FEmailNomeOrigem := qConsulta.GetAsString('EMAIL_NOME_ORIGEM');
end;

class function TConfiguracaoFiscalServ.DuplicarConfiguracaoFiscal(
  const AIdConfiguracaoFiscalOrigem: Integer): Integer;
var
  queryOrigem: IFastQuery;
  queryDestino: IFastQuery;

  const CAMPOS_NAO_COPIAR: string = 'id';
begin
  queryOrigem := TFastQuery.ModoDeConsulta('SELECT * FROM CONFIGURACAO_FISCAL WHERE ID = :ID',
    TArray<Variant>.Create(AIdConfiguracaoFiscalOrigem));

  queryDestino := TFastQuery.ModoDeInclusao('CONFIGURACAO_FISCAL');

  //Novo Registro em outra conta corrente
  TDatasetUtilsServ.DuplicarRegistro(
    (queryOrigem as TFastQuery), //Origem
    (queryDestino as TFastQuery), //Destino
     [CAMPOS_NAO_COPIAR], //No Copy
    [], //Campos para mudar
    TArray<Variant>.Create(now) //Valores para Mudar
  );

  queryDestino.Persistir;

  result := queryDestino.GetAsInteger('ID');
end;

class function TConfiguracaoFiscalServ.GerarConfiguracaoFiscal(
    const AConfiguracaoFiscal: TConfiguracaoFiscalProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('CONFIGURACAO_FISCAL');
    qEntidade.Incluir;

    qEntidade.SetAsString('BO_ATIVO', AConfiguracaoFiscal.FBoAtivo);
    qEntidade.SetAsString('LOGOMARCA', AConfiguracaoFiscal.FLogomarca);
    qEntidade.SetAsString('CERTIFICADO_CAMINHO', AConfiguracaoFiscal.FCertificadoCaminho);
    qEntidade.SetAsString('CERTIFICADO_SENHA', AConfiguracaoFiscal.FCertificadoSenha);
    qEntidade.SetAsString('CERTIFICADO_NUMERO_SERIE', AConfiguracaoFiscal.FCertificadoNumeroSerie);
    qEntidade.SetAsString('NFE_MODO_IMPRESSAO', AConfiguracaoFiscal.FNfeModoImpressao);
    qEntidade.SetAsString('NFE_FORMA_EMISSAO', AConfiguracaoFiscal.FNfeFormaEmissao);
    qEntidade.SetAsString('NFE_WEBSERVICE_UF', AConfiguracaoFiscal.FNfeWebserviceUf);
    qEntidade.SetAsString('NFE_WEBSERVICE_AMBIENTE', AConfiguracaoFiscal.FNfeWebserviceAmbiente);
    qEntidade.SetAsString('NFE_WEBSERVICE_VISUALIZAR_MENSAGEM', AConfiguracaoFiscal.FNfeWebserviceVisualizarMensagem);
    qEntidade.SetAsString('NFE_PROXY_HOST', AConfiguracaoFiscal.FNfeProxyHost);
    qEntidade.SetAsInteger('NFE_PROXY_PORTA', AConfiguracaoFiscal.FNfeProxyPorta);
    qEntidade.SetAsString('NFE_PROXY_USUARIO', AConfiguracaoFiscal.FNfeProxyUsuario);
    qEntidade.SetAsString('NFE_PROXY_SENHA', AConfiguracaoFiscal.FNfeProxySenha);
    qEntidade.SetAsString('EMAIL_SMTP', AConfiguracaoFiscal.FEmailSmtp);
    qEntidade.SetAsString('EMAIL_PORTA', AConfiguracaoFiscal.FEmailPorta);
    qEntidade.SetAsString('EMAIL_USUARIO', AConfiguracaoFiscal.FEmailUsuario);
    qEntidade.SetAsString('EMAIL_SENHA', AConfiguracaoFiscal.FEmailSenha);
    qEntidade.SetAsString('EMAIL_ASSUNTO', AConfiguracaoFiscal.FEmailAssunto);
    qEntidade.SetAsString('EMAIL_MENSAGEM', AConfiguracaoFiscal.FEmailMensagem);
    qEntidade.SetAsString('NFCE_MODO_IMPRESSAO', AConfiguracaoFiscal.FNfceModoImpressao);
    qEntidade.SetAsString('NFCE_WEBSERVICE_UF', AConfiguracaoFiscal.FNfceWebserviceUf);
    qEntidade.SetAsString('NFCE_WEBSERVICE_AMBIENTE', AConfiguracaoFiscal.FNfceWebserviceAmbiente);
    qEntidade.SetAsString('NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM', AConfiguracaoFiscal.FNfceWebserviceVisualizarMensagem);
    qEntidade.SetAsString('NFCE_PROXY_HOST', AConfiguracaoFiscal.FNfceProxyHost);
    qEntidade.SetAsInteger('NFCE_PROXY_PORTA', AConfiguracaoFiscal.FNfceProxyPorta);
    qEntidade.SetAsString('NFCE_PROXY_USUARIO', AConfiguracaoFiscal.FNfceProxyUsuario);
    qEntidade.SetAsString('NFCE_PROXY_SENHA', AConfiguracaoFiscal.FNfceProxySenha);

    if AConfiguracaoFiscal.FIdFilial > 0 then
    begin
      qEntidade.SetAsInteger('ID_FILIAL', AConfiguracaoFiscal.FIdFilial);
    end;
    qEntidade.SetAsInteger('NFE_ID_RELATORIO_FR', AConfiguracaoFiscal.FNfeIdRelatorioFr);
    qEntidade.SetAsString('EXPANDIR_LOGOMARCAR', AConfiguracaoFiscal.FExpandirLogomarcar);
    qEntidade.SetAsString('NFCE_ID_CSC', AConfiguracaoFiscal.FNfceIdCsc);
    qEntidade.SetAsString('NFCE_CSC', AConfiguracaoFiscal.FNfceCsc);
    qEntidade.SetAsString('ARQUIVO_PATH_LOGS', AConfiguracaoFiscal.FArquivoPathLogs);
    qEntidade.SetAsString('ARQUIVO_PATH_SCHEMAS', AConfiguracaoFiscal.FArquivoPathSchemas);
    qEntidade.SetAsString('ARQUIVO_PATH_NFE', AConfiguracaoFiscal.FArquivoPathNfe);
    qEntidade.SetAsString('ARQUIVO_PATH_CANCELAMENTO', AConfiguracaoFiscal.FArquivoPathCancelamento);
    qEntidade.SetAsString('ARQUIVO_PATH_INUTILIZACAO', AConfiguracaoFiscal.FArquivoPathInutilizacao);
    qEntidade.SetAsString('ARQUIVO_PATH_DPEC', AConfiguracaoFiscal.FArquivoPathDpec);
    qEntidade.SetAsString('ARQUIVO_PATH_EVENTO', AConfiguracaoFiscal.FArquivoPathEvento);
    qEntidade.SetAsString('ARQUIVO_RELATORIO_NFCE_FR', AConfiguracaoFiscal.FArquivoRelatorioNfceFr);
    qEntidade.SetAsString('ARQUIVO_EVENTO_NFCE_FR', AConfiguracaoFiscal.FArquivoEventoNfceFr);
    qEntidade.SetAsString('ARQUIVO_RELATORIO_NFE_FR', AConfiguracaoFiscal.FArquivoRelatorioNfeFr);
    qEntidade.SetAsString('ARQUIVO_EVENTO_NFE_FR', AConfiguracaoFiscal.FArquivoEventoNfeFr);
    qEntidade.SetAsString('IMPRESSORA_NFE', AConfiguracaoFiscal.FImpressoraNfe);
    qEntidade.SetAsString('IMPRESSORA_NFCE', AConfiguracaoFiscal.FImpressoraNfce);
    qEntidade.SetAsString('ARQUIVO_PATH_PDF', AConfiguracaoFiscal.FArquivoPathPdf);
    qEntidade.SetAsString('NFCE_PREVIEW', AConfiguracaoFiscal.FNfcePreview);
    qEntidade.SetAsString('NFE_PREVIEW', AConfiguracaoFiscal.FNfePreview);
    qEntidade.SetAsString('SALVAR_PDF_JUNTO_XML', AConfiguracaoFiscal.FSalvarPdfJuntoXml);
    qEntidade.SetAsString('EMAIL_ENVIAR_NFE', AConfiguracaoFiscal.FEmailEnviarNfe);
    qEntidade.SetAsString('EMAIL_ENVIAR_NFCE', AConfiguracaoFiscal.FEmailEnviarNfce);
    qEntidade.SetAsString('EMAIL_EMAIL_HOST', AConfiguracaoFiscal.FEmailEmailHost);
    qEntidade.SetAsString('EMAIL_EMAIL_DESTINO', AConfiguracaoFiscal.FEmailEmailDestino);
    qEntidade.SetAsString('EMAIL_EMAIL_CC', AConfiguracaoFiscal.FEmailEmailCc);
    qEntidade.SetAsString('EMAIL_EMAIL_CCO', AConfiguracaoFiscal.FEmailEmailCco);
    qEntidade.SetAsString('EMAIL_SSL', AConfiguracaoFiscal.FEmailSsl);
    qEntidade.SetAsString('EMAIL_SOLICITA_CONFIRMACAO', AConfiguracaoFiscal.FEmailSolicitaConfirmacao);
    qEntidade.SetAsString('EMAIL_USAR_THREAD', AConfiguracaoFiscal.FEmailUsarThread);
    qEntidade.SetAsString('EMAIL_NOME_ORIGEM', AConfiguracaoFiscal.FEmailNomeOrigem);
    qEntidade.Salvar;
    qEntidade.Persistir;

    AConfiguracaoFiscal.FID := qEntidade.GetAsInteger('ID');
    result :=     AConfiguracaoFiscal.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar ConfiguracaoFiscal.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.


