unit uCstIpiServ;

Interface

Uses uCstIpiProxy;

type TCstIpiServ = class
  class function GetCstIpi(
    const AIdCstIpi: Integer): TCstIpiProxy;
end;

implementation

{ TCstIpiServ }

uses uFactoryQuery;

class function TCstIpiServ.GetCstIpi(
    const AIdCstIpi: Integer): TCstIpiProxy;
const
  SQL = 'SELECT * FROM cst_ipi WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCstIpiProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCstIpi));

  result.FId := qConsulta.GetAsString('ID');
  result.FCodigoCst := qConsulta.GetAsString('CODIGO_CST');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FCENQ := qConsulta.GetAsString('CENQ');
end;

end.


