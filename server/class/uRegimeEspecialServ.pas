unit uRegimeEspecialServ;

Interface

Uses uRegimeEspecialProxy;

type TRegimeEspecialServ = class
  class function GetRegimeEspecial(
    const AIdRegimeEspecial: Integer): TRegimeEspecialProxy;
end;

implementation

{ TRegimeEspecialServ }

uses uFactoryQuery;

class function TRegimeEspecialServ.GetRegimeEspecial(
    const AIdRegimeEspecial: Integer): TRegimeEspecialProxy;
const
  SQL = 'SELECT * FROM regime_especial WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TRegimeEspecialProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdRegimeEspecial));

  result.FId := qConsulta.GetAsString('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FTextoEspecial := qConsulta.GetAsString('TEXTO_ESPECIAL');
  result.FBoAtivo := qConsulta.GetAsString('BO_ATIVO');
end;

end.


