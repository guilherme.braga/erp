unit uCNAEServ;

Interface

Uses uCnaeProxy;

type TCnaeServ = class
  class function GetCnae(const AIdCnae: Integer): TCnaeProxy;
  class function GerarCnae(const ACnae: TCnaeProxy): Integer;
  class function BuscarCodigoCNAE(const AIdCNAE: Integer): Integer;
end;

implementation

{ TCnaeServ }

uses uFactoryQuery, SysUtils;

class function TCnaeServ.GetCnae(
    const AIdCnae: Integer): TCnaeProxy;
const
  SQL = 'SELECT * FROM CNAE WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  result := TCnaeProxy.Create;

  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCnae));

  result.FId := qConsulta.GetAsInteger('ID');
  result.FDescricao := qConsulta.GetAsString('DESCRICAO');
  result.FSequencia := qConsulta.GetAsString('SEQUENCIA');
  result.FSecao := qConsulta.GetAsString('SECAO');
  result.FNivel := qConsulta.GetAsInteger('NIVEL');
end;

class function TCnaeServ.BuscarCodigoCNAE(const AIdCNAE: Integer): Integer;
const
  SQL = ' SELECT REPLACE(REPLACE(REPLACE(SEQUENCIA,''.'', ''''), ''/'', ''''),''-'','''') as CNAE'+
        ' FROM CNAE WHERE id = :id ';
var
   qConsulta: IFastQuery;
begin
  qConsulta := TFastQuery.ModoDeConsulta(SQL,
    TArray<Variant>.Create(AIdCnae));

  result := StrtoIntDef(qConsulta.GetAsString('CNAE'), 0);
end;

class function TCnaeServ.GerarCnae(
    const ACnae: TCnaeProxy): Integer;
var
  qEntidade: IFastQuery;
begin
  try

    qEntidade := TFastQuery.ModoDeInclusao('CNAE');
    qEntidade.Incluir;

    qEntidade.SetAsString('DESCRICAO', ACnae.FDescricao);
    qEntidade.SetAsString('SEQUENCIA', ACnae.FSequencia);
    qEntidade.SetAsString('SECAO', ACnae.FSecao);
    qEntidade.SetAsInteger('NIVEL', ACnae.FNivel);
    qEntidade.Salvar;
    qEntidade.Persistir;

    ACnae.FID := qEntidade.GetAsInteger('ID');
    result :=     ACnae.FID;
  except
    on e : Exception do
    begin
      raise Exception.Create('Erro ao gerar Cnae.'+
        'Mensagem Original: '+e.message);
    end;
  end;
end;

end.

