unit uDmConnection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  Data.DB, FireDAC.Comp.Client, FireDAC.VCLUI.Wait, FireDAC.Stan.StorageJSON,
  FireDAC.Stan.StorageBin, FireDAC.Comp.UI, FMX.Dialogs, Forms;

type
  TDm = class(TDataModule)
    Connection: TFDConnection;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dm: TDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFrmServer, uIniFileServ;

{$R *.dfm}

procedure TDm.DataModuleCreate(Sender: TObject);
begin
  try
    Connection.Connected := false;

    Connection.Params.Clear;
    Connection.Params.Add('Database='+IniSystemFile.databaseERP);
    Connection.Params.Add('User_Name=root');
    Connection.Params.Add('Password=t23mhcr247');
    Connection.Params.Add('Server='+IniSystemFile.server);
    Connection.Params.Add('DriverID=MySQL');

    Connection.Connected := true;
  Except
    try
      Connection.Connected := false;

      Connection.Params.Clear;
      Connection.Params.Add('Database='+IniSystemFile.databaseERP);
      Connection.Params.Add('User_Name=root');
      Connection.Params.Add('Password=root');
      Connection.Params.Add('port='+IniSystemFile.porta);
      Connection.Params.Add('Server='+IniSystemFile.server);
      Connection.Params.Add('DriverID=MySQL');

      Connection.Connected := true;
    except
      on e:Exception do
      begin
        showMessage('Erro ao tentar estabelecer conex�o com o banco de dados: '+e.message);
        Application.Terminate;
      end;
    end;
  end;
end;

end.
