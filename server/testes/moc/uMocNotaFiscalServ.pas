unit uMocNotaFiscalServ;

interface

type TMocNotaFiscalServ = class
  class procedure MocGerarNotaFiscalDaVenda;
  class procedure MocGerarNotaFiscalDaVendaLoop;
  class procedure MocEmitirNFE;
  class procedure MocImprimirDocumentoFiscal;
end;

implementation

uses uNotaFiscalServ, uNotaFiscalEletronicaServ, SysUtils, Dialogs, uVendaProxy;

{ TMocNotaFiscalServ }

class procedure TMocNotaFiscalServ.MocGerarNotaFiscalDaVenda;
var
  idVenda: Integer;
  cpf: String;
  tipoDocumentoFiscal: String;
  vendaProxy: TVendaProxy;
begin
  vendaProxy := TVendaProxy.Create;

  vendaProxy.FIdContaCorrente := 1;
  vendaProxy.FIdFormaPagamentoDinheiro := 1;
  vendaProxy.FIdCarteira := 1;

  idVenda := StrToInt(InputBox('C�digo da Venda','Digite o c�digo da venda',''));
  cpf := InputBox('CPF','Digite o cpf','');
  tipoDocumentoFiscal := InputBox('Documento Fiscal','Digite o documento fiscal','NFCE');

  TNotaFiscalServ.GerarNotaFiscalDaVenda(idVenda, cpf, tipoDocumentoFiscal, vendaProxy);
end;

class procedure TMocNotaFiscalServ.MocGerarNotaFiscalDaVendaLoop;
var
  idVenda: Integer;
  tipoDocumentoFiscal: String;
  qtdeVendas: Integer;
  i: Integer;
begin
  idVenda := StrToInt(InputBox('C�digo da Venda','Digite o c�digo da venda',''));
  tipoDocumentoFiscal := InputBox('Documento Fiscal','Digite o documento fiscal','NFCE');
  qtdeVendas := StrtoIntDef(InputBox('Loop de Inser��o','Digite a quantidade de vendas a ser geradas', '100'), 0);
  for i := 1 to qtdeVendas do
  begin
    TNotaFiscalServ.GerarNotaFiscalDaVenda(idVenda, '', tipoDocumentoFiscal, nil);
  end;
end;

class procedure TMocNotaFiscalServ.MocEmitirNFE;
var
  idNotaFiscal: Integer;
  tipoDocumentoFiscal: String;
begin
  idNotaFiscal := StrToInt(InputBox('C�digo da Nota Fiscal','Digite o c�digo da Nota Fiscal',''));
  tipoDocumentoFiscal := InputBox('Documento Fiscal','Digite o documento fiscal','NFCE');

  ShowMessage(TNotaFiscalEletronicaServ.EmitirNFE(idNotaFiscal, tipoDocumentoFiscal));
end;

class procedure TMocNotaFiscalServ.MocImprimirDocumentoFiscal;
var
  idNotaFiscal: Integer;
  tipoDocumentoFiscal: String;
begin
  idNotaFiscal := StrToInt(InputBox('C�digo da Nota Fiscal','Digite o c�digo da Nota Fiscal',''));
  tipoDocumentoFiscal := InputBox('Documento Fiscal','Digite o documento fiscal','NFCE');

  TNotaFiscalEletronicaServ.ImprimirDocumentoFiscal(idNotaFiscal, tipoDocumentoFiscal);
end;

end.

