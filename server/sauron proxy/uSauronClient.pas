unit uSauronClient;

interface

uses
  System.SysUtils, System.Classes, uSauronClasses, Data.DBXDataSnap, IPPeerClient, Data.DBXCommon,
  Data.DbxHTTPLayer, Data.DB, Data.SqlExpr;

type
  TDmSauronClient = class(TDataModule)
    SQLConnection1: TSQLConnection;
  private
    FInstanceOwner: Boolean;
    FSMClient: TSMClient;
    function GetSMClient: TSMClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property SMClient: TSMClient read GetSMClient write FSMClient;

end;

var
  DmSauronClient: TDmSauronClient;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

constructor TDmSauronClient.Create(AOwner: TComponent);
begin
  inherited;
  FInstanceOwner := True;
end;

destructor TDmSauronClient.Destroy;
begin
  FSMClient.Free;
  inherited;
end;

function TDmSauronClient.GetSMClient: TSMClient;
begin
  if FSMClient = nil then
  begin
    SQLConnection1.Open;
    FSMClient:= TSMClient.Create(SQLConnection1.DBXConnection, FInstanceOwner);
  end;
  Result := FSMClient;
end;

end.
