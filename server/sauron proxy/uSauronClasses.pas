//
// Created by the DataSnap proxy generator.
// 25/09/2015 02:21:13
// 

unit uSauronClasses;

interface

uses System.JSON, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.DBXJSONReflect;

type
  TSMClient = class(TDSAdminClient)
  private
    FEchoStringCommand: TDBXCommand;
    FReverseStringCommand: TDBXCommand;
    FAutorizarCommand: TDBXCommand;
    FGetListaArquivosCommand: TDBXCommand;
    FAutorizarGestaoEmpresarialCommand: TDBXCommand;
    FInserirNovaMaquinaCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function Autorizar(AValue: string): string;
    function GetListaArquivos(AValue: string): string;
    function AutorizarGestaoEmpresarial(ARequisicao: string): Boolean;
    procedure InserirNovaMaquina(ARequisicao: TStringList);
  end;

implementation

function TSMClient.EchoString(Value: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FDBXConnection.CreateCommand;
    FEchoStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEchoStringCommand.Text := 'TSM.EchoString';
    FEchoStringCommand.Prepare;
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.ExecuteUpdate;
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TSMClient.ReverseString(Value: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FDBXConnection.CreateCommand;
    FReverseStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReverseStringCommand.Text := 'TSM.ReverseString';
    FReverseStringCommand.Prepare;
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.ExecuteUpdate;
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TSMClient.Autorizar(AValue: string): string;
begin
  if FAutorizarCommand = nil then
  begin
    FAutorizarCommand := FDBXConnection.CreateCommand;
    FAutorizarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAutorizarCommand.Text := 'TSM.Autorizar';
    FAutorizarCommand.Prepare;
  end;
  FAutorizarCommand.Parameters[0].Value.SetWideString(AValue);
  FAutorizarCommand.ExecuteUpdate;
  Result := FAutorizarCommand.Parameters[1].Value.GetWideString;
end;

function TSMClient.GetListaArquivos(AValue: string): string;
begin
  if FGetListaArquivosCommand = nil then
  begin
    FGetListaArquivosCommand := FDBXConnection.CreateCommand;
    FGetListaArquivosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetListaArquivosCommand.Text := 'TSM.GetListaArquivos';
    FGetListaArquivosCommand.Prepare;
  end;
  FGetListaArquivosCommand.Parameters[0].Value.SetWideString(AValue);
  FGetListaArquivosCommand.ExecuteUpdate;
  Result := FGetListaArquivosCommand.Parameters[1].Value.GetWideString;
end;

function TSMClient.AutorizarGestaoEmpresarial(ARequisicao: string): Boolean;
begin
  if FAutorizarGestaoEmpresarialCommand = nil then
  begin
    FAutorizarGestaoEmpresarialCommand := FDBXConnection.CreateCommand;
    FAutorizarGestaoEmpresarialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAutorizarGestaoEmpresarialCommand.Text := 'TSM.AutorizarGestaoEmpresarial';
    FAutorizarGestaoEmpresarialCommand.Prepare;
  end;
  FAutorizarGestaoEmpresarialCommand.Parameters[0].Value.SetWideString(ARequisicao);
  FAutorizarGestaoEmpresarialCommand.ExecuteUpdate;
  Result := FAutorizarGestaoEmpresarialCommand.Parameters[1].Value.GetBoolean;
end;

procedure TSMClient.InserirNovaMaquina(ARequisicao: TStringList);
begin
  if FInserirNovaMaquinaCommand = nil then
  begin
    FInserirNovaMaquinaCommand := FDBXConnection.CreateCommand;
    FInserirNovaMaquinaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FInserirNovaMaquinaCommand.Text := 'TSM.InserirNovaMaquina';
    FInserirNovaMaquinaCommand.Prepare;
  end;
  if not Assigned(ARequisicao) then
    FInserirNovaMaquinaCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FInserirNovaMaquinaCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FInserirNovaMaquinaCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ARequisicao), True);
      if FInstanceOwner then
        ARequisicao.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FInserirNovaMaquinaCommand.ExecuteUpdate;
end;


constructor TSMClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMClient.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FAutorizarCommand.DisposeOf;
  FGetListaArquivosCommand.DisposeOf;
  FAutorizarGestaoEmpresarialCommand.DisposeOf;
  FInserirNovaMaquinaCommand.DisposeOf;
  inherited;
end;

end.
