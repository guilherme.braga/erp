object FrmServer: TFrmServer
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Servidor - Gest'#227'o Empresarial'
  ClientHeight = 87
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel1: TcxLabel
    Left = 0
    Top = 0
    Align = alClient
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clSilver
    Style.Font.Height = -21
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Transparent = True
    AnchorX = 335
    AnchorY = 44
  end
  object lbVersao: TcxLabel
    Left = 3
    Top = 0
    Style.TextColor = clWhite
    Transparent = True
  end
  object lbServer: TcxLabel
    Left = 3
    Top = 21
    Style.TextColor = clWhite
    Transparent = True
  end
  object lbPorta: TcxLabel
    Left = 3
    Top = 42
    Style.TextColor = clWhite
    Transparent = True
  end
  object lbStatus: TcxLabel
    Left = 3
    Top = 63
    Style.TextColor = clWhite
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object JvTrayIcon: TJvTrayIcon
    Active = True
    IconIndex = 0
    Visibility = [tvVisibleTaskBar, tvVisibleTaskList, tvAutoHide, tvRestoreDbClick, tvMinimizeDbClick]
    Left = 264
  end
end
