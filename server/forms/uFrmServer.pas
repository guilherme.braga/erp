unit uFrmServer;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, JvComponentBase, JvTrayIcon, cxLabel,IdHTTPWebBrokerBridge,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TFrmServer = class(TForm)
    cxLabel1: TcxLabel;
    JvTrayIcon: TJvTrayIcon;
    lbVersao: TcxLabel;
    lbServer: TcxLabel;
    lbPorta: TcxLabel;
    lbStatus: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CriaChaveDeRegistroPrimeiraConexaoComInternet;
  public
    { Public declarations }
    FServerIndoor : TIdHTTPWebBrokerBridge;
    procedure StartServer;
    procedure StopServer;
    procedure StartServerIndoor;
    procedure StopServerIndoor;
    procedure SetDadosServidor;
    procedure VerificarLicenca;
    function ExisteConexaoComInternet: Boolean;
  end;

var
  FrmServer: TFrmServer;

implementation

{$R *.dfm}

uses
  Datasnap.DSSession, uIniFileServ, uVersao, uServerContainer, uSeguranca, WinInet, uMocNotaFiscalServ;

{ TFrmServer }

procedure TerminateThreads;
begin
  if TDSSessionManager.Instance <> nil then
    TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TFrmServer.CriaChaveDeRegistroPrimeiraConexaoComInternet;
begin
{var
 Reg: TRegistry;
begin
 Reg := TRegistry.Create;
 Try
  Reg.RootKey := HKEY_CURRENT_USER;
  Reg.OpenKey('Control Panel\Desktop', False);
  If Reg.ValueExists('Wallpaper') then
  begin
   ShowMessage('Valor existe no registro.');
  end
  Else
  begin
   ShowMessage('Valor n�o existe no registro.');
   Reg.WriteString('Wallpaper', 'C:\meupapeldeparede.bmp');
  end;
 Finally
  Reg.Free;
 end;}
end;

function TFrmServer.ExisteConexaoComInternet: Boolean;
var
  dwConnectionTypes: Integer;
begin
  try
    Result := InternetCheckConnection('http://www.google.com.br/', 1, 0);
  except
    Result := false;
  end;
end;

procedure TFrmServer.FormCreate(Sender: TObject);
begin
  VerificarLicenca;
  StartServer;
  SetDadosServidor;
  //TMocNotaFiscalServ.MocGerarNotaFiscalDaVenda;
end;

procedure TFrmServer.StartServerIndoor;
begin
  FServerIndoor := TIdHTTPWebBrokerBridge.Create(Self);
  if not FServerIndoor.Active then
  begin
    FServerIndoor.Bindings.Clear;
    FServerIndoor.DefaultPort := IniSystemFile.IndoorPort;
    FServerIndoor.Active := True;
  end;
end;

procedure TFrmServer.StopServerIndoor;
begin
  if Assigned(FServerIndoor) then
  begin
    FServerIndoor.Active := False;
    FServerIndoor.Bindings.Clear;
    FreeAndNil(FServerIndoor);
  end;
end;

procedure TFrmServer.VerificarLicenca;
var
  maquinaOfflineAutorizada: Boolean;
begin
  if ExisteConexaoComInternet then
  begin
    if not TSeguranca.MaquinaLiberada then
    begin
      TSeguranca.DesautorizarMaquinaOffline;
      ShowMessage('Falha ao validar seu certificado de licen�a de uso do software.'+#13+
        'Entre em contato com a Kratos Tecnologia.');
      FatalExit(0);
    end
    else
    begin
      TSeguranca.AutorizarMaquinaOffline;
    end;
  end
  else
  begin
    maquinaOfflineAutorizada := TSeguranca.MaquinaAutorizadaOffline;
    if not(maquinaOfflineAutorizada) or (maquinaOfflineAutorizada and (not TSeguranca.AcessoOfflineValido)) then
    begin
      ShowMessage('Falha ao validar seu certificado de licen�a de uso do software. '+#13+
        'Tente conectar a internet para restabelecer a autoriza��o com nossos servidores,'+#13+
        'ou entre em contato com a Kratos Tecnologia.');
      FatalExit(0);
    end
    else
    begin
      TSeguranca.IncrementarAcessosOffline;
    end;
  end;
end;

procedure TFrmServer.FormDestroy(Sender: TObject);
begin
  StopServer;
end;

procedure TFrmServer.SetDadosServidor;
begin
  Self.Caption := IniSystemFile.RotuloServidor;
  lbVersao.Caption := 'Vers�o do Servidor: '+TVersao.GetExeVersion(Application.ExeName);
  lbServer.Caption := 'Servidor de Banco de Dados: '+IniSystemFile.server+' : '+IniSystemFile.porta;
  lbPorta.Caption := 'Porta da Aplica��o: '+InttoStr(ServerContainer.DSTCPServerTransport.Port);
  lbStatus.Caption := 'Servidor Online'
end;

procedure TFrmServer.StartServer;
begin
  if IniSystemFile.IndoorPort > 0 then
  begin
    StartServerIndoor;
  end;
end;

procedure TFrmServer.StopServer;
begin
  TerminateThreads;

  if IniSystemFile.IndoorPort > 0 then
  begin
    StopServerIndoor;
  end;
end;

end.

