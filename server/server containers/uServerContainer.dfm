object ServerContainer: TServerContainer
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 536
  Width = 880
  object DSServer: TDSServer
    AutoStart = False
    Left = 40
    Top = 11
  end
  object DSTCPServerTransport: TDSTCPServerTransport
    Server = DSServer
    Filters = <
      item
        FilterId = 'ZLibCompression'
        Properties.Strings = (
          'CompressMoreThan=1024')
      end>
    Left = 136
    Top = 12
  end
  object SCLocalidade: TDSServerClass
    OnGetClass = SCLocalidadeGetClass
    Server = DSServer
    Left = 40
    Top = 67
  end
  object SCMethods: TDSServerClass
    OnGetClass = SCMethodsGetClass
    Server = DSServer
    Left = 40
    Top = 131
  end
  object SCPessoa: TDSServerClass
    OnGetClass = SCPessoaGetClass
    Server = DSServer
    Left = 40
    Top = 187
  end
  object SCFinanceiro: TDSServerClass
    OnGetClass = SCFinanceiroGetClass
    Server = DSServer
    Left = 40
    Top = 243
  end
  object SCIndoor: TDSServerClass
    OnGetClass = SCIndoorGetClass
    Server = DSServer
    Left = 40
    Top = 304
  end
  object SCRestaurante: TDSServerClass
    OnGetClass = SCRestauranteGetClass
    Server = DSServer
    Left = 40
    Top = 411
  end
  object SCProduto: TDSServerClass
    OnGetClass = SCProdutoGetClass
    Server = DSServer
    Left = 40
    Top = 355
  end
  object SCChaveProcesso: TDSServerClass
    OnGetClass = SCChaveProcessoGetClass
    Server = DSServer
    Left = 128
    Top = 67
  end
  object SCEmpresaFilial: TDSServerClass
    OnGetClass = SCEmpresaFilialGetClass
    Server = DSServer
    Left = 128
    Top = 123
  end
  object SCNotaFiscal: TDSServerClass
    OnGetClass = SCNotaFiscalGetClass
    Server = DSServer
    Left = 128
    Top = 187
  end
  object SCOperacao: TDSServerClass
    OnGetClass = SCOperacaoGetClass
    Server = DSServer
    Left = 128
    Top = 251
  end
  object SCTabelaPreco: TDSServerClass
    OnGetClass = SCTabelaPrecoGetClass
    Server = DSServer
    Left = 128
    Top = 307
  end
  object SCContaReceber: TDSServerClass
    OnGetClass = SCContaReceberGetClass
    Server = DSServer
    Left = 128
    Top = 363
  end
  object SCContaPagar: TDSServerClass
    OnGetClass = SCContaPagarGetClass
    Server = DSServer
    Left = 128
    Top = 411
  end
  object SCGeracaoDocumento: TDSServerClass
    OnGetClass = SCGeracaoDocumentoGetClass
    Server = DSServer
    Left = 240
    Top = 67
  end
  object SCVenda: TDSServerClass
    OnGetClass = SCVendaGetClass
    Server = DSServer
    Left = 240
    Top = 123
  end
  object SCRelatorioFR: TDSServerClass
    OnGetClass = SCRelatorioFRGetClass
    Server = DSServer
    Left = 240
    Top = 179
  end
  object SCAjusteEstoque: TDSServerClass
    OnGetClass = SCAjusteEstoqueGetClass
    Server = DSServer
    Left = 240
    Top = 243
  end
  object SCMotivo: TDSServerClass
    OnGetClass = SCMotivoGetClass
    Server = DSServer
    Left = 240
    Top = 299
  end
  object SCCheque: TDSServerClass
    OnGetClass = SCChequeGetClass
    Server = DSServer
    Left = 240
    Top = 363
  end
  object SCServico: TDSServerClass
    OnGetClass = SCServicoGetClass
    Server = DSServer
    Left = 240
    Top = 419
  end
  object SCChecklist: TDSServerClass
    OnGetClass = SCChecklistGetClass
    Server = DSServer
    Left = 336
    Top = 67
  end
  object SCEquipamento: TDSServerClass
    OnGetClass = SCEquipamentoGetClass
    Server = DSServer
    Left = 344
    Top = 131
  end
  object SCSituacao: TDSServerClass
    OnGetClass = SCSituacaoGetClass
    Server = DSServer
    Left = 336
    Top = 195
  end
  object SCOrdemServico: TDSServerClass
    OnGetClass = SCOrdemServicoGetClass
    Server = DSServer
    Left = 336
    Top = 251
  end
  object SCLotePagamento: TDSServerClass
    OnGetClass = SCLotePagamentoGetClass
    Server = DSServer
    Left = 336
    Top = 315
  end
  object SCLoteRecebimento: TDSServerClass
    OnGetClass = SCLoteRecebimentoGetClass
    Server = DSServer
    Left = 336
    Top = 379
  end
  object SCCNAE: TDSServerClass
    OnGetClass = SCCNAEGetClass
    Server = DSServer
    Left = 432
    Top = 67
  end
  object SCCor: TDSServerClass
    OnGetClass = SCCorGetClass
    Server = DSServer
    Left = 432
    Top = 123
  end
  object SCOperadoraCartao: TDSServerClass
    OnGetClass = SCOperadoraCartaoGetClass
    Server = DSServer
    Left = 432
    Top = 179
  end
  object SCVeiculo: TDSServerClass
    OnGetClass = SCVeiculoGetClass
    Server = DSServer
    Left = 432
    Top = 235
  end
  object SMCreditoPessoa: TDSServerClass
    OnGetClass = SMCreditoPessoaGetClass
    Server = DSServer
    Left = 432
    Top = 291
  end
  object SMNCM: TDSServerClass
    OnGetClass = SMNCMGetClass
    Server = DSServer
    Left = 432
    Top = 347
  end
  object SMCarteira: TDSServerClass
    OnGetClass = SMCarteiraGetClass
    Server = DSServer
    Left = 432
    Top = 411
  end
  object SMCSOSN: TDSServerClass
    OnGetClass = SMCSOSNGetClass
    Server = DSServer
    Left = 512
    Top = 467
  end
  object SMRegimeEspecial: TDSServerClass
    OnGetClass = SMRegimeEspecialGetClass
    Server = DSServer
    Left = 512
    Top = 179
  end
  object SMSituacaoEspecial: TDSServerClass
    OnGetClass = SMSituacaoEspecialGetClass
    Server = DSServer
    Left = 512
    Top = 243
  end
  object SMZoneamento: TDSServerClass
    OnGetClass = SMZoneamentoGetClass
    Server = DSServer
    Left = 512
    Top = 299
  end
  object SMReceitaOtica: TDSServerClass
    OnGetClass = SMReceitaOticaGetClass
    Server = DSServer
    Left = 512
    Top = 123
  end
  object SMMedico: TDSServerClass
    OnGetClass = SMMedicoGetClass
    Server = DSServer
    Left = 512
    Top = 67
  end
  object SCRegraImposto: TDSServerClass
    OnGetClass = SCRegraImpostoGetClass
    Server = DSServer
    Left = 512
    Top = 355
  end
  object SCCSTIPI: TDSServerClass
    OnGetClass = SCCSTIPIGetClass
    Server = DSServer
    Left = 512
    Top = 411
  end
  object SCCSTPISCofins: TDSServerClass
    OnGetClass = SCCSTPISCofinsGetClass
    Server = DSServer
    Left = 592
    Top = 67
  end
  object SCImpostoIPI: TDSServerClass
    OnGetClass = SCImpostoIPIGetClass
    Server = DSServer
    Left = 592
    Top = 123
  end
  object SCImpostoICMS: TDSServerClass
    OnGetClass = SCImpostoICMSGetClass
    Server = DSServer
    Left = 600
    Top = 179
  end
  object SCImpostoPISCofins: TDSServerClass
    OnGetClass = SCImpostoPISCofinsGetClass
    Server = DSServer
    Left = 608
    Top = 243
  end
  object SCConfiguracaoFiscal: TDSServerClass
    OnGetClass = SCConfiguracaoFiscalGetClass
    Server = DSServer
    Left = 608
    Top = 307
  end
  object SCInventario: TDSServerClass
    OnGetClass = SCInventarioGetClass
    Server = DSServer
    Left = 608
    Top = 363
  end
  object SCFluxoCaixaResumido: TDSServerClass
    OnGetClass = SCFluxoCaixaResumidoGetClass
    Server = DSServer
    Left = 608
    Top = 419
  end
  object SCMovimentacaoCheque: TDSServerClass
    OnGetClass = SCMovimentacaoChequeGetClass
    Server = DSServer
    Left = 608
    Top = 475
  end
  object SCMontadora: TDSServerClass
    OnGetClass = SCMontadoraGetClass
    Server = DSServer
    Left = 728
    Top = 67
  end
  object SCModeloMontadora: TDSServerClass
    OnGetClass = SCModeloMontadoraGetClass
    Server = DSServer
    Left = 728
    Top = 123
  end
  object SCNegociacaoContaReceber: TDSServerClass
    OnGetClass = SCNegociacaoContaReceberGetClass
    Server = DSServer
    Left = 728
    Top = 187
  end
  object SCTamanho: TDSServerClass
    OnGetClass = SCTamanhoGetClass
    Server = DSServer
    Left = 736
    Top = 259
  end
  object SCGradeProduto: TDSServerClass
    OnGetClass = SCGradeProdutoGetClass
    Server = DSServer
    Left = 736
    Top = 315
  end
  object SCBackup: TDSServerClass
    OnGetClass = SCBackupGetClass
    Server = DSServer
    Left = 736
    Top = 371
  end
  object SCCFOP: TDSServerClass
    OnGetClass = SCCFOPGetClass
    Server = DSServer
    Left = 736
    Top = 427
  end
  object SCBloqueioPersonalizado: TDSServerClass
    OnGetClass = SCBloqueioPersonalizadoGetClass
    Server = DSServer
    Left = 736
    Top = 483
  end
  object SCTipoRestricao: TDSServerClass
    OnGetClass = SCTipoRestricaoGetClass
    Server = DSServer
    Left = 808
    Top = 67
  end
end
