unit uServerContainer;

interface

uses System.SysUtils, System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer, Datasnap.DSCommonServer,
  Datasnap.DSAuth, IPPeerServer, DbxCompressionFilter;

type
  TServerContainer = class(TDataModule)
    DSServer: TDSServer;
    DSTCPServerTransport: TDSTCPServerTransport;
    SCLocalidade: TDSServerClass;
    SCMethods: TDSServerClass;
    SCPessoa: TDSServerClass;
    SCFinanceiro: TDSServerClass;
    SCIndoor: TDSServerClass;
    SCRestaurante: TDSServerClass;
    SCProduto: TDSServerClass;
    SCChaveProcesso: TDSServerClass;
    SCEmpresaFilial: TDSServerClass;
    SCNotaFiscal: TDSServerClass;
    SCOperacao: TDSServerClass;
    SCTabelaPreco: TDSServerClass;
    SCContaReceber: TDSServerClass;
    SCContaPagar: TDSServerClass;
    SCGeracaoDocumento: TDSServerClass;
    SCVenda: TDSServerClass;
    SCRelatorioFR: TDSServerClass;
    SCAjusteEstoque: TDSServerClass;
    SCMotivo: TDSServerClass;
    SCCheque: TDSServerClass;
    SCServico: TDSServerClass;
    SCChecklist: TDSServerClass;
    SCEquipamento: TDSServerClass;
    SCSituacao: TDSServerClass;
    SCOrdemServico: TDSServerClass;
    SCLotePagamento: TDSServerClass;
    SCLoteRecebimento: TDSServerClass;
    SCCNAE: TDSServerClass;
    SCCor: TDSServerClass;
    SCOperadoraCartao: TDSServerClass;
    SCVeiculo: TDSServerClass;
    SMCreditoPessoa: TDSServerClass;
    SMNCM: TDSServerClass;
    SMCarteira: TDSServerClass;
    SMCSOSN: TDSServerClass;
    SMRegimeEspecial: TDSServerClass;
    SMSituacaoEspecial: TDSServerClass;
    SMZoneamento: TDSServerClass;
    SMReceitaOtica: TDSServerClass;
    SMMedico: TDSServerClass;
    SCRegraImposto: TDSServerClass;
    SCCSTIPI: TDSServerClass;
    SCCSTPISCofins: TDSServerClass;
    SCImpostoIPI: TDSServerClass;
    SCImpostoICMS: TDSServerClass;
    SCImpostoPISCofins: TDSServerClass;
    SCConfiguracaoFiscal: TDSServerClass;
    SCInventario: TDSServerClass;
    SCFluxoCaixaResumido: TDSServerClass;
    SCMovimentacaoCheque: TDSServerClass;
    SCMontadora: TDSServerClass;
    SCModeloMontadora: TDSServerClass;
    SCNegociacaoContaReceber: TDSServerClass;
    SCTamanho: TDSServerClass;
    SCGradeProduto: TDSServerClass;
    SCBackup: TDSServerClass;
    SCCFOP: TDSServerClass;
    SCBloqueioPersonalizado: TDSServerClass;
    SCTipoRestricao: TDSServerClass;
    procedure SCMethodsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCLocalidadeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCPessoaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCFinanceiroGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCIndoorGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCRestauranteGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCProdutoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCExtratoPlanoContaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCChaveProcessoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCEmpresaFilialGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCNotaFiscalGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCOperacaoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCTabelaPrecoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCContaReceberGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCContaPagarGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCGeracaoDocumentoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCVendaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCRelatorioFRGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCChequeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCMotivoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCAjusteEstoqueGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCServicoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCChecklistGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCEquipamentoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCSituacaoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCOrdemServicoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCLotePagamentoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCLoteRecebimentoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCCNAEGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCCorGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCOperadoraCartaoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCVeiculoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMCreditoPessoaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMNCMGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMCarteiraGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMCSOSNGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMRegimeEspecialGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMSituacaoEspecialGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMZoneamentoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMMedicoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SMReceitaOticaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCRegraImpostoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCCSTIPIGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCCSTCSOSNGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCCSTPISCofinsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCImpostoIPIGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCImpostoICMSGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCImpostoPISCofinsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCConfiguracaoFiscalGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCInventarioGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCFluxoCaixaResumidoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure DataModuleCreate(Sender: TObject);
    procedure SCMovimentacaoChequeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCMontadoraGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCModeloMontadoraGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCNegociacaoContaReceberGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCGradeProdutoGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCTamanhoGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCBackupGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCCFOPGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure SCBloqueioPersonalizadoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure SCTipoRestricaoGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  ServerContainer: TServerContainer;

implementation

{$R *.dfm}

uses Winapi.Windows, uServerMethods, uSMLocalidade, uSMPessoa, uSMFinanceiro,
  uSMIndoor, uSMRestaurante, uSMProduto, uSMExtratoPlanoConta, uSMChaveProcesso,
  uSMEmpresaFilial, uSMNotaFiscal, uSMOperacao, uSMTabelaPreco, uSMContaPagar,
  uSMContaReceber, uSMGeracaoDocumento, uSMVenda, uSMRelatorioFR,
  uSMAjusteEstoque, uSMCheque, uSMMotivo, uSMChecklist, uSMEquipamento,
  uSMOrdemServico, uSMServico, uSMSituacao, uSMLotePagamento,
  uSMLoteRecebimento, uSMCNAE, uSMCor, uSMCreditoPessoa, uSMOperadoraCartao,
  uSMVeiculo, uSMCarteira, uSMCSOSN, uSMNCM, uSMRegimeEspecial,
  uSMSituacaoEspecial, uSMZoneamento, uSMMedico, uSMReceitaOtica,
  uSMConfiguracoesFiscais, uSMCSTIPI, uSMCSTPISCofins, uSMImpostoICMS,
  uSMImpostoIPI, uSMImpostoPisCofins, uSMRegraImposto, uSMInventario, uSMFluxoCaixaResumido, uIniFileServ,
  uSMMovimentacaoCheque, uSMModeloMontadora, uSMMontadora, uSMNegociacaoContaReceber, uSMGradeproduto,
  uSMTamanho, uSMBackup, uSMCFOP, uSMBloqueioPersonalizado, uSMTipoRestricao;

procedure TServerContainer.DataModuleCreate(Sender: TObject);
begin
  DSTCPServerTransport.Port := IniSystemFile.portaDatasnap;
  DSServer.Start;
end;

procedure TServerContainer.SCAjusteEstoqueGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMAjusteEstoque.TSMAjusteEstoque;
end;

procedure TServerContainer.SCBackupGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMBackup.TSMBackup;
end;

procedure TServerContainer.SCBloqueioPersonalizadoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMBloqueioPersonalizado.TSMBloqueioPersonalizado;
end;

procedure TServerContainer.SCCFOPGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCFOP.TSMCFOP;
end;

procedure TServerContainer.SCChaveProcessoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMChaveProcesso.TSMChaveProcesso;
end;

procedure TServerContainer.SCChecklistGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMChecklist.TSMChecklist;
end;

procedure TServerContainer.SCChequeGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCheque.TSMCheque;
end;

procedure TServerContainer.SCCNAEGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCNAE.TSMCNAE;
end;

procedure TServerContainer.SCConfiguracaoFiscalGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMConfiguracoesFiscais.TSMConfiguracoesFiscais;
end;

procedure TServerContainer.SCContaPagarGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMContaPagar.TSMContaPagar;
end;

procedure TServerContainer.SCContaReceberGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMContaReceber.TSMContaReceber;
end;

procedure TServerContainer.SCCorGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCor.TSMCor;
end;

procedure TServerContainer.SCCSTCSOSNGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCSOSN.TSMCSOSN;
end;

procedure TServerContainer.SCCSTIPIGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCSTIPI.TSMCSTIPI;
end;

procedure TServerContainer.SCCSTPISCofinsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCSTPISCofins.TSMCSTPISCofins;
end;

procedure TServerContainer.SCEmpresaFilialGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMEmpresaFilial.TSMEmpresaFilial;
end;

procedure TServerContainer.SCEquipamentoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMEquipamento.TSMEquipamento;
end;

procedure TServerContainer.SCExtratoPlanoContaGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMExtratoPlanoConta.TSMExtratoPlanoConta;
end;

procedure TServerContainer.SCFinanceiroGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMFinanceiro.TSMFinanceiro;
end;

procedure TServerContainer.SCFluxoCaixaResumidoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMFluxoCaixaResumido.TSMFluxoCaixaResumido;
end;

procedure TServerContainer.SCGeracaoDocumentoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMGeracaoDocumento.TSMGeracaoDocumento;
end;

procedure TServerContainer.SCImpostoICMSGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMImpostoICMS.TSMImpostoICMS;
end;

procedure TServerContainer.SCImpostoIPIGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMImpostoIPI.TSMImpostoIPI;
end;

procedure TServerContainer.SCImpostoPISCofinsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMImpostoPISCofins.TSMImpostoPISCofins;
end;

procedure TServerContainer.SCIndoorGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMIndoor.TSMIndoor;
end;

procedure TServerContainer.SCInventarioGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMInventario.TSMInventario;
end;

procedure TServerContainer.SCLocalidadeGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMLocalidade.TSMLocalidade;
end;

procedure TServerContainer.SCLotePagamentoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMLotePagamento.TSMLotePagamento;
end;

procedure TServerContainer.SCLoteRecebimentoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSmLoteRecebimento.TSMLoteRecebimento;
end;

procedure TServerContainer.SCMethodsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uServerMethods.TServerMethods;
end;

procedure TServerContainer.SCModeloMontadoraGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMModeloMontadora.TSMModeloMontadora;
end;

procedure TServerContainer.SCMontadoraGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMMontadora.TSMMontadora;
end;

procedure TServerContainer.SCMotivoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMMotivo.TSMMotivo;
end;

procedure TServerContainer.SCMovimentacaoChequeGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMMovimentacaoCheque.TSMMovimentacaoCheque;
end;

procedure TServerContainer.SCNegociacaoContaReceberGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMNegociacaoContaReceber.TSMNegociacaoContaReceber;
end;

procedure TServerContainer.SCNotaFiscalGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMNotaFiscal.TSMNotaFiscal;
end;

procedure TServerContainer.SCOperacaoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMOperacao.TSMOperacao;
end;

procedure TServerContainer.SCOperadoraCartaoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMOperadoraCartao.TSMOperadoraCartao;
end;

procedure TServerContainer.SCOrdemServicoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMOrdemServico.TSMOrdemServico;
end;

procedure TServerContainer.SCPessoaGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMPessoa.TSMPessoa;
end;

procedure TServerContainer.SCProdutoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMProduto.TSMProduto;
end;

procedure TServerContainer.SCGradeProdutoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMGradeProduto.TSMGradeProduto;
end;

procedure TServerContainer.SCRegraImpostoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMRegraImposto.TSMRegraImposto;
end;

procedure TServerContainer.SCRelatorioFRGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMRelatorioFR.TSMRelatorioFR;
end;

procedure TServerContainer.SCRestauranteGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMRestaurante.TSMRestaurante;
end;

procedure TServerContainer.SCServicoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMServico.TSMServico;
end;

procedure TServerContainer.SCSituacaoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMSituacao.TSMSituacao;
end;

procedure TServerContainer.SCTabelaPrecoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMTabelaPreco.TSMTabelaPreco;
end;

procedure TServerContainer.SCTamanhoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMTamanho.TSMTamanho;
end;

procedure TServerContainer.SCTipoRestricaoGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMTipoRestricao.TSMTipoRestricao;
end;

procedure TServerContainer.SCVeiculoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMVeiculo.TSMVeiculo;
end;

procedure TServerContainer.SCVendaGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMVenda.TSMVenda;
end;

procedure TServerContainer.SMCarteiraGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCarteira.TSMCarteira;
end;

procedure TServerContainer.SMCreditoPessoaGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCreditoPessoa.TSMCreditoPessoa;
end;

procedure TServerContainer.SMCSOSNGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMCSOSN.TSMCSOSN;
end;

procedure TServerContainer.SMMedicoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMMedico.TSMMedico;
end;

procedure TServerContainer.SMNCMGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMNCM.TSMNCM;
end;

procedure TServerContainer.SMReceitaOticaGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMReceitaOtica.TSMReceitaOtica;
end;

procedure TServerContainer.SMRegimeEspecialGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMRegimeEspecial.TSMRegimeEspecial;
end;

procedure TServerContainer.SMSituacaoEspecialGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMSituacaoEspecial.TSMSituacaoEspecial;
end;

procedure TServerContainer.SMZoneamentoGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := uSMZoneamento.TSMZoneamento;
end;

end.

