object SMImpostoIPI: TSMImpostoIPI
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqImpostoIPI: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ii.* '
      
        '       ,(SELECT descricao FROM cst_ipi WHERE id = ii.id_cst_ipi)' +
        ' AS JOIN_DESCRICAO_CST_IPI'
      
        '       ,(SELECT codigo_cst FROM cst_ipi WHERE id = ii.id_cst_ipi' +
        ') AS JOIN_CODIGO_CST_IPI'
      'from IMPOSTO_IPI ii'
      'where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqImpostoIPIID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqImpostoIPIPERC_ALIQUOTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Aliquota'
      FieldName = 'PERC_ALIQUOTA'
      Origin = 'PERC_ALIQUOTA'
      Precision = 24
      Size = 9
    end
    object fdqImpostoIPIID_CST_IPI: TIntegerField
      DisplayLabel = 'C'#243'digo CST IPI'
      FieldName = 'ID_CST_IPI'
      Origin = 'ID_CST_IPI'
      Required = True
    end
    object fdqImpostoIPIJOIN_DESCRICAO_CST_IPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CST IPI'
      FieldName = 'JOIN_DESCRICAO_CST_IPI'
      Origin = 'JOIN_DESCRICAO_CST_IPI'
      ProviderFlags = []
      Size = 255
    end
    object fdqImpostoIPIJOIN_CODIGO_CST_IPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo CST IPI'
      FieldName = 'JOIN_CODIGO_CST_IPI'
      Origin = 'JOIN_CODIGO_CST_IPI'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
  end
  object dspImpostoIPI: TDataSetProvider
    DataSet = fdqImpostoIPI
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
