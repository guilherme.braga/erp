object SMEmpresaFilial: TSMEmpresaFilial
  OldCreateOrder = False
  Height = 240
  Width = 500
  object fdqEmpresa: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT e.*'
      'FROM empresa e WHERE id = :id')
    Left = 48
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqEmpresaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqEmpresaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dspEmpresa: TDataSetProvider
    DataSet = fdqEmpresa
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object dsEmpresa: TDataSource
    DataSet = fdqEmpresa
    Left = 110
    Top = 15
  end
  object fdqFilial: TgbFDQuery
    MasterSource = dsEmpresa
    MasterFields = 'ID'
    DetailFields = 'ID_EMPRESA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT f.*'
      '      ,z.descricao AS JOIN_DESCRICAO_ZONEAMENTO '
      '      ,c.descricao AS JOIN_DESCRICAO_CIDADE'
      '      ,cnae.sequencia AS JOIN_SEQUENCIA_CNAE'
      '      ,cnae.descricao AS JOIN_DESCRICAO_CNAE '
      'FROM filial f '
      'LEFT JOIN zoneamento z ON f.id_zoneamento = z.id'
      'LEFT JOIN cidade c ON f.id_cidade  = c.id'
      'LEFT JOIN cnae ON f.id_cnae = cnae.id'
      'WHERE f.id_empresa = :id'
      ''
      '')
    Left = 48
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqFilialID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqFilialFANTASIA: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'FANTASIA'
      Origin = 'FANTASIA'
      Required = True
      Size = 80
    end
    object fdqFilialRAZAO_SOCIAL: TStringField
      DisplayLabel = 'Raz'#227'o Social'
      FieldName = 'RAZAO_SOCIAL'
      Origin = 'RAZAO_SOCIAL'
      Required = True
      Size = 80
    end
    object fdqFilialCNPJ: TStringField
      FieldName = 'CNPJ'
      Origin = 'CNPJ'
      Required = True
      Size = 14
    end
    object fdqFilialIE: TStringField
      DisplayLabel = 'Inscri'#231#227'o Estadual'
      FieldName = 'IE'
      Origin = 'IE'
      Required = True
    end
    object fdqFilialDT_FUNDACAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Funda'#231#227'o'
      FieldName = 'DT_FUNDACAO'
      Origin = 'DT_FUNDACAO'
    end
    object fdqFilialID_EMPRESA: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'ID_EMPRESA'
      Origin = 'ID_EMPRESA'
    end
    object fdqFilialNR_ITEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
    end
    object fdqFilialID_ZONEAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'dido do Zoneamento'
      FieldName = 'ID_ZONEAMENTO'
      Origin = 'ID_ZONEAMENTO'
    end
    object fdqFilialREGIME_TRIBUTARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Tribut'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO'
      Origin = 'REGIME_TRIBUTARIO'
      Size = 45
    end
    object fdqFilialJOIN_DESCRICAO_ZONEAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqFilialBO_MATRIZ: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Matriz'
      FieldName = 'BO_MATRIZ'
      Origin = 'BO_MATRIZ'
      FixedChar = True
      Size = 1
    end
    object fdqFilialID_CIDADE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
    end
    object fdqFilialLOGRADOURO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Logradouro'
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      Size = 100
    end
    object fdqFilialNUMERO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object fdqFilialCOMPLEMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Complemento'
      FieldName = 'COMPLEMENTO'
      Origin = 'COMPLEMENTO'
      Size = 50
    end
    object fdqFilialBAIRRO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      Size = 100
    end
    object fdqFilialCEP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CEP'
      Origin = 'CEP'
      Size = 15
    end
    object fdqFilialJOIN_DESCRICAO_CIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqFilialTELEFONE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE'
      Origin = 'TELEFONE'
      Size = 11
    end
    object fdqFilialTIPO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo da Empresa'
      FieldName = 'TIPO_EMPRESA'
      Origin = 'TIPO_EMPRESA'
      Size = 15
    end
    object fdqFilialCRT: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CRT'
      Origin = 'CRT'
    end
    object fdqFilialBO_CONTRIBUINTE_ICMS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte ICMS'
      FieldName = 'BO_CONTRIBUINTE_ICMS'
      Origin = 'BO_CONTRIBUINTE_ICMS'
      FixedChar = True
      Size = 1
    end
    object fdqFilialBO_CONTRIBUINTE_IPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte IPI'
      FieldName = 'BO_CONTRIBUINTE_IPI'
      Origin = 'BO_CONTRIBUINTE_IPI'
      FixedChar = True
      Size = 1
    end
    object fdqFilialEMAIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 255
    end
    object fdqFilialSITE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Site'
      FieldName = 'SITE'
      Origin = 'SITE'
      Size = 255
    end
    object fdqFilialID_CNAE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo CNAE'
      FieldName = 'ID_CNAE'
      Origin = 'ID_CNAE'
    end
    object fdqFilialIM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Inscri'#231#227'o Municipal'
      FieldName = 'IM'
      Origin = 'IM'
      Size = 15
    end
    object fdqFilialJOIN_SEQUENCIA_CNAE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object fdqFilialJOIN_DESCRICAO_CNAE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsFilial: TDataSource
    DataSet = fdqFilial
    Left = 76
    Top = 64
  end
end
