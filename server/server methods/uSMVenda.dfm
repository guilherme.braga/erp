object SMVenda: TSMVenda
  OldCreateOrder = False
  Height = 438
  Width = 705
  object dspVenda: TDataSetProvider
    DataSet = fdqVenda
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object fdqVenda: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT v.*'
      '      ,p.nome as JOIN_NOME_PESSOA'
      '      ,o.descricao as JOIN_DESCRICAO_OPERACAO'
      '      ,ca.sequencia as JOIN_SEQUENCIA_CONTA_ANALISE'
      '      ,ca.descricao as JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao as JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,pp.descricao as JOIN_DESCRICAO_PLANO_PAGAMENTO'
      '      ,fp.descricao as JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,tp.descricao as JOIN_DESCRICAO_TABELA_PRECO'
      '      ,pc.vl_credito as JOIN_VALOR_PESSOA_CREDITO'
      
        '      ,(select nome from pessoa_usuario where id = v.id_vendedor' +
        ') AS JOIN_NOME_VENDEDOR'
      'FROM venda v'
      'INNER JOIN pessoa p ON v.id_pessoa = p.id'
      'LEFT JOIN pessoa_credito pc ON p.id = pc.id_pessoa'
      'LEFT JOIN operacao o ON v.id_operacao = o.id'
      'LEFT JOIN conta_analise ca ON v.id_conta_analise = ca.id'
      'LEFT JOIN centro_resultado cr ON v.id_centro_resultado = cr.id'
      'LEFT JOIN plano_pagamento pp ON v.id_plano_pagamento = pp.id'
      'LEFT JOIN forma_pagamento fp ON v.id_forma_pagamento = fp.id'
      'INNER JOIN tabela_preco tp ON v.id_tabela_preco = tp.id'
      'WHERE v.id = :id')
    Left = 48
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqVendaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqVendaDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqVendaVL_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Precision = 24
      Size = 9
    end
    object fdqVendaVL_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object fdqVendaVL_TOTAL_PRODUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total dos Produtos'
      FieldName = 'VL_TOTAL_PRODUTO'
      Origin = 'VL_TOTAL_PRODUTO'
      Precision = 24
      Size = 9
    end
    object fdqVendaVL_VENDA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor L'#237'quido'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      Precision = 24
      Size = 9
    end
    object fdqVendaSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object fdqVendaID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqVendaID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqVendaID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object fdqVendaID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqVendaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object fdqVendaID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object fdqVendaID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
    end
    object fdqVendaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object fdqVendaID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
      Required = True
    end
    object fdqVendaPERC_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Precision = 24
      Size = 9
    end
    object fdqVendaPERC_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object fdqVendaTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object fdqVendaDH_FECHAMENTO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object fdqVendaVL_PAGAMENTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Pagamento'
      FieldName = 'VL_PAGAMENTO'
      Origin = 'VL_PAGAMENTO'
      Precision = 24
      Size = 9
    end
    object fdqVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Cr'#233'dito Utilizado'
      FieldName = 'VL_PESSOA_CREDITO_UTILIZADO'
      Origin = 'VL_PESSOA_CREDITO_UTILIZADO'
      Precision = 24
      Size = 9
    end
    object fdqVendaJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaJOIN_DESCRICAO_OPERACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia da Conta de An'#225'lise'
      FieldName = 'JOIN_SEQUENCIA_CONTA_ANALISE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object fdqVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 45
    end
    object fdqVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Cr'#233'dito Dispon'#237'vel'
      FieldName = 'JOIN_VALOR_PESSOA_CREDITO'
      Origin = 'VL_CREDITO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqVendaID_VENDEDOR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Vendedor'
      FieldName = 'ID_VENDEDOR'
      Origin = 'ID_VENDEDOR'
    end
    object fdqVendaJOIN_NOME_VENDEDOR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vendedor'
      FieldName = 'JOIN_NOME_VENDEDOR'
      Origin = 'JOIN_NOME_VENDEDOR'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaID_NOTA_FISCAL_NFE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da NFE'
      FieldName = 'ID_NOTA_FISCAL_NFE'
      Origin = 'ID_NOTA_FISCAL_NFE'
    end
    object fdqVendaID_OPERACAO_FISCAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object fdqVendaID_NOTA_FISCAL_NFSE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da NFSE'
      FieldName = 'ID_NOTA_FISCAL_NFSE'
      Origin = 'ID_NOTA_FISCAL_NFSE'
    end
    object fdqVendaID_NOTA_FISCAL_NFCE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da NFCE'
      FieldName = 'ID_NOTA_FISCAL_NFCE'
      Origin = 'ID_NOTA_FISCAL_NFCE'
    end
  end
  object fdqVendaItem: TgbFDQuery
    MasterSource = dsVenda
    MasterFields = 'ID'
    DetailFields = 'ID_VENDA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT vi.*'
      '       ,p.descricao AS JOIN_DESCRICAO_PRODUTO'
      '       ,p.codigo_barra AS JOIN_CODIGO_BARRA_PRODUTO'
      '       ,ue.sigla AS JOIN_SIGLA_UNIDADE_ESTOQUE'
      '       ,pe.qt_estoque AS JOIN_ESTOQUE_PRODUTO'
      'FROM venda_item vi'
      'INNER JOIN venda nf ON vi.id_venda = nf.id'
      'INNER JOIN produto p ON vi.id_produto = p.id'
      'LEFT JOIN unidade_estoque ue ON p.id_unidade_estoque = ue.id'
      
        'LEFT JOIN produto_filial pe ON pe.id_produto = p.id and pe.id_fi' +
        'lial = nf.id_filial'
      'WHERE vi.id_venda = :id')
    Left = 48
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqVendaItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqVendaItemID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object fdqVendaItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqVendaItemNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqVendaItemQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaItemJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object fdqVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
    object fdqVendaItemDESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO_PRODUTO'
      Size = 120
    end
  end
  object dsVenda: TDataSource
    DataSet = fdqVenda
    Left = 110
    Top = 15
  end
  object fdqVendaParcela: TgbFDQuery
    MasterSource = dsVenda
    MasterFields = 'ID'
    DetailFields = 'ID_VENDA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT vip.* '
      '      ,fp.descricao as JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,tq.tipo as JOIN_TIPO_FORMA_PAGAMENTO'
      '      ,oc.descricao as JOIN_DESCRICAO_OPERADORA_CARTAO'
      '      ,car.descricao as JOIN_DESCRICAO_CARTEIRA'
      'FROM venda_parcela vip'
      'INNER JOIN forma_pagamento fp ON vip.id_forma_pagamento = fp.id'
      'INNER JOIN tipo_quitacao tq ON fp.id_tipo_quitacao = tq.id'
      'INNER JOIN carteira car ON vip.id_carteira = car.id'
      'LEFT JOIN operadora_cartao oc ON vip.id_operadora_cartao = oc.id'
      'WHERE vip.id_venda = :id')
    Left = 80
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqVendaParcelaID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqVendaParcelaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqVendaParcelaID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object fdqVendaParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqVendaParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqVendaParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqVendaParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqVendaParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqVendaParcelaCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqVendaParcelaCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqVendaParcelaCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqVendaParcelaCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqVendaParcelaCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqVendaParcelaCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqVendaParcelaCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqVendaParcelaCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqVendaParcelaID_FORMA_PAGAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object fdqVendaParcelaOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object fdqVendaParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo da Forma de Pagamento'
      FieldName = 'JOIN_TIPO_FORMA_PAGAMENTO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 30
    end
    object fdqVendaParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqVendaParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqVendaParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqVendaReceitaOtica: TgbFDQuery
    MasterSource = dsVenda
    MasterFields = 'ID'
    DetailFields = 'ID_VENDA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT vro.* '
      '      ,ro.id_pessoa AS JOIN_ID_PESSOA'
      
        '      ,(select nome from pessoa where id = ro.id_pessoa) AS JOIN' +
        '_NOME_PESSOA'
      '      ,ro.id_medico AS JOIN_ID_MEDICO'
      
        '      ,(select nome from medico where id = ro.id_medico) AS JOIN' +
        '_NOME_MEDICO'
      '      ,ro.especificacao AS JOIN_ESPECIFICACAO_RECEITA_OTICA'
      'FROM venda_receita_otica vro'
      'INNER JOIN receita_otica ro ON vro.id_receita_otica = ro.id'
      'WHERE vro.id_venda = :id')
    Left = 112
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqVendaReceitaOticaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqVendaReceitaOticaID_RECEITA_OTICA: TIntegerField
      DisplayLabel = 'C'#243'digo da Receita '#211'tica'
      FieldName = 'ID_RECEITA_OTICA'
      Origin = 'ID_RECEITA_OTICA'
      Required = True
    end
    object fdqVendaReceitaOticaID_VENDA: TIntegerField
      DisplayLabel = 'C'#243'digo da Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object fdqVendaReceitaOticaJOIN_ID_PESSOA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'JOIN_ID_PESSOA'
      Origin = 'ID_PESSOA'
      ProviderFlags = []
    end
    object fdqVendaReceitaOticaJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'JOIN_NOME_PESSOA'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaReceitaOticaJOIN_ID_MEDICO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do M'#233'dico'
      FieldName = 'JOIN_ID_MEDICO'
      Origin = 'ID_MEDICO'
      ProviderFlags = []
    end
    object fdqVendaReceitaOticaJOIN_NOME_MEDICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'M'#233'dico'
      FieldName = 'JOIN_NOME_MEDICO'
      Origin = 'JOIN_NOME_MEDICO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaReceitaOticaJOIN_ESPECIFICACAO_RECEITA_OTICA: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Especifica'#231#227'o'
      FieldName = 'JOIN_ESPECIFICACAO_RECEITA_OTICA'
      Origin = 'ESPECIFICACAO'
      ProviderFlags = []
    end
  end
  object fdqVendaItemDevolucao: TgbFDQuery
    MasterSource = dsVenda
    MasterFields = 'ID'
    DetailFields = 'ID_VENDA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT vi.*'
      '       ,p.descricao AS JOIN_DESCRICAO_PRODUTO'
      '       ,p.codigo_barra AS JOIN_CODIGO_BARRA_PRODUTO'
      '       ,ue.sigla AS JOIN_SIGLA_UNIDADE_ESTOQUE'
      '       ,pe.qt_estoque AS JOIN_ESTOQUE_PRODUTO'
      'FROM venda_item_devolucao vi'
      'INNER JOIN venda nf ON vi.id_venda = nf.id'
      'INNER JOIN produto p ON vi.id_produto = p.id'
      'LEFT JOIN unidade_estoque ue ON p.id_unidade_estoque = ue.id'
      
        'LEFT JOIN produto_filial pe ON pe.id_produto = p.id and pe.id_fi' +
        'lial = nf.id_filial'
      'WHERE vi.id_venda = :id')
    Left = 48
    Top = 116
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ReadOnly = True
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FMTBCDField1: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField2: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField3: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField4: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField5: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField6: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object FMTBCDField7: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object StringField1: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object StringField2: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object FMTBCDField8: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object StringField3: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
  end
  object fdqVendaBloqueioPersonalizado: TgbFDQuery
    MasterSource = dsVenda
    MasterFields = 'ID'
    DetailFields = 'ID_VENDA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT vbp.*'
      '      ,pu.nome as JOIN_NOME_PESSOA_USUARIO '
      '      ,bp.descricao as JOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO'
      'FROM venda_bloqueio_personalizado vbp'
      'INNER JOIN pessoa_usuario pu'
      '  ON vbp.id_pessoa_usuario = pu.id'
      'INNER JOIN bloqueio_personalizado bp'
      '  ON vbp.id_bloqueio_personalizado = bp.id '
      'WHERE vbp.id_venda = :id')
    Left = 80
    Top = 116
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqVendaBloqueioPersonalizadoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object fdqVendaBloqueioPersonalizadoID_BLOQUEIO_PERSONALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bloqueio Personalizado'
      FieldName = 'ID_BLOQUEIO_PERSONALIZADO'
      Origin = 'ID_BLOQUEIO_PERSONALIZADO'
      Required = True
    end
    object fdqVendaBloqueioPersonalizadoID_VENDA: TIntegerField
      DisplayLabel = 'C'#243'digo da Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object fdqVendaBloqueioPersonalizadoSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Size = 15
    end
    object fdqVendaBloqueioPersonalizadoDH_BLOQUEIO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Hora do Bloqueio'
      FieldName = 'DH_BLOQUEIO'
      Origin = 'DH_BLOQUEIO'
    end
    object fdqVendaBloqueioPersonalizadoDH_LIBERACAO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. da Libera'#231#227'o'
      FieldName = 'DH_LIBERACAO'
      Origin = 'DH_LIBERACAO'
    end
    object fdqVendaBloqueioPersonalizadoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object fdqVendaBloqueioPersonalizadoJOIN_NOME_PESSOA_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqVendaBloqueioPersonalizadoJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bloqueio'
      FieldName = 'JOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
