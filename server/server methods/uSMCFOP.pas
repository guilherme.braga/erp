unit uSMCFOP;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, Datasnap.Provider, FireDAC.Comp.Client,
  uGBFDQuery, Datasnap.DSProviderDataModuleAdapter;

type
  TSMCFOP = class(TDSServerModule)
    fdqCFOP: TgbFDQuery;
    dspCFOP: TDataSetProvider;
    fdqCFOPID: TFDAutoIncField;
    fdqCFOPCODIGO: TIntegerField;
    fdqCFOPDESCRICAO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

