unit uSMIndoor;

interface

uses
  System.SysUtils,
  System.Classes,
  Datasnap.DSServer,
  Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider, uGBFDQuery, Data.DBXPlatform;

type
  TSMIndoor = class(TDSServerModule)
    fdqEstacao: TgbFDQuery;
    dspEstacao: TDataSetProvider;
    fdqEstacaoDESCRICAO: TStringField;
    fdqEstacaoSISTEMA_OPERACIONAL: TStringField;
    fdqEstacaoTV: TStringField;
    fdqEstacaoENDERECO_IP: TStringField;
    fdqEstacaoOBSERVACAO: TStringField;
    fdqEstacaoID: TFDAutoIncField;
    fdqMidia: TgbFDQuery;
    dspMidia: TDataSetProvider;
    fdqMidiaID: TFDAutoIncField;
    fdqMidiaNOME: TStringField;
    fdqMidiaDESCRICAO: TStringField;
    fdqMidiaTIPO: TStringField;
    fdqMidiaTAMANHO: TFMTBCDField;
    fdqMidiaDURACAO: TIntegerField;
    fdqMidiaDH_CADASTRO: TDateTimeField;
    fdqMidiaENDERECO_FTP: TStringField;
    fdqGerenciamentoEstacao: TgbFDQuery;
    fdqGerenciamentoEstacaoMidia: TgbFDQuery;
    dspGerenciamentoEstacao: TDataSetProvider;
    fdqGerenciamentoEstacaoID: TFDAutoIncField;
    fdqGerenciamentoEstacaoID_ESTACAO: TIntegerField;
    fdqGerenciamentoEstacaoMidiaID: TFDAutoIncField;
    fdqGerenciamentoEstacaoMidiaID_MIDIA: TIntegerField;
    fdqGerenciamentoEstacaoMidiaID_GERENCIAMENTO_ESTACAO: TIntegerField;
    dsGerenciamentoEstacao: TDataSource;
    fdqGerenciamentoEstacaoJOIN_DESCRICAO_ESTACAO: TStringField;
    fdqGerenciamentoEstacaoMidiaJOIN_DESCRICAO_MIDIA: TStringField;
    fdqEstacaoID_PESSOA: TIntegerField;
    fdqEstacaoJOIN_NOME_PESSOA: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
    function RestTest(AValue: string): string;
    function ObterListaArquivos(AValue: string): string;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  uDmConnection,
  uIndoorServ;

{$R *.dfm}

function TSMIndoor.RestTest(AValue: string): string;
var
  response : TDSInvocationMetadata;
begin
  response := GetInvocationMetadata();
  response.ResponseContentType := 'text/plain';
  response.ResponseCode        := 200;
  response.ResponseContent     := 'Par�metro Recebido:' + AValue + '.';
end;

function TSMIndoor.ObterListaArquivos(AValue: string): string;
var
  response : TDSInvocationMetadata;
begin

  response := GetInvocationMetadata();
  response.ResponseContentType := 'application/json';

  if AValue = '' then
  begin
    response.ResponseCode := 400;
    response.ResponseContent := '';
    Exit;
  end;

  try
    response.ResponseCode    := 200;
    response.ResponseContent := TIndoorServ.ObterListaArquivos(AValue);
  except
    response.ResponseCode := 500;
    response.ResponseContent := '';
  end;

end;

end.

