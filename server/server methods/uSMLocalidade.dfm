object SMLocalidade: TSMLocalidade
  OldCreateOrder = False
  Height = 252
  Width = 460
  object fdqPais: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from pais where id = :id')
    Left = 24
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqPaisID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPaisdescricao: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'descricao'
      Origin = 'descricao'
      Size = 80
    end
  end
  object dspPais: TDataSetProvider
    DataSet = fdqPais
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 8
  end
  object fdqEstado: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select e.*, p.descricao as JOIN_DESCRICAO_PAIS'
      'from estado e '
      'inner join pais p on e.id_pais = p.id'
      'where e.id = :id')
    Left = 24
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqEstadoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqEstadoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqEstadoUF: TStringField
      FieldName = 'UF'
      Origin = 'UF'
      Required = True
      Size = 2
    end
    object fdqEstadoID_PAIS: TIntegerField
      DisplayLabel = 'C'#243'digo do Pa'#237's'
      FieldName = 'ID_PAIS'
      Origin = 'ID_PAIS'
      Required = True
    end
    object fdqEstadoJOIN_DESCRICAO_PAIS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pa'#237's'
      FieldName = 'JOIN_DESCRICAO_PAIS'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspEstado: TDataSetProvider
    DataSet = fdqEstado
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 64
  end
  object fdqCidade: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select c.*, e.descricao as JOIN_UF_ESTADO'
      'from cidade c '
      'inner join estado e on c.id_estado = e.id'
      'where c.id = :id')
    Left = 24
    Top = 120
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqCidadeID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCidadeDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCidadeIBGE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'IBGE'
      Origin = 'IBGE'
    end
    object fdqCidadeID_ESTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado'
      FieldName = 'ID_ESTADO'
      Origin = 'ID_ESTADO'
      Required = True
    end
    object fdqCidadeJOIN_UF_ESTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspCidade: TDataSetProvider
    DataSet = fdqCidade
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 120
  end
  object fdqBairro: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM bairro WHERE id = :id')
    Left = 144
    Top = 40
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqBairroID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqBairroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
  end
  object dspBairro: TDataSetProvider
    DataSet = fdqBairro
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 176
    Top = 40
  end
  object fdqCEP: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select cep.*'
      '     , c.descricao as JOIN_CIDADE_CIDADE'
      '     , b.descricao as JOIN_DESCRICAO_BAIRRO'
      'from cep '
      'inner join cidade c on cep.id_cidade = c.id'
      'inner join bairro b on cep.id_bairro = b.id'
      'where cep.id = :id')
    Left = 144
    Top = 96
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqCEPID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCEPCEP: TStringField
      FieldName = 'CEP'
      Origin = 'CEP'
      Required = True
      Size = 15
    end
    object fdqCEPID_CIDADE: TIntegerField
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
      Required = True
    end
    object fdqCEPRUA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Rua'
      FieldName = 'RUA'
      Origin = 'RUA'
      Size = 255
    end
    object fdqCEPID_BAIRRO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bairro'
      FieldName = 'ID_BAIRRO'
      Origin = 'ID_BAIRRO'
      Required = True
    end
    object fdqCEPJOIN_CIDADE_CIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_CIDADE_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqCEPJOIN_DESCRICAO_BAIRRO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bairro'
      FieldName = 'JOIN_DESCRICAO_BAIRRO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspCEP: TDataSetProvider
    DataSet = fdqCEP
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 176
    Top = 96
  end
end
