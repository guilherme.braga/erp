object SMRegraImposto: TSMRegraImposto
  OldCreateOrder = False
  Height = 255
  Width = 340
  object fdqRegraImposto: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ri.* '
      '      ,ncm.descricao AS JOIN_DESCRICAO_NCM'
      '      ,ncm.codigo AS JOIN_CODIGO_NCM'
      
        '      ,(select ncme.descricao FROM ncm_especializado ncme where ' +
        'ri.id_ncm_especializado = ncme.id) AS JOIN_DESCRICAO_NCM_ESPECIA' +
        'LIZADO'
      'from regra_imposto ri'
      'LEFT JOIN ncm ON ri.id_ncm = ncm.id'
      'where ri.id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqRegraImpostoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqRegraImpostoID_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'ID_NCM'
      Origin = 'ID_NCM'
    end
    object fdqRegraImpostoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqRegraImpostoEXCECAO_IPI: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Exce'#231#227'o IPI'
      FieldName = 'EXCECAO_IPI'
      Origin = 'EXCECAO_IPI'
    end
    object fdqRegraImpostoID_NCM_ESPECIALIZADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do NCM Especializado'
      FieldName = 'ID_NCM_ESPECIALIZADO'
      Origin = 'ID_NCM_ESPECIALIZADO'
    end
    object fdqRegraImpostoJOIN_DESCRICAO_NCM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'NCM'
      FieldName = 'JOIN_DESCRICAO_NCM'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'NCM Especializado'
      FieldName = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoJOIN_CODIGO_NCM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo NCM'
      FieldName = 'JOIN_CODIGO_NCM'
      Origin = 'CODIGO'
      ProviderFlags = []
    end
  end
  object dspRegraImposto: TDataSetProvider
    DataSet = fdqRegraImposto
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
  object fdqRegraImpostoFiltro: TgbFDQuery
    MasterSource = dsRegraImposto
    MasterFields = 'ID'
    DetailFields = 'ID_REGRA_IMPOSTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select rif.* '
      
        '      ,(SELECT descricao FROM zoneamento WHERE id = rif.id_zonea' +
        'mento_emitente) AS JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
      
        '      ,(SELECT descricao FROM zoneamento WHERE id = rif.id_zonea' +
        'mento_destinatario) AS JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
      
        '      ,(SELECT descricao FROM regime_especial WHERE id = rif.id_' +
        'regime_especial_emitente) AS JOIN_DESCRICAO_REGIME_ESPECIAL_EMIT' +
        'ENTE'
      
        '      ,(SELECT descricao FROM regime_especial WHERE id = rif.id_' +
        'regime_especial_destinatario) AS JOIN_DESCRICAO_REGIME_ESPECIAL_' +
        'DESTINATARIO'
      
        '      ,(SELECT descricao FROM situacao_especial WHERE id = rif.i' +
        'd_situacao_especial) AS JOIN_DESCRICAO_SITUACAO_ESPECIAL'
      
        '      ,(SELECT descricao FROM operacao_fiscal WHERE id = rif.id_' +
        'operacao_fiscal) AS JOIN_DESCRICAO_OPERACAO_FISCAL'
      
        '      ,(SELECT descricao FROM estado WHERE id = rif.id_estado_or' +
        'igem) AS JOIN_UF_ESTADO_ORIGEM'
      
        '      ,(SELECT descricao FROM estado WHERE id = rif.id_estado_de' +
        'stino) AS JOIN_UF_ESTADO_DESTINO'
      
        '      ,(SELECT codigo_cst FROM cst_csosn WHERE id = (select id_c' +
        'st_csosn from imposto_icms where id = rif.id_imposto_icms)) AS J' +
        'OIN_CST_CSOSN'
      
        '      ,(SELECT codigo_cst FROM cst_ipi WHERE id = (select id_cst' +
        '_ipi from imposto_ipi where id = rif.id_imposto_ipi)) AS JOIN_CS' +
        'T_IPI  '
      
        '      ,(SELECT codigo_cst FROM cst_pis_cofins WHERE id = (select' +
        ' id_cst_pis_cofins from imposto_pis_cofins where id = rif.id_imp' +
        'osto_pis_cofins)) AS JOIN_CST_PIS_COFINS'
      'from regra_imposto_filtro rif'
      'where rif.id_regra_imposto = :id')
    Left = 48
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqRegraImpostoFiltroID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqRegraImpostoFiltroTIPO_EMPRESA_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Empresa'
      FieldName = 'TIPO_EMPRESA_PESSOA'
      Origin = 'TIPO_EMPRESA_PESSOA'
      Size = 15
    end
    object fdqRegraImpostoFiltroFORMA_AQUISICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Aquisi'#231#227'o'
      FieldName = 'FORMA_AQUISICAO'
      Origin = 'FORMA_AQUISICAO'
      Size = 45
    end
    object fdqRegraImpostoFiltroREGIME_TRIBUTARIO_DESTINATARIO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Tribut'#225'rio do Destinat'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO_DESTINATARIO'
      Origin = 'REGIME_TRIBUTARIO_DESTINATARIO'
    end
    object fdqRegraImpostoFiltroREGIME_TRIBUTARIO_EMITENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Tribut'#225'rio do Emitente'
      FieldName = 'REGIME_TRIBUTARIO_EMITENTE'
      Origin = 'REGIME_TRIBUTARIO_EMITENTE'
    end
    object fdqRegraImpostoFiltroCRT_EMITENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CRT Emitente'
      FieldName = 'CRT_EMITENTE'
      Origin = 'CRT_EMITENTE'
    end
    object fdqRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_EMITENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte do ICMS Emitente'
      FieldName = 'BO_CONTRIBUINTE_ICMS_EMITENTE'
      Origin = 'BO_CONTRIBUINTE_ICMS_EMITENTE'
      FixedChar = True
      Size = 1
    end
    object fdqRegraImpostoFiltroBO_CONTRIBUINTE_IPI_EMITENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte do IPI Emitente'
      FieldName = 'BO_CONTRIBUINTE_IPI_EMITENTE'
      Origin = 'BO_CONTRIBUINTE_IPI_EMITENTE'
      FixedChar = True
      Size = 1
    end
    object fdqRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_DESTINATARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte do ICMS Destinat'#225'rio'
      FieldName = 'BO_CONTRIBUINTE_ICMS_DESTINATARIO'
      Origin = 'BO_CONTRIBUINTE_ICMS_DESTINATARIO'
      FixedChar = True
      Size = 1
    end
    object fdqRegraImpostoFiltroBO_CONTRIBUINTE_IPI_DESTINATARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte do IPI Destinat'#225'rio'
      FieldName = 'BO_CONTRIBUINTE_IPI_DESTINATARIO'
      Origin = 'BO_CONTRIBUINTE_IPI_DESTINATARIO'
      FixedChar = True
      Size = 1
    end
    object fdqRegraImpostoFiltroID_ZONEAMENTO_EMITENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento do Emitente'
      FieldName = 'ID_ZONEAMENTO_EMITENTE'
      Origin = 'ID_ZONEAMENTO_EMITENTE'
    end
    object fdqRegraImpostoFiltroID_ZONEAMENTO_DESTINATARIO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento do Destinat'#225'rio'
      FieldName = 'ID_ZONEAMENTO_DESTINATARIO'
      Origin = 'ID_ZONEAMENTO_DESTINATARIO'
    end
    object fdqRegraImpostoFiltroID_REGIME_ESPECIAL_EMITENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Regime Especial do Emitente'
      FieldName = 'ID_REGIME_ESPECIAL_EMITENTE'
      Origin = 'ID_REGIME_ESPECIAL_EMITENTE'
    end
    object fdqRegraImpostoFiltroID_REGIME_ESPECIAL_DESTINATARIO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Regime Especial do Destinat'#225'rio'
      FieldName = 'ID_REGIME_ESPECIAL_DESTINATARIO'
      Origin = 'ID_REGIME_ESPECIAL_DESTINATARIO'
    end
    object fdqRegraImpostoFiltroID_SITUACAO_ESPECIAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Situa'#231#227'o Especial'
      FieldName = 'ID_SITUACAO_ESPECIAL'
      Origin = 'ID_SITUACAO_ESPECIAL'
    end
    object fdqRegraImpostoFiltroID_REGRA_IMPOSTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Regra de Imposto'
      FieldName = 'ID_REGRA_IMPOSTO'
      Origin = 'ID_REGRA_IMPOSTO'
    end
    object fdqRegraImpostoFiltroID_ESTADO_ORIGEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado de Origem'
      FieldName = 'ID_ESTADO_ORIGEM'
      Origin = 'ID_ESTADO_ORIGEM'
    end
    object fdqRegraImpostoFiltroID_ESTADO_DESTINO: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado de Destino'
      FieldName = 'ID_ESTADO_DESTINO'
      Origin = 'ID_ESTADO_DESTINO'
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_EMITENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento do Emitente'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
      Origin = 'JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento do Destinat'#225'rio'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
      Origin = 'JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Especial do Emitente'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
      Origin = 'JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Especial do Destinat'#225'rio'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
      Origin = 'JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_SITUACAO_ESPECIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o Especial'
      FieldName = 'JOIN_DESCRICAO_SITUACAO_ESPECIAL'
      Origin = 'JOIN_DESCRICAO_SITUACAO_ESPECIAL'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_UF_ESTADO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estado de Origem'
      FieldName = 'JOIN_UF_ESTADO_ORIGEM'
      Origin = 'JOIN_UF_ESTADO_ORIGEM'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroJOIN_UF_ESTADO_DESTINO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estado de Destino'
      FieldName = 'JOIN_UF_ESTADO_DESTINO'
      Origin = 'JOIN_UF_ESTADO_DESTINO'
      ProviderFlags = []
      Size = 255
    end
    object fdqRegraImpostoFiltroDH_INICIO_VIGENCIA: TDateTimeField
      DisplayLabel = 'Dh do In'#237'cio da Vig'#234'ncia'
      FieldName = 'DH_INICIO_VIGENCIA'
      Origin = 'DH_INICIO_VIGENCIA'
      Required = True
    end
    object fdqRegraImpostoFiltroDH_FIM_VIGENCIA: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh do T'#233'rmino da Vig'#234'ncia'
      FieldName = 'DH_FIM_VIGENCIA'
      Origin = 'DH_FIM_VIGENCIA'
    end
    object fdqRegraImpostoFiltroORIGEM_DA_MERCADORIA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'ORIGEM_DA_MERCADORIA'
      Origin = 'ORIGEM_DA_MERCADORIA'
    end
    object fdqRegraImpostoFiltroID_IMPOSTO_PIS_COFINS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Imposto PIS Cofins'
      FieldName = 'ID_IMPOSTO_PIS_COFINS'
      Origin = 'ID_IMPOSTO_PIS_COFINS'
    end
    object fdqRegraImpostoFiltroID_IMPOSTO_ICMS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Imposto ICMS'
      FieldName = 'ID_IMPOSTO_ICMS'
      Origin = 'ID_IMPOSTO_ICMS'
    end
    object fdqRegraImpostoFiltroID_IMPOSTO_IPI: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Imposto IPI'
      FieldName = 'ID_IMPOSTO_IPI'
      Origin = 'ID_IMPOSTO_IPI'
    end
    object fdqRegraImpostoFiltroJOIN_CST_CSOSN: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_CST_CSOSN'
      Origin = 'JOIN_CST_CSOSN'
      ProviderFlags = []
      Size = 5
    end
    object fdqRegraImpostoFiltroJOIN_CST_IPI: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_CST_IPI'
      Origin = 'JOIN_CST_IPI'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object fdqRegraImpostoFiltroJOIN_CST_PIS_COFINS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_CST_PIS_COFINS'
      Origin = 'JOIN_CST_PIS_COFINS'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object fdqRegraImpostoFiltroID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
      Required = True
    end
    object fdqRegraImpostoFiltroJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o da Opera'#231#227'o Fiscal'
      FieldName = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      Origin = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsRegraImposto: TDataSource
    DataSet = fdqRegraImposto
    Left = 104
    Top = 24
  end
end
