object SMNotaFiscal: TSMNotaFiscal
  OldCreateOrder = False
  Height = 222
  Width = 358
  object dspNotaFiscal: TDataSetProvider
    DataSet = fdqNotaFiscal
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object fdqNotaFiscal: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT nf.*'
      '      ,p.nome as JOIN_NOME_PESSOA'
      '      ,o.descricao as JOIN_DESCRICAO_OPERACAO'
      '      ,of.descricao as JOIN_DESCRICAO_OPERACAO_FISCAL'
      '      ,m.descricao as JOIN_DESCRICAO_MODELO'
      '      ,m.modelo as JOIN_MODELO_MODELO'
      '      ,ca.sequencia as JOIN_SEQUENCIA_CONTA_ANALISE'
      '      ,ca.descricao as JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao as JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,pp.descricao as JOIN_DESCRICAO_PLANO_PAGAMENTO'
      '      ,fp.descricao as JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,pu.nome as JOIN_NOME_PESSOA_USUARIO'
      'FROM nota_fiscal nf'
      'INNER JOIN pessoa p ON nf.id_pessoa = p.id'
      'INNER JOIN operacao o ON nf.id_operacao = o.id'
      'LEFT JOIN operacao_fiscal of ON nf.id_operacao_fiscal = of.id'
      'INNER JOIN modelo_nota m ON nf.id_modelo_nota = m.id'
      'INNER JOIN conta_analise ca ON nf.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado cr ON nf.id_centro_resultado = cr.id'
      'INNER JOIN plano_pagamento pp ON nf.id_plano_pagamento = pp.id'
      'INNER JOIN forma_pagamento fp ON nf.id_forma_pagamento = fp.id'
      'INNER JOIN pessoa_usuario pu ON nf.id_pessoa_usuario = pu.id'
      'WHERE nf.id = :id')
    Left = 48
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqNotaFiscalID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNotaFiscalDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqNotaFiscalB_NNF: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'B_NNF'
      Origin = 'B_NNF'
    end
    object fdqNotaFiscalB_SERIE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'S'#233'rie'
      FieldName = 'B_SERIE'
      Origin = 'B_SERIE'
    end
    object fdqNotaFiscalID_MODELO_NOTA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO_NOTA'
      Origin = 'ID_MODELO_NOTA'
    end
    object fdqNotaFiscalB_DEMI: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'B_DEMI'
      Origin = 'B_DEMI'
    end
    object fdqNotaFiscalB_DSAIENT: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Entrada'
      FieldName = 'B_DSAIENT'
      Origin = 'B_DSAIENT'
    end
    object fdqNotaFiscalB_HSAIENT: TTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Hr. Entrada'
      FieldName = 'B_HSAIENT'
      Origin = 'B_HSAIENT'
    end
    object fdqNotaFiscalW_VPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Produto'
      FieldName = 'W_VPROD'
      Origin = 'W_VPROD'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'W_VST'
      Origin = 'W_VST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VSEG: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Seguro'
      FieldName = 'W_VSEG'
      Origin = 'W_VSEG'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VDESC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'W_VDESC'
      Origin = 'W_VDESC'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VOUTRO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'W_VOUTRO'
      Origin = 'W_VOUTRO'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VIPI: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. IPI'
      FieldName = 'W_VIPI'
      Origin = 'W_VIPI'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VFRETE: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Frete'
      FieldName = 'W_VFRETE'
      Origin = 'W_VFRETE'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VNF: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Liqu'#237'do'
      FieldName = 'W_VNF'
      Origin = 'W_VNF'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalX_XNOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Transportadora'
      FieldName = 'X_XNOME'
      Origin = 'X_XNOME'
      Size = 45
    end
    object fdqNotaFiscalID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object fdqNotaFiscalID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object fdqNotaFiscalID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqNotaFiscalID_OPERACAO: TIntegerField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
      Required = True
    end
    object fdqNotaFiscalID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqNotaFiscalID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
    end
    object fdqNotaFiscalID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object fdqNotaFiscalVL_FRETE_TRANSPORTADORA: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Frete Transportadora'
      FieldName = 'VL_FRETE_TRANSPORTADORA'
      Origin = 'VL_FRETE_TRANSPORTADORA'
      Precision = 15
      Size = 2
    end
    object sta: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object fdqNotaFiscalDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object fdqNotaFiscalID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqNotaFiscalJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqNotaFiscalJOIN_DESCRICAO_OPERACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_DESCRICAO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_SEQUENCIA_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_SEQUENCIA_CONTA_ANALISE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object fdqNotaFiscalJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqNotaFiscalJOIN_NOME_PESSOA_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqNotaFiscalJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Opera'#231#227'o Fiscal'
      FieldName = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNotaFiscalJOIN_MODELO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_MODELO_MODELO'
      Origin = 'MODELO'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object fdqNotaFiscalPERC_VST: TBCDField
      FieldName = 'PERC_VST'
      Origin = 'PERC_VST'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalPERC_VSEG: TBCDField
      FieldName = 'PERC_VSEG'
      Origin = 'PERC_VSEG'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalPERC_VDESC: TBCDField
      FieldName = 'PERC_VDESC'
      Origin = 'PERC_VDESC'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalPERC_VOUTRO: TBCDField
      FieldName = 'PERC_VOUTRO'
      Origin = 'PERC_VOUTRO'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalPERC_VIPI: TBCDField
      FieldName = 'PERC_VIPI'
      Origin = 'PERC_VIPI'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalPERC_VFRETE: TBCDField
      FieldName = 'PERC_VFRETE'
      Origin = 'PERC_VFRETE'
      Required = True
      Precision = 7
    end
    object fdqNotaFiscalSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Status'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqNotaFiscalID_PESSOA_USUARIO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object fdqNotaFiscalTIPO_NF: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo da Nota'
      FieldName = 'TIPO_NF'
      Origin = 'TIPO_NF'
      Size = 15
    end
    object fdqNotaFiscalNFE_CHAVE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave NFE'
      FieldName = 'NFE_CHAVE'
      Origin = 'NFE_CHAVE'
      Size = 50
    end
    object fdqNotaFiscalAR_VERSAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vers'#227'o do Layout'
      FieldName = 'AR_VERSAO'
      Origin = 'AR_VERSAO'
      Size = 12
    end
    object fdqNotaFiscalAR_TP_AMB: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ambiente Receita Estadual'
      FieldName = 'AR_TP_AMB'
      Origin = 'AR_TP_AMB'
      Size = 15
    end
    object fdqNotaFiscalAR_VERAPLIC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vers'#227'o do Aplicativo Fiscal'
      FieldName = 'AR_VERAPLIC'
      Origin = 'AR_VERAPLIC'
    end
    object fdqNotaFiscalAR_XMOTIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Resposta do Servidor da Receita Estadual'
      FieldName = 'AR_XMOTIVO'
      Origin = 'AR_XMOTIVO'
      Size = 255
    end
    object fdqNotaFiscalAR_CUF: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF do Servidor da Receita Estadual'
      FieldName = 'AR_CUF'
      Origin = 'AR_CUF'
      Size = 2
    end
    object fdqNotaFiscalAR_DHRECBTO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Data e Hora do Recebimento da Resposta'
      FieldName = 'AR_DHRECBTO'
      Origin = 'AR_DHRECBTO'
    end
    object fdqNotaFiscalAR_INFREC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Informa'#231#245'es do Recebimento'
      FieldName = 'AR_INFREC'
      Origin = 'AR_INFREC'
      Size = 255
    end
    object fdqNotaFiscalAR_NREC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero do Recebimento'
      FieldName = 'AR_NREC'
      Origin = 'AR_NREC'
      Size = 15
    end
    object fdqNotaFiscalAR_TMED: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tempo M'#233'dio de Resposta do Servidor'
      FieldName = 'AR_TMED'
      Origin = 'AR_TMED'
    end
    object fdqNotaFiscalAR_PROTNFE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dados do Protocolo da NFE'
      FieldName = 'AR_PROTNFE'
      Origin = 'AR_PROTNFE'
      Size = 255
    end
    object fdqNotaFiscalID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'Opera'#231#227'o Fiscal da NFE'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object fdqNotaFiscalFORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      Size = 15
    end
    object fdqNotaFiscalX_MODFRETE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modalidade do Frete'
      FieldName = 'X_MODFRETE'
      Origin = 'X_MODFRETE'
    end
    object fdqNotaFiscalVL_QUITACAO_DINHEIRO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Entrada/Quita'#231#227'o em Dinheiro'
      FieldName = 'VL_QUITACAO_DINHEIRO'
      Origin = 'VL_QUITACAO_DINHEIRO'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalDOC_FEDERAL_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento Federal'
      FieldName = 'DOC_FEDERAL_PESSOA'
      Origin = 'DOC_FEDERAL_PESSOA'
      Size = 14
    end
    object fdqNotaFiscalBO_ENVIO_DPEC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'DPEC Enviado'
      FieldName = 'BO_ENVIO_DPEC'
      Origin = 'BO_ENVIO_DPEC'
      FixedChar = True
      Size = 1
    end
    object fdqNotaFiscalNR_REG_DPEC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero do Registro DPEC'
      FieldName = 'NR_REG_DPEC'
      Origin = 'NR_REG_DPEC'
      Size = 255
    end
    object fdqNotaFiscalDH_REG_DPEC: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Data e Hora do Registro do DPEC'
      FieldName = 'DH_REG_DPEC'
      Origin = 'DH_REG_DPEC'
    end
    object fdqNotaFiscalW_VBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total da Base de C'#225'lculo'
      FieldName = 'W_VBC'
      Origin = 'W_VBC'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VICMS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor ICMS'
      FieldName = 'W_VICMS'
      Origin = 'W_VICMS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VBCST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total da Base de C'#225'lculo'
      FieldName = 'W_VBCST'
      Origin = 'W_VBCST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VII: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor II'
      FieldName = 'W_VII'
      Origin = 'W_VII'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VPIS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor PIS'
      FieldName = 'W_VPIS'
      Origin = 'W_VPIS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VCOFINS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor COFINS'
      FieldName = 'W_VCOFINS'
      Origin = 'W_VCOFINS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VRETPIS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor PIS Retido'
      FieldName = 'W_VRETPIS'
      Origin = 'W_VRETPIS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VRETCOFINS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor COFINS Retido'
      FieldName = 'W_VRETCOFINS'
      Origin = 'W_VRETCOFINS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VRETCSLL: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor CSLL Retido'
      FieldName = 'W_VRETCSLL'
      Origin = 'W_VRETCSLL'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VBCIRRF: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor Base IRRF'
      FieldName = 'W_VBCIRRF'
      Origin = 'W_VBCIRRF'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VIRRF: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor IRRF'
      FieldName = 'W_VIRRF'
      Origin = 'W_VIRRF'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VBCRETPREV: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total da Base de Calculo Retida Prev'
      FieldName = 'W_VBCRETPREV'
      Origin = 'W_VBCRETPREV'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalW_VRETPREV: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total do Valor Retido Prev'
      FieldName = 'W_VRETPREV'
      Origin = 'W_VRETPREV'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalPERC_ICMS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% ICMS'
      FieldName = 'PERC_ICMS'
      Origin = 'PERC_ICMS'
      Precision = 7
    end
    object fdqNotaFiscalPERC_PIS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% PIS'
      FieldName = 'PERC_PIS'
      Origin = 'PERC_PIS'
      Precision = 7
    end
    object fdqNotaFiscalPERC_COFINS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% COFINS'
      FieldName = 'PERC_COFINS'
      Origin = 'PERC_COFINS'
      Precision = 7
    end
    object fdqNotaFiscalAR_CSTAT: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Status da NFE'
      FieldName = 'AR_CSTAT'
      Origin = 'AR_CSTAT'
      Size = 25
    end
    object fdqNotaFiscalDOCUMENTO_FISCAL_XML: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'XML'
      FieldName = 'DOCUMENTO_FISCAL_XML'
      Origin = 'DOCUMENTO_FISCAL_XML'
    end
  end
  object fdqNotaFiscalItem: TgbFDQuery
    MasterSource = dsNotaFiscal
    MasterFields = 'ID'
    DetailFields = 'ID_NOTA_FISCAL'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache, evDetailOptimize]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT nfi.*'
      '       ,p.descricao AS JOIN_DESCRICAO_PRODUTO'
      
        '       ,p.bo_produto_agrupador AS JOIN_BO_PRODUTO_AGRUPADOR_PROD' +
        'UTO'
      
        '       ,p.id_produto_agrupador AS JOIN_ID_PRODUTO_AGRUPADOR_PROD' +
        'UTO '
      
        '       ,(select concat(id, '#39' - '#39', descricao) from produto where ' +
        'id = IF(p.id_produto_agrupador = 0, p.id_produto_agrupador, p.id' +
        ')) JOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO '
      '       ,ue.sigla AS JOIN_SIGLA_UNIDADE_ESTOQUE'
      '       ,pe.qt_estoque AS JOIN_ESTOQUE_PRODUTO'
      'FROM nota_fiscal_item nfi'
      'INNER JOIN nota_fiscal nf ON nfi.id_nota_fiscal = nf.id'
      'INNER JOIN produto p ON nfi.id_produto = p.id'
      'LEFT JOIN unidade_estoque ue ON p.id_unidade_estoque = ue.id'
      
        'LEFT JOIN produto_filial pe ON pe.id_produto = p.id and pe.id_fi' +
        'lial = nf.id_filial'
      'WHERE nfi.id_nota_fiscal = :id')
    Left = 48
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqNotaFiscalItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNotaFiscalItemH_NITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'H_NITEM'
      Origin = 'H_NITEM'
      Required = True
    end
    object fdqNotaFiscalItemI_QCOM: TBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'I_QCOM'
      Origin = 'I_QCOM'
      Required = True
      Precision = 15
    end
    object fdqNotaFiscalItemI_VUNCOM: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'I_VUNCOM'
      Origin = 'I_VUNCOM'
      Precision = 21
      Size = 10
    end
    object fdqNotaFiscalItemI_VDESC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'I_VDESC'
      Origin = 'I_VDESC'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemI_VFRETE: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Frete'
      FieldName = 'I_VFRETE'
      Origin = 'I_VFRETE'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemI_VSEG: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Seguro'
      FieldName = 'I_VSEG'
      Origin = 'I_VSEG'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemI_VOUTRO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'I_VOUTRO'
      Origin = 'I_VOUTRO'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_NVICMSST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'N_NVICMSST'
      Origin = 'N_NVICMSST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemO_VIPI: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. IPI'
      FieldName = 'O_VIPI'
      Origin = 'O_VIPI'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqNotaFiscalItemJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqNotaFiscalItemVL_LIQUIDO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemVL_CUSTO_IMPOSTO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo + Imposto'
      FieldName = 'VL_CUSTO_IMPOSTO'
      Origin = 'VL_CUSTO_IMPOSTO'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemID_NOTA_FISCAL: TIntegerField
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'ID_NOTA_FISCAL'
      Origin = 'ID_NOTA_FISCAL'
    end
    object fdqNotaFiscalItemPERC_DESCONTO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Precision = 7
    end
    object fdqNotaFiscalItemPERC_ACRESCIMO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acrescimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Precision = 7
    end
    object fdqNotaFiscalItemPERC_FRETE: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Frete'
      FieldName = 'PERC_FRETE'
      Origin = 'PERC_FRETE'
      Precision = 7
    end
    object fdqNotaFiscalItemPERC_SEGURO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Seguro'
      FieldName = 'PERC_SEGURO'
      Origin = 'PERC_SEGURO'
      Precision = 7
    end
    object fdqNotaFiscalItemPERC_IPI: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% IPI'
      FieldName = 'PERC_IPI'
      Origin = 'PERC_IPI'
      Precision = 7
    end
    object fdqNotaFiscalItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object fdqNotaFiscalItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      DisplayFormat = '###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalItemVL_CUSTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalItemVL_CUSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo Operacional'
      FieldName = 'VL_CUSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalItemPERC_CUSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Custo Operacional'
      FieldName = 'PERC_CUSTO_OPERACIONAL'
      Origin = 'PERC_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalItemI_XPROD: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'I_XPROD'
      Origin = 'I_XPROD'
      Size = 120
    end
    object fdqNotaFiscalItemN_VICMS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl ICMS'
      FieldName = 'N_VICMS'
      Origin = 'N_VICMS'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_VBCST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Base de C'#225'lculo CST'
      FieldName = 'N_VBCST'
      Origin = 'N_VBCST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_PREDBCST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Redu'#231#227'o da Base de C'#225'lculo da CST'
      FieldName = 'N_PREDBCST'
      Origin = 'N_PREDBCST'
      Precision = 5
      Size = 2
    end
    object fdqNotaFiscalItemN_MOTDESICMS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Motivo da Desonera'#231#227'o do ICMS'
      FieldName = 'N_MOTDESICMS'
      Origin = 'N_MOTDESICMS'
    end
    object fdqNotaFiscalItemN_PMVAST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% da MV de Antecipa'#231#227'o da ST'
      FieldName = 'N_PMVAST'
      Origin = 'N_PMVAST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_MODBCST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Motivo da Desonera'#231#227'o da Base de C'#225'lculo da CST'
      FieldName = 'N_MODBCST'
      Origin = 'N_MODBCST'
    end
    object fdqNotaFiscalItemI_NCM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'NCM'
      FieldName = 'I_NCM'
      Origin = 'I_NCM'
    end
    object fdqNotaFiscalItemI_EXTIPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Extens'#227'o do IPI'
      FieldName = 'I_EXTIPI'
      Origin = 'I_EXTIPI'
      Size = 3
    end
    object fdqNotaFiscalItemI_CFOP: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CFOP'
      FieldName = 'I_CFOP'
      Origin = 'I_CFOP'
    end
    object fdqNotaFiscalItemI_CEANTRIB: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Barra do Produto Tributado'
      FieldName = 'I_CEANTRIB'
      Origin = 'I_CEANTRIB'
      Size = 13
    end
    object fdqNotaFiscalItemI_UTRIB: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Unidade Tribut'#225'vel'
      FieldName = 'I_UTRIB'
      Origin = 'I_UTRIB'
      Size = 6
    end
    object fdqNotaFiscalItemI_QTRIB: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Quantidade Tribut'#225'vel'
      FieldName = 'I_QTRIB'
      Origin = 'I_QTRIB'
      Precision = 15
    end
    object fdqNotaFiscalItemI_VUNTRIB: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Unit'#225'rio Tribut'#225'vel'
      FieldName = 'I_VUNTRIB'
      Origin = 'I_VUNTRIB'
      Precision = 21
      Size = 10
    end
    object fdqNotaFiscalItemI_INDTOT: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Composi'#231#227'o do Total da NFe'
      FieldName = 'I_INDTOT'
      Origin = 'I_INDTOT'
    end
    object fdqNotaFiscalItemI_NVE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nomenclatura de Valor Aduaneiro e Estat'#237'stica'
      FieldName = 'I_NVE'
      Origin = 'I_NVE'
      Size = 6
    end
    object fdqNotaFiscalItemI_CEAN: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'I_CEAN'
      Origin = 'I_CEAN'
      Size = 14
    end
    object fdqNotaFiscalItemN_ORIG: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'N_ORIG'
      Origin = 'N_ORIG'
    end
    object fdqNotaFiscalItemN_CST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tributa'#231#227'o pelo ICMS'
      FieldName = 'N_CST'
      Origin = 'N_CST'
    end
    object fdqNotaFiscalItemN_MODBC: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modalidade da Base de C'#225'lculo'
      FieldName = 'N_MODBC'
      Origin = 'N_MODBC'
    end
    object fdqNotaFiscalItemN_PREDBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Percentual da Redu'#231#227'o de BC'
      FieldName = 'N_PREDBC'
      Origin = 'N_PREDBC'
      Precision = 5
      Size = 2
    end
    object fdqNotaFiscalItemN_VBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da BC do ICMS'
      FieldName = 'N_VBC'
      Origin = 'N_VBC'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_PICMS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% ICMS'
      FieldName = 'N_PICMS'
      Origin = 'N_PICMS'
      Precision = 5
      Size = 2
    end
    object fdqNotaFiscalItemN_VICMSOP: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do ICMS da Opera'#231#227'o'
      FieldName = 'N_VICMSOP'
      Origin = 'N_VICMSOP'
      Precision = 5
      Size = 2
    end
    object fdqNotaFiscalItemN_UFST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF ST'
      FieldName = 'N_UFST'
      Origin = 'N_UFST'
      Size = 2
    end
    object fdqNotaFiscalItemN_PDIF: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Percentual do Diferimento'
      FieldName = 'N_PDIF'
      Origin = 'N_PDIF'
      Precision = 5
      Size = 2
    end
    object fdqNotaFiscalItemN_VLICMSDIF: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do Diferimento ICMS'
      FieldName = 'N_VLICMSDIF'
      Origin = 'N_VLICMSDIF'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemN_VICMSDESON: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do ICMS de Disonera'#231#227'o'
      FieldName = 'N_VICMSDESON'
      Origin = 'N_VICMSDESON'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemN_PBCOP: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% da Base de C'#225'lculo da Opera'#231#227'o Pr'#243'pria'
      FieldName = 'N_PBCOP'
      Origin = 'N_PBCOP'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_BCSTRET: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da BC do ICMS'
      FieldName = 'N_BCSTRET'
      Origin = 'N_BCSTRET'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_PCREDSN: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Al'#237'quota aplic'#225'vel de c'#225'lculo do cr'#233'dito (Simples Nacional).'
      FieldName = 'N_PCREDSN'
      Origin = 'N_PCREDSN'
      Precision = 15
    end
    object fdqNotaFiscalItemN_VBCSTDEST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da BC do ICMS ST da UF destino'
      FieldName = 'N_VBCSTDEST'
      Origin = 'N_VBCSTDEST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_VICMSSTDEST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da do ICMS ST da UF destino'
      FieldName = 'N_VICMSSTDEST'
      Origin = 'N_VICMSSTDEST'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemN_VCREDICMSSN: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do Cr'#233'dito ICMS SSN'
      FieldName = 'N_VCREDICMSSN'
      Origin = 'N_VCREDICMSSN'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemO_CLENQ: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Classe de enquadramento do IPI para Cigarros e Bebidas'
      FieldName = 'O_CLENQ'
      Origin = 'O_CLENQ'
      Size = 5
    end
    object fdqNotaFiscalItemO_CNPJPROD: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CNPJ do Produtor da Mercadoria'
      FieldName = 'O_CNPJPROD'
      Origin = 'O_CNPJPROD'
      Size = 14
    end
    object fdqNotaFiscalItemO_CSELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do selo de controle IPI'
      FieldName = 'O_CSELO'
      Origin = 'O_CSELO'
      Size = 60
    end
    object fdqNotaFiscalItemO_QSELO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Quantidade de selo de controle'
      FieldName = 'O_QSELO'
      Origin = 'O_QSELO'
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalItemO_CENQ: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Enquadramento Legal do IPI'
      FieldName = 'O_CENQ'
      Origin = 'O_CENQ'
      Size = 3
    end
    object fdqNotaFiscalItemO_CST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CST'
      FieldName = 'O_CST'
      Origin = 'O_CST'
    end
    object fdqNotaFiscalItemO_VBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da BC do IPI'
      FieldName = 'O_VBC'
      Origin = 'O_VBC'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemO_QUNID: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Quantidade Total na Unidade Padr'#227'o'
      FieldName = 'O_QUNID'
      Origin = 'O_QUNID'
      Precision = 12
    end
    object fdqNotaFiscalItemO_VUNID: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor por Unidade Tribut'#225'vel'
      FieldName = 'O_VUNID'
      Origin = 'O_VUNID'
      Precision = 11
    end
    object fdqNotaFiscalItemO_PIPI: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% IPI'
      FieldName = 'O_PIPI'
      Origin = 'O_PIPI'
      Precision = 15
    end
    object fdqNotaFiscalItemQ_CST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CST PIS'
      FieldName = 'Q_CST'
      Origin = 'Q_CST'
    end
    object fdqNotaFiscalItemQ_VBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da Base de C'#225'lculo do PIS'
      FieldName = 'Q_VBC'
      Origin = 'Q_VBC'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemQ_PPIS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Al'#237'quota do PIS (em percentual)'
      FieldName = 'Q_PPIS'
      Origin = 'Q_PPIS'
      Precision = 15
    end
    object fdqNotaFiscalItemQ_VPIS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do PIS'
      FieldName = 'Q_VPIS'
      Origin = 'Q_VPIS'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemQ_QBCPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Quantidade Vendida'
      FieldName = 'Q_QBCPROD'
      Origin = 'Q_QBCPROD'
      Precision = 12
    end
    object fdqNotaFiscalItemQ_VALIQPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Al'#237'quota do PIS (em reais)'
      FieldName = 'Q_VALIQPROD'
      Origin = 'Q_VALIQPROD'
      Precision = 11
    end
    object fdqNotaFiscalItemS_CST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo de Situa'#231#227'o Tribut'#225'ria da COFINS'
      FieldName = 'S_CST'
      Origin = 'S_CST'
    end
    object fdqNotaFiscalItemS_VBC: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor da Base de C'#225'lculo da COFINS'
      FieldName = 'S_VBC'
      Origin = 'S_VBC'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemS_PCOFINS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Al'#237'quota da COFINS (em percentual)'
      FieldName = 'S_PCOFINS'
      Origin = 'S_PCOFINS'
      Precision = 15
    end
    object fdqNotaFiscalItemS_QBCPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Quantidade Vendida'
      FieldName = 'S_QBCPROD'
      Origin = 'S_QBCPROD'
      Precision = 12
    end
    object fdqNotaFiscalItemS_VALIQPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Al'#237'quota da COFINS (em reais)'
      FieldName = 'S_VALIQPROD'
      Origin = 'S_VALIQPROD'
      Precision = 11
    end
    object fdqNotaFiscalItemS_VCOFINS: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Cofins'
      FieldName = 'S_VCOFINS'
      Origin = 'S_VCOFINS'
      Precision = 13
      Size = 2
    end
    object fdqNotaFiscalItemI_UCOM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Unidade'
      FieldName = 'I_UCOM'
      Origin = 'I_UCOM'
      Size = 6
    end
    object fdqNotaFiscalItemI_VPROD: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor Total Bruto'
      FieldName = 'I_VPROD'
      Origin = 'I_VPROD'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemI_XPED: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pedido de Compra'
      FieldName = 'I_XPED'
      Origin = 'I_XPED'
      Size = 15
    end
    object fdqNotaFiscalItemI_NITEMPED: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Item do Pedido de Compra'
      FieldName = 'I_NITEMPED'
      Origin = 'I_NITEMPED'
    end
    object fdqNotaFiscalItemJOIN_BO_PRODUTO_AGRUPADOR_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto Agrupador?'
      FieldName = 'JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'BO_PRODUTO_AGRUPADOR'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object fdqNotaFiscalItemJOIN_ID_PRODUTO_AGRUPADOR_PRODUTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Produto Agrupador'
      FieldName = 'JOIN_ID_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'ID_PRODUTO_AGRUPADOR'
      ProviderFlags = []
    end
    object fdqNotaFiscalItemJOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'JOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO'
      ProviderFlags = []
      ReadOnly = True
      Size = 94
    end
    object fdqNotaFiscalItemN_VICMSSTRET: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor do ICMS ST Retido'
      FieldName = 'N_VICMSSTRET'
      Origin = 'N_VICMSSTRET'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object fdqNotaFiscalItemID_IMPOSTO_ICMS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_IMPOSTO_ICMS'
      Origin = 'ID_IMPOSTO_ICMS'
      DisplayFormat = 'C'#243'digo do Imposto ICMS'
    end
    object fdqNotaFiscalItemID_IMPOSTO_PIS_COFINS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Imposto PIS Cofins'
      FieldName = 'ID_IMPOSTO_PIS_COFINS'
      Origin = 'ID_IMPOSTO_PIS_COFINS'
    end
    object fdqNotaFiscalItemID_OPERACAO_FISCAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object fdqNotaFiscalItemID_IMPOSTO_IPI: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Imposto IPI'
      FieldName = 'ID_IMPOSTO_IPI'
      Origin = 'ID_IMPOSTO_IPI'
    end
    object fdqNotaFiscalItemPERC_ICMS_ST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% ICMS ST'
      FieldName = 'PERC_ICMS_ST'
      Origin = 'PERC_ICMS_ST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 7
    end
    object fdqNotaFiscalItemN_PICMSST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% ICMS ST'
      FieldName = 'N_PICMSST'
      Origin = 'N_PICMSST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
    end
    object fdqNotaFiscalItemN_VICMSST: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'N_VICMSST'
      Origin = 'N_VICMSST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
    end
  end
  object dsNotaFiscal: TDataSource
    DataSet = fdqNotaFiscal
    Left = 110
    Top = 15
  end
  object fdqNotaFiscalParcela: TgbFDQuery
    MasterSource = dsNotaFiscal
    MasterFields = 'ID'
    DetailFields = 'ID_NOTA_FISCAL'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT nfp.* '
      '      ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,c.descricao AS JOIN_DESCRICAO_CARTEIRA'
      'FROM nota_fiscal_parcela nfp'
      'LEFT JOIN forma_pagamento fp on nfp.id_forma_pagamento = fp.id'
      'INNER JOIN carteira c on nfp.id_carteira = c.id'
      'WHERE nfp.id_nota_fiscal = :id')
    Left = 80
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqNotaFiscalParcelaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNotaFiscalParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqNotaFiscalParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqNotaFiscalParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqNotaFiscalParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqNotaFiscalParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqNotaFiscalParcelaID_NOTA_FISCAL: TIntegerField
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'ID_NOTA_FISCAL'
      Origin = 'ID_NOTA_FISCAL'
    end
    object fdqNotaFiscalParcelaOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object fdqNotaFiscalParcelaCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqNotaFiscalParcelaCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqNotaFiscalParcelaCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqNotaFiscalParcelaCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqNotaFiscalParcelaCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqNotaFiscalParcelaCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqNotaFiscalParcelaCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqNotaFiscalParcelaCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Vencimento Cheque'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqNotaFiscalParcelaID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqNotaFiscalParcelaID_FORMA_PAGAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object fdqNotaFiscalParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqNotaFiscalParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqNotaFiscalParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqModeloNota: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from modelo_nota where id = :id')
    Left = 48
    Top = 128
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqModeloNotaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqModeloNotaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqModeloNotaMODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Required = True
      FixedChar = True
      Size = 2
    end
  end
  object dspModeloNota: TDataSetProvider
    DataSet = fdqModeloNota
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 128
  end
end
