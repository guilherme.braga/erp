unit uSMBackup;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  uBackupProxy;

type
  TSMBackup = class(TDSServerModule)
  private
    { Private declarations }
  public
    function ValidarConfiguracoesParaBackup(): Boolean;
    function RealizarBackup(const AIdUsuario: Integer): Boolean;
    function BackupDiarioRealizado: Boolean;
    function GerarRegistroBackup(const ABackup: TBackupProxy): Integer;
    function DownloadBackup(out Size: Int64): TStream;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uBackupServ;

{$R *.dfm}

{ TSMBackup }

function TSMBackup.BackupDiarioRealizado: Boolean;
begin
  result := TBackupServ.BackupDiarioRealizado();
end;

function TSMBackup.DownloadBackup(out Size: Int64): TStream;
begin
  result := TBackupServ.DownloadBackup(Size);
end;

function TSMBackup.GerarRegistroBackup(const ABackup: TBackupProxy): Integer;
begin
  result := TBackupServ.GerarRegistroBackup(ABackup);
end;

function TSMBackup.RealizarBackup(const AIdUsuario: Integer): Boolean;
begin
  result := TBackupServ.RealizarBackup(AIdUsuario);
end;

function TSMBackup.ValidarConfiguracoesParaBackup(): Boolean;
begin
  result := TBackupServ.ValidarConfiguracoesParaBackup();
end;

end.

