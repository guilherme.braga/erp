object SMMotivo: TSMMotivo
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqMotivo: TgbFDQuery
    Connection = Dm.Connection
    SQL.Strings = (
      'SELECT * FROM motivo WHERE id = :id')
    Left = 88
    Top = 56
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqMotivoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMotivoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqMotivoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 45
    end
  end
  object dspMotivo: TDataSetProvider
    DataSet = fdqMotivo
    Options = [poIncFieldProps, poUseQuoteChar]
    Left = 117
    Top = 56
  end
end
