object SMExtratoPlanoConta: TSMExtratoPlanoConta
  OldCreateOrder = False
  Height = 450
  Width = 757
  object fdqPrimeiroNivel: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      '           select ca.id'
      '                 ,ca.sequencia'
      '                 ,ca.descricao'
      '                 ,cr.id as id_centro_resultado'
      
        '                 ,concat(cr.id, '#39' - '#39', cr.descricao) as desc_cen' +
        'tro_resultado'
      '                 ,gr.id as id_grupo_centro_resultado'
      '                 ,gr.descricao as desc_grupo_centro_resultado'
      '                 ,cast('#39'N'#39' as char(1)) AS BO_CHECKED    '
      '             from plano_conta_item pci'
      
        '       inner join conta_analise ca on pci.id_conta_analise = ca.' +
        'id and ca.bo_exibir_resumo_conta = '#39'S'#39' and ca.nivel = :nivel'
      
        '       inner join plano_conta pc on pci.id_plano_conta = pc.id a' +
        'nd pc.id_centro_resultado in (:id_centro_resultado)'
      
        '       inner join centro_resultado cr on pc.id_centro_resultado ' +
        '= cr.id'
      
        '       inner join grupo_centro_resultado gr on cr.id_grupo_centr' +
        'o_resultado = gr.id'
      '         order by gr.id, cr.id, sequencia')
    Left = 48
    Top = 8
    ParamData = <
      item
        Name = 'NIVEL'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end
      item
        Name = 'ID_CENTRO_RESULTADO'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqPrimeiroNivelid: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object fdqPrimeiroNivelsequencia: TStringField
      FieldName = 'sequencia'
      Origin = 'sequencia'
      Required = True
    end
    object fdqPrimeiroNiveldescricao: TStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Required = True
      Size = 255
    end
    object fdqPrimeiroNivelid_centro_resultado: TFDAutoIncField
      FieldName = 'id_centro_resultado'
      Origin = 'id_centro_resultado'
      ReadOnly = True
    end
    object fdqPrimeiroNiveldesc_centro_resultado: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'desc_centro_resultado'
      Origin = 'desc_centro_resultado'
      Size = 269
    end
    object fdqPrimeiroNivelid_grupo_centro_resultado: TFDAutoIncField
      FieldName = 'id_grupo_centro_resultado'
      Origin = 'id_grupo_centro_resultado'
      ReadOnly = True
    end
    object fdqPrimeiroNiveldesc_grupo_centro_resultado: TStringField
      FieldName = 'desc_grupo_centro_resultado'
      Origin = 'desc_grupo_centro_resultado'
      Required = True
      Size = 255
    end
    object fdqPrimeiroNivelBO_CHECKED: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BO_CHECKED'
      Origin = 'BO_CHECKED'
      Size = 1
    end
  end
  object dspPrimeiroNivel: TDataSetProvider
    DataSet = fdqPrimeiroNivel
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 8
  end
end
