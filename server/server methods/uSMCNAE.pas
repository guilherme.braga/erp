unit uSMCNAE;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, DataSnap.DSProviderDataModuleAdapter,
  uGBFDQuery;

type
  TSMCNAE = class(TDSServerModule)
    fdqCNAE: TgbFDQuery;
    dspCNAE: TDataSetProvider;
    fdqCNAEID: TFDAutoIncField;
    fdqCNAEDESCRICAO: TStringField;
    fdqCNAESEQUENCIA: TStringField;
    fdqCNAENIVEL: TIntegerField;
    fdqCNAESECAO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

