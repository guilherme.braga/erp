object SMCor: TSMCor
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqCor: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT c.*      '
      'FROM cor c'
      'WHERE c.id = :id')
    Left = 24
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqCorID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCorDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object fdqCorRGB: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RGB'
      Origin = 'RGB'
      Size = 30
    end
    object fdqCorCMYK: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CMYK'
      Origin = 'CMYK'
      Size = 30
    end
    object fdqCorBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspCor: TDataSetProvider
    DataSet = fdqCor
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 88
    Top = 16
  end
  object fdqListaCor: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM cor where bo_ativo = '#39'S'#39)
    Left = 40
    Top = 72
    object fdqListaCorID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqListaCorDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object fdqListaCorBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqListaCorRGB: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RGB'
      Origin = 'RGB'
      Size = 30
    end
    object fdqListaCorCMYK: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CMYK'
      Origin = 'CMYK'
      Size = 30
    end
  end
  object dspListaCor: TDataSetProvider
    DataSet = fdqListaCor
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 68
    Top = 72
  end
end
