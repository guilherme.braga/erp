object SMGeracaoDocumento: TSMGeracaoDocumento
  OldCreateOrder = False
  Height = 360
  Width = 536
  object dspGeracaoDocumento: TDataSetProvider
    DataSet = fdqGeracaoDocumento
    Options = [poIncFieldProps, poAutoRefresh, poPropogateChanges, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 93
    Top = 16
  end
  object fdqGeracaoDocumento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT gd.*'
      '      ,p.nome AS JOIN_DESCRICAO_NOME_PESSOA'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,c.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,pp.descricao AS JOIN_DESCRICAO_PLANO_PAGAMENTO'
      '      ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,car.descricao as JOIN_DESCRICAO_CARTEIRA'
      
        '      ,(SELECT descricao FROM forma_pagamento WHERE id = gd.id_f' +
        'orma_pagamento_entrada) AS JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRAD' +
        'A'
      
        '      ,(SELECT descricao FROM conta_corrente WHERE id = gd.id_co' +
        'nta_corrente_entrada) AS JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
      
        '      ,(SELECT descricao FROM operadora_cartao WHERE id = gd.id_' +
        'operadora_cartao_entrada) AS JOIN_DESCRICAO_OPERADORA_CARTAO_ENT' +
        'RADA'
      'FROM geracao_documento gd'
      'INNER JOIN pessoa p ON gd.id_pessoa = p.id'
      'INNER JOIN conta_analise ca ON gd.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado c ON gd.id_centro_resultado = c.id'
      'LEFT JOIN plano_pagamento pp ON gd.id_plano_pagamento = pp.id'
      'INNER JOIN forma_pagamento fp ON gd.id_forma_pagamento = fp.id'
      'INNER JOIN carteira car ON gd.id_carteira = car.id'
      'LEFT JOIN conta_corrente cc ON gd.id_conta_corrente = cc.id'
      'WHERE gd.id = :id')
    Left = 56
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqGeracaoDocumentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGeracaoDocumentoID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
    end
    object fdqGeracaoDocumentoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqGeracaoDocumentoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqGeracaoDocumentoDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 80
    end
    object fdqGeracaoDocumentoVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqGeracaoDocumentoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqGeracaoDocumentoSTATUS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqGeracaoDocumentoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object fdqGeracaoDocumentoID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqGeracaoDocumentoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqGeracaoDocumentoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqGeracaoDocumentoID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object fdqGeracaoDocumentoID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqGeracaoDocumentoDT_BASE: TDateField
      DisplayLabel = 'Dt. Base'
      FieldName = 'DT_BASE'
      Origin = 'DT_BASE'
      Required = True
    end
    object fdqGeracaoDocumentoTIPO_PLANO_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo do Plano de Pagamento'
      FieldName = 'TIPO_PLANO_PAGAMENTO'
      Origin = 'TIPO_PLANO_PAGAMENTO'
    end
    object fdqGeracaoDocumentoQT_PARCELAS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Qt. Parcelas'
      FieldName = 'QT_PARCELAS'
      Origin = 'QT_PARCELAS'
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqGeracaoDocumentoPARCELA_BASE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Parcela Inicial'
      FieldName = 'PARCELA_BASE'
      Origin = 'PARCELA_BASE'
    end
    object fdqGeracaoDocumentoDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object fdqGeracaoDocumentoDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object fdqGeracaoDocumentoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqGeracaoDocumentoID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoVL_ENTRADA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Entrada'
      FieldName = 'VL_ENTRADA'
      Origin = 'VL_ENTRADA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqGeracaoDocumentoID_FORMA_PAGAMENTO_ENTRADA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Forma de Pagamento da Entrada'
      FieldName = 'ID_FORMA_PAGAMENTO_ENTRADA'
      Origin = 'ID_FORMA_PAGAMENTO_ENTRADA'
    end
    object fdqGeracaoDocumentoID_CONTA_CORRENTE_ENTRADA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente da Entrada'
      FieldName = 'ID_CONTA_CORRENTE_ENTRADA'
      Origin = 'ID_CONTA_CORRENTE_ENTRADA'
    end
    object fdqGeracaoDocumentoCHEQUE_SACADO_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO_ENTRADA'
      Origin = 'CHEQUE_SACADO_ENTRADA'
      Size = 255
    end
    object fdqGeracaoDocumentoCHEQUE_DOC_FEDERAL_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Documento Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL_ENTRADA'
      Origin = 'CHEQUE_DOC_FEDERAL_ENTRADA'
      Size = 14
    end
    object fdqGeracaoDocumentoCHEQUE_BANCO_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO_ENTRADA'
      Origin = 'CHEQUE_BANCO_ENTRADA'
      Size = 100
    end
    object fdqGeracaoDocumentoCHEQUE_DT_EMISSAO_ENTRADA: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO_ENTRADA'
      Origin = 'CHEQUE_DT_EMISSAO_ENTRADA'
    end
    object fdqGeracaoDocumentoCHEQUE_AGENCIA_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA_ENTRADA'
      Origin = 'CHEQUE_AGENCIA_ENTRADA'
      Size = 15
    end
    object fdqGeracaoDocumentoCHEQUE_CONTA_CORRENTE_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE_ENTRADA'
      Origin = 'CHEQUE_CONTA_CORRENTE_ENTRADA'
      Size = 15
    end
    object fdqGeracaoDocumentoCHEQUE_NUMERO_ENTRADA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO_ENTRADA'
      Origin = 'CHEQUE_NUMERO_ENTRADA'
    end
    object fdqGeracaoDocumentoCHEQUE_DT_VENCIMENTO_ENTRADA: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO_ENTRADA'
      Origin = 'CHEQUE_DT_VENCIMENTO_ENTRADA'
    end
    object fdqGeracaoDocumentoID_OPERADORA_CARTAO_ENTRADA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO_ENTRADA'
      Origin = 'ID_OPERADORA_CARTAO_ENTRADA'
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento Entrada'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA'
      Origin = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA'
      ProviderFlags = []
      Size = 80
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente da Entrada'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
      Origin = 'JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoJOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA'
      Origin = 'JOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqGeracaoDocumentoParcela: TgbFDQuery
    MasterSource = dsGeracaoDocumento
    MasterFields = 'ID'
    DetailFields = 'ID_GERACAO_DOCUMENTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select gdp.*, '
      '       cr.descricao as JOIN_DESCRICAO_CENTRO_RESULTADO,'
      '       ca.descricao as JOIN_DESCRICAO_CONTA_ANALISE,'
      '       cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      'from geracao_documento_parcela gdp'
      
        'INNER JOIN centro_resultado cr on gdp.id_centro_resultado = cr.i' +
        'd'
      'INNER JOIN conta_analise ca on gdp.id_conta_analise = ca.id'
      'LEFT JOIN conta_corrente cc on gdp.id_conta_corrente = cc.id'
      'where gdp.id_geracao_documento = :id')
    Left = 56
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '6'
      end>
    object fdqGeracaoDocumentoParcelaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGeracaoDocumentoParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqGeracaoDocumentoParcelaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqGeracaoDocumentoParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqGeracaoDocumentoParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Nr. Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaID_GERACAO_DOCUMENTO: TIntegerField
      DisplayLabel = 'Gera'#231#227'o de Documento'
      FieldName = 'ID_GERACAO_DOCUMENTO'
      Origin = 'ID_GERACAO_DOCUMENTO'
    end
    object fdqGeracaoDocumentoParcelaTOTAL_PARCELAS: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'TOTAL_PARCELAS'
      Origin = 'TOTAL_PARCELAS'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqGeracaoDocumentoParcelaID_CONTA_CORRENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsGeracaoDocumento: TDataSource
    DataSet = fdqGeracaoDocumento
    Left = 115
    Top = 15
  end
end
