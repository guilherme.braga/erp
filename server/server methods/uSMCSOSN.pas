unit uSMCSOSN;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMCSOSN = class(TDSServerModule)
    fdqCSOSN: TgbFDQuery;
    dspCSOSN: TDataSetProvider;
    fdqCSOSNID: TFDAutoIncField;
    fdqCSOSNCODIGO_CST: TStringField;
    fdqCSOSNCODIGO_CSOSN: TStringField;
    fdqCSOSNDESCRICAO: TStringField;
    fdqCSOSNOBSERVACAO: TBlobField;
  private
    { Private declarations }
  public
    function GetCstCsosn(AIdCSOSCN: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses REST.JSON, uCSTCSOSNProxy, uCSTCSOSNServ;

{$R *.dfm}

{ TSMCSOSCN }

function TSMCSOSN.GetCstCsosn(
  AIdCSOSCN: Integer): String;
begin
  result := TJson.ObjectToJsonString(TCSTCSOSNServ.GetCstCsosn(AIdCSOSCN));
end;

end.

