unit uSMCor;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter, uCorProxy;

type
  TSMCor = class(TDSServerModule)
    fdqCor: TgbFDQuery;
    dspCor: TDataSetProvider;
    fdqCorID: TFDAutoIncField;
    fdqCorDESCRICAO: TStringField;
    fdqCorRGB: TStringField;
    fdqCorCMYK: TStringField;
    fdqCorBO_ATIVO: TStringField;
    fdqListaCor: TgbFDQuery;
    dspListaCor: TDataSetProvider;
    fdqListaCorID: TFDAutoIncField;
    fdqListaCorDESCRICAO: TStringField;
    fdqListaCorBO_ATIVO: TStringField;
    fdqListaCorRGB: TStringField;
    fdqListaCorCMYK: TStringField;
  private
    { Private declarations }
  public
    function GetCor(const AIdCor: Integer): TCorProxy;
    function GerarCor(const ACor: TCorProxy): Integer;
    function ExisteCor(const ADescricaoCor: String): Boolean;
    function GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
    function GetDescricaoCor(const AIdCor: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uCorServ;

{$R *.dfm}

{ TSMCor }

function TSMCor.ExisteCor(const ADescricaoCor: String): Boolean;
begin
  result := TCorServ.ExisteCor(ADescricaoCor);
end;

function TSMCor.GerarCor(const ACor: TCorProxy): Integer;
begin
  result := TCorServ.GerarCor(ACor);
end;

function TSMCor.GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
begin
  result := TCorServ.GetIdCorPorDescricao(ADescricaoCor);
end;

function TSMCor.GetCor(const AIdCor: Integer): TCorProxy;
begin
  result := TCorServ.GetCor(AIdCor);
end;

function TSMCor.GetDescricaoCor(const AIdCor: Integer): String;
begin
  result := TCorServ.GetDescricaoCor(AIdCor);
end;

end.

