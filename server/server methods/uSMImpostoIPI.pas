unit uSMImpostoIPI;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter;

type
  TSMImpostoIPI = class(TDSServerModule)
    fdqImpostoIPI: TgbFDQuery;
    dspImpostoIPI: TDataSetProvider;
    fdqImpostoIPIID: TFDAutoIncField;
    fdqImpostoIPIPERC_ALIQUOTA: TFMTBCDField;
    fdqImpostoIPIID_CST_IPI: TIntegerField;
    fdqImpostoIPIJOIN_DESCRICAO_CST_IPI: TStringField;
    fdqImpostoIPIJOIN_CODIGO_CST_IPI: TStringField;
  private
    { Private declarations }
  public
    function GetImpostoIPI(AIdImpostoIPI: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uImpostoIPIProxy, uImpostoIPIServ, REST.JSON;

{$R *.dfm}

{ TSMImpostoIPI }

function TSMImpostoIPI.GetImpostoIPI(
  AIdImpostoIPI: Integer): String;
begin
  result := TJson.ObjectToJsonString(TImpostoIPIServ.GetImpostoIPI(AIdImpostoIPI));
end;

end.

