object SMCreditoPessoa: TSMCreditoPessoa
  OldCreateOrder = False
  Height = 242
  Width = 276
  object fdqPessoaCredito: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT c.*'
      '      ,p.nome AS "JOIN_NOME_PESSOA"'
      'FROM pessoa_credito c'
      'INNER JOIN pessoa p ON c.id_pessoa = p.id'
      'WHERE c.id = :id')
    Left = 40
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqPessoaCreditoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaCreditoVL_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor'
      FieldName = 'VL_CREDITO'
      Origin = 'VL_CREDITO'
      Precision = 24
      Size = 9
    end
    object fdqPessoaCreditoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqPessoaCreditoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspPessoaCredito: TDataSetProvider
    DataSet = fdqPessoaCredito
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 136
    Top = 16
  end
  object fdqPessoaCreditoMovimento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT c.*'
      '      ,p.nome AS "JOIN_NOME_PESSOA"'
      '      ,cp.origem AS "JOIN_ORIGEM_CHAVE_PROCESSO"'
      '      ,pu.usuario AS "JOIN_USUARIO_USUARIO"'
      'FROM pessoa_credito_movimento c'
      'INNER JOIN pessoa p ON c.id_pessoa = p.id'
      'INNER JOIN pessoa_usuario pu ON c.id_pessoa_usuario = pu.id'
      'INNER JOIN chave_processo cp ON c.id_chave_processo = cp.id'
      'WHERE c.id = :id')
    Left = 64
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqPessoaCreditoMovimentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaCreditoMovimentoVL_CREDITO_ANTERIOR: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Anterior'
      FieldName = 'VL_CREDITO_ANTERIOR'
      Origin = 'VL_CREDITO_ANTERIOR'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqPessoaCreditoMovimentoVL_CREDITO_MOVIMENTO: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Movimento'
      FieldName = 'VL_CREDITO_MOVIMENTO'
      Origin = 'VL_CREDITO_MOVIMENTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqPessoaCreditoMovimentoVL_CREDITO_ATUAL: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Atual'
      FieldName = 'VL_CREDITO_ATUAL'
      Origin = 'VL_CREDITO_ATUAL'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqPessoaCreditoMovimentoDH_MOVIMENTO: TDateTimeField
      DisplayLabel = 'Dh. Movimento'
      FieldName = 'DH_MOVIMENTO'
      Origin = 'DH_MOVIMENTO'
      Required = True
    end
    object fdqPessoaCreditoMovimentoID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Documento de Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
      Required = True
    end
    object fdqPessoaCreditoMovimentoDOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Documento de Origem'
      FieldName = 'DOCUMENTO_ORIGEM'
      Origin = 'DOCUMENTO_ORIGEM'
      Required = True
      Size = 100
    end
    object fdqPessoaCreditoMovimentoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqPessoaCreditoMovimentoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqPessoaCreditoMovimentoID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqPessoaCreditoMovimentoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 15
    end
    object fdqPessoaCreditoMovimentoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqPessoaCreditoMovimentoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqPessoaCreditoMovimentoJOIN_USUARIO_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
  end
  object dspPessoaCreditoMovimento: TDataSetProvider
    DataSet = fdqPessoaCreditoMovimento
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 160
    Top = 80
  end
end
