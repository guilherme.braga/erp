object SMOrdemServico: TSMOrdemServico
  OldCreateOrder = False
  Height = 367
  Width = 468
  object dspOrdemServico: TDataSetProvider
    DataSet = fdqOrdemServico
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 8
  end
  object dsOrdemServico: TDataSource
    DataSet = fdqOrdemServico
    Left = 128
    Top = 40
  end
  object fdqOrdemServicoParcela: TgbFDQuery
    MasterSource = dsOrdemServico
    MasterFields = 'ID'
    DetailFields = 'ID_ORDEM_SERVICO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT osp.* '
      '      ,fp.descricao as JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,tq.tipo as JOIN_TIPO_FORMA_PAGAMENTO'
      '      ,oc.descricao as JOIN_DESCRICAO_OPERADORA_CARTAO'
      '      ,car.descricao AS JOIN_DESCRICAO_CARTEIRA'
      'FROM ordem_servico_parcela osp '
      'INNER JOIN forma_pagamento fp ON osp.id_forma_pagamento = fp.id'
      'INNER JOIN tipo_quitacao tq ON fp.id_tipo_quitacao = tq.id'
      'LEFT JOIN operadora_cartao oc ON osp.id_operadora_cartao = oc.id'
      'inner join carteira car on osp.id_carteira = car.id'
      'WHERE osp.id_ordem_servico = :id'
      ''
      ''
      '')
    Left = 216
    Top = 160
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqOrdemServicoParcelaID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOrdemServicoParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqOrdemServicoParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Total de Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object fdqOrdemServicoParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqOrdemServicoParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqOrdemServicoParcelaID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object fdqOrdemServicoParcelaOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object fdqOrdemServicoParcelaCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqOrdemServicoParcelaCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqOrdemServicoParcelaCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqOrdemServicoParcelaCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqOrdemServicoParcelaCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqOrdemServicoParcelaCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqOrdemServicoParcelaCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqOrdemServicoParcelaCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqOrdemServicoParcelaID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqOrdemServicoParcelaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object fdqOrdemServicoParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqOrdemServicoParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo da Forma de Pagamento'
      FieldName = 'JOIN_TIPO_FORMA_PAGAMENTO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object fdqOrdemServicoParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqOrdemServicoChecklist: TgbFDQuery
    MasterSource = dsOrdemServico
    MasterFields = 'ID'
    DetailFields = 'ID_ORDEM_SERVICO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select osc.*'
      '      ,c.descricao AS "JOIN_DESCRICAO_CHECKLIST"'
      '      ,c.nr_item AS "JOIN_NR_ITEM_CHECKLIST"'
      'from ordem_servico_checklist osc'
      'inner join checklist_item c on osc.id_checklist_item = c.id'
      'where osc.id_ordem_servico = :id')
    Left = 80
    Top = 128
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqOrdemServicoChecklistID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOrdemServicoChecklistBO_MARCADO: TStringField
      DisplayLabel = 'Selecionado'
      FieldName = 'BO_MARCADO'
      Origin = 'BO_MARCADO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqOrdemServicoChecklistID_CHECKLIST_ITEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Item do Checklist'
      FieldName = 'ID_CHECKLIST_ITEM'
      Origin = 'ID_CHECKLIST_ITEM'
      Required = True
    end
    object fdqOrdemServicoChecklistID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object fdqOrdemServicoChecklistJOIN_DESCRICAO_CHECKLIST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_CHECKLIST'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoChecklistJOIN_NR_ITEM_CHECKLIST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Item'
      FieldName = 'JOIN_NR_ITEM_CHECKLIST'
      Origin = 'NR_ITEM'
      ProviderFlags = []
    end
  end
  object fdqOrdemServicoProduto: TgbFDQuery
    MasterSource = dsOrdemServico
    MasterFields = 'ID'
    DetailFields = 'ID_ORDEM_SERVICO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT osp.*      '
      '       ,p.descricao AS JOIN_DESCRICAO_PRODUTO'
      '       ,ue.sigla AS JOIN_SIGLA_UNIDADE_ESTOQUE'
      '       ,pe.qt_estoque AS JOIN_ESTOQUE_PRODUTO'
      'FROM ordem_servico_produto osp'
      'INNER JOIN ordem_servico os ON osp.id_ordem_servico = os.id'
      'INNER JOIN produto p ON osp.id_produto = p.id'
      'LEFT JOIN unidade_estoque ue ON p.id_unidade_estoque = ue.id'
      
        'LEFT JOIN produto_filial pe ON pe.id_produto = p.id and pe.id_fi' +
        'lial = os.id_filial'
      'WHERE osp.id_ordem_servico = :id'
      ''
      ''
      '')
    Left = 192
    Top = 88
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object FMTBCDField1: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField2: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField3: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField4: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField5: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoProdutoVL_UNITARIO: TFMTBCDField
      DisplayLabel = 'Vl. Unit'#225'rio'
      FieldName = 'VL_UNITARIO'
      Origin = 'VL_UNITARIO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField6: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField7: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FMTBCDField8: TFMTBCDField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object fdqOrdemServicoProdutoID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqOrdemServicoProdutoJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object fdqOrdemServicoProdutoJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
  end
  object fdqOrdemServicoServico: TgbFDQuery
    MasterSource = dsOrdemServico
    MasterFields = 'ID'
    DetailFields = 'ID_ORDEM_SERVICO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT oss.*'
      '      ,s.descricao AS "JOIN_DESCRICAO_SERVICO"'
      '      ,s.vl_venda AS "JOIN_VALOR_VENDA_SERVICO"'
      '      ,s.duracao AS "JOIN_DURACAO_SERVICO"'
      'FROM ordem_servico_servico oss'
      'INNER JOIN servico s ON oss.id_servico = s.id'
      'WHERE oss.id_ordem_servico = :id')
    Left = 56
    Top = 72
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqOrdemServicoServicoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOrdemServicoServicoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqOrdemServicoServicoQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoPERC_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoPERC_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_BRUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_LIQUIDO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoVL_UNITARIO: TFMTBCDField
      DisplayLabel = 'Vl. Unit'#225'rio'
      FieldName = 'VL_UNITARIO'
      Origin = 'VL_UNITARIO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object fdqOrdemServicoServicoID_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo do Servi'#231'o'
      FieldName = 'ID_SERVICO'
      Origin = 'ID_SERVICO'
      Required = True
    end
    object fdqOrdemServicoServicoJOIN_DESCRICAO_SERVICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Servi'#231'o'
      FieldName = 'JOIN_DESCRICAO_SERVICO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoServicoJOIN_VALOR_VENDA_SERVICO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor'
      FieldName = 'JOIN_VALOR_VENDA_SERVICO'
      Origin = 'VL_VENDA'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoServicoJOIN_DURACAO_SERVICO: TTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'JOIN_DURACAO_SERVICO'
      Origin = 'DURACAO'
      ProviderFlags = []
    end
    object fdqOrdemServicoServicoBO_SERVICO_TERCEIRO: TStringField
      DisplayLabel = 'Servi'#231'o de Terceiro'
      FieldName = 'BO_SERVICO_TERCEIRO'
      Origin = 'BO_SERVICO_TERCEIRO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object fdqOrdemServico: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select os.*'
      '      ,pu.usuario as "JOIN_USUARIO"'
      '      ,f.fantasia as "JOIN_FANTASIA_FILIAL"'
      '      ,cv.origem as "JOIN_ORIGEM_CHAVE_PROCESSO"'
      '      ,s.descricao as "JOIN_DESCRICAO_SITUACAO"'
      '      ,c.descricao as "JOIN_DESCRICAO_CHECKLIST"'
      '      ,tp.descricao as "JOIN_DESCRICAO_TABELA_PRECO"'
      '      ,pp.descricao as "JOIN_DESCRICAO_PLANO_PAGAMENTO"'
      '      ,o.descricao as "JOIN_DESCRICAO_OPERACAO"'
      '      ,ca.descricao as "JOIN_DESCRICAO_CONTA_ANALISE"'
      '      ,e.descricao as "JOIN_DESCRICAO_EQUIPAMENTO"'
      '      ,e.serie as "JOIN_SERIE_EQUIPAMENTO"'
      '      ,e.imei as "JOIN_IMEI_EQUIPAMENTO"'
      '      ,p.nome as "JOIN_NOME_PESSOA"'
      '      ,cr.descricao as "JOIN_DESCRICAO_CENTRO_RESULTADO"'
      '      ,pc.vl_credito as JOIN_VALOR_PESSOA_CREDITO'
      '      ,v.placa AS JOIN_PLACA_VEICULO'
      '      ,v.ano AS JOIN_ANO_VEICULO'
      '      ,v.combustivel AS JOIN_COMBUSTIVEL_VEICULO'
      
        '      ,(select descricao from cor where id = v.id_cor) as JOIN_C' +
        'OR_VEICULO'
      
        '      ,(select descricao from modelo where id = v.id_modelo) as ' +
        'JOIN_MODELO_VEICULO'
      
        '      ,(select descricao from marca where id = v.id_marca) as JO' +
        'IN_MARCA_VEICULO'
      'from ordem_servico os'
      'inner join pessoa_usuario pu on os.id_usuario_recepcao = pu.id'
      'inner join filial f on os.id_filial = f.id'
      'inner join chave_processo cv on os.id_chave_processo = cv.id'
      'inner join situacao s on os.id_situacao = s.id'
      ' left join checklist c on os.id_checklist = c.id'
      ' left join tabela_preco tp on os.id_tabela_preco = tp.id'
      'inner join centro_resultado cr on os.id_centro_resultado = cr.id'
      'inner join conta_analise ca on os.id_conta_analise = ca.id'
      ' left join plano_pagamento pp on os.id_plano_pagamento = pp.id'
      'inner join operacao o on os.id_operacao = o.id'
      ' left join equipamento e on os.id_equipamento = e.id'
      'inner join pessoa p on os.id_pessoa = p.id'
      ' left join pessoa_credito pc ON p.id = pc.id_pessoa'
      ' left join veiculo v on os.id_veiculo = v.id'
      'where os.id = :id')
    Left = 288
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqOrdemServicoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOrdemServicoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqOrdemServicoDH_FECHAMENTO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object fdqOrdemServicoDH_ENTRADA: TDateTimeField
      DisplayLabel = 'Dh. Entrada'
      FieldName = 'DH_ENTRADA'
      Origin = 'DH_ENTRADA'
      Required = True
    end
    object fdqOrdemServicoDH_PREVISAO_TERMINO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Previs'#227'o do T'#233'rmino'
      FieldName = 'DH_PREVISAO_TERMINO'
      Origin = 'DH_PREVISAO_TERMINO'
    end
    object fdqOrdemServicoSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object fdqOrdemServicoVL_TOTAL_BRUTO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Produto'
      FieldName = 'VL_TOTAL_BRUTO_PRODUTO'
      Origin = 'VL_TOTAL_BRUTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_DESCONTO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Produto'
      FieldName = 'VL_TOTAL_DESCONTO_PRODUTO'
      Origin = 'VL_TOTAL_DESCONTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_DESCONTO_PRODUTO: TFMTBCDField
      DisplayLabel = '% Desconto Produto'
      FieldName = 'PERC_TOTAL_DESCONTO_PRODUTO'
      Origin = 'PERC_TOTAL_DESCONTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Produto'
      FieldName = 'VL_TOTAL_ACRESCIMO_PRODUTO'
      Origin = 'VL_TOTAL_ACRESCIMO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Produto'
      FieldName = 'PERC_TOTAL_ACRESCIMO_PRODUTO'
      Origin = 'PERC_TOTAL_ACRESCIMO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_LIQUIDO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Produto'
      FieldName = 'VL_TOTAL_LIQUIDO_PRODUTO'
      Origin = 'VL_TOTAL_LIQUIDO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_BRUTO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Servi'#231'o'
      FieldName = 'VL_TOTAL_BRUTO_SERVICO'
      Origin = 'VL_TOTAL_BRUTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_DESCONTO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Servi'#231'o'
      FieldName = 'VL_TOTAL_DESCONTO_SERVICO'
      Origin = 'VL_TOTAL_DESCONTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_DESCONTO_SERVICO: TFMTBCDField
      DisplayLabel = '% Desconto Servi'#231'o'
      FieldName = 'PERC_TOTAL_DESCONTO_SERVICO'
      Origin = 'PERC_TOTAL_DESCONTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Servi'#231'o'
      FieldName = 'VL_TOTAL_ACRESCIMO_SERVICO'
      Origin = 'VL_TOTAL_ACRESCIMO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Servi'#231'o'
      FieldName = 'PERC_TOTAL_ACRESCIMO_SERVICO'
      Origin = 'PERC_TOTAL_ACRESCIMO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_LIQUIDO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Servi'#231'o'
      FieldName = 'VL_TOTAL_LIQUIDO_SERVICO'
      Origin = 'VL_TOTAL_LIQUIDO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_BRUTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_BRUTO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_BRUTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = '% Desconto Servi'#231'o Terceiro'
      FieldName = 'PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Origin = 'PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Servi'#231'o Terceiro'
      FieldName = 'PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Origin = 'PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_LIQUIDO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_TOTAL_DESCONTO'
      Origin = 'VL_TOTAL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_DESCONTO: TFMTBCDField
      DisplayLabel = '% Total Desconto'
      FieldName = 'PERC_TOTAL_DESCONTO'
      Origin = 'PERC_TOTAL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_TOTAL_ACRESCIMO'
      Origin = 'VL_TOTAL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoPERC_TOTAL_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Total Acr'#233'scimo'
      FieldName = 'PERC_TOTAL_ACRESCIMO'
      Origin = 'PERC_TOTAL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_TOTAL_LIQUIDO'
      Origin = 'VL_TOTAL_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_TOTAL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_TOTAL_BRUTO'
      Origin = 'VL_TOTAL_BRUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_PAGAMENTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Pagamento'
      FieldName = 'VL_PAGAMENTO'
      Origin = 'VL_PAGAMENTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cr'#233'dito Utilizado'
      FieldName = 'VL_PESSOA_CREDITO_UTILIZADO'
      Origin = 'VL_PESSOA_CREDITO_UTILIZADO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqOrdemServicoID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqOrdemServicoID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqOrdemServicoID_CHECKLIST: TIntegerField
      DisplayLabel = 'C'#243'digo do Checklist'
      FieldName = 'ID_CHECKLIST'
      Origin = 'ID_CHECKLIST'
    end
    object fdqOrdemServicoID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
    end
    object fdqOrdemServicoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object fdqOrdemServicoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object fdqOrdemServicoID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
    end
    object fdqOrdemServicoID_SITUACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Situa'#231#227'o'
      FieldName = 'ID_SITUACAO'
      Origin = 'ID_SITUACAO'
      Required = True
    end
    object fdqOrdemServicoID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
      Required = True
    end
    object fdqOrdemServicoID_EQUIPAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Equipamento'
      FieldName = 'ID_EQUIPAMENTO'
      Origin = 'ID_EQUIPAMENTO'
    end
    object fdqOrdemServicoID_USUARIO_RECEPCAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Recep'#231#227'o'
      FieldName = 'ID_USUARIO_RECEPCAO'
      Origin = 'ID_USUARIO_RECEPCAO'
      Required = True
    end
    object fdqOrdemServicoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqOrdemServicoID_VEICULO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Ve'#237'culo'
      FieldName = 'ID_VEICULO'
      Origin = 'ID_VEICULO'
    end
    object fdqOrdemServicoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqOrdemServicoPROBLEMA_RELATADO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Problema Relatado'
      FieldName = 'PROBLEMA_RELATADO'
      Origin = 'PROBLEMA_RELATADO'
    end
    object fdqOrdemServicoPROBLEMA_ENCONTRADO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Problema Encontrado'
      FieldName = 'PROBLEMA_ENCONTRADO'
      Origin = 'PROBLEMA_ENCONTRADO'
    end
    object fdqOrdemServicoSOLUCAO_TECNICA: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Solu'#231#227'o T'#233'cnica'
      FieldName = 'SOLUCAO_TECNICA'
      Origin = 'SOLUCAO_TECNICA'
    end
    object fdqOrdemServicoKM: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'KM'
      Origin = 'KM'
    end
    object fdqOrdemServicoTANQUE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TANQUE'
      Origin = 'TANQUE'
      Size = 3
    end
    object fdqOrdemServicoJOIN_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object fdqOrdemServicoJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem da Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqOrdemServicoJOIN_DESCRICAO_SITUACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_SITUACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_DESCRICAO_CHECKLIST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Checklist'
      FieldName = 'JOIN_DESCRICAO_CHECKLIST'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_DESCRICAO_TABELA_PRECO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 45
    end
    object fdqOrdemServicoJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_DESCRICAO_OPERACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_DESCRICAO_EQUIPAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Equipamento'
      FieldName = 'JOIN_DESCRICAO_EQUIPAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_SERIE_EQUIPAMENTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_SERIE_EQUIPAMENTO'
      Origin = 'SERIE'
      ProviderFlags = []
      Size = 255
    end
    object fdqOrdemServicoJOIN_IMEI_EQUIPAMENTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_IMEI_EQUIPAMENTO'
      Origin = 'IMEI'
      ProviderFlags = []
      Size = 100
    end
    object fdqOrdemServicoJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cr'#233'dito Dispon'#237'vel'
      FieldName = 'JOIN_VALOR_PESSOA_CREDITO'
      Origin = 'VL_CREDITO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqOrdemServicoJOIN_PLACA_VEICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Placa do Ve'#237'culo'
      FieldName = 'JOIN_PLACA_VEICULO'
      Origin = 'PLACA'
      ProviderFlags = []
      Size = 7
    end
    object fdqOrdemServicoJOIN_ANO_VEICULO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ano do Ve'#237'culo'
      FieldName = 'JOIN_ANO_VEICULO'
      Origin = 'ANO'
      ProviderFlags = []
    end
    object fdqOrdemServicoJOIN_COMBUSTIVEL_VEICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Combust'#237'vel do Ve'#237'culo'
      FieldName = 'JOIN_COMBUSTIVEL_VEICULO'
      Origin = 'COMBUSTIVEL'
      ProviderFlags = []
      Size = 30
    end
    object fdqOrdemServicoJOIN_COR_VEICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cor do Ve'#237'culo'
      FieldName = 'JOIN_COR_VEICULO'
      Origin = 'JOIN_COR_VEICULO'
      ProviderFlags = []
      Size = 100
    end
    object fdqOrdemServicoJOIN_MODELO_VEICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo do Ve'#237'culo'
      FieldName = 'JOIN_MODELO_VEICULO'
      Origin = 'JOIN_MODELO_VEICULO'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoJOIN_MARCA_VEICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Marca do Ve'#237'culo'
      FieldName = 'JOIN_MARCA_VEICULO'
      Origin = 'JOIN_MARCA_VEICULO'
      ProviderFlags = []
      Size = 80
    end
    object fdqOrdemServicoEQUIPAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Equipamento'
      FieldName = 'EQUIPAMENTO'
      Origin = 'EQUIPAMENTO'
      Size = 255
    end
    object fdqOrdemServicoEQUIPAMENTO_SERIE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'S'#233'rie Equipamento'
      FieldName = 'EQUIPAMENTO_SERIE'
      Origin = 'EQUIPAMENTO_SERIE'
      Size = 255
    end
    object fdqOrdemServicoEQUIPAMENTO_IMEI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'IMEI Equipamento'
      FieldName = 'EQUIPAMENTO_IMEI'
      Origin = 'EQUIPAMENTO_IMEI'
      Size = 100
    end
  end
end
