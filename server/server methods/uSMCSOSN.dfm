object SMCSOSN: TSMCSOSN
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqCSOSN: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from CST_CSOSN where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCSOSNID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCSOSNCODIGO_CST: TStringField
      DisplayLabel = 'C'#243'digo CST'
      FieldName = 'CODIGO_CST'
      Origin = 'CODIGO_CST'
      Size = 5
    end
    object fdqCSOSNCODIGO_CSOSN: TStringField
      DisplayLabel = 'C'#243'digo CSOSN'
      FieldName = 'CODIGO_CSOSN'
      Origin = 'CODIGO_CSOSN'
      Size = 5
    end
    object fdqCSOSNDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCSOSNOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspCSOSN: TDataSetProvider
    DataSet = fdqCSOSN
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
