object SMProduto: TSMProduto
  OldCreateOrder = False
  Height = 486
  Width = 496
  object fdqProduto: TgbFDQuery
    AfterPost = fdqProdutoAfterPost
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT p.* '
      '      ,gp.descricao AS JOIN_DESCRICAO_GRUPO_PRODUTO '
      '      ,sgp.descricao AS JOIN_DESCRICAO_SUB_GRUPO_PRODUTO '
      '      ,m.descricao AS JOIN_DESCRICAO_MARCA '
      '      ,c.descricao AS JOIN_DESCRICAO_CATEGORIA'
      '      ,sb.descricao AS JOIN_DESCRICAO_SUB_CATEGORIA'
      '      ,l.descricao AS JOIN_DESCRICAO_LINHA'
      '      ,m.descricao AS JOIN_DESCRICAO_MODELO'
      '      ,un.descricao AS JOIN_DESCRICAO_UNIDADE_ESTOQUE'
      '      ,cor.descricao AS JOIN_DESCRICAO_COR'
      '      ,tamanho.descricao AS JOIN_DESCRICAO_TAMANHO'
      '      ,ncm.descricao AS JOIN_DESCRICAO_NCM'
      '      ,ncm.codigo AS JOIN_CODIGO_NCM'
      '      ,ncme.descricao AS JOIN_DESCRICAO_NCM_ESPECIALIZADO'
      '      ,ri.descricao AS JOIN_DESCRICAO_REGRA_IMPOSTO'
      'FROM produto p'
      'LEFT JOIN grupo_produto gp ON p.id_grupo_produto = gp.id'
      
        'LEFT JOIN sub_grupo_produto sgp ON p.id_sub_grupo_produto = sgp.' +
        'id'
      'LEFT JOIN marca m ON p.id_marca = m.id'
      'LEFT JOIN categoria c ON p.id_categoria = c.id'
      'LEFT JOIN sub_categoria sb ON p.id_sub_categoria = sb.id'
      'LEFT JOIN linha l ON p.id_linha = l.id'
      'LEFT JOIN modelo md ON p.id_modelo = md.id'
      'LEFT JOIN unidade_estoque un ON p.id_categoria = un.id'
      'LEFT JOIN cor ON p.id_cor = cor.id'
      'LEFT JOIN tamanho ON p.id_tamanho = tamanho.id'
      'LEFT JOIN ncm ON p.id_ncm = ncm.id'
      
        'LEFT JOIN ncm_especializado ncme ON p.id_ncm_especializado = ncm' +
        'e.id'
      'LEFT JOIN regra_imposto ri ON p.id_regra_imposto = ri.id'
      'WHERE p.id = :ID')
    Left = 32
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqProdutoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqProdutoID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
    end
    object fdqProdutoID_SUB_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
    end
    object fdqProdutoID_MARCA: TIntegerField
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
    end
    object fdqProdutoID_CATEGORIA: TIntegerField
      DisplayLabel = 'Categoria'
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
    end
    object fdqProdutoID_SUB_CATEGORIA: TIntegerField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
    end
    object fdqProdutoID_LINHA: TIntegerField
      DisplayLabel = 'Linha'
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
    end
    object fdqProdutoID_MODELO: TIntegerField
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object fdqProdutoID_UNIDADE_ESTOQUE: TIntegerField
      DisplayLabel = 'UN'
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
    end
    object fdqProdutoID_COR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
    end
    object fdqProdutoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 30
    end
    object fdqProdutoCODIGO_BARRA: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO_BARRA'
      Origin = 'CODIGO_BARRA'
      Required = True
      Size = 30
    end
    object fdqProdutoJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_MARCA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_CATEGORIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Categoria'
      FieldName = 'JOIN_DESCRICAO_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_SUB_CATEGORIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sub Categoria'
      FieldName = 'JOIN_DESCRICAO_SUB_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_LINHA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Linha'
      FieldName = 'JOIN_DESCRICAO_LINHA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UN'
      FieldName = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoJOIN_DESCRICAO_COR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object fdqProdutoJOIN_DESCRICAO_TAMANHO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tamanho'
      FieldName = 'JOIN_DESCRICAO_TAMANHO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqProdutoFORMA_AQUISICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Aquisi'#231#227'o'
      FieldName = 'FORMA_AQUISICAO'
      Origin = 'FORMA_AQUISICAO'
      Size = 45
    end
    object fdqProdutoID_NCM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'ID_NCM'
      Origin = 'ID_NCM'
    end
    object fdqProdutoID_NCM_ESPECIALIZADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do NCM Especializado'
      FieldName = 'ID_NCM_ESPECIALIZADO'
      Origin = 'ID_NCM_ESPECIALIZADO'
    end
    object fdqProdutoJOIN_DESCRICAO_NCM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'NCM'
      FieldName = 'JOIN_DESCRICAO_NCM'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqProdutoJOIN_CODIGO_NCM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo NCM'
      FieldName = 'JOIN_CODIGO_NCM'
      Origin = 'CODIGO'
      ProviderFlags = []
    end
    object fdqProdutoJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'NCM Especializado'
      FieldName = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqProdutoTIPO_TRIBUTACAO_PIS_COFINS_ST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Tributa'#231#227'o PIS/COFINS/ST'
      FieldName = 'TIPO_TRIBUTACAO_PIS_COFINS_ST'
      Origin = 'TIPO_TRIBUTACAO_PIS_COFINS_ST'
    end
    object fdqProdutoTIPO_TRIBUTACAO_IPI: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Tributa'#231#227'o IPI'
      FieldName = 'TIPO_TRIBUTACAO_IPI'
      Origin = 'TIPO_TRIBUTACAO_IPI'
    end
    object fdqProdutoEXCECAO_IPI: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Exce'#231#227'o do IPI'
      FieldName = 'EXCECAO_IPI'
      Origin = 'EXCECAO_IPI'
    end
    object fdqProdutoID_REGRA_IMPOSTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Regra de Imposto'
      FieldName = 'ID_REGRA_IMPOSTO'
      Origin = 'ID_REGRA_IMPOSTO'
    end
    object fdqProdutoJOIN_DESCRICAO_REGRA_IMPOSTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regra Imposto'
      FieldName = 'JOIN_DESCRICAO_REGRA_IMPOSTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqProdutoORIGEM_DA_MERCADORIA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'ORIGEM_DA_MERCADORIA'
      Origin = 'ORIGEM_DA_MERCADORIA'
    end
    object fdqProdutoDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object fdqProdutoBO_PRODUTO_AGRUPADOR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto Agrupador'
      FieldName = 'BO_PRODUTO_AGRUPADOR'
      Origin = 'BO_PRODUTO_AGRUPADOR'
      FixedChar = True
      Size = 1
    end
    object fdqProdutoBO_PRODUTO_AGRUPADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto Agrupado'
      FieldName = 'BO_PRODUTO_AGRUPADO'
      Origin = 'BO_PRODUTO_AGRUPADO'
      FixedChar = True
      Size = 1
    end
    object fdqProdutoID_PRODUTO_AGRUPADOR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Produto Agrupador'
      FieldName = 'ID_PRODUTO_AGRUPADOR'
      Origin = 'ID_PRODUTO_AGRUPADOR'
    end
    object fdqProdutoID_TAMANHO: TIntegerField
      FieldName = 'ID_TAMANHO'
      Origin = 'ID_TAMANHO'
    end
  end
  object dspProduto: TDataSetProvider
    DataSet = fdqProduto
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 8
  end
  object fdqConsulta: TFDQuery
    Connection = Dm.Connection
    Left = 440
    Top = 16
  end
  object fdqGrupoProduto: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM GRUPO_PRODUTO WHERE ID = :ID')
    Left = 33
    Top = 61
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqGrupoProdutoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGrupoProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqGrupoProdutoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspGrupoProduto: TDataSetProvider
    DataSet = fdqGrupoProduto
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 63
    Top = 61
  end
  object fdqSubGrupo: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT '
      '  sub_grupo_produto.*,'
      '  grupo_produto.descricao as "JOIN_DESCRICAO_GRUPO_PRODUTO" '
      'FROM SUB_GRUPO_PRODUTO '
      'INNER JOIN GRUPO_PRODUTO ON '
      '  SUB_GRUPO_PRODUTO.ID_GRUPO_PRODUTO = GRUPO_PRODUTO.ID'
      'WHERE SUB_GRUPO_PRODUTO.ID = :ID')
    Left = 33
    Top = 117
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqSubGrupoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqSubGrupoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqSubGrupoID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
    end
    object fdqSubGrupoJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspSubGrupo: TDataSetProvider
    DataSet = fdqSubGrupo
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 63
    Top = 117
  end
  object fdqCategoria: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM CATEGORIA WHERE ID = :ID')
    Left = 33
    Top = 173
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCategoriaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCategoriaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqCategoriaOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspCategoria: TDataSetProvider
    DataSet = fdqCategoria
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 63
    Top = 173
  end
  object fdqSubCategoria: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM SUB_CATEGORIA WHERE ID = :ID')
    Left = 33
    Top = 229
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqSubCategoriaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqSubCategoriaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqSubCategoriaOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspSubCategoria: TDataSetProvider
    DataSet = fdqSubCategoria
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 63
    Top = 229
  end
  object fdqMarca: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM MARCA WHERE ID = :ID')
    Left = 36
    Top = 280
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqMarcaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMarcaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dspMarca: TDataSetProvider
    DataSet = fdqMarca
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 66
    Top = 280
  end
  object fdqLinha: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM LINHA WHERE ID = :ID')
    Left = 36
    Top = 336
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqLinhaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLinhaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqLinhaOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspLinha: TDataSetProvider
    DataSet = fdqLinha
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 66
    Top = 336
  end
  object fdqModelo: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM MODELO WHERE ID = :ID')
    Left = 160
    Top = 128
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqModeloID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqModeloDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqModeloOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspModelo: TDataSetProvider
    DataSet = fdqModelo
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 190
    Top = 128
  end
  object dspUnidadeEstoque: TDataSetProvider
    DataSet = fdqUnidadeEstoque
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 190
    Top = 176
  end
  object fdqUnidadeEstoque: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM UNIDADE_ESTOQUE WHERE ID = :ID')
    Left = 160
    Top = 176
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqUnidadeEstoqueID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqUnidadeEstoqueSIGLA: TStringField
      DisplayLabel = 'Sigla'
      FieldName = 'SIGLA'
      Origin = 'SIGLA'
      Required = True
      Size = 3
    end
    object fdqUnidadeEstoqueDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dsProduto: TDataSource
    DataSet = fdqProduto
    Left = 96
    Top = 8
  end
  object fdqProdutoCodigoBarra: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM produto_codigo_barra WHERE id_produto = :ID')
    Left = 172
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoCodigoBarraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoCodigoBarraCODIGO: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Required = True
      Size = 100
    end
    object fdqProdutoCodigoBarraID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
  end
  object fdqProdutoFilial: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT pf.*'
      '      ,f.fantasia as JOIN_DESCRICAO_FILIAL '
      'FROM PRODUTO_FILIAL pf'
      'INNER JOIN filial f ON pf.id_filial = f.id'
      'WHERE pf.id_produto = :id')
    Left = 284
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoFilialID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoFilialQT_ESTOQUE: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'QT_ESTOQUE'
      Origin = 'QT_ESTOQUE'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialLOCALIZACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Localiza'#231#227'o'
      FieldName = 'LOCALIZACAO'
      Origin = 'LOCALIZACAO'
      Size = 255
    end
    object fdqProdutoFilialQT_ESTOQUE_MINIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque M'#237'nimo'
      FieldName = 'QT_ESTOQUE_MINIMO'
      Origin = 'QT_ESTOQUE_MINIMO'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialQT_ESTOQUE_MAXIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque M'#225'ximo'
      FieldName = 'QT_ESTOQUE_MAXIMO'
      Origin = 'QT_ESTOQUE_MAXIMO'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqProdutoFilialID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqProdutoFilialJOIN_DESCRICAO_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_DESCRICAO_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqProdutoFilialQT_ESTOQUE_CONDICIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'QT_ESTOQUE_CONDICIONAL'
      Origin = 'QT_ESTOQUE_CONDICIONAL'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_CUSTO_MEDIO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo M'#233'dio'
      FieldName = 'VL_CUSTO_MEDIO'
      Origin = 'VL_CUSTO_MEDIO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_CUSTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo Custo'
      FieldName = 'VL_ULTIMO_CUSTO'
      Origin = 'VL_ULTIMO_CUSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_FRETE: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo Frete'
      FieldName = 'VL_ULTIMO_FRETE'
      Origin = 'VL_ULTIMO_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_SEGURO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo Seguro'
      FieldName = 'VL_ULTIMO_SEGURO'
      Origin = 'VL_ULTIMO_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_ICMS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo ICMS ST'
      FieldName = 'VL_ULTIMO_ICMS_ST'
      Origin = 'VL_ULTIMO_ICMS_ST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_IPI: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo IPI'
      FieldName = 'VL_ULTIMO_IPI'
      Origin = 'VL_ULTIMO_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo Desconto'
      FieldName = 'VL_ULTIMO_DESCONTO'
      Origin = 'VL_ULTIMO_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltima Despesa Acessoria'
      FieldName = 'VL_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'VL_ULTIMA_DESPESA_ACESSORIA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. '#218'ltimo Custo Imposto'
      FieldName = 'VL_ULTIMO_CUSTO_IMPOSTO'
      Origin = 'VL_ULTIMO_CUSTO_IMPOSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_CUSTO_MEDIO_IMPOSTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo M'#233'dio Imposto'
      FieldName = 'VL_CUSTO_MEDIO_IMPOSTO'
      Origin = 'VL_CUSTO_MEDIO_IMPOSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_CUSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo Operacional'
      FieldName = 'VL_CUSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo Imposto Operacional'
      FieldName = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMO_FRETE: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltimo Frete'
      FieldName = 'PERC_ULTIMO_FRETE'
      Origin = 'PERC_ULTIMO_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMO_SEGURO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltimo Seguro'
      FieldName = 'PERC_ULTIMO_SEGURO'
      Origin = 'PERC_ULTIMO_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMO_ICMS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltimo ICMS ST'
      FieldName = 'PERC_ULTIMO_ICMS_ST'
      Origin = 'PERC_ULTIMO_ICMS_ST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMO_IPI: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltimo IPI'
      FieldName = 'PERC_ULTIMO_IPI'
      Origin = 'PERC_ULTIMO_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMO_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltimo Desconto'
      FieldName = 'PERC_ULTIMO_DESCONTO'
      Origin = 'PERC_ULTIMO_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% '#218'ltima Despesa Acess'#243'ria'
      FieldName = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialPERC_CUSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Custo Operacional'
      FieldName = 'PERC_CUSTO_OPERACIONAL'
      Origin = 'PERC_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoFilialVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo M'#233'dio Imposto Operacional'
      FieldName = 'VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
  end
  object fdqProdutoFornecedor: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      
        'SELECT *, (SELECT nome FROM pessoa WHERE id = PRODUTO_FORNECEDOR' +
        '.id_pessoa) as JOIN_NOME_FORNECEDOR'
      'FROM PRODUTO_FORNECEDOR '
      'WHERE ID_PRODUTO = :ID')
    Left = 172
    Top = 56
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoFornecedorID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoFornecedorREFERENCIA: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
      Required = True
      Size = 100
    end
    object fdqProdutoFornecedorID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object fdqProdutoFornecedorID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo do Fornecedor'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqProdutoFornecedorJOIN_NOME_FORNECEDOR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Fornecedor'
      FieldName = 'JOIN_NOME_FORNECEDOR'
      Origin = 'JOIN_NOME_FORNECEDOR'
      ProviderFlags = []
      Size = 80
    end
  end
  object fdqProdutoTabelaPreco: TgbFDQuery
    AfterPost = fdqProdutoAfterPost
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * '
      'FROM produto p'
      '')
    Left = 344
    Top = 168
    object fdqProdutoTabelaPrecoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoTabelaPrecoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqProdutoTabelaPrecoBO_ATIVO: TStringField
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqProdutoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_MARCA: TIntegerField
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_CATEGORIA: TIntegerField
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_SUB_CATEGORIA: TIntegerField
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_LINHA: TIntegerField
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_MODELO: TIntegerField
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
      Required = True
    end
    object fdqProdutoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
      Required = True
    end
    object fdqProdutoTabelaPrecoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 30
    end
    object fdqProdutoTabelaPrecoCODIGO_BARRA: TStringField
      FieldName = 'CODIGO_BARRA'
      Origin = 'CODIGO_BARRA'
      Required = True
      Size = 30
    end
  end
  object dspProdutoTabelaPreco: TDataSetProvider
    DataSet = fdqProdutoTabelaPreco
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 376
    Top = 168
  end
  object fdqTodosGruposProduto: TgbFDQuery
    AfterPost = fdqProdutoAfterPost
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM GRUPO_PRODUTO')
    Left = 328
    Top = 264
    object fdqTodosGruposProdutoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqTodosGruposProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dspTodosGruposProduto: TDataSetProvider
    DataSet = fdqTodosGruposProduto
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 360
    Top = 264
  end
  object fdqProdutoGrupoTabelaPreco: TgbFDQuery
    AfterPost = fdqProdutoAfterPost
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      '    SELECT P.*, TP.VL_VENDA'
      '      FROM PRODUTO P'
      'INNER JOIN TABELA_PRECO_ITEM TP'
      '        ON P.id = tp.id_produto  '
      '     WHERE id_grupo_produto = :id_grupo_produto'
      '       and tp.id_tabela_preco = :id_tabela_preco'
      'ORDER BY p.descricao')
    Left = 328
    Top = 320
    ParamData = <
      item
        Name = 'ID_GRUPO_PRODUTO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'ID_TABELA_PRECO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoGrupoTabelaPrecoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoGrupoTabelaPrecoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqProdutoGrupoTabelaPrecoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqProdutoGrupoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_MARCA: TIntegerField
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_CATEGORIA: TIntegerField
      DisplayLabel = 'Categoria'
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_SUB_CATEGORIA: TIntegerField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_LINHA: TIntegerField
      DisplayLabel = 'Linha'
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_MODELO: TIntegerField
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField
      DisplayLabel = 'UN'
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
      Required = True
    end
    object fdqProdutoGrupoTabelaPrecoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 30
    end
    object fdqProdutoGrupoTabelaPrecoCODIGO_BARRA: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO_BARRA'
      Origin = 'CODIGO_BARRA'
      Required = True
      Size = 30
    end
    object fdqProdutoGrupoTabelaPrecoVL_VENDA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      ProviderFlags = []
      ReadOnly = True
      Precision = 24
      Size = 9
    end
  end
  object dspProdutoGrupoTabelaPreco: TDataSetProvider
    DataSet = fdqProdutoGrupoTabelaPreco
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 360
    Top = 320
  end
  object fdqProdutoTributacaoPorUnidade: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      
        'SELECT * FROM PRODUTO_TRIBUTACAO_POR_UNIDADE WHERE ID_PRODUTO = ' +
        ':ID')
    Left = 332
    Top = 56
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoTributacaoPorUnidadeID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_ICMS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Base C'#225'lculo ICMS'
      FieldName = 'VL_BASE_CALCULO_ICMS'
      Origin = 'VL_BASE_CALCULO_ICMS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_ICMS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Base C'#225'lculo ICMS ST'
      FieldName = 'VL_BASE_CALCULO_ICMS_ST'
      Origin = 'VL_BASE_CALCULO_ICMS_ST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_IPI: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Base C'#225'lculo IPI'
      FieldName = 'VL_BASE_CALCULO_IPI'
      Origin = 'VL_BASE_CALCULO_IPI'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_PIS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. PIS'
      FieldName = 'VL_PIS'
      Origin = 'VL_PIS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_COFINS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Cofins'
      FieldName = 'VL_COFINS'
      Origin = 'VL_COFINS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_PIS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. PIS ST'
      FieldName = 'VL_PIS_ST'
      Origin = 'VL_PIS_ST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeVL_COFINS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Cofins'
      FieldName = 'VL_COFINS_ST'
      Origin = 'VL_COFINS_ST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqProdutoTributacaoPorUnidadeID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
  end
  object fdqProdutoMontadora: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT PRODUTO_MONTADORA.* '
      'FROM produto_montadora'
      'WHERE PRODUTO_MONTADORA.ID_PRODUTO = :ID')
    Left = 160
    Top = 232
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoMontadoraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoMontadoraID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object fdqProdutoMontadoraNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqProdutoMontadoraMONTADORA: TStringField
      DisplayLabel = 'Montadora'
      FieldName = 'MONTADORA'
      Origin = 'MONTADORA'
      Required = True
      Size = 255
    end
    object fdqProdutoMontadoraREFERENCIA: TStringField
      DisplayLabel = 'C'#243'digo Original'
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
      Size = 255
    end
    object fdqProdutoMontadoraESPECIFICACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Especifica'#231#227'o'
      FieldName = 'ESPECIFICACAO'
      Origin = 'ESPECIFICACAO'
    end
  end
  object fdqProdutoModeloMontadora: TgbFDQuery
    MasterSource = dsProduto
    MasterFields = 'ID'
    DetailFields = 'ID_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT produto_modelo_montadora.* '
      'FROM produto_modelo_montadora'
      'WHERE produto_modelo_montadora.ID_PRODUTO = :ID')
    Left = 160
    Top = 280
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqProdutoModeloMontadoraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqProdutoModeloMontadoraID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object fdqProdutoModeloMontadoraNR_ITEM_MONTADORA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Item Montadora'
      FieldName = 'NR_ITEM_MONTADORA'
      Origin = 'NR_ITEM_MONTADORA'
    end
    object fdqProdutoModeloMontadoraMODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Required = True
      Size = 255
    end
  end
end
