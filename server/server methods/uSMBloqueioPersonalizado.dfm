object SMBloqueioPersonalizado: TSMBloqueioPersonalizado
  OldCreateOrder = False
  Height = 280
  Width = 405
  object fdqBloqueioPersonalizado: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from bloqueio_personalizado where id = :id')
    Left = 112
    Top = 40
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqBloqueioPersonalizadoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqBloqueioPersonalizadoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqBloqueioPersonalizadoSQL: TBlobField
      FieldName = 'SQL'
      Origin = '`SQL`'
      Required = True
    end
    object fdqBloqueioPersonalizadoBO_VENDA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Utilizar na Venda'
      FieldName = 'BO_VENDA'
      Origin = 'BO_VENDA'
      FixedChar = True
      Size = 1
    end
    object fdqBloqueioPersonalizadoBO_NOTA_FISCAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Utilizar na Nota Fiscal'
      FieldName = 'BO_NOTA_FISCAL'
      Origin = 'BO_NOTA_FISCAL'
      FixedChar = True
      Size = 1
    end
    object fdqBloqueioPersonalizadoBO_ORDEM_SERVICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Utilizar na Ordem de Servi'#231'o'
      FieldName = 'BO_ORDEM_SERVICO'
      Origin = 'BO_ORDEM_SERVICO'
      FixedChar = True
      Size = 1
    end
    object fdqBloqueioPersonalizadoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspBloqueioPersonalizado: TDataSetProvider
    DataSet = fdqBloqueioPersonalizado
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 192
    Top = 40
  end
  object dsBloqueioPersonalizado: TDataSource
    DataSet = fdqBloqueioPersonalizado
    Left = 288
    Top = 40
  end
  object fdqBloqueioPersonalizadoLib: TgbFDQuery
    MasterSource = dsBloqueioPersonalizado
    MasterFields = 'ID'
    DetailFields = 'ID_BLOQUEIO_PERSONALIZADO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select bloqueio_personalizado_usuario_liberacao.* '
      '      ,pessoa_usuario.nome as JOIN_NOME_PESSOA_USUARIO'
      '  from bloqueio_personalizado_usuario_liberacao '
      'inner join pessoa_usuario '
      
        '   on bloqueio_personalizado_usuario_liberacao.id_pessoa_usuario' +
        ' = pessoa_usuario.id'
      
        ' where bloqueio_personalizado_usuario_liberacao.id_bloqueio_pers' +
        'onalizado = :id')
    Left = 112
    Top = 104
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqBloqueioPersonalizadoLibID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqBloqueioPersonalizadoLibID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqBloqueioPersonalizadoLibID_BLOQUEIO_PERSONALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bloqueio Personalizado'
      FieldName = 'ID_BLOQUEIO_PERSONALIZADO'
      Origin = 'ID_BLOQUEIO_PERSONALIZADO'
    end
    object fdqBloqueioPersonalizadoLibJOIN_NOME_PESSOA_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome do Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
end
