object SMMovimentacaoCheque: TSMMovimentacaoCheque
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqMovimentacaoCheque: TgbFDQuery
    AggregatesActive = True
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT movimentacao_cheque.*'
      
        '      ,(SELECT nome FROM pessoa_usuario WHERE id = movimentacao_' +
        'cheque.id_pessoa_usuario_cadastro) AS JOIN_NOME_USUARIO'
      
        '      ,(SELECT descricao FROM conta_corrente WHERE id = moviment' +
        'acao_cheque.id_conta_corrente_destino) AS JOIN_DESCRICAO_CONTA_C' +
        'ORRENTE_DESTINO'
      '      ,filial.fantasia AS JOIN_FANTASIA_FILIAL'
      '      ,chave_processo.origem AS JOIN_ORIGEM_CHAVE_PROCESSO'
      '      ,conta_corrente.descricao AS JOIN_DESCRICAO_CONTA_CORRENTE'
      'FROM movimentacao_cheque'
      'INNER JOIN filial ON movimentacao_cheque.id_filial = filial.id'
      
        'INNER JOIN chave_processo ON movimentacao_cheque.id_chave_proces' +
        'so = chave_processo.id'
      
        'INNER JOIN conta_corrente ON movimentacao_cheque.id_conta_corren' +
        'te = conta_corrente.id'
      'WHERE movimentacao_cheque.id = :id')
    Left = 48
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqMovimentacaoChequeID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMovimentacaoChequeTIPO_MOVIMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Movimento'
      FieldName = 'TIPO_MOVIMENTO'
      Origin = 'TIPO_MOVIMENTO'
      Size = 35
    end
    object fdqMovimentacaoChequeID_PESSOA_USUARIO_CADASTRO: TIntegerField
      DisplayLabel = 'Pessoa que Realizou o Cadastro'
      FieldName = 'ID_PESSOA_USUARIO_CADASTRO'
      Origin = 'ID_PESSOA_USUARIO_CADASTRO'
      Required = True
    end
    object fdqMovimentacaoChequeID_PESSOA_USUARIO_EFETIVACAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa que Realizou a Efetiva'#231#227'o'
      FieldName = 'ID_PESSOA_USUARIO_EFETIVACAO'
      Origin = 'ID_PESSOA_USUARIO_EFETIVACAO'
    end
    object fdqMovimentacaoChequeID_PESSOA_USUARIO_REABERTURA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa que Realizou a Reabertura'
      FieldName = 'ID_PESSOA_USUARIO_REABERTURA'
      Origin = 'ID_PESSOA_USUARIO_REABERTURA'
    end
    object fdqMovimentacaoChequeID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqMovimentacaoChequeID_CONTA_CORRENTE_DESTINO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente Destino'
      FieldName = 'ID_CONTA_CORRENTE_DESTINO'
      Origin = 'ID_CONTA_CORRENTE_DESTINO'
    end
    object fdqMovimentacaoChequeID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqMovimentacaoChequeID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave do Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqMovimentacaoChequeDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Data de Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqMovimentacaoChequeDH_FECHAMENTO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object fdqMovimentacaoChequeOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqMovimentacaoChequeSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqMovimentacaoChequeVL_TOTAL_CHEQUE: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Total Cheque'
      FieldName = 'VL_TOTAL_CHEQUE'
      Origin = 'VL_TOTAL_CHEQUE'
      Precision = 24
      Size = 9
    end
    object fdqMovimentacaoChequeJOIN_NOME_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_USUARIO'
      Origin = 'JOIN_NOME_USUARIO'
      ProviderFlags = []
      Size = 80
    end
    object fdqMovimentacaoChequeJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqMovimentacaoChequeJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqMovimentacaoChequeJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqMovimentacaoChequeJOIN_DESCRICAO_CONTA_CORRENTE_DESTINO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente Destino'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
      Origin = 'JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspMovimentacaoCheque: TDataSetProvider
    DataSet = fdqMovimentacaoCheque
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object dsMovimentacaoCheque: TDataSource
    DataSet = fdqMovimentacaoCheque
    Left = 110
    Top = 15
  end
  object fdqMovimentacaoChequeCheque: TgbFDQuery
    MasterSource = dsMovimentacaoCheque
    MasterFields = 'ID'
    DetailFields = 'ID_MOVIMENTACAO_CHEQUE'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT movimentacao_cheque_cheque.*'
      'FROM movimentacao_cheque_cheque'
      'WHERE movimentacao_cheque_cheque.id_movimentacao_cheque = :id')
    Left = 48
    Top = 68
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqMovimentacaoChequeChequeID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMovimentacaoChequeChequeID_MOVIMENTACAO_CHEQUE: TIntegerField
      DisplayLabel = 'C'#243'digo da Movimenta'#231#227'o do Cheque'
      FieldName = 'ID_MOVIMENTACAO_CHEQUE'
      Origin = 'ID_MOVIMENTACAO_CHEQUE'
    end
    object fdqMovimentacaoChequeChequeID_CHEQUE: TIntegerField
      DisplayLabel = 'C'#243'digo do Cheque'
      FieldName = 'ID_CHEQUE'
      Origin = 'ID_CHEQUE'
      Required = True
    end
    object fdqMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente Movimento Anterior'
      FieldName = 'ID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR'
      Origin = 'ID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR'
    end
    object fdqMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ATUAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente Movimento Atual'
      FieldName = 'ID_CONTA_CORRENTE_MOVIMENTO_ATUAL'
      Origin = 'ID_CONTA_CORRENTE_MOVIMENTO_ATUAL'
    end
  end
end
