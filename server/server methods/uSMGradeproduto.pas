unit uSMGradeproduto;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uGBFDQuery, DataSnap.DSProviderDataModuleAdapter;

type
  TSMGradeProduto = class(TDSServerModule)
    fdqGradeProduto: TgbFDQuery;
    dspGradeProduto: TDataSetProvider;
    dsGradeProduto: TDataSource;
    fdqGradeProdutoCor: TgbFDQuery;
    fdqGradeProdutoCorTamanho: TgbFDQuery;
    fdqGradeProdutoID: TFDAutoIncField;
    fdqGradeProdutoDESCRICAO: TStringField;
    fdqGradeProdutoOBSERVACAO: TBlobField;
    fdqGradeProdutoBO_ATIVO: TStringField;
    fdqGradeProdutoCorID: TFDAutoIncField;
    fdqGradeProdutoCorID_GRADE_PRODUTO: TIntegerField;
    fdqGradeProdutoCorID_COR: TIntegerField;
    fdqGradeProdutoCorTamanhoID: TFDAutoIncField;
    fdqGradeProdutoCorTamanhoID_GRADE_PRODUTO: TIntegerField;
    fdqGradeProdutoCorTamanhoID_COR: TIntegerField;
    fdqGradeProdutoCorJOIN_DESCRICAO_COR: TStringField;
    fdqGradeProdutoCorTamanhoJOIN_DESCRICAO_COR: TStringField;
    fdqGradeProdutoCorTamanhoID_TAMANHO: TIntegerField;
    fdqGradeProdutoCorTamanhoJOIN_DESCRICAO_TAMANHO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

