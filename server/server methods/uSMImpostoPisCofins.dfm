object SMImpostoPisCofins: TSMImpostoPisCofins
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqImpostoPisCofins: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ii.* '
      
        '       ,(SELECT descricao FROM cst_pis_cofins WHERE id = ii.id_c' +
        'st_pis_cofins) AS JOIN_DESCRICAO_CST_PIS_COFINS'
      
        '       ,(SELECT codigo_cst FROM cst_pis_cofins WHERE id = ii.id_' +
        'cst_pis_cofins) AS JOIN_CODIGO_CST_PIS_COFINS'
      'from IMPOSTO_PIS_COFINS ii'
      'where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqImpostoPisCofinsID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqImpostoPisCofinsPERC_ALIQUOTA_PIS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota PIS'
      FieldName = 'PERC_ALIQUOTA_PIS'
      Origin = 'PERC_ALIQUOTA_PIS'
      Precision = 24
      Size = 9
    end
    object fdqImpostoPisCofinsPERC_ALIQUOTA_COFINS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota Cofins'
      FieldName = 'PERC_ALIQUOTA_COFINS'
      Origin = 'PERC_ALIQUOTA_COFINS'
      Precision = 24
      Size = 9
    end
    object fdqImpostoPisCofinsPERC_ALIQUOTA_PIS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota PIS ST'
      FieldName = 'PERC_ALIQUOTA_PIS_ST'
      Origin = 'PERC_ALIQUOTA_PIS_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoPisCofinsPERC_ALIQUOTA_COFINS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota Cofins ST'
      FieldName = 'PERC_ALIQUOTA_COFINS_ST'
      Origin = 'PERC_ALIQUOTA_COFINS_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoPisCofinsID_CST_PIS_COFINS: TIntegerField
      DisplayLabel = 'C'#243'digo do CST PIS COFINS'
      FieldName = 'ID_CST_PIS_COFINS'
      Origin = 'ID_CST_PIS_COFINS'
      Required = True
    end
    object fdqImpostoPisCofinsJOIN_DESCRICAO_CST_PIS_COFINS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CST PIS COFINS'
      FieldName = 'JOIN_DESCRICAO_CST_PIS_COFINS'
      Origin = 'JOIN_DESCRICAO_CST_PIS_COFINS'
      ProviderFlags = []
      Size = 255
    end
    object fdqImpostoPisCofinsJOIN_CODIGO_CST_PIS_COFINS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo CST PIS Cofins'
      FieldName = 'JOIN_CODIGO_CST_PIS_COFINS'
      Origin = 'JOIN_CODIGO_CST_PIS_COFINS'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
  end
  object dspImpostoPisCofins: TDataSetProvider
    DataSet = fdqImpostoPisCofins
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
