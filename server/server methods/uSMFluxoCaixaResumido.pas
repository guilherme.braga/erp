unit uSMFluxoCaixaResumido;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  Data.FireDACJSONReflect, uFluxoCaixaResumidoProxy;

type
  TSMFluxoCaixaResumido = class(TDSServerModule)
  private
    { Private declarations }
  public
   function GetContasCorrentes: TFDJSONDataSets;
   function GetFluxoCaixaResumido(const AContasCorrentes: String;
      const AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;
   function GetTotalPorContaCorrente(const AContasCorrentes,
      ADtInicial, ADtFinal, ATipoPeriodo: String): TFDJSONDataSets;
   function GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
   function GerarFluxoCaixaResumido(
      const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
   function GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;
   function GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFluxoCaixaResumidoServ;

{$R *.dfm}

{ TSMFluxoCaixaResumido }

function TSMFluxoCaixaResumido.GerarFluxoCaixaResumido(
  const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
begin
  result := TFluxoCaixaResumidoServ.GerarFluxoCaixaResumido(AFluxoCaixaResumido);
end;

function TSMFluxoCaixaResumido.GetContasCorrentes: TFDJSONDataSets;
begin
  result := TFluxoCaixaResumidoServ.GetContasCorrentes;
end;

function TSMFluxoCaixaResumido.GetFluxoCaixaResumido(const AContasCorrentes,
  AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;
begin
  result := TFluxoCaixaResumidoServ.GetFluxoCaixaResumido(
    AContasCorrentes, AListaCamposPeriodo, ATipoPeriodo);
end;

function TSMFluxoCaixaResumido.GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
begin
  result := TFluxoCaixaResumidoServ.GetFluxoCaixaResumidoProxy;
end;

function TSMFluxoCaixaResumido.GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;
begin
  result := TFluxoCaixaResumidoServ.GetTotalContaPagar(ADtInicial, ADtFinal);
end;

function TSMFluxoCaixaResumido.GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;
begin
  result := TFluxoCaixaResumidoServ.GetTotalContaReceber(ADtInicial, ADtFinal);
end;

function TSMFluxoCaixaResumido.GetTotalPorContaCorrente(const AContasCorrentes, ADtInicial,
  ADtFinal, ATipoPeriodo: String): TFDJSONDataSets;
begin
   result := TFluxoCaixaResumidoServ.GetTotalPorContaCorrente(AContasCorrentes, ADtInicial, ADtFinal,
     ATipoPeriodo);
end;

end.

