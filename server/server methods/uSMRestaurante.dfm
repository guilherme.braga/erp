object SMRestaurante: TSMRestaurante
  OldCreateOrder = False
  Height = 385
  Width = 575
  object fdqCaixa: TgbFDQuery
    Connection = Dm.Connection
    SQL.Strings = (
      'select * from caixa where id = :id'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 32
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqCaixaID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCaixaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dspCaixa: TDataSetProvider
    DataSet = fdqCaixa
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 16
  end
  object fdqConsulta: TFDQuery
    Connection = Dm.Connection
    Left = 440
    Top = 16
  end
  object fdqConsultaCaixa: TgbFDQuery
    Connection = Dm.Connection
    SQL.Strings = (
      '       select u.id as id_usuario'
      '          ,c.id as id_caixa'
      '          ,m.id as id_caixa_movimento'
      '          ,u.usuario'
      '          ,m.dt_fechamento'
      '          ,m.dt_abertura'
      '          ,m.hr_fechamento'
      '          ,m.hr_abertura'
      '          ,m.saldo_inicial'
      '          ,c.descricao'
      '            from caixa c'
      '       left join caixa_movimento m'
      '              on c.id = m.id_caixa and m.dt_fechamento is null'
      '       left join pessoa_usuario u'
      '              on u.id = m.id_pessoa_usuario'
      '        order by c.descricao'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 32
    Top = 72
    object fdqConsultaCaixaid_usuario: TFDAutoIncField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
      ReadOnly = True
    end
    object fdqConsultaCaixaid_caixa: TFDAutoIncField
      FieldName = 'id_caixa'
      Origin = 'id_caixa'
      ReadOnly = True
    end
    object fdqConsultaCaixaid_caixa_movimento: TFDAutoIncField
      FieldName = 'id_caixa_movimento'
      Origin = 'id_caixa_movimento'
      ReadOnly = True
    end
    object fdqConsultaCaixausuario: TStringField
      FieldName = 'usuario'
      Origin = 'usuario'
      Size = 50
    end
    object fdqConsultaCaixadt_fechamento: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'dt_fechamento'
      Origin = 'dt_fechamento'
    end
    object fdqConsultaCaixadt_abertura: TDateField
      FieldName = 'dt_abertura'
      Origin = 'dt_abertura'
    end
    object fdqConsultaCaixahr_fechamento: TTimeField
      AutoGenerateValue = arDefault
      FieldName = 'hr_fechamento'
      Origin = 'hr_fechamento'
    end
    object fdqConsultaCaixahr_abertura: TTimeField
      FieldName = 'hr_abertura'
      Origin = 'hr_abertura'
    end
    object fdqConsultaCaixasaldo_inicial: TFMTBCDField
      FieldName = 'saldo_inicial'
      Origin = 'saldo_inicial'
      Precision = 24
      Size = 9
    end
    object fdqConsultaCaixadescricao: TStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Required = True
      Size = 80
    end
  end
  object dspConsultaCaixa: TDataSetProvider
    DataSet = fdqConsultaCaixa
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 61
    Top = 72
  end
end
