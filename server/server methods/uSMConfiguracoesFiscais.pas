unit uSMConfiguracoesFiscais;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMConfiguracoesFiscais = class(TDSServerModule)
    fdqConfiguracoesFiscais: TgbFDQuery;
    dspConfiguracoesFiscais: TDataSetProvider;
    fdqConfiguracoesFiscaisID: TFDAutoIncField;
    fdqConfiguracoesFiscaisBO_ATIVO: TStringField;
    fdqConfiguracoesFiscaisLOGOMARCA: TBlobField;
    fdqConfiguracoesFiscaisCERTIFICADO_CAMINHO: TStringField;
    fdqConfiguracoesFiscaisCERTIFICADO_SENHA: TStringField;
    fdqConfiguracoesFiscaisCERTIFICADO_NUMERO_SERIE: TStringField;
    fdqConfiguracoesFiscaisNFE_MODO_IMPRESSAO: TStringField;
    fdqConfiguracoesFiscaisNFE_FORMA_EMISSAO: TStringField;
    fdqConfiguracoesFiscaisNFE_WEBSERVICE_UF: TStringField;
    fdqConfiguracoesFiscaisNFE_WEBSERVICE_AMBIENTE: TStringField;
    fdqConfiguracoesFiscaisNFE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField;
    fdqConfiguracoesFiscaisNFE_PROXY_HOST: TStringField;
    fdqConfiguracoesFiscaisNFE_PROXY_PORTA: TIntegerField;
    fdqConfiguracoesFiscaisNFE_PROXY_USUARIO: TStringField;
    fdqConfiguracoesFiscaisNFE_PROXY_SENHA: TStringField;
    fdqConfiguracoesFiscaisEMAIL_SMTP: TStringField;
    fdqConfiguracoesFiscaisEMAIL_PORTA: TStringField;
    fdqConfiguracoesFiscaisEMAIL_USUARIO: TStringField;
    fdqConfiguracoesFiscaisEMAIL_SENHA: TStringField;
    fdqConfiguracoesFiscaisEMAIL_ASSUNTO: TStringField;
    fdqConfiguracoesFiscaisEMAIL_MENSAGEM: TBlobField;
    fdqConfiguracoesFiscaisNFCE_MODO_IMPRESSAO: TStringField;
    fdqConfiguracoesFiscaisNFCE_WEBSERVICE_UF: TStringField;
    fdqConfiguracoesFiscaisNFCE_WEBSERVICE_AMBIENTE: TStringField;
    fdqConfiguracoesFiscaisNFCE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField;
    fdqConfiguracoesFiscaisNFCE_PROXY_HOST: TStringField;
    fdqConfiguracoesFiscaisNFCE_PROXY_PORTA: TIntegerField;
    fdqConfiguracoesFiscaisNFCE_PROXY_USUARIO: TStringField;
    fdqConfiguracoesFiscaisNFCE_PROXY_SENHA: TStringField;
    fdqConfiguracoesFiscaisID_FILIAL: TIntegerField;
    fdqConfiguracoesFiscaisJOIN_FANTASIA_FILIAL: TStringField;
    fdqConfiguracoesFiscaisNFE_ID_RELATORIO_FR: TIntegerField;
    fdqConfiguracoesFiscaisEXPANDIR_LOGOMARCAR: TStringField;
    fdqConfiguracoesFiscaisNFCE_ID_CSC: TStringField;
    fdqConfiguracoesFiscaisNFCE_CSC: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_LOGS: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_SCHEMAS: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_NFE: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_CANCELAMENTO: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_INUTILIZACAO: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_DPEC: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_EVENTO: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_RELATORIO_NFCE_FR: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_EVENTO_NFCE_FR: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_RELATORIO_NFE_FR: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_EVENTO_NFE_FR: TStringField;
    fdqConfiguracoesFiscaisIMPRESSORA_NFE: TStringField;
    fdqConfiguracoesFiscaisIMPRESSORA_NFCE: TStringField;
    fdqConfiguracoesFiscaisARQUIVO_PATH_PDF: TStringField;
    fdqConfiguracoesFiscaisNFCE_PREVIEW: TStringField;
    fdqConfiguracoesFiscaisNFE_PREVIEW: TStringField;
    fdqConfiguracoesFiscaisSALVAR_PDF_JUNTO_XML: TStringField;
    fdqConfiguracoesFiscaisEMAIL_ENVIAR_NFE: TStringField;
    fdqConfiguracoesFiscaisEMAIL_ENVIAR_NFCE: TStringField;
    fdqConfiguracoesFiscaisEMAIL_EMAIL_HOST: TStringField;
    fdqConfiguracoesFiscaisEMAIL_EMAIL_DESTINO: TStringField;
    fdqConfiguracoesFiscaisEMAIL_EMAIL_CC: TStringField;
    fdqConfiguracoesFiscaisEMAIL_EMAIL_CCO: TStringField;
    fdqConfiguracoesFiscaisEMAIL_SSL: TStringField;
    fdqConfiguracoesFiscaisEMAIL_SOLICITA_CONFIRMACAO: TStringField;
    fdqConfiguracoesFiscaisEMAIL_USAR_THREAD: TStringField;
    fdqConfiguracoesFiscaisEMAIL_NOME_ORIGEM: TStringField;
  private
    { Private declarations }
  public
    function GetConfiguracaoFiscal(AIdConfiguracaoFiscal: Integer): String;
    function DuplicarConfiguracaoFiscal(const AIdConfiguracaoFiscalOrigem: Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uConfiguracaoFiscalProxy, uConfiguracaoFiscalServ, REST.JSON;

{$R *.dfm}

{ TSMConfiguracaoFiscal }

function TSMConfiguracoesFiscais.DuplicarConfiguracaoFiscal(const AIdConfiguracaoFiscalOrigem: Integer): Integer;
begin
  result := TConfiguracaoFiscalServ.DuplicarConfiguracaoFiscal(AIdConfiguracaoFiscalOrigem);
end;

function TSMConfiguracoesFiscais.GetConfiguracaoFiscal(
  AIdConfiguracaoFiscal: Integer): String;
begin
  result := TJson.ObjectToJsonString(TConfiguracaoFiscalServ.GetConfiguracaoFiscal(AIdConfiguracaoFiscal));
end;

end.

