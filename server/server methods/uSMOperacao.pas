unit uSMOperacao;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery, Datasnap.DSProviderDataModuleAdapter,
  uNaturezaOperacaoProxy;

type
  TSMOperacao = class(TDSServerModule)
    fdqOperacao: TgbFDQuery;
    dspOperacao: TDataSetProvider;
    dsOperacao: TDataSource;
    fdqOperacaoItem: TgbFDQuery;
    fdqAcao: TgbFDQuery;
    dspAcao: TDataSetProvider;
    fdqOperacaoID: TFDAutoIncField;
    fdqOperacaoDESCRICAO: TStringField;
    fdqOperacaoItemID: TFDAutoIncField;
    fdqOperacaoItemID_OPERACAO: TIntegerField;
    fdqOperacaoItemID_ACAO: TIntegerField;
    fdqOperacaoItemJOIN_DESCRICAO_ACAO: TStringField;
    fdqAcaoID: TFDAutoIncField;
    fdqAcaoDESCRICAO: TStringField;
    fdqAcaoACAO: TStringField;
    fdqOperacaoNATUREZA_OPERACAO: TStringField;
    fdqOperacaoFiscal: TgbFDQuery;
    fdqOperacaoFiscalID: TFDAutoIncField;
    fdqOperacaoFiscalDESCRICAO: TStringField;
    fdqOperacaoFiscalID_OPERACAO: TIntegerField;
    fdqOperacaoFiscalCFOP_PRODUTO: TIntegerField;
    fdqOperacaoFiscalCFOP_PRODUTO_ST: TIntegerField;
    fdqOperacaoFiscalCFOP_MERCADORIA: TIntegerField;
    fdqOperacaoFiscalCFOP_MERCADORIA_ST: TIntegerField;
    fdqNaturezaOperacao: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    StringField1: TStringField;
    dspNaturezaOperacao: TDataSetProvider;
    fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO: TStringField;
    fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO_ST: TStringField;
    fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA: TStringField;
    fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA_ST: TStringField;
  private
    { Private declarations }
  public
    function GetNaturezaOperacao(const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
    function GerarNaturezaOperacao(const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
    function ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
    function GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
    function GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
    function ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uNaturezaOperacaoServ, uOperacaoServ;

{$R *.dfm}

function TSMOperacao.ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
begin
  result := TOperacaoServ.ExisteAcao(AAcao, AIdOperacao);
end;

function TSMOperacao.ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
begin
  result := TNaturezaOperacaoServ.ExisteNaturezaOperacao(ADescricaoNaturezaOperacao);
end;

function TSMOperacao.GerarNaturezaOperacao(const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
begin
  result := TNaturezaOperacaoServ.GerarNaturezaOperacao(ANaturezaOperacao);
end;

function TSMOperacao.GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
begin
  result := TNaturezaOperacaoServ.GetIdNaturezaOperacaoPorDescricao(ADescricaoNaturezaOperacao);
end;

function TSMOperacao.GetNaturezaOperacao(const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
begin
  result := TNaturezaOperacaoServ.GetNaturezaOperacao(AIdNaturezaOperacao);
end;

function TSMOperacao.GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
begin
  result := TNaturezaOperacaoServ.GetDescricaoNaturezaOperacao(AIdNaturezaOperacao);
end;


end.

