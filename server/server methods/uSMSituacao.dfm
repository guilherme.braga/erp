object SMSituacao: TSMSituacao
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqSituacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from situacao where id = :id')
    Left = 24
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqSituacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqSituacaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqSituacaoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dspSituacao: TDataSetProvider
    DataSet = fdqSituacao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 8
  end
end
