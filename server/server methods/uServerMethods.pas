unit uServerMethods;

interface

uses System.SysUtils, System.Classes, System.Json,
    Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect, uGBFDQuery;

type
  TServerMethods = class(TDSServerModule)
    fdqPesquisaPadrao: TFDQuery;
    dspPesquisaPadrao: TDataSetProvider;
    fdqPesquisaPadraoID: TFDAutoIncField;
    fdqPesquisaPadraoDESCRICAO: TStringField;
    fdqPesquisaPadraoBO_ATIVO: TStringField;
    fdqPesquisaPadraoID_PESSOA_USUARIO: TIntegerField;
    fdqPesquisaPadraoSQL_PESQUISA: TBlobField;
    fdqPesquisaPadraoBO_CONSULTA_AUTOMATICA: TStringField;
    fdqConsulta: TgbFDQuery;
  private
    procedure PrepararSQL(var ASQL: WideString; AWhereBundle: String; ACamposInjetados: String = '');
  public
    function ReverseString(Value: string): string;
    function GetDados(AIdentifier, AWhereBundle: String; const AIdUsuario: Integer = 0): TFDJSONDataSets;
    function GetDadosComInjecaoCampos(AIdentifier, AWhereBundle: String; const AIdUsuario: Integer = 0;
      const ACamposInjetados: String = ''): TFDJSONDataSets;
    function GetDadosFromSQL(const ASQL: String): TFDJSONDataSets;
    procedure Commit;
    procedure RollBack;
    procedure StartTransaction;
    function GetLastedAutoIncrementValue: Integer;
    function GetSQLPesquisaPadrao(const AIdentifier: String; const AIdUsuario: Integer): String;
    procedure SetSQLPesquisaPadrao(const AIdentifier: String; const AIdUsuario: Integer; ASQL: String);
    function GetUltimaChaveDaTabela(const ATabela: String; const ACampoRetorno: String): String;

    procedure HabilitarConsultaAutomatica(const ACodigoUsuario: Integer; const AFormName: String; const AHabilitar: Boolean);
    function EstaConsultandoAutomatico(const ACodigoUsuario: Integer; const AFormName: String): Boolean;
  end;

implementation


{$R *.dfm}


uses System.StrUtils, uDmConnection, uUsuarioServer;

function TServerMethods.EstaConsultandoAutomatico(const ACodigoUsuario: Integer;
  const AFormName: String): Boolean;
begin
  result := TUsuarioPesquisaPadrao.EstaConsultandoAutomatico(fdqConsulta, ACodigoUsuario, AFormName);
end;

function TServerMethods.GetDadosComInjecaoCampos(AIdentifier, AWhereBundle: String;
  const AIdUsuario: Integer = 0; const ACamposInjetados: String = ''): TFDJSONDataSets;
var SQL: WideString;
begin
  result := TFDJSONDataSets.Create;
  try
    SQL := GetSQLPesquisaPadrao(AIdentifier, AIdUsuario);

    if SQL <> '' then
    begin
      PrepararSQL(SQL, AWhereBundle, ACamposInjetados);
      fdqConsulta.Close;
      fdqConsulta.SQL.Clear;
      fdqConsulta.SQL.Add(SQL);
      fdqConsulta.Open;

      TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Consulta Incorreta:'+e.message);
    end;
  end;
end;

function TServerMethods.GetDados(AIdentifier, AWhereBundle: String;
  const AIdUsuario: Integer = 0): TFDJSONDataSets;
var SQL: WideString;
begin
  result := TFDJSONDataSets.Create;
  try
    SQL := GetSQLPesquisaPadrao(AIdentifier, AIdUsuario);

    if SQL <> '' then
    begin
      PrepararSQL(SQL, AWhereBundle);
      fdqConsulta.Close;
      fdqConsulta.SQL.Clear;
      fdqConsulta.SQL.Add(SQL);
      fdqConsulta.Open;

      TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
    end;
  except
    on e : Exception do
    begin
      raise Exception.Create('Consulta Incorreta:'+e.message);
    end;
  end;
end;

function TServerMethods.GetSQLPesquisaPadrao(const AIdentifier: String; const AIdUsuario: Integer): String;
begin
  result := TUsuarioPesquisaPadrao.GetSQLPesquisaPadrao(fdqConsulta, AIdentifier, AIdUsuario);
end;

function TServerMethods.GetUltimaChaveDaTabela(const ATabela: String; const ACampoRetorno: String): String;
var
  registro: TFDJSONDataSets;
  queryConsulta: TFDMemTable;
begin
  registro := GetDadosFromSQL('SELECT '+ACampoRetorno+' FROM '+ATabela+' WHERE id = '+
    '(select max(id) from '+ATabela+')');
  try
    queryConsulta := TFDMemTable.Create(nil);
    try
      queryConsulta.AppendData(TFDJSONDataSetsReader.GetListValue(registro,0));
      result := queryConsulta.Fields[0].AsString;
    finally
      FreeAndNil(queryConsulta);
    end;
  finally
    FreeAndNil(registro);
  end;
end;

procedure TServerMethods.SetSQLPesquisaPadrao(const AIdentifier: String;
  const AIdUsuario: Integer; ASQL: String);
begin
  TUsuarioPesquisaPadrao.SetSQLPesquisaPadrao(fdqConsulta, AIdentifier, AIdUsuario, ASQL);
end;

function TServerMethods.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;

procedure TServerMethods.Commit;
begin
  Dm.Connection.Commit();
end;

procedure TServerMethods.RollBack;
begin
  Dm.Connection.Rollback();
end;

procedure TServerMethods.HabilitarConsultaAutomatica(const ACodigoUsuario: Integer;
  const AFormName: String; const AHabilitar: Boolean);
begin
  TUsuarioPesquisaPadrao.HabilitarConsultaAutomatica(fdqConsulta, ACodigoUsuario, AFormName, AHabilitar);
end;

procedure TServerMethods.PrepararSQL(var ASQL: WideString; AWhereBundle: String; ACamposInjetados: String = '');
var
  posicaoGroupBy, PosicaoOrderBy, posicaoWhere, PosicaoLimit, PosicaoFrom: Integer;
  existeJoinAposUltimoWhere: Boolean;
  posicaoAnterior: Integer;
begin
  if not ACamposInjetados.IsEmpty then
  begin
    PosicaoFrom := POS('FROM', UpperCase(ASQL), posicaoWhere+5);
    Insert(' ,'+ACamposInjetados+' ', ASQL,PosicaoFrom);
  end;

  posicaoGroupBy := 0;
  PosicaoOrderBy := 0;
  posicaoWhere := 0;
  PosicaoLimit := 0;

  fdqConsulta.Close;
  fdqConsulta.SQL.Clear;

  repeat
    posicaoAnterior := posicaoWhere;
    posicaoWhere := POSEx('WHERE', UpperCase(ASQL), posicaoWhere+5);

    if posicaoWhere = 0 then
    begin
      posicaoWhere := posicaoAnterior;
    end;
  until (posicaoAnterior = posicaoWhere);

  existeJoinAposUltimoWhere := POSEx('JOIN', UpperCase(ASQL), posicaoWhere+5) > 0;
  if existeJoinAposUltimoWhere then
  begin
    posicaoWhere := 0;
  end;

  repeat
    posicaoAnterior := posicaoGroupBy;
    posicaoGroupBy := POSEx('GROUP BY', UpperCase(ASQL), posicaoGroupBy+8);

    if posicaoGroupBy = 0 then
    begin
      posicaoGroupBy := posicaoAnterior;
    end;
  until (posicaoAnterior = posicaoGroupBy);

  repeat
    posicaoAnterior := PosicaoOrderBy;
    PosicaoOrderBy := POSEx('ORDER BY', UpperCase(ASQL), PosicaoOrderBy+8);

    if PosicaoOrderBy = 0 then
    begin
      PosicaoOrderBy := posicaoAnterior;
    end;
  until (posicaoAnterior = PosicaoOrderBy);

  repeat
    posicaoAnterior := PosicaoLimit;
    PosicaoLimit := POSEx('LIMIT', UpperCase(ASQL), PosicaoLimit+5);

    if PosicaoLimit = 0 then
    begin
      PosicaoLimit := posicaoAnterior;
    end;
  until (posicaoAnterior = PosicaoLimit);

  if (posicaoWhere > 0) and not(AWhereBundle.IsEmpty) then
  begin
    delete(ASQL, posicaoWhere, 5);
    Insert('  AND  ', ASQL, posicaoWhere);
    //SQL := StringReplace(UpperCase(SQL), 'WHERE', '  AND  ', [rfReplaceAll]);
    Insert(' '+AWhereBundle+' ', ASQL, posicaoWhere);
  end
  else
  if posicaoGroupBy > 0 then
    Insert(' '+AWhereBundle+' ', ASQL,posicaoGroupBy)
  else if PosicaoOrderBy > 0 then
    Insert(' '+AWhereBundle+' ', ASQL,PosicaoOrderBy)
  else if PosicaoLimit > 0 then
    Insert(' '+AWhereBundle+' ', ASQL,PosicaoLimit)
  else
    ASQL := ASQL+' '+AWhereBundle;
end;

procedure TServerMethods.StartTransaction;
begin
  Dm.Connection.StartTransaction();
end;

function TServerMethods.GetDadosFromSQL(const ASQL: String): TFDJSONDataSets;
begin
  result := TFDJSONDataSets.Create;

  if not ASQL.IsEmpty then
  begin
    fdqConsulta.Close;
    fdqConsulta.SQL.Clear;
    fdqConsulta.SQL.Add(ASQL);
    fdqConsulta.Open;

    TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
  end;
end;

function TServerMethods.GetLastedAutoIncrementValue: Integer;
begin
  result := Dm.Connection.GetLastAutoGenValue('');
end;

end.

