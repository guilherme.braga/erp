object SMVeiculo: TSMVeiculo
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqVeiculo: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT v.*'
      '      ,ma.DESCRICAO AS "JOIN_DESCRICAO_MARCA"'
      '      ,mo.DESCRICAO AS "JOIN_DESCRICAO_MODELO"'
      '      ,c.DESCRICAO AS "JOIN_DESCRICAO_COR"'
      '      ,p.NOME AS "JOIN_NOME_PESSOA"'
      '      ,f.FANTASIA AS "JOIN_FANTASIA_FILIAL"'
      'FROM VEICULO v'
      'LEFT JOIN MARCA ma ON v.id_marca = ma.id'
      'LEFT JOIN MODELO mo ON v.id_modelo = mo.id'
      'LEFT JOIN COR c ON v.id_cor = c.id'
      'LEFT JOIN PESSOA p ON v.id_pessoa = p.id'
      'LEFT JOIN FILIAL f ON v.id_filial = f.id'
      'WHERE'
      '  v.id = :id')
    Left = 24
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqVeiculoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqVeiculoPLACA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Placa'
      FieldName = 'PLACA'
      Origin = 'PLACA'
      Size = 7
    end
    object fdqVeiculoANO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ano'
      FieldName = 'ANO'
      Origin = 'ANO'
    end
    object fdqVeiculoCOMBUSTIVEL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Combust'#237'vel'
      FieldName = 'COMBUSTIVEL'
      Origin = 'COMBUSTIVEL'
      Size = 30
    end
    object fdqVeiculoCHASSI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chassi'
      FieldName = 'CHASSI'
      Origin = 'CHASSI'
      Size = 30
    end
    object fdqVeiculoRENAVAM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Renavam'
      FieldName = 'RENAVAM'
      Origin = 'RENAVAM'
      Size = 30
    end
    object fdqVeiculoID_MODELO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object fdqVeiculoID_MARCA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
    end
    object fdqVeiculoID_COR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
    end
    object fdqVeiculoID_PESSOA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqVeiculoID_FILIAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
    end
    object fdqVeiculoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqVeiculoJOIN_DESCRICAO_MARCA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVeiculoJOIN_DESCRICAO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqVeiculoJOIN_DESCRICAO_COR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object fdqVeiculoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqVeiculoJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspVeiculo: TDataSetProvider
    DataSet = fdqVeiculo
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 88
    Top = 16
  end
end
