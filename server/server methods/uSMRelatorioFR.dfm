object SMRelatorioFR: TSMRelatorioFR
  OldCreateOrder = False
  Height = 232
  Width = 446
  object fdqRelatorioFR: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM RELATORIO_FR WHERE ID = :ID')
    Left = 96
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqRelatorioFRID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqRelatorioFRNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object fdqRelatorioFRDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqRelatorioFROBSERVACOES: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#245'es'
      FieldName = 'OBSERVACOES'
      Origin = 'OBSERVACOES'
    end
    object fdqRelatorioFRARQUIVO: TBlobField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
      Required = True
    end
    object fdqRelatorioFRTIPO_ARQUIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Arquivo'
      FieldName = 'TIPO_ARQUIVO'
      Origin = 'TIPO_ARQUIVO'
      Size = 25
    end
  end
  object dspRelatorioFR: TDataSetProvider
    DataSet = fdqRelatorioFR
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 126
    Top = 24
  end
  object fdqRelatorioFRFormulario: TgbFDQuery
    MasterSource = dsRelatorioFR
    MasterFields = 'ID'
    DetailFields = 'ID_RELATORIO_FR'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT RELATORIO_FR_FORMULARIO.*'
      '      ,PESSOA_USUARIO.USUARIO AS JOIN_USUARIO_PESSOA_USUARIO '
      'FROM RELATORIO_FR_FORMULARIO '
      'INNER JOIN PESSOA_USUARIO ON '
      '  RELATORIO_FR_FORMULARIO.ID_PESSOA_USUARIO = PESSOA_USUARIO.ID'
      'WHERE ID_RELATORIO_FR = :ID')
    Left = 96
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqRelatorioFRFormularioID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqRelatorioFRFormularioNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 255
    end
    object fdqRelatorioFRFormularioDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqRelatorioFRFormularioACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ACAO'
      Origin = 'ACAO'
      Size = 30
    end
    object fdqRelatorioFRFormularioID_RELATORIO_FR: TIntegerField
      DisplayLabel = 'C'#243'digo do Relat'#243'rio'
      FieldName = 'ID_RELATORIO_FR'
      Origin = 'ID_RELATORIO_FR'
    end
    object fdqRelatorioFRFormularioID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqRelatorioFRFormularioJOIN_USUARIO_PESSOA_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO_PESSOA_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object fdqRelatorioFRFormularioIMPRESSORA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Impressora'
      FieldName = 'IMPRESSORA'
      Origin = 'IMPRESSORA'
      Size = 255
    end
    object fdqRelatorioFRFormularioNUMERO_COPIAS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'pias'
      FieldName = 'NUMERO_COPIAS'
      Origin = 'NUMERO_COPIAS'
    end
    object fdqRelatorioFRFormularioFILTROS_PERSONALIZADOS: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filtros Personalizados'
      FieldName = 'FILTROS_PERSONALIZADOS'
      Origin = 'FILTROS_PERSONALIZADOS'
    end
  end
  object dsRelatorioFR: TDataSource
    DataSet = fdqRelatorioFR
    Left = 154
    Top = 24
  end
  object dspRelatorioFRFormulario: TDataSetProvider
    DataSet = fdqRelatorioFRFormulario
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 124
    Top = 80
  end
  object fdqConsultaRelatorioFR: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT relatorio.ID,'
      '       relatorio.DESCRICAO,'
      '       relatorio.NOME,'
      '       relatorio.TIPO_ARQUIVO,'
      '       relatorio.ARQUIVO,'
      '       formulario.IMPRESSORA,'
      '       formulario.ACAO,      '
      '       formulario.NUMERO_COPIAS,'
      '       formulario.FILTROS_PERSONALIZADOS'
      'FROM RELATORIO_FR_FORMULARIO formulario'
      
        'INNER JOIN RELATORIO_FR relatorio ON formulario.id_relatorio_fr ' +
        '= relatorio.id'
      'WHERE formulario.NOME = :NOME'
      '  AND ID_PESSOA_USUARIO = :ID_PESSOA_USUARIO')
    Left = 136
    Top = 168
    ParamData = <
      item
        Name = 'NOME'
        DataType = ftString
        ParamType = ptInput
        Value = #39'TESTE'#39
      end
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqConsultaRelatorioFRID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqConsultaRelatorioFRDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqConsultaRelatorioFRNOME: TStringField
      DisplayLabel = 'Relat'#243'rio'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object fdqConsultaRelatorioFRIMPRESSORA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Impressora'
      FieldName = 'IMPRESSORA'
      Origin = 'IMPRESSORA'
      Size = 255
    end
    object fdqConsultaRelatorioFRACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ACAO'
      Origin = 'ACAO'
      Size = 30
    end
    object fdqConsultaRelatorioFRNUMERO_COPIAS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'pias'
      FieldName = 'NUMERO_COPIAS'
      Origin = 'NUMERO_COPIAS'
    end
    object fdqConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filtros Personalizados'
      FieldName = 'FILTROS_PERSONALIZADOS'
      Origin = 'FILTROS_PERSONALIZADOS'
    end
    object fdqConsultaRelatorioFRTIPO_ARQUIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Arquivo'
      FieldName = 'TIPO_ARQUIVO'
      Origin = 'TIPO_ARQUIVO'
      Size = 25
    end
    object fdqConsultaRelatorioFRARQUIVO: TBlobField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
      Required = True
    end
  end
  object dspConsultaRelatorioFR: TDataSetProvider
    DataSet = fdqConsultaRelatorioFR
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 164
    Top = 168
  end
  object frxReport: TfrxReport
    Version = '4.15.10'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42102.561736064820000000
    ReportOptions.LastChange = 42102.561736064820000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 208
    Top = 104
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxDotMatrixExport: TfrxDotMatrixExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    EscModel = 1
    GraphicFrames = False
    SaveToFile = False
    UseIniSettings = True
    Left = 288
    Top = 104
  end
end
