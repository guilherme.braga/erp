object ServerMethods: TServerMethods
  OldCreateOrder = False
  Height = 311
  Width = 528
  object fdqPesquisaPadrao: TFDQuery
    Connection = Dm.Connection
    SQL.Strings = (
      'select p.*'
      'from pesquisa_padrao p'
      'where p.id = :id')
    Left = 120
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqPesquisaPadraoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqPesquisaPadraoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqPesquisaPadraoSQL_PESQUISA: TBlobField
      DisplayLabel = 'SQL'
      FieldName = 'SQL_PESQUISA'
      Origin = 'SQL_PESQUISA'
      Required = True
    end
    object fdqPesquisaPadraoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqPesquisaPadraoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object fdqPesquisaPadraoBO_CONSULTA_AUTOMATICA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BO_CONSULTA_AUTOMATICA'
      Origin = 'BO_CONSULTA_AUTOMATICA'
      FixedChar = True
      Size = 1
    end
  end
  object dspPesquisaPadrao: TDataSetProvider
    DataSet = fdqPesquisaPadrao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 148
    Top = 8
  end
  object fdqConsulta: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.RowsetSize = -1
    FetchOptions.DetailOptimize = False
    Left = 32
    Top = 8
  end
end
