object SMNegociacaoContaReceber: TSMNegociacaoContaReceber
  OldCreateOrder = False
  Height = 289
  Width = 474
  object fdqNegociacaoContaReceber: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ncr.*'
      '      ,p.nome AS JOIN_DESCRICAO_NOME_PESSOA'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,c.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,cp.origem AS JOIN_ORIGEM_CHAVE_PROCESSO'
      '      ,cp.id_origem AS JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      '      ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,car.descricao as JOIN_DESCRICAO_CARTEIRA'
      '      ,pu.NOME AS JOIN_NOME_USUARIO'
      'FROM negociacao_conta_receber ncr'
      'INNER JOIN pessoa p ON ncr.id_pessoa = p.id'
      'INNER JOIN conta_analise ca ON ncr.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado c ON ncr.id_centro_resultado = c.id'
      'INNER JOIN forma_pagamento fp ON ncr.id_forma_pagamento = fp.id'
      'INNER JOIN chave_processo cp ON ncr.id_chave_processo = cp.id'
      'INNER JOIN carteira car ON ncr.id_carteira = car.id'
      'INNER JOIN pessoa_usuario pu ON ncr.id_pessoa_usuario = pu.id'
      'LEFT JOIN conta_corrente cc ON ncr.id_conta_corrente = cc.id'
      'WHERE ncr.id = :id')
    Left = 72
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqNegociacaoContaReceberID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNegociacaoContaReceberDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqNegociacaoContaReceberDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqNegociacaoContaReceberDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdqNegociacaoContaReceberVL_TOTAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberVL_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberVL_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberVL_LIQUIDO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberVL_ENTRADA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Entrada'
      FieldName = 'VL_ENTRADA'
      Origin = 'VL_ENTRADA'
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberDT_PARCELA_INICIAL: TDateField
      DisplayLabel = 'Dt. Parcela Inicial'
      FieldName = 'DT_PARCELA_INICIAL'
      Origin = 'DT_PARCELA_INICIAL'
      Required = True
    end
    object fdqNegociacaoContaReceberDT_COMPETENCIA_INICIAL: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia Inicial'
      FieldName = 'DT_COMPETENCIA_INICIAL'
      Origin = 'DT_COMPETENCIA_INICIAL'
      Required = True
    end
    object fdqNegociacaoContaReceberSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Size = 15
    end
    object fdqNegociacaoContaReceberID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqNegociacaoContaReceberID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqNegociacaoContaReceberID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqNegociacaoContaReceberID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object fdqNegociacaoContaReceberID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      Required = True
    end
    object fdqNegociacaoContaReceberID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqNegociacaoContaReceberID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqNegociacaoContaReceberID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqNegociacaoContaReceberID_CONTA_CORRENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqNegociacaoContaReceberOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqNegociacaoContaReceberID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNegociacaoContaReceberJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem da Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqNegociacaoContaReceberJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Origem da Chave de Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNegociacaoContaReceberJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqNegociacaoContaReceberJOIN_NOME_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspNegociacaoContaReceber: TDataSetProvider
    DataSet = fdqNegociacaoContaReceber
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 101
    Top = 16
  end
  object dsNegociacaoContaReceber: TDataSource
    DataSet = fdqNegociacaoContaReceber
    Left = 131
    Top = 15
  end
  object fdqNegociacaoContaReceberParcela: TgbFDQuery
    MasterSource = dsNegociacaoContaReceber
    MasterFields = 'ID'
    DetailFields = 'ID_NEGOCIACAO_CONTA_RECEBER'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ncrp.*'
      'from negociacao_conta_receber_parcela ncrp'
      'where ncrp.id_negociacao_conta_receber = :id')
    Left = 96
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqNegociacaoContaReceberParcelaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNegociacaoContaReceberParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqNegociacaoContaReceberParcelaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqNegociacaoContaReceberParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqNegociacaoContaReceberParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqNegociacaoContaReceberParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqNegociacaoContaReceberParcelaSEQUENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Size = 10
    end
    object fdqNegociacaoContaReceberParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqNegociacaoContaReceberParcelaDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      Required = True
    end
    object fdqNegociacaoContaReceberParcelaDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object fdqNegociacaoContaReceberParcelaID_NEGOCIACAO_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Recener'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
  end
  object fdqNegociacaoContaReceberGerado: TgbFDQuery
    MasterSource = dsNegociacaoContaReceber
    MasterFields = 'ID'
    DetailFields = 'ID_NEGOCIACAO_CONTA_RECEBER'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ncrtg.*'
      'from negociacao_conta_receber_titulo_gerado ncrtg'
      'where ncrtg.id_negociacao_conta_receber = :id')
    Left = 96
    Top = 115
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqNegociacaoContaReceberGeradoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNegociacaoContaReceberGeradoID_NEGOCIACAO_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Receber'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
    object fdqNegociacaoContaReceberGeradoID_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo do Conta a Receber'
      FieldName = 'ID_CONTA_RECEBER'
      Origin = 'ID_CONTA_RECEBER'
      Required = True
    end
  end
  object fdqNegociacaoContaReceberAnteriro: TgbFDQuery
    MasterSource = dsNegociacaoContaReceber
    MasterFields = 'ID'
    DetailFields = 'ID_NEGOCIACAO_CONTA_RECEBER'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ncrtg.*'
      'from negociacao_conta_receber_titulo_gerado ncrtg'
      'where ncrtg.id_negociacao_conta_receber = :id')
    Left = 96
    Top = 171
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Receber'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'C'#243'digo do Conta a Receber'
      FieldName = 'ID_CONTA_RECEBER'
      Origin = 'ID_CONTA_RECEBER'
      Required = True
    end
  end
end
