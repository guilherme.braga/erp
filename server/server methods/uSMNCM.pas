unit uSMNCM;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMNCM = class(TDSServerModule)
    fdqNCM: TgbFDQuery;
    dspNCM: TDataSetProvider;
    dsNCM: TDataSource;
    fdqNCMEspecializado: TgbFDQuery;
    fdqNCMEspecializadoID: TFDAutoIncField;
    fdqNCMEspecializadoDESCRICAO: TStringField;
    fdqNCMEspecializadoID_NCM: TIntegerField;
    fdqNCMEspecializadoJOIN_CODIGO_NCM: TIntegerField;
    fdqNCMEspecializadoJOIN_DESCRICAO_NCM: TStringField;
    fdqNCMID: TFDAutoIncField;
    fdqNCMCODIGO: TIntegerField;
    fdqNCMDESCRICAO: TStringField;
    fdqNCMUN: TStringField;
  private
    { Private declarations }
  public
    function GetNCM(AIdNCM: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uNCMProxy, uNCMServ, REST.JSON;

{$R *.dfm}

{ TSMNCM }

function TSMNCM.GetNCM(
  AIdNCM: Integer): String;
begin
  result := TJson.ObjectToJsonString(TNCMServ.GetNCM(AIdNCM));
end;

end.

