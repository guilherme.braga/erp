object SMCSTPISCofins: TSMCSTPISCofins
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqCSTPISCofins: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from CST_PIS_COFINS where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCSTPISCofinsID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCSTPISCofinsCODIGO_CST: TStringField
      DisplayLabel = 'C'#243'digo CST'
      FieldName = 'CODIGO_CST'
      Origin = 'CODIGO_CST'
      Required = True
      FixedChar = True
      Size = 2
    end
    object fdqCSTPISCofinsDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
  end
  object dspCSTPISCofins: TDataSetProvider
    DataSet = fdqCSTPISCofins
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
