object SMLoteRecebimento: TSMLoteRecebimento
  OldCreateOrder = False
  Height = 248
  Width = 340
  object fdqLoteRecebimento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select lr.*'
      '      ,f.fantasia as JOIN_FANTASIA_FILIAL'
      '      ,pu.usuario as JOIN_USUARIO'
      '      ,cp.origem as JOIN_CHAVE_PROCESSO'
      'from lote_Recebimento lr'
      'inner join filial f on lr.id_filial = f.id'
      'inner join pessoa_usuario pu on lr.id_pessoa_usuario = pu.id'
      'inner join chave_processo cp on lr.id_chave_processo = cp.id'
      'where lr.id = :id')
    Left = 56
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqLoteRecebimentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLoteRecebimentoDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object fdqLoteRecebimentoSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqLoteRecebimentoID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'd. Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqLoteRecebimentoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqLoteRecebimentoID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqLoteRecebimentoDH_EFETIVACAO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Efetiva'#231#227'o'
      FieldName = 'DH_EFETIVACAO'
      Origin = 'DH_EFETIVACAO'
    end
    object fdqLoteRecebimentoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqLoteRecebimentoVL_QUITADO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoVL_QUITADO_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado L'#237'quido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoVL_ABERTO: TFMTBCDField
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Fantasia Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqLoteRecebimentoJOIN_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLoteRecebimentoJOIN_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
  end
  object fdqLoteRecebimentoTitulo: TgbFDQuery
    MasterSource = dsLoteRecebimento
    MasterFields = 'ID'
    DetailFields = 'ID_LOTE_Recebimento'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT '
      '  lrt.*  '
      '  ,cr.DH_CADASTRO'
      '  ,cr.DOCUMENTO                 '
      '  ,cr.DESCRICAO                 '
      '  ,cr.VL_TITULO                 '
      '  ,cr.VL_QUITADO                '
      '  ,cr.VL_ABERTO                 '
      '  ,cr.QT_PARCELA                '
      '  ,cr.NR_PARCELA                '
      '  ,cr.DT_VENCIMENTO             '
      '  ,cr.SEQUENCIA                 '
      '  ,cr.BO_VENCIDO                '
      '  ,cr.OBSERVACAO                '
      '  ,cr.ID_PESSOA                 '
      '  ,cr.ID_CONTA_ANALISE          '
      '  ,cr.ID_CENTRO_RESULTADO       '
      '  ,cr.STATUS                    '
      '  ,cr.ID_CHAVE_PROCESSO         '
      '  ,cr.ID_FORMA_PAGAMENTO        '
      '  ,cr.VL_QUITADO_LIQUIDO        '
      '  ,cr.DT_COMPETENCIA            '
      '  ,cr.ID_DOCUMENTO_ORIGEM    '
      '  ,cr.TP_DOCUMENTO_ORIGEM'
      '  ,cr.DT_DOCUMENTO'
      '  ,cr.ID_CONTA_CORRENTE'
      '  ,cr.ID_FILIAL       '
      '  ,cr.BO_NEGOCIADO'
      '  ,cr.ID_CARTEIRA'
      '  ,p.nome AS JOIN_DESCRICAO_NOME_PESSOA_CR'
      '  ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE_CR'
      '  ,c.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO_CR'
      '  ,cp.origem AS JOIN_ORIGEM_CHAVE_PROCESSO_CR'
      '  ,cp.id_origem AS JOIN_ID_ORIGEM_CHAVE_PROCESSO_CR'
      '  ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO_CR'
      '  ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE_CR'
      '  ,car.descricao as JOIN_DESCRICAO_CARTEIRA_CR'
      
        '  ,(select nome from pessoa where id = cr.id_pessoa) as JOIN_NOM' +
        'E_PESSOA '
      'FROM lote_recebimento_titulo lrt'
      'INNER JOIN conta_receber cr ON lrt.id_conta_receber = cr.id'
      'INNER JOIN pessoa p ON cr.id_pessoa = p.id'
      'INNER JOIN conta_analise ca ON cr.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado c ON cr.id_centro_resultado = c.id'
      'INNER JOIN forma_pagamento fp ON cr.id_forma_pagamento = fp.id'
      'INNER JOIN chave_processo cp ON cr.id_chave_processo = cp.id'
      'INNER JOIN carteira car ON cr.id_carteira = car.id'
      'LEFT JOIN conta_corrente cc ON cr.id_conta_corrente = cc.id'
      'WHERE lrt.id_lote_Recebimento = :id')
    Left = 56
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqLoteRecebimentoTituloID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLoteRecebimentoTituloVL_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloVL_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloID_LOTE_Recebimento: TIntegerField
      DisplayLabel = 'C'#243'd. Lote Recebimento'
      FieldName = 'ID_LOTE_Recebimento'
      Origin = 'ID_LOTE_Recebimento'
    end
    object fdqLoteRecebimentoTituloID_CONTA_Receber: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Receber'
      FieldName = 'ID_CONTA_Receber'
      Origin = 'ID_CONTA_Receber'
      Required = True
    end
    object fdqLoteRecebimentoTituloVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLoteRecebimentoTituloDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoTituloVL_TITULO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloVL_QUITADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloVL_ABERTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloQT_PARCELA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloNR_PARCELA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloDT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloSEQUENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
      Size = 10
    end
    object fdqLoteRecebimentoTituloBO_VENCIDO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vencido?'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object fdqLoteRecebimentoTituloOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_PESSOA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_CONTA_ANALISE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_CENTRO_RESULTADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Quitado T'#237'tulo'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoTituloDT_COMPETENCIA: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      ProviderFlags = []
      Size = 50
    end
    object fdqLoteRecebimentoTituloID_FORMA_PAGAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloDT_DOCUMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_CONTA_CORRENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_FILIAL: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloID_CARTEIRA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloBO_NEGOCIADO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BO_NEGOCIADO'
      Origin = 'BO_NEGOCIADO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_NOME_PESSOA_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA_CR'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_ANALISE_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE_CR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_CENTRO_RESULTADO_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO_CR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoTituloJOIN_ORIGEM_CHAVE_PROCESSO_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO_CR'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqLoteRecebimentoTituloJOIN_ID_ORIGEM_CHAVE_PROCESSO_CR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem Chave Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO_CR'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_FORMA_PAGAMENTO_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_CR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_CORRENTE_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE_CR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoTituloJOIN_DESCRICAO_CARTEIRA_CR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA_CR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoTituloJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'JOIN_NOME_PESSOA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dsLoteRecebimento: TDataSource
    DataSet = fdqLoteRecebimento
    Left = 152
    Top = 24
  end
  object dspLoteRecebimento: TDataSetProvider
    DataSet = fdqLoteRecebimento
    Options = [poFetchBlobsOnDemand, poIncFieldProps, poUseQuoteChar]
    Left = 248
    Top = 24
  end
  object fdqLoteRecebimentoQuitacao: TgbFDQuery
    MasterSource = dsLoteRecebimento
    MasterFields = 'ID'
    DetailFields = 'ID_LOTE_Recebimento'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT lrq.* '
      '      ,tq.descricao JOIN_DESCRICAO_TIPO_QUITACAO '
      '      ,tq.tipo AS JOIN_TIPO_TIPO_QUITACAO'
      '      ,cc.descricao JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,ca.descricao JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,oc.descricao AS JOIN_DESCRICAO_OPERADORA_CARTAO'
      
        '      ,(select nome from pessoa_usuario where id = lrq.id_usuari' +
        'o_baixa) AS JOIN_NOME_USUARIO_BAIXA'
      '  FROM lote_recebimento_quitacao lrq'
      ' INNER JOIN tipo_quitacao  tq ON lrq.id_tipo_quitacao   = tq.id'
      ' INNER JOIN conta_corrente cc ON lrq.id_conta_corrente = cc.id'
      ' INNER JOIN conta_analise  ca ON lrq.id_conta_analise   = ca.id'
      
        ' INNER JOIN centro_resultado cr ON lrq.id_centro_resultado = cr.' +
        'id'
      
        ' LEFT JOIN operadora_cartao oc ON lrq.id_operadora_cartao = oc.i' +
        'd'
      ' WHERE lrq.id_lote_recebimento = :id')
    Left = 56
    Top = 136
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqLoteRecebimentoQuitacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLoteRecebimentoQuitacaoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqLoteRecebimentoQuitacaoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Nr. Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoPERC_ACRESCIMO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Precision = 7
    end
    object fdqLoteRecebimentoQuitacaoPERC_DESCONTO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Precision = 7
    end
    object fdqLoteRecebimentoQuitacaoVL_TOTAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoID_LOTE_Recebimento: TIntegerField
      DisplayLabel = 'C'#243'digo do Lote Recebimento'
      FieldName = 'ID_LOTE_Recebimento'
      Origin = 'ID_LOTE_Recebimento'
    end
    object fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object fdqLoteRecebimentoQuitacaoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLoteRecebimentoQuitacaoVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLoteRecebimentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqLoteRecebimentoQuitacaoID_USUARIO_BAIXA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object fdqLoteRecebimentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
  end
end
