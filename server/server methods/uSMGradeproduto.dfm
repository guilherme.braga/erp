object SMGradeProduto: TSMGradeProduto
  OldCreateOrder = False
  Height = 275
  Width = 358
  object fdqGradeProduto: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT grade_produto.*'
      'FROM grade_produto'
      'WHERE grade_produto.id = :ID')
    Left = 80
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqGradeProdutoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGradeProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqGradeProdutoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqGradeProdutoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspGradeProduto: TDataSetProvider
    DataSet = fdqGradeProduto
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 112
    Top = 8
  end
  object dsGradeProduto: TDataSource
    DataSet = fdqGradeProduto
    Left = 144
    Top = 8
  end
  object fdqGradeProdutoCor: TgbFDQuery
    MasterSource = dsGradeProduto
    MasterFields = 'ID'
    DetailFields = 'ID_GRADE_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache, evDetailOptimize]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT grade_produto_cor.*'
      '      ,cor.descricao AS JOIN_DESCRICAO_COR'
      'FROM grade_produto_cor'
      'INNER JOIN cor ON grade_produto_cor.id_cor = cor.id'
      'WHERE grade_produto_cor.id_grade_produto = :ID')
    Left = 49
    Top = 61
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqGradeProdutoCorID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGradeProdutoCorID_GRADE_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Grade Produto'
      FieldName = 'ID_GRADE_PRODUTO'
      Origin = 'ID_GRADE_PRODUTO'
    end
    object fdqGradeProdutoCorID_COR: TIntegerField
      DisplayLabel = 'C'#243'digo da Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
      Required = True
    end
    object fdqGradeProdutoCorJOIN_DESCRICAO_COR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object fdqGradeProdutoCorTamanho: TgbFDQuery
    MasterSource = dsGradeProduto
    MasterFields = 'ID'
    DetailFields = 'ID_GRADE_PRODUTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache, evDetailOptimize]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT grade_produto_cor_tamanho.*'
      '      ,cor.descricao AS JOIN_DESCRICAO_COR'
      '      ,tamanho.descricao AS JOIN_DESCRICAO_TAMANHO'
      'FROM grade_produto_cor_tamanho'
      'INNER JOIN cor ON grade_produto_cor_tamanho.id_cor = cor.id'
      
        'INNER JOIN tamanho ON grade_produto_cor_tamanho.id_tamanho = tam' +
        'anho.id'
      'WHERE grade_produto_cor_tamanho.id_grade_produto = :ID')
    Left = 177
    Top = 61
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqGradeProdutoCorTamanhoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGradeProdutoCorTamanhoID_GRADE_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Grade de Produto'
      FieldName = 'ID_GRADE_PRODUTO'
      Origin = 'ID_GRADE_PRODUTO'
    end
    object fdqGradeProdutoCorTamanhoID_COR: TIntegerField
      DisplayLabel = 'Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
      Required = True
    end
    object fdqGradeProdutoCorTamanhoJOIN_DESCRICAO_COR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object fdqGradeProdutoCorTamanhoID_TAMANHO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tamanho'
      FieldName = 'ID_TAMANHO'
      Origin = 'ID_TAMANHO'
      Required = True
    end
    object fdqGradeProdutoCorTamanhoJOIN_DESCRICAO_TAMANHO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tamanho'
      FieldName = 'JOIN_DESCRICAO_TAMANHO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
