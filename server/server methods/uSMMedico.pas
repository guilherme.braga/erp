unit uSMMedico;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMMedico = class(TDSServerModule)
    fdqMedico: TgbFDQuery;
    dspMedico: TDataSetProvider;
    fdqMedicoID: TFDAutoIncField;
    fdqMedicoNOME: TStringField;
    fdqMedicoNUMERO: TIntegerField;
    fdqMedicoCOMPLEMENTO: TStringField;
    fdqMedicoBAIRRO: TStringField;
    fdqMedicoCEP: TStringField;
    fdqMedicoLOGRADOURO: TStringField;
    fdqMedicoID_CIDADE: TIntegerField;
    fdqMedicoJOIN_DESCRICAO_CIDADE: TStringField;
    fdqMedicoTELEFONE: TStringField;
    fdqMedicoOBSERVACAO: TBlobField;
    fdqMedicoCRM: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection;

{$R *.dfm}

end.

