object SMMontadora: TSMMontadora
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqMontadora: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * '
      '  FROM montadora'
      'WHERE montadora.id = :ID')
    Left = 32
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqMontadoraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMontadoraDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqMontadoraBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqMontadoraOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspMontadora: TDataSetProvider
    DataSet = fdqMontadora
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 8
  end
  object dsMontadora: TDataSource
    DataSet = fdqMontadora
    Left = 96
    Top = 8
  end
  object fdqListaMontadora: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM montadora')
    Left = 40
    Top = 72
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object StringField1: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object StringField2: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object BlobField1: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspListaMontadora: TDataSetProvider
    DataSet = fdqListaMontadora
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 68
    Top = 72
  end
end
