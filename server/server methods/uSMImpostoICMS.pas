unit uSMImpostoICMS;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter, uImpostoICMSProxy;

type
  TSMImpostoICMS = class(TDSServerModule)
    fdqImpostoICMS: TgbFDQuery;
    fdqImpostoICMSID: TFDAutoIncField;
    dspImpostoICMS: TDataSetProvider;
    fdqImpostoICMSBO_INCLUIR_FRETE_NA_BASE_IPI: TStringField;
    fdqImpostoICMSBO_INCLUIR_FRETE_NA_BASE_ICMS: TStringField;
    fdqImpostoICMSBO_INCLUIR_IPI_NA_BASE_ICMS: TStringField;
    fdqImpostoICMSPERC_REDUCAO_ICMS_ST: TFMTBCDField;
    fdqImpostoICMSMODALIDADE_BASE_ICMS: TIntegerField;
    fdqImpostoICMSMODALIDADE_BASE_ICMS_ST: TIntegerField;
    fdqImpostoICMSPERC_REDUCAO_ICMS: TFMTBCDField;
    fdqImpostoICMSPERC_ALIQUOTA_ICMS: TFMTBCDField;
    fdqImpostoICMSPERC_ALIQUOTA_ICMS_ST: TFMTBCDField;
    fdqImpostoICMSPERC_MVA_AJUSTADO_ST: TFMTBCDField;
    fdqImpostoICMSPERC_MVA_PROPRIO: TFMTBCDField;
    fdqImpostoICMSMOTIVO_DESONERACAO_ICMS: TIntegerField;
    fdqImpostoICMSPERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST: TFMTBCDField;
    fdqImpostoICMSBO_DESTACAR_VALOR_ICMS: TStringField;
    fdqImpostoICMSID_CST_CSOSN: TIntegerField;
    fdqImpostoICMSJOIN_DESCRICAO_CST_CSOSN: TStringField;
    fdqImpostoICMSPERC_BASE_ICMS_PROPRIO: TFMTBCDField;
    fdqImpostoICMSJOIN_CODIGO_CST_CSOSN: TStringField;
  private
    { Private declarations }
  public
    function GetImpostoICMS(AIdImpostoICMS: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uImpostoICMSServ, REST.JSON;

{$R *.dfm}

{ TSMImpostoICMS }

function TSMImpostoICMS.GetImpostoICMS(
  AIdImpostoICMS: Integer): String;
begin
  result := TJson.ObjectToJsonString(TImpostoICMSServ.GetImpostoICMS(AIdImpostoICMS));
end;

end.

