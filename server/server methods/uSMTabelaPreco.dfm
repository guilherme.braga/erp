object SMTabelaPreco: TSMTabelaPreco
  OldCreateOrder = False
  Height = 360
  Width = 707
  object fdqTabelaPreco: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT tp.*'
      '      ,f.fantasia as JOIN_DESCRICAO_FILIAL  '
      '  FROM TABELA_PRECO tp'
      ' INNER JOIN FILIAL f ON tp.id_filial = f.id'
      'WHERE tp.ID = :ID')
    Left = 36
    Top = 18
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqTabelaPrecoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqTabelaPrecoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 45
    end
    object fdqTabelaPrecoID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqTabelaPrecoJOIN_DESCRICAO_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Filial'
      FieldName = 'JOIN_DESCRICAO_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspTabelaPreco: TDataSetProvider
    DataSet = fdqTabelaPreco
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 128
    Top = 17
  end
  object fdqTabelaPrecoItem: TgbFDQuery
    MasterSource = dsTabelaPreco
    MasterFields = 'ID'
    DetailFields = 'ID_TABELA_PRECO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT tbi.*'
      '      ,p.descricao as JOIN_DESCRICAO_PRODUTO '
      '      ,p.tipo'
      '      ,p.id_grupo_produto'
      '      ,gp.descricao as JOIN_DESCRICAO_GRUPO_PRODUTO'
      '      ,p.id_sub_grupo_produto'
      '      ,sgp.descricao as JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
      '      ,p.id_categoria'
      '      ,c.descricao as JOIN_DESCRICAO_CATEGORIA'
      '      ,p.id_sub_categoria'
      '      ,sc.descricao as JOIN_DESCRICAO_SUB_CATEGORIA'
      '      ,p.id_modelo'
      '      ,m.descricao as JOIN_DESCRICAO_MODELO '
      '      ,p.id_unidade_estoque'
      '      ,ue.descricao as JOIN_DESCRICAO_UNIDADE_ESTOQUE '
      '      ,p.id_marca'
      '      ,mc.descricao as JOIN_DESCRICAO_MARCA'
      '      ,p.id_linha'
      '      ,l.descricao as JOIN_DESCRICAO_LINHA '
      'FROM tabela_preco_item tbi'
      ''
      'INNER JOIN produto p ON tbi.id_produto = p.id'
      'LEFT JOIN grupo_produto gp ON p.id_grupo_produto = gp.id'
      
        'LEFT JOIN sub_grupo_produto sgp ON p.id_sub_grupo_produto = sgp.' +
        'id'
      'LEFT JOIN categoria c ON p.id_categoria = c.id'
      'LEFT JOIN sub_categoria sc ON p.id_sub_categoria = sc.id'
      'LEFT JOIN modelo m ON p.id_modelo = m.id'
      'LEFT JOIN unidade_estoque ue ON p.id_unidade_estoque = ue.id'
      'LEFT JOIN marca mc ON p.id_marca = mc.id'
      'LEFT JOIN linha l ON p.id_linha = l.id'
      ''
      'WHERE id_tabela_preco = :id')
    Left = 36
    Top = 66
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqTabelaPrecoItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqTabelaPrecoItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqTabelaPrecoItemID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
    end
    object fdqTabelaPrecoItemPERC_LUCRO: TFMTBCDField
      AutoGenerateValue = arDefault
      DefaultExpression = '0'
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      Origin = 'PERC_LUCRO'
      DisplayFormat = '###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqTabelaPrecoItemVL_VENDA: TFMTBCDField
      AutoGenerateValue = arDefault
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemtipo: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo'
      FieldName = 'tipo'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 30
    end
    object fdqTabelaPrecoItemid_grupo_produto: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Gr. Produto'
      FieldName = 'id_grupo_produto'
      Origin = 'ID_GRUPO_PRODUTO'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Grupo Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_sub_grupo_produto: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Sub. Gr. Produto'
      FieldName = 'id_sub_grupo_produto'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sub Grupo Produto'
      FieldName = 'JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_categoria: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Categoria'
      FieldName = 'id_categoria'
      Origin = 'ID_CATEGORIA'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_CATEGORIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Categoria'
      FieldName = 'JOIN_DESCRICAO_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_sub_categoria: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Sub Categoria'
      FieldName = 'id_sub_categoria'
      Origin = 'ID_SUB_CATEGORIA'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_SUB_CATEGORIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sub Categoria'
      FieldName = 'JOIN_DESCRICAO_SUB_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_modelo: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Modelo'
      FieldName = 'id_modelo'
      Origin = 'ID_MODELO'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_unidade_estoque: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Un. Estoque'
      FieldName = 'id_unidade_estoque'
      Origin = 'ID_UNIDADE_ESTOQUE'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Un. Estoque'
      FieldName = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_marca: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Marca'
      FieldName = 'id_marca'
      Origin = 'ID_MARCA'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_MARCA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemid_linha: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Linha'
      FieldName = 'id_linha'
      Origin = 'ID_LINHA'
      ProviderFlags = []
    end
    object fdqTabelaPrecoItemJOIN_DESCRICAO_LINHA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Linha'
      FieldName = 'JOIN_DESCRICAO_LINHA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo Operacional Imposto'
      FieldName = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Precision = 24
      Size = 9
    end
  end
  object dsTabelaPreco: TDataSource
    DataSet = fdqTabelaPreco
    Left = 136
    Top = 66
  end
end
