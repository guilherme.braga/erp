unit uSMRestaurante;

interface

uses System.SysUtils, System.Classes, System.Json,
     Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
     FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
     FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
     FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
     FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect, uGBFDQuery;

type
  TSMRestaurante = class(TDSServerModule)
    fdqCaixa: TgbFDQuery;
    dspCaixa: TDataSetProvider;
    fdqConsulta: TFDQuery;
    fdqCaixaID: TFDAutoIncField;
    fdqCaixaDESCRICAO: TStringField;
    fdqConsultaCaixa: TgbFDQuery;
    dspConsultaCaixa: TDataSetProvider;
    fdqConsultaCaixaid_usuario: TFDAutoIncField;
    fdqConsultaCaixaid_caixa: TFDAutoIncField;
    fdqConsultaCaixaid_caixa_movimento: TFDAutoIncField;
    fdqConsultaCaixausuario: TStringField;
    fdqConsultaCaixadt_fechamento: TDateField;
    fdqConsultaCaixadt_abertura: TDateField;
    fdqConsultaCaixahr_fechamento: TTimeField;
    fdqConsultaCaixahr_abertura: TTimeField;
    fdqConsultaCaixasaldo_inicial: TFMTBCDField;
    fdqConsultaCaixadescricao: TStringField;
  private
    { Private declarations }
  public
    procedure FecharCaixa(const AIdCaixaMovimento: Integer);
    function GetCaixa(): TFDJSONDataSets;
    function checkUsuario(const AUsername, APassword: string): Integer;
    procedure imprimirCaixa(const id_caixa_movimento: Integer);
    procedure SetTrocoComanda(const vr_troco: Double; const id_comanda: Integer);
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uRestauranteServ, uDmConnection;

{$R *.dfm}

{ TSMRestaurante }

function TSMRestaurante.checkUsuario(const AUsername,
  APassword: string): Integer;
begin
  result := TRestauranteServ.checkUsuario(fdqConsulta, AUsername, APassword);
end;

procedure TSMRestaurante.FecharCaixa(const AIdCaixaMovimento: Integer);
begin
  TRestauranteServ.FecharCaixa(fdqConsulta, AIdCaixaMovimento);
end;

function TSMRestaurante.GetCaixa: TFDJSONDataSets;
begin
  result := TFDJSONDataSets.Create;
  TRestauranteServ.SetCaixa(fdqConsulta);
  TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
end;

procedure TSMRestaurante.imprimirCaixa(const id_caixa_movimento: Integer);
begin
  TRestauranteServ.ImprimirCaixa(fdqConsulta, id_caixa_movimento);
end;

procedure TSMRestaurante.SetTrocoComanda(const vr_troco: Double; const id_comanda: Integer);
begin
  TRestauranteServ.trocoComanda(fdqConsulta, vr_troco, id_comanda);
end;

end.

