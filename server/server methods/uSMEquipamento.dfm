object SMEquipamento: TSMEquipamento
  OldCreateOrder = False
  Height = 197
  Width = 354
  object fdqEquipamento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select e.*'
      '      ,p.nome as "JOIN_NOME_PESSOA"'
      '      ,m.descricao as "JOIN_DESCRICAO_MODELO"'
      '      ,mc.descricao as "JOIN_DESCRICAO_MARCA" '
      'from equipamento e'
      'LEFT join pessoa p on e.id_pessoa = p.id'
      'LEFT join modelo m on e.id_modelo = m.id'
      'LEFT join marca mc on e.id_marca = mc.id'
      'where e.id = :id')
    Left = 24
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqEquipamentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqEquipamentoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqEquipamentoID_MARCA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
    end
    object fdqEquipamentoID_MODELO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object fdqEquipamentoSERIE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'S'#233'rie'
      FieldName = 'SERIE'
      Origin = 'SERIE'
      Size = 255
    end
    object fdqEquipamentoIMEI: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'IMEI'
      Origin = 'IMEI'
      Size = 100
    end
    object fdqEquipamentoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqEquipamentoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqEquipamentoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqEquipamentoJOIN_DESCRICAO_MODELO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqEquipamentoJOIN_DESCRICAO_MARCA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspEquipamento: TDataSetProvider
    DataSet = fdqEquipamento
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 8
  end
end
