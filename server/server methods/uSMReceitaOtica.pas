unit uSMReceitaOtica;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMReceitaOtica = class(TDSServerModule)
    fdqReceitaOtica: TgbFDQuery;
    dspReceitaOtica: TDataSetProvider;
    fdqReceitaOticaID: TFDAutoIncField;
    fdqReceitaOticaDH_CADASTRO: TDateTimeField;
    fdqReceitaOticaID_PESSOA_USUARIO: TIntegerField;
    fdqReceitaOticaID_PESSOA: TIntegerField;
    fdqReceitaOticaDH_PREVISAO_ENTREGA: TDateTimeField;
    fdqReceitaOticaID_MEDICO: TIntegerField;
    fdqReceitaOticaESPECIFICACAO: TBlobField;
    fdqReceitaOticaOBSERVACAO: TBlobField;
    fdqReceitaOticaID_VENDA: TIntegerField;
    fdqReceitaOticaLONGE_DIREITO_ESFERICO: TStringField;
    fdqReceitaOticaLONGE_DIREITO_CILINDRICO: TStringField;
    fdqReceitaOticaLONGE_DIREITO_EIXO: TStringField;
    fdqReceitaOticaLONGE_ESQUERDO_ESFERICO: TStringField;
    fdqReceitaOticaLONGE_ESQUERDO_CILINDRICO: TStringField;
    fdqReceitaOticaLONGE_ESQUERDO_EIXO: TStringField;
    fdqReceitaOticaPERTO_DIREITO_ESFERICO: TStringField;
    fdqReceitaOticaPERTO_DIREITO_CILINDRICO: TStringField;
    fdqReceitaOticaPERTO_DIREITO_EIXO: TStringField;
    fdqReceitaOticaPERTO_ESQUERDO_ESFERICO: TStringField;
    fdqReceitaOticaPERTO_ESQUERDO_CILINDRICO: TStringField;
    fdqReceitaOticaPERTO_ESQUERDO_EIXO: TStringField;
    fdqReceitaOticaID_FILIAL: TIntegerField;
    fdqReceitaOticaJOIN_NOME_USUARIO: TStringField;
    fdqReceitaOticaJOIN_NOME_PESSOA: TStringField;
    fdqReceitaOticaJOIN_FANTASIA_FILIAL: TStringField;
    fdqReceitaOticaJOIN_NOME_MEDICO: TStringField;
    fdqReceitaOticaPERTO_DNP_DIREITO: TBCDField;
    fdqReceitaOticaPERTO_DNP_ESQUERDO: TBCDField;
    fdqReceitaOticaPERTO_DNP_GERAL: TBCDField;
    fdqReceitaOticaLONGE_DNP_DIREITO: TBCDField;
    fdqReceitaOticaLONGE_DNP_ESQUERDO: TBCDField;
    fdqReceitaOticaLONGE_DNP_GERAL: TBCDField;
    fdqReceitaOticaALTURA: TStringField;
    fdqReceitaOticaADICAO: TStringField;
  private
    { Private declarations }
  public
     function GetReceitaOtica(const AIdReceitaOtica: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uReceitaOticaProxy, uReceitaOticaServ, rest.JSON;

{$R *.dfm}

{ TSMReceitaOtica }

function TSMReceitaOtica.GetReceitaOtica(const AIdReceitaOtica: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TReceitaOticaServ.GetReceitaOtica(AIdReceitaOtica));
end;

end.

