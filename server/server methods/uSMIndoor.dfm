object SMIndoor: TSMIndoor
  OldCreateOrder = False
  Height = 326
  Width = 458
  object fdqEstacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select e.*'
      '      ,p.nome as JOIN_NOME_PESSOA'
      '  from estacao e '
      ' inner join pessoa p on e.id_pessoa = p.id'
      ' where e.id = :id')
    Left = 24
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object fdqEstacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo da Esta'#231#227'o'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqEstacaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqEstacaoSISTEMA_OPERACIONAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sistema Operacional'
      FieldName = 'SISTEMA_OPERACIONAL'
      Origin = 'SISTEMA_OPERACIONAL'
      Size = 255
    end
    object fdqEstacaoTV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TV'
      Origin = 'TV'
      Size = 255
    end
    object fdqEstacaoENDERECO_IP: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Endere'#231'o IP'
      FieldName = 'ENDERECO_IP'
      Origin = 'ENDERECO_IP'
      Size = 12
    end
    object fdqEstacaoOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 5000
    end
    object fdqEstacaoID_PESSOA: TIntegerField
      DisplayLabel = 'Id. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqEstacaoJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspEstacao: TDataSetProvider
    DataSet = fdqEstacao
    Options = [poIncFieldProps, poUseQuoteChar]
    Left = 56
    Top = 16
  end
  object fdqMidia: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from midia where id = :id')
    Left = 24
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object fdqMidiaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo M'#237'dia'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqMidiaNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 255
    end
    object fdqMidiaDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdqMidiaTIPO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de M'#237'dia'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 45
    end
    object fdqMidiaTAMANHO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tamanho'
      FieldName = 'TAMANHO'
      Origin = 'TAMANHO'
      Precision = 24
      Size = 2
    end
    object fdqMidiaDURACAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'DURACAO'
      Origin = 'DURACAO'
    end
    object fdqMidiaDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object fdqMidiaENDERECO_FTP: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Endere'#231'o FTP'
      FieldName = 'ENDERECO_FTP'
      Origin = 'ENDERECO_FTP'
      Size = 255
    end
  end
  object dspMidia: TDataSetProvider
    DataSet = fdqMidia
    Options = [poIncFieldProps, poUseQuoteChar]
    Left = 56
    Top = 80
  end
  object fdqGerenciamentoEstacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ge.*'
      '      ,e.descricao JOIN_DESCRICAO_ESTACAO '
      'from gerenciamento_estacao ge'
      'inner join estacao e on ge.id_estacao = e.id'
      'where ge.id = :id ')
    Left = 64
    Top = 144
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object fdqGerenciamentoEstacaoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGerenciamentoEstacaoID_ESTACAO: TIntegerField
      FieldName = 'ID_ESTACAO'
      Origin = 'ID_ESTACAO'
      Required = True
    end
    object fdqGerenciamentoEstacaoJOIN_DESCRICAO_ESTACAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_ESTACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqGerenciamentoEstacaoMidia: TgbFDQuery
    MasterSource = dsGerenciamentoEstacao
    MasterFields = 'ID'
    DetailFields = 'ID_GERENCIAMENTO_ESTACAO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'select gem.*'
      '      ,m.nome as JOIN_DESCRICAO_MIDIA'
      '  from gerenciamento_estacao_midia gem'
      'inner join midia m on gem.id_midia = m.id '
      'where gem.id_gerenciamento_estacao = :id')
    Left = 64
    Top = 192
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqGerenciamentoEstacaoMidiaID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGerenciamentoEstacaoMidiaID_MIDIA: TIntegerField
      FieldName = 'ID_MIDIA'
      Origin = 'ID_MIDIA'
      Required = True
    end
    object fdqGerenciamentoEstacaoMidiaID_GERENCIAMENTO_ESTACAO: TIntegerField
      FieldName = 'ID_GERENCIAMENTO_ESTACAO'
      Origin = 'ID_GERENCIAMENTO_ESTACAO'
    end
    object fdqGerenciamentoEstacaoMidiaJOIN_DESCRICAO_MIDIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_MIDIA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspGerenciamentoEstacao: TDataSetProvider
    DataSet = fdqGerenciamentoEstacao
    Options = [poIncFieldProps, poUseQuoteChar]
    Left = 96
    Top = 144
  end
  object dsGerenciamentoEstacao: TDataSource
    DataSet = fdqGerenciamentoEstacao
    Left = 124
    Top = 144
  end
end
