object SMChaveProcesso: TSMChaveProcesso
  OldCreateOrder = False
  Height = 260
  Width = 552
  object fdqChaveProcesso: TgbFDQuery
    MasterFields = 'ID'
    DetailFields = 'ID_PLANO_CONTA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM chave_processo WHERE id = :id')
    Left = 48
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqChaveProcessoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqChaveProcessoCHAVE: TStringField
      DisplayLabel = 'Chave'
      FieldName = 'CHAVE'
      Origin = 'CHAVE'
      Required = True
      Size = 255
    end
    object fdqChaveProcessoORIGEM: TStringField
      DisplayLabel = 'Origem'
      FieldName = 'ORIGEM'
      Origin = 'ORIGEM'
      Required = True
      Size = 100
    end
  end
  object dspChaveProcesso: TDataSetProvider
    DataSet = fdqChaveProcesso
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 76
    Top = 8
  end
end
