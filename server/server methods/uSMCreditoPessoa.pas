unit uSMCreditoPessoa;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMCreditoPessoa = class(TDSServerModule)
    fdqPessoaCredito: TgbFDQuery;
    dspPessoaCredito: TDataSetProvider;
    fdqPessoaCreditoID: TFDAutoIncField;
    fdqPessoaCreditoVL_CREDITO: TFMTBCDField;
    fdqPessoaCreditoID_PESSOA: TIntegerField;
    fdqPessoaCreditoJOIN_NOME_PESSOA: TStringField;
    fdqPessoaCreditoMovimento: TgbFDQuery;
    dspPessoaCreditoMovimento: TDataSetProvider;
    fdqPessoaCreditoMovimentoID: TFDAutoIncField;
    fdqPessoaCreditoMovimentoVL_CREDITO_ANTERIOR: TFMTBCDField;
    fdqPessoaCreditoMovimentoVL_CREDITO_MOVIMENTO: TFMTBCDField;
    fdqPessoaCreditoMovimentoVL_CREDITO_ATUAL: TFMTBCDField;
    fdqPessoaCreditoMovimentoDH_MOVIMENTO: TDateTimeField;
    fdqPessoaCreditoMovimentoID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqPessoaCreditoMovimentoDOCUMENTO_ORIGEM: TStringField;
    fdqPessoaCreditoMovimentoID_PESSOA: TIntegerField;
    fdqPessoaCreditoMovimentoID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaCreditoMovimentoID_CHAVE_PROCESSO: TIntegerField;
    fdqPessoaCreditoMovimentoTIPO: TStringField;
    fdqPessoaCreditoMovimentoJOIN_NOME_PESSOA: TStringField;
    fdqPessoaCreditoMovimentoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqPessoaCreditoMovimentoJOIN_USUARIO_USUARIO: TStringField;
  private
    { Private declarations }
  public
    function CreditoDisponivel(AIdPessoa: Integer): Double;
    function RealizarTransacaoCredito(APessoaCredito: String): Boolean;
    function ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uPessoaCreditoServ;

{$R *.dfm}

{ TSMCreditoPessoa }

function TSMCreditoPessoa.CreditoDisponivel(AIdPessoa: Integer): Double;
begin
  result := TPessoaCreditoServ.CreditoDisponivel(AIdPessoa);
end;

function TSMCreditoPessoa.ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
begin
  result := TPessoaCreditoServ.ExisteMovimentacao(AIdPessoa);
end;

function TSMCreditoPessoa.RealizarTransacaoCredito(
  APessoaCredito: String): Boolean;
begin
  result := TPessoaCreditoServ.RealizarTransacaoCredito(APessoaCredito);
end;

end.

