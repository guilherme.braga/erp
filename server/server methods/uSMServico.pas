unit uSMServico;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter, uServicoProxy;

type
  TSMServico = class(TDSServerModule)
    fdqServico: TgbFDQuery;
    dspServico: TDataSetProvider;
    fdqServicoID: TFDAutoIncField;
    fdqServicoDESCRICAO: TStringField;
    fdqServicoVL_CUSTO: TFMTBCDField;
    fdqServicoPERC_LUCRO: TFMTBCDField;
    fdqServicoVL_VENDA: TFMTBCDField;
    fdqServicoBO_SERVICO_TERCEIRO: TStringField;
    fdqServicoBO_ATIVO: TStringField;
    fdqServicoDURACAO: TTimeField;
  private

  public
    function GetServico(const AIdServico: Integer): String;
    function GetDescricao(const AIdServico: Integer): String;
    function GerarServico(const AServico: TServicoProxy): Integer;
    function ExisteServico(const ADescricaoServico: String): Boolean;
    function ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
    function GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
    function GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uServicoServ, rest.json;

{$R *.dfm}

{ TSMServico }

function TSMServico.ExisteServico(const ADescricaoServico: String): Boolean;
begin
  result := TServicoServ.ExisteServico(ADescricaoServico);
end;

function TSMServico.ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
begin
  result := TServicoServ.ExisteServicoTerceiro(ADescricaoServico);
end;

function TSMServico.GerarServico(const AServico: TServicoProxy): Integer;
begin
  result := TServicoServ.GerarServico(AServico);
end;

function TSMServico.GetDescricao(const AIdServico: Integer): String;
begin
  result := TServicoServ.GetDescricao(AIdServico);
end;

function TSMServico.GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
begin
  result := TServicoServ.GetIdServicoPorDescricao(ADescricaoServico);
end;

function TSMServico.GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
begin
  result := TServicoServ.GetIdServicoTerceiroPorDescricao(ADescricaoServico);
end;

function TSMServico.GetServico(const AIdServico: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TServicoServ.GetServico(AIdServico));
end;

end.

