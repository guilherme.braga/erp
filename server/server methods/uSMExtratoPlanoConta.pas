unit uSMExtratoPlanoConta;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect,
  uGBFDQuery, dialogs;

type
  TSMExtratoPlanoConta = class(TDSServerModule)
    fdqPrimeiroNivel: TgbFDQuery;
    dspPrimeiroNivel: TDataSetProvider;
    fdqPrimeiroNivelid: TIntegerField;
    fdqPrimeiroNivelsequencia: TStringField;
    fdqPrimeiroNiveldescricao: TStringField;
    fdqPrimeiroNivelid_centro_resultado: TFDAutoIncField;
    fdqPrimeiroNiveldesc_centro_resultado: TStringField;
    fdqPrimeiroNivelid_grupo_centro_resultado: TFDAutoIncField;
    fdqPrimeiroNiveldesc_grupo_centro_resultado: TStringField;
    fdqPrimeiroNivelBO_CHECKED: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection;

{$R *.dfm}

end.

