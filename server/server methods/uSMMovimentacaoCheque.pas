unit uSMMovimentacaoCheque;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, Datasnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMMovimentacaoCheque = class(TDSServerModule)
    fdqMovimentacaoCheque: TgbFDQuery;
    dspMovimentacaoCheque: TDataSetProvider;
    dsMovimentacaoCheque: TDataSource;
    fdqMovimentacaoChequeCheque: TgbFDQuery;
    fdqMovimentacaoChequeID: TFDAutoIncField;
    fdqMovimentacaoChequeTIPO_MOVIMENTO: TStringField;
    fdqMovimentacaoChequeID_PESSOA_USUARIO_CADASTRO: TIntegerField;
    fdqMovimentacaoChequeID_PESSOA_USUARIO_EFETIVACAO: TIntegerField;
    fdqMovimentacaoChequeID_PESSOA_USUARIO_REABERTURA: TIntegerField;
    fdqMovimentacaoChequeID_CONTA_CORRENTE: TIntegerField;
    fdqMovimentacaoChequeID_CONTA_CORRENTE_DESTINO: TIntegerField;
    fdqMovimentacaoChequeID_FILIAL: TIntegerField;
    fdqMovimentacaoChequeID_CHAVE_PROCESSO: TIntegerField;
    fdqMovimentacaoChequeDH_CADASTRO: TDateTimeField;
    fdqMovimentacaoChequeDH_FECHAMENTO: TDateTimeField;
    fdqMovimentacaoChequeOBSERVACAO: TBlobField;
    fdqMovimentacaoChequeSTATUS: TStringField;
    fdqMovimentacaoChequeJOIN_NOME_USUARIO: TStringField;
    fdqMovimentacaoChequeJOIN_FANTASIA_FILIAL: TStringField;
    fdqMovimentacaoChequeJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqMovimentacaoChequeJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqMovimentacaoChequeJOIN_DESCRICAO_CONTA_CORRENTE_DESTINO: TStringField;
    fdqMovimentacaoChequeChequeID: TFDAutoIncField;
    fdqMovimentacaoChequeChequeID_MOVIMENTACAO_CHEQUE: TIntegerField;
    fdqMovimentacaoChequeChequeID_CHEQUE: TIntegerField;
    fdqMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR: TIntegerField;
    fdqMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ATUAL: TIntegerField;
    fdqMovimentacaoChequeVL_TOTAL_CHEQUE: TFMTBCDField;
  private

  public
    function Efetivar(const AIdChaveProcesso: Integer): Boolean;
    function Estornar(const AIdChaveProcesso: Integer): Boolean;
    function PodeReabrir(const AIdMovimentacaoCheque: Integer): Boolean;
    function PodeExcluir(const AIdMovimentacaoCheque: Integer): Boolean;
    function BuscarIDPelaChaveProcesso(const AIDChaveProcesso: Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uMovimentacaoChequeServ;

{$R *.dfm}

{ TSMMovimentacaoCheque }

function TSMMovimentacaoCheque.BuscarIDPelaChaveProcesso(const AIDChaveProcesso: Integer): Integer;
begin
  result := TMovimentacaoChequeServ.BuscarIDPelaChaveProcesso(AIdChaveProcesso);
end;

function TSMMovimentacaoCheque.Efetivar(const AIdChaveProcesso: Integer): Boolean;
begin
  result := TMovimentacaoChequeServ.Efetivar(AIdChaveProcesso);
end;

function TSMMovimentacaoCheque.Estornar(const AIdChaveProcesso: Integer): Boolean;
begin
  result := TMovimentacaoChequeServ.Estornar(AIdChaveProcesso);
end;

function TSMMovimentacaoCheque.PodeExcluir(const AIdMovimentacaoCheque: Integer): Boolean;
begin
  result := TMovimentacaoChequeServ.PodeExcluir(AIdMovimentacaoCheque);
end;

function TSMMovimentacaoCheque.PodeReabrir(const AIdMovimentacaoCheque: Integer): Boolean;
begin
  result := TMovimentacaoChequeServ.PodeReabrir(AIdMovimentacaoCheque);
end;

end.

