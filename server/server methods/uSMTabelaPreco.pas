unit uSMTabelaPreco;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery, Datasnap.DSProviderDataModuleAdapter,
  Data.FireDACJSONReflect, REST.JSON, uTabelaPrecoProxy, uProdutoProxy;

type
  TSMTabelaPreco = class(TDSServerModule)
    fdqTabelaPreco: TgbFDQuery;
    dspTabelaPreco: TDataSetProvider;
    fdqTabelaPrecoID: TFDAutoIncField;
    fdqTabelaPrecoDESCRICAO: TStringField;
    fdqTabelaPrecoID_FILIAL: TIntegerField;
    fdqTabelaPrecoItem: TgbFDQuery;
    dsTabelaPreco: TDataSource;
    fdqTabelaPrecoItemID: TFDAutoIncField;
    fdqTabelaPrecoItemID_PRODUTO: TIntegerField;
    fdqTabelaPrecoItemID_TABELA_PRECO: TIntegerField;
    fdqTabelaPrecoItemPERC_LUCRO: TFMTBCDField;
    fdqTabelaPrecoItemVL_VENDA: TFMTBCDField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqTabelaPrecoJOIN_DESCRICAO_FILIAL: TStringField;
    fdqTabelaPrecoItemtipo: TStringField;
    fdqTabelaPrecoItemid_grupo_produto: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    fdqTabelaPrecoItemid_sub_grupo_produto: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField;
    fdqTabelaPrecoItemid_categoria: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_CATEGORIA: TStringField;
    fdqTabelaPrecoItemid_sub_categoria: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_SUB_CATEGORIA: TStringField;
    fdqTabelaPrecoItemid_modelo: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_MODELO: TStringField;
    fdqTabelaPrecoItemid_unidade_estoque: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField;
    fdqTabelaPrecoItemid_marca: TIntegerField;
    fdqTabelaPrecoItemid_linha: TIntegerField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_LINHA: TStringField;
    fdqTabelaPrecoItemJOIN_DESCRICAO_MARCA: TStringField;
    fdqTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField;
  private
    { Private declarations }
  public
    procedure AtualizarRegistro(AIdTabelaPreco, AIdProduto: Integer;
      AVlCusto, APercLucro, AVlVenda: Double);
    function GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
    function GetProdutosTabelaPreco(const AIdTabelaPreco: Integer): TFDJSONDataSets;
    function BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
    function GetDescricaoTabelaPreco(const AID: Integer): String;
    function GetProdutoTabelaPreco(const AIdTabelaPreco, AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
    function GetTabelaPrecoItem(const AIdTabelaPreco, AIdProduto: Integer): TTabelaPrecoItemProxy;
    function ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uTabelaPrecoServ;

{$R *.dfm}

{ TSMTabelaPreco }

procedure TSMTabelaPreco.AtualizarRegistro(AIdTabelaPreco, AIdProduto: Integer;
  AVlCusto, APercLucro, AVlVenda: Double);
begin
  TTabelaPrecoServ.AtualizarRegistro(AIdTabelaPreco, AIdProduto,
    AVlCusto, APercLucro, AVlVenda);
end;

function TSMTabelaPreco.BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
begin
  result := TTabelaPrecoServ.BuscarProdutoEmSuasTabelasPreco(AIdProduto);
end;

function TSMTabelaPreco.ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
begin
  result := TTabelaPrecoServ.ExcluirTabelaDePreco(AIdTabelaPreco);
end;

function TSMTabelaPreco.GetDescricaoTabelaPreco(const AID: Integer): String;
begin
  result := TTabelaPrecoServ.GetDescricaoTabelaPreco(AID);
end;

function TSMTabelaPreco.GetProdutosTabelaPreco(
  const AIdTabelaPreco: Integer): TFDJSONDataSets;
begin
  result := TTabelaPrecoServ.GetProdutosTabelaPreco(AIdTabelaPreco);
end;

function TSMTabelaPreco.GetProdutoTabelaPreco(const AIdTabelaPreco,
  AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
begin
  result := TTabelaPrecoServ.GetProdutoTabelaPreco(AIdTabelaPreco, AIdProduto, AIdFilial);
end;

function TSMTabelaPreco.GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
begin
  result := TTabelaPrecoServ.GetTabelaPrecoComMaisProdutos;
end;

function TSMTabelaPreco.GetTabelaPrecoItem(const AIdTabelaPreco, AIdProduto: Integer): TTabelaPrecoItemProxy;
begin
  result := TTabelaPrecoItemServ.GetTabelaPrecoItem(AIdTabelaPreco, AIdProduto);
end;

end.

