unit uSMRegimeEspecial;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMRegimeEspecial = class(TDSServerModule)
    fdqRegimeEspecial: TgbFDQuery;
    dspRegimeEspecial: TDataSetProvider;
    fdqRegimeEspecialID: TFDAutoIncField;
    fdqRegimeEspecialDESCRICAO: TStringField;
    fdqRegimeEspecialTEXTO_ESPECIAL: TBlobField;
    fdqRegimeEspecialBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    function GetRegimeEspecial(AIdRegimeEspecial: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uRegimeEspecialProxy, uRegimeEspecialServ, REST.JSON;

{$R *.dfm}

{ TSMRegimeEspecial }

function TSMRegimeEspecial.GetRegimeEspecial(
  AIdRegimeEspecial: Integer): String;
begin
  result := TJson.ObjectToJsonString(TRegimeEspecialServ.GetRegimeEspecial(AIdRegimeEspecial));
end;

end.

