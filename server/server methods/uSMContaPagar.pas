unit uSMContaPagar;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery, Datasnap.Provider, Data.FireDACJSONReflect;

type
  TSMContaPagar = class(TDSServerModule)
    dspContaPagar: TDataSetProvider;
    fdqContaPagar: TgbFDQuery;
    fdqContaPagarQuitacao: TgbFDQuery;
    dsContaPagar: TDataSource;
    fdqContaPagarID: TFDAutoIncField;
    fdqContaPagarDH_CADASTRO: TDateTimeField;
    fdqContaPagarDOCUMENTO: TStringField;
    fdqContaPagarDESCRICAO: TStringField;
    fdqContaPagarVL_TITULO: TFMTBCDField;
    fdqContaPagarVL_QUITADO: TFMTBCDField;
    fdqContaPagarVL_ABERTO: TFMTBCDField;
    fdqContaPagarQT_PARCELA: TIntegerField;
    fdqContaPagarNR_PARCELA: TIntegerField;
    fdqContaPagarDT_VENCIMENTO: TDateField;
    fdqContaPagarSEQUENCIA: TStringField;
    fdqContaPagarBO_VENCIDO: TStringField;
    fdqContaPagarOBSERVACAO: TBlobField;
    fdqContaPagarID_PESSOA: TIntegerField;
    fdqContaPagarID_CONTA_ANALISE: TIntegerField;
    fdqContaPagarID_CENTRO_RESULTADO: TIntegerField;
    fdqContaPagarSTATUS: TStringField;
    fdqContaPagarID_CHAVE_PROCESSO: TIntegerField;
    fdqContaPagarJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    fdqContaPagarJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqContaPagarJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqContaPagarJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqContaPagarJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    fdqContaPagarQuitacaoID: TFDAutoIncField;
    fdqContaPagarQuitacaoDH_CADASTRO: TDateTimeField;
    fdqContaPagarQuitacaoOBSERVACAO: TBlobField;
    fdqContaPagarQuitacaoID_TIPO_QUITACAO: TIntegerField;
    fdqContaPagarQuitacaoDT_QUITACAO: TDateField;
    fdqContaPagarQuitacaoID_FILIAL: TIntegerField;
    fdqContaPagarQuitacaoID_CONTA_CORRENTE: TIntegerField;
    fdqContaPagarQuitacaoVL_DESCONTO: TFMTBCDField;
    fdqContaPagarQuitacaoVL_ACRESCIMO: TFMTBCDField;
    fdqContaPagarQuitacaoVL_QUITACAO: TFMTBCDField;
    fdqContaPagarQuitacaoNR_ITEM: TIntegerField;
    fdqContaPagarQuitacaoVL_TOTAL: TFMTBCDField;
    fdqContaPagarQuitacaoID_CONTA_PAGAR: TIntegerField;
    fdqContaPagarQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    fdqContaPagarQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqContaPagarID_FORMA_PAGAMENTO: TIntegerField;
    fdqContaPagarVL_ACRESCIMO: TFMTBCDField;
    fdqContaPagarVL_DECRESCIMO: TFMTBCDField;
    fdqContaPagarVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqContaPagarJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqContaPagarDT_COMPETENCIA: TDateField;
    fdqContaPagarQuitacaoID_CONTA_ANALISE: TIntegerField;
    fdqContaPagarQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqContaPagarID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqContaPagarTP_DOCUMENTO_ORIGEM: TStringField;
    fdqContaPagarQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqContaPagarQuitacaoTP_DOCUMENTO_ORIGEM: TStringField;
    fdqContaPagarQuitacaoSTATUS: TStringField;
    fdqContaPagarDT_DOCUMENTO: TDateField;
    fdqContaPagarID_CONTA_CORRENTE: TIntegerField;
    fdqContaPagarJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqContaPagarQuitacaoCHEQUE_NUMERO: TIntegerField;
    fdqContaPagarQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    fdqContaPagarQuitacaoCHEQUE_AGENCIA: TStringField;
    fdqContaPagarQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    fdqContaPagarQuitacaoCHEQUE_BANCO: TStringField;
    fdqContaPagarQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    fdqContaPagarQuitacaoCHEQUE_SACADO: TStringField;
    fdqContaPagarQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    fdqContaPagarQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqContaPagarQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqContaPagarQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    fdqContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    fdqContaPagarID_FILIAL: TIntegerField;
    fdqContaPagarQuitacaoVL_JUROS: TFMTBCDField;
    fdqContaPagarQuitacaoPERC_JUROS: TFMTBCDField;
    fdqContaPagarQuitacaoVL_MULTA: TFMTBCDField;
    fdqContaPagarQuitacaoPERC_MULTA: TFMTBCDField;
    fdqContaPagarVL_JUROS: TFMTBCDField;
    fdqContaPagarPERC_JUROS: TFMTBCDField;
    fdqContaPagarVL_MULTA: TFMTBCDField;
    fdqContaPagarPERC_MULTA: TFMTBCDField;
    fdqContaPagarID_CARTEIRA: TIntegerField;
    fdqContaPagarJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqContaPagarQuitacaoPERC_ACRESCIMO: TFMTBCDField;
    fdqContaPagarQuitacaoPERC_DESCONTO: TFMTBCDField;
    fdqContaPagarQuitacaoVL_TROCO: TFMTBCDField;
    fdqContaPagarQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    fdqContaPagarQuitacaoID_USUARIO_BAIXA: TIntegerField;
    fdqContaPagarQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
  private


    { Private declarations }
  public
    function GerarContaPagar(AValue: String) : Boolean;
    function RemoverContaPagar(AValue: Integer) : Boolean;
    function PodeRemover(AValue: Integer) : Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso: Integer;
      AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean;
    function GetIdContaPagarPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;

    function GetTotaisContaPagar(const AIdsContaPagar: String): TFDJSONDatasets;

    function PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Integer;
    function ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uContaPagarServ;

{$R *.dfm}

{ TSMContaPagar }

function TSMContaPagar.GerarContaPagar(AValue: String): Boolean;
begin
  Result := TContaPagarServ.GerarContaPagar(AValue) > 0;
end;

function TSMContaPagar.GetIdContaPagarPeloIdDaQuitacao(
  AIdQuitacao: Integer): Integer;
begin
  result := TContaPagarServ.GetIdContaPagarPeloIdDaQuitacao(AIdQuitacao);
end;

function TSMContaPagar.GetTotaisContaPagar(const AIdsContaPagar: String): TFDJSONDatasets;
begin
  Result := TContaPagarServ.GetTotaisContaPagar(AIdsContaPagar);
end;

function TSMContaPagar.PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Integer;
begin
  result := TContaPagarServ.PessoaPossuiContaPagarEmAberto(AIdPessoa);
end;

function TSMContaPagar.PodeRemover(AValue: Integer): Boolean;
begin
  Result := TContaPagarServ.PodeRemover(AValue);
end;

function TSMContaPagar.ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Integer;
begin
  result := TContaPagarServ.ProcessoPossuiContaPagarEmAberto(AIdChaveProcesso);
end;

function TSMContaPagar.RemoverContaPagar(AValue: Integer): Boolean;
begin
  Result := TContaPagarServ.RemoverContaPagar(AValue);
end;

function TSMContaPagar.AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso: Integer;
  AGerarContraPartida: Boolean) : Boolean;
begin
  Result := TContaPagarServ.AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso,
    AGerarContraPartida);
end;

function TSMContaPagar.RemoverMovimentosContaCorrente(AIdContaPagar: Integer;
  AGerarContraPartida: Boolean): Boolean;
begin
  Result := TContaPagarServ.RemoverMovimentosContaCorrente(AIdContaPagar, AGerarContraPartida);
end;

end.

