object SMAjusteEstoque: TSMAjusteEstoque
  OldCreateOrder = False
  Height = 271
  Width = 459
  object fdqAjusteEstoque: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ae.*'
      '      ,f.fantasia as JOIN_FANTASIA_FILIAL'
      '      ,pu.usuario as JOIN_USUARIO'
      '      ,m.descricao as JOIN_DESCRICAO_MOTIVO'
      '      ,cp.origem as JOIN_CHAVE_PROCESSO'
      'from ajuste_estoque ae'
      'inner join filial f on ae.id_filial = f.id'
      'inner join pessoa_usuario pu on ae.id_pessoa_usuario = pu.id'
      'inner join motivo m on ae.id_motivo = m.id'
      'inner join chave_processo cp on ae.id_chave_processo = cp.id'
      'where ae.id = :id')
    Left = 56
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqAjusteEstoqueID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqAjusteEstoqueDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqAjusteEstoqueDH_FECHAMENTO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object fdqAjusteEstoqueSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
      Size = 30
    end
    object fdqAjusteEstoqueID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqAjusteEstoqueID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqAjusteEstoqueID_MOTIVO: TIntegerField
      DisplayLabel = 'C'#243'digo do Motivo'
      FieldName = 'ID_MOTIVO'
      Origin = 'ID_MOTIVO'
      Required = True
    end
    object fdqAjusteEstoqueID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqAjusteEstoqueJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqAjusteEstoqueJOIN_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object fdqAjusteEstoqueJOIN_DESCRICAO_MOTIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Motivo'
      FieldName = 'JOIN_DESCRICAO_MOTIVO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqAjusteEstoqueJOIN_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo de Origem'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
  end
  object fdqAjusteEstoqueItem: TgbFDQuery
    MasterSource = dsAjusteEstoque
    MasterFields = 'ID'
    DetailFields = 'ID_AJUSTE_ESTOQUE'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT aji.*'
      '      ,p.descricao as JOIN_DESCRICAO_PRODUTO'
      '      ,pf.qt_estoque as JOIN_ESTOQUE_PRODUTO'
      'FROM ajuste_estoque_item aji'
      'INNER JOIN ajuste_estoque ae ON aji.id_ajuste_estoque = ae.id'
      'INNER JOIN produto p ON aji.id_produto = p.id'
      
        'INNER JOIN produto_filial pf ON ae.id_filial = pf.id_filial and ' +
        'aji.id_produto = pf.id_produto'
      'WHERE aji.id_ajuste_estoque = :id')
    Left = 56
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqAjusteEstoqueItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqAjusteEstoqueItemNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqAjusteEstoqueItemQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqAjusteEstoqueItemTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 15
    end
    object fdqAjusteEstoqueItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqAjusteEstoqueItemID_AJUSTE_ESTOQUE: TIntegerField
      DisplayLabel = 'C'#243'digo do Ajuste de Estoque'
      FieldName = 'ID_AJUSTE_ESTOQUE'
      Origin = 'ID_AJUSTE_ESTOQUE'
    end
    object fdqAjusteEstoqueItemJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqAjusteEstoqueItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
  end
  object dsAjusteEstoque: TDataSource
    DataSet = fdqAjusteEstoque
    Left = 136
    Top = 48
  end
  object dspAjusteEstoque: TDataSetProvider
    DataSet = fdqAjusteEstoque
    Options = [poFetchBlobsOnDemand, poIncFieldProps, poUseQuoteChar]
    Left = 216
    Top = 120
  end
end
