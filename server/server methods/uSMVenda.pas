unit uSMVenda;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDQuery, Datasnap.Provider, uVendaProxy,
  Datasnap.DSProviderDataModuleAdapter, Data.FireDACJSONReflect;

type
  TSMVenda = class(TDSServerModule)
    dspVenda: TDataSetProvider;
    fdqVenda: TgbFDQuery;
    fdqVendaItem: TgbFDQuery;
    dsVenda: TDataSource;
    fdqVendaParcela: TgbFDQuery;
    fdqVendaID: TFDAutoIncField;
    fdqVendaDH_CADASTRO: TDateTimeField;
    fdqVendaVL_DESCONTO: TFMTBCDField;
    fdqVendaVL_ACRESCIMO: TFMTBCDField;
    fdqVendaVL_TOTAL_PRODUTO: TFMTBCDField;
    fdqVendaVL_VENDA: TFMTBCDField;
    fdqVendaSTATUS: TStringField;
    fdqVendaID_PESSOA: TIntegerField;
    fdqVendaID_FILIAL: TIntegerField;
    fdqVendaID_OPERACAO: TIntegerField;
    fdqVendaID_CHAVE_PROCESSO: TIntegerField;
    fdqVendaID_CENTRO_RESULTADO: TIntegerField;
    fdqVendaID_CONTA_ANALISE: TIntegerField;
    fdqVendaID_PLANO_PAGAMENTO: TIntegerField;
    fdqVendaID_FORMA_PAGAMENTO: TIntegerField;
    fdqVendaPERC_DESCONTO: TFMTBCDField;
    fdqVendaPERC_ACRESCIMO: TFMTBCDField;
    fdqVendaJOIN_NOME_PESSOA: TStringField;
    fdqVendaJOIN_DESCRICAO_OPERACAO: TStringField;
    fdqVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField;
    fdqVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    fdqVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqVendaItemID: TFDAutoIncField;
    fdqVendaItemID_VENDA: TIntegerField;
    fdqVendaItemID_PRODUTO: TIntegerField;
    fdqVendaItemNR_ITEM: TIntegerField;
    fdqVendaItemQUANTIDADE: TFMTBCDField;
    fdqVendaItemVL_DESCONTO: TFMTBCDField;
    fdqVendaItemVL_ACRESCIMO: TFMTBCDField;
    fdqVendaItemVL_BRUTO: TFMTBCDField;
    fdqVendaItemVL_LIQUIDO: TFMTBCDField;
    fdqVendaItemPERC_DESCONTO: TFMTBCDField;
    fdqVendaItemPERC_ACRESCIMO: TFMTBCDField;
    fdqVendaItemJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    fdqVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    fdqVendaParcelaID: TFDAutoIncField;
    fdqVendaParcelaID_VENDA: TIntegerField;
    fdqVendaParcelaDOCUMENTO: TStringField;
    fdqVendaParcelaVL_TITULO: TFMTBCDField;
    fdqVendaParcelaQT_PARCELA: TIntegerField;
    fdqVendaParcelaNR_PARCELA: TIntegerField;
    fdqVendaParcelaDT_VENCIMENTO: TDateField;
    fdqVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    fdqVendaID_TABELA_PRECO: TIntegerField;
    fdqVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    fdqVendaTIPO: TStringField;
    fdqVendaDH_FECHAMENTO: TDateTimeField;
    fdqVendaParcelaOBSERVACAO: TStringField;
    fdqVendaParcelaCHEQUE_SACADO: TStringField;
    fdqVendaParcelaCHEQUE_DOC_FEDERAL: TStringField;
    fdqVendaParcelaCHEQUE_BANCO: TStringField;
    fdqVendaParcelaCHEQUE_DT_EMISSAO: TDateField;
    fdqVendaParcelaCHEQUE_AGENCIA: TStringField;
    fdqVendaParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    fdqVendaParcelaCHEQUE_NUMERO: TIntegerField;
    fdqVendaParcelaID_OPERADORA_CARTAO: TIntegerField;
    fdqVendaVL_PAGAMENTO: TFMTBCDField;
    fdqVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField;
    fdqVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField;
    fdqVendaParcelaID_FORMA_PAGAMENTO: TIntegerField;
    fdqVendaParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField;
    fdqVendaParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqVendaParcelaID_CARTEIRA: TIntegerField;
    fdqVendaParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqVendaID_VENDEDOR: TIntegerField;
    fdqVendaJOIN_NOME_VENDEDOR: TStringField;
    fdqVendaReceitaOtica: TgbFDQuery;
    fdqVendaReceitaOticaID: TFDAutoIncField;
    fdqVendaReceitaOticaID_RECEITA_OTICA: TIntegerField;
    fdqVendaReceitaOticaID_VENDA: TIntegerField;
    fdqVendaReceitaOticaJOIN_ID_PESSOA: TIntegerField;
    fdqVendaReceitaOticaJOIN_NOME_PESSOA: TStringField;
    fdqVendaReceitaOticaJOIN_ID_MEDICO: TIntegerField;
    fdqVendaReceitaOticaJOIN_NOME_MEDICO: TStringField;
    fdqVendaReceitaOticaJOIN_ESPECIFICACAO_RECEITA_OTICA: TBlobField;
    fdqVendaItemDevolucao: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    FMTBCDField1: TFMTBCDField;
    FMTBCDField2: TFMTBCDField;
    FMTBCDField3: TFMTBCDField;
    FMTBCDField4: TFMTBCDField;
    FMTBCDField5: TFMTBCDField;
    FMTBCDField6: TFMTBCDField;
    FMTBCDField7: TFMTBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    FMTBCDField8: TFMTBCDField;
    StringField3: TStringField;
    fdqVendaParcelaCHEQUE_DT_VENCIMENTO: TDateField;
    fdqVendaItemDESCRICAO_PRODUTO: TStringField;
    fdqVendaID_NOTA_FISCAL_NFE: TIntegerField;
    fdqVendaID_OPERACAO_FISCAL: TIntegerField;
    fdqVendaID_NOTA_FISCAL_NFSE: TIntegerField;
    fdqVendaID_NOTA_FISCAL_NFCE: TIntegerField;
    fdqVendaBloqueioPersonalizado: TgbFDQuery;
    fdqVendaBloqueioPersonalizadoID: TFDAutoIncField;
    fdqVendaBloqueioPersonalizadoID_BLOQUEIO_PERSONALIZADO: TIntegerField;
    fdqVendaBloqueioPersonalizadoID_VENDA: TIntegerField;
    fdqVendaBloqueioPersonalizadoSTATUS: TStringField;
    fdqVendaBloqueioPersonalizadoDH_BLOQUEIO: TDateTimeField;
    fdqVendaBloqueioPersonalizadoDH_LIBERACAO: TDateTimeField;
    fdqVendaBloqueioPersonalizadoID_PESSOA_USUARIO: TIntegerField;
    fdqVendaBloqueioPersonalizadoJOIN_NOME_PESSOA_USUARIO: TStringField;
    fdqVendaBloqueioPersonalizadoJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TStringField;
  private

  public
    function Efetivar(AIdChaveProcesso: Integer; AVendaProxy: String): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GetId(AIdChaveProcesso: Integer): Integer;

    function EfetivarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
    function CancelarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
    function GetVendaProxy(const AIdVenda: Integer): String;
    function GetIdNotaFiscalNFCE(AId: Integer): Integer;
    function GetIdNotaFiscalNFE(AId: Integer): Integer;

    function GerarDevolucaoEmDinheiroParaCliente(const AVendaProxy: TVendaProxy;
      const AValorDevolucao: Double): Boolean;
    function GerarDevolucaoEmCreditoParaCliente(const AIdChaveProcesso: Integer;
      const AValorDevolucao: Double): Boolean;
    function GerarDevolucaoCreditandoNoContasAReceber(const AIdChaveProcesso: Integer; const AValorDevolucao: Double;
      AIdsContasReceber: String): Boolean;

    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;

    procedure ExcluirVenda(const AIdChaveProcesso: Integer);
    function GetTotaisPorTipoVenda(const AIdsVenda: String): TFDJSONDatasets;

    function BuscarBloqueiosVenda(const AIdVenda: Integer): TFDJSONDatasets;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uVendaServ, rest.JSON;

{$R *.dfm}

{ TSMVenda }

function TSMVenda.CancelarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
begin
  result := TVendaServ.CancelarMovimentacaoVendaCondicional(AIdChaveProcesso);
end;

function TSMVenda.Efetivar(AIdChaveProcesso: Integer; AVendaProxy: String): Boolean;
begin
  result := TVendaServ.Efetivar(AIdChaveProcesso, AVendaProxy);
end;

function TSMVenda.EfetivarMovimentacaoVendaCondicional(const AIdChaveProcesso: Integer): Boolean;
begin
  result := TVendaServ.EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso);
end;

procedure TSMVenda.ExcluirVenda(const AIdChaveProcesso: Integer);
begin
  TVendaServ.ExcluirVenda(AIdChaveProcesso);
end;

function TSMVenda.GerarDevolucaoCreditandoNoContasAReceber(const AIdChaveProcesso: Integer;
  const AValorDevolucao: Double; AIdsContasReceber: String): Boolean;
begin
  result := TVendaServ.GerarDevolucaoCreditandoNoContasAReceber(
    AIdChaveProcesso, AValorDevolucao, AIdsContasReceber);
end;

function TSMVenda.GerarDevolucaoEmCreditoParaCliente(const AIdChaveProcesso: Integer;
  const AValorDevolucao: Double): Boolean;
begin
  result := TVendaServ.GerarDevolucaoEmCreditoParaCliente(AIdChaveProcesso, AValorDevolucao);
end;

function TSMVenda.GerarDevolucaoEmDinheiroParaCliente(const AVendaProxy: TVendaProxy;
  const AValorDevolucao: Double): Boolean;
begin
  result := TVendaServ.GerarDevolucaoEmDinheiroParaCliente(AVendaProxy, AValorDevolucao);
end;

function TSMVenda.GetId(AIdChaveProcesso: Integer): Integer;
begin
  result := TVendaServ.GetId(AIdChaveProcesso);
end;

function TSMVenda.GetIdNotaFiscalNFCE(AId: Integer): Integer;
begin
  result := TVendaServ.GetIdNotaFiscalNFCE(AId);
end;

function TSMVenda.GetIdNotaFiscalNFE(AId: Integer): Integer;
begin
  result := TVendaServ.GetIdNotaFiscalNFE(AId);
end;

function TSMVenda.GetTotaisPorTipoVenda(
  const AIdsVenda: String): TFDJSONDatasets;
begin
  result := TVendaServ.GetTotaisPorTipoVenda(AIdsVenda);
end;

function TSMVenda.GetVendaProxy(const AIdVenda: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TVendaServ.GetVendaProxy(AIdVenda));
end;

function TSMVenda.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TVendaServ.PodeEstornar(AIdChaveProcesso);
end;

function TSMVenda.BuscarBloqueiosVenda(const AIdVenda: Integer): TFDJSONDatasets;
begin
  result := TVendaServ.BuscarBloqueiosVenda(AIdVenda);
end;

function TSMVenda.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TVendaServ.Cancelar(AIdChaveProcesso);
end;


end.

