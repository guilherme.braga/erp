object SMContaReceber: TSMContaReceber
  OldCreateOrder = False
  Height = 327
  Width = 746
  object dspContaReceber: TDataSetProvider
    DataSet = fdqContaReceber
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 85
    Top = 16
  end
  object fdqContaReceber: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT cr.*'
      '      ,p.nome AS JOIN_DESCRICAO_NOME_PESSOA'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,c.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,cp.origem AS JOIN_ORIGEM_CHAVE_PROCESSO'
      '      ,cp.id_origem AS JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      '      ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,car.descricao as JOIN_DESCRICAO_CARTEIRA'
      'FROM conta_receber cr'
      'INNER JOIN pessoa p ON cr.id_pessoa = p.id'
      'INNER JOIN conta_analise ca ON cr.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado c ON cr.id_centro_resultado = c.id'
      'INNER JOIN forma_pagamento fp ON cr.id_forma_pagamento = fp.id'
      'INNER JOIN chave_processo cp ON cr.id_chave_processo = cp.id'
      'INNER JOIN carteira car ON cr.id_carteira = car.id'
      'LEFT JOIN conta_corrente cc ON cr.id_conta_corrente = cc.id'
      'WHERE cr.id = :id')
    Left = 56
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaReceberID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqContaReceberDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaReceberDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqContaReceberDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqContaReceberVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_QUITADO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_ABERTO: TFMTBCDField
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQT_PARCELA: TIntegerField
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqContaReceberNR_PARCELA: TIntegerField
      DisplayLabel = 'Nr. Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqContaReceberDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqContaReceberSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
      Size = 10
    end
    object fdqContaReceberBO_VENCIDO: TStringField
      DisplayLabel = 'Vencido'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaReceberOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaReceberSTATUS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqContaReceberID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqContaReceberID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqContaReceberID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqContaReceberID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqContaReceberID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object fdqContaReceberJOIN_DESCRICAO_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaReceberJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqContaReceberJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave de Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object fdqContaReceberJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaReceberVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_DECRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Decr'#233'scimo'
      FieldName = 'VL_DECRESCIMO'
      Origin = 'VL_DECRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_QUITADO_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado Liquido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object fdqContaReceberID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqContaReceberTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object fdqContaReceberDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object fdqContaReceberID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqContaReceberJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqContaReceberBO_NEGOCIADO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BO_NEGOCIADO'
      Origin = 'BO_NEGOCIADO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaReceberBO_GERADO_POR_NEGOCIACAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BO_GERADO_POR_NEGOCIACAO'
      Origin = 'BO_GERADO_POR_NEGOCIACAO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaReceberVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqContaReceberJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberVL_NEGOCIADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Negociado'
      FieldName = 'VL_NEGOCIADO'
      Origin = 'VL_NEGOCIADO'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberVL_TITULO_ANTES_NEGOCIACAO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Titulo Antes da Negocia'#231#227'o'
      FieldName = 'VL_TITULO_ANTES_NEGOCIACAO'
      Origin = 'VL_TITULO_ANTES_NEGOCIACAO'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberBO_BOLETO_EMITIDO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto Emitido'
      FieldName = 'BO_BOLETO_EMITIDO'
      Origin = 'BO_BOLETO_EMITIDO'
      FixedChar = True
      Size = 1
    end
    object fdqContaReceberBOLETO_NOSSO_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nosso N'#250'mero'
      FieldName = 'BOLETO_NOSSO_NUMERO'
      Origin = 'BOLETO_NOSSO_NUMERO'
    end
    object fdqContaReceberDH_EMISSAO_BOLETO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Emiss'#227'o do Boleto'
      FieldName = 'DH_EMISSAO_BOLETO'
      Origin = 'DH_EMISSAO_BOLETO'
    end
    object fdqContaReceberBOLETO_LINHA_DIGITAVEL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto Linha Digit'#225'vel'
      FieldName = 'BOLETO_LINHA_DIGITAVEL'
      Origin = 'BOLETO_LINHA_DIGITAVEL'
      Size = 255
    end
    object fdqContaReceberBOLETO_CODIGO_BARRAS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto C'#243'digo de Barras'
      FieldName = 'BOLETO_CODIGO_BARRAS'
      Origin = 'BOLETO_CODIGO_BARRAS'
      Size = 255
    end
  end
  object fdqContaReceberQuitacao: TgbFDQuery
    MasterSource = dsContaReceber
    MasterFields = 'ID'
    DetailFields = 'ID_CONTA_RECEBER'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select cri.*'
      '      ,tq.descricao AS JOIN_DESCRICAO_TIPO_QUITACAO '
      '      ,tq.TIPO AS JOIN_TIPO_TIPO_QUITACAO '
      '      ,cc.descricao AS JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,oc.descricao AS JOIN_DESCRICAO_OPERADORA_CARTAO'
      
        '      ,(select nome from pessoa_usuario where id = cri.id_usuari' +
        'o_baixa) AS JOIN_NOME_USUARIO_BAIXA'
      'from conta_receber_quitacao cri'
      'INNER JOIN tipo_quitacao tq ON cri.id_tipo_quitacao = tq.id'
      'INNER JOIN conta_corrente cc ON cri.id_conta_corrente = cc.id'
      'INNER JOIN conta_analise ca ON cri.id_conta_analise = ca.id'
      
        'INNER JOIN centro_resultado cr ON cri.id_centro_resultado = cr.i' +
        'd'
      'LEFT JOIN operadora_cartao oc ON cri.id_operadora_cartao = oc.id'
      'where cri.id_conta_receber = :id')
    Left = 56
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaReceberQuitacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqContaReceberQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqContaReceberQuitacaoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaReceberQuitacaoID_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'Conta a Receber'
      FieldName = 'ID_CONTA_RECEBER'
      Origin = 'ID_CONTA_RECEBER'
    end
    object fdqContaReceberQuitacaoID_TIPO_QUITACAO: TIntegerField
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object fdqContaReceberQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
    end
    object fdqContaReceberQuitacaoID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqContaReceberQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqContaReceberQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoVL_TOTAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaReceberQuitacaoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaReceberQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberQuitacaoID_CENTRO_RESULTADO: TIntegerField
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqContaReceberQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Id. Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqContaReceberQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqContaReceberQuitacaoTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object fdqContaReceberQuitacaoSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqContaReceberQuitacaoCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqContaReceberQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqContaReceberQuitacaoCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqContaReceberQuitacaoCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqContaReceberQuitacaoCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqContaReceberQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqContaReceberQuitacaoCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqContaReceberQuitacaoID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqContaReceberQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo'
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object fdqContaReceberQuitacaoVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoPERC_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoPERC_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoVL_TROCO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Troco'
      FieldName = 'VL_TROCO'
      Origin = 'VL_TROCO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaReceberQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqContaReceberQuitacaoID_USUARIO_BAIXA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object fdqContaReceberQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dsContaReceber: TDataSource
    DataSet = fdqContaReceber
    Left = 115
    Top = 15
  end
end
