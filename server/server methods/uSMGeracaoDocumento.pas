unit uSMGeracaoDocumento;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery, Datasnap.Provider,
  DIALOGS, Data.FireDACJSONReflect;

type
  TSMGeracaoDocumento = class(TDSServerModule)
    dspGeracaoDocumento: TDataSetProvider;
    fdqGeracaoDocumento: TgbFDQuery;
    fdqGeracaoDocumentoParcela: TgbFDQuery;
    dsGeracaoDocumento: TDataSource;
    fdqGeracaoDocumentoID: TFDAutoIncField;
    fdqGeracaoDocumentoDH_CADASTRO: TDateTimeField;
    fdqGeracaoDocumentoDESCRICAO: TStringField;
    fdqGeracaoDocumentoVL_TITULO: TFMTBCDField;
    fdqGeracaoDocumentoOBSERVACAO: TBlobField;
    fdqGeracaoDocumentoSTATUS: TStringField;
    fdqGeracaoDocumentoID_PESSOA: TIntegerField;
    fdqGeracaoDocumentoID_CONTA_ANALISE: TIntegerField;
    fdqGeracaoDocumentoID_CENTRO_RESULTADO: TIntegerField;
    fdqGeracaoDocumentoID_CHAVE_PROCESSO: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqGeracaoDocumentoParcelaID: TFDAutoIncField;
    fdqGeracaoDocumentoParcelaDOCUMENTO: TStringField;
    fdqGeracaoDocumentoParcelaDESCRICAO: TStringField;
    fdqGeracaoDocumentoParcelaVL_TITULO: TFMTBCDField;
    fdqGeracaoDocumentoParcelaQT_PARCELA: TIntegerField;
    fdqGeracaoDocumentoParcelaNR_PARCELA: TIntegerField;
    fdqGeracaoDocumentoParcelaDT_VENCIMENTO: TDateField;
    fdqGeracaoDocumentoParcelaID_GERACAO_DOCUMENTO: TIntegerField;
    fdqGeracaoDocumentoTIPO: TStringField;
    fdqGeracaoDocumentoID_PLANO_PAGAMENTO: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    fdqGeracaoDocumentoDT_BASE: TDateField;
    fdqGeracaoDocumentoDOCUMENTO: TStringField;
    fdqGeracaoDocumentoID_FORMA_PAGAMENTO: TIntegerField;
    fdqGeracaoDocumentoParcelaTOTAL_PARCELAS: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqGeracaoDocumentoTIPO_PLANO_PAGAMENTO: TStringField;
    fdqGeracaoDocumentoQT_PARCELAS: TIntegerField;
    fdqGeracaoDocumentoParcelaID_CONTA_ANALISE: TIntegerField;
    fdqGeracaoDocumentoParcelaID_CENTRO_RESULTADO: TIntegerField;
    fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqGeracaoDocumentoPARCELA_BASE: TIntegerField;
    fdqGeracaoDocumentoDT_COMPETENCIA: TDateField;
    fdqGeracaoDocumentoParcelaDT_COMPETENCIA: TDateField;
    fdqGeracaoDocumentoDT_DOCUMENTO: TDateField;
    fdqGeracaoDocumentoParcelaDT_DOCUMENTO: TDateField;
    fdqGeracaoDocumentoID_CONTA_CORRENTE: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqGeracaoDocumentoParcelaID_CONTA_CORRENTE: TIntegerField;
    fdqGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqGeracaoDocumentoID_FILIAL: TIntegerField;
    fdqGeracaoDocumentoID_CARTEIRA: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqGeracaoDocumentoVL_ENTRADA: TFMTBCDField;
    fdqGeracaoDocumentoID_FORMA_PAGAMENTO_ENTRADA: TIntegerField;
    fdqGeracaoDocumentoID_CONTA_CORRENTE_ENTRADA: TIntegerField;
    fdqGeracaoDocumentoCHEQUE_SACADO_ENTRADA: TStringField;
    fdqGeracaoDocumentoCHEQUE_DOC_FEDERAL_ENTRADA: TStringField;
    fdqGeracaoDocumentoCHEQUE_BANCO_ENTRADA: TStringField;
    fdqGeracaoDocumentoCHEQUE_DT_EMISSAO_ENTRADA: TDateField;
    fdqGeracaoDocumentoCHEQUE_AGENCIA_ENTRADA: TStringField;
    fdqGeracaoDocumentoCHEQUE_CONTA_CORRENTE_ENTRADA: TStringField;
    fdqGeracaoDocumentoCHEQUE_NUMERO_ENTRADA: TIntegerField;
    fdqGeracaoDocumentoCHEQUE_DT_VENCIMENTO_ENTRADA: TDateField;
    fdqGeracaoDocumentoID_OPERADORA_CARTAO_ENTRADA: TIntegerField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA: TStringField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA: TStringField;
    fdqGeracaoDocumentoJOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA: TStringField;
  private
    { Private declarations }
  public
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function GerarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: String): Boolean;
    function EstornarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: String): Boolean;
    function GetContasReceberAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
    function GetContasPagarAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
    function Duplicar(const AIdGeracaoDocumento: Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uGeracaoDocumentoServ, rest.JSON, uGeracaoDocumentoProxy;

{$R *.dfm}

{ TSMGeracaoDocumento }


{ TSMGeracaoDocumento }

function TSMGeracaoDocumento.Duplicar(const AIdGeracaoDocumento: Integer): Integer;
begin
  result := TGeracaoDocumentoServ.Duplicar(AIdGeracaoDocumento);
end;

function TSMGeracaoDocumento.EstornarDocumento(AIdChaveProcesso: Integer;
  AGeracaoDocumentoProxy: String): Boolean;
begin
  result := TGeracaoDocumentoServ.EstornarDocumento(AIdChaveProcesso,
    TJSON.JsonToObject<TGeracaoDocumentoProxy>(AGeracaoDocumentoProxy));
end;

function TSMGeracaoDocumento.GerarDocumento(AIdChaveProcesso: Integer;
  AGeracaoDocumentoProxy: String): Boolean;
begin
  Result := TGeracaoDocumentoServ.GerarDocumento(AIdChaveProcesso,
    TJSON.JsonToObject<TGeracaoDocumentoProxy>(AGeracaoDocumentoProxy));
end;

function TSMGeracaoDocumento.GetContasPagarAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
begin
  result := TGeracaoDocumentoServ.GetContasPagarAberto(AIdChaveProcesso);
end;

function TSMGeracaoDocumento.GetContasReceberAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
begin
  result := TGeracaoDocumentoServ.GetContasReceberAberto(AIdChaveProcesso);
end;

function TSMGeracaoDocumento.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  Result := TGeracaoDocumentoServ.PodeEstornar(AIdChaveProcesso);
end;

function TSMGeracaoDocumento.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  Result := TGeracaoDocumentoServ.PodeExcluir(AIdChaveProcesso);
end;

end.

