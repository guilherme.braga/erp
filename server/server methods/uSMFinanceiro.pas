unit uSMFinanceiro;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect,
  uPlanoContaServ, uContaCorrenteServ, uGBFDQuery, dialogs;

type
  TSMFinanceiro = class(TDSServerModule)
    fdqCentroResultado: TgbFDQuery;
    dspCentroResultado: TDataSetProvider;
    dspContaAnalise: TDataSetProvider;
    fdqContaAnalise: TgbFDQuery;
    dspContaCorrente: TDataSetProvider;
    fdqContaCorrente: TgbFDQuery;
    dspContaCorrenteMovimento: TDataSetProvider;
    fdqContaCorrenteMovimento: TgbFDQuery;
    dspPlanoConta: TDataSetProvider;
    fdqPlanoConta: TgbFDQuery;
    fdqPlanoContaItem: TgbFDQuery;
    fdqGrupoCentroResultado: TgbFDQuery;
    dspGrupoCentroResultado: TDataSetProvider;
    fdqCentroResultadoID: TFDAutoIncField;
    fdqCentroResultadoDESCRICAO: TStringField;
    fdqCentroResultadoOBSERVACAO: TBlobField;
    fdqCentroResultadoID_GRUPO_CENTRO_RESULTADO: TIntegerField;
    fdqContaAnaliseID: TFDAutoIncField;
    fdqContaAnaliseDESCRICAO: TStringField;
    fdqContaAnaliseSEQUENCIA: TStringField;
    fdqContaAnaliseNIVEL: TIntegerField;
    fdqContaAnaliseOBSERVACAO: TBlobField;
    fdqContaAnaliseBO_ATIVO: TStringField;
    fdqContaAnaliseBO_EXIBIR_RESUMO_CONTA: TStringField;
    fdqContaCorrenteID: TFDAutoIncField;
    fdqContaCorrenteDESCRICAO: TStringField;
    fdqContaCorrenteBO_ATIVO: TStringField;
    fdqContaCorrenteOBSERVACAO: TBlobField;
    fdqContaCorrenteDH_CADASTRO: TDateTimeField;
    fdqContaCorrenteMovimentoDESCRICAO: TStringField;
    fdqContaCorrenteMovimentoDH_CADASTRO: TDateTimeField;
    fdqContaCorrenteMovimentoVL_MOVIMENTO: TFMTBCDField;
    fdqContaCorrenteMovimentoTP_MOVIMENTO: TStringField;
    fdqContaCorrenteMovimentoBO_CONCILIADO: TStringField;
    fdqContaCorrenteMovimentoOBSERVACAO: TBlobField;
    fdqContaCorrenteMovimentoDT_EMISSAO: TDateField;
    fdqContaCorrenteMovimentoDT_MOVIMENTO: TDateField;
    fdqContaCorrenteMovimentoVL_SALDO_CONCILIADO: TFMTBCDField;
    fdqContaCorrenteMovimentoVL_SALDO_GERAL: TFMTBCDField;
    fdqContaCorrenteMovimentoDOCUMENTO: TStringField;
    fdqContaCorrenteMovimentoID_CONTA_CORRENTE: TIntegerField;
    fdqContaCorrenteMovimentoID_CONTA_ANALISE: TIntegerField;
    fdqContaCorrenteMovimentoID_CENTRO_RESULTADO: TIntegerField;
    dsPlanoConta: TDataSource;
    fdqGrupoCentroResultadoID: TFDAutoIncField;
    fdqGrupoCentroResultadoDESCRICAO: TStringField;
    fdqGrupoCentroResultadoOBSERVACAO: TStringField;
    fdqPlanoContaID: TFDAutoIncField;
    fdqPlanoContaID_CENTRO_RESULTADO: TIntegerField;
    fdqPlanoContaItemID: TFDAutoIncField;
    fdqPlanoContaItemID_CONTA_ANALISE: TIntegerField;
    fdqPlanoContaItemID_PLANO_CONTA: TIntegerField;
    fdqConsulta: TgbFDQuery;
    fdqCentroResultadoJOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO: TStringField;
    fdqPlanoContaJOIN_CENTRO_RESULTADO: TStringField;
    fdqContaCorrenteMovimentoID_MOVIMENTO_ORIGEM: TIntegerField;
    fdqContaCorrenteMovimentoID_MOVIMENTO_DESTINO: TIntegerField;
    fdqContaCorrenteMovimentoBO_MOVIMENTO_ORIGEM: TStringField;
    fdqContaCorrenteMovimentoBO_MOVIMENTO_DESTINO: TStringField;
    fdqContaCorrenteMovimentoBO_TRANSFERENCIA: TStringField;
    fdqContaCorrenteMovimentoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqContaCorrenteMovimentoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqContaCorrenteMovimentoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqContaCorrenteMovimentoID: TFDAutoIncField;
    fdqContaCorrenteMovimentoDH_CONCILIADO: TDateTimeField;
    fdqContaCorrenteMovimentoID_CONTA_CORRENTE_DESTINO: TIntegerField;
    fdqPlanoPagamento: TgbFDQuery;
    dspPlanoPagamento: TDataSetProvider;
    fdqPlanoPagamentoID: TFDAutoIncField;
    fdqPlanoPagamentoDESCRICAO: TStringField;
    fdqPlanoPagamentoQT_PARCELA: TIntegerField;
    fdqPlanoPagamentoPRAZO: TStringField;
    fdqFormaPagamento: TgbFDQuery;
    dspFormaPagamento: TDataSetProvider;
    fdqFormaPagamentoID: TFDAutoIncField;
    fdqFormaPagamentoDESCRICAO: TStringField;
    fdqPlanoPagamentoTIPO: TStringField;
    fdqPlanoPagamentoDIA_PARCELA: TIntegerField;
    fdqContaCorrenteMovimentoID_CHAVE_PROCESSO: TIntegerField;
    fdqContaCorrenteMovimentoID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqContaCorrenteMovimentoTP_DOCUMENTO_ORIGEM: TStringField;
    fdqContaCorrenteMovimentoPARCELAMENTO: TStringField;
    fdqContaCorrenteMovimentoDT_COMPETENCIA: TDateField;
    fdqContaCorrenteMovimentoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqTipoQuitacao: TgbFDQuery;
    dspTipoQuitacao: TDataSetProvider;
    fdqTipoQuitacaoID: TFDAutoIncField;
    fdqTipoQuitacaoDESCRICAO: TStringField;
    fdqTipoQuitacaoTIPO: TStringField;
    fdqTipoQuitacaoBO_ATIVO: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_SACADO: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_DOC_FEDERAL: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_BANCO: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_DT_EMISSAO: TDateField;
    fdqContaCorrenteMovimentoCHEQUE_AGENCIA: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_CONTA_CORRENTE: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_NUMERO: TIntegerField;
    fdqContaCorrenteMovimentoID_OPERADORA_CARTAO: TIntegerField;
    fdqContaCorrenteMovimentoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqContaCorrenteMovimentoID_FILIAL: TIntegerField;
    fdqFormaPagamentoID_TIPO_QUITACAO: TIntegerField;
    fdqFormaPagamentoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    fdqFormaPagamentoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    fdqContaCorrenteMovimentoBO_CHEQUE: TStringField;
    fdqPlanoPagamentoBO_ATIVO: TStringField;
    fdqContaCorrenteMovimentoBO_CHEQUE_PROPRIO: TStringField;
    fdqContaCorrenteMovimentoBO_CHEQUE_TERCEIRO: TStringField;
    fdqContaCorrenteMovimentoCHEQUE_DT_VENCIMENTO: TDateField;
    fdqContaCorrenteMovimentoBO_PROCESSO_AMORTIZADO: TStringField;
    fdqContaCorrenteBO_VISUALIZA_FLUXO_CAIXA: TStringField;
    fdqCentroResultadoBO_EXIBIR_EXTRATO_PLANO_CONTA: TStringField;
    procedure fdqContaCorrenteMovimentoAfterPost(DataSet: TDataSet);
  private
        { Private declarations }
  public
    {TContaCorrente}
    function GetContaCorrente(const AAtivo: boolean): TFDJSONDataSets;
    procedure AtualizarSaldoMovimentacao(const AIdContaCorrente: Integer);
    function RealizarTransferencia(const AIdMovimentoOrigem,
      AIdContaCorrenteDestino: Integer): Integer;
    procedure ConciliarRegistro(const AIdMovimentacao: Integer);
    procedure DesconciliarRegistro(const AIdMovimentacao: Integer);
    function RegistroEstaConciliado(const AIdMovimentacao: Integer): boolean;
    function GerarMovimentoContaCorrente(AValue: String): Boolean;
    function RemoverMovimentoContaCorrente(AValue: Integer): Boolean;
    function GerarDocumento(AValue: String): Boolean;
    function GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
      const ADtMovimento: String): Double;

    {TPlanoConta}
    function GetIDNivel(const ANivel: Integer;ASequencia: String): Integer;
    function GetPlanoContaCompleto: TFDJSONDataSets;
    function GetPlanoContaPorCentroResultado(AIdCentroResultado: Integer): TFDJSONDataSets;
    procedure SetIDPlanoContaAssociado;
    function GetDescricaoCentroResultado(const AID: Integer): String;
    function GetDescricaoContaAnalise(const AID: Integer): String;

    {TExtratoPlanoConta}
    function GetCentroResultados: TFDJSONDataSets;
    function GetPrimeiroNivel(const ANivel: Integer; const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
    function GetNivelAninhado(const ANivel: Integer; const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
    function GetMovimentacao(const ACentrosResultados, APlanosContas, AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal: String): TFDJSONDataSets;
    function GetContasCorrentes(const ACentrosResultados, APlanosContas,
    AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
    function GetTotalPorCentroResultado(const ACentrosResultados, ADtInicial, ADtFinal: String): TFDJSONDataSets;
    function GetTotalPorNivel(const ANivel: Integer; const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
    function GetTotalPorContaCorrente(const ACentrosResultados, AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
    function BuscarValorPrevisao(const ANivel: Integer; const APeriodoPrevisaoInicial,
      APeriodoPrevisaoFinal: String; const AIdCentroResultado: Integer;
      const AIdContaAnalise: Integer): Double;
    procedure SalvarPrevisao(AIdCentroResultado: Integer; AIdContaAnalise: Integer;
      APeriodo: String; AValor: Double);

    {TPlanoPagamento}
    function GetPlanoPagamento(AIdPlanoPagamento: Integer): TFDJSONDataSets;
    function GetDescricaoPlanoPagamento(const AID: Integer): String;

    {TFormaPagamento}
    function GetDescricaoFormaPagamento(const AID: Integer): String;
    function GetFormaPagamento(const AID: Integer): String;
    function GetCodigosFormaPagamentoCartao: String;
    function FormaPagamentoCartao(const AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCartaoDebito(const AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCartaoCredito(const AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoDinheiro(const AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCheque(const AIdFormaPagamento: Integer): Boolean;

  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uPlanoPagamentoServ, uFormaPagamentoServ;

{$R *.dfm}

{ TSMFinanceiro }

procedure TSMFinanceiro.AtualizarSaldoMovimentacao(
  const AIdContaCorrente: Integer);
begin
  TContaCorrenteServ.AtualizarSaldoMovimentacao(AidContaCorrente);
end;

function TSMFinanceiro.BuscarValorPrevisao(const ANivel: Integer;
  const APeriodoPrevisaoInicial, APeriodoPrevisaoFinal: String;
  const AIdCentroResultado: Integer; const AIdContaAnalise: Integer): Double;
begin
  result := TExtratoPlanoContaServ.BuscarValorPrevisao(ANivel, APeriodoPrevisaoInicial,
    APeriodoPrevisaoFinal, AIdCentroResultado, AIdContaAnalise);
end;

procedure TSMFinanceiro.ConciliarRegistro(const AIdMovimentacao: Integer);
begin
  TContaCorrenteServ.ConciliarRegistro(AIdMovimentacao);
end;

procedure TSMFinanceiro.DesconciliarRegistro(const AIdMovimentacao: Integer);
begin
  TContaCorrenteServ.DesconciliarRegistro(AIdMovimentacao);
end;

procedure TSMFinanceiro.fdqContaCorrenteMovimentoAfterPost(DataSet: TDataSet);
begin
  if TContaCorrenteServ.IsTransferencia(fdqContaCorrenteMovimento) then
    TContaCorrenteServ.RealizarTransferencia(fdqContaCorrenteMovimentoID.AsInteger,
      fdqContaCorrenteMovimentoID_CONTA_CORRENTE_DESTINO.AsInteger);
end;

function TSMFinanceiro.FormaPagamentoCartao(const AIdFormaPagamento: Integer): Boolean;
begin
  result := TFormaPagamentoServ.FormaPagamentoCartaoCredito(AIdFormaPagamento) or
    TFormaPagamentoServ.FormaPagamentoCartaoDebito(AIdFormaPagamento);
end;

function TSMFinanceiro.FormaPagamentoCartaoCredito(const AIdFormaPagamento: Integer): Boolean;
begin
  result := TFormaPagamentoServ.FormaPagamentoCartaoCredito(AIdFormaPagamento);
end;

function TSMFinanceiro.FormaPagamentoCartaoDebito(const AIdFormaPagamento: Integer): Boolean;
begin
  result := TFormaPagamentoServ.FormaPagamentoCartaoDebito(AIdFormaPagamento);
end;

function TSMFinanceiro.FormaPagamentoCheque(const AIdFormaPagamento: Integer): Boolean;
begin
  result := TFormaPagamentoServ.FormaPagamentoCheque(AIdFormaPagamento);
end;

function TSMFinanceiro.FormaPagamentoDinheiro(const AIdFormaPagamento: Integer): Boolean;
begin
  result := TFormaPagamentoServ.FormaPagamentoDinheiro(AIdFormaPagamento);
end;

function TSMFinanceiro.GetCentroResultados: TFDJSONDataSets;
begin
  Result := TExtratoPlanoContaServ.GetCentroResultados;
end;

function TSMFinanceiro.GetCodigosFormaPagamentoCartao: String;
begin
  result := TFormaPagamentoServ.GetCodigosFormaPagamentoCartao;
end;

function TSMFinanceiro.GetContaCorrente(const AAtivo: boolean): TFDJSONDataSets;
begin
  result := TFDJSONDataSets.Create;
  TContaCorrenteServ.SetContaCorrente(fdqConsulta, AAtivo);
  TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
end;

function TSMFinanceiro.GetContasCorrentes(const ACentrosResultados, APlanosContas,
    AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
begin
  result := TExtratoPlanoContaServ.GetContasCorrentes(ACentrosResultados,
    APlanosContas, AListaCamposPeriodo, ADtInicial, ADtFinal);
end;

function TSMFinanceiro.GetDescricaoCentroResultado(const AID: Integer): String;
begin
  result := TPlanoContaServ.GetDescricaoCentroResultado(AID);
end;

function TSMFinanceiro.GetDescricaoContaAnalise(const AID: Integer): String;
begin
  result := TPlanoContaServ.GetDescricaoContaAnalise(AID);
end;

function TSMFinanceiro.GetDescricaoFormaPagamento(const AID: Integer): String;
begin
  result := TFormaPagamentoServ.GetDescricaoFormaPagamento(AID);
end;

function TSMFinanceiro.GetDescricaoPlanoPagamento(const AID: Integer): String;
begin
  result := TPlanoPagamentoServ.GetDescricaoPlanoPagamento(AID);
end;

function TSMFinanceiro.GetFormaPagamento(const AID: Integer): String;
begin
  result := TFormaPagamentoServ.GetFormaPagamento(AID);
end;

function TSMFinanceiro.GetIDNivel(const ANivel: Integer; ASequencia: String): Integer;
begin
  result := TPlanoContaServ.GetIDNivel(Anivel, ASequencia);
end;

function TSMFinanceiro.GetMovimentacao(const ACentrosResultados, APlanosContas,
  AContasCorrentes, AListaCamposPeriodo, ADtInicial,
  ADtFinal: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetMovimentacao(ACentrosResultados, APlanosContas,
    AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal);
end;

function TSMFinanceiro.GetNivelAninhado(const ANivel: Integer;
  const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetNivelAninhado(ANivel, ACentrosResultados, APlanosContas, AListaCamposPeriodo);
end;

function TSMFinanceiro.GetPlanoContaCompleto: TFDJSONDataSets;
begin
  result := TFDJSONDataSets.Create;
  TPlanoContaServ.SetPlanoContaCompleto(fdqConsulta);
  TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
end;

function TSMFinanceiro.GetPlanoContaPorCentroResultado(AIdCentroResultado: Integer): TFDJSONDataSets;
begin
  result := TFDJSONDataSets.Create;
  TPlanoContaServ.SetPlanoContaPorCentroResultado(fdqConsulta, AIdCentroResultado);
  TFDJSONDataSetsWriter.ListAdd(Result, fdqConsulta);
end;

function TSMFinanceiro.GetPlanoPagamento(
  AIdPlanoPagamento: Integer): TFDJSONDataSets;
begin
  result := TPlanoPagamentoServ.GetPlanoPagamento(AIdPlanoPagamento);
end;

function TSMFinanceiro.GetPrimeiroNivel(const ANivel: Integer;
  const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetPrimeiroNivel(ANivel, ACentrosResultados, AListaCamposPeriodo);
end;

function TSMFinanceiro.GetTotalPorCentroResultado(const ACentrosResultados,
  ADtInicial, ADtFinal: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetTotalPorCentroResultado(ACentrosResultados,
    ADtInicial, ADtFinal);
end;

function TSMFinanceiro.GetTotalPorContaCorrente(const ACentrosResultados,
  AContasAnalises, ADtInicial, ADtFinal: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetTotalPorContaCorrente(ACentrosResultados,
    AContasAnalises, ADtInicial, ADtFinal);
end;

function TSMFinanceiro.GetTotalPorNivel(const ANivel: Integer;
  const ACentrosResultados, AContasAnalises, ADtInicial,
  ADtFinal: String): TFDJSONDataSets;
begin
  result := TExtratoPlanoContaServ.GetTotalPorNivel(ANivel, ACentrosResultados,
    AContasAnalises, ADtInicial, ADtFinal);
end;

function TSMFinanceiro.RealizarTransferencia(const AIdMovimentoOrigem,
  AIdContaCorrenteDestino: Integer): Integer;
begin
  TContaCorrenteServ.RealizarTransferencia(AIdMovimentoOrigem, AIdContaCorrenteDestino);
end;

function TSMFinanceiro.RegistroEstaConciliado(
  const AIdMovimentacao: Integer): boolean;
begin
  result := TContaCorrenteServ.RegistroEstaConciliado(AIdMovimentacao);
end;

function TSMFinanceiro.GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
  const ADtMovimento: String): Double;
begin
  result := TContaCorrenteServ.GetSaldoAnteriorContaCorrente(AIdContaCorrente, ADtMovimento);
end;

procedure TSMFinanceiro.SalvarPrevisao(AIdCentroResultado: Integer; AIdContaAnalise: Integer;
  APeriodo: String; AValor: Double);
begin
  TExtratoPlanoContaServ.SalvarPrevisao(AIdCentroResultado, AIdContaAnalise, APeriodo, AValor);
end;

procedure TSMFinanceiro.SetIDPlanoContaAssociado;
begin
  TPlanoContaServ.SetIDPlanoContaAssociado;
end;

function TSMFinanceiro.GerarMovimentoContaCorrente(AValue : String) : Boolean;
begin
  Result := TContaCorrenteServ.GerarMovimentoContaCorrente(AValue);
end;

function TSMFinanceiro.RemoverMovimentoContaCorrente(AValue: Integer) : Boolean;
begin
  Result := TContaCorrenteServ.RemoverMovimentoContaCorrente(AValue);
end;

function TSMFinanceiro.GerarDocumento(AValue: String) : Boolean;
begin
  Result := TContaCorrenteServ.GerarDocumento(AValue);
end;

end.

