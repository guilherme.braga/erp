unit uSMPessoa;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.Provider, dialogs, uGBFDQuery,
  Data.FireDACJSONReflect, Datasnap.DBClient, uPessoaProxy;

type
  TSMPessoa = class(TDSServerModule)
    fdqPessoaUsuarioDesignGrid: TgbFDQuery;
    dspPessoaUsuarioDesignGrid: TDataSetProvider;
    fdqPessoa: TgbFDQuery;
    dspPessoa: TDataSetProvider;
    fdqPessoaEndereco: TgbFDQuery;
    fdqPessoaContato: TgbFDQuery;
    fdqPessoaUsuarioDesignControl: TgbFDQuery;
    dspPessoaUsuarioDesignControl: TDataSetProvider;
    fdqPessoaUsuarioPermAcoes: TgbFDQuery;
    dspPessoaUsuarioPermAcoes: TDataSetProvider;
    fdqPessoaUsuarioPermAcoesEx: TgbFDQuery;
    dspPessoaUsuarioPermAcoesEx: TDataSetProvider;
    fdqPessoaUsuarioPermForm: TgbFDQuery;
    dspPessoaUsuarioPermForm: TDataSetProvider;
    fdqPessoaEnderecoTIPO: TStringField;
    fdqPessoaEnderecoLOGRADOURO: TStringField;
    fdqPessoaEnderecoCOMPLEMENTO: TStringField;
    fdqPessoaEnderecoOBSERVACAO: TBlobField;
    fdqPessoaEnderecoID_PESSOA: TIntegerField;
    fdqPessoaContatoTIPO: TStringField;
    fdqPessoaContatoID_PESSOA: TIntegerField;
    fdqPessoaUsuarioDesignGridID: TFDAutoIncField;
    fdqPessoaUsuarioDesignGridFORMULARIO: TStringField;
    fdqPessoaUsuarioDesignGridARQUIVO_GRID: TBlobField;
    fdqPessoaUsuarioDesignGridARQUIVO_FIELD_CAPTION: TBlobField;
    fdqPessoaUsuarioDesignGridID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaUsuarioPermAcoesID: TFDAutoIncField;
    fdqPessoaUsuarioPermAcoesFORMULARIO: TStringField;
    fdqPessoaUsuarioPermAcoesARQUIVO_GRID: TBlobField;
    fdqPessoaUsuarioPermAcoesARQUIVO_FIELD_CAPTION: TBlobField;
    fdqPessoaUsuarioPermAcoesID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaUsuarioPermAcoesExID: TFDAutoIncField;
    fdqPessoaUsuarioPermAcoesExFORMULARIO: TStringField;
    fdqPessoaUsuarioPermAcoesExARQUIVO_GRID: TBlobField;
    fdqPessoaUsuarioPermAcoesExARQUIVO_FIELD_CAPTION: TBlobField;
    fdqPessoaUsuarioPermAcoesExID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaUsuarioPermFormID: TFDAutoIncField;
    fdqPessoaUsuarioPermFormFORMULARIO: TStringField;
    fdqPessoaUsuarioPermFormARQUIVO_GRID: TBlobField;
    fdqPessoaUsuarioPermFormARQUIVO_FIELD_CAPTION: TBlobField;
    fdqPessoaUsuarioPermFormID_PESSOA_USUARIO: TIntegerField;
    dsPessoa: TDataSource;
    fdqPessoaContatoOBSERVACAO: TStringField;
    fdqPessoaContatoCONTATO: TStringField;
    fdqPessoaEnderecoID: TFDAutoIncField;
    fdqPessoaContatoID: TFDAutoIncField;
    fdqGrupoPessoa: TgbFDQuery;
    dspGrupoPessoa: TDataSetProvider;
    fdqGrupoPessoaID: TFDAutoIncField;
    fdqGrupoPessoaDESCRICAO: TStringField;
    fdqPessoaID: TFDAutoIncField;
    fdqPessoaDH_CADASTRO: TDateTimeField;
    fdqPessoaTP_PESSOA: TStringField;
    fdqPessoaBO_CLIENTE: TStringField;
    fdqPessoaBO_FORNECEDOR: TStringField;
    fdqPessoaBO_ATIVO: TStringField;
    fdqPessoaBO_TRANSPORTADORA: TStringField;
    fdqPessoaID_GRUPO_PESSOA: TIntegerField;
    fdqPessoaOBSERVACAO: TBlobField;
    fdqPessoaRAZAO_SOCIAL: TStringField;
    fdqPessoaNOME: TStringField;
    fdqPessoaDOC_FEDERAL: TStringField;
    fdqPessoaDOC_ESTADUAL: TStringField;
    fdqPessoaDT_NASCIMENTO: TDateField;
    fdqPessoaJOIN_DESCRICAO_GRUPO_PESSOA: TStringField;
    fdqPessoaEnderecoNUMERO: TStringField;
    fdqPessoaUsuario: TgbFDQuery;
    dspPessoaUsuario: TDataSetProvider;
    fdqPessoaUsuarioID: TFDAutoIncField;
    fdqPessoaUsuarioUSUARIO: TStringField;
    fdqPessoaUsuarioSENHA: TStringField;
    fdqPessoaUsuarioDT_EXPIRACAO: TDateField;
    fdqPessoaUsuarioUSUARIO_EXPIRADO: TIntegerField;
    fdqPessoaUsuarioUSUARIO_DIAS_EXPIRAR_SENHA: TIntegerField;
    fdqPessoaUsuarioEMAIL: TStringField;
    fdqPessoaUsuarioPRIVILEGIADO: TIntegerField;
    fdqPessoaUsuarioSENHA_KEY: TStringField;
    fdqPessoaUsuarioBO_ATIVO: TStringField;
    fdqPessoaUsuarioID_USUARIO_PERFIL: TIntegerField;
    fdqPessoaUsuarioUSUARIO_TIPO: TStringField;
    fdqPessoaUsuarioESTACAO_ID: TIntegerField;
    fdqPessoaUsuarioDesignControlID: TFDAutoIncField;
    fdqPessoaUsuarioDesignControlFORMULARIO: TStringField;
    fdqPessoaUsuarioDesignControlARQUIVO_CONTROL: TBlobField;
    fdqPessoaUsuarioDesignControlARQUIVO_INVISIBLE: TBlobField;
    fdqPessoaUsuarioDesignControlID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaUsuarioDesignControlARQUIVO_VALORES_PADRAO: TBlobField;
    fdqPessoaUsuarioDesignControlARQUIVO_FILTRO_PADRAO: TBlobField;
    fdqPessoaNOME_PAI: TStringField;
    fdqPessoaNOME_MAE: TStringField;
    fdqPessoaORGAO_EMISSOR: TStringField;
    fdqPessoaSEXO: TStringField;
    fdqPessoaEMPRESA: TStringField;
    fdqPessoaENDERECO_EMPRESA: TStringField;
    fdqPessoaCEP_EMPRESA: TStringField;
    fdqPessoaCNPJ_EMPRESA: TStringField;
    fdqPessoaBAIRRO_EMPRESA: TStringField;
    fdqPessoaDT_ADMISSAO: TDateField;
    fdqPessoaTELEFONE_EMPRESA: TStringField;
    fdqPessoaRAMAL_EMPRESA: TStringField;
    fdqPessoaRENDA_MENSAL: TFMTBCDField;
    fdqPessoaSUFRAMA: TStringField;
    fdqPessoaDOC_MUNICIPAL: TStringField;
    fdqPessoaID_NATURALIDADE: TIntegerField;
    fdqPessoaID_CIDADE_EMPRESA: TIntegerField;
    fdqPessoaID_UF_EMISSOR: TIntegerField;
    fdqPessoaConjuge: TgbFDQuery;
    fdqPessoaConjugeID: TFDAutoIncField;
    fdqPessoaConjugeNOME: TStringField;
    fdqPessoaConjugeCPF: TStringField;
    fdqPessoaConjugeSEXO: TStringField;
    fdqPessoaConjugeRG: TStringField;
    fdqPessoaConjugeORGAO_EMISSOR: TStringField;
    fdqPessoaConjugeDT_NASCIMENTO: TDateField;
    fdqPessoaConjugeEMPRESA: TStringField;
    fdqPessoaConjugeENDERECO_EMPRESA: TStringField;
    fdqPessoaConjugeCEP_EMPRESA: TStringField;
    fdqPessoaConjugeCNPJ_EMPRESA: TStringField;
    fdqPessoaConjugeBAIRRO_EMPRESA: TStringField;
    fdqPessoaConjugeDT_ADMISSAO: TDateField;
    fdqPessoaConjugeRENDA_MENSAL: TFMTBCDField;
    fdqPessoaConjugeTELEFONE_EMPRESA: TStringField;
    fdqPessoaConjugeRAMAL_EMPRESA: TStringField;
    fdqPessoaConjugeID_NATURALIDADE: TIntegerField;
    fdqPessoaConjugeID_CIDADE_EMPRESA: TIntegerField;
    fdqPessoaConjugeID_UF_EMISSOR: TIntegerField;
    fdqPessoaConjugeID_PESSOA: TIntegerField;
    fdqPessoaJOIN_UF_ESTADO: TStringField;
    fdqPessoaJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField;
    fdqPessoaJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField;
    fdqPessoaConjugeJOIN_UF_ESTADO: TStringField;
    fdqPessoaConjugeJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField;
    fdqPessoaConjugeJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField;
    fdqOcupacao: TgbFDQuery;
    dspOcupacao: TDataSetProvider;
    fdqOcupacaoID: TFDAutoIncField;
    fdqOcupacaoDESCRICAO: TStringField;
    fdqOcupacaoCODIGO: TStringField;
    fdqAreaAtuacao: TgbFDQuery;
    dspAreaAtuacao: TDataSetProvider;
    dspClassificacaoEmpresa: TDataSetProvider;
    fdqClassificacaoEmpresa: TgbFDQuery;
    fdqClassificacaoEmpresaID: TFDAutoIncField;
    fdqClassificacaoEmpresaDESCRICAO: TStringField;
    fdqClassificacaoEmpresaVALOR_INICIAL: TFMTBCDField;
    fdqClassificacaoEmpresaVALOR_FINAL: TFMTBCDField;
    fdqAreaAtuacaoID: TFDAutoIncField;
    fdqAreaAtuacaoDESCRICAO: TStringField;
    fdqPessoaID_AREA_ATUACAO: TIntegerField;
    fdqPessoaID_CLASSIFICACAO_EMPRESA: TIntegerField;
    fdqPessoaID_ATIVIDADE_PRINCIPAL: TIntegerField;
    fdqPessoaJOIN_DESCRICAO_CLASSIFICAO_EMPRESA: TStringField;
    fdqPessoaJOIN_DESCRICAO_CNAE: TStringField;
    fdqPessoaESTADO_CIVIL: TStringField;
    fdqPessoaRepresentante: TgbFDQuery;
    fdqPessoaSetor: TgbFDQuery;
    fdqPessoaRepresentanteID: TFDAutoIncField;
    fdqPessoaRepresentanteNOME: TStringField;
    fdqPessoaRepresentanteCPF: TStringField;
    fdqPessoaRepresentanteRG: TStringField;
    fdqPessoaRepresentanteDT_NASCIMENTO: TDateField;
    fdqPessoaRepresentanteESTADO_CIVIL: TStringField;
    fdqPessoaRepresentanteID_PESSOA: TIntegerField;
    fdqPessoaSetorID: TFDAutoIncField;
    fdqPessoaSetorQUANTIDADE_FUNCIONARIOS: TIntegerField;
    fdqPessoaSetorID_PESSOA: TIntegerField;
    fdqPessoaAtividadeSecundaria: TgbFDQuery;
    fdqPessoaAtividadeSecundariaID: TFDAutoIncField;
    fdqPessoaAtividadeSecundariaID_PESSOA: TIntegerField;
    fdqPessoaAtividadeSecundariaID_CNAE: TIntegerField;
    fdqPessoaJOIN_SEQUENCIA_CNAE: TStringField;
    fdqPessoaConjugeNUMERO_EMPRESA: TIntegerField;
    fdqPessoaNUMERO_EMPRESA: TIntegerField;
    fdqSetorComercial: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    dspSetorComercial: TDataSetProvider;
    fdqSetorComercialDESCRICAO: TStringField;
    fdqSetorComercialOBSERVACAO: TBlobField;
    fdqPessoaSetorID_SETOR_COMERCIAL: TIntegerField;
    fdqPessoaSetorJOIN_DESCRICAO_SETOR_COMERCIAL: TStringField;
    fdqUserControl: TgbFDQuery;
    dspUserControl: TDataSetProvider;
    fdqPessoaEnderecoCEP: TStringField;
    fdqPessoaEnderecoBAIRRO: TStringField;
    fdqPessoaEnderecoID_CIDADE: TIntegerField;
    fdqPessoaEnderecoJOIN_DESCRICAO_CIDADE: TStringField;
    fdqPessoaAtividadeSecundariaJOIN_DESCRICAO_CNAE: TStringField;
    fdqPessoaAtividadeSecundariaJOIN_SEQUENCIA_CNAE: TStringField;
    fdqPessoaUsuarioDesignControlARQUIVO_PARAMETROS: TBlobField;
    fdqPessoaJOIN_UF_ESTADO_NATURALIDADE: TStringField;
    fdqPessoaTEMPO_TRABALHO_EMPRESA: TStringField;
    fdqPessoaQUANTIDADE_FILHOS: TIntegerField;
    fdqPessoaTIPO_RESIDENCIA: TStringField;
    fdqPessoaRENDA_EXTRA: TStringField;
    fdqPessoaVL_RENDA_EXTRA: TFMTBCDField;
    fdqPessoaVL_LIMITE: TFMTBCDField;
    fdqPessoaConjugeTEMPO_TRABALHO: TStringField;
    fdqPessoaConjugeOCUPACAO_EMPRESA: TStringField;
    fdqPessoaOCUPACAO: TStringField;
    fdqPessoaJOIN_DESCRICAO_AREA_ATUACAO: TStringField;
    fdqPessoaDT_ASSOCIACAO: TDateField;
    fdqPessoaID_ZONEAMENTO: TIntegerField;
    fdqPessoaJOIN_DESCRICAO_ZONEAMENTO: TStringField;
    fdqPessoaCRT: TIntegerField;
    fdqPessoaREGIME_TRIBUTARIO: TIntegerField;
    fdqPessoaBO_CONTRIBUINTE_ICMS: TStringField;
    fdqPessoaBO_CONTRIBUINTE_IPI: TStringField;
    fdqPessoaBO_CONTRIBUINTE: TStringField;
    fdqPessoaID_REGIME_ESPECIAL: TIntegerField;
    fdqPessoaJOIN_DESCRICAO_REGIME_ESPECIAL: TStringField;
    fdqPessoaTIPO_EMPRESA: TStringField;
    fdqPessoaUsuarioDesignGrid_ConsultaFormulario: TgbFDQuery;
    FDAutoIncField2: TFDAutoIncField;
    StringField1: TStringField;
    BlobField1: TBlobField;
    BlobField2: TBlobField;
    IntegerField1: TIntegerField;
    dspPessoaUsuarioDesignGrid_ConsultaFormulario: TDataSetProvider;
    fdqPessoaCredito: TgbFDQuery;
    fdqPessoaCreditoID: TFDAutoIncField;
    fdqPessoaCreditoVL_CREDITO: TFMTBCDField;
    fdqPessoaCreditoID_PESSOA: TIntegerField;
    fdqPessoaRestricao: TgbFDQuery;
    fdqPessoaRestricaoID: TFDAutoIncField;
    fdqPessoaRestricaoDESCRICAO: TStringField;
    fdqPessoaRestricaoDH_INCLUSAO: TDateTimeField;
    fdqPessoaRestricaoBO_ATIVO: TStringField;
    fdqPessoaRestricaoID_PESSOA: TIntegerField;
    fdqPessoaRestricaoID_PESSOA_USUARIO: TIntegerField;
    fdqPessoaRestricaoID_TIPO_RESTRICAO: TIntegerField;
    fdqPessoaRestricaoJOIN_DESCRICAO_RESTRICAO: TStringField;
    fdqPessoaContatoBO_EMAIL_ENVIO_DOCUMENTO_FISCAL: TStringField;
  private

  public
    function GetNomePessoa(const AIDPessoa: Integer): String;
    function GetPessoaEndereco(const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
    function GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
    function GetPessoa(const AIdPessoa: Integer): String;
    function GerarPessoa(const APessoa: String): Integer;
    function GetIdUsuario(const AUsuario: String): Integer;

    procedure SetClienteConsumidorFinal(const AIdPessoa: Integer);
    function VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
    function ValidaCnpjCeiCpf(Numero: String): Boolean;
    function PossuiCPFValido(const AIdPessoa: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uFrmServer, uPessoaServ, uUsuarioServer, rest.JSON, uFastReport, uFactoryQuery;

{$R *.dfm}

{ TSMPessoa }

function TSMPessoa.GerarPessoa(const APessoa: String): Integer;
begin
  result := TPessoaServ.GerarPessoa(TJSON.JsonToObject<TPessoaProxy>(APessoa));
end;

function TSMPessoa.GetIdUsuario(const AUsuario: String): Integer;
begin
  result := TUsuarioServ.GetIdUsuario(AUsuario);
end;

function TSMPessoa.GetNomePessoa(const AIDPessoa: Integer): String;
begin
  result := TPessoaServ.GetNomePessoa(AIDPessoa);
end;

function TSMPessoa.GetPessoa(const AIdPessoa: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TPessoaServ.GetPessoa(AIdPessoa));
end;

function TSMPessoa.GetPessoaEndereco(const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
begin
  result := TPessoaEnderecoServ.GetPessoaEndereco(AIdPessoaEndereco);
end;

function TSMPessoa.GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
begin
  result := TUsuarioServ.GetUsuario(AIdUsuario);
end;

function TSMPessoa.PossuiCPFValido(const AIdPessoa: Integer): Boolean;
begin
  result := TPessoaServ.PossuiCPFValido(AIdPessoa);
end;

procedure TSMPessoa.SetClienteConsumidorFinal(const AIdPessoa: Integer);
begin
  TPessoaServ.SetClienteConsumidorFinal(AIdPessoa);
end;

function TSMPessoa.ValidaCnpjCeiCpf(Numero: String): Boolean;
begin
  result := TPessoaServ.ValidaCnpjCeiCpf(Numero);
end;

function TSMPessoa.VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
begin
  result := TPessoaServ.VerificarConsumidorFinal(AIdPessoa);
end;

end.

