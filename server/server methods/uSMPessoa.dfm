object SMPessoa: TSMPessoa
  OldCreateOrder = False
  Height = 463
  Width = 528
  object fdqPessoaUsuarioDesignGrid: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_GRID WHERE '
      'id_pessoa_usuario = :id_pessoa_usuario and'
      'formulario = :formulario')
    Left = 272
    Top = 56
    ParamData = <
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    object fdqPessoaUsuarioDesignGridID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqPessoaUsuarioDesignGridFORMULARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 255
    end
    object fdqPessoaUsuarioDesignGridARQUIVO_GRID: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object fdqPessoaUsuarioDesignGridARQUIVO_FIELD_CAPTION: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object fdqPessoaUsuarioDesignGridID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspPessoaUsuarioDesignGrid: TDataSetProvider
    DataSet = fdqPessoaUsuarioDesignGrid
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 304
    Top = 56
  end
  object fdqPessoa: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT p.*'
      '      ,gp.descricao AS JOIN_DESCRICAO_GRUPO_PESSOA'
      '      ,e.uf AS JOIN_UF_ESTADO'
      '      ,cn.descricao AS JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      
        '      ,(select uf from estado where id = cn.id_estado) as JOIN_U' +
        'F_ESTADO_NATURALIDADE'
      '      ,cempresa.descricao AS JOIN_DESCRICAO_CIDADE_EMPRESA      '
      '      ,at.descricao AS JOIN_DESCRICAO_AREA_ATUACAO'
      '      ,ce.descricao AS JOIN_DESCRICAO_CLASSIFICAO_EMPRESA'
      '      ,cnae.descricao AS JOIN_DESCRICAO_CNAE   '
      '      ,cnae.sequencia AS JOIN_SEQUENCIA_CNAE '
      '      ,z.descricao AS JOIN_DESCRICAO_ZONEAMENTO'
      '      ,re.descricao AS JOIN_DESCRICAO_REGIME_ESPECIAL'
      'FROM PESSOA p'
      ' LEFT JOIN grupo_pessoa gp ON p.id_grupo_pessoa = gp.id'
      ' LEFT JOIN estado e        ON p.id_uf_emissor = e.id'
      ' LEFT JOIN cidade cn       ON p.id_naturalidade = cn.id'
      ' LEFT JOIN cidade cempresa ON p.id_cidade_empresa = cempresa.id'
      ' LEFT JOIN area_atuacao at ON p.id_area_atuacao = at.id'
      
        ' LEFT JOIN classificacao_empresa ce ON p.id_classificacao_empres' +
        'a = ce.id'
      ' LEFT JOIN cnae            ON p.id_atividade_principal = cnae.id'
      ' LEFT JOIN zoneamento z    ON p.id_zoneamento = z.id'
      ' LEFT JOIN regime_especial re ON p.id_regime_especial = re.id '
      'WHERE p.id = :id'
      '')
    Left = 62
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqPessoaID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqPessoaDT_NASCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object fdqPessoaTP_PESSOA: TStringField
      DisplayLabel = 'Tp. Pessoa'
      FieldName = 'TP_PESSOA'
      Origin = 'TP_PESSOA'
      Required = True
      FixedChar = True
      Size = 10
    end
    object fdqPessoaBO_ATIVO: TStringField
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqPessoaBO_CLIENTE: TStringField
      DisplayLabel = 'Cliente?'
      FieldName = 'BO_CLIENTE'
      Origin = 'BO_CLIENTE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqPessoaBO_FORNECEDOR: TStringField
      DisplayLabel = 'Fornecedor?'
      FieldName = 'BO_FORNECEDOR'
      Origin = 'BO_FORNECEDOR'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqPessoaBO_TRANSPORTADORA: TStringField
      DisplayLabel = 'Transportadora?'
      FieldName = 'BO_TRANSPORTADORA'
      Origin = 'BO_TRANSPORTADORA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqPessoaDOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Doc. Federal'
      FieldName = 'DOC_FEDERAL'
      Origin = 'DOC_FEDERAL'
      Size = 14
    end
    object fdqPessoaDOC_ESTADUAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Doc. Estadual'
      FieldName = 'DOC_ESTADUAL'
      Origin = 'DOC_ESTADUAL'
      Size = 25
    end
    object fdqPessoaDOC_MUNICIPAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Inscri'#231#227'o Municipal'
      FieldName = 'DOC_MUNICIPAL'
      Origin = 'DOC_MUNICIPAL'
      Size = 45
    end
    object fdqPessoaID_GRUPO_PESSOA: TIntegerField
      DisplayLabel = 'Gr. Pessoa'
      FieldName = 'ID_GRUPO_PESSOA'
      Origin = 'ID_GRUPO_PESSOA'
    end
    object fdqPessoaJOIN_DESCRICAO_GRUPO_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Grupo Pessoa'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PESSOA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object fdqPessoaRAZAO_SOCIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Raz'#227'o Social'
      FieldName = 'RAZAO_SOCIAL'
      Origin = 'RAZAO_SOCIAL'
      Size = 80
    end
    object fdqPessoaNOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 80
    end
    object fdqPessoaOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqPessoaNOME_PAI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome do Pai'
      FieldName = 'NOME_PAI'
      Origin = 'NOME_PAI'
      Size = 80
    end
    object fdqPessoaNOME_MAE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome da M'#227'e'
      FieldName = 'NOME_MAE'
      Origin = 'NOME_MAE'
      Size = 80
    end
    object fdqPessoaORGAO_EMISSOR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Org'#227'o Emissor'
      FieldName = 'ORGAO_EMISSOR'
      Origin = 'ORGAO_EMISSOR'
      Size = 25
    end
    object fdqPessoaID_UF_EMISSOR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. UF Emissor'
      FieldName = 'ID_UF_EMISSOR'
      Origin = 'ID_UF_EMISSOR'
    end
    object fdqPessoaSEXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sexo'
      FieldName = 'SEXO'
      Origin = 'SEXO'
      Size = 10
    end
    object fdqPessoaEMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome Empresa'
      FieldName = 'EMPRESA'
      Origin = 'EMPRESA'
      Size = 80
    end
    object fdqPessoaENDERECO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Endere'#231'o Empresa'
      FieldName = 'ENDERECO_EMPRESA'
      Origin = 'ENDERECO_EMPRESA'
      Size = 80
    end
    object fdqPessoaNUMERO_EMPRESA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_EMPRESA'
      Origin = 'NUMERO_EMPRESA'
    end
    object fdqPessoaID_CIDADE_EMPRESA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Cidade Empresa'
      FieldName = 'ID_CIDADE_EMPRESA'
      Origin = 'ID_CIDADE_EMPRESA'
    end
    object fdqPessoaCEP_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CEP Empresa'
      FieldName = 'CEP_EMPRESA'
      Origin = 'CEP_EMPRESA'
      Size = 8
    end
    object fdqPessoaCNPJ_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CNPJ Empresa'
      FieldName = 'CNPJ_EMPRESA'
      Origin = 'CNPJ_EMPRESA'
      Size = 14
    end
    object fdqPessoaBAIRRO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bairro Empresa'
      FieldName = 'BAIRRO_EMPRESA'
      Origin = 'BAIRRO_EMPRESA'
      Size = 45
    end
    object fdqPessoaDT_ADMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Admiss'#227'o'
      FieldName = 'DT_ADMISSAO'
      Origin = 'DT_ADMISSAO'
    end
    object fdqPessoaTELEFONE_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Telefone Empresa'
      FieldName = 'TELEFONE_EMPRESA'
      Origin = 'TELEFONE_EMPRESA'
    end
    object fdqPessoaRAMAL_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ramal Empresa'
      FieldName = 'RAMAL_EMPRESA'
      Origin = 'RAMAL_EMPRESA'
      Size = 10
    end
    object fdqPessoaRENDA_MENSAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Renda Mensal'
      FieldName = 'RENDA_MENSAL'
      Origin = 'RENDA_MENSAL'
      Precision = 24
      Size = 9
    end
    object fdqPessoaSUFRAMA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'SUFRAMA'
      Origin = 'SUFRAMA'
      Size = 45
    end
    object fdqPessoaID_NATURALIDADE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Naturalidade'
      FieldName = 'ID_NATURALIDADE'
      Origin = 'ID_NATURALIDADE'
    end
    object fdqPessoaJOIN_UF_ESTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF Estado'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'UF'
      ProviderFlags = []
      Size = 2
    end
    object fdqPessoaJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o Cidade Naturalidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o Cidade Empresa'
      FieldName = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaID_AREA_ATUACAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = #193'rea de Atua'#231#227'o'
      FieldName = 'ID_AREA_ATUACAO'
      Origin = 'ID_AREA_ATUACAO'
    end
    object fdqPessoaID_CLASSIFICACAO_EMPRESA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Classifica'#231#227'o Empresa'
      FieldName = 'ID_CLASSIFICACAO_EMPRESA'
      Origin = 'ID_CLASSIFICACAO_EMPRESA'
    end
    object fdqPessoaID_ATIVIDADE_PRINCIPAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Atividade Principal'
      FieldName = 'ID_ATIVIDADE_PRINCIPAL'
      Origin = 'ID_ATIVIDADE_PRINCIPAL'
    end
    object fdqPessoaJOIN_DESCRICAO_CLASSIFICAO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Classifica'#231#227'o Empresa'
      FieldName = 'JOIN_DESCRICAO_CLASSIFICAO_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqPessoaJOIN_SEQUENCIA_CNAE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia CNAE'
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object fdqPessoaJOIN_DESCRICAO_CNAE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Atividade Principal'
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaESTADO_CIVIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estado Civil'
      FieldName = 'ESTADO_CIVIL'
      Origin = 'ESTADO_CIVIL'
      Size = 10
    end
    object fdqPessoaJOIN_UF_ESTADO_NATURALIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF Naturalidade'
      FieldName = 'JOIN_UF_ESTADO_NATURALIDADE'
      Origin = 'JOIN_UF_ESTADO_NATURALIDADE'
      ProviderFlags = []
      ReadOnly = True
      Size = 2
    end
    object fdqPessoaTEMPO_TRABALHO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tempo de Trabalho'
      FieldName = 'TEMPO_TRABALHO_EMPRESA'
      Origin = 'TEMPO_TRABALHO_EMPRESA'
      Size = 100
    end
    object fdqPessoaQUANTIDADE_FILHOS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Filhos'
      FieldName = 'QUANTIDADE_FILHOS'
      Origin = 'QUANTIDADE_FILHOS'
    end
    object fdqPessoaTIPO_RESIDENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tempo de Resid'#234'ncia'
      FieldName = 'TIPO_RESIDENCIA'
      Origin = 'TIPO_RESIDENCIA'
      Size = 100
    end
    object fdqPessoaRENDA_EXTRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Renda Extra'
      FieldName = 'RENDA_EXTRA'
      Origin = 'RENDA_EXTRA'
      Size = 255
    end
    object fdqPessoaVL_RENDA_EXTRA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Renda Extra'
      FieldName = 'VL_RENDA_EXTRA'
      Origin = 'VL_RENDA_EXTRA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqPessoaVL_LIMITE: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Limite de Cr'#233'dito'
      FieldName = 'VL_LIMITE'
      Origin = 'VL_LIMITE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqPessoaOCUPACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ocupa'#231#227'o'
      FieldName = 'OCUPACAO'
      Origin = 'OCUPACAO'
      Size = 255
    end
    object fdqPessoaJOIN_DESCRICAO_AREA_ATUACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = #193'rea de Atua'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_AREA_ATUACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqPessoaDT_ASSOCIACAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Associa'#231#227'o'
      FieldName = 'DT_ASSOCIACAO'
      Origin = 'DT_ASSOCIACAO'
    end
    object fdqPessoaID_ZONEAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Zoneamento'
      FieldName = 'ID_ZONEAMENTO'
      Origin = 'ID_ZONEAMENTO'
    end
    object fdqPessoaJOIN_DESCRICAO_ZONEAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Zoneamento'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaCRT: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Regime Tribut'#225'rio'
      FieldName = 'CRT'
      Origin = 'CRT'
    end
    object fdqPessoaID_REGIME_ESPECIAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Regime Especial'
      FieldName = 'ID_REGIME_ESPECIAL'
      Origin = 'ID_REGIME_ESPECIAL'
    end
    object fdqPessoaREGIME_TRIBUTARIO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Tribut'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO'
      Origin = 'REGIME_TRIBUTARIO'
    end
    object fdqPessoaBO_CONTRIBUINTE_ICMS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte ICMS'
      FieldName = 'BO_CONTRIBUINTE_ICMS'
      Origin = 'BO_CONTRIBUINTE_ICMS'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaBO_CONTRIBUINTE_IPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte IPI'
      FieldName = 'BO_CONTRIBUINTE_IPI'
      Origin = 'BO_CONTRIBUINTE_IPI'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaBO_CONTRIBUINTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Contribuinte'
      FieldName = 'BO_CONTRIBUINTE'
      Origin = 'BO_CONTRIBUINTE'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaJOIN_DESCRICAO_REGIME_ESPECIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Regime Especial'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaTIPO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo da Empresa'
      FieldName = 'TIPO_EMPRESA'
      Origin = 'TIPO_EMPRESA'
      Size = 15
    end
  end
  object dspPessoa: TDataSetProvider
    DataSet = fdqPessoa
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 118
    Top = 8
  end
  object fdqPessoaEndereco: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT pe.*'
      '      ,c.descricao AS "JOIN_DESCRICAO_CIDADE" '
      'FROM PESSOA_ENDERECO pe'
      'LEFT JOIN CIDADE c ON pe.id_cidade = pe.id'
      'WHERE pe.id_pessoa = :id')
    Left = 62
    Top = 56
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaEnderecoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaEnderecoCEP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CEP'
      Origin = 'CEP'
      Size = 15
    end
    object fdqPessoaEnderecoBAIRRO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      Size = 100
    end
    object fdqPessoaEnderecoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 35
    end
    object fdqPessoaEnderecoLOGRADOURO: TStringField
      DisplayLabel = 'Logradouro'
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      Required = True
      Size = 100
    end
    object fdqPessoaEnderecoNUMERO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object fdqPessoaEnderecoCOMPLEMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Complemento'
      FieldName = 'COMPLEMENTO'
      Origin = 'COMPLEMENTO'
      Size = 50
    end
    object fdqPessoaEnderecoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqPessoaEnderecoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaEnderecoID_CIDADE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
    end
    object fdqPessoaEnderecoJOIN_DESCRICAO_CIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqPessoaContato: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT * FROM pessoa_contato WHERE id_pessoa = :id')
    Left = 62
    Top = 104
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaContatoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaContatoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object fdqPessoaContatoCONTATO: TStringField
      DisplayLabel = 'Contato'
      FieldName = 'CONTATO'
      Origin = 'CONTATO'
      Required = True
      Size = 255
    end
    object fdqPessoaContatoOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object fdqPessoaContatoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaContatoBO_EMAIL_ENVIO_DOCUMENTO_FISCAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email para Envio de Documento Fiscal'
      FieldName = 'BO_EMAIL_ENVIO_DOCUMENTO_FISCAL'
      Origin = 'BO_EMAIL_ENVIO_DOCUMENTO_FISCAL'
      FixedChar = True
      Size = 1
    end
  end
  object fdqPessoaUsuarioDesignControl: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_CONTROL WHERE '
      'id_pessoa_usuario = :id_pessoa_usuario and'
      'formulario = :formulario')
    Left = 272
    Top = 8
    ParamData = <
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = #39'0'#39
      end>
    object fdqPessoaUsuarioDesignControlID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaUsuarioDesignControlFORMULARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 255
    end
    object fdqPessoaUsuarioDesignControlARQUIVO_CONTROL: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_CONTROL'
      Origin = 'ARQUIVO_CONTROL'
    end
    object fdqPessoaUsuarioDesignControlARQUIVO_INVISIBLE: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_INVISIBLE'
      Origin = 'ARQUIVO_INVISIBLE'
    end
    object fdqPessoaUsuarioDesignControlID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqPessoaUsuarioDesignControlARQUIVO_VALORES_PADRAO: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_VALORES_PADRAO'
      Origin = 'ARQUIVO_VALORES_PADRAO'
    end
    object fdqPessoaUsuarioDesignControlARQUIVO_FILTRO_PADRAO: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FILTRO_PADRAO'
      Origin = 'ARQUIVO_FILTRO_PADRAO'
    end
    object fdqPessoaUsuarioDesignControlARQUIVO_PARAMETROS: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_PARAMETROS'
      Origin = 'ARQUIVO_PARAMETROS'
    end
  end
  object dspPessoaUsuarioDesignControl: TDataSetProvider
    DataSet = fdqPessoaUsuarioDesignControl
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 304
    Top = 8
  end
  object fdqPessoaUsuarioPermAcoes: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_GRID WHERE '
      'id_pessoa_usuario = :id_pessoa_usuario and'
      'formulario = :formulario')
    Left = 272
    Top = 104
    ParamData = <
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    object fdqPessoaUsuarioPermAcoesID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqPessoaUsuarioPermAcoesFORMULARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 50
    end
    object fdqPessoaUsuarioPermAcoesARQUIVO_GRID: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object fdqPessoaUsuarioPermAcoesARQUIVO_FIELD_CAPTION: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object fdqPessoaUsuarioPermAcoesID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspPessoaUsuarioPermAcoes: TDataSetProvider
    DataSet = fdqPessoaUsuarioPermAcoes
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 304
    Top = 104
  end
  object fdqPessoaUsuarioPermAcoesEx: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_GRID WHERE '
      'id_pessoa_usuario = :id_pessoa_usuario and'
      'formulario = :formulario')
    Left = 272
    Top = 152
    ParamData = <
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    object fdqPessoaUsuarioPermAcoesExID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqPessoaUsuarioPermAcoesExFORMULARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 50
    end
    object fdqPessoaUsuarioPermAcoesExARQUIVO_GRID: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object fdqPessoaUsuarioPermAcoesExARQUIVO_FIELD_CAPTION: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object fdqPessoaUsuarioPermAcoesExID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspPessoaUsuarioPermAcoesEx: TDataSetProvider
    DataSet = fdqPessoaUsuarioPermAcoesEx
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 304
    Top = 152
  end
  object fdqPessoaUsuarioPermForm: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_GRID WHERE '
      'id_pessoa_usuario = :id_pessoa_usuario and'
      'formulario = :formulario')
    Left = 272
    Top = 200
    ParamData = <
      item
        Name = 'ID_PESSOA_USUARIO'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    object fdqPessoaUsuarioPermFormID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqPessoaUsuarioPermFormFORMULARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 50
    end
    object fdqPessoaUsuarioPermFormARQUIVO_GRID: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object fdqPessoaUsuarioPermFormARQUIVO_FIELD_CAPTION: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object fdqPessoaUsuarioPermFormID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspPessoaUsuarioPermForm: TDataSetProvider
    DataSet = fdqPessoaUsuarioPermForm
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 305
    Top = 200
  end
  object dsPessoa: TDataSource
    DataSet = fdqPessoa
    Left = 146
    Top = 8
  end
  object fdqGrupoPessoa: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM GRUPO_PESSOA WHERE '
      'id = :id')
    Left = 240
    Top = 272
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqGrupoPessoaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGrupoPessoaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 100
    end
  end
  object dspGrupoPessoa: TDataSetProvider
    DataSet = fdqGrupoPessoa
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 272
    Top = 272
  end
  object fdqPessoaUsuario: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO WHERE id = :id')
    Left = 240
    Top = 328
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqPessoaUsuarioID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaUsuarioUSUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'USUARIO'
      Origin = 'USUARIO'
      Required = True
      Size = 50
    end
    object fdqPessoaUsuarioSENHA: TStringField
      DisplayLabel = 'Senha'
      FieldName = 'SENHA'
      Origin = 'SENHA'
      Required = True
      Size = 255
    end
    object fdqPessoaUsuarioDT_EXPIRACAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Experi'#231#227'o'
      FieldName = 'DT_EXPIRACAO'
      Origin = 'DT_EXPIRACAO'
    end
    object fdqPessoaUsuarioUSUARIO_EXPIRADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio Expirado'
      FieldName = 'USUARIO_EXPIRADO'
      Origin = 'USUARIO_EXPIRADO'
    end
    object fdqPessoaUsuarioUSUARIO_DIAS_EXPIRAR_SENHA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dias de Expira'#231#227'o'
      FieldName = 'USUARIO_DIAS_EXPIRAR_SENHA'
      Origin = 'USUARIO_DIAS_EXPIRAR_SENHA'
    end
    object fdqPessoaUsuarioEMAIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 100
    end
    object fdqPessoaUsuarioPRIVILEGIADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Privilegiado'
      FieldName = 'PRIVILEGIADO'
      Origin = 'PRIVILEGIADO'
    end
    object fdqPessoaUsuarioSENHA_KEY: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Chave Senha'
      FieldName = 'SENHA_KEY'
      Origin = 'SENHA_KEY'
      Size = 255
    end
    object fdqPessoaUsuarioBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaUsuarioID_USUARIO_PERFIL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Perfil'
      FieldName = 'ID_USUARIO_PERFIL'
      Origin = 'ID_USUARIO_PERFIL'
    end
    object fdqPessoaUsuarioUSUARIO_TIPO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Usu'#225'rio'
      FieldName = 'USUARIO_TIPO'
      Origin = 'USUARIO_TIPO'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaUsuarioESTACAO_ID: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Esta'#231#227'o'
      FieldName = 'ESTACAO_ID'
      Origin = 'ESTACAO_ID'
    end
  end
  object dspPessoaUsuario: TDataSetProvider
    DataSet = fdqPessoaUsuario
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 272
    Top = 328
  end
  object fdqPessoaConjuge: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT c.*'
      '      ,e.uf AS JOIN_UF_ESTADO'
      '      ,cn.descricao AS JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      '      ,ce.descricao AS JOIN_DESCRICAO_CIDADE_EMPRESA   '
      '  FROM pessoa_conjuge c '
      '  LEFT JOIN estado e        ON c.id_uf_emissor = e.id'
      '  LEFT JOIN cidade cn       ON c.id_naturalidade = cn.id'
      '  LEFT JOIN cidade ce       ON c.id_cidade_empresa = ce.id'
      ' WHERE c.id_pessoa = :id')
    Left = 62
    Top = 160
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaConjugeID: TFDAutoIncField
      DisplayLabel = 'C'#243'd. Conjug'#234
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaConjugeID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaConjugeNOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 80
    end
    object fdqPessoaConjugeCPF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CPF'
      Origin = 'CPF'
      Size = 11
    end
    object fdqPessoaConjugeSEXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sexo'
      FieldName = 'SEXO'
      Origin = 'SEXO'
    end
    object fdqPessoaConjugeRG: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RG'
      Origin = 'RG'
      Size = 25
    end
    object fdqPessoaConjugeORGAO_EMISSOR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Org'#227'o Emissor'
      FieldName = 'ORGAO_EMISSOR'
      Origin = 'ORGAO_EMISSOR'
      Size = 3
    end
    object fdqPessoaConjugeID_UF_EMISSOR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. UF Emissor'
      FieldName = 'ID_UF_EMISSOR'
      Origin = 'ID_UF_EMISSOR'
    end
    object fdqPessoaConjugeDT_NASCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object fdqPessoaConjugeEMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome Empresa'
      FieldName = 'EMPRESA'
      Origin = 'EMPRESA'
      Size = 80
    end
    object fdqPessoaConjugeENDERECO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Endere'#231'o Empresa'
      FieldName = 'ENDERECO_EMPRESA'
      Origin = 'ENDERECO_EMPRESA'
      Size = 80
    end
    object fdqPessoaConjugeNUMERO_EMPRESA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_EMPRESA'
      Origin = 'NUMERO_EMPRESA'
    end
    object fdqPessoaConjugeCEP_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CEP Empresa'
      FieldName = 'CEP_EMPRESA'
      Origin = 'CEP_EMPRESA'
      Size = 8
    end
    object fdqPessoaConjugeID_CIDADE_EMPRESA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Cidade Empresa'
      FieldName = 'ID_CIDADE_EMPRESA'
      Origin = 'ID_CIDADE_EMPRESA'
    end
    object fdqPessoaConjugeCNPJ_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CNPJ Empresa'
      FieldName = 'CNPJ_EMPRESA'
      Origin = 'CNPJ_EMPRESA'
      Size = 14
    end
    object fdqPessoaConjugeBAIRRO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Bairro Empresa'
      FieldName = 'BAIRRO_EMPRESA'
      Origin = 'BAIRRO_EMPRESA'
      Size = 45
    end
    object fdqPessoaConjugeDT_ADMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Admiss'#227'o'
      FieldName = 'DT_ADMISSAO'
      Origin = 'DT_ADMISSAO'
    end
    object fdqPessoaConjugeRENDA_MENSAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Renda Mensal'
      FieldName = 'RENDA_MENSAL'
      Origin = 'RENDA_MENSAL'
      Precision = 24
      Size = 9
    end
    object fdqPessoaConjugeTELEFONE_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Telefone Empresa'
      FieldName = 'TELEFONE_EMPRESA'
      Origin = 'TELEFONE_EMPRESA'
    end
    object fdqPessoaConjugeRAMAL_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ramal Empresa'
      FieldName = 'RAMAL_EMPRESA'
      Origin = 'RAMAL_EMPRESA'
      Size = 10
    end
    object fdqPessoaConjugeID_NATURALIDADE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'd. Naturalidade'
      FieldName = 'ID_NATURALIDADE'
      Origin = 'ID_NATURALIDADE'
    end
    object fdqPessoaConjugeJOIN_UF_ESTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'UF Estado'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'UF'
      ProviderFlags = []
      Size = 2
    end
    object fdqPessoaConjugeJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Cidade Naturalidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaConjugeJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Cidade Empresa'
      FieldName = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaConjugeTEMPO_TRABALHO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tempo de Trabalho'
      FieldName = 'TEMPO_TRABALHO'
      Origin = 'TEMPO_TRABALHO'
      Size = 100
    end
    object fdqPessoaConjugeOCUPACAO_EMPRESA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ocupa'#231#227'o Empresa'
      FieldName = 'OCUPACAO_EMPRESA'
      Origin = 'OCUPACAO_EMPRESA'
      Size = 255
    end
  end
  object fdqOcupacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT c.*'
      '  FROM ocupacao c'
      ' WHERE c.id = :id'
      '')
    Left = 384
    Top = 264
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqOcupacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOcupacaoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 100
    end
    object fdqOcupacaoCODIGO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo CLT'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Size = 6
    end
  end
  object dspOcupacao: TDataSetProvider
    DataSet = fdqOcupacao
    Left = 416
    Top = 264
  end
  object fdqAreaAtuacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT at.*'
      '  FROM area_atuacao at'
      ' WHERE at.id = :id'
      '')
    Left = 384
    Top = 328
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqAreaAtuacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqAreaAtuacaoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 80
    end
  end
  object dspAreaAtuacao: TDataSetProvider
    DataSet = fdqAreaAtuacao
    Left = 416
    Top = 328
  end
  object dspClassificacaoEmpresa: TDataSetProvider
    DataSet = fdqClassificacaoEmpresa
    Left = 416
    Top = 392
  end
  object fdqClassificacaoEmpresa: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ce.*'
      '  FROM classificacao_empresa ce'
      ' WHERE ce.id = :id'
      '')
    Left = 384
    Top = 392
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqClassificacaoEmpresaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqClassificacaoEmpresaDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 80
    end
    object fdqClassificacaoEmpresaVALOR_INICIAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor Inicial'
      FieldName = 'VALOR_INICIAL'
      Origin = 'VALOR_INICIAL'
      Precision = 24
      Size = 9
    end
    object fdqClassificacaoEmpresaVALOR_FINAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor Final'
      FieldName = 'VALOR_FINAL'
      Origin = 'VALOR_FINAL'
      Precision = 24
      Size = 9
    end
  end
  object fdqPessoaRepresentante: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT pr.*'
      '  FROM PESSOA_REPRESENTANTE pr'
      ' WHERE pr.id_pessoa = :id')
    Left = 63
    Top = 224
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaRepresentanteID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaRepresentanteNOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 255
    end
    object fdqPessoaRepresentanteCPF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CPF'
      Origin = 'CPF'
      Size = 11
    end
    object fdqPessoaRepresentanteRG: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RG'
      Origin = 'RG'
      Size = 9
    end
    object fdqPessoaRepresentanteDT_NASCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object fdqPessoaRepresentanteESTADO_CIVIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Estado Civil'
      FieldName = 'ESTADO_CIVIL'
      Origin = 'ESTADO_CIVIL'
      Size = 10
    end
    object fdqPessoaRepresentanteID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
  end
  object fdqPessoaSetor: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT ps.*'
      '      ,sc.descricao AS "JOIN_DESCRICAO_SETOR_COMERCIAL"'
      'FROM pessoa_setor ps  '
      'INNER JOIN setor_comercial sc ON ps.id_setor_comercial = sc.id'
      'WHERE ps.id_pessoa = :id')
    Left = 63
    Top = 280
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaSetorID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaSetorQUANTIDADE_FUNCIONARIOS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Qtde. Funcion'#225'rios'
      FieldName = 'QUANTIDADE_FUNCIONARIOS'
      Origin = 'QUANTIDADE_FUNCIONARIOS'
    end
    object fdqPessoaSetorID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaSetorID_SETOR_COMERCIAL: TIntegerField
      DisplayLabel = 'C'#243'digo do Setor Comercial'
      FieldName = 'ID_SETOR_COMERCIAL'
      Origin = 'ID_SETOR_COMERCIAL'
      Required = True
    end
    object fdqPessoaSetorJOIN_DESCRICAO_SETOR_COMERCIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Setor Comercial'
      FieldName = 'JOIN_DESCRICAO_SETOR_COMERCIAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object fdqPessoaAtividadeSecundaria: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT pas.*'
      '      ,cnae.descricao as JOIN_DESCRICAO_CNAE'
      '      ,cnae.sequencia as JOIN_SEQUENCIA_CNAE'
      '  FROM pessoa_atividade_secundaria pas'
      ' INNER JOIN cnae on pas.id_cnae = cnae.id'
      ' WHERE pas.id_pessoa = :id')
    Left = 63
    Top = 336
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaAtividadeSecundariaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaAtividadeSecundariaID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaAtividadeSecundariaID_CNAE: TIntegerField
      DisplayLabel = 'C'#243'd. CNAE'
      FieldName = 'ID_CNAE'
      Origin = 'ID_CNAE'
      Required = True
    end
    object fdqPessoaAtividadeSecundariaJOIN_DESCRICAO_CNAE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o CNAE'
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqPessoaAtividadeSecundariaJOIN_SEQUENCIA_CNAE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia CNAE'
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
  end
  object fdqSetorComercial: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT s.*'
      '  FROM setor_comercial s'
      ' WHERE s.id = :id'
      '')
    Left = 200
    Top = 384
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqSetorComercialDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object fdqSetorComercialOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object dspSetorComercial: TDataSetProvider
    DataSet = fdqSetorComercial
    Left = 232
    Top = 384
  end
  object fdqUserControl: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    Left = 424
    Top = 40
  end
  object dspUserControl: TDataSetProvider
    DataSet = fdqUserControl
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 456
    Top = 40
  end
  object fdqPessoaUsuarioDesignGrid_ConsultaFormulario: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM PESSOA_USUARIO_DESIGN_GRID WHERE '
      'formulario = :formulario LIMIT 1')
    Left = 424
    Top = 104
    ParamData = <
      item
        Name = 'FORMULARIO'
        DataType = ftString
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    object FDAutoIncField2: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object StringField1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 255
    end
    object BlobField1: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object BlobField2: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object IntegerField1: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspPessoaUsuarioDesignGrid_ConsultaFormulario: TDataSetProvider
    DataSet = fdqPessoaUsuarioDesignGrid_ConsultaFormulario
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 456
    Top = 104
  end
  object fdqPessoaCredito: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT *      '
      'FROM pessoa_credito'
      'WHERE id_pessoa = :id')
    Left = 63
    Top = 392
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaCreditoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaCreditoVL_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor Cr'#233'dito'
      FieldName = 'VL_CREDITO'
      Origin = 'VL_CREDITO'
      Precision = 24
      Size = 9
    end
    object fdqPessoaCreditoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
  end
  object fdqPessoaRestricao: TgbFDQuery
    MasterSource = dsPessoa
    MasterFields = 'ID'
    DetailFields = 'ID_PESSOA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    SQL.Strings = (
      'SELECT pr.*'
      '      ,r.descricao AS "JOIN_DESCRICAO_RESTRICAO"'
      'FROM pessoa_restricao pr  '
      'INNER JOIN tipo_restricao r ON pr.id_tipo_restricao = r.id'
      'WHERE pr.id_pessoa = :id')
    Left = 143
    Top = 128
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqPessoaRestricaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaRestricaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqPessoaRestricaoDH_INCLUSAO: TDateTimeField
      DisplayLabel = 'Dh. Inclus'#227'o'
      FieldName = 'DH_INCLUSAO'
      Origin = 'DH_INCLUSAO'
      Required = True
    end
    object fdqPessoaRestricaoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqPessoaRestricaoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object fdqPessoaRestricaoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqPessoaRestricaoID_TIPO_RESTRICAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo da Restri'#231#227'o'
      FieldName = 'ID_TIPO_RESTRICAO'
      Origin = 'ID_TIPO_RESTRICAO'
      Required = True
    end
    object fdqPessoaRestricaoJOIN_DESCRICAO_RESTRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Restri'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_RESTRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
