unit uSMCSTIPI;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter;

type
  TSMCSTIPI = class(TDSServerModule)
    fdqCSTIPI: TgbFDQuery;
    fdqCSTIPIID: TFDAutoIncField;
    fdqCSTIPIDESCRICAO: TStringField;
    dspCSTIPI: TDataSetProvider;
    fdqCSTIPICODIGO_CST: TStringField;
    fdqEnquadramentoLegalIPI: TgbFDQuery;
    dspEnquadramentoLegaIPI: TDataSetProvider;
    fdqEnquadramentoLegalIPIID: TFDAutoIncField;
    fdqEnquadramentoLegalIPICODIGO: TStringField;
    fdqEnquadramentoLegalIPIGRUPO_CST: TStringField;
    fdqEnquadramentoLegalIPIDESCRICAO: TBlobField;
    fdqCSTIPICENQ: TStringField;
  private
    { Private declarations }
  public
    function GetCSTIPI(AIdCSTIPI: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uCSTIPIProxy, uCSTIPIServ, REST.JSON;

{$R *.dfm}

{ TSMCSTIPI }

function TSMCSTIPI.GetCSTIPI(
  AIdCSTIPI: Integer): String;
begin
  result := TJson.ObjectToJsonString(TCSTIPIServ.GetCSTIPI(AIdCSTIPI));
end;

end.

