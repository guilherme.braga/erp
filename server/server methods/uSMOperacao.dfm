object SMOperacao: TSMOperacao
  OldCreateOrder = False
  Height = 149
  Width = 381
  object fdqOperacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT o.*'
      'FROM operacao o'
      'where o.id = :id')
    Left = 48
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqOperacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOperacaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqOperacaoNATUREZA_OPERACAO: TStringField
      DisplayLabel = 'Natureza de Opera'#231#227'o'
      FieldName = 'NATUREZA_OPERACAO'
      Origin = 'NATUREZA_OPERACAO'
      Required = True
      Size = 255
    end
  end
  object dspOperacao: TDataSetProvider
    DataSet = fdqOperacao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 77
    Top = 8
  end
  object dsOperacao: TDataSource
    DataSet = fdqOperacao
    Left = 105
    Top = 8
  end
  object fdqOperacaoItem: TgbFDQuery
    MasterSource = dsOperacao
    MasterFields = 'ID'
    DetailFields = 'ID_OPERACAO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT oi.*'
      '      ,a.descricao as JOIN_DESCRICAO_ACAO'
      'FROM operacao_item oi'
      'INNER JOIN acao a on oi.id_acao = a.id'
      'WHERE oi.id_operacao = :id')
    Left = 48
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqOperacaoItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOperacaoItemID_OPERACAO: TIntegerField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object fdqOperacaoItemID_ACAO: TIntegerField
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ID_ACAO'
      Origin = 'ID_ACAO'
      Required = True
    end
    object fdqOperacaoItemJOIN_DESCRICAO_ACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_ACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqAcao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM acao WHERE id = :id')
    Left = 248
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqAcaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqAcaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqAcaoACAO: TStringField
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ACAO'
      Origin = 'ACAO'
      Required = True
      Size = 100
    end
  end
  object dspAcao: TDataSetProvider
    DataSet = fdqAcao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 276
    Top = 8
  end
  object fdqOperacaoFiscal: TgbFDQuery
    MasterSource = dsOperacao
    MasterFields = 'ID'
    DetailFields = 'ID_OPERACAO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT of.*'
      
        '      ,(SELECT descricao FROM cfop WHERE codigo = of.cfop_produt' +
        'o) AS JOIN_DESCRICAO_CFOP_PRODUTO'
      
        '      ,(SELECT descricao FROM cfop WHERE codigo = of.cfop_produt' +
        'o_st) AS JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      
        '      ,(SELECT descricao FROM cfop WHERE codigo = of.cfop_mercad' +
        'oria) AS JOIN_DESCRICAO_CFOP_MERCADORIA'
      
        '      ,(SELECT descricao FROM cfop WHERE codigo = of.cfop_mercad' +
        'oria_st) AS JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      'FROM operacao_fiscal of'
      'WHERE of.id_operacao = :id')
    Left = 80
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqOperacaoFiscalID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqOperacaoFiscalDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdqOperacaoFiscalID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object fdqOperacaoFiscalCFOP_PRODUTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CFOP de Produto'
      FieldName = 'CFOP_PRODUTO'
      Origin = 'CFOP_PRODUTO'
    end
    object fdqOperacaoFiscalCFOP_PRODUTO_ST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CFOP de Produto ST'
      FieldName = 'CFOP_PRODUTO_ST'
      Origin = 'CFOP_PRODUTO_ST'
    end
    object fdqOperacaoFiscalCFOP_MERCADORIA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CFOP da Mercadoria'
      FieldName = 'CFOP_MERCADORIA'
      Origin = 'CFOP_MERCADORIA'
    end
    object fdqOperacaoFiscalCFOP_MERCADORIA_ST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CFOP da Mercadoria ST'
      FieldName = 'CFOP_MERCADORIA_ST'
      Origin = 'CFOP_MERCADORIA_ST'
    end
    object fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CFOP_PRODUTO'
      Origin = 'JOIN_DESCRICAO_CFOP_PRODUTO'
      ProviderFlags = []
      Size = 255
    end
    object fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO_ST: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      Origin = 'JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      ProviderFlags = []
      Size = 255
    end
    object fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CFOP_MERCADORIA'
      Origin = 'JOIN_DESCRICAO_CFOP_MERCADORIA'
      ProviderFlags = []
      Size = 255
    end
    object fdqOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA_ST: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      Origin = 'JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqNaturezaOperacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM natureza_operacao WHERE id = :id')
    Left = 248
    Top = 72
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object StringField1: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
  end
  object dspNaturezaOperacao: TDataSetProvider
    DataSet = fdqNaturezaOperacao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 276
    Top = 72
  end
end
