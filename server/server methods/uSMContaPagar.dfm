object SMContaPagar: TSMContaPagar
  OldCreateOrder = False
  Height = 342
  Width = 569
  object dspContaPagar: TDataSetProvider
    DataSet = fdqContaPagar
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 133
    Top = 16
  end
  object fdqContaPagar: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT cp.*'
      '      ,p.nome AS JOIN_DESCRICAO_NOME_PESSOA'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,c.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,chp.origem AS JOIN_ORIGEM_CHAVE_PROCESSO'
      '      ,chp.id_origem AS JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      '      ,fp.descricao AS JOIN_DESCRICAO_FORMA_PAGAMENTO'
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,car.descricao as JOIN_DESCRICAO_CARTEIRA'
      'FROM conta_pagar cp'
      'INNER JOIN pessoa p ON cp.id_pessoa = p.id'
      'INNER JOIN conta_analise ca ON cp.id_conta_analise = ca.id'
      'INNER JOIN centro_resultado c ON cp.id_centro_resultado = c.id'
      'INNER JOIN forma_pagamento fp ON cp.id_forma_pagamento = fp.id'
      'INNER JOIN chave_processo chp ON cp.id_chave_processo = chp.id'
      'INNER JOIN carteira car ON cp.id_carteira = car.id'
      'LEFT JOIN conta_corrente cc ON cp.id_conta_corrente = cc.id'
      'WHERE cp.id = :id')
    Left = 56
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaPagarID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqContaPagarDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaPagarDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object fdqContaPagarDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqContaPagarVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_QUITADO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_ABERTO: TFMTBCDField
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQT_PARCELA: TIntegerField
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqContaPagarNR_PARCELA: TIntegerField
      DisplayLabel = 'Nr. Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object fdqContaPagarDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqContaPagarSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
      Size = 10
    end
    object fdqContaPagarBO_VENCIDO: TStringField
      DisplayLabel = 'Vencido?'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaPagarOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaPagarID_PESSOA: TIntegerField
      DisplayLabel = 'Id. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqContaPagarID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Id. Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqContaPagarID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Id. Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqContaPagarSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object fdqContaPagarID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Id. Chave Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqContaPagarID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Id. Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object fdqContaPagarJOIN_DESCRICAO_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaPagarJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Centro Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem Chave Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqContaPagarJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Origem Chave Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object fdqContaPagarJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaPagarVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_DECRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DECRESCIMO'
      Origin = 'VL_DECRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_QUITADO_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado L'#237'quido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object fdqContaPagarID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqContaPagarTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object fdqContaPagarDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object fdqContaPagarID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqContaPagarJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarID_FILIAL: TIntegerField
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqContaPagarPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarID_CARTEIRA: TIntegerField
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object fdqContaPagarJOIN_DESCRICAO_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqContaPagarQuitacao: TgbFDQuery
    MasterSource = dsContaPagar
    MasterFields = 'ID'
    DetailFields = 'ID_CONTA_PAGAR'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evCache, evDetailOptimize]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select cpi.*'
      '      ,tq.descricao JOIN_DESCRICAO_TIPO_QUITACAO '
      '      ,tq.tipo JOIN_TIPO_TIPO_QUITACAO '
      '      ,cc.descricao JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,ca.descricao JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,oc.descricao AS JOIN_DESCRICAO_OPERADORA_CARTAO'
      
        '      ,(select nome from pessoa_usuario where id = cpi.id_usuari' +
        'o_baixa) AS JOIN_NOME_USUARIO_BAIXA'
      'from conta_pagar_quitacao cpi'
      'INNER JOIN tipo_quitacao tq ON cpi.id_tipo_quitacao = tq.id'
      'INNER JOIN conta_corrente cc ON cpi.id_conta_corrente = cc.id'
      'INNER JOIN conta_analise ca ON cpi.id_conta_analise = ca.id'
      
        'INNER JOIN centro_resultado cr ON cpi.id_centro_resultado = cr.i' +
        'd'
      'LEFT JOIN operadora_cartao oc ON cpi.id_operadora_cartao = oc.id'
      'where cpi.id_conta_pagar = :id'
      '')
    Left = 56
    Top = 71
    ParamData = <
      item
        Name = 'ID'
        DataType = ftAutoInc
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object fdqContaPagarQuitacaoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqContaPagarQuitacaoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaPagarQuitacaoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaPagarQuitacaoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'Id. Tipo Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object fdqContaPagarQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
    end
    object fdqContaPagarQuitacaoID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqContaPagarQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Id. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqContaPagarQuitacaoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqContaPagarQuitacaoVL_TOTAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoID_CONTA_PAGAR: TIntegerField
      DisplayLabel = 'Id. Conta Pagar'
      FieldName = 'ID_CONTA_PAGAR'
      Origin = 'ID_CONTA_PAGAR'
    end
    object fdqContaPagarQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqContaPagarQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarQuitacaoID_CENTRO_RESULTADO: TIntegerField
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqContaPagarQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Id. Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqContaPagarQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Desc. Conta An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqContaPagarQuitacaoTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object fdqContaPagarQuitacaoSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqContaPagarQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo Quita'#231#227'o'
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object fdqContaPagarQuitacaoID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqContaPagarQuitacaoCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqContaPagarQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqContaPagarQuitacaoCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqContaPagarQuitacaoCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqContaPagarQuitacaoCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqContaPagarQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqContaPagarQuitacaoCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqContaPagarQuitacaoPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoPERC_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoPERC_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoVL_TROCO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Troco'
      FieldName = 'VL_TROCO'
      Origin = 'VL_TROCO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaPagarQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqContaPagarQuitacaoID_USUARIO_BAIXA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object fdqContaPagarQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dsContaPagar: TDataSource
    DataSet = fdqContaPagar
    Left = 211
    Top = 16
  end
end
