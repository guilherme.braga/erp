unit uSMSituacaoEspecial;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMSituacaoEspecial = class(TDSServerModule)
    fdqSituacaoEspecial: TgbFDQuery;
    dspSituacaoEspecial: TDataSetProvider;
    fdqSituacaoEspecialID: TFDAutoIncField;
    fdqSituacaoEspecialDESCRICAO: TStringField;
    fdqPessoaProdutoSituacaoEspecial: TgbFDQuery;
    dspPessoaProdutoSituacaoEspecial: TDataSetProvider;
    fdqPessoaProdutoSituacaoEspecialID: TFDAutoIncField;
    fdqPessoaProdutoSituacaoEspecialID_SITUACAO_ESPECIAL: TIntegerField;
    fdqPessoaProdutoSituacaoEspecialID_PESSOA: TIntegerField;
    fdqPessoaProdutoSituacaoEspecialID_PRODUTO: TIntegerField;
    fdqPessoaProdutoSituacaoEspecialJOIN_NOME_PESSOA: TStringField;
    fdqPessoaProdutoSituacaoEspecialJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqSituacaoEspecialBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    function GetSituacaoEspecial(AIdSituacaoEspecial: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uSituacaoEspecialProxy, uSituacaoEspecialServ, REST.JSON;

{$R *.dfm}

{ TSMSituacaoEspecial }

function TSMSituacaoEspecial.GetSituacaoEspecial(
  AIdSituacaoEspecial: Integer): String;
begin
  result := TJson.ObjectToJsonString(TSituacaoEspecialServ.GetSituacaoEspecial(AIdSituacaoEspecial));
end;

end.

