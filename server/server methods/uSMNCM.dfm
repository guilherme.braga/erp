object SMNCM: TSMNCM
  OldCreateOrder = False
  Height = 278
  Width = 348
  object fdqNCM: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ncm.*'
      'FROM ncm WHERE id = :id')
    Left = 48
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqNCMID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNCMCODIGO: TIntegerField
      DisplayLabel = 'C'#243'digo NCM'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Required = True
    end
    object fdqNCMDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqNCMUN: TStringField
      FieldName = 'UN'
      Origin = 'UN'
      Required = True
      FixedChar = True
      Size = 2
    end
  end
  object dspNCM: TDataSetProvider
    DataSet = fdqNCM
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object dsNCM: TDataSource
    DataSet = fdqNCM
    Left = 110
    Top = 15
  end
  object fdqNCMEspecializado: TgbFDQuery
    MasterSource = dsNCM
    MasterFields = 'ID'
    DetailFields = 'ID_NCM'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ncme.*'
      '      ,ncm.codigo AS JOIN_CODIGO_NCM'
      '      ,ncm.descricao AS JOIN_DESCRICAO_NCM'
      'FROM ncm_especializado ncme'
      'INNER JOIN ncm ON ncme.id_ncm = ncm.id'
      'WHERE ncme.id_ncm = :id'
      ''
      '')
    Left = 48
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqNCMEspecializadoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqNCMEspecializadoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqNCMEspecializadoID_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'ID_NCM'
      Origin = 'ID_NCM'
    end
    object fdqNCMEspecializadoJOIN_CODIGO_NCM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'JOIN_CODIGO_NCM'
      Origin = 'CODIGO'
      ProviderFlags = []
    end
    object fdqNCMEspecializadoJOIN_DESCRICAO_NCM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o NCM'
      FieldName = 'JOIN_DESCRICAO_NCM'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
