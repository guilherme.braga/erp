object SMConfiguracoesFiscais: TSMConfiguracoesFiscais
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqConfiguracoesFiscais: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT cf.*'
      '      ,f.fantasia AS JOIN_FANTASIA_FILIAL'
      'FROM configuracao_fiscal cf'
      'INNER JOIN filial f ON cf.id_filial = f.id'
      'WHERE cf.id = :id')
    Left = 64
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqConfiguracoesFiscaisID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqConfiguracoesFiscaisBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisLOGOMARCA: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Logomarca'
      FieldName = 'LOGOMARCA'
      Origin = 'LOGOMARCA'
    end
    object fdqConfiguracoesFiscaisCERTIFICADO_CAMINHO: TStringField
      DisplayLabel = 'Caminho do Certificado'
      FieldName = 'CERTIFICADO_CAMINHO'
      Origin = 'CERTIFICADO_CAMINHO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisCERTIFICADO_SENHA: TStringField
      DisplayLabel = 'Senha do Certificado'
      FieldName = 'CERTIFICADO_SENHA'
      Origin = 'CERTIFICADO_SENHA'
      Size = 255
    end
    object fdqConfiguracoesFiscaisCERTIFICADO_NUMERO_SERIE: TStringField
      DisplayLabel = 'N'#250'mero de S'#233'rie do Certificado'
      FieldName = 'CERTIFICADO_NUMERO_SERIE'
      Origin = 'CERTIFICADO_NUMERO_SERIE'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFE_MODO_IMPRESSAO: TStringField
      DisplayLabel = 'Modo de Impress'#227'o NFE'
      FieldName = 'NFE_MODO_IMPRESSAO'
      Origin = 'NFE_MODO_IMPRESSAO'
      Required = True
      Size = 15
    end
    object fdqConfiguracoesFiscaisNFE_FORMA_EMISSAO: TStringField
      DisplayLabel = 'Forma de Emiss'#227'o NFE'
      FieldName = 'NFE_FORMA_EMISSAO'
      Origin = 'NFE_FORMA_EMISSAO'
      Required = True
      Size = 15
    end
    object fdqConfiguracoesFiscaisNFE_WEBSERVICE_UF: TStringField
      DisplayLabel = 'UF NFE'
      FieldName = 'NFE_WEBSERVICE_UF'
      Origin = 'NFE_WEBSERVICE_UF'
      Required = True
      Size = 2
    end
    object fdqConfiguracoesFiscaisNFE_WEBSERVICE_AMBIENTE: TStringField
      DisplayLabel = 'Ambiente NFE'
      FieldName = 'NFE_WEBSERVICE_AMBIENTE'
      Origin = 'NFE_WEBSERVICE_AMBIENTE'
      Required = True
      Size = 15
    end
    object fdqConfiguracoesFiscaisNFE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Visualizar Mensagem NFE'
      FieldName = 'NFE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      Origin = 'NFE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisNFE_PROXY_HOST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Host Proxy NFE'
      FieldName = 'NFE_PROXY_HOST'
      Origin = 'NFE_PROXY_HOST'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFE_PROXY_PORTA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Porta Proxy NFE'
      FieldName = 'NFE_PROXY_PORTA'
      Origin = 'NFE_PROXY_PORTA'
    end
    object fdqConfiguracoesFiscaisNFE_PROXY_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio Proxy NFE'
      FieldName = 'NFE_PROXY_USUARIO'
      Origin = 'NFE_PROXY_USUARIO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFE_PROXY_SENHA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Senha Proxy NFE'
      FieldName = 'NFE_PROXY_SENHA'
      Origin = 'NFE_PROXY_SENHA'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_SMTP: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Email do Remetente'
      FieldName = 'EMAIL_SMTP'
      Origin = 'EMAIL_SMTP'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_PORTA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Porta SMTP'
      FieldName = 'EMAIL_PORTA'
      Origin = 'EMAIL_PORTA'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Email de Autentica'#231#227'o'
      FieldName = 'EMAIL_USUARIO'
      Origin = 'EMAIL_USUARIO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_SENHA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Senha de Autentica'#231#227'o'
      FieldName = 'EMAIL_SENHA'
      Origin = 'EMAIL_SENHA'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_ASSUNTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Assunto'
      FieldName = 'EMAIL_ASSUNTO'
      Origin = 'EMAIL_ASSUNTO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_MENSAGEM: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Mensagem'
      FieldName = 'EMAIL_MENSAGEM'
      Origin = 'EMAIL_MENSAGEM'
    end
    object fdqConfiguracoesFiscaisEMAIL_ENVIAR_NFE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Enviar Email Ap'#243's Emiss'#227'o da NFE'
      FieldName = 'EMAIL_ENVIAR_NFE'
      Origin = 'EMAIL_ENVIAR_NFE'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisEMAIL_ENVIAR_NFCE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Enviar Email Ap'#243's Emiss'#227'o da NFCE'
      FieldName = 'EMAIL_ENVIAR_NFCE'
      Origin = 'EMAIL_ENVIAR_NFCE'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisEMAIL_EMAIL_HOST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Host'
      FieldName = 'EMAIL_EMAIL_HOST'
      Origin = 'EMAIL_EMAIL_HOST'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_EMAIL_DESTINO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Email Principal do Destinat'#225'rio'
      FieldName = 'EMAIL_EMAIL_DESTINO'
      Origin = 'EMAIL_EMAIL_DESTINO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_EMAIL_CC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Emails para Envio em C'#243'pia'
      FieldName = 'EMAIL_EMAIL_CC'
      Origin = 'EMAIL_EMAIL_CC'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_EMAIL_CCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Emails para Envio em C'#243'pia Oculta'
      FieldName = 'EMAIL_EMAIL_CCO'
      Origin = 'EMAIL_EMAIL_CCO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisEMAIL_SSL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Utiliza SSL'
      FieldName = 'EMAIL_SSL'
      Origin = 'EMAIL_SSL'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisEMAIL_SOLICITA_CONFIRMACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Solicita Confirma'#231#227'o de Email'
      FieldName = 'EMAIL_SOLICITA_CONFIRMACAO'
      Origin = 'EMAIL_SOLICITA_CONFIRMACAO'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisEMAIL_USAR_THREAD: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Usar Thread'
      FieldName = 'EMAIL_USAR_THREAD'
      Origin = 'EMAIL_USAR_THREAD'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisEMAIL_NOME_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Email - Nome do Remetente'
      FieldName = 'EMAIL_NOME_ORIGEM'
      Origin = 'EMAIL_NOME_ORIGEM'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFCE_MODO_IMPRESSAO: TStringField
      DisplayLabel = 'Modo de Impress'#227'o NFCE'
      FieldName = 'NFCE_MODO_IMPRESSAO'
      Origin = 'NFCE_MODO_IMPRESSAO'
      Required = True
      Size = 15
    end
    object fdqConfiguracoesFiscaisNFCE_WEBSERVICE_UF: TStringField
      DisplayLabel = 'UF Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_UF'
      Origin = 'NFCE_WEBSERVICE_UF'
      Required = True
      Size = 2
    end
    object fdqConfiguracoesFiscaisNFCE_WEBSERVICE_AMBIENTE: TStringField
      DisplayLabel = 'Ambiente Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_AMBIENTE'
      Origin = 'NFCE_WEBSERVICE_AMBIENTE'
      Required = True
      Size = 15
    end
    object fdqConfiguracoesFiscaisNFCE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Visualizar Mensagem Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      Origin = 'NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisNFCE_PROXY_HOST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Host Proxy NFCE'
      FieldName = 'NFCE_PROXY_HOST'
      Origin = 'NFCE_PROXY_HOST'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFCE_PROXY_PORTA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Porta Proxy NFCE'
      FieldName = 'NFCE_PROXY_PORTA'
      Origin = 'NFCE_PROXY_PORTA'
    end
    object fdqConfiguracoesFiscaisNFCE_PROXY_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio Proxy NFCE'
      FieldName = 'NFCE_PROXY_USUARIO'
      Origin = 'NFCE_PROXY_USUARIO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFCE_PROXY_SENHA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Senha Proxy NFCE'
      FieldName = 'NFCE_PROXY_SENHA'
      Origin = 'NFCE_PROXY_SENHA'
      Size = 255
    end
    object fdqConfiguracoesFiscaisID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqConfiguracoesFiscaisJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Fantasia'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqConfiguracoesFiscaisNFE_ID_RELATORIO_FR: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Relat'#243'rio FR'
      FieldName = 'NFE_ID_RELATORIO_FR'
      Origin = 'NFE_ID_RELATORIO_FR'
    end
    object fdqConfiguracoesFiscaisEXPANDIR_LOGOMARCAR: TStringField
      DisplayLabel = 'Expandir Logomarca'
      FieldName = 'EXPANDIR_LOGOMARCAR'
      Origin = 'EXPANDIR_LOGOMARCAR'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisNFCE_ID_CSC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'ID CSC - NFCe'
      FieldName = 'NFCE_ID_CSC'
      Origin = 'NFCE_ID_CSC'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFCE_CSC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CSC - NFCe'
      FieldName = 'NFCE_CSC'
      Origin = 'NFCE_CSC'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_LOGS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio de Logs'
      FieldName = 'ARQUIVO_PATH_LOGS'
      Origin = 'ARQUIVO_PATH_LOGS'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_SCHEMAS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio Schemas'
      FieldName = 'ARQUIVO_PATH_SCHEMAS'
      Origin = 'ARQUIVO_PATH_SCHEMAS'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_NFE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio NFE'
      FieldName = 'ARQUIVO_PATH_NFE'
      Origin = 'ARQUIVO_PATH_NFE'
      Size = 255
    end
    object fdqConfiguracoesFiscaisSALVAR_PDF_JUNTO_XML: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Salvar PDF Junto com XML'
      FieldName = 'SALVAR_PDF_JUNTO_XML'
      Origin = 'SALVAR_PDF_JUNTO_XML'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_CANCELAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio de Cancelamento'
      FieldName = 'ARQUIVO_PATH_CANCELAMENTO'
      Origin = 'ARQUIVO_PATH_CANCELAMENTO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_INUTILIZACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio de Inutiliza'#231#227'o'
      FieldName = 'ARQUIVO_PATH_INUTILIZACAO'
      Origin = 'ARQUIVO_PATH_INUTILIZACAO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_DPEC: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio DPEC'
      FieldName = 'ARQUIVO_PATH_DPEC'
      Origin = 'ARQUIVO_PATH_DPEC'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_EVENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio Eventos'
      FieldName = 'ARQUIVO_PATH_EVENTO'
      Origin = 'ARQUIVO_PATH_EVENTO'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_RELATORIO_NFCE_FR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Arquivo DANFE NFCE Fast Report'
      FieldName = 'ARQUIVO_RELATORIO_NFCE_FR'
      Origin = 'ARQUIVO_RELATORIO_NFCE_FR'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_EVENTO_NFCE_FR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Arquivo Eventos DANFE NFCE Fast Report'
      FieldName = 'ARQUIVO_EVENTO_NFCE_FR'
      Origin = 'ARQUIVO_EVENTO_NFCE_FR'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_RELATORIO_NFE_FR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Arquivo DANFE NFE Fast Report'
      FieldName = 'ARQUIVO_RELATORIO_NFE_FR'
      Origin = 'ARQUIVO_RELATORIO_NFE_FR'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_EVENTO_NFE_FR: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Arquivo Eventos DANFE NFE Fast Report'
      FieldName = 'ARQUIVO_EVENTO_NFE_FR'
      Origin = 'ARQUIVO_EVENTO_NFE_FR'
      Size = 255
    end
    object fdqConfiguracoesFiscaisIMPRESSORA_NFE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Impressora NF-e'
      FieldName = 'IMPRESSORA_NFE'
      Origin = 'IMPRESSORA_NFE'
      Size = 255
    end
    object fdqConfiguracoesFiscaisIMPRESSORA_NFCE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Impressora NFC-e'
      FieldName = 'IMPRESSORA_NFCE'
      Origin = 'IMPRESSORA_NFCE'
      Size = 255
    end
    object fdqConfiguracoesFiscaisARQUIVO_PATH_PDF: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Diret'#243'rio para salvar arquivos PDF'
      FieldName = 'ARQUIVO_PATH_PDF'
      Origin = 'ARQUIVO_PATH_PDF'
      Size = 255
    end
    object fdqConfiguracoesFiscaisNFCE_PREVIEW: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Visualizar NFCE'
      FieldName = 'NFCE_PREVIEW'
      Origin = 'NFCE_PREVIEW'
      FixedChar = True
      Size = 1
    end
    object fdqConfiguracoesFiscaisNFE_PREVIEW: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Visualizar NFE'
      FieldName = 'NFE_PREVIEW'
      Origin = 'NFE_PREVIEW'
      FixedChar = True
      Size = 1
    end
  end
  object dspConfiguracoesFiscais: TDataSetProvider
    DataSet = fdqConfiguracoesFiscais
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 96
    Top = 16
  end
end
