unit uSMContaReceber;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDQuery, Datasnap.Provider,
  DataSnap.DSProviderDataModuleAdapter, Generics.Collections, Data.FireDACJSONReflect, uRetornoProxy;

type
  TSMContaReceber = class(TDSServerModule)
    dspContaReceber: TDataSetProvider;
    fdqContaReceber: TgbFDQuery;
    fdqContaReceberQuitacao: TgbFDQuery;
    dsContaReceber: TDataSource;
    fdqContaReceberID: TFDAutoIncField;
    fdqContaReceberDH_CADASTRO: TDateTimeField;
    fdqContaReceberDOCUMENTO: TStringField;
    fdqContaReceberDESCRICAO: TStringField;
    fdqContaReceberVL_TITULO: TFMTBCDField;
    fdqContaReceberVL_QUITADO: TFMTBCDField;
    fdqContaReceberVL_ABERTO: TFMTBCDField;
    fdqContaReceberQT_PARCELA: TIntegerField;
    fdqContaReceberNR_PARCELA: TIntegerField;
    fdqContaReceberDT_VENCIMENTO: TDateField;
    fdqContaReceberSEQUENCIA: TStringField;
    fdqContaReceberBO_VENCIDO: TStringField;
    fdqContaReceberOBSERVACAO: TBlobField;
    fdqContaReceberID_PESSOA: TIntegerField;
    fdqContaReceberID_CONTA_ANALISE: TIntegerField;
    fdqContaReceberID_CENTRO_RESULTADO: TIntegerField;
    fdqContaReceberSTATUS: TStringField;
    fdqContaReceberJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    fdqContaReceberJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqContaReceberJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqContaReceberQuitacaoID: TFDAutoIncField;
    fdqContaReceberQuitacaoDH_CADASTRO: TDateTimeField;
    fdqContaReceberQuitacaoVL_QUITACAO: TFMTBCDField;
    fdqContaReceberQuitacaoOBSERVACAO: TBlobField;
    fdqContaReceberQuitacaoID_CONTA_RECEBER: TIntegerField;
    fdqContaReceberID_CHAVE_PROCESSO: TIntegerField;
    fdqContaReceberJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqContaReceberJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    fdqContaReceberID_FORMA_PAGAMENTO: TIntegerField;
    fdqContaReceberJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqContaReceberQuitacaoID_TIPO_QUITACAO: TIntegerField;
    fdqContaReceberQuitacaoDT_QUITACAO: TDateField;
    fdqContaReceberQuitacaoID_FILIAL: TIntegerField;
    fdqContaReceberQuitacaoID_CONTA_CORRENTE: TIntegerField;
    fdqContaReceberQuitacaoVL_DESCONTO: TFMTBCDField;
    fdqContaReceberQuitacaoVL_ACRESCIMO: TFMTBCDField;
    fdqContaReceberQuitacaoNR_ITEM: TIntegerField;
    fdqContaReceberQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    fdqContaReceberQuitacaoVL_TOTAL: TFMTBCDField;
    fdqContaReceberQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqContaReceberVL_ACRESCIMO: TFMTBCDField;
    fdqContaReceberVL_DECRESCIMO: TFMTBCDField;
    fdqContaReceberVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqContaReceberDT_COMPETENCIA: TDateField;
    fdqContaReceberQuitacaoID_CONTA_ANALISE: TIntegerField;
    fdqContaReceberQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqContaReceberID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqContaReceberTP_DOCUMENTO_ORIGEM: TStringField;
    fdqContaReceberQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqContaReceberQuitacaoTP_DOCUMENTO_ORIGEM: TStringField;
    fdqContaReceberQuitacaoSTATUS: TStringField;
    fdqContaReceberDT_DOCUMENTO: TDateField;
    fdqContaReceberID_CONTA_CORRENTE: TIntegerField;
    fdqContaReceberJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqContaReceberQuitacaoCHEQUE_SACADO: TStringField;
    fdqContaReceberQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    fdqContaReceberQuitacaoCHEQUE_BANCO: TStringField;
    fdqContaReceberQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    fdqContaReceberQuitacaoCHEQUE_AGENCIA: TStringField;
    fdqContaReceberQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    fdqContaReceberQuitacaoCHEQUE_NUMERO: TIntegerField;
    fdqContaReceberQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    fdqContaReceberQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqContaReceberQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    fdqContaReceberQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    fdqContaReceberID_FILIAL: TIntegerField;
    fdqContaReceberBO_NEGOCIADO: TStringField;
    fdqContaReceberBO_GERADO_POR_NEGOCIACAO: TStringField;
    fdqContaReceberQuitacaoVL_JUROS: TFMTBCDField;
    fdqContaReceberQuitacaoVL_MULTA: TFMTBCDField;
    fdqContaReceberID_CARTEIRA: TIntegerField;
    fdqContaReceberVL_JUROS: TFMTBCDField;
    fdqContaReceberPERC_JUROS: TFMTBCDField;
    fdqContaReceberVL_MULTA: TFMTBCDField;
    fdqContaReceberPERC_MULTA: TFMTBCDField;
    fdqContaReceberJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqContaReceberQuitacaoPERC_JUROS: TFMTBCDField;
    fdqContaReceberQuitacaoPERC_MULTA: TFMTBCDField;
    fdqContaReceberQuitacaoPERC_ACRESCIMO: TFMTBCDField;
    fdqContaReceberQuitacaoPERC_DESCONTO: TFMTBCDField;
    fdqContaReceberQuitacaoVL_TROCO: TFMTBCDField;
    fdqContaReceberQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    fdqContaReceberQuitacaoID_USUARIO_BAIXA: TIntegerField;
    fdqContaReceberQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    fdqContaReceberVL_NEGOCIADO: TFMTBCDField;
    fdqContaReceberVL_TITULO_ANTES_NEGOCIACAO: TFMTBCDField;
    fdqContaReceberBO_BOLETO_EMITIDO: TStringField;
    fdqContaReceberBOLETO_NOSSO_NUMERO: TIntegerField;
    fdqContaReceberDH_EMISSAO_BOLETO: TDateTimeField;
    fdqContaReceberBOLETO_LINHA_DIGITAVEL: TStringField;
    fdqContaReceberBOLETO_CODIGO_BARRAS: TStringField;
  private


  public
    procedure GerarContaReceber(AValue: String);
    function RemoverContaReceber(AValue: Integer) : Boolean;
    function PodeExcluir(AValue: Integer) : Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaReceber, AIdChaveProcesso: Integer;
      AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean;
    function GetIdContaReceberPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
    function GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber: Integer): TList<Integer>;
    function GetTotaisContaReceber(const AIdsContaReceber: String): TFDJSONDatasets;
    function PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Integer;
    function ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Integer;
    function GetValorJuros(const AIdContaReceber: Integer): Double;
    function GetValorMulta(const AIdContaReceber: Integer): Double;
    function GetValorAberto(const AIdContaReceber: Integer): Double;
    procedure AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
      const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);
    function GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: String): TFDJSONDatasets;
    procedure CancelarBoletoEmitido(const AIdsContaReceber: String);
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uContaReceberServ;

{$R *.dfm}

{ TSMContaReceber }

function TSMContaReceber.PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Integer;
begin
  result := TContaReceberServ.PessoaPossuiContaReceberEmAberto(AIdPessoa);
end;

function TSMContaReceber.ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Integer;
begin
  result := TContaReceberServ.ProcessoPossuiContaReceberEmAberto(AIdChaveProcesso);
end;

procedure TSMContaReceber.GerarContaReceber(AValue : String);
begin
  TContaReceberServ.GerarContaReceber(AValue);
end;

function TSMContaReceber.GetIdContaReceberPeloIdDaQuitacao(
  AIdQuitacao: Integer): Integer;
begin
  result := TContaReceberServ.GetIdContaReceberPeloIdDaQuitacao(AIdQuitacao);
end;

function TSMContaReceber.GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber: Integer): TList<Integer>;
begin
  Result := TContaReceberServ.GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber);
end;

function TSMContaReceber.GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: String): TFDJSONDatasets;
begin
  Result := TContaReceberServ.GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber);
end;

function TSMContaReceber.GetTotaisContaReceber(const AIdsContaReceber: String): TFDJSONDatasets;
begin
  Result := TContaReceberServ.GetTotaisContaReceber(AIdsContaReceber);
end;

function TSMContaReceber.GetValorAberto(const AIdContaReceber: Integer): Double;
begin
  Result := TContaReceberServ.GetValorAberto(AIdContaReceber);
end;

function TSMContaReceber.GetValorJuros(const AIdContaReceber: Integer): Double;
begin
  Result := TContaReceberServ.GetValorJuros(AIdContaReceber);
end;

function TSMContaReceber.GetValorMulta(const AIdContaReceber: Integer): Double;
begin
  Result := TContaReceberServ.GetValorMulta(AIdContaReceber);
end;

function TSMContaReceber.PodeExcluir(AValue: Integer): Boolean;
begin
  Result := TContaReceberServ.PodeExcluir(AValue);
end;

function TSMContaReceber.RemoverContaReceber(AValue: Integer) : Boolean;
begin
  Result := TContaReceberServ.RemoverContaReceber(AValue);
end;

procedure TSMContaReceber.AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
  const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);
begin
  TContaReceberServ.AtualizarDadosBoletoContasReceber(AIdContaReceber,
    ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras);
end;

function TSMContaReceber.AtualizarMovimentosContaCorrente(AIdContaReceber, AIdChaveProcesso: Integer;
  AGerarContraPartida: Boolean) : Boolean;
begin
  Result := TContaReceberServ.AtualizarMovimentosContaCorrente(AIdContaReceber, AIdChaveProcesso,
    AGerarContraPartida);
end;

procedure TSMContaReceber.CancelarBoletoEmitido(const AIdsContaReceber: String);
begin
  TContaReceberServ.CancelarBoletoEmitido(AIdsContaReceber);
end;

function TSMContaReceber.RemoverMovimentosContaCorrente(AIdContaReceber: Integer;
  AGerarContraPartida: Boolean): Boolean;
begin
  Result := TContaReceberServ.RemoverMovimentosContaCorrente(AIdContaReceber, AGerarContraPartida);
end;

end.

