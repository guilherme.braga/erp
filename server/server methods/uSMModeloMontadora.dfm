object SMModeloMontadora: TSMModeloMontadora
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqModeloMontadora: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT modelo_montadora.*'
      '      ,montadora.descricao AS JOIN_DESCRICAO_MONTADORA '
      '  FROM modelo_montadora'
      
        'INNER JOIN montadora ON modelo_montadora.id_montadora = montador' +
        'a.id '
      'WHERE modelo_montadora.id = :ID')
    Left = 32
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqModeloMontadoraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqModeloMontadoraDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqModeloMontadoraBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqModeloMontadoraOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqModeloMontadoraID_MONTADORA: TIntegerField
      DisplayLabel = 'C'#243'digo da Montadora'
      FieldName = 'ID_MONTADORA'
      Origin = 'ID_MONTADORA'
      Required = True
    end
    object fdqModeloMontadoraJOIN_DESCRICAO_MONTADORA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Montadora'
      FieldName = 'JOIN_DESCRICAO_MONTADORA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dspModeloMontadora: TDataSetProvider
    DataSet = fdqModeloMontadora
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 8
  end
  object dsModeloMontadora: TDataSource
    DataSet = fdqModeloMontadora
    Left = 96
    Top = 8
  end
  object fdqListaModeloMontadora: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM modelo_montadora')
    Left = 56
    Top = 72
    object FDAutoIncField1: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object StringField1: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object StringField2: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object BlobField1: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'C'#243'digo da Montadora'
      FieldName = 'ID_MONTADORA'
      Origin = 'ID_MONTADORA'
      Required = True
    end
  end
  object dspListaModeloMontadora: TDataSetProvider
    DataSet = fdqListaModeloMontadora
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 88
    Top = 72
  end
end
