unit uSMMontadora;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, Datasnap.Provider, FireDAC.Comp.Client,
  uGBFDQuery, DataSnap.DSProviderDataModuleAdapter, uMontadoraProxy;

type
  TSMMontadora = class(TDSServerModule)
    fdqMontadora: TgbFDQuery;
    dspMontadora: TDataSetProvider;
    dsMontadora: TDataSource;
    fdqMontadoraID: TFDAutoIncField;
    fdqMontadoraDESCRICAO: TStringField;
    fdqMontadoraBO_ATIVO: TStringField;
    fdqMontadoraOBSERVACAO: TBlobField;
    fdqListaMontadora: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    StringField1: TStringField;
    StringField2: TStringField;
    BlobField1: TBlobField;
    dspListaMontadora: TDataSetProvider;
  private
    { Private declarations }
  public
    function GetMontadora(const AIdMontadora: Integer): TMontadoraProxy;
    function GerarMontadora(const AMontadora: TMontadoraProxy): Integer;
    function ExisteMontadora(const ADescricaoMontadora: String): Boolean;
    function GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uMontadoraServ;

{$R *.dfm}

{ TSMMontadora }

function TSMMontadora.ExisteMontadora(const ADescricaoMontadora: String): Boolean;
begin
  result := TMontadoraServ.ExisteMontadora(ADescricaoMontadora);
end;

function TSMMontadora.GerarMontadora(const AMontadora: TMontadoraProxy): Integer;
begin
  result := TMontadoraServ.GerarMontadora(AMontadora);
end;

function TSMMontadora.GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
begin
  result := TMontadoraServ.GetIdMontadoraPorDescricao(ADescricaoMontadora);
end;

function TSMMontadora.GetMontadora(const AIdMontadora: Integer): TMontadoraProxy;
begin
  result := TMontadoraServ.GetMontadora(AIdMontadora);
end;

end.

