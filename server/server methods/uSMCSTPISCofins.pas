unit uSMCSTPISCofins;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter;

type
  TSMCSTPISCofins = class(TDSServerModule)
    fdqCSTPISCofins: TgbFDQuery;
    fdqCSTPISCofinsID: TFDAutoIncField;
    fdqCSTPISCofinsCODIGO_CST: TStringField;
    fdqCSTPISCofinsDESCRICAO: TStringField;
    dspCSTPISCofins: TDataSetProvider;
  private
    { Private declarations }
  public
    function GetCSTPISCofins(AIdCSTPISCofins: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uCSTPISCofinsProxy, uCSTPISCofinsServ, REST.JSON;

{$R *.dfm}

{ TSMCSTPISCofins }

function TSMCSTPISCofins.GetCSTPISCofins(
  AIdCSTPISCofins: Integer): String;
begin
  result := TJson.ObjectToJsonString(TCSTPISCofinsServ.GetCSTPISCofins(AIdCSTPISCofins));
end;

end.

