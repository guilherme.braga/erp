object SMCNAE: TSMCNAE
  OldCreateOrder = False
  Height = 142
  Width = 273
  object fdqCNAE: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from CNAE where id = :id')
    Left = 72
    Top = 56
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqCNAEID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCNAEDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCNAESEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
    end
    object fdqCNAENIVEL: TIntegerField
      DisplayLabel = 'N'#237'vel'
      FieldName = 'NIVEL'
      Origin = 'NIVEL'
      Required = True
    end
    object fdqCNAESECAO: TStringField
      DisplayLabel = 'Se'#231#227'o'
      FieldName = 'SECAO'
      Origin = 'SECAO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dspCNAE: TDataSetProvider
    DataSet = fdqCNAE
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 159
    Top = 56
  end
end
