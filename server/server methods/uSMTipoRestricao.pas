unit uSMTipoRestricao;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMTipoRestricao = class(TDSServerModule)
    fdqTipoRestricao: TgbFDQuery;
    dspTipoRestricao: TDataSetProvider;
    fdqTipoRestricaoID: TFDAutoIncField;
    fdqTipoRestricaoDESCRICAO: TStringField;
    fdqTipoRestricaoBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

