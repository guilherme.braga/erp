unit uSMProduto;

interface

uses System.SysUtils, System.Classes, System.Json,
     Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
     FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
     FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
     FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
     FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect, uGBFDQuery, uProdutoProxy,
     uProdutoGradeProxy, Generics.Collections;

type
  TSMProduto = class(TDSServerModule)
    fdqProduto: TgbFDQuery;
    dspProduto: TDataSetProvider;
    fdqConsulta: TFDQuery;
    fdqGrupoProduto: TgbFDQuery;
    dspGrupoProduto: TDataSetProvider;
    fdqSubGrupo: TgbFDQuery;
    dspSubGrupo: TDataSetProvider;
    fdqCategoria: TgbFDQuery;
    dspCategoria: TDataSetProvider;
    fdqSubCategoria: TgbFDQuery;
    dspSubCategoria: TDataSetProvider;
    fdqMarca: TgbFDQuery;
    dspMarca: TDataSetProvider;
    fdqProdutoID: TFDAutoIncField;
    fdqProdutoDESCRICAO: TStringField;
    fdqProdutoBO_ATIVO: TStringField;
    fdqProdutoID_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoID_SUB_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoID_MARCA: TIntegerField;
    fdqProdutoID_CATEGORIA: TIntegerField;
    fdqProdutoID_SUB_CATEGORIA: TIntegerField;
    fdqProdutoID_LINHA: TIntegerField;
    fdqProdutoID_MODELO: TIntegerField;
    fdqProdutoID_UNIDADE_ESTOQUE: TIntegerField;
    fdqProdutoTIPO: TStringField;
    fdqProdutoCODIGO_BARRA: TStringField;
    fdqProdutoJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    fdqProdutoJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField;
    fdqProdutoJOIN_DESCRICAO_MARCA: TStringField;
    fdqProdutoJOIN_DESCRICAO_CATEGORIA: TStringField;
    fdqProdutoJOIN_DESCRICAO_SUB_CATEGORIA: TStringField;
    fdqProdutoJOIN_DESCRICAO_LINHA: TStringField;
    fdqProdutoJOIN_DESCRICAO_MODELO: TStringField;
    fdqProdutoJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField;
    fdqGrupoProdutoID: TFDAutoIncField;
    fdqGrupoProdutoDESCRICAO: TStringField;
    fdqSubGrupoID: TFDAutoIncField;
    fdqSubGrupoDESCRICAO: TStringField;
    fdqCategoriaID: TFDAutoIncField;
    fdqCategoriaDESCRICAO: TStringField;
    fdqSubCategoriaID: TFDAutoIncField;
    fdqSubCategoriaDESCRICAO: TStringField;
    fdqMarcaID: TFDAutoIncField;
    fdqMarcaDESCRICAO: TStringField;
    fdqLinha: TgbFDQuery;
    dspLinha: TDataSetProvider;
    fdqLinhaID: TFDAutoIncField;
    fdqLinhaDESCRICAO: TStringField;
    fdqLinhaOBSERVACAO: TBlobField;
    fdqSubCategoriaOBSERVACAO: TBlobField;
    fdqCategoriaOBSERVACAO: TBlobField;
    fdqModelo: TgbFDQuery;
    dspModelo: TDataSetProvider;
    dspUnidadeEstoque: TDataSetProvider;
    fdqUnidadeEstoque: TgbFDQuery;
    fdqModeloID: TFDAutoIncField;
    fdqModeloDESCRICAO: TStringField;
    fdqModeloOBSERVACAO: TBlobField;
    fdqUnidadeEstoqueID: TFDAutoIncField;
    fdqUnidadeEstoqueSIGLA: TStringField;
    fdqUnidadeEstoqueDESCRICAO: TStringField;
    dsProduto: TDataSource;
    fdqProdutoCodigoBarra: TgbFDQuery;
    fdqProdutoFilial: TgbFDQuery;
    fdqProdutoFornecedor: TgbFDQuery;
    fdqProdutoCodigoBarraID: TFDAutoIncField;
    fdqProdutoCodigoBarraCODIGO: TStringField;
    fdqProdutoCodigoBarraID_PRODUTO: TIntegerField;
    fdqProdutoFornecedorID: TFDAutoIncField;
    fdqProdutoFornecedorREFERENCIA: TStringField;
    fdqProdutoFornecedorID_PRODUTO: TIntegerField;
    fdqProdutoFilialID: TFDAutoIncField;
    fdqProdutoFilialQT_ESTOQUE: TFMTBCDField;
    fdqProdutoFilialLOCALIZACAO: TStringField;
    fdqProdutoFilialQT_ESTOQUE_MINIMO: TFMTBCDField;
    fdqProdutoFilialQT_ESTOQUE_MAXIMO: TFMTBCDField;
    fdqProdutoFilialID_PRODUTO: TIntegerField;
    fdqProdutoFilialID_FILIAL: TIntegerField;
    fdqProdutoFilialJOIN_DESCRICAO_FILIAL: TStringField;
    fdqGrupoProdutoOBSERVACAO: TBlobField;
    fdqProdutoTabelaPreco: TgbFDQuery;
    fdqProdutoTabelaPrecoID: TFDAutoIncField;
    fdqProdutoTabelaPrecoDESCRICAO: TStringField;
    fdqProdutoTabelaPrecoBO_ATIVO: TStringField;
    fdqProdutoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoTabelaPrecoID_MARCA: TIntegerField;
    fdqProdutoTabelaPrecoID_CATEGORIA: TIntegerField;
    fdqProdutoTabelaPrecoID_SUB_CATEGORIA: TIntegerField;
    fdqProdutoTabelaPrecoID_LINHA: TIntegerField;
    fdqProdutoTabelaPrecoID_MODELO: TIntegerField;
    fdqProdutoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField;
    fdqProdutoTabelaPrecoTIPO: TStringField;
    fdqProdutoTabelaPrecoCODIGO_BARRA: TStringField;
    dspProdutoTabelaPreco: TDataSetProvider;
    fdqTodosGruposProduto: TgbFDQuery;
    dspTodosGruposProduto: TDataSetProvider;
    fdqTodosGruposProdutoID: TFDAutoIncField;
    fdqTodosGruposProdutoDESCRICAO: TStringField;
    fdqProdutoGrupoTabelaPreco: TgbFDQuery;
    dspProdutoGrupoTabelaPreco: TDataSetProvider;
    fdqProdutoGrupoTabelaPrecoID: TFDAutoIncField;
    fdqProdutoGrupoTabelaPrecoDESCRICAO: TStringField;
    fdqProdutoGrupoTabelaPrecoBO_ATIVO: TStringField;
    fdqProdutoGrupoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_MARCA: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_CATEGORIA: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_SUB_CATEGORIA: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_LINHA: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_MODELO: TIntegerField;
    fdqProdutoGrupoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField;
    fdqProdutoGrupoTabelaPrecoTIPO: TStringField;
    fdqProdutoGrupoTabelaPrecoCODIGO_BARRA: TStringField;
    fdqProdutoGrupoTabelaPrecoVL_VENDA: TFMTBCDField;
    fdqSubGrupoID_GRUPO_PRODUTO: TIntegerField;
    fdqSubGrupoJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    fdqProdutoID_COR: TIntegerField;
    fdqProdutoJOIN_DESCRICAO_COR: TStringField;
    fdqProdutoFORMA_AQUISICAO: TStringField;
    fdqProdutoID_NCM: TIntegerField;
    fdqProdutoID_NCM_ESPECIALIZADO: TIntegerField;
    fdqProdutoJOIN_DESCRICAO_NCM: TStringField;
    fdqProdutoJOIN_CODIGO_NCM: TIntegerField;
    fdqProdutoJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField;
    fdqProdutoTributacaoPorUnidade: TgbFDQuery;
    fdqProdutoTributacaoPorUnidadeID: TFDAutoIncField;
    fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_ICMS: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_ICMS_ST: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_BASE_CALCULO_IPI: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_PIS: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_COFINS: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_PIS_ST: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeVL_COFINS_ST: TFMTBCDField;
    fdqProdutoTributacaoPorUnidadeID_PRODUTO: TIntegerField;
    fdqProdutoTIPO_TRIBUTACAO_PIS_COFINS_ST: TIntegerField;
    fdqProdutoTIPO_TRIBUTACAO_IPI: TIntegerField;
    fdqProdutoEXCECAO_IPI: TIntegerField;
    fdqProdutoID_REGRA_IMPOSTO: TIntegerField;
    fdqProdutoJOIN_DESCRICAO_REGRA_IMPOSTO: TStringField;
    fdqProdutoORIGEM_DA_MERCADORIA: TIntegerField;
    fdqProdutoFilialQT_ESTOQUE_CONDICIONAL: TFMTBCDField;
    fdqProdutoFilialVL_CUSTO_MEDIO: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_CUSTO: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_FRETE: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_SEGURO: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_ICMS_ST: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_IPI: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_DESCONTO: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    fdqProdutoFilialVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField;
    fdqProdutoFilialVL_CUSTO_MEDIO_IMPOSTO: TFMTBCDField;
    fdqProdutoFilialVL_CUSTO_OPERACIONAL: TFMTBCDField;
    fdqProdutoFilialVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMO_FRETE: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMO_SEGURO: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMO_ICMS_ST: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMO_IPI: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMO_DESCONTO: TFMTBCDField;
    fdqProdutoFilialPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    fdqProdutoFilialPERC_CUSTO_OPERACIONAL: TFMTBCDField;
    fdqProdutoFilialVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    fdqProdutoFornecedorID_PESSOA: TIntegerField;
    fdqProdutoFornecedorJOIN_NOME_FORNECEDOR: TStringField;
    fdqProdutoDH_CADASTRO: TDateTimeField;
    fdqProdutoMontadora: TgbFDQuery;
    fdqProdutoModeloMontadora: TgbFDQuery;
    fdqProdutoModeloMontadoraID: TFDAutoIncField;
    fdqProdutoMontadoraID: TFDAutoIncField;
    fdqProdutoMontadoraID_PRODUTO: TIntegerField;
    fdqProdutoMontadoraNR_ITEM: TIntegerField;
    fdqProdutoMontadoraMONTADORA: TStringField;
    fdqProdutoModeloMontadoraID_PRODUTO: TIntegerField;
    fdqProdutoModeloMontadoraNR_ITEM_MONTADORA: TIntegerField;
    fdqProdutoModeloMontadoraMODELO: TStringField;
    fdqProdutoMontadoraREFERENCIA: TStringField;
    fdqProdutoMontadoraESPECIFICACAO: TBlobField;
    fdqProdutoBO_PRODUTO_AGRUPADOR: TStringField;
    fdqProdutoBO_PRODUTO_AGRUPADO: TStringField;
    fdqProdutoID_PRODUTO_AGRUPADOR: TIntegerField;
    fdqProdutoID_TAMANHO: TIntegerField;
    fdqProdutoJOIN_DESCRICAO_TAMANHO: TStringField;
    procedure fdqProdutoAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure GerarMovimentacaoProduto(AValue: String);
    function GetChaveProcessoMovimentacaoInicial(const AIdProduto: Integer): Integer;
    function RemoverMovimentacaoProduto(AValue: Integer): Boolean;
    function GetProximoID: Integer;
    function CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
      const AIdProduto: Integer): Integer;
    function GetProduto(AIdProduto: Integer): TFDJSONDataSets;
    function GetTodosGruposProduto(): TFDJSONDataSets;
    function GetProdutoPeloCodigoBarra(ACodigoBarraProduto: String): TFDJSONDataSets;
    function GetProdutos(AIdsProdutos: string): TFDJSONDataSets;
    function GetProdutosCompleto(AIdsProdutos: string): TFDJSONDataSets;
    function GetProdutoFilial(const AIdProduto: Integer;
      AIdFilial: Integer): String;
    function GetProdutoFilialProxy(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
    function GetListaProduto(const AIdsProdutos: String; AIdFilial: Integer): String;
    function ExisteMovimentacoesDiferentesDaMovimentacaoInicial(
      const AIdProduto, AIdFilial: Integer): Boolean;
    function DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
    function GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
    function GerarProdutos(const AProdutosEmGrade: String): Boolean;
    function VisaoProdutoGrade(const AIdGradeProduto, AIdProdutoAgrupador: Integer): TFDJsonDatasets;
    procedure InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
    procedure LimparRegistrosDaEmissaoEtiqueta;
    function GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
    function VerificarTipoMercadoria(const AIdProduto: Integer): Boolean;
    function VerificarTipoProduto(const AIdProduto: Integer): Boolean;
    procedure RefazerEAN13TodosProdutos;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uProdutoServ, uFilialServ, REST.JSON, uProdutoGradeServ, uEmissaoEtiquetaServ;

{$R *.dfm}

{ TSMProduto }

function TSMProduto.CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
  const AIdProduto: Integer): Integer;
begin
  result := TProdutoServ.CodigoDeBarraJaCadastrado(ACodigoBarra, AIdProduto);
end;

function TSMProduto.DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
begin
  result := TProdutoServ.DuplicarProduto(AIdProdutoOrigem);
end;

function TSMProduto.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(const AIdProduto,
  AIdFilial: Integer): Boolean;
begin
  result := TProdutoServ.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto, AIdFilial);
end;

procedure TSMProduto.fdqProdutoAfterPost(DataSet: TDataSet);
begin
  {Validar se existe filial para o produto}
  if not TProdutoServ.ExisteFilialParaProduto(fdqProdutoID.AsInteger, TFilialServ.GetIdFilial) then
    TProdutoServ.CriarFilialParaProduto(fdqProdutoID.AsInteger, TFilialServ.GetIdFilial);
end;

function TSMProduto.GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
begin
  result := TProdutoServ.GerarCodigoBarraEAN13(ACodigoProduto);
end;

procedure TSMProduto.GerarMovimentacaoProduto(AValue : String);
begin
  TProdutoMovimentoServ.GerarMovimentacaoProduto(AValue);
end;

function TSMProduto.GerarProdutos(const AProdutosEmGrade: String): Boolean;
var
  listaProdutosEmGrade: TList<TProdutoGradeProxy>;
begin
  listaProdutosEmGrade := TList<TProdutoGradeProxy>.Create;
  try
    listaProdutosEmGrade := TJSON.JsonToObject<TList<TProdutoGradeProxy>>(AProdutosEmGrade);
    result := TProdutoGradeServ.GerarProdutos(listaProdutosEmGrade);
  finally
  end;
end;

function TSMProduto.GetChaveProcessoMovimentacaoInicial(const AIdProduto: Integer): Integer;
begin
  result := TProdutoMovimentoServ.GetChaveProcessoMovimentacaoInicial(AIdProduto);
end;

function TSMProduto.GetListaProduto(const AIdsProdutos: String;
  AIdFilial: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TProdutoServ.GetLitaProdutos(AIdsProdutos, AIdFilial));
end;

function TSMProduto.GetProduto(AIdProduto: Integer): TFDJSONDataSets;
begin
  result := TProdutoServ.GetProduto(AIdProduto);
end;

function TSMProduto.GetProdutoFilial(const AIdProduto: Integer;
  AIdFilial: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TProdutoServ.GetProdutoFilial(AIdProduto, AIdFilial));
end;

function TSMProduto.GetProdutoFilialProxy(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
begin
  result := TProdutoServ.GetProdutoFilial(AIdProduto, AIdFilial);
end;

function TSMProduto.GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
begin
  result := TProdutoServ.GetProdutoFiscal(AIdProduto, AIdPessoa, AFilial);
end;

function TSMProduto.GetProdutoPeloCodigoBarra(
  ACodigoBarraProduto: String): TFDJSONDataSets;
begin
  result := TProdutoServ.GetProdutoPeloCodigoBarra(ACodigoBarraProduto);
end;

function TSMProduto.GetProximoID: Integer;
begin
  result := TProdutoServ.GetProximoID;
end;

function TSMProduto.GetTodosGruposProduto: TFDJSONDataSets;
begin
  result := TProdutoServ.GetTodosGruposProduto;
end;

procedure TSMProduto.InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
begin
  TEmissaoEtiquetaServ.InserirProdutoParaEmissaoEtiqueta(AIdProduto);
end;

procedure TSMProduto.LimparRegistrosDaEmissaoEtiqueta;
begin
  TEmissaoEtiquetaServ.LimparRegistrosDaEmissaoEtiqueta();
end;

procedure TSMProduto.RefazerEAN13TodosProdutos;
begin
  TProdutoServ.RefazerEAN13TodosProdutos;
end;

function TSMProduto.RemoverMovimentacaoProduto(AValue : Integer) : Boolean;
begin
  result := TProdutoMovimentoServ.RemoverMovimentacaoProduto(AValue);
end;

function TSMProduto.VerificarTipoMercadoria(const AIdProduto: Integer): Boolean;
begin
  result := TProdutoServ.VerificarTipoMercadoria(AIdProduto);
end;

function TSMProduto.VerificarTipoProduto(const AIdProduto: Integer): Boolean;
begin
  result := TProdutoServ.VerificarTipoProduto(AIdProduto);
end;

function TSMProduto.VisaoProdutoGrade(const AIdGradeProduto, AIdProdutoAgrupador: Integer): TFDJsonDatasets;
begin
  result := TProdutoGradeServ.VisaoProdutoGrade(AIdGradeProduto, AIdProdutoAgrupador);
end;

function TSMProduto.GetProdutos(AIdsProdutos: string): TFDJSONDataSets;
begin
  Result := TProdutoServ.GetProdutos(AIdsProdutos);
end;

function TSMProduto.GetProdutosCompleto(AIdsProdutos: string): TFDJSONDataSets;
begin
  Result := TProdutoServ.GetProdutosCompleto(AIdsProdutos);
end;

end.

