unit uSMTamanho;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter, uTamanhoProxy;

type
  TSMTamanho = class(TDSServerModule)
    fdqTamanho: TgbFDQuery;
    dspTamanho: TDataSetProvider;
    fdqTamanhoID: TFDAutoIncField;
    fdqTamanhoDESCRICAO: TStringField;
    fdqTamanhoBO_ATIVO: TStringField;
    fdqListaTamanho: TgbFDQuery;
    dspListaTamanho: TDataSetProvider;
    fdqListaTamanhoID: TFDAutoIncField;
    fdqListaTamanhoDESCRICAO: TStringField;
    fdqListaTamanhoBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    function GetTamanho(const AIdTamanho: Integer): TTamanhoProxy;
    function GerarTamanho(const ATamanho: TTamanhoProxy): Integer;
    function ExisteTamanho(const ADescricaoTamanho: String): Boolean;
    function GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uTamanhoServ;

{$R *.dfm}

{ TSMTamanho }

function TSMTamanho.ExisteTamanho(const ADescricaoTamanho: String): Boolean;
begin
  result := TTamanhoServ.ExisteTamanho(ADescricaoTamanho);
end;

function TSMTamanho.GerarTamanho(const ATamanho: TTamanhoProxy): Integer;
begin
  result := TTamanhoServ.GerarTamanho(ATamanho);
end;

function TSMTamanho.GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
begin
  result := TTamanhoServ.GetIdTamanhoPorDescricao(ADescricaoTamanho);
end;

function TSMTamanho.GetTamanho(const AIdTamanho: Integer): TTamanhoProxy;
begin
  result := TTamanhoServ.GetTamanho(AIdTamanho);
end;

end.

