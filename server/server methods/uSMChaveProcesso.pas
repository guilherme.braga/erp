unit uSMChaveProcesso;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter;

type
  TSMChaveProcesso = class(TDSServerModule)
    fdqChaveProcesso: TgbFDQuery;
    dspChaveProcesso: TDataSetProvider;
    fdqChaveProcessoID: TFDAutoIncField;
    fdqChaveProcessoCHAVE: TStringField;
    fdqChaveProcessoORIGEM: TStringField;
  private

  public
    function NovaChaveProcesso(AOrigem   : string;
                               AIdOrigem : Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uChaveProcessoServ;

{$R *.dfm}

{ TSMChaveProcesso }

function TSMChaveProcesso.NovaChaveProcesso(AOrigem   : string;
                                            AIdOrigem : Integer): Integer;
begin
  result := TChaveProcessoServ.NovaChaveProcesso(AOrigem, AIdOrigem);
end;

end.

