unit uSMRegraImposto;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMRegraImposto = class(TDSServerModule)
    fdqRegraImposto: TgbFDQuery;
    dspRegraImposto: TDataSetProvider;
    fdqRegraImpostoID: TFDAutoIncField;
    fdqRegraImpostoID_NCM: TIntegerField;
    fdqRegraImpostoDESCRICAO: TStringField;
    fdqRegraImpostoEXCECAO_IPI: TIntegerField;
    fdqRegraImpostoID_NCM_ESPECIALIZADO: TIntegerField;
    fdqRegraImpostoJOIN_DESCRICAO_NCM: TStringField;
    fdqRegraImpostoJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField;
    fdqRegraImpostoFiltro: TgbFDQuery;
    dsRegraImposto: TDataSource;
    fdqRegraImpostoFiltroID: TFDAutoIncField;
    fdqRegraImpostoFiltroTIPO_EMPRESA_PESSOA: TStringField;
    fdqRegraImpostoFiltroFORMA_AQUISICAO: TStringField;
    fdqRegraImpostoFiltroREGIME_TRIBUTARIO_DESTINATARIO: TIntegerField;
    fdqRegraImpostoFiltroREGIME_TRIBUTARIO_EMITENTE: TIntegerField;
    fdqRegraImpostoFiltroCRT_EMITENTE: TIntegerField;
    fdqRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_EMITENTE: TStringField;
    fdqRegraImpostoFiltroBO_CONTRIBUINTE_IPI_EMITENTE: TStringField;
    fdqRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_DESTINATARIO: TStringField;
    fdqRegraImpostoFiltroBO_CONTRIBUINTE_IPI_DESTINATARIO: TStringField;
    fdqRegraImpostoFiltroID_ZONEAMENTO_EMITENTE: TIntegerField;
    fdqRegraImpostoFiltroID_ZONEAMENTO_DESTINATARIO: TIntegerField;
    fdqRegraImpostoFiltroID_REGIME_ESPECIAL_EMITENTE: TIntegerField;
    fdqRegraImpostoFiltroID_REGIME_ESPECIAL_DESTINATARIO: TIntegerField;
    fdqRegraImpostoFiltroID_SITUACAO_ESPECIAL: TIntegerField;
    fdqRegraImpostoFiltroID_REGRA_IMPOSTO: TIntegerField;
    fdqRegraImpostoFiltroID_ESTADO_ORIGEM: TIntegerField;
    fdqRegraImpostoFiltroID_ESTADO_DESTINO: TIntegerField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_EMITENTE: TStringField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO: TStringField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE: TStringField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO: TStringField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_SITUACAO_ESPECIAL: TStringField;
    fdqRegraImpostoFiltroJOIN_UF_ESTADO_ORIGEM: TStringField;
    fdqRegraImpostoFiltroJOIN_UF_ESTADO_DESTINO: TStringField;
    fdqRegraImpostoFiltroDH_INICIO_VIGENCIA: TDateTimeField;
    fdqRegraImpostoFiltroDH_FIM_VIGENCIA: TDateTimeField;
    fdqRegraImpostoJOIN_CODIGO_NCM: TIntegerField;
    fdqRegraImpostoFiltroORIGEM_DA_MERCADORIA: TIntegerField;
    fdqRegraImpostoFiltroID_IMPOSTO_PIS_COFINS: TIntegerField;
    fdqRegraImpostoFiltroID_IMPOSTO_ICMS: TIntegerField;
    fdqRegraImpostoFiltroID_IMPOSTO_IPI: TIntegerField;
    fdqRegraImpostoFiltroJOIN_CST_CSOSN: TStringField;
    fdqRegraImpostoFiltroJOIN_CST_IPI: TStringField;
    fdqRegraImpostoFiltroJOIN_CST_PIS_COFINS: TStringField;
    fdqRegraImpostoFiltroID_OPERACAO_FISCAL: TIntegerField;
    fdqRegraImpostoFiltroJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField;
  private
    { Private declarations }
  public
    function GetRegraImposto(AIdRegraImposto: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uRegraImpostoProxy, uRegraImpostoServ, REST.JSON;

{$R *.dfm}

{ TSMRegraImposto }

function TSMRegraImposto.GetRegraImposto(
  AIdRegraImposto: Integer): String;
begin
  result := TJson.ObjectToJsonString(TRegraImpostoServ.GetRegraImposto(AIdRegraImposto));
end;

end.

