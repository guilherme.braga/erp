unit uSMModeloMontadora;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uGBFDQuery, DataSnap.DSProviderDataModuleAdapter, uMontadoraProxy;

type
  TSMModeloMontadora = class(TDSServerModule)
    fdqModeloMontadora: TgbFDQuery;
    dspModeloMontadora: TDataSetProvider;
    dsModeloMontadora: TDataSource;
    fdqModeloMontadoraID: TFDAutoIncField;
    fdqModeloMontadoraDESCRICAO: TStringField;
    fdqModeloMontadoraBO_ATIVO: TStringField;
    fdqModeloMontadoraOBSERVACAO: TBlobField;
    fdqModeloMontadoraID_MONTADORA: TIntegerField;
    fdqModeloMontadoraJOIN_DESCRICAO_MONTADORA: TStringField;
    fdqListaModeloMontadora: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    StringField1: TStringField;
    StringField2: TStringField;
    BlobField1: TBlobField;
    IntegerField1: TIntegerField;
    dspListaModeloMontadora: TDataSetProvider;
  private
    { Private declarations }
  public
    function GetModeloMontadora(const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
    function GerarModeloMontadora(const AModeloMontadora: TModeloMontadoraProxy): Integer;
    function ExisteModelo(const ADescricaoModelo: String; const ADescricaoMontadora: String): Boolean;

  end;

implementation

uses uMontadoraServ;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TSMModeloMontadora }

function TSMModeloMontadora.ExisteModelo(const ADescricaoModelo, ADescricaoMontadora: String): Boolean;
begin
  result := TModeloMontadoraServ.ExisteModelo(ADescricaoModelo, ADescricaoMontadora);
end;

function TSMModeloMontadora.GerarModeloMontadora(
  const AModeloMontadora: TModeloMontadoraProxy): Integer;
begin
  result := TModeloMontadoraServ.GerarModeloMontadora(AModeloMontadora);
end;

function TSMModeloMontadora.GetModeloMontadora(
  const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
begin
  result := TModeloMontadoraServ.GetModeloMontadora(AIdModeloMontadora);
end;

end.

