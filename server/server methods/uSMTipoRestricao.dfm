object SMTipoRestricao: TSMTipoRestricao
  OldCreateOrder = False
  Height = 201
  Width = 283
  object fdqTipoRestricao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT '
      '  *'
      'FROM '
      '  tipo_restricao'
      'WHERE'
      '  id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqTipoRestricaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object fdqTipoRestricaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqTipoRestricaoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspTipoRestricao: TDataSetProvider
    DataSet = fdqTipoRestricao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
