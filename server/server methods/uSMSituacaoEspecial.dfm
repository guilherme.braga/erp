object SMSituacaoEspecial: TSMSituacaoEspecial
  OldCreateOrder = False
  Height = 242
  Width = 321
  object fdqSituacaoEspecial: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT * FROM situacao_especial where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqSituacaoEspecialID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqSituacaoEspecialDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqSituacaoEspecialBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspSituacaoEspecial: TDataSetProvider
    DataSet = fdqSituacaoEspecial
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
  object fdqPessoaProdutoSituacaoEspecial: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ppse.* '
      '      ,p.nome as JOIN_NOME_PESSOA'
      '      ,pr.descricao as JOIN_DESCRICAO_PRODUTO'
      'FROM pessoa_produto_situacao_especial ppse '
      'INNER JOIN pessoa p ON ppse.id_pessoa = p.id'
      'INNER JOIN produto pr ON ppse.id_produto = pr.id'
      'WHERE ppse.id = :id')
    Left = 72
    Top = 96
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqPessoaProdutoSituacaoEspecialID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPessoaProdutoSituacaoEspecialID_SITUACAO_ESPECIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Situa'#231#227'o Especial'
      FieldName = 'ID_SITUACAO_ESPECIAL'
      Origin = 'ID_SITUACAO_ESPECIAL'
      Required = True
    end
    object fdqPessoaProdutoSituacaoEspecialID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqPessoaProdutoSituacaoEspecialID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object fdqPessoaProdutoSituacaoEspecialJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqPessoaProdutoSituacaoEspecialJOIN_DESCRICAO_PRODUTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspPessoaProdutoSituacaoEspecial: TDataSetProvider
    DataSet = fdqPessoaProdutoSituacaoEspecial
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 104
    Top = 96
  end
end
