unit uSMLoteRecebimento;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter,
  Data.FireDACJSONReflect;

type
  TSMLoteRecebimento = class(TDSServerModule)
    fdqLoteRecebimento: TgbFDQuery;
    fdqLoteRecebimentoTitulo: TgbFDQuery;
    dsLoteRecebimento: TDataSource;
    dspLoteRecebimento: TDataSetProvider;
    fdqLoteRecebimentoQuitacao: TgbFDQuery;
    fdqLoteRecebimentoID: TFDAutoIncField;
    fdqLoteRecebimentoDH_CADASTRO: TDateTimeField;
    fdqLoteRecebimentoSTATUS: TStringField;
    fdqLoteRecebimentoID_FILIAL: TIntegerField;
    fdqLoteRecebimentoID_PESSOA_USUARIO: TIntegerField;
    fdqLoteRecebimentoID_CHAVE_PROCESSO: TIntegerField;
    fdqLoteRecebimentoDH_EFETIVACAO: TDateTimeField;
    fdqLoteRecebimentoOBSERVACAO: TBlobField;
    fdqLoteRecebimentoJOIN_FANTASIA_FILIAL: TStringField;
    fdqLoteRecebimentoJOIN_USUARIO: TStringField;
    fdqLoteRecebimentoJOIN_CHAVE_PROCESSO: TStringField;
    fdqLoteRecebimentoTituloID: TFDAutoIncField;
    fdqLoteRecebimentoTituloVL_ACRESCIMO: TFMTBCDField;
    fdqLoteRecebimentoTituloVL_DESCONTO: TFMTBCDField;
    fdqLoteRecebimentoTituloID_LOTE_Recebimento: TIntegerField;
    fdqLoteRecebimentoTituloID_CONTA_Receber: TIntegerField;
    fdqLoteRecebimentoTituloDH_CADASTRO: TDateTimeField;
    fdqLoteRecebimentoTituloDOCUMENTO: TStringField;
    fdqLoteRecebimentoTituloDESCRICAO: TStringField;
    fdqLoteRecebimentoTituloVL_TITULO: TFMTBCDField;
    fdqLoteRecebimentoTituloVL_QUITADO: TFMTBCDField;
    fdqLoteRecebimentoTituloVL_ABERTO: TFMTBCDField;
    fdqLoteRecebimentoTituloQT_PARCELA: TIntegerField;
    fdqLoteRecebimentoTituloNR_PARCELA: TIntegerField;
    fdqLoteRecebimentoTituloDT_VENCIMENTO: TDateField;
    fdqLoteRecebimentoTituloSEQUENCIA: TStringField;
    fdqLoteRecebimentoTituloBO_VENCIDO: TStringField;
    fdqLoteRecebimentoTituloOBSERVACAO: TBlobField;
    fdqLoteRecebimentoTituloID_PESSOA: TIntegerField;
    fdqLoteRecebimentoTituloID_CONTA_ANALISE: TIntegerField;
    fdqLoteRecebimentoTituloID_CENTRO_RESULTADO: TIntegerField;
    fdqLoteRecebimentoTituloSTATUS: TStringField;
    fdqLoteRecebimentoTituloID_CHAVE_PROCESSO: TIntegerField;
    fdqLoteRecebimentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqLoteRecebimentoTituloDT_COMPETENCIA: TDateField;
    fdqLoteRecebimentoTituloID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqLoteRecebimentoTituloTP_DOCUMENTO_ORIGEM: TStringField;
    fdqLoteRecebimentoQuitacaoID: TFDAutoIncField;
    fdqLoteRecebimentoQuitacaoOBSERVACAO: TBlobField;
    fdqLoteRecebimentoQuitacaoID_TIPO_QUITACAO: TIntegerField;
    fdqLoteRecebimentoQuitacaoDT_QUITACAO: TDateField;
    fdqLoteRecebimentoQuitacaoID_CONTA_CORRENTE: TIntegerField;
    fdqLoteRecebimentoQuitacaoVL_DESCONTO: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoVL_ACRESCIMO: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoVL_QUITACAO: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoNR_ITEM: TIntegerField;
    fdqLoteRecebimentoQuitacaoPERC_ACRESCIMO: TBCDField;
    fdqLoteRecebimentoQuitacaoPERC_DESCONTO: TBCDField;
    fdqLoteRecebimentoQuitacaoVL_TOTAL: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoID_CONTA_ANALISE: TIntegerField;
    fdqLoteRecebimentoQuitacaoID_LOTE_Recebimento: TIntegerField;
    fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqLoteRecebimentoQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqLoteRecebimentoVL_QUITADO: TFMTBCDField;
    fdqLoteRecebimentoVL_ACRESCIMO: TFMTBCDField;
    fdqLoteRecebimentoVL_DESCONTO: TFMTBCDField;
    fdqLoteRecebimentoVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqLoteRecebimentoVL_ABERTO: TFMTBCDField;
    fdqLoteRecebimentoTituloID_FORMA_PAGAMENTO: TIntegerField;
    fdqLoteRecebimentoQuitacaoDOCUMENTO: TStringField;
    fdqLoteRecebimentoQuitacaoDESCRICAO: TStringField;
    fdqLoteRecebimentoTituloDT_DOCUMENTO: TDateField;
    fdqLoteRecebimentoTituloID_CONTA_CORRENTE: TIntegerField;
    fdqLoteRecebimentoTituloID_FILIAL: TIntegerField;
    fdqLoteRecebimentoQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    fdqLoteRecebimentoQuitacaoCHEQUE_SACADO: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_BANCO: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    fdqLoteRecebimentoQuitacaoCHEQUE_AGENCIA: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_NUMERO: TIntegerField;
    fdqLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    fdqLoteRecebimentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqLoteRecebimentoTituloBO_NEGOCIADO: TStringField;
    fdqLoteRecebimentoQuitacaoVL_JUROS: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoPERC_JUROS: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoVL_MULTA: TFMTBCDField;
    fdqLoteRecebimentoQuitacaoPERC_MULTA: TFMTBCDField;
    fdqLoteRecebimentoTituloVL_JUROS: TFMTBCDField;
    fdqLoteRecebimentoTituloPERC_JUROS: TFMTBCDField;
    fdqLoteRecebimentoTituloVL_MULTA: TFMTBCDField;
    fdqLoteRecebimentoTituloPERC_MULTA: TFMTBCDField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_NOME_PESSOA_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_ANALISE_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_CENTRO_RESULTADO_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_ORIGEM_CHAVE_PROCESSO_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_ID_ORIGEM_CHAVE_PROCESSO_CR: TIntegerField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_FORMA_PAGAMENTO_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_CORRENTE_CR: TStringField;
    fdqLoteRecebimentoTituloJOIN_DESCRICAO_CARTEIRA_CR: TStringField;
    fdqLoteRecebimentoTituloID_CARTEIRA: TIntegerField;
    fdqLoteRecebimentoTituloJOIN_NOME_PESSOA: TStringField;
    fdqLoteRecebimentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    fdqLoteRecebimentoQuitacaoID_USUARIO_BAIXA: TIntegerField;
    fdqLoteRecebimentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
  private
    { Private declarations }
  public
    function RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
    function GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
    function RemoverLancamentosAgrupados(AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
    function GetContasAReceber(AIdsContaReceber : string): TFDJSONDataSets;
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  uLoteRecebimentoProxy,
  uLoteRecebimentoServ;

{$R *.dfm}

{ TSMLoteRecebimento }

function TSMLoteRecebimento.GerarLancamentosAgrupados(
  AChaveProcesso: Integer): string;
var
  IdLoteRecebimento : Integer;
begin
  IdLoteRecebimento := TLoteRecebimentoServ.GetIdLoteRecebimentoPelaChaveProcesso(AChaveProcesso);
  Result := TLoteRecebimentoServ.GerarLancamentosAgrupados(IdLoteRecebimento);
end;

function TSMLoteRecebimento.RemoverLancamentosAgrupados(
  AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
begin
  Result := TLoteRecebimentoServ.RemoverLancamentosAgrupados(AIdLoteRecebimento, AGerarContraPartida);
end;

function TSMLoteRecebimento.GetContasAReceber(
  AIdsContaReceber: string): TFDJSONDataSets;
begin
  Result := TLoteRecebimentoServ.GetContasAReceber(AIdsContaReceber);
end;

function TSMLoteRecebimento.RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
begin
  Result := TLoteRecebimentoServ.RemoverQuitacoesIndividuais(AIdLoteRecebimento);
end;



end.

