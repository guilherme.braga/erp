unit uSMEmpresaFilial;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery, Datasnap.DSProviderDataModuleAdapter, uFilialProxy;

type
  TSMEmpresaFilial = class(TDSServerModule)
    fdqEmpresa: TgbFDQuery;
    dspEmpresa: TDataSetProvider;
    dsEmpresa: TDataSource;
    fdqFilial: TgbFDQuery;
    fdqEmpresaID: TFDAutoIncField;
    fdqFilialID: TFDAutoIncField;
    fdqFilialFANTASIA: TStringField;
    fdqFilialRAZAO_SOCIAL: TStringField;
    fdqFilialCNPJ: TStringField;
    fdqFilialIE: TStringField;
    fdqFilialDT_FUNDACAO: TDateField;
    fdqFilialID_EMPRESA: TIntegerField;
    fdqFilialNR_ITEM: TIntegerField;
    fdqFilialID_ZONEAMENTO: TIntegerField;
    fdqFilialREGIME_TRIBUTARIO: TStringField;
    fdqFilialJOIN_DESCRICAO_ZONEAMENTO: TStringField;
    dsFilial: TDataSource;
    fdqFilialBO_MATRIZ: TStringField;
    fdqFilialID_CIDADE: TIntegerField;
    fdqFilialLOGRADOURO: TStringField;
    fdqFilialNUMERO: TStringField;
    fdqFilialCOMPLEMENTO: TStringField;
    fdqFilialBAIRRO: TStringField;
    fdqFilialCEP: TStringField;
    fdqFilialJOIN_DESCRICAO_CIDADE: TStringField;
    fdqEmpresaDESCRICAO: TStringField;
    fdqFilialTELEFONE: TStringField;
    fdqFilialTIPO_EMPRESA: TStringField;
    fdqFilialCRT: TIntegerField;
    fdqFilialBO_CONTRIBUINTE_ICMS: TStringField;
    fdqFilialBO_CONTRIBUINTE_IPI: TStringField;
    fdqFilialEMAIL: TStringField;
    fdqFilialSITE: TStringField;
    fdqFilialID_CNAE: TIntegerField;
    fdqFilialIM: TStringField;
    fdqFilialJOIN_SEQUENCIA_CNAE: TStringField;
    fdqFilialJOIN_DESCRICAO_CNAE: TStringField;
  private
    { Private declarations }
  public
    function GetFantasia(AIdFilial: Integer): String;
    function OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
    function GetCRT(AIdFilial: Integer): Integer;
    function GetFilial(const AIdFilial: Integer): TFilialProxy;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFilialServ;

{$R *.dfm}

{ TSMEmpresaFilial }

function TSMEmpresaFilial.GetCRT(AIdFilial: Integer): Integer;
begin
  result := TFilialServ.GetCRT(AIdFilial);
end;

function TSMEmpresaFilial.GetFantasia(AIdFilial: Integer): String;
begin
  result := TFilialServ.GetFantasia(AIdFilial);
end;

function TSMEmpresaFilial.GetFilial(const AIdFilial: Integer): TFilialProxy;
begin
  result := TFilialServ.GetFilial(AIdFilial);
end;

function TSMEmpresaFilial.OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
begin
  result := TFilialServ.OptanteSimplesNacional(AIdFilial);
end;

end.

