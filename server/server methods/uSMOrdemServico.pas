unit uSMOrdemServico;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, DataSnap.DSProviderDataModuleAdapter,
  uGBFDQuery;

type
  TSMOrdemServico = class(TDSServerModule)
    dspOrdemServico: TDataSetProvider;
    dsOrdemServico: TDataSource;
    fdqOrdemServicoChecklist: TgbFDQuery;
    fdqOrdemServicoChecklistID: TFDAutoIncField;
    fdqOrdemServicoChecklistBO_MARCADO: TStringField;
    fdqOrdemServicoChecklistID_CHECKLIST_ITEM: TIntegerField;
    fdqOrdemServicoChecklistID_ORDEM_SERVICO: TIntegerField;
    fdqOrdemServicoChecklistJOIN_DESCRICAO_CHECKLIST: TStringField;
    fdqOrdemServicoProduto: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    IntegerField1: TIntegerField;
    FMTBCDField1: TFMTBCDField;
    FMTBCDField2: TFMTBCDField;
    FMTBCDField3: TFMTBCDField;
    FMTBCDField4: TFMTBCDField;
    FMTBCDField5: TFMTBCDField;
    FMTBCDField6: TFMTBCDField;
    FMTBCDField7: TFMTBCDField;
    FMTBCDField8: TFMTBCDField;
    IntegerField2: TIntegerField;
    fdqOrdemServicoProdutoID_PRODUTO: TIntegerField;
    fdqOrdemServicoProdutoJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqOrdemServicoProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    fdqOrdemServicoProdutoJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    fdqOrdemServicoParcela: TgbFDQuery;
    fdqOrdemServicoParcelaID: TFDAutoIncField;
    fdqOrdemServicoParcelaDOCUMENTO: TStringField;
    fdqOrdemServicoParcelaVL_TITULO: TFMTBCDField;
    fdqOrdemServicoParcelaQT_PARCELA: TIntegerField;
    fdqOrdemServicoParcelaNR_PARCELA: TIntegerField;
    fdqOrdemServicoParcelaDT_VENCIMENTO: TDateField;
    fdqOrdemServicoParcelaID_ORDEM_SERVICO: TIntegerField;
    fdqOrdemServicoProdutoVL_UNITARIO: TFMTBCDField;
    fdqOrdemServicoChecklistJOIN_NR_ITEM_CHECKLIST: TIntegerField;
    fdqOrdemServicoParcelaOBSERVACAO: TStringField;
    fdqOrdemServicoParcelaCHEQUE_SACADO: TStringField;
    fdqOrdemServicoParcelaCHEQUE_DOC_FEDERAL: TStringField;
    fdqOrdemServicoParcelaCHEQUE_BANCO: TStringField;
    fdqOrdemServicoParcelaCHEQUE_DT_EMISSAO: TDateField;
    fdqOrdemServicoParcelaCHEQUE_AGENCIA: TStringField;
    fdqOrdemServicoParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    fdqOrdemServicoParcelaCHEQUE_NUMERO: TIntegerField;
    fdqOrdemServicoParcelaID_OPERADORA_CARTAO: TIntegerField;
    fdqOrdemServicoParcelaID_FORMA_PAGAMENTO: TIntegerField;
    fdqOrdemServicoParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField;
    fdqOrdemServicoParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqOrdemServicoServico: TgbFDQuery;
    fdqOrdemServicoServicoID: TFDAutoIncField;
    fdqOrdemServicoServicoNR_ITEM: TIntegerField;
    fdqOrdemServicoServicoQUANTIDADE: TFMTBCDField;
    fdqOrdemServicoServicoVL_DESCONTO: TFMTBCDField;
    fdqOrdemServicoServicoPERC_DESCONTO: TFMTBCDField;
    fdqOrdemServicoServicoVL_ACRESCIMO: TFMTBCDField;
    fdqOrdemServicoServicoPERC_ACRESCIMO: TFMTBCDField;
    fdqOrdemServicoServicoVL_BRUTO: TFMTBCDField;
    fdqOrdemServicoServicoVL_LIQUIDO: TFMTBCDField;
    fdqOrdemServicoServicoVL_CUSTO: TFMTBCDField;
    fdqOrdemServicoServicoVL_UNITARIO: TFMTBCDField;
    fdqOrdemServicoServicoID_ORDEM_SERVICO: TIntegerField;
    fdqOrdemServicoServicoID_SERVICO: TIntegerField;
    fdqOrdemServicoServicoJOIN_DESCRICAO_SERVICO: TStringField;
    fdqOrdemServicoServicoJOIN_VALOR_VENDA_SERVICO: TFMTBCDField;
    fdqOrdemServicoServicoJOIN_DURACAO_SERVICO: TTimeField;
    fdqOrdemServicoServicoBO_SERVICO_TERCEIRO: TStringField;
    fdqOrdemServico: TgbFDQuery;
    fdqOrdemServicoID: TFDAutoIncField;
    fdqOrdemServicoDH_CADASTRO: TDateTimeField;
    fdqOrdemServicoDH_FECHAMENTO: TDateTimeField;
    fdqOrdemServicoDH_ENTRADA: TDateTimeField;
    fdqOrdemServicoDH_PREVISAO_TERMINO: TDateTimeField;
    fdqOrdemServicoSTATUS: TStringField;
    fdqOrdemServicoVL_TOTAL_BRUTO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_DESCONTO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_DESCONTO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_LIQUIDO_PRODUTO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_BRUTO_SERVICO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_DESCONTO_SERVICO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_DESCONTO_SERVICO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_LIQUIDO_SERVICO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_BRUTO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_LIQUIDO_SERVICO_TERCEIRO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_DESCONTO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_DESCONTO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_ACRESCIMO: TFMTBCDField;
    fdqOrdemServicoPERC_TOTAL_ACRESCIMO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_LIQUIDO: TFMTBCDField;
    fdqOrdemServicoVL_TOTAL_BRUTO: TFMTBCDField;
    fdqOrdemServicoVL_PAGAMENTO: TFMTBCDField;
    fdqOrdemServicoVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField;
    fdqOrdemServicoID_PESSOA_USUARIO: TIntegerField;
    fdqOrdemServicoID_FILIAL: TIntegerField;
    fdqOrdemServicoID_CHAVE_PROCESSO: TIntegerField;
    fdqOrdemServicoID_CHECKLIST: TIntegerField;
    fdqOrdemServicoID_TABELA_PRECO: TIntegerField;
    fdqOrdemServicoID_CENTRO_RESULTADO: TIntegerField;
    fdqOrdemServicoID_CONTA_ANALISE: TIntegerField;
    fdqOrdemServicoID_PLANO_PAGAMENTO: TIntegerField;
    fdqOrdemServicoID_SITUACAO: TIntegerField;
    fdqOrdemServicoID_OPERACAO: TIntegerField;
    fdqOrdemServicoID_EQUIPAMENTO: TIntegerField;
    fdqOrdemServicoID_USUARIO_RECEPCAO: TIntegerField;
    fdqOrdemServicoID_PESSOA: TIntegerField;
    fdqOrdemServicoID_VEICULO: TIntegerField;
    fdqOrdemServicoOBSERVACAO: TBlobField;
    fdqOrdemServicoPROBLEMA_RELATADO: TBlobField;
    fdqOrdemServicoPROBLEMA_ENCONTRADO: TBlobField;
    fdqOrdemServicoSOLUCAO_TECNICA: TBlobField;
    fdqOrdemServicoKM: TIntegerField;
    fdqOrdemServicoTANQUE: TStringField;
    fdqOrdemServicoJOIN_USUARIO: TStringField;
    fdqOrdemServicoJOIN_FANTASIA_FILIAL: TStringField;
    fdqOrdemServicoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_SITUACAO: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_CHECKLIST: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_OPERACAO: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_EQUIPAMENTO: TStringField;
    fdqOrdemServicoJOIN_NOME_PESSOA: TStringField;
    fdqOrdemServicoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqOrdemServicoJOIN_SERIE_EQUIPAMENTO: TStringField;
    fdqOrdemServicoJOIN_IMEI_EQUIPAMENTO: TStringField;
    fdqOrdemServicoJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField;
    fdqOrdemServicoJOIN_PLACA_VEICULO: TStringField;
    fdqOrdemServicoJOIN_ANO_VEICULO: TIntegerField;
    fdqOrdemServicoJOIN_COMBUSTIVEL_VEICULO: TStringField;
    fdqOrdemServicoJOIN_COR_VEICULO: TStringField;
    fdqOrdemServicoJOIN_MODELO_VEICULO: TStringField;
    fdqOrdemServicoJOIN_MARCA_VEICULO: TStringField;
    fdqOrdemServicoParcelaID_CARTEIRA: TIntegerField;
    fdqOrdemServicoParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqOrdemServicoEQUIPAMENTO: TStringField;
    fdqOrdemServicoEQUIPAMENTO_SERIE: TStringField;
    fdqOrdemServicoEQUIPAMENTO_IMEI: TStringField;
    fdqOrdemServicoParcelaCHEQUE_DT_VENCIMENTO: TDateField;
  private
    { Private declarations }
  public
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function Efetivar(AIdChaveProcesso: Integer; OrdemServicoProxy: String): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GetId(AIdChaveProcesso: Integer): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uOrdemServicoServ, uDmConnection;

{$R *.dfm}

{ TSMOrdemServico }

function TSMOrdemServico.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TOrdemServicoServ.Cancelar(AIdChaveProcesso);
end;

function TSMOrdemServico.Efetivar(AIdChaveProcesso: Integer; OrdemServicoProxy: String): Boolean;
begin
  result := TOrdemServicoServ.Efetivar(AIdChaveProcesso, OrdemServicoProxy);
end;

function TSMOrdemServico.GetId(AIdChaveProcesso: Integer): Integer;
begin
  result := TOrdemServicoServ.GetId(AIdChaveProcesso);
end;

function TSMOrdemServico.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TOrdemServicoServ.PodeEstornar(AIdChaveProcesso);
end;

function TSMOrdemServico.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  result := TOrdemServicoServ.PodeExcluir(AIdChaveProcesso);
end;

end.

