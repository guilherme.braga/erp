object SMCFOP: TSMCFOP
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqCFOP: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select *'
      'from CFOP'
      'where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCFOPID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object fdqCFOPCODIGO: TIntegerField
      DisplayLabel = 'C'#243'digo CFOP'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Required = True
    end
    object fdqCFOPDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o CFOP'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
  end
  object dspCFOP: TDataSetProvider
    DataSet = fdqCFOP
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
