object SMServico: TSMServico
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqServico: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT s.*'
      'FROM servico s'
      'WHERE s.id = :ID')
    Left = 32
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqServicoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqServicoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqServicoVL_CUSTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqServicoPERC_LUCRO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      Origin = 'PERC_LUCRO'
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqServicoVL_VENDA: TFMTBCDField
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      Required = True
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqServicoDURACAO: TTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'DURACAO'
      Origin = 'DURACAO'
    end
    object fdqServicoBO_SERVICO_TERCEIRO: TStringField
      DisplayLabel = 'Servi'#231'o de Terceiro'
      FieldName = 'BO_SERVICO_TERCEIRO'
      Origin = 'BO_SERVICO_TERCEIRO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqServicoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dspServico: TDataSetProvider
    DataSet = fdqServico
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 8
  end
end
