unit uSMMotivo;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDQuery, DataSnap.DSProviderDataModuleAdapter,
  Datasnap.Provider;

type
  TSMMotivo = class(TDSServerModule)
    fdqMotivo: TgbFDQuery;
    fdqMotivoID: TFDAutoIncField;
    fdqMotivoDESCRICAO: TStringField;
    fdqMotivoTIPO: TStringField;
    dspMotivo: TDataSetProvider;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection;

{$R *.dfm}

end.

