unit uSMNegociacaoContaReceber;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMNegociacaoContaReceber = class(TDSServerModule)
    fdqNegociacaoContaReceber: TgbFDQuery;
    dspNegociacaoContaReceber: TDataSetProvider;
    dsNegociacaoContaReceber: TDataSource;
    fdqNegociacaoContaReceberParcela: TgbFDQuery;
    fdqNegociacaoContaReceberGerado: TgbFDQuery;
    fdqNegociacaoContaReceberID: TFDAutoIncField;
    fdqNegociacaoContaReceberDH_CADASTRO: TDateTimeField;
    fdqNegociacaoContaReceberVL_TOTAL: TFMTBCDField;
    fdqNegociacaoContaReceberVL_ACRESCIMO: TFMTBCDField;
    fdqNegociacaoContaReceberVL_DESCONTO: TFMTBCDField;
    fdqNegociacaoContaReceberVL_LIQUIDO: TFMTBCDField;
    fdqNegociacaoContaReceberVL_ENTRADA: TFMTBCDField;
    fdqNegociacaoContaReceberDT_PARCELA_INICIAL: TDateField;
    fdqNegociacaoContaReceberDT_COMPETENCIA_INICIAL: TDateField;
    fdqNegociacaoContaReceberSTATUS: TStringField;
    fdqNegociacaoContaReceberID_PESSOA: TIntegerField;
    fdqNegociacaoContaReceberID_FILIAL: TIntegerField;
    fdqNegociacaoContaReceberID_CHAVE_PROCESSO: TIntegerField;
    fdqNegociacaoContaReceberID_FORMA_PAGAMENTO: TIntegerField;
    fdqNegociacaoContaReceberID_PLANO_PAGAMENTO: TIntegerField;
    fdqNegociacaoContaReceberID_CONTA_ANALISE: TIntegerField;
    fdqNegociacaoContaReceberID_PESSOA_USUARIO: TIntegerField;
    fdqNegociacaoContaReceberID_CENTRO_RESULTADO: TIntegerField;
    fdqNegociacaoContaReceberID_CONTA_CORRENTE: TIntegerField;
    fdqNegociacaoContaReceberOBSERVACAO: TBlobField;
    fdqNegociacaoContaReceberID_CARTEIRA: TIntegerField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqNegociacaoContaReceberJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqNegociacaoContaReceberJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqNegociacaoContaReceberJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqNegociacaoContaReceberJOIN_NOME_USUARIO: TStringField;
    fdqNegociacaoContaReceberParcelaID: TFDAutoIncField;
    fdqNegociacaoContaReceberParcelaDOCUMENTO: TStringField;
    fdqNegociacaoContaReceberParcelaDESCRICAO: TStringField;
    fdqNegociacaoContaReceberParcelaVL_TITULO: TFMTBCDField;
    fdqNegociacaoContaReceberParcelaQT_PARCELA: TIntegerField;
    fdqNegociacaoContaReceberParcelaNR_PARCELA: TIntegerField;
    fdqNegociacaoContaReceberParcelaDT_VENCIMENTO: TDateField;
    fdqNegociacaoContaReceberParcelaDT_COMPETENCIA: TDateField;
    fdqNegociacaoContaReceberParcelaDT_DOCUMENTO: TDateField;
    fdqNegociacaoContaReceberParcelaID_NEGOCIACAO_CONTA_RECEBER: TIntegerField;
    fdqNegociacaoContaReceberParcelaSEQUENCIA: TStringField;
    fdqNegociacaoContaReceberGeradoID: TFDAutoIncField;
    fdqNegociacaoContaReceberGeradoID_NEGOCIACAO_CONTA_RECEBER: TIntegerField;
    fdqNegociacaoContaReceberGeradoID_CONTA_RECEBER: TIntegerField;
    fdqNegociacaoContaReceberAnteriro: TgbFDQuery;
    FDAutoIncField1: TFDAutoIncField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    fdqNegociacaoContaReceberDOCUMENTO: TStringField;
    fdqNegociacaoContaReceberDESCRICAO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

