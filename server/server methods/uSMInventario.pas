unit uSMInventario;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMInventario = class(TDSServerModule)
    fdqInventario: TgbFDQuery;
    fdqInventarioID: TFDAutoIncField;
    fdqInventarioJOIN_FANTASIA_FILIAL: TStringField;
    fdqInventarioJOIN_USUARIO: TStringField;
    fdqInventarioJOIN_CHAVE_PROCESSO: TStringField;
    fdqInventarioProduto: TgbFDQuery;
    fdqInventarioProdutoID: TFDAutoIncField;
    fdqInventarioProdutoID_PRODUTO: TIntegerField;
    fdqInventarioProdutoJOIN_DESCRICAO_PRODUTO: TStringField;
    dsInventario: TDataSource;
    dspAjusteEstoque: TDataSetProvider;
    fdqInventarioDH_CADASTRO: TDateTimeField;
    fdqInventarioTIPO: TStringField;
    fdqInventarioID_FILIAL: TIntegerField;
    fdqInventarioID_PESSOA_USUARIO: TIntegerField;
    fdqInventarioID_CHAVE_PROCESSO: TIntegerField;
    fdqInventarioDH_INICIO: TDateTimeField;
    fdqInventarioDH_FIM: TDateTimeField;
    fdqInventarioOBSERVACAO: TBlobField;
    fdqInventarioProdutoID_INVENTARIO: TIntegerField;
    fdqInventarioCONTAGEM_ESCOLHIDA: TIntegerField;
    fdqInventarioSTATUS: TStringField;
    fdqInventarioProdutoQUANTIDADE_ESTOQUE: TFMTBCDField;
    fdqInventarioProdutoPRIMEIRA_CONTAGEM: TFMTBCDField;
    fdqInventarioProdutoSEGUNDA_CONTAGEM: TFMTBCDField;
    fdqInventarioProdutoTERCEIRA_CONTAGEM: TFMTBCDField;
    fdqInventarioProdutoJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    fdqInventarioDH_FECHAMENTO: TDateTimeField;
    fdqInventarioID_MOTIVO: TIntegerField;
    fdqInventarioJOIN_DESCRICAO_MOTIVO: TStringField;
  private
    { Private declarations }
  public
    function GetInventario(const AIdInventario: Integer): String;
    function GerarInventario(const AInventario: String): Integer;
    function Efetivar(AIdChaveProcesso: Integer): Boolean;
    function Reabrir(AIdChaveProcesso: Integer): Boolean;
  end;

implementation

Uses uInventarioProxy, uInventarioServ, rest.json;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TSMInventario }

function TSMInventario.Efetivar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TInventarioServ.Efetivar(AIdChaveProcesso);
end;

function TSMInventario.GerarInventario(
  const AInventario: String): Integer;
begin
  result := TInventarioServ.GerarInventario(
    TJson.JsonToObject<TInventarioProxy>(AInventario));
end;

function TSMInventario.GetInventario(
  const AIdInventario: Integer): String;
begin
  result := TJson.ObjectToJsonString(TInventarioServ.GetInventario(AIdInventario));
end;

function TSMInventario.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  result := TInventarioServ.Reabrir(AIdChaveProcesso);
end;

end.

