object SMLotePagamento: TSMLotePagamento
  OldCreateOrder = False
  Height = 248
  Width = 340
  object fdqLotePagamento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select lp.*'
      '      ,f.fantasia as JOIN_FANTASIA_FILIAL'
      '      ,pu.usuario as JOIN_USUARIO'
      '      ,cp.origem as JOIN_CHAVE_PROCESSO'
      'from lote_pagamento lp'
      'inner join filial f on lp.id_filial = f.id'
      'inner join pessoa_usuario pu on lp.id_pessoa_usuario = pu.id'
      'inner join chave_processo cp on lp.id_chave_processo = cp.id'
      'where lp.id = :id')
    Left = 56
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqLotePagamentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLotePagamentoDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object fdqLotePagamentoSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object fdqLotePagamentoID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'd. Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqLotePagamentoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqLotePagamentoID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqLotePagamentoDH_EFETIVACAO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Efetiva'#231#227'o'
      FieldName = 'DH_EFETIVACAO'
      Origin = 'DH_EFETIVACAO'
    end
    object fdqLotePagamentoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqLotePagamentoVL_QUITADO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoVL_QUITADO_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado L'#237'quido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoVL_ABERTO: TFMTBCDField
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Fantasia Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqLotePagamentoJOIN_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLotePagamentoJOIN_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
  end
  object fdqLotePagamentoTitulo: TgbFDQuery
    MasterSource = dsLotePagamento
    MasterFields = 'ID'
    DetailFields = 'ID_LOTE_PAGAMENTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT '
      '  lpt.*  '
      '  ,cp.DH_CADASTRO'
      '  ,cp.DOCUMENTO                    '
      '  ,cp.DESCRICAO                    '
      '  ,cp.VL_TITULO                    '
      '  ,cp.VL_QUITADO                   '
      '  ,cp.VL_ABERTO                    '
      '  ,cp.QT_PARCELA                   '
      '  ,cp.NR_PARCELA                   '
      '  ,cp.DT_VENCIMENTO                '
      '  ,cp.SEQUENCIA                    '
      '  ,cp.BO_VENCIDO                   '
      '  ,cp.OBSERVACAO                    '
      '  ,cp.ID_PESSOA                    '
      '  ,cp.ID_CONTA_ANALISE             '
      '  ,cp.ID_CENTRO_RESULTADO          '
      '  ,cp.STATUS                       '
      '  ,cp.ID_CHAVE_PROCESSO            '
      '  ,cp.ID_FORMA_PAGAMENTO           '
      '  ,cp.VL_ACRESCIMO as VL_ACRESCIMO_CP'
      '  ,cp.VL_DECRESCIMO as VL_DESCONTO_CP                '
      '  ,cp.VL_QUITADO_LIQUIDO           '
      '  ,cp.DT_COMPETENCIA               '
      '  ,cp.ID_DOCUMENTO_ORIGEM          '
      '  ,cp.TP_DOCUMENTO_ORIGEM        '
      '  ,cp.DT_DOCUMENTO'
      '  ,cp.ID_CONTA_CORRENTE     '
      '  ,cp.ID_FILIAL'
      
        '  ,(select nome from pessoa where id = cp.id_pessoa) as JOIN_NOM' +
        'E_PESSOA '
      'FROM lote_pagamento_titulo lpt'
      'INNER JOIN conta_pagar cp'
      'ON lpt.id_conta_pagar = cp.id'
      'WHERE lpt.id_lote_pagamento = :id')
    Left = 56
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqLotePagamentoTituloID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLotePagamentoTituloVL_ACRESCIMO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloVL_DESCONTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloID_LOTE_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'd. Lote Pagamento'
      FieldName = 'ID_LOTE_PAGAMENTO'
      Origin = 'ID_LOTE_PAGAMENTO'
    end
    object fdqLotePagamentoTituloID_CONTA_PAGAR: TIntegerField
      DisplayLabel = 'C'#243'd. Conta PAgar'
      FieldName = 'ID_CONTA_PAGAR'
      Origin = 'ID_CONTA_PAGAR'
      Required = True
    end
    object fdqLotePagamentoTituloDH_CADASTRO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLotePagamentoTituloDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLotePagamentoTituloVL_TITULO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloVL_QUITADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloVL_ABERTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloQT_PARCELA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloNR_PARCELA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloDT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloSEQUENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
      Size = 10
    end
    object fdqLotePagamentoTituloBO_VENCIDO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vencido?'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object fdqLotePagamentoTituloOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_PESSOA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_CONTA_ANALISE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_CENTRO_RESULTADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloSTATUS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_FORMA_PAGAMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Forma Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Quitado T'#237'tulo'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloDT_COMPETENCIA: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      ProviderFlags = []
      Size = 50
    end
    object fdqLotePagamentoTituloVL_ACRESCIMO_CP: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Acr'#233'scimo CP'
      FieldName = 'VL_ACRESCIMO_CP'
      Origin = 'VL_ACRESCIMO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloVL_DESCONTO_CP: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Desconto CP'
      FieldName = 'VL_DESCONTO_CP'
      Origin = 'VL_DECRESCIMO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloDT_DOCUMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_CONTA_CORRENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloID_FILIAL: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      ProviderFlags = []
    end
    object fdqLotePagamentoTituloVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoTituloJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'JOIN_NOME_PESSOA'
      ProviderFlags = []
      Size = 80
    end
  end
  object dsLotePagamento: TDataSource
    DataSet = fdqLotePagamento
    Left = 152
    Top = 24
  end
  object dspLotePagamento: TDataSetProvider
    DataSet = fdqLotePagamento
    Options = [poFetchBlobsOnDemand, poIncFieldProps, poUseQuoteChar]
    Left = 248
    Top = 24
  end
  object fdqLotePagamentoQuitacao: TgbFDQuery
    MasterSource = dsLotePagamento
    MasterFields = 'ID'
    DetailFields = 'ID_LOTE_PAGAMENTO'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT lpq.* '
      '      ,tq.descricao AS JOIN_DESCRICAO_TIPO_QUITACAO '
      '      ,tq.tipo AS JOIN_TIPO_TIPO_QUITACAO'
      '      ,cc.descricao AS JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,ca.descricao AS JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao AS JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,oc.descricao AS JOIN_DESCRICAO_OPERADORA_CARTAO'
      
        '      ,(select nome from pessoa_usuario where id = lpq.id_usuari' +
        'o_baixa) AS JOIN_NOME_USUARIO_BAIXA'
      '  FROM lote_pagamento_quitacao lpq'
      ' INNER JOIN tipo_quitacao  tq ON lpq.id_tipo_quitacao   = tq.id'
      ' INNER JOIN conta_corrente cc ON lpq.id_conta_corrente = cc.id'
      ' INNER JOIN conta_analise  ca ON lpq.id_conta_analise   = ca.id'
      
        ' INNER JOIN centro_resultado cr ON lpq.id_centro_resultado = cr.' +
        'id'
      
        ' LEFT JOIN operadora_cartao oc ON lpq.id_operadora_cartao = oc.i' +
        'd'
      ' WHERE lpq.id_lote_pagamento = :id'
      '')
    Left = 56
    Top = 136
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqLotePagamentoQuitacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqLotePagamentoQuitacaoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqLotePagamentoQuitacaoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object fdqLotePagamentoQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
    end
    object fdqLotePagamentoQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqLotePagamentoQuitacaoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Nr. Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqLotePagamentoQuitacaoPERC_ACRESCIMO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Precision = 7
    end
    object fdqLotePagamentoQuitacaoPERC_DESCONTO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Precision = 7
    end
    object fdqLotePagamentoQuitacaoVL_TOTAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqLotePagamentoQuitacaoID_LOTE_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Lote Pagamento'
      FieldName = 'ID_LOTE_PAGAMENTO'
      Origin = 'ID_LOTE_PAGAMENTO'
    end
    object fdqLotePagamentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLotePagamentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqLotePagamentoQuitacaoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqLotePagamentoQuitacaoDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object fdqLotePagamentoQuitacaoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdqLotePagamentoQuitacaoID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqLotePagamentoQuitacaoCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqLotePagamentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqLotePagamentoQuitacaoCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqLotePagamentoQuitacaoCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqLotePagamentoQuitacaoCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqLotePagamentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqLotePagamentoQuitacaoCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqLotePagamentoQuitacaoVL_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoVL_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object fdqLotePagamentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
    object fdqLotePagamentoQuitacaoID_USUARIO_BAIXA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object fdqLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
  end
end
