unit uSMLocalidade;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.MySQL, uGBFDQuery;

type
  TSMLocalidade = class(TDSServerModule)
    fdqPais: TgbFDQuery;
    fdqPaisdescricao: TStringField;
    dspPais: TDataSetProvider;
    fdqEstado: TgbFDQuery;
    dspEstado: TDataSetProvider;
    fdqEstadoDESCRICAO: TStringField;
    fdqEstadoUF: TStringField;
    fdqEstadoID_PAIS: TIntegerField;
    fdqEstadoJOIN_DESCRICAO_PAIS: TStringField;
    fdqCidade: TgbFDQuery;
    dspCidade: TDataSetProvider;
    fdqCidadeIBGE: TIntegerField;
    fdqCidadeID_ESTADO: TIntegerField;
    fdqCidadeJOIN_UF_ESTADO: TStringField;
    fdqCidadeDESCRICAO: TStringField;
    fdqPaisID: TFDAutoIncField;
    fdqEstadoID: TFDAutoIncField;
    fdqCidadeID: TFDAutoIncField;
    fdqBairro: TgbFDQuery;
    dspBairro: TDataSetProvider;
    fdqCEP: TgbFDQuery;
    dspCEP: TDataSetProvider;
    fdqBairroID: TFDAutoIncField;
    fdqBairroDESCRICAO: TStringField;
    fdqCEPID: TFDAutoIncField;
    fdqCEPCEP: TStringField;
    fdqCEPID_CIDADE: TIntegerField;
    fdqCEPRUA: TStringField;
    fdqCEPID_BAIRRO: TIntegerField;
    fdqCEPJOIN_CIDADE_CIDADE: TStringField;
    fdqCEPJOIN_DESCRICAO_BAIRRO: TStringField;
  private
    { Private declarations }
  public
    function GetLogradouroPeloCEP(const AIdCEP: String): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uCEPServ, REST.JSON;

{$R *.dfm}

{ TSMLocalidade }

function TSMLocalidade.GetLogradouroPeloCEP(const AIdCEP: String): String;
begin
  Result := TJSON.ObjectToJsonString(TCEPServ.GetLogradouroPeloCEP(AIdCEP));
end;

end.

