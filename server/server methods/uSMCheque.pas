unit uSMCheque;

interface

uses
  System.SysUtils,
  System.Classes,
  DataSnap.DSProviderDataModuleAdapter,
  Datasnap.DSServer,
  Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery;

type
  TSMCheque = class(TDSServerModule)
    fdqCheque: TgbFDQuery;
    dspCheque: TDataSetProvider;
    fdqChequeID: TFDAutoIncField;
    fdqChequeSACADO: TStringField;
    fdqChequeDOC_FEDERAL: TStringField;
    fdqChequeVL_CHEQUE: TFMTBCDField;
    fdqChequeBANCO: TStringField;
    fdqChequeAGENCIA: TStringField;
    fdqChequeCONTA_CORRENTE: TStringField;
    fdqChequeDT_EMISSAO: TDateField;
    fdqChequeDT_VENCIMENTO: TDateField;
    fdqChequeID_CHAVE_PROCESSO: TIntegerField;
    fdqChequeJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    fdqChequeID_CONTA_CORRENTE: TIntegerField;
    fdqChequeBO_CONCILIADO: TStringField;
    fdqChequeDH_CONCILIADO: TDateTimeField;
    fdqChequeID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqChequeDOCUMENTO_ORIGEM: TStringField;
    fdqChequeJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqChequeNUMERO: TIntegerField;
    fdqChequeID_CONTA_CORRENTE_MOVIMENTO: TIntegerField;
    fdqChequeBO_CHEQUE_UTILIZADO: TStringField;
    fdqChequeTIPO: TStringField;
  private

  public
  procedure GerarUtilizacaoCheque(const AIdCheque: Integer);
  procedure EstornarUtilizacaoCheque(const AIdCheque: Integer);
  function GetCheque(const AIdCheque: Integer): String;
  function ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uChequeServ, Rest.JSON, uChequeProxy;

{$R *.dfm}

{ TSMCheque }

function TSMCheque.ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
begin
  result := TChequeServ.ChequeDisponivelParaUtilizacao(AIdCheque);
end;

procedure TSMCheque.EstornarUtilizacaoCheque(const AIdCheque: Integer);
begin
  TChequeServ.EstornarUtilizacaoCheque(AIdCheque);
end;

procedure TSMCheque.GerarUtilizacaoCheque(const AIdCheque: Integer);
begin
  TChequeServ.GerarUtilizacaoCheque(AIdCheque);
end;

function TSMCheque.GetCheque(const AIdCheque: Integer): String;
begin
  result := TJSON.ObjectToJsonString(TChequeServ.GetCheque(AIdCheque));
end;

end.

