unit uSMAjusteEstoque;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDQuery, DataSnap.DSProviderDataModuleAdapter,
  Datasnap.Provider;

type
  TSMAjusteEstoque = class(TDSServerModule)
    fdqAjusteEstoque: TgbFDQuery;
    fdqAjusteEstoqueItem: TgbFDQuery;
    fdqAjusteEstoqueID: TFDAutoIncField;
    fdqAjusteEstoqueDH_CADASTRO: TDateTimeField;
    fdqAjusteEstoqueDH_FECHAMENTO: TDateTimeField;
    fdqAjusteEstoqueSTATUS: TStringField;
    fdqAjusteEstoqueID_FILIAL: TIntegerField;
    fdqAjusteEstoqueID_PESSOA_USUARIO: TIntegerField;
    fdqAjusteEstoqueID_MOTIVO: TIntegerField;
    fdqAjusteEstoqueJOIN_FANTASIA_FILIAL: TStringField;
    fdqAjusteEstoqueJOIN_USUARIO: TStringField;
    fdqAjusteEstoqueJOIN_DESCRICAO_MOTIVO: TStringField;
    dsAjusteEstoque: TDataSource;
    fdqAjusteEstoqueItemID: TFDAutoIncField;
    fdqAjusteEstoqueItemQUANTIDADE: TFMTBCDField;
    fdqAjusteEstoqueItemID_PRODUTO: TIntegerField;
    fdqAjusteEstoqueItemID_AJUSTE_ESTOQUE: TIntegerField;
    fdqAjusteEstoqueItemJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqAjusteEstoqueID_CHAVE_PROCESSO: TIntegerField;
    fdqAjusteEstoqueJOIN_CHAVE_PROCESSO: TStringField;
    dspAjusteEstoque: TDataSetProvider;
    fdqAjusteEstoqueItemNR_ITEM: TIntegerField;
    fdqAjusteEstoqueItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    fdqAjusteEstoqueItemTIPO: TStringField;
  private
    { Private declarations }
  public
    //Efetivar o ajuste de estoque
    function Efetivar(AIdChaveProcesso: Integer): Boolean;

    //Cancelar o Ajuste de Estoque
    function Cancelar(AIdChaveProcesso: Integer): Boolean;

    // Reabrir o Ajuste de Estoque
    function Reabrir(AIdChaveProcesso: Integer): Boolean;

end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uAjusteEstoqueServ;

{$R *.dfm}

{ TSMAjusteEstoque }

function TSMAjusteEstoque.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TAjusteEstoqueServ.Cancelar(AIdChaveProcesso)
end;

function TSMAjusteEstoque.Efetivar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TAjusteEstoqueServ.Efetivar(AIdChaveProcesso);
end;

function TSMAjusteEstoque.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  result := TAjusteEstoqueServ.Reabrir(AIdChaveProcesso)
end;

end.

