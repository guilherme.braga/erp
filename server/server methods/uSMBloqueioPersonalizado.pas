unit uSMBloqueioPersonalizado;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, Datasnap.Provider, FireDAC.Comp.Client,
  uGBFDQuery, DataSnap.DSProviderDataModuleAdapter;

type
  TSMBloqueioPersonalizado = class(TDSServerModule)
    fdqBloqueioPersonalizado: TgbFDQuery;
    dspBloqueioPersonalizado: TDataSetProvider;
    dsBloqueioPersonalizado: TDataSource;
    fdqBloqueioPersonalizadoID: TFDAutoIncField;
    fdqBloqueioPersonalizadoDESCRICAO: TStringField;
    fdqBloqueioPersonalizadoSQL: TBlobField;
    fdqBloqueioPersonalizadoBO_VENDA: TStringField;
    fdqBloqueioPersonalizadoBO_NOTA_FISCAL: TStringField;
    fdqBloqueioPersonalizadoBO_ORDEM_SERVICO: TStringField;
    fdqBloqueioPersonalizadoBO_ATIVO: TStringField;
    fdqBloqueioPersonalizadoLib: TgbFDQuery;
    fdqBloqueioPersonalizadoLibID: TFDAutoIncField;
    fdqBloqueioPersonalizadoLibID_PESSOA_USUARIO: TIntegerField;
    fdqBloqueioPersonalizadoLibID_BLOQUEIO_PERSONALIZADO: TIntegerField;
    fdqBloqueioPersonalizadoLibJOIN_NOME_PESSOA_USUARIO: TStringField;
  private
    { Private declarations }
  public
    function UsuarioAutorizado(const AIdBloqueioPersonalizado, AIdUsuario: Integer): Boolean;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uBloqueioPersonalizadoServ;

{$R *.dfm}

{ TSMBloqueioPersonalizado }

function TSMBloqueioPersonalizado.UsuarioAutorizado(const AIdBloqueioPersonalizado,
  AIdUsuario: Integer): Boolean;
begin
  result := TBloqueioPersonalizadoServ.UsuarioAutorizado(AIdBloqueioPersonalizado, AIdUsuario);
end;

end.

