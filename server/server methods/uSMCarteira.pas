unit uSMCarteira;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMCarteira = class(TDSServerModule)
    fdqCarteira: TgbFDQuery;
    dspCarteira: TDataSetProvider;
    fdqCarteiraID: TFDAutoIncField;
    fdqCarteiraDESCRICAO: TStringField;
    fdqCarteiraPERC_JUROS: TFMTBCDField;
    fdqCarteiraPERC_MULTA: TFMTBCDField;
    fdqCarteiraBO_ATIVO: TStringField;
    fdqCarteiraCARENCIA: TIntegerField;
    fdqCarteiraOBSERVACAO: TBlobField;
    fdqCarteiraTIPO_BOLETO: TStringField;
    fdqCarteiraCEDENTE_NOME: TStringField;
    fdqCarteiraCEDENTE_DOC_FEDERAL: TStringField;
    fdqCarteiraCEDENTE_BANCO: TStringField;
    fdqCarteiraCEDENTE_CARTEIRA: TStringField;
    fdqCarteiraCEDENTE_NOME_PERSONALIZADO: TStringField;
    fdqCarteiraCEDENTE_AGENCIA: TStringField;
    fdqCarteiraCEDENTE_CONTA_CORRENTE: TStringField;
    fdqCarteiraBOLETO_NOSSO_NUMERO_INICIAL: TIntegerField;
    fdqCarteiraBOLETO_NOSSO_NUMERO_FINAL: TIntegerField;
    fdqCarteiraBOLETO_NOSSO_NUMERO_PROXIMO: TIntegerField;
    fdqCarteiraBOLETO_MODALIDADE: TIntegerField;
    fdqCarteiraBOLETO_CODIGO_TRANSMISSAO: TIntegerField;
    fdqCarteiraBOLETO_LOCAL_PAGAMENTO: TStringField;
    fdqCarteiraBOLETO_INSTRUCOES: TBlobField;
    fdqCarteiraBOLETO_DEMONSTRATIVO: TBlobField;
    fdqCarteiraBOLETO_LAYOUT_IMPRESSAO: TStringField;
    fdqCarteiraBOLETO_LAYOUT_REMESSA: TStringField;
    fdqCarteiraBOLETO_LAYOUT_RETORNO: TStringField;
    fdqCarteiraBOLETO_PATH_REMESSA: TStringField;
    fdqCarteiraBOLETO_PATH_RETORNO: TStringField;
    fdqCarteiraBOLETO_ESPECIE_DOCUMENTO: TStringField;
    fdqCarteiraBOLETO_SEQUENCIA_REMESSA: TIntegerField;
    fdqCarteiraBOLETO_AMBIENTE: TStringField;
    fdqCarteiraBOLETO_DIAS_PROTESTO: TIntegerField;
    fdqCarteiraBO_ACEITE_BOLETO: TStringField;
    fdqCarteiraBOLETO_VL_ACRESCIMO: TBCDField;
    fdqCarteiraBOLETO_VL_TARIFA_BANCARIA: TBCDField;
    fdqCarteiraBOLETO_PERC_MULTA: TBCDField;
    fdqCarteiraBOLETO_PERC_MORA_DIARIA: TBCDField;
    fdqCarteiraBOLETO_PERC_DESCONTO: TBCDField;
    fdqCarteiraBOLETO_MOEDA: TStringField;
    fdqCarteiraBO_ENVIA_BOLETO_EMAIL: TStringField;
    fdqCarteiraBOLETO_ASSUNTO_EMAIL: TStringField;
    fdqCarteiraBOLETO_LAYOUT_BOLETO: TStringField;
    fdqCarteiraBOLETO_ID_RECIBO_EMAIL_PERSONALIZADO: TIntegerField;
    fdqCarteiraBOLETO_ID_RECIBO_BOLETO_PERSONALIZADO: TIntegerField;
    fdqCarteiraBOLETO_PREFIXO_NOSSONUMERO: TIntegerField;
    fdqCarteiraBOLETO_TIPO_DOCUMENTO: TStringField;
    fdqCarteiraBOLETO_LAYOUT_EMAIL: TStringField;
    fdqCarteiraID_CONTA_CORRENTE: TIntegerField;
    fdqCarteiraJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqCarteiraCBX_ARQUIVO_LICENCA: TBlobField;
    fdqCarteiraCEDENTE_CODIGO: TStringField;
  private
    { Private declarations }
  public
    function GetCarteira(AIdCarteira: Integer): String;
    function BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
    function BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
    procedure IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConnection, uCarteiraProxy, uCarteiraServ, Rest.JSON;

{$R *.dfm}

{ TSMCarteira }

function TSMCarteira.BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
begin
  result := TCarteiraServ.BuscarProximaSequenciaNossoNumero(AIdCarteira);
end;

function TSMCarteira.BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
begin
  result := TCarteiraServ.BuscarProximaSequenciaRemessa(AIdCarteira);
end;

function TSMCarteira.GetCarteira(AIdCarteira: Integer): String;
var carteiraProxy: TCarteiraProxy;
begin
  carteiraProxy := TCarteiraServ.GetCarteira(AIdCarteira);
  try
    result := TJSON.ObjectToJsonString(carteiraProxy);
  finally
    FreeAndNil(carteiraProxy);
  end;
end;

procedure TSMCarteira.IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
begin
  TCarteiraServ.IncrementarBoletoNossoNumeroProximo(AIdCarteira);
end;

end.

