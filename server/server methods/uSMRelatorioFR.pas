unit uSMRelatorioFR;

interface

uses System.SysUtils, System.Classes, System.Json,
     Datasnap.DSServer, Datasnap.DSAuth, DataSnap.DSProviderDataModuleAdapter,
     FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
     FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
     FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
     FireDAC.Comp.Client, Datasnap.Provider, Data.FireDACJSONReflect, uGBFDQuery,
  frxClass, frxDMPExport;

type
  TSMRelatorioFR = class(TDSServerModule)
    fdqRelatorioFR: TgbFDQuery;
    dspRelatorioFR: TDataSetProvider;
    fdqRelatorioFRID: TFDAutoIncField;
    fdqRelatorioFRNOME: TStringField;
    fdqRelatorioFRDESCRICAO: TStringField;
    fdqRelatorioFROBSERVACOES: TBlobField;
    fdqRelatorioFRFormulario: TgbFDQuery;
    fdqRelatorioFRFormularioID: TFDAutoIncField;
    fdqRelatorioFRFormularioNOME: TStringField;
    fdqRelatorioFRFormularioDESCRICAO: TStringField;
    fdqRelatorioFRFormularioACAO: TStringField;
    fdqRelatorioFRFormularioID_RELATORIO_FR: TIntegerField;
    fdqRelatorioFRFormularioID_PESSOA_USUARIO: TIntegerField;
    dsRelatorioFR: TDataSource;
    dspRelatorioFRFormulario: TDataSetProvider;
    fdqRelatorioFRFormularioJOIN_USUARIO_PESSOA_USUARIO: TStringField;
    fdqConsultaRelatorioFR: TgbFDQuery;
    dspConsultaRelatorioFR: TDataSetProvider;
    fdqConsultaRelatorioFRID: TFDAutoIncField;
    fdqConsultaRelatorioFRDESCRICAO: TStringField;
    fdqConsultaRelatorioFRNOME: TStringField;
    frxReport: TfrxReport;
    fdqRelatorioFRFormularioIMPRESSORA: TStringField;
    fdqConsultaRelatorioFRIMPRESSORA: TStringField;
    fdqConsultaRelatorioFRACAO: TStringField;
    fdqRelatorioFRARQUIVO: TBlobField;
    fdqRelatorioFRFormularioNUMERO_COPIAS: TIntegerField;
    fdqConsultaRelatorioFRNUMERO_COPIAS: TIntegerField;
    frxDotMatrixExport: TfrxDotMatrixExport;
    fdqRelatorioFRFormularioFILTROS_PERSONALIZADOS: TBlobField;
    fdqConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField;
    fdqRelatorioFRTIPO_ARQUIVO: TStringField;
    fdqConsultaRelatorioFRTIPO_ARQUIVO: TStringField;
    fdqConsultaRelatorioFRARQUIVO: TBlobField;
  private

  public
    Function BuscarRelatorio(AIdMaster, AIdRelatorio: Integer; AWhere: String): TStream;

    function BuscarRelatoriosAssociados(const AClasseFormulario: String;
      AIdUsuario: Integer): TFDJSONDataSets;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFactoryQuery, uRelatorioFRServer;

{$R *.dfm}

{ TSMRelatorioFR }

function TSMRelatorioFR.BuscarRelatorio(AIdMaster, AIdRelatorio: Integer;
  AWhere: String): TStream;
begin
  result := TRelatorioFRServer.BuscarRelatorio(AIdMaster, AIdRelatorio, AWhere);
end;

function TSMRelatorioFR.BuscarRelatoriosAssociados(
  const AClasseFormulario: String; AIdUsuario: Integer): TFDJSONDataSets;
begin
  result := TRelatorioFRServer.BuscarRelatoriosAssociados(AClasseFormulario,
    AIdUsuario);
end;

end.

