unit uSMZoneamento;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMZoneamento = class(TDSServerModule)
    fdqZoneamento: TgbFDQuery;
    dspZoneamento: TDataSetProvider;
    fdqZoneamentoID: TFDAutoIncField;
    fdqZoneamentoDESCRICAO: TStringField;
    fdqZoneamentoBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    function GetZoneamento(AIdZoneamento: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uZoneamentoProxy, uZoneamentoServ, REST.JSON;

{$R *.dfm}

{ TSMZoneamento }

function TSMZoneamento.GetZoneamento(
  AIdZoneamento: Integer): String;
begin
  result := TJson.ObjectToJsonString(TZoneamentoServ.GetZoneamento(AIdZoneamento));
end;

end.

