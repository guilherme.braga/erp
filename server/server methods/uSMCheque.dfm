object SMCheque: TSMCheque
  OldCreateOrder = False
  Height = 299
  Width = 477
  object fdqCheque: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT c.*'
      '      ,cp.origem as JOIN_ORIGEM_CHAVE_PROCESSO  '
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '  FROM cheque c'
      ' INNER JOIN chave_processo cp on c.id_chave_processo = cp.id'
      ' INNER JOIN conta_corrente cc on c.id_conta_corrente = cc.id'
      ' WHERE c.id = :id')
    Left = 24
    Top = 16
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object fdqChequeID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqChequeSACADO: TStringField
      DisplayLabel = 'Sacado'
      FieldName = 'SACADO'
      Origin = 'SACADO'
      Size = 80
    end
    object fdqChequeDOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal'
      FieldName = 'DOC_FEDERAL'
      Origin = 'DOC_FEDERAL'
      Size = 14
    end
    object fdqChequeVL_CHEQUE: TFMTBCDField
      DisplayLabel = 'Vl. Cheque'
      FieldName = 'VL_CHEQUE'
      Origin = 'VL_CHEQUE'
      Required = True
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqChequeNUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object fdqChequeBANCO: TStringField
      DisplayLabel = 'Banco'
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 80
    end
    object fdqChequeAGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object fdqChequeCONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'CONTA_CORRENTE'
      Origin = 'CONTA_CORRENTE'
      Size = 10
    end
    object fdqChequeDT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'DT_EMISSAO'
      Origin = 'DT_EMISSAO'
    end
    object fdqChequeDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object fdqChequeID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object fdqChequeID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqChequeBO_CONCILIADO: TStringField
      DisplayLabel = 'Conciliado'
      FieldName = 'BO_CONCILIADO'
      Origin = 'BO_CONCILIADO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqChequeDH_CONCILIADO: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Concilia'#231#227'o'
      FieldName = 'DH_CONCILIADO'
      Origin = 'DH_CONCILIADO'
    end
    object fdqChequeID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Documento de Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqChequeDOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento de Origem'
      FieldName = 'DOCUMENTO_ORIGEM'
      Origin = 'DOCUMENTO_ORIGEM'
      Size = 255
    end
    object fdqChequeJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqChequeJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo de Origem'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqChequeID_CONTA_CORRENTE_MOVIMENTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Movimenta'#231#227'o de Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE_MOVIMENTO'
      Origin = 'ID_CONTA_CORRENTE_MOVIMENTO'
    end
    object fdqChequeBO_CHEQUE_UTILIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque Utilizado'
      FieldName = 'BO_CHEQUE_UTILIZADO'
      Origin = 'BO_CHEQUE_UTILIZADO'
      FixedChar = True
      Size = 1
    end
    object fdqChequeTIPO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 15
    end
  end
  object dspCheque: TDataSetProvider
    DataSet = fdqCheque
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 88
    Top = 16
  end
end
