object SMCheckList: TSMCheckList
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqChecklist: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from checklist where id = :id')
    Left = 24
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqChecklistID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqChecklistDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqChecklistBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dspChecklist: TDataSetProvider
    DataSet = fdqChecklist
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 8
  end
  object fdqChecklistItem: TgbFDQuery
    MasterSource = dsChecklist
    MasterFields = 'ID'
    DetailFields = 'ID_CHECKLIST'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from checklist_item where id_checklist = :id')
    Left = 32
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqChecklistItemID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqChecklistItemNR_ITEM: TIntegerField
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object fdqChecklistItemDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqChecklistItemID_CHECKLIST: TIntegerField
      FieldName = 'ID_CHECKLIST'
      Origin = 'ID_CHECKLIST'
    end
    object fdqChecklistItemBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dsChecklist: TDataSource
    DataSet = fdqChecklist
    Left = 88
    Top = 56
  end
end
