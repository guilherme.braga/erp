unit uSMEquipamento;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, DataSnap.DSProviderDataModuleAdapter,
  uGBFDQuery, uEquipamentoProxy;

type
  TSMEquipamento = class(TDSServerModule)
    fdqEquipamento: TgbFDQuery;
    fdqEquipamentoID: TFDAutoIncField;
    dspEquipamento: TDataSetProvider;
    fdqEquipamentoDESCRICAO: TStringField;
    fdqEquipamentoID_MARCA: TIntegerField;
    fdqEquipamentoID_MODELO: TIntegerField;
    fdqEquipamentoSERIE: TStringField;
    fdqEquipamentoID_PESSOA: TIntegerField;
    fdqEquipamentoJOIN_NOME_PESSOA: TStringField;
    fdqEquipamentoBO_ATIVO: TStringField;
    fdqEquipamentoJOIN_DESCRICAO_MODELO: TStringField;
    fdqEquipamentoJOIN_DESCRICAO_MARCA: TStringField;
    fdqEquipamentoIMEI: TStringField;
  private

  public
    function GetEquipamento(const AIdEquipamento: Integer): TEquipamentoProxy;
    function GerarEquipamento(const AEquipamento: TEquipamentoProxy): Integer;
    function BuscarIdEquipamentoPelaDescricao(const ADescricaoEquipamento: String): Integer;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uEquipamentoServ;

{$R *.dfm}

{ TSMEquipamento }

function TSMEquipamento.BuscarIdEquipamentoPelaDescricao(const ADescricaoEquipamento: String): Integer;
begin
  result := TEquipamentoServ.BuscarIdEquipamentoPelaDescricao(ADescricaoEquipamento);
end;

function TSMEquipamento.GerarEquipamento(const AEquipamento: TEquipamentoProxy): Integer;
begin
  result := TEquipamentoServ.GerarEquipamento(AEquipamento);
end;

function TSMEquipamento.GetEquipamento(const AIdEquipamento: Integer): TEquipamentoProxy;
begin
  result := TEquipamentoServ.GetEquipamento(AIdEquipamento);
end;

end.

