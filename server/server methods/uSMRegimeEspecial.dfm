object SMRegimeEspecial: TSMRegimeEspecial
  OldCreateOrder = False
  Height = 256
  Width = 383
  object fdqRegimeEspecial: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from regime_especial where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqRegimeEspecialID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqRegimeEspecialDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqRegimeEspecialTEXTO_ESPECIAL: TBlobField
      DisplayLabel = 'Texto'
      FieldName = 'TEXTO_ESPECIAL'
      Origin = 'TEXTO_ESPECIAL'
      Required = True
    end
    object fdqRegimeEspecialBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspRegimeEspecial: TDataSetProvider
    DataSet = fdqRegimeEspecial
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
