object SMCSTIPI: TSMCSTIPI
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqCSTIPI: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from CST_IPI where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCSTIPIID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCSTIPICODIGO_CST: TStringField
      DisplayLabel = 'C'#243'digo CST'
      FieldName = 'CODIGO_CST'
      Origin = 'CODIGO_CST'
      Required = True
      FixedChar = True
      Size = 2
    end
    object fdqCSTIPIDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCSTIPICENQ: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do Enquadramento Legal do IPI'
      FieldName = 'CENQ'
      Origin = 'CENQ'
      Size = 3
    end
  end
  object dspCSTIPI: TDataSetProvider
    DataSet = fdqCSTIPI
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
  object fdqEnquadramentoLegalIPI: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from ENQUADRAMENTO_LEGAL_IPI where id = :id')
    Left = 64
    Top = 80
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqEnquadramentoLegalIPIID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqEnquadramentoLegalIPICODIGO: TStringField
      DisplayLabel = 'C'#243'digo Enquadramento'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Required = True
      Size = 3
    end
    object fdqEnquadramentoLegalIPIGRUPO_CST: TStringField
      DisplayLabel = 'Grupo CST'
      FieldName = 'GRUPO_CST'
      Origin = 'GRUPO_CST'
      Required = True
      Size = 255
    end
    object fdqEnquadramentoLegalIPIDESCRICAO: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
    end
  end
  object dspEnquadramentoLegaIPI: TDataSetProvider
    DataSet = fdqEnquadramentoLegalIPI
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 96
    Top = 80
  end
end
