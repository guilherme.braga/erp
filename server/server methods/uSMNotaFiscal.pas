unit uSMNotaFiscal;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDQuery, Datasnap.Provider, Datasnap.DSProviderDataModuleAdapter, uRetornoProxy,
  frxClass, Data.FireDACJSONReflect, uVendaProxy, uNotaFiscalProxy;

type
  TSMNotaFiscal = class(TDSServerModule)
    dspNotaFiscal: TDataSetProvider;
    fdqNotaFiscal: TgbFDQuery;
    fdqNotaFiscalItem: TgbFDQuery;
    dsNotaFiscal: TDataSource;
    fdqNotaFiscalParcela: TgbFDQuery;
    fdqNotaFiscalID: TFDAutoIncField;
    fdqNotaFiscalDH_CADASTRO: TDateTimeField;
    fdqNotaFiscalB_NNF: TIntegerField;
    fdqNotaFiscalB_SERIE: TIntegerField;
    fdqNotaFiscalID_MODELO_NOTA: TIntegerField;
    fdqNotaFiscalB_DEMI: TDateField;
    fdqNotaFiscalB_DSAIENT: TDateField;
    fdqNotaFiscalB_HSAIENT: TTimeField;
    fdqNotaFiscalW_VPROD: TBCDField;
    fdqNotaFiscalW_VST: TBCDField;
    fdqNotaFiscalW_VSEG: TBCDField;
    fdqNotaFiscalW_VDESC: TBCDField;
    fdqNotaFiscalW_VOUTRO: TBCDField;
    fdqNotaFiscalW_VIPI: TBCDField;
    fdqNotaFiscalW_VFRETE: TBCDField;
    fdqNotaFiscalW_VNF: TBCDField;
    fdqNotaFiscalX_XNOME: TStringField;
    fdqNotaFiscalID_CENTRO_RESULTADO: TIntegerField;
    fdqNotaFiscalID_CONTA_ANALISE: TIntegerField;
    fdqNotaFiscalID_CHAVE_PROCESSO: TIntegerField;
    fdqNotaFiscalID_OPERACAO: TIntegerField;
    fdqNotaFiscalID_FILIAL: TIntegerField;
    fdqNotaFiscalJOIN_DESCRICAO_OPERACAO: TStringField;
    fdqNotaFiscalJOIN_DESCRICAO_MODELO: TStringField;
    fdqNotaFiscalJOIN_SEQUENCIA_CONTA_ANALISE: TStringField;
    fdqNotaFiscalJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqNotaFiscalJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqNotaFiscalItemID: TFDAutoIncField;
    fdqNotaFiscalItemH_NITEM: TIntegerField;
    fdqNotaFiscalItemI_QCOM: TBCDField;
    fdqNotaFiscalItemI_VUNCOM: TFMTBCDField;
    fdqNotaFiscalItemI_VDESC: TBCDField;
    fdqNotaFiscalItemI_VFRETE: TBCDField;
    fdqNotaFiscalItemI_VSEG: TBCDField;
    fdqNotaFiscalItemI_VOUTRO: TBCDField;
    fdqNotaFiscalItemN_NVICMSST: TBCDField;
    fdqNotaFiscalItemO_VIPI: TBCDField;
    fdqNotaFiscalItemID_PRODUTO: TIntegerField;
    fdqNotaFiscalItemJOIN_DESCRICAO_PRODUTO: TStringField;
    fdqNotaFiscalVL_FRETE_TRANSPORTADORA: TBCDField;
    sta: TDateField;
    fdqNotaFiscalID_PESSOA: TIntegerField;
    fdqNotaFiscalJOIN_NOME_PESSOA: TStringField;
    fdqNotaFiscalItemVL_LIQUIDO: TBCDField;
    fdqNotaFiscalItemVL_CUSTO_IMPOSTO: TBCDField;
    fdqNotaFiscalItemID_NOTA_FISCAL: TIntegerField;
    fdqModeloNota: TgbFDQuery;
    dspModeloNota: TDataSetProvider;
    fdqModeloNotaID: TFDAutoIncField;
    fdqModeloNotaDESCRICAO: TStringField;
    fdqNotaFiscalItemPERC_DESCONTO: TBCDField;
    fdqNotaFiscalItemPERC_ACRESCIMO: TBCDField;
    fdqNotaFiscalItemPERC_FRETE: TBCDField;
    fdqNotaFiscalItemPERC_SEGURO: TBCDField;
    fdqNotaFiscalItemPERC_IPI: TBCDField;
    fdqNotaFiscalPERC_VST: TBCDField;
    fdqNotaFiscalPERC_VSEG: TBCDField;
    fdqNotaFiscalPERC_VDESC: TBCDField;
    fdqNotaFiscalPERC_VOUTRO: TBCDField;
    fdqNotaFiscalPERC_VIPI: TBCDField;
    fdqNotaFiscalPERC_VFRETE: TBCDField;
    fdqNotaFiscalSTATUS: TStringField;
    fdqNotaFiscalID_PLANO_PAGAMENTO: TIntegerField;
    fdqNotaFiscalJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    fdqNotaFiscalItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    fdqNotaFiscalItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    fdqNotaFiscalParcelaID: TFDAutoIncField;
    fdqNotaFiscalParcelaDOCUMENTO: TStringField;
    fdqNotaFiscalParcelaVL_TITULO: TFMTBCDField;
    fdqNotaFiscalParcelaQT_PARCELA: TIntegerField;
    fdqNotaFiscalParcelaNR_PARCELA: TIntegerField;
    fdqNotaFiscalParcelaDT_VENCIMENTO: TDateField;
    fdqNotaFiscalParcelaID_NOTA_FISCAL: TIntegerField;
    fdqNotaFiscalID_FORMA_PAGAMENTO: TIntegerField;
    fdqNotaFiscalJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqNotaFiscalDT_COMPETENCIA: TDateField;
    fdqNotaFiscalParcelaOBSERVACAO: TStringField;
    fdqNotaFiscalParcelaCHEQUE_SACADO: TStringField;
    fdqNotaFiscalParcelaCHEQUE_DOC_FEDERAL: TStringField;
    fdqNotaFiscalParcelaCHEQUE_BANCO: TStringField;
    fdqNotaFiscalParcelaCHEQUE_DT_EMISSAO: TDateField;
    fdqNotaFiscalParcelaCHEQUE_AGENCIA: TStringField;
    fdqNotaFiscalParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    fdqNotaFiscalParcelaCHEQUE_NUMERO: TIntegerField;
    fdqNotaFiscalParcelaID_OPERADORA_CARTAO: TIntegerField;
    fdqNotaFiscalParcelaID_FORMA_PAGAMENTO: TIntegerField;
    fdqNotaFiscalParcelaID_CARTEIRA: TIntegerField;
    fdqNotaFiscalParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    fdqNotaFiscalParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    fdqNotaFiscalItemVL_CUSTO: TFMTBCDField;
    fdqNotaFiscalItemVL_CUSTO_OPERACIONAL: TFMTBCDField;
    fdqNotaFiscalItemPERC_CUSTO_OPERACIONAL: TFMTBCDField;
    fdqNotaFiscalItemI_XPROD: TStringField;
    fdqNotaFiscalItemN_VICMS: TBCDField;
    fdqNotaFiscalItemN_VBCST: TBCDField;
    fdqNotaFiscalItemN_PREDBCST: TBCDField;
    fdqNotaFiscalItemN_MOTDESICMS: TIntegerField;
    fdqNotaFiscalItemN_PMVAST: TBCDField;
    fdqNotaFiscalItemN_MODBCST: TIntegerField;
    fdqNotaFiscalItemI_NCM: TIntegerField;
    fdqNotaFiscalItemI_EXTIPI: TStringField;
    fdqNotaFiscalItemI_CFOP: TIntegerField;
    fdqNotaFiscalItemI_CEANTRIB: TStringField;
    fdqNotaFiscalItemI_UTRIB: TStringField;
    fdqNotaFiscalItemI_QTRIB: TBCDField;
    fdqNotaFiscalItemI_VUNTRIB: TFMTBCDField;
    fdqNotaFiscalItemI_INDTOT: TIntegerField;
    fdqNotaFiscalItemI_NVE: TStringField;
    fdqNotaFiscalItemI_CEAN: TStringField;
    fdqNotaFiscalItemN_ORIG: TIntegerField;
    fdqNotaFiscalItemN_CST: TIntegerField;
    fdqNotaFiscalItemN_MODBC: TIntegerField;
    fdqNotaFiscalItemN_PREDBC: TBCDField;
    fdqNotaFiscalItemN_VBC: TBCDField;
    fdqNotaFiscalItemN_PICMS: TBCDField;
    fdqNotaFiscalItemN_VICMSOP: TBCDField;
    fdqNotaFiscalItemN_UFST: TStringField;
    fdqNotaFiscalItemN_PDIF: TBCDField;
    fdqNotaFiscalItemN_VLICMSDIF: TBCDField;
    fdqNotaFiscalItemN_VICMSDESON: TBCDField;
    fdqNotaFiscalItemN_PBCOP: TBCDField;
    fdqNotaFiscalItemN_BCSTRET: TBCDField;
    fdqNotaFiscalItemN_PCREDSN: TBCDField;
    fdqNotaFiscalItemN_VBCSTDEST: TBCDField;
    fdqNotaFiscalItemN_VICMSSTDEST: TBCDField;
    fdqNotaFiscalItemN_VCREDICMSSN: TBCDField;
    fdqNotaFiscalItemO_CLENQ: TStringField;
    fdqNotaFiscalItemO_CNPJPROD: TStringField;
    fdqNotaFiscalItemO_CSELO: TStringField;
    fdqNotaFiscalItemO_QSELO: TFMTBCDField;
    fdqNotaFiscalItemO_CENQ: TStringField;
    fdqNotaFiscalItemO_CST: TIntegerField;
    fdqNotaFiscalItemO_VBC: TBCDField;
    fdqNotaFiscalItemO_QUNID: TBCDField;
    fdqNotaFiscalItemO_VUNID: TBCDField;
    fdqNotaFiscalItemO_PIPI: TBCDField;
    fdqNotaFiscalItemQ_CST: TIntegerField;
    fdqNotaFiscalItemQ_VBC: TBCDField;
    fdqNotaFiscalItemQ_PPIS: TBCDField;
    fdqNotaFiscalItemQ_VPIS: TBCDField;
    fdqNotaFiscalItemQ_QBCPROD: TBCDField;
    fdqNotaFiscalItemQ_VALIQPROD: TBCDField;
    fdqNotaFiscalItemS_CST: TIntegerField;
    fdqNotaFiscalItemS_VBC: TBCDField;
    fdqNotaFiscalItemS_PCOFINS: TBCDField;
    fdqNotaFiscalItemS_QBCPROD: TBCDField;
    fdqNotaFiscalItemS_VALIQPROD: TBCDField;
    fdqNotaFiscalItemS_VCOFINS: TBCDField;
    fdqNotaFiscalItemJOIN_BO_PRODUTO_AGRUPADOR_PRODUTO: TStringField;
    fdqNotaFiscalItemJOIN_ID_PRODUTO_AGRUPADOR_PRODUTO: TIntegerField;
    fdqNotaFiscalItemJOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO: TStringField;
    fdqNotaFiscalID_PESSOA_USUARIO: TIntegerField;
    fdqNotaFiscalTIPO_NF: TStringField;
    fdqNotaFiscalJOIN_NOME_PESSOA_USUARIO: TStringField;
    fdqNotaFiscalParcelaCHEQUE_DT_VENCIMENTO: TDateField;
    fdqNotaFiscalItemI_UCOM: TStringField;
    fdqNotaFiscalItemI_VPROD: TBCDField;
    fdqNotaFiscalItemI_XPED: TStringField;
    fdqNotaFiscalItemI_NITEMPED: TIntegerField;
    fdqNotaFiscalNFE_CHAVE: TStringField;
    fdqNotaFiscalAR_VERSAO: TStringField;
    fdqNotaFiscalAR_TP_AMB: TStringField;
    fdqNotaFiscalAR_VERAPLIC: TStringField;
    fdqNotaFiscalAR_XMOTIVO: TStringField;
    fdqNotaFiscalAR_CUF: TStringField;
    fdqNotaFiscalAR_DHRECBTO: TDateTimeField;
    fdqNotaFiscalAR_INFREC: TStringField;
    fdqNotaFiscalAR_NREC: TStringField;
    fdqNotaFiscalAR_TMED: TIntegerField;
    fdqNotaFiscalAR_PROTNFE: TStringField;
    fdqNotaFiscalID_OPERACAO_FISCAL: TIntegerField;
    fdqNotaFiscalFORMA_PAGAMENTO: TStringField;
    fdqNotaFiscalJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField;
    fdqNotaFiscalJOIN_MODELO_MODELO: TStringField;
    fdqNotaFiscalX_MODFRETE: TIntegerField;
    fdqNotaFiscalVL_QUITACAO_DINHEIRO: TFMTBCDField;
    fdqNotaFiscalItemN_VICMSSTRET: TBCDField;
    fdqNotaFiscalItemID_IMPOSTO_ICMS: TIntegerField;
    fdqModeloNotaMODELO: TStringField;
    fdqNotaFiscalDOC_FEDERAL_PESSOA: TStringField;
    fdqNotaFiscalBO_ENVIO_DPEC: TStringField;
    fdqNotaFiscalNR_REG_DPEC: TStringField;
    fdqNotaFiscalDH_REG_DPEC: TDateTimeField;
    fdqNotaFiscalW_VBC: TBCDField;
    fdqNotaFiscalW_VICMS: TBCDField;
    fdqNotaFiscalW_VBCST: TBCDField;
    fdqNotaFiscalW_VII: TBCDField;
    fdqNotaFiscalW_VPIS: TBCDField;
    fdqNotaFiscalW_VCOFINS: TBCDField;
    fdqNotaFiscalW_VRETPIS: TBCDField;
    fdqNotaFiscalW_VRETCOFINS: TBCDField;
    fdqNotaFiscalW_VRETCSLL: TBCDField;
    fdqNotaFiscalW_VBCIRRF: TBCDField;
    fdqNotaFiscalW_VIRRF: TBCDField;
    fdqNotaFiscalW_VBCRETPREV: TBCDField;
    fdqNotaFiscalW_VRETPREV: TBCDField;
    fdqNotaFiscalPERC_ICMS: TBCDField;
    fdqNotaFiscalPERC_PIS: TBCDField;
    fdqNotaFiscalPERC_COFINS: TBCDField;
    fdqNotaFiscalAR_CSTAT: TStringField;
    fdqNotaFiscalDOCUMENTO_FISCAL_XML: TBlobField;
    fdqNotaFiscalItemID_IMPOSTO_PIS_COFINS: TIntegerField;
    fdqNotaFiscalItemID_OPERACAO_FISCAL: TIntegerField;
    fdqNotaFiscalItemID_IMPOSTO_IPI: TIntegerField;
    fdqNotaFiscalItemPERC_ICMS_ST: TBCDField;
    fdqNotaFiscalItemN_PICMSST: TBCDField;
    fdqNotaFiscalItemN_VICMSST: TBCDField;
  private

  public
    function GerarContaReceber(AIdChaveProcesso: Integer;
      ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
    function GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
    function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
    function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
    procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function Efetivar(AIdChaveProcesso: Integer;
      ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GerarNotaFiscal(const ANotaFiscal: String): Integer;
    function EmitirNFE(const AIdNotaFiscal: Integer; const ATipoDocumentoFiscal: String): String;
    function NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
    function GerarNotaFiscalDaVenda(const AId: Integer; const ACPFNaNota: String;
      ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
    function GerarNotaFiscalDeVendaAgrupada(const AIdsVendas: String; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
    function GerarNotaFiscalDaOrdemServico(const AIdChaveProcesso: Integer; const ACPFNaNota: String;
      ATipoDocumentoFiscal: String): Boolean;
    procedure AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
    function ImprimirDocumentoFiscal(const AIdNotaFiscal: Integer;
      const ATipoDocumentoFiscal: String): String;
    function ChecarServidorAtivo(const AIdFilial, AIdUsuario: Integer): String;
    function GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
    function GetNumeroDocumentoNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
    function GetSerieNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
    function CancelarDocumentoFiscal(const AIdNotaFiscal: Integer; const AMotivoCancelamento: String;
      const ATipoDocumentoFiscal: String): Boolean;
    procedure InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);
    function BuscarXML(const AIdNotaFiscal: Integer): String;
    function BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes, AAno: Integer): TFDJSONDataSets;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uNotaFiscalServ, Rest.JSON, uNotaFiscalEletronicaServ, uModeloNotaServ,
  uNotaFiscalConsumidorEletronica;

{$R *.dfm}

{ TSMNotaFiscal }

procedure TSMNotaFiscal.AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
begin
  TNotaFiscalServ.AtualizarCPFNaNota(AIdNotaFiscal, ACPF);
end;

procedure TSMNotaFiscal.AtualizarStatus(AIdChaveProcesso: Integer; AStatus: String);
begin
  TNotaFiscalServ.AtualizarStatus(AIdChaveProcesso, AStatus);
end;

function TSMNotaFiscal.BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes,
  AAno: Integer): TFDJSONDataSets;
begin
  result := TNotaFiscalServ.BuscarNotasFiscaisParaInutilizacao(AIdFIlial, AMes, AAno);
end;

function TSMNotaFiscal.BuscarXML(const AIdNotaFiscal: Integer): String;
begin
  result := TNotaFiscalServ.BuscarXML(AIdNotaFiscal);
end;

function TSMNotaFiscal.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.Cancelar(AIdChaveProcesso);
end;

function TSMNotaFiscal.CancelarDocumentoFiscal(const AIdNotaFiscal: Integer; const AMotivoCancelamento: String;
  const ATipoDocumentoFiscal: String): Boolean;
begin
  result := TNotaFiscalEletronicaServ.CancelarDocumentoFiscal(AIdNotaFiscal, AMotivoCancelamento,
    ATipoDocumentoFiscal);
end;

function TSMNotaFiscal.ChecarServidorAtivo(const AIdFilial, AIdUsuario: Integer): String;
begin
  result := TNotaFiscalEletronicaServ.ChecarServidorAtivo(AIdFilial, AIdUsuario);
end;

function TSMNotaFiscal.Efetivar(AIdChaveProcesso: Integer;
  ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
begin
  result := TNotaFiscalServ.Efetivar(AIdChaveProcesso, ANotaFiscalTransienteProxy);
end;

function TSMNotaFiscal.EmitirNFE(const AIdNotaFiscal: Integer; const ATipoDocumentoFiscal: String): String;
begin
  result := TNotaFiscalEletronicaServ.EmitirNFE(AIdNotaFiscal, ATipoDocumentoFiscal);
end;

function TSMNotaFiscal.EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.EstornarContaPagar(AIdChaveProcesso);
end;

function TSMNotaFiscal.EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.EstornarContaReceber(AIdChaveProcesso);
end;

function TSMNotaFiscal.GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.GerarContaPagar(AIdChaveProcesso);
end;

function TSMNotaFiscal.GerarContaReceber(AIdChaveProcesso: Integer;
  ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
begin
  result := TNotaFiscalServ.GerarContaReceber(AIdChaveProcesso, ANotaFiscalTransienteProxy);
end;

function TSMNotaFiscal.GerarNotaFiscalDaVenda(const AId: Integer; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
begin
  result := TNotaFiscalServ.GerarNotaFiscalDaVenda(AId, ACPFNaNota, ATipoDocumentoFiscal, AVendaProxy);
end;

function TSMNotaFiscal.GerarNotaFiscalDeVendaAgrupada(const AIdsVendas: String; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String; AVendaProxy: TVendaProxy): Boolean;
begin
  result := TNotaFiscalServ.GerarNotaFiscalDeVendaAgrupada(AIdsVendas, ACPFNaNota,
    ATipoDocumentoFiscal, AVendaProxy);
end;

function TSMNotaFiscal.GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
begin
  result := TmodeloNotaServ.GetModeloNotaFiscal(AIdModeloNota);
end;

function TSMNotaFiscal.GetNumeroDocumentoNotaFiscalSaida(
  const ATipoDocumentoFiscal: String): Integer;
begin
  result := TNotaFiscalServ.GetNumeroDocumentoNotaFiscalSaida(ATipoDocumentoFiscal);
end;

function TSMNotaFiscal.GetSerieNotaFiscalSaida(
  const ATipoDocumentoFiscal: String): Integer;
begin
  result := TNotaFiscalServ.GetSerieNotaFiscalSaida(ATipoDocumentoFiscal);
end;

function TSMNotaFiscal.ImprimirDocumentoFiscal(const AIdNotaFiscal: Integer;
  const ATipoDocumentoFiscal: String): String;
begin
  result := TNotaFiscalEletronicaServ.ImprimirDocumentoFiscal(AIdNotaFiscal, ATipoDocumentoFiscal);
end;

procedure TSMNotaFiscal.InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);
begin
  TNotaFiscalEletronicaServ.InutilizarDocumentoFiscal(AIdsNotaFiscal);
end;

function TSMNotaFiscal.GerarNotaFiscalDaOrdemServico(const AIdChaveProcesso: Integer; const ACPFNaNota: String;
  ATipoDocumentoFiscal: String): Boolean;
begin

end;

function TSMNotaFiscal.GerarNotaFiscal(const ANotaFiscal: String): Integer;
begin
  result := TNotaFiscalServ.GerarNotaFiscal(TJSON.JsonToObject<TNotaFiscalProxy>(ANotaFiscal));
end;

function TSMNotaFiscal.NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
begin
  result := TNotaFiscalEletronicaServ.NFEEmitida(AIdNotaFiscal);
end;

function TSMNotaFiscal.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.PodeEstornar(AIdChaveProcesso);
end;

function TSMNotaFiscal.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  result := TNotaFiscalServ.PodeExcluir(AIdChaveProcesso);
end;

end.

