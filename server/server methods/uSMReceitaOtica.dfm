object SMReceitaOtica: TSMReceitaOtica
  OldCreateOrder = False
  Height = 309
  Width = 453
  object fdqReceitaOtica: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT ro.*'
      '      ,pu.nome AS JOIN_NOME_USUARIO'
      '      ,p.nome AS JOIN_NOME_PESSOA'
      '      ,f.fantasia AS JOIN_FANTASIA_FILIAL'
      '      ,m.nome AS JOIN_NOME_MEDICO '
      'FROM receita_otica ro'
      'INNER JOIN pessoa_usuario pu ON ro.id_pessoa_usuario = pu.id'
      'INNER JOIN pessoa p ON ro.id_pessoa = p.id'
      'INNER JOIN filial f ON ro.id_filial = f.id'
      'LEFT JOIN medico m ON ro.id_medico = m.id'
      'where ro.id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqReceitaOticaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqReceitaOticaDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqReceitaOticaID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object fdqReceitaOticaID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object fdqReceitaOticaDH_PREVISAO_ENTREGA: TDateTimeField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dh. Previs'#227'o de Entrega'
      FieldName = 'DH_PREVISAO_ENTREGA'
      Origin = 'DH_PREVISAO_ENTREGA'
    end
    object fdqReceitaOticaID_MEDICO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo do M'#233'dico'
      FieldName = 'ID_MEDICO'
      Origin = 'ID_MEDICO'
    end
    object fdqReceitaOticaESPECIFICACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Especifica'#231#227'o'
      FieldName = 'ESPECIFICACAO'
      Origin = 'ESPECIFICACAO'
    end
    object fdqReceitaOticaOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqReceitaOticaID_VENDA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object fdqReceitaOticaLONGE_DIREITO_ESFERICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Direito Esf'#233'rico'
      FieldName = 'LONGE_DIREITO_ESFERICO'
      Origin = 'LONGE_DIREITO_ESFERICO'
      Size = 10
    end
    object fdqReceitaOticaLONGE_DIREITO_CILINDRICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Direito Cilindrico'
      FieldName = 'LONGE_DIREITO_CILINDRICO'
      Origin = 'LONGE_DIREITO_CILINDRICO'
      Size = 10
    end
    object fdqReceitaOticaLONGE_DIREITO_EIXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Direito Eixo'
      FieldName = 'LONGE_DIREITO_EIXO'
      Origin = 'LONGE_DIREITO_EIXO'
      Size = 10
    end
    object fdqReceitaOticaLONGE_ESQUERDO_ESFERICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Esquerdo Esferico'
      FieldName = 'LONGE_ESQUERDO_ESFERICO'
      Origin = 'LONGE_ESQUERDO_ESFERICO'
      Size = 10
    end
    object fdqReceitaOticaLONGE_ESQUERDO_CILINDRICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Esquerdo Cilindrico'
      FieldName = 'LONGE_ESQUERDO_CILINDRICO'
      Origin = 'LONGE_ESQUERDO_CILINDRICO'
      Size = 10
    end
    object fdqReceitaOticaLONGE_ESQUERDO_EIXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe Esquerdo Eixo'
      FieldName = 'LONGE_ESQUERDO_EIXO'
      Origin = 'LONGE_ESQUERDO_EIXO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_DIREITO_ESFERICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Direito Esferico'
      FieldName = 'PERTO_DIREITO_ESFERICO'
      Origin = 'PERTO_DIREITO_ESFERICO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_DIREITO_CILINDRICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Direito Cilindrico'
      FieldName = 'PERTO_DIREITO_CILINDRICO'
      Origin = 'PERTO_DIREITO_CILINDRICO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_DIREITO_EIXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Direito Eixo'
      FieldName = 'PERTO_DIREITO_EIXO'
      Origin = 'PERTO_DIREITO_EIXO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_ESQUERDO_ESFERICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Esquerdo Esf'#233'rico'
      FieldName = 'PERTO_ESQUERDO_ESFERICO'
      Origin = 'PERTO_ESQUERDO_ESFERICO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_ESQUERDO_CILINDRICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Esquerdo Cilindrico'
      FieldName = 'PERTO_ESQUERDO_CILINDRICO'
      Origin = 'PERTO_ESQUERDO_CILINDRICO'
      Size = 10
    end
    object fdqReceitaOticaPERTO_ESQUERDO_EIXO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto Esquerdo Eixo'
      FieldName = 'PERTO_ESQUERDO_EIXO'
      Origin = 'PERTO_ESQUERDO_EIXO'
      Size = 10
    end
    object fdqReceitaOticaID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqReceitaOticaALTURA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Altura'
      FieldName = 'ALTURA'
      Size = 14
    end
    object fdqReceitaOticaADICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Adi'#231#227'o'
      FieldName = 'ADICAO'
      Size = 14
    end
    object fdqReceitaOticaPERTO_DNP_DIREITO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto DNP Direito'
      FieldName = 'PERTO_DNP_DIREITO'
      Origin = 'PERTO_DNP_DIREITO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaPERTO_DNP_ESQUERDO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto DNP Esquerdo'
      FieldName = 'PERTO_DNP_ESQUERDO'
      Origin = 'PERTO_DNP_ESQUERDO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaPERTO_DNP_GERAL: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Perto DNP Geral'
      FieldName = 'PERTO_DNP_GERAL'
      Origin = 'PERTO_DNP_GERAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaLONGE_DNP_DIREITO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe DNP Direito'
      FieldName = 'LONGE_DNP_DIREITO'
      Origin = 'LONGE_DNP_DIREITO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaLONGE_DNP_ESQUERDO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe DNP Esquerdo'
      FieldName = 'LONGE_DNP_ESQUERDO'
      Origin = 'LONGE_DNP_ESQUERDO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaLONGE_DNP_GERAL: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Longe DNP Geral'
      FieldName = 'LONGE_DNP_GERAL'
      Origin = 'LONGE_DNP_GERAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 14
      Size = 2
    end
    object fdqReceitaOticaJOIN_NOME_USUARIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqReceitaOticaJOIN_NOME_PESSOA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object fdqReceitaOticaJOIN_FANTASIA_FILIAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Fantasia'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object fdqReceitaOticaJOIN_NOME_MEDICO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'M'#233'dico'
      FieldName = 'JOIN_NOME_MEDICO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
  object dspReceitaOtica: TDataSetProvider
    DataSet = fdqReceitaOtica
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
