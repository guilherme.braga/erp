unit uSMChecklist;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  Datasnap.Provider, FireDAC.Comp.Client, DataSnap.DSProviderDataModuleAdapter,
  Data.FireDACJSONReflect, uGBFDQuery;

type
  TSMCheckList = class(TDSServerModule)
    fdqChecklist: TgbFDQuery;
    dspChecklist: TDataSetProvider;
    fdqChecklistID: TFDAutoIncField;
    fdqChecklistDESCRICAO: TStringField;
    fdqChecklistBO_ATIVO: TStringField;
    fdqChecklistItem: TgbFDQuery;
    fdqChecklistItemID: TFDAutoIncField;
    fdqChecklistItemNR_ITEM: TIntegerField;
    fdqChecklistItemDESCRICAO: TStringField;
    fdqChecklistItemID_CHECKLIST: TIntegerField;
    fdqChecklistItemBO_ATIVO: TStringField;
    dsChecklist: TDataSource;
  private
    { Private declarations }
  public
    function GetTodosItensAtivosDoChecklist(const AIdChecklist: Integer): TFDJSONDataSets;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uChecklistServ;

{$R *.dfm}

{ TSMCheckList }

function TSMCheckList.GetTodosItensAtivosDoChecklist(
  const AIdChecklist: Integer): TFDJSONDataSets;
begin
  result := TCheckListServ.GetTodosItensAtivosDoChecklist(AIdChecklist);
end;

end.

