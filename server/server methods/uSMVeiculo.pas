unit uSMVeiculo;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMVeiculo = class(TDSServerModule)
    fdqVeiculo: TgbFDQuery;
    dspVeiculo: TDataSetProvider;
    fdqVeiculoID: TFDAutoIncField;
    fdqVeiculoPLACA: TStringField;
    fdqVeiculoANO: TIntegerField;
    fdqVeiculoCOMBUSTIVEL: TStringField;
    fdqVeiculoCHASSI: TStringField;
    fdqVeiculoRENAVAM: TStringField;
    fdqVeiculoID_MODELO: TIntegerField;
    fdqVeiculoID_MARCA: TIntegerField;
    fdqVeiculoID_COR: TIntegerField;
    fdqVeiculoID_PESSOA: TIntegerField;
    fdqVeiculoID_FILIAL: TIntegerField;
    fdqVeiculoJOIN_DESCRICAO_MARCA: TStringField;
    fdqVeiculoJOIN_DESCRICAO_MODELO: TStringField;
    fdqVeiculoJOIN_DESCRICAO_COR: TStringField;
    fdqVeiculoJOIN_NOME_PESSOA: TStringField;
    fdqVeiculoJOIN_FANTASIA_FILIAL: TStringField;
    fdqVeiculoBO_ATIVO: TStringField;
  private
    { Private declarations }
  public
    function GetVeiculo(const AIdVeiculo: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uVeiculoServ, rest.json;

{$R *.dfm}

{ TSMVeiculo }

function TSMVeiculo.GetVeiculo(const AIdVeiculo: Integer): String;
begin
  result := TJSON.ObjectToJsonString(
    TVeiculoServ.GetVeiculo(AIdVeiculo));
end;

end.

