object SMFinanceiro: TSMFinanceiro
  OldCreateOrder = False
  Height = 407
  Width = 494
  object fdqCentroResultado: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select cr.*'
      '      ,gcr.descricao AS JOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO '
      'from centro_resultado cr'
      'inner join grupo_centro_resultado gcr '
      '        on cr.id_grupo_centro_resultado = gcr.id '
      'where cr.id = :id')
    Left = 48
    Top = 8
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqCentroResultadoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqCentroResultadoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCentroResultadoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqCentroResultadoID_GRUPO_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Grupo de Centro de Resultado'
      FieldName = 'ID_GRUPO_CENTRO_RESULTADO'
      Origin = 'ID_GRUPO_CENTRO_RESULTADO'
      Required = True
    end
    object fdqCentroResultadoJOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Grupo de Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqCentroResultadoBO_EXIBIR_EXTRATO_PLANO_CONTA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Exibir Extrato de Plano de Conta'
      FieldName = 'BO_EXIBIR_EXTRATO_PLANO_CONTA'
      Origin = 'BO_EXIBIR_EXTRATO_PLANO_CONTA'
      FixedChar = True
      Size = 1
    end
  end
  object dspCentroResultado: TDataSetProvider
    DataSet = fdqCentroResultado
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 8
  end
  object dspContaAnalise: TDataSetProvider
    DataSet = fdqContaAnalise
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 64
  end
  object fdqContaAnalise: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from conta_analise where id = :id')
    Left = 48
    Top = 64
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaAnaliseID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqContaAnaliseDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqContaAnaliseSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
    end
    object fdqContaAnaliseNIVEL: TIntegerField
      DisplayLabel = 'N'#237'vel'
      FieldName = 'NIVEL'
      Origin = 'NIVEL'
      Required = True
    end
    object fdqContaAnaliseOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaAnaliseBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqContaAnaliseBO_EXIBIR_RESUMO_CONTA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Exibir no Resumo de Contas?'
      FieldName = 'BO_EXIBIR_RESUMO_CONTA'
      Origin = 'BO_EXIBIR_RESUMO_CONTA'
      FixedChar = True
      Size = 1
    end
  end
  object dspContaCorrente: TDataSetProvider
    DataSet = fdqContaCorrente
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 120
  end
  object fdqContaCorrente: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from conta_corrente where id = :id')
    Left = 48
    Top = 120
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaCorrenteID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      IdentityInsert = True
    end
    object fdqContaCorrenteDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqContaCorrenteBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaCorrenteDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaCorrenteBO_VISUALIZA_FLUXO_CAIXA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Visualiza no Fluxo de Caixa'
      FieldName = 'BO_VISUALIZA_FLUXO_CAIXA'
      Origin = 'BO_VISUALIZA_FLUXO_CAIXA'
      FixedChar = True
      Size = 1
    end
  end
  object dspContaCorrenteMovimento: TDataSetProvider
    DataSet = fdqContaCorrenteMovimento
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 176
  end
  object fdqContaCorrenteMovimento: TgbFDQuery
    AfterPost = fdqContaCorrenteMovimentoAfterPost
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ccm.* '
      '      ,ca.descricao as JOIN_DESCRICAO_CONTA_ANALISE'
      '      ,cr.descricao as JOIN_DESCRICAO_CENTRO_RESULTADO'
      '      ,cc.descricao as JOIN_DESCRICAO_CONTA_CORRENTE'
      '      ,cp.origem    AS JOIN_ORIGEM_CHAVE_PROCESSO'
      '      ,oc.descricao as JOIN_DESCRICAO_OPERADORA_CARTAO'
      'from conta_corrente_movimento ccm'
      'inner join conta_corrente cc on ccm.id_conta_corrente = cc.id'
      'inner join conta_analise ca on ccm.id_conta_analise = ca.id'
      
        'inner join centro_resultado cr on ccm.id_centro_resultado = cr.i' +
        'd'
      'left join operadora_cartao oc on ccm.id_operadora_cartao = oc.id'
      'left join chave_processo cp on ccm.id_chave_processo = cp.id'
      'where ccm.id = :id')
    Left = 48
    Top = 176
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqContaCorrenteMovimentoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqContaCorrenteMovimentoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqContaCorrenteMovimentoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object fdqContaCorrenteMovimentoDT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'DT_EMISSAO'
      Origin = 'DT_EMISSAO'
      Required = True
    end
    object fdqContaCorrenteMovimentoDT_MOVIMENTO: TDateField
      DisplayLabel = 'Dt. Movimento'
      FieldName = 'DT_MOVIMENTO'
      Origin = 'DT_MOVIMENTO'
      Required = True
    end
    object fdqContaCorrenteMovimentoTP_MOVIMENTO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TP_MOVIMENTO'
      Origin = 'TP_MOVIMENTO'
      Required = True
      Size = 10
    end
    object fdqContaCorrenteMovimentoBO_CONCILIADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conciliado?'
      FieldName = 'BO_CONCILIADO'
      Origin = 'BO_CONCILIADO'
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoDH_CONCILIADO: TDateTimeField
      AutoGenerateValue = arDefault
      FieldName = 'DH_CONCILIADO'
      Origin = 'DH_CONCILIADO'
    end
    object fdqContaCorrenteMovimentoOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqContaCorrenteMovimentoVL_MOVIMENTO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Valor'
      FieldName = 'VL_MOVIMENTO'
      Origin = 'VL_MOVIMENTO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaCorrenteMovimentoVL_SALDO_CONCILIADO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Saldo Conciliado'
      FieldName = 'VL_SALDO_CONCILIADO'
      Origin = 'VL_SALDO_CONCILIADO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaCorrenteMovimentoVL_SALDO_GERAL: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Saldo Geral'
      FieldName = 'VL_SALDO_GERAL'
      Origin = 'VL_SALDO_GERAL'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqContaCorrenteMovimentoDOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object fdqContaCorrenteMovimentoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object fdqContaCorrenteMovimentoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqContaCorrenteMovimentoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqContaCorrenteMovimentoID_MOVIMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_MOVIMENTO_ORIGEM'
      Origin = 'ID_MOVIMENTO_ORIGEM'
    end
    object fdqContaCorrenteMovimentoID_MOVIMENTO_DESTINO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_MOVIMENTO_DESTINO'
      Origin = 'ID_MOVIMENTO_DESTINO'
    end
    object fdqContaCorrenteMovimentoBO_MOVIMENTO_ORIGEM: TStringField
      FieldName = 'BO_MOVIMENTO_ORIGEM'
      Origin = 'BO_MOVIMENTO_ORIGEM'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoBO_MOVIMENTO_DESTINO: TStringField
      FieldName = 'BO_MOVIMENTO_DESTINO'
      Origin = 'BO_MOVIMENTO_DESTINO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoBO_TRANSFERENCIA: TStringField
      FieldName = 'BO_TRANSFERENCIA'
      Origin = 'BO_TRANSFERENCIA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoID_CONTA_CORRENTE_DESTINO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente de Destino'
      FieldName = 'ID_CONTA_CORRENTE_DESTINO'
      Origin = 'ID_CONTA_CORRENTE_DESTINO'
    end
    object fdqContaCorrenteMovimentoID_CHAVE_PROCESSO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Chave Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
    end
    object fdqContaCorrenteMovimentoID_DOCUMENTO_ORIGEM: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object fdqContaCorrenteMovimentoTP_DOCUMENTO_ORIGEM: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object fdqContaCorrenteMovimentoPARCELAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Parcelamento'
      FieldName = 'PARCELAMENTO'
      Origin = 'PARCELAMENTO'
      Size = 50
    end
    object fdqContaCorrenteMovimentoDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object fdqContaCorrenteMovimentoCHEQUE_SACADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object fdqContaCorrenteMovimentoCHEQUE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object fdqContaCorrenteMovimentoCHEQUE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object fdqContaCorrenteMovimentoCHEQUE_DT_EMISSAO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object fdqContaCorrenteMovimentoCHEQUE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object fdqContaCorrenteMovimentoCHEQUE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object fdqContaCorrenteMovimentoCHEQUE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object fdqContaCorrenteMovimentoID_OPERADORA_CARTAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object fdqContaCorrenteMovimentoJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Origem Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object fdqContaCorrenteMovimentoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaCorrenteMovimentoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaCorrenteMovimentoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaCorrenteMovimentoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cart'#227'o: Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqContaCorrenteMovimentoID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object fdqContaCorrenteMovimentoBO_CHEQUE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque'
      FieldName = 'BO_CHEQUE'
      Origin = 'BO_CHEQUE'
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoBO_CHEQUE_PROPRIO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque Pr'#243'prio'
      FieldName = 'BO_CHEQUE_PROPRIO'
      Origin = 'BO_CHEQUE_PROPRIO'
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoBO_CHEQUE_TERCEIRO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque de Terceiro'
      FieldName = 'BO_CHEQUE_TERCEIRO'
      Origin = 'BO_CHEQUE_TERCEIRO'
      FixedChar = True
      Size = 1
    end
    object fdqContaCorrenteMovimentoCHEQUE_DT_VENCIMENTO: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object fdqContaCorrenteMovimentoBO_PROCESSO_AMORTIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Processo Amortizado'
      FieldName = 'BO_PROCESSO_AMORTIZADO'
      Origin = 'BO_PROCESSO_AMORTIZADO'
      FixedChar = True
      Size = 1
    end
  end
  object dspPlanoConta: TDataSetProvider
    DataSet = fdqPlanoConta
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 232
  end
  object fdqPlanoConta: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select pc.*'
      '      ,cr.descricao AS JOIN_CENTRO_RESULTADO'
      'from plano_conta pc '
      'inner join centro_resultado cr on pc.id_centro_resultado = cr.id'
      'where pc.id = :id')
    Left = 48
    Top = 232
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqPlanoContaID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPlanoContaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object fdqPlanoContaJOIN_CENTRO_RESULTADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object fdqPlanoContaItem: TgbFDQuery
    MasterSource = dsPlanoConta
    MasterFields = 'ID'
    DetailFields = 'ID_PLANO_CONTA'
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select pci.*'
      'from plano_conta_item pci'
      'where pci.id_plano_conta = :id')
    Left = 48
    Top = 280
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqPlanoContaItemID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPlanoContaItemID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object fdqPlanoContaItemID_PLANO_CONTA: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Conta'
      FieldName = 'ID_PLANO_CONTA'
      Origin = 'ID_PLANO_CONTA'
    end
  end
  object fdqGrupoCentroResultado: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from grupo_centro_resultado where id = :id')
    Left = 48
    Top = 336
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object fdqGrupoCentroResultadoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqGrupoCentroResultadoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqGrupoCentroResultadoOBSERVACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 5000
    end
  end
  object dspGrupoCentroResultado: TDataSetProvider
    DataSet = fdqGrupoCentroResultado
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 336
  end
  object dsPlanoConta: TDataSource
    DataSet = fdqPlanoConta
    Left = 110
    Top = 231
  end
  object fdqConsulta: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    Left = 440
    Top = 16
  end
  object fdqPlanoPagamento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select * from plano_pagamento where id = :id')
    Left = 200
    Top = 40
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqPlanoPagamentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqPlanoPagamentoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqPlanoPagamentoQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object fdqPlanoPagamentoPRAZO: TStringField
      DisplayLabel = 'Prazo'
      FieldName = 'PRAZO'
      Origin = 'PRAZO'
      Size = 255
    end
    object fdqPlanoPagamentoTIPO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
    end
    object fdqPlanoPagamentoDIA_PARCELA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Dia Fixo'
      FieldName = 'DIA_PARCELA'
      Origin = 'DIA_PARCELA'
    end
    object fdqPlanoPagamentoBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dspPlanoPagamento: TDataSetProvider
    DataSet = fdqPlanoPagamento
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 232
    Top = 40
  end
  object fdqFormaPagamento: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select fp.* '
      '      ,tq.descricao AS JOIN_DESCRICAO_TIPO_QUITACAO'
      '      ,tq.tipo AS JOIN_TIPO_TIPO_QUITACAO'
      'from forma_pagamento fp '
      'inner join tipo_quitacao tq on fp.id_tipo_quitacao = tq.id'
      'where fp.id = :id'
      '')
    Left = 200
    Top = 96
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqFormaPagamentoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqFormaPagamentoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqFormaPagamentoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo de Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object fdqFormaPagamentoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object fdqFormaPagamentoJOIN_TIPO_TIPO_QUITACAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Tipo do Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
  end
  object dspFormaPagamento: TDataSetProvider
    DataSet = fdqFormaPagamento
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 232
    Top = 96
  end
  object fdqTipoQuitacao: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'SELECT tq.* '
      '  FROM tipo_quitacao tq '
      ' WHERE tq.id = :id')
    Left = 200
    Top = 152
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqTipoQuitacaoID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqTipoQuitacaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object fdqTipoQuitacaoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 50
    end
    object fdqTipoQuitacaoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dspTipoQuitacao: TDataSetProvider
    DataSet = fdqTipoQuitacao
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 272
    Top = 152
  end
end
