object SMImpostoICMS: TSMImpostoICMS
  OldCreateOrder = False
  Height = 150
  Width = 215
  object fdqImpostoICMS: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select ii.* '
      
        '       ,(SELECT descricao FROM cst_csosn WHERE id = ii.id_cst_cs' +
        'osn) AS JOIN_DESCRICAO_CST_CSOSN'
      
        '       ,(SELECT codigo_cst FROM cst_csosn WHERE id = ii.id_cst_c' +
        'sosn) AS JOIN_CODIGO_CST_CSOSN'
      'from IMPOSTO_ICMS ii'
      'where id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqImpostoICMSID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqImpostoICMSBO_INCLUIR_FRETE_NA_BASE_IPI: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Incluir Frete na Base IPI'
      FieldName = 'BO_INCLUIR_FRETE_NA_BASE_IPI'
      Origin = 'BO_INCLUIR_FRETE_NA_BASE_IPI'
      FixedChar = True
      Size = 1
    end
    object fdqImpostoICMSBO_INCLUIR_FRETE_NA_BASE_ICMS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Incluir Frete na Base do ICMS'
      FieldName = 'BO_INCLUIR_FRETE_NA_BASE_ICMS'
      Origin = 'BO_INCLUIR_FRETE_NA_BASE_ICMS'
      FixedChar = True
      Size = 1
    end
    object fdqImpostoICMSBO_INCLUIR_IPI_NA_BASE_ICMS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Incluir IPI na Base do ICMS'
      FieldName = 'BO_INCLUIR_IPI_NA_BASE_ICMS'
      Origin = 'BO_INCLUIR_IPI_NA_BASE_ICMS'
      FixedChar = True
      Size = 1
    end
    object fdqImpostoICMSPERC_REDUCAO_ICMS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Percentual de Redu'#231#227'o do ICMS ST'
      FieldName = 'PERC_REDUCAO_ICMS_ST'
      Origin = 'PERC_REDUCAO_ICMS_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSMODALIDADE_BASE_ICMS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modalidade Base ICMS'
      FieldName = 'MODALIDADE_BASE_ICMS'
      Origin = 'MODALIDADE_BASE_ICMS'
    end
    object fdqImpostoICMSMODALIDADE_BASE_ICMS_ST: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Modalidade Base ICMS ST'
      FieldName = 'MODALIDADE_BASE_ICMS_ST'
      Origin = 'MODALIDADE_BASE_ICMS_ST'
    end
    object fdqImpostoICMSPERC_REDUCAO_ICMS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Redu'#231#227'o ICMS'
      FieldName = 'PERC_REDUCAO_ICMS'
      Origin = 'PERC_REDUCAO_ICMS'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSPERC_ALIQUOTA_ICMS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota ICMS'
      FieldName = 'PERC_ALIQUOTA_ICMS'
      Origin = 'PERC_ALIQUOTA_ICMS'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSPERC_ALIQUOTA_ICMS_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota ICMS ST'
      FieldName = 'PERC_ALIQUOTA_ICMS_ST'
      Origin = 'PERC_ALIQUOTA_ICMS_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSPERC_MVA_AJUSTADO_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% MVA Ajustado ST'
      FieldName = 'PERC_MVA_AJUSTADO_ST'
      Origin = 'PERC_MVA_AJUSTADO_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSPERC_MVA_PROPRIO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% MVA Pr'#243'prio'
      FieldName = 'PERC_MVA_PROPRIO'
      Origin = 'PERC_MVA_PROPRIO'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSMOTIVO_DESONERACAO_ICMS: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Motivo de Desonera'#231#227'o do ICMS'
      FieldName = 'MOTIVO_DESONERACAO_ICMS'
      Origin = 'MOTIVO_DESONERACAO_ICMS'
    end
    object fdqImpostoICMSPERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Al'#237'quota ICMS Pr'#243'prio para ST'
      FieldName = 'PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST'
      Origin = 'PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSBO_DESTACAR_VALOR_ICMS: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Destacar Valor do ICMS'
      FieldName = 'BO_DESTACAR_VALOR_ICMS'
      Origin = 'BO_DESTACAR_VALOR_ICMS'
      FixedChar = True
      Size = 1
    end
    object fdqImpostoICMSPERC_BASE_ICMS_PROPRIO: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Base ICMS Pr'#243'prio'
      FieldName = 'PERC_BASE_ICMS_PROPRIO'
      Origin = 'PERC_BASE_ICMS_PROPRIO'
      Precision = 24
      Size = 9
    end
    object fdqImpostoICMSID_CST_CSOSN: TIntegerField
      DisplayLabel = 'C'#243'digo do CST CSOSN'
      FieldName = 'ID_CST_CSOSN'
      Origin = 'ID_CST_CSOSN'
      Required = True
    end
    object fdqImpostoICMSJOIN_DESCRICAO_CST_CSOSN: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CST CSOSN'
      FieldName = 'JOIN_DESCRICAO_CST_CSOSN'
      Origin = 'JOIN_DESCRICAO_CST_CSOSN'
      ProviderFlags = []
      Size = 255
    end
    object fdqImpostoICMSJOIN_CODIGO_CST_CSOSN: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo CST CSOSN'
      FieldName = 'JOIN_CODIGO_CST_CSOSN'
      Origin = 'JOIN_CODIGO_CST_CSOSN'
      ProviderFlags = []
      Size = 5
    end
  end
  object dspImpostoICMS: TDataSetProvider
    DataSet = fdqImpostoICMS
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
