unit uSMLotePagamento;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter,
  Data.FireDACJSONReflect;

type
  TSMLotePagamento = class(TDSServerModule)
    fdqLotePagamento: TgbFDQuery;
    fdqLotePagamentoTitulo: TgbFDQuery;
    dsLotePagamento: TDataSource;
    dspLotePagamento: TDataSetProvider;
    fdqLotePagamentoQuitacao: TgbFDQuery;
    fdqLotePagamentoID: TFDAutoIncField;
    fdqLotePagamentoDH_CADASTRO: TDateTimeField;
    fdqLotePagamentoSTATUS: TStringField;
    fdqLotePagamentoID_FILIAL: TIntegerField;
    fdqLotePagamentoID_PESSOA_USUARIO: TIntegerField;
    fdqLotePagamentoID_CHAVE_PROCESSO: TIntegerField;
    fdqLotePagamentoDH_EFETIVACAO: TDateTimeField;
    fdqLotePagamentoOBSERVACAO: TBlobField;
    fdqLotePagamentoJOIN_FANTASIA_FILIAL: TStringField;
    fdqLotePagamentoJOIN_USUARIO: TStringField;
    fdqLotePagamentoJOIN_CHAVE_PROCESSO: TStringField;
    fdqLotePagamentoTituloID: TFDAutoIncField;
    fdqLotePagamentoTituloVL_ACRESCIMO: TFMTBCDField;
    fdqLotePagamentoTituloVL_DESCONTO: TFMTBCDField;
    fdqLotePagamentoTituloID_LOTE_PAGAMENTO: TIntegerField;
    fdqLotePagamentoTituloID_CONTA_PAGAR: TIntegerField;
    fdqLotePagamentoTituloDH_CADASTRO: TDateTimeField;
    fdqLotePagamentoTituloDOCUMENTO: TStringField;
    fdqLotePagamentoTituloDESCRICAO: TStringField;
    fdqLotePagamentoTituloVL_TITULO: TFMTBCDField;
    fdqLotePagamentoTituloVL_QUITADO: TFMTBCDField;
    fdqLotePagamentoTituloVL_ABERTO: TFMTBCDField;
    fdqLotePagamentoTituloQT_PARCELA: TIntegerField;
    fdqLotePagamentoTituloNR_PARCELA: TIntegerField;
    fdqLotePagamentoTituloDT_VENCIMENTO: TDateField;
    fdqLotePagamentoTituloSEQUENCIA: TStringField;
    fdqLotePagamentoTituloBO_VENCIDO: TStringField;
    fdqLotePagamentoTituloOBSERVACAO: TBlobField;
    fdqLotePagamentoTituloID_PESSOA: TIntegerField;
    fdqLotePagamentoTituloID_CONTA_ANALISE: TIntegerField;
    fdqLotePagamentoTituloID_CENTRO_RESULTADO: TIntegerField;
    fdqLotePagamentoTituloSTATUS: TStringField;
    fdqLotePagamentoTituloID_CHAVE_PROCESSO: TIntegerField;
    fdqLotePagamentoTituloID_FORMA_PAGAMENTO: TIntegerField;
    fdqLotePagamentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqLotePagamentoTituloDT_COMPETENCIA: TDateField;
    fdqLotePagamentoTituloID_DOCUMENTO_ORIGEM: TIntegerField;
    fdqLotePagamentoTituloTP_DOCUMENTO_ORIGEM: TStringField;
    fdqLotePagamentoQuitacaoID: TFDAutoIncField;
    fdqLotePagamentoQuitacaoOBSERVACAO: TBlobField;
    fdqLotePagamentoQuitacaoID_TIPO_QUITACAO: TIntegerField;
    fdqLotePagamentoQuitacaoDT_QUITACAO: TDateField;
    fdqLotePagamentoQuitacaoID_CONTA_CORRENTE: TIntegerField;
    fdqLotePagamentoQuitacaoVL_DESCONTO: TFMTBCDField;
    fdqLotePagamentoQuitacaoVL_ACRESCIMO: TFMTBCDField;
    fdqLotePagamentoQuitacaoVL_QUITACAO: TFMTBCDField;
    fdqLotePagamentoQuitacaoNR_ITEM: TIntegerField;
    fdqLotePagamentoQuitacaoPERC_ACRESCIMO: TBCDField;
    fdqLotePagamentoQuitacaoPERC_DESCONTO: TBCDField;
    fdqLotePagamentoQuitacaoVL_TOTAL: TFMTBCDField;
    fdqLotePagamentoQuitacaoID_CONTA_ANALISE: TIntegerField;
    fdqLotePagamentoQuitacaoID_LOTE_PAGAMENTO: TIntegerField;
    fdqLotePagamentoTituloVL_ACRESCIMO_CP: TFMTBCDField;
    fdqLotePagamentoTituloVL_DESCONTO_CP: TFMTBCDField;
    fdqLotePagamentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    fdqLotePagamentoQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    fdqLotePagamentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    fdqLotePagamentoVL_QUITADO: TFMTBCDField;
    fdqLotePagamentoVL_ACRESCIMO: TFMTBCDField;
    fdqLotePagamentoVL_DESCONTO: TFMTBCDField;
    fdqLotePagamentoVL_QUITADO_LIQUIDO: TFMTBCDField;
    fdqLotePagamentoVL_ABERTO: TFMTBCDField;
    fdqLotePagamentoQuitacaoDOCUMENTO: TStringField;
    fdqLotePagamentoQuitacaoDESCRICAO: TStringField;
    fdqLotePagamentoTituloDT_DOCUMENTO: TDateField;
    fdqLotePagamentoTituloID_CONTA_CORRENTE: TIntegerField;
    fdqLotePagamentoTituloID_FILIAL: TIntegerField;
    fdqLotePagamentoQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    fdqLotePagamentoQuitacaoCHEQUE_SACADO: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_BANCO: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    fdqLotePagamentoQuitacaoCHEQUE_AGENCIA: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_NUMERO: TIntegerField;
    fdqLotePagamentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    fdqLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    fdqLotePagamentoQuitacaoVL_JUROS: TFMTBCDField;
    fdqLotePagamentoQuitacaoPERC_JUROS: TFMTBCDField;
    fdqLotePagamentoQuitacaoVL_MULTA: TFMTBCDField;
    fdqLotePagamentoQuitacaoPERC_MULTA: TFMTBCDField;
    fdqLotePagamentoTituloVL_JUROS: TFMTBCDField;
    fdqLotePagamentoTituloPERC_JUROS: TFMTBCDField;
    fdqLotePagamentoTituloVL_MULTA: TFMTBCDField;
    fdqLotePagamentoTituloPERC_MULTA: TFMTBCDField;
    fdqLotePagamentoTituloJOIN_NOME_PESSOA: TStringField;
    fdqLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    fdqLotePagamentoQuitacaoID_USUARIO_BAIXA: TIntegerField;
    fdqLotePagamentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
  private
    { Private declarations }
  public
    function RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
    function RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
    function GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
    function RemoverLancamentosAgrupados(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
    function GetContasAPagar(AIdsContaPagar : string): TFDJSONDataSets;
    { Public declarations }
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  uLotePagamentoProxy,
  uLotePagamentoServ;

{$R *.dfm}

{ TSMLotePagamento }

function TSMLotePagamento.GerarLancamentosAgrupados(
  AChaveProcesso: Integer): string;
var
  IdLotePagamento : Integer;
begin
  IdLotePagamento := TLotePagamentoServ.GetIdLotePagamentoPelaChaveProcesso(AChaveProcesso);
  Result := TLotePagamentoServ.GerarLancamentosAgrupados(IdLotePagamento);
end;

function TSMLotePagamento.RemoverLancamentosAgrupados(
  AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
begin
  Result := TLotePagamentoServ.RemoverLancamentosAgrupados(AIdLotePagamento, AGerarContraPartida);
end;

function TSMLotePagamento.GetContasAPagar(
  AIdsContaPagar: string): TFDJSONDataSets;
begin
  Result := TLotePagamentoServ.GetContasAPagar(AIdsContaPagar);
end;

function TSMLotePagamento.RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
begin
  Result := TLotePagamentoServ.RemoverMovimentosIndividuais(AIdLotePagamento);
end;

function TSMLotePagamento.RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
begin
  Result := TLotePagamentoServ.RemoverQuitacoesIndividuais(AIdLotePagamento);
end;



end.

