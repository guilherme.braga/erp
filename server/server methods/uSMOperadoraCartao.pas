unit uSMOperadoraCartao;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  DataSnap.DSProviderDataModuleAdapter;

type
  TSMOperadoraCartao = class(TDSServerModule)
    fdqOperadoraCartao: TgbFDQuery;
    dspOperadoraCartao: TDataSetProvider;
    fdqOperadoraCartaoDESCRICAO: TStringField;
    fdqOperadoraCartaoID_PESSOA: TIntegerField;
    fdqOperadoraCartaoJOIN_NOME_PESSOA: TStringField;
    fdqOperadoraCartaoBO_ATIVO: TStringField;
    fdqOperadoraCartaoID: TFDAutoIncField;
  private
    { Private declarations }
  public
    class function GetOperadoraCartao(AIdOperadoraCartao: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TSMOperadoraCartao }

Uses uOperadoraCartaoProxy, uOperadoraCartaoServ, REST.JSON;

class function TSMOperadoraCartao.GetOperadoraCartao(
  AIdOperadoraCartao: Integer): String;
var
  AOperadoraCartao: TOperadoraCartaoProxy;
begin
  AOperadoraCartao := TOperadoraCartaoServ.GetOperadoraCartao(AIdOperadoraCartao);
  try
    result := TJSON.ObjectToJsonString(AOperadoraCartao);
  finally
    FreeAndNil(AOperadoraCartao);
  end;
end;

end.

