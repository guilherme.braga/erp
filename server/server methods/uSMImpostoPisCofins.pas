unit uSMImpostoPisCofins;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,
  Datasnap.DSProviderDataModuleAdapter;
type
  TSMImpostoPisCofins = class(TDSServerModule)
    fdqImpostoPisCofins: TgbFDQuery;
    fdqImpostoPisCofinsID: TFDAutoIncField;
    dspImpostoPisCofins: TDataSetProvider;
    fdqImpostoPisCofinsPERC_ALIQUOTA_PIS: TFMTBCDField;
    fdqImpostoPisCofinsPERC_ALIQUOTA_COFINS: TFMTBCDField;
    fdqImpostoPisCofinsPERC_ALIQUOTA_PIS_ST: TFMTBCDField;
    fdqImpostoPisCofinsPERC_ALIQUOTA_COFINS_ST: TFMTBCDField;
    fdqImpostoPisCofinsID_CST_PIS_COFINS: TIntegerField;
    fdqImpostoPisCofinsJOIN_DESCRICAO_CST_PIS_COFINS: TStringField;
    fdqImpostoPisCofinsJOIN_CODIGO_CST_PIS_COFINS: TStringField;
  private
    { Private declarations }
  public
    function GetImpostoPisCofins(AIdImpostoPisCofins: Integer): String;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uImpostoPisCofinsProxy, uImpostoPisCofinsServ, REST.JSON;

{$R *.dfm}

{ TSMImpostoPisCofins }

function TSMImpostoPisCofins.GetImpostoPisCofins(
  AIdImpostoPisCofins: Integer): String;
begin
  result := TJson.ObjectToJsonString(TImpostoPisCofinsServ.GetImpostoPisCofins(AIdImpostoPisCofins));
end;

end.

