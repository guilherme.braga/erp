object SMCarteira: TSMCarteira
  OldCreateOrder = False
  Height = 309
  Width = 399
  object fdqCarteira: TgbFDQuery
    Connection = Dm.Connection
    FetchOptions.AssignedValues = [evDetailOptimize]
    FetchOptions.DetailOptimize = False
    SQL.Strings = (
      'select carteira.*'
      
        '      ,conta_corrente.descricao AS JOIN_DESCRICAO_CONTA_CORRENTE' +
        ' '
      '  from carteira '
      
        'left join conta_corrente on carteira.id_conta_corrente = conta_c' +
        'orrente.id'
      ' where carteira.id = :id')
    Left = 40
    Top = 24
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object fdqCarteiraID: TFDAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object fdqCarteiraDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object fdqCarteiraPERC_JUROS: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqCarteiraPERC_MULTA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdqCarteiraBO_ATIVO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object fdqCarteiraCBX_ARQUIVO_LICENCA: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Arquivo de Licen'#231'a'
      FieldName = 'CBX_ARQUIVO_LICENCA'
      Origin = 'CBX_ARQUIVO_LICENCA'
    end
    object fdqCarteiraOBSERVACAO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object fdqCarteiraTIPO_BOLETO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TIPO_BOLETO'
      Origin = 'TIPO_BOLETO'
      Size = 25
    end
    object fdqCarteiraCARENCIA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Car'#234'ncia'
      FieldName = 'CARENCIA'
      Origin = 'CARENCIA'
    end
    object fdqCarteiraCEDENTE_NOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Nome'
      FieldName = 'CEDENTE_NOME'
      Origin = 'CEDENTE_NOME'
      Size = 255
    end
    object fdqCarteiraCEDENTE_DOC_FEDERAL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Documento Federal'
      FieldName = 'CEDENTE_DOC_FEDERAL'
      Origin = 'CEDENTE_DOC_FEDERAL'
      Size = 11
    end
    object fdqCarteiraCEDENTE_BANCO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Banco'
      FieldName = 'CEDENTE_BANCO'
      Origin = 'CEDENTE_BANCO'
      Size = 25
    end
    object fdqCarteiraCEDENTE_CARTEIRA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Carteira'
      FieldName = 'CEDENTE_CARTEIRA'
      Origin = 'CEDENTE_CARTEIRA'
      Size = 5
    end
    object fdqCarteiraCEDENTE_NOME_PERSONALIZADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Nome Personalizado'
      FieldName = 'CEDENTE_NOME_PERSONALIZADO'
      Origin = 'CEDENTE_NOME_PERSONALIZADO'
      Size = 255
    end
    object fdqCarteiraCEDENTE_AGENCIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Ag'#234'ncia'
      FieldName = 'CEDENTE_AGENCIA'
      Origin = 'CEDENTE_AGENCIA'
      Size = 10
    end
    object fdqCarteiraCEDENTE_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - Conta Corrente'
      FieldName = 'CEDENTE_CONTA_CORRENTE'
      Origin = 'CEDENTE_CONTA_CORRENTE'
      Size = 10
    end
    object fdqCarteiraBOLETO_NOSSO_NUMERO_INICIAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Nosso N'#250'mero Inicial'
      FieldName = 'BOLETO_NOSSO_NUMERO_INICIAL'
      Origin = 'BOLETO_NOSSO_NUMERO_INICIAL'
    end
    object fdqCarteiraBOLETO_NOSSO_NUMERO_FINAL: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Nosso N'#250'mero Final'
      FieldName = 'BOLETO_NOSSO_NUMERO_FINAL'
      Origin = 'BOLETO_NOSSO_NUMERO_FINAL'
    end
    object fdqCarteiraBOLETO_NOSSO_NUMERO_PROXIMO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Nosso N'#250'mero Pr'#243'ximo'
      FieldName = 'BOLETO_NOSSO_NUMERO_PROXIMO'
      Origin = 'BOLETO_NOSSO_NUMERO_PROXIMO'
    end
    object fdqCarteiraBOLETO_MODALIDADE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Modalidade'
      FieldName = 'BOLETO_MODALIDADE'
      Origin = 'BOLETO_MODALIDADE'
    end
    object fdqCarteiraBOLETO_CODIGO_TRANSMISSAO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - C'#243'digo de Transmiss'#227'o'
      FieldName = 'BOLETO_CODIGO_TRANSMISSAO'
      Origin = 'BOLETO_CODIGO_TRANSMISSAO'
    end
    object fdqCarteiraBOLETO_LOCAL_PAGAMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Local de Pagamento'
      FieldName = 'BOLETO_LOCAL_PAGAMENTO'
      Origin = 'BOLETO_LOCAL_PAGAMENTO'
      Size = 255
    end
    object fdqCarteiraBOLETO_INSTRUCOES: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Instru'#231#245'es'
      FieldName = 'BOLETO_INSTRUCOES'
      Origin = 'BOLETO_INSTRUCOES'
    end
    object fdqCarteiraBOLETO_DEMONSTRATIVO: TBlobField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Demonstrativo'
      FieldName = 'BOLETO_DEMONSTRATIVO'
      Origin = 'BOLETO_DEMONSTRATIVO'
    end
    object fdqCarteiraBOLETO_LAYOUT_IMPRESSAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Layout Impress'#227'o'
      FieldName = 'BOLETO_LAYOUT_IMPRESSAO'
      Origin = 'BOLETO_LAYOUT_IMPRESSAO'
      Size = 120
    end
    object fdqCarteiraBOLETO_LAYOUT_REMESSA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Layout Remessa'
      FieldName = 'BOLETO_LAYOUT_REMESSA'
      Origin = 'BOLETO_LAYOUT_REMESSA'
      Size = 50
    end
    object fdqCarteiraBOLETO_LAYOUT_RETORNO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Layout Retorno'
      FieldName = 'BOLETO_LAYOUT_RETORNO'
      Origin = 'BOLETO_LAYOUT_RETORNO'
      Size = 50
    end
    object fdqCarteiraBOLETO_PATH_REMESSA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Diret'#243'rio para Salvar as Remessas'
      FieldName = 'BOLETO_PATH_REMESSA'
      Origin = 'BOLETO_PATH_REMESSA'
      Size = 255
    end
    object fdqCarteiraBOLETO_PATH_RETORNO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Diret'#243'rio para Salvar os Retornos'
      FieldName = 'BOLETO_PATH_RETORNO'
      Origin = 'BOLETO_PATH_RETORNO'
      Size = 255
    end
    object fdqCarteiraBOLETO_ESPECIE_DOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Esp'#233'cie do Documento'
      FieldName = 'BOLETO_ESPECIE_DOCUMENTO'
      Origin = 'BOLETO_ESPECIE_DOCUMENTO'
      Size = 50
    end
    object fdqCarteiraBOLETO_SEQUENCIA_REMESSA: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Sequ'#234'ncia da Remessa'
      FieldName = 'BOLETO_SEQUENCIA_REMESSA'
      Origin = 'BOLETO_SEQUENCIA_REMESSA'
    end
    object fdqCarteiraBOLETO_AMBIENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Ambiente'
      FieldName = 'BOLETO_AMBIENTE'
      Origin = 'BOLETO_AMBIENTE'
      Size = 15
    end
    object fdqCarteiraBOLETO_DIAS_PROTESTO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Dias de Protesto'
      FieldName = 'BOLETO_DIAS_PROTESTO'
      Origin = 'BOLETO_DIAS_PROTESTO'
    end
    object fdqCarteiraBO_ACEITE_BOLETO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Aceite'
      FieldName = 'BO_ACEITE_BOLETO'
      Origin = 'BO_ACEITE_BOLETO'
      FixedChar = True
      Size = 1
    end
    object fdqCarteiraBOLETO_VL_ACRESCIMO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Acr'#233'scimo'
      FieldName = 'BOLETO_VL_ACRESCIMO'
      Origin = 'BOLETO_VL_ACRESCIMO'
      Precision = 15
      Size = 2
    end
    object fdqCarteiraBOLETO_VL_TARIFA_BANCARIA: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Tarifa Banc'#225'ria'
      FieldName = 'BOLETO_VL_TARIFA_BANCARIA'
      Origin = 'BOLETO_VL_TARIFA_BANCARIA'
      Precision = 15
      Size = 2
    end
    object fdqCarteiraBOLETO_PERC_MULTA: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Percentual da Multa'
      FieldName = 'BOLETO_PERC_MULTA'
      Origin = 'BOLETO_PERC_MULTA'
      Precision = 15
      Size = 2
    end
    object fdqCarteiraBOLETO_PERC_MORA_DIARIA: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Percentual da Mora Di'#225'ria'
      FieldName = 'BOLETO_PERC_MORA_DIARIA'
      Origin = 'BOLETO_PERC_MORA_DIARIA'
      Precision = 15
      Size = 2
    end
    object fdqCarteiraBOLETO_PERC_DESCONTO: TBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Percentual do Desconto'
      FieldName = 'BOLETO_PERC_DESCONTO'
      Origin = 'BOLETO_PERC_DESCONTO'
      Precision = 15
      Size = 2
    end
    object fdqCarteiraBOLETO_MOEDA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Moeda'
      FieldName = 'BOLETO_MOEDA'
      Origin = 'BOLETO_MOEDA'
      Size = 5
    end
    object fdqCarteiraBO_ENVIA_BOLETO_EMAIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Enviar Email'
      FieldName = 'BO_ENVIA_BOLETO_EMAIL'
      Origin = 'BO_ENVIA_BOLETO_EMAIL'
      FixedChar = True
      Size = 1
    end
    object fdqCarteiraBOLETO_ASSUNTO_EMAIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Assunto do Email'
      FieldName = 'BOLETO_ASSUNTO_EMAIL'
      Origin = 'BOLETO_ASSUNTO_EMAIL'
      Size = 255
    end
    object fdqCarteiraBOLETO_LAYOUT_BOLETO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Layout do Boleto'
      FieldName = 'BOLETO_LAYOUT_BOLETO'
      Origin = 'BOLETO_LAYOUT_BOLETO'
      Size = 50
    end
    object fdqCarteiraBOLETO_ID_RECIBO_EMAIL_PERSONALIZADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - C'#243'digo do Recibo Personalizado'
      FieldName = 'BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO'
      Origin = 'BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO'
    end
    object fdqCarteiraBOLETO_ID_RECIBO_BOLETO_PERSONALIZADO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - C'#243'digo do Recibo do Boleto Personalizado'
      FieldName = 'BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO'
      Origin = 'BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO'
    end
    object fdqCarteiraBOLETO_PREFIXO_NOSSONUMERO: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Prefixo do Nosso N'#250'mero'
      FieldName = 'BOLETO_PREFIXO_NOSSO NUMERO'
      Origin = '`BOLETO_PREFIXO_NOSSO NUMERO`'
    end
    object fdqCarteiraBOLETO_TIPO_DOCUMENTO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Tipo de Documento'
      FieldName = 'BOLETO_TIPO_DOCUMENTO'
      Origin = 'BOLETO_TIPO_DOCUMENTO'
      Size = 50
    end
    object fdqCarteiraBOLETO_LAYOUT_EMAIL: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Boleto - Layout E-mail'
      FieldName = 'BOLETO_LAYOUT_EMAIL'
      Origin = 'BOLETO_LAYOUT_EMAIL'
      Size = 50
    end
    object fdqCarteiraID_CONTA_CORRENTE: TIntegerField
      AutoGenerateValue = arDefault
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object fdqCarteiraJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object fdqCarteiraCEDENTE_CODIGO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cedente - C'#243'digo'
      FieldName = 'CEDENTE_CODIGO'
      Origin = 'CEDENTE_CODIGO'
      Size = 11
    end
  end
  object dspCarteira: TDataSetProvider
    DataSet = fdqCarteira
    Options = [poIncFieldProps, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 72
    Top = 24
  end
end
