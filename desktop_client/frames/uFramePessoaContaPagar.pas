unit uFramePessoaContaPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameConsultaDadosDetalheGrade, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxBar, dxRibbonRadialMenu, cxClasses, dxPSCore, dxPScxCommon,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, System.Actions, Vcl.ActnList, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, uGBPanel, cxGroupBox, cxRadioGroup,
  cxLabel, uGBLabel, Vcl.ExtCtrls;

type
  TFramePessoaContaPagar = class(TFrameConsultaDadosDetalheGrade)
    PnTotalizadorConsulta: TFlowPanel;
    pnTotalizadorVlAberto: TgbLabel;
    pnTotalizadorAcrecimoAberto: TgbLabel;
    pnTotalizadorTotalAberto: TgbLabel;
    pnTotalizadorVlVencido: TgbLabel;
    pnTotalizadorVlVencer: TgbLabel;
    pnTotalizadorVlRecebido: TgbLabel;
    pnTotalizadorAcrescimoRecebido: TgbLabel;
    pnTotalizadorTotalRecebido: TgbLabel;
    rgTipoContaPagar: TcxRadioGroup;
    procedure viewConsultaDadosDetalhadaGradeDataControllerDataChanged(Sender: TObject);
    procedure cxRadioGroup1PropertiesEditValueChanged(Sender: TObject);
  private
    FCalculandoTotalRodape: Boolean;
    FFiltroRegistroGradeAnterior: String;
    procedure CalcularTotaisDoRodape;
    procedure AlinharBarraTotalizadoraConsulta;
    procedure DefinirIdentificadorDaPesquisaPadrao;
    function ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
    function ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
    procedure ExibirFiltroPeriodoQuitacao;
  public
    procedure DepoisDeExecutarConsulta; override;
    procedure AntesDeExecutarConsulta; override;

    const FIDENTIFICADOR_SQL_ABERTO = 'FRAMEPESSOACONTAPAGARPESQUISAABERTO';
    const FIDENTIFICADOR_SQL_QUITADO = 'FRAMEPESSOACONTAPAGARPESQUISAQUITADO';
    const FIDENTIFICADOR_SQL_GERAL = 'FRAMEPESSOACONTAPAGAR';
  protected
    procedure AoCriarFrame; override;
    procedure DefinirFiltros; override;


  end;

var
  FramePessoaContaPagar: TFramePessoaContaPagar;

implementation

{$R *.dfm}

uses uDatasetUtils, uContaPagar, uFiltroFormulario, uUsuarioDesignControl, uFrameFiltroVerticalPadrao,
  uContaPagarProxy, uDmConnection, uSistema;

procedure TFramePessoaContaPagar.AlinharBarraTotalizadoraConsulta;
begin
  PnTotalizadorConsulta.AutoWrap := false;
  PnTotalizadorConsulta.AutoWrap := true;
end;

procedure TFramePessoaContaPagar.AntesDeExecutarConsulta;
begin
  inherited;
  if VartoStr(rgTipoContaPagar.EditValue).Equals(QUITADO) and ExistePesquisaConfiguradaParaSituacaoQuitado then
  begin
    FIdentificadorFrame := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(rgTipoContaPagar.EditValue).Equals(ABERTO) and ExistePesquisaConfiguradaParaSituacaoAberto then
  begin
    FIdentificadorFrame := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  FIdentificadorFrame := FIDENTIFICADOR_SQL_GERAL;
end;

procedure TFramePessoaContaPagar.DefinirIdentificadorDaPesquisaPadrao;
begin
  if VartoStr(rgTipoContaPagar.EditValue).Equals(QUITADO) then
  begin
    FIdentificadorFrame := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(rgTipoContaPagar.EditValue).Equals(ABERTO) then
  begin
    FIdentificadorFrame := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  FIdentificadorFrame := FIDENTIFICADOR_SQL_GERAL;
end;

procedure TFramePessoaContaPagar.ExibirFiltroPeriodoQuitacao;
begin
  if VartoStr(rgTipoContaPagar.EditValue).Equals(QUITADO) then
  begin
    FFiltroVertical.ExibirFiltro('DT_QUITACAO');
  end
  else
  begin
    FFiltroVertical.OcultarFiltro('DT_QUITACAO');
  end;
end;

function TFramePessoaContaPagar.ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_ABERTO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

function TFramePessoaContaPagar.ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_QUITADO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

procedure TFramePessoaContaPagar.AoCriarFrame;
begin
  inherited;
  OcultarTituloFrame;
  FCalculandoTotalRodape := false;
  FFiltroRegistroGradeAnterior := '';
  CalcularTotaisDoRodape;
  FIdentificadorFrame := FIDENTIFICADOR_SQL_GERAL;
  DefinirIdentificadorDaPesquisaPadrao;
  rgTipoContaPagar.EditValue := 'TODOS';
end;

procedure TFramePessoaContaPagar.CalcularTotaisDoRodape;
var
  fdmTotais: TgbFDMemTable;
  idsContaPagar: String;
begin
  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    if Assigned(viewConsultaDadosDetalhadaGrade.DataController.DataSource.Dataset) and
      (viewConsultaDadosDetalhadaGrade.DataController.DataSource.Dataset.Active) then
    begin
      idsContaPagar := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
        viewConsultaDadosDetalhadaGrade.DataController.DataSource.Dataset.FieldByName('ID'),
        viewConsultaDadosDetalhadaGrade);
    end;

    if idsContaPagar.IsEmpty then
    begin
      idsContaPagar := '-9999999';
    end;

    fdmTotais := TContaPagar.GetTotaisContaPagar(idsContaPagar);

    pnTotalizadorVlAberto.Caption := 'Aberto R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto'));
    pnTotalizadorAcrecimoAberto.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_aberto'));
    pnTotalizadorTotalAberto.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_liquido'));
    pnTotalizadorVlVencido.Caption := 'Vencido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencido'));
    pnTotalizadorVlVencer.Caption := 'A Vencer R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencer'));
    pnTotalizadorVlRecebido.Caption := 'Recebido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido'));
    pnTotalizadorAcrescimoRecebido.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_recebido'));
    pnTotalizadorTotalRecebido.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido_liquido'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;


procedure TFramePessoaContaPagar.cxRadioGroup1PropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  DefinirIdentificadorDaPesquisaPadrao;
  ExibirFiltroPeriodoQuitacao;
end;

procedure TFramePessoaContaPagar.DefinirFiltros;
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Documento';
  campoFiltro.campo := 'DT_DOCUMENTO';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Status
 { campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TContaPagarMovimento.ABERTO+';'+
  TContaPagarMovimento.QUITADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  FFiltroVertical.FCampos.Add(campoFiltro); }

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'CONTA_Pagar';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFiltroVertical.FCampos.Add(campoFiltro);

  //Per�odo de Quita��o (Dt de Recebimento)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Dt. Recebimento';
  campoFiltro.campo := 'DT_QUITACAO';
  campoFiltro.tabela := 'CONTA_PAGAR_QUITACAO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TFramePessoaContaPagar.DepoisDeExecutarConsulta;
begin
  inherited;
  CalcularTotaisDoRodape;
  DefinirIdentificadorDaPesquisaPadrao;
end;

procedure TFramePessoaContaPagar.viewConsultaDadosDetalhadaGradeDataControllerDataChanged(Sender: TObject);
var
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not viewConsultaDadosDetalhadaGrade.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodape;
  end;

  FFiltroRegistroGradeAnterior := viewConsultaDadosDetalhadaGrade.DataController.Filter.FilterText;
end;

end.

