unit uFrameConsultaDadosDetalheGrade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameConsultaDadosDetalhe, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, System.Actions, Vcl.ActnList,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, JvExControls, JvButton,
  JvTransparentButton, uGBPanel, cxGroupBox, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, dxPSCore, dxPScxCommon, cxSplitter, uFramePadrao,
  uFrameFiltroVerticalPadrao, dxBar, dxRibbonRadialMenu, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter;

type
  TFrameConsultaDadosDetalheGrade = class(TFrameConsultaDadosDetalhe)
    dxComponentPrinter: TdxComponentPrinter;
    dxComponentPrinterLink1: TdxGridReportLink;
    cxGridConsultaDadosDetalahadaGrade: TcxGrid;
    viewConsultaDadosDetalhadaGrade: TcxGridDBBandedTableView;
    cxGridConsultaDadosDetalahadaGradeLevel1: TcxGridLevel;
    spFiltroVertical: TcxSplitter;
    alFrameConsultaDadosDetalheGrade: TActionList;
    ActSalvarConfiguracoes: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActAlterarColunasGrid: TAction;
    ActExibirAgrupamento: TAction;
    ActAlterarSQLPesquisaPadrao: TAction;
    dxBarManagerPadrao: TdxBarManager;
    dxBarManager: TdxBar;
    barNavegador: TdxBar;
    RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    procedure viewConsultaDadosDetalhadaGradeNavigatorButtonsButtonClick(Sender: TObject; AButtonIndex: Integer;
      var ADone: Boolean);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure spFiltroVerticalCanResize(Sender: TObject; var NewSize: Integer; var Accept: Boolean);
  private
    procedure ExibirFiltroDeMemoriaDaGrid(const AExibir: Boolean);
  public
    procedure DepoisConsultar; override;
    procedure AntesConsultar; override;
    procedure Imprimir; override;
    procedure DepoisDeExecutarConsulta; virtual;
    procedure AntesDeExecutarConsulta; virtual;
    procedure ExecutarClickColunaSelecao;
    procedure HabilitarEdicaoColunaSelecao(const AHabilitar: Boolean = true);
    procedure LimparDatasetConsulta;
  protected
    procedure ExibirAgrupamento(const AGridView: TcxGridDBBandedTableView);
    procedure AlterarSQL(const AIdentificadorFrame: String);
    procedure SalvarConfiguracoes(const AGridView: TcxGridDBBandedTableView;
      const AIdentificador: String);
    procedure AlterarColunas(const AGridView: TcxGridDBBandedTableView);
    procedure RestaurarColunas(const AGridView: TcxGridDBBandedTableView;
      const AIdentificador: String);

    procedure ExecutarAcaoExibirAgrupamento; virtual;
    procedure ExecutarAcaoAlterarSQL; virtual;
    procedure ExecutarAcaoSalvarConfiguracoes; virtual;
    procedure ExecutarAcaoAlterarColunas; virtual;
    procedure ExecutarAcaoRestaurarColunas; virtual;

    procedure DepoisConsultarDetalheGrade; virtual;
  end;

var
  FrameConsultaDadosDetalheGrade: TFrameConsultaDadosDetalheGrade;

implementation

{$R *.dfm}

uses uDevExpressUtils, uTUsuario, uSistema, uFrmApelidarColunasGrid, uFrmAlterarSQLPesquisaPadrao,
  uFrmVisualizacaoRelatoriosAssociados, uConstantes;

procedure TFrameConsultaDadosDetalheGrade.ActAlterarColunasGridExecute(Sender: TObject);
begin
  ExecutarAcaoAlterarColunas;
end;

procedure TFrameConsultaDadosDetalheGrade.ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
begin
  ExecutarAcaoAlterarSQL;
end;

procedure TFrameConsultaDadosDetalheGrade.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  ExecutarAcaoExibirAgrupamento;
end;

procedure TFrameConsultaDadosDetalheGrade.ActRestaurarColunasPadraoExecute(Sender: TObject);
begin
  ExecutarAcaoRestaurarColunas;
end;

procedure TFrameConsultaDadosDetalheGrade.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  ExecutarAcaoSalvarConfiguracoes;
end;

procedure TFrameConsultaDadosDetalheGrade.AlterarColunas(
  const AGridView: TcxGridDBBandedTableView);
begin
  TFrmApelidarColunasGrid.SetColumns(AGridView);
end;

procedure TFrameConsultaDadosDetalheGrade.AlterarSQL(
  const AIdentificadorFrame: String);
begin
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, AIdentificadorFrame) then
    ActConsultar.Execute;
end;

procedure TFrameConsultaDadosDetalheGrade.AntesConsultar;
begin
  inherited;
  AntesDeExecutarConsulta;
end;

procedure TFrameConsultaDadosDetalheGrade.AntesDeExecutarConsulta;
begin
  //Implementar na heran�a
end;

procedure TFrameConsultaDadosDetalheGrade.DepoisConsultar;
begin
  inherited;
  TcxGridUtils.AdicionarTodosCamposNaView(viewConsultaDadosDetalhadaGrade);

  TUsuarioGridView.LoadGridView(viewConsultaDadosDetalhadaGrade,
    TSistema.Sistema.Usuario.idSeguranca, FIdentificadorFrame);

  if not FInjecaoCampos.IsEmpty then
  begin
    if Assigned(fdmDados.FindField(TDBConstantes.ALIAS_BO_CHECKED)) then
    begin
      TcxGridUtils.ConfigurarColunasParaSomenteLeitura(viewConsultaDadosDetalhadaGrade);

      viewConsultaDadosDetalhadaGrade.OptionsData.Editing := true;
      viewConsultaDadosDetalhadaGrade.OptionsSelection.CellSelect := true;

      TcxGridUtils.ConfigurarColunaSelecao(
        viewConsultaDadosDetalhadaGrade.GetColumnByFieldName(TDBConstantes.ALIAS_BO_CHECKED));
    end;
  end
  else
  begin
    viewConsultaDadosDetalhadaGrade.OptionsData.Editing := false;
    viewConsultaDadosDetalhadaGrade.OptionsSelection.CellSelect := false;
  end;

  ExibirFiltroDeMemoriaDaGrid(not fdmDados.IsEmpty);

  DepoisConsultarDetalheGrade;

  DepoisDeExecutarConsulta;
end;

procedure TFrameConsultaDadosDetalheGrade.DepoisConsultarDetalheGrade;
begin
  //implementar na heran�a
end;

procedure TFrameConsultaDadosDetalheGrade.DepoisDeExecutarConsulta;
begin
 //implementar na heran�a
end;

procedure TFrameConsultaDadosDetalheGrade.viewConsultaDadosDetalhadaGradeNavigatorButtonsButtonClick(Sender: TObject;
  AButtonIndex: Integer; var ADone: Boolean);
const
  BOTAO_IMPRESSORA: Integer = 16;
  BOTAO_EXCEL: Integer = 17;
  BOTAO_PDF: Integer = 18;
begin
  inherited;
  if AButtonIndex = BOTAO_IMPRESSORA then
  begin
    dxComponentPrinterLink1.PDFExportOptions.DefaultFileName := Self.Caption;
    dxComponentPrinterLink1.PDFExportOptions.OpenDocumentAfterExport := true;
    dxComponentPrinterLink1.Component := cxGridConsultaDadosDetalahadaGrade;
    dxComponentPrinterLink1.Preview();
  end
  else if AButtonIndex = BOTAO_EXCEL then
  begin
    TcxGridUtils.ExportarParaExcel(cxGridConsultaDadosDetalahadaGrade, Self.Caption)
  end
  else if AButtonIndex = BOTAO_PDF then
  begin
    dxComponentPrinterLink1.PDFExportOptions.DefaultFileName := Self.Caption;
    dxComponentPrinterLink1.PDFExportOptions.OpenDocumentAfterExport := true;
    dxComponentPrinterLink1.Component := cxGridConsultaDadosDetalahadaGrade;
    dxComponentPrinterLink1.ExportToPDF;
  end;
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarAcaoAlterarColunas;
begin
  AlterarColunas(viewConsultaDadosDetalhadaGrade);
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarAcaoAlterarSQL;
begin
  AlterarSQL(FIdentificadorFrame);
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarAcaoExibirAgrupamento;
begin
  ExibirAgrupamento(viewConsultaDadosDetalhadaGrade);
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarAcaoRestaurarColunas;
begin
  RestaurarColunas(viewConsultaDadosDetalhadaGrade, FIdentificadorFrame);
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarAcaoSalvarConfiguracoes;
begin
  SalvarConfiguracoes(viewConsultaDadosDetalhadaGrade, FIdentificadorFrame);
end;

procedure TFrameConsultaDadosDetalheGrade.ExecutarClickColunaSelecao;
var
  colunaSelecao: TcxGridDBBandedColumn;
begin
  colunaSelecao := viewConsultaDadosDetalhadaGrade.GetColumnByFieldName(TDBConstantes.ALIAS_BO_CHECKED);

  if Assigned(colunaSelecao) then
  begin
    colunaSelecao.OnHeaderClick(colunaSelecao);
  end;
end;

procedure TFrameConsultaDadosDetalheGrade.ExibirAgrupamento(
  const AGridView: TcxGridDBBandedTableView);
begin
  AGridView.OptionsView.GroupByBox := not AGridView.OptionsView.GroupByBox;
end;

procedure TFrameConsultaDadosDetalheGrade.ExibirFiltroDeMemoriaDaGrid(
  const AExibir: Boolean);
begin
  viewConsultaDadosDetalhadaGrade.FilterRow.Visible := AExibir;
end;

procedure TFrameConsultaDadosDetalheGrade.HabilitarEdicaoColunaSelecao(const AHabilitar: Boolean = true);
var
  colunaSelecao: TcxGridDBBandedColumn;
begin
  colunaSelecao := viewConsultaDadosDetalhadaGrade.GetColumnByFieldName(TDBConstantes.ALIAS_BO_CHECKED);
  if Assigned(colunaSelecao) then
  begin
    colunaSelecao.Options.Editing := AHabilitar;
  end;
end;

procedure TFrameConsultaDadosDetalheGrade.Imprimir;
begin
  inherited;
  if Assigned(fdmDados.FindField('ID')) and (not fdmDados.IsEmpty) then
  begin
    TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
     fdmDados.FieldByName('ID').AsInteger, FIdentificadorFrame, TSistema.Sistema.usuario.idSeguranca);
  end;
end;

procedure TFrameConsultaDadosDetalheGrade.LimparDatasetConsulta;
begin
  if (fdmDados.Active) and (not fdmDados.IsEmpty) then
  begin
    fdmDados.EmptyDataset;
  end;
end;

procedure TFrameConsultaDadosDetalheGrade.RestaurarColunas(
  const AGridView: TcxGridDBBandedTableView; const AIdentificador: String);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, AIdentificador);
  AGridView.RestoreDefaults;
end;

procedure TFrameConsultaDadosDetalheGrade.SalvarConfiguracoes(
  const AGridView: TcxGridDBBandedTableView; const AIdentificador: String);
begin
  TUsuarioGridView.SaveGridView(AGridView,
    TSistema.Sistema.usuario.idSeguranca, AIdentificador);
end;

procedure TFrameConsultaDadosDetalheGrade.spFiltroVerticalCanResize(Sender: TObject; var NewSize: Integer;
  var Accept: Boolean);
begin
  inherited;
  Accept := false;
end;

end.
