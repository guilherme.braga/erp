inherited FrameConsultaDadosDetalheGrade: TFrameConsultaDadosDetalheGrade
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloFrameDetailPadrao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      Left = 325
      ExplicitLeft = 325
      ExplicitWidth = 124
      Width = 124
      object cxGridConsultaDadosDetalahadaGrade: TcxGrid
        Left = 2
        Top = 2
        Width = 120
        Height = 278
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object viewConsultaDadosDetalhadaGrade: TcxGridDBBandedTableView
          PopupMenu = RadialPopUpMenuTransientePadrao
          Navigator.Buttons.OnButtonClick = viewConsultaDadosDetalhadaGradeNavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <
            item
              Hint = 'Realiza a impress'#227'o desta grade'
              ImageIndex = 12
            end
            item
              Hint = 'Exportar grade para arquivo excel'
              ImageIndex = 274
            end
            item
              Hint = 'Exportar grade para arquivo PDF'
              ImageIndex = 273
            end>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          DataController.DataSource = dsDados
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.Active = True
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.ApplyChanges = fracImmediately
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.ShowColumnFilterButtons = sfbAlways
          OptionsView.BandHeaders = False
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
            end>
        end
        object cxGridConsultaDadosDetalahadaGradeLevel1: TcxGridLevel
          GridView = viewConsultaDadosDetalhadaGrade
        end
      end
    end
    object spFiltroVertical: TcxSplitter
      Left = 317
      Top = 21
      Width = 8
      Height = 282
      HotZoneClassName = 'TcxMediaPlayer9Style'
      AllowHotZoneDrag = False
      MinSize = 0
      Control = pnFiltroVertical
      OnCanResize = spFiltroVerticalCanResize
    end
  end
  inherited alAcoes: TActionList
    Left = 136
    Top = 112
  end
  inherited fdmDados: TgbFDMemTable
    Left = 32
    Top = 80
  end
  inherited dsDados: TDataSource
    Left = 60
    Top = 80
  end
  object dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Version = 0
    Left = 207
    Top = 80
    object dxComponentPrinterLink1: TdxGridReportLink
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.GrayShading = True
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42227.948684189820000000
      ShrinkToPageWidth = True
      OptionsOnEveryPage.BandHeaders = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.BandHeaders = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object alFrameConsultaDadosDetalheGrade: TActionList
    Left = 80
    Top = 168
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
  end
  object dxBarManagerPadrao: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Navegacao')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    FlatCloseButton = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    PopupMenuLinks = <>
    ShowHelpButton = True
    ShowShortCutInHint = True
    Style = bmsOffice11
    UseF10ForMenu = False
    UseSystemFont = True
    Left = 402
    Top = 120
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Gest'#227'o '
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      UseRecentItems = False
      Visible = False
      WholeRow = False
    end
    object barNavegador: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Navega'#231#227'o'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 264
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 722
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = ActAlterarColunasGrid
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = ActAlterarSQLPesquisaPadrao
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = ActRestaurarColunasPadrao
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = ActSalvarConfiguracoes
      Category = 0
    end
  end
  object RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButton1'
      end
      item
        Visible = True
        ItemName = 'dxBarButton2'
      end
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end
      item
        Visible = True
        ItemName = 'dxBarButton4'
      end
      item
        Visible = True
        ItemName = 'dxBarButton5'
      end>
    Images = DmAcesso.cxImage16x16
    UseOwnFont = False
    Left = 312
    Top = 152
  end
end
