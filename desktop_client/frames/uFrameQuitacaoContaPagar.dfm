inherited FrameQuitacaoContaPagar: TFrameQuitacaoContaPagar
  inherited cxpcMain: TcxPageControl
    ExplicitWidth = 451
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 182
        DesignSize = (
          451
          182)
        Height = 182
        object bevelDadosdeOrigem: TdxBevel [0]
          Left = 2
          Top = 7
          Width = 447
          Height = 30
          Align = alTop
          LookAndFeel.Kind = lfUltraFlat
          Shape = dxbsLineBottom
          ExplicitWidth = 645
        end
        inherited dxBevel1: TdxBevel
          Height = 5
          ExplicitLeft = 2
          ExplicitTop = 2
          ExplicitWidth = 661
          ExplicitHeight = 5
        end
        object Label14: TLabel
          Left = 309
          Top = 42
          Width = 24
          Height = 13
          Caption = 'Valor'
        end
        object Label3: TLabel
          Left = 8
          Top = 42
          Width = 84
          Height = 13
          Caption = 'Data da Quita'#231#227'o'
        end
        object Label8: TLabel
          Left = 8
          Top = 165
          Width = 58
          Height = 13
          Caption = 'Observa'#231#227'o'
        end
        object Label11: TLabel
          Left = 8
          Top = 115
          Width = 81
          Height = 13
          Caption = 'Tipo da Quita'#231#227'o'
        end
        object Label1: TLabel
          Left = 560
          Top = 115
          Width = 75
          Height = 13
          Caption = 'Conta Corrente'
        end
        object Label2: TLabel
          Left = 560
          Top = 42
          Width = 50
          Height = 13
          Caption = 'Descontos'
        end
        object Label4: TLabel
          Left = 560
          Top = 66
          Width = 53
          Height = 13
          Caption = 'Acr'#233'scimos'
        end
        object Label18: TLabel
          Left = 752
          Top = 42
          Width = 11
          Height = 13
          Caption = '%'
        end
        object Label5: TLabel
          Left = 753
          Top = 65
          Width = 11
          Height = 13
          Caption = '%'
        end
        object Label6: TLabel
          Left = 309
          Top = 90
          Width = 43
          Height = 13
          Caption = 'Quita'#231#227'o'
        end
        object Label7: TLabel
          Left = 8
          Top = 13
          Width = 85
          Height = 13
          Caption = 'Data de Cadastro'
        end
        object Label9: TLabel
          Left = 560
          Top = 140
          Width = 66
          Height = 13
          Caption = 'Conta An'#225'lise'
        end
        object labelOrigem: TLabel
          Left = 309
          Top = 12
          Width = 34
          Height = 13
          Caption = 'Origem'
        end
        object labelSituacao: TLabel
          Left = 560
          Top = 12
          Width = 41
          Height = 13
          Caption = 'Situa'#231#227'o'
        end
        object Label10: TLabel
          Left = 8
          Top = 90
          Width = 24
          Height = 13
          Caption = 'Total'
        end
        object Label12: TLabel
          Left = 8
          Top = 140
          Width = 99
          Height = 13
          Caption = 'Centro de Resultado'
        end
        object Label13: TLabel
          Left = 309
          Top = 66
          Width = 26
          Height = 13
          Caption = 'Juros'
        end
        object Label15: TLabel
          Left = 8
          Top = 66
          Width = 26
          Height = 13
          Caption = 'Multa'
        end
        object edtDtQuitacao: TgbDBDateEdit
          Left = 111
          Top = 38
          DataBinding.DataField = 'DT_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.ReadOnly = False
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbRequired = True
          gbDateTime = False
          Width = 121
        end
        object edtVlQuitacao: TgbDBTextEdit
          Left = 355
          Top = 38
          TabStop = False
          DataBinding.DataField = 'VL_ABERTO'
          DataBinding.DataSource = dsContaPagar
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          OnEnter = edtVlQuitacaoEnter
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 120
        end
        object edtObservacao: TgbDBBlobEdit
          Left = 111
          Top = 161
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OBSERVACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ClearKey = 16430
          Properties.ImmediatePost = True
          Properties.PopupHeight = 300
          Properties.PopupWidth = 160
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 22
          Width = 323
        end
        object descTipoQuitacao: TgbDBTextEdit
          Left = 168
          Top = 111
          TabStop = False
          DataBinding.DataField = 'JOIN_DESCRICAO_TIPO_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 15
          gbReadyOnly = True
          gbPassword = False
          Width = 389
        end
        object edtIdTipoQuitacao: TgbDBButtonEditFK
          Left = 111
          Top = 111
          DataBinding.DataField = 'ID_TIPO_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 14
          gbTextEdit = descTipoQuitacao
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 
            'ID_TIPO_QUITACAO;JOIN_DESCRICAO_TIPO_QUITACAO;JOIN_TIPO_TIPO_QUI' +
            'TACAO'
          gbTableName = 'TIPO_QUITACAO'
          gbCamposConsulta = 'ID;DESCRICAO;TIPO'
          gbDepoisDeConsultar = edtIdTipoQuitacaogbDepoisDeConsultar
          gbIdentificadorConsulta = 'TIPO_QUITACAO'
          Width = 60
        end
        object descContaCorrente: TgbDBTextEdit
          Left = 696
          Top = 111
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 17
          gbReadyOnly = True
          gbPassword = False
          Width = 0
        end
        object edtIdContaCorrente: TgbDBButtonEditFK
          Left = 639
          Top = 111
          DataBinding.DataField = 'ID_CONTA_CORRENTE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 16
          gbTextEdit = descContaCorrente
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_CONTA_CORRENTE;JOIN_DESCRICAO_CONTA_CORRENTE'
          gbTableName = 'CONTA_CORRENTE'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'CONTA_CORRENTE'
          Width = 60
        end
        object edtVlDescontos: TgbDBTextEdit
          Left = 616
          Top = 38
          DataBinding.DataField = 'VL_DESCONTO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 6
          OnEnter = edtVlDescontosEnter
          gbPassword = False
          Width = 97
        end
        object edtVlAcrescimos: TgbDBTextEdit
          Left = 616
          Top = 62
          TabStop = False
          DataBinding.DataField = 'VL_ACRESCIMO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 10
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object edtPcDescontos: TgbDBTextEdit
          Left = 713
          Top = 38
          DataBinding.DataField = 'PERC_DESCONTO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 7
          gbPassword = False
          Width = 38
        end
        object edtPcAcrescimos: TgbDBTextEdit
          Left = 713
          Top = 62
          TabStop = False
          DataBinding.DataField = 'PERC_ACRESCIMO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 11
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 38
        end
        object edtVlTotalQuitacao: TgbDBTextEdit
          Left = 355
          Top = 86
          DataBinding.DataField = 'IC_VL_PAGAMENTO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = False
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 13
          OnExit = edtVlTotalQuitacaoExit
          gbRequired = True
          gbPassword = False
          Width = 120
        end
        object edtDtCadastro: TgbDBDateEdit
          Left = 99
          Top = 9
          TabStop = False
          DataBinding.DataField = 'DH_CADASTRO'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnNow]
          Properties.ImmediatePost = True
          Properties.Kind = ckDateTime
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbReadyOnly = True
          gbRequired = True
          gbDateTime = True
          Width = 130
        end
        object descContaAnalise: TgbDBTextEdit
          Left = 696
          Top = 136
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 21
          gbReadyOnly = True
          gbPassword = False
          Width = 0
        end
        object edtTpDocumentoOrigem: TgbDBTextEdit
          Left = 355
          Top = 8
          TabStop = False
          DataBinding.DataField = 'TP_DOCUMENTO_ORIGEM'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.Alignment.Horz = taCenter
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 122
        end
        object edtIdDocumentoOrigem: TgbDBTextEdit
          Left = 477
          Top = 8
          TabStop = False
          DataBinding.DataField = 'ID_DOCUMENTO_ORIGEM'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbReadyOnly = True
          gbPassword = False
          Width = 65
        end
        object edSituacao: TgbDBTextEdit
          Left = 616
          Top = 8
          TabStop = False
          DataBinding.DataField = 'STATUS'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.Alignment.Horz = taCenter
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbReadyOnly = True
          gbPassword = False
          Width = 135
        end
        object gbDBTextEdit1: TgbDBTextEdit
          Left = 111
          Top = 86
          TabStop = False
          DataBinding.DataField = 'VL_TOTAL'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 12
          OnEnter = edtVlQuitacaoEnter
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 119
        end
        object lbTrocoDiferenca: TcxLabel
          Left = 560
          Top = 88
          Caption = 'lbTrocoDiferenca'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          AnchorY = 97
        end
        object gbDBButtonEditFK1: TgbDBButtonEditFK
          Left = 111
          Top = 136
          DataBinding.DataField = 'ID_CENTRO_RESULTADO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 18
          gbTextEdit = gbDBTextEdit2
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
          gbTableName = 'CENTRO_RESULTADO'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'CENTRO_RESULTADO'
          Width = 60
        end
        object gbDBTextEdit2: TgbDBTextEdit
          Left = 168
          Top = 136
          TabStop = False
          DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 19
          gbReadyOnly = True
          gbPassword = False
          Width = 389
        end
        object EdtContaAnalise: TcxDBButtonEdit
          Left = 639
          Top = 136
          DataBinding.DataField = 'ID_CONTA_ANALISE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.ClickKey = 13
          Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
          Style.Color = 14606074
          TabOrder = 20
          OnExit = EdtContaAnalisePropertiesEditValueChanged
          Width = 60
        end
        object EdtJuros: TGBDBValorComPercentual
          Left = 355
          Top = 62
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 9
          Transparent = True
          gbFieldValorBase = 'VL_ABERTO'
          gbDataSourceValorBase = dsContaPagar
          gbFieldValor = 'VL_JUROS'
          gbDataSourceValor = dsDataFrame
          gbFieldPercentual = 'PERC_JUROS'
          gbDataSourcePercentual = dsDataFrame
          Height = 21
          Width = 191
        end
        object EdtMulta: TGBDBValorComPercentual
          Left = 111
          Top = 62
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 8
          Transparent = True
          gbFieldValorBase = 'VL_ABERTO'
          gbDataSourceValorBase = dsContaPagar
          gbFieldValor = 'VL_MULTA'
          gbDataSourceValor = dsDataFrame
          gbFieldPercentual = 'PERC_MULTA'
          gbDataSourcePercentual = dsDataFrame
          Height = 21
          Width = 193
        end
      end
      inherited PnQuitacaoCartao: TgbPanel
        Top = 182
        ExplicitTop = 182
        ExplicitHeight = 55
        Height = 55
      end
      inherited PnQuitacaoCheque: TgbPanel
        Top = 237
        ExplicitTop = 231
        ExplicitHeight = 112
        Height = 112
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Quita'#231#227'o'
    Style.IsFontAssigned = True
  end
  inherited ActionListMain: TActionList
    Left = 359
    Top = 56
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 330
    Top = 56
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = MovContaPagar.cdsContaPagarQuitacao
    Left = 302
    Top = 56
  end
  inherited ActionListAssistent: TActionList
    Left = 524
    Top = 56
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 496
    Top = 56
  end
  object dsContaPagar: TDataSource
    DataSet = MovContaPagar.cdsData
    Left = 392
    Top = 184
  end
end
