unit uFrameChecklistItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxCheckBox, cxDBEdit, uGBDBCheckBox, cxMaskEdit, cxSpinEdit, uGBDBSpinEdit,
  Vcl.StdCtrls, cxTextEdit, uGBDBTextEdit;

type
  TFrameChecklistItem = class(TFrameDetailPadrao)
    edDescricao: TgbDBTextEdit;
    Label2: TLabel;
    Label1: TLabel;
    gbDBSpinEdit1: TgbDBSpinEdit;
    gbDBCheckBox1: TgbDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure AntesDeConfirmar; override;
  end;

var
  FrameChecklistItem: TFrameChecklistItem;

implementation

{$R *.dfm}

uses uCadChecklist;

{ TFrameChecklistItem }

procedure TFrameChecklistItem.AntesDeConfirmar;
begin
  if gbDBSpinEdit1.CanFocus then
    gbDBSpinEdit1.SetFocus;
end;

end.
