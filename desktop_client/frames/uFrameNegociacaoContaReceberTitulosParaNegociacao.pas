unit uFrameNegociacaoContaReceberTitulosParaNegociacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameConsultaDadosDetalheGrade, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxBar, dxRibbonRadialMenu, cxClasses, dxPSCore, dxPScxCommon,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, System.Actions, Vcl.ActnList, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, uGBPanel, cxGroupBox, Vcl.StdCtrls,
  Vcl.DBCtrls;

type
  TFrameNegociacaoContaReceberTitulosParaNegociacao = class(TFrameConsultaDadosDetalheGrade)
    gbPanel1: TgbPanel;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameNegociacaoContaReceberTitulosParaNegociacao: TFrameNegociacaoContaReceberTitulosParaNegociacao;

implementation

{$R *.dfm}

end.
