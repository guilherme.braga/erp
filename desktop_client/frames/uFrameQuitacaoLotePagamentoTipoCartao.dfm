inherited FrameQuitacaoLotePagamentoTipoCartao: TFrameQuitacaoLotePagamentoTipoCartao
  inherited cxGBDadosMain: TcxGroupBox
    inherited edtDtVencimento: TgbDBDateEdit
      DataBinding.DataField = 'DT_QUITACAO'
      DataBinding.DataSource = dsFrame
    end
    inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
      DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      DataBinding.DataSource = dsFrame
    end
    inherited PnTituloCartao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited EdtValor: TgbDBCalcEdit
      DataBinding.DataField = 'VL_TOTAL'
      DataBinding.DataSource = dsFrame
    end
    inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
      DataBinding.DataField = 'ID_OPERADORA_CARTAO'
      DataBinding.DataSource = dsFrame
    end
  end
  object dsFrame: TDataSource
    DataSet = MovLotePagamento.cdsLotePagamentoQuitacao
    Left = 472
    Top = 4
  end
end
