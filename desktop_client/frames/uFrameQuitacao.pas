unit uFrameQuitacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, uGBPanel,
  dxBarExtItems, uFrameTipoQuitacaoCheque, uFrameTipoQuitacaoCartao,
  uFramePadrao, ugbCLientDataset;

type
  TFrameQuitacao = class(TFrameDetailPadrao)
    PnQuitacaoCartao: TgbPanel;
    PnQuitacaoCheque: TgbPanel;
  private
    procedure RenomearAcoes;
  public
    procedure ExibirFrameCartao(const AExibir: Boolean);
    procedure ExibirFrameCheque(const AExibir: Boolean);
    procedure SetConfiguracoesIniciais(ADataset: TgbClientDataset);override;
  protected
    procedure AoCriarFrame; override;
    //Utilize esse m�todo para instanciar os frames de cart�o e cheque
    procedure InstanciarFrames; virtual; abstract;
    procedure DepoisExibirFrameCheque; virtual;
  end;

var
  FrameQuitacao: TFrameQuitacao;

implementation

{$R *.dfm}

{ TFrameQuitacao }

{ TFrameQuitacao }

{ TFrameQuitacao }

procedure TFrameQuitacao.AoCriarFrame;
begin
  ExibirFrameCartao(false);
  ExibirFrameCheque(false);
  RenomearAcoes;
end;

procedure TFrameQuitacao.DepoisExibirFrameCheque;
begin
  //Implementar nas heran�as
end;

procedure TFrameQuitacao.ExibirFrameCartao(const AExibir: Boolean);
begin
  PnQuitacaoCartao.Visible := AExibir;
end;

procedure TFrameQuitacao.ExibirFrameCheque(const AExibir: Boolean);
begin
  PnQuitacaoCheque.Visible := AExibir;

  if Assigned(dsDataFrame.Dataset) then
  begin
    DepoisExibirFrameCheque;
  end;
end;

procedure TFrameQuitacao.RenomearAcoes;
begin
  ActInsert.Caption := 'Baixar';
  ActRemove.Caption := 'Estornar';
end;

procedure TFrameQuitacao.SetConfiguracoesIniciais(ADataset: TgbClientDataset);
begin
  inherited;
  if Assigned(dsDataFrame.Dataset) then
  begin
    InstanciarFrames;
  end;
end;

end.
