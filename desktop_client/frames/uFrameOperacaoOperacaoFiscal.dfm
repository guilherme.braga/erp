inherited FrameOperacaoOperacaoFiscal: TFrameOperacaoOperacaoFiscal
  inherited cxpcMain: TcxPageControl
    Top = 145
    Height = 160
    ExplicitTop = 145
    ExplicitHeight = 160
    ClientRectBottom = 160
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 136
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 136
        ExplicitHeight = 136
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 136
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 136
        Height = 136
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 941
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Opera'#231#227'o Fiscal'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 126
    Height = 126
    object Label1: TLabel [0]
      Left = 5
      Top = 5
      Width = 76
      Height = 13
      Caption = 'Opera'#231#227'o Fiscal'
    end
    object Label2: TLabel [1]
      Left = 5
      Top = 29
      Width = 68
      Height = 13
      Caption = 'CFOP Produto'
    end
    object Label3: TLabel [2]
      Left = 5
      Top = 53
      Width = 83
      Height = 13
      Caption = 'CFOP Produto ST'
    end
    object Label4: TLabel [3]
      Left = 5
      Top = 77
      Width = 83
      Height = 13
      Caption = 'CFOP Mercadoria'
    end
    object Label5: TLabel [4]
      Left = 5
      Top = 101
      Width = 98
      Height = 13
      Caption = 'CFOP Mercadoria ST'
    end
    inherited gbPanel3: TgbPanel
      Left = 375
      Top = 95
      Anchors = [akTop, akRight]
      TabOrder = 2
      ExplicitLeft = 375
      ExplicitTop = 95
      inherited JvTransparentButton1: TJvTransparentButton
        ExplicitTop = 3
        ExplicitHeight = 20
      end
      inherited JvTransparentButton2: TJvTransparentButton
        ExplicitLeft = 5
        ExplicitTop = 10
      end
    end
    object gbDBTextEdit1: TgbDBTextEdit
      Left = 120
      Top = 1
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'DESCRICAO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      gbRequired = True
      gbPassword = False
      Width = 328
    end
    object EdtNaturezaOperacao: TgbDBButtonEditFK
      Left = 120
      Top = 25
      DataBinding.DataField = 'CFOP_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 112
      Style.Color = 14606074
      TabOrder = 1
      gbRequired = True
      gbCampoPK = 'CODIGO'
      gbCamposRetorno = 'CFOP_PRODUTO;JOIN_DESCRICAO_CFOP_PRODUTO'
      gbTableName = 'CFOP'
      gbCamposConsulta = 'CODIGO;DESCRICAO'
      gbClasseDoCadastro = 'TCadCFOP'
      gbIdentificadorConsulta = 'CFOP'
      Width = 72
    end
    object gbDBButtonEditFK1: TgbDBButtonEditFK
      Left = 120
      Top = 49
      DataBinding.DataField = 'CFOP_PRODUTO_ST'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 112
      Style.Color = 14606074
      TabOrder = 3
      gbRequired = True
      gbCampoPK = 'CODIGO'
      gbCamposRetorno = 'CFOP_PRODUTO_ST;JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      gbTableName = 'CFOP'
      gbCamposConsulta = 'CODIGO;DESCRICAO'
      gbClasseDoCadastro = 'TCadCFOP'
      gbIdentificadorConsulta = 'CFOP'
      Width = 72
    end
    object gbDBButtonEditFK2: TgbDBButtonEditFK
      Left = 120
      Top = 73
      DataBinding.DataField = 'CFOP_MERCADORIA'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 112
      Style.Color = 14606074
      TabOrder = 4
      gbRequired = True
      gbCampoPK = 'CODIGO'
      gbCamposRetorno = 'CFOP_MERCADORIA;JOIN_DESCRICAO_CFOP_MERCADORIA'
      gbTableName = 'CFOP'
      gbCamposConsulta = 'CODIGO;DESCRICAO'
      gbClasseDoCadastro = 'TCadCFOP'
      gbIdentificadorConsulta = 'CFOP'
      Width = 72
    end
    object gbDBButtonEditFK3: TgbDBButtonEditFK
      Left = 120
      Top = 97
      DataBinding.DataField = 'CFOP_MERCADORIA_ST'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 112
      Style.Color = 14606074
      TabOrder = 5
      gbRequired = True
      gbCampoPK = 'CODIGO'
      gbCamposRetorno = 'CFOP_MERCADORIA_ST;JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      gbTableName = 'CFOP'
      gbCamposConsulta = 'CODIGO;DESCRICAO'
      gbClasseDoCadastro = 'TCadCFOP'
      gbIdentificadorConsulta = 'CFOP'
      Width = 72
    end
    object gbDBTextEdit2: TgbDBTextEdit
      Left = 189
      Top = 25
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_DESCRICAO_CFOP_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 259
    end
    object gbDBTextEdit3: TgbDBTextEdit
      Left = 189
      Top = 49
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 259
    end
    object gbDBTextEdit4: TgbDBTextEdit
      Left = 189
      Top = 73
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_DESCRICAO_CFOP_MERCADORIA'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 259
    end
    object gbDBTextEdit5: TgbDBTextEdit
      Left = 189
      Top = 97
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 9
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 185
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadOperacao.cdsOperacaoFiscal
  end
end
