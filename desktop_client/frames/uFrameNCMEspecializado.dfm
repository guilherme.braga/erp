inherited FrameNCMEspecializado: TFrameNCMEspecializado
  inherited cxpcMain: TcxPageControl
    Top = 50
    Height = 255
    ExplicitTop = 50
    ExplicitWidth = 451
    ExplicitHeight = 255
    ClientRectBottom = 255
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 231
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 451
        Height = 231
        ExplicitWidth = 451
        ExplicitHeight = 231
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 231
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 231
        Height = 231
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 587
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'NCM Especializado'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 31
    Height = 31
    object Label1: TLabel [0]
      Left = 7
      Top = 8
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
    end
    inherited gbPanel3: TgbPanel
      Left = 370
      Top = 2
      Anchors = [akTop, akRight]
      ExplicitLeft = 370
      ExplicitTop = 2
      ExplicitWidth = 76
      Width = 76
    end
    object edDescricao: TgbDBTextEdit
      Left = 58
      Top = 4
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'DESCRICAO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      OnExit = edDescricaoExit
      gbRequired = True
      gbPassword = False
      Width = 310
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadNCM.cdsNCMEspecializado
  end
end
