unit uFrameRegraImpostoFiltro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, dxBar,
  dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset, Vcl.Menus,
  dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls,
  JvButton, JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxRadioGroup,
  cxDBEdit, uGBDBRadioGroup, cxCheckBox, uGBDBCheckBox, cxMaskEdit,
  cxButtonEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls,
  cxDropDownEdit, cxCalendar, uGBDBDateEdit, cxLabel, uFrameDetailTransientePadrao, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameRegraImpostoFiltro = class(TFrameDetailPadrao)
    gbDadosTransiente: TgbPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBRadioGroup2: TgbDBRadioGroup;
    gbDBRadioGroup3: TgbDBRadioGroup;
    gbDBRadioGroup4: TgbDBRadioGroup;
    gbDBRadioGroup6: TgbDBRadioGroup;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBButtonEditFK4: TgbDBButtonEditFK;
    gbDBTextEdit5: TgbDBTextEdit;
    gbDBButtonEditFK5: TgbDBButtonEditFK;
    gbDBButtonEditFK6: TgbDBButtonEditFK;
    gbDBTextEdit6: TgbDBTextEdit;
    Label7: TLabel;
    Label8: TLabel;
    gbDBButtonEditFK7: TgbDBButtonEditFK;
    gbDBButtonEditFK8: TgbDBButtonEditFK;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBDateEdit2: TgbDBDateEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    Label9: TLabel;
    gbDBButtonEditFK9: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    Label10: TLabel;
    gbDBTextEdit8: TgbDBTextEdit;
    Label11: TLabel;
    gbDBTextEdit9: TgbDBTextEdit;
    EdtEmitenteContribuinteICMS: TgbDBRadioGroup;
    gbDBRadioGroup5: TgbDBRadioGroup;
    EdtDestinatarioContribuinteICMS: TgbDBRadioGroup;
    EdtEmitenteContribuinteIPI: TgbDBRadioGroup;
    EdtDestinatarioContribuinteIPI: TgbDBRadioGroup;
    gbDBButtonEditFK10: TgbDBButtonEditFK;
    gbDBButtonEditFK11: TgbDBButtonEditFK;
  private
    { Private declarations }
  public
    procedure AntesDeConfirmar;
  end;

var
  FrameRegraImpostoFiltro: TFrameRegraImpostoFiltro;

implementation

{$R *.dfm}

uses uCadRegraImposto;

{ TFrameRegraImpostoFiltro }

procedure TFrameRegraImpostoFiltro.AntesDeConfirmar;
begin
  {if EdtEmitenteContribuinteICMS.ItemIndex = 0 then
  begin
    dsDataFrame.Dataset.FieldByName('BO_CONTRIBUINTE_ICMS_EMITENTE').Clear;
  end;

  if EdtDestinatarioContribuinteICMS.ItemIndex = 0 then
  begin
    dsDataFrame.Dataset.FieldByName('BO_CONTRIBUINTE_ICMS_DESTINATARIO').Clear;
  end;

  if EdtEmitenteContribuinteIPI.ItemIndex = 0 then
  begin
    dsDataFrame.Dataset.FieldByName('BO_CONTRIBUINTE_IPI_EMITENTE').Clear;
  end;

  if EdtDestinatarioContribuinteIPI.ItemIndex = 0 then
  begin
    dsDataFrame.Dataset.FieldByName('BO_CONTRIBUINTE_IPI_DESTINATARIO').Clear;
  end;   }
end;

end.
