inherited FrameEmpresaFilial: TFrameEmpresaFilial
  Width = 1068
  Height = 418
  inherited cxpcMain: TcxPageControl
    Top = 278
    Width = 1068
    Height = 140
    TabOrder = 1
    Properties.ActivePage = cxtsData
    ExplicitTop = 237
    ExplicitHeight = 68
    ClientRectBottom = 140
    ClientRectRight = 1068
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 44
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1068
        Height = 116
        ExplicitHeight = 44
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 44
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 44
        Height = 116
        Width = 1068
        inherited dxBevel1: TdxBevel
          Width = 1064
          ExplicitWidth = 863
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Filiais'
    Style.IsFontAssigned = True
    TabOrder = 0
    Transparent = True
    Width = 1068
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitWidth = 1068
    ExplicitHeight = 259
    Height = 259
    Width = 1068
    object PnLogradouro: TcxGroupBox [0]
      Left = 2
      Top = 49
      Align = alTop
      PanelStyle.Active = True
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      ExplicitWidth = 447
      DesignSize = (
        1064
        98)
      Height = 98
      Width = 1064
      object lbComplemento: TLabel
        Left = 8
        Top = 31
        Width = 65
        Height = 13
        Caption = 'Complemento'
      end
      object lbLogradouro: TLabel
        Left = 168
        Top = 6
        Width = 55
        Height = 13
        Caption = 'Logradouro'
      end
      object lnNumero: TLabel
        Left = 546
        Top = 6
        Width = 37
        Height = 13
        Caption = 'N'#250'mero'
      end
      object lbCEP: TLabel
        Left = 8
        Top = 6
        Width = 19
        Height = 13
        Caption = 'CEP'
      end
      object Label1: TLabel
        Left = 642
        Top = 6
        Width = 28
        Height = 13
        Caption = 'Bairro'
      end
      object lbCidadeEmpresa: TLabel
        Left = 429
        Top = 31
        Width = 33
        Height = 13
        Caption = 'Cidade'
      end
      object Label4: TLabel
        Left = 854
        Top = 31
        Width = 42
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Telefone'
      end
      object Label6: TLabel
        Left = 8
        Top = 55
        Width = 24
        Height = 13
        Caption = 'Email'
      end
      object Label7: TLabel
        Left = 429
        Top = 55
        Width = 18
        Height = 13
        Caption = 'Site'
      end
      object Label8: TLabel
        Left = 8
        Top = 79
        Width = 27
        Height = 13
        Caption = 'CNAE'
      end
      object edtLogradouro: TgbDBTextEdit
        Left = 227
        Top = 2
        DataBinding.DataField = 'LOGRADOURO'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbRequired = True
        gbPassword = False
        Width = 314
      end
      object edtNumero: TgbDBTextEdit
        Left = 587
        Top = 2
        DataBinding.DataField = 'NUMERO'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        gbPassword = False
        Width = 49
      end
      object edtIDCEP: TgbDBButtonEditFK
        Left = 74
        Top = 2
        DataBinding.DataField = 'CEP'
        DataBinding.DataSource = dsDataFrame
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 112
        Style.Color = 14606074
        TabOrder = 0
        gbRequired = True
        gbCampoPK = 'CEP'
        gbCamposRetorno = 'CEP'
        gbTableName = 'CEP'
        gbCamposConsulta = 'CEP'
        gbIdentificadorConsulta = 'CEP'
        Width = 90
      end
      object descCidade: TgbDBTextEdit
        Left = 519
        Top = 27
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = False
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 6
        gbRequired = True
        gbPassword = False
        Width = 329
      end
      object descBairro: TgbDBButtonEditFK
        Left = 676
        Top = 2
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'BAIRRO'
        DataBinding.DataSource = dsDataFrame
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 112
        Properties.ReadOnly = False
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        gbRequired = True
        gbCampoPK = 'DESCRICAO'
        gbCamposRetorno = 'BAIRRO'
        gbTableName = 'BAIRRO'
        gbCamposConsulta = 'DESCRICAO'
        gbIdentificadorConsulta = 'BAIRRO'
        Width = 385
      end
      object edtCidade: TgbDBButtonEditFK
        Left = 466
        Top = 27
        DataBinding.DataField = 'ID_CIDADE'
        DataBinding.DataSource = dsDataFrame
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = 14606074
        TabOrder = 5
        gbRequired = True
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_CIDADE;JOIN_DESCRICAO_CIDADE'
        gbTableName = 'CIDADE'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbIdentificadorConsulta = 'CIDADE'
        Width = 56
      end
      object edtComplemento: TgbDBTextEdit
        Left = 74
        Top = 27
        DataBinding.DataField = 'COMPLEMENTO'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 4
        gbPassword = False
        Width = 349
      end
      object gbDBTextEdit2: TgbDBTextEdit
        Left = 902
        Top = 27
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'TELEFONE'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 7
        gbPassword = False
        ExplicitLeft = 285
        Width = 159
      end
      object gbDBTextEdit4: TgbDBTextEdit
        Left = 74
        Top = 51
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 8
        gbPassword = False
        Width = 349
      end
      object gbDBTextEdit5: TgbDBTextEdit
        Left = 466
        Top = 51
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'SITE'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 9
        gbPassword = False
        Width = 595
      end
      object gbDBTextEdit6: TgbDBTextEdit
        Left = 168
        Top = 75
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_CNAE'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 11
        gbReadyOnly = True
        gbPassword = False
        Width = 893
      end
      object gbDBButtonEditFK2: TgbDBButtonEditFK
        Left = 74
        Top = 75
        DataBinding.DataField = 'JOIN_SEQUENCIA_CNAE'
        DataBinding.DataSource = dsDataFrame
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 112
        Style.Color = clWhite
        TabOrder = 10
        gbCampoPK = 'SEQUENCIA'
        gbCamposRetorno = 'ID_CNAE;JOIN_SEQUENCIA_CNAE;JOIN_DESCRICAO_CNAE'
        gbTableName = 'CNAE'
        gbCamposConsulta = 'ID;SEQUENCIA;DESCRICAO'
        gbIdentificadorConsulta = 'CNAE'
        Width = 97
      end
    end
    object PnDadosEmpresa: TcxGroupBox [1]
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      ExplicitWidth = 447
      DesignSize = (
        1064
        47)
      Height = 47
      Width = 1064
      object lbRazaoSocial: TLabel
        Left = 8
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Raz'#227'o Social'
      end
      object lbDtFundacao: TLabel
        Left = 907
        Top = 4
        Width = 65
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Dt. Funda'#231#227'o'
        ExplicitLeft = 694
      end
      object lbNomeFantasia: TLabel
        Left = 8
        Top = 28
        Width = 41
        Height = 13
        Caption = 'Fantasia'
      end
      object lbCNPJ: TLabel
        Left = 676
        Top = 27
        Width = 25
        Height = 14
        Anchors = [akTop, akRight]
        Caption = 'CNPJ'
      end
      object lbInscricaoEstadual: TLabel
        Left = 962
        Top = 28
        Width = 12
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'IM'
      end
      object Label5: TLabel
        Left = 837
        Top = 28
        Width = 10
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'IE'
      end
      object edRazaoSocial: TgbDBTextEdit
        Left = 74
        Top = 0
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'RAZAO_SOCIAL'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        gbRequired = True
        gbPassword = False
        ExplicitWidth = 62
        Width = 679
      end
      object edtDtFundacao: TgbDBDateEdit
        Left = 978
        Top = 0
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'DT_FUNDACAO'
        DataBinding.DataSource = dsDataFrame
        Properties.DateButtons = [btnClear, btnToday]
        Properties.ImmediatePost = True
        Properties.SaveTime = False
        Properties.ShowTime = False
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        gbDateTime = False
        ExplicitLeft = 361
        Width = 83
      end
      object edtNomeFantasia: TgbDBTextEdit
        Left = 74
        Top = 24
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'FANTASIA'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        gbRequired = True
        gbPassword = False
        Width = 596
      end
      object edtCNPJ: TgbDBTextEdit
        Left = 707
        Top = 24
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'CNPJ'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 4
        gbRequired = True
        gbPassword = False
        Width = 124
      end
      object edtInscricaoEstadual: TgbDBTextEdit
        Left = 978
        Top = 24
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'IM'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 6
        gbPassword = False
        Width = 83
      end
      object gbDBCheckBox3: TgbDBCheckBox
        Left = 754
        Top = 0
        Anchors = [akTop, akRight]
        Caption = 'Esta filial '#233' a matriz?'
        DataBinding.DataField = 'BO_MATRIZ'
        DataBinding.DataSource = dsDataFrame
        Properties.ImmediatePost = True
        Properties.ValueChecked = 'S'
        Properties.ValueUnchecked = 'N'
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        ExplicitLeft = 137
        Width = 124
      end
      object gbDBTextEdit3: TgbDBTextEdit
        Left = 854
        Top = 24
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'IE'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 5
        gbPassword = False
        Width = 100
      end
    end
    object gbPanel1: TgbPanel [2]
      Left = 2
      Top = 147
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Transparent = True
      ExplicitLeft = 1
      ExplicitHeight = 69
      DesignSize = (
        1064
        110)
      Height = 110
      Width = 1064
      object Label2: TLabel
        Left = 8
        Top = 5
        Width = 60
        Height = 13
        Caption = 'Zoneamento'
      end
      object Label3: TLabel
        Left = 854
        Top = 5
        Width = 84
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Regime Tribut'#225'rio'
        ExplicitLeft = 642
      end
      object gbDBButtonEditFK1: TgbDBButtonEditFK
        Left = 74
        Top = 1
        DataBinding.DataField = 'ID_CIDADE'
        DataBinding.DataSource = dsDataFrame
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 0
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_ZONEAMENTO;JOIN_DESCRICAO_ZONEAMENTO'
        gbTableName = 'ZONEAMENTO'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbIdentificadorConsulta = 'ZONEAMENTO'
        Width = 62
      end
      object gbDBTextEdit1: TgbDBTextEdit
        Left = 133
        Top = 1
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_ZONEAMENTO'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = False
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbPassword = False
        ExplicitWidth = 98
        Width = 715
      end
      object gbDBComboBox1: TgbDBComboBox
        Left = 944
        Top = 1
        Anchors = [akTop, akRight]
        DataBinding.DataField = 'REGIME_TRIBUTARIO'
        DataBinding.DataSource = dsDataFrame
        Properties.CharCase = ecUpperCase
        Properties.Items.Strings = (
          'PRESUMIDO'
          'REAL'
          'SIMPLES')
        Style.BorderStyle = ebsOffice11
        Style.Color = 14606074
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        gbRequired = True
        ExplicitLeft = 327
        Width = 117
      end
      object gbDBRadioGroup1: TgbDBRadioGroup
        Left = 6
        Top = 26
        Caption = 'Tipo da Empresa do Destinat'#225'rio'
        DataBinding.DataField = 'TIPO_EMPRESA'
        DataBinding.DataSource = dsDataFrame
        Properties.Items = <
          item
            Caption = 'Empresa Privada'
            Value = 'PRIVADA'
          end
          item
            Caption = 'Empresa P'#250'blica'
            Value = 'PUBLICA'
          end
          item
            Caption = 'Empresa Mista'
            Value = 'MISTA'
          end>
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        Height = 82
        Width = 193
      end
      object gbDBRadioGroup2: TgbDBRadioGroup
        Left = 205
        Top = 28
        Caption = 'C'#243'digo do Regime Tribut'#225'rio (CRT)'
        DataBinding.DataField = 'CRT'
        DataBinding.DataSource = dsDataFrame
        Properties.Items = <
          item
            Caption = 'Simples Nacional'
            Value = '1'
          end
          item
            Caption = 'Simples Nacional - Excesso de sublimite de receita bruta'
            Value = '2'
          end
          item
            Caption = 'Regime Normal'
            Value = '3'
          end>
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 4
        Height = 80
        Width = 299
      end
      object gbDBCheckBox1: TgbDBCheckBox
        Left = 510
        Top = 30
        Caption = 'Contribuinte ICMS'
        DataBinding.DataField = 'BO_CONTRIBUINTE_ICMS'
        DataBinding.DataSource = dsDataFrame
        Properties.ImmediatePost = True
        Properties.ValueChecked = 'S'
        Properties.ValueUnchecked = 'N'
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 5
        Transparent = True
        Width = 115
      end
      object gbDBCheckBox2: TgbDBCheckBox
        Left = 510
        Top = 57
        Caption = 'Contribuinte IPI'
        DataBinding.DataField = 'BO_CONTRIBUINTE_IPI'
        DataBinding.DataSource = dsDataFrame
        Properties.ImmediatePost = True
        Properties.ValueChecked = 'S'
        Properties.ValueUnchecked = 'N'
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 6
        Transparent = True
        Width = 102
      end
    end
    inherited gbPanel3: TgbPanel
      Left = 512
      Top = 227
      Anchors = [akTop, akRight]
      TabOrder = 3
      ExplicitLeft = 512
      ExplicitTop = 227
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadEmpresaFilial.cdsFilial
  end
end
