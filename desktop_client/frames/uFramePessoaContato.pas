unit uFramePessoaContato;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, dxBar, cxClasses, System.Actions, Vcl.ActnList,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxMaskEdit, cxSpinEdit, cxDBEdit, cxTextEdit, Vcl.StdCtrls, cxDropDownEdit,
  uGBDBComboBox, uGBDBTextEditPK, cxBlobEdit, uGBDBBlobEdit, uGBDBTextEdit,
  Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu, dxBarExtItems, uGBPanel, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, cxCheckBox, uGBDBCheckBox;

type
  TFramePessoaContato = class(TFrameDetailPadrao)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    EdtContato: TgbDBTextEdit;
    gbDBBlobEdit1: TgbDBBlobEdit;
    gbDBTextEditPK1: TgbDBTextEditPK;
    gbDBComboBox1: TgbDBComboBox;
    checkboxEmailEmissaoDocumentoFiscal: TgbDBCheckBox;
    procedure gbDBComboBox1PropertiesEditValueChanged(Sender: TObject);
  private
    procedure ConfigurarMascaraTelefone;
    procedure ExibirCheckBoxEmailParaDocumentoFiscal;
  public

  protected
    procedure AoCriarFrame; override;
    procedure GerenciarControles; override;
  end;

var
  FramePessoaContato: TFramePessoaContato;

implementation

{$R *.dfm}

uses uCadPessoa, uTControl_Function, uConstantes, uPessoaProxy;

{ TFramePessoaContato }

{ TFramePessoaContato }

procedure TFramePessoaContato.ConfigurarMascaraTelefone;
begin
  if Assigned(dsDataFrame.Dataset) and (dsDataFrame.Dataset.Active)
    and (cxpcMain.ActivePage = cxtsData) then
  if dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
    TPessoaProxy.TIPO_CONTATO_TELEFONE) then
  begin
    dsDataFrame.Dataset.FieldByName('CONTATO').EditMask :=
      TConstantes.MASCARA_TELEFONE;//'\(00\)0000-0000;0;_';
  end
  else
  begin
    dsDataFrame.Dataset.FieldByName('CONTATO').EditMask := EmptyStr;
  end;
end;

procedure TFramePessoaContato.ExibirCheckBoxEmailParaDocumentoFiscal;
begin
  checkboxEmailEmissaoDocumentoFiscal.Visible := dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
    TPessoaProxy.TIPO_CONTATO_EMAIL)
end;

procedure TFramePessoaContato.gbDBComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  ConfigurarMascaraTelefone;
end;

procedure TFramePessoaContato.GerenciarControles;
begin
  inherited;
  ConfigurarMascaraTelefone;
end;

procedure TFramePessoaContato.AoCriarFrame;
begin
  inherited;
  EdtContato.DesabilitarCaseMode;
end;

end.
