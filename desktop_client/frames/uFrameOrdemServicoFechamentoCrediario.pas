unit uFrameOrdemServicoFechamentoCrediario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameFechamentoCrediario, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxButtonEdit, cxCalc, cxMemo, cxDBEdit, uGBDBTextEdit, cxTextEdit,
  cxMaskEdit, uGBDBButtonEditFK, cxLabel, uGBPanel, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid,
  uFrameTipoQuitacaoCrediario, uFrameTipoQuitacaoCheque, uFramePadrao,
  uFrameTipoQuitacaoCartao, JvExControls, JvButton, JvTransparentButton,
  cxGroupBox, dxBar, dxRibbonRadialMenu, System.Actions, Vcl.ActnList,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameOrdemServicoFechamentoCrediario = class(TFrameFechamentoCrediario)
    procedure EdtValorPagamentoDinheiroExit(Sender: TObject);
  private
    { Private declarations }
  public

  protected
    procedure GerarParcelamentoPrazo;override;
  end;

var
  FrameOrdemServicoFechamentoCrediario: TFrameOrdemServicoFechamentoCrediario;

implementation

{$R *.dfm}

uses uMovOrdemServico, uTControl_Function;

{ TFrameOrdemServicoFechamentoCrediario }

{ TFrameOrdemServicoFechamentoCrediario }

procedure TFrameOrdemServicoFechamentoCrediario.EdtValorPagamentoDinheiroExit(Sender: TObject);
begin
  inherited;
  GerarParcelamento;
end;

procedure TFrameOrdemServicoFechamentoCrediario.GerarParcelamentoPrazo;
begin
  inherited;
  TMovOrdemServico(TControlFunction.GetInstance('TMovOrdemServico')).GerarParcelamento;
end;

end.
