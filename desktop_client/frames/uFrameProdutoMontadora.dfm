inherited FrameProdutoMontadora: TFrameProdutoMontadora
  Height = 484
  ExplicitHeight = 484
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitHeight = 484
    Height = 484
    object gridModelos: TcxGrid
      Left = 616
      Top = 2
      Width = 256
      Height = 480
      Align = alRight
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object viewModelos: TcxGridDBBandedTableView
        OnKeyDown = viewModelosKeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        DataController.DataSource = dsModeloMontadora
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.Active = True
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
            Caption = 'Modelos'
          end>
        object viewModelosMODELO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MODELO'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsEditList
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'DESCRICAO'
          Properties.ListColumns = <
            item
              Caption = 'Modelo'
              FieldName = 'DESCRICAO'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dsListaModelo
          Properties.PostPopupValueOnTab = True
          Properties.OnEditValueChanged = viewModelosMODELOPropertiesEditValueChanged
          Width = 865
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = viewModelos
      end
    end
    object gridMontadoras: TcxGrid
      Left = 2
      Top = 2
      Width = 614
      Height = 480
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 1
      OnEnter = gridMontadorasEnter
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object viewMontadoras: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        OnEditKeyDown = viewMontadorasEditKeyDown
        DataController.DataSource = dsMontadora
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.Active = True
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        NewItemRow.Visible = True
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.DeletingConfirmation = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
            Caption = 'Montadoras'
          end>
        object viewMontadorasMONTADORA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MONTADORA'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsEditList
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'DESCRICAO'
          Properties.ListColumns = <
            item
              Caption = 'Montadoras'
              FieldName = 'DESCRICAO'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dsListaMontadora
          Properties.PostPopupValueOnTab = True
          Options.AutoWidthSizable = False
          Options.Moving = False
          Width = 235
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object viewMontadorasREFERENCIA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'REFERENCIA'
          Options.AutoWidthSizable = False
          Options.Moving = False
          Width = 123
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object viewMontadorasESPECIFICACAO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ESPECIFICACAO'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ImmediatePost = True
          Width = 254
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object cxGridLevel2: TcxGridLevel
        GridView = viewMontadoras
      end
    end
  end
  object dsMontadora: TDataSource
    DataSet = CadProduto.cdsProdutoMontadora
    Left = 600
    Top = 376
  end
  object dsModeloMontadora: TDataSource
    DataSet = CadProduto.cdsProdutoModeloMontadora
    Left = 528
    Top = 360
  end
  object dsListaModelo: TDataSource
    DataSet = cdsListaModelo
    Left = 480
    Top = 232
  end
  object dsListaMontadora: TDataSource
    DataSet = cdsListaMontadora
    Left = 544
    Top = 232
  end
  object cdsListaMontadora: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspListaMontadora'
    RemoteServer = DmConnection.dspMontadora
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 517
    Top = 232
    object cdsListaMontadoraID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsListaMontadoraDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsListaMontadoraBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsListaMontadoraOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  object cdsListaModelo: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspListaModeloMontadora'
    RemoteServer = DmConnection.dspModeloMontadora
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 453
    Top = 232
    object cdsListaModeloID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsListaModeloDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsListaModeloBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsListaModeloOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsListaModeloID_MONTADORA: TIntegerField
      DisplayLabel = 'C'#243'digo da Montadora'
      FieldName = 'ID_MONTADORA'
      Origin = 'ID_MONTADORA'
      Required = True
    end
  end
end
