inherited FrameQuitacaoGeracaoDocumentoTipoCheque: TFrameQuitacaoGeracaoDocumentoTipoCheque
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloCheque: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      inherited PnDadosDireita: TgbPanel
        inherited edtDocFederal: TgbDBTextEdit
          DataBinding.DataField = 'CHEQUE_DOC_FEDERAL_ENTRADA'
          DataBinding.DataSource = dsDadosCheque
        end
        inherited edtDtVencimento: TgbDBDateEdit
          DataBinding.DataField = 'CHEQUE_DT_VENCIMENTO_ENTRADA'
          DataBinding.DataSource = dsDadosCheque
        end
        inherited EdtValor: TgbDBCalcEdit
          DataBinding.DataField = 'VL_ENTRADA'
          DataBinding.DataSource = dsDadosCheque
        end
      end
      inherited PnDadosEsquerda: TgbPanel
        inherited PnTopo3PainelEsquerda: TgbPanel
          inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
            inherited edtNumero: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_NUMERO_ENTRADA'
              DataBinding.DataSource = dsDadosCheque
            end
          end
          inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
            inherited edtAgencia: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_AGENCIA_ENTRADA'
              DataBinding.DataSource = dsDadosCheque
            end
            inherited edtContaCorrente: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_CONTA_CORRENTE_ENTRADA'
              DataBinding.DataSource = dsDadosCheque
            end
          end
        end
        inherited PnTopo1PainelEsquerda: TgbPanel
          inherited PnSacado: TgbPanel
            inherited edtSacado: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_SACADO_ENTRADA'
              DataBinding.DataSource = dsDadosCheque
            end
          end
        end
        inherited PnTopo2PainelEsquerda: TgbPanel
          inherited edtBanco: TgbDBTextEdit
            DataBinding.DataField = 'CHEQUE_BANCO_ENTRADA'
            DataBinding.DataSource = dsDadosCheque
          end
          inherited edtDtEmissao: TgbDBDateEdit
            DataBinding.DataField = 'CHEQUE_DT_EMISSAO_ENTRADA'
            DataBinding.DataSource = dsDadosCheque
            ExplicitLeft = 435
          end
        end
      end
    end
  end
  object dsDadosCheque: TDataSource
    DataSet = MovGeracaoDocumento.cdsData
    Left = 480
    Top = 40
  end
end
