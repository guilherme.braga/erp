unit uFrameRelatorioFRFormulario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxMaskEdit,
  cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit,
  cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, uGBDBComboBox, cxButtonEdit,
  uGBDBButtonEditFK, System.Generics.Collections, uGBPanel, dxBarExtItems, cxSpinEdit, uGBDBSpinEdit,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter, uFrameFiltroVerticalPadrao;

type
  TFrameRelatorioFRFormulario = class(TFrameDetailPadrao)
    Label1: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    ComboBoxFormularios: TgbDBComboBox;
    Label2: TLabel;
    cbAcao: TgbDBComboBox;
    Label3: TLabel;
    Label4: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    Label5: TLabel;
    EdtNomeFormulario: TgbDBTextEdit;
    cbImpressoras: TgbDBComboBox;
    Label6: TLabel;
    Label7: TLabel;
    gbDBSpinEdit1: TgbDBSpinEdit;
    ActDefinirPersonalizacaoFiltros: TAction;
    bbDefinirPersonalizacaoFiltros: TdxBarButton;
    ActLimparDefinicaoFiltros: TAction;
    bbLimparDefinicaoFiltros: TdxBarButton;
    procedure EdtNomeFormularioExit(Sender: TObject);
    procedure ActDefinirPersonalizacaoFiltrosExecute(Sender: TObject);
    procedure ActLimparDefinicaoFiltrosExecute(Sender: TObject);
  private
    FFormularios: TDictionary<String, String>;
    procedure AtualizarFormulariosComboBox;
    procedure BuscarImpressoras;
    function BuscarListaFiltrosPersonalizados: TList<TCampoFiltro>;
    function FormularioRelatorioCarregadoEmMemoria: Boolean;
    procedure AjustarFramePeloTipoRelatorio;
    procedure AjustarFramePeloTipoRelatorioZebra;
    procedure AjustarFramePeloTipoRelatorioFastReport;
  public

  protected
    procedure DepoisDeIncluir; override;
    procedure DepoisDeAlterar; override;
    procedure AoCriarFrame; Override;
    procedure AntesDeConfirmar; Override;
    procedure DepoisDeConfirmar; Override;
    procedure GerenciarControles; override;
  end;

var
  FrameRelatorioFRFormulario: TFrameRelatorioFRFormulario;

implementation

{$R *.dfm}

uses uCadRelatorioFR, uTVariables, uImpressora, Rest.JSON,
  uCadRelatorioFRDefinirFiltrosPersonalizados, uControlsUtils, uRelatorioFR, uTControl_Function,
  uFrmMessage, uFrmRelatorioFRFiltroVertical;

{ TFrameRelatorioFRFormulario }

procedure TFrameRelatorioFRFormulario.ActDefinirPersonalizacaoFiltrosExecute(
  Sender: TObject);
var
  retornoListaFiltrosPersonalizados: TList<TCampoFiltro>;
  StrData: String;
begin
  inherited;
  retornoListaFiltrosPersonalizados := BuscarListaFiltrosPersonalizados;

  if not Assigned(retornoListaFiltrosPersonalizados) then
  begin
    exit;
  end;

  StrData := TCadRelatorioFrDefinirFiltrosPersonalizados.DefinirPersonalizacao(
    retornoListaFiltrosPersonalizados);

  if not (StrData.IsEmpty) then
  begin
    dsDataFrame.Dataset.FieldByName('FILTROS_PERSONALIZADOS').AsString := StrData;
  end;
end;

procedure TFrameRelatorioFRFormulario.ActLimparDefinicaoFiltrosExecute(Sender: TObject);
begin
  inherited;
  dsDataFrame.Dataset.FieldByName('FILTROS_PERSONALIZADOS').Clear;
end;

procedure TFrameRelatorioFRFormulario.AjustarFramePeloTipoRelatorio;
begin
  if TCadRelatorioFR(TControlFunction.GetInstance(
    'TCadRelatorioFR')).VerificarImpressaoZebra then
  begin
    AjustarFramePeloTipoRelatorioZebra;
  end
  else
  begin
    AjustarFramePeloTipoRelatorioFastReport;
  end;
end;

procedure TFrameRelatorioFRFormulario.AjustarFramePeloTipoRelatorioFastReport;
begin
  ComboBoxFormularios.gbReadyOnly := false;
  EdtNomeFormulario.gbReadyOnly := false;
  cbAcao.gbReadyOnly := false;
end;

procedure TFrameRelatorioFRFormulario.AjustarFramePeloTipoRelatorioZebra;
begin
  ComboBoxFormularios.gbReadyOnly := true;
  EdtNomeFormulario.gbReadyOnly := true;
  cbAcao.gbReadyOnly := true;

  dsDataFrame.DataSet.FieldByName('DESCRICAO').AsString := 'Emiss�o de Etiquetas';
  dsDataFrame.DataSet.FieldByName('NOME').AsString := 'FrmEmissaoEtiqueta';
  dsDataFrame.DataSet.FieldByName('ACAO').AsString := TRelatorioFR.ACAO_IMPRIMIR;
  TControlsUtils.SetFocus(cbImpressoras);
end;

procedure TFrameRelatorioFRFormulario.AntesDeConfirmar;
begin
  inherited;
  if dsDataFrame.DataSet.FieldByName('NOME').AsString.IsEmpty then
  begin
    dsDataFrame.DataSet.FieldByName('NOME').AsString :=
      FFormularios.Items[dsDataFrame.DataSet.FieldByName('DESCRICAO').AsString];
  end;
end;

procedure TFrameRelatorioFRFormulario.AoCriarFrame;
begin
  inherited;
  ComboBoxFormularios.Properties.CharCase := ecNormal;
  EdtNomeFormulario.DesabilitarCaseMode;
end;

procedure TFrameRelatorioFRFormulario.AtualizarFormulariosComboBox;
var Item: TPair<String, TForm>;
var ExisteWhereNoSQL: Boolean;
begin
  if not Assigned(FFormularios) then
    FFormularios := TDictionary<String, String>.Create;

  ComboBoxFormularios.Properties.Items.Clear;
  FFormularios.Clear;

  for Item in Variables.formInstances do
  begin
    ComboBoxFormularios.Properties.Items.Add(TForm(Item.Value).Caption);
    FFormularios.Add(TForm(Item.Value).Caption, TForm(Item.Value).Name);
  end;
end;

procedure TFrameRelatorioFRFormulario.BuscarImpressoras;
var impressoras: TStringList;
begin
  cbImpressoras.Properties.Items.Clear;
  cbImpressoras.Properties.Items.CommaText:= TImpressora.GetListaImpressoras;
end;

function TFrameRelatorioFRFormulario.BuscarListaFiltrosPersonalizados: TList<TCampoFiltro>;
var
  descricao: String;
  nomeReal: String;
  instancia: TFrmRelatorioFRFiltroVertical;
begin
  descricao := dsDataFrame.DataSet.FieldByName('DESCRICAO').AsString;

  if descricao = '' then
  begin
    TFrmMessage.Information('Informe o relat�rio que deseja personalizar os filtros.');
    result := nil;
    exit;
  end;

  nomeReal := FFormularios.Items[dsDataFrame.DataSet.FieldByName('DESCRICAO').AsString];

  if nomeReal = '' then
  begin
    TFrmMessage.Information('O relat�rio informado n�o � v�lido.');
    result := nil;
    exit;
  end;

  instancia := TFrmRelatorioFRFiltroVertical(TControlFunction.GetInstance(
    'TFrmRelatorioFRFiltroVertical'+nomeReal));

  if not Assigned(instancia) then
  begin
    result := nil;
    TFrmMessage.Information('N�o foi poss�vel localizar o formul�rio que cont�m os filtros, '+
    'certifique-se de que ele esteja selecionado corretamente e que o formul�rio esteja aberto.');
    exit;
  end;

  result := instancia.FFiltroVertical.FCampos;

  if not dsDataFrame.Dataset.FieldByName('FILTROS_PERSONALIZADOS').AsString.IsEmpty then
  begin
    TFrameFiltroVerticalPadrao.AplicarPersonalizacaoNosFiltros(result,
      TRelatorioFR.BuscarFiltrosPersonalizados(
      dsDataFrame.Dataset.FieldByName('FILTROS_PERSONALIZADOS').AsString));
  end;
end;

procedure TFrameRelatorioFRFormulario.DepoisDeAlterar;
begin
  inherited;
  AtualizarFormulariosComboBox;
  BuscarImpressoras;
  AjustarFramePeloTipoRelatorio;
end;

procedure TFrameRelatorioFRFormulario.DepoisDeConfirmar;
begin
  inherited;
  if Assigned(FFormularios) then
    FreeAndNil(FFormularios);
end;

procedure TFrameRelatorioFRFormulario.DepoisDeIncluir;
begin
  AtualizarFormulariosComboBox;
  BuscarImpressoras;
  AjustarFramePeloTipoRelatorio;
end;

procedure TFrameRelatorioFRFormulario.EdtNomeFormularioExit(Sender: TObject);
begin
  inherited;
  if dsDataFrame.Dataset.FieldByName('DESCRICAO').AsString.IsEmpty then
  begin
    dsDataFrame.Dataset.FieldByName('DESCRICAO').AsString :=
      dsDataFrame.Dataset.FieldByName('NOME').AsString;
  end;
end;

function TFrameRelatorioFRFormulario.FormularioRelatorioCarregadoEmMemoria: Boolean;
begin

end;

procedure TFrameRelatorioFRFormulario.GerenciarControles;
begin
  inherited;
  if not Assigned(dsDataFrame.Dataset) then
  begin
    exit;
  end;

  ActDefinirPersonalizacaoFiltros.Visible := dsDataFrame.Dataset.State in dsEditModes;

  ActLimparDefinicaoFiltros.Visible := (dsDataFrame.Dataset.State in dsEditModes) and
    (not dsDataFrame.Dataset.FieldByName('FILTROS_PERSONALIZADOS').AsString.IsEmpty);
end;

end.
