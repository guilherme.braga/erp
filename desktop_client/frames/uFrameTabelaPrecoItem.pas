unit uFrameTabelaPrecoItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDBEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls, uGBDBTextEditPK, dxBarExtItems, uGBPanel, cxLabel, ugbdbCalcEdit, cxDropDownEdit, cxCalc,
  ugbClientDataset;

type
  TFrameTabelaPrecoItem = class(TFrameDetailPadrao)
    labelCodigo: TLabel;
    labelCodProduto: TLabel;
    labelVlCusto: TLabel;
    labelPercLucro: TLabel;
    labelVlVenda: TLabel;
    edtCodProduto: TgbDBButtonEditFK;
    edtVlCusto: TgbDBCalcEdit;
    edtVlVenda: TgbDBTextEdit;
    edtPercLucro: TgbDBTextEdit;
    descProduto: TgbDBTextEdit;
    edtCodigo: TgbDBTextEditPK;
    dxBarButton6: TdxBarButton;
    ActIncluirTodosProdutos: TAction;
    actInsertPlus: TAction;
    dxBBInsertPlus: TdxBarButton;
    lbProdutoJaInserido: TcxLabel;
    procedure actInsertPlusExecute(Sender: TObject);
    procedure edtCodProdutoExit(Sender: TObject);
    procedure edtCodProdutogbDepoisDeConsultar(Sender: TObject);
  private
    procedure ExibirLabelProdutoJaInserido;
    { Private declarations }
  protected
    procedure GerenciarControles; override;
    procedure AntesDeConfirmar; override;
  public
    { Public declarations }
  end;

var
  FrameTabelaPrecoItem: TFrameTabelaPrecoItem;

implementation

{$R *.dfm}

uses uMovTabelaPreco, FireDAC.Comp.Client, Data.FireDACJSONReflect, uProduto,
  uPesqProduto, uDatasetUtils, uTControl_Function, uTMessage, uProdutoProxy, uSistema;

{ TFrameTabelaPrecoItem }
procedure TFrameTabelaPrecoItem.actInsertPlusExecute(Sender: TObject);
var
  IDs: string;
begin
  inherited;
  IDs := TPesqProduto.BuscarProdutos;
  if IDs <> EmptyStr then
  begin
    (TControlFunction.GetInstance('TMovTabelaPreco') as TMovTabelaPreco).InserirProdutos(IDs);
  end;
end;

procedure TFrameTabelaPrecoItem.AntesDeConfirmar;
var
  vBoProdutoJaInserido: Boolean;
begin
  inherited;
  Self.SelectNext(self, True, False);

  if dsDataFrame.Dataset.State in [dsInsert] then
  begin
    vBoProdutoJaInserido := TDatasetUtils.ValorExistente(dsDataFrame.DataSet.FieldByName('ID_PRODUTO')
      ,dsDataFrame.DataSet.FieldByName('ID_PRODUTO').Value);
  end;

  if vBoProdutoJaInserido then
  begin
    TMessage.MessageInformation('Produto j� Inserido nesta Tabela de Pre�o.');
    Abort;
  end;
end;

procedure TFrameTabelaPrecoItem.edtCodProdutoExit(Sender: TObject);
begin
  inherited;
  ExibirLabelProdutoJaInserido;
end;

procedure TFrameTabelaPrecoItem.edtCodProdutogbDepoisDeConsultar(Sender: TObject);
var dataset: TgbClientDataset;
  produto: TProdutoProxy;
begin
  dataset := TgbClientDataset(dsDataFrame.Dataset);
  inherited;
  if (dataset.GetAsInteger('ID_PRODUTO') > 0) then
  begin
    produto := TProduto.GetProdutoFilial(
      dataset.GetAsInteger('ID_PRODUTO'),
      TSistema.Sistema.Filial.Fid);
    try
      dataset.SetAsInteger('ID_PRODUTO', produto.FID);
      dataset.SetAsString('JOIN_DESCRICAO_PRODUTO', produto.FDescricao);
      dataset.SetAsFloat('VL_CUSTO_IMPOSTO_OPERACIONAL', produto.FProdutoFilial.FVlCustoImpostoOperacional);
    finally
      FreeAndNil(produto);
    end;
  end;
end;

procedure TFrameTabelaPrecoItem.ExibirLabelProdutoJaInserido;
var
  vBoProdutoJaInserido: Boolean;
begin
  vBoProdutoJaInserido := TDatasetUtils.ValorExistente(dsDataFrame.DataSet.FieldByName('ID_PRODUTO')
                                                      ,dsDataFrame.DataSet.FieldByName('ID_PRODUTO').Value);

  lbProdutoJaInserido.Visible := vBoProdutoJaInserido;
end;

procedure TFrameTabelaPrecoItem.GerenciarControles;
begin
  inherited;
  if Assigned(dsDataFrame.dataset) then
  begin
    if dsDataFrame.dataset.Active then
      actInsertPlus.Enabled :=  not (dsDataFrame.DataSet.State in dsEditModes)
    else
      actInsertPlus.Enabled := False;
    ExibirLabelProdutoJaInserido;
  end
  else
  begin
    actInsertPlus.Enabled := False;
  end;
end;

end.
