inherited FramePessoaRestricao: TFramePessoaRestricao
  inherited cxpcMain: TcxPageControl
    Top = 98
    Height = 207
    ClientRectBottom = 207
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 183
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        Height = 183
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Restri'#231#245'es'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 79
    Height = 79
    object lbDescricao: TLabel [0]
      Left = 8
      Top = 32
      Width = 83
      Height = 13
      Caption = 'Tipo de Restri'#231#227'o'
    end
    object lbQtdeFuncionarios: TLabel [1]
      Left = 8
      Top = 56
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 8
      Width = 73
      Height = 13
      Caption = 'Cadastrado Em'
    end
    inherited gbPanel3: TgbPanel
      Left = 363
      Top = 51
      Anchors = [akTop, akRight]
      TabOrder = 4
      ExplicitLeft = 363
      ExplicitTop = 51
    end
    object edtDescricaoRestricao: TgbDBTextEdit
      Left = 92
      Top = 52
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'DESCRICAO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = clWhite
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      OnExit = edtDescricaoRestricaoExit
      gbPassword = False
      Width = 267
    end
    object edtDescricaoTipoRestricao: TgbDBTextEdit
      Left = 151
      Top = 28
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_DESCRICAO_RESTRICAO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 286
    end
    object edtTipoRestricao: TgbDBButtonEditFK
      Left = 92
      Top = 28
      DataBinding.DataField = 'ID_TIPO_RESTRICAO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Style.Color = 14606074
      TabOrder = 1
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 'ID_TIPO_RESTRICAO;JOIN_DESCRICAO_RESTRICAO'
      gbTableName = 'TIPO_RESTRICAO'
      gbCamposConsulta = 'ID;DESCRICAO'
      gbClasseDoCadastro = 'TCadTipoRestricao'
      gbIdentificadorConsulta = 'TIPO_RESTRICAO'
      Width = 62
    end
    object gbDBCheckBox1: TgbDBCheckBox
      Left = 382
      Top = 4
      Anchors = [akTop, akRight]
      Caption = 'Ativo?'
      DataBinding.DataField = 'BO_ATIVO'
      DataBinding.DataSource = dsDataFrame
      Properties.ImmediatePost = True
      Properties.ValueChecked = 'S'
      Properties.ValueUnchecked = 'N'
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Transparent = True
      Width = 55
    end
    object EdtCadastradoEm: TgbDBDateEdit
      Left = 92
      Top = 4
      TabStop = False
      DataBinding.DataField = 'DH_INCLUSAO'
      DataBinding.DataSource = dsDataFrame
      Properties.DateButtons = [btnClear, btnNow]
      Properties.ImmediatePost = True
      Properties.Kind = ckDateTime
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      gbReadyOnly = True
      gbDateTime = True
      Width = 130
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaRestricao
  end
end
