unit uFrameConsultaDadosDetalhe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, uGBPanel, uFiltroFormulario, JvExControls,
  JvButton, JvTransparentButton, System.Actions, Vcl.ActnList, Data.DBXCommon, cxSplitter,
  uFrameFiltroVerticalPadrao, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrameConsultaDadosDetalhe = class(TFramePadrao)
    PnTituloFrameDetailPadrao: TgbPanel;
    PnDados: TgbPanel;
    alAcoes: TActionList;
    ActConsultar: TAction;
    ActLimparFiltros: TAction;
    ActImprimir: TAction;
    fdmDados: TgbFDMemTable;
    dsDados: TDataSource;
    pnFiltroVertical: TgbPanel;
    pnAcoesFiltroVertical: TgbPanel;
    JvTransparentButton4: TJvTransparentButton;
    JvTransparentButton5: TJvTransparentButton;
    BtnImprimir: TJvTransparentButton;
    procedure ActConsultarExecute(Sender: TObject);
    procedure ActLimparFiltrosExecute(Sender: TObject);
    procedure ActImprimirExecute(Sender: TObject);
  private
    procedure ClearWhereBundle;
    procedure ConfigurarPainelTitulo;
    procedure GerenciarControlesFrame;
  public
    FFiltroVertical: TFrameFiltroVerticalPadrao;
    FIdentificadorFrame: String;
    procedure SetDadosMaster(AFieldMaster: String; AIdMaster: Integer);
    constructor Create(AOwner: TComponent; AIdentificadorFrame: String; ARotuloFrame: String); overload;
    procedure OcultarTituloFrame;
    procedure OcultarCampoImpressao;
    procedure SetInjecaoCamposNaConsulta(const ACampos: String);
    procedure DesabilitarBotaoPesquisar;
    procedure HabilitarBotaoPesquisar;

    procedure Consultar; virtual;
    procedure ConsultarInjetandoWhere(const AWhereFixo: TFiltroPadrao); virtual;
  protected
    FWhereSearch: TFiltroPadrao;
    FFieldMaster: String;
    FIdMaster: Integer;
    FInjecaoCampos: String;
    FExecutandoConsulta: Boolean;
    procedure AoCriarFrame; override;
    procedure AoDestruirFrame; override;
    procedure DepoisConsultar; virtual;
    procedure AntesConsultar; virtual;
    procedure Imprimir; virtual;
    procedure SetWhereBundle; virtual;
    procedure PrepararFiltros; virtual;
    procedure GerenciarControles; virtual;
    procedure DefinirFiltros; virtual;
    procedure HabilitarPainelTitulo;
    procedure LimparDados;
  end;

var
  FrameConsultaDadosDetalhe: TFrameConsultaDadosDetalhe;

implementation

{$R *.dfm}

uses uDmAcesso, uDmConnection, uDatasetUtils, uFrmMessage, uSistema, uTUsuario, Data.FireDACJSONReflect,
  uDevExpressUtils, uConstantes, uFrmMessage_Process;

{ TFrameConsultaDadosDetalhe }

procedure TFrameConsultaDadosDetalhe.ActConsultarExecute(Sender: TObject);
begin
  inherited;
  FExecutandoConsulta := true;
  try
    FocarProximoControle(Self);
    AntesConsultar;
    ClearWhereBundle;
    Consultar;
    DepoisConsultar;
  finally
    FExecutandoConsulta := false;
  end;
end;

procedure TFrameConsultaDadosDetalhe.ActImprimirExecute(Sender: TObject);
begin
  inherited;
  Imprimir;
end;

procedure TFrameConsultaDadosDetalhe.ActLimparFiltrosExecute(Sender: TObject);
begin
  inherited;
  FFiltroVertical.LimparTodosFiltros;
end;

procedure TFrameConsultaDadosDetalhe.AntesConsultar;
begin

end;

procedure TFrameConsultaDadosDetalhe.AoCriarFrame;
begin
  inherited;
  FExecutandoConsulta := false;
  FWhereSearch := TFiltroPadrao.Create;

  ConfigurarPainelTitulo;
  HabilitarPainelTitulo;
  FFiltroVertical := TFrameFiltroVerticalPadrao.Create(pnFiltroVertical);
  FFiltroVertical.Parent := pnFiltroVertical;
  DefinirFiltros;

  if FFiltroVertical.ExisteFiltros then
  begin
    FFiltroVertical.CriarFiltros;
  end;
end;

procedure TFrameConsultaDadosDetalhe.AoDestruirFrame;
begin
  inherited;

  if Assigned(FFiltroVertical) then
  begin
    FreeAndNil(FFiltroVertical);
  end;

  if Assigned(FWhereSearch) then
  begin
    FreeAndNil(FWhereSearch);
  end;
end;

procedure TFrameConsultaDadosDetalhe.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TFrameConsultaDadosDetalhe.ConfigurarPainelTitulo;
begin
  PnTituloFrameDetailPadrao.Transparent := false;
  PnTituloFrameDetailPadrao.PanelStyle.OfficeBackgroundKind := pobkGradient;
end;

procedure TFrameConsultaDadosDetalhe.Consultar;
var DSList: TFDJSONDataSets;
begin
  inherited;
  try
    try
      TFrmMessage_Process.SendMessage('Aplicando os filtros a consulta de dados');
      LimparDados;

      SetWhereBundle;

      if FInjecaoCampos.IsEmpty then
      begin
        DSList := DmConnection.ServerMethodsClient.GetDados(FIdentificadorFrame,
          FWhereSearch.where, TSistema.Sistema.Usuario.idSeguranca);
      end
      else
      begin
        DSList := DmConnection.ServerMethodsClient.GetDadosComInjecaoCampos(FIdentificadorFrame,
          FWhereSearch.where, TSistema.Sistema.Usuario.idSeguranca, FInjecaoCampos);
      end;

      try
        TDatasetUtils.JSONDatasetToMemTable(DSList, fdmDados);
      finally
        FreeAndNil(DSList);
      end;
      ClearWhereBundle;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  Except
    on e : Exception do
    begin
      if e is TDBXError then
      begin
        TFrmMessage.Failure('Falha ao executar a consulta vinculada a tela.');
      end
      else
      begin
        TFrmMessage.Failure(e.message);
      end;

      abort;
    end;
  end;
end;

procedure TFrameConsultaDadosDetalhe.ConsultarInjetandoWhere(const AWhereFixo: TFiltroPadrao);
var
  DSList: TFDJSONDataSets;
begin
  inherited;
  try
    try
      TFrmMessage_Process.SendMessage('Realizando consulta de dados');
      if FInjecaoCampos.IsEmpty then
      begin
        DSList := DmConnection.ServerMethodsClient.GetDados(FIdentificadorFrame,
          AWhereFixo.where, TSistema.Sistema.Usuario.idSeguranca);
      end
      else
      begin
        DSList := DmConnection.ServerMethodsClient.GetDadosComInjecaoCampos(FIdentificadorFrame,
          AWhereFixo.where, TSistema.Sistema.Usuario.idSeguranca, FInjecaoCampos);
      end;

      try
        TDatasetUtils.JSONDatasetToMemTable(DSList, fdmDados);
      finally
        FreeAndNil(DSList);
      end;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  Except
    on e : Exception do
    begin
      if e is TDBXError then
      begin
        TFrmMessage.Failure('Falha ao executar a consulta vinculada a tela.');
      end
      else
      begin
        TFrmMessage.Failure(e.message);
      end;
    end;
  end;
end;

constructor TFrameConsultaDadosDetalhe.Create(AOwner: TComponent; AIdentificadorFrame: String;
  ARotuloFrame: String);
begin
  inherited Create(AOwner);
  FIdentificadorFrame := AIdentificadorFrame;
  PnTituloFrameDetailPadrao.Caption := ARotuloFrame;
end;

procedure TFrameConsultaDadosDetalhe.DefinirFiltros;
begin
 //Definir os filtros que ser�o utilizados
end;

procedure TFrameConsultaDadosDetalhe.DepoisConsultar;
begin
  //
end;

procedure TFrameConsultaDadosDetalhe.DesabilitarBotaoPesquisar;
begin
  ActConsultar.Enabled := False;
end;

procedure TFrameConsultaDadosDetalhe.GerenciarControles;
begin
  //
end;

procedure TFrameConsultaDadosDetalhe.GerenciarControlesFrame;
begin
  GerenciarControles;
end;

procedure TFrameConsultaDadosDetalhe.HabilitarBotaoPesquisar;
begin
  ActConsultar.Enabled := true;
end;

procedure TFrameConsultaDadosDetalhe.HabilitarPainelTitulo;
begin
  PnTituloFrameDetailPadrao.Visible := true;
end;

procedure TFrameConsultaDadosDetalhe.Imprimir;
begin
  //
end;

procedure TFrameConsultaDadosDetalhe.LimparDados;
begin
  if fdmDados.Active then
  begin
    fdmDados.EmptyDataset;
  end;
end;

procedure TFrameConsultaDadosDetalhe.OcultarCampoImpressao;
begin
  ActImprimir.Visible := false;
end;

procedure TFrameConsultaDadosDetalhe.OcultarTituloFrame;
begin
  PnTituloFrameDetailPadrao.Visible := false;
end;

procedure TFrameConsultaDadosDetalhe.PrepararFiltros;
begin
  //
end;

procedure TFrameConsultaDadosDetalhe.SetDadosMaster(AFieldMaster: String; AIdMaster: Integer);
begin
  FFieldMaster := AFieldMaster;
  FIdMaster := AIdMaster;
end;

procedure TFrameConsultaDadosDetalhe.SetInjecaoCamposNaConsulta(const ACampos: String);
begin
  FInjecaoCampos := ACampos;
end;

procedure TFrameConsultaDadosDetalhe.SetWhereBundle;
begin
  ClearWhereBundle;

  if not (FFieldMaster.IsEmpty) then
  begin
    FWhereSearch.AddIgual(FFieldMaster, FIdMaster);
  end;

  FFiltroVertical.SetWhere(FWhereSearch);
end;

end.
