inherited FrameTabelaPrecoItem: TFrameTabelaPrecoItem
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 794
        end
        object labelCodigo: TLabel
          Left = 8
          Top = 9
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object labelCodProduto: TLabel
          Left = 8
          Top = 41
          Width = 89
          Height = 13
          Caption = 'C'#243'digo do Produto'
        end
        object labelVlCusto: TLabel
          Left = 8
          Top = 66
          Width = 43
          Height = 13
          Caption = 'Vl. Custo'
        end
        object labelPercLucro: TLabel
          Left = 8
          Top = 91
          Width = 40
          Height = 13
          Caption = '% Lucro'
        end
        object labelVlVenda: TLabel
          Left = 8
          Top = 116
          Width = 45
          Height = 13
          Caption = 'Vl. Venda'
        end
        object edtCodProduto: TgbDBButtonEditFK
          Left = 121
          Top = 38
          DataBinding.DataField = 'ID_PRODUTO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Style.Color = 14606074
          TabOrder = 0
          OnExit = edtCodProdutoExit
          gbTextEdit = descProduto
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_PRODUTO;JOIN_DESCRICAO_PRODUTO'
          gbTableName = 'PRODUTO'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbDepoisDeConsultar = edtCodProdutogbDepoisDeConsultar
          Width = 63
        end
        object edtVlCusto: TgbDBCalcEdit
          Left = 121
          Top = 63
          TabStop = False
          DataBinding.DataField = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
          DataBinding.DataSource = dsDataFrame
          Properties.DisplayFormat = '###,###,###,###,###,##0.00'
          Properties.ImmediatePost = True
          Properties.ReadOnly = True
          Properties.UseThousandSeparator = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbReadyOnly = True
          gbRequired = True
          Width = 112
        end
        object edtVlVenda: TgbDBTextEdit
          Left = 121
          Top = 113
          DataBinding.DataField = 'VL_VENDA'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbRequired = True
          gbPassword = False
          Width = 112
        end
        object edtPercLucro: TgbDBTextEdit
          Left = 121
          Top = 88
          DataBinding.DataField = 'PERC_LUCRO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbRequired = True
          gbPassword = False
          Width = 112
        end
        object descProduto: TgbDBTextEdit
          Left = 181
          Top = 38
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_PRODUTO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbReadyOnly = True
          gbPassword = False
          Width = 263
        end
        object edtCodigo: TgbDBTextEditPK
          Left = 47
          Top = 6
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          Width = 60
        end
        object lbProdutoJaInserido: TcxLabel
          Left = 121
          Top = 6
          Caption = 'Produto j'#225' inserido nesta Tabela de Pre'#231'o.'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          AnchorY = 15
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Produtos'
    Style.IsFontAssigned = True
  end
  inherited ActionListMain: TActionList
    object ActIncluirTodosProdutos: TAction
      Caption = 'Incluir todos os produtos'
      Visible = False
    end
    object actInsertPlus: TAction
      Category = 'Manager'
      Caption = 'Inclus'#227'o Personalizada'
      OnExecute = actInsertPlusExecute
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
    inherited dxBarManager: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'siMaisAcoes'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBBInsertPlus'
        end>
    end
    inherited barNavegador: TdxBar
      DockedLeft = 373
    end
    object dxBarButton6: TdxBarButton [9]
      Caption = 'Incluir todos os produtos'
      Category = 0
      Hint = 'Incluir todos os produtos'
      Visible = ivAlways
    end
    object dxBBInsertPlus: TdxBarButton [10]
      Action = actInsertPlus
      Category = 0
    end
  end
end
