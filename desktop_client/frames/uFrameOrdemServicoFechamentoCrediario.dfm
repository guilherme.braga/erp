inherited FrameOrdemServicoFechamentoCrediario: TFrameOrdemServicoFechamentoCrediario
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnParcelamentoFechamento: TcxGroupBox
      inherited PnParcelamento: TcxGroupBox
        inherited gridParcelas: TcxGrid
          inherited viewParcelas: TcxGridDBBandedTableView
            inherited viewParcelasID_CARTEIRA: TcxGridDBBandedColumn
              DataBinding.FieldName = 'ID_CARTEIRA'
            end
            inherited viewParcelasJOIN_DESCRICAO_CARTEIRA: TcxGridDBBandedColumn
              DataBinding.FieldName = 'JOIN_DESCRICAO_CARTEIRA'
            end
          end
        end
        inherited PnPlanoPagamento: TgbPanel
          inherited cxLabel2: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
          inherited EdtIdPlanoPagamento: TgbDBButtonEditFK
            DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
            Style.IsFontAssigned = True
          end
          inherited EdtDescricaoPlanoPagamento: TgbDBTextEdit
            DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
            Style.IsFontAssigned = True
          end
        end
        inherited PnTituloParcelamento: TgbPanel
          Style.IsFontAssigned = True
        end
        inherited FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque
          inherited cxGBDadosMain: TcxGroupBox
            inherited PnTituloCheque: TgbPanel
              Style.IsFontAssigned = True
            end
            inherited PnDados: TgbPanel
              inherited PnDadosDireita: TgbPanel
                inherited edtDocFederal: TgbDBTextEdit
                  DataBinding.DataField = 'CHEQUE_DOC_FEDERAL'
                  DataBinding.DataSource = dsParcelas
                end
                inherited edtDtVencimento: TgbDBDateEdit
                  DataBinding.DataField = 'CHEQUE_DT_VENCIMENTO'
                  DataBinding.DataSource = dsParcelas
                end
                inherited EdtValor: TgbDBCalcEdit
                  DataBinding.DataField = 'VL_TITULO'
                  DataBinding.DataSource = dsParcelas
                end
              end
              inherited PnDadosEsquerda: TgbPanel
                inherited PnTopo3PainelEsquerda: TgbPanel
                  inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
                    inherited edtNumero: TgbDBTextEdit
                      DataBinding.DataField = 'CHEQUE_NUMERO'
                      DataBinding.DataSource = dsParcelas
                    end
                  end
                  inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
                    inherited edtAgencia: TgbDBTextEdit
                      DataBinding.DataField = 'CHEQUE_AGENCIA'
                      DataBinding.DataSource = dsParcelas
                    end
                    inherited edtContaCorrente: TgbDBTextEdit
                      DataBinding.DataField = 'CHEQUE_CONTA_CORRENTE'
                      DataBinding.DataSource = dsParcelas
                    end
                  end
                end
                inherited PnTopo1PainelEsquerda: TgbPanel
                  inherited PnSacado: TgbPanel
                    inherited edtSacado: TgbDBTextEdit
                      DataBinding.DataField = 'CHEQUE_SACADO'
                      DataBinding.DataSource = dsParcelas
                    end
                  end
                end
                inherited PnTopo2PainelEsquerda: TgbPanel
                  inherited edtBanco: TgbDBTextEdit
                    DataBinding.DataField = 'CHEQUE_BANCO'
                    DataBinding.DataSource = dsParcelas
                  end
                  inherited edtDtEmissao: TgbDBDateEdit
                    DataBinding.DataField = 'CHEQUE_DT_EMISSAO'
                    DataBinding.DataSource = dsParcelas
                  end
                end
              end
            end
          end
        end
        inherited FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao
          inherited cxGBDadosMain: TcxGroupBox
            inherited PnTituloCartao: TgbPanel
              Style.IsFontAssigned = True
            end
            inherited PnDados: TgbPanel
              inherited PnDtVencimento: TgbPanel
                inherited edtDtVencimento: TgbDBDateEdit
                  DataBinding.DataField = 'DT_VENCIMENTO'
                  DataBinding.DataSource = dsParcelas
                end
              end
              inherited PnOperadoraCartao: TgbPanel
                inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
                  DataBinding.DataField = 'ID_OPERADORA_CARTAO'
                  DataBinding.DataSource = dsParcelas
                end
                inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
                  DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
                  DataBinding.DataSource = dsParcelas
                end
              end
              inherited PnValor: TgbPanel
                inherited EdtValor: TgbDBCalcEdit
                  DataBinding.DataField = 'VL_TITULO'
                  DataBinding.DataSource = dsParcelas
                end
              end
            end
          end
        end
        inherited FrameTipoQuitacaoCrediario: TFrameTipoQuitacaoCrediario
          inherited cxGBDadosMain: TcxGroupBox
            inherited PnTituloCartao: TgbPanel
              Style.IsFontAssigned = True
            end
            inherited PnCampos: TgbPanel
              inherited PnHistorico: TgbPanel
                inherited EdtHistorico: TgbDBTextEdit
                  DataBinding.DataField = 'OBSERVACAO'
                  DataBinding.DataSource = dsParcelas
                end
              end
              inherited pnDtVencimento: TgbPanel
                inherited edtDtVencimento: TgbDBDateEdit
                  DataBinding.DataField = 'DT_VENCIMENTO'
                  DataBinding.DataSource = dsParcelas
                end
              end
              inherited PnValor: TgbPanel
                inherited EdtValor: TgbDBCalcEdit
                  DataBinding.DataField = 'VL_TITULO'
                  DataBinding.DataSource = dsParcelas
                end
              end
            end
          end
        end
      end
    end
    inherited PnLateral: TcxGroupBox
      inherited PnCreditoPessoa: TcxGroupBox
        inherited PnCreditoPessoaUtilizado: TgbPanel
          inherited EdtCreditoPessoaUtilizado: TgbDBTextEdit
            DataBinding.DataField = 'VL_PESSOA_CREDITO_UTILIZADO'
            Style.IsFontAssigned = True
          end
          inherited cxLabel3: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnCreditoPessoaSaldo: TgbPanel
          inherited EdtCreditoPessoaSaldo: TgbDBTextEdit
            DataBinding.DataField = 'CC_SALDO_PESSOA_CREDITO'
            Style.IsFontAssigned = True
          end
          inherited cxLabel7: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnCreditoPessoaDisponivel: TgbPanel
          inherited EdtCreditoDisponivel: TgbDBTextEdit
            DataBinding.DataField = 'JOIN_VALOR_PESSOA_CREDITO'
            Style.IsFontAssigned = True
          end
          inherited cxLabel6: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnTituloCreditoPessoa: TgbPanel
          Style.IsFontAssigned = True
        end
      end
      inherited PnTotalGeral: TcxGroupBox
        inherited PnTituloTotalGeral: TgbPanel
          Style.IsFontAssigned = True
        end
        inherited PnValorBruto: TgbPanel
          inherited EdtValorBruto: TgbDBTextEdit
            DataBinding.DataField = 'VL_TOTAL_BRUTO'
            Style.IsFontAssigned = True
          end
          inherited cxLabel1: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnDesconto: TgbPanel
          inherited EdtDesconto: TgbDBTextEdit
            DataBinding.DataField = 'VL_TOTAL_DESCONTO'
            Style.IsFontAssigned = True
          end
          inherited EdtPercDesconto: TgbDBTextEdit
            DataBinding.DataField = 'PERC_TOTAL_DESCONTO'
            Style.IsFontAssigned = True
          end
          inherited lbDesconto: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnAcrescimo: TgbPanel
          inherited EdtAcrescimo: TgbDBTextEdit
            DataBinding.DataField = 'VL_TOTAL_ACRESCIMO'
            Style.IsFontAssigned = True
          end
          inherited lbAcrescimo: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
          inherited EdtPercAcrescimo: TgbDBTextEdit
            DataBinding.DataField = 'PERC_TOTAL_ACRESCIMO'
            Style.IsFontAssigned = True
          end
        end
        inherited PnValorLiquido: TgbPanel
          inherited EdtValorLiquido: TgbDBTextEdit
            DataBinding.DataField = 'VL_TOTAL_LIQUIDO'
            Style.IsFontAssigned = True
          end
          inherited lbValorLiquido: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
      end
      inherited PnTroco: TcxGroupBox
        inherited PnValorTroco: TgbPanel
          inherited cxLabel8: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
          inherited EdtTroco: TgbDBTextEdit
            DataBinding.DataField = 'CC_VL_TROCO'
            Style.IsFontAssigned = True
          end
        end
        inherited mmTroco: TcxMemo
          Style.IsFontAssigned = True
        end
        inherited gbPanel11: TgbPanel
          Style.IsFontAssigned = True
        end
      end
      inherited cxGroupBox5: TcxGroupBox
        inherited PnPagamentoDinheiro: TgbPanel
          Style.IsFontAssigned = True
        end
        inherited gbPanel1: TgbPanel
          inherited EdtValorPagamentoDinheiro: TgbDBTextEdit
            DataBinding.DataField = 'VL_PAGAMENTO'
            Style.IsFontAssigned = True
          end
          inherited cxLabel10: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
        inherited PnDiferenca: TgbPanel
          inherited EdtDiferenca: TgbDBTextEdit
            DataBinding.DataField = 'CC_VL_DIFERENCA'
            Style.IsFontAssigned = True
          end
          inherited cxLabel11: TcxLabel
            Style.IsFontAssigned = True
            AnchorY = 14
          end
        end
      end
    end
  end
  inherited dxBarManagerFechamentoCrediario: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
