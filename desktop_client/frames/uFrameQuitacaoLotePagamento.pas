unit uFrameQuitacaoLotePagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameQuitacao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxBlobEdit, uGBDBBlobEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxLabel, uFrameTipoQuitacaoCheque, uFramePadrao,
  uFrameTipoQuitacaoCartao, uFrameQuitacaoLotePagamentoTipoCheque,
  uFrameQuitacaoLotePagamentoTipoCartao;

type
  TFrameQuitacaoLotePagamento = class(TFrameQuitacao)
    labelDtQuitacao: TLabel;
    labelTipoQuitacao: TLabel;
    labelContaCorrente: TLabel;
    labelDescontos: TLabel;
    labelAcrescimos: TLabel;
    labelPercDesconto: TLabel;
    labelPercAcrescimos: TLabel;
    labelVlTotalQuitacao: TLabel;
    labelContaAnalise: TLabel;
    edtDtQuitacao: TgbDBDateEdit;
    descTipoQuitacao: TgbDBTextEdit;
    edtIdTipoQuitacao: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    edtIdContaCorrente: TgbDBButtonEditFK;
    edtVlDescontos: TgbDBTextEdit;
    edtVlAcrescimos: TgbDBTextEdit;
    edtPcDescontos: TgbDBTextEdit;
    edtPcAcrescimos: TgbDBTextEdit;
    edtVlTotalQuitacao: TgbDBTextEdit;
    descContaAnalise: TgbDBTextEdit;
    dxBevel2: TdxBevel;
    EdtContaAnalise: TcxDBButtonEdit;
    labelCentroResultado: TLabel;
    edtFkCentroResultado: TgbDBButtonEditFK;
    descCentroResultado: TgbDBTextEdit;
    labelValor: TLabel;
    labelQuitacao: TLabel;
    edtValor: TgbDBTextEdit;
    edtQuitacao: TgbDBTextEdit;
    lbTrocoDiferenca: TcxLabel;
    dsLote: TDataSource;
    labelObservacao: TLabel;
    edtObservacao: TgbDBBlobEdit;
    PnDadosBaixaPorTitulo: TgbPanel;
    labelDocumento: TLabel;
    labelDescricao: TLabel;
    edtDocumento: TgbDBTextEdit;
    edtDescricao: TgbDBTextEdit;
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure edtQuitacaoExit(Sender: TObject);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure edtIdTipoQuitacaogbDepoisDeConsultar(Sender: TObject);
  private
    procedure ExibirTrocoDiferenca;
    procedure ExibirFrameTipoQuitacao;
  public
    FFrameQuitacaoLotePagamentoTipoCheque: TFrameQuitacaoLotePagamentoTipoCheque;
    FFrameQuitacaoLotePagamentoTipoCartao: TFrameQuitacaoLotePagamentoTipoCartao;
    procedure PreencherValorPagamento;
    procedure ExibirDadosDeBaixaPorTitulo(AExibir: Boolean);
  protected
    procedure AntesDeConfirmar; override;
    procedure AntesDeIncluir; override;
    procedure AntesDeRemover; override;
    procedure DepoisDeConfirmar; override;
    procedure AoCriarFrame; override;
    procedure InstanciarFrames; override;
    procedure GerenciarControles; override;
    procedure DepoisExibirFrameCheque; override;
  end;

var
  FrameQuitacaoLotePagamento: TFrameQuitacaoLotePagamento;

implementation

{$R *.dfm}

uses uMovLotePagamento, uTControl_Function, uLotePagamentoProxy, uTMessage,
  uPesqContaAnalise, uTipoQuitacaoProxy;

procedure TFrameQuitacaoLotePagamento.AntesDeConfirmar;
var
  vDataSet : TDataSet;
begin
  inherited;

  vDataSet := dsDataFrame.DataSet;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat <= 0) then
  begin
    TMessage.MessageInformation('Informe um valor para quita��o do documento.');
    Abort;
  end;

  if (TControlFunction.GetInstance('TMovLotePagamento') as TMovLotePagamento).QuitacaoEmCartao then
  begin
    FFrameQuitacaoLotePagamentoTipoCartao.ValidarDados;
  end;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    vDataSet.FieldByName('VL_TOTAL').AsFloat) then
  begin
    vDataSet.FieldByName('VL_TOTAL').AsFloat :=
      vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat;
  end;
end;

procedure TFrameQuitacaoLotePagamento.ExibirDadosDeBaixaPorTitulo(
  AExibir: Boolean);
begin
  PnDadosBaixaPorTitulo.Visible := AExibir;
end;

procedure TFrameQuitacaoLotePagamento.ExibirFrameTipoQuitacao;
begin
  if Assigned(dsDataFrame.Dataset) then
  begin
    ExibirFrameCartao(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CREDITO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO));

   ExibirFrameCheque(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO));
  end;
end;

procedure TFrameQuitacaoLotePagamento.AntesDeIncluir;
var
  Status  : string;
begin
  inherited;

  Status := (TControlFunction.GetInstance('TMovLotePagamento') as TMovLotePagamento).cdsDataSTATUS.AsString;
  if Status = TLotePagamentoProxy.STATUS_EFETIVADO then
  begin
    TMessage.MessageInformation('Lote de Pagamento j� se encontra Efetivado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoLotePagamento.AntesDeRemover;
var
  Status  : string;
begin
  inherited;

  Status := (TControlFunction.GetInstance('TMovLotePagamento') as TMovLotePagamento).cdsDataSTATUS.AsString;
  if Status = TLotePagamentoProxy.STATUS_EFETIVADO then
  begin
    TMessage.MessageInformation('Lote de Pagamento j� se encontra Efetivado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoLotePagamento.AoCriarFrame;
begin
  inherited;
  EdtDescricao.Properties.CharCase := ecNormal;
  EdtDocumento.Properties.CharCase := ecNormal;
end;

procedure TFrameQuitacaoLotePagamento.DepoisDeConfirmar;
begin
  inherited;
//  (TControlFunction.GetInstance('TMovLotePagamento') as TMovLotePagamento)
end;

procedure TFrameQuitacaoLotePagamento.DepoisExibirFrameCheque;
begin
  inherited;
  if dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) then
  begin
    FFrameQuitacaoLotePagamentoTipoCheque.ConfigurarParaChequeTerceiro;
  end
  else
  begin
    FFrameQuitacaoLotePagamentoTipoCheque.ConfigurarParaChequeProprio;
  end;
end;

procedure TFrameQuitacaoLotePagamento.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLotePagamento.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLotePagamento.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if not Assigned(dsDataFrame.Dataset) then
    exit;

  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLotePagamento.ExibirTrocoDiferenca;
begin
  if not Assigned(dsDataFrame.DataSet) then
    exit;

  if dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat >
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat -
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Troco R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  if dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat -
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Diferen�a R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat := 0;
    lbTrocoDiferenca.Visible := false;
  end;
end;

procedure TFrameQuitacaoLotePagamento.GerenciarControles;
begin
  inherited;
  PreencherValorPagamento;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoLotePagamento.InstanciarFrames;
begin
  inherited;
  if not Assigned(FFrameQuitacaoLotePagamentoTipoCheque) then
  begin
    FFrameQuitacaoLotePagamentoTipoCheque := TFrameQuitacaoLotePagamentoTipoCheque.Create(PnQuitacaoCheque);
    FFrameQuitacaoLotePagamentoTipoCheque.Parent := PnQuitacaoCheque;
    FFrameQuitacaoLotePagamentoTipoCheque.Align := alClient;
    FFrameQuitacaoLotePagamentoTipoCheque.SetDataset(dsDataFrame.Dataset);
  end;

  if not Assigned(FFrameQuitacaoLotePagamentoTipoCartao) then
  begin
    FFrameQuitacaoLotePagamentoTipoCartao := TFrameQuitacaoLotePagamentoTipoCartao.Create(PnQuitacaoCartao);
    FFrameQuitacaoLotePagamentoTipoCartao.Parent := PnQuitacaoCartao;
    FFrameQuitacaoLotePagamentoTipoCartao.Align := alClient;
    FFrameQuitacaoLotePagamentoTipoCartao.SetDataset(dsDataFrame.Dataset);
  end;
end;

procedure TFrameQuitacaoLotePagamento.edtIdTipoQuitacaogbDepoisDeConsultar(
  Sender: TObject);
begin
  inherited;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoLotePagamento.edtQuitacaoExit(Sender: TObject);
begin
  inherited;
  ExibirTrocoDiferenca;
end;

procedure TFrameQuitacaoLotePagamento.PreencherValorPagamento;
begin
  if Assigned(dsDataFrame.DataSet) and (dsDataFrame.DataSet.State in dsEditModes) then
  begin
    dsDataFrame.DataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat :=
      dsDataFrame.DataSet.FieldByName('VL_TOTAL').AsFloat;

    ExibirTrocoDiferenca;
  end;
end;

end.
