inherited FrameVendaVarejoReceitaOtica: TFrameVendaVarejoReceitaOtica
  inherited cxpcMain: TcxPageControl
    Top = 57
    Height = 248
    ExplicitTop = 57
    ExplicitWidth = 451
    ExplicitHeight = 248
    ClientRectBottom = 248
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 224
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 451
        Height = 224
        ExplicitWidth = 451
        ExplicitHeight = 224
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 224
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 224
        Height = 224
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 433
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Receita '#211'tica'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 38
    Height = 38
    object Label1: TLabel [0]
      Left = 7
      Top = 9
      Width = 64
      Height = 13
      Caption = 'Receita '#211'tica'
    end
    inherited gbPanel3: TgbPanel
      Left = 371
      Top = 5
      Anchors = [akTop, akRight]
      ExplicitLeft = 371
      ExplicitTop = 5
      inherited JvTransparentButton2: TJvTransparentButton
        ExplicitLeft = 69
        ExplicitTop = -14
      end
    end
    object campoReceitaotica: TgbDBButtonEditFK
      Left = 74
      Top = 6
      DataBinding.DataField = 'ID_RECEITA_OTICA'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 1
      gbTextEdit = gbDBTextEdit1
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 'ID_RECEITA_OTICA;JOIN_ESPECIFICACAO_RECEITA_OTICA'
      gbTableName = 'RECEITA_OTICA'
      gbCamposConsulta = 'ID;ESPECIFICACAO'
      gbDepoisDeConsultar = campoReceitaoticagbDepoisDeConsultar
      Width = 60
    end
    object gbDBTextEdit1: TgbDBTextEdit
      Left = 131
      Top = 6
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_ESPECIFICACAO_RECEITA_OTICA'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 234
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = MovVendaVarejo.cdsVendaReceitaOtica
  end
end
