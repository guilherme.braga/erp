inherited FrameGerenciamentoEstacaoMidia: TFrameGerenciamentoEstacaoMidia
  inherited cxpcMain: TcxPageControl
    ExplicitWidth = 462
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 259
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 462
        ExplicitWidth = 462
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 738
        end
        object lCodigo: TLabel
          Left = 8
          Top = 9
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object lMidia: TLabel
          Left = 8
          Top = 38
          Width = 24
          Height = 13
          Caption = 'M'#237'dia'
        end
        object dbCodigo: TgbDBTextEditPK
          Left = 49
          Top = 6
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object bdIdMidia: TgbDBButtonEditFK
          Left = 49
          Top = 35
          DataBinding.DataField = 'ID_MIDIA'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 1
          gbTextEdit = descMidia
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_MIDIA;JOIN_DESCRICAO_MIDIA'
          gbTableName = 'MIDIA'
          gbCamposConsulta = 'ID;NOME'
          Width = 60
        end
        object descMidia: TgbDBTextEdit
          Left = 106
          Top = 35
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_MIDIA'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbReadyOnly = True
          gbPassword = False
          Width = 339
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
end
