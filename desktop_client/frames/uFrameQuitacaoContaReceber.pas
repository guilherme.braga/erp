unit uFrameQuitacaoContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameQuitacao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxBlobEdit,
  cxDBEdit, uGBDBBlobEdit, Vcl.StdCtrls, uGBDBTextEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, uGBDBDateEdit, cxButtonEdit, uGBDBButtonEditFK,
  uGBPanel, dxBarExtItems, cxLabel, uFramePadrao,
  uFrameQuitacaoContaReceberTipoCheque, uFrameQuitacaoContaReceberTipoCartao,
  uGBDBValorComPercentual, uUsuarioDesignControl;

type
  TFrameQuitacaoContaReceber = class(TFrameQuitacao)
    labelDtCadastro: TLabel;
    edtDtCadastro: TgbDBDateEdit;
    bevelDadosdeOrigem: TdxBevel;
    labelOrigem: TLabel;
    edtTpDocumentoOrigem: TgbDBTextEdit;
    edtIdDocumentoOrigem: TgbDBTextEdit;
    labelSituacao: TLabel;
    edSituacao: TgbDBTextEdit;
    dsContaReceber: TDataSource;
    Label7: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    edtDtQuitacao: TgbDBDateEdit;
    edtVlQuitacao: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    descTipoQuitacao: TgbDBTextEdit;
    edtIdTipoQuitacao: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    edtIdContaCorrente: TgbDBButtonEditFK;
    edtVlDescontos: TgbDBTextEdit;
    edtVlAcrescimos: TgbDBTextEdit;
    edtPcDescontos: TgbDBTextEdit;
    edtPcAcrescimos: TgbDBTextEdit;
    edtVlTotalQuitacao: TgbDBTextEdit;
    descContaAnalise: TgbDBTextEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    lbTrocoDiferenca: TcxLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    EdtJuros: TGBDBValorComPercentual;
    EdtMulta: TGBDBValorComPercentual;
    procedure edtVlQuitacaoEnter(Sender: TObject);
    procedure edtVlDescontosEnter(Sender: TObject);
    procedure edtVlTotalQuitacaoExit(Sender: TObject);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure edtIdTipoQuitacaogbDepoisDeConsultar(Sender: TObject);
  private
    procedure ExibirTrocoDiferenca;
    procedure ExibirFrameTipoQuitacao;
    procedure BloquearEdicaoJurosMulta;
    procedure BloquearCampoDataQuitacao;
  public
    FFrameQuitacaoContaReceberTipoCheque: TFrameQuitacaoContaReceberTipoCheque;
    FFrameQuitacaoContaReceberTipoCartao: TFrameQuitacaoContaReceberTipoCartao;

    VlQuitacaoEnter : Double;
    VlDescontoEnter : Double;
    VlAbertoEnter   : Double;

    procedure PreencherValorPagamento;
    function ExisteTroco: Boolean;
    function TipoQuitacaoCheque: Boolean;

    const NOME_FRAME = 'FrameQuitacaoContaReceber';
  protected
    procedure AoCriarFrame;override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeIncluir; override;
    procedure DepoisDeIncluir; override;
    procedure AntesDeAlterar; override;
    procedure DepoisDeAlterar; override;
    procedure GerenciarControles; override;
    procedure InstanciarFrames; override;
    procedure AntesDeRemover; override;
    procedure DepoisDeRemover; override;
    procedure DepoisExibirFrameCheque; override;
  end;

var
  FrameQuitacaoContaReceber: TFrameQuitacaoContaReceber;

implementation

{$R *.dfm}

uses uMovContaReceber, uTControl_Function, uTMessage , uDatasetUtils,
  uContaReceberProxy, uPesqContaAnalise, uFormaPagamentoProxy,
  uTipoQuitacaoProxy, uFrameDetailPadrao, uMathUtils, uControlsUtils;

{ TFrameQuitacaoContaReceber }

procedure TFrameQuitacaoContaReceber.AntesDeIncluir;
var
  Status  : string;
begin

  Status := (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).cdsDataSTATUS.AsString;

  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
  VlAbertoEnter   := (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).cdsDataVL_ABERTO.AsFloat;

  if Status = TContaReceberMovimento.QUITADO then
  begin
    TMessage.MessageInformation('Conta a Receber Quitado.');
    Abort;
  end;

  if Status = TContaReceberMovimento.CANCELADO then
  begin
    TMessage.MessageInformation('Conta a Receber Cancelado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoContaReceber.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  TPesqContaAnalise.ExecutarConsulta(
    dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
    dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'),
    dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
end;

procedure TFrameQuitacaoContaReceber.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  TPesqContaAnalise.ExecutarConsultaOculta(
    dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
    dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'),
    dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
end;

procedure TFrameQuitacaoContaReceber.AntesDeAlterar;
begin
  inherited;
  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
  VlAbertoEnter   := (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).cdsDataVL_ABERTO.AsFloat
                     + dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat
                     + dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
end;

procedure TFrameQuitacaoContaReceber.AntesDeConfirmar;
var
  vDataSet : TDataSet;
begin
  inherited;
  vDataSet := dsDataFrame.DataSet;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat <= 0) then
  begin
    TMessage.MessageInformation('Informe um valor para quita��o do documento.');
    Abort;
  end;

  if (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).QuitacaoEmCartao then
  begin
    FFrameQuitacaoContaReceberTipoCartao.ValidarDados;
  end;


  if vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat < vDataSet.FieldByName('VL_ACRESCIMO').AsFloat then
  try
    (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).FAtualizarTotalItem := false;
    if vDataSet.FieldByName('VL_MULTA').AsFloat > vDataSet.FieldByName('VL_ACRESCIMO').AsFloat then
    begin
      vDataSet.FieldByName('VL_MULTA').AsFloat := vDataSet.FieldByName('VL_ACRESCIMO').AsFloat;
      vDataSet.FieldByName('VL_JUROS').AsFloat := 0;
    end
    else
    begin
      vDataSet.FieldByName('VL_JUROS').AsFloat := vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat -
        vDataSet.FieldByName('VL_MULTA').AsFloat;
    end;

    vDataSet.FieldByName('PERC_JUROS').AsFloat := TMathUtils.PercentualSobreValor(
      vDataSet.FieldByName('VL_JUROS').AsFloat,
      (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).cdsDataVL_ABERTO.AsFloat);

    vDataSet.FieldByName('PERC_MULTA').AsFloat := TMathUtils.PercentualSobreValor(
      vDataSet.FieldByName('VL_MULTA').AsFloat,
      (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).cdsDataVL_ABERTO.AsFloat);
  finally
    (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).FAtualizarTotalItem := true;
  end;

  if ExisteTroco then
  begin
    vDataSet.FieldByName('VL_TROCO').AsFloat :=
      vDataSet.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat;
  end;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    vDataSet.FieldByName('VL_TOTAL').AsFloat) then
  begin
    vDataSet.FieldByName('VL_TOTAL').AsFloat :=
      vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat;
  end;
end;

procedure TFrameQuitacaoContaReceber.AntesDeRemover;
var
  Origem  : string;
begin
  inherited;
  Origem := dsDataFrame.DataSet.FieldByName('tp_documento_origem').AsString;

  if Origem <> TContaReceberMovimento.ORIGEM_MANUAL then
  begin
    TMessage.MessageInformation('Esta Quita��o s� pode ser removida a partir de sua origem.');
    Abort;
  end;
end;

procedure TFrameQuitacaoContaReceber.AoCriarFrame;
begin
  inherited;

end;

procedure TFrameQuitacaoContaReceber.BloquearCampoDataQuitacao;
begin
  edtDtQuitacao.gbReadyOnly := (TControlFunction.GetInstance('TMovContaReceber')
    as TMovContaReceber).FParametroBloquearDataQuitacao.ValorSim;

  if not edtDtQuitacao.gbReadyOnly then
  begin
    TControlsUtils.SetFocus(edtDtQuitacao);
  end;
end;

procedure TFrameQuitacaoContaReceber.BloquearEdicaoJurosMulta;
var
  bloquearJuros: Boolean;
  bloquearMulta: Boolean;
  formContaAReceber: TMovContaReceber;
begin
  formContaAReceber := (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber);

  bloquearJuros := Assigned(formContaAReceber) and
    Assigned(formContaAReceber.FParametroBloquearEdicaoJuros)
    and formContaAReceber.FParametroBloquearEdicaoJuros.ValorSim;

  bloquearMulta := Assigned(formContaAReceber) and
    Assigned(formContaAReceber.FParametroBloquearEdicaoMulta)
    and formContaAReceber.FParametroBloquearEdicaoMulta.ValorSim;

  EdtMulta.gbReadOnlyPercentual := bloquearMulta;
  EdtMulta.gbReadOnlyValor := bloquearMulta;
  EdtJuros.gbReadOnlyPercentual := bloquearJuros;
  EdtJuros.gbReadOnlyValor := bloquearJuros;
end;

procedure TFrameQuitacaoContaReceber.DepoisDeAlterar;
begin
  inherited;
  BloquearCampoDataQuitacao;
end;

procedure TFrameQuitacaoContaReceber.DepoisDeConfirmar;
begin
  inherited;
end;

procedure TFrameQuitacaoContaReceber.DepoisDeIncluir;
begin
  inherited;
  BloquearCampoDataQuitacao;
end;

procedure TFrameQuitacaoContaReceber.DepoisDeRemover;
begin
  inherited;
  (TControlFunction.GetInstance('TMovContaReceber') as TMovContaReceber).ActConfirm.Execute;
end;

procedure TFrameQuitacaoContaReceber.DepoisExibirFrameCheque;
begin
  inherited;
{  if dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) then
  begin
    FFrameQuitacaoContaReceberTipoCheque.ConfigurarParaChequeTerceiro;
  end
  else
  begin
    FFrameQuitacaoContaReceberTipoCheque.ConfigurarParaChequeProprio;
  end;}
end;

procedure TFrameQuitacaoContaReceber.edtVlQuitacaoEnter(Sender: TObject);
begin
  inherited;
  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
end;

procedure TFrameQuitacaoContaReceber.edtVlTotalQuitacaoExit(Sender: TObject);
begin
  inherited;
  ExibirTrocoDiferenca;
end;

procedure TFrameQuitacaoContaReceber.ExibirFrameTipoQuitacao;
begin
  if Assigned(dsDataFrame.Dataset) then
  begin
    ExibirFrameCartao(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CREDITO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO));

   ExibirFrameCheque(TipoQuitacaoCheque);
  end;
end;

procedure TFrameQuitacaoContaReceber.ExibirTrocoDiferenca;
begin
  if not Assigned(dsDataFrame.DataSet) then
    exit;

  if ExisteTroco then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat -
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Troco R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  if dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat -
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Diferen�a R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat := 0;
    lbTrocoDiferenca.Visible := false;
  end;
end;

function TFrameQuitacaoContaReceber.ExisteTroco: Boolean;
begin
  result := dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat >
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat;
end;

procedure TFrameQuitacaoContaReceber.GerenciarControles;
var
  Origem : string;
begin
  inherited;

  if Assigned(dsDataFrame.DataSet) then
  begin
    Origem := dsDataFrame.DataSet.FieldByName('TP_DOCUMENTO_ORIGEM').AsString;
    cxGBDadosMain.Enabled := (Origem = TContaReceberMovimento.ORIGEM_MANUAL);
  end;

  BloquearEdicaoJurosMulta;

  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoContaReceber.InstanciarFrames;
begin
  inherited;
  if not Assigned(FFrameQuitacaoContaReceberTipoCheque) then
  begin
    FFrameQuitacaoContaReceberTipoCheque :=
      TFrameQuitacaoContaReceberTipoCheque.Create(PnQuitacaoCheque);
    FFrameQuitacaoContaReceberTipoCheque.Parent := PnQuitacaoCheque;
    FFrameQuitacaoContaReceberTipoCheque.Align := alClient;
    FFrameQuitacaoContaReceberTipoCheque.SetDataset(dsDataFrame.Dataset);
  end;

  if not Assigned(FFrameQuitacaoContaReceberTipoCartao) then
  begin
    FFrameQuitacaoContaReceberTipoCartao :=
      TFrameQuitacaoContaReceberTipoCartao.Create(PnQuitacaoCartao);
    FFrameQuitacaoContaReceberTipoCartao.Parent := PnQuitacaoCartao;
    FFrameQuitacaoContaReceberTipoCartao.Align := alClient;
    FFrameQuitacaoContaReceberTipoCartao.SetDataset(dsDataFrame.Dataset);
  end;
end;

procedure TFrameQuitacaoContaReceber.PreencherValorPagamento;
begin
  if Assigned(dsDataFrame.DataSet) and (dsDataFrame.DataSet.State in dsEditModes) then
  begin
    dsDataFrame.DataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat :=
      dsDataFrame.DataSet.FieldByName('VL_TOTAL').AsFloat;

    ExibirTrocoDiferenca;
  end;
end;

function TFrameQuitacaoContaReceber.TipoQuitacaoCheque: Boolean;
begin
  result := dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO)
end;

procedure TFrameQuitacaoContaReceber.edtIdTipoQuitacaogbDepoisDeConsultar(
  Sender: TObject);
begin
  inherited;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoContaReceber.edtVlDescontosEnter(Sender: TObject);
begin
  inherited;
  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
end;

end.
