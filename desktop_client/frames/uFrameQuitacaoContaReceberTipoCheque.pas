unit uFrameQuitacaoContaReceberTipoCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCheque, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxCalc, cxDBEdit, uGBDBCalcEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, uGBPanel, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, cxGroupBox,
  Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable,
  cxButtonEdit, uGBDBButtonEditFK;

type
  TFrameQuitacaoContaReceberTipoCheque = class(TFrameTipoQuitacaoCheque)
    dsDataFrame: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoContaReceberTipoCheque: TFrameQuitacaoContaReceberTipoCheque;

implementation

{$R *.dfm}

uses uMovContaReceber;

{ TFrameQuitacaoContaReceberTipoCheque }

procedure TFrameQuitacaoContaReceberTipoCheque.SetDataset(ADataset: TDataset);
begin
  dsDataFrame.Dataset := ADataset;
end;

end.
