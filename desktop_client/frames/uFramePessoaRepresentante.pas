unit uFramePessoaRepresentante;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxDropDownEdit, cxDBEdit, uGBDBComboBox, cxMaskEdit, cxCalendar,
  uGBDBDateEdit, uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls;

type
  TFramePessoaRepresentante = class(TFrameDetailPadrao)
    lbCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    lbCPFCliente: TLabel;
    edDocFederal: TgbDBTextEdit;
    lbRG: TLabel;
    edDocEstadual: TgbDBTextEdit;
    lbNome: TLabel;
    edNome: TgbDBTextEdit;
    ldDtNascimento: TLabel;
    deDtNascimento: TgbDBDateEdit;
    lbEstadoCivil: TLabel;
    cbEstadoCivil: TgbDBComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FramePessoaRepresentante: TFramePessoaRepresentante;

implementation

{$R *.dfm}

uses uCadPessoa;

end.
