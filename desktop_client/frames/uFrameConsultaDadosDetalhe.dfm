inherited FrameConsultaDadosDetalhe: TFrameConsultaDadosDetalhe
  Width = 451
  Height = 305
  Align = alClient
  ExplicitWidth = 451
  ExplicitHeight = 305
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 451
    ExplicitHeight = 305
    Height = 305
    Width = 451
    object PnTituloFrameDetailPadrao: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      Alignment = alCenterCenter
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentBackground = False
      ParentFont = False
      Style.BorderStyle = ebsOffice11
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 19
      Width = 447
    end
    object PnDados: TgbPanel
      Left = 317
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 282
      Width = 132
    end
    object pnFiltroVertical: TgbPanel
      Left = 2
      Top = 21
      Align = alLeft
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Transparent = True
      Height = 282
      Width = 315
      object pnAcoesFiltroVertical: TgbPanel
        AlignWithMargins = True
        Left = 5
        Top = 5
        Align = alTop
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Transparent = True
        Height = 26
        Width = 305
        object JvTransparentButton4: TJvTransparentButton
          Left = 2
          Top = 2
          Width = 102
          Height = 22
          Action = ActConsultar
          Align = alLeft
          Caption = 'Aplicar Filtros '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsNone
          ParentFont = False
          TextAlign = ttaRight
          Images.ActiveImage = DmAcesso.cxImage16x16
          Images.ActiveIndex = 11
          Images.GrayIndex = 11
          Images.DisabledIndex = 11
          Images.DownIndex = 11
          Images.HotIndex = 11
        end
        object JvTransparentButton5: TJvTransparentButton
          Left = 104
          Top = 2
          Width = 102
          Height = 22
          Action = ActLimparFiltros
          Align = alLeft
          Caption = 'Limpar Filtros '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsNone
          ParentFont = False
          TextAlign = ttaRight
          Images.ActiveImage = DmAcesso.cxImage16x16
          Images.ActiveIndex = 17
          Images.GrayIndex = 17
          Images.DisabledIndex = 17
          Images.DownIndex = 17
          Images.HotIndex = 17
          ExplicitLeft = 102
        end
        object BtnImprimir: TJvTransparentButton
          Left = 206
          Top = 2
          Width = 96
          Height = 22
          Action = ActImprimir
          Align = alLeft
          Caption = 'Impress'#245'es '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsNone
          ParentFont = False
          TextAlign = ttaRight
          Images.ActiveImage = DmAcesso.cxImage16x16
          Images.ActiveIndex = 140
          Images.GrayIndex = 140
          Images.DisabledIndex = 140
          Images.DownIndex = 140
          Images.HotIndex = 140
          ExplicitLeft = 204
        end
      end
    end
  end
  object alAcoes: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 368
    Top = 96
    object ActConsultar: TAction
      ImageIndex = 11
      OnExecute = ActConsultarExecute
    end
    object ActLimparFiltros: TAction
      ImageIndex = 17
      OnExecute = ActLimparFiltrosExecute
    end
    object ActImprimir: TAction
      ImageIndex = 140
      OnExecute = ActImprimirExecute
    end
  end
  object fdmDados: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 336
    Top = 152
  end
  object dsDados: TDataSource
    DataSet = fdmDados
    Left = 364
    Top = 152
  end
end
