unit uFrameGerenciamentoEstacaoMidia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, dxBar, cxClasses, System.Actions, Vcl.ActnList,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  uGBDBTextEdit, cxMaskEdit, cxButtonEdit, cxDBEdit, uGBDBButtonEditFK,
  cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, Vcl.Menus, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxBarExtItems, uGBPanel;

type
  TFrameGerenciamentoEstacaoMidia = class(TFrameDetailPadrao)
    lCodigo: TLabel;
    dbCodigo: TgbDBTextEditPK;
    lMidia: TLabel;
    bdIdMidia: TgbDBButtonEditFK;
    descMidia: TgbDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameGerenciamentoEstacaoMidia: TFrameGerenciamentoEstacaoMidia;

implementation

{$R *.dfm}

end.
