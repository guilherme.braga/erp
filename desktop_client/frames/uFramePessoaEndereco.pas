unit uFramePessoaEndereco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, uGBDBTextEdit, cxDropDownEdit, cxBlobEdit, cxDBEdit,
  uGBDBBlobEdit, cxMaskEdit, uGBDBComboBox, cxTextEdit, uGBDBTextEditPK,
  Vcl.StdCtrls, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxButtonEdit, uGBDBButtonEditFK;

type
  TFramePessoaEndereco = class(TFrameDetailPadrao)
    lbCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    lbTipo: TLabel;
    cbTipo: TgbDBComboBox;
    edtObservacao: TgbDBBlobEdit;
    lbObservacao: TLabel;
    edtLogradouro: TgbDBTextEdit;
    lbComplemento: TLabel;
    lbLogradouro: TLabel;
    edtNumero: TgbDBTextEdit;
    lnNumero: TLabel;
    lbCEP: TLabel;
    edtIDCEP: TgbDBButtonEditFK;
    descCidade: TgbDBTextEdit;
    descBairro: TgbDBButtonEditFK;
    Label1: TLabel;
    edtCidade: TgbDBButtonEditFK;
    lbCidadeEmpresa: TLabel;
    edtComplemento: TgbDBTextEdit;
    procedure edtIDCEPgbDepoisDeConsultar(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure AoCriarFrame; override;
  end;

var
  FramePessoaEndereco: TFramePessoaEndereco;

implementation

{$R *.dfm}

uses uCadPessoa, uCEPProxy, uCEP;

{ TFramePessoaEndereco }

procedure TFramePessoaEndereco.AoCriarFrame;
begin
  edtIDCEP.NaoLimparCampoAposConsultaInvalida;
  descBairro.NaoLimparCampoAposConsultaInvalida;
end;

procedure TFramePessoaEndereco.edtIDCEPgbDepoisDeConsultar(Sender: TObject);
var
  CEP: TCEPProxy;
begin
  inherited;
  if (not dsDataFrame.Dataset.FieldByName('CEP').AsString.IsEmpty) then
  begin
    CEP := TCEP.GetLogradouroPeloCEP(dsDataFrame.Dataset.FieldByName('CEP').AsString);
    try
      dsDataFrame.Dataset.FieldByName('LOGRADOURO').AsString :=  CEP.FRua;
      dsDataFrame.Dataset.FieldByName('BAIRRO').AsString := CEP.FBairro;
      if CEP.FIdCidade > 0 then
      begin
        dsDataFrame.Dataset.FieldByName('ID_CIDADE').AsInteger := CEP.FIdCidade;
      end
      else
      begin
        dsDataFrame.Dataset.FieldByName('ID_CIDADE').Clear;
      end;
      dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CIDADE').AsString := CEP.FCidade;
    finally
      FreeAndNil(CEP);
    end;
  end;
end;

end.
