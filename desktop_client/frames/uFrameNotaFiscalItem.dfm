inherited FrameNotaFiscalItem: TFrameNotaFiscalItem
  inherited cxpcMain: TcxPageControl
    Top = 96
    Height = 209
    ExplicitTop = 96
    ExplicitHeight = 209
    ClientRectBottom = 209
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 185
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 185
        ExplicitHeight = 185
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 185
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 185
        Height = 185
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 1010
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Itens da Nota Fiscal'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 77
    Height = 77
    object Label6: TLabel [0]
      Left = 6
      Top = 7
      Width = 38
      Height = 13
      Caption = 'Produto'
    end
    object Label5: TLabel [1]
      Left = 514
      Top = 7
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label1: TLabel [2]
      Left = 342
      Top = 7
      Width = 14
      Height = 13
      Caption = 'UN'
    end
    object Label3: TLabel [3]
      Left = 389
      Top = 7
      Width = 39
      Height = 13
      Caption = 'Estoque'
    end
    object Label2: TLabel [4]
      Left = 774
      Top = 7
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label4: TLabel [5]
      Left = 264
      Top = 31
      Width = 45
      Height = 13
      Caption = 'Desconto'
    end
    object Label7: TLabel [6]
      Left = 6
      Top = 31
      Width = 48
      Height = 13
      Caption = 'Acr'#233'scimo'
    end
    object Label8: TLabel [7]
      Left = 774
      Top = 31
      Width = 60
      Height = 13
      Caption = 'Valor L'#237'quido'
    end
    object Label10: TLabel [8]
      Left = 6
      Top = 57
      Width = 26
      Height = 13
      Caption = 'Frete'
    end
    object Label11: TLabel [9]
      Left = 514
      Top = 31
      Width = 34
      Height = 13
      Caption = 'Seguro'
    end
    object Label12: TLabel [10]
      Left = 264
      Top = 57
      Width = 25
      Height = 13
      Caption = 'ICMS'
    end
    object Label13: TLabel [11]
      Left = 514
      Top = 57
      Width = 14
      Height = 13
      Caption = 'IPI'
    end
    object Label17: TLabel [12]
      Left = 774
      Top = 57
      Width = 51
      Height = 13
      Caption = 'Valor Total'
    end
    inherited gbPanel3: TgbPanel
      Left = 966
      Top = 51
      TabOrder = 14
      ExplicitLeft = 966
      ExplicitTop = 51
      inherited JvTransparentButton1: TJvTransparentButton
        Width = 34
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 34
        ExplicitHeight = 20
      end
      inherited JvTransparentButton2: TJvTransparentButton
        Left = 36
        Width = 34
        ExplicitLeft = 36
        ExplicitTop = 2
        ExplicitWidth = 34
      end
    end
    object CampoProduto: TgbDBButtonEditFK
      Left = 66
      Top = 3
      DataBinding.DataField = 'ID_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 0
      gbTextEdit = EdtDescricaoProduto
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 
        'ID_PRODUTO;JOIN_DESCRICAO_PRODUTO;JOIN_SIGLA_UNIDADE_ESTOQUE;JOI' +
        'N_ESTOQUE_PRODUTO'
      gbTableName = 'PRODUTO'
      gbCamposConsulta = 'ID;DESCRICAO;SIGLA;QT_ESTOQUE'
      gbDepoisDeConsultar = CampoProdutogbDepoisDeConsultar
      gbIdentificadorConsulta = 'PRODUTO'
      Width = 60
    end
    object EdtDescricaoProduto: TgbDBTextEdit
      Left = 123
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_DESCRICAO_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      gbReadyOnly = True
      gbPassword = False
      Width = 213
    end
    object EdtQuantidade: TgbDBCalcEdit
      Left = 573
      Top = 3
      DataBinding.DataField = 'I_QCOM'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      Style.TextStyle = [fsBold]
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      gbRequired = True
      Width = 120
    end
    object EdtUN: TgbDBTextEdit
      Left = 360
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 25
    end
    object EdtEstoque: TgbDBTextEdit
      Left = 430
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_ESTOQUE_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      gbReadyOnly = True
      gbPassword = False
      Width = 66
    end
    object EdtValorUnitario: TgbDBCalcEdit
      Left = 846
      Top = 3
      DataBinding.DataField = 'I_VUNCOM'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      Style.TextStyle = [fsBold]
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      gbRequired = True
      Width = 120
    end
    object EdtDesconto: TGBDBValorComPercentual
      Left = 315
      Top = 27
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      Transparent = True
      OnExit = EdtDescontoExit
      gbFieldValorBase = 'I_VUNCOM'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'I_VDESC'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_DESCONTO'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtAcrescimo: TGBDBValorComPercentual
      Left = 66
      Top = 27
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      Transparent = True
      gbFieldValorBase = 'I_VUNCOM'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'I_VOUTRO'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_ACRESCIMO'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtFrete: TGBDBValorComPercentual
      Left = 66
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 10
      Transparent = True
      gbFieldValorBase = 'VL_LIQUIDO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'I_VFRETE'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_FRETE'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtSeguro: TGBDBValorComPercentual
      Left = 573
      Top = 27
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      Transparent = True
      gbFieldValorBase = 'VL_LIQUIDO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'I_VSEG'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_SEGURO'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtICMS: TGBDBValorComPercentual
      Left = 315
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 11
      Transparent = True
      gbFieldValorBase = 'VL_LIQUIDO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'N_VICMS'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'N_PICMS'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtIPI: TGBDBValorComPercentual
      Left = 573
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 12
      Transparent = True
      OnExit = EdtIPIExit
      gbFieldValorBase = 'VL_LIQUIDO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'O_VIPI'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_IPI'
      gbDataSourcePercentual = dsDataFrame
      Height = 21
      Width = 191
    end
    object EdtValorLiquidoImposto: TgbDBCalcEdit
      Left = 846
      Top = 53
      TabStop = False
      DataBinding.DataField = 'VL_LIQUIDO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 13
      gbReadyOnly = True
      gbRequired = True
      Width = 120
    end
    object EdtValorLiquido: TgbDBCalcEdit
      Left = 846
      Top = 27
      TabStop = False
      DataBinding.DataField = 'I_VPROD'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 9
      gbReadyOnly = True
      gbRequired = True
      Width = 120
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = MovNotaFiscal.cdsNotaFiscalItem
  end
end
