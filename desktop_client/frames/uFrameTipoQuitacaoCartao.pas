unit uFrameTipoQuitacaoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox,
  cxCalc, cxDBEdit, uGBDBCalcEdit, uGBPanel, uGBDBTextEdit, cxButtonEdit,
  uGBDBButtonEditFK, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrameTipoQuitacaoCartao = class(TFramePadrao)
    labelDtVencimento: TLabel;
    edtDtVencimento: TgbDBDateEdit;
    Label1: TLabel;
    EdtDescricaoOperadoraCartao: TgbDBTextEdit;
    PnTituloCartao: TgbPanel;
    labelVlCheque: TLabel;
    EdtValor: TgbDBCalcEdit;
    EdtIdOperadoraCartao: TgbDBButtonEditFK;
    PnDados: TgbPanel;
    PnDtVencimento: TgbPanel;
    PnOperadoraCartao: TgbPanel;
    PnValor: TgbPanel;
  private
    { Private declarations }
  public
    procedure ValidarDados;
  protected
    procedure AoCriarFrame;
  end;

var
  FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao;

implementation

{$R *.dfm}

uses uDatasetUtils;

{ TFrameTipoQuitacaoCartao }

procedure TFrameTipoQuitacaoCartao.AoCriarFrame;
begin
  PnTituloCartao.Transparent := false;
end;

procedure TFrameTipoQuitacaoCartao.ValidarDados;
begin
  if Assigned(EdtIdOperadoraCartao.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(EdtIdOperadoraCartao.DataBinding.Field);
  end;
end;

end.
