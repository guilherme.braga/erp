inherited FrameTipoQuitacaoCrediario: TFrameTipoQuitacaoCrediario
  Width = 860
  Height = 49
  ExplicitWidth = 860
  ExplicitHeight = 49
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 860
    ExplicitHeight = 49
    Height = 49
    Width = 860
    object PnTituloCartao: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      Alignment = alCenterCenter
      Caption = 'Dados do Credi'#225'rio'
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentBackground = False
      ParentFont = False
      Style.BorderStyle = ebsOffice11
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 19
      Width = 856
    end
    object PnCampos: TgbPanel
      Left = 2
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 26
      Width = 856
      object PnHistorico: TgbPanel
        Left = 167
        Top = 2
        Align = alClient
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        DesignSize = (
          529
          22)
        Height = 22
        Width = 529
        object Label1: TLabel
          Left = 2
          Top = 5
          Width = 41
          Height = 13
          Caption = 'Hist'#243'rico'
        end
        object EdtHistorico: TgbDBTextEdit
          Left = 48
          Top = 1
          Anchors = [akLeft, akTop, akRight]
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbPassword = False
          Width = 481
        end
      end
      object pnDtVencimento: TgbPanel
        Left = 2
        Top = 2
        Align = alLeft
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Transparent = True
        Height = 22
        Width = 165
        object labelDtVencimento: TLabel
          Left = 1
          Top = 5
          Width = 73
          Height = 13
          Caption = 'Dt. Vencimento'
        end
        object edtDtVencimento: TgbDBDateEdit
          Left = 77
          Top = 1
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbDateTime = False
          Width = 83
        end
      end
      object PnValor: TgbPanel
        Left = 696
        Top = 2
        Align = alRight
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        Transparent = True
        DesignSize = (
          158
          22)
        Height = 22
        Width = 158
        object labelVlCheque: TLabel
          Left = 4
          Top = 5
          Width = 24
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Valor'
        end
        object EdtValor: TgbDBCalcEdit
          Left = 31
          Top = 1
          Anchors = [akTop, akRight]
          Properties.DisplayFormat = '###,###,###,###,###,##0.00'
          Properties.ImmediatePost = True
          Properties.UseThousandSeparator = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 127
        end
      end
    end
  end
end
