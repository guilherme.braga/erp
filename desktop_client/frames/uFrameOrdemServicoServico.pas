unit uFrameOrdemServicoServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, Datasnap.DBClient,
  uGBClientDataset, Vcl.Menus, dxBar, dxBarExtItems, cxClasses, System.Actions,
  Vcl.ActnList, JvExControls, JvButton, JvTransparentButton, uGBPanel, dxBevel,
  cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  uGBDBValorComPercentual, cxDropDownEdit, cxCalc, cxDBEdit, uGBDBCalcEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls, dxRibbonRadialMenu;

type
  TFrameOrdemServicoServico = class(TFrameDetailTransientePadrao)
    Label7: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    CampoServico: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    EdtQuantidade: TgbDBCalcEdit;
    EdtValorUnitario: TgbDBCalcEdit;
    EdtValorBruto: TgbDBCalcEdit;
    EdtValorLiquido: TgbDBCalcEdit;
    EdtDesconto: TGBDBValorComPercentual;
    EdtAcrescimo: TGBDBValorComPercentual;
    procedure CalcularValorBruto(Sender: TObject);
    procedure CalcularValorLiquido(Sender: TObject);
    procedure EdtAcrescimoExit(Sender: TObject);
    procedure CampoServicogbDepoisDeConsultar(Sender: TObject);
  private
    FServicoTerceiro: String;
  public
    procedure AntesDeConfirmar; override;
    procedure SetConfiguracoesIniciaisServico(ADataset: TgbClientDataset; AServicoTerceiro: Boolean);
  protected
    procedure AoCriarFrame; override;
  end;

var
  FrameOrdemServicoServico: TFrameOrdemServicoServico;

implementation

{$R *.dfm}

uses uMovOrdemServico, uConstantes, uOrdemServico, uFiltroFormulario,
  uProdutoProxy, uSistema, uTControl_Function, uFrameTabelaPrecoItem, uServicoProxy,
  uServico;

{ TFrameOrdemServicoServico }

procedure TFrameOrdemServicoServico.AntesDeConfirmar;
var
  idOrdemServico: Integer;
begin
  inherited;

  idOrdemServico := (TControlFunction.GetInstance('TMovOrdemServico') as TMovOrdemServico).cdsDataID.AsInteger;

  if idOrdemServico > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_ORDEM_SERVICO', idOrdemServico);
  end;

  fdmDatasetTransiente.SetAsString('BO_SERVICO_TERCEIRO', FServicoTerceiro);
end;

procedure TFrameOrdemServicoServico.AoCriarFrame;
begin
  inherited;
end;

procedure TFrameOrdemServicoServico.CalcularValorBruto(Sender: TObject);
begin
  TOrdemServico.CalcularValorBrutoProduto(fdmDatasetTransiente);
end;

procedure TFrameOrdemServicoServico.CalcularValorLiquido(Sender: TObject);
begin
  TOrdemServico.CalcularValorLiquidoProduto(fdmDatasetTransiente);
end;

procedure TFrameOrdemServicoServico.CampoServicogbDepoisDeConsultar(
  Sender: TObject);
var
  servico: TServicoProxy;
begin
  inherited;
  if (fdmDatasetTransiente.GetAsInteger('ID_SERVICO') > 0) then
  begin
    servico := TServico.GetServico(
      fdmDatasetTransiente.GetAsInteger('ID_SERVICO'));

    fdmDatasetTransiente.SetAsInteger('ID_SERVICO', servico.FID);
    fdmDatasetTransiente.SetAsString('JOIN_DESCRICAO_SERVICO', servico.FDescricao);
    fdmDatasetTransiente.SetAsString('JOIN_DURACAO_SERVICO', servico.FDuracao);
    fdmDatasetTransiente.SetAsFloat('VL_CUSTO', servico.FVlCusto);
    fdmDatasetTransiente.SetAsFloat('VL_UNITARIO', servico.FVlVenda);
    fdmDatasetTransiente.SetAsString('BO_SERVICO_TERCEIRO', servico.FBoServicoTerceiro);
  end;
end;

procedure TFrameOrdemServicoServico.EdtAcrescimoExit(Sender: TObject);
begin
  inherited;
  ActConfirm.Execute;
end;

procedure TFrameOrdemServicoServico.SetConfiguracoesIniciaisServico(
  ADataset: TgbClientDataset; AServicoTerceiro: Boolean);
begin
  SetConfiguracoesIniciais(ADataset);
  if AServicoTerceiro then
  begin
    FServicoTerceiro := TConstantes.BO_SIM;
  end
  else
  begin
    FServicoTerceiro := TConstantes.BO_NAO;
  end;

  CampoServico.FWhereInjetado := TFiltroPadrao.Create;
  CampoServico.FWhereInjetado.AddIgual(TServicoProxy.FIELD_BO_SERVICO_TERCEIRO,
    FServicoTerceiro);

  Level1BandedTableView1.DataController.Filter.Root.AddItem(
    Level1BandedTableView1.GetColumnByFieldName('BO_SERVICO_TERCEIRO'),
    foEqual,FServicoTerceiro, FServicoTerceiro);
end;

end.
