inherited FrameRegraImpostoFiltro: TFrameRegraImpostoFiltro
  Width = 936
  inherited cxpcMain: TcxPageControl
    Width = 936
    Properties.ActivePage = cxtsData
    ClientRectRight = 936
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 936
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        Width = 936
        inherited dxBevel1: TdxBevel
          Width = 932
          ExplicitLeft = 3
          ExplicitTop = -9
          ExplicitWidth = 949
        end
        object Label7: TLabel
          Left = 489
          Top = 8
          Width = 58
          Height = 13
          Caption = 'UF Emitente'
        end
        object Label8: TLabel
          Left = 625
          Top = 8
          Width = 74
          Height = 13
          Caption = 'UF Destinat'#225'rio'
        end
        object gbDadosTransiente: TgbPanel
          Left = 2
          Top = 34
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          Transparent = True
          ExplicitWidth = 447
          Height = 204
          Width = 932
          object Label1: TLabel
            Left = 14
            Top = 304
            Width = 120
            Height = 13
            Caption = 'Zoneamento do Emitente'
            Visible = False
          end
          object Label2: TLabel
            Left = 431
            Top = 304
            Width = 136
            Height = 13
            Caption = 'Zoneamento do Destinat'#225'rio'
            Visible = False
          end
          object Label3: TLabel
            Left = 14
            Top = 327
            Width = 136
            Height = 13
            Caption = 'Regime Especial do Emitente'
            Visible = False
          end
          object Label4: TLabel
            Left = 431
            Top = 327
            Width = 152
            Height = 13
            Caption = 'Regime Especial do Destinat'#225'rio'
            Visible = False
          end
          object Label5: TLabel
            Left = 14
            Top = 348
            Width = 82
            Height = 13
            Caption = 'Situa'#231#227'o Especial'
            Visible = False
          end
          object Label6: TLabel
            Left = 14
            Top = 6
            Width = 76
            Height = 13
            Caption = 'Opera'#231#227'o Fiscal'
          end
          object Label9: TLabel
            Left = 14
            Top = 29
            Width = 67
            Height = 13
            Caption = 'Imposto ICMS'
          end
          object Label10: TLabel
            Left = 312
            Top = 29
            Width = 56
            Height = 13
            Caption = 'Imposto IPI'
          end
          object Label11: TLabel
            Left = 563
            Top = 29
            Width = 99
            Height = 13
            Caption = 'Imposto PIS COFINS'
          end
          object gbDBRadioGroup1: TgbDBRadioGroup
            Left = 14
            Top = 123
            Caption = 'Tipo da Empresa do Destinat'#225'rio'
            DataBinding.DataField = 'TIPO_EMPRESA_PESSOA'
            DataBinding.DataSource = dsDataFrame
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
              end
              item
                Caption = 'Empresa Privada'
                Value = 'PRIVADA'
              end
              item
                Caption = 'Empresa P'#250'blica'
                Value = 'PUBLICA'
              end
              item
                Caption = 'Empresa Mista'
                Value = 'MISTA'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 12
            Visible = False
            Height = 95
            Width = 249
          end
          object gbDBRadioGroup2: TgbDBRadioGroup
            Left = 14
            Top = 224
            Caption = 'Forma de Arquisi'#231#227'o da Mercadoria'
            DataBinding.DataField = 'FORMA_AQUISICAO'
            DataBinding.DataSource = dsDataFrame
            Properties.Items = <
              item
                Caption = 'Produzida'
                Value = 'PRODUZIDA'
              end
              item
                Caption = 'Importada'
                Value = 'IMPORTADA'
              end
              item
                Caption = 'Adquirida de Terceiros'
                Value = 'ADQUIRIDA'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 15
            Visible = False
            Height = 72
            Width = 249
          end
          object gbDBRadioGroup3: TgbDBRadioGroup
            Left = 269
            Top = 205
            Caption = 'Origem da Mercadoria'
            DataBinding.DataField = 'ORIGEM_DA_MERCADORIA'
            DataBinding.DataSource = dsDataFrame
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
              end
              item
                Caption = 'Nacional'
                Value = '0'
              end
              item
                Caption = 'Importada - Diretamente'
                Value = '1'
              end
              item
                Caption = 'Importada - Adquirida no mercado interno'
                Value = '2'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 16
            Visible = False
            Height = 91
            Width = 274
          end
          object gbDBRadioGroup4: TgbDBRadioGroup
            Left = 269
            Top = 123
            Caption = 'Regime Tribut'#225'rio do Destinat'#225'rio'
            DataBinding.DataField = 'REGIME_TRIBUTARIO_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.Columns = 3
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
              end
              item
                Caption = 'Simples Nacional'
                Value = '1'
              end
              item
                Caption = 'Peri'#243'dico de Apura'#231#227'o'
                Value = '0'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 13
            Visible = False
            Height = 34
            Width = 582
          end
          object gbDBRadioGroup6: TgbDBRadioGroup
            Left = 549
            Top = 205
            Caption = 'C'#243'digo do Regime Tribut'#225'rio do Emitente'
            DataBinding.DataField = 'CRT_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
              end
              item
                Caption = 'Simples Nacional'
                Value = '1'
              end
              item
                Caption = 'Simples Nacional - excesso de sublimite de receita bruta'
                Value = '2'
              end
              item
                Caption = 'Regime Normal'
                Value = '3'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 17
            Visible = False
            Height = 91
            Width = 302
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 211
            Top = 300
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 19
            Visible = False
            gbReadyOnly = True
            gbPassword = False
            Width = 213
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 154
            Top = 300
            DataBinding.DataField = 'ID_ZONEAMENTO_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 18
            Visible = False
            gbTextEdit = gbDBTextEdit1
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_ZONEAMENTO;JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
            gbTableName = 'ZONEAMENTO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'ZONEAMENTO'
            Width = 60
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 645
            Top = 300
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 21
            Visible = False
            gbReadyOnly = True
            gbPassword = False
            Width = 206
          end
          object gbDBButtonEditFK2: TgbDBButtonEditFK
            Left = 588
            Top = 300
            DataBinding.DataField = 'ID_ZONEAMENTO_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 20
            Visible = False
            gbTextEdit = gbDBTextEdit2
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_ZONEAMENTO;JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
            gbTableName = 'ZONEAMENTO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'ZONEAMENTO'
            Width = 60
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 211
            Top = 323
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 23
            Visible = False
            gbReadyOnly = True
            gbPassword = False
            Width = 213
          end
          object gbDBButtonEditFK3: TgbDBButtonEditFK
            Left = 154
            Top = 323
            DataBinding.DataField = 'ID_REGIME_ESPECIAL_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 22
            Visible = False
            gbTextEdit = gbDBTextEdit3
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_REGIME_ESPECIAL;JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
            gbTableName = 'REGIME_ESPECIAL'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'REGIME_ESPECIAL'
            Width = 60
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 645
            Top = 323
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 25
            Visible = False
            gbReadyOnly = True
            gbPassword = False
            Width = 206
          end
          object gbDBButtonEditFK4: TgbDBButtonEditFK
            Left = 588
            Top = 323
            DataBinding.DataField = 'ID_REGIME_ESPECIAL_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 24
            Visible = False
            gbTextEdit = gbDBTextEdit4
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_REGIME_ESPECIAL;JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
            gbTableName = 'REGIME_ESPECIAL'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'REGIME_ESPECIAL'
            Width = 60
          end
          object gbDBTextEdit5: TgbDBTextEdit
            Left = 211
            Top = 346
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_SITUACAO_ESPECIAL'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 27
            Visible = False
            gbReadyOnly = True
            gbPassword = False
            Width = 640
          end
          object gbDBButtonEditFK5: TgbDBButtonEditFK
            Left = 154
            Top = 346
            DataBinding.DataField = 'ID_SITUACAO_ESPECIAL'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 26
            Visible = False
            gbTextEdit = gbDBTextEdit5
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_SITUACAO_ESPECIAL;JOIN_DESCRICAO_SITUACAO_ESPECIAL'
            gbTableName = 'SITUACAO_ESPECIAL'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'SITUACAO_ESPECIAL'
            Width = 60
          end
          object gbDBButtonEditFK6: TgbDBButtonEditFK
            Left = 120
            Top = 2
            DataBinding.DataField = 'ID_OPERACAO_FISCAL'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 0
            gbTextEdit = gbDBTextEdit6
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_OPERACAO_FISCAL;JOIN_DESCRICAO_OPERACAO_FISCAL'
            gbTableName = 'OPERACAO_FISCAL'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'OPERACAO_FISCAL'
            Width = 60
          end
          object gbDBTextEdit6: TgbDBTextEdit
            Left = 177
            Top = 2
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            Width = 674
          end
          object gbDBButtonEditFK9: TgbDBButtonEditFK
            Left = 120
            Top = 25
            DataBinding.DataField = 'ID_IMPOSTO_ICMS'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 2
            gbTextEdit = gbDBTextEdit7
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_IMPOSTO_ICMS;JOIN_CST_CSOSN'
            gbTableName = 'IMPOSTO_ICMS'
            gbCamposConsulta = 'ID;CODIGO_CST_CSOSN'
            gbClasseDoCadastro = 'TCadImpostoICMS'
            gbIdentificadorConsulta = 'IMPOSTO_ICMS'
            Width = 60
          end
          object gbDBTextEdit7: TgbDBTextEdit
            Left = 177
            Top = 25
            TabStop = False
            DataBinding.DataField = 'JOIN_CST_CSOSN'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 130
          end
          object gbDBTextEdit8: TgbDBTextEdit
            Left = 429
            Top = 25
            TabStop = False
            DataBinding.DataField = 'JOIN_CST_IPI'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 130
          end
          object gbDBTextEdit9: TgbDBTextEdit
            Left = 721
            Top = 25
            TabStop = False
            DataBinding.DataField = 'JOIN_CST_PIS_COFINS'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbPassword = False
            Width = 130
          end
          object EdtEmitenteContribuinteICMS: TgbDBRadioGroup
            Left = 14
            Top = 50
            Caption = 'Emitente '#233' contribuinte de ICMS'
            DataBinding.DataField = 'BO_CONTRIBUINTE_ICMS_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
                Value = 'I'
              end
              item
                Caption = 'Contribuinte'
                Value = 'S'
              end
              item
                Caption = 'N'#227'o Contribuinte'
                Value = 'N'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            Height = 68
            Width = 205
          end
          object gbDBRadioGroup5: TgbDBRadioGroup
            Left = 269
            Top = 165
            Caption = 'Regime Tribut'#225'rio do Destinat'#225'rio'
            DataBinding.DataField = 'REGIME_TRIBUTARIO_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.Columns = 3
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
              end
              item
                Caption = 'Simples Nacional'
                Value = '1'
              end
              item
                Caption = 'Peri'#243'dico de Apura'#231#227'o'
                Value = '0'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 14
            Visible = False
            Height = 35
            Width = 582
          end
          object EdtDestinatarioContribuinteICMS: TgbDBRadioGroup
            Left = 224
            Top = 50
            Caption = 'Destinat'#225'rio '#233' contribuinte de ICMS'
            DataBinding.DataField = 'BO_CONTRIBUINTE_ICMS_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
                Value = 'I'
              end
              item
                Caption = 'Contribuinte'
                Value = 'S'
              end
              item
                Caption = 'N'#227'o Contribuinte'
                Value = 'N'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            Height = 68
            Width = 205
          end
          object EdtEmitenteContribuinteIPI: TgbDBRadioGroup
            Left = 435
            Top = 50
            Caption = 'Emitente '#233' contribuinte de IPI'
            DataBinding.DataField = 'BO_CONTRIBUINTE_IPI_EMITENTE'
            DataBinding.DataSource = dsDataFrame
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
                Value = 'I'
              end
              item
                Caption = 'Contribuinte'
                Value = 'S'
              end
              item
                Caption = 'N'#227'o Contribuinte'
                Value = 'N'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            Height = 68
            Width = 205
          end
          object EdtDestinatarioContribuinteIPI: TgbDBRadioGroup
            Left = 646
            Top = 50
            Caption = 'Destinat'#225'rio '#233' contribuinte de IPI'
            DataBinding.DataField = 'BO_CONTRIBUINTE_IPI_DESTINATARIO'
            DataBinding.DataSource = dsDataFrame
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'N'#227'o Informado'
                Value = 'I'
              end
              item
                Caption = 'Contribuinte'
                Value = 'S'
              end
              item
                Caption = 'N'#227'o Contribuinte'
                Value = 'N'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 11
            Height = 68
            Width = 204
          end
          object gbDBButtonEditFK10: TgbDBButtonEditFK
            Left = 372
            Top = 25
            DataBinding.DataField = 'ID_IMPOSTO_IPI'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 4
            gbTextEdit = gbDBTextEdit8
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_IMPOSTO_IPI;JOIN_CST_IPI'
            gbTableName = 'IMPOSTO_IPI'
            gbCamposConsulta = 'ID;CODIGO_CST_IPI'
            gbClasseDoCadastro = 'TCadImpostoIPI'
            gbIdentificadorConsulta = 'IMPOSTO_IPI'
            Width = 60
          end
          object gbDBButtonEditFK11: TgbDBButtonEditFK
            Left = 664
            Top = 25
            DataBinding.DataField = 'ID_IMPOSTO_PIS_COFINS'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 6
            gbTextEdit = gbDBTextEdit8
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_IMPOSTO_PIS_COFINS;JOIN_CST_PIS_COFINS'
            gbTableName = 'IMPOSTO_PIS_COFINS'
            gbCamposConsulta = 'ID;CODIGO_CST_PIS_COFINS'
            gbClasseDoCadastro = 'TCadImpostoPisCofins'
            gbIdentificadorConsulta = 'IMPOSTO_PIS_COFINS'
            Width = 60
          end
        end
        object gbDBButtonEditFK7: TgbDBButtonEditFK
          Left = 551
          Top = 4
          DataBinding.DataField = 'JOIN_UF_ESTADO_ORIGEM'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Style.Color = clWhite
          TabOrder = 2
          gbTextEdit = gbDBTextEdit3
          gbCampoPK = 'UF'
          gbCamposRetorno = 'ID_ESTADO_ORIGEM;JOIN_UF_ESTADO_ORIGEM'
          gbTableName = 'ESTADO'
          gbCamposConsulta = 'ID;UF'
          gbIdentificadorConsulta = 'ESTADO'
          Width = 68
        end
        object gbDBButtonEditFK8: TgbDBButtonEditFK
          Left = 703
          Top = 4
          DataBinding.DataField = 'JOIN_UF_ESTADO_DESTINO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Style.Color = clWhite
          TabOrder = 3
          gbTextEdit = gbDBTextEdit3
          gbCampoPK = 'UF'
          gbCamposRetorno = 'ID_ESTADO_DESTINO;JOIN_UF_ESTADO_DESTINO'
          gbTableName = 'ESTADO'
          gbCamposConsulta = 'ID;UF'
          gbIdentificadorConsulta = 'ESTADO'
          Width = 63
        end
        object gbDBDateEdit1: TgbDBDateEdit
          Left = 122
          Top = 4
          DataBinding.DataField = 'DH_INICIO_VIGENCIA'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnNow]
          Properties.ImmediatePost = True
          Properties.Kind = ckDateTime
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbRequired = True
          gbDateTime = True
          Width = 130
        end
        object gbDBDateEdit2: TgbDBDateEdit
          Left = 355
          Top = 4
          DataBinding.DataField = 'DH_FIM_VIGENCIA'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnNow]
          Properties.ImmediatePost = True
          Properties.Kind = ckDateTime
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbDateTime = True
          Width = 130
        end
        object cxLabel1: TcxLabel
          Left = 14
          Top = 6
          Caption = 'Dt. '#205'nicio da Vig'#234'ncia'
          Style.BorderStyle = ebsNone
          Transparent = True
        end
        object cxLabel2: TcxLabel
          Left = 256
          Top = 6
          Caption = 'Dt. Fim da Vig'#234'ncia'
          Style.BorderStyle = ebsNone
          Transparent = True
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Configura'#231#245'es de Regras de Impostos'
    Style.IsFontAssigned = True
    TabOrder = 4
    Width = 936
  end
  inherited ActionListMain: TActionList
    Left = 191
    Top = 40
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 162
    Top = 40
    DockControlHeights = (
      0
      0
      22
      0)
    inherited barNavegador: TdxBar
      DockedLeft = 328
    end
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadRegraImposto.cdsRegraImpostoFiltro
    Left = 134
    Top = 40
  end
  inherited ActionListAssistent: TActionList
    Left = 52
    Top = 24
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 24
    Top = 24
  end
end
