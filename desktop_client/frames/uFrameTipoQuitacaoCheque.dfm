inherited FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque
  Width = 730
  Height = 104
  ExplicitWidth = 730
  ExplicitHeight = 104
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 730
    ExplicitHeight = 104
    Height = 104
    Width = 730
    object PnTituloCheque: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      Alignment = alCenterCenter
      Caption = 'Dados do Cheque'
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentBackground = False
      ParentFont = False
      Style.BorderStyle = ebsOffice11
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 19
      Width = 726
    end
    object PnDados: TgbPanel
      Left = 2
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 81
      Width = 726
      object PnDadosDireita: TgbPanel
        Left = 522
        Top = 2
        Align = alRight
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        DesignSize = (
          202
          77)
        Height = 77
        Width = 202
        object labelDocFederal: TLabel
          Left = 6
          Top = 9
          Width = 61
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Doc. Federal'
        end
        object labelDtVencimento: TLabel
          Left = 6
          Top = 33
          Width = 73
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Dt. Vencimento'
        end
        object labelVlCheque: TLabel
          Left = 6
          Top = 57
          Width = 67
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Vl. do Cheque'
        end
        object edtDocFederal: TgbDBTextEdit
          Left = 81
          Top = 5
          Anchors = [akTop, akRight]
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          OnExit = edtDocFederalExit
          gbPassword = False
          Width = 121
        end
        object edtDtVencimento: TgbDBDateEdit
          Left = 81
          Top = 30
          Anchors = [akTop, akRight]
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbDateTime = False
          Width = 121
        end
        object EdtValor: TgbDBCalcEdit
          Left = 81
          Top = 54
          Anchors = [akTop, akRight]
          Properties.DisplayFormat = '###,###,###,###,###,##0.00'
          Properties.ImmediatePost = True
          Properties.UseThousandSeparator = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Width = 121
        end
      end
      object PnDadosEsquerda: TgbPanel
        Left = 2
        Top = 2
        Align = alClient
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Transparent = True
        Height = 77
        Width = 520
        object PnTopo3PainelEsquerda: TgbPanel
          Left = 2
          Top = 51
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Height = 25
          Width = 516
          object PnTopo3PainelEsquerdaPainel2: TgbPanel
            Left = 371
            Top = 2
            Align = alRight
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              143
              21)
            Height = 21
            Width = 143
            object labelNumero: TLabel
              Left = 6
              Top = 4
              Width = 37
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'N'#250'mero'
            end
            object edtNumero: TgbDBTextEdit
              Left = 64
              Top = 1
              Anchors = [akTop, akRight]
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbPassword = False
              Width = 79
            end
          end
          object PnTopo3PainelEsquerdaPainel1: TgbPanel
            Left = 2
            Top = 2
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            DesignSize = (
              369
              21)
            Height = 21
            Width = 369
            object labelAgencia: TLabel
              Left = 1
              Top = 4
              Width = 38
              Height = 13
              Caption = 'Ag'#234'ncia'
            end
            object labelContaCorrente: TLabel
              Left = 119
              Top = 4
              Width = 75
              Height = 13
              Caption = 'Conta Corrente'
            end
            object edtAgencia: TgbDBTextEdit
              Left = 42
              Top = 1
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbPassword = False
              Width = 73
            end
            object edtContaCorrente: TgbDBTextEdit
              Left = 198
              Top = 1
              Anchors = [akLeft, akTop, akRight]
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbPassword = False
              Width = 168
            end
          end
        end
        object PnTopo1PainelEsquerda: TgbPanel
          Left = 2
          Top = 2
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 26
          Width = 516
          object PnConsultaCheque: TgbPanel
            Left = 2
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            Height = 22
            Width = 116
            object lbConsultaCheque: TLabel
              Left = 0
              Top = 5
              Width = 37
              Height = 13
              Caption = 'Cheque'
            end
            object EdtConsultaCheque: TgbDBButtonEditFK
              Left = 42
              Top = 1
              DataBinding.DataField = 'ID_CHEQUE'
              DataBinding.DataSource = dsChequeTerceiro
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 112
              Style.Color = clWhite
              TabOrder = 0
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID'
              gbTableName = 'CHEQUE'
              gbCamposConsulta = 'ID'
              gbIdentificadorConsulta = 'CHEQUE'
              gbDesligarConsultaPadrao = True
              Width = 73
            end
          end
          object PnSacado: TgbPanel
            Left = 118
            Top = 2
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              396
              22)
            Height = 22
            Width = 396
            object labelSacado: TLabel
              Left = 2
              Top = 5
              Width = 35
              Height = 13
              Caption = 'Sacado'
            end
            object edtSacado: TgbDBTextEdit
              Left = 43
              Top = 1
              Anchors = [akLeft, akTop, akRight]
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbPassword = False
              Width = 353
            end
          end
        end
        object PnTopo2PainelEsquerda: TgbPanel
          Left = 2
          Top = 28
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          DesignSize = (
            516
            23)
          Height = 23
          Width = 516
          object labelBanco: TLabel
            Left = 3
            Top = 5
            Width = 29
            Height = 13
            Caption = 'Banco'
          end
          object labelDtEmissao: TLabel
            Left = 377
            Top = 5
            Width = 56
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Dt. Emiss'#227'o'
          end
          object edtBanco: TgbDBTextEdit
            Left = 44
            Top = 2
            Anchors = [akLeft, akTop, akRight]
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbPassword = False
            Width = 324
          end
          object edtDtEmissao: TgbDBDateEdit
            Left = 435
            Top = 2
            Anchors = [akTop, akRight]
            Properties.DateButtons = [btnClear, btnToday]
            Properties.ImmediatePost = True
            Properties.SaveTime = False
            Properties.ShowTime = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbDateTime = False
            ExplicitLeft = 433
            Width = 79
          end
        end
      end
    end
  end
  object fdmChequeTerceiro: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 352
    Top = 40
    object fdmChequeTerceiroID_CHEQUE: TIntegerField
      FieldName = 'ID_CHEQUE'
      OnChange = fdmChequeTerceiroID_CHEQUEChange
    end
  end
  object dsChequeTerceiro: TDataSource
    DataSet = fdmChequeTerceiro
    Left = 384
    Top = 40
  end
end
