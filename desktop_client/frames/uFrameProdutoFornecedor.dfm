inherited FrameProdutoFornecedor: TFrameProdutoFornecedor
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        inherited Level1BandedTableView1: TcxGridDBBandedTableView
          object Level1BandedTableView1ID: TcxGridDBBandedColumn
            DataBinding.FieldName = 'ID'
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object Level1BandedTableView1REFERENCIA: TcxGridDBBandedColumn
            DataBinding.FieldName = 'REFERENCIA'
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object Level1BandedTableView1ID_PRODUTO: TcxGridDBBandedColumn
            DataBinding.FieldName = 'ID_PRODUTO'
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
        end
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 708
        end
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object Label1: TLabel
          Left = 8
          Top = 64
          Width = 52
          Height = 13
          Caption = 'Refer'#234'ncia'
        end
        object Label2: TLabel
          Left = 8
          Top = 38
          Width = 55
          Height = 13
          Caption = 'Fornecedor'
        end
        object gbDBTextEditPK1: TgbDBTextEditPK
          Left = 72
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object gbDBTextEdit1: TgbDBTextEdit
          Left = 72
          Top = 61
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'REFERENCIA'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbRequired = True
          gbPassword = False
          Width = 373
        end
        object gbDBButtonEditFK1: TgbDBButtonEditFK
          Left = 72
          Top = 34
          DataBinding.DataField = 'ID_PESSOA'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 1
          gbTextEdit = gbDBTextEdit2
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_FORNECEDOR'
          gbTableName = 'PESSOA'
          gbCamposConsulta = 'ID;NOME'
          gbIdentificadorConsulta = 'PESSOA'
          Width = 60
        end
        object gbDBTextEdit2: TgbDBTextEdit
          Left = 129
          Top = 34
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_NOME_FORNECEDOR'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbReadyOnly = True
          gbPassword = False
          Width = 316
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Refer'#234'ncias nos Fornecedores'
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadProduto.cdsProdutoFornecedor
  end
end
