unit uFrameGradeProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxDBLookupComboBox, cxBlobEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid,
  Datasnap.DBClient, uGBClientDataset, cxGroupBox;

type
  TFrameGradeProduto = class(TFramePadrao)
    cdsListaCor: TGBClientDataSet;
    dsListaCor: TDataSource;
    cdsListaTamanho: TGBClientDataSet;
    dsListaTamanho: TDataSource;
    dsTamanhoCor: TDataSource;
    dsCor: TDataSource;
    gridTamanho: TcxGrid;
    viewTamanho: TcxGridDBBandedTableView;
    viewTamanhoTAMANHO: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    gridCor: TcxGrid;
    viewCor: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    cdsListaTamanhoID: TAutoIncField;
    cdsListaTamanhoDESCRICAO: TStringField;
    cdsListaTamanhoBO_ATIVO: TStringField;
    cdsListaCorID: TAutoIncField;
    cdsListaCorDESCRICAO: TStringField;
    cdsListaCorBO_ATIVO: TStringField;
    cdsListaCorRGB: TStringField;
    cdsListaCorCMYK: TStringField;
    procedure viewTamanhosTAMANHOPropertiesEditValueChanged(Sender: TObject);
    procedure viewCorsEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure viewTamanhoEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
  private
  public
    procedure SetDatasets(ADatasetCor, ADatasetTamanhoCor: TDataset);
    procedure CarregarDadosCor;
    procedure CarregarDadosTamanhoCor;
  protected
    procedure AoCriarFrame; override;
  end;

var
  FrameGradeProduto: TFrameGradeProduto;

implementation

{$R *.dfm}

uses uConsultaGrid, uDmConnection, uCor, uCadGradeProduto;

{ TFramePadrao1 }

procedure TFrameGradeProduto.AoCriarFrame;
begin
  inherited;
  CarregarDadosCor;
  CarregarDadosTamanhoCor;
end;

procedure TFrameGradeProduto.CarregarDadosTamanhoCor;
begin
  cdsListaTamanho.Fechar;
  cdsListaTamanho.Abrir;
end;

procedure TFrameGradeProduto.CarregarDadosCor;
begin
  cdsListaCor.Fechar;
  cdsListaCor.Abrir;
end;

procedure TFrameGradeProduto.SetDatasets(ADatasetCor, ADatasetTamanhoCor: TDataset);
begin
  dsCor.Dataset := ADatasetCor;
  dsTamanhoCor.Dataset := ADatasetTamanhoCor;
end;

procedure TFrameGradeProduto.viewTamanhoEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if (dsTamanhoCor.Dataset.State in [dsEdit]) and
      (not dsTamanhoCor.Dataset.FieldByName('JOIN_DESCRICAO_TAMANHO').AsString.IsEmpty) then
    begin
      TgbClientDataset(dsTamanhoCor.Dataset).Post;
    end;

    keybd_event(VK_INSERT, 0, 0, 0);
  end;
end;

procedure TFrameGradeProduto.viewTamanhosTAMANHOPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if not(TgbClientDataset(dsTamanhoCor.Dataset).FieldByName('JOIN_DESCRICAO_TAMANHO').AsString.IsEmpty) and
    (TgbClientDataset(dsTamanhoCor.Dataset).State in [dsInsert]) then
  begin
    TgbClientDataset(dsTamanhoCor.Dataset).Post;
  end;
end;

procedure TFrameGradeProduto.viewCorsEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if (dsCor.Dataset.State in [dsEdit]) and
      (not dsCor.Dataset.FieldByName('JOIN_DESCRICAO_COR').AsString.IsEmpty) then
    begin
      TgbClientDataset(dsCor.Dataset).Post;
    end;

    keybd_event(VK_INSERT, 0, 0, 0);
  end;
end;


end.
