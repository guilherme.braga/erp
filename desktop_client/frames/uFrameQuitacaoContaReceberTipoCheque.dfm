inherited FrameQuitacaoContaReceberTipoCheque: TFrameQuitacaoContaReceberTipoCheque
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloCheque: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      inherited PnDadosDireita: TgbPanel
        inherited edtDocFederal: TgbDBTextEdit
          DataBinding.DataField = 'CHEQUE_DOC_FEDERAL'
          DataBinding.DataSource = dsDataFrame
        end
        inherited edtDtVencimento: TgbDBDateEdit
          TabStop = False
          DataBinding.DataField = 'CHEQUE_DT_VENCIMENTO'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.Color = clGradientActiveCaption
          gbReadyOnly = True
        end
        inherited EdtValor: TgbDBCalcEdit
          TabStop = False
          DataBinding.DataField = 'IC_VL_PAGAMENTO'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.Color = clGradientActiveCaption
          gbReadyOnly = True
        end
      end
      inherited PnDadosEsquerda: TgbPanel
        inherited PnTopo3PainelEsquerda: TgbPanel
          inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
            inherited edtNumero: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_NUMERO'
              DataBinding.DataSource = dsDataFrame
            end
          end
          inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
            inherited edtAgencia: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_AGENCIA'
              DataBinding.DataSource = dsDataFrame
            end
            inherited edtContaCorrente: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_CONTA_CORRENTE'
              DataBinding.DataSource = dsDataFrame
            end
          end
        end
        inherited PnTopo1PainelEsquerda: TgbPanel
          inherited PnSacado: TgbPanel
            inherited edtSacado: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_SACADO'
              DataBinding.DataSource = dsDataFrame
            end
          end
        end
        inherited PnTopo2PainelEsquerda: TgbPanel
          inherited edtBanco: TgbDBTextEdit
            DataBinding.DataField = 'CHEQUE_BANCO'
            DataBinding.DataSource = dsDataFrame
          end
          inherited edtDtEmissao: TgbDBDateEdit
            DataBinding.DataField = 'CHEQUE_DT_EMISSAO'
            DataBinding.DataSource = dsDataFrame
          end
        end
      end
    end
  end
  object dsDataFrame: TDataSource
    DataSet = MovContaReceber.cdsContaReceberQuitacao
    Left = 344
    Top = 32
  end
end
