inherited FramePessoaAtividadeSecundaria: TFramePessoaAtividadeSecundaria
  Width = 451
  Height = 305
  inherited cxpcMain: TcxPageControl
    Width = 451
    Height = 264
    Properties.ActivePage = cxtsData
    ClientRectBottom = 264
    ClientRectRight = 451
    inherited cxtsSearch: TcxTabSheet
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 451
        Height = 240
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitWidth = 451
        ExplicitHeight = 240
        DesignSize = (
          451
          240)
        Height = 240
        Width = 451
        inherited dxBevel1: TdxBevel
          Width = 447
          ExplicitWidth = 766
        end
        object lbCodigo: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object lbAtividadeSecundaria: TLabel
          Left = 8
          Top = 39
          Width = 101
          Height = 13
          Caption = 'Atividade Secund'#225'ria'
        end
        object edtCodigo: TgbDBTextEditPK
          Left = 60
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object descAtividadeSecundaria: TgbDBTextEdit
          Left = 206
          Top = 35
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CNAE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbReadyOnly = True
          gbPassword = False
          Width = 0
        end
        object edtAtividadePrincipal: TgbDBButtonEditFK
          Left = 115
          Top = 35
          DataBinding.DataField = 'JOIN_SEQUENCIA_CNAE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Style.Color = clWhite
          TabOrder = 2
          gbCampoPK = 'SEQUENCIA'
          gbCamposRetorno = 'ID_CNAE;JOIN_DESCRICAO_CNAE;JOIN_SEQUENCIA_CNAE'
          gbTableName = 'CNAE'
          gbCamposConsulta = 'ID;DESCRICAO;SEQUENCIA'
          Width = 94
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Atividades Secund'#225'rias'
    Style.IsFontAssigned = True
    Width = 451
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaAtividadeSecundaria
  end
end
