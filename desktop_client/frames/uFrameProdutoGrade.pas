unit uFrameProdutoGrade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, uGBPanel, cxStyles, cxInplaceContainer, cxVGrid,
  cxDBVGrid, cxMaskEdit, cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData, cxDBLookupComboBox, cxBlobEdit,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxGridCustomView, cxGrid;

type
  TModoProdutoGrade = (modoInclusao, modoConsulta, modoAlteracao);

  TFrameProdutoGrade = class(TFramePadrao)
    gbPanel1: TgbPanel;
    pnFiltros: TgbPanel;
    dsProdutoGrade: TDataSource;
    fdmProdutoGrade: TgbFDMemTable;
    dsFiltros: TDataSource;
    fdmFiltros: TgbFDMemTable;
    fdmFiltrosID_PRODUTO_AGRUPADOR: TIntegerField;
    fdmFiltrosID_PRODUTO_GRADE: TIntegerField;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    Label2: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    fdmFiltrosDESCRICAO_PRODUTO_GRADE: TStringField;
    fdmFiltrosDESCRICAO_PRODUTO_AGRUPADOR: TStringField;
    CampoProduto: TgbDBButtonEditFK;
    gridProdutoGrade: TcxGrid;
    viewProdutoGrade: TcxGridDBBandedTableView;
    gridLevelProdutoGrade: TcxGridLevel;
    procedure EventoConstruirGradeSeCamposObrigatoriosEstiveremPreenchidos(Sender: TField);
  private
    FIdProdutoAgrupadorAnterior: Integer;
    FIdGradeProdutoAnterior: Integer;

    const PREFIXO_CAMPO_DESCRICAO_COR = 'DESCRICAO_COR_';

    procedure ConstruirVisaoProdutoGrade;
    procedure RealizarConsultaProdutoGrade;
    procedure AplicarFiltroSomenteProdutosAgrupadores;
    procedure CriarColuna(const AFieldName: String; const ACaption: String;
      const ASomenteLeitura: boolean = false);
    procedure PermitirEditarTodosFields;
    procedure LimparConsultaProdutoGrade;
  public
    FModo: TModoProdutoGrade;
    procedure ModoDeInclusao(const AIdProdutoAgrupador: Integer);
    procedure ModoDeConsulta(const AIdProdutoAgrupador: Integer);
    function ProdutosEmJSON: String;
    function FiltrosPreenchidos: Boolean;
    function ExisteRegistro: Boolean;
    function ExisteRegistrosValorados: Boolean;

    //procedure ModoAlteracao(const AIdProdutoAgrupador: Integer);
  protected
    procedure AoCriarFrame; override;
  end;

var
  FrameProdutoGrade: TFrameProdutoGrade;

implementation

{$R *.dfm}

uses uProduto, uProdutoGradeProxy, Generics.Collections, Rest.JSON, uDevExpressUtils, uCor, uFiltroFormulario,
  uProdutoProxy, uConstantes, uDatasetUtils;

{ TFrameProdutoGrade }

procedure TFrameProdutoGrade.AoCriarFrame;
begin
  inherited;
  AplicarFiltroSomenteProdutosAgrupadores;
  fdmFiltros.CreateDataset;
  fdmFiltros.Append;
end;

procedure TFrameProdutoGrade.AplicarFiltroSomenteProdutosAgrupadores;
begin
  CampoProduto.FWhereInjetado := TFiltroPadrao.Create;
  CampoProduto.FWhereInjetado.AddIgual(TProdutoProxy.FIELD_BO_PRODUTO_AGRUPADO, TConstantes.BO_NAO);
end;

procedure TFrameProdutoGrade.ConstruirVisaoProdutoGrade;
const
  POSSIBILITAR_EDICAO: Boolean = true;
var
  i: Integer;
  idCor: Integer;
  consultaJaRealizada: Boolean;
  coluna: TcxGridDBBandedColumn;
begin
  consultaJaRealizada := (FIdProdutoAgrupadorAnterior = fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger) and
    (FIdGradeProdutoAnterior = fdmFiltrosID_PRODUTO_GRADE.AsInteger);

  if consultaJaRealizada then
  begin
    exit;
  end;

  LimparConsultaProdutoGrade;
  RealizarConsultaProdutoGrade;
  PermitirEditarTodosFields;

  CriarColuna('JOIN_DESCRICAO_TAMANHO', 'Tamanho', POSSIBILITAR_EDICAO);
  CriarColuna('JOIN_DESCRICAO_COR', 'Cor');

  if FModo = modoConsulta then
  begin
    CriarColuna('QUANTIDADE', 'Quantidade', POSSIBILITAR_EDICAO);
  end;

  //CriarColuna('CODIGO_BARRA', 'C�digo de Barras');

  {for i := 0 to fdmProdutoGrade.FieldCount - 1 do
  begin
    if Pos(PREFIXO_CAMPO_DESCRICAO_COR, fdmProdutoGrade.Fields[i].FieldName) > 0 then
    begin
      idCor := StrtoIntDef(StringReplace(fdmProdutoGrade.Fields[i].FieldName,
        PREFIXO_CAMPO_DESCRICAO_COR, '', [rfReplaceAll]), 0);

      if idCor > 0 then
      begin
        CriarColuna(fdmProdutoGrade.Fields[i].FieldName, TCor.GetDescricaoCor(idCor), (FModo = modoConsulta));
      end;
    end;
  end;}

  coluna := viewProdutoGrade.GetColumnByFieldName('JOIN_DESCRICAO_TAMANHO');
  coluna.Width := 300;

  coluna := viewProdutoGrade.GetColumnByFieldName('JOIN_DESCRICAO_COR');
  coluna.GroupIndex := 0;
  coluna.Visible := false;

  TcxGridUtils.PersonalizarColunaPeloTipoCampo(viewProdutoGrade);
  viewProdutoGrade.DataController.Groups.FullExpand;

  FIdProdutoAgrupadorAnterior := fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger;
  FIdGradeProdutoAnterior := fdmFiltrosID_PRODUTO_GRADE.AsInteger;
end;

procedure TFrameProdutoGrade.CriarColuna(const AFieldName: String; const ACaption: String;
  const ASomenteLeitura: boolean = false);
var
  coluna: TcxGridDBBandedColumn;
const
  LARGURA_MINIMA = 200;
begin
  coluna := viewProdutoGrade.CreateColumn;
  coluna.Caption := ACaption;
  coluna.DataBinding.FieldName := AFieldName;
  coluna.Options.Editing := ASomenteLeitura;
  coluna.Options.Moving := false;
  coluna.Options.Sorting := false;

  if coluna.Width < LARGURA_MINIMA then
  begin
     coluna.Width := LARGURA_MINIMA;
  end;
end;

procedure TFrameProdutoGrade.EventoConstruirGradeSeCamposObrigatoriosEstiveremPreenchidos(Sender: TField);
begin
  if (fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger > 0) and (fdmFiltrosID_PRODUTO_GRADE.AsInteger > 0) then
  begin
    ConstruirVisaoProdutoGrade;
  end
  else
  begin
    LimparConsultaProdutoGrade;
  end;
end;

function TFrameProdutoGrade.ExisteRegistro: Boolean;
begin
  result := not fdmProdutoGrade.IsEmpty;
end;

function TFrameProdutoGrade.ExisteRegistrosValorados: Boolean;
begin
  result := TDatasetUtils.MaiorValor(fdmProdutoGrade.FieldByName('QUANTIDADE')) > 0;
end;

function TFrameProdutoGrade.FiltrosPreenchidos: Boolean;
begin
  result := (fdmFiltrosID_PRODUTO_GRADE.AsInteger > 0) and (fdmFiltrosID_PRODUTO_GRADE.AsInteger > 0);
end;

procedure TFrameProdutoGrade.LimparConsultaProdutoGrade;
begin
  if fdmProdutoGrade.Active then
  begin
    fdmProdutoGrade.EmptyDataset;
    viewProdutoGrade.ClearItems;
  end;
end;

procedure TFrameProdutoGrade.ModoDeConsulta(const AIdProdutoAgrupador: Integer);
begin
  FModo := modoConsulta;
  CampoProduto.ExecutarOnEnter;
  fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger := AIdProdutoAgrupador;
  CampoProduto.ExecutarOnExit;
  CampoProduto.gbReadyOnly := true;
end;

procedure TFrameProdutoGrade.ModoDeInclusao(const AIdProdutoAgrupador: Integer);
begin
  FModo := modoInclusao;
  CampoProduto.ExecutarOnEnter;
  fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger := AIdProdutoAgrupador;
  CampoProduto.ExecutarOnExit;
  CampoProduto.gbReadyOnly := false;
end;

procedure TFrameProdutoGrade.PermitirEditarTodosFields;
var
  i: Integer;
begin
  for i := 0 to fdmProdutoGrade.FieldCount - 1 do
  begin
    fdmProdutoGrade.Fields[i].ReadOnly := false;
  end;
end;

function TFrameProdutoGrade.ProdutosEmJSON: String;
var
  listaProdutos: TList<TProdutoGradeProxy>;
  produtoGrade: TProdutoGradeProxy;
  i: Integer;
begin
  listaProdutos := TList<TProdutoGradeProxy>.Create;
  try
    fdmProdutoGrade.First;
    while not fdmProdutoGrade.Eof do
    begin
      produtoGrade := TProdutoGradeProxy.Create;
      produtoGrade.FIdProdutoAgrupador := fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger;
      produtoGrade.FIdCor := fdmProdutoGrade.GetAsInteger('ID_COR');
      produtoGrade.FIdTamanho := fdmProdutoGrade.GetAsInteger('ID_TAMANHO');
      produtoGrade.FQuantidade := fdmProdutoGrade.GetAsFloat('QUANTIDADE');
      produtoGrade.FIdProduto := fdmProdutoGrade.GetAsInteger('ID_PRODUTO');
      produtoGrade.FCodigoBarra := '';//fdmProdutoGrade.GetAsString('CODIGO_BARRA');
      listaProdutos.Add(produtoGrade);

      fdmProdutoGrade.Next;
    end;
    result := TJSON.ObjectToJsonString(listaProdutos);
  finally
    FreeAndNIl(listaProdutos);
  end;
end;

procedure TFrameProdutoGrade.RealizarConsultaProdutoGrade;
begin
  TProduto.SetVisaoProdutoGrade(fdmFiltrosID_PRODUTO_GRADE.AsInteger,
    fdmFiltrosID_PRODUTO_AGRUPADOR.AsInteger, fdmProdutoGrade);
end;

end.
