unit uFrameProdutoFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, uGBDBTextEdit,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, uGBPanel, dxBarExtItems, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK;

type
  TFrameProdutoFornecedor = class(TFrameDetailPadrao)
    Label5: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    Level1BandedTableView1ID: TcxGridDBBandedColumn;
    Level1BandedTableView1REFERENCIA: TcxGridDBBandedColumn;
    Level1BandedTableView1ID_PRODUTO: TcxGridDBBandedColumn;
    Label2: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameProdutoFornecedor: TFrameProdutoFornecedor;

implementation

{$R *.dfm}

uses uCadProduto;

end.
