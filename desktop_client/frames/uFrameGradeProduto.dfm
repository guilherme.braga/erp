inherited FrameGradeProduto: TFrameGradeProduto
  Height = 478
  ExplicitHeight = 478
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitHeight = 478
    Height = 478
    object gridTamanho: TcxGrid
      Left = 616
      Top = 2
      Width = 256
      Height = 474
      Align = alRight
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object viewTamanho: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        OnEditKeyDown = viewTamanhoEditKeyDown
        DataController.DataSource = dsTamanhoCor
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.Active = True
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
            Caption = 'Tamanhos'
          end>
        object viewTamanhoTAMANHO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JOIN_DESCRICAO_TAMANHO'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsEditList
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'DESCRICAO'
          Properties.ListColumns = <
            item
              Caption = 'Modelo'
              FieldName = 'DESCRICAO'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dsListaTamanho
          Properties.PostPopupValueOnTab = True
          Properties.OnEditValueChanged = viewTamanhosTAMANHOPropertiesEditValueChanged
          Width = 865
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = viewTamanho
      end
    end
    object gridCor: TcxGrid
      Left = 2
      Top = 2
      Width = 614
      Height = 474
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object viewCor: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        OnEditKeyDown = viewCorsEditKeyDown
        DataController.DataSource = dsCor
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.Active = True
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
            Caption = 'Cores'
          end>
        object cxGridDBBandedColumn1: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JOIN_DESCRICAO_COR'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsEditList
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'DESCRICAO'
          Properties.ListColumns = <
            item
              FieldName = 'DESCRICAO'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = dsListaCor
          Properties.PostPopupValueOnTab = True
          Width = 865
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
      end
      object cxGridLevel2: TcxGridLevel
        GridView = viewCor
      end
    end
  end
  object cdsListaCor: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspListaCor'
    RemoteServer = DmConnection.dspCor
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 453
    Top = 232
    object cdsListaCorID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsListaCorDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object cdsListaCorBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsListaCorRGB: TStringField
      FieldName = 'RGB'
      Origin = 'RGB'
      Size = 30
    end
    object cdsListaCorCMYK: TStringField
      FieldName = 'CMYK'
      Origin = 'CMYK'
      Size = 30
    end
  end
  object dsListaCor: TDataSource
    DataSet = cdsListaCor
    Left = 480
    Top = 232
  end
  object cdsListaTamanho: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspListaTamanho'
    RemoteServer = DmConnection.dspTamanho
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 517
    Top = 232
    object cdsListaTamanhoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsListaTamanhoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsListaTamanhoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dsListaTamanho: TDataSource
    DataSet = cdsListaTamanho
    Left = 544
    Top = 232
  end
  object dsTamanhoCor: TDataSource
    DataSet = CadGradeProduto.cdsGradeProdutoCorTamanho
    Left = 528
    Top = 360
  end
  object dsCor: TDataSource
    DataSet = CadGradeProduto.cdsGradeProdutoCor
    Left = 600
    Top = 376
  end
end
