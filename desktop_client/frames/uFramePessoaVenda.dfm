inherited FramePessoaVenda: TFramePessoaVenda
  Width = 958
  Height = 379
  inherited cxGBDadosMain: TcxGroupBox
    Height = 379
    Width = 958
    inherited PnTituloFrameDetailPadrao: TgbPanel
      Style.IsFontAssigned = True
      Width = 954
    end
    inherited PnDados: TgbPanel
      Height = 356
      Width = 631
      inherited cxGridConsultaDadosDetalahadaGrade: TcxGrid
        Width = 627
        Height = 137
        ExplicitLeft = -7
        ExplicitTop = -21
        ExplicitWidth = 724
        ExplicitHeight = 74
        inherited viewConsultaDadosDetalhadaGrade: TcxGridDBBandedTableView
          OptionsView.BandHeaders = True
          Bands = <
            item
              Caption = 'Vendas'
            end>
        end
      end
      object cxGridConsultaDadosDetalahadaGradeProduto: TcxGrid
        Left = 2
        Top = 139
        Width = 627
        Height = 215
        Align = alBottom
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 1
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        ExplicitTop = 2
        ExplicitWidth = 724
        object ViewConsultaDadosDetalheProduto: TcxGridDBBandedTableView
          PopupMenu = RadialPopUpMenuTransientePadrao
          Navigator.Buttons.OnButtonClick = viewConsultaDadosDetalhadaGradeNavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <
            item
              Hint = 'Realiza a impress'#227'o desta grade'
              ImageIndex = 12
            end
            item
              Hint = 'Exportar grade para arquivo excel'
              ImageIndex = 274
            end
            item
              Hint = 'Exportar grade para arquivo PDF'
              ImageIndex = 273
            end>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          DataController.DataSource = dsDadosProdutos
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.Active = True
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.ApplyChanges = fracImmediately
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.ShowColumnFilterButtons = sfbAlways
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
              Caption = 'Produtos'
            end>
        end
        object cxGridLevel1: TcxGridLevel
          GridView = ViewConsultaDadosDetalheProduto
        end
      end
    end
    inherited pnFiltroVertical: TgbPanel
      Height = 356
    end
    inherited spFiltroVertical: TcxSplitter
      Height = 356
    end
  end
  inherited fdmDados: TgbFDMemTable
    AfterScroll = fdmDadosAfterScroll
    Left = 144
    Top = 232
  end
  inherited dsDados: TDataSource
    Left = 172
    Top = 232
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  object fdmDadosProdutos: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 144
    Top = 288
  end
  object dsDadosProdutos: TDataSource
    DataSet = fdmDadosProdutos
    Left = 172
    Top = 288
  end
end
