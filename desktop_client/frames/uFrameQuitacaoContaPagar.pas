unit uFrameQuitacaoContaPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameQuitacao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxBlobEdit,
  cxDBEdit, uGBDBBlobEdit, Vcl.StdCtrls, uGBDBTextEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, uGBDBDateEdit, cxButtonEdit, uGBDBButtonEditFK,
  uGBPanel, dxBarExtItems, cxLabel, uFramePadrao,
  uFrameQuitacaoContaPagarTipoCheque, uFrameQuitacaoContaPagarTipoCartao,
  cxCalc, uGBDBCalcEdit, ugbDBValorComPercentual, uUsuarioDesignControl;

type
  TFrameQuitacaoContaPagar = class(TFrameQuitacao)
    edtDtQuitacao: TgbDBDateEdit;
    edtVlQuitacao: TgbDBTextEdit;
    Label14: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    edtObservacao: TgbDBBlobEdit;
    Label11: TLabel;
    descTipoQuitacao: TgbDBTextEdit;
    edtIdTipoQuitacao: TgbDBButtonEditFK;
    Label1: TLabel;
    descContaCorrente: TgbDBTextEdit;
    edtIdContaCorrente: TgbDBButtonEditFK;
    Label2: TLabel;
    edtVlDescontos: TgbDBTextEdit;
    Label4: TLabel;
    edtVlAcrescimos: TgbDBTextEdit;
    edtPcDescontos: TgbDBTextEdit;
    Label18: TLabel;
    edtPcAcrescimos: TgbDBTextEdit;
    Label5: TLabel;
    Label6: TLabel;
    edtVlTotalQuitacao: TgbDBTextEdit;
    Label7: TLabel;
    edtDtCadastro: TgbDBDateEdit;
    Label9: TLabel;
    descContaAnalise: TgbDBTextEdit;
    labelOrigem: TLabel;
    edtTpDocumentoOrigem: TgbDBTextEdit;
    edtIdDocumentoOrigem: TgbDBTextEdit;
    labelSituacao: TLabel;
    edSituacao: TgbDBTextEdit;
    bevelDadosdeOrigem: TdxBevel;
    Label10: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    dsContaPagar: TDataSource;
    lbTrocoDiferenca: TcxLabel;
    Label12: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    EdtJuros: TgbDBValorComPercentual;
    Label13: TLabel;
    EdtMulta: TgbDBValorComPercentual;
    Label15: TLabel;
    procedure edtVlQuitacaoEnter(Sender: TObject);
    procedure edtVlDescontosEnter(Sender: TObject);
    procedure edtVlTotalQuitacaoExit(Sender: TObject);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure edtIdTipoQuitacaogbDepoisDeConsultar(Sender: TObject);
  private
    procedure ExibirTrocoDiferenca;
    procedure ExibirFrameTipoQuitacao;
    procedure BloquearCampoDataQuitacao;
  public
    FFrameQuitacaoContaPagarTipoCheque: TFrameQuitacaoContaPagarTipoCheque;
    FFrameQuitacaoContaPagarTipoCartao: TFrameQuitacaoContaPagarTipoCartao;
    VlQuitacaoEnter : Double;
    VlDescontoEnter : Double;
    VlAbertoEnter   : Double;

    procedure PreencherValorPagamento;
  protected
    procedure AoCriarFrame; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeIncluir; override;
    procedure DepoisDeIncluir; override;
    procedure DepoisDeAlterar; override;
    procedure AntesDeAlterar; override;
    procedure AntesDeRemover; override;
    procedure GerenciarControles; override;
    procedure InstanciarFrames; override;
    procedure DepoisDeRemover; override;
    procedure DepoisExibirFrameCheque; override;
  end;

var
  FrameQuitacaoContaPagar: TFrameQuitacaoContaPagar;

implementation

{$R *.dfm}

uses uMovContaPagar, uTControl_Function, uTMessage , uDatasetUtils,
  uContaPagarProxy, uPesqContaAnalise, uFormaPagamentoProxy, uTipoQuitacaoProxy, uFrameDetailPadrao,
  uMathUtils, uControlsUtils;

{ TFrameQuitacaoContaPagar }



procedure TFrameQuitacaoContaPagar.AntesDeIncluir;
var
  Status  : string;
begin
  inherited;
  Status := (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).cdsDataSTATUS.AsString;

  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
  VlAbertoEnter   := (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).cdsDataVL_ABERTO.AsFloat;

  if Status = TContaPagarMovimento.QUITADO then
  begin
    TMessage.MessageInformation('Conta a Pagar Quitado.');
    Abort;
  end;

  if Status = TContaPagarMovimento.CANCELADO then
  begin
    TMessage.MessageInformation('Conta a Pagar Cancelado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoContaPagar.AntesDeRemover;
var
  Origem  : string;
begin
  inherited;
  Origem := dsDataFrame.DataSet.FieldByName('tp_documento_origem').AsString;

  if Origem <> TContaPagarMovimento.ORIGEM_MANUAL then
  begin
    TMessage.MessageInformation('Esta Quita��o s� pode ser removida a partir de sua origem.');
    Abort;
  end;
end;

procedure TFrameQuitacaoContaPagar.AoCriarFrame;
begin
  inherited;

end;

procedure TFrameQuitacaoContaPagar.BloquearCampoDataQuitacao;
begin
  edtDtQuitacao.gbReadyOnly := (TControlFunction.GetInstance('TMovContaPagar')
    as TMovContaPagar).FParametroBloquearDataQuitacao.ValorSim;

  if not edtDtQuitacao.gbReadyOnly then
  begin
    TControlsUtils.SetFocus(edtDtQuitacao);
  end;
end;

procedure TFrameQuitacaoContaPagar.DepoisDeAlterar;
begin
  inherited;
  BloquearCampoDataQuitacao;
end;

procedure TFrameQuitacaoContaPagar.DepoisDeConfirmar;
begin
  inherited;
end;

procedure TFrameQuitacaoContaPagar.DepoisDeRemover;
begin
  inherited;
  (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).ActConfirm.Execute;
end;

procedure TFrameQuitacaoContaPagar.DepoisExibirFrameCheque;
begin
  inherited;
  if dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) then
  begin
    FFrameQuitacaoContaPagarTipoCheque.ConfigurarParaChequeTerceiro;
  end
  else
  begin
    FFrameQuitacaoContaPagarTipoCheque.ConfigurarParaChequeProprio;
  end;
end;

procedure TFrameQuitacaoContaPagar.DepoisDeIncluir;
begin
  inherited;
  BloquearCampoDataQuitacao;
end;

procedure TFrameQuitacaoContaPagar.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  TPesqContaAnalise.ExecutarConsulta(
    dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
    dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'),
    dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
end;

procedure TFrameQuitacaoContaPagar.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  TPesqContaAnalise.ExecutarConsultaOculta(
    dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
    dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'),
    dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
end;

procedure TFrameQuitacaoContaPagar.AntesDeAlterar;
begin
  inherited;
  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
  VlAbertoEnter   := (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).cdsDataVL_ABERTO.AsFloat
                   + dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat
                   + dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
end;

procedure TFrameQuitacaoContaPagar.AntesDeConfirmar;
var
  vDataSet : TDataSet;
begin
  inherited;

  vDataSet := dsDataFrame.DataSet;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat <= 0) then
  begin
    TMessage.MessageInformation('Informe um valor para quita��o do documento.');
    Abort;
  end;

  if (TControlFunction.GetInstance('TMovContaPagar') as TMovContapagar).QuitacaoEmCartao then
  begin
    FFrameQuitacaoContaPagarTipoCartao.ValidarDados;
  end;

  if vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat < vDataSet.FieldByName('VL_ACRESCIMO').AsFloat then
  try
    (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).FAtualizarTotalItem := false;
    if vDataSet.FieldByName('VL_MULTA').AsFloat > vDataSet.FieldByName('VL_ACRESCIMO').AsFloat then
    begin
      vDataSet.FieldByName('VL_MULTA').AsFloat := vDataSet.FieldByName('VL_ACRESCIMO').AsFloat;
      vDataSet.FieldByName('VL_JUROS').AsFloat := 0;
    end
    else
    begin
      vDataSet.FieldByName('VL_JUROS').AsFloat := vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat -
        vDataSet.FieldByName('VL_MULTA').AsFloat;
    end;

    vDataSet.FieldByName('PERC_JUROS').AsFloat := TMathUtils.PercentualSobreValor(
      vDataSet.FieldByName('VL_JUROS').AsFloat,
      (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).cdsDataVL_ABERTO.AsFloat);

    vDataSet.FieldByName('PERC_MULTA').AsFloat := TMathUtils.PercentualSobreValor(
      vDataSet.FieldByName('VL_MULTA').AsFloat,
      (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).cdsDataVL_ABERTO.AsFloat);
  finally
    (TControlFunction.GetInstance('TMovContaPagar') as TMovContaPagar).FAtualizarTotalItem := true;
  end;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    vDataSet.FieldByName('VL_TOTAL').AsFloat) then
  begin
    vDataSet.FieldByName('VL_TOTAL').AsFloat :=
      vDataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat;
  end;
end;

procedure TFrameQuitacaoContaPagar.edtVlQuitacaoEnter(Sender: TObject);
begin
  inherited;
  VlQuitacaoEnter := dsDataFrame.DataSet.FieldByName('vl_quitacao').AsFloat;
end;

procedure TFrameQuitacaoContaPagar.edtVlTotalQuitacaoExit(Sender: TObject);
begin
  inherited;
  ExibirTrocoDiferenca;
end;

procedure TFrameQuitacaoContaPagar.PreencherValorPagamento;
begin
  if Assigned(dsDataFrame.DataSet) and (dsDataFrame.DataSet.State in dsEditModes) then
  begin
    dsDataFrame.DataSet.FieldByName('IC_VL_PAGAMENTO').AsFloat :=
      dsDataFrame.DataSet.FieldByName('VL_TOTAL').AsFloat;

    ExibirTrocoDiferenca;
  end;
end;

procedure TFrameQuitacaoContaPagar.ExibirFrameTipoQuitacao;
begin
  if Assigned(dsDataFrame.Dataset) then
  begin
    ExibirFrameCartao(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CREDITO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO));

   ExibirFrameCheque(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO));
  end;
end;

procedure TFrameQuitacaoContaPagar.ExibirTrocoDiferenca;
begin
  if not Assigned(dsDataFrame.DataSet) then
    exit;

  if dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat >
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat -
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Troco R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  if dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat <
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat -
      dsDataFrame.Dataset.FieldByName('IC_VL_PAGAMENTO').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Diferen�a R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat := 0;
    lbTrocoDiferenca.Visible := false;
  end;
end;

procedure TFrameQuitacaoContaPagar.GerenciarControles;
var
  Origem : string;
begin
  inherited;

  if Assigned(dsDataFrame.DataSet) then
  begin
    Origem := dsDataFrame.DataSet.FieldByName('TP_DOCUMENTO_ORIGEM').AsString;
    cxGBDadosMain.Enabled := (Origem = TContaPagarMovimento.ORIGEM_MANUAL);
  end;

  PreencherValorPagamento;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoContaPagar.InstanciarFrames;
begin
  inherited;
  if not Assigned(FFrameQuitacaoContaPagarTipoCheque) then
  begin
    FFrameQuitacaoContaPagarTipoCheque := TFrameQuitacaoContaPagarTipoCheque.Create(PnQuitacaoCheque);
    FFrameQuitacaoContaPagarTipoCheque.Parent := PnQuitacaoCheque;
    FFrameQuitacaoContaPagarTipoCheque.Align := alClient;
    FFrameQuitacaoContaPagarTipoCheque.SetDataset(dsDataFrame.Dataset);
  end;

  if not Assigned(FFrameQuitacaoContaPagarTipoCartao) then
  begin
    FFrameQuitacaoContaPagarTipoCartao := TFrameQuitacaoContaPagarTipoCartao.Create(PnQuitacaoCartao);
    FFrameQuitacaoContaPagarTipoCartao.Parent := PnQuitacaoCartao;
    FFrameQuitacaoContaPagarTipoCartao.Align := alClient;
    FFrameQuitacaoContaPagarTipoCartao.SetDataset(dsDataFrame.Dataset);
  end;
end;

procedure TFrameQuitacaoContaPagar.edtIdTipoQuitacaogbDepoisDeConsultar(
  Sender: TObject);
begin
  inherited;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoContaPagar.edtVlDescontosEnter(Sender: TObject);
begin
  inherited;
  VlDescontoEnter := dsDataFrame.DataSet.FieldByName('vl_desconto').AsFloat;
end;

end.
