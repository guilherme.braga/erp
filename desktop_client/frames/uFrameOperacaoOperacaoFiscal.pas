unit uFrameOperacaoOperacaoFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxContainer, dxBar, dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset,
  Vcl.Menus, dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls, JvButton,
  JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxMaskEdit, cxButtonEdit,
  cxDBEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameOperacaoOperacaoFiscal = class(TFrameDetailTransientePadrao)
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    Label2: TLabel;
    EdtNaturezaOperacao: TgbDBButtonEditFK;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    procedure gbDBButtonEditFK3Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure AntesDeConfirmar; override;
  end;

var
  FrameOperacaoOperacaoFiscal: TFrameOperacaoOperacaoFiscal;

implementation

{$R *.dfm}

uses uCadOperacao, uControlsUtils, uTControl_Function;

procedure TFrameOperacaoOperacaoFiscal.gbDBButtonEditFK3Exit(Sender: TObject);
begin
  inherited;
  ActConfirm.Execute;
end;

procedure TFrameOperacaoOperacaoFiscal.AntesDeConfirmar;
var
  idOperacao: Integer;
begin
  idOperacao :=
    (TControlFunction.GetInstance('TCadOperacao') as TCadOperacao).cdsData.GetAsInteger('ID');

  if idOperacao > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_OPERACAO', idOperacao);
  end;

  inherited;
end;

end.
