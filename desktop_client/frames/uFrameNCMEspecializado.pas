unit uFrameNCMEspecializado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, cxTextEdit, cxDBEdit,
  uGBDBTextEdit, Vcl.StdCtrls, dxBar, dxRibbonRadialMenu, Datasnap.DBClient,
  uGBClientDataset, Vcl.Menus, dxBarExtItems, cxClasses, System.Actions,
  Vcl.ActnList, JvExControls, JvButton, JvTransparentButton, uGBPanel, dxBevel,
  cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC;

type
  TFrameNCMEspecializado = class(TFrameDetailTransientePadrao)
    Label1: TLabel;
    edDescricao: TgbDBTextEdit;
    procedure edDescricaoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameNCMEspecializado: TFrameNCMEspecializado;

implementation

{$R *.dfm}

uses uCadNCM;

procedure TFrameNCMEspecializado.edDescricaoExit(Sender: TObject);
begin
  inherited;
  if not fdmDatasetTransiente.FieldByName('DESCRICAO').AsString.IsEmpty then
  begin
    ActConfirm.Execute;
  end;
end;

end.
