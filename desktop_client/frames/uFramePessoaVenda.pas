unit uFramePessoaVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameConsultaDadosDetalheGrade,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter, dxBar,
  dxRibbonRadialMenu, cxClasses, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDMemTable, System.Actions, Vcl.ActnList, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, uGBPanel, cxGroupBox;

type
  TFramePessoaVenda = class(TFrameConsultaDadosDetalheGrade)
    cxGridConsultaDadosDetalahadaGradeProduto: TcxGrid;
    ViewConsultaDadosDetalheProduto: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    fdmDadosProdutos: TgbFDMemTable;
    dsDadosProdutos: TDataSource;
    procedure fdmDadosAfterScroll(DataSet: TDataSet);
  private
    const FIdentificadorProduto = 'FrameConsultaDadosDetalheGradeVendasProdutos';
    procedure AbrirProdutos;
  public
    procedure RedimencionarTamanhoGrade;
  protected
    procedure ExecutarAcaoExibirAgrupamento; override;
    procedure ExecutarAcaoAlterarSQL; override;
    procedure ExecutarAcaoSalvarConfiguracoes; override;
    procedure ExecutarAcaoAlterarColunas; override;
    procedure ExecutarAcaoRestaurarColunas; override;
    procedure DepoisConsultarDetalheGrade; override;
  end;

var
  FramePessoaVenda: TFramePessoaVenda;

implementation

{$R *.dfm}

uses uDevExpressUtils, uTUsuario, uSistema, uConstantes, uFrmMessage,
  uFrmMessage_Process, uDatasetUtils, uDmConnection, Data.FireDACJSONReflect,
  uFiltroFormulario, uVendaProxy, Data.DBXCommon;

{ TFrameConsultaDadosDetalheGrade1 }

procedure TFramePessoaVenda.AbrirProdutos;
var
  DSList: TFDJSONDataSets;
  whereProduto: TFiltroPadrao;
begin
  inherited;
  try
    if fdmDadosProdutos.Active then
    begin
      fdmDadosProdutos.EmptyDataset;
    end;

    try
      whereProduto := TFiltroPadrao.Create;
      whereProduto.AddIgual(TVendaItemProxy.FIELD_ID_VENDA,
        fdmDados.FieldByName('ID').AsInteger);

      DSList := DmConnection.ServerMethodsClient.GetDados(FIdentificadorProduto,
        whereProduto.where, TSistema.Sistema.Usuario.idSeguranca);
      try
        TDatasetUtils.JSONDatasetToMemTable(DSList, fdmDadosProdutos);
      finally
        FreeAndNil(DSList);
      end;
    finally
      FreeAndNil(whereProduto);
    end;

    TcxGridUtils.AdicionarTodosCamposNaView(ViewConsultaDadosDetalheProduto);

    TUsuarioGridView.LoadGridView(ViewConsultaDadosDetalheProduto,
      TSistema.Sistema.Usuario.idSeguranca, FIdentificadorProduto);

    ViewConsultaDadosDetalheProduto.OptionsData.Editing := false;
    ViewConsultaDadosDetalheProduto.OptionsSelection.CellSelect := false;

    ViewConsultaDadosDetalheProduto.FilterRow.Visible :=
      not fdmDadosProdutos.IsEmpty;
  Except
    on e : Exception do
    begin
      if e is TDBXError then
      begin
        TFrmMessage.Failure('Falha ao executar a consulta vinculada a tela.');
      end
      else
      begin
        TFrmMessage.Failure(e.message);
      end;

      abort;
    end;
  end;
end;

procedure TFramePessoaVenda.DepoisConsultarDetalheGrade;
begin
  inherited;
  AbrirProdutos;
end;

procedure TFramePessoaVenda.ExecutarAcaoAlterarColunas;
begin
  inherited;
  AlterarColunas(ViewConsultaDadosDetalheProduto);
end;

procedure TFramePessoaVenda.ExecutarAcaoAlterarSQL;
begin
  inherited;
  AlterarSQL(FIdentificadorProduto);
end;

procedure TFramePessoaVenda.ExecutarAcaoExibirAgrupamento;
begin
  inherited;
  ExibirAgrupamento(ViewConsultaDadosDetalheProduto);
end;

procedure TFramePessoaVenda.ExecutarAcaoRestaurarColunas;
begin
  inherited;
  RestaurarColunas(ViewConsultaDadosDetalheProduto, FIdentificadorProduto);
end;

procedure TFramePessoaVenda.ExecutarAcaoSalvarConfiguracoes;
begin
  inherited;
  SalvarConfiguracoes(ViewConsultaDadosDetalheProduto, FIdentificadorProduto);
end;

procedure TFramePessoaVenda.fdmDadosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not FExecutandoConsulta then
  begin
    AbrirProdutos;
  end;
end;

procedure TFramePessoaVenda.RedimencionarTamanhoGrade;
begin
  cxGridConsultaDadosDetalahadaGradeProduto.Height := PnDados.Height div 2;
end;

end.
