unit uFrameQuitacaoLoteRecebimentoTipoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCartao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxCalc, uGBDBCalcEdit, uGBPanel,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxGroupBox, Data.DB;

type
  TFrameQuitacaoLoteRecebimentoTipoCartao = class(TFrameTipoQuitacaoCartao)
    dsFrame: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoLoteRecebimentoTipoCartao: TFrameQuitacaoLoteRecebimentoTipoCartao;

implementation

{$R *.dfm}

uses uMovLoteRecebimento;

{ TFrameQuitacaoLoteRecebimentoTipoCartao }

procedure TFrameQuitacaoLoteRecebimentoTipoCartao.SetDataset(
  ADataset: TDataset);
begin
  dsFrame.Dataset := ADataset;
end;

end.
