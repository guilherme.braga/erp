object FrameDetailPadrao: TFrameDetailPadrao
  Left = 0
  Top = 0
  Width = 451
  Height = 305
  Align = alClient
  TabOrder = 0
  object cxpcMain: TcxPageControl
    Left = 0
    Top = 41
    Width = 451
    Height = 264
    Align = alClient
    Focusable = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    TabStop = False
    Properties.ActivePage = cxtsSearch
    Properties.CustomButtons.Buttons = <>
    Properties.NavigatorPosition = npLeftTop
    Properties.Style = 8
    OnChange = cxpcMainChange
    ClientRectBottom = 264
    ClientRectRight = 451
    ClientRectTop = 24
    object cxtsSearch: TcxTabSheet
      Caption = 'Pesquisar'
      ImageIndex = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGridPesquisaPadrao: TcxGrid
        Left = 0
        Top = 0
        Width = 451
        Height = 240
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object Level1BandedTableView1: TcxGridDBBandedTableView
          OnDblClick = Level1BandedTableView1DblClick
          OnKeyDown = Level1BandedTableView1KeyDown
          OnKeyPress = Level1BandedTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          DataController.DataSource = dsDataFrame
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.Active = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.ShowColumnFilterButtons = sfbAlways
          OptionsView.BandHeaders = False
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
            end>
        end
        object cxGridPesquisaPadraoLevel1: TcxGridLevel
          GridView = Level1BandedTableView1
        end
      end
    end
    object cxtsData: TcxTabSheet
      Caption = 'Dados'
      ImageIndex = 1
      object cxGBDadosMain: TcxGroupBox
        Left = 0
        Top = 0
        Align = alClient
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Height = 240
        Width = 451
        object dxBevel1: TdxBevel
          Left = 2
          Top = 2
          Width = 447
          Height = 32
          Align = alTop
          Shape = dxbsLineBottom
          ExplicitLeft = 1
          ExplicitTop = 0
          ExplicitWidth = 614
        end
      end
    end
  end
  object PnTituloFrameDetailPadrao: TgbPanel
    Left = 0
    Top = 0
    Align = alTop
    Alignment = alCenterCenter
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    ParentBackground = False
    ParentFont = False
    Style.BorderStyle = ebsOffice11
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TextStyle = [fsBold]
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Transparent = True
    Height = 19
    Width = 451
  end
  object ActionListMain: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 551
    Top = 184
    object ActConfirm: TAction
      Category = 'Action'
      Caption = '&Confirmar'
      Hint = 'Confirmar Altera'#231#245'es'
      ImageIndex = 18
      OnExecute = ActConfirmExecute
    end
    object ActInsert: TAction
      Category = 'Manager'
      Caption = '&Incluir'
      Hint = 'Inserir Registro'
      ImageIndex = 2
      OnExecute = ActInsertExecute
    end
    object ActCancel: TAction
      Category = 'Action'
      Caption = 'Ca&ncelar'
      Hint = 'Cancelar Altera'#231#245'es'
      ImageIndex = 21
      OnExecute = ActCancelExecute
    end
    object ActUpdate: TAction
      Category = 'Manager'
      Caption = '&Alterar'
      Hint = 'Alterar Registro'
      ImageIndex = 4
      OnExecute = ActUpdateExecute
    end
    object ActRemove: TAction
      Category = 'Manager'
      Caption = '&Excluir'
      Hint = 'Remover Registro'
      ImageIndex = 8
      OnExecute = ActRemoveExecute
    end
    object ActPrimeiro: TAction
      Category = 'Navegacao'
      Caption = '&Primeiro'
      Hint = 'Ir para o primeiro registro'
      ImageIndex = 113
      OnExecute = ActPrimeiroExecute
    end
    object ActProximo: TAction
      Category = 'Navegacao'
      Caption = 'P&r'#243'ximo'
      ImageIndex = 115
      OnExecute = ActProximoExecute
    end
    object ActAnterior: TAction
      Category = 'Navegacao'
      Caption = 'An&terior'
      ImageIndex = 114
      OnExecute = ActAnteriorExecute
    end
    object ActUltimo: TAction
      Category = 'Navegacao'
      Caption = #218'&ltimo'
      ImageIndex = 112
      OnExecute = ActUltimoExecute
    end
  end
  object dxBarManagerPadrao: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Manager'
      'Navegacao')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    FlatCloseButton = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    PopupMenuLinks = <>
    ShowHelpButton = True
    ShowShortCutInHint = True
    Style = bmsOffice11
    UseF10ForMenu = False
    UseSystemFont = True
    Left = 522
    Top = 184
    DockControlHeights = (
      0
      0
      22
      0)
    object dxBarManager: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Gest'#227'o '
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbImprimir'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'siMaisAcoes'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      UseRecentItems = False
      Visible = True
      WholeRow = False
    end
    object barNavegador: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Navega'#231#227'o'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 264
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 722
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbPrimeiro'
        end
        item
          Visible = True
          ItemName = 'lbProximo'
        end
        item
          Visible = True
          ItemName = 'lbAnterior'
        end
        item
          Visible = True
          ItemName = 'lbUltimo'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbRegistro'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = ActInsert
      Category = 0
      CloseSubMenuOnClick = False
      DropDownEnabled = False
    end
    object dxBarButton2: TdxBarButton
      Action = ActUpdate
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = ActRemove
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = ActConfirm
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = ActCancel
      Category = 0
    end
    object lbSeparador: TdxBarButton
      Category = 0
      Visible = ivAlways
    end
    object lbRegistro: TdxBarStatic
      Caption = '1 de 1'
      Category = 0
      Hint = '1 de 1'
      Visible = ivAlways
    end
    object siMaisAcoes: TdxBarSubItem
      Action = ActMaisAcoes
      Category = 0
      Images = DmAcesso.cxImage16x16
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bbAlterarRotuloColunas'
        end
        item
          Visible = True
          ItemName = 'bbExibirAgrupamento'
        end
        item
          Visible = True
          ItemName = 'bbSalvarConfiguracoes'
        end>
    end
    object bbAlterarRotuloColunas: TdxBarButton
      Action = ActAlterar_Colunas_Grid
      Category = 0
    end
    object bbSalvarConfiguracoes: TdxBarButton
      Action = ActSalvarConfiguracoes
      Category = 0
    end
    object bbExibirAgrupamento: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 0
    end
    object bbImprimir: TdxBarButton
      Action = ActImprimir
      Category = 0
    end
    object lbPrimeiro: TdxBarButton
      Action = ActPrimeiro
      Category = 1
    end
    object lbProximo: TdxBarButton
      Action = ActProximo
      Category = 1
      Hint = 'Ir para o pr'#243'ximo registro'
    end
    object lbAnterior: TdxBarButton
      Action = ActAnterior
      Category = 1
      Hint = 'Ir para o registro Anterior'
    end
    object lbUltimo: TdxBarButton
      Action = ActUltimo
      Category = 1
      Hint = 'Ir para o '#250'ltimo registro'
    end
  end
  object dsDataFrame: TDataSource
    Left = 494
    Top = 184
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 388
    Top = 200
    object ActAlterar_Colunas_Grid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterar_Colunas_GridExecute
    end
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActImprimir: TAction
      Category = 'filter'
      Caption = 'Imprimir'
      Hint = 'Realizar a impress'#227'o do registro selecionado'
      ImageIndex = 261
      OnExecute = ActImprimirExecute
    end
    object ActMaisAcoes: TAction
      Category = 'filter'
      OnExecute = ActMaisAcoesExecute
    end
  end
  object PMGridPesquisa: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = PMGridPesquisaPopup
    Left = 360
    Top = 200
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterar_Colunas_Grid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ExibirAgrupamento2: TMenuItem
      Action = ActExibirAgrupamento
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
end
