unit uFrameQuitacaoGeracaoDocumentoTipoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCartao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxCalc, cxDBEdit, uGBDBCalcEdit, uGBDBTextEdit, cxButtonEdit,
  uGBDBButtonEditFK, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit, Vcl.StdCtrls,
  uGBPanel, cxGroupBox, Data.DB;

type
  TFrameQuitacaoGeracaoDocumentoTipoCartao = class(TFrameTipoQuitacaoCartao)
    dsDadosCartao: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoGeracaoDocumentoTipoCartao: TFrameQuitacaoGeracaoDocumentoTipoCartao;

implementation

{$R *.dfm}

uses uMovGeracaoDocumento;

procedure TFrameQuitacaoGeracaoDocumentoTipoCartao.SetDataset(ADataset: TDataset);
begin
  dsDadosCartao.Dataset := ADataset;
end;

end.
