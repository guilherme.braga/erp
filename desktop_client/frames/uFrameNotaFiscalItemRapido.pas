unit uFrameNotaFiscalItemRapido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDBEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls, uGBPanel, JvExControls, JvButton, JvTransparentButton, uGBClientDataset,
  FireDAC.Comp.Client, dxBarExtItems, dxRibbonRadialMenu, uProdutoGradeProxy, Generics.Collections;

type
  TFrameNotaFiscalItemRapido = class(TFrameDetailPadrao)
    gbPanel1: TgbPanel;
    Label9: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    CampoProduto: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBTextEdit7: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    gbDBTextEdit9: TgbDBTextEdit;
    gbDBTextEdit10: TgbDBTextEdit;
    gbDBTextEdit11: TgbDBTextEdit;
    gbDBTextEdit12: TgbDBTextEdit;
    gbDBTextEdit13: TgbDBTextEdit;
    gbDBTextEdit14: TgbDBTextEdit;
    gbDBTextEdit15: TgbDBTextEdit;
    gbDBTextEdit16: TgbDBTextEdit;
    gbDBTextEdit17: TgbDBTextEdit;
    gbDBTextEdit18: TgbDBTextEdit;
    gbDBTextEdit19: TgbDBTextEdit;
    JvTransparentButton1: TJvTransparentButton;
    JvTransparentButton2: TJvTransparentButton;
    dsItensDaNota: TDataSource;
    RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    ActAlterarProdutoGrade: TAction;
    dxBarButton9: TdxBarButton;
    procedure gbDBTextEdit15Exit(Sender: TObject);
    procedure CampoProdutogbDepoisDeConsultar(Sender: TObject);
  private
    procedure CopiarItemTransienteParaPersistente;
    procedure CopiarItemPersistenteParaTransiente;

    procedure FocarCampoProduto;
    procedure NovoItem;
    procedure IncluirProdutosEmGrade(const AIdProdutoAgrupador: Integer);
    procedure InserirProdutoEmGradeNosItensDaNotaFiscal(const AProdutoGrade: TProdutoGradeProxy);
    procedure AlterarProdutoEmGradeNosItensDaNotaFiscal(const AProdutoGrade: TProdutoGradeProxy);
    procedure DeletarProdutosDaNotaQueForamDeletadosNaGrade(const AIdProdutoAgrupador: Integer;
      const AProdutoGrade: TList<TProdutoGradeProxy>);

    procedure CarregarDadosProdutoEmModoTransiente(const AIdProduto: Integer);
  public
    procedure SetConfiguracoesIniciaisFrameNotaItem(
      AItensPersistentes: TgbClientDataset; AItensTransientes: TFDMemTable);

  protected
    procedure GerenciarControles; override;
    procedure AoCriarFrame; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure DepoisDeAlterar; override;
    procedure DepoisDeCancelar; override;
    procedure DepoisDeRemover; override;
    procedure Cancelar; override;
    procedure Confirmar; override;
    procedure Excluir; override;
  end;

var
  FrameNotaFiscalItemRapido: TFrameNotaFiscalItemRapido;

implementation

{$R *.dfm}

uses uMovNotaFiscal, uDatasetUtils, uDevExpressUtils, uTUsuario, uControlsUtils,
  uTMessage, uSistema, uFiltroFormulario, uProdutoProxy, uProduto, uConstantes, uFrmMessage,
  uFrmGerarProdutoGrade, uTControl_Function;

{ TFrameDetailPadrao1 }

procedure TFrameNotaFiscalItemRapido.CampoProdutogbDepoisDeConsultar(
  Sender: TObject);
var
  dataset: TgbClientDataset;
  idProduto: Integer;
begin
  dataset := TgbClientDataset(dsDataFrame.Dataset);
  inherited;
  if (dataset.GetAsInteger('ID_PRODUTO') > 0) then
  begin
    idProduto := dataset.GetAsInteger('ID_PRODUTO');
    CarregarDadosProdutoEmModoTransiente(idProduto);

    if dataset.GetAsString('JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO').Equals(TConstantes.BO_SIM) then
    begin
      if TFrmMessage.Question('O produto selecionado possui similares gerados por grade,'+
        ' deseja inclu�-los') = MCONFIRMED then
      begin
        ActCancel.Execute;
          IncluirProdutosEmGrade(idProduto);
      end;
    end;
  end;
end;

procedure TFrameNotaFiscalItemRapido.Cancelar;
begin
  //inherited;
  if not (dsDataFrame.Dataset.State in dsEditModes) then
    dsDataFrame.Dataset.Edit;

  dsDataFrame.Dataset.Cancel;
  dsDataFrame.Dataset.Edit;
end;

procedure TFrameNotaFiscalItemRapido.CarregarDadosProdutoEmModoTransiente(const AIdProduto: Integer);
var
  dataset: TDataset;
  produto: TProdutoProxy;
begin
  dataset := dsDataFrame.Dataset;

  produto := TProduto.GetProdutoFilialProxy(AIdProduto, TSistema.Sistema.Filial.Fid);

  dataset.FieldByName('ID_PRODUTO').AsInteger := produto.FID;
  dataset.FieldByName('JOIN_DESCRICAO_PRODUTO').AsString := produto.FDescricao;
  dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat := produto.FQtdeEstoque;
  dataset.FieldByName('JOIN_SIGLA_UNIDADE_ESTOQUE').AsString :=  produto.FUN;
  dataset.FieldByName('VL_CUSTO').AsFloat := produto.FProdutoFilial.FVlUltimoCusto;
  dataset.FieldByName('VL_CUSTO_OPERACIONAL').AsFloat := produto.FProdutoFilial.FVlCustoOperacional;
  dataset.FieldByName('PERC_CUSTO_OPERACIONAL').AsFloat := produto.FProdutoFilial.FPercCustoOperacional;
  dataset.FieldByName('JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO').AsString := produto.FBOProdutoAgrupador;
  dataset.FieldByName('JOIN_ID_PRODUTO_AGRUPADOR_PRODUTO').AsInteger := produto.FIDProdutoAgrupador;
end;

procedure TFrameNotaFiscalItemRapido.Confirmar;
begin
  //inherited;
  if not (dsDataFrame.Dataset.State in dsEditModes) then
    dsDataFrame.Dataset.Edit;

  dsDataFrame.Dataset.Post;
end;

procedure TFrameNotaFiscalItemRapido.CopiarItemPersistenteParaTransiente;
begin
  TDatasetUtils.CopiarRegistro(dsItensDaNota.Dataset, dsDataFrame.Dataset);
end;

procedure TFrameNotaFiscalItemRapido.CopiarItemTransienteParaPersistente;
var
  idNotaFiscal: Integer;
begin
  if FAcaoCrud = TAcaoCrud(Incluir) then
    TGbClientDataset(dsItensDaNota.Dataset).Incluir
  else if FAcaoCrud = TAcaoCrud(Alterar) then
    TGbClientDataset(dsItensDaNota.Dataset).Alterar;

  TDatasetUtils.CopiarRegistro(dsDataFrame.Dataset, dsItensDaNota.Dataset);

  idNotaFiscal := (TControlFunction.GetInstance('TMovNotaFiscal') as TMovNotaFiscal).cdsDataID.AsInteger;

  if idNotaFiscal > 0 then
  begin
    dsItensDaNota.Dataset.FieldByName('ID_NOTA_FISCAL').AsInteger := idNotaFiscal;
  end;

  TGbClientDataset(dsItensDaNota.Dataset).Salvar;
end;

procedure TFrameNotaFiscalItemRapido.DeletarProdutosDaNotaQueForamDeletadosNaGrade(
  const AIdProdutoAgrupador: Integer; const AProdutoGrade: TList<TProdutoGradeProxy>);
var
  i: Integer;
  produtoGrade: TProdutoGradeProxy;
  cdsNotaFiscalItem: TgbClientDataset;
  cdsCloneNotaFiscalItem: TgbClientDataset;
  produtoEncontrado: Boolean;
begin
  cdsNotaFiscalItem := TgbClientDataset(dsItensDaNota.Dataset);

  cdsCloneNotaFiscalItem := TgbClientDataset.Create(nil);
  try
    cdsCloneNotaFiscalItem.CloneCursor(cdsNotaFiscalItem, true);

    cdsCloneNotaFiscalItem.First;
    while not cdsCloneNotaFiscalItem.Eof do
    begin
      produtoEncontrado := false;
      for i := 0 to Pred(AProdutoGrade.Count) do
      begin
        produtoGrade := AProdutoGrade[i];

        if cdsCloneNotaFiscalItem.GetAsInteger('ID_PRODUTO') = produtoGrade.FIdProduto then
        begin
          produtoEncontrado := true;
          break;
        end;
      end;

      if not produtoEncontrado then
      begin
        if cdsNotaFiscalItem.Locate('ID_PRODUTO', produtoGrade.FIdProduto, []) then
        begin
          cdsNotaFiscalItem.Delete;
        end;
      end;

      cdsCloneNotaFiscalItem.Next;
    end;
  finally
    FreeAndNil(cdsCloneNotaFiscalItem);
  end;
end;

procedure TFrameNotaFiscalItemRapido.DepoisDeAlterar;
begin
  inherited;
  CopiarItemPersistenteParaTransiente;
end;

procedure TFrameNotaFiscalItemRapido.DepoisDeCancelar;
begin
  inherited;
  FocarCampoProduto;
end;

procedure TFrameNotaFiscalItemRapido.DepoisDeConfirmar;
begin
  CopiarItemTransienteParaPersistente;
  inherited;
  NovoItem;
  FocarCampoProduto
end;

procedure TFrameNotaFiscalItemRapido.DepoisDeRemover;
begin
  inherited;
  NovoItem;
end;

procedure TFrameNotaFiscalItemRapido.Excluir;
begin
  //inherited;
  ActCancel.Execute;
  dsItensDaNota.dataset.Delete;
end;

procedure TFrameNotaFiscalItemRapido.FocarCampoProduto;
begin
  TControlsUtils.SetFocus(CampoProduto);
end;

procedure TFrameNotaFiscalItemRapido.gbDBTextEdit15Exit(Sender: TObject);
begin
  inherited;
  ActConfirm.Execute;
end;

procedure TFrameNotaFiscalItemRapido.SetConfiguracoesIniciaisFrameNotaItem(
  AItensPersistentes: TgbClientDataset; AItensTransientes: TFDMemTable);
begin
  AoCriarFrame;

  if Assigned(AItensPersistentes) then
    dsItensDaNota.DataSet := AItensPersistentes;

  if Assigned(AItensTransientes) then
    dsDataFrame.DataSet := AItensTransientes;

  cxpcMain.ActivePage := cxtsSearch;
  cxpcMain.Properties.HideTabs := true;

  Level1BandedTableView1.ClearItems;
  Level1BandedTableView1.DataController.CreateAllItems();
  TcxGridUtils.PersonalizarColunaPeloTipoCampo(Level1BandedTableView1);

  TUsuarioGridView.LoadGridView(Level1BandedTableView1,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name);

  NovoItem;

  GerenciarControles;
end;

procedure TFrameNotaFiscalItemRapido.AlterarProdutoEmGradeNosItensDaNotaFiscal(
  const AProdutoGrade: TProdutoGradeProxy);
var
  cdsNotaFiscalItem: TDataset;
begin
  cdsNotaFiscalItem := dsDataFrame.Dataset;

  ActUpdate.Execute;

  cdsNotaFiscalItem.FieldByName('QUANTIDADE').AsFloat := AProdutoGrade.fQuantidade;

  ActConfirm.Execute;
end;

procedure TFrameNotaFiscalItemRapido.AntesDeConfirmar;
begin
  inherited;
  TDatasetUtils.ValidarCamposObrigatorios(dsDataFrame.Dataset);
end;

procedure TFrameNotaFiscalItemRapido.AoCriarFrame;
begin
  inherited;
  dxBarManager.Visible := false;
  barNavegador.Visible := false;
  ActConfirm.Caption := '';
  ActCancel.Caption := '';
end;

procedure TFrameNotaFiscalItemRapido.GerenciarControles;
var
  cdsNotaFiscalItem: TDataset;
begin
  //inherited;
  barNavegador.Visible := false;

  if (not Assigned(dsDataFrame.Dataset)) or (not dsDataFrame.Dataset.Active) then
  begin
    exit;
  end;

  //cdsNotaFiscalItem := dsDataFrame.Dataset;
  //ActAlterarProdutoGrade.Visible := cdsNotaFiscalItem.FieldByName('JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO').AsString.Equals(TConstantes.BO_SIM);
  //ActUpdate.Visible := cdsNotaFiscalItem.FieldByName('JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO').AsString.Equals(TConstantes.BO_NAO);
end;

procedure TFrameNotaFiscalItemRapido.IncluirProdutosEmGrade(const AIdProdutoAgrupador: Integer);
var
  listaProdutosEmGrade: TList<TProdutoGradeProxy>;
  listaProdutosSelecionados: TList<TProdutoGradeProxy>;
  produtoGrade: TProdutoGradeProxy;
  cdsNotaFiscalItem: TgbClientDataset;
  produtoCadastrado: Boolean;
  i: Integer;
begin
  listaProdutosEmGrade := TFrmGerarProdutoGrade.SelecionarProdutoGrade(AIdProdutoAgrupador);
  try
    if Assigned(listaProdutosEmGrade) and (listaProdutosEmGrade.Count > 0) then
    begin
      listaProdutosSelecionados := TList<TProdutoGradeProxy>.Create;
      try
        cdsNotaFiscalItem :=  TgbClientDataset(dsDataFrame.Dataset);
        for i := 0 to Pred(listaProdutosEmGrade.Count) do
        begin
          produtoGrade := listaProdutosEmGrade[i];

          if (produtoGrade.FIdProduto <= 0) or (produtoGrade.FQuantidade <= 0) then
          begin
            continue;
          end;

          listaProdutosSelecionados.Add(produtoGrade);

          produtoCadastrado := cdsNotaFiscalItem.Locate('ID_PRODUTO', produtoGrade.FIdProduto, []);

          if not produtoCadastrado then
          begin
            InserirProdutoEmGradeNosItensDaNotaFiscal(produtoGrade);
          end
          else
          begin
            AlterarProdutoEmGradeNosItensDaNotaFiscal(produtoGrade);
          end;
        end;

        DeletarProdutosDaNotaQueForamDeletadosNaGrade(AIdProdutoAgrupador, listaProdutosSelecionados);
      finally
        FreeAndNil(listaProdutosSelecionados);
      end;
    end;
  finally
    //FreeAndNil(listaProdutosEmGrade);
  end;
end;

procedure TFrameNotaFiscalItemRapido.InserirProdutoEmGradeNosItensDaNotaFiscal(
  const AProdutoGrade: TProdutoGradeProxy);
var
  cdsNotaFiscalItem: TDataset;
begin
  NovoItem;
  cdsNotaFiscalItem := dsDataFrame.Dataset;
  CarregarDadosProdutoEmModoTransiente(AProdutoGrade.FIdProduto);
  cdsNotaFiscalItem.FieldByName('I_QCOM').AsFloat := AProdutoGrade.fQuantidade;

  ActConfirm.Execute;
end;

procedure TFrameNotaFiscalItemRapido.NovoItem;
begin
  if TFDMemTable(dsDataFrame.Dataset).Active then
    TFDMemTable(dsDataFrame.Dataset).EmptyDataSet
  else
    TFDMemTable(dsDataFrame.Dataset).Open;

  ActInsert.Execute;
  FAcaoCrud := TAcaoCrud(Incluir);
end;

end.
