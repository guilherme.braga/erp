unit uFrameQuitacaoLoteRecebimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameQuitacao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxBlobEdit, uGBDBBlobEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxLabel, uFrameTipoQuitacaoCheque, uFramePadrao,
  uFrameTipoQuitacaoCartao, uFrameQuitacaoLoteRecebimentoTipoCheque,
  uFrameQuitacaoLoteRecebimentoTipoCartao, uGBDBValorComPercentual;

type
  TFrameQuitacaoLoteRecebimento = class(TFrameQuitacao)
    labelDtQuitacao: TLabel;
    labelObservacao: TLabel;
    labelTipoQuitacao: TLabel;
    labelContaCorrente: TLabel;
    labelDescontos: TLabel;
    labelVlTotalQuitacao: TLabel;
    labelContaAnalise: TLabel;
    edtDtQuitacao: TgbDBDateEdit;
    edtObservacao: TgbDBBlobEdit;
    descTipoQuitacao: TgbDBTextEdit;
    edtIdTipoQuitacao: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    edtIdContaCorrente: TgbDBButtonEditFK;
    edtVlDescontos: TgbDBTextEdit;
    edtPcDescontos: TgbDBTextEdit;
    edtVlTotalQuitacao: TgbDBTextEdit;
    descContaAnalise: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    labelCentroResultado: TLabel;
    edtFkCentroResultado: TgbDBButtonEditFK;
    descCentroResultado: TgbDBTextEdit;
    labelValor: TLabel;
    labelQuitacao: TLabel;
    edtValor: TgbDBTextEdit;
    edtQuitacao: TgbDBTextEdit;
    lbTrocoDiferenca: TcxLabel;
    dsLote: TDataSource;
    PnDadosBaixaPorTitulo: TgbPanel;
    labelDocumento: TLabel;
    edtDocumento: TgbDBTextEdit;
    labelDescricao: TLabel;
    edtDescricao: TgbDBTextEdit;
    EdtMulta: TGBDBValorComPercentual;
    EdtJuros: TGBDBValorComPercentual;
    Label10: TLabel;
    Label9: TLabel;
    Label1: TLabel;
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure edtQuitacaoExit(Sender: TObject);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure edtIdTipoQuitacaogbDepoisDeConsultar(Sender: TObject);
  private
    procedure ExibirTrocoDiferenca;
    procedure ExibirFrameTipoQuitacao;
  public
    FFrameQuitacaoLoteRecebimentoTipoCheque: TFrameQuitacaoLoteRecebimentoTipoCheque;
    FFrameQuitacaoLoteRecebimentoTipoCartao: TFrameQuitacaoLoteRecebimentoTipoCartao;
    procedure PreencherValorRecebimento;
    procedure ExibirDadosDeBaixaPorTitulo(AExibir: Boolean);
  protected
    procedure AntesDeConfirmar; override;
    procedure AntesDeIncluir; override;
    procedure AntesDeRemover; override;
    procedure DepoisDeConfirmar; override;
    procedure AoCriarFrame; override;
    procedure InstanciarFrames; override;
    procedure GerenciarControles; override;
  end;

var
  FrameQuitacaoLoteRecebimento: TFrameQuitacaoLoteRecebimento;

implementation

{$R *.dfm}

uses uMovLoteRecebimento, uTControl_Function, uLoteRecebimentoProxy, uTMessage,
  uPesqContaAnalise, uTipoQuitacaoProxy;

procedure TFrameQuitacaoLoteRecebimento.AntesDeConfirmar;
var
  vDataSet : TDataSet;
begin
  inherited;

  vDataSet := dsDataFrame.DataSet;

  {if (vDataSet.FieldByName('VL_TOTAL').AsFloat <= 0) then
  begin
    TMessage.MessageInformation('Informe um valor para quita��o do documento.');
    Abort;
  end;}

  if (TControlFunction.GetInstance('TMovLoteRecebimento') as TMovLoteRecebimento).QuitacaoEmCartao then
  begin
    FFrameQuitacaoLoteRecebimentoTipoCartao.ValidarDados;
  end;

  if (vDataSet.FieldByName('VL_TOTAL').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_Recebimento').AsFloat > 0) and
    (vDataSet.FieldByName('IC_VL_Recebimento').AsFloat <
    vDataSet.FieldByName('VL_TOTAL').AsFloat) then
  begin
    vDataSet.FieldByName('VL_TOTAL').AsFloat :=
      vDataSet.FieldByName('IC_VL_Recebimento').AsFloat;
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.AntesDeIncluir;
var
  Status  : string;
begin
  inherited;

  Status := (TControlFunction.GetInstance('TMovLoteRecebimento') as TMovLoteRecebimento).cdsDataSTATUS.AsString;
  if Status = TLoteRecebimentoProxy.STATUS_EFETIVADO then
  begin
    TMessage.MessageInformation('Lote de Recebimento j� se encontra Efetivado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoLoteRecebimento.AntesDeRemover;
var
  Status  : string;
begin
  inherited;

  Status := (TControlFunction.GetInstance('TMovLoteRecebimento') as TMovLoteRecebimento).cdsDataSTATUS.AsString;
  if Status = TLoteRecebimentoProxy.STATUS_EFETIVADO then
  begin
    TMessage.MessageInformation('Lote de Recebimento j� se encontra Efetivado.');
    Abort;
  end;

end;

procedure TFrameQuitacaoLoteRecebimento.AoCriarFrame;
begin
  inherited;
  EdtDescricao.Properties.CharCase := ecNormal;
  EdtDocumento.Properties.CharCase := ecNormal;
end;

procedure TFrameQuitacaoLoteRecebimento.DepoisDeConfirmar;
begin
  inherited;
//  (TControlFunction.GetInstance('TMovLoteRecebimento') as TMovLoteRecebimento)
end;

procedure TFrameQuitacaoLoteRecebimento.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if not Assigned(dsDataFrame.Dataset) then
    exit;

  if not dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(dsDataFrame.Dataset.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_CONTA_ANALISE'), dsDataFrame.Dataset.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.ExibirDadosDeBaixaPorTitulo(
  AExibir: Boolean);
begin
  PnDadosBaixaPorTitulo.Visible := AExibir;
end;

procedure TFrameQuitacaoLoteRecebimento.ExibirFrameTipoQuitacao;
begin
  if Assigned(dsDataFrame.Dataset) then
  begin
    ExibirFrameCartao(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CREDITO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO));

   ExibirFrameCheque(
    dsDataFrame.Dataset.FieldByName('JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(
    TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or dsDataFrame.Dataset.FieldByName(
    'JOIN_TIPO_TIPO_QUITACAO').AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO));
  end;

end;

procedure TFrameQuitacaoLoteRecebimento.ExibirTrocoDiferenca;
begin
  if not Assigned(dsDataFrame.DataSet) then
    exit;

  if dsDataFrame.Dataset.FieldByName('IC_VL_Recebimento').AsFloat >
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('IC_VL_Recebimento').AsFloat -
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Troco R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  if dsDataFrame.Dataset.FieldByName('IC_VL_Recebimento').AsFloat <
    dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat then
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat :=
      dsDataFrame.Dataset.FieldByName('VL_TOTAL').AsFloat -
      dsDataFrame.Dataset.FieldByName('IC_VL_Recebimento').AsFloat;

    lbTrocoDiferenca.Visible := false;
    lbTrocoDiferenca.Caption := 'Diferen�a R$ '+FormatFloat('###,###,###,##0.00',
      dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat);
    Application.ProcessMessages;
    lbTrocoDiferenca.Visible := true;
  end
  else
  begin
    dsDataFrame.Dataset.FieldByName('CC_VL_TROCO_DIFERENCA').AsFloat := 0;
    lbTrocoDiferenca.Visible := false;
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.GerenciarControles;
begin
  inherited;
  PreencherValorRecebimento;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoLoteRecebimento.InstanciarFrames;
begin
  inherited;
  if not Assigned(FFrameQuitacaoLoteRecebimentoTipoCheque) then
  begin
    FFrameQuitacaoLoteRecebimentoTipoCheque := TFrameQuitacaoLoteRecebimentoTipoCheque.Create(PnQuitacaoCheque);
    FFrameQuitacaoLoteRecebimentoTipoCheque.Parent := PnQuitacaoCheque;
    FFrameQuitacaoLoteRecebimentoTipoCheque.Align := alClient;
    FFrameQuitacaoLoteRecebimentoTipoCheque.SetDataset(dsDataFrame.Dataset);
  end;

  if not Assigned(FFrameQuitacaoLoteRecebimentoTipoCartao) then
  begin
    FFrameQuitacaoLoteRecebimentoTipoCartao := TFrameQuitacaoLoteRecebimentoTipoCartao.Create(PnQuitacaoCartao);
    FFrameQuitacaoLoteRecebimentoTipoCartao.Parent := PnQuitacaoCartao;
    FFrameQuitacaoLoteRecebimentoTipoCartao.Align := alClient;
    FFrameQuitacaoLoteRecebimentoTipoCartao.SetDataset(dsDataFrame.Dataset);
  end;
end;

procedure TFrameQuitacaoLoteRecebimento.edtIdTipoQuitacaogbDepoisDeConsultar(
  Sender: TObject);
begin
  inherited;
  ExibirFrameTipoQuitacao;
end;

procedure TFrameQuitacaoLoteRecebimento.edtQuitacaoExit(Sender: TObject);
begin
  inherited;
  ExibirTrocoDiferenca;
end;

procedure TFrameQuitacaoLoteRecebimento.PreencherValorRecebimento;
begin
  if Assigned(dsDataFrame.DataSet) and (dsDataFrame.DataSet.State in dsEditModes) then
  begin
    dsDataFrame.DataSet.FieldByName('IC_VL_Recebimento').AsFloat :=
      dsDataFrame.DataSet.FieldByName('VL_TOTAL').AsFloat;

    ExibirTrocoDiferenca;
  end;
end;

end.
