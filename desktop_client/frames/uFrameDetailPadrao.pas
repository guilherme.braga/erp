unit uFrameDetailPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox, System.Actions,
  Vcl.ActnList, dxNavBar, dxNavBarCollns, cxClasses, dxNavBarBase, dxBar,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, dxBevel, Vcl.ExtCtrls, JvDesignSurface,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset,
  Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu, uGBPanel, dxBarExtItems,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type TFieldFilterFrame = (All, Required);

type TAcaoCrud = (Incluir, Alterar, Excluir, None);

type
  TFrameDetailPadrao = class(TFrame)
    ActionListMain: TActionList;
    ActConfirm: TAction;
    ActInsert: TAction;
    ActCancel: TAction;
    ActUpdate: TAction;
    ActRemove: TAction;
    dxBarManagerPadrao: TdxBarManager;
    dxBarManager: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dsDataFrame: TDataSource;
    cxpcMain: TcxPageControl;
    cxtsSearch: TcxTabSheet;
    cxGridPesquisaPadrao: TcxGrid;
    Level1BandedTableView1: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    cxtsData: TcxTabSheet;
    cxGBDadosMain: TcxGroupBox;
    dxBevel1: TdxBevel;
    ActionListAssistent: TActionList;
    ActAlterar_Colunas_Grid: TAction;
    ActSalvarConfiguracoes: TAction;
    ActExibirAgrupamento: TAction;
    PMGridPesquisa: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    ExibirAgrupamento2: TMenuItem;
    SalvarConfiguraes1: TMenuItem;
    PnTituloFrameDetailPadrao: TgbPanel;
    barNavegador: TdxBar;
    ActPrimeiro: TAction;
    ActProximo: TAction;
    ActAnterior: TAction;
    ActUltimo: TAction;
    lbPrimeiro: TdxBarButton;
    lbProximo: TdxBarButton;
    lbAnterior: TdxBarButton;
    lbUltimo: TdxBarButton;
    lbSeparador: TdxBarButton;
    lbRegistro: TdxBarStatic;
    siMaisAcoes: TdxBarSubItem;
    bbAlterarRotuloColunas: TdxBarButton;
    bbSalvarConfiguracoes: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    ActMaisAcoes: TAction;
    ActImprimir: TAction;
    bbImprimir: TdxBarButton;
  procedure ActInsertExecute(Sender: TObject);
  procedure ActUpdateExecute(Sender: TObject);
  procedure ActRemoveExecute(Sender: TObject);
  procedure ActConfirmExecute(Sender: TObject);
  procedure ActCancelExecute(Sender: TObject);
    procedure cxpcMainChange(Sender: TObject);
    procedure Level1BandedTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure ActAlterar_Colunas_GridExecute(Sender: TObject);
    procedure ActRestaurarColunasExecute(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure PMGridPesquisaPopup(Sender: TObject);
    procedure Level1BandedTableView1DblClick(Sender: TObject);
    procedure Level1BandedTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ActPrimeiroExecute(Sender: TObject);
    procedure ActProximoExecute(Sender: TObject);
    procedure ActAnteriorExecute(Sender: TObject);
    procedure ActUltimoExecute(Sender: TObject);
    procedure ActMaisAcoesExecute(Sender: TObject);
    procedure ActImprimirExecute(Sender: TObject);
  private
    FDataset: TDataset;
    FConsultaExecutada: Boolean;
    FPermiteAlterar: Boolean;
    FPermiteExcluir: Boolean;
    FPermiteIncluir: Boolean;
    function GetFields(AFieldFilter: TFieldFilterFrame; ADataset: TDataset = nil): TStringList;
    procedure FocarContainer(AControl: TWinControl = nil);
    procedure ExibirFiltroDeMemoriaDaGrid(const AExibir: Boolean);
    procedure ConfigurarPainelTitulo;
  public
    FAcaoCrud: TAcaoCrud;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AtualizarContadorRegistros;

    procedure FocarProximoControle(AControl: TWinControl);

    procedure SetConfiguracoesIniciais(ADataset: TgbClientDataset); virtual;

    property permiteIncluir: Boolean read FPermiteIncluir write FPermiteIncluir default true;
    property permiteAlterar: Boolean read FPermiteAlterar write FPermiteAlterar default true;
    property permiteExcluir: Boolean read FPermiteExcluir write FPermiteExcluir default true;

    procedure AtivarSomenteLeitura;
    procedure DesativarSomenteLeitura;
  protected
    property dataset: TDataset read FDataset write FDataset;
    property consultaExecutada: Boolean read FConsultaExecutada write FConsultaExecutada default false;

    procedure GerenciarControles;virtual;
    procedure AntesDeConfirmar;virtual;
    procedure DepoisDeConfirmar;virtual;
    procedure AntesDeCancelar; virtual;
    procedure DepoisDeCancelar;virtual;
    procedure AntesDeIncluir; virtual;
    procedure DepoisDeIncluir; virtual;
    procedure AntesDeAlterar; virtual;
    procedure DepoisDeAlterar; virtual;
    procedure AoCriarFrame; virtual;
    procedure AoDestruirFrame; virtual;
    procedure Confirmar; virtual;
    procedure Cancelar; virtual;
    procedure Excluir; virtual;
    procedure AcaoAlterar; virtual;
    procedure AntesDeRemover; virtual;
    procedure DepoisDeRemover; virtual;
    procedure SelecionarRegistroComDuploClique; virtual;

  end;

implementation

{$R *.dfm}

uses uDmAcesso, uTUsuario, uFrmApelidarColunasGrid, uFrmMessage, uTMessage,
  uDmConnection, uTFunction, uDevExpressUtils, uControlsUtils, uSistema, uFrmVisualizacaoRelatoriosAssociados;

procedure TFrameDetailPadrao.Cancelar;
begin
  TGBClientDataset(dsDataFrame.dataset).Cancel;
end;

procedure TFrameDetailPadrao.ConfigurarPainelTitulo;
begin
  PnTituloFrameDetailPadrao.Transparent := false;
  PnTituloFrameDetailPadrao.PanelStyle.OfficeBackgroundKind := pobkGradient;
end;

procedure TFrameDetailPadrao.Confirmar;
begin
  TGBClientDataset(dsDataFrame.dataset).Salvar;
end;

constructor TFrameDetailPadrao.Create(AOwner: TComponent);
begin
  inherited;
  permiteIncluir := true;
  permiteAlterar := true;
  permiteExcluir := true;
  SetConfiguracoesIniciais(nil);
end;

procedure TFrameDetailPadrao.ActAnteriorExecute(Sender: TObject);
begin
  LockWindowUpdate(Self.Handle);
  try
    if dsDataFrame.Dataset.State in dsEditModes then
    begin
      ActConfirm.Execute;
    end;
    dsDataFrame.Dataset.Prior;
    ActUpdate.Execute;
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TFrameDetailPadrao.ActCancelExecute(Sender: TObject);
begin
  FocarProximoControle(Self);
  AntesDeCancelar;
  Cancelar;
  DepoisDeCancelar;
  GerenciarControles;
end;

function TFrameDetailPadrao.GetFields(AFieldFilter: TFieldFilterFrame; ADataset: TDataset = nil): TStringList;
var dataset: TDataset;
    i: Integer;
begin
  result := TStringList.Create;

  if ADataset <> nil then
    dataset := ADataset
  else
    dataset := dsDataFrame.dataset;

  for i := 0 to Pred(dataset.Fields.Count) do
    if ((AFieldFilter = TFieldFilterFrame(Required)) and dataset.Fields[i].Required) or (AFieldFilter <> TFieldFilterFrame(Required)) then
      result.Add(dataset.Fields[i].FieldName);
end;

procedure TFrameDetailPadrao.Level1BandedTableView1DblClick(Sender: TObject);
begin
  SelecionarRegistroComDuploClique;
end;

procedure TFrameDetailPadrao.Level1BandedTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
    ActRemove.Execute;
end;

procedure TFrameDetailPadrao.Level1BandedTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := UpperCase(String(Key)).ToCharArray[0];
end;

procedure TFrameDetailPadrao.PMGridPesquisaPopup(Sender: TObject);
begin
  ActExibirAgrupamento.Caption := TFunction.Iif(Level1BandedTableView1.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');
end;

procedure TFrameDetailPadrao.GerenciarControles;
var bEdicao: Boolean;
    bExisteRegistro: Boolean;
begin

  if Assigned(dsDataFrame.dataset) then
  try
    LockWindowUpdate(Self.Handle);

    if dsDataFrame.dataset.Active then
      bEdicao := dsDataFrame.State in dsEditModes
    else
      bEdicao := false;

    if dsDataFrame.Dataset.Active then
      bExisteRegistro := dsDataFrame.Dataset.RecordCount > 0
    else
      bExisteRegistro := false;

    ActInsert.Enabled := not(bEdicao) and FPermiteIncluir;
    ActUpdate.Enabled := not(bEdicao) and bExisteRegistro and FPermiteAlterar;
    ActRemove.Enabled := not(bEdicao) and bExisteRegistro and FPermiteExcluir;
    ActConfirm.Enabled := bEdicao;
    ActCancel.Enabled := bEdicao;
    ActImprimir.Enabled := bExisteRegistro and not(bEdicao);

    barNavegador.Visible := bEdicao;

    AtualizarContadorRegistros;

    if not(cxpcMain.ActivePage = cxtsData) and (bEdicao) then
      cxpcMain.ActivePage := cxtsData
    else
      cxpcMain.ActivePage := cxtsSearch;
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TFrameDetailPadrao.ActConfirmExecute(Sender: TObject);
begin
  FocarProximoControle(Self);
  AntesDeConfirmar;
  Confirmar;
  DepoisDeConfirmar;
  GerenciarControles;
end;

procedure TFrameDetailPadrao.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  Level1BandedTableView1.OptionsView.GroupByBox := not Level1BandedTableView1.OptionsView.GroupByBox;
end;

procedure TFrameDetailPadrao.ActImprimirExecute(Sender: TObject);
var
  id: Integer;
begin
  TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
    dsDataFrame.Dataset.FieldByName('ID').AsInteger, Self.Name, TSistema.Sistema.usuario.idSeguranca);
end;

procedure TFrameDetailPadrao.ActInsertExecute(Sender: TObject);
begin
  AntesDeIncluir;
  TGBClientDataset(dsDataFrame.dataset).Incluir;
  GerenciarControles;
  DepoisDeIncluir;
end;

procedure TFrameDetailPadrao.ActMaisAcoesExecute(Sender: TObject);
begin
  //Sem implementa��o, mais a��es
end;

procedure TFrameDetailPadrao.ActPrimeiroExecute(Sender: TObject);
begin
  LockWindowUpdate(Self.Handle);
  try
    if dsDataFrame.Dataset.State in dsEditModes then
    begin
      ActConfirm.Execute;
    end;
    dsDataFrame.Dataset.First;
    ActUpdate.Execute;
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TFrameDetailPadrao.ActProximoExecute(Sender: TObject);
begin
  LockWindowUpdate(Self.Handle);
  try
    if dsDataFrame.Dataset.State in dsEditModes then
    begin
      ActConfirm.Execute;
    end;
    dsDataFrame.Dataset.Next;
    ActUpdate.Execute;
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TFrameDetailPadrao.ActRemoveExecute(Sender: TObject);
begin
  AntesDeRemover;
  if TMessage.MessageDeleteAsk() then
  begin
    Excluir;
    DepoisDeRemover;
  end;
end;

procedure TFrameDetailPadrao.ActRestaurarColunasExecute(Sender: TObject);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, Self.Name);
  Level1BandedTableView1.RestoreDefaults;
end;

procedure TFrameDetailPadrao.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  TUsuarioGridView.SaveGridView(Level1BandedTableView1,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TFrameDetailPadrao.ActUltimoExecute(Sender: TObject);
begin
  LockWindowUpdate(Self.Handle);
  try
    if dsDataFrame.Dataset.State in dsEditModes then
    begin
      ActConfirm.Execute;
    end;
    dsDataFrame.Dataset.Last;
    ActUpdate.Execute;
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TFrameDetailPadrao.ActUpdateExecute(Sender: TObject);
begin
  AntesDeAlterar;
  AcaoAlterar;
  DepoisDeAlterar;
  GerenciarControles;
end;

procedure TFrameDetailPadrao.AcaoAlterar;
begin
  TGBClientDataset(dsDataFrame.dataset).Alterar;
end;

procedure TFrameDetailPadrao.AntesDeAlterar;
begin
 // Implementar na Sub Classe
end;

procedure TFrameDetailPadrao.AntesDeRemover;
begin
 // Implementar na Sub Classe
end;

procedure TFrameDetailPadrao.AntesDeCancelar;
begin
    // Implementar na Sub Classe
end;

procedure TFrameDetailPadrao.AntesDeConfirmar;
begin
  // Implementar na Sub Classe
end;

procedure TFrameDetailPadrao.AntesDeIncluir;
begin
  // Implementar na Suuuuub Classe
end;

procedure TFrameDetailPadrao.AoCriarFrame;
begin
  ConfigurarPainelTitulo;
  //implementar nas classes herdadas
end;

procedure TFrameDetailPadrao.AoDestruirFrame;
begin

end;

procedure TFrameDetailPadrao.AtivarSomenteLeitura;
begin
  FPermiteAlterar := true;
  FPermiteExcluir := true;
  FPermiteIncluir := true;
end;

procedure TFrameDetailPadrao.AtualizarContadorRegistros;
begin
  if not(dsDataFrame.Dataset.Active) or (dsDataFrame.Dataset.RecordCount = 0) then
  begin
    lbRegistro.Caption := '1 de 1';
  end
  else
  begin
    lbRegistro.Caption := InttoStr(dsDataFrame.Dataset.Recno) + ' de ' +
      InttoStr(dsDataFrame.Dataset.RecordCount);
  end;
end;

procedure TFrameDetailPadrao.cxpcMainChange(Sender: TObject);
begin
  if cxpcMain.ActivePage = cxtsData then
    FocarContainer;
end;

procedure TFrameDetailPadrao.DepoisDeAlterar;
begin
  FAcaoCrud := TAcaoCrud(Alterar);
  // Implementar na Sub Classe
end;

procedure TFrameDetailPadrao.DepoisDeCancelar;
begin
  FAcaoCrud := TAcaoCrud(None);
  //implementar nas classes herdadas
end;

procedure TFrameDetailPadrao.DepoisDeConfirmar;
begin
  FAcaoCrud := TAcaoCrud(None);
  //implementar nas classes herdadas
end;

procedure TFrameDetailPadrao.DepoisDeIncluir;
begin
  FAcaoCrud := TAcaoCrud(Incluir);
  // Implementar nas classes herdadas
end;

procedure TFrameDetailPadrao.DepoisDeRemover;
begin
 ///
end;

procedure TFrameDetailPadrao.DesativarSomenteLeitura;
begin
  FPermiteAlterar := false;
  FPermiteExcluir := false;
  FPermiteIncluir := false;
end;

destructor TFrameDetailPadrao.Destroy;
begin
  inherited;
end;

procedure TFrameDetailPadrao.FocarContainer(AControl: TWinControl);
var container: TWinControl;
begin
  if cxpcMain.ActivePage = cxtsData then
  begin
    if AControl <> nil then
      container := AControl
    else
      container := cxGBDadosMain;

    if (container <> nil) and (container.CanFocus) then
    begin
      container.SetFocus;
      Self.SelectNext(container, true, true);
    end;
  end;
end;

procedure TFrameDetailPadrao.FocarProximoControle(AControl: TWinControl);
begin
  Self.SelectNext(AControl, true, true);
end;

procedure TFrameDetailPadrao.SelecionarRegistroComDuploClique;
begin
  if ActUpdate.Enabled and ActUpdate.Visible then
    ActUpdate.Execute;
end;

procedure TFrameDetailPadrao.SetConfiguracoesIniciais(ADataset: TgbClientDataset);
begin
  AoCriarFrame;

  if Assigned(ADataset) then
    dsDataFrame.DataSet := ADataset;

  cxpcMain.ActivePage := cxtsSearch;
  cxpcMain.Properties.HideTabs := true;

  Level1BandedTableView1.ClearItems;
  Level1BandedTableView1.DataController.CreateAllItems();
  TcxGridUtils.PersonalizarColunaPeloTipoCampo(Level1BandedTableView1);

  TUsuarioGridView.LoadGridView(Level1BandedTableView1,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name);

  GerenciarControles;
end;

procedure TFrameDetailPadrao.ActAlterar_Colunas_GridExecute(Sender: TObject);
begin
  TFrmApelidarColunasGrid.SetColumns(Level1BandedTableView1);
end;

procedure TFrameDetailPadrao.Excluir;
begin
  dsDataFrame.dataset.Delete;
end;

procedure TFrameDetailPadrao.ExibirFiltroDeMemoriaDaGrid(
  const AExibir: Boolean);
begin
  Level1BandedTableView1.FilterRow.Visible := AExibir;
end;

end.
