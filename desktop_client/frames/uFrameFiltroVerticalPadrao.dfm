inherited FrameFiltroVerticalPadrao: TFrameFiltroVerticalPadrao
  Width = 451
  Height = 305
  Align = alClient
  ExplicitWidth = 451
  ExplicitHeight = 305
  inherited cxGBDadosMain: TcxGroupBox
    Top = 19
    ExplicitTop = 19
    ExplicitWidth = 451
    ExplicitHeight = 286
    Height = 286
    Width = 451
    object pnScrollBox: TcxScrollBox
      Left = 2
      Top = 2
      Width = 447
      Height = 282
      Align = alClient
      BorderStyle = cxcbsNone
      LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
    end
  end
  object PnTituloFrameFiltroVerticalPadrao: TgbPanel
    Left = 0
    Top = 0
    Align = alTop
    Alignment = alCenterCenter
    Caption = 'Informe os filtros desejados'
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    ParentBackground = False
    ParentFont = False
    Style.BorderStyle = ebsOffice11
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TextStyle = [fsBold]
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Transparent = True
    Height = 19
    Width = 451
  end
  object dsFiltro: TDataSource
    DataSet = fdmFiltro
    OnDataChange = dsFiltroDataChange
    Left = 424
    Top = 112
  end
  object StyleFiltros: TcxStyleRepository
    Left = 64
    Top = 40
    PixelsPerInch = 96
    object FundoBranco: TcxStyle
      AssignedValues = [svColor]
      Color = clWhite
    end
  end
  object fdmFiltro: TgbFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 208
    Top = 136
  end
end
