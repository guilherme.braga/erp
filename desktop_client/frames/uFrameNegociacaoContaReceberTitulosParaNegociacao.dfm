inherited FrameNegociacaoContaReceberTitulosParaNegociacao: TFrameNegociacaoContaReceberTitulosParaNegociacao
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloFrameDetailPadrao: TgbPanel
      Caption = 
        'Selecione os t'#237'tulos a receber que deseja adicionar a esta negoc' +
        'ia'#231#227'o'
      Style.IsFontAssigned = True
    end
    inherited pnFiltroVertical: TgbPanel
      ExplicitLeft = 3
      ExplicitTop = 27
      inherited pnAcoesFiltroVertical: TgbPanel
        Top = 22
      end
      object gbPanel1: TgbPanel
        Left = 2
        Top = 2
        Align = alTop
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        Height = 17
        Width = 311
        object DBCheckBox2: TDBCheckBox
          Left = 8
          Top = 0
          Width = 95
          Height = 17
          Caption = 'Dispensar Juros'
          TabOrder = 0
        end
        object DBCheckBox1: TDBCheckBox
          Left = 107
          Top = 0
          Width = 120
          Height = 17
          Caption = 'Dispensar Multa'
          TabOrder = 1
        end
      end
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
