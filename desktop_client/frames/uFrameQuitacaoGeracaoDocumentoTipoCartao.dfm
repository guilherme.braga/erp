inherited FrameQuitacaoGeracaoDocumentoTipoCartao: TFrameQuitacaoGeracaoDocumentoTipoCartao
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloCartao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      inherited PnDtVencimento: TgbPanel
        inherited edtDtVencimento: TgbDBDateEdit
          DataBinding.DataField = 'DT_BASE'
          DataBinding.DataSource = MovGeracaoDocumento.dsData
        end
      end
      inherited PnOperadoraCartao: TgbPanel
        inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
          DataBinding.DataField = 'ID_OPERADORA_CARTAO_ENTRADA'
          DataBinding.DataSource = MovGeracaoDocumento.dsData
          gbCamposRetorno = 
            'ID_OPERADORA_CARTAO_ENTRADA;JOIN_DESCRICAO_OPERADORA_CARTAO_ENTR' +
            'ADA'
        end
        inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
          DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA'
          DataBinding.DataSource = MovGeracaoDocumento.dsData
        end
      end
      inherited PnValor: TgbPanel
        inherited EdtValor: TgbDBCalcEdit
          DataBinding.DataField = 'VL_ENTRADA'
          DataBinding.DataSource = MovGeracaoDocumento.dsData
        end
      end
    end
  end
  object dsDadosCartao: TDataSource
    DataSet = MovGeracaoDocumento.cdsData
    Left = 472
    Top = 4
  end
end
