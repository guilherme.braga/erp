inherited FrameOperacaoItem: TFrameOperacaoItem
  inherited cxpcMain: TcxPageControl
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitHeight = 240
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 708
        end
        object Label1: TLabel
          Left = 8
          Top = 9
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 24
          Height = 13
          Caption = 'A'#231#227'o'
        end
        object gbDBTextEditPK1: TgbDBTextEditPK
          Left = 66
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object gbDBButtonEditFK1: TgbDBButtonEditFK
          Left = 66
          Top = 36
          DataBinding.DataField = 'ID_ACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 1
          gbTextEdit = gbDBTextEdit2
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_ACAO;JOIN_DESCRICAO_ACAO'
          gbTableName = 'ACAO'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'ACAO'
          Width = 60
        end
        object gbDBTextEdit2: TgbDBTextEdit
          Left = 123
          Top = 36
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_ACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbReadyOnly = True
          gbPassword = False
          Width = 317
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'A'#231#245'es'
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
end
