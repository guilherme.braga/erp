unit uFramePessoaAtividadeSecundaria;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, uGBDBTextEdit,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK;

type
  TFramePessoaAtividadeSecundaria = class(TFrameDetailPadrao)
    lbCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    lbAtividadeSecundaria: TLabel;
    descAtividadeSecundaria: TgbDBTextEdit;
    edtAtividadePrincipal: TgbDBButtonEditFK;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FramePessoaAtividadeSecundaria: TFramePessoaAtividadeSecundaria;

implementation

{$R *.dfm}

uses uCadPessoa;

end.
