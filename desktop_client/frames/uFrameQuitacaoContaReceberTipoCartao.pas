unit uFrameQuitacaoContaReceberTipoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCartao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxCalc, uGBDBCalcEdit, uGBPanel,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxGroupBox, Data.DB;

type
  TFrameQuitacaoContaReceberTipoCartao = class(TFrameTipoQuitacaoCartao)
    dsDataFrame: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoContaReceberTipoCartao: TFrameQuitacaoContaReceberTipoCartao;

implementation

{$R *.dfm}

uses uMovContaReceber;

{ TFrameQuitacaoContaReceberTipoCartao }

procedure TFrameQuitacaoContaReceberTipoCartao.SetDataset(ADataset: TDataset);
begin
  dsDataFrame.Dataset := ADataset;
end;

end.
