inherited FramePessoaRepresentante: TFramePessoaRepresentante
  Width = 631
  inherited cxpcMain: TcxPageControl
    Width = 631
    Properties.ActivePage = cxtsData
    ClientRectRight = 631
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 631
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          631
          240)
        Width = 631
        inherited dxBevel1: TdxBevel
          Width = 627
          ExplicitWidth = 841
        end
        object lbCodigo: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object lbCPFCliente: TLabel
          Left = 8
          Top = 63
          Width = 19
          Height = 13
          Caption = 'CPF'
        end
        object lbRG: TLabel
          Left = 142
          Top = 63
          Width = 14
          Height = 13
          Caption = 'RG'
        end
        object lbNome: TLabel
          Left = 8
          Top = 39
          Width = 27
          Height = 13
          Caption = 'Nome'
        end
        object ldDtNascimento: TLabel
          Left = 259
          Top = 40
          Width = 73
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Dt. Nascimento'
          ExplicitLeft = 473
        end
        object lbEstadoCivil: TLabel
          Left = 447
          Top = 40
          Width = 55
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Estado Civil'
          ExplicitLeft = 661
        end
        object edtCodigo: TgbDBTextEditPK
          Left = 47
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object edDocFederal: TgbDBTextEdit
          Left = 38
          Top = 60
          DataBinding.DataField = 'CPF'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbPassword = False
          Width = 90
        end
        object edDocEstadual: TgbDBTextEdit
          Left = 162
          Top = 60
          DataBinding.DataField = 'RG'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          gbPassword = False
          Width = 90
        end
        object edNome: TgbDBTextEdit
          Left = 38
          Top = 35
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'NOME'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbPassword = False
          ExplicitWidth = 36
          Width = 216
        end
        object deDtNascimento: TgbDBDateEdit
          Left = 336
          Top = 36
          Anchors = [akTop, akRight]
          DataBinding.DataField = 'DT_NASCIMENTO'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbDateTime = False
          ExplicitLeft = 156
          Width = 107
        end
        object cbEstadoCivil: TgbDBComboBox
          Left = 506
          Top = 36
          Anchors = [akTop, akRight]
          DataBinding.DataField = 'ESTADO_CIVIL'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.Items.Strings = (
            'CASADO'
            'DIVORCIADO'
            'SEPARADO'
            'SOLTEIRO'
            'VI'#218'VO')
          Properties.ReadOnly = False
          Properties.Sorted = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          ExplicitLeft = 326
          Width = 121
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Representantes'
    Style.IsFontAssigned = True
    Width = 631
  end
  inherited ActionListMain: TActionList
    Left = 583
    Top = 48
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 554
    Top = 48
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaRepresentante
    Left = 526
    Top = 48
  end
  inherited ActionListAssistent: TActionList
    Left = 428
    Top = 48
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 400
    Top = 48
  end
end
