inherited FramePessoaContato: TFramePessoaContato
  Width = 601
  inherited cxpcMain: TcxPageControl
    Width = 601
    Properties.ActivePage = cxtsData
    ClientRectRight = 601
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 601
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          601
          240)
        Width = 601
        inherited dxBevel1: TdxBevel
          Width = 597
          ExplicitWidth = 637
        end
        object Label1: TLabel
          Left = 8
          Top = 39
          Width = 20
          Height = 13
          Caption = 'Tipo'
        end
        object Label2: TLabel
          Left = 221
          Top = 39
          Width = 39
          Height = 13
          Caption = 'Contato'
        end
        object Label3: TLabel
          Left = 8
          Top = 64
          Width = 58
          Height = 13
          Caption = 'Observa'#231#227'o'
        end
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object EdtContato: TgbDBTextEdit
          Left = 264
          Top = 35
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'CONTATO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbRequired = True
          gbPassword = False
          ExplicitWidth = 25
          Width = 332
        end
        object gbDBBlobEdit1: TgbDBBlobEdit
          Left = 72
          Top = 60
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OBSERVACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ClearKey = 16430
          Properties.ImmediatePost = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          ExplicitWidth = 217
          Width = 524
        end
        object gbDBTextEditPK1: TgbDBTextEditPK
          Left = 72
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object gbDBComboBox1: TgbDBComboBox
          Left = 72
          Top = 35
          DataBinding.DataField = 'TIPO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.Items.Strings = (
            'TELEFONE'
            'FAX'
            'EMAIL'
            'FACEBOOK'
            'LINKEDIN'
            'SITE'
            'SKYPE'
            'TWITTER'
            'VIBER'
            'WHATSAPP')
          Properties.PostPopupValueOnTab = True
          Properties.OnEditValueChanged = gbDBComboBox1PropertiesEditValueChanged
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbRequired = True
          Width = 145
        end
        object checkboxEmailEmissaoDocumentoFiscal: TgbDBCheckBox
          Left = 380
          Top = 5
          Anchors = [akTop, akRight]
          Caption = 'Email para emiss'#227'o de documento fiscal?'
          DataBinding.DataField = 'BO_EMAIL_ENVIO_DOCUMENTO_FISCAL'
          DataBinding.DataSource = dsDataFrame
          Properties.ImmediatePost = True
          Properties.ValueChecked = 'S'
          Properties.ValueUnchecked = 'N'
          Style.BorderStyle = ebsOffice11
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          Transparent = True
          Width = 216
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Contatos'
    Style.IsFontAssigned = True
    Width = 601
  end
  inherited ActionListMain: TActionList
    Left = 367
    Top = 8
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 338
    Top = 8
    DockControlHeights = (
      0
      0
      22
      0)
    inherited barNavegador: TdxBar
      DockedLeft = 328
    end
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaContato
    Left = 310
    Top = 8
  end
end
