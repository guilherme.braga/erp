inherited FrameFechamentoCrediario: TFrameFechamentoCrediario
  Width = 1118
  Height = 441
  ExplicitWidth = 1118
  ExplicitHeight = 441
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 1118
    ExplicitHeight = 441
    Height = 441
    Width = 1118
    object PnParcelamentoFechamento: TcxGroupBox
      Left = 345
      Top = 2
      Align = alClient
      PanelStyle.Active = True
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Height = 437
      Width = 771
      object PnParcelamento: TcxGroupBox
        Left = 2
        Top = 2
        Align = alClient
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Height = 433
        Width = 767
        object gridParcelas: TcxGrid
          AlignWithMargins = True
          Left = 5
          Top = 58
          Width = 757
          Height = 170
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Images = DmAcesso.cxImage16x16
          ParentFont = False
          PopupMenu = RadialPopUpMenuTransientePadrao
          TabOrder = 2
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object viewParcelas: TcxGridDBBandedTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Images = DmAcesso.cxImage16x16
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsParcelas
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Images = DmAcesso.cxImage16x16
            OptionsBehavior.NavigatorHints = True
            OptionsBehavior.ExpandMasterRowOnDblClick = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsCustomize.NestedBands = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.ShowColumnFilterButtons = sfbAlways
            OptionsView.BandHeaders = False
            Styles.ContentOdd = DmAcesso.OddColor
            Bands = <
              item
              end>
            object viewParcelasNR_PARCELA: TcxGridDBBandedColumn
              DataBinding.FieldName = 'NR_PARCELA'
              HeaderAlignmentHorz = taCenter
              HeaderGlyphAlignmentHorz = taCenter
              Options.Editing = False
              Options.Sorting = False
              Width = 50
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object viewParcelasDT_VENCIMENTO: TcxGridDBBandedColumn
              DataBinding.FieldName = 'DT_VENCIMENTO'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 84
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object viewParcelasIC_DIAS: TcxGridDBBandedColumn
              DataBinding.FieldName = 'IC_DIAS'
              HeaderAlignmentHorz = taCenter
              HeaderGlyphAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 58
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object viewParcelasID_FORMA_PAGAMENTO: TcxGridDBBandedColumn
              DataBinding.FieldName = 'ID_FORMA_PAGAMENTO'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = viewParcelasID_FORMA_PAGAMENTOPropertiesButtonClick
              Properties.OnEditValueChanged = viewParcelasID_FORMA_PAGAMENTOPropertiesEditValueChanged
              Visible = False
              Options.Sorting = False
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object viewParcelasJOIN_DESCRICAO_FORMA_PAGAMENTO: TcxGridDBBandedColumn
              DataBinding.FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = True
              Properties.OnButtonClick = viewParcelasID_FORMA_PAGAMENTOPropertiesButtonClick
              Properties.OnEditValueChanged = viewParcelasID_FORMA_PAGAMENTOPropertiesEditValueChanged
              Options.Sorting = False
              Width = 242
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object viewParcelasVL_TITULO: TcxGridDBBandedColumn
              DataBinding.FieldName = 'VL_TITULO'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '###,###,###,##0.00'
              Properties.ImmediatePost = True
              Properties.OnEditValueChanged = viewParcelasVL_TITULOPropertiesEditValueChanged
              Options.Sorting = False
              Width = 95
              Position.BandIndex = 0
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
            object viewParcelasID_CARTEIRA: TcxGridDBBandedColumn
              Caption = 'C'#243'digo da Carteira'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = viewParcelasID_CARTEIRAPropertiesButtonClick
              Properties.OnEditValueChanged = viewParcelasID_CARTEIRAPropertiesEditValueChanged
              Visible = False
              Width = 82
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object viewParcelasJOIN_DESCRICAO_CARTEIRA: TcxGridDBBandedColumn
              Caption = 'Carteira'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = viewParcelasID_CARTEIRAPropertiesButtonClick
              Properties.OnEditValueChanged = viewParcelasID_CARTEIRAPropertiesEditValueChanged
              Width = 62
              Position.BandIndex = 0
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = viewParcelas
          end
        end
        object PnPlanoPagamento: TgbPanel
          Left = 2
          Top = 27
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 28
          Width = 763
          object cxLabel2: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Plano de Pagamento'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 142
            AnchorY = 14
          end
          object EdtIdPlanoPagamento: TgbDBButtonEditFK
            Left = 144
            Top = 2
            Align = alLeft
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 1
            OnExit = EdtIdPlanoPagamentoExit
            gbTextEdit = EdtDescricaoPlanoPagamento
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO'
            gbTableName = 'PLANO_PAGAMENTO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'PLANO_PAGAMENTO'
            ExplicitHeight = 21
            Width = 58
          end
          object EdtDescricaoPlanoPagamento: TgbDBTextEdit
            Left = 202
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 559
          end
        end
        object PnTituloParcelamento: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Parcelamento'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 19
          Width = 757
        end
        inline FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque
          Left = 2
          Top = 279
          Width = 763
          Height = 103
          Align = alBottom
          TabOrder = 3
          ExplicitLeft = 2
          ExplicitTop = 279
          ExplicitWidth = 763
          ExplicitHeight = 103
          inherited cxGBDadosMain: TcxGroupBox
            ExplicitWidth = 763
            ExplicitHeight = 103
            Height = 103
            Width = 763
            inherited PnTituloCheque: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 759
              Width = 759
            end
            inherited PnDados: TgbPanel
              ExplicitWidth = 759
              ExplicitHeight = 80
              Height = 80
              Width = 759
              inherited PnDadosDireita: TgbPanel
                Left = 555
                ExplicitLeft = 555
                ExplicitHeight = 76
                Height = 76
                inherited EdtValor: TgbDBCalcEdit
                  OnExit = FrameTipoQuitacaoChequegbDBCalcEdit1Exit
                end
              end
              inherited PnDadosEsquerda: TgbPanel
                ExplicitWidth = 553
                ExplicitHeight = 76
                Height = 76
                Width = 553
                inherited PnTopo3PainelEsquerda: TgbPanel
                  ExplicitWidth = 549
                  Width = 549
                  inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
                    Left = 404
                    ExplicitLeft = 404
                    inherited labelNumero: TLabel
                      Left = 3
                      ExplicitLeft = 3
                    end
                  end
                  inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
                    ExplicitWidth = 402
                    Width = 402
                    inherited edtContaCorrente: TgbDBTextEdit
                      ExplicitWidth = 201
                      Width = 201
                    end
                  end
                end
                inherited PnTopo1PainelEsquerda: TgbPanel
                  ExplicitWidth = 549
                  Width = 549
                  inherited PnSacado: TgbPanel
                    ExplicitWidth = 429
                    Width = 429
                    inherited edtSacado: TgbDBTextEdit
                      ExplicitWidth = 503
                      Width = 503
                    end
                  end
                end
                inherited PnTopo2PainelEsquerda: TgbPanel
                  ExplicitWidth = 549
                  Width = 549
                  inherited labelDtEmissao: TLabel
                    Left = 410
                    ExplicitLeft = 410
                  end
                  inherited edtBanco: TgbDBTextEdit
                    ExplicitWidth = 357
                    Width = 357
                  end
                  inherited edtDtEmissao: TgbDBDateEdit
                    Left = 470
                    ExplicitLeft = 470
                  end
                end
              end
            end
          end
        end
        inline FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao
          Left = 2
          Top = 231
          Width = 763
          Height = 48
          Align = alBottom
          TabOrder = 4
          ExplicitLeft = 2
          ExplicitTop = 231
          ExplicitWidth = 763
          inherited cxGBDadosMain: TcxGroupBox
            ExplicitWidth = 763
            Width = 763
            inherited PnTituloCartao: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 759
              Width = 759
            end
            inherited PnDados: TgbPanel
              ExplicitWidth = 759
              Width = 759
              inherited PnOperadoraCartao: TgbPanel
                ExplicitWidth = 428
                Width = 428
                inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
                  ExplicitWidth = 262
                  Width = 262
                end
              end
              inherited PnValor: TgbPanel
                Left = 594
                ExplicitLeft = 594
                inherited EdtValor: TgbDBCalcEdit
                  OnExit = FrameTipoQuitacaoCartaoEdtValorExit
                end
              end
            end
          end
        end
        inline FrameTipoQuitacaoCrediario: TFrameTipoQuitacaoCrediario
          Left = 2
          Top = 382
          Width = 763
          Height = 49
          Align = alBottom
          TabOrder = 5
          ExplicitLeft = 2
          ExplicitTop = 382
          ExplicitWidth = 763
          inherited cxGBDadosMain: TcxGroupBox
            ExplicitWidth = 763
            Width = 763
            inherited PnTituloCartao: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 759
              Width = 759
            end
            inherited PnCampos: TgbPanel
              ExplicitWidth = 759
              Width = 759
              inherited PnHistorico: TgbPanel
                ExplicitWidth = 432
                Width = 432
                inherited EdtHistorico: TgbDBTextEdit
                  ExplicitWidth = 384
                  Width = 384
                end
              end
              inherited PnValor: TgbPanel
                Left = 599
                ExplicitLeft = 599
                inherited EdtValor: TgbDBCalcEdit
                  OnExit = FrameTipoQuitacaoCrediarioEdtValorExit
                end
              end
            end
          end
        end
      end
    end
    object PnLateral: TcxGroupBox
      Left = 2
      Top = 2
      Align = alLeft
      PanelStyle.Active = True
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Height = 437
      Width = 343
      object PnCreditoPessoa: TcxGroupBox
        Left = 2
        Top = 138
        Align = alTop
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Height = 108
        Width = 339
        object PnCreditoPessoaUtilizado: TgbPanel
          Left = 2
          Top = 55
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 28
          Width = 335
          object EdtCreditoPessoaUtilizado: TgbDBTextEdit
            Left = 90
            Top = 2
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            OnExit = EventoCalcularSaldoCreditoPessoa
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object cxLabel3: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Utilizado'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
        object PnCreditoPessoaSaldo: TgbPanel
          Left = 2
          Top = 83
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Height = 28
          Width = 335
          object EdtCreditoPessoaSaldo: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object cxLabel7: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Saldo'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
        object PnCreditoPessoaDisponivel: TgbPanel
          Left = 2
          Top = 27
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 28
          Width = 335
          object EdtCreditoDisponivel: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object cxLabel6: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Dispon'#237'vel'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
        object PnTituloCreditoPessoa: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Cr'#233'dito de Pessoa'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          Transparent = True
          Height = 19
          Width = 329
        end
      end
      object PnTotalGeral: TcxGroupBox
        Left = 2
        Top = 2
        Align = alTop
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Height = 136
        Width = 339
        object PnTituloTotalGeral: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Total Geral'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 19
          Width = 329
        end
        object PnValorBruto: TgbPanel
          Left = 2
          Top = 27
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 28
          Width = 335
          object EdtValorBruto: TgbDBTextEdit
            Left = 92
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 241
          end
          object cxLabel1: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Valor Bruto'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 90
            AnchorY = 14
          end
        end
        object PnDesconto: TgbPanel
          Left = 2
          Top = 83
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          Transparent = True
          Height = 28
          Width = 335
          object EdtDesconto: TgbDBTextEdit
            Left = 90
            Top = 2
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            OnExit = CalcularValorLiquido
            gbPassword = False
            ExplicitHeight = 21
            Width = 178
          end
          object EdtPercDesconto: TgbDBTextEdit
            Left = 268
            Top = 2
            Align = alRight
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            OnExit = CalcularValorLiquido
            gbPassword = False
            ExplicitHeight = 21
            Width = 65
          end
          object lbDesconto: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Desconto'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
        object PnAcrescimo: TgbPanel
          Left = 2
          Top = 55
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Visible = False
          Height = 28
          Width = 335
          object EdtAcrescimo: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            OnExit = CalcularValorLiquido
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 178
          end
          object lbAcrescimo: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Acr'#233'scimo'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
          object EdtPercAcrescimo: TgbDBTextEdit
            Left = 268
            Top = 2
            TabStop = False
            Align = alRight
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            OnExit = CalcularValorLiquido
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 65
          end
        end
        object PnValorLiquido: TgbPanel
          Left = 2
          Top = 111
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          Transparent = True
          Height = 28
          Width = 335
          object EdtValorLiquido: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object lbValorLiquido: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Valor L'#237'quido'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
      end
      object PnTroco: TcxGroupBox
        Left = 2
        Top = 326
        Align = alClient
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        Height = 109
        Width = 339
        object PnValorTroco: TgbPanel
          Left = 2
          Top = 27
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 28
          Width = 335
          object cxLabel8: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Troco'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
          object EdtTroco: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
        end
        object mmTroco: TcxMemo
          Left = 2
          Top = 55
          Align = alClient
          Lines.Strings = (
            'cxMemo1')
          ParentFont = False
          Style.Color = clCream
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -19
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          TabOrder = 1
          Height = 52
          Width = 335
        end
        object gbPanel11: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Troco'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Height = 19
          Width = 329
        end
      end
      object cxGroupBox5: TcxGroupBox
        Left = 2
        Top = 246
        Align = alTop
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        Height = 80
        Width = 339
        object PnPagamentoDinheiro: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Pagamento em Dinheiro'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Height = 19
          Width = 329
        end
        object gbPanel1: TgbPanel
          Left = 2
          Top = 27
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 28
          Width = 335
          object EdtValorPagamentoDinheiro: TgbDBTextEdit
            Left = 90
            Top = 2
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            OnExit = EdtValorPagamentoDinheiroExit
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object cxLabel10: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Valor'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
        object PnDiferenca: TgbPanel
          Left = 2
          Top = 55
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 28
          Width = 335
          object EdtDiferenca: TgbDBTextEdit
            Left = 90
            Top = 2
            TabStop = False
            Align = alClient
            DataBinding.DataSource = dsFechamento
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 243
          end
          object cxLabel11: TcxLabel
            Left = 2
            Top = 2
            Align = alLeft
            AutoSize = False
            Caption = 'Diferen'#231'a'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 24
            Width = 88
            AnchorY = 14
          end
        end
      end
    end
  end
  object dsFechamento: TDataSource
    Left = 416
    Top = 184
  end
  object dsParcelas: TDataSource
    Left = 512
    Top = 176
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 596
    Top = 160
    object ActAlterar_Colunas_Grid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterar_Colunas_GridExecute
    end
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
  end
  object dxBarManagerFechamentoCrediario: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Manager')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    FlatCloseButton = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    PopupMenuLinks = <>
    ShowHelpButton = True
    ShowShortCutInHint = True
    Style = bmsOffice11
    UseF10ForMenu = False
    UseSystemFont = True
    Left = 594
    Top = 200
    DockControlHeights = (
      0
      0
      0
      0)
    object bbAlterarRotuloColunas: TdxBarButton
      Action = ActAlterar_Colunas_Grid
      Category = 0
    end
    object bbSalvarConfiguracoes: TdxBarButton
      Action = ActSalvarConfiguracoes
      Category = 0
    end
    object bbExibirAgrupamento: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 0
    end
  end
  object RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
        ItemName = 'bbAlterarRotuloColunas'
      end
      item
        Visible = True
        ItemName = 'bbExibirAgrupamento'
      end
      item
        Visible = True
        ItemName = 'bbSalvarConfiguracoes'
      end>
    Images = DmAcesso.cxImage16x16
    UseOwnFont = False
    Left = 504
    Top = 120
  end
end
