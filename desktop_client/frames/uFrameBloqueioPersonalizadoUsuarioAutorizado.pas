unit uFrameBloqueioPersonalizadoUsuarioAutorizado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxContainer, dxBar, dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset,
  Vcl.Menus, dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls, JvButton,
  JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDBEdit, uGBDBTextEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, Vcl.StdCtrls, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameBloqueioPersonalizadoUsuarioAutorizado = class(TFrameDetailTransientePadrao)
    Label1: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
  private
    { Private declarations }
  public
    procedure AoCriarFrame; override;
    procedure AntesDeConfirmar; override;
  end;

var
  FrameBloqueioPersonalizadoUsuarioAutorizado: TFrameBloqueioPersonalizadoUsuarioAutorizado;

implementation

{$R *.dfm}

uses uCadBloqueioPersonalizado, uTControl_Function;

{ TFrameBloqueioPersonalizadoUsuarioAutorizado }

procedure TFrameBloqueioPersonalizadoUsuarioAutorizado.AntesDeConfirmar;
var
  idBloqueioPersonalizado: Integer;
begin
  idBloqueioPersonalizado := TCadBloqueioPersonalizado(
    TControlFunction.GetInstance('TCadBloqueioPersonalizado')).cdsData.GetAsInteger('ID');

  if idBloqueioPersonalizado > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_BLOQUEIO_PERSONALIZADO', idBloqueioPersonalizado);
  end;
end;

procedure TFrameBloqueioPersonalizadoUsuarioAutorizado.AoCriarFrame;
begin
  inherited;
  PnTituloFrameDetailPadrao.Visible := false;
end;

end.
