inherited FrameQuitacaoContaReceberTipoCartao: TFrameQuitacaoContaReceberTipoCartao
  inherited cxGBDadosMain: TcxGroupBox
    inherited edtDtVencimento: TgbDBDateEdit
      TabStop = False
      DataBinding.DataField = 'DT_QUITACAO'
      DataBinding.DataSource = dsDataFrame
      Properties.ReadOnly = True
      Style.Color = clGradientActiveCaption
      gbReadyOnly = True
    end
    inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
      DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      DataBinding.DataSource = dsDataFrame
    end
    inherited PnTituloCartao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited EdtValor: TgbDBCalcEdit
      TabStop = False
      DataBinding.DataField = 'VL_TOTAL'
      DataBinding.DataSource = dsDataFrame
      Properties.ReadOnly = True
      Style.Color = clGradientActiveCaption
      gbReadyOnly = True
    end
    inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
      DataBinding.DataField = 'ID_OPERADORA_CARTAO'
      DataBinding.DataSource = dsDataFrame
    end
  end
  object dsDataFrame: TDataSource
    DataSet = MovContaReceber.cdsContaReceberQuitacao
    Left = 360
    Top = 8
  end
end
