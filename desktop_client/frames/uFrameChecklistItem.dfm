inherited FrameChecklistItem: TFrameChecklistItem
  inherited cxpcMain: TcxPageControl
    ExplicitWidth = 628
    ExplicitHeight = 290
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 628
      ExplicitHeight = 266
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 628
        Height = 266
        ExplicitWidth = 628
        ExplicitHeight = 266
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 775
        end
        object Label2: TLabel
          Left = 14
          Top = 37
          Width = 46
          Height = 13
          Caption = 'Descri'#231#227'o'
        end
        object Label1: TLabel
          Left = 14
          Top = 10
          Width = 22
          Height = 13
          Caption = 'Item'
        end
        object edDescricao: TgbDBTextEdit
          Left = 66
          Top = 34
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'DESCRICAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbRequired = True
          gbPassword = False
          ExplicitWidth = 496
          Width = 319
        end
        object gbDBSpinEdit1: TgbDBSpinEdit
          Left = 66
          Top = 6
          DataBinding.DataField = 'NR_ITEM'
          DataBinding.DataSource = dsDataFrame
          Properties.MaxValue = 9999999.000000000000000000
          Properties.MinValue = 1.000000000000000000
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbRequired = True
          Width = 55
        end
        object gbDBCheckBox1: TgbDBCheckBox
          Left = 385
          Top = 34
          Anchors = [akTop, akRight]
          Caption = 'Ativo?'
          DataBinding.DataField = 'BO_ATIVO'
          DataBinding.DataSource = dsDataFrame
          Properties.ImmediatePost = True
          Properties.ValueChecked = 'S'
          Properties.ValueUnchecked = 'N'
          Style.BorderStyle = ebsOffice11
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          Transparent = True
          Width = 57
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Itens do Check List'
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadChecklist.cdsChecklistItem
  end
end
