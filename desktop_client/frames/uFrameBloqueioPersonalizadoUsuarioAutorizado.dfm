inherited FrameBloqueioPersonalizadoUsuarioAutorizado: TFrameBloqueioPersonalizadoUsuarioAutorizado
  inherited cxpcMain: TcxPageControl
    Top = 57
    Height = 248
    ExplicitTop = 57
    ExplicitHeight = 248
    ClientRectBottom = 248
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 224
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 224
        ExplicitHeight = 224
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 224
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 224
        Height = 224
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Usu'#225'rios Autorizados'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 38
    Height = 38
    object Label1: TLabel [0]
      Left = 8
      Top = 10
      Width = 36
      Height = 13
      Caption = 'Usu'#225'rio'
    end
    inherited gbPanel3: TgbPanel
      Left = 367
      Top = 4
      Anchors = [akTop, akRight]
      ExplicitLeft = 367
      ExplicitTop = 4
    end
    object gbDBButtonEditFK1: TgbDBButtonEditFK
      Left = 50
      Top = 6
      DataBinding.DataField = 'ID_PESSOA_USUARIO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 112
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 1
      gbTextEdit = gbDBTextEdit1
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 'ID_PESSOA_USUARIO;JOIN_NOME_PESSOA_USUARIO'
      gbTableName = 'PESSOA_USUARIO'
      gbCamposConsulta = 'ID;USUARIO'
      gbIdentificadorConsulta = 'PESSOA_USUARIO'
      Width = 60
    end
    object gbDBTextEdit1: TgbDBTextEdit
      Left = 107
      Top = 6
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'JOIN_NOME_PESSOA_USUARIO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 256
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadBloqueioPersonalizado.cdsBloqueioPersonalizadoUsuarioLiberacao
  end
end
