inherited FramePessoaSetor: TFramePessoaSetor
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 652
        end
        object lbCodigo: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object lbDescricao: TLabel
          Left = 8
          Top = 40
          Width = 26
          Height = 13
          Caption = 'Setor'
        end
        object lbQtdeFuncionarios: TLabel
          Left = 304
          Top = 40
          Width = 60
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Funcion'#225'rios'
          ExplicitLeft = 348
        end
        object edtCodigo: TgbDBTextEditPK
          Left = 44
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object edtQtFuncionarios: TgbDBTextEdit
          Left = 368
          Top = 36
          Anchors = [akTop, akRight]
          DataBinding.DataField = 'QUANTIDADE_FUNCIONARIOS'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbPassword = False
          Width = 69
        end
        object descCidade: TgbDBTextEdit
          Left = 103
          Top = 36
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_SETOR_COMERCIAL'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 195
        end
        object edtCidade: TgbDBButtonEditFK
          Left = 44
          Top = 36
          DataBinding.DataField = 'ID_SETOR_COMERCIAL'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Style.Color = 14606074
          TabOrder = 1
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_SETOR_COMERCIAL;JOIN_DESCRICAO_SETOR_COMERCIAL'
          gbTableName = 'SETOR_COMERCIAL'
          gbCamposConsulta = 'ID;DESCRICAO'
          Width = 62
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Setores'
    Style.IsFontAssigned = True
  end
  inherited ActionListMain: TActionList
    Left = 599
    Top = 56
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 570
    Top = 56
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaSetor
    Left = 542
    Top = 56
  end
  inherited ActionListAssistent: TActionList
    Left = 452
    Top = 56
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 424
    Top = 56
  end
end
