inherited FrameProdutoGrade: TFrameProdutoGrade
  Width = 775
  Height = 435
  ExplicitWidth = 775
  ExplicitHeight = 435
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 775
    ExplicitHeight = 435
    Height = 435
    Width = 775
    object gbPanel1: TgbPanel
      Left = 2
      Top = 55
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 378
      Width = 771
      object gridProdutoGrade: TcxGrid
        Left = 2
        Top = 2
        Width = 767
        Height = 374
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object viewProdutoGrade: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Visible = False
          Navigator.Buttons.Delete.Visible = True
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          DataController.DataSource = dsProdutoGrade
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.Active = True
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.ApplyChanges = fracImmediately
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.Indicator = True
          OptionsView.ShowColumnFilterButtons = sfbAlways
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
              Caption = 'Cores e Tamanhos'
            end>
        end
        object gridLevelProdutoGrade: TcxGridLevel
          GridView = viewProdutoGrade
        end
      end
    end
    object pnFiltros: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      DesignSize = (
        771
        53)
      Height = 53
      Width = 771
      object Label1: TLabel
        Left = 7
        Top = 32
        Width = 29
        Height = 13
        Caption = 'Grade'
      end
      object Label2: TLabel
        Left = 6
        Top = 7
        Width = 38
        Height = 13
        Caption = 'Produto'
      end
      object gbDBTextEdit1: TgbDBTextEdit
        Left = 104
        Top = 28
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'DESCRICAO_PRODUTO_GRADE'
        DataBinding.DataSource = dsFiltros
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        gbReadyOnly = True
        gbPassword = False
        Width = 661
      end
      object gbDBButtonEditFK1: TgbDBButtonEditFK
        Left = 48
        Top = 28
        DataBinding.DataField = 'ID_PRODUTO_GRADE'
        DataBinding.DataSource = dsFiltros
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = 14606074
        TabOrder = 2
        gbTextEdit = gbDBTextEdit1
        gbRequired = True
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_PRODUTO_GRADE;DESCRICAO_PRODUTO_GRADE'
        gbTableName = 'GRADE_PRODUTO'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbClasseDoCadastro = 'TCadGradeProduto'
        gbIdentificadorConsulta = 'GRADE_PRODUTO'
        Width = 59
      end
      object gbDBTextEdit2: TgbDBTextEdit
        Left = 104
        Top = 3
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'DESCRICAO_PRODUTO_AGRUPADOR'
        DataBinding.DataSource = dsFiltros
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbReadyOnly = True
        gbPassword = False
        Width = 661
      end
      object CampoProduto: TgbDBButtonEditFK
        Left = 48
        Top = 3
        DataBinding.DataField = 'ID_PRODUTO_AGRUPADOR'
        DataBinding.DataSource = dsFiltros
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Properties.ReadOnly = False
        Style.Color = 14606074
        TabOrder = 0
        gbRequired = True
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_PRODUTO_AGRUPADOR;DESCRICAO_PRODUTO_AGRUPADOR'
        gbTableName = 'PRODUTO'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbIdentificadorConsulta = 'PRODUTO'
        Width = 60
      end
    end
  end
  object dsProdutoGrade: TDataSource
    DataSet = fdmProdutoGrade
    Left = 108
    Top = 152
  end
  object fdmProdutoGrade: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 80
    Top = 152
  end
  object dsFiltros: TDataSource
    DataSet = fdmFiltros
    Left = 84
    Top = 96
  end
  object fdmFiltros: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 56
    Top = 96
    object fdmFiltrosID_PRODUTO_AGRUPADOR: TIntegerField
      FieldName = 'ID_PRODUTO_AGRUPADOR'
      OnChange = EventoConstruirGradeSeCamposObrigatoriosEstiveremPreenchidos
    end
    object fdmFiltrosDESCRICAO_PRODUTO_AGRUPADOR: TStringField
      FieldName = 'DESCRICAO_PRODUTO_AGRUPADOR'
      Size = 255
    end
    object fdmFiltrosID_PRODUTO_GRADE: TIntegerField
      FieldName = 'ID_PRODUTO_GRADE'
      OnChange = EventoConstruirGradeSeCamposObrigatoriosEstiveremPreenchidos
    end
    object fdmFiltrosDESCRICAO_PRODUTO_GRADE: TStringField
      FieldName = 'DESCRICAO_PRODUTO_GRADE'
      Size = 255
    end
  end
end
