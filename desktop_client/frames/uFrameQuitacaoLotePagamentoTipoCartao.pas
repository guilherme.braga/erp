unit uFrameQuitacaoLotePagamentoTipoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCartao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxCalc, uGBDBCalcEdit, uGBPanel,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxGroupBox, db;

type
  TFrameQuitacaoLotePagamentoTipoCartao = class(TFrameTipoQuitacaoCartao)
    dsFrame: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoLotePagamentoTipoCartao: TFrameQuitacaoLotePagamentoTipoCartao;

implementation

{$R *.dfm}

uses uMovLotePagamento;

{ TFrameQuitacaoLotePagamentoTipoCartao }

procedure TFrameQuitacaoLotePagamentoTipoCartao.SetDataset(ADataset: TDataset);
begin
  dsFrame.Dataset := ADataset;
end;

end.
