inherited FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao
  Width = 741
  Height = 48
  ExplicitWidth = 741
  ExplicitHeight = 48
  inherited cxGBDadosMain: TcxGroupBox
    ExplicitWidth = 741
    ExplicitHeight = 48
    Height = 48
    Width = 741
    object PnTituloCartao: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      Alignment = alCenterCenter
      Caption = 'Dados do Cart'#227'o'
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentBackground = False
      ParentFont = False
      Style.BorderStyle = ebsOffice11
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 19
      Width = 737
    end
    object PnDados: TgbPanel
      Left = 2
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 25
      Width = 737
      object PnDtVencimento: TgbPanel
        Left = 2
        Top = 2
        Align = alLeft
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Transparent = True
        Height = 21
        Width = 164
        object labelDtVencimento: TLabel
          Left = 2
          Top = 5
          Width = 73
          Height = 13
          Caption = 'Dt. Vencimento'
        end
        object edtDtVencimento: TgbDBDateEdit
          Left = 77
          Top = 1
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbRequired = True
          gbDateTime = False
          Width = 83
        end
      end
      object PnOperadoraCartao: TgbPanel
        Left = 166
        Top = 2
        Align = alClient
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        DesignSize = (
          406
          21)
        Height = 21
        Width = 406
        object Label1: TLabel
          Left = 2
          Top = 5
          Width = 103
          Height = 13
          Caption = 'Operadora de Cart'#227'o'
        end
        object EdtIdOperadoraCartao: TgbDBButtonEditFK
          Left = 109
          Top = 1
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Style.Color = 14606074
          TabOrder = 0
          gbTextEdit = EdtDescricaoOperadoraCartao
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_OPERADORA_CARTAO;JOIN_DESCRICAO_OPERADORA_CARTAO'
          gbTableName = 'OPERADORA_CARTAO'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'OPERADORA_CARTAO'
          Width = 60
        end
        object EdtDescricaoOperadoraCartao: TgbDBTextEdit
          Left = 166
          Top = 1
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_NOME_PESSOA'
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbReadyOnly = True
          gbPassword = False
          Width = 240
        end
      end
      object PnValor: TgbPanel
        Left = 572
        Top = 2
        Align = alRight
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        Transparent = True
        DesignSize = (
          163
          21)
        Height = 21
        Width = 163
        object labelVlCheque: TLabel
          Left = 6
          Top = 5
          Width = 24
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Valor'
        end
        object EdtValor: TgbDBCalcEdit
          Left = 34
          Top = 1
          Anchors = [akTop, akRight]
          Properties.DisplayFormat = '###,###,###,###,###,##0.00'
          Properties.ImmediatePost = True
          Properties.UseThousandSeparator = True
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbRequired = True
          Width = 127
        end
      end
    end
  end
end
