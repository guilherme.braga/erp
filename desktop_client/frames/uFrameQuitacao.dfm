inherited FrameQuitacao: TFrameQuitacao
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    Properties.Options = [pcoAlwaysShowGoDialogButton, pcoGradient, pcoNoArrows, pcoRedrawOnResize]
    ExplicitWidth = 1024
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        Align = alTop
        ExplicitHeight = 114
        Height = 114
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 992
        end
      end
      object PnQuitacaoCartao: TgbPanel
        Left = 0
        Top = 214
        Align = alTop
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        Height = 49
        Width = 451
      end
      object PnQuitacaoCheque: TgbPanel
        Left = 0
        Top = 114
        Align = alTop
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        Transparent = True
        Height = 100
        Width = 451
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
end
