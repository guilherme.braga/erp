unit uFrameNotaFiscalItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxContainer, dxBar, dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset,
  Vcl.Menus, dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls, JvButton,
  JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDBEdit, uGBDBTextEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, Vcl.StdCtrls, ugbDBValorComPercentual, ugbDBCalcEdit,
  cxDropDownEdit, cxCalc, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameNotaFiscalItem = class(TFrameDetailTransientePadrao)
    Label6: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    CampoProduto: TgbDBButtonEditFK;
    EdtDescricaoProduto: TgbDBTextEdit;
    EdtQuantidade: TgbDBCalcEdit;
    EdtUN: TgbDBTextEdit;
    EdtEstoque: TgbDBTextEdit;
    EdtValorUnitario: TgbDBCalcEdit;
    EdtDesconto: TgbDBValorComPercentual;
    EdtAcrescimo: TgbDBValorComPercentual;
    EdtFrete: TgbDBValorComPercentual;
    EdtSeguro: TgbDBValorComPercentual;
    EdtICMS: TgbDBValorComPercentual;
    EdtIPI: TgbDBValorComPercentual;
    EdtValorLiquidoImposto: TgbDBCalcEdit;
    EdtValorLiquido: TgbDBCalcEdit;
    procedure CampoProdutogbDepoisDeConsultar(Sender: TObject);
    procedure EdtDescontoExit(Sender: TObject);
    procedure EdtIPIExit(Sender: TObject);
  private
    FDatasetNotaFiscal: TgbClientDataset;
  public
    procedure SetConfiguracoesIniciais(ANotaFiscal, ANotaFiscalProduto: TgbClientDataset); overload;
    procedure GerenciarCamposDeAcordoComTipoNotaFiscal;
  protected
    procedure AntesDeConfirmar; override;
    procedure DefinirCamposNaoValorarNoSource(
      var AComaTextCamposNaoValorarNoSource: String); override;
  end;

var
  FrameNotaFiscalItem: TFrameNotaFiscalItem;

implementation

{$R *.dfm}

uses uMovNotaFiscal, uTControl_Function, uNotaFiscal, uNotaFiscalProxy,
  uTabelaPreco, uTabelaPrecoProxy, FireDAC.Comp.Client, uSistema;

{ TFrameNotaFiscalItem }

procedure TFrameNotaFiscalItem.AntesDeConfirmar;
var
  idNotaFiscal: Integer;
begin
  idNotaFiscal := FDatasetNotaFiscal.GetAsInteger('ID');

  if idNotaFiscal > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_NOTA_FISCAL', idNotaFiscal);
  end;

  inherited;
end;

procedure TFrameNotaFiscalItem.CampoProdutogbDepoisDeConsultar(Sender: TObject);
var
  frmNotaFiscal: TMovNotaFiscal;
  produto: TTabelaPrecoProdutoProxy;
begin
  inherited;
  if FDatasetNotaFiscal.GetAsString('TIPO_NF').Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    TNotaFiscal.SetInformacoesFiscaisProduto(FDatasetNotaFiscal, TgbClientDataset(dsDataFrame.Dataset));
  end;

  frmNotaFiscal := (TControlFunction.GetInstance('TMovNotaFiscal') as TMovNotaFiscal);
  if (frmNotaFiscal.FParametroHabilitarEmissaoNFCE.ValorSim or
      frmNotaFiscal.FParametroHabilitarEmissaoNFE.ValorSim) and
    FDatasetNotaFiscal.GetAsString('TIPO_NF').Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    produto := TTabelaPreco.GetProdutoTabelaPreco(frmNotaFiscal.FParametroTabelaPrecoPadrao.AsInteger,
      dsDataFrame.Dataset.FieldByName('ID_PRODUTO').AsInteger, TSistema.Sistema.Filial.Fid);

    if produto.FId > 0 then
    begin
      dsDataFrame.Dataset.FieldByName('I_VUNCOM').AsFloat := produto.FVlVenda;
    end;
  end;
end;

procedure TFrameNotaFiscalItem.DefinirCamposNaoValorarNoSource(
  var AComaTextCamposNaoValorarNoSource: String);
begin
  inherited;
  AComaTextCamposNaoValorarNoSource := 'H_NITEM';
end;

procedure TFrameNotaFiscalItem.EdtDescontoExit(Sender: TObject);
begin
  inherited;
  if FDatasetNotaFiscal.GetAsString('TIPO_NF').Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    ActConfirm.Execute;
  end;
end;

procedure TFrameNotaFiscalItem.EdtIPIExit(Sender: TObject);
begin
  inherited;
  if FDatasetNotaFiscal.GetAsString('TIPO_NF').Equals(TNotaFiscalProxy.TIPO_NF_ENTRADA) then
  begin
    ActConfirm.Execute;
  end;
end;

procedure TFrameNotaFiscalItem.GerenciarCamposDeAcordoComTipoNotaFiscal;
var
  notaFiscalSaida: Boolean;
begin
  notaFiscalSaida := FDatasetNotaFiscal.GetAsString('TIPO_NF').Equals(TNotaFiscalProxy.TIPO_NF_SAIDA);

  EdtFrete.gbReadOnlyPercentual := notaFiscalSaida;
  EdtFrete.gbReadOnlyValor := notaFiscalSaida;

  EdtSeguro.gbReadOnlyPercentual := notaFiscalSaida;
  EdtSeguro.gbReadOnlyValor := notaFiscalSaida;

  EdtIPI.gbReadOnlyPercentual := notaFiscalSaida;
  EdtIPI.gbReadOnlyValor := notaFiscalSaida;

  EdtICMS.gbReadOnlyPercentual := notaFiscalSaida;
  EdtICMS.gbReadOnlyValor := notaFiscalSaida;
end;

procedure TFrameNotaFiscalItem.SetConfiguracoesIniciais(ANotaFiscal, ANotaFiscalProduto: TgbClientDataset);
begin
  FDatasetNotaFiscal := ANotaFiscal;
  SetConfiguracoesIniciais(ANotaFiscalProduto);
end;

end.
