inherited FrameQuitacaoLotePagamento: TFrameQuitacaoLotePagamento
  inherited cxpcMain: TcxPageControl
    ExplicitWidth = 451
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        inherited Level1BandedTableView1: TcxGridDBBandedTableView
          FilterRow.Visible = False
        end
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 161
        DesignSize = (
          451
          161)
        Height = 161
        inherited dxBevel1: TdxBevel
          Top = 6
          Height = 54
          ExplicitLeft = 2
          ExplicitTop = 6
          ExplicitWidth = 661
          ExplicitHeight = 54
        end
        object labelDtQuitacao: TLabel
          Left = 8
          Top = 11
          Width = 84
          Height = 13
          Caption = 'Data da Quita'#231#227'o'
        end
        object labelTipoQuitacao: TLabel
          Left = 8
          Top = 66
          Width = 81
          Height = 13
          Caption = 'Tipo da Quita'#231#227'o'
        end
        object labelContaCorrente: TLabel
          Left = 8
          Top = 91
          Width = 75
          Height = 13
          Caption = 'Conta Corrente'
        end
        object labelDescontos: TLabel
          Left = 342
          Top = 11
          Width = 50
          Height = 13
          Caption = 'Descontos'
        end
        object labelAcrescimos: TLabel
          Left = 547
          Top = 11
          Width = 53
          Height = 13
          Caption = 'Acr'#233'scimos'
        end
        object labelPercDesconto: TLabel
          Left = 532
          Top = 11
          Width = 11
          Height = 13
          Caption = '%'
        end
        object labelPercAcrescimos: TLabel
          Left = 743
          Top = 11
          Width = 11
          Height = 13
          Caption = '%'
        end
        object labelVlTotalQuitacao: TLabel
          Left = 8
          Top = 35
          Width = 29
          Height = 13
          Caption = 'Total'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object labelContaAnalise: TLabel
          Left = 429
          Top = 91
          Width = 66
          Height = 13
          Caption = 'Conta An'#225'lise'
        end
        object dxBevel2: TdxBevel
          Left = 2
          Top = 2
          Width = 447
          Height = 4
          Align = alTop
          Shape = dxbsLineBottom
          ExplicitTop = 3
          ExplicitWidth = 832
        end
        object labelCentroResultado: TLabel
          Left = 429
          Top = 66
          Width = 84
          Height = 13
          Caption = 'Centro Resultado'
        end
        object labelValor: TLabel
          Left = 197
          Top = 11
          Width = 24
          Height = 13
          Caption = 'Valor'
        end
        object labelQuitacao: TLabel
          Left = 197
          Top = 35
          Width = 43
          Height = 13
          Caption = 'Quita'#231#227'o'
        end
        object labelObservacao: TLabel
          Left = 9
          Top = 114
          Width = 58
          Height = 13
          Caption = 'Observa'#231#227'o'
        end
        object edtDtQuitacao: TgbDBDateEdit
          Left = 96
          Top = 7
          DataBinding.DataField = 'DT_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.DateButtons = [btnClear, btnToday]
          Properties.ImmediatePost = True
          Properties.ReadOnly = False
          Properties.SaveTime = False
          Properties.ShowTime = False
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          gbRequired = True
          gbDateTime = False
          Width = 97
        end
        object descTipoQuitacao: TgbDBTextEdit
          Left = 153
          Top = 62
          TabStop = False
          DataBinding.DataField = 'JOIN_DESCRICAO_TIPO_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 9
          gbReadyOnly = True
          gbPassword = False
          Width = 259
        end
        object edtIdTipoQuitacao: TgbDBButtonEditFK
          Left = 96
          Top = 62
          DataBinding.DataField = 'ID_TIPO_QUITACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 8
          gbTextEdit = descTipoQuitacao
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 
            'ID_TIPO_QUITACAO;JOIN_DESCRICAO_TIPO_QUITACAO;JOIN_TIPO_TIPO_QUI' +
            'TACAO'
          gbTableName = 'TIPO_QUITACAO'
          gbCamposConsulta = 'ID;DESCRICAO;TIPO'
          gbDepoisDeConsultar = edtIdTipoQuitacaogbDepoisDeConsultar
          gbIdentificadorConsulta = 'TIPO_QUITACAO'
          Width = 60
        end
        object descContaCorrente: TgbDBTextEdit
          Left = 153
          Top = 87
          TabStop = False
          DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 11
          gbReadyOnly = True
          gbPassword = False
          Width = 259
        end
        object edtIdContaCorrente: TgbDBButtonEditFK
          Left = 96
          Top = 87
          DataBinding.DataField = 'ID_CONTA_CORRENTE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 10
          gbTextEdit = descContaCorrente
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_CONTA_CORRENTE;JOIN_DESCRICAO_CONTA_CORRENTE'
          gbTableName = 'CONTA_CORRENTE'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'CONTA_CORRENTE'
          Width = 60
        end
        object edtVlDescontos: TgbDBTextEdit
          Left = 395
          Top = 7
          DataBinding.DataField = 'VL_DESCONTO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object edtVlAcrescimos: TgbDBTextEdit
          Left = 606
          Top = 7
          DataBinding.DataField = 'VL_ACRESCIMO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object edtPcDescontos: TgbDBTextEdit
          Left = 492
          Top = 7
          DataBinding.DataField = 'PERC_DESCONTO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbRequired = True
          gbPassword = False
          Width = 38
        end
        object edtPcAcrescimos: TgbDBTextEdit
          Left = 703
          Top = 7
          DataBinding.DataField = 'PERC_ACRESCIMO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          gbRequired = True
          gbPassword = False
          Width = 38
        end
        object edtVlTotalQuitacao: TgbDBTextEdit
          Left = 96
          Top = 31
          TabStop = False
          DataBinding.DataField = 'VL_TOTAL'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 6
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object descContaAnalise: TgbDBTextEdit
          Left = 573
          Top = 87
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 15
          gbReadyOnly = True
          gbPassword = False
          Width = 0
        end
        object EdtContaAnalise: TcxDBButtonEdit
          Left = 516
          Top = 87
          DataBinding.DataField = 'ID_CONTA_ANALISE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.ClickKey = 13
          Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
          Properties.OnEditValueChanged = EdtContaAnalisePropertiesEditValueChanged
          Style.Color = 14606074
          TabOrder = 14
          OnDblClick = EdtContaAnaliseDblClick
          Width = 60
        end
        object edtFkCentroResultado: TgbDBButtonEditFK
          Left = 516
          Top = 62
          DataBinding.DataField = 'ID_CENTRO_RESULTADO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 12
          gbTextEdit = descCentroResultado
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
          gbTableName = 'CENTRO_RESULTADO'
          gbCamposConsulta = 'ID;DESCRICAO'
          gbIdentificadorConsulta = 'CENTRO_RESULTADO'
          Width = 60
        end
        object descCentroResultado: TgbDBTextEdit
          Left = 573
          Top = 62
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 13
          gbReadyOnly = True
          gbPassword = False
          Width = 0
        end
        object edtValor: TgbDBTextEdit
          Left = 241
          Top = 7
          TabStop = False
          DataBinding.DataField = 'VL_ABERTO'
          DataBinding.DataSource = dsLote
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbReadyOnly = True
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object edtQuitacao: TgbDBTextEdit
          Left = 241
          Top = 31
          DataBinding.DataField = 'IC_VL_PAGAMENTO'
          DataBinding.DataSource = dsDataFrame
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = False
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 7
          OnExit = edtQuitacaoExit
          gbRequired = True
          gbPassword = False
          Width = 97
        end
        object lbTrocoDiferenca: TcxLabel
          Left = 342
          Top = 32
          Caption = 'lbTrocoDiferenca'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          AnchorY = 41
        end
        object edtObservacao: TgbDBBlobEdit
          Left = 96
          Top = 111
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OBSERVACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ClearKey = 16430
          Properties.ImmediatePost = True
          Properties.PopupHeight = 300
          Properties.PopupWidth = 160
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 16
          Width = 952
        end
        object PnDadosBaixaPorTitulo: TgbPanel
          Left = 9
          Top = 133
          Anchors = [akLeft, akTop, akRight]
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 17
          Transparent = True
          DesignSize = (
            1039
            23)
          Height = 23
          Width = 1039
          object labelDocumento: TLabel
            Left = 0
            Top = 6
            Width = 54
            Height = 13
            Caption = 'Documento'
          end
          object labelDescricao: TLabel
            Left = 420
            Top = 6
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object edtDocumento: TgbDBTextEdit
            Left = 87
            Top = 2
            DataBinding.DataField = 'DOCUMENTO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbPassword = False
            Width = 316
          end
          object edtDescricao: TgbDBTextEdit
            Left = 507
            Top = 2
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbPassword = False
            Width = 532
          end
        end
      end
      inherited PnQuitacaoCartao: TgbPanel
        Top = 161
        ExplicitTop = 161
        ExplicitHeight = 55
        Height = 55
      end
      inherited PnQuitacaoCheque: TgbPanel
        Top = 216
        ExplicitTop = 210
        ExplicitHeight = 112
        Height = 112
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Quita'#231#227'o'
    Style.IsFontAssigned = True
  end
  inherited ActionListMain: TActionList
    Left = 207
    Top = 320
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 178
    Top = 320
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    Left = 150
    Top = 320
  end
  inherited ActionListAssistent: TActionList
    Left = 84
    Top = 312
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 56
    Top = 312
  end
  object dsLote: TDataSource
    DataSet = MovLotePagamento.cdsData
    Left = 440
    Top = 176
  end
end
