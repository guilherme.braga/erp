unit uFrameEmpresaFilial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, cxMaskEdit, cxButtonEdit,
  cxDBEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, dxBar,
  dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset, Vcl.Menus,
  dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls,
  JvButton, JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDropDownEdit,
  cxCalendar, uGBDBDateEdit, uGBDBComboBox, cxCheckBox, uGBDBCheckBox, cxRadioGroup, uGBDBRadioGroup;

type
  TFrameEmpresaFilial = class(TFrameDetailTransientePadrao)
    PnLogradouro: TcxGroupBox;
    lbComplemento: TLabel;
    lbLogradouro: TLabel;
    lnNumero: TLabel;
    lbCEP: TLabel;
    Label1: TLabel;
    lbCidadeEmpresa: TLabel;
    edtLogradouro: TgbDBTextEdit;
    edtNumero: TgbDBTextEdit;
    edtIDCEP: TgbDBButtonEditFK;
    descCidade: TgbDBTextEdit;
    descBairro: TgbDBButtonEditFK;
    edtCidade: TgbDBButtonEditFK;
    edtComplemento: TgbDBTextEdit;
    PnDadosEmpresa: TcxGroupBox;
    lbRazaoSocial: TLabel;
    lbDtFundacao: TLabel;
    lbNomeFantasia: TLabel;
    lbCNPJ: TLabel;
    lbInscricaoEstadual: TLabel;
    edRazaoSocial: TgbDBTextEdit;
    edtDtFundacao: TgbDBDateEdit;
    edtNomeFantasia: TgbDBTextEdit;
    edtCNPJ: TgbDBTextEdit;
    edtInscricaoEstadual: TgbDBTextEdit;
    gbPanel1: TgbPanel;
    Label2: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    Label3: TLabel;
    gbDBComboBox1: TgbDBComboBox;
    Label4: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBRadioGroup2: TgbDBRadioGroup;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBCheckBox2: TgbDBCheckBox;
    gbDBCheckBox3: TgbDBCheckBox;
    Label5: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    Label6: TLabel;
    gbDBTextEdit4: TgbDBTextEdit;
    Label7: TLabel;
    gbDBTextEdit5: TgbDBTextEdit;
    Label8: TLabel;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
  private
    procedure ValidarDocumentoFederal(AField: TField);
  public
    { Public declarations }
  end;

var
  FrameEmpresaFilial: TFrameEmpresaFilial;

implementation

{$R *.dfm}

uses uTPessoa, uCadEmpresaFilial;

procedure TFrameEmpresaFilial.ValidarDocumentoFederal(AField: TField);
begin
  if not(AField.AsString.IsEmpty) and
   not(TPessoa.ValidaCnpjCeiCpf(AField.AsString))
  then
  begin
    AField.Clear;
    AField.FocusControl;
  end;
end;

end.
