inherited FramePessoaEndereco: TFramePessoaEndereco
  Width = 842
  inherited cxpcMain: TcxPageControl
    Width = 842
    Properties.ActivePage = cxtsData
    ClientRectRight = 842
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 1104
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 842
        ExplicitWidth = 1104
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          842
          240)
        Width = 842
        inherited dxBevel1: TdxBevel
          Width = 838
          ExplicitWidth = 767
        end
        object lbCodigo: TLabel
          Left = 8
          Top = 8
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object lbTipo: TLabel
          Left = 8
          Top = 36
          Width = 20
          Height = 13
          Caption = 'Tipo'
        end
        object lbObservacao: TLabel
          Left = 8
          Top = 115
          Width = 58
          Height = 13
          Caption = 'Observa'#231#227'o'
        end
        object lbComplemento: TLabel
          Left = 8
          Top = 89
          Width = 65
          Height = 13
          Caption = 'Complemento'
        end
        object lbLogradouro: TLabel
          Left = 168
          Top = 62
          Width = 55
          Height = 13
          Caption = 'Logradouro'
        end
        object lnNumero: TLabel
          Left = 604
          Top = 62
          Width = 37
          Height = 13
          Caption = 'N'#250'mero'
        end
        object lbCEP: TLabel
          Left = 8
          Top = 62
          Width = 19
          Height = 13
          Caption = 'CEP'
        end
        object Label1: TLabel
          Left = 739
          Top = 62
          Width = 28
          Height = 13
          Caption = 'Bairro'
        end
        object lbCidadeEmpresa: TLabel
          Left = 608
          Top = 89
          Width = 33
          Height = 13
          Caption = 'Cidade'
        end
        object edtCodigo: TgbDBTextEditPK
          Left = 72
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object cbTipo: TgbDBComboBox
          Left = 74
          Top = 33
          DataBinding.DataField = 'TIPO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.Items.Strings = (
            'RESIDENCIAL'
            'CONJUGE'
            'COMERCIAL'
            'ENTREGA'
            'COBRAN'#199'A'
            'REFERENCIA')
          Properties.PostPopupValueOnTab = True
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbRequired = True
          Width = 177
        end
        object edtObservacao: TgbDBBlobEdit
          Left = 74
          Top = 111
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OBSERVACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ClearKey = 16430
          Properties.ImmediatePost = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 9
          ExplicitWidth = 372
          Width = 763
        end
        object edtLogradouro: TgbDBTextEdit
          Left = 227
          Top = 58
          DataBinding.DataField = 'LOGRADOURO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbPassword = False
          Width = 371
        end
        object edtNumero: TgbDBTextEdit
          Left = 645
          Top = 58
          DataBinding.DataField = 'NUMERO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbPassword = False
          Width = 90
        end
        object edtIDCEP: TgbDBButtonEditFK
          Left = 74
          Top = 58
          DataBinding.DataField = 'CEP'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Style.Color = clWhite
          TabOrder = 2
          gbCampoPK = 'CEP'
          gbCamposRetorno = 'CEP'
          gbTableName = 'CEP'
          gbCamposConsulta = 'CEP'
          gbDepoisDeConsultar = edtIDCEPgbDepoisDeConsultar
          Width = 90
        end
        object descCidade: TgbDBTextEdit
          Left = 704
          Top = 85
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = False
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 8
          gbPassword = False
          ExplicitWidth = 162
          Width = 133
        end
        object descBairro: TgbDBButtonEditFK
          Left = 772
          Top = 58
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'BAIRRO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Properties.ReadOnly = False
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 5
          gbCampoPK = 'DESCRICAO'
          gbCamposRetorno = 'BAIRRO'
          gbTableName = 'BAIRRO'
          gbCamposConsulta = 'DESCRICAO'
          ExplicitWidth = 94
          Width = 65
        end
        object edtCidade: TgbDBButtonEditFK
          Left = 645
          Top = 85
          DataBinding.DataField = 'ID_CIDADE'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 13
          Style.Color = clWhite
          TabOrder = 7
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_CIDADE;JOIN_DESCRICAO_CIDADE'
          gbTableName = 'CIDADE'
          gbCamposConsulta = 'ID;DESCRICAO'
          Width = 62
        end
        object edtComplemento: TgbDBTextEdit
          Left = 74
          Top = 85
          DataBinding.DataField = 'COMPLEMENTO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = clWhite
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 6
          gbPassword = False
          Width = 524
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Endere'#231'os'
    Style.IsFontAssigned = True
    Transparent = False
    Width = 842
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = CadPessoa.cdsPessoaEndereco
  end
end
