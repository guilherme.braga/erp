unit uFrameFechamentoCrediario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox,
  Data.DB, cxLabel, cxTextEdit, cxDBEdit, uGBDBTextEdit, uGBPanel, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, cxMaskEdit,
  cxButtonEdit, uGBDBButtonEditFK, cxMemo, JvExControls, JvButton,
  JvTransparentButton, uFrameTipoQuitacaoCrediario, uFrameTipoQuitacaoCheque,
  uFrameTipoQuitacaoCartao, acbrTroco, ugbClientDataset, cxCalc, dxBar,
  dxRibbonRadialMenu, System.Actions, Vcl.ActnList, uFiltroFormulario, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameFechamentoCrediario = class(TFramePadrao)
    PnParcelamentoFechamento: TcxGroupBox;
    PnLateral: TcxGroupBox;
    PnCreditoPessoa: TcxGroupBox;
    PnCreditoPessoaUtilizado: TgbPanel;
    EdtCreditoPessoaUtilizado: TgbDBTextEdit;
    cxLabel3: TcxLabel;
    PnCreditoPessoaSaldo: TgbPanel;
    EdtCreditoPessoaSaldo: TgbDBTextEdit;
    cxLabel7: TcxLabel;
    dsFechamento: TDataSource;
    PnParcelamento: TcxGroupBox;
    PnCreditoPessoaDisponivel: TgbPanel;
    EdtCreditoDisponivel: TgbDBTextEdit;
    cxLabel6: TcxLabel;
    PnTotalGeral: TcxGroupBox;
    PnTituloTotalGeral: TgbPanel;
    PnValorBruto: TgbPanel;
    EdtValorBruto: TgbDBTextEdit;
    cxLabel1: TcxLabel;
    PnDesconto: TgbPanel;
    EdtDesconto: TgbDBTextEdit;
    EdtPercDesconto: TgbDBTextEdit;
    lbDesconto: TcxLabel;
    PnAcrescimo: TgbPanel;
    EdtAcrescimo: TgbDBTextEdit;
    lbAcrescimo: TcxLabel;
    EdtPercAcrescimo: TgbDBTextEdit;
    PnValorLiquido: TgbPanel;
    EdtValorLiquido: TgbDBTextEdit;
    lbValorLiquido: TcxLabel;
    PnTroco: TcxGroupBox;
    PnValorTroco: TgbPanel;
    cxLabel8: TcxLabel;
    EdtTroco: TgbDBTextEdit;
    mmTroco: TcxMemo;
    PnTituloCreditoPessoa: TgbPanel;
    gbPanel11: TgbPanel;
    cxGroupBox5: TcxGroupBox;
    PnPagamentoDinheiro: TgbPanel;
    gbPanel1: TgbPanel;
    EdtValorPagamentoDinheiro: TgbDBTextEdit;
    cxLabel10: TcxLabel;
    PnDiferenca: TgbPanel;
    EdtDiferenca: TgbDBTextEdit;
    cxLabel11: TcxLabel;
    dsParcelas: TDataSource;
    gridParcelas: TcxGrid;
    viewParcelas: TcxGridDBBandedTableView;
    viewParcelasNR_PARCELA: TcxGridDBBandedColumn;
    viewParcelasDT_VENCIMENTO: TcxGridDBBandedColumn;
    viewParcelasIC_DIAS: TcxGridDBBandedColumn;
    viewParcelasVL_TITULO: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    viewParcelasID_FORMA_PAGAMENTO: TcxGridDBBandedColumn;
    viewParcelasJOIN_DESCRICAO_FORMA_PAGAMENTO: TcxGridDBBandedColumn;
    PnPlanoPagamento: TgbPanel;
    PnTituloParcelamento: TgbPanel;
    cxLabel2: TcxLabel;
    EdtIdPlanoPagamento: TgbDBButtonEditFK;
    EdtDescricaoPlanoPagamento: TgbDBTextEdit;
    ActionListAssistent: TActionList;
    ActAlterar_Colunas_Grid: TAction;
    ActSalvarConfiguracoes: TAction;
    ActExibirAgrupamento: TAction;
    dxBarManagerFechamentoCrediario: TdxBarManager;
    bbAlterarRotuloColunas: TdxBarButton;
    bbSalvarConfiguracoes: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu;
    FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque;
    FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao;
    FrameTipoQuitacaoCrediario: TFrameTipoQuitacaoCrediario;
    viewParcelasID_CARTEIRA: TcxGridDBBandedColumn;
    viewParcelasJOIN_DESCRICAO_CARTEIRA: TcxGridDBBandedColumn;
    procedure CalcularValorLiquido(Sender: TObject);
    procedure EventoCalcularSaldoCreditoPessoa(Sender: TObject);
    procedure EdtValorPagamentoDinheiroExit(Sender: TObject);
    procedure EdtIdPlanoPagamentoExit(Sender: TObject);
    procedure AoAlterarValorParcela(Sender: TField);
    procedure AoAlterarFormaPagamentoParcela(Sender: TField);
    procedure AoAlterarCarteira(Sender: TField);
    procedure AoNavegarEntreAsParcelas(Sender: TDataset);
    procedure viewParcelasID_FORMA_PAGAMENTOPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure viewParcelasID_FORMA_PAGAMENTOPropertiesEditValueChanged(
      Sender: TObject);
    procedure viewParcelasVL_TITULOPropertiesEditValueChanged(Sender: TObject);
    procedure FrameTipoQuitacaoCartaoEdtValorExit(Sender: TObject);
    procedure FrameTipoQuitacaoChequegbDBCalcEdit1Exit(Sender: TObject);
    procedure FrameTipoQuitacaoCrediarioEdtValorExit(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure ActAlterar_Colunas_GridExecute(Sender: TObject);
    procedure ActRestaurarColunasExecute(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure VoltarFocoNaGridDeParcelas(Sender: TObject);
    procedure viewParcelasID_CARTEIRAPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure viewParcelasID_CARTEIRAPropertiesEditValueChanged(Sender: TObject);
  private
    FPodeExecutarGeracaoParcela: Boolean;
    FPodeExecutarAlteracaoFormaPagamento: Boolean;
    FPodeExecutarAlteracaoCarteira: Boolean;

    procedure ExibirTrocoDetalhado;
    procedure BloquearEscolhaPlanoPagamento;
  public
    FNomeFieldIdFormaPagamentoParcela: String;
    FNomeFieldIdCarteira: String;
    FNomeFieldDescricaoCarteira: String;
    FNomeFieldDescricaoFormaPagamentoParcela: String;
    FNomeFieldValorParcela: String;
    FIdFormaPagamentoAVista: Integer;
    FIdPlanoPagamentoAVista: Integer;
    FNomeFieldTipoFormaPagamento: String;
    procedure SetConfiguracoesIniciais(AIdFormaPagamentoAVista,
      AIdPlanoPagamentoAVista: Integer; ADatasetFechamento, ADatasetParcelas: TDataset); virtual;
    procedure ExibirInformacoesCreditoPessoa;
    procedure ExibirInformacoesTroco;
    procedure ExibirInformacoesDiferenca;
    procedure ExibirDadosFormaPagamento(ATipoFormaPagamento: String);
    procedure ProcessarCalculoTrocoDiferenca;
    procedure GerarParcelamento;
    procedure ReplicarDadosEntreAsParcelas;
    procedure ReprocessarDatasDeVencimentoDasParcelas;
    procedure ConfigurarEventoAlterarFormaPagamentoParcela;virtual;
    procedure ConfigurarEventoAlterarCarteiraParcela;virtual;
    procedure ConfigurarEventoNavegarEntreParcelas;virtual;

    procedure ConsultarPorFormaPagamento(AExibirFormulario: Boolean = true;
      AValorConsultaOculta: String = ''; AFiltro: TFiltroPadrao = nil);
    procedure ConsultarPorCarteira(AExibirFormulario: Boolean = true;
      AValorConsultaOculta: String = '');
  protected
    procedure AoCriarFrame;override;
    procedure GerarParcelamentoPrazo;virtual;
    procedure LimparParcelamento;virtual;
    procedure CalcularSaldoCreditoPessoa; virtual;
    procedure CalcularTrocoDiferenca; virtual;
    procedure ExecutarAcaoConsultarFormaPagamentoPeloClick; virtual;
    procedure ExecutarAcaoConsultarFormaPagamentoPeloEdit; virtual;
  end;

var
  FrameFechamentoCrediario: TFrameFechamentoCrediario;

implementation

{$R *.dfm}

uses uFormaPagamento, uFormaPagamentoProxy, uPlanoPagamento, uFrmConsultaPadrao,
  uAcbrUtils, uTipoQuitacaoProxy, uTUsuario, uSistema, uFrmApelidarColunasGrid,
  uCarteiraProxy, uCarteira, uDatasetUtils;

{ TFrameFechamentoCrediario }

procedure TFrameFechamentoCrediario.ActAlterar_Colunas_GridExecute(
  Sender: TObject);
begin
  TFrmApelidarColunasGrid.SetColumns(viewParcelas);
end;

procedure TFrameFechamentoCrediario.ActExibirAgrupamentoExecute(
  Sender: TObject);
begin
  viewParcelas.OptionsView.GroupByBox := not viewParcelas.OptionsView.GroupByBox;
end;

procedure TFrameFechamentoCrediario.ActRestaurarColunasExecute(Sender: TObject);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, Self.Name);
    viewParcelas.RestoreDefaults;
end;

procedure TFrameFechamentoCrediario.ActSalvarConfiguracoesExecute(
  Sender: TObject);
begin
  TUsuarioGridView.SaveGridView(viewParcelas,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TFrameFechamentoCrediario.AoAlterarCarteira(Sender: TField);
var
  idCarteira: Integer;
  carteira: TCarteiraProxy;
  Bookmark: TBookmark;
begin
  if FPodeExecutarAlteracaoCarteira then
  try
    FPodeExecutarAlteracaoCarteira := false;

    idCarteira :=
      Sender.Dataset.FieldByName(FNomeFieldIdCarteira).AsInteger;

    carteira := TCarteira.GetCarteira(idCarteira);

    try
      Sender.Dataset.DisableControls;
      Bookmark := Sender.Dataset.GetBookMark;
      while not Sender.Dataset.Eof do
      begin
        Sender.Dataset.Edit;
        Sender.AsInteger := carteira.Fid;
        Sender.Dataset.FieldByName(FNomeFieldDescricaoCarteira).AsString :=
          carteira.FDescricao;
        Sender.Dataset.Post;

        Sender.Dataset.Next;
      end;
    finally
      if (Bookmark <> nil) and Sender.Dataset.BookmarkValid(Bookmark) then
        Sender.Dataset.GoToBookmark(Bookmark);

      Sender.Dataset.EnableControls;
    end;
  finally
    FPodeExecutarAlteracaoCarteira := true;
  end;
end;

procedure TFrameFechamentoCrediario.AoAlterarFormaPagamentoParcela(
  Sender: TField);
var
  idFormaPagamento: Integer;
  formaPagamento: TFormaPagamentoProxy;
  Bookmark: TBookmark;
begin
  if FPodeExecutarAlteracaoFormaPagamento then
  try
    FPodeExecutarAlteracaoFormaPagamento := false;

    idFormaPagamento :=
      Sender.Dataset.FieldByName(FNomeFieldIdFormaPagamentoParcela).AsInteger;

    formaPagamento := TFormaPagamento.GetFormaPagamento(idFormaPagamento);

    try
      Sender.Dataset.DisableControls;
      Bookmark := Sender.Dataset.GetBookMark;
      while not Sender.Dataset.Eof do
      begin
        Sender.Dataset.Edit;
        Sender.AsInteger := formaPagamento.Fid;
        Sender.Dataset.FieldByName(FNomeFieldDescricaoFormaPagamentoParcela).AsString :=
          formaPagamento.FDescricao;
        Sender.Dataset.FieldByName(FNomeFieldTipoFormaPagamento).AsString :=
          formaPagamento.FTipoTipoQuitacao;
        Sender.Dataset.Post;

        Sender.Dataset.Next;
      end;
    finally
      if (Bookmark <> nil) and Sender.Dataset.BookmarkValid(Bookmark) then
        Sender.Dataset.GoToBookmark(Bookmark);

      Sender.Dataset.EnableControls;
    end;
  finally
    FPodeExecutarAlteracaoFormaPagamento := true;
  end;
end;

procedure TFrameFechamentoCrediario.AoAlterarValorParcela(Sender: TField);
begin
  if FPodeExecutarGeracaoParcela then
  try
    FPodeExecutarGeracaoParcela := false;

    if Sender.Dataset.FieldByName(FNomeFieldIdFormaPagamentoParcela).AsInteger =
      FIdFormaPagamentoAVista then
    begin
      Exit;
    end;

    try
      Sender.Dataset.DisableControls;

      TParcelamentoUtils.Reparcelar(Sender,
        EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
        EdtDiferenca.DataBinding.DataField).AsFloat);

      if TParcelamentoUtils.ParcelamentoInvalido(Sender) then
        GerarParcelamento;
    finally
      Sender.Dataset.EnableControls;
    end;
  finally
    TParcelamentoUtils.ConferirValorParcelado(Sender,
      EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
      EdtDiferenca.DataBinding.DataField).AsFloat);
    FPodeExecutarGeracaoParcela := true;
  end;
end;

procedure TFrameFechamentoCrediario.AoCriarFrame;
begin
  inherited;
  PnCreditoPessoa.Visible := false;
  PnTroco.Visible := false;
  PnDiferenca.Visible := false;
  FrameTipoQuitacaoCrediario.Visible := false;
  FrameTipoQuitacaoCheque.Visible := false;
  FrameTipoQuitacaoCartao.Visible := false;
  FPodeExecutarGeracaoParcela := true;
  FPodeExecutarAlteracaoFormaPagamento := true;
  FPodeExecutarAlteracaoCarteira := true;
  FNomeFieldIdCarteira := 'ID_CARTEIRA';
  FNomeFieldDescricaoCarteira := 'JOIN_DESCRICAO_CARTEIRA';
  FNomeFieldIdFormaPagamentoParcela := 'ID_FORMA_PAGAMENTO';
  FNomeFieldDescricaoFormaPagamentoParcela := 'JOIN_DESCRICAO_FORMA_PAGAMENTO';
  FNomeFieldValorParcela := 'VL_TITULO';
  FNomeFieldTipoFormaPagamento := 'JOIN_TIPO_FORMA_PAGAMENTO';
end;

procedure TFrameFechamentoCrediario.AoNavegarEntreAsParcelas(Sender: TDataset);
begin
  ExibirDadosFormaPagamento(
    Sender.FieldByName(FNomeFieldTipoFormaPagamento).AsString);
end;

procedure TFrameFechamentoCrediario.BloquearEscolhaPlanoPagamento;
begin
  EdtIdPlanoPagamento.gbReadyOnly :=
    EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
        EdtDiferenca.DataBinding.DataField).AsFloat <= 0;
end;

procedure TFrameFechamentoCrediario.EdtValorPagamentoDinheiroExit(
  Sender: TObject);
begin
  inherited;
  if Sender is TgbDBTextEdit then
  begin
    if TgbDBTextEdit(Sender).valorAnteriorAsFloat = StrtoFloatDef(VartoStr(TgbDBTextEdit(Sender).EditValue), 0) then
      Exit;
  end;

  AoAlterarValorParcela(viewParcelasVL_TITULO.DataBinding.Field);
  ProcessarCalculoTrocoDiferenca;
end;

procedure TFrameFechamentoCrediario.ExecutarAcaoConsultarFormaPagamentoPeloClick;
begin
  ConsultarPorFormaPagamento(true);
end;

procedure TFrameFechamentoCrediario.ExecutarAcaoConsultarFormaPagamentoPeloEdit;
begin
  ConsultarPorFormaPagamento(false, viewParcelasID_FORMA_PAGAMENTO.EditValue);
end;

procedure TFrameFechamentoCrediario.ExibirDadosFormaPagamento(
  ATipoFormaPagamento: String);
begin
  FrameTipoQuitacaoCrediario.Visible :=
    ATipoFormaPagamento.Equals(TTipoQuitacaoProxy.TIPO_DINHEIRO);

  FrameTipoQuitacaoCartao.Visible :=
    ATipoFormaPagamento.Equals(TTipoQuitacaoProxy.TIPO_DEBITO) or
    ATipoFormaPagamento.Equals(TTipoQuitacaoProxy.TIPO_CREDITO);

  FrameTipoQuitacaoCheque.Visible :=
    ATipoFormaPagamento.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    ATipoFormaPagamento.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO);
end;

procedure TFrameFechamentoCrediario.ExibirInformacoesCreditoPessoa;
begin
  PnCreditoPessoa.Visible :=
    EdtCreditoDisponivel.DataBinding.DataSource.DataSet.FieldByName(
      EdtCreditoDisponivel.DataBinding.DataField).AsFloat > 0;
end;

procedure TFrameFechamentoCrediario.ExibirInformacoesDiferenca;
begin
  PnDiferenca.Visible := EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
    EdtDiferenca.DataBinding.DataField).AsFloat > 0;
end;

procedure TFrameFechamentoCrediario.ExibirInformacoesTroco;
begin
  PnTroco.Visible := EdtTroco.DataBinding.DataSource.DataSet.FieldByName(
    EdtTroco.DataBinding.DataField).AsFloat > 0;

  if PnTroco.Visible then
  begin
    ExibirTrocoDetalhado;
  end;
end;

procedure TFrameFechamentoCrediario.ExibirTrocoDetalhado;
begin
  TAcbrUtils.ExibirTroco(EdtTroco.DataBinding.DataSource.DataSet.FieldByName(
      EdtTroco.DataBinding.DataField).AsFloat, mmTroco);
end;

procedure TFrameFechamentoCrediario.FrameTipoQuitacaoCartaoEdtValorExit(
  Sender: TObject);
begin
  inherited;
  (dsParcelas.Dataset as TgbClientDataset).GuardarBookMark;
  try
    AoAlterarValorParcela(FrameTipoQuitacaoCartao.EdtValor.DataBinding.Field);
  finally
    (dsParcelas.Dataset as TgbClientDataset).PosicionarBookmark;
    VoltarFocoNaGridDeParcelas(Sender);
  end;
end;

procedure TFrameFechamentoCrediario.FrameTipoQuitacaoChequegbDBCalcEdit1Exit(
  Sender: TObject);
begin
  inherited;
  (dsParcelas.Dataset as TgbClientDataset).GuardarBookMark;
  try
    AoAlterarValorParcela(FrameTipoQuitacaoCheque.EdtValor.DataBinding.Field);
  finally
    (dsParcelas.Dataset as TgbClientDataset).PosicionarBookmark;
    VoltarFocoNaGridDeParcelas(Sender);
  end;
end;

procedure TFrameFechamentoCrediario.FrameTipoQuitacaoCrediarioEdtValorExit(
  Sender: TObject);
begin
  inherited;
  (dsParcelas.Dataset as TgbClientDataset).GuardarBookMark;
  try
    AoAlterarValorParcela(FrameTipoQuitacaoCheque.EdtValor.DataBinding.Field);
  finally
    (dsParcelas.Dataset as TgbClientDataset).PosicionarBookmark;
    VoltarFocoNaGridDeParcelas(Sender);
  end;
end;

procedure TFrameFechamentoCrediario.EdtIdPlanoPagamentoExit(Sender: TObject);
begin
  inherited;
  if not EdtIdPlanoPagamento.gbReadyOnly then
    GerarParcelamento;
end;

procedure TFrameFechamentoCrediario.GerarParcelamento;
begin
  if FIdPlanoPagamentoAVista = EdtIdPlanoPagamento.DataBinding.DataSource.DataSet.FieldByName(
        EdtIdPlanoPagamento.DataBinding.DataField).AsInteger then
  begin
    Exit;
  end;

  if (EdtValorPagamentoDinheiro.DataBinding.DataSource.DataSet.FieldByName(
      EdtValorPagamentoDinheiro.DataBinding.DataField).AsFloat = 0) or
      (not dsParcelas.Dataset.IsEmpty) then
  begin
    LimparParcelamento;
  end;

  ProcessarCalculoTrocoDiferenca;

  if EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
    EdtDiferenca.DataBinding.DataField).AsFloat > 0 then
    GerarParcelamentoPrazo;
end;

procedure TFrameFechamentoCrediario.GerarParcelamentoPrazo;
begin
  if EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
      EdtDiferenca.DataBinding.DataField).AsFloat <= 0 then
    Exit;
end;

procedure TFrameFechamentoCrediario.LimparParcelamento;
begin
  while not viewParcelas.DataController.Datasource.Dataset.IsEmpty do
    viewParcelas.DataController.Datasource.Dataset.Delete;
end;

procedure TFrameFechamentoCrediario.SetConfiguracoesIniciais(
  AIdFormaPagamentoAVista, AIdPlanoPagamentoAVista: Integer;
  ADatasetFechamento, ADatasetParcelas: TDataset);
begin
  FIdFormaPagamentoAVista := AIdFormaPagamentoAVista;
  FIdPlanoPagamentoAVista := AIdPlanoPagamentoAVista;
  dsFechamento.Dataset := ADatasetFechamento;
  dsParcelas.Dataset := ADatasetParcelas;

  TUsuarioGridView.LoadGridView(viewParcelas,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name);

  ConfigurarEventoAlterarFormaPagamentoParcela;
  ConfigurarEventoAlterarCarteiraParcela;
  ConfigurarEventoNavegarEntreParcelas;
end;

procedure TFrameFechamentoCrediario.viewParcelasID_CARTEIRAPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  ConsultarPorCarteira(true);
end;

procedure TFrameFechamentoCrediario.viewParcelasID_CARTEIRAPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  gridParcelas.SetFocus;
  ConsultarPorCarteira(false, viewParcelasID_CARTEIRA.EditValue);
end;

procedure TFrameFechamentoCrediario.viewParcelasID_FORMA_PAGAMENTOPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  ExecutarAcaoConsultarFormaPagamentoPeloClick;
end;

procedure TFrameFechamentoCrediario.viewParcelasID_FORMA_PAGAMENTOPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  gridParcelas.SetFocus;
  ExecutarAcaoConsultarFormaPagamentoPeloEdit;
end;

procedure TFrameFechamentoCrediario.viewParcelasVL_TITULOPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  gridParcelas.SetFocus;
  AoAlterarValorParcela(viewParcelasVL_TITULO.DataBinding.Field);
end;

procedure TFrameFechamentoCrediario.VoltarFocoNaGridDeParcelas(Sender: TObject);
begin
  gridParcelas.SetFocus;

  if assigned(dsParcelas.Dataset) then
    dsParcelas.Dataset.Next;
end;

procedure TFrameFechamentoCrediario.EventoCalcularSaldoCreditoPessoa(Sender: TObject);
begin
  CalcularSaldoCreditoPessoa;
  ProcessarCalculoTrocoDiferenca;
end;

procedure TFrameFechamentoCrediario.CalcularSaldoCreditoPessoa;
begin
  if not (EdtCreditoPessoaSaldo.DataBinding.DataSource.DataSet.State in dsEditModes) then
    Exit;

  EdtCreditoPessoaSaldo.DataBinding.DataSource.DataSet.FieldByName(
      EdtCreditoPessoaSaldo.DataBinding.DataField).AsFloat :=

  EdtCreditoDisponivel.DataBinding.DataSource.DataSet.FieldByName(
      EdtCreditoDisponivel.DataBinding.DataField).AsFloat -

  EdtCreditoPessoaUtilizado.DataBinding.DataSource.DataSet.FieldByName(
      EdtCreditoPessoaUtilizado.DataBinding.DataField).AsFloat;
end;

procedure TFrameFechamentoCrediario.CalcularTrocoDiferenca;
var diferenca: Double;
begin
  if not (EdtDiferenca.DataBinding.DataSource.DataSet.State in dsEditModes) then
    Exit;

  diferenca :=

  EdtValorLiquido.DataBinding.DataSource.DataSet.FieldByName(
      EdtValorLiquido.DataBinding.DataField).AsFloat -

  EdtCreditoPessoaUtilizado.DataBinding.DataSource.DataSet.FieldByName(
      EdtCreditoPessoaUtilizado.DataBinding.DataField).AsFloat -

  EdtValorPagamentoDinheiro.DataBinding.DataSource.DataSet.FieldByName(
      EdtValorPagamentoDinheiro.DataBinding.DataField).AsFloat;

  if diferenca > 0 then
  begin
    EdtDiferenca.DataBinding.DataSource.DataSet.FieldByName(
      EdtDiferenca.DataBinding.DataField).AsFloat := diferenca;
  end
  else if diferenca < 0 then
  begin
    EdtTroco.DataBinding.DataSource.DataSet.FieldByName(
      EdtTroco.DataBinding.DataField).AsFloat := diferenca * -1;
  end;
end;

procedure TFrameFechamentoCrediario.ProcessarCalculoTrocoDiferenca;
begin
  CalcularTrocoDiferenca;

  ExibirInformacoesTroco;
  ExibirInformacoesDiferenca;
end;

procedure TFrameFechamentoCrediario.ReplicarDadosEntreAsParcelas;
begin
  if FrameTipoQuitacaoCheque.Visible then
    TParcelamentoUtils.ReplicarInformacoesCheque(dsParcelas.Dataset)
  else if FrameTipoQuitacaoCartao.Visible then
    TParcelamentoUtils.ReplicarInformacoesCartao(dsParcelas.Dataset);
end;

procedure TFrameFechamentoCrediario.ReprocessarDatasDeVencimentoDasParcelas;
var
  fieldDataVencimento: TField;
  fieldDiaVencimento: TField;
begin
  fieldDataVencimento := viewParcelasDT_VENCIMENTO.DataBinding.Field;
  fieldDiaVencimento := viewParcelasIC_DIAS.DataBinding.Field;

  if not TDatasetUtils.ValorFoiAlterado(fieldDataVencimento) then
    Exit;

  TParcelamentoUtils.ReprocessarVencimento(fieldDataVencimento, fieldDiaVencimento);
end;

procedure TFrameFechamentoCrediario.CalcularValorLiquido(Sender: TObject);
begin
    if not (EdtValorLiquido.DataBinding.DataSource.DataSet.State in dsEditModes) then
    Exit;

  if Sender is TgbDBTextEdit then
  begin
    if TgbDBTextEdit(Sender).valorAnteriorAsFloat = StrtoFloatDef(VartoStr(TgbDBTextEdit(Sender).EditValue), 0) then
      Exit;
  end;

  EdtValorLiquido.DataBinding.DataSource.DataSet.FieldByName(
      EdtValorLiquido.DataBinding.DataField).AsFloat :=

  EdtValorBruto.DataBinding.DataSource.DataSet.FieldByName(
      EdtValorBruto.DataBinding.DataField).AsFloat +

  EdtAcrescimo.DataBinding.DataSource.DataSet.FieldByName(
      EdtAcrescimo.DataBinding.DataField).AsFloat -

  EdtDesconto.DataBinding.DataSource.DataSet.FieldByName(
      EdtDesconto.DataBinding.DataField).AsFloat;

  ProcessarCalculoTrocoDiferenca;
end;

procedure TFrameFechamentoCrediario.ConfigurarEventoAlterarCarteiraParcela;
begin
  viewParcelas.DataController.Datasource.Dataset.FieldByName(
    FNomeFieldIdCarteira).OnChange := AoAlterarCarteira;
end;

procedure TFrameFechamentoCrediario.ConfigurarEventoAlterarFormaPagamentoParcela;
begin
  viewParcelas.DataController.Datasource.Dataset.FieldByName(
    FNomeFieldIdFormaPagamentoParcela).OnChange := AoAlterarFormaPagamentoParcela;
end;

procedure TFrameFechamentoCrediario.ConfigurarEventoNavegarEntreParcelas;
begin
  viewParcelas.DataController.DataSource.Dataset.AfterScroll :=
    AoNavegarEntreAsParcelas;
end;

procedure TFrameFechamentoCrediario.ConsultarPorCarteira(
  AExibirFormulario: Boolean; AValorConsultaOculta: String);
var sourceFields, consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    dsParcelas.Dataset.Edit;
    sourceFields.Add(FNomeFieldIdCarteira);
    consultaFields.Add('ID');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        dsParcelas.Dataset,
        'CARTEIRA',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        dsParcelas.Dataset,
        'CARTEIRA',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if dsParcelas.Dataset.State in dsEditModes then
      dsParcelas.Dataset.Post;
  end;
end;

procedure TFrameFechamentoCrediario.ConsultarPorFormaPagamento(
  AExibirFormulario: Boolean = true; AValorConsultaOculta: String = ''; AFiltro: TFiltroPadrao = nil);
var sourceFields, consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    dsParcelas.Dataset.Edit;
    sourceFields.Add(FNomeFieldIdFormaPagamentoParcela);
    consultaFields.Add('ID');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        dsParcelas.Dataset,
        'FORMA_PAGAMENTO',
        sourceFields,
        consultaFields, '', AFiltro);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        dsParcelas.Dataset,
        'FORMA_PAGAMENTO',
        sourceFields,
        consultaFields,
        '', AFiltro, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if dsParcelas.Dataset.State in dsEditModes then
      dsParcelas.Dataset.Post;
  end;
end;

end.
