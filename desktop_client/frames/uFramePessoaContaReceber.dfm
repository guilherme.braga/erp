inherited FramePessoaContaReceber: TFramePessoaContaReceber
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloFrameDetailPadrao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      ExplicitHeight = 190
      Height = 190
      inherited cxGridConsultaDadosDetalahadaGrade: TcxGrid
        Height = 186
        ExplicitHeight = 186
        inherited viewConsultaDadosDetalhadaGrade: TcxGridDBBandedTableView
          DataController.OnDataChanged = viewConsultaDadosDetalhadaGradeDataControllerDataChanged
        end
      end
    end
    inherited pnFiltroVertical: TgbPanel
      ExplicitHeight = 190
      Height = 190
      inherited pnAcoesFiltroVertical: TgbPanel
        Top = 50
        ExplicitTop = 50
      end
      object rgTipoContaReceber: TcxRadioGroup
        AlignWithMargins = True
        Left = 5
        Top = 5
        Align = alTop
        Caption = 'Situa'#231#227'o do Contas a Receber'
        ParentFont = False
        Properties.Columns = 3
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = 'Todos'
            Value = 'TODOS'
          end
          item
            Caption = 'Aberto'
            Value = 'ABERTO'
          end
          item
            Caption = 'Quitado'
            Value = 'QUITADO'
          end>
        Properties.OnEditValueChanged = cxRadioGroup1PropertiesEditValueChanged
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 1
        Height = 39
        Width = 305
      end
    end
    inherited spFiltroVertical: TcxSplitter
      Height = 190
      ExplicitHeight = 190
    end
    object PnTotalizadorConsulta: TFlowPanel
      Left = 2
      Top = 211
      Width = 447
      Height = 92
      Align = alBottom
      AutoSize = True
      BevelOuter = bvNone
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 4
      object pnTotalizadorVlAberto: TgbLabel
        AlignWithMargins = True
        Left = 0
        Top = 1
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 12
      end
      object pnTotalizadorAcrecimoAberto: TgbLabel
        AlignWithMargins = True
        Left = 168
        Top = 1
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 12
      end
      object pnTotalizadorTotalAberto: TgbLabel
        AlignWithMargins = True
        Left = 0
        Top = 24
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 35
      end
      object pnTotalizadorVlVencido: TgbLabel
        AlignWithMargins = True
        Left = 168
        Top = 24
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentColor = False
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Color = clBlack
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clRed
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 35
      end
      object pnTotalizadorVlVencer: TgbLabel
        AlignWithMargins = True
        Left = 0
        Top = 47
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clNavy
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 58
      end
      object pnTotalizadorVlRecebido: TgbLabel
        AlignWithMargins = True
        Left = 168
        Top = 47
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clGreen
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 58
      end
      object pnTotalizadorAcrescimoRecebido: TgbLabel
        AlignWithMargins = True
        Left = 0
        Top = 70
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clGreen
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 81
      end
      object pnTotalizadorTotalRecebido: TgbLabel
        AlignWithMargins = True
        Left = 168
        Top = 70
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        AutoSize = False
        Caption = 'TOTALIZA R$ 999.999.999.00'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clGreen
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 21
        Width = 167
        AnchorY = 81
      end
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
