unit uFrameVendaVarejoFechamentoCrediario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameFechamentoCrediario, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxButtonEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxClasses,
  cxGridCustomView, cxGrid, cxLabel, cxMaskEdit, cxDBEdit, uGBDBButtonEditFK,
  cxTextEdit, uGBDBTextEdit, uGBPanel, uFrameTipoQuitacaoCrediario,
  uFrameTipoQuitacaoCheque, uFramePadrao, uFrameTipoQuitacaoCartao, cxGroupBox,
  cxCalc, JvExControls, JvButton, JvTransparentButton, dxBar,
  dxRibbonRadialMenu, System.Actions, Vcl.ActnList, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameVendaVarejoFechamentoCrediario = class(TFrameFechamentoCrediario)
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure GerarParcelamentoPrazo;override;
    procedure ExecutarAcaoConsultarFormaPagamentoPeloClick; override;
    procedure ExecutarAcaoConsultarFormaPagamentoPeloEdit; override;
  end;

var
  FrameVendaVarejoFechamentoCrediario: TFrameVendaVarejoFechamentoCrediario;

implementation

{$R *.dfm}

uses uMovVendaVarejo, uControlsUtils, uTControl_Function, uFiltroFormulario, uFormaPagamento,
  uFormaPagamentoProxy;

{ TFrameVendaVarejoFechamentoCrediario }

procedure TFrameVendaVarejoFechamentoCrediario.ExecutarAcaoConsultarFormaPagamentoPeloClick;
var
  filtroPrazoConsumidorFinal: TFiltroPadrao;
  codigosFormaPagamentoCartao: String;
begin
  //inherited;
  filtroPrazoConsumidorFinal := TFiltroPadrao.Create;
  try
    if TMovVendaVarejo(TControlFunction.GetInstance('TMovVendaVarejo')).FClienteConsumidorFinal then
    begin
      codigosFormaPagamentoCartao := TFormaPagamento.GetCodigosFormaPagamentoCartao;

      if not codigosFormaPagamentoCartao.IsEmpty then
      begin
        filtroPrazoConsumidorFinal.AddFaixa(TFormaPagamentoProxy.FIELD_ID, codigosFormaPagamentoCartao);
      end;
    end;

    ConsultarPorFormaPagamento(true, '', filtroPrazoConsumidorFinal);
  finally
    FreeAndNil(filtroPrazoConsumidorFinal);
  end;
end;

procedure TFrameVendaVarejoFechamentoCrediario.ExecutarAcaoConsultarFormaPagamentoPeloEdit;
var
  filtroPrazoConsumidorFinal: TFiltroPadrao;
  codigosFormaPagamentoCartao: String;
begin
  //inherited;
  filtroPrazoConsumidorFinal := TFiltroPadrao.Create;
  try
    if TMovVendaVarejo(TControlFunction.GetInstance('TMovVendaVarejo')).FClienteConsumidorFinal then
    begin
      codigosFormaPagamentoCartao := TFormaPagamento.GetCodigosFormaPagamentoCartao;

      if not codigosFormaPagamentoCartao.IsEmpty then
      begin
        filtroPrazoConsumidorFinal.AddFaixa(TFormaPagamentoProxy.FIELD_ID, codigosFormaPagamentoCartao);
      end;
    end;

    ConsultarPorFormaPagamento(false, viewParcelasID_FORMA_PAGAMENTO.EditValue, filtroPrazoConsumidorFinal);
  finally
    FreeAndNil(filtroPrazoConsumidorFinal);
  end;
end;

procedure TFrameVendaVarejoFechamentoCrediario.GerarParcelamentoPrazo;
begin
  TMovVendaVarejo(TControlFunction.GetInstance('TMovVendaVarejo')).GerarParcelamento;
end;

end.
