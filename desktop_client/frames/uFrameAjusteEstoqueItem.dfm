inherited FrameAjusteEstoqueItem: TFrameAjusteEstoqueItem
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 240
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 451
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 662
        end
        object lbItem: TLabel
          Left = 8
          Top = 9
          Width = 22
          Height = 13
          Caption = 'Item'
        end
        object edtItem: TgbDBTextEditPK
          Left = 49
          Top = 5
          HelpType = htKeyword
          TabStop = False
          DataBinding.DataField = 'NR_ITEM'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object gbPanel1: TgbPanel
          Left = 2
          Top = 34
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 105
          Width = 447
          object lbProduto: TLabel
            Left = 6
            Top = 6
            Width = 38
            Height = 13
            Caption = 'Produto'
          end
          object lbQuantidade: TLabel
            Left = 194
            Top = 33
            Width = 56
            Height = 13
            Caption = 'Quantidade'
          end
          object lbEstoqueAtual: TLabel
            Left = 376
            Top = 6
            Width = 67
            Height = 13
            Caption = 'Estoque Atual'
          end
          object lbTipo: TLabel
            Left = 23
            Top = 33
            Width = 20
            Height = 13
            Caption = 'Tipo'
          end
          object lbEstoqueDesejado: TLabel
            Left = 356
            Top = 33
            Width = 87
            Height = 13
            Caption = 'Estoque Desejado'
          end
          object btnFKProduto: TgbDBButtonEditFK
            Left = 47
            Top = 2
            DataBinding.DataField = 'ID_PRODUTO'
            DataBinding.DataSource = dsDataFrame
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 0
            OnExit = CalcularEstoqueDesejado
            gbTextEdit = edtDescProduto
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 
              'ID_PRODUTO;JOIN_DESCRICAO_PRODUTO;JOIN_SIGLA_UNIDADE_ESTOQUE;JOI' +
              'N_ESTOQUE_PRODUTO'
            gbTableName = 'PRODUTO'
            gbCamposConsulta = 'ID;DESCRICAO;SIGLA;QT_ESTOQUE'
            gbDepoisDeConsultar = btnFKProdutogbDepoisDeConsultar
            Width = 60
          end
          object edtDescProduto: TgbDBTextEdit
            Left = 104
            Top = 2
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_PRODUTO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            Width = 247
          end
          object EdtQuantidade: TgbDBTextEdit
            Left = 254
            Top = 29
            DataBinding.DataField = 'QUANTIDADE'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            OnExit = CalcularEstoqueDesejado
            gbRequired = True
            gbPassword = False
            Width = 97
          end
          object edtEstoqueAtual: TgbDBTextEdit
            Left = 446
            Top = 2
            TabStop = False
            DataBinding.DataField = 'JOIN_ESTOQUE_PRODUTO'
            DataBinding.DataSource = dsDataFrame
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 97
          end
          object gbTipo: TgbDBRadioGroup
            Left = 47
            Top = 25
            DataBinding.DataField = 'TIPO'
            DataBinding.DataSource = dsDataFrame
            Properties.Columns = 2
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'Entrada'
                Value = 'ENTRADA'
              end
              item
                Caption = 'Sa'#237'da'
                Value = 'SAIDA'
              end>
            Properties.OnChange = gbDBRadioGroup1PropertiesChange
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            OnExit = CalcularEstoqueDesejado
            Height = 25
            Width = 143
          end
          object calcEditEstoqueDesejado: TcxCalcEdit
            Left = 446
            Top = 29
            EditValue = 0.000000000000000000
            TabOrder = 5
            OnExit = CalcularEstoqueDesejado
            Width = 97
          end
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Produtos'
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
  end
end
