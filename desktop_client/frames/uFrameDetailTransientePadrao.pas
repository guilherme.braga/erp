unit uFrameDetailTransientePadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  JvExControls, JvButton, JvTransparentButton, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ugbClientDataset, Datasnap.DBClient, Vcl.StdCtrls,
  cxButtons, dxRibbonRadialMenu, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TFrameDetailTransientePadrao = class(TFrameDetailPadrao)
    gbDadosTransiente: TgbPanel;
    gbPanel3: TgbPanel;
    JvTransparentButton1: TJvTransparentButton;
    JvTransparentButton2: TJvTransparentButton;
    dsDataSetTransiente: TDataSource;
    fdmDatasetTransiente: TGBClientDataSet;
    RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu;
    dxBarButton6: TdxBarButton;
  private
    FCamposParaNaoValorarNoSource: String;
    procedure CriarDatasetTransiente;
    procedure ConfigurarFieldClonado(AFieldOriginal, AFieldClone: TField);

    procedure CopiarItemTransienteParaPersistente;
    procedure CopiarItemPersistenteParaTransiente;

    procedure FocarCampo;
    procedure NovoItem;
    procedure ConfigurarPainelDados;
  public
    procedure SetConfiguracoesIniciais(ADataset: TgbClientDataset); override;

  protected
    FDatasetPersistente: TgbClientDataset;
    procedure AoCriarFrame; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure DepoisDeAlterar; override;
    procedure DepoisDeCancelar; override;
    procedure Cancelar; override;
    procedure Confirmar; override;
    procedure Excluir; override;
    procedure AcaoAlterar; override;
    procedure GerenciarControles; override;
    procedure DefinirCamposNaoValorarNoSource(
      var AComaTextCamposNaoValorarNoSource: String); virtual;
  end;

var
  FrameDetailTransientePadrao: TFrameDetailTransientePadrao;

implementation

{$R *.dfm}

uses uDatasetUtils, uControlsUtils, uDevExpressUtils, uTUsuario, uSistema,
  uDmAcesso;

{ TFrameDetailTransientePadrao }

procedure TFrameDetailTransientePadrao.AcaoAlterar;
begin
  inherited;
  fdmDatasetTransiente.Incluir;
end;

procedure TFrameDetailTransientePadrao.AntesDeConfirmar;
begin
  inherited;
  TDatasetUtils.ValidarCamposObrigatorios(dsDataFrame.Dataset);
end;

procedure TFrameDetailTransientePadrao.AoCriarFrame;
begin
  inherited;
  ConfigurarPainelDados;
  DefinirCamposNaoValorarNoSource(FCamposParaNaoValorarNoSource);
end;

procedure TFrameDetailTransientePadrao.Cancelar;
begin
  //inherited;
  fdmDatasetTransiente.Cancelar;
  fdmDatasetTransiente.Limpar;
end;

procedure TFrameDetailTransientePadrao.Confirmar;
begin
  //inherited;
  fdmDatasetTransiente.Salvar;
end;

procedure TFrameDetailTransientePadrao.CopiarItemPersistenteParaTransiente;
begin
  TDatasetUtils.CopiarRegistro(dsDataSetTransiente.Dataset, dsDataFrame.Dataset);
end;

procedure TFrameDetailTransientePadrao.CopiarItemTransienteParaPersistente;
var
  copiarSomenteAlterados: Boolean;
begin
  if FAcaoCrud = TAcaoCrud(Incluir) then
  begin
    TGbClientDataset(dsDataSetTransiente.Dataset).Incluir;
    copiarSomenteAlterados := false;
  end
  else if FAcaoCrud = TAcaoCrud(Alterar) then
  begin
    TGbClientDataset(dsDataSetTransiente.Dataset).Alterar;
    copiarSomenteAlterados := true;
  end;

  TDatasetUtils.CopiarRegistro(dsDataFrame.Dataset, dsDataSetTransiente.Dataset,
    copiarSomenteAlterados, FCamposParaNaoValorarNoSource);

  TGbClientDataset(dsDataSetTransiente.Dataset).Salvar;
end;

procedure TFrameDetailTransientePadrao.CriarDatasetTransiente;
var
  i: Integer;
  datasetOrigem: TDataset;
  fieldFloat: TFMTBCDField;
  fieldInteger: TIntegerField;
  fieldAutoInc: TAutoIncField;
  fieldDateTime: TDateTimeField;
  fieldDate: TDateField;
  fieldString: TStringField;
  fieldBlob: TBlobField;
begin
  datasetOrigem := dsDataFrame.DataSet;
  for i := 0 to Pred(datasetOrigem.FieldCount) do
  begin
    if datasetOrigem.Fields[i] is TAutoIncField then
    begin
      fieldAutoInc := TAutoIncField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldAutoInc);
    end
    else if (datasetOrigem.Fields[i] is TFMTBCDField) or (datasetOrigem.Fields[i] is TBCDField) then
    begin
      fieldFloat := TFMTBCDField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldFloat);
      fieldFloat.DisplayFormat := fieldFloat.DisplayFormat;
      fieldFloat.Precision := fieldFloat.Precision;

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldFloat.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end
    else if datasetOrigem.Fields[i] is TDateTimeField then
    begin
      fieldDateTime := TDateTimeField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldDateTime);

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldDateTime.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end
    else if datasetOrigem.Fields[i] is TDateField then
    begin
      fieldDate := TDateField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldDate);

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldDate.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end
    else if datasetOrigem.Fields[i] is TStringField then
    begin
      fieldString := TStringField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldString);

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldString.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end
    else if datasetOrigem.Fields[i] is TIntegerField then
    begin
      fieldInteger := TIntegerField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldInteger);
      fieldInteger.DisplayFormat := fieldInteger.DisplayFormat;

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldInteger.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end
    else if datasetOrigem.Fields[i] is TBlobField then
    begin
      fieldBlob := TBlobField.Create(nil);
      ConfigurarFieldClonado(datasetOrigem.Fields[i], fieldBlob);

      if Assigned(datasetOrigem.Fields[i].OnChange) then
      begin
        fieldBlob.OnChange := datasetOrigem.Fields[i].OnChange;
      end;
    end;
  end;

  if Assigned(datasetOrigem.OnNewRecord) then
    fdmDataSetTransiente.OnNewRecord := datasetOrigem.OnNewRecord;
end;

procedure TFrameDetailTransientePadrao.ConfigurarFieldClonado(AFieldOriginal,
  AFieldClone: TField);
begin
  AFieldClone.Alignment := AFieldOriginal.Alignment;
  AFieldClone.DisplayWidth := AFieldOriginal.DisplayWidth;
  AFieldClone.Origin := AFieldOriginal.Origin;
  AFieldClone.Size := AFieldOriginal.Size;
  AFieldClone.ProviderFlags := AFieldOriginal.ProviderFlags;
  AFieldClone.ReadOnly := AFieldOriginal.ReadOnly;
  AFieldClone.Required := AFieldOriginal.Required;
  AFieldClone.FieldName := AFieldOriginal.FieldName;
  AFieldClone.Name := fdmDataSetTransiente.Name+AFieldClone.FieldName;
  AFieldClone.DisplayLabel := AFieldOriginal.DisplayLabel;
  AFieldClone.Dataset := fdmDataSetTransiente;
end;

procedure TFrameDetailTransientePadrao.ConfigurarPainelDados;
begin
  gbDadosTransiente.Transparent := false;
  gbDadosTransiente.PanelStyle.OfficeBackgroundKind := pobkOffice11Color;
end;

procedure TFrameDetailTransientePadrao.DefinirCamposNaoValorarNoSource(
  var AComaTextCamposNaoValorarNoSource: String);
begin
  //Implementar na heran�a.
end;

procedure TFrameDetailTransientePadrao.DepoisDeAlterar;
begin
  inherited;
  CopiarItemPersistenteParaTransiente;
end;

procedure TFrameDetailTransientePadrao.DepoisDeCancelar;
begin
  inherited;
  FocarCampo;
end;

procedure TFrameDetailTransientePadrao.DepoisDeConfirmar;
begin
  CopiarItemTransienteParaPersistente;
  inherited;
  NovoItem;
  FocarCampo;
end;

procedure TFrameDetailTransientePadrao.Excluir;
begin
  //inherited;
  dsDataSetTransiente.dataset.Delete;
end;

procedure TFrameDetailTransientePadrao.FocarCampo;
begin
  Self.SelectNext(gbDadosTransiente, true, true);
end;

procedure TFrameDetailTransientePadrao.GerenciarControles;
begin
  //inherited; N�o far� tratamento de controles no transientepadrao

end;

procedure TFrameDetailTransientePadrao.NovoItem;
begin
  if TgbClientDataset(dsDataFrame.Dataset).Aberto then
    TgbClientDataset(dsDataFrame.Dataset).Limpar
  else
    TgbClientDataset(dsDataFrame.Dataset).Abrir;

  ActInsert.Execute;
end;

procedure TFrameDetailTransientePadrao.SetConfiguracoesIniciais(
  ADataset: TgbClientDataset);
var
  datasetPersistente: TDataset;
  datasetTransiente: TDataset;
begin
//  inherited;

  AoCriarFrame;

  if not Assigned(ADataset) then
    Exit;

  FDatasetPersistente := ADataset;

  dsDataFrame.DataSet := ADataset;

  cxpcMain.ActivePage := cxtsSearch;
  cxpcMain.Properties.HideTabs := true;

  GerenciarControles;

  CriarDatasetTransiente;

  datasetPersistente := dsDataFrame.Dataset;
  datasetTransiente := dsDataSetTransiente.Dataset;

  dsDataFrame.Dataset := datasetTransiente;
  dsDataSetTransiente.Dataset := datasetPersistente;

  Level1BandedTableView1.DataController.DataSource := dsDataSetTransiente;

  Level1BandedTableView1.ClearItems;
  Level1BandedTableView1.DataController.CreateAllItems();
  TcxGridUtils.PersonalizarColunaPeloTipoCampo(Level1BandedTableView1);

  TUsuarioGridView.LoadGridView(Level1BandedTableView1,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name);

  fdmDataSetTransiente.CreateDataset;
  fdmDataSetTransiente.FieldDefs.Update;

  NovoItem;
end;

end.
