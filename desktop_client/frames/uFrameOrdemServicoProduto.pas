unit uFrameOrdemServicoProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDMemTable, Vcl.Menus, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxBarExtItems, dxBar, cxClasses, System.Actions,
  Vcl.ActnList, JvExControls, JvButton, JvTransparentButton, uGBPanel, dxBevel,
  cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  cxDropDownEdit, cxCalc, cxDBEdit, uGBDBCalcEdit, uGBDBValorComPercentual,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls, Datasnap.DBClient, uGBClientDataset, dxRibbonRadialMenu;

type
  TFrameOrdemServicoProduto = class(TFrameDetailTransientePadrao)
    Label9: TLabel;
    CampoProduto: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    Label1: TLabel;
    gbDBTextEdit6: TgbDBTextEdit;
    Label3: TLabel;
    gbDBTextEdit7: TgbDBTextEdit;
    Label5: TLabel;
    EdtQuantidade: TgbDBCalcEdit;
    Label2: TLabel;
    EdtValorUnitario: TgbDBCalcEdit;
    Label8: TLabel;
    EdtValorBruto: TgbDBCalcEdit;
    Label18: TLabel;
    EdtValorLiquido: TgbDBCalcEdit;
    Label4: TLabel;
    Label6: TLabel;
    EdtDesconto: TGBDBValorComPercentual;
    EdtAcrescimo: TGBDBValorComPercentual;
    procedure CalcularValorBruto(Sender: TObject);
    procedure CalcularValorLiquido(Sender: TObject);
    procedure CampoProdutogbDepoisDeConsultar(Sender: TObject);
    procedure EdtAcrescimoExit(Sender: TObject);
  private
    FDatasetOrdemServico: TgbClientDataset;
  public
    procedure AoCriarFrame; override;
    procedure AntesDeConfirmar; override;
    procedure SetConfiguracoesIniciais(AOrdemServico, AOrdemServicoProduto: TgbClientDataset); overload;
  end;

var
  FrameOrdemServicoProduto: TFrameOrdemServicoProduto;

implementation

{$R *.dfm}

uses uMovOrdemServico, uOrdemServico, uSistema, uFiltroFormulario,
  uProdutoProxy, uControlsUtils, uTControl_Function, uOrdemServicoProxy,
  uConstantes, uTabelaPrecoProxy, uTabelaPreco;

{ TFrameOrdemServicoProduto }

procedure TFrameOrdemServicoProduto.AntesDeConfirmar;
var
  idOrdemServico: Integer;
begin
  inherited;
  idOrdemServico := (TControlFunction.GetInstance('TMovOrdemServico') as TMovOrdemServico).cdsDataID.AsInteger;

  if idOrdemServico > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_ORDEM_SERVICO', idOrdemServico);
  end;
end;

procedure TFrameOrdemServicoProduto.AoCriarFrame;
begin
  inherited;
end;

procedure TFrameOrdemServicoProduto.CalcularValorBruto(Sender: TObject);
begin
  TOrdemServico.CalcularValorBrutoProduto(fdmDatasetTransiente);
end;

procedure TFrameOrdemServicoProduto.CalcularValorLiquido(Sender: TObject);
begin
  TOrdemServico.CalcularValorLiquidoProduto(fdmDatasetTransiente);
end;

procedure TFrameOrdemServicoProduto.CampoProdutogbDepoisDeConsultar(
  Sender: TObject);
var
  produto: TTabelaPrecoProdutoProxy;
begin
  inherited;
  if (fdmDatasetTransiente.GetAsInteger('ID_PRODUTO') > 0) then
  begin
    produto :=TTabelaPreco.GetProdutoTabelaPreco(
      FDatasetOrdemServico.GetAsInteger('ID_TABELA_PRECO'),
      fdmDatasetTransiente.GetAsInteger('ID_PRODUTO'),
      TSistema.Sistema.Filial.Fid);
    try
      fdmDatasetTransiente.SetAsInteger('ID_PRODUTO', produto.FID);
      fdmDatasetTransiente.SetAsString('JOIN_DESCRICAO_PRODUTO', produto.FDescricao);
      fdmDatasetTransiente.SetAsString('JOIN_SIGLA_UNIDADE_ESTOQUE', produto.FUN);
      fdmDatasetTransiente.SetAsFloat('JOIN_ESTOQUE_PRODUTO', produto.FQtdeEstoque);
      fdmDatasetTransiente.SetAsFloat('VL_CUSTO', produto.FVlCusto);
      fdmDatasetTransiente.SetAsFloat('VL_UNITARIO', produto.FVlVenda);
    finally
      //FreeAndNil(produto);
    end;
  end;
end;

procedure TFrameOrdemServicoProduto.EdtAcrescimoExit(Sender: TObject);
begin
  inherited;
  ActConfirm.Execute;
end;

procedure TFrameOrdemServicoProduto.SetConfiguracoesIniciais(AOrdemServico,
  AOrdemServicoProduto: TgbClientDataset);
begin
  FDatasetOrdemServico := AOrdemServico;
  SetConfiguracoesIniciais(AOrdemServicoProduto);
end;

end.


