unit uFramePessoaRestricao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, dxSkinsdxBarPainter, dxBar, dxRibbonRadialMenu,
  Datasnap.DBClient, uGBClientDataset, Vcl.Menus, dxBarExtItems, cxClasses,
  System.Actions, Vcl.ActnList, JvExControls, JvButton, JvTransparentButton,
  uGBPanel, dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, cxMaskEdit, cxButtonEdit, cxDBEdit,
  uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, cxDropDownEdit,
  cxCalendar, uGBDBDateEdit, cxCheckBox, uGBDBCheckBox;

type
  TFramePessoaRestricao = class(TFrameDetailTransientePadrao)
    lbDescricao: TLabel;
    lbQtdeFuncionarios: TLabel;
    edtDescricaoRestricao: TgbDBTextEdit;
    edtDescricaoTipoRestricao: TgbDBTextEdit;
    edtTipoRestricao: TgbDBButtonEditFK;
    Label1: TLabel;
    gbDBCheckBox1: TgbDBCheckBox;
    EdtCadastradoEm: TgbDBDateEdit;
    procedure edtDescricaoRestricaoExit(Sender: TObject);
  private
    FDatasetPessoa: TgbClientDataset;
  public
    procedure AntesDeConfirmar; override;
     procedure SetConfiguracoesIniciais(APessoa,
       APessoaRestricao: TgbClientDataset); overload;
  end;

var
  FramePessoaRestricao: TFramePessoaRestricao;

implementation

{$R *.dfm}

uses uCadPessoa, uDatasetUtils;

procedure TFramePessoaRestricao.AntesDeConfirmar;
var
  idPessoa: Integer;
begin
  idPessoa := FDatasetPessoa.GetAsInteger('ID');

  if idPessoa > 0 then
  begin
    fdmDatasetTransiente.SetAsInteger('ID_PESSOA', idPessoa);
  end;
end;

procedure TFramePessoaRestricao.edtDescricaoRestricaoExit(Sender: TObject);
const
  NAO_EXIBIR_MENSAGEM = false;
begin
  inherited;
  if TValidacaoCampo.CampoMaiorQueZero(edtTipoRestricao.DataBinding.Field, NAO_EXIBIR_MENSAGEM) and
    TValidacaoCampo.CampoPreenchido(edtDescricaoRestricao.DataBinding.Field, NAO_EXIBIR_MENSAGEM) then
  begin
    ActConfirm.Execute;
  end;
end;

procedure TFramePessoaRestricao.SetConfiguracoesIniciais(APessoa,
  APessoaRestricao: TgbClientDataset);
begin
  FDatasetPessoa := APessoa;
  SetConfiguracoesIniciais(APessoaRestricao);
end;

end.
