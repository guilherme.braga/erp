unit uFramePessoaSetor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBarExtItems, dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel,
  dxBevel, cxGroupBox, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  uGBDBTextEdit, cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK;

type
  TFramePessoaSetor = class(TFrameDetailPadrao)
    lbCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    lbDescricao: TLabel;
    lbQtdeFuncionarios: TLabel;
    edtQtFuncionarios: TgbDBTextEdit;
    descCidade: TgbDBTextEdit;
    edtCidade: TgbDBButtonEditFK;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FramePessoaSetor: TFramePessoaSetor;

implementation

{$R *.dfm}

uses uCadPessoa;

end.
