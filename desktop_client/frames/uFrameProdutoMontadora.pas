unit uFrameProdutoMontadora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, Vcl.StdCtrls, uGBPanel, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGrid, cxTextEdit, cxMemo, cxDBEdit, uGBDBMemo, cxButtonEdit, cxMRUEdit, cxBlobEdit, cxDropDownEdit,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxLabel, uGBLabel, Datasnap.DBClient,
  uGBClientDataset, Vcl.Grids, Vcl.DBGrids;

type
  TFrameProdutoMontadora = class(TFramePadrao)
    gridModelos: TcxGrid;
    viewModelos: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    gridMontadoras: TcxGrid;
    viewMontadoras: TcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    dsMontadora: TDataSource;
    dsModeloMontadora: TDataSource;
    viewMontadorasMONTADORA: TcxGridDBBandedColumn;
    viewModelosMODELO: TcxGridDBBandedColumn;
    dsListaModelo: TDataSource;
    dsListaMontadora: TDataSource;
    cdsListaMontadora: TGBClientDataSet;
    cdsListaMontadoraID: TAutoIncField;
    cdsListaMontadoraDESCRICAO: TStringField;
    cdsListaMontadoraBO_ATIVO: TStringField;
    cdsListaMontadoraOBSERVACAO: TBlobField;
    viewMontadorasREFERENCIA: TcxGridDBBandedColumn;
    viewMontadorasESPECIFICACAO: TcxGridDBBandedColumn;
    cdsListaModelo: TGBClientDataSet;
    cdsListaModeloID: TAutoIncField;
    cdsListaModeloDESCRICAO: TStringField;
    cdsListaModeloBO_ATIVO: TStringField;
    cdsListaModeloOBSERVACAO: TBlobField;
    cdsListaModeloID_MONTADORA: TIntegerField;
    procedure gridMontadorasEnter(Sender: TObject);
    procedure viewModelosMODELOPropertiesEditValueChanged(Sender: TObject);
    procedure viewMontadorasEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure viewModelosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
  public
    procedure SetDatasets(ADatasetMontadora, ADatasetModeloMontadora: TDataset);
    procedure FiltrarConsultaModelo;
    procedure CarregarDadosMontadora;
    procedure CarregarDadosModeloMontadora;
  protected
    procedure AoCriarFrame; override;
  end;

var
  FrameProdutoMontadora: TFrameProdutoMontadora;

implementation

{$R *.dfm}

uses uCadProduto, uConsultaGrid, uDmConnection, uMontadora;

{ TFramePadrao1 }

procedure TFrameProdutoMontadora.AoCriarFrame;
begin
  inherited;
  CarregarDadosMontadora;
  CarregarDadosModeloMontadora;
end;

procedure TFrameProdutoMontadora.CarregarDadosModeloMontadora;
begin
  cdsListaModelo.Fechar;
  cdsListaModelo.Abrir;
end;

procedure TFrameProdutoMontadora.CarregarDadosMontadora;
begin
  cdsListaMontadora.Fechar;
  cdsListaMontadora.Abrir;
end;

procedure TFrameProdutoMontadora.FiltrarConsultaModelo;
var
  idMontadora: Integer;
begin
  if cdsListaModelo.Filtered then
  begin
    cdsListaModelo.Filtered := false;
  end;

  idMontadora := TMontadora.GetIdMontadoraPorDescricao(dsMontadora.Dataset.FieldByName('MONTADORA').AsString);

  cdsListaModelo.Filter := 'id_montadora = '+InttoStr(idMontadora);
  cdsListaModelo.Filtered := true;
end;

procedure TFrameProdutoMontadora.gridMontadorasEnter(Sender: TObject);
begin
  inherited;
  tcxBlobEditProperties(viewMontadorasESPECIFICACAO.Properties).PopupWidth :=
    viewMontadorasESPECIFICACAO.Width;
end;

procedure TFrameProdutoMontadora.SetDatasets(ADatasetMontadora, ADatasetModeloMontadora: TDataset);
begin
  dsMontadora.Dataset := ADatasetMontadora;
  dsModeloMontadora.Dataset := ADatasetModeloMontadora;
  FiltrarConsultaModelo;
end;

procedure TFrameProdutoMontadora.viewModelosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if (dsModeloMontadora.Dataset.State in [dsEdit]) and
      (not dsModeloMontadora.Dataset.FieldByName('MODELO').AsString.IsEmpty) then
    begin
      TgbClientDataset(dsModeloMontadora.Dataset).Salvar;
    end;

    keybd_event(VK_INSERT, 0, 0, 0);
  end;
end;

procedure TFrameProdutoMontadora.viewModelosMODELOPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if not(TgbClientDataset(dsModeloMontadora.Dataset).FieldByName('MODELO').AsString.IsEmpty) and
    (TgbClientDataset(dsModeloMontadora.Dataset).State in [dsInsert]) then
  begin
    TgbClientDataset(dsModeloMontadora.Dataset).Salvar;
  end;
end;

procedure TFrameProdutoMontadora.viewMontadorasEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if (dsMontadora.Dataset.State in [dsEdit]) and
      (not dsMontadora.Dataset.FieldByName('MONTADORA').AsString.IsEmpty) then
    begin
      TgbClientDataset(dsMontadora.Dataset).Salvar;
    end;

    keybd_event(VK_INSERT, 0, 0, 0);
  end;
end;

end.
