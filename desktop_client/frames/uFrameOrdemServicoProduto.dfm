inherited FrameOrdemServicoProduto: TFrameOrdemServicoProduto
  Width = 895
  inherited cxpcMain: TcxPageControl
    Top = 102
    Width = 895
    Height = 203
    ExplicitTop = 102
    ExplicitHeight = 203
    ClientRectBottom = 203
    ClientRectRight = 895
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 179
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 179
        ExplicitHeight = 179
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 179
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 179
        Height = 179
        Width = 895
        inherited dxBevel1: TdxBevel
          Width = 891
          ExplicitWidth = 879
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Produtos'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
    Width = 895
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 83
    Height = 83
    Width = 895
    object Label9: TLabel [0]
      Left = 6
      Top = 7
      Width = 38
      Height = 13
      Caption = 'Produto'
    end
    object Label1: TLabel [1]
      Left = 538
      Top = 7
      Width = 14
      Height = 13
      Caption = 'UN'
    end
    object Label3: TLabel [2]
      Left = 601
      Top = 7
      Width = 39
      Height = 13
      Caption = 'Estoque'
    end
    object Label5: TLabel [3]
      Left = 6
      Top = 32
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label2: TLabel [4]
      Left = 264
      Top = 32
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label8: TLabel [5]
      Left = 532
      Top = 32
      Width = 53
      Height = 13
      Caption = 'Valor Bruto'
    end
    object Label18: TLabel [6]
      Left = 532
      Top = 57
      Width = 60
      Height = 13
      Caption = 'Valor Liqu'#237'do'
    end
    object Label4: TLabel [7]
      Left = 6
      Top = 57
      Width = 45
      Height = 13
      Caption = 'Desconto'
    end
    object Label6: TLabel [8]
      Left = 264
      Top = 57
      Width = 48
      Height = 13
      Caption = 'Acr'#233'scimo'
    end
    inherited gbPanel3: TgbPanel
      Left = 707
      Top = 51
      TabOrder = 10
      ExplicitLeft = 707
      ExplicitTop = 51
    end
    object CampoProduto: TgbDBButtonEditFK
      Left = 64
      Top = 3
      DataBinding.DataField = 'ID_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 0
      gbTextEdit = gbDBTextEdit3
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 
        'ID_PRODUTO;JOIN_DESCRICAO_PRODUTO;JOIN_SIGLA_UNIDADE_ESTOQUE;JOI' +
        'N_ESTOQUE_PRODUTO;VL_CUSTO;VL_UNITARIO '
      gbTableName = 'PRODUTO'
      gbCamposConsulta = 'ID;DESCRICAO;SIGLA;QT_ESTOQUE;VL_CUSTO;VL_VENDA'
      gbDepoisDeConsultar = CampoProdutogbDepoisDeConsultar
      gbIdentificadorConsulta = 'PRODUTO'
      Width = 60
    end
    object gbDBTextEdit3: TgbDBTextEdit
      Left = 121
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_DESCRICAO_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      gbReadyOnly = True
      gbPassword = False
      Width = 411
    end
    object gbDBTextEdit6: TgbDBTextEdit
      Left = 556
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 35
    end
    object gbDBTextEdit7: TgbDBTextEdit
      Left = 644
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_ESTOQUE_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      gbReadyOnly = True
      gbPassword = False
      Width = 64
    end
    object EdtQuantidade: TgbDBCalcEdit
      Left = 64
      Top = 28
      DataBinding.DataField = 'QUANTIDADE'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorBruto
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      gbRequired = True
      Width = 120
    end
    object EdtValorUnitario: TgbDBCalcEdit
      Left = 332
      Top = 28
      DataBinding.DataField = 'VL_UNITARIO'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorBruto
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      gbRequired = True
      Width = 120
    end
    object EdtValorBruto: TgbDBCalcEdit
      Left = 595
      Top = 28
      TabStop = False
      DataBinding.DataField = 'VL_BRUTO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorLiquido
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      gbReadyOnly = True
      gbRequired = True
      Width = 113
    end
    object EdtValorLiquido: TgbDBCalcEdit
      Left = 595
      Top = 53
      TabStop = False
      DataBinding.DataField = 'VL_LIQUIDO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 9
      gbReadyOnly = True
      gbRequired = True
      Width = 113
    end
    object EdtDesconto: TGBDBValorComPercentual
      Left = 64
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      Transparent = True
      gbFieldValorBase = 'VL_BRUTO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'VL_DESCONTO'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_DESCONTO'
      gbDataSourcePercentual = dsDataFrame
      gbAoAlterarValor = CalcularValorLiquido
      gbAoAlterarPercentual = CalcularValorLiquido
      Height = 21
      Width = 194
    end
    object EdtAcrescimo: TGBDBValorComPercentual
      Left = 332
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      Transparent = True
      OnExit = EdtAcrescimoExit
      gbFieldValorBase = 'VL_BRUTO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'VL_ACRESCIMO'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_ACRESCIMO'
      gbDataSourcePercentual = dsDataFrame
      gbAoAlterarValor = CalcularValorLiquido
      gbAoAlterarPercentual = CalcularValorLiquido
      Height = 21
      Width = 194
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
