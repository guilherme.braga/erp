unit uFrameTipoQuitacaoCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, uGBDBDateEdit, uGBPanel,
  cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, cxCalc, uGBDBCalcEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, cxButtonEdit, uGBDBButtonEditFK,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TFrameTipoQuitacaoCheque = class(TFramePadrao)
    labelSacado: TLabel;
    labelDocFederal: TLabel;
    labelBanco: TLabel;
    edtSacado: TgbDBTextEdit;
    edtDocFederal: TgbDBTextEdit;
    edtBanco: TgbDBTextEdit;
    edtDtEmissao: TgbDBDateEdit;
    edtDtVencimento: TgbDBDateEdit;
    EdtValor: TgbDBCalcEdit;
    PnTituloCheque: TgbPanel;
    labelVlCheque: TLabel;
    labelDtEmissao: TLabel;
    labelDtVencimento: TLabel;
    PnDados: TgbPanel;
    PnDadosDireita: TgbPanel;
    PnDadosEsquerda: TgbPanel;
    PnTopo3PainelEsquerda: TgbPanel;
    PnTopo3PainelEsquerdaPainel2: TgbPanel;
    PnTopo3PainelEsquerdaPainel1: TgbPanel;
    labelAgencia: TLabel;
    labelContaCorrente: TLabel;
    edtAgencia: TgbDBTextEdit;
    edtContaCorrente: TgbDBTextEdit;
    labelNumero: TLabel;
    edtNumero: TgbDBTextEdit;
    EdtConsultaCheque: TgbDBButtonEditFK;
    lbConsultaCheque: TLabel;
    fdmChequeTerceiro: TgbFDMemTable;
    fdmChequeTerceiroID_CHEQUE: TIntegerField;
    dsChequeTerceiro: TDataSource;
    PnTopo1PainelEsquerda: TgbPanel;
    PnTopo2PainelEsquerda: TgbPanel;
    PnConsultaCheque: TgbPanel;
    PnSacado: TgbPanel;
    procedure edtDocFederalExit(Sender: TObject);
    procedure EventoExitConsultaCheque(Sender: TObject);
    procedure EventoButtonClickConsultaCheque(Sender: TObject; AButtonIndex: Integer);
    procedure EventoDblClickConsultaCheque(Sender: TObject);
    procedure fdmChequeTerceiroID_CHEQUEChange(Sender: TField);
  private
    procedure PreencherDadosDoCheque(const AIdCheque: Integer);
    procedure LimparTodosCampos;
    procedure DeixarCamposSomenteLeitura;
    procedure RemoverSomenteLeituraCampos;
    procedure AplicarSomenteLeituraNosCampos(ASomenteLeitura: Boolean);
  public
    procedure ConfigurarParaChequeProprio;
    procedure ConfigurarParaChequeTerceiro;
    procedure ValidarDados;
    procedure PreencherDadosPessoa(const AIdPessoa: Integer);
  protected
    procedure AoCriarFrame; override;
  end;

var
  FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque;

implementation

{$R *.dfm}

uses uTPessoa, uTMessage, uDatasetUtils, uCheque, uChequeProxy, uPesqCheque, uPessoaProxy;

{ TFrameTipoQuitacaoCheque }

procedure TFrameTipoQuitacaoCheque.AoCriarFrame;
begin
  inherited;
  if Assigned(Self) then
  begin
    PnTituloCheque.Transparent := false;
    ConfigurarParaChequeProprio;
    EdtConsultaCheque.OnExit := EventoExitConsultaCheque;
    EdtConsultaCheque.Properties.OnButtonClick := EventoButtonClickConsultaCheque;
    EdtConsultaCheque.OnDblClick := EventoDblClickConsultaCheque;
  end;
end;

procedure TFrameTipoQuitacaoCheque.AplicarSomenteLeituraNosCampos(ASomenteLeitura: Boolean);
begin
  edtSacado.gbReadyOnly := ASomenteLeitura;
  edtDocFederal.gbReadyOnly := ASomenteLeitura;
  EdtValor.gbReadyOnly := ASomenteLeitura;
  edtNumero.gbReadyOnly := ASomenteLeitura;
  EdtBanco.gbReadyOnly := ASomenteLeitura;
  EdtAgencia.gbReadyOnly := ASomenteLeitura;
  EdtContaCorrente.gbReadyOnly := ASomenteLeitura;
  EdtDtEmissao.gbReadyOnly := ASomenteLeitura;
  EdtDtVencimento.gbReadyOnly := ASomenteLeitura;
end;

procedure TFrameTipoQuitacaoCheque.ConfigurarParaChequeProprio;
begin
  if Assigned(Self) then
  begin
    //lbConsultaCheque.Visible := false;
    //EdtConsultaCheque.Visible := false;
    //labelSacado.Left := 5;
    //edtSacado.Left := 46;

    PnConsultaCheque.Visible := false;

    if not fdmChequeTerceiroID_CHEQUE.AsString.IsEmpty then
    begin
      fdmChequeTerceiroID_CHEQUE.Clear;
    end;

    RemoverSomenteLeituraCampos;
  end;
end;

procedure TFrameTipoQuitacaoCheque.ConfigurarParaChequeTerceiro;
begin
  if Assigned(Self) then
  begin
    PnConsultaCheque.Visible := true;
    //lbConsultaCheque.Visible := true;
    //EdtConsultaCheque.Visible := true;
    //labelSacado.Left := 123;
    //edtSacado.Left := 160;
    DeixarCamposSomenteLeitura;

    if not fdmChequeTerceiro.Active then
    begin
      fdmChequeTerceiro.CreateDataset;
      fdmChequeTerceiro.Append;
    end;

    if fdmChequeTerceiroID_CHEQUE.AsString.IsEmpty then
    begin
      LimparTodosCampos;
    end;
  end;
end;

procedure TFrameTipoQuitacaoCheque.DeixarCamposSomenteLeitura;
begin
  AplicarSomenteLeituraNosCampos(true);
end;

procedure TFrameTipoQuitacaoCheque.edtDocFederalExit(Sender: TObject);
begin
  inherited;
  if Assigned(edtDocFederal.DataBinding.Field) and
    not(edtDocFederal.DataBinding.Field.AsString.IsEmpty) and
    not(TPessoa.ValidaCnpjCeiCpf(edtDocFederal.DataBinding.Field.AsString))
  then
  begin
    edtDocFederal.DataBinding.Field.Clear;
    edtDocFederal.DataBinding.Field.FocusControl;
  end;
end;

procedure TFrameTipoQuitacaoCheque.EventoButtonClickConsultaCheque(Sender: TObject; AButtonIndex: Integer);
begin
  TPesqCheque.ExecutarConsulta(fdmChequeTerceiroID_CHEQUE,terceiro);
end;

procedure TFrameTipoQuitacaoCheque.EventoDblClickConsultaCheque(Sender: TObject);
begin
  TPesqCheque.ExecutarConsulta(fdmChequeTerceiroID_CHEQUE,terceiro);
end;

procedure TFrameTipoQuitacaoCheque.EventoExitConsultaCheque(Sender: TObject);
begin
  TPesqCheque.ExecutarConsultaOculta(fdmChequeTerceiroID_CHEQUE,terceiro);
end;

procedure TFrameTipoQuitacaoCheque.fdmChequeTerceiroID_CHEQUEChange(Sender: TField);
begin
  inherited;
  if Sender.AsString.IsEmpty then
  begin
    LimparTodosCampos;
  end
  else
  begin
    PreencherDadosDoCheque(fdmChequeTerceiroID_CHEQUE.AsInteger);
  end;
end;

procedure TFrameTipoQuitacaoCheque.LimparTodosCampos;
var
  dataset: TDataset;
begin
  dataset := edtSacado.DataBinding.Datasource.DataSet;

  if not Assigned(dataset) then
  begin
    Exit;
  end;

  if not(dataset.State in dsEditModes) then
  begin
    Exit;
  end;

  dataset.FieldByName(edtSacado.DataBinding.DataField).Clear;
  dataset.FieldByName(edtDocFederal.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtValor.DataBinding.DataField).Clear;
  dataset.FieldByName(edtNumero.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtBanco.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtAgencia.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtContaCorrente.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtDtEmissao.DataBinding.DataField).Clear;
  dataset.FieldByName(EdtDtVencimento.DataBinding.DataField).Clear;
end;

procedure TFrameTipoQuitacaoCheque.PreencherDadosDoCheque(const AIdCheque: Integer);
var
  cheque: TChequeProxy;
  dataset: TDataset;
begin
  dataset := edtSacado.DataBinding.Datasource.DataSet;

  if not Assigned(dataset) then
  begin
    Exit;
  end;

  cheque := TChequeProxy.Create;
  try
    cheque := TCheque.GetCheque(AIdCheque);

    if cheque.FId > 0 then
    begin
      dataset.FieldByName(edtSacado.DataBinding.DataField).AsString := cheque.FSacado;
      dataset.FieldByName(edtDocFederal.DataBinding.DataField).AsString := cheque.FDocFederal;
      dataset.FieldByName(EdtValor.DataBinding.DataField).AsFloat := cheque.FVlCheque;
      dataset.FieldByName(edtNumero.DataBinding.DataField).AsInteger := cheque.FNumero;
      dataset.FieldByName(EdtBanco.DataBinding.DataField).AsString := cheque.FBanco;
      dataset.FieldByName(EdtAgencia.DataBinding.DataField).AsString := cheque.FAgencia;
      dataset.FieldByName(EdtContaCorrente.DataBinding.DataField).AsString := cheque.FContaCorrente;
      dataset.FieldByName(EdtDtEmissao.DataBinding.DataField).AsString := cheque.FDtEmissao;
      dataset.FieldByName(EdtDtVencimento.DataBinding.DataField).AsString := cheque.FDtVencimento;
    end;
  finally
    FreeAndNil(cheque);
  end;
end;

procedure TFrameTipoQuitacaoCheque.PreencherDadosPessoa(const AIdPessoa: Integer);
var
  pessoa: TPessoaProxy;
  dataset: TDataset;
begin
  dataset := edtSacado.DataBinding.Datasource.DataSet;

  if not Assigned(dataset) then
  begin
    Exit;
  end;

  pessoa := TPessoaProxy.Create;
  try
    pessoa := TPessoa.GetPessoa(AIdPessoa);
    dataset.FieldByName(edtSacado.DataBinding.DataField).AsString := pessoa.FNome;
    dataset.FieldByName(edtDocFederal.DataBinding.DataField).AsString := pessoa.FDocFederal;
  finally
    FreeAndNil(pessoa);
  end;
end;

procedure TFrameTipoQuitacaoCheque.RemoverSomenteLeituraCampos;
begin
  AplicarSomenteLeituraNosCampos(false);
end;

procedure TFrameTipoQuitacaoCheque.ValidarDados;
begin
  if Assigned(edtSacado.DataBinding.Field) then
  begin
   TValidacaoCampo.CampoPreenchido(edtSacado.DataBinding.Field);
  end;

  if Assigned(edtDocFederal.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtDocFederal.DataBinding.Field);
  end;

  if Assigned(edtNumero.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtNumero.DataBinding.Field);
  end;

  if Assigned(edtBanco.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtBanco.DataBinding.Field);
  end;

  if Assigned(edtAgencia.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtAgencia.DataBinding.Field);
  end;

  if Assigned(edtContaCorrente.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtContaCorrente.DataBinding.Field);
  end;

  if Assigned(edtDtEmissao.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtDtEmissao.DataBinding.Field);
  end;

  if Assigned(edtDtVencimento.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(edtDtVencimento.DataBinding.Field);
  end;

  if Assigned(EdtValor.DataBinding.Field) then
  begin
    TValidacaoCampo.CampoPreenchido(EdtValor.DataBinding.Field);
  end;
end;

end.
