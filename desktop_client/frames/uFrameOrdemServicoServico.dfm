inherited FrameOrdemServicoServico: TFrameOrdemServicoServico
  inherited cxpcMain: TcxPageControl
    Top = 98
    Height = 207
    ExplicitTop = 98
    ExplicitHeight = 207
    ClientRectBottom = 207
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 183
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 183
        ExplicitHeight = 183
        inherited Level1BandedTableView1: TcxGridDBBandedTableView
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
        end
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 183
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 183
        Height = 183
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 787
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Servi'#231'os'
    Style.IsFontAssigned = True
    TabOrder = 4
    Transparent = True
  end
  inherited gbDadosTransiente: TgbPanel
    ExplicitHeight = 79
    Height = 79
    object Label7: TLabel [0]
      Left = 6
      Top = 7
      Width = 35
      Height = 13
      Caption = 'Servi'#231'o'
    end
    object Label1: TLabel [1]
      Left = 532
      Top = 7
      Width = 78
      Height = 13
      Caption = 'Tempo Estimado'
    end
    object Label5: TLabel [2]
      Left = 6
      Top = 32
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label2: TLabel [3]
      Left = 264
      Top = 32
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label8: TLabel [4]
      Left = 532
      Top = 32
      Width = 53
      Height = 13
      Caption = 'Valor Bruto'
    end
    object Label9: TLabel [5]
      Left = 532
      Top = 57
      Width = 60
      Height = 13
      Caption = 'Valor Liqu'#237'do'
    end
    object Label4: TLabel [6]
      Left = 6
      Top = 57
      Width = 45
      Height = 13
      Caption = 'Desconto'
    end
    object Label6: TLabel [7]
      Left = 264
      Top = 57
      Width = 48
      Height = 13
      Caption = 'Acr'#233'scimo'
    end
    inherited gbPanel3: TgbPanel
      Left = 730
      Top = 52
      ExplicitLeft = 730
      ExplicitTop = 52
    end
    object CampoServico: TgbDBButtonEditFK
      Left = 64
      Top = 3
      DataBinding.DataField = 'ID_SERVICO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 1
      gbTextEdit = gbDBTextEdit1
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 'ID_SERVICO;JOIN_DESCRICAO_SERVICO'
      gbTableName = 'SERVICO'
      gbCamposConsulta = 'ID;DESCRICAO'
      gbDepoisDeConsultar = CampoServicogbDepoisDeConsultar
      gbIdentificadorConsulta = 'SERVICO'
      Width = 60
    end
    object gbDBTextEdit1: TgbDBTextEdit
      Left = 121
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_DESCRICAO_SERVICO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 405
    end
    object gbDBTextEdit2: TgbDBTextEdit
      Left = 613
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_DURACAO_SERVICO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      gbReadyOnly = True
      gbPassword = False
      Width = 113
    end
    object EdtQuantidade: TgbDBCalcEdit
      Left = 64
      Top = 28
      DataBinding.DataField = 'QUANTIDADE'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorBruto
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      gbRequired = True
      Width = 120
    end
    object EdtValorUnitario: TgbDBCalcEdit
      Left = 332
      Top = 28
      DataBinding.DataField = 'VL_UNITARIO'
      DataBinding.DataSource = dsDataFrame
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorBruto
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      gbRequired = True
      Width = 120
    end
    object EdtValorBruto: TgbDBCalcEdit
      Left = 613
      Top = 28
      TabStop = False
      DataBinding.DataField = 'VL_BRUTO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Properties.OnEditValueChanged = CalcularValorLiquido
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      gbReadyOnly = True
      gbRequired = True
      Width = 113
    end
    object EdtValorLiquido: TgbDBCalcEdit
      Left = 613
      Top = 53
      TabStop = False
      DataBinding.DataField = 'VL_LIQUIDO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.DisplayFormat = '###,###,###,###,###,##0.00'
      Properties.ImmediatePost = True
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 9
      gbReadyOnly = True
      gbRequired = True
      Width = 113
    end
    object EdtDesconto: TGBDBValorComPercentual
      Left = 64
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      Transparent = True
      gbFieldValorBase = 'VL_BRUTO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'VL_DESCONTO'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_DESCONTO'
      gbDataSourcePercentual = dsDataFrame
      gbAoAlterarValor = CalcularValorLiquido
      gbAoAlterarPercentual = CalcularValorLiquido
      Height = 21
      Width = 194
    end
    object EdtAcrescimo: TGBDBValorComPercentual
      Left = 332
      Top = 53
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      Transparent = True
      OnExit = EdtAcrescimoExit
      gbFieldValorBase = 'VL_BRUTO'
      gbDataSourceValorBase = dsDataFrame
      gbFieldValor = 'VL_ACRESCIMO'
      gbDataSourceValor = dsDataFrame
      gbFieldPercentual = 'PERC_ACRESCIMO'
      gbDataSourcePercentual = dsDataFrame
      gbAoAlterarValor = CalcularValorLiquido
      gbAoAlterarPercentual = CalcularValorLiquido
      Height = 21
      Width = 194
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dsDataFrame: TDataSource
    DataSet = MovOrdemServico.cdsOrdemServicoServico
  end
end
