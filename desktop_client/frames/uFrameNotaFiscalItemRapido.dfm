inherited FrameNotaFiscalItemRapido: TFrameNotaFiscalItemRapido
  Width = 934
  Height = 461
  inherited cxpcMain: TcxPageControl
    Top = 124
    Width = 934
    Height = 337
    ExplicitTop = 124
    ExplicitHeight = 181
    ClientRectBottom = 337
    ClientRectRight = 934
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 157
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 934
        Height = 313
        ExplicitHeight = 157
        inherited Level1BandedTableView1: TcxGridDBBandedTableView
          PopupMenu = RadialPopUpMenuTransientePadrao
          DataController.DataSource = dsItensDaNota
        end
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 565
      ExplicitHeight = 182
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitWidth = 565
        ExplicitHeight = 182
        Height = 313
        Width = 934
        inherited dxBevel1: TdxBevel
          Width = 930
          ExplicitWidth = 708
        end
      end
    end
  end
  object gbPanel1: TgbPanel [1]
    Left = 0
    Top = 19
    Align = alTop
    Alignment = alTopCenter
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    ParentBackground = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Transparent = True
    ExplicitWidth = 451
    Height = 83
    Width = 934
    object Label9: TLabel
      Left = 6
      Top = 7
      Width = 38
      Height = 13
      Caption = 'Produto'
    end
    object Label5: TLabel
      Left = 6
      Top = 32
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label1: TLabel
      Left = 771
      Top = 7
      Width = 14
      Height = 13
      Caption = 'UN'
    end
    object Label3: TLabel
      Left = 818
      Top = 7
      Width = 39
      Height = 13
      Caption = 'Estoque'
    end
    object Label2: TLabel
      Left = 183
      Top = 32
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label4: TLabel
      Left = 367
      Top = 32
      Width = 45
      Height = 13
      Caption = 'Desconto'
    end
    object Label7: TLabel
      Left = 560
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Acr'#233'scimo'
    end
    object Label14: TLabel
      Left = 542
      Top = 32
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label15: TLabel
      Left = 738
      Top = 32
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label8: TLabel
      Left = 758
      Top = 32
      Width = 60
      Height = 13
      Caption = 'Valor L'#237'quido'
    end
    object Label10: TLabel
      Left = 6
      Top = 57
      Width = 26
      Height = 13
      Caption = 'Frete'
    end
    object Label11: TLabel
      Left = 213
      Top = 57
      Width = 34
      Height = 13
      Caption = 'Seguro'
    end
    object Label12: TLabel
      Left = 372
      Top = 57
      Width = 40
      Height = 13
      Caption = 'ICMS ST'
    end
    object Label13: TLabel
      Left = 594
      Top = 57
      Width = 14
      Height = 13
      Caption = 'IPI'
    end
    object Label16: TLabel
      Left = 738
      Top = 57
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label17: TLabel
      Left = 542
      Top = 57
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label18: TLabel
      Left = 767
      Top = 57
      Width = 51
      Height = 13
      Caption = 'Valor Total'
    end
    object Label19: TLabel
      Left = 349
      Top = 57
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label20: TLabel
      Left = 165
      Top = 57
      Width = 11
      Height = 13
      Caption = '%'
    end
    object JvTransparentButton1: TJvTransparentButton
      Left = 932
      Top = 51
      Width = 35
      Height = 23
      Action = ActConfirm
      FrameStyle = fsNone
      Images.ActiveImage = DmAcesso.cxImage16x16
      Images.ActiveIndex = 18
      Images.GrayImage = DmAcesso.cxImage16x16
      Images.GrayIndex = 18
      Images.DisabledImage = DmAcesso.cxImage16x16
      Images.DisabledIndex = 18
      Images.DownImage = DmAcesso.cxImage16x16
      Images.DownIndex = 18
      Images.HotImage = DmAcesso.cxImage16x16
      Images.HotIndex = 18
    end
    object JvTransparentButton2: TJvTransparentButton
      Left = 968
      Top = 51
      Width = 35
      Height = 23
      Action = ActCancel
      FrameStyle = fsNone
      Images.ActiveImage = DmAcesso.cxImage16x16
      Images.ActiveIndex = 21
      Images.GrayImage = DmAcesso.cxImage16x16
      Images.GrayIndex = 21
      Images.DisabledImage = DmAcesso.cxImage16x16
      Images.DisabledIndex = 21
      Images.DownImage = DmAcesso.cxImage16x16
      Images.DownIndex = 21
      Images.HotImage = DmAcesso.cxImage16x16
      Images.HotIndex = 21
    end
    object CampoProduto: TgbDBButtonEditFK
      Left = 66
      Top = 3
      DataBinding.DataField = 'ID_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.CharCase = ecUpperCase
      Properties.ClickKey = 13
      Properties.ReadOnly = False
      Style.Color = 14606074
      TabOrder = 0
      gbTextEdit = gbDBTextEdit3
      gbRequired = True
      gbCampoPK = 'ID'
      gbCamposRetorno = 
        'ID_PRODUTO;JOIN_DESCRICAO_PRODUTO;JOIN_SIGLA_UNIDADE_ESTOQUE;JOI' +
        'N_ESTOQUE_PRODUTO'
      gbTableName = 'PRODUTO'
      gbCamposConsulta = 'ID;DESCRICAO;SIGLA;QT_ESTOQUE'
      gbDepoisDeConsultar = CampoProdutogbDepoisDeConsultar
      gbIdentificadorConsulta = 'PRODUTO'
      Width = 60
    end
    object gbDBTextEdit3: TgbDBTextEdit
      Left = 123
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_DESCRICAO_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      gbReadyOnly = True
      gbPassword = False
      Width = 641
    end
    object gbDBTextEdit1: TgbDBTextEdit
      Left = 66
      Top = 28
      DataBinding.DataField = 'I_QCOM'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      gbRequired = True
      gbPassword = False
      Width = 97
    end
    object gbDBTextEdit6: TgbDBTextEdit
      Left = 789
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      gbReadyOnly = True
      gbPassword = False
      Width = 25
    end
    object gbDBTextEdit7: TgbDBTextEdit
      Left = 861
      Top = 3
      TabStop = False
      DataBinding.DataField = 'JOIN_ESTOQUE_PRODUTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      gbReadyOnly = True
      gbPassword = False
      Width = 68
    end
    object gbDBTextEdit2: TgbDBTextEdit
      Left = 251
      Top = 28
      DataBinding.DataField = 'I_VUNCOM'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      gbRequired = True
      gbPassword = False
      Width = 97
    end
    object gbDBTextEdit4: TgbDBTextEdit
      Left = 416
      Top = 28
      DataBinding.DataField = 'I_VDESC'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      gbRequired = True
      gbPassword = False
      Width = 87
    end
    object gbDBTextEdit5: TgbDBTextEdit
      Left = 612
      Top = 28
      DataBinding.DataField = 'I_VOUTRO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 8
      gbRequired = True
      gbPassword = False
      Width = 87
    end
    object gbDBTextEdit8: TgbDBTextEdit
      Left = 502
      Top = 28
      DataBinding.DataField = 'PERC_DESCONTO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 7
      gbRequired = True
      gbPassword = False
      Width = 38
    end
    object gbDBTextEdit9: TgbDBTextEdit
      Left = 698
      Top = 28
      DataBinding.DataField = 'PERC_ACRESCIMO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 9
      gbRequired = True
      gbPassword = False
      Width = 38
    end
    object gbDBTextEdit10: TgbDBTextEdit
      Left = 822
      Top = 28
      TabStop = False
      DataBinding.DataField = 'VL_LIQUIDO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 10
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 107
    end
    object gbDBTextEdit11: TgbDBTextEdit
      Left = 66
      Top = 53
      DataBinding.DataField = 'I_VFRETE'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 11
      gbRequired = True
      gbPassword = False
      Width = 60
    end
    object gbDBTextEdit12: TgbDBTextEdit
      Left = 251
      Top = 53
      DataBinding.DataField = 'I_VSEG'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 13
      gbRequired = True
      gbPassword = False
      Width = 60
    end
    object gbDBTextEdit13: TgbDBTextEdit
      Left = 416
      Top = 53
      DataBinding.DataField = 'N_NVICMSST'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 15
      gbRequired = True
      gbPassword = False
      Width = 87
    end
    object gbDBTextEdit14: TgbDBTextEdit
      Left = 612
      Top = 53
      DataBinding.DataField = 'O_VIPI'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 17
      gbRequired = True
      gbPassword = False
      Width = 87
    end
    object gbDBTextEdit15: TgbDBTextEdit
      Left = 698
      Top = 53
      DataBinding.DataField = 'PERC_IPI'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 18
      OnExit = gbDBTextEdit15Exit
      gbRequired = True
      gbPassword = False
      Width = 38
    end
    object gbDBTextEdit16: TgbDBTextEdit
      Left = 502
      Top = 53
      DataBinding.DataField = 'PERC_ICMS_ST'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 16
      gbRequired = True
      gbPassword = False
      Width = 38
    end
    object gbDBTextEdit17: TgbDBTextEdit
      Left = 822
      Top = 53
      TabStop = False
      DataBinding.DataField = 'VL_CUSTO_IMPOSTO'
      DataBinding.DataSource = dsDataFrame
      ParentFont = False
      Properties.CharCase = ecUpperCase
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.Color = clGradientActiveCaption
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 19
      gbReadyOnly = True
      gbRequired = True
      gbPassword = False
      Width = 107
    end
    object gbDBTextEdit18: TgbDBTextEdit
      Left = 310
      Top = 53
      DataBinding.DataField = 'PERC_SEGURO'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 14
      gbRequired = True
      gbPassword = False
      Width = 38
    end
    object gbDBTextEdit19: TgbDBTextEdit
      Left = 125
      Top = 53
      DataBinding.DataField = 'PERC_FRETE'
      DataBinding.DataSource = dsDataFrame
      Properties.CharCase = ecUpperCase
      Style.BorderStyle = ebsOffice11
      Style.Color = 14606074
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 12
      gbRequired = True
      gbPassword = False
      Width = 38
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Itens da Nota Fiscal'
    Style.IsFontAssigned = True
    TabOrder = 6
    Width = 934
  end
  inherited ActionListMain: TActionList
    object ActAlterarProdutoGrade: TAction
      Category = 'Manager'
      Caption = 'Alterar Grade'
      Visible = False
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
    inherited dxBarManager: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbImprimir'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'siMaisAcoes'
        end>
    end
    inherited barNavegador: TdxBar
      DockedLeft = 405
    end
    object dxBarButton6: TdxBarButton [13]
      Action = ActAlterar_Colunas_Grid
      Category = 0
    end
    object dxBarButton7: TdxBarButton [14]
      Action = ActExibirAgrupamento
      Category = 0
    end
    object dxBarButton8: TdxBarButton [15]
      Action = ActSalvarConfiguracoes
      Category = 0
    end
    object dxBarButton9: TdxBarButton [17]
      Action = ActAlterarProdutoGrade
      Category = 0
    end
  end
  object dsItensDaNota: TDataSource
    DataSet = MovNotaFiscal.cdsNotaFiscalItem
    Left = 526
    Top = 256
  end
  object RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
        ItemName = 'dxBarButton6'
      end
      item
        Visible = True
        ItemName = 'dxBarButton7'
      end
      item
        Visible = True
        ItemName = 'dxBarButton8'
      end>
    Images = DmAcesso.cxImage16x16
    UseOwnFont = False
    Left = 312
    Top = 152
  end
end
