inherited FrameRelatorioFRFormulario: TFrameRelatorioFRFormulario
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 451
      ExplicitHeight = 240
    end
    inherited cxtsData: TcxTabSheet
      inherited cxGBDadosMain: TcxGroupBox
        DesignSize = (
          451
          240)
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 654
        end
        object Label1: TLabel
          Left = 8
          Top = 9
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 50
          Height = 13
          Caption = 'Formul'#225'rio'
        end
        object Label3: TLabel
          Left = 8
          Top = 87
          Width = 24
          Height = 13
          Caption = 'A'#231#227'o'
        end
        object Label4: TLabel
          Left = 8
          Top = 135
          Width = 36
          Height = 13
          Caption = 'Usu'#225'rio'
        end
        object Label5: TLabel
          Left = 8
          Top = 64
          Width = 51
          Height = 13
          Caption = 'Nome Real'
        end
        object Label6: TLabel
          Left = 8
          Top = 111
          Width = 54
          Height = 13
          Caption = 'Impressora'
        end
        object Label7: TLabel
          Left = 341
          Top = 111
          Width = 32
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'C'#243'pias'
        end
        object gbDBTextEditPK1: TgbDBTextEditPK
          Left = 68
          Top = 5
          TabStop = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsDataFrame
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Width = 60
        end
        object ComboBoxFormularios: TgbDBComboBox
          Left = 68
          Top = 36
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'DESCRICAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsEditFixedList
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.PostPopupValueOnTab = True
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          gbRequired = True
          Width = 373
        end
        object cbAcao: TgbDBComboBox
          Left = 68
          Top = 83
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'ACAO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.Items.Strings = (
            'VISUALIZAR'
            'IMPRIMIR')
          Properties.PostPopupValueOnTab = True
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 3
          gbRequired = True
          Width = 373
        end
        object gbDBButtonEditFK1: TgbDBButtonEditFK
          Left = 68
          Top = 131
          DataBinding.DataField = 'ID_PESSOA_USUARIO'
          DataBinding.DataSource = dsDataFrame
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.CharCase = ecUpperCase
          Properties.ClickKey = 112
          Properties.ReadOnly = False
          Style.Color = 14606074
          TabOrder = 5
          gbTextEdit = gbDBTextEdit2
          gbRequired = True
          gbCampoPK = 'ID'
          gbCamposRetorno = 'ID_PESSOA_USUARIO;JOIN_USUARIO_PESSOA_USUARIO'
          gbTableName = 'PESSOA_USUARIO'
          gbCamposConsulta = 'ID;USUARIO'
          gbIdentificadorConsulta = 'PESSOA_USUARIO'
          Width = 60
        end
        object gbDBTextEdit2: TgbDBTextEdit
          Left = 125
          Top = 131
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'JOIN_USUARIO_PESSOA_USUARIO'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ReadOnly = True
          Style.BorderStyle = ebsOffice11
          Style.Color = clGradientActiveCaption
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 6
          gbReadyOnly = True
          gbPassword = False
          Width = 316
        end
        object EdtNomeFormulario: TgbDBTextEdit
          Left = 68
          Top = 59
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'NOME'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 2
          OnExit = EdtNomeFormularioExit
          gbRequired = True
          gbPassword = False
          Width = 373
        end
        object cbImpressoras: TgbDBComboBox
          Left = 68
          Top = 107
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataBinding.DataField = 'IMPRESSORA'
          DataBinding.DataSource = dsDataFrame
          Properties.CharCase = ecUpperCase
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.Items.Strings = (
            'VISUALIZAR'
            'IMPRIMIR')
          Properties.PostPopupValueOnTab = True
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 4
          gbRequired = True
          Width = 267
        end
        object gbDBSpinEdit1: TgbDBSpinEdit
          Left = 376
          Top = 107
          Anchors = [akTop, akRight]
          DataBinding.DataField = 'NUMERO_COPIAS'
          DataBinding.DataSource = dsDataFrame
          Properties.MaxValue = 100.000000000000000000
          Properties.MinValue = 1.000000000000000000
          Style.BorderStyle = ebsOffice11
          Style.Color = 14606074
          Style.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 7
          gbRequired = True
          Width = 65
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Caption = 'Vinculos de Usu'#225'rios e Formul'#225'rios'
    Style.IsFontAssigned = True
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      22
      0)
    inherited dxBarManager: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbDefinirPersonalizacaoFiltros'
        end
        item
          Visible = True
          ItemName = 'bbLimparDefinicaoFiltros'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbImprimir'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'siMaisAcoes'
        end>
    end
    inherited barNavegador: TdxBar
      DockedLeft = 387
    end
    object bbDefinirPersonalizacaoFiltros: TdxBarButton [14]
      Action = ActDefinirPersonalizacaoFiltros
      Category = 0
    end
    object bbLimparDefinicaoFiltros: TdxBarButton [15]
      Action = ActLimparDefinicaoFiltros
      Category = 0
    end
  end
  inherited ActionListAssistent: TActionList
    Left = 364
    Top = 240
    object ActDefinirPersonalizacaoFiltros: TAction
      Category = 'filter'
      Caption = 'Definir Filtros'
      Hint = 'Realiza a Defini'#231#227'o dos Filtros'
      OnExecute = ActDefinirPersonalizacaoFiltrosExecute
    end
    object ActLimparDefinicaoFiltros: TAction
      Category = 'filter'
      Caption = 'Restaurar Filtros'
      Hint = 'Limpa a defini'#231#227'o de filtros'
      OnExecute = ActLimparDefinicaoFiltrosExecute
    end
  end
  inherited PMGridPesquisa: TPopupMenu
    Left = 336
    Top = 240
  end
end
