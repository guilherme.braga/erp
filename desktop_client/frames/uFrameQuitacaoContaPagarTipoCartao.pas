unit uFrameQuitacaoContaPagarTipoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCartao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxCalc, uGBDBCalcEdit, uGBPanel,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, Vcl.StdCtrls, cxGroupBox, Data.DB;

type
  TFrameQuitacaoContaPagarTipoCartao = class(TFrameTipoQuitacaoCartao)
    dsFrame: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoContaPagarTipoCartao: TFrameQuitacaoContaPagarTipoCartao;

implementation

{$R *.dfm}

uses uMovContaPagar;

{ TFrameQuitacaoContaPagarTipoCartao }

procedure TFrameQuitacaoContaPagarTipoCartao.SetDataset(ADataset: TDataset);
begin
  dsFrame.Dataset := ADataset;
end;

end.
