inherited FrameQuitacaoContaPagarTipoCartao: TFrameQuitacaoContaPagarTipoCartao
  inherited cxGBDadosMain: TcxGroupBox
    inherited edtDtVencimento: TgbDBDateEdit
      TabStop = False
      DataBinding.DataField = 'DT_QUITACAO'
      DataBinding.DataSource = dsFrame
      Properties.ReadOnly = True
      Style.Color = clGradientActiveCaption
      gbReadyOnly = True
    end
    inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
      DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      DataBinding.DataSource = dsFrame
    end
    inherited PnTituloCartao: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited EdtValor: TgbDBCalcEdit
      TabStop = False
      DataBinding.DataField = 'VL_TOTAL'
      DataBinding.DataSource = dsFrame
      Properties.ReadOnly = True
      Style.Color = clGradientActiveCaption
      gbReadyOnly = True
    end
    inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
      DataBinding.DataField = 'ID_OPERADORA_CARTAO'
      DataBinding.DataSource = dsFrame
    end
  end
  object dsFrame: TDataSource
    DataSet = MovContaPagar.cdsContaPagarQuitacao
    Left = 360
    Top = 8
  end
end
