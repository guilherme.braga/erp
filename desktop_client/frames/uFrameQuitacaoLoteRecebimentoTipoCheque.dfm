inherited FrameQuitacaoLoteRecebimentoTipoCheque: TFrameQuitacaoLoteRecebimentoTipoCheque
  inherited cxGBDadosMain: TcxGroupBox
    inherited PnTituloCheque: TgbPanel
      Style.IsFontAssigned = True
    end
    inherited PnDados: TgbPanel
      inherited PnDadosDireita: TgbPanel
        inherited edtDocFederal: TgbDBTextEdit
          DataBinding.DataField = 'CHEQUE_DOC_FEDERAL'
          DataBinding.DataSource = dsFrame
        end
        inherited edtDtVencimento: TgbDBDateEdit
          DataBinding.DataField = 'CHEQUE_DT_VENCIMENTO'
          DataBinding.DataSource = dsFrame
        end
        inherited EdtValor: TgbDBCalcEdit
          DataBinding.DataField = 'VL_TOTAL'
          DataBinding.DataSource = dsFrame
        end
      end
      inherited PnDadosEsquerda: TgbPanel
        inherited PnTopo3PainelEsquerda: TgbPanel
          inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
            inherited edtNumero: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_NUMERO'
              DataBinding.DataSource = dsFrame
            end
          end
          inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
            inherited edtAgencia: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_AGENCIA'
              DataBinding.DataSource = dsFrame
            end
            inherited edtContaCorrente: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_CONTA_CORRENTE'
              DataBinding.DataSource = dsFrame
            end
          end
        end
        inherited PnTopo1PainelEsquerda: TgbPanel
          inherited PnSacado: TgbPanel
            inherited edtSacado: TgbDBTextEdit
              DataBinding.DataField = 'CHEQUE_SACADO'
              DataBinding.DataSource = dsFrame
            end
          end
        end
        inherited PnTopo2PainelEsquerda: TgbPanel
          inherited edtBanco: TgbDBTextEdit
            DataBinding.DataField = 'CHEQUE_BANCO'
            DataBinding.DataSource = dsFrame
          end
          inherited edtDtEmissao: TgbDBDateEdit
            DataBinding.DataField = 'CHEQUE_DT_EMISSAO'
            DataBinding.DataSource = dsFrame
          end
        end
      end
    end
  end
  object dsFrame: TDataSource
    DataSet = MovLoteRecebimento.cdsLoteRecebimentoQuitacao
    Left = 360
    Top = 8
  end
end
