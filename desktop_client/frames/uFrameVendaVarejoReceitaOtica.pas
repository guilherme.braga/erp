unit uFrameVendaVarejoReceitaOtica;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailTransientePadrao,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, dxBar,
  dxRibbonRadialMenu, Datasnap.DBClient, uGBClientDataset, Vcl.Menus,
  dxBarExtItems, cxClasses, System.Actions, Vcl.ActnList, JvExControls,
  JvButton, JvTransparentButton, uGBPanel, dxBevel, cxGroupBox, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, cxDBEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls;

type
  TFrameVendaVarejoReceitaOtica = class(TFrameDetailTransientePadrao)
    Label1: TLabel;
    campoReceitaotica: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    procedure campoReceitaoticagbDepoisDeConsultar(Sender: TObject);
  private
    { Private declarations }
  public
    procedure AoCriarFrame; override;
  end;

var
  FrameVendaVarejoReceitaOtica: TFrameVendaVarejoReceitaOtica;

implementation

{$R *.dfm}

uses uMovVendaVarejo, uSistema, uReceitaOticaProxy, uReceitaOtica, uFiltroFormulario, uCadReceitaOtica;

procedure TFrameVendaVarejoReceitaOtica.AoCriarFrame;
begin
  inherited;
  campoReceitaotica.FWhereInjetado := TFiltroPadrao.Create;
  campoReceitaotica.FWhereInjetado.AddIsNull(TReceitaOticaProxy.FIELD_VENDA);
end;

procedure TFrameVendaVarejoReceitaOtica.campoReceitaoticagbDepoisDeConsultar(Sender: TObject);
var dataset: TgbClientDataset;
  receitaOtica: TReceitaOticaProxy;
begin
  dataset := TgbClientDataset(dsDataFrame.Dataset);
  inherited;
  if (dataset.GetAsInteger('ID_RECEITA_OTICA') > 0) then
  begin
    receitaOtica := TReceitaOtica.GetReceitaOtica(dataset.GetAsInteger('ID_RECEITA_OTICA'));
    try
      dataset.SetAsInteger('JOIN_ID_PESSOA',receitaOtica.FIdPessoa);
      dataset.SetAsString('JOIN_NOME_PESSOA',receitaOtica.FNomePessoa);
      dataset.SetAsInteger('JOIN_ID_MEDICO',receitaOtica.FIdMedico);
      dataset.SetAsString('JOIN_NOME_MEDICO',receitaOtica.FNomeMedico);
      dataset.SetAsString('JOIN_ESPECIFICACAO_RECEITA_OTICA',receitaOtica.FEspecificacao);
    finally
      FreeAndNil(receitaOtica);
    end;
  end;
end;

end.
