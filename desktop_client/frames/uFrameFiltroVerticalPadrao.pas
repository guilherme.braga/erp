unit uFrameFiltroVerticalPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxVGrid, cxInplaceContainer, cxGroupBox,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxDropDownEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, Generics.Collections,
  uFiltroFormulario, ugbPanel, ugbLabel, JvExControls, JvButton, JvTransparentButton, ugbDBTextEdit,
  ugbDBDateEdit, ugbDBButtonEditFK, Vcl.StdCtrls, JvExStdCtrls, JvCombobox, JvDBCombobox, ugbDBCalcEdit,
  cxScrollBox, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, DBClient;

type
  TDominioFiltro = (inteiro, real, texto, data, hora, datahora, flag, fk, caixaSelecao);

  TCondicaoFiltro = (Igual, Entre, Contem);

  TConstantesCampoFiltro = class
    const ITENS_BO = 'Sim;N�o';
    const VALORES_BO = 'S;N';
  end;

  TCampoFK = class
    campoPK: String;
    tabelaFK: String;
    camposConsulta: String;
    camposRetorno: String;
  end;

  TCampoCaixaSelecao = class
    itens: String;
    valores: String;
  end;

  TCampoFiltro = class
    rotulo: String;
    campo: String;
    tabela: String;
    condicaoAberta: String; //substitui o campo e tabela na hora de fazer o where
    valorPadrao: String;
    valorPadraoInicial: String;
    valorPadraoFinal: String;
    condicao: TCondicaoFiltro;
    tipo: TDominioFiltro;
    campoFK: TCampoFK;
    campoCaixaSelecao: TCampoCaixaSelecao;
    somenteLeitura: boolean;
    visivel: boolean;
    obrigatorio: boolean;

    //Referencias
    painelFiltro: TgbPanel;

    constructor Create;
  end;

  TFrameFiltroVerticalPadrao = class(TFramePadrao)
    dsFiltro: TDataSource;
    StyleFiltros: TcxStyleRepository;
    FundoBranco: TcxStyle;
    fdmFiltro: TgbFDMemTable;
    PnTituloFrameFiltroVerticalPadrao: TgbPanel;
    pnScrollBox: TcxScrollBox;
    procedure dsFiltroDataChange(Sender: TObject; Field: TField);
    procedure LimparFiltroIndividual(Sender: TObject);
  private
    const CAMPO_PREENCHIDO = 'S';
    const CAMPO_NAO_PREENCHIDO = 'N';
    const COMPLEMENTO_FIELD_DETALHE_FK = 'DETALHE_FK';
    const COMPLEMENTO_FIELD_DATA_INICIO = 'INICIO';
    const COMPLEMENTO_FIELD_DATA_FIM = 'FIM';
    const TITULO_FRAME_FILTRO_VERTICAL_PADRAO = 'Informe os filtros desejados';
    procedure AjustarTabOrder;
    procedure CriarCampoNoDataset(ACampoFiltro: TCampoFiltro);
    function NovoPainel(AContainer: TCustomControl; AFiltro: TCampoFiltro): TgbPanel;
    procedure CriarBotaoLimpar(AContainer: TCustomControl; AFiltro: TCampoFiltro);
    procedure CriarCampoCondicao(AContainer: TCustomControl; AFiltro: TCampoFiltro);
    procedure CriarCampoValor(AContainer: TCustomControl; AFiltro: TCampoFiltro);
    procedure CriarRotuloPainel(AContainer: TCustomControl; AFiltro: TCampoFiltro);
    procedure CarregarFiltros;
    procedure SalvarFiltros;
    procedure PersonalizarFiltros;
    procedure AtualizarContadorDeFiltroPreenchido;
    function BuscarPainelPeloFiltro(AFiltro: TCampoFiltro): TComponent;

    procedure SetWhereContem(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereDiferente(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereEntre(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereIgual(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereMaior(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereMaiorIgual(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereMenor(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereMenorIgal(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereNaoContem(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWhereNaoPertence(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
    procedure SetWherePertence(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);

    procedure AbrirDataset;
    procedure InserirValoresPadrao;
    procedure RealizarConsultaFK(AFiltroPadrao: TCampoFiltro);
  public
    FCampos: TList<TCampoFiltro>;
    function GetNomeFieldDetalhe(const ANomeField: String): String;
    procedure SetWhere(AFiltroPadrao: TFiltroPadrao);
    procedure CriarFiltros;
    procedure ConfigurarPainelTitulo;
    procedure LimparTodosFiltros;
    function ExisteFiltros: Boolean;
    procedure OcultarFiltro(ANomeCampo: String);
    procedure ExibirFiltro(ANomeCampo: String);
    procedure AplicarPersonalizacaoFiltro(const APersonalizacaoFiltros: TClientDataset);
    class procedure AplicarPersonalizacaoNosFiltros(const AListaCampos: TList<TCampoFiltro>;
      const APersonalizacaoFiltros: TClientDataset);
    procedure ValidarFiltrosObrigatorios;
    function BuscarCampoFiltroPorNomeCampo(ANomeCampo: String): TCampoFiltro;
  protected
    procedure AoCriarFrame; override;
    procedure AoDestruirFrame; override;
  end;

var
  FrameFiltroVerticalPadrao: TFrameFiltroVerticalPadrao;

implementation

{$R *.dfm}

uses uDmAcesso, uFrmConsultaPadrao, uControlsUtils, uDatasetUtils;

{ TFrameFiltroVerticalPadrao }

procedure TFrameFiltroVerticalPadrao.AoCriarFrame;
begin
  inherited;
  CarregarFiltros;
  FCampos := TList<TCampoFiltro>.Create;
  Self.Align := alClient;
  Self.Width := 315;
  ConfigurarPainelTitulo;
end;

procedure TFrameFiltroVerticalPadrao.ConfigurarPainelTitulo;
begin
  PnTituloFrameFiltroVerticalPadrao.Transparent := false;
  PnTituloFrameFiltroVerticalPadrao.PanelStyle.OfficeBackgroundKind := pobkGradient;
end;

procedure TFrameFiltroVerticalPadrao.AoDestruirFrame;
begin
  inherited;
  if Assigned(FCampos) then
  begin
    FreeAndNil(FCampos);
  end;
end;

procedure TFrameFiltroVerticalPadrao.AplicarPersonalizacaoFiltro(const APersonalizacaoFiltros: TClientDataset);
begin
  TFrameFiltroVerticalPadrao.AplicarPersonalizacaoNosFiltros(FCampos, APersonalizacaoFiltros);
end;

class procedure TFrameFiltroVerticalPadrao.AplicarPersonalizacaoNosFiltros(
  const AListaCampos: TList<TCampoFiltro>; const APersonalizacaoFiltros: TClientDataset);
var
  iCamposPadrao: Integer;
begin
  while not APersonalizacaoFiltros.Eof do
  begin
    for iCamposPadrao := 0 to AListaCampos.Count -1 do
    begin
      if (AListaCampos[iCamposPadrao].Tabela = APersonalizacaoFiltros.FieldByName('TABELA').AsString) and
        (AListaCampos[iCamposPadrao].Campo = APersonalizacaoFiltros.FieldByName('CAMPO').AsString) then
      begin
        //FCampos[iCamposPadrao].rotulo := AListaCamposPersonalizados[iCamposPersonalizados].rotulo;
        //FCampos[iCamposPadrao].campo := AListaCamposPersonalizados[iCamposPersonalizados].campo;
        //FCampos[iCamposPadrao].tabela := AListaCamposPersonalizados[iCamposPersonalizados].tabela;
        //FCampos[iCamposPadrao].condicaoAberta := AListaCamposPersonalizados[iCamposPersonalizados].condicaoAberta;
        //FCampos[iCamposPadrao].valorPadrao := AListaCamposPersonalizados[iCamposPersonalizados].valorPadrao;
        //FCampos[iCamposPadrao].valorPadraoInicial := AListaCamposPersonalizados[iCamposPersonalizados].valorPadraoInicial;
        //FCampos[iCamposPadrao].valorPadraoFinal := AListaCamposPersonalizados[iCamposPersonalizados].valorPadraoFinal;
        //FCampos[iCamposPadrao].condicao := AListaCamposPersonalizados[iCamposPersonalizados].condicao;
        //FCampos[iCamposPadrao].tipo := AListaCamposPersonalizados[iCamposPersonalizados].tipo;
        //FCampos[iCamposPadrao].campoFK := AListaCamposPersonalizados[iCamposPersonalizados].campoFK;
        //FCampos[iCamposPadrao].campoCaixaSelecao := AListaCamposPersonalizados[iCamposPersonalizados].campoCaixaSelecao;
        //FCampos[iCamposPadrao].somenteLeitura := AListaCamposPersonalizados[iCamposPersonalizados].somenteLeitura;
        AListaCampos[iCamposPadrao].visivel := APersonalizacaoFiltros.FieldByName('VISIVEL').AsBoolean;
        AListaCampos[iCamposPadrao].obrigatorio := APersonalizacaoFiltros.FieldByName('OBRIGATORIO').AsBoolean;
      end;
    end;
    APersonalizacaoFiltros.Next;
  end;
end;

procedure TFrameFiltroVerticalPadrao.AtualizarContadorDeFiltroPreenchido;
var
  detalhamento: String;
  i: Integer;
  filtrosAtivos: Integer;
begin
  filtrosAtivos := 0;
  try
    for i := 0 to Pred(fdmFiltro.FieldCount) do
    begin
      if (not fdmFiltro.Fields[i].Origin.IsEmpty) and
        (not fdmFiltro.Fields[i].AsString.IsEmpty) and
        fdmFiltro.Fields[i].Visible then
      begin
        Inc(filtrosAtivos);
      end;
    end;

    if filtrosAtivos > 0 then
    begin
      detalhamento := ' ('+InttoStr(filtrosAtivos)+')';
    end;
  finally
    PnTituloFrameFiltroVerticalPadrao.Caption := TITULO_FRAME_FILTRO_VERTICAL_PADRAO+detalhamento;
  end;
end;

procedure TFrameFiltroVerticalPadrao.CarregarFiltros;
begin
  //Para tornar essa tela dinamica
end;

procedure TFrameFiltroVerticalPadrao.CriarCampoNoDataset(ACampoFiltro: TCampoFiltro);
var
  fieldPrincipal: TField;
  fieldSecundario: TField;
begin
  case ACampoFiltro.tipo of
    inteiro:
    begin
      fieldPrincipal := TIntegerField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;
    end;
    real:
    begin
      fieldPrincipal := TFloatField.Create(fdmFiltro);
      TFloatField(fieldPrincipal).DisplayFormat := '###,###,###,##0.00';
      fieldPrincipal.FieldName := ACampoFiltro.campo;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;
    end;
    texto:
    begin
      fieldPrincipal := TStringField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;
    end;
    data:
    begin
      fieldPrincipal := TDateField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;

      fieldSecundario := TDateField.Create(fdmFiltro);
      fieldSecundario.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.DisplayLabel := ACampoFiltro.rotulo;
      fieldSecundario.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.Index := fdmFiltro.FieldCount;
      fieldSecundario.DataSet := fdmFiltro;
      fieldSecundario.FieldKind := fkData;
      fieldSecundario.Origin := '';
      fieldSecundario.Visible := ACampoFiltro.visivel;
    end;
    hora:
    begin
      fieldPrincipal := TTimeField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;

      fieldSecundario := TTimeField.Create(fdmFiltro);
      fieldSecundario.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.DisplayLabel := ACampoFiltro.rotulo;
      fieldSecundario.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.Index := fdmFiltro.FieldCount;
      fieldSecundario.DataSet := fdmFiltro;
      fieldSecundario.FieldKind := fkData;
      fieldSecundario.Origin := '';
      fieldSecundario.Visible := ACampoFiltro.visivel;
    end;
    datahora:
    begin
      fieldPrincipal := TDateTimeField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;

      fieldSecundario := TDateTimeField.Create(fdmFiltro);
      fieldSecundario.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.DisplayLabel := ACampoFiltro.rotulo;
      fieldSecundario.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
      fieldSecundario.Index := fdmFiltro.FieldCount;
      fieldSecundario.DataSet := fdmFiltro;
      fieldSecundario.FieldKind := fkData;
      fieldSecundario.Origin := '';
      fieldSecundario.Visible := ACampoFiltro.visivel;
    end;
    flag:
    begin
      fieldPrincipal := TStringField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;
    end;
    fk:
    begin
      fieldPrincipal := TIntegerField.Create(fdmFiltro);

      fieldPrincipal := TIntegerField.Create(fdmFiltro);
      fieldPrincipal.FieldName := ACampoFiltro.campo;
      fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
      fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
      fieldPrincipal.Index := fdmFiltro.FieldCount;
      fieldPrincipal.DataSet := fdmFiltro;
      fieldPrincipal.FieldKind := fkData;
      fieldPrincipal.Origin := ACampoFiltro.campo;
      fieldPrincipal.Visible := ACampoFiltro.visivel;

      fieldSecundario := TStringField.Create(fdmFiltro);
      fieldSecundario.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DETALHE_FK;
      fieldSecundario.DisplayLabel := ACampoFiltro.rotulo;
      fieldSecundario.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DETALHE_FK;
      fieldSecundario.Index := fdmFiltro.FieldCount;
      fieldSecundario.DataSet := fdmFiltro;
      fieldSecundario.FieldKind := fkData;
      fieldSecundario.Origin := '';
      fieldSecundario.Visible := ACampoFiltro.visivel;
    end;
    caixaSelecao:
    begin
      case ACampoFiltro.condicao of
        igual:
        begin
          fieldPrincipal := TStringField.Create(fdmFiltro);
          fieldPrincipal.FieldName := ACampoFiltro.campo;
          fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
          fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo;
          fieldPrincipal.Index := fdmFiltro.FieldCount;
          fieldPrincipal.DataSet := fdmFiltro;
          fieldPrincipal.FieldKind := fkData;
          fieldPrincipal.Origin := ACampoFiltro.campo;
          fieldPrincipal.Visible := ACampoFiltro.visivel;
        end;
        entre:
        begin
          fieldPrincipal := TStringField.Create(fdmFiltro);
          fieldPrincipal.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
          fieldPrincipal.DisplayLabel := ACampoFiltro.rotulo;
          fieldPrincipal.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_INICIO;
          fieldPrincipal.Index := fdmFiltro.FieldCount;
          fieldPrincipal.DataSet := fdmFiltro;
          fieldPrincipal.FieldKind := fkData;
          fieldPrincipal.Origin := ACampoFiltro.campo;
          fieldPrincipal.Visible := ACampoFiltro.visivel;

          fieldSecundario := TStringField.Create(fdmFiltro);
          fieldSecundario.FieldName := ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
          fieldSecundario.DisplayLabel := ACampoFiltro.rotulo;
          fieldSecundario.Name := fdmFiltro.Name + ACampoFiltro.campo+COMPLEMENTO_FIELD_DATA_FIM;
          fieldSecundario.Index := fdmFiltro.FieldCount;
          fieldSecundario.DataSet := fdmFiltro;
          fieldSecundario.FieldKind := fkData;
          fieldSecundario.Origin := '';
          fieldSecundario.Visible := ACampoFiltro.visivel;
        end;
      end;
    end;
  end;

  if Assigned(fieldPrincipal) then
  begin
    fieldPrincipal.Required := ACampoFiltro.Obrigatorio;
  end;
end;

function TFrameFiltroVerticalPadrao.NovoPainel(AContainer: TCustomControl; AFiltro: TCampoFiltro): TgbPanel;
begin
  result := TgbPanel.Create(AContainer);
  result.Align := alTop;
  result.Height := 40;
  result.Name := 'pn'+AFiltro.campo;
  result.Caption := '';
  result.Style.BorderStyle := ebsNone;
  result.PanelStyle.Active := true;
  result.PanelStyle.OfficeBackgroundKind := pobkOffice11Color;
  result.Parent := AContainer;
  result.Visible := AFiltro.Visivel;

  AFiltro.painelFiltro := result;
end;

procedure TFrameFiltroVerticalPadrao.OcultarFiltro(ANomeCampo: String);
var
  i: Integer;
  field: TField;
begin
  for i := 0 to Pred(FCampos.Count) do
  begin
    if FCampos[i].Campo = ANomeCampo then
    begin
      if Assigned(FCampos[i].painelFiltro) then
      begin
        FCampos[i].painelFiltro.Visible := false;
      end;

      field := fdmFiltro.findField(ANomeCampo);
      if Assigned(field) then
      begin
        field.Visible := false;
      end;

      break;
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.CriarRotuloPainel(AContainer: TCustomControl; AFiltro: TCampoFiltro);
var
  rotulo: TgbLabel;
begin
  rotulo := TgbLabel.Create(AContainer);
  rotulo.Parent := AContainer;
  rotulo.Top := 0;
  rotulo.Left := 2;
  rotulo.AutoSize := false;
  rotulo.Width := 178;
  rotulo.Name := 'lbRotulo'+AFiltro.Campo;
  rotulo.Caption := AFiltro.Rotulo;
  rotulo.Style.TextStyle := [fsBold];
end;

procedure TFrameFiltroVerticalPadrao.dsFiltroDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  AtualizarContadorDeFiltroPreenchido;
end;

procedure TFrameFiltroVerticalPadrao.ExibirFiltro(ANomeCampo: String);
var
  i: Integer;
  field: TField;
begin
  for i := 0 to Pred(FCampos.Count) do
  begin
    if FCampos[i].Campo = ANomeCampo then
    begin
      if Assigned(FCampos[i].painelFiltro) then
      begin
        FCampos[i].painelFiltro.Visible := true;
      end;

      field := fdmFiltro.findField(ANomeCampo);
      if Assigned(field) then
      begin
        field.Visible := false;
      end;

      break;
    end;
  end;
end;

function TFrameFiltroVerticalPadrao.ExisteFiltros: Boolean;
begin
  result := FCampos.Count > 0;
end;

function TFrameFiltroVerticalPadrao.GetNomeFieldDetalhe(const ANomeField: String): String;
begin
  result := ANomeField+COMPLEMENTO_FIELD_DETALHE_FK;
end;

procedure TFrameFiltroVerticalPadrao.InserirValoresPadrao;
var i: Integer;
  filtro: TCampoFiltro;
begin
  for i := Pred(FCampos.Count) Downto 0 do
  begin
    filtro := FCampos[i];

    if filtro.tipo = fk then
    begin
      if not filtro.valorPadrao.IsEmpty then
      begin
        fdmFiltro.FieldByName(filtro.campo).AsString := filtro.valorPadrao;
        RealizarConsultaFK(filtro);
      end;
    end
    else
    begin
      case filtro.condicao of
        igual:
        begin
          if not filtro.valorPadrao.IsEmpty then
          begin
            fdmFiltro.FieldByName(filtro.campo).AsString := filtro.valorPadrao;
          end;
        end;
        entre:
        begin
          if (not filtro.valorPadraoInicial.IsEmpty) and (not filtro.valorPadraoFinal.IsEmpty) then
          begin
            fdmFiltro.FieldByName(filtro.campo+COMPLEMENTO_FIELD_DATA_INICIO).AsString := filtro.valorPadraoInicial;
            fdmFiltro.FieldByName(filtro.campo+COMPLEMENTO_FIELD_DATA_FIM).AsString := filtro.valorPadraoFinal;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.LimparFiltroIndividual(Sender: TObject);
var
  filtro: TCampoFiltro;
begin
  filtro := BuscarCampoFiltroPorNomeCampo(TJvTransparentButton(Sender).Hint);

  if filtro.tipo = fk then
  begin
    fdmFiltro.FieldByName(filtro.Campo).Clear;
    fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DETALHE_FK).Clear;
  end
  else if filtro.condicao = entre then
  begin
    fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_INICIO).Clear;
    fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_FIM).Clear;
  end
  else
  begin
    fdmFiltro.FieldByName(filtro.Campo).Clear;
  end;
end;

procedure TFrameFiltroVerticalPadrao.LimparTodosFiltros;
var
  i: Integer;
  filtro: TCampoFiltro;
begin
  for i := 0 to Pred(fdmFiltro.FieldCount) do
  begin
    filtro := BuscarCampoFiltroPorNomeCampo(fdmFiltro.Fields[i].Origin);

    if not filtro.SomenteLeitura then
    begin
      fdmFiltro.Fields[i].Clear;
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.CriarCampoCondicao(AContainer: TCustomControl; AFiltro: TCampoFiltro);
var
  condicao: TgbLabel;
begin
  condicao := TgbLabel.Create(AContainer);
  condicao.Parent := AContainer;
  condicao.Top := 0;
  condicao.Left := 186;
  condicao.AutoSize := false;
  condicao.Width := 97;
  condicao.Name := 'lbCondicao'+AFiltro.Campo;

  case AFiltro.condicao of
    igual:
    begin
      condicao.Caption := 'Igual';
    end;
    entre:
    begin
      condicao.Caption := 'Entre';
    end;
    contem:
    begin
      condicao.Caption := 'Cont�m';
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.CriarCampoValor(AContainer: TCustomControl; AFiltro: TCampoFiltro);
var
  campoValor: TWinControl;
  campoValorDetalhe: TWinControl;
begin
  case AFiltro.tipo of
    inteiro:
    begin
      campoValor := TgbDBTextEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 266;
      campoValor.Name := 'edtCampo'+AFiltro.Campo;
      TgbDBTextEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBTextEdit(campoValor).DataBinding.DataField := AFiltro.Campo;
      TgbDBTextEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBTextEdit(campoValor).gbRequired := AFiltro.Obrigatorio;
    end;
    real:
    begin
      campoValor := TgbDBCalcEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 266;
      campoValor.Name := 'edtCampo'+AFiltro.Campo;
      TgbDBCalcEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBCalcEdit(campoValor).DataBinding.DataField := AFiltro.Campo;
      TgbDBCalcEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBCalcEdit(campoValor).gbRequired := AFiltro.Obrigatorio;
    end;
    texto:
    begin
      campoValor := TgbDBTextEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 266;
      campoValor.Name := 'edtCampo'+AFiltro.Campo;
      TgbDBTextEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBTextEdit(campoValor).DataBinding.DataField := AFiltro.Campo;
      TgbDBTextEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBTextEdit(campoValor).gbRequired := AFiltro.Obrigatorio;
    end;
    data, hora:
    begin
      campoValor := TgbDBDateEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      TgbDBDateEdit(campoValor).gbDateTime := false;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 130;
      campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
      TgbDBDateEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBDateEdit(campoValor).DataBinding.DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
      TgbDBDateEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBDateEdit(campoValor).gbRequired := AFiltro.Obrigatorio;

      campoValor := TgbDBDateEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      TgbDBDateEdit(campoValor).gbDateTime := false;
      campoValor.Top := 15;
      campoValor.Left := 138;
      campoValor.Width := 130;
      campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
      TgbDBDateEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBDateEdit(campoValor).DataBinding.DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
      TgbDBDateEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBDateEdit(campoValor).gbRequired := AFiltro.Obrigatorio;
    end;
    datahora:
    begin
      campoValor := TgbDBDateEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      TgbDBDateEdit(campoValor).gbDateTime := true;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 130;
      campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
      TgbDBDateEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBDateEdit(campoValor).DataBinding.DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
      TgbDBDateEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBDateEdit(campoValor).gbRequired := AFiltro.Obrigatorio;

      campoValor := TgbDBDateEdit.Create(AContainer);
      campoValor.Parent := AContainer;
      TgbDBDateEdit(campoValor).gbDateTime := true;
      campoValor.Top := 15;
      campoValor.Left := 138;
      campoValor.Width := 130;
      campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
      TgbDBDateEdit(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBDateEdit(campoValor).DataBinding.DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
      TgbDBDateEdit(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBDateEdit(campoValor).gbRequired := AFiltro.Obrigatorio;
    end;
    fk:
    begin
      campoValor := TgbDBButtonEditfk.Create(AContainer);
      campoValor.Parent := AContainer;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 71;
      campoValor.Name := 'edtCampo'+AFiltro.Campo;
      TgbDBButtonEditfk(campoValor).DataBinding.DataSource := dsFiltro;
      TgbDBButtonEditfk(campoValor).DataBinding.DataField := AFiltro.Campo;
      TgbDBButtonEditfk(campoValor).gbReadyOnly := AFiltro.somenteLeitura;
      TgbDBButtonEditfk(campoValor).gbRequired := AFiltro.Obrigatorio;

      if AFiltro.campoFK.campoPK.IsEmpty then
      begin
        AFiltro.campoFK.campoPK := AFiltro.Campo;
      end;

      if AFiltro.campoFK.tabelaFK.IsEmpty then
      begin
        AFiltro.campoFK.tabelaFK := AFiltro.tabela;
      end;

      AFiltro.campoFK.camposRetorno := AFiltro.Campo+';'+AFiltro.Campo+COMPLEMENTO_FIELD_DETALHE_FK;

      TgbDBButtonEditfk(campoValor).gbCampoPK := AFiltro.campoFK.campoPK;
      TgbDBButtonEditfk(campoValor).gbCamposRetorno := AFiltro.campoFK.camposRetorno;
      TgbDBButtonEditfk(campoValor).gbCamposConsulta := AFiltro.campoFK.camposConsulta;
      TgbDBButtonEditfk(campoValor).gbTableName := AFiltro.campoFK.tabelaFK;

      campoValorDetalhe := TgbDBTextEdit.Create(AContainer);
      campoValorDetalhe.Parent := AContainer;
      campoValorDetalhe.Top := 15;
      campoValorDetalhe.Left := 70;
      campoValorDetalhe.Width := 198;
      campoValorDetalhe.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DETALHE_FK;
      TgbDBTextEdit(campoValorDetalhe).DataBinding.DataSource := dsFiltro;
      TgbDBTextEdit(campoValorDetalhe).DataBinding.DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DETALHE_FK;
      TgbDBTextEdit(campoValorDetalhe).gbReadyOnly := true;
      TgbDBTextEdit(campoValorDetalhe).gbRequired := AFiltro.Obrigatorio;

      TgbDBButtonEditfk(campoValor).gbTextEdit := TgbDBTextEdit(campoValorDetalhe);
    end;
    flag:
    begin
      campoValor := TjvDBComboBox.Create(AContainer);
      campoValor.Parent := AContainer;
      campoValor.Top := 15;
      campoValor.Left := 2;
      campoValor.Width := 266;
      campoValor.Name := 'edtCampo'+AFiltro.Campo;
      TjvDBComboBox(campoValor).DataSource := dsFiltro;
      TjvDBComboBox(campoValor).DataField := AFiltro.Campo;
      TjvDBComboBox(campoValor).UpdateFieldImmediatelly := true;
      TjvDBComboBox(campoValor).Items.Text := AFiltro.campoCaixaSelecao.Itens;
      TjvDBComboBox(campoValor).Values.Text := AFiltro.campoCaixaSelecao.Valores;
      TjvDBComboBox(campoValor).ReadOnly := AFiltro.somenteLeitura;
    end;
    caixaSelecao:
    begin
      case AFiltro.condicao of
        igual:
        begin
          campoValor := TjvDBComboBox.Create(AContainer);
          campoValor.Parent := AContainer;
          campoValor.Top := 15;
          campoValor.Left := 2;
          campoValor.Width := 266;
          campoValor.Name := 'edtCampo'+AFiltro.Campo;
          TjvDBComboBox(campoValor).DataSource := dsFiltro;
          TjvDBComboBox(campoValor).DataField := AFiltro.Campo;
          TjvDBComboBox(campoValor).UpdateFieldImmediatelly := true;
          TjvDBComboBox(campoValor).Items.Delimiter := ';';
          TjvDBComboBox(campoValor).Items.DelimitedText := AFiltro.campoCaixaSelecao.Itens;
          TjvDBComboBox(campoValor).Values.Delimiter := ';';
          TjvDBComboBox(campoValor).Values.DelimitedText := AFiltro.campoCaixaSelecao.Valores;
           TjvDBComboBox(campoValor).ReadOnly := AFiltro.somenteLeitura;
        end;
        entre:
        begin
          campoValor := TjvDBComboBox.Create(AContainer);
          campoValor.Parent := AContainer;
          campoValor.Top := 15;
          campoValor.Left := 2;
          campoValor.Width := 130;
          campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
          TjvDBComboBox(campoValor).DataSource := dsFiltro;
          TjvDBComboBox(campoValor).DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_INICIO;
          TjvDBComboBox(campoValor).UpdateFieldImmediatelly := true;
          TjvDBComboBox(campoValor).Items.Delimiter := ';';
          TjvDBComboBox(campoValor).Items.DelimitedText := AFiltro.campoCaixaSelecao.Itens;
          TjvDBComboBox(campoValor).Values.Delimiter := ';';
          TjvDBComboBox(campoValor).Values.DelimitedText := AFiltro.campoCaixaSelecao.Valores;
           TjvDBComboBox(campoValor).ReadOnly := AFiltro.somenteLeitura;

          campoValor := TjvDBComboBox.Create(AContainer);
          campoValor.Parent := AContainer;
          campoValor.Top := 15;
          campoValor.Left := 138;
          campoValor.Width := 130;
          campoValor.Name := 'edtCampo'+AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
          TjvDBComboBox(campoValor).DataSource := dsFiltro;
          TjvDBComboBox(campoValor).DataField := AFiltro.Campo+COMPLEMENTO_FIELD_DATA_FIM;
          TjvDBComboBox(campoValor).UpdateFieldImmediatelly := true;
          TjvDBComboBox(campoValor).Items.Delimiter := ';';
          TjvDBComboBox(campoValor).Items.DelimitedText := AFiltro.campoCaixaSelecao.Itens;
          TjvDBComboBox(campoValor).Values.Delimiter := ';';
          TjvDBComboBox(campoValor).Values.DelimitedText := AFiltro.campoCaixaSelecao.Valores;
          TjvDBComboBox(campoValor).ReadOnly := AFiltro.somenteLeitura;
        end;
      end;
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.CriarBotaoLimpar(AContainer: TCustomControl; AFiltro: TCampoFiltro);
var
  botao: TjvTransparentButton;
begin
  if AFiltro.SomenteLeitura then
  begin
    Exit;
  end;

  botao := TjvTransparentButton.Create(AContainer);
  botao.Parent := AContainer;
  botao.Top := 17;
  botao.Left := 270;
  botao.Height := 17;
  botao.Width := 23;
  botao.Name := 'btnLimpar'+AFiltro.Campo;
  botao.Caption := '';
  botao.Images.ActiveImage := DmAcesso.cxImage16x16;
  botao.Images.ActiveIndex := 21;
  botao.FrameStyle := fsNone;
  botao.OnClick := LimparFiltroIndividual;
  botao.Hint := AFiltro.campo;
  botao.ShowHint := false;
end;

procedure TFrameFiltroVerticalPadrao.CriarFiltros;
var
  filtro: TCampoFiltro;
  i: Integer;
  painel: TgbPanel;
  tabOrderContador: Integer;
begin
  try
    TControlsUtils.CongelarFormulario(Application.MainForm);
    tabOrderContador := 0;
    for i := Pred(FCampos.Count) Downto 0 do
    begin
      filtro := FCampos[i];

      CriarCampoNoDataset(filtro);

      painel := NovoPainel(pnScrollBox, filtro);
      painel.TabOrder := tabOrderContador;
      inc(tabOrderContador);

      CriarRotuloPainel(painel, filtro);
      CriarBotaoLimpar(painel, filtro);
      CriarCampoCondicao(painel, filtro);
      CriarCampoValor(painel, filtro);
    end;

    AjustarTabOrder;

    AbrirDataset;
    InserirValoresPadrao;
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TFrameFiltroVerticalPadrao.AbrirDataset;
begin
  fdmFiltro.CreateDataset;
  fdmFiltro.Append;
end;

procedure TFrameFiltroVerticalPadrao.AjustarTabOrder;
var
  painel: tgbPanel;
  ordem: Integer;
  i: Integer;
begin
  ordem := 0;
  for i := Pred(FCampos.Count) downto 0 do
  begin
    painel := TgbPanel(BuscarPainelPeloFiltro(FCampos[i]));

    if not Assigned(painel) then
    begin
      continue;
    end;

    painel.TabOrder := ordem;
    inc(ordem);
  end;
end;

function TFrameFiltroVerticalPadrao.BuscarCampoFiltroPorNomeCampo(ANomeCampo: String): TCampoFiltro;
var
  i: Integer;
begin
  for i := 0 to Pred(FCampos.Count) do
  begin
    if FCampos[i].Campo = ANomeCampo then
    begin
      result := FCampos[i];
      break;
    end;
  end;
end;

function TFrameFiltroVerticalPadrao.BuscarPainelPeloFiltro(AFiltro: TCampoFiltro): TComponent;
begin
  result := pnScrollBox.FindComponent('pn'+AFiltro.campo);
end;

procedure TFrameFiltroVerticalPadrao.SetWhere(AFiltroPadrao: TFiltroPadrao);
var
  i: Integer;
  field: TField;
  fieldDetalhe: TField;
  filtro: TCampoFiltro;
  condicao: String;
begin
  for i := 0 to Pred(fdmFiltro.FieldCount) do
  begin
    field := fdmFiltro.Fields[i];

    if (field.Origin.IsEmpty) then
      continue;

    filtro := BuscarCampoFiltroPorNomeCampo(field.Origin);

    if (not field.AsString.IsEmpty) then
    begin
      if filtro.condicaoAberta.IsEmpty then
      begin
        condicao := filtro.tabela+'.'+filtro.campo;
      end
      else
      begin
        condicao := filtro.condicaoAberta;
      end;

      case filtro.condicao of
        igual:
        begin
          case filtro.tipo of
            inteiro: AFiltroPadrao.AddIgual(condicao, Field.AsInteger);
            real: AFiltroPadrao.AddIgual(condicao, Field.AsFloat);
          else
            AFiltroPadrao.AddIgual(condicao, Field.AsString);
          end;
        end;
        entre:
        begin
          case filtro.tipo of
            data, hora, datahora:
            begin
              AFiltroPadrao.AddEntre(condicao,
                fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_INICIO).AsDateTime,
                fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_FIM).AsDateTime);
            end;
            caixaSelecao:
            begin
              AFiltroPadrao.AddEntre(condicao,
                fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_INICIO).AsInteger,
                fdmFiltro.FieldByName(filtro.Campo+COMPLEMENTO_FIELD_DATA_FIM).AsInteger);
            end;
          end;
        end;
        contem:
        begin
          AFiltroPadrao.AddContem(condicao, Field.AsString);
        end;
      end;

{      if fdmFiltroCONDICAO.AsString.Equals('Cont�m') then
      begin
        SetWhereContem(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Diferente') then
      begin
        SetWhereDiferente(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Entre') then
      begin
        SetWhereEntre(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Igual') then
      begin
        SetWhereIgual(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Maior') then
      begin
        SetWhereMaior(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Maior ou Igual') then
      begin
        SetWhereMaiorIgual(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Menor') then
      begin
       SetWhereMenor(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Menor ou Igual') then
      begin
        SetWhereMenorIgal(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('N�o Cont�m') then
      begin
        SetWhereNaoContem(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('N�o Pertence') then
      begin
        SetWhereNaoPertence(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end
      else if fdmFiltroCONDICAO.AsString.Equals('Pertence') then
      begin
        SetWherePertence(result, fdmFiltroCAMPO.AsString, fdmFiltroVALOR.AsString, fdmFiltroTIPO.AsString);
      end;  }
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.PersonalizarFiltros;
begin
  //para quando for dinamico
end;

procedure TFrameFiltroVerticalPadrao.RealizarConsultaFK(AFiltroPadrao: TCampoFiltro);
var
  listaCamposConsulta: TStringList;
  listaCamposRetorno: TStringList;
begin
  listaCamposConsulta := TStringList.Create;
  listaCamposRetorno := TStringList.Create;
  try
    listaCamposConsulta.Delimiter := ';';
    listaCamposConsulta.DelimitedText := AFiltroPadrao.campoFK.camposConsulta;

    listaCamposRetorno.Delimiter := ';';
    listaCamposRetorno.DelimitedText := AFiltroPadrao.campoFK.camposRetorno;

    TFrmConsultaPadrao.Consultar(
      fdmFiltro,
      AFiltroPadrao.campoFK.tabelaFK,
      listaCamposConsulta,
      listaCamposRetorno,
      '', nil, true, AFiltroPadrao.campoFK.campoPK,
      AFiltroPadrao.valorPadrao);
  finally
    if Assigned(listaCamposConsulta) then
    begin
      FreeAndNil(listaCamposConsulta);
    end;

    if Assigned(listaCamposRetorno) then
    begin
      FreeAndNil(listaCamposRetorno);
    end;
  end;
end;

procedure TFrameFiltroVerticalPadrao.SalvarFiltros;
begin
  //Para quando for dinamico
end;

procedure TFrameFiltroVerticalPadrao.SetWhereContem(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereDiferente(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereEntre(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereIgual(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin
  AFiltro.AddIgual(ACampo, AValor);
end;

procedure TFrameFiltroVerticalPadrao.SetWhereMaior(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereMaiorIgual(AFiltro: TFiltroPadrao; ACampo, AValor,
  ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereMenor(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereMenorIgal(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereNaoContem(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWhereNaoPertence(AFiltro: TFiltroPadrao; ACampo, AValor,
  ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.SetWherePertence(AFiltro: TFiltroPadrao; ACampo, AValor, ATipo: String);
begin

end;

procedure TFrameFiltroVerticalPadrao.ValidarFiltrosObrigatorios;
begin
  TDatasetUtils.ValidarCamposObrigatorios(fdmFiltro);
end;

constructor TCampoFiltro.Create;
begin
  somenteLeitura := false;
  visivel := true;
  obrigatorio := false;
end;

end.


