unit uFrameQuitacaoGeracaoDocumentoTipoCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCheque, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDMemTable, cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxCalc, uGBDBCalcEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, uGBPanel,
  cxGroupBox;

type
  TFrameQuitacaoGeracaoDocumentoTipoCheque = class(TFrameTipoQuitacaoCheque)
    dsDadosCheque: TDataSource;
  private
    { Private declarations }
  public
    procedure SetDataset(ADataset: TDataset);
  end;

var
  FrameQuitacaoGeracaoDocumentoTipoCheque: TFrameQuitacaoGeracaoDocumentoTipoCheque;

implementation

{$R *.dfm}

uses uMovGeracaoDocumento;

procedure TFrameQuitacaoGeracaoDocumentoTipoCheque.SetDataset(ADataset: TDataset);
begin
  dsDadosCheque.Dataset := ADataset;
end;

end.
