unit uFrameTipoQuitacaoCrediario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFramePadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxGroupBox,
  cxCalc, cxDBEdit, uGBDBCalcEdit, uGBPanel, uGBDBTextEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit, Vcl.StdCtrls,
  cxButtonEdit, uGBDBButtonEditFK, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrameTipoQuitacaoCrediario = class(TFramePadrao)
    labelDtVencimento: TLabel;
    edtDtVencimento: TgbDBDateEdit;
    Label1: TLabel;
    PnTituloCartao: TgbPanel;
    labelVlCheque: TLabel;
    EdtValor: TgbDBCalcEdit;
    EdtHistorico: TgbDBTextEdit;
    PnCampos: TgbPanel;
    PnHistorico: TgbPanel;
    pnDtVencimento: TgbPanel;
    PnValor: TgbPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameTipoQuitacaoCrediario: TFrameTipoQuitacaoCrediario;

implementation

{$R *.dfm}

end.
