inherited FrameDetailTransientePadrao: TFrameDetailTransientePadrao
  inherited cxpcMain: TcxPageControl
    Top = 124
    Height = 181
    ExplicitTop = 124
    ExplicitHeight = 181
    ClientRectBottom = 181
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 157
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 157
        PopupMenu = RadialPopUpMenuTransientePadrao
        ExplicitHeight = 157
      end
    end
    inherited cxtsData: TcxTabSheet
      TabVisible = False
      ExplicitHeight = 157
      inherited cxGBDadosMain: TcxGroupBox
        ExplicitHeight = 157
        Height = 157
        inherited dxBevel1: TdxBevel
          ExplicitWidth = 727
        end
      end
    end
  end
  inherited PnTituloFrameDetailPadrao: TgbPanel
    Style.IsFontAssigned = True
    Transparent = False
  end
  object gbDadosTransiente: TgbPanel [2]
    Left = 0
    Top = 19
    Align = alTop
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    ParentBackground = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Transparent = True
    Height = 105
    Width = 451
    object gbPanel3: TgbPanel
      Left = 312
      Top = 77
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 24
      Width = 74
      object JvTransparentButton1: TJvTransparentButton
        Left = 2
        Top = 2
        Width = 35
        Height = 20
        Action = ActConfirm
        Align = alLeft
        FrameStyle = fsNone
        TextAlign = ttaBottom
        Images.ActiveImage = DmAcesso.cxImage16x16
        Images.ActiveIndex = 18
        Images.GrayImage = DmAcesso.cxImage16x16
        Images.GrayIndex = 18
        Images.DisabledImage = DmAcesso.cxImage16x16
        Images.DisabledIndex = 18
        Images.DownImage = DmAcesso.cxImage16x16
        Images.DownIndex = 18
        Images.HotImage = DmAcesso.cxImage16x16
        Images.HotIndex = 18
        ExplicitLeft = -4
        ExplicitTop = 0
        ExplicitHeight = 19
      end
      object JvTransparentButton2: TJvTransparentButton
        Left = 37
        Top = 2
        Width = 35
        Height = 20
        Action = ActCancel
        Align = alLeft
        FrameStyle = fsNone
        TextAlign = ttaBottom
        Images.ActiveImage = DmAcesso.cxImage16x16
        Images.ActiveIndex = 21
        Images.GrayImage = DmAcesso.cxImage16x16
        Images.GrayIndex = 21
        Images.DisabledImage = DmAcesso.cxImage16x16
        Images.DisabledIndex = 21
        Images.DownImage = DmAcesso.cxImage16x16
        Images.DownIndex = 21
        Images.HotImage = DmAcesso.cxImage16x16
        Images.HotIndex = 21
        ExplicitLeft = 43
        ExplicitTop = -6
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      Visible = False
    end
    inherited barNavegador: TdxBar
      Visible = False
    end
    object dxBarButton6: TdxBarButton [13]
      Action = ActImprimir
      Category = 0
    end
  end
  object dsDataSetTransiente: TDataSource
    DataSet = fdmDatasetTransiente
    Left = 253
    Top = 256
  end
  object fdmDatasetTransiente: TGBClientDataSet
    Aggregates = <>
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 280
    Top = 256
  end
  object RadialPopUpMenuTransientePadrao: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbAlterarRotuloColunas'
      end
      item
        Visible = True
        ItemName = 'bbExibirAgrupamento'
      end
      item
        Visible = True
        ItemName = 'bbSalvarConfiguracoes'
      end
      item
        Visible = True
        ItemName = 'dxBarButton6'
      end>
    Images = DmAcesso.cxImage16x16
    UseOwnFont = False
    Left = 312
    Top = 152
  end
end
