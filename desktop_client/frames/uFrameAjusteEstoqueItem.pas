unit uFrameAjusteEstoqueItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameDetailPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxContainer, Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxBar, cxClasses, System.Actions, Vcl.ActnList, uGBPanel, dxBevel, cxGroupBox,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, uGBDBTextEdit,
  cxMaskEdit, cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, dxBarExtItems, cxRadioGroup, uGBDBRadioGroup,
  cxDropDownEdit, cxCalc, ugbClientDataset;

type
  TFrameAjusteEstoqueItem = class(TFrameDetailPadrao)
    lbItem: TLabel;
    edtItem: TgbDBTextEditPK;
    gbPanel1: TgbPanel;
    lbProduto: TLabel;
    lbQuantidade: TLabel;
    lbEstoqueAtual: TLabel;
    btnFKProduto: TgbDBButtonEditFK;
    edtDescProduto: TgbDBTextEdit;
    EdtQuantidade: TgbDBTextEdit;
    edtEstoqueAtual: TgbDBTextEdit;
    lbTipo: TLabel;
    gbTipo: TgbDBRadioGroup;
    lbEstoqueDesejado: TLabel;
    calcEditEstoqueDesejado: TcxCalcEdit;
    procedure CalcularEstoqueDesejado(Sender : TObject);
    procedure gbDBRadioGroup1PropertiesChange(Sender: TObject);
    procedure btnFKProdutogbDepoisDeConsultar(Sender: TObject);

  private
    FCalculandoEstoqueDesejado: Boolean;
    procedure AtualizarEstoqueDesejado;
    procedure AtualizarQuantidadePeloEstoqueDesejado;

  public

  protected
    procedure AoCriarFrame; override;
    procedure GerenciarControles; override;

  end;

var
  FrameAjusteEstoqueItem: TFrameAjusteEstoqueItem;

implementation

{$R *.dfm}

uses uMovAjusteEstoque, uAjusteEstoqueProxy, uFiltroFormulario, uProdutoProxy,
  uSistema, uProduto;

procedure TFrameAjusteEstoqueItem.AoCriarFrame;
begin
  inherited;
end;

procedure TFrameAjusteEstoqueItem.CalcularEstoqueDesejado(Sender : TObject);
begin
  if FCalculandoEstoqueDesejado then
    Exit;

  FCalculandoEstoqueDesejado := True;

  if Sender = EdtQuantidade then
  begin
    AtualizarEstoqueDesejado;
  end
  else
  if Sender = calcEditEstoqueDesejado then
  begin
    AtualizarQuantidadePeloEstoqueDesejado
  end
  else
  if (Sender = gbTipo) or (Sender = btnFKProduto) then
  begin
    AtualizarEstoqueDesejado;
  end;

  FCalculandoEstoqueDesejado := False;

end;

procedure TFrameAjusteEstoqueItem.AtualizarEstoqueDesejado;
begin
  if not Assigned(dsDataFrame.Dataset) then
    Exit;

  if dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
      TAjusteEstoqueProxy.TIPO_ENTRADA)
  then
  begin
    calcEditEstoqueDesejado.Value :=
      dsDataFrame.Dataset.FieldByName('QUANTIDADE').AsFloat +
      dsDataFrame.Dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat;
  end
  else
  if dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
    TAjusteEstoqueProxy.TIPO_SAIDA)
  then
  begin
    calcEditEstoqueDesejado.Value :=
      dsDataFrame.Dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat -
      dsDataFrame.Dataset.FieldByName('QUANTIDADE').AsFloat;
  end;
end;

procedure TFrameAjusteEstoqueItem.AtualizarQuantidadePeloEstoqueDesejado;
begin
  if dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
      TAjusteEstoqueProxy.TIPO_ENTRADA)
  then
  begin
    dsDataFrame.Dataset.FieldByName('QUANTIDADE').AsFloat :=
      calcEditEstoqueDesejado.Value -
      dsDataFrame.Dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat;
  end
  else
  if dsDataFrame.Dataset.FieldByName('TIPO').AsString.Equals(
    TAjusteEstoqueProxy.TIPO_SAIDA)
  then
  begin
    dsDataFrame.Dataset.FieldByName('QUANTIDADE').AsFloat :=
      dsDataFrame.Dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat -
      calcEditEstoqueDesejado.Value;
  end;
end;

procedure TFrameAjusteEstoqueItem.btnFKProdutogbDepoisDeConsultar(
  Sender: TObject);
var dataset: TgbClientDataset;
  produto: TProdutoProxy;
begin
  dataset := TgbClientDataset(dsDataFrame.Dataset);
  inherited;
  if (dataset.GetAsInteger('ID_PRODUTO') > 0) then
  begin
    produto :=TProduto.GetProdutoFilial(
      dataset.GetAsInteger('ID_PRODUTO'),
      TSistema.Sistema.Filial.Fid);
    try
      dataset.SetAsInteger('ID_PRODUTO', produto.FID);
      dataset.SetAsString('JOIN_DESCRICAO_PRODUTO', produto.FDescricao);
      dataset.SetAsFloat('JOIN_ESTOQUE_PRODUTO', produto.FQtdeEstoque);
    finally
      FreeAndNil(produto);
    end;
  end;
end;

procedure TFrameAjusteEstoqueItem.gbDBRadioGroup1PropertiesChange(
  Sender: TObject);
begin
  inherited;
  dsDataFrame.Dataset.FieldByName('QUANTIDADE').AsFloat := 0;
  calcEditEstoqueDesejado.Value := 0;
end;

procedure TFrameAjusteEstoqueItem.GerenciarControles;
begin
  inherited;
  AtualizarEstoqueDesejado;
end;

end.
