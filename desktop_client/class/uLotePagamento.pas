unit uLotePagamento;

interface

uses
  uLotePagamentoProxy,
  FireDAC.Comp.Client,
  Data.FireDACJSONReflect;

type TLotePagamento = class
  private

  public
    class function Efetivar(AIdLotePagamento: Integer): string;
    class function Reabrir(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
    class procedure SetContasAPagar(AFDMemTable: TFDMemTable;
                                    AIdsContaPagar: string);
  private
    class function RemoverQuitacoesIndividuais(AIdLotePagamento: Integer) : string;
    class function RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
    // private stuff goes here

end;

implementation

uses
  uDmConnection,
  System.JSON,
  REST.Json,
  DBXJSONReflect,
  uClientClasses,
  System.SysUtils, uDatasetUtils;

{ TLotePagamento }

class procedure TLotePagamento.SetContasAPagar(AFDMemTable: TFDMemTable;
                                               AIdsContaPagar: string);
var
  smLote       : TSMLotePagamentoClient;
  JSONDataset  : TFDJSONDataSets;
begin
  smLote := TSMLotePagamentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smLote.GetContasAPagar(AIdsContaPagar);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smLote.Free;
  end;
end;

class function TLotePagamento.Efetivar(AIdLotePagamento: Integer): string;
var
  smLotePagamento : TSMLotePagamentoClient;
begin
  smLotePagamento := TSMLotePagamentoClient.Create(DmConnection.Connection.DBXConnection);
  Result := smLotePagamento.GerarLancamentosAgrupados(AIdLotePagamento)
end;

class function TLotePagamento.Reabrir(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
var
  smLotePagamento : TSMLotePagamentoClient;
begin
  smLotePagamento := TSMLotePagamentoClient.Create(DmConnection.Connection.DBXConnection);
  Result := smLotePagamento.RemoverLancamentosAgrupados(AIdLotePagamento, AGerarContraPartida);
end;

class function TLotePagamento.RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
var
  smLotePagamento : TSMLotePagamentoClient;
begin
  smLotePagamento := TSMLotePagamentoClient.Create(DmConnection.Connection.DBXConnection);
  Result          := smLotePagamento.RemoverMovimentosIndividuais(AIdLotePagamento)
end;

class function TLotePagamento.RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
var
  smLotePagamento : TSMLotePagamentoClient;
begin
  smLotePagamento := TSMLotePagamentoClient.Create(DmConnection.Connection.DBXConnection);
  Result          := smLotePagamento.RemoverQuitacoesIndividuais(AIdLotePagamento);
end;

end.
