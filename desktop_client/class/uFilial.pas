unit uFilial;

interface

Uses uFilialProxy;

type TFilial = class
  class function GetIdFilial: Integer;
  class function GetCRT(AIdFilial: Integer): Integer;
  class function GetFantasia(AIdFilial: Integer): String;
  class function OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
  class function GetFilial(const AIdFilial: Integer): TFilialProxy;
end;

implementation

{ TFilial }

uses uClientClasses, uDmConnection;

class function TFilial.GetCRT(AIdFilial: Integer): Integer;
var smEmpresaFilial: TSMEmpresaFilialClient;
begin
  smEmpresaFilial := TsmEmpresaFilialClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEmpresaFilial.GetCRT(AIdFilial);
  finally
    smEmpresaFilial.Free;
  end;
end;

class function TFilial.GetFantasia(AIdFilial: Integer): String;
var smEmpresaFilial: TSMEmpresaFilialClient;
begin
  smEmpresaFilial := TsmEmpresaFilialClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEmpresaFilial.GetFantasia(AIdFilial);
  finally
    smEmpresaFilial.Free;
  end;
end;

class function TFilial.GetFilial(const AIdFilial: Integer): TFilialProxy;
var smEmpresaFilial: TSMEmpresaFilialClient;
begin
  smEmpresaFilial := TsmEmpresaFilialClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEmpresaFilial.GetFilial(AIdFilial);
  finally
    //smEmpresaFilial.Free;
  end;
end;

class function TFilial.GetIdFilial: Integer;
begin
  result := 1;
end;

class function TFilial.OptanteSimplesNacional(const AIdFilial: Integer): Boolean;
var smEmpresaFilial: TSMEmpresaFilialClient;
begin
  smEmpresaFilial := TsmEmpresaFilialClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEmpresaFilial.OptanteSimplesNacional(AIdFilial);
  finally
    smEmpresaFilial.Free;
  end;
end;

end.
