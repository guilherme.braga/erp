unit uMovimentacaoCheque;

interface

Uses uMovimentacaoChequeProxy;

type TMovimentacaoCheque = class
  public
    class function Efetivar(const AIdChaveProcesso: Integer): Boolean;
    class function Estornar(const AIdChaveProcesso: Integer): Boolean;
    class function PodeReabrir(const AIdMovimentacaoCheque: Integer): Boolean;
    class function PodeExcluir(const AIdMovimentacaoCheque: Integer): Boolean;
    class function BuscarIDPelaChaveProcesso(const AIdChaveProcesso: Integer): Integer;
end;

implementation

{ TMovimentacaoCheque }

uses uClientClasses, uDmConnection;

class function TMovimentacaoCheque.BuscarIDPelaChaveProcesso(const AIdChaveProcesso: Integer): Integer;
var smMovimentacaoCheque: TsmMovimentacaoChequeClient;
begin
  smMovimentacaoCheque := TsmMovimentacaoChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMovimentacaoCheque.BuscarIDPelaChaveProcesso(AIdChaveProcesso);
  finally
    smMovimentacaoCheque.Free;
  end;
end;

class function TMovimentacaoCheque.Efetivar(const AIdChaveProcesso: Integer): Boolean;
var smMovimentacaoCheque: TsmMovimentacaoChequeClient;
begin
  smMovimentacaoCheque := TsmMovimentacaoChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMovimentacaoCheque.Efetivar(AIdChaveProcesso);
  finally
    smMovimentacaoCheque.Free;
  end;
end;

class function TMovimentacaoCheque.Estornar(const AIdChaveProcesso: Integer): Boolean;
var smMovimentacaoCheque: TsmMovimentacaoChequeClient;
begin
  smMovimentacaoCheque := TsmMovimentacaoChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMovimentacaoCheque.Estornar(AIdChaveProcesso);
  finally
    smMovimentacaoCheque.Free;
  end;
end;

class function TMovimentacaoCheque.PodeExcluir(const AIdMovimentacaoCheque: Integer): Boolean;
var smMovimentacaoCheque: TsmMovimentacaoChequeClient;
begin
  smMovimentacaoCheque := TsmMovimentacaoChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMovimentacaoCheque.PodeExcluir(AIdMovimentacaoCheque);
  finally
    smMovimentacaoCheque.Free;
  end;
end;

class function TMovimentacaoCheque.PodeReabrir(const AIdMovimentacaoCheque: Integer): Boolean;
var smMovimentacaoCheque: TsmMovimentacaoChequeClient;
begin
  smMovimentacaoCheque := TsmMovimentacaoChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMovimentacaoCheque.PodeReabrir(AIdMovimentacaoCheque);
  finally
    smMovimentacaoCheque.Free;
  end;
end;

end.
