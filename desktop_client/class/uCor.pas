unit uCor;

interface

Uses uCorProxy, SysUtils;

type TCor = class
  private
    class function GerarCor(const ACor: TCorProxy): Integer;
    class function ExisteCor(const ADescricaoCor: String): Boolean;
  public
    class function GetCor(const AIdCor: Integer): TCorProxy;
    class function InserirCorCasoNaoExista(const ADescricaoCor: String; var AIdCor: Integer): Boolean;
    class function GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
    class function GetDescricaoCor(const AIdCor: Integer): String;
end;

Implementation

uses uClientClasses, uDmConnection;

class function TCor.ExisteCor(const ADescricaoCor: String): Boolean;
var smCor: TSMCorClient;
begin
  smCor := TSMCorClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCor.ExisteCor(ADescricaoCor);
  finally
    smCor.Free;
  end;
end;

class function TCor.GerarCor(const ACor: TCorProxy): Integer;
var smCor: TSMCorClient;
begin
  smCor := TSMCorClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCor.GerarCor(ACor);
  finally
    smCor.Free;
  end;
end;

class function TCor.GetIdCorPorDescricao(const ADescricaoCor: String): Integer;
var
  smCor: TSMCorClient;
begin
  smCor := TSMCorClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCor.GetIdCorPorDescricao(ADescricaoCor);
  finally
    smCor.Free;
  end;
end;

class function TCor.GetCor(const AIdCor: Integer): TCorProxy;
var smCor: TSMCorClient;
begin
  smCor := TSMCorClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCor.GetCor(AIdCor);
  finally
    //smCor.Free;
  end;
end;

class function TCor.GetDescricaoCor(const AIdCor: Integer): String;
var smCor: TSMCorClient;
begin
  smCor := TSMCorClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCor.GetDescricaoCor(AIdCor);
  finally
    smCor.Free;
  end;
end;

class function TCor.InserirCorCasoNaoExista(const ADescricaoCor: String; var AIdCor: Integer): Boolean;
var
  cor: TCorProxy;
begin
  result := false;
  if not TCor.ExisteCor(ADescricaoCor) then
  begin
    cor := TCorProxy.Create;
    try
      cor.FDescricao := ADescricaoCor;
      AIdCor := TCor.GerarCor(cor);
      result := true;
    finally
      //FreeAndNil(montadora);
    end;
  end
  else
  begin
    AIdCor := TCor.GetIdCorPorDescricao(ADescricaoCor);
  end;
end;

end.
