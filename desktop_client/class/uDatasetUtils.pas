unit uDatasetUtils;

interface

Uses Data.FireDACJSONReflect, FireDAC.Comp.Client, SysUtils, db, dbClient,
     generics.Collections, cxGridDBBandedTableView, FireDAC.Comp.DataSet, cxGridDBDataDefinitions;

type TDatasetUtils = class
  private

  public
    class procedure JSONDatasetToMemTable(AJSONDataset: TFDJSONDataSets;
      AFDMemTable: TFDMemTable; AResetMemTable: Boolean = true);
    class function ValorFoiAlterado(AField: TField): Boolean;
    class function GetAggregateAsFloat(AField: TAggregateField): Double;
    class function SomarColuna(AField: TField): Double;
    class function SomarColunaExpressaoMultiplicacao(AField1, AField2: TField): Double;
    class function SomarColunaComCondicao(AField: TField; AFieldCondicao: TField; AValorCondicao: Variant): Double;
    class function MaiorValor(AField: TField): Double;
    class function GetOldFloatValue(AField: TField): Double;
    class function GetOldStringValue(AField: TField): String;
    class procedure ValidarCamposObrigatorios(ADataset: TDataset);
    class procedure CopiarRegistro(ADatasetOrigem, ADatasetDestino: TDataset;
      ASomenteAlterados: Boolean = false; const AComaTextCamposQueNaoSeraoCopiados: String = '');
    class procedure RatearValores(AValorTotal,
                                  AValorDesconto: Double;
                                  AFieldQtProduto,
                                  AFieldVlProduto,
                                  AFieldVlDesconto,
                                  AFieldPcDesconto,
                                  AFieldVlBruto: TField;
                                  AAumentando: Boolean = True;
                                  AValorDescontoAnterior: Double = 0);
  class procedure CopiarRegistroComCondicoes(AQueryOrigem,
      AQueryDestino: TDataset; const ACamposQueNaoSeraoCopiados,
      ACamposQueSeraoCopiadosMasTeraoOutrosValores: array of String;
      ANovosValores: array of Variant); static;
  class function Concatenar(AField: TField): string;
  class function ConcatenarComCondicao(AField: TField; AFieldCondicao: TField; AValorCondicao: Variant): string;
  class function ConcatenarRegistrosFiltradosNaView(AField: TField; AView: TcxGridDBBandedTableView): string;
  class function ConcatenarRegistrosSelecionadosNaView(AField: TField; AView: TcxGridDBBandedTableView): string;
  class function ConcatenarRegistrosFiltradosNoDataControllerDaView(
    AField: TField; ADataController: TcxGridDBDataController): string;
  class function SomarRegistrosFiltradosNaView(var AField: TField; AView: TcxGridDBBandedTableView): Double;
  class function ConcatenarRegistrosFiltradosNaViewComCriterios(AField: TField;
    AView: TcxGridDBBandedTableView; AFieldCriterio: TField; AValorCriterio: String): string;
  class function ValorExistente(Afield: TField; AValue: Variant): Boolean;
  class function ClientDatasetToClientDatasetDataStream(const AClientDataset: TClientDataset): String;
  class function ClientDatasetDataStreamToClientDataset(const ADataStream: String): TClientDataset;
  class procedure ConfigurarMascaraFloat(const ADataset: TDataset);
end;

type TValidacaoCampo = class
  public
    class function CampoMaiorQueZero(AField: TField; AExibirMensagem: Boolean = true; AAbort: Boolean = true): Boolean;
    class function CampoPreenchido(AField: TField; AExibirMensagem: Boolean = true; AAbort: Boolean = true): Boolean;
end;

implementation

{ TDatasetUtils }

uses
  uFrmMessage,
  System.Variants,
  System.Classes,
  uMathUtils,
  StrUtils;

class function TDatasetUtils.ConcatenarComCondicao(AField: TField; AFieldCondicao: TField;
  AValorCondicao: Variant): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CloneCursor(TFDMemTable(AField.Dataset), true);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    cdsClone.First;
    while not cdsClone.Eof do
    try
      if cdsClone.FieldByName(AFieldCondicao.Fullname).AsString = VartoStr(AValorCondicao) then
      begin
        Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
      end;
      cdsClone.Next;
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class function TDatasetUtils.ConcatenarRegistrosFiltradosNaView(AField: TField;
  AView: TcxGridDBBandedTableView): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
  i: Integer;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    for i := 0 to AView.DataController.FilteredRecordCount - 1 do
    try
      cdsClone.Recno :=  AView.DataController.FilteredRecordIndex[i] + 1;
      Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class function TDatasetUtils.ConcatenarRegistrosFiltradosNaViewComCriterios(AField: TField;
  AView: TcxGridDBBandedTableView; AFieldCriterio: TField; AValorCriterio: String): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
  i: Integer;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    for i := 0 to AView.DataController.FilteredRecordCount - 1 do
    try
      cdsClone.Recno :=  AView.DataController.FilteredRecordIndex[i] + 1;

      if cdsClone.FieldByName(AFieldCriterio.FullName).AsString.Equals(AValorCriterio) then
      begin
        Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
      end;
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class function TDatasetUtils.ConcatenarRegistrosFiltradosNoDataControllerDaView(AField: TField;
  ADataController: TcxGridDBDataController): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
  i: Integer;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    for i := 0 to ADataController.FilteredRecordCount - 1 do
    try
      cdsClone.Recno :=  ADataController.FilteredRecordIndex[i] + 1;
      Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class function TDatasetUtils.ConcatenarRegistrosSelecionadosNaView(AField: TField;
  AView: TcxGridDBBandedTableView): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
  i: Integer;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    for i := 0 to AView.Controller.SelectedRowCount - 1 do
    try
      cdsClone.Recno :=  AView.Controller.SelectedRows[i].Index + 1;
      Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class procedure TDatasetUtils.ConfigurarMascaraFloat(const ADataset: TDataset);
var
  i: Integer;
begin
  for i := 0 to Pred(ADataset.Fields.Count) do
  begin
    if (ADataset.Fields[i] is TFMTBCDField) then
    begin
      TFMTBCDField(ADataset.Fields[i]).DisplayFormat := '###,###,###,##0.00';
    end
    else if (ADataset.Fields[i] is TBCDField) then
    begin
      TBCDField(ADataset.Fields[i]).DisplayFormat := '###,###,###,##0.00';
    end;
  end;
end;

class procedure TDatasetUtils.CopiarRegistro(ADatasetOrigem,
  ADatasetDestino: TDataset; ASomenteAlterados: Boolean = false;
  const AComaTextCamposQueNaoSeraoCopiados: String = '');
var i, indice: Integer;
  field: TField;
  camposNaoCopiar: TStringList;
begin
  camposNaoCopiar := TStringList.Create;
  try
    camposNaoCopiar.Delimiter := ',';
    camposNaoCopiar.DelimitedText := UpperCase(AComaTextCamposQueNaoSeraoCopiados);

    for i := 0 to ADatasetOrigem.FieldCount - 1 do
    begin
      field := ADatasetDestino.FindField(ADatasetOrigem.Fields[i].FieldName);
      if (field <> Nil) and (field.CanModify) and not(field is TAutoIncField) then
      begin
        if ASomenteAlterados then
        begin
          if ADatasetDestino.FieldByName(ADatasetOrigem.Fields[i].FieldName).AsString <>
            ADatasetOrigem.Fields[i].AsString then
          begin
            ADatasetDestino.FieldByName(ADatasetOrigem.Fields[i].FieldName).AsString :=
              ADatasetOrigem.Fields[i].AsString;
          end;
        end
        else
        begin
          if (AComaTextCamposQueNaoSeraoCopiados.IsEmpty) or
            (not camposNaoCopiar.Find(UpperCase(ADatasetOrigem.Fields[i].FieldName), indice)) then
          begin
            ADatasetDestino.FieldByName(ADatasetOrigem.Fields[i].FieldName).AsString :=
              ADatasetOrigem.Fields[i].AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(camposNaoCopiar);
  end;
end;

class procedure TDatasetUtils.CopiarRegistroComCondicoes(AQueryOrigem, AQueryDestino: TDataset;
  const ACamposQueNaoSeraoCopiados, ACamposQueSeraoCopiadosMasTeraoOutrosValores: Array of String; ANovosValores: Array of Variant);
var
  i,x, c: Integer;
  lAdiciona: Boolean;
begin
  for i:=0 to Pred(AQueryOrigem.FieldCount) do
  begin
    lAdiciona := True;

    for x:= 0 to Pred(Length(ACamposQueNaoSeraoCopiados)) do
      if (UPPERCASE(AQueryOrigem.Fields[i].FullName) = UPPERCASE(ACamposQueNaoSeraoCopiados[x]))
        or (AQueryOrigem.Fields[i] is TDataSetField) then
        lAdiciona := False;

    if lAdiciona then
    begin
      c:= -1;
      for x:= 0 to Pred(Length(ACamposQueSeraoCopiadosMasTeraoOutrosValores)) do
        if UPPERCASE(AQueryOrigem.Fields[i].FullName) = UPPERCASE(ACamposQueSeraoCopiadosMasTeraoOutrosValores[x]) then
          c:= x;
      if Assigned(AQueryDestino.FindField(AQueryOrigem.Fields[i].FullName)) then
      begin
        if (c > -1) then
        begin
          if AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName) is TDateTimeField then
            AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsDateTime := ANovosValores[c]
          else if AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName) is TIntegerField then
            AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsInteger := ANovosValores[c]
          else
            AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsString := ANovosValores[c];
        end
        else
          AQueryDestino.FieldByName(AQueryOrigem.Fields[i].FullName).AsString := AQueryOrigem.FieldByName(AQueryOrigem.Fields[i].FullName).AsString;
      end;
    end;
  end;
end;

class function TDatasetUtils.GetAggregateAsFloat(
  AField: TAggregateField): Double;
begin
  StrToFloatDef(AField.AsString, 0);
end;

class function TDatasetUtils.GetOldFloatValue(AField: TField): Double;
begin
  try
    Result := StrToFloatDef(VarToStr(AField.OldValue), 0);
  except
    Result := 0;
  end;
end;

class function TDatasetUtils.GetOldStringValue(AField: TField): String;
begin
  try
    Result := VarToStr(AField.OldValue);
  except
    Result := '';
  end;
end;

class procedure TDatasetUtils.ValidarCamposObrigatorios(ADataset: TDataset);
var i: Integer;
begin
  for i := 0 to Pred(ADataset.Fields.Count) do
  if ADataset.Fields[i].Required and ADataset.Fields[i].AsString.IsEmpty then
  begin
    TFrmMessage.Information('O campo '+UpperCase(ADataset.Fields[i].DisplayName)+' deve possuir algum valor.');
    ADataset.Fields[i].FocusControl;
    abort;
  end
end;

class procedure TDatasetUtils.JSONDatasetToMemTable(
  AJSONDataset: TFDJSONDataSets; AFDMemTable: TFDMemTable; AResetMemTable: Boolean = true);
begin
  if AJSONDataset.ToString.IsEmpty then
  begin
    exit;
  end;

  if not AResetMemTable then
  begin
    AFDMemTable.AppendData(TFDJSONDataSetsReader.GetListValue(AJSONDataset,0));
    exit;
  end;

  if AFDMemTable.Active then
  begin
    AFDMemTable.EmptyDataSet;
    AFDMemTable.Close;
  end;

  AFDMemTable.AppendData(TFDJSONDataSetsReader.GetListValue(AJSONDataset,0));

  //AFDMemTable.CopyDataset(TFDJSONDataSetsReader.GetListValue(AJSONDataset,0), [coStructure, coAppend]);
end;

class function TDatasetUtils.MaiorValor(AField: TField): Double;
var
  cdsClone: TDataset;
begin
  Result := 0;

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  try
    cdsClone.First;
    while not cdsClone.Eof do
    begin
      if cdsClone.FieldByName(AField.FullName).AsFloat > Result then
        result := cdsClone.FieldByName(AField.FullName).AsFloat;

      cdsClone.Next;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

class procedure TDatasetUtils.RatearValores(AValorTotal,
                                            AValorDesconto: Double;
                                            AFieldQtProduto,
                                            AFieldVlProduto,
                                            AFieldVlDesconto,
                                            AFieldPcDesconto,
                                            AFieldVlBruto: TField;
                                            AAumentando: Boolean = True;
                                            AValorDescontoAnterior: Double = 0);
var
  DataSet           : TDataSet;
  Percentual        : Double;
  Valor             : Double;
  diferencaRateio   : Double;
  ValorDesconto     : Double;
  registroAtual: TBookMark;
  descontoTotal: Double;
begin
  descontoTotal := 0;
  DataSet       := AFieldVlProduto.DataSet;
  registroAtual := DataSet.GetBookMark;
  try
    DataSet.DisableControls;
    DataSet.First;

    ValorDesconto := AValorDesconto - TDataSetUtils.SomarColunaExpressaoMultiplicacao(AFieldQtProduto, AFieldVlDesconto);

    ValorDesconto := ValorDesconto;
    while not DataSet.Eof do
    begin
      DataSet.Edit;

      if AAumentando then
      begin
        Percentual := TMathUtils.PercentualSobreValor(AFieldVlProduto.AsFloat, AValorTotal);
        Valor      := TMathUtils.ValorSobrePercentual(Percentual, ValorDesconto);
        AFieldVlDesconto.AsFloat := AFieldVlDesconto.AsFloat + (Valor / AFieldQtProduto.AsFloat);
      end
      else
      begin
        Percentual := TMathUtils.PercentualSobreValor(AFieldVlDesconto.AsFloat, AValorDescontoAnterior);
        Valor      := TMathUtils.ValorSobrePercentual(Percentual, AValorDesconto);
        AFieldVlDesconto.AsFloat := Valor / AFieldQtProduto.AsFloat;
      end;

      if Assigned(AFieldPcDesconto) then
      begin
        AFieldPcDesconto.AsFloat := TMathUtils.PercentualSobreValor(AFieldVlDesconto.AsFloat, AFieldVlBruto.AsFloat);
      end;

      AFieldVlProduto.AsFloat := AFieldQtProduto.AsFloat * (AFieldVlBruto.AsFloat - AFieldVlDesconto.AsFloat);

      descontoTotal := descontoTotal + (AFieldQtProduto.AsFloat * AFieldVlDesconto.AsFloat);
      DataSet.Next;
    end;

    diferencaRateio := (AValorDesconto - descontoTotal);
    if diferencaRateio <> 0  then
    begin
      DataSet.Last;
      DataSet.Edit;
      if diferencaRateio < 0  then
      begin
        AFieldVlDesconto.AsFloat := AFieldVlDesconto.AsFloat - Abs(diferencaRateio);
        AFieldVlProduto.AsFloat  := AFieldVlProduto.AsFloat  + Abs(diferencaRateio);
      end
      else
      begin
        AFieldVlDesconto.AsFloat := AFieldVlDesconto.AsFloat + abs(diferencaRateio);
        AFieldVlProduto.AsFloat  := AFieldVlProduto.AsFloat  - Abs(diferencaRateio);
      end;
    end;
  finally
    if (registroAtual <> nil) and DataSet.BookmarkValid(registroAtual) then
      DataSet.GotoBookmark(registroAtual);

    DataSet.EnableControls;
  end;

end;

class function TDatasetUtils.SomarColuna(AField: TField): Double;
var
  cdsClone: TClientDataset;
begin
  Result := 0;
  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(AField.Dataset), true);
  try
    cdsClone.First;
    while not cdsClone.Eof do
    try
      Result := Result + cdsClone.FieldByName(AField.FullName).AsFloat;
      cdsClone.Next;
    Except
      break;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

class function TDatasetUtils.SomarColunaComCondicao(AField, AFieldCondicao: TField;
  AValorCondicao: Variant): Double;
var
  cdsClone: TDataset;
begin
  Result := 0;

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  try
    cdsClone.First;
    while not cdsClone.Eof do
    try
      if cdsClone.FieldByName(AFieldCondicao.Fullname).AsString = VartoStr(AValorCondicao) then
      begin
        Result := Result + cdsClone.FieldByName(AField.FullName).AsFloat;
      end;
      cdsClone.Next;
    Except
      break;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

class function TDatasetUtils.SomarColunaExpressaoMultiplicacao(AField1, AField2: TField): Double;
var
  cdsClone: TClientDataset;
begin
  Result := 0;
  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(AField1.Dataset), true);
  try
    cdsClone.First;
    while not cdsClone.Eof do
    try
      Result := Result + (cdsClone.FieldByName(AField1.FullName).AsFloat *
        cdsClone.FieldByName(AField2.FullName).AsFloat);

      cdsClone.Next;
    Except
      break;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

class function TDatasetUtils.SomarRegistrosFiltradosNaView(var AField: TField;
  AView: TcxGridDBBandedTableView): Double;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
  i: Integer;
begin
  Result := 0;

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CopyDataSet(TFDMemTable(AField.Dataset), [coStructure, coRestart, coAppend]);
  end
  else
  begin
    exit;
  end;

  try
    for i := 0 to AView.DataController.FilteredRecordCount - 1 do
    try
      cdsClone.Recno :=  AView.DataController.FilteredRecordIndex[i] + 1;
      Result := Result + cdsClone.FieldByName(AField.FullName).AsFloat;
    Except
      break;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

class function TDatasetUtils.ClientDatasetDataStreamToClientDataset(const ADataStream: String): TClientDataSet;
var StrStream: TStringStream;
begin
  result := TClientDataSet.Create(nil);

  StrStream:= TStringStream.Create(ADataStream);
  try
    StrStream.Position := 0;
    result.LoadFromStream(StrStream);
  finally
    StrStream.Free;
  end;
end;

class function TDatasetUtils.ClientDatasetToClientDatasetDataStream(
  const AClientDataset: TClientDataset): String;
 var StrStream: TStringStream;
begin
  StrStream:= TStringStream.Create('');
  try
    AClientDataset.SaveToStream( StrStream, dfXML );
    result := StrStream.DataString;
  finally
    StrStream.Free;
  end;
end;

class function TDatasetUtils.Concatenar(AField: TField): string;
var
  cdsClone: TDataset;
  Concatenacao: TStringList;
begin
  Result := '';

  if (AField.Dataset is TClientDataset) then
  begin
    cdsClone := TClientDataset.Create(nil);
    TClientDataset(cdsClone).CloneCursor(TClientDataset(AField.Dataset), true);
  end
  else if (AField.Dataset is TFDMemTable) then
  begin
    cdsClone := TFDMemTable.Create(nil);
    TFDMemTable(cdsClone).CloneCursor(TFDMemTable(AField.Dataset), true);
  end
  else
  begin
    exit;
  end;

  Concatenacao := TStringList.Create;
  try
    cdsClone.First;
    while not cdsClone.Eof do
    try
      Concatenacao.add(cdsClone.FieldByName(AField.FullName).AsString);
      cdsClone.Next;
    Except
      break;
    end;
    Result := Concatenacao.CommaText;
  finally
    FreeAndNil(cdsClone);
    FreeAndNil(Concatenacao);
  end;
end;

class function TDatasetUtils.ValorExistente(Afield: TField;
  AValue: Variant): Boolean;
var
  cdsClone: TClientDataset;
begin

  Result := False;

  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(AField.Dataset), true);
  try

    cdsClone.First;
    while not cdsClone.Eof do
    begin
      if (cdsClone.FieldByName(AField.FullName).Value = AValue) and
        (AField.DataSet.RecNo <> cdsClone.RecNo)
      then
      begin
        result := True;
        break;
      end;
      cdsClone.Next;
    end;

  finally
    FreeAndNil(cdsClone);
  end;

end;

class function TDatasetUtils.ValorFoiAlterado(AField: TField): Boolean;
begin
  try
    result := AField.OldValue <> AField.NewValue;
  except
    result := false;
  end;
end;

{ TValidacaoCampo }

class function TValidacaoCampo.CampoMaiorQueZero(AField: TField; AExibirMensagem: Boolean = true;
  AAbort: Boolean = true): Boolean;
begin
  result := AField.AsFloat > 0;

  if not(result) and AExibirMensagem then
    TFrmMessage.Information('O campo '+AField.DisplayLabel+' deve ser maior que zero.');

  if not(result) then
    AField.FocusControl;

  if not(result) and AAbort then
    Abort;
end;

class function TValidacaoCampo.CampoPreenchido(AField: TField; AExibirMensagem: Boolean = true;
  AAbort: Boolean = true): Boolean;
begin
  result := not AField.AsString.IsEmpty;

  if not(result) and AExibirMensagem then
    TFrmMessage.Information('O campo '+AField.DisplayLabel+' deve possuir algum valor.');

  if not(result) then
    AField.FocusControl;

  if not(result) and AAbort then
    Abort;
end;

end.
