unit uConfiguracaoFiscal;

interface

Uses uConfiguracaoFiscalProxy;

type TConfiguracaoFiscal = class
  class function GetConfiguracaoFiscal(
    const AIdConfiguracaoFiscal: Integer): TConfiguracaoFiscalProxy;
  class function DuplicarConfiguracaoFiscal(const AIdConfiguracaoFiscalOrigem: Integer): Integer;
end;

implementation

uses uDmConnection, uClientClasses, Rest.JSON;

{ TConfiguracaoFiscal }

class function TConfiguracaoFiscal.DuplicarConfiguracaoFiscal(
  const AIdConfiguracaoFiscalOrigem: Integer): Integer;
var smConfiguracaoFiscal: TSMConfiguracoesFiscaisClient;
begin
  smConfiguracaoFiscal := TSMConfiguracoesFiscaisClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smConfiguracaoFiscal.DuplicarConfiguracaoFiscal(AIdConfiguracaoFiscalOrigem);
  finally
    smConfiguracaoFiscal.Free;
  end;
end;

class function TConfiguracaoFiscal.GetConfiguracaoFiscal(const AIdConfiguracaoFiscal: Integer): TConfiguracaoFiscalProxy;
var smConfiguracaoFiscal: TSMConfiguracoesFiscaisClient;
begin
  result := TConfiguracaoFiscalProxy.Create;

  smConfiguracaoFiscal := TSMConfiguracoesFiscaisClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TConfiguracaoFiscalProxy>(
      smConfiguracaoFiscal.GetConfiguracaoFiscal(AIdConfiguracaoFiscal));
  finally
    smConfiguracaoFiscal.Free;
  end;
end;

end.
