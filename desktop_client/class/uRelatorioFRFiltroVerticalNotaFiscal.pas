unit uRelatorioFRFiltroVerticalNotaFiscal;

interface

Uses uFrameFiltroVerticalPadrao;

type TRelatorioFRFiltroVerticalNotaFiscal = class
  class procedure CriarFiltrosRelatorioNotaFiscal(var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

uses uNotaFiscalProxy;

class procedure TRelatorioFRFiltroVerticalNotaFiscal.CriarFiltrosRelatorioNotaFiscal(
  var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //Código
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Código';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Numero da NF
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Número';
  campoFiltro.campo := 'B_NNF';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Numero da NF
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Série';
  campoFiltro.campo := 'B_SERIE';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Emissão
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Emissão';
  campoFiltro.campo := 'B_DEMI';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Entrada/Saída
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Entrada/Saída';
  campoFiltro.campo := 'B_DSAIENT';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Hora de Entrada/Saída
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Hora de Entrada/Saída';
  campoFiltro.campo := 'B_HSAIENT';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.hora);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Valor da Nota Fiscal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'W_VNF';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Status Comercial
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situação Comercial';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TNotaFiscalProxy.LISTA_STATUS;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Status Fiscal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situação Fiscal';
  campoFiltro.campo := 'AR_CSTAT';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TNotaFiscalProxy.LISTA_STATUS_NFE;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Tipo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo';
  campoFiltro.campo := 'TIPO_NF';
  campoFiltro.tabela := 'NOTA_FISCAL';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TNotaFiscalProxy.LISTA_TIPO_NF;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);
end;

end.
