unit uRelatorioFRFiltroVertical;

interface

Uses uFrameFiltroVerticalPadrao;

type TListaRelatoriosFRFiltroVertical = (estoque, venda, contaReceber,
  contaPagar, ordemServico, contaCorrenteMovimento, notaFiscal);

type TRelatorioFRFiltroVertical = class
  private
    class function GetNomeRelatorioPeloIdentificadorDoRelatorio(
      const AIdentificadorRelatorio: String): TListaRelatoriosFRFiltroVertical;
  public
    class procedure AdicionarFiltros(const AIdentificadorRelatorio: String;
      var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

{ TRelatorioFRFiltroVertical }

uses uProdutoProxy, uRelatorioFRFiltroVerticalVenda, uRelatorioFRFiltroVerticalContaReceber,
  uRelatorioFRFiltroVerticalContaPagar, uRelatorioFRFiltroVerticalEstoque,
  uRelatorioFRFiltroVerticalOrdemServico, uRelatorioFRFiltroVerticalContaCorrenteMovimento;

class procedure TRelatorioFRFiltroVertical.AdicionarFiltros(
  const AIdentificadorRelatorio: String; var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
begin
  case GetNomeRelatorioPeloIdentificadorDoRelatorio(AIdentificadorRelatorio) of
    estoque: TRelatorioFRFiltroVerticalEstoque.CriarFiltrosRelatorioEstoque(AFiltroVerticalPadrao);
    venda: TRelatorioFRFiltroVerticalVenda.CriarFiltrosRelatorioVenda(AFiltroVerticalPadrao);
    contaReceber: TRelatorioFRFiltroVerticalContaReceber.CriarFiltrosRelatorioContaReceber(AFiltroVerticalPadrao);
    contaPagar: TRelatorioFRFiltroVerticalContaPagar.CriarFiltrosRelatorioContaPagar(AFiltroVerticalPadrao);
    ordemServico: TRelatorioFRFiltroVerticalOrdemServico.CriarFiltrosRelatorioOrdemServico(AFiltroVerticalPadrao);
    contaCorrenteMovimento: TRelatorioFRFiltroVerticalContaCorrenteMovimento.CriarFiltrosRelatorioContaCorrenteMovimento(AFiltroVerticalPadrao);
    notaFiscal: TRelatorioFRFiltroVerticalNotaFiscal.CriarFiltrosRelatorioNotaFiscal(AFiltroVerticalPadrao);
  end;
end;

class function TRelatorioFRFiltroVertical.GetNomeRelatorioPeloIdentificadorDoRelatorio(
  const AIdentificadorRelatorio: String): TListaRelatoriosFRFiltroVertical;
begin
  if AIdentificadorRelatorio = 'RelEstoque' then
  begin
    result := TListaRelatoriosFRFiltroVertical(estoque);
  end
  else if AIdentificadorRelatorio = 'RelVenda' then
  begin
    result := TListaRelatoriosFRFiltroVertical(venda);
  end
  else if AIdentificadorRelatorio = 'RelContaReceber' then
  begin
    result := TListaRelatoriosFRFiltroVertical(contaReceber);
  end
  else if AIdentificadorRelatorio = 'RelContaPagar' then
  begin
    result := TListaRelatoriosFRFiltroVertical(contaPagar);
  end
  else if AIdentificadorRelatorio = 'RelOrdemServico' then
  begin
    result := TListaRelatoriosFRFiltroVertical(ordemServico);
  end
  else if AIdentificadorRelatorio = 'RelContaCorrenteMovimento' then
  begin
    result := TListaRelatoriosFRFiltroVertical(contaCorrenteMovimento);
  end
  else if AIdentificadorRelatorio = 'RelNotaFiscal' then
  begin
    result := TListaRelatoriosFRFiltroVertical(notaFiscal);
  end;
end;

end.
