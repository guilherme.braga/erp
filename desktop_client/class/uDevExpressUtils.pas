unit uDevExpressUtils;

interface

uses db, cxGrid, cxGridDBBandedTableView, cxCurrencyEdit, cxCheckBox,
  dxTabbedMDI, cxGridExportLink, ShellAPI, vcl.Dialogs, SysUtils, Windows,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, cxGridDBDataDefinitions, cxCustomData;

type TcxGridUtils = class
  class procedure PersonalizarColunaPeloTipoCampo(AView: TcxGridDBBandedTableView);
  class procedure AdicionarTodosCamposNaView(AView: TcxGridDBBandedTableView);
  class procedure OcultarTodosCamposView(AView: TcxGridDBBandedTableView);
  class function ExportarParaExcel(AGrid: TcxGrid;
    ANomeArquivoSugerido: String = ''; AExpandido: Boolean = true): Boolean;
  class function ExportarParaPDF(AGrid: TcxGrid;
    ANomeArquivoSugerido: String = ''; AExpandido: Boolean = true): Boolean;
  class procedure CriarTotalRodapeSUM(AColumn: TcxGridDBBandedColumn);
  class procedure ConfigurarColunaSelecao(AColumn: TcxGridDBBandedColumn);
  class procedure ConfigurarColunasParaSomenteLeitura(AView: TcxGridDBBandedTableView);
end;

type TcxGridMetodos = class
  procedure MarcarTodos(Sender: TObject);
end;

var cxGridMetodos: TcxGridMetodos;

implementation

{ TcxGridUtils }

class procedure TcxGridUtils.AdicionarTodosCamposNaView(
  AView: TcxGridDBBandedTableView);
begin
  AView.ClearItems;
  AView.DataController.CreateAllItems();
  TcxGridUtils.PersonalizarColunaPeloTipoCampo(AView);
end;

class function TcxGridUtils.ExportarParaExcel(AGrid: TcxGrid;
    ANomeArquivoSugerido: String = ''; AExpandido: Boolean = true): Boolean;
var caixaDialogo: TSaveDialog;
begin
  Result := false;

  if ANomeArquivoSugerido = '' then
  begin
    ANomeArquivoSugerido := 'ArquivoExportado.xls';
  end;

  caixaDialogo := TSaveDialog.Create(nil);
  try
    caixaDialogo.FileName := ANomeArquivoSugerido;
    caixaDialogo.InitialDir := GetCurrentDir;
    caixaDialogo.Filter := 'Arquivos Excel (*.xlsx)|*.xlsx';

    if caixaDialogo.Execute then
    begin
      ExportGridToXLSX(caixaDialogo.FileName, AGrid);
      Result := true;
    end;
  finally
    FreeAndNil(caixaDialogo);
  end;
end;

class function TcxGridUtils.ExportarParaPDF(AGrid: TcxGrid;
    ANomeArquivoSugerido: String = ''; AExpandido: Boolean = true): Boolean;
var
  dxComponentPrinter: TdxComponentPrinter;
  //dxComponentPrinterLink:TdxGridReportLink;
begin
  Result := false;

  if ANomeArquivoSugerido = '' then
  begin
    ANomeArquivoSugerido := 'ArquivoExportado.pdf';
  end;

  dxComponentPrinter := TdxComponentPrinter.Create(nil);
  try
    //dxComponentPrinterLink := TdxGridReportLink.Create(dxComponentPrinter);
    try
      //dxComponentPrinter.CurrentLink := dxComponentPrinterLink;
      with dxComponentPrinter.CurrentLink do
      begin
        Component := AGrid;
        PrinterPage.DMPaper := 1;
        PrinterPage.Footer := 5080;
        PrinterPage.Header := 5080;
        PrinterPage.Margins.Bottom := 12700;
        PrinterPage.Margins.Left := 12700;
        PrinterPage.Margins.Right := 12700;
        PrinterPage.Margins.Top := 12700;
        PrinterPage.PageSize.X := 215900;
        PrinterPage.PageSize.Y := 279400;
      end;

      dxComponentPrinter.CurrentLink.ExportToPDF;
    finally
      //FreeAndNil(dxComponentPrinterLink);
    end;
    Result := true;
  finally
    FreeAndNil(dxComponentPrinter);
  end;
end;

class procedure TcxGridUtils.OcultarTodosCamposView(AView: TcxGridDBBandedTableView);
var
  i: Integer;
begin
  for i := 0 to Pred(AView.ColumnCount) do
  begin
    AView.Columns[i].Visible := false;
  end;
end;

class procedure TcxGridUtils.PersonalizarColunaPeloTipoCampo(AView: TcxGridDBBandedTableView);
var i: Integer;
begin
  for i := 0 to Pred(AView.ColumnCount) do
  begin
    if (AView.Columns[i].DataBinding.Field is TFMTBCDField) then
    begin
      AView.Columns[i].PropertiesClassName := 'TcxCurrencyEditProperties';
      TcxCurrencyEditProperties(AView.Columns[i].Properties).DisplayFormat := '###,###,###,###,##0.00';
      TcxCurrencyEditProperties(AView.Columns[i].Properties).DecimalPlaces := 2;
    end
    else if (AView.Columns[i].DataBinding.Field.Size = 1) and
      (AView.Columns[i].DataBinding.Field is TStringField) and
      (Pos('BO', AView.Columns[i].DataBinding.Field.FullName) > 0) then
    begin
      AView.Columns[i].PropertiesClassName := 'TcxCheckBoxProperties';
      TcxCheckBoxProperties(AView.Columns[i].Properties).DisplayChecked := 'Sim';
      TcxCheckBoxProperties(AView.Columns[i].Properties).DisplayUnchecked := 'N�o';
      TcxCheckBoxProperties(AView.Columns[i].Properties).ValueChecked := 'S';
      TcxCheckBoxProperties(AView.Columns[i].Properties).ValueUnchecked := 'N';
      TcxCheckBoxProperties(AView.Columns[i].Properties).ImmediatePost := true;
    end
    else if (AView.Columns[i].DataBinding.Field is TBlobField) then
    begin
      AView.Columns[i].PropertiesClassName := 'TcxTextEditProperties';
    end
  end;
end;

class procedure TcxGridUtils.ConfigurarColunaSelecao(AColumn: TcxGridDBBandedColumn);
begin
  if not Assigned(AColumn) then
  begin
    exit;
  end;

  AColumn.DataBinding.Field.ReadOnly := false;
  AColumn.Options.Editing := true;
  AColumn.Options.Sorting := false;
  AColumn.OnHeaderClick := cxGridMetodos.MarcarTodos;
end;

class procedure TcxGridUtils.ConfigurarColunasParaSomenteLeitura(AView: TcxGridDBBandedTableView);
var i: Integer;
begin
  for i := Pred(AView.ColumnCount) downto 0 do
  begin
    AView.Columns[i].Options.Editing := false;
  end;
end;

class procedure TcxGridUtils.CriarTotalRodapeSUM(AColumn: TcxGridDBBandedColumn);
begin
{var
  summary:
  sumGroup: TcxDataSummaryGroup;
  link: TcxGridTableSummaryGroupItemLink;
  item: TcxGridDBTableSummaryItem;
  view: TcxGridDBBandedTableView;
begin
  view := AColumn;
  try
    view.DataController.Summary.BeginUpdate;
    view.DataController.Summary.SummaryGroups.Clear;
  finally
    view.DataController.Summary.EndUpdate;
  end;

  AColumn.Summary.FooterKind := skSum;
  AColumn.Summary.FooterFormat := ',0';
  AColumn.Summary.GroupKind := skSum;
  AColumn.Summary.GroupFormat :=  ',0';
  AColumn.GroupIndex := -1;
  sumGroup := ASummary.SummaryGroups.Add;
  link := sumGroup.Links.Add as TcxGridTableSummaryGroupItemLink;
  link.Column :=  AColumn;
  item := sumGroup.SummaryItems.Add as TcxGridDBTableSummaryItem;
  item.Column := AColumn;
  item.Kind := skSum;
  item.Position := spGroup;
  item.Format := AColumn.Summary.FooterFormat;      }
end;

procedure TcxGridMetodos.MarcarTodos(Sender: TObject);
var
  dataset: TDataset;
  field: TField;
begin
  if not TcxGridDBBandedColumn(Sender).Options.Editing then
  begin
    Exit;
  end;

  dataset := TcxGridDBBandedColumn(Sender).DataBinding.Field.DataSet;
  field := TcxGridDBBandedColumn(Sender).DataBinding.Field;

  if (dataset <> nil) and (dataset.Active) then
  try
    dataset.DisableControls;
    dataset.First;

    while not dataset.Eof do
    begin
      dataset.Edit;

      if field.AsString = 'S' then
      begin
        field.AsString := 'N'
      end
      else
      begin
        field.AsString := 'S';
      end;

      if dataset.State in dsEditModes then
      begin
        dataset.Post;
      end;

      dataset.Next;
    end;
  finally
    dataset.First;
    dataset.EnableControls;
  end;
end;

initialization
  cxGridMetodos := TcxGridMetodos.Create;

Finalization
  if Assigned(cxGridMetodos) then
    FreeAndNil(cxGridMetodos);

end.
