unit uControlsUtils;

interface

Uses Vcl.Controls, Windows, Forms;

type TControlsUtils = class
  class procedure SetFocus(AControl: TWinControl);
  class procedure CongelarFormulario(AFormulario: TForm);
  class procedure DescongelarFormulario;
end;

implementation

{ TControlsUtils }

class procedure TControlsUtils.CongelarFormulario(AFormulario: TForm);
begin
  try
    LockWindowUpdate(AFormulario.Handle);
  except
    LockWindowUpdate(0);
  end;
end;

class procedure TControlsUtils.DescongelarFormulario;
begin
  LockWindowUpdate(0);
end;

class procedure TControlsUtils.SetFocus(AControl: TWinControl);
begin
  if AControl.CanFocus then
    AControl.SetFocus;
end;

end.
