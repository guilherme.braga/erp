unit uModeloNota;

Interface

Uses uModeloNotaProxy, SysUtils;

type TModeloNota = class
  class function GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
end;

implementation

{ TModeloNota }

uses uClientClasses, uDmConnection;

class function TModeloNota.GetModeloNotaFiscal(const AIdModeloNota: Integer): String;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GetModeloNotaFiscal(AIdModeloNota);
  finally
    smNotaFiscal.Free;
  end;
end;


end.
