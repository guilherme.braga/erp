unit uConstantes;

interface

type TDBConstantes = class
  const FIELD_BO_CHECKED = 'cast(''S'' as char(1)) AS BO_CHECKED';
  const FIELD_BO_UNCHECKED = 'cast(''N'' as char(1)) AS BO_CHECKED';
  const ALIAS_BO_CHECKED = 'BO_CHECKED';
end;

type TConstantes = class
  const MASCARA_TELEFONE: String = '\(00\)0000-00000;0;*';
  const BO_SIM: String = 'S';
  const BO_NAO: String = 'N';
end;

implementation

end.
