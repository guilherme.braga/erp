unit uNotaFiscal;

interface

Uses db, Generics.Collections, ugbClientDataset, SysUtils, uNotaFiscalProxy, uRetornoProxy, dialogs, Classes,
  Forms, Data.FireDACJSONReflect, ugbFDMemTable, uVendaProxy;

type TNotaFiscal = class
  //Calculos dos valores
  class procedure CalcularValorNotaFiscal(ADatasetNotaFiscal, ADatasetProduto: TDataset);
  class procedure CalcularValorTotalItem(ADatasetNotaFiscal, ADatasetProduto: TDataset);
  class procedure RatearFreteNotaParaItens(ADatasetNotaFiscal, ADatasetProduto: TgbClientDataset);
  class function BuscarTipoDocumentoFiscal(ADatasetNotaFiscal: TDataset): String;

  class procedure SetInformacoesFiscaisProduto(ADatasetNotaFiscal, ADatasetProduto: TgbClientDataset);

  class function GerarContaReceber(AIdChaveProcesso: Integer;
    ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
  class function GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
  class function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
  class function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
  class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
  class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
  class function Efetivar(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
  class function Cancelar(AIdChaveProcesso: Integer): Boolean;
  class procedure GerarParcelas(ANotaFiscal,ANotaFiscalParcela: TGbClientDataset; AIdCarteira: Integer);
  class procedure LimparParcelas(AGeracaoDocumentoParcela: TgbClientDataset);
  class function GerarNotaFiscal(ANotaFiscal: TNotaFiscalProxy): Integer;
  class function EmitirNFE(const AIdNotaFiscal: Integer): Boolean;
  class procedure ChecarServidorAtivo(const AidFilial, AIdUsuario: Integer);
  class function EmitirNFCE(const AIdNotaFiscal: Integer): Boolean;
  class function NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
  class function EmitirDocumentoFiscal(const AIdNotaFiscal: Integer;
    ATipoDocumento: String): TRetornoDocumentoFiscalProxy;
  class function GerarNFCEDaVenda(const AIdVenda: Integer; const ACPFNaNota: String;
    AVendaProxy: TVendaProxy): Boolean;
  class function GerarNFEDaVenda(const AIdVenda: Integer; AVendaProxy: TVendaProxy): Boolean;
  class function GerarNFCEDeVendaAgrupada(const AIdsVendas: String; const ACPFNaNota: String;
    AVendaProxy: TVendaProxy): Boolean;
  class function GerarNFEDeVendaAgrupada(const AIdsVendas: String; AVendaProxy: TVendaProxy): Boolean;
  class function GerarNFEDaOrdemServico(const AIdOrdemServico: Integer; const ACPFNaNota: String): Boolean;
  class procedure AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
  class function ImprimirNotaFiscal(const AIdNotaFiscal: Integer; const ATipoDocumentoFiscal: String): String;
  class function GetNumeroDocumentoNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
  class function GetSerieNotaFiscalSaida(const ATipoDocumentoFiscal: String): Integer;
  class function CancelarDocumentoFiscal(const AIdNotaFiscal: Integer; ATipoDocumento: String): Boolean;
  class function BuscarXML(const AIdNotaFiscal: Integer): String;
  class procedure BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes, AAno: Integer;
    ADatasetDados: TgbFDMemTable);
  class procedure InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);
end;

const STATUS_NOTA_CONFIRMADA: String = 'EFETIVADO';
const STATUS_NOTA_ABERTA: String = 'ABERTA';
const STATUS_NOTA_CANCELADA: String = 'CANCELADO';

implementation

{ TNotaFiscal }

uses uProduto, uClientClasses, uDmConnection, uPlanoPagamento, uMathUtils, Rest.JSON,
  uCarteira, uCarteiraProxy, uDatasetUtils, uProdutoProxy, uFrmMessage, uImpostoICMSProxy,
  uCadImpostoICMS, uImpostoICMS, uFrmMessage_Process, uFrmExibirListaMensagemSimples, uSistema,
  uFrmIdentificacaoDestinatarioNFCe, uFrmRelatorioFR, uControlsUtils, uMotorCalculoNotaFiscal, uModeloNota,
  uImpostoIPIProxy, uImpostoPisCofinsProxy, uImpostoIPI, uImpostoPisCofins, uFrmComponentesFiscais,
  uConfiguracaoFiscalProxy, uFilial, uUsuarioProxy, uTUsuario, uFilialProxy, uConfiguracaoFiscal, ACBrNFeDANFEFR;

class procedure TNotaFiscal.CalcularValorTotalItem(ADatasetNotaFiscal, ADatasetProduto: TDataset);
var
  motorCalculoNotaFiscal: TMotorCalculoNotaFiscal;
  impostoICMS: TImpostoICMSProxy;
  impostoIPI: TImpostoIPIProxy;
  impostoPisCofins: TImpostoPisCofinsProxy;
begin
  try
    impostoICMS := nil;
    impostoIPI := nil;
    impostoPisCofins := nil;

    motorCalculoNotaFiscal := TMotorCalculoNotaFiscal.Create(
      BuscarTipoDocumentoFiscal(ADatasetNotaFiscal), ADatasetNotaFiscal.FieldByName('TIPO_NF').AsString,
      ADatasetNotaFiscal, ADatasetProduto, TSistema.Sistema.Filial);
    try
      if ADatasetProduto.FieldByName('ID_IMPOSTO_ICMS').AsInteger > 0 then
      begin
        impostoICMS := TImpostoICMS.GetImpostoIcms(
          ADatasetProduto.FieldByName('ID_IMPOSTO_ICMS').AsInteger);
      end;

      if ADatasetProduto.FieldByName('ID_IMPOSTO_IPI').AsInteger > 0 then
      begin
        impostoIPI := TImpostoIPI.GetImpostoIPI(ADatasetProduto.FieldByName('ID_IMPOSTO_IPI').AsInteger);
      end;

      if ADatasetProduto.FieldByName('ID_IMPOSTO_PIS_COFINS').AsInteger > 0 then
      begin
        impostoPisCofins := TImpostoPisCofins.GetImpostoPISCofins(
          ADatasetProduto.FieldByName('ID_IMPOSTO_PIS_COFINS').AsInteger);
      end;

      motorCalculoNotaFiscal.CalcularValorTotalItem(impostoICMS, impostoIPI, impostoPisCofins);
    finally
      if Assigned(impostoICMS) then
      begin
        FreeAndNil(impostoICMS);
      end;

      if Assigned(impostoIPI) then
      begin
        FreeAndNil(impostoIPI);
      end;

      if Assigned(impostoPisCofins) then
      begin
        FreeAndNil(impostoPisCofins);
      end;
    end;
  finally
    FreeAndNil(motorCalculoNotaFiscal);
  end;
end;

class procedure TNotaFiscal.BuscarNotasFiscaisParaInutilizacao(const AIdFIlial, AMes,
  AAno: Integer; ADatasetDados: TgbFDMemTable);
var
  smNotaFiscal: TSMNotaFiscalClient;
  dados: TFDJSONDataSets;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    dados := smNotaFiscal.BuscarNotasFiscaisParaInutilizacao(AIdFIlial, AMes, AAno);
    TDatasetUtils.JSONDatasetToMemTable(dados, ADatasetDados);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.BuscarTipoDocumentoFiscal(ADatasetNotaFiscal: TDataset): String;
var
  idModeloNF: Integer;
  modeloNF: String;
begin
  idModeloNF := ADatasetNotaFiscal.FieldByName('ID_MODELO_NOTA').AsInteger;
  modeloNF := TModeloNota.GetModeloNotaFiscal(ADatasetNotaFiscal.FieldByName('ID_MODELO_NOTA').AsInteger);
  if modeloNF.Equals(TNotaFiscalProxy.MODELO_NFE) then
  begin
    result := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE;
  end
  else if modeloNF.Equals(TNotaFiscalProxy.MODELO_NFCE) then
  begin
    result := TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE;
  end
  else
  begin
    result := '';
  end;
end;

class function TNotaFiscal.BuscarXML(const AIdNotaFiscal: Integer): String;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.BuscarXML(AIdNotaFiscal);
  finally
    smNotaFiscal.Free;
  end;
end;

class procedure TNotaFiscal.CalcularValorNotaFiscal(ADatasetNotaFiscal, ADatasetProduto: TDataset);
var
  motorCalculoNotaFiscal: TMotorCalculoNotaFiscal;
begin
  try
    motorCalculoNotaFiscal := TMotorCalculoNotaFiscal.Create(
      BuscarTipoDocumentoFiscal(ADatasetNotaFiscal), ADatasetNotaFiscal.FieldByName('TIPO_NF').AsString,
      ADatasetNotaFiscal, ADatasetProduto, TSistema.Sistema.Filial);

    motorCalculoNotaFiscal.CalcularValorNotaFiscal;
  finally
    FreeAndNil(motorCalculoNotaFiscal);
  end;
end;

class function TNotaFiscal.Cancelar(AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.Cancelar(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.CancelarDocumentoFiscal(const AIdNotaFiscal: Integer; ATipoDocumento: String): Boolean;
var
  smNotaFiscal: TSMNotaFiscalClient;
  motivoCancelamento: String;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    motivoCancelamento := Copy(InputBox('Cancelamento de '+ATipoDocumento+' - Informe o motivo', 'Motivo (M�nimo de 15 Caracteres)', ''), 1, 255);

    if motivoCancelamento.IsEmpty then
    begin
      TFrmMessage.Information('O Cancelamento n�o foi realizado pois o motivo n�o foi informado.');
      result := false;
      exit;
    end;

    if (Length(motivoCancelamento) < 15) then
    begin
      TFrmMessage.Information('O Cancelamento n�o foi realizado pois o motivo informado tem menos do que 15 caracteres.');
      result := false;
      exit;
    end;

    result := smNotaFiscal.CancelarDocumentoFiscal(AIdNotaFiscal, motivoCancelamento, ATipoDocumento);

    if result then
    begin
      TFrmMessage.Information(ATipoDocumento+' cancelada com sucesso!');
    end;
  finally
    smNotaFiscal.Free;
  end;
end;

class procedure TNotaFiscal.ChecarServidorAtivo(const AidFilial, AIdUsuario: Integer);
var
  smNotaFiscal: TSMNotaFiscalClient;
  retornoProxy: TRetornoDocumentoFiscalProxy;
  listaMensagensRetorno: TStringList;
const
  mensagemAdvertencia =
    'A emiss�o da NF-e n�o pode ser conclu�da pois as seguintes inconsist�ncias foram encontradas:';
  tituloListaMensagens = 'Inconsist�ncias NF-e';
begin
  retornoProxy := TRetornoDocumentoFiscalProxy.Create;

  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    retornoProxy := TJSON.JsonToObject<TRetornoDocumentoFiscalProxy>(smNotaFiscal.ChecarServidorAtivo(
      AidFilial, AIdUsuario));
  finally
    smNotaFiscal.Free;
  end;

  try
    TFrmMessage_Process.SendMessage('Realizando comunica��o com servidor da SEFAZ');
    try
      if retornoProxy.FRetorno then
      begin
        TFrmMessage.Information('Servidor ativo e operante!');
      end
      else
      begin
        TFrmMessage_Process.CloseMessage;
        listaMensagensRetorno := TStringList.Create;
        try
          listaMensagensRetorno.CommaText := retornoProxy.FMensagensCommaText;
          TFrmExibirListaMensagemSimples.ExibirListaMensagem(tituloListaMensagens, listaMensagensRetorno,
            mensagemAdvertencia);
        finally
          FreeAndNil(listaMensagensRetorno);
        end;
      end;
    finally
      FreeAndNil(retornoProxy);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

class procedure TNotaFiscal.AtualizarCPFNaNota(const AIdNotaFiscal: Integer; const ACPF: String);
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.AtualizarCPFNaNota(AIdNotaFiscal, ACPF);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.Efetivar(AIdChaveProcesso: Integer;
  ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.Efetivar(AIdChaveProcesso, ANotaFiscalTransienteProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.EmitirDocumentoFiscal(const AIdNotaFiscal: Integer;
  ATipoDocumento: String): TRetornoDocumentoFiscalProxy;
var
  smNotaFiscal: TSMNotaFiscalClient;
  retornoProxy: String;
begin
  result := TRetornoDocumentoFiscalProxy.Create;

  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    retornoProxy := smNotaFiscal.EmitirNFE(AIdNotaFiscal, ATipoDocumento);
  finally
    smNotaFiscal.Free;
  end;

  result := TJSON.JsonToObject<TRetornoDocumentoFiscalProxy>(retornoProxy);
end;

class function TNotaFiscal.EmitirNFCE(const AIdNotaFiscal: Integer): Boolean;
var
  retornoProxy: TRetornoDocumentoFiscalProxy;
  listaMensagensRetorno: TStringList;
  cpfNaNota: String;
const
  mensagemAdvertencia =
    'A emiss�o da NFC-e n�o pode ser conclu�da pois as seguintes inconsist�ncias foram encontradas:';
  tituloListaMensagens = 'Inconsist�ncias NFC-e';
begin
  result := false;
  try
    TFrmMessage_Process.SendMessage('Realizando envio ao servidor da SEFAZ');

    cpfNaNota := TFrmIdentificacaoDestinatarioNFCe.CPFNaNota;
    if not cpfNaNota.IsEmpty then
    begin
      TNotaFiscal.AtualizarCPFNaNota(AIdNotaFiscal, cpfNaNota);
    end;

    retornoProxy := EmitirDocumentoFiscal(AIdNotaFiscal, 'NFCE');
    try
      if retornoProxy.FRetorno then
      begin
        result := true;
        //Desenvolver visualiza��o da nota
      end
      else
      begin
        listaMensagensRetorno := TStringList.Create;
        try
          listaMensagensRetorno.CommaText := retornoProxy.FMensagensCommaText;
          TFrmExibirListaMensagemSimples.ExibirListaMensagem(tituloListaMensagens, listaMensagensRetorno,
            mensagemAdvertencia);
        finally
          FreeAndNil(listaMensagensRetorno);
        end;
      end;
    finally
      FreeAndNil(retornoProxy);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

class function TNotaFiscal.EmitirNFE(const AIdNotaFiscal: Integer): Boolean;
var
  retornoProxy: TRetornoDocumentoFiscalProxy;
  listaMensagensRetorno: TStringList;
const
  mensagemAdvertencia =
    'A emiss�o da NF-e n�o pode ser conclu�da pois as seguintes inconsist�ncias foram encontradas:';
  tituloListaMensagens = 'Inconsist�ncias NF-e';
begin
  result := false;
  try
    TFrmMessage_Process.SendMessage('Realizando envio ao servidor da SEFAZ');

    retornoProxy := EmitirDocumentoFiscal(AIdNotaFiscal, 'NFE');
    try
      if retornoProxy.FRetorno then
      begin
        result := true;
        //Desenvolver visualiza��o da nota
      end
      else
      begin
        listaMensagensRetorno := TStringList.Create;
        try
          listaMensagensRetorno.CommaText := retornoProxy.FMensagensCommaText;
          TFrmExibirListaMensagemSimples.ExibirListaMensagem(tituloListaMensagens, listaMensagensRetorno,
            mensagemAdvertencia);
        finally
          FreeAndNil(listaMensagensRetorno);
        end;
      end;
    finally
      FreeAndNil(retornoProxy);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

class function TNotaFiscal.EstornarContaPagar(
  AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.EstornarContaPagar(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.EstornarContaReceber(
  AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.EstornarContaReceber(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.GerarContaPagar(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarContaReceber(
  AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.GerarContaReceber(AIdChaveProcesso,ANotaFiscalTransienteProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarNFCEDaVenda(const AIdVenda: Integer; const ACPFNaNota: String;
  AVendaProxy: TVendaProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GerarNotaFiscalDaVenda(AIdVenda, ACPFNaNota, 'NFCE', AVendaProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarNFCEDeVendaAgrupada(const AIdsVendas, ACPFNaNota: String;
  AVendaProxy: TVendaProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GerarNotaFiscalDeVendaAgrupada(AIdsVendas, ACPFNaNota, 'NFCE', AVendaProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarNFEDaOrdemServico(const AIdOrdemServico: Integer;
  const ACPFNaNota: String): Boolean;
begin

end;

class function TNotaFiscal.GerarNFEDaVenda(const AIdVenda: Integer; AVendaProxy: TVendaProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GerarNotaFiscalDaVenda(AIdVenda, '', 'NFE', AVendaProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarNFEDeVendaAgrupada(const AIdsVendas: String;
  AVendaProxy: TVendaProxy): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GerarNotaFiscalDeVendaAgrupada(AIdsVendas, '', 'NFE', AVendaProxy);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GerarNotaFiscal(
  ANotaFiscal: TNotaFiscalProxy): Integer;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GerarNotaFiscal(TJSON.ObjectToJsonString(ANotaFiscal));
  finally
    smNotaFiscal.Free;
  end;
end;

class procedure TNotaFiscal.GerarParcelas(ANotaFiscal, ANotaFiscalParcela: TGbClientDataset;
  AIdCarteira: Integer);
var parcelamento: TParcelamento;
    parcela: TParcela;
    valorParcela, valorTitulo, diferencaNoParcelamento, totalParcelas: Double;
    i: Integer;
    carteira: TCarteiraProxy;
begin
  if ANotaFiscal.FieldByName('ID_PLANO_PAGAMENTO').AsString.IsEmpty then
    exit;

  totalParcelas := 0;
  diferencaNoParcelamento := 0;
  valorTitulo := ANotaFiscal.FieldByName('W_VNF').AsFloat - ANotaFiscal.FieldByName('VL_QUITACAO_DINHEIRO').AsFloat;

  parcelamento := TParcelamento.ParcelamentoBaseCadastro(ANotaFiscal.FieldByName('ID_PLANO_PAGAMENTO').AsInteger,
    ANotaFiscal.FieldByName('DT_COMPETENCIA').AsDateTime, 1);

  valorParcela := TMathUtils.Arredondar( valorTitulo / parcelamento.quantidadeParcelas);

  for i := 0 to Pred(parcelamento.parcelas.Count) do
  begin
    parcela := TParcela(parcelamento.parcelas[i]);

    ANotaFiscalParcela.Incluir;
    ANotaFiscalParcela.FieldByName('DOCUMENTO').AsString :=
      ANotaFiscal.FieldByName('B_NNF').AsString;

    ANotaFiscalParcela.FieldByName('VL_TITULO').AsFloat := valorParcela;
    ANotaFiscalParcela.FieldByName('ID_CARTEIRA').AsFloat := AIdCarteira;
    ANotaFiscalParcela.FieldByName('QT_PARCELA').AsInteger := parcela.quantidadeParcelas;
    ANotaFiscalParcela.FieldByName('NR_PARCELA').AsInteger := parcela.numeroParcela;
    ANotaFiscalParcela.FieldByName('DT_VENCIMENTO').AsDateTime := parcela.dataParcela;

    if AIdCarteira > 0 then
    begin
      carteira := TCarteira.GetCarteira(AIdCarteira);
      try
        ANotaFiscalParcela.FieldByName('ID_CARTEIRA').AsInteger :=
          carteira.FId;
        ANotaFiscalParcela.FieldByName('JOIN_DESCRICAO_CARTEIRA').AsString :=
          carteira.FDescricao;
      finally
        FreeAndNil(carteira);
      end;
    end;

    ANotaFiscalParcela.Salvar;

    totalParcelas := totalParcelas + valorParcela;
  end;

  diferencaNoParcelamento := valorTitulo - totalParcelas;

  if diferencaNoParcelamento <> 0 then
  begin
    ANotaFiscalParcela.Alterar;
    ANotaFiscalParcela.FieldByName('VL_TITULO').AsFloat :=
      ANotaFiscalParcela.FieldByName('VL_TITULO').AsFloat + diferencaNoParcelamento;
    ANotaFiscalParcela.Salvar;
  end;
end;

class function TNotaFiscal.GetNumeroDocumentoNotaFiscalSaida(
  const ATipoDocumentoFiscal: String): Integer;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GetNumeroDocumentoNotaFiscalSaida(ATipoDocumentoFiscal);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.GetSerieNotaFiscalSaida(
  const ATipoDocumentoFiscal: String): Integer;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.GetSerieNotaFiscalSaida(ATipoDocumentoFiscal);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.ImprimirNotaFiscal(const AIdNotaFiscal: Integer;
  const ATipoDocumentoFiscal: String): String;
var
  {smNotaFiscal: TSMNotaFiscalClient;
  frmRelatorioFR: TFrmRelatorioFR;}
  frmComponentesFiscais: TFrmComponentesFiscais;
  configuracoesFiscais: TConfiguracaoFiscalProxy;
  filial: TFilialProxy;
  usuario: TUsuarioProxy;
  xml: String;
begin
  try
    TFrmMessage_Process.SendMessage('Processando visualiza��o');

    Application.CreateForm(TFrmComponentesFiscais, frmComponentesFiscais);
    try
      configuracoesFiscais := TConfiguracaoFiscal.GetConfiguracaoFiscal(TSistema.Sistema.Filial.Fid);
      filial := TFilial.GetFilial(TSistema.Sistema.Filial.Fid);
      usuario := TUsuario.GetUsuario(TSistema.Sistema.Usuario.id);

      if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE then
      begin
        frmComponentesFiscais.ValidarConfiguracoesNFE(configuracoesFiscais);
        frmComponentesFiscais.ConfigurarACBrNFE(configuracoesFiscais, filial, usuario);
      end
      else if ATipoDocumentoFiscal = TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE then
      begin
        frmComponentesFiscais.ValidarConfiguracoesNFCE(configuracoesFiscais);
        frmComponentesFiscais.ConfigurarACBrNFCE(configuracoesFiscais, filial, usuario);
      end;

      xml := TNotaFiscal.BuscarXML(AIdNotaFiscal);

      if xml.IsEmpty then
      begin
        exit;
      end;

      frmComponentesFiscais.ACbrNFE.NotasFiscais.Clear;
      frmComponentesFiscais.ACbrNFE.NotasFiscais.LoadFromString(xml);

      if frmComponentesFiscais.ACbrNFE.DANFE.MostrarPreview then
      begin
        TACBrNFeDANFEFR(frmComponentesFiscais.ACbrNFE.DANFE).PreparedReport.ShowReport;
      end
      else
      begin
        frmComponentesFiscais.ACbrNFE.NotasFiscais.Imprimir;
      end;

      if frmComponentesFiscais.ACbrNFE.DANFE.PathPDF <> '' then
      begin
        frmComponentesFiscais.ACbrNFE.NotasFiscais.ImprimirPDF;
      end;

      if configuracoesFiscais.FSalvarPDFJuntoXML.Equals('S') then
      begin
        frmComponentesFiscais.SalvarCopiaPDFJuntoComXML;
      end;
    finally
      FreeAndNil(frmComponentesFiscais);
    end;

  {  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
    try
      frmRelatorioFR := TFrmRelatorioFR.Create(nil);
      try
        frmRelatorioFR.frxReport.LoadFromStream(smNotaFiscal.ImprimirDocumentoFiscal(
          AIdNotaFiscal, ATipoDocumentoFiscal));

        frmRelatorioFR.frxReport.ShowReport();
      finally
        FreeAndNil(frmRelatorioFR);
      end;

      smNotaFiscal.ImprimirDocumentoFiscal(AIdNotaFiscal, ATipoDocumentoFiscal);//.ShowReport;


    finally
      smNotaFiscal.Free;
    end;}
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

class procedure TNotaFiscal.InutilizarDocumentoFiscal(const AIdsNotaFiscal: String);
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.InutilizarDocumentoFiscal(AIdsNotaFiscal);
  finally
    smNotaFiscal.Free;
  end;
end;

class procedure TNotaFiscal.LimparParcelas(
  AGeracaoDocumentoParcela: TgbClientDataset);
begin
  while not AGeracaoDocumentoParcela.IsEmpty do
    AGeracaoDocumentoParcela.Delete;
end;

class function TNotaFiscal.NFEEmitida(const AIdNotaFiscal: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    smNotaFiscal.NFEEmitida(AIdNotaFiscal);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.PodeEstornar(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class function TNotaFiscal.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var smNotaFiscal: TSMNotaFiscalClient;
begin
  smNotaFiscal := TsmNotaFiscalClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNotaFiscal.PodeExcluir(AIdChaveProcesso);
  finally
    smNotaFiscal.Free;
  end;
end;

class procedure TNotaFiscal.RatearFreteNotaParaItens(ADatasetNotaFiscal, ADatasetProduto: TgbClientDataset);
var
  percentualFreteItemProporcional: Double;
begin
  try
    ADatasetProduto.DisableControls;
    ADatasetProduto.GuardarBookMark;

    while not ADatasetProduto.Eof do
    begin
      percentualFreteItemProporcional := TMathUtils.PercentualSobreValor(
        ADatasetProduto.GetAsFloat('I_VFRETE'), ADatasetNotaFiscal.GetAsFloat('W_VFRETE'));

      if percentualFreteItemProporcional > 0 then
      begin
        ADatasetProduto.Alterar;

        ADatasetProduto.SetAsFloat('I_VFRETE', TMathUtils.ValorSobrePercentual(
          percentualFreteItemProporcional, ADatasetProduto.GetAsFloat('VL_LIQUIDO')));

        ADatasetProduto.SetAsFloat('PERC_FRETE', percentualFreteItemProporcional);

        ADatasetProduto.Salvar;
      end
      else
      begin
        ADatasetProduto.Alterar;
        ADatasetProduto.SetAsFloat('I_VFRETE', 0);
        ADatasetProduto.SetAsFloat('PERC_FRETE', 0);
        ADatasetProduto.Salvar;
      end;

      ADatasetProduto.Next;
    end;
  finally
    ADatasetProduto.PosicionarBookMark;
    ADatasetProduto.EnableControls;
  end;
end;

class procedure TNotaFiscal.SetInformacoesFiscaisProduto(ADatasetNotaFiscal, ADatasetProduto: TgbClientDataset);
var
  produtoFiscal: TProdutoFiscalProxy;
begin
  produtoFiscal := TProduto.GetProdutoFiscal(ADatasetProduto.GetAsInteger('id_produto'),
    ADataSetNotaFiscal.GetAsInteger('id_pessoa'), ADataSetNotaFiscal.GetAsInteger('id_filial'));

  //Internos
  if produtoFiscal.ID_REGRA_IMPOSTO_FILTRO = 0 then
  begin
    ADatasetProduto.Fields.Clear;
    TFrmMessage.Information('N�o foi encontrado um regra de imposto compat�vel para o produto selecionado!');
    Abort;
  end;

  //Informa��es Gerais
  ADatasetProduto.SetAsString('I_CEAN', produtoFiscal.I_CEAN);
  ADatasetProduto.SetAsString('I_CEANTRIB', produtoFiscal.I_CEANTRIB);
  ADatasetProduto.SetAsString('I_XPROD', produtoFiscal.I_XPROD);
  ADatasetProduto.SetAsString('I_NCM', produtoFiscal.I_NCM);
  ADatasetProduto.SetAsString('I_CFOP', produtoFiscal.I_CFOP);
  ADatasetProduto.SetAsString('I_UCOM', produtoFiscal.I_UCOM);
  ADatasetProduto.SetAsString('I_UTRIB', produtoFiscal.I_UTRIB);
  //ADatasetProduto.SetAsInteger('H_NITEM', produtoFiscal.H_NITEM);
  //ADatasetProduto.SetAsFloat('I_QCOM', produtoFiscal.I_QCOM);
  //ADatasetProduto.SetAsFloat('I_VUNCOM', produtoFiscal.I_VUNCOM);
  //ADatasetProduto.SetAsFloat('I_VPROD', produtoFiscal.I_VPROD);
  //ADatasetProduto.SetAsFloat('I_QTRIB', produtoFiscal.I_QTRIB);
  //ADatasetProduto.SetAsFloat('I_VUNTRIB', produtoFiscal.I_VUNTRIB);
  //ADatasetProduto.SetAsFloat('I_VFRETE', produtoFiscal.I_VFRETE);
  //ADatasetProduto.SetAsFloat('I_VSEG', produtoFiscal.I_VSEG);
  //ADatasetProduto.SetAsFloat('I_VDESC', produtoFiscal.I_VDESC);
  //ADatasetProduto.SetAsFloat('I_VOUTRO', produtoFiscal.I_VOUTRO);
  //ADatasetProduto.SetAsInteger('I_INDTOT', produtoFiscal.I_INDTOT);
  //ADatasetProduto.SetAsString('I_XPED', produtoFiscal.I_XPED);
  //ADatasetProduto.SetAsInteger('I_NITEMPED', produtoFiscal.I_NITEMPED);

  //Imposto ICMS
  if produtoFiscal.ID_IMPOSTO_ICMS > 0 then
  begin
    ADatasetProduto.SetAsInteger('ID_IMPOSTO_ICMS', produtoFiscal.ID_IMPOSTO_ICMS);
    ADatasetProduto.SetAsString('N_CST', produtoFiscal.N_CST);
    ADatasetProduto.SetAsString('N_MODBC', produtoFiscal.N_MODBC);
    ADatasetProduto.SetAsFloat('N_vBC', produtoFiscal.N_vBC);
    ADatasetProduto.SetAsFloat('N_pICMS', produtoFiscal.N_pICMS);
    ADatasetProduto.SetAsFloat('N_vICMS', produtoFiscal.N_vICMS);
    ADatasetProduto.SetAsString('N_MODBCST', produtoFiscal.N_MODBCST);
    ADatasetProduto.SetAsFloat('N_pMVAST', produtoFiscal.N_pMVAST);
    ADatasetProduto.SetAsFloat('N_pRedBCST', produtoFiscal.N_pRedBCST);
    ADatasetProduto.SetAsFloat('N_vBCST', produtoFiscal.N_vBCST);
    ADatasetProduto.SetAsFloat('PERC_ICMS_ST', produtoFiscal.N_pICMSST);
    ADatasetProduto.SetAsFloat('N_NVICMSST', produtoFiscal.N_vICMSST);
    ADatasetProduto.SetAsFloat('N_pRedBC', produtoFiscal.N_pRedBC);
    ADatasetProduto.SetAsInteger('N_motDesICMS', produtoFiscal.N_motDesICMS);
    ADatasetProduto.SetAsFloat('N_BCSTRet', produtoFiscal.N_vBCSTRet);
    //ADatasetProduto.SetAsFloat('N_vICMSSTRet', produtoFiscal.N_vICMSSTRet);
    ADatasetProduto.SetAsFloat('N_pCredSN', produtoFiscal.N_pCredSN);
    ADatasetProduto.SetAsFloat('N_vCredICMSSN', produtoFiscal.N_vCredICMSSN);
  end;

  //Imposto IPI
  if produtoFiscal.ID_IMPOSTO_IPI > 0 then
  begin
    ADatasetProduto.SetAsString('O_clEnq', produtoFiscal.O_clEnq);
    ADatasetProduto.SetAsString('O_CNPJProd', produtoFiscal.O_CNPJProd);
    ADatasetProduto.SetAsString('O_cSelo', produtoFiscal.O_cSelo);
    ADatasetProduto.SetAsInteger('O_qSelo', produtoFiscal.O_qSelo);
    ADatasetProduto.SetAsString('O_cEnq', produtoFiscal.O_cEnq);
    ADatasetProduto.SetAsFloat('O_qUnid', produtoFiscal.O_qUnid);
    ADatasetProduto.SetAsFloat('O_vUnid', produtoFiscal.O_vUnid);
    ADatasetProduto.SetAsFloat('O_vIPI', produtoFiscal.O_vIPI);
    ADatasetProduto.SetAsString('O_CST', produtoFiscal.O_CST);
    ADatasetProduto.SetAsFloat('O_vBC', produtoFiscal.O_vBC);
    ADatasetProduto.SetAsFloat('O_pIPI', produtoFiscal.O_pIPI);
  end;

  //Imposto PIS
  if produtoFiscal.ID_IMPOSTO_PISCofins > 0 then
  begin
    ADatasetProduto.SetAsString('Q_CST', produtoFiscal.Q_CST);
    ADatasetProduto.SetAsFloat('Q_VBC', produtoFiscal.Q_VBC);
    ADatasetProduto.SetAsFloat('Q_PPIS', produtoFiscal.Q_PPIS);
    ADatasetProduto.SetAsFloat('Q_VPIS', produtoFiscal.Q_VPIS);
    ADatasetProduto.SetAsFloat('Q_QBCPROD', produtoFiscal.Q_BCPROD);
    ADatasetProduto.SetAsFloat('Q_vAliqProd', produtoFiscal.Q_vAliqProd);

    //Imposto COFINS
    ADatasetProduto.SetAsString('S_CST', produtoFiscal.S_CST);
    ADatasetProduto.SetAsFloat('S_vBC', produtoFiscal.S_vBC);
    ADatasetProduto.SetAsFloat('S_pCOFINS', produtoFiscal.S_pCOFINS);
    ADatasetProduto.SetAsFloat('S_vCOFINS', produtoFiscal.S_vCOFINS);
    ADatasetProduto.SetAsFloat('S_QBCProd', produtoFiscal.S_BCProd);
    ADatasetProduto.SetAsFloat('S_vAliqProd', produtoFiscal.S_vAliqProd);
    ADatasetProduto.SetAsFloat('S_qBCProd', produtoFiscal.S_qBCProd);
  end;
end;

end.
