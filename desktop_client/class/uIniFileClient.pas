unit uIniFileClient;

interface

uses
  Classes, SysUtils, IniFiles, Forms, Windows;

type TIniSystemFile = record
  server, porta, caminho_relatorios: String;

  impressora_argox: String; //Remover

  //Remover
  ftp_host          : string;
  ftp_port          : Integer;
  ftp_username      : string;
  ftp_password      : string;
  ftp_listentimeout : Integer;
  ftp_idletimems    : Integer;
  ftp_intervalms    : Integer;

  //Remover   {bematech}
  dll_bematech: AnsiString;
  porta_bematech: AnsiString;
  arquivo_impressao: AnsiString;
  tipo_impressora: Integer;
end;

type TIniOptions = class(TObject)
  private
    function FileName: String;
  public
    constructor Create;
    procedure LoadSettings(Ini: TIniFile);
    procedure SaveSettings(Ini: TIniFile);

    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);

  end;

var
  IniOptions: TIniOptions = nil;
  IniSystemFile: TIniSystemFile;

implementation

procedure TIniOptions.LoadSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin
      IniSystemFile.Server           := ReadString('DATASNAP','server','127.0.0.1');
      IniSystemFile.porta            := ReadString('DATASNAP','porta','211');
      IniSystemFile.impressora_argox := ReadString('ARGOX','impressora','\\pc-name\argox');

      IniSystemFile.ftp_host          := ReadString('FTP',  'host', '');
      IniSystemFile.ftp_port          := ReadInteger('FTP', 'port', 8080);
      IniSystemFile.ftp_username      := ReadString('FTP',  'username', '');
      IniSystemFile.ftp_password      := ReadString('FTP',  'password', '');
      IniSystemFile.ftp_listentimeout := ReadInteger('FTP', 'listentimeout', 6000);
      IniSystemFile.ftp_idletimems    := ReadInteger('FTP', 'idletimems', 250);
      IniSystemFile.ftp_intervalms    := ReadInteger('FTP', 'intervalms', 250);

      IniSystemFile.dll_bematech := ReadString('BEMATECH','dll','');
      IniSystemFile.porta_bematech := ReadString('BEMATECH','porta','');
      IniSystemFile.arquivo_impressao := ReadString('BEMATECH','arquivoImpressao','');
      IniSystemFile.tipo_impressora := ReadInteger('BEMATECH','tipo',0);
    end;
  end;
end;

procedure TIniOptions.SaveSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin

      WriteString('DATASNAP','server','127.0.0.1');
      WriteString('DATASNAP','porta','211');
      WriteString('ARGOX','impressora','\\pc-name\argox');

      WriteString('FTP', 'host', '127.0.0.1');
      WriteString('FTP', 'port', '8080');
      WriteString('FTP', 'username','usuario');
      WriteString('FTP', 'password','senha');
      WriteString('FTP', 'listentimeout','6000');
      WriteString('FTP', 'idletimems', '250');
      WriteString('FTP', 'intervalms', '250');

      WriteString('BEMATECH','dll','MP2032.dll');
      WriteString('BEMATECH','porta','USB');
      WriteString('BEMATECH','arquivoImpressao','Impressao.txt');
      WriteInteger('BEMATECH','tipo',7);
    end;
  end;
end;

constructor TIniOptions.Create;
begin
  inherited Create;
  LoadFromFile(FileName);
end;

function TIniOptions.FileName: String;
begin
  result :=
    ExtractFilePath(Application.ExeName)+
    StringReplace(ExtractFileName(Application.ExeName)+'.ini',
                  ExtractFileExt(ExtractFileName(Application.ExeName)),
                  '', [rfReplaceAll]);
end;

procedure TIniOptions.LoadFromFile(const FileName: string);
var
  Ini: TIniFile;
begin
  if not FileExists(FileName) then
    SaveToFile(FileName);

  Ini := TIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    Ini.Free;
  end;
end;

procedure TIniOptions.SaveToFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    SaveSettings(Ini);
  finally
    Ini.Free;
  end;
end;

initialization
  IniOptions := TIniOptions.Create;

finalization
  IniOptions.Free;

end.


