unit uContaReceber;

interface

uses uContaReceberProxy, db, Generics.Collections, ugbFDMemTable, Data.FireDACJSONReflect;

type TContaReceber = class
  public
    {Client}
    class procedure SetEncargosEmAberto(
      AContaReceber, AContaReceberQuitacao: TDataset);

    {Server}
    procedure GerarContaReceber(AContaReceber: TContaReceberMovimento);
    function PodeExcluir(AChaveProcesso: Integer): Boolean;
    function RemoverContaReceber(AChaveProcesso: Integer) : Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaReceber, AChaveProcesso: Integer;
      AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean;
    class procedure GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber: Integer; var lista: TList<Integer>);
    class function GetTotaisContaReceber(const AIdsContaReceber: String): TgbFDMemTable;
    class function PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Boolean;
    class function QuantidadeContasReceberEmAbertoParaPessoa(const AIdPessoa: Integer): Integer;
    class function ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Boolean;
    class function QuantidadeContasReceberEmAbertoParaProcesso(const AIdChaveProcesso: Integer): Integer;
    class function GetValorJuros(const AIdContaReceber: Integer): Double;
    class function GetValorAberto(const AIdContaReceber: Integer): Double;
    class function GetValorMulta(const AIdContaReceber: Integer): Double;
    class procedure AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
      const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);
    class function GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: String): TgbFDMemTable;
    class procedure CancelarBoletoEmitido(const AIdsContaReceber: String);
end;

const
  FIELD_DOCUMENTO= 'CONTA_RECEBER.DOCUMENTO';
  FIELD_DESCRICAO = 'CONTA_RECEBER.DESCRICAO';
  FIELD_DATA_VENCIMENTO = 'CONTA_RECEBER.DT_VENCIMENTO';
  FIELD_STATUS = 'CONTA_RECEBER.STATUS';
  FIELD_VALOR_TITULO = 'CONTA_RECEBER.VL_TITULO';

  ABERTO    = 'ABERTO';
  QUITADO   = 'QUITADO';
  CANCELADO = 'CANCELADO';

implementation

{ TContaReceber }

uses
  uDmConnection,
  System.JSON,
  REST.Json,
  DBXJSONReflect,
  uClientClasses,
  System.SysUtils, uCarteira, uCarteiraProxy, uMathUtils, DateUtils, uDatasetUtils;

procedure TContaReceber.GerarContaReceber(
  AContaReceber: TContaReceberMovimento);
var
  smContaReceber   : TSMContaReceberClient;
  ContaReceberJson : String;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    ContaReceberJson := TJSON.ObjectToJsonString(AContaReceber);
    smContaReceber.GerarContaReceber(ContaReceberJson);
  finally
    smContaReceber.Free;
  end;
end;

class procedure TContaReceber.GetIdsNovasQuitacoes(
  AIdUltimaQuitacao, AIdContaReceber: Integer; var lista: TList<Integer>);
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    lista.AddRange(smContaReceber.GetIdsNovasQuitacoes(AIdUltimaQuitacao, AIdContaReceber));
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.GetQueryVariosContasReceberJSONDatasets(
  AIdsContasReceber: String): TgbFDMemTable;
var
  smContaReceber : TSMContaReceberClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TgbFDMemTable.Create(nil);
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smContaReceber.GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.GetTotaisContaReceber(const AIdsContaReceber: String): TgbFDMemTable;
var
  smContaReceber : TSMContaReceberClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TgbFDMemTable.Create(nil);
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smContaReceber.GetTotaisContaReceber(AIdsContaReceber);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.GetValorAberto(const AIdContaReceber: Integer): Double;
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.GetValorAberto(AIdContaReceber);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.GetValorJuros(const AIdContaReceber: Integer): Double;
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.GetValorJuros(AIdContaReceber);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.GetValorMulta(const AIdContaReceber: Integer): Double;
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.GetValorMulta(AIdContaReceber);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.PessoaPossuiContaReceberEmAberto(const AIdPessoa: Integer): Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.PessoaPossuiContaReceberEmAberto(AIdPessoa) > 0;
  finally
    smContaReceber.Free;
  end;
end;

function TContaReceber.PodeExcluir(AChaveProcesso: Integer) : Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.PodeExcluir(AChaveProcesso);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.ProcessoPossuiContaReceberEmAberto(const AIdChaveProcesso: Integer): Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.ProcessoPossuiContaReceberEmAberto(AIdChaveProcesso) > 0;
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.QuantidadeContasReceberEmAbertoParaPessoa(const AIdPessoa: Integer): Integer;
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.PessoaPossuiContaReceberEmAberto(AIdPessoa);
  finally
    smContaReceber.Free;
  end;
end;

class function TContaReceber.QuantidadeContasReceberEmAbertoParaProcesso(
  const AIdChaveProcesso: Integer): Integer;
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.ProcessoPossuiContaReceberEmAberto(AIdChaveProcesso);
  finally
    smContaReceber.Free;
  end;
end;

function TContaReceber.RemoverContaReceber(AChaveProcesso: Integer) : Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.RemoverContaReceber(AChaveProcesso);
  finally
    smContaReceber.Free;
  end;
end;

class procedure TContaReceber.AtualizarDadosBoletoContasReceber(const AIdContaReceber: Integer;
  const ABoletoNossoNumero, ABoletoLinhaDigitavel, ABoletoCodigoBarras: String);
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    smContaReceber.AtualizarDadosBoletoContasReceber(AIdContaReceber, ABoletoNossoNumero,
      ABoletoLinhaDigitavel, ABoletoCodigoBarras);
  finally
    smContaReceber.Free;
  end;
end;

function TContaReceber.AtualizarMovimentosContaCorrente(AIdContaReceber, AChaveProcesso: Integer;
  AGerarContraPartida: Boolean) : Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.AtualizarMovimentosContaCorrente(AIdContaReceber, AChaveProcesso,
      AGerarContraPartida);
  finally
    smContaReceber.Free;
  end;
end;

class procedure TContaReceber.CancelarBoletoEmitido(const AIdsContaReceber: String);
var
  smContaReceber : TSMContaReceberClient;
begin
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    smContaReceber.CancelarBoletoEmitido(AIdsContaReceber);
  finally
    smContaReceber.Free;
  end;
end;

function TContaReceber.RemoverMovimentosContaCorrente(AIdContaReceber: Integer;
  AGerarContraPartida: Boolean) : Boolean;
var
  smContaReceber : TSMContaReceberClient;
begin
  Result := False;
  smContaReceber := TSMContaReceberClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaReceber.RemoverMovimentosContaCorrente(AIdContaReceber, AGerarContraPartida);
  finally
    smContaReceber.Free;
  end;
end;

class procedure TContaReceber.SetEncargosEmAberto(AContaReceber,
  AContaReceberQuitacao: TDataset);
var
  carteira: TCarteiraProxy;
  estaVencido: Boolean;
  estaAberto: Boolean;
  diasEmAtraso: Integer;
begin
  estaVencido := (AContaReceber.FieldByName('DT_VENCIMENTO').AsDateTime < Date);
  estaAberto := AContaReceber.FieldByName('STATUS').AsString.Equals(
    TContaReceberMovimento.ABERTO);

  if not(estaVencido) or not(estaAberto) then
  begin
    Exit;
  end;

  carteira := TCarteira.GetCarteira(
    AContaReceber.FieldByName('ID_CARTEIRA').AsInteger);
  try
    estaVencido :=(AContaReceber.FieldByName('DT_VENCIMENTO').AsDateTime
      + carteira.FCarencia) < (Date);

    diasEmAtraso := DaysBetween(AContaReceber.FieldByName('DT_VENCIMENTO').AsDateTime
      + carteira.FCarencia, Date);

    if not estaVencido  then
    begin
      Exit;
    end;

    if AContaReceber.FieldByName('PERC_MULTA').AsFloat < TMathUtils.Arredondar(carteira.FPercMulta, 4) then
    begin
      AContaReceberQuitacao.FieldByName('PERC_MULTA').AsFloat :=
        carteira.FPercMulta - AContaReceber.FieldByName('PERC_MULTA').AsFloat;
    end;

    if AContaReceber.FieldByName('PERC_JUROS').AsFloat <
      TMathUtils.Arredondar(carteira.FPercJuros * diasEmAtraso, 4) then
    begin
      AContaReceberQuitacao.FieldByName('PERC_JUROS').AsFloat :=
        (carteira.FPercJuros * diasEmAtraso) - AContaReceber.FieldByName('PERC_JUROS').AsFloat;
    end;

    if AContaReceberQuitacao.FieldByName('PERC_MULTA').AsFloat > 0 then
    begin
      AContaReceberQuitacao.FieldByName('VL_MULTA').AsFloat :=
        TMathUtils.ValorSobrePercentual(
        AContaReceberQuitacao.FieldByName('PERC_MULTA').AsFloat,
        AContaReceber.FieldByName('VL_ABERTO').AsFloat);
    end;

    if AContaReceberQuitacao.FieldByName('PERC_JUROS').AsFloat > 0 then
    begin
      AContaReceberQuitacao.FieldByName('VL_JUROS').AsFloat :=
        TMathUtils.ValorSobrePercentual(
        AContaReceberQuitacao.FieldByName('PERC_JUROS').AsFloat,
        AContaReceber.FieldByName('VL_ABERTO').AsFloat);
    end;
  finally
    FreeAndNil(carteira);
  end;
end;


end.
