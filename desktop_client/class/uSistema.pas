unit uSistema;

interface

uses uUsuarioProxy, uFilialProxy, uEmpresaProxy, sysutils, FireDAC.Comp.Client;

type
  TSistema = class
  strict private
    FUsuario: TUsuarioProxy;
    FFilial: TFilialProxy;
    FEmpresa: TEmpresaProxy;

    class var Instancia: TSistema;
    class destructor Destroy;
  public
    class function Sistema: TSistema;

    procedure SetEmpresa(const AIdFilial: Integer);
    procedure SetFilial(const AIdFilial: Integer);
    procedure SetUsuario(const AIdUsuario: Integer);

    property usuario: TUsuarioProxy read FUsuario write FUsuario;
    property filial: TFilialProxy read FFilial write FFilial;
    property empresa: TEmpresaProxy read FEmpresa write FEmpresa;
  end;

implementation

{ TSistema }

uses uFilial, uTUsuario;

class destructor TSistema.Destroy;
begin
  if not Assigned(TSistema.Instancia) then
    exit;

  if TSistema.Instancia.usuario <> nil then
  begin
    TSistema.Instancia.usuario.Free;
  end;

  if TSistema.Instancia.filial <> nil then
  begin
    TSistema.Instancia.filial.Free;
  end;

  if TSistema.Instancia.empresa <> nil then
  begin
    TSistema.Instancia.empresa.Free;
  end;

  if TSistema.Instancia <> nil then
  begin
    TSistema.Instancia.Free;
  end;
end;

class function TSistema.Sistema: TSistema;
begin
  if TSistema.Instancia = nil then
  begin
    TSistema.Instancia := TSistema.Create;
    Instancia.SetEmpresa(1);
    Instancia.SetFilial(1);
  end;
  Result := TSistema.Instancia;
end;

procedure TSistema.SetEmpresa(const AIdFilial: Integer);
begin

end;

procedure TSistema.SetFilial(const AIdFilial: Integer);
var fdmFilial: TFDMemTable;
begin
  if not Assigned(filial) then
    filial := TFilialProxy.Create;

  filial.Fid := 1;
  filial.Ffantasia := TFilial.GetFantasia(1);
  filial.FCrt := TFilial.GetCRT(1);;
end;

procedure TSistema.SetUsuario(const AIdUsuario: Integer);
begin
  if Assigned(usuario) then
    usuario.Free;

  usuario := TUsuario.GetUsuario(AIdUsuario);
end;

end.
