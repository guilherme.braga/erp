unit uPlanoPagamento;

interface

uses Contnrs, FireDAC.Comp.Client, Data.FireDACJSONReflect, classes, SysUtils,
  DateUtils, dbClient, db;

const PRAZO_FIXO: String = 'FIXO';
const PRAZO_PERSONALIZADO: String = 'PERSONALIZADO';
const PRAZO_DATA_MOVIMENTO: String = 'DATA_MOVIMENTO';

type TParcelamentoUtils = class
  class procedure Reparcelar(AField: TField; AValorTotal: Double);
  class procedure ReprocessarVencimento(AFieldDataVencimento, AFieldDiasVencimento: TField);
  class procedure ReplicarDiaDeVencimento(AFieldDataVencimento: TField);
  class procedure TrocarDiaDeVencimento(AFieldDataVencimento: TField; ANovoDiaDeVencimento: Integer);
  class function ConferirValorParcelado(AField: TField; AValorTotal: Double): Boolean;
  class function ParcelamentoInvalido(AField: TField): Boolean;
  class procedure ReplicarInformacoesCheque(ADataset: TDataset);
  class procedure ReplicarInformacoesCartao(ADataset: TDataset);
end;

type TParcela = class
  private
    FquantidadeParcelas: Integer;
    FNumeroParcela: Integer;
    FdataParcela: TDate;
  public
    property numeroParcela: Integer read FNumeroParcela write FNumeroParcela;
    property dataParcela: TDate read FdataParcela write FdataParcela;
    property quantidadeParcelas: Integer read FquantidadeParcelas write FquantidadeParcelas;
end;

type TParcelaCheque = class (TParcela)
  private

  public

end;

type TParcelaCartao = class (TParcela)

end;

type TParcelaCrediario = class (TParcela)

end;

type TListaParcelas = class(TObjectList)

end;

type TParcelamento = class
  private
    FParcelas: TListaParcelas;
    FQuantidadeParcelas: Integer;
  public
    property parcelas: TListaParcelas read FParcelas write FParcelas;
    property quantidadeParcelas: Integer read FQuantidadeParcelas write FQuantidadeParcelas;

    constructor ParcelamentoBaseCadastro(AIdPlanoPagamento: Integer; ADataBase: TDate; AParcelaBase: Integer); overload;
    constructor ParcelamentoSemCadastro(AQuantidadeParcelas: Integer; ADataBase: TDate; AParcelaBase: Integer);overload;
end;

type TPlanoPagamento = class
  public
    class function GetPlanoPagamento(AIdPlanoPagamento: Integer): TFDMemTable;
    class function GetDescricaoPlanoPagamento(const AID: Integer): String;
end;

implementation

{ TPlanoPagamento }

uses uClientClasses, uDmConnection, uDatasetUtils, uDateUtils;

class function TPlanoPagamento.GetDescricaoPlanoPagamento(
  const AID: Integer): String;
var smFinanceiro: TsmFinanceiroClient;
begin
  smFinanceiro := TsmFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetDescricaoPlanoPagamento(AID);
  finally
    smFinanceiro.Free;
  end;
end;

class function TPlanoPagamento.GetPlanoPagamento(
  AIdPlanoPagamento: Integer): TFDMemTable;
var smFinanceiro: TsmFinanceiroClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smFinanceiro := TsmFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetPlanoPagamento(AIdPlanoPagamento);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smFinanceiro.Free;
  end;
end;

{ TParcelamento }

constructor TParcelamento.ParcelamentoBaseCadastro(AIdPlanoPagamento: Integer; ADataBase: TDate; AParcelaBase: Integer);
var fdmPlanoPagamento: TFDMemTable;
    i: Integer;
    parcela: TParcela;
    listaParcelamentoPersonalizado: TStrings;
begin
  fdmPlanoPagamento := TPlanoPagamento.GetPlanoPagamento(AIdPlanoPagamento);
  Fparcelas := TListaParcelas.Create;
  FquantidadeParcelas := fdmPlanoPagamento.FieldByName('Qt_Parcela').AsInteger;

  fdmPlanoPagamento := TPlanoPagamento.GetPlanoPagamento(AIdPlanoPagamento);
  try
    for i := AParcelaBase to FquantidadeParcelas do
    begin
      parcela := TParcela.Create;
      parcela.quantidadeParcelas := fdmPlanoPagamento.FieldByName('Qt_Parcela').AsInteger;
      parcela.numeroParcela := I;

      if fdmPlanoPagamento.FieldByName('tipo').AsString.Equals(PRAZO_PERSONALIZADO) then
      begin
        listaParcelamentoPersonalizado := TStringList.Create;
        try
          listaParcelamentoPersonalizado.DelimitedText :=
            StringReplace(fdmPlanoPagamento.FieldByName('prazo').AsString, '/', ',', [rfReplaceAll]);

          parcela.dataParcela := ADataBase + StrtoIntDef(listaParcelamentoPersonalizado[i-1], 0);
        finally
          FreeAndNil(listaParcelamentoPersonalizado);
        end;
      end
      else if fdmPlanoPagamento.FieldByName('tipo').AsString.Equals(PRAZO_FIXO) then
      begin
        parcela.dataParcela := IncMonth(ADataBase);
        parcela.dataParcela := TDateUtils.TrocarDia(fdmPlanoPagamento.FieldByName('DIA_PARCELA').AsInteger, parcela.dataParcela);
      end
      else if fdmPlanoPagamento.FieldByName('tipo').AsString.Equals(PRAZO_DATA_MOVIMENTO) then
      begin
        parcela.dataParcela := ADataBase;
      end;

      ADataBase := parcela.dataParcela;
      Fparcelas.add(parcela);
    end;
  finally
    FreeAndNil(fdmPlanoPagamento);
  end;
end;

constructor TParcelamento.ParcelamentoSemCadastro(AQuantidadeParcelas: Integer; ADataBase: TDate; AParcelaBase: Integer);
var i: Integer;
    parcela: TParcela;
    listaParcelamentoPersonalizado: TStrings;
    dia, mes, ano: Word;
begin
  if AQuantidadeParcelas <= 0 then
    exit;

  Fparcelas := TListaParcelas.Create;
  FquantidadeParcelas := AQuantidadeParcelas;

  for i := AParcelaBase to Pred(AParcelaBase + FquantidadeParcelas) do
  begin
    parcela := TParcela.Create;
    parcela.quantidadeParcelas := (AParcelaBase + FquantidadeParcelas) -1;
    parcela.numeroParcela := I;

    parcela.dataParcela := IncMonth(ADataBase);

    ADataBase := parcela.dataParcela;
    Fparcelas.add(parcela);
  end;
end;

class procedure TParcelamentoUtils.Reparcelar(AField: TField; AValorTotal: Double);
var
  bookMarkparcelaAlterada: TBookMark;
  numeroParcela: Integer;
  totalParcelas: Integer;
  dataset: TClientDataset;
  ultimaParcela: Boolean;
  possueUmaUnicaParcela: Boolean;
  valorFixoNoParcelamento: Double;
  valorRecalculadoPorParcela: Double;
begin
  dataset := TClientDataset(AField.Dataset);
  bookMarkparcelaAlterada := dataset.GetBookMark;
  numeroParcela := dataset.Recno;
  totalParcelas := dataset.RecordCount;
  ultimaParcela := numeroParcela = totalParcelas;
  possueUmaUnicaParcela := totalParcelas = 1;

  if possueUmaUnicaParcela then
  begin
    dataset.Edit;
    AField.AsFloat := AValorTotal;
    exit;
  end;

  try
    dataset.DisableControls;
    if ultimaParcela then
    begin
      valorFixoNoParcelamento := AField.AsFloat;
      valorRecalculadoPorParcela := (AValorTotal - valorFixoNoParcelamento) / (totalParcelas -1);
      dataset.First;
      while not dataset.Eof do
      begin
        if dataset.Recno <> numeroParcela then
        begin
      	  dataset.Edit;
      	  AField.AsFloat := valorRecalculadoPorParcela;

          if dataset.State in dsEditModes then
      	    dataset.Post;
      	end;
        dataset.Next;
      end;
    end
    else
    begin
      valorFixoNoParcelamento := 0;

      dataset.First;

      repeat
        valorFixoNoParcelamento := valorFixoNoParcelamento + AField.AsFloat;
        dataset.Next;
      until (dataset.Recno > numeroParcela);

      valorRecalculadoPorParcela := (AValorTotal - valorFixoNoParcelamento) / (totalParcelas - numeroParcela);

      while not dataset.Eof do
      begin
        dataset.Edit;
        AField.AsFloat := valorRecalculadoPorParcela;

        if dataset.State in dsEditModes then
      	  dataset.Post;

        dataset.Next;
      end;
    end;
  finally
    if (bookMarkparcelaAlterada <> nil) and (dataset.BookMarkValid(bookMarkparcelaAlterada)) then
      dataset.GotoBookmark(bookMarkparcelaAlterada);

    dataset.EnableControls;
  end;
end;

class procedure TParcelamentoUtils.ReplicarDiaDeVencimento(AFieldDataVencimento: TField);
var
  bookMarkparcelaAlterada: TBookMark;
  dataset: TClientDataset;
  diaBase: Word;
  dataValida: Boolean;
  novaData: TDateTime;
  dia,mes,ano: Word;
begin
  dataset := TClientDataset(AFieldDataVencimento.Dataset);

  if dataset.Eof then
  begin
    Exit;
  end;

  try
    dataset.DisableControls;
    bookMarkparcelaAlterada := dataset.GetBookMark;

    diaBase := DayOf(AFieldDataVencimento.AsDateTime);

    dataset.Next;
    while not dataset.Eof do
    begin
      DecodeDate(AFieldDataVencimento.AsDateTime, ano, mes, dia);
      dataValida := TryEncodeDate(ano, mes, diaBase, novaData);

      if dataValida then
      begin
        dataset.Edit;
        AFieldDataVencimento.AsDateTime := novaData;
        dataset.Post;
      end;

      dataset.Next;
    end;
  finally
    if (bookMarkparcelaAlterada <> nil) and (dataset.BookMarkValid(bookMarkparcelaAlterada)) then
      dataset.GotoBookmark(bookMarkparcelaAlterada);
    dataset.EnableControls;
  end;
end;

class procedure TParcelamentoUtils.ReplicarInformacoesCartao(
  ADataset: TDataset);
var
  Bookmark: TBookmark;
  idOperadoraCartao: Integer;
  descricaoOperadoraCartao: String;
  idFormaPagamento: Integer;
const
  FIELD_ID_OPERADORA_CARTAO: String = 'ID_OPERADORA_CARTAO';
  FIELD_DESCRICAO_OPERADORA_CARTAO: String = 'JOIN_DESCRICAO_OPERADORA_CARTAO';
  FIELD_ID_FORMA_PAGAMENTO: String = 'ID_FORMA_PAGAMENTO';
begin
  try
    ADataset.DisableControls;
    Bookmark := ADataset.GetBookMark;

    idOperadoraCartao := ADataset.FieldByName(FIELD_ID_OPERADORA_CARTAO).AsInteger;
    descricaoOperadoraCartao := ADataset.FieldByName(FIELD_DESCRICAO_OPERADORA_CARTAO).AsString;

    idFormaPagamento := ADataset.FieldByName(FIELD_ID_FORMA_PAGAMENTO).AsInteger;

    ADataset.Next;
    while not ADataset.Eof do
    begin
      if ADataset.FieldByName(FIELD_ID_FORMA_PAGAMENTO).AsInteger = idFormaPagamento then
      begin
        ADataset.Edit;
        if ADataset.FieldByName(FIELD_ID_OPERADORA_CARTAO).AsInteger <= 0 then
        begin
          ADataset.FieldByName(FIELD_ID_OPERADORA_CARTAO).AsInteger := idOperadoraCartao;
          ADataset.FieldByName(FIELD_DESCRICAO_OPERADORA_CARTAO).AsString := descricaoOperadoraCartao;
        end;
        ADataset.Post;
      end;

      ADataset.Next;
    end;
  finally
    if (Bookmark <> nil) and ADataset.BookmarkValid(Bookmark) then
      ADataset.GoToBookmark(Bookmark);

    ADataset.EnableControls;
  end;
end;

class procedure TParcelamentoUtils.ReplicarInformacoesCheque(
  ADataset: TDataset);
var
  Bookmark: TBookmark;
  chequeSacado: string;
  chequeDocFederal: string;
  chequeBanco: string;
  chequeDtEmissao: string;
  chequeAgencia: string;
  chequeContaCorrente: string;
  chequeNumero: Integer;
  idFormaPagamento: Integer;

const
  FIELD_CHEQUE_SACADO: String = 'CHEQUE_SACADO';
  FIELD_CHEQUE_DOC_FEDERAL: String = 'CHEQUE_DOC_FEDERAL';
  FIELD_CHEQUE_BANCO: String = 'CHEQUE_BANCO';
  FIELD_CHEQUE_DT_EMISSAO: String = 'CHEQUE_DT_EMISSAO';
  FIELD_CHEQUE_AGENCIA: String = 'CHEQUE_AGENCIA';
  FIELD_CHEQUE_CONTA_CORRENTE: String = 'CHEQUE_CONTA_CORRENTE';
  FIELD_CHEQUE_NUMERO: String = 'CHEQUE_NUMERO';
  FIELD_ID_FORMA_PAGAMENTO: String = 'ID_FORMA_PAGAMENTO';
begin
  try
    ADataset.DisableControls;
    Bookmark := ADataset.GetBookMark;

    chequeSacado       := ADataset.FieldByName(FIELD_CHEQUE_SACADO).AsString;
    chequeDocFederal   := ADataset.FieldByName(FIELD_CHEQUE_DOC_FEDERAL).AsString;
    chequeBanco        := ADataset.FieldByName(FIELD_CHEQUE_BANCO).AsString;
    chequeDtEmissao    := ADataset.FieldByName(FIELD_CHEQUE_DT_EMISSAO).AsString;
    chequeAgencia      := ADataset.FieldByName(FIELD_CHEQUE_AGENCIA).AsString;
    chequeContaCorrente:= ADataset.FieldByName(FIELD_CHEQUE_CONTA_CORRENTE).AsString;
    chequeNumero       := ADataset.FieldByName(FIELD_CHEQUE_NUMERO).AsInteger;

    idFormaPagamento := ADataset.FieldByName(FIELD_ID_FORMA_PAGAMENTO).AsInteger;

    ADataset.Next;
    while not ADataset.Eof do
    begin
      Inc(chequeNumero);

      if ADataset.FieldByName(FIELD_ID_FORMA_PAGAMENTO).AsInteger = idFormaPagamento then
      begin
        ADataset.Edit;
        if ADataset.FieldByName(FIELD_CHEQUE_SACADO).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_SACADO).AsString := chequeSacado;

        if ADataset.FieldByName(FIELD_CHEQUE_DOC_FEDERAL).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_DOC_FEDERAL).AsString := chequeDocFederal;

        if ADataset.FieldByName(FIELD_CHEQUE_BANCO).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_BANCO).AsString := chequeBanco;

        if ADataset.FieldByName(FIELD_CHEQUE_DT_EMISSAO).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_DT_EMISSAO).AsString := chequeDtEmissao;

        if ADataset.FieldByName(FIELD_CHEQUE_AGENCIA).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_AGENCIA).AsString := chequeAgencia;

        if ADataset.FieldByName(FIELD_CHEQUE_CONTA_CORRENTE).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_CONTA_CORRENTE).AsString := chequeContaCorrente;

        if ADataset.FieldByName(FIELD_CHEQUE_NUMERO).AsString.IsEmpty then
          ADataset.FieldByName(FIELD_CHEQUE_NUMERO).AsInteger := chequeNumero;
        ADataset.Post;
      end;

      ADataset.Next;
    end;
  finally
    if (Bookmark <> nil) and ADataset.BookmarkValid(Bookmark) then
      ADataset.GoToBookmark(Bookmark);

    ADataset.EnableControls;
  end;
end;

class procedure TParcelamentoUtils.ReprocessarVencimento(AFieldDataVencimento, AFieldDiasVencimento: TField);
var
  bookMarkparcelaAlterada: TBookMark;
  dataset: TClientDataset;
  dataBase: TDate;
begin
  dataset := TClientDataset(AFieldDataVencimento.Dataset);

  if dataset.Eof then
  begin
    Exit;
  end;

  try
    dataset.DisableControls;
    bookMarkparcelaAlterada := dataset.GetBookMark;

    dataBase := AFieldDataVencimento.AsDateTime;

    dataset.Next;
    while not dataset.Eof do
    begin
      dataset.Edit;
      AFieldDataVencimento.AsDateTime := dataBase + AFieldDiasVencimento.AsInteger;
      dataset.Post;

      dataset.Next;
    end;
  finally
    if (bookMarkparcelaAlterada <> nil) and (dataset.BookMarkValid(bookMarkparcelaAlterada)) then
      dataset.GotoBookmark(bookMarkparcelaAlterada);
    dataset.EnableControls;
  end;
end;

class procedure TParcelamentoUtils.TrocarDiaDeVencimento(AFieldDataVencimento: TField;
  ANovoDiaDeVencimento: Integer);
var
  dataset: TClientDataset;
  dataValida: Boolean;
  novaData: TDateTime;
  dia,mes,ano: Word;
begin
  dataset := TClientDataset(AFieldDataVencimento.Dataset);

  DecodeDate(AFieldDataVencimento.AsDateTime, ano, mes, dia);
  dataValida := TryEncodeDate(ano, mes, ANovoDiaDeVencimento, novaData);

  if dataValida then
  begin
    dataset.Edit;
    AFieldDataVencimento.AsDateTime := novaData;
    dataset.Post;
  end;
end;

class function TParcelamentoUtils.ConferirValorParcelado(AField: TField; AValorTotal: Double): Boolean;
var totalDasParcelas: Double;
    dataset: TClientDataset;
    diferencaDeValor: Double;
    bookMarkparcela: TBookMark;
begin
  dataset := TClientDataset(AField.Dataset);
  bookMarkparcela := dataset.GetBookmark;
  try
    totalDasParcelas := TDataSetUtils.SomarColuna(AField);

    diferencaDeValor := totalDasParcelas - AValorTotal;

    if diferencaDeValor > 0 then
    begin
      dataset.Edit;
      AField.AsFloat := AField.AsFloat + diferencaDeValor;
      if dataset.State in dsEditModes then
        dataset.Post;
    end;
  finally
    if (bookMarkparcela <> nil) and (dataset.BookmarkValid(bookMarkparcela)) then
    begin
      dataset.GotoBookmark(bookMarkparcela);
    end;
  end;
end;

class function TParcelamentoUtils.ParcelamentoInvalido(AField: TField): Boolean;
var existemParcelasNegativas: boolean;
    dataset: TClientDataset;
begin
  dataset := TClientDataset(AField.Dataset);
  try
    dataset.Filter := AField.FullName+' < 0';
    dataset.Filtered := true;
    existemParcelasNegativas := not dataset.IsEmpty;
    result := existemParcelasNegativas;
  finally
    dataset.Filter := '';
    dataset.Filtered := false;
  end;
end;

end.
