unit uUsuarioDesignControl;

interface

Uses cxGridDBBandedTableView, System.IniFiles, Windows, Classes, SysUtils, Forms,
     cxGridCustomView, db, Data.DBXJSONReflect, Contnrs, Data.DBXJSON, System.JSON,
     Data.DBXJSONCommon, System.Generics.Collections, cxVGrid, Variants, Rest.JSON;

type TParametroFormulario = class
  private
    FValor: String;
    FDescricao: String;
    FParametro: String;
    FObrigatorio: boolean;
  public
    const VALOR_SIM = 'S';
    const VALOR_NAO = 'N';
    const PARAMETRO_NAO_OBRIGATORIO = false;
    const PARAMETRO_OBRIGATORIO = true;
    const NUMERO_INVALIDO = -999987;

    property parametro: String read FParametro write FParametro;
    property valor: String read FValor write FValor;
    property descricao: String read FDescricao write FDescricao;
    property obrigatorio: boolean read FObrigatorio write FObrigatorio;

    constructor Create(ANomeParamentro, ADescricaoParametro: String;
      AParametroObrigatorio: Boolean = false;
      AValorParametro: String = '');overload;

    function EstaPreenchido: Boolean;
    function ValorNumericoValido: Boolean;
    function ValorSim: Boolean;
    function ValorNao: Boolean;
    function AsInteger: Integer;
    function AsString: String;
end;

type TListaParametroFormulario = class(TObjectList)
  public
    procedure AdicionarParametro(AParametro: TParametroFormulario);
    function Valor(AParametro: String): String;
    function ParametrosConfigurados(var AMensagem: String): Boolean;
end;

type TUsuarioParametrosFormulario = class
  private
    class function DatasetToJSON(ADataset:TDataset): TJSONArray;
    class procedure PopularDatasetComParametros(AParametros: TJSONArray; ADataset: TDataset);
    class procedure PopularListaComParametros(AParametros: TJSONArray;
      AListaParametros: TListaParametroFormulario);
  public
    class procedure SalvarParametros(ACodigoUsuario: Integer; AFormName: String;
      AParametros: TDataset);
    class procedure BuscarParametros(ACodigoUsuario: Integer; AFormName: String;
      ADataset: TDataset);
    class procedure CriarParametro(AParametro: TListaParametroFormulario;
      ACodigoUsuario: Integer; AFormName: String);
    class procedure CarregarParametros(AParametros: TListaParametroFormulario;
      ACodigoUsuario: Integer; AFormName: String);
end;

type TUsuarioDesignControl = class
  private
    //Monta uma lista com os datasets e nestedDatasets
    procedure MontarDicionarioDeDatasets(
      ADicionarioDatasets: TDictionary<String, TDataset>; ADataset: TDataset);

    //Alimenta os campos com os valores padr�o salvos
    procedure SetValoresPadraoNoDataset(ADataset: TDataset; ADatasets: TJsonArray);

    //Devolve a string do arquivo referente aos valores padrao CAMPO=VALOR#13CAMPO=VALOR
    function GetFieldsFile(AVerticalGrid: TcxVerticalGrid): String;

    //Devolve o nome do formulario que sera salvo no banco de dados para recupera��o posterior
    function GetFormName(ADataset: TDataset; AFormName: String): String;

    const DelimitadorSeparacaoField: String = '=';
  public
    //Salva a vis�o da grid por usuario
    class procedure SalvarValoresPadrao(AVerticalGrid: TcxVerticalGrid; ACodigoUsuario: Integer; AFormName: String);

    //Seta os valores padrao no novo registro
    class procedure CarregarValoresPadrao(ADataset: TDataset; ACodigoUsuario: Integer; AFormName: String);

    //Deleta a vis�o do banco de dados
    class procedure RemoverValoresPadrao(ADataset: TDataset; ACodigoUsuario: Integer; AFormName: String);

    //Verifica se existe arquivo de valores padr�o salvo
    class function ExisteArquivoValoresPadrao(ACodigoUsuario: Integer; AFormName: String): Boolean;

    //Popula a grid com os valores j� salvos no banco
    class procedure CarregarValoresPadraoNaGrid(AVerticalGrid: TcxVerticalGrid; ACodigoUsuario: Integer; AFormName: String);
end;

implementation

{ TUsuarioDesignControl }

uses uDmConnection, uDevExpressUtils, uVerticalGridUtils;

class procedure TUsuarioDesignControl.RemoverValoresPadrao(ADataset: TDataset;
  ACodigoUsuario: Integer; AFormName: String);
var usuario: TUsuarioDesignControl;
begin
  usuario := TUsuarioDesignControl.Create;
  try
    with usuario do
    begin
      DmConnection.cdsDesignControl.Close;
      DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
      DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
      DmConnection.cdsDesignControl.Open;

      if not DmConnection.cdsDesignControl.IsEmpty then
      begin
        DmConnection.cdsDesignControl.Delete;
        DmConnection.cdsDesignControl.Commit;
      end;
    end;
  finally
    usuario.Free;
  end;
end;

function TUsuarioDesignControl.GetFormName(ADataset: TDataset; AFormName: String): String;
begin
  result := UpperCase(AFormName+'_'+ADataset.Name);
end;

class procedure TUsuarioDesignControl.CarregarValoresPadrao(ADataset: TDataset;
  ACodigoUsuario: Integer; AFormName: String);
var usuario: TUsuarioDesignControl;
    stream: TMemoryStream;
    datasetsCamposPadrao: TJSONArray;
begin
  usuario := TUsuarioDesignControl.Create;
  try
    with usuario do
    begin
      try
        DmConnection.cdsDesignControl.Close;
        DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
        DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
        DmConnection.cdsDesignControl.Open;

        if not(DmConnection.cdsDesignControl.IsEmpty) and
           not(DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString.IsEmpty) then
        begin
          datasetsCamposPadrao := TJSONObject.ParseJSONValue(
            TEncoding.ASCII.GetBytes(DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString),0) as TJSONArray;

          try
            SetValoresPadraoNoDataset(ADataset, datasetsCamposPadrao);
          finally
            datasetsCamposPadrao.Free;
          end;
        end;
      finally
        DmConnection.cdsDesignControl.Close;
      end;
    end;
  finally
    usuario.Free;
  end;
end;

class procedure TUsuarioDesignControl.SalvarValoresPadrao(
  AVerticalGrid: TcxVerticalGrid; ACodigoUsuario: Integer; AFormName: String);
var Stream: TMemoryStream;
    usuario: TUsuarioDesignControl;
begin
  usuario := TUsuarioDesignControl.Create;
  try
    with usuario do
    try
      DmConnection.cdsDesignControl.Close;
      DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
      DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
      DmConnection.cdsDesignControl.Open;

      if DmConnection.cdsDesignControl.IsEmpty then
        DmConnection.cdsDesignControl.Incluir
      else
        DmConnection.cdsDesignControl.Alterar;

      DmConnection.cdsDesignControlID_PESSOA_USUARIO.AsInteger := ACodigoUsuario;
      DmConnection.cdsDesignControlFORMULARIO.AsString := UpperCase(AFormName);
      DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString := GetFieldsFile(AVerticalGrid);
      DmConnection.cdsDesignControl.Salvar;

      DmConnection.cdsDesignControl.Commit;
    finally
      DmConnection.cdsDesignControl.Close;
    end;
  finally
    FreeAndNil(usuario);
  end;
end;

class procedure TUsuarioDesignControl.CarregarValoresPadraoNaGrid(
  AVerticalGrid: TcxVerticalGrid; ACodigoUsuario: Integer; AFormName: String);
var
  datasetJSON: TJSONObject;
  linha: Integer;
  categoria: TcxCategoryRow;
  i, iJSONObject: Integer;
  datasetsCamposPadrao: TJSONArray;
  valorJSON: TJSONValue;
begin
  DmConnection.cdsDesignControl.Close;
  DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
  DmConnection.cdsDesignControl.Open;

  if not(DmConnection.cdsDesignControl.IsEmpty) and
     not(DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString.IsEmpty) then
  begin
    datasetsCamposPadrao := TJSONObject.ParseJSONValue(
      TEncoding.ASCII.GetBytes(DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString),0) as TJSONArray;
    try
      for linha := 0 to Pred(AVerticalGrid.Rows.Count) do
      begin
        if not (AVerticalGrid.Rows[linha] is TcxCategoryRow) then
          continue;

        categoria := TcxCategoryRow(AVerticalGrid.Rows[linha]);
        if categoria.HasChildren then
        begin
          for iJSONObject := 0 to Pred(datasetsCamposPadrao.Count) do
          begin
            datasetJSON := TJSONObject(datasetsCamposPadrao.Items[iJSONObject]);
            if datasetJSON.Get(0).JsonValue.Value = categoria.Properties.Caption then
            begin
              for i := 0 to Pred(categoria.Count) do
              begin
                valorJSON := datasetJSON.GetValue(TcxCategoryRow(categoria.Rows[i]).Properties.Caption);
                if Assigned(valorJSON) then
                begin
                  TcxEditorRow(categoria.Rows[i]).Properties.Value := valorJSON.Value;
                end;
              end;
            end;
          end;
        end;
      end;
    finally
      datasetsCamposPadrao.Free;
    end;
  end;
end;

class function TUsuarioDesignControl.ExisteArquivoValoresPadrao(ACodigoUsuario: Integer; AFormName: String): Boolean;
begin
  DmConnection.cdsDesignControl.Close;
  DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
  DmConnection.cdsDesignControl.Open;

  result := DmConnection.cdsDesignControl.IsEmpty and
    not(DmConnection.cdsDesignControlARQUIVO_VALORES_PADRAO.AsString.IsEmpty);
end;

function TUsuarioDesignControl.GetFieldsFile(AVerticalGrid: TcxVerticalGrid): String;
var
  gridJSON: TJSONArray;
  categoriaJSON: TJSONObject;
  categoria: TcxCategoryRow;
  i, linha: Integer;
begin
  gridJSON := TJSONArray.Create;

  for linha := 0 to Pred(AVerticalGrid.Rows.Count) do
  begin
    if not (AVerticalGrid.Rows[linha] is TcxCategoryRow) then
      continue;

    categoria := TcxCategoryRow(AVerticalGrid.Rows[linha]);
    if categoria.HasChildren then
    begin
      categoriaJSON := TJSONObject.Create;
      //Define o nome do dataset
      categoriaJSON.AddPair(TJSONPair.Create('dataset', categoria.Properties.Caption));

      for i := 0 to Pred(categoria.Count) do
      begin
        if not TVerticalGridUtils.RowisNull(TcxEditorRow(categoria.Rows[i])) then
        begin
          categoriaJSON.AddPair(
            TcxEditorRow(categoria.Rows[i]).Properties.Caption,
            VartoStr(TcxEditorRow(categoria.Rows[i]).Properties.Value));
        end;
      end;
    end;

    gridJSON.AddElement(categoriaJSON);
  end;

  result := gridJSON.ToString;
end;

procedure TUsuarioDesignControl.MontarDicionarioDeDatasets(
  ADicionarioDatasets: TDictionary<String, TDataset>; ADataset: TDataset);
var
  i: Integer;
begin
  ADicionarioDatasets.Add(ADataset.Name, ADataset);

  for i := 0 to Pred(ADataset.Fields.Count) do
  begin
    if ADataset.Fields[i] is TDatasetField then
      MontarDicionarioDeDatasets(ADicionarioDatasets, TDatasetField(ADataset.Fields[i]).NestedDataSet);
  end;
end;

procedure TUsuarioDesignControl.SetValoresPadraoNoDataset(ADataset: TDataset; ADatasets: TJsonArray);
var i, iJSONObject, iJSONPair: Integer;
    posicao, tamanho: Integer;
    fieldSearch, caption: String;
    fieldList: TDictionary<String, String>;
    datasetJSON: TJSONObject;
    pairJSON: TJSONPair;
    dicionarioDatasets: TDictionary<String, TDataset>;
    datasetEncontrado: TDataset;
    valor: String;
const
  DATA_ATUAL: String = '01/01/1001';
  DATA_HORA: String = '01/01/1001 00:00:00';
  DATA_VAZIA: String = '31/01/1001 00:00:00';
  DATA_HORA_VAZIA: String = '31/01/1001 00:00:00';
  VAZIO: String = '#VAZIO#';
begin
  dicionarioDatasets := TDictionary<String, TDataset>.Create;

  MontarDicionarioDeDatasets(dicionarioDatasets, ADataset);

  for iJSONObject := 0 to Pred(ADatasets.Count) do
  begin
    datasetJSON := TJSONObject(ADatasets.Items[iJSONObject]);
    for iJSONPair := 0 to Pred(datasetJSON.Count) do
    begin
      pairJSON := datasetJSON.Pairs[iJSONPair];
      if pairJSON.JsonString.Value = 'dataset' then
      begin
        if not dicionarioDatasets.TryGetValue(pairJSON.JsonValue.Value, datasetEncontrado) then
          continue;
      end
      else
      begin
        if Assigned(datasetEncontrado) then
        begin
          if Assigned(datasetEncontrado.FindField(pairJSON.JsonString.Value)) then
          try
            valor := StringReplace(VartoStr(pairJSON.JsonValue.Value),
              DATA_ATUAL,DatetoStr(Date), [rfReplaceAll]);

            valor := StringReplace(valor,
              DATA_HORA,DateTimetoStr(Now), [rfReplaceAll]);

            valor := StringReplace(valor,
              VAZIO,'', [rfReplaceAll]);

            valor := StringReplace(valor,
              DATA_HORA_VAZIA,'', [rfReplaceAll]);

            valor := StringReplace(valor,
              DATA_VAZIA,'', [rfReplaceAll]);

            if valor = '' then
            begin
              datasetEncontrado.FieldByName(pairJSON.JsonString.Value).Clear;
            end
            else
            begin
              datasetEncontrado.FieldByName(pairJSON.JsonString.Value).AsString :=
                valor;
            end;
          Except
          end;
        end;
      end;
    end;
  end;
end;

{ TUsuarioParametrosFormulario }

class procedure TUsuarioParametrosFormulario.BuscarParametros(ACodigoUsuario: Integer;
  AFormName: String; ADataset: TDataset);
var
  parametrosJSON: TJSONArray;
begin
  DmConnection.cdsDesignControl.Close;
  DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
  DmConnection.cdsDesignControl.Open;

  if not(DmConnection.cdsDesignControl.IsEmpty) and
     not(DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString.IsEmpty) then
  begin
    parametrosJSON := TJSONObject.ParseJSONValue(
      TEncoding.ASCII.GetBytes(DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString),0) as TJSONArray;

    try
      TUsuarioParametrosFormulario.PopularDatasetComParametros(parametrosJSON,
        ADataset);
    finally
      parametrosJSON.Free;
    end;
  end;
end;

class procedure TUsuarioParametrosFormulario.CarregarParametros(
  AParametros: TListaParametroFormulario; ACodigoUsuario: Integer;
  AFormName: String);
var
  parametrosJSON: TJSONArray;
begin
  DmConnection.cdsDesignControl.Close;
  DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
  DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
  DmConnection.cdsDesignControl.Open;

  if not(DmConnection.cdsDesignControl.IsEmpty) and
     not(DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString.IsEmpty) then
  begin
    parametrosJSON := TJSONObject.ParseJSONValue(
      TEncoding.ASCII.GetBytes(DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString),0) as TJSONArray;

    if parametrosJSON <> nil then
    try
      TUsuarioParametrosFormulario.PopularListaComParametros(parametrosJSON,
        AParametros);
    finally
      parametrosJSON.Free;
    end;
  end;
end;

class procedure TUsuarioParametrosFormulario.CriarParametro(
  AParametro: TListaParametroFormulario; ACodigoUsuario: Integer; AFormName: String);
var
  parametroFormularioJSON: TJSONArray;
  parametroJSON: TJSONObject;
  i: Integer;
  AParametrosExistentes: TListaParametroFormulario;
  valor: String;
begin
  AParametrosExistentes := TListaParametroFormulario.Create;
  TUsuarioParametrosFormulario.CarregarParametros(AParametrosExistentes,
    ACodigoUsuario, AFormName);

  parametroFormularioJSON := TJSONArray.Create;
  try
    for i := 0 to Pred(AParametro.Count) do
    begin
      parametroJSON := TJSONObject.Create;

      parametroJSON.AddPair(TJSONPair.Create('PARAMETRO',
        TParametroFormulario(AParametro[i]).parametro));

      valor := AParametrosExistentes.Valor(
        TParametroFormulario(AParametro[i]).parametro);

      if valor.IsEmpty then
      begin
        parametroJSON.AddPair(TJSONPair.Create('VALOR',
          TParametroFormulario(AParametro[i]).valor));
      end
      else
      begin
        parametroJSON.AddPair(TJSONPair.Create('VALOR',valor));
        TParametroFormulario(AParametro[i]).Valor := valor;
      end;

      parametroJSON.AddPair(TJSONPair.Create('DESCRICAO',
        TParametroFormulario(AParametro[i]).descricao));

      if TParametroFormulario(AParametro[i]).obrigatorio then
      begin
        parametroJSON.AddPair(TJSONPair.Create('BO_OBRIGATORIO','S'));
      end
      else
      begin
        parametroJSON.AddPair(TJSONPair.Create('BO_OBRIGATORIO','N'));
      end;

      parametroFormularioJSON.AddElement(parametroJSON);
    end;

    DmConnection.cdsDesignControl.Close;
    DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
    DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
    DmConnection.cdsDesignControl.Open;

    if DmConnection.cdsDesignControl.IsEmpty then
      DmConnection.cdsDesignControl.Incluir
    else
      DmConnection.cdsDesignControl.Alterar;

    DmConnection.cdsDesignControlID_PESSOA_USUARIO.AsInteger := ACodigoUsuario;
    DmConnection.cdsDesignControlFORMULARIO.AsString := UpperCase(AFormName);

    DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString :=
      parametroFormularioJSON.ToString;

    DmConnection.cdsDesignControl.Salvar;

    DmConnection.cdsDesignControl.Commit;
  finally
    DmConnection.cdsDesignControl.Close;
    FreeAndNil(parametroFormularioJSON);
  end;
end;

class function TUsuarioParametrosFormulario.DatasetToJSON(
  ADataset: TDataset): TJSONArray;
var
  parametroJSON: TJSONObject;
  categoria: TcxCategoryRow;
  i, linha: Integer;
begin
  result := TJSONArray.Create;
  try
    ADataset.DisableControls;
    ADataset.First;
    while not ADataset.Eof do
    begin
      parametroJSON := TJSONObject.Create;

      for i := 0 to Pred(ADataset.FieldCount) do
      begin
        parametroJSON.AddPair(TJSONPair.Create(ADataset.Fields[i].FieldName,
          ADataset.Fields[i].AsString));
      end;

      result.AddElement(parametroJSON);

      ADataset.Next;
    end;
  finally
    ADataset.EnableControls;
  end;
end;

class procedure TUsuarioParametrosFormulario.PopularDatasetComParametros(
  AParametros: TJSONArray; ADataset: TDataset);
var iJSONObject, iJSONPair: Integer;
    datasetJSON: TJSONObject;
    pairJSON: TJSONPair;
    valor: String;
begin
  for iJSONObject := 0 to Pred(AParametros.Count) do
  begin
    datasetJSON := TJSONObject(AParametros.Items[iJSONObject]);
    ADataset.Append;
    for iJSONPair := 0 to Pred(datasetJSON.Count) do
    begin
      pairJSON := datasetJSON.Pairs[iJSONPair];
      if Assigned(ADataset.FindField(pairJSON.JsonString.Value)) then
      try
        valor := VartoStr(pairJSON.JsonValue.Value);

        if valor = '' then
        begin
          ADataset.FieldByName(pairJSON.JsonString.Value).Clear;
        end
        else
        begin
          ADataset.FieldByName(pairJSON.JsonString.Value).AsString :=
            valor;
        end;
      Except
      end;
    end;

    if ADataset.Modified then
      ADataset.Post;
  end;
end;

class procedure TUsuarioParametrosFormulario.PopularListaComParametros(
  AParametros: TJSONArray; AListaParametros: TListaParametroFormulario);
var iJSONObject: Integer;
    datasetJSON: TJSONObject;
    pairJSON: TJSONPair;
    parametro: TParametroFormulario;
    valor: String;
begin
  AListaParametros.Clear;
  for iJSONObject := 0 to Pred(AParametros.Count) do
  begin
    datasetJSON := TJSONObject(AParametros.Items[iJSONObject]);

    parametro := TParametroFormulario.Create;
    parametro.parametro := datasetJSON.GetValue('PARAMETRO').Value;
    parametro.valor := datasetJSON.GetValue('VALOR').Value;
    parametro.descricao := datasetJSON.GetValue('DESCRICAO').Value;
    parametro.obrigatorio := VartoStr(datasetJSON.GetValue('BO_OBRIGATORIO').Value) = 'S';

    AListaParametros.Add(parametro);
  end;
end;

class procedure TUsuarioParametrosFormulario.SalvarParametros(ACodigoUsuario: Integer;
  AFormName: String; AParametros: TDataset);
begin
  try
    DmConnection.cdsDesignControl.Close;
    DmConnection.cdsDesignControl.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
    DmConnection.cdsDesignControl.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
    DmConnection.cdsDesignControl.Open;

    if DmConnection.cdsDesignControl.IsEmpty then
      DmConnection.cdsDesignControl.Incluir
    else
      DmConnection.cdsDesignControl.Alterar;

    DmConnection.cdsDesignControlID_PESSOA_USUARIO.AsInteger := ACodigoUsuario;
    DmConnection.cdsDesignControlFORMULARIO.AsString := UpperCase(AFormName);
    DmConnection.cdsDesignControlARQUIVO_PARAMETROS.AsString :=
      TUsuarioParametrosFormulario.DatasetToJSON(AParametros).ToString;
    DmConnection.cdsDesignControl.Salvar;

    DmConnection.cdsDesignControl.Commit;
  finally
    DmConnection.cdsDesignControl.Close;
  end;
end;

{ TListaParametroFormulario }

procedure TListaParametroFormulario.AdicionarParametro(AParametro: TParametroFormulario);
begin
  Self.Add(AParametro);
end;

function TListaParametroFormulario.ParametrosConfigurados(
  var AMensagem: String): Boolean;
var i: Integer;
  parametro: TParametroFormulario;
begin
  result := true;
  for i := 0 to Pred(Self.Count) do
  begin
    parametro := TParametroFormulario(Self.GetItem(i));
    if parametro.obrigatorio and Trim(parametro.valor).IsEmpty then
    begin
      result := false;
      AMensagem := AMensagem + parametro.PARAMETRO+#13;
    end;
  end;
  if not result then
  begin
    AMensagem.Remove(Length(AMensagem)-1);
    AMensagem := 'Existem par�metros obrigat�rios n�o configurados: '+AMensagem;
  end;
end;

function TListaParametroFormulario.Valor(AParametro: String): String;
var i: Integer;
  parametro: TParametroFormulario;
begin
  result := '';
  for i := 0 to Pred(Self.Count) do
  begin
    parametro := TParametroFormulario(Self.GetItem(i));
    if parametro.parametro = AParametro then
    begin
      result := parametro.valor;
      break;
    end;
  end;
end;

{ TParametroFormulario }

function TParametroFormulario.AsInteger: Integer;
begin
  result := StrtoIntDef(Self.Valor,NUMERO_INVALIDO);
end;

function TParametroFormulario.AsString: String;
begin
  result := Self.Valor;
end;

constructor TParametroFormulario.Create(ANomeParamentro,
  ADescricaoParametro: String; AParametroObrigatorio: Boolean;
  AValorParametro: String);
begin
  inherited Create;

  Self.Parametro := ANomeParamentro;
  Self.Descricao := ADescricaoParametro;
  Self.Obrigatorio := AParametroObrigatorio;
  Self.Valor := AValorParametro;
end;

function TParametroFormulario.EstaPreenchido: Boolean;
begin
  result := not Self.Valor.IsEmpty;
end;

function TParametroFormulario.ValorNumericoValido: Boolean;
begin
  result := Self.AsInteger <> NUMERO_INVALIDO;
end;

function TParametroFormulario.ValorNao: Boolean;
begin
  result := UpperCase(Self.Valor).Equals(VALOR_NAO);
end;

function TParametroFormulario.ValorSim: Boolean;
begin
  result := UpperCase(Self.Valor).Equals(VALOR_SIM);
end;

end.
