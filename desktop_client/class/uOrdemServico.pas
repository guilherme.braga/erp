unit uOrdemServico;

interface

Uses db, Generics.Collections, ugbClientDataset, SysUtils, uOrdemServicoProxy;

type TOrdemServico = class
  {Client}

  //Calculos de valores do produto
  class procedure CalcularValorBrutoProduto(ADataset: TgbClientDataset);
  class procedure CalcularValorLiquidoProduto(ADataset: TgbClientDataset);

  //Calculos de valores do servico
  class procedure CalcularValorBrutoServico(ADataset: TgbClientDataset);
  class procedure CalcularValorLiquidoServico(ADataset: TgbClientDataset);

  //Calculos dos totais da OS
  class procedure CalcularValorTotalProdutos(ADatasetOrdemServico, ADatasetProduto: TgbClientDataset);
  class procedure CalcularValorTotalServicos(ADatasetOrdemServico, ADatasetServico: TgbClientDataset);
  class procedure CalcularValorTotalOS(ADataset: TgbClientDataset);

  class procedure GerarParcelas(AOrdemServico, AOrdemServicoParcela: TGbClientDataset;
    AIdFormaPagamento: Integer = 0; AIdCarteira: Integer = 0);
  class procedure LimparParcelas(AGeracaoDocumentoParcela: TgbClientDataset);

  class function OrdemServicoQuitada(AOrdemServico, AOrdemServicoParcela: TgbClientDataset): Boolean;
  class function ValorTotalParcelado(AOrdemServicoParcela: TgbClientDataset): Double;

  {Serve}
  class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
  class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
  class function Efetivar(AIdChaveProcesso: Integer;
    AOrdemServicoProxy: TOrdemServicoProxy): Boolean;
  class function Cancelar(AIdChaveProcesso: Integer): Boolean;
  class function GetId(AIdChaveProcesso: Integer): Integer;
end;

implementation

{ TOrdemServico }

uses uProduto, uClientClasses, uDmConnection, uPlanoPagamento, uMathUtils,
  uConstantes, uFormaPagamentoProxy, uFormaPagamento, REST.JSON, uDatasetUtils,
  uConsultaGrid, uCarteira, uCarteiraProxy;

class procedure TOrdemServico.CalcularValorBrutoProduto(
  ADataset: TgbClientDataset);
begin
  if ADataset.EstadoDeAlteracao then
  begin
    ADataset.SetAsFloat('VL_BRUTO',
      (ADataset.GetAsFloat('QUANTIDADE') * ADataset.GetAsFloat('VL_UNITARIO')));
  end;
end;

class procedure TOrdemServico.CalcularValorBrutoServico(
  ADataset: TgbClientDataset);
begin
  if ADataset.EstadoDeAlteracao then
  begin
    ADataset.SetAsFloat('VL_BRUTO',
      (ADataset.GetAsFloat('QUANTIDADE') * ADataset.GetAsFloat('VL_UNITARIO')));
  end;
end;

class procedure TOrdemServico.CalcularValorLiquidoProduto(
  ADataset: TgbClientDataset);
begin
  if ADataset.EstadoDeAlteracao then
  begin
    ADataset.SetAsFloat('VL_LIQUIDO',
     (ADataset.GetAsFloat('VL_BRUTO') +
      ADataset.GetAsFloat('VL_ACRESCIMO') -
      ADataset.GetAsFloat('VL_DESCONTO')));
  end;
end;

class procedure TOrdemServico.CalcularValorLiquidoServico(
  ADataset: TgbClientDataset);
begin
  if ADataset.EstadoDeAlteracao then
  begin
    ADataset.SetAsFloat('VL_LIQUIDO',
     (ADataset.GetAsFloat('VL_BRUTO') +
      ADataset.GetAsFloat('VL_ACRESCIMO') -
      ADataset.GetAsFloat('VL_DESCONTO')));
  end;
end;

class procedure TOrdemServico.CalcularValorTotalOS(ADataset: TgbClientDataset);
begin
  if ADataset.EstadoDeAlteracao then
  begin
    ADataset.SetAsFloat('VL_TOTAL_BRUTO',
      (ADataset.GetAsFloat('VL_TOTAL_BRUTO_PRODUTO') +
       ADataset.GetAsFloat('VL_TOTAL_BRUTO_SERVICO') +
       ADataset.GetAsFloat('VL_TOTAL_BRUTO_SERVICO_TERCEIRO')));

    ADataset.SetAsFloat('VL_TOTAL_DESCONTO',
      (ADataset.GetAsFloat('VL_TOTAL_DESCONTO_PRODUTO') +
       ADataset.GetAsFloat('VL_TOTAL_DESCONTO_SERVICO') +
       ADataset.GetAsFloat('VL_TOTAL_DESCONTO_SERVICO_TERCEIRO')));

    ADataset.SetAsFloat('VL_TOTAL_ACRESCIMO',
      (ADataset.GetAsFloat('VL_TOTAL_ACRESCIMO_PRODUTO') +
       ADataset.GetAsFloat('VL_TOTAL_ACRESCIMO_SERVICO') +
       ADataset.GetAsFloat('VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO')));

    ADataset.SetAsFloat('PERC_TOTAL_DESCONTO',
      TMathUtils.PercentualSobreValor(ADataset.GetAsFloat('VL_TOTAL_DESCONTO'), ADataset.GetAsFloat('VL_TOTAL_BRUTO')));

    ADataset.SetAsFloat('PERC_TOTAL_ACRESCIMO',
      TMathUtils.PercentualSobreValor(ADataset.GetAsFloat('VL_TOTAL_ACRESCIMO'), ADataset.GetAsFloat('VL_TOTAL_BRUTO')));

    ADataset.SetAsFloat('VL_TOTAL_LIQUIDO',
      (ADataset.GetAsFloat('VL_TOTAL_LIQUIDO_PRODUTO') +
       ADataset.GetAsFloat('VL_TOTAL_LIQUIDO_SERVICO') +
       ADataset.GetAsFloat('VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO')));
  end;
end;

class procedure TOrdemServico.CalcularValorTotalProdutos(
  ADatasetOrdemServico, ADatasetProduto: TgbClientDataset);
var
  produtos: TgbClientDataset;
  vlTotalBrutoProduto: Double;
  vlTotalDescontoProduto: Double;
  vlTotalAcrescimoProduto: Double;
  vlTotalLiquidoProduto: Double;
begin
  if not ADatasetOrdemServico.Alterando then
    Exit;

  vlTotalBrutoProduto := 0;
  vlTotalDescontoProduto := 0;
  vlTotalAcrescimoProduto := 0;
  vlTotalLiquidoProduto := 0;

  produtos := TgbClientDataset.Create(nil);
  try
    produtos.CloneCursor(ADatasetProduto, false);
    produtos.First;
    while not produtos.Eof do
    begin
      vlTotalBrutoProduto := vlTotalBrutoProduto +
        produtos.GetAsFloat('VL_BRUTO');
      vlTotalDescontoProduto := vlTotalDescontoProduto +
        produtos.GetAsFloat('VL_DESCONTO');
      vlTotalAcrescimoProduto := vlTotalAcrescimoProduto +
        produtos.GetAsFloat('VL_ACRESCIMO');
      vlTotalLiquidoProduto := vlTotalLiquidoProduto +
        produtos.GetAsFloat('VL_LIQUIDO');

      produtos.Next;
    end;

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_BRUTO_PRODUTO',
      vlTotalBrutoProduto);

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_DESCONTO_PRODUTO',
      vlTotalDescontoProduto);

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_ACRESCIMO_PRODUTO',
      vlTotalAcrescimoProduto);

    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_DESCONTO_PRODUTO',
      TMathUtils.PercentualSobreValor(vlTotalDescontoProduto, vlTotalBrutoProduto));

    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_ACRESCIMO_PRODUTO',
      TMathUtils.PercentualSobreValor(vlTotalAcrescimoProduto, vlTotalBrutoProduto));

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_LIQUIDO_PRODUTO',
      vlTotalLiquidoProduto);
  finally
    FreeAndNil(produtos);
  end;
end;

class procedure TOrdemServico.CalcularValorTotalServicos(
  ADatasetOrdemServico, ADatasetServico: TgbClientDataset);
var
  servicos: TgbClientDataset;
  vlTotalBrutoServico: Double;
  vlTotalDescontoServico: Double;
  vlTotalAcrescimoServico: Double;
  vlTotalLiquidoServico: Double;
  vlTotalBrutoServicoTerceiro: Double;
  vlTotalDescontoServicoTerceiro: Double;
  vlTotalAcrescimoServicoTerceiro: Double;
  vlTotalLiquidoServicoTerceiro: Double;
begin
  if not ADatasetOrdemServico.Alterando then
    Exit;

  vlTotalBrutoServico := 0;
  vlTotalDescontoServico := 0;
  vlTotalAcrescimoServico := 0;
  vlTotalLiquidoServico := 0;
  vlTotalBrutoServicoTerceiro := 0;
  vlTotalDescontoServicoTerceiro := 0;
  vlTotalAcrescimoServicoTerceiro := 0;
  vlTotalLiquidoServicoTerceiro := 0;

  servicos := TgbClientDataset.Create(nil);
  try
    servicos.CloneCursor(ADatasetServico, false);
    servicos.First;
    while not servicos.Eof do
    begin
      if servicos.GetAsString('BO_SERVICO_TERCEIRO').Equals(TConstantes.BO_SIM) then
      begin
        vlTotalBrutoServicoTerceiro := vlTotalBrutoServicoTerceiro +
          servicos.GetAsFloat('VL_BRUTO');
        vlTotalDescontoServicoTerceiro := vlTotalDescontoServicoTerceiro +
          servicos.GetAsFloat('VL_DESCONTO');
        vlTotalAcrescimoServicoTerceiro := vlTotalAcrescimoServicoTerceiro +
          servicos.GetAsFloat('VL_ACRESCIMO');
        vlTotalLiquidoServicoTerceiro := vlTotalLiquidoServicoTerceiro +
          servicos.GetAsFloat('VL_LIQUIDO');
      end
      else
      begin
        vlTotalBrutoServico := vlTotalBrutoServico +
          servicos.GetAsFloat('VL_BRUTO');
        vlTotalDescontoServico := vlTotalDescontoServico +
          servicos.GetAsFloat('VL_DESCONTO');
        vlTotalAcrescimoServico := vlTotalAcrescimoServico +
          servicos.GetAsFloat('VL_ACRESCIMO');
        vlTotalLiquidoServico := vlTotalLiquidoServico +
          servicos.GetAsFloat('VL_LIQUIDO');
      end;

      servicos.Next;
    end;

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_BRUTO_SERVICO',
      vlTotalBrutoServico);
    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_DESCONTO_SERVICO',
      vlTotalDescontoServico);
    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_ACRESCIMO_SERVICO',
      vlTotalAcrescimoServico);
    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_DESCONTO_SERVICO',
      TMathUtils.PercentualSobreValor(vlTotalDescontoServico, vlTotalBrutoServico));
    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_ACRESCIMO_SERVICO',
      TMathUtils.PercentualSobreValor(vlTotalAcrescimoServico, vlTotalBrutoServico));
    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_LIQUIDO_SERVICO',
      vlTotalLiquidoServico);

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_BRUTO_SERVICO_TERCEIRO',
      vlTotalBrutoServicoTerceiro);
    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_DESCONTO_SERVICO_TERCEIRO',
      vlTotalDescontoServicoTerceiro);
    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO',
      vlTotalAcrescimoServicoTerceiro);
    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO',
      TMathUtils.PercentualSobreValor(vlTotalDescontoServicoTerceiro, vlTotalBrutoServicoTerceiro));
    ADatasetOrdemServico.SetAsFloat('PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO',
      TMathUtils.PercentualSobreValor(vlTotalAcrescimoServicoTerceiro, vlTotalBrutoServicoTerceiro));

    ADatasetOrdemServico.SetAsFloat('VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO',
      vlTotalLiquidoServicoTerceiro);
  finally
    FreeAndNil(servicos);
  end;
end;

class function TOrdemServico.Cancelar(AIdChaveProcesso: Integer): Boolean;
var smOrdemServico: TSMOrdemServicoClient;
begin
  smOrdemServico := TsmOrdemServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOrdemServico.Cancelar(AIdChaveProcesso);
  finally
    smOrdemServico.Free;
  end;
end;

class function TOrdemServico.Efetivar(AIdChaveProcesso: Integer;
    AOrdemServicoProxy: TOrdemServicoProxy): Boolean;
var smOrdemServico: TSMOrdemServicoClient;
begin
  smOrdemServico := TsmOrdemServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOrdemServico.Efetivar(AIdChaveProcesso, TJSON.ObjectToJsonString(AOrdemServicoProxy));
  finally
    smOrdemServico.Free;
  end;
end;

class procedure TOrdemServico.GerarParcelas(AOrdemServico,
  AOrdemServicoParcela: TGbClientDataset; AIdFormaPagamento: Integer = 0; AIdCarteira: Integer = 0);
var parcelamento: TParcelamento;
    parcela: TParcela;
    valorParcela, valorTitulo, diferencaNoParcelamento, totalParcelas: Double;
    i: Integer;
    formaPagamento: TFormaPagamentoProxy;
    carteira: TCarteiraProxy;
begin
  if AOrdemServico.FieldByName('ID_PLANO_PAGAMENTO').AsString.IsEmpty then
    exit;

  totalParcelas := 0;
  diferencaNoParcelamento := 0;
  valorTitulo := AOrdemServico.FieldByName('VL_TOTAL_LIQUIDO').AsFloat - AOrdemServico.FieldByName('VL_PAGAMENTO').AsFloat;

  parcelamento := TParcelamento.ParcelamentoBaseCadastro(AOrdemServico.FieldByName('ID_PLANO_PAGAMENTO').AsInteger,
    Date, 1);

  if parcelamento.quantidadeParcelas <= 0 then
    Exit;

  valorParcela := TMathUtils.Arredondar( valorTitulo / parcelamento.quantidadeParcelas);

  for i := 0 to Pred(parcelamento.parcelas.Count) do
  begin
    parcela := TParcela(parcelamento.parcelas[i]);

    AOrdemServicoParcela.Incluir;
    AOrdemServicoParcela.FieldByName('DOCUMENTO').AsString :=
      'ORDEM DE SERVI�O';

    AOrdemServicoParcela.FieldByName('VL_TITULO').AsFloat := valorParcela;
    AOrdemServicoParcela.FieldByName('QT_PARCELA').AsInteger := parcela.quantidadeParcelas;
    AOrdemServicoParcela.FieldByName('NR_PARCELA').AsInteger := parcela.numeroParcela;
    AOrdemServicoParcela.FieldByName('DT_VENCIMENTO').AsDateTime := parcela.dataParcela;

    if AIdFormaPagamento > 0 then
    begin
      formaPagamento := TFormaPagamento.GetFormaPagamento(AIdFormaPagamento);
      try
        AOrdemServicoParcela.FieldByName('ID_FORMA_PAGAMENTO').AsInteger :=
          formaPagamento.FId;
        AOrdemServicoParcela.FieldByName('JOIN_DESCRICAO_FORMA_PAGAMENTO').AsString :=
          formaPagamento.FDescricao;
        AOrdemServicoParcela.FieldByName('JOIN_TIPO_FORMA_PAGAMENTO').AsString :=
          formaPagamento.FTipoTipoQuitacao;
      finally
        FreeAndNil(formaPagamento);
      end;
    end;

    if AIdCarteira > 0 then
    begin
      carteira := TCarteira.GetCarteira(AIdCarteira);
      try
        AOrdemServicoParcela.FieldByName('ID_CARTEIRA').AsInteger :=
          carteira.FId;
        AOrdemServicoParcela.FieldByName('JOIN_DESCRICAO_CARTEIRA').AsString :=
          carteira.FDescricao;
      finally
        FreeAndNil(carteira);
      end;
    end;

    AOrdemServicoParcela.Salvar;

    totalParcelas := totalParcelas + valorParcela;
  end;

  diferencaNoParcelamento := valorTitulo - totalParcelas;

  if diferencaNoParcelamento <> 0 then
  begin
    AOrdemServicoParcela.Alterar;
    AOrdemServicoParcela.FieldByName('VL_TITULO').AsFloat :=
      AOrdemServicoParcela.FieldByName('VL_TITULO').AsFloat + diferencaNoParcelamento;
    AOrdemServicoParcela.Salvar;
  end;
end;

class function TOrdemServico.GetId(AIdChaveProcesso: Integer): Integer;
var smOrdemServico: TSMOrdemServicoClient;
begin
  smOrdemServico := TsmOrdemServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOrdemServico.GetId(AIdChaveProcesso);
  finally
    smOrdemServico.Free;
  end;
end;

class procedure TOrdemServico.LimparParcelas(
  AGeracaoDocumentoParcela: TgbClientDataset);
begin
  while not AGeracaoDocumentoParcela.IsEmpty do
    AGeracaoDocumentoParcela.Delete;
end;

class function TOrdemServico.OrdemServicoQuitada(AOrdemServico, AOrdemServicoParcela: TgbClientDataset): Boolean;
begin
  result := AOrdemServico.GetAsFloat('VL_TOTAL_LIQUIDO') -
    AOrdemServico.GetAsFloat('VL_PAGAMENTO') -
    ValorTotalParcelado(AOrdemServicoParcela) <= 0;
end;

class function TOrdemServico.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
var smOrdemServico: TSMOrdemServicoClient;
begin
  smOrdemServico := TsmOrdemServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOrdemServico.PodeEstornar(AIdChaveProcesso);
  finally
    smOrdemServico.Free;
  end;
end;

class function TOrdemServico.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var smOrdemServico: TSMOrdemServicoClient;
begin
  smOrdemServico := TsmOrdemServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOrdemServico.PodeExcluir(AIdChaveProcesso);
  finally
    smOrdemServico.Free;
  end;
end;

class function TOrdemServico.ValorTotalParcelado(AOrdemServicoParcela: TgbClientDataset): Double;
begin
  result := TDatasetUtils.SomarColuna(
    AOrdemServicoParcela.FieldByName('VL_TITULO'));
end;

end.
