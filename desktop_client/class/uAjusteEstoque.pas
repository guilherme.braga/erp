unit uAjusteEstoque;

interface

type TAjusteEstoque = class
  class function Efetivar(AIdChaveProcesso: Integer): Boolean;
  class function Cancelar(AIdChaveProcesso: Integer): Boolean;
  class function Reabrir(AIdChaveProcesso: Integer): Boolean;
end;

implementation

{ TAjusteEstoque }

uses uDmConnection, uClientClasses;

class function TAjusteEstoque.Cancelar(AIdChaveProcesso: Integer): Boolean;
var smAjusteEstoque: TSMAjusteEstoqueClient;
begin
  smAjusteEstoque := TsmAjusteEstoqueClient.Create(DmConnection.Connection.DBXConnection);
  try
    smAjusteEstoque.Cancelar(AIdChaveProcesso);
  finally
    smAjusteEstoque.Free;
  end;

end;

class function TAjusteEstoque.Efetivar(AIdChaveProcesso: Integer): Boolean;
var smAjusteEstoque: TSMAjusteEstoqueClient;
begin
  smAjusteEstoque := TsmAjusteEstoqueClient.Create(DmConnection.Connection.DBXConnection);
  try
    smAjusteEstoque.Efetivar(AIdChaveProcesso);
  finally
    smAjusteEstoque.Free;
  end;
end;

class function TAjusteEstoque.Reabrir(AIdChaveProcesso: Integer): Boolean;
var
  smAjusteEstoque: TSMAjusteEstoqueClient;
begin
  smAjusteEstoque := TsmAjusteEstoqueClient.Create(DmConnection.Connection.DBXConnection);
  try
    smAjusteEstoque.Reabrir(AIdChaveProcesso);
  finally
    smAjusteEstoque.Free;
  end;
end;

end.
