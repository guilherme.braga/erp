unit uLoteRecebimento;

interface

uses
  uLoteRecebimentoProxy,
  FireDAC.Comp.Client,
  Data.FireDACJSONReflect;

type TLoteRecebimento = class
  private

  public
    class function Efetivar(AIdLoteRecebimento: Integer): string;
    class function Reabrir(AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
    class procedure SetContasAReceber(AFDMemTable: TFDMemTable;
                                    AIdsContaReceber: string);
  private
    class function RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer) : string;

end;

implementation

uses
  uDmConnection,
  System.JSON,
  REST.Json,
  DBXJSONReflect,
  uClientClasses,
  System.SysUtils, uDatasetUtils;

{ TLoteRecebimento }

class procedure TLoteRecebimento.SetContasAReceber(AFDMemTable: TFDMemTable;
                                               AIdsContaReceber: string);
var
  smLote       : TSMLoteRecebimentoClient;
  JSONDataset  : TFDJSONDataSets;
begin
  smLote := TSMLoteRecebimentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smLote.GetContasAReceber(AIdsContaReceber);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smLote.Free;
  end;
end;

class function TLoteRecebimento.Efetivar(AIdLoteRecebimento: Integer): string;
var
  smLoteRecebimento : TSMLoteRecebimentoClient;
begin
  smLoteRecebimento := TSMLoteRecebimentoClient.Create(DmConnection.Connection.DBXConnection);
  Result := smLoteRecebimento.GerarLancamentosAgrupados(AIdLoteRecebimento)
end;

class function TLoteRecebimento.Reabrir(AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
var
  smLoteRecebimento : TSMLoteRecebimentoClient;
begin
  smLoteRecebimento := TSMLoteRecebimentoClient.Create(DmConnection.Connection.DBXConnection);
  Result := smLoteRecebimento.RemoverLancamentosAgrupados(AIdLoteRecebimento, AGerarContraPartida);
end;

class function TLoteRecebimento.RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
var
  smLoteRecebimento : TSMLoteRecebimentoClient;
begin
  smLoteRecebimento := TSMLoteRecebimentoClient.Create(DmConnection.Connection.DBXConnection);
  Result          := smLoteRecebimento.RemoverQuitacoesIndividuais(AIdLoteRecebimento);
end;

end.
