unit uRelatorioFRFiltroVerticalContaReceber;

interface

Uses uFrameFiltroVerticalPadrao;

type TRelatorioFRFiltroVerticalContaReceber = class
  class procedure CriarFiltrosRelatorioContaReceber(var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

uses uContaReceberProxy;

class procedure TRelatorioFRFiltroVerticalContaReceber.CriarFiltrosRelatorioContaReceber(
  var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Documento';
  campoFiltro.campo := 'DT_DOCUMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TContaReceberMovimento.ABERTO+';'+
  TContaReceberMovimento.QUITADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Centro de Resultado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Centro de Resultado';
  campoFiltro.campo := 'ID_CENTRO_RESULTADO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CENTRO_RESULTADO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Conta de Analise
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta de Analise';
  campoFiltro.campo := 'ID_CONTA_ANALISE';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_ANALISE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Carteira
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Carteira';
  campoFiltro.campo := 'ID_CARTEIRA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CARTEIRA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Per�odo de Quita��o (Dt de Recebimento)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Dt. Recebimento';
  campoFiltro.campo := 'DT_QUITACAO';
  campoFiltro.tabela := 'CONTA_RECEBER_QUITACAO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);
end;

end.

