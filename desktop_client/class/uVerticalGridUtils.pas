unit uVerticalGridUtils;

interface

Uses Classes, DB, cxVGrid, cxMaskEdit, cxTextEdit, StdCtrls,
  cxCalendar, cxCalc, cxButtonEdit, Variants, cxDBVGrid,
  cxGridDBBandedTableView, cxCurrencyEdit, cxCheckBox,
  dxTabbedMDI, cxGridExportLink, ShellAPI, vcl.Dialogs, SysUtils, Windows,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, cxExportVGLink;

type TVerticalGridUtils = class
  class procedure setEditorRowsFromDataset(const vg: TcxVerticalGrid; const vgCategory: TcxCategoryRow; const ds: TDataset; lDefaultConfiguration: Boolean = true; lUsarCaption: Boolean = false);
  class procedure setConfigurationOfType(const fieldType: TFieldType; const vgRow: TcxEditorRow);
  class function RowisNull(const vgRow: TcxEditorRow): Boolean;
  class procedure setDefaultValue(const vgRow: TcxEditorRow);
  class procedure PersonalizarColunaParaButtonEdit(const vgRow: TcxEditorRow);
  class function getWhereFromCategoryRows(const vgCategory: TcxCategoryRow; const ACommandText: WideString; const ALike: Boolean): String;

  class procedure AdicionarTodosCamposNaView(AView: TcxDBVerticalGrid);
  class function ExportarParaExcel(AGrid: TcxDBVerticalGrid;
    ANomeArquivoSugerido: String = ''; AExpandido: Boolean = true): Boolean;
end;


implementation

Uses uTFunction;

class procedure TVerticalGridUtils.setDefaultValue(const vgRow: TcxEditorRow);
begin
  TcxEditorRow(vgRow).Properties.Value := '';
end;

class procedure TVerticalGridUtils.PersonalizarColunaParaButtonEdit(
  const vgRow: TcxEditorRow);
begin
  vgRow.Properties.DataBinding.ValueType := 'LargeInt';
  vgRow.Properties.EditPropertiesClass := TcxButtonEditProperties;
  TcxButtonEditProperties(vgRow.Properties.EditProperties).MaskKind := emkRegExpr;
  TcxButtonEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
  TcxButtonEditProperties(vgRow.Properties.EditProperties).EditMask := '[0-9]+';
  //vgRow.Properties.Value := null;
end;

class function TVerticalGridUtils.RowisNull(const vgRow: TcxEditorRow): Boolean;
begin
  result := VartoStr(TcxEditorRow(vgRow).Properties.Value) = '';
end;

{Fun��o que realiza a leitura em uma categoria, e abstrai o Caption do Row e seu Valor
         C�digo|<'1'>|Cidade|<'PARANAVA�'>|Estado|<'678'>|Pa�s|<'BRASIL'>
}
class procedure TVerticalGridUtils.AdicionarTodosCamposNaView(AView: TcxDBVerticalGrid);
begin
  AView.ClearRows;
  AView.DataController.CreateAllItems();
end;

class function TVerticalGridUtils.ExportarParaExcel(AGrid: TcxDBVerticalGrid; ANomeArquivoSugerido: String;
  AExpandido: Boolean): Boolean;
var
  caixaDialogo: TSaveDialog;
begin
  Result := false;

  if ANomeArquivoSugerido = '' then
  begin
    ANomeArquivoSugerido := 'ArquivoExportado.xls';
  end;

  caixaDialogo := TSaveDialog.Create(nil);
  try
    caixaDialogo.FileName := ANomeArquivoSugerido;
    caixaDialogo.InitialDir := GetCurrentDir;
    caixaDialogo.Filter := 'Arquivos Excel (*.xlsx)|*.xlsx';

    if caixaDialogo.Execute then
    begin
      cxExportVGToXLSX(caixaDialogo.FileName, AGrid);
      Result := true;
    end;
  finally
    FreeAndNil(caixaDialogo);
  end;

end;

class function TVerticalGridUtils.getWhereFromCategoryRows(const vgCategory: TcxCategoryRow; const ACommandText: WideString; const ALike: Boolean): String;
var i: Integer;
    cWhere: String;
begin
  cWhere := '';

  if vgCategory.HasChildren then
    for i := 0 to Pred(vgCategory.Count) do
      begin
        if not RowisNull(TcxEditorRow(vgCategory.Rows[i])) then
          cWhere := cWhere
            +TFunction.IIF(cWhere = '', '', ' AND ')+TFunction.getFieldByName(ACommandText,TcxEditorRow(vgCategory.Rows[i]).Properties.Caption)+
            TFunction.IIF(ALike, ' CONTAINING('+QuotedStr(String((TcxEditorRow(vgCategory.Rows[i]).Properties.Value)))+') ',
                                 ' = '+QuotedStr(String((TcxEditorRow(vgCategory.Rows[i]).Properties.Value))));
      end;

  result := cWhere;
end;

class procedure TVerticalGridUtils.setConfigurationOfType(const fieldType: TFieldType; const vgRow: TcxEditorRow);
begin
  case fieldType of
    ftLargeInt:
      begin
        vgRow.Properties.DataBinding.ValueType := 'LargeInt';
        vgRow.Properties.EditPropertiesClass := TcxButtonEditProperties;
        TcxButtonEditProperties(vgRow.Properties.EditProperties).MaskKind := emkRegExpr;
        TcxButtonEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        TcxButtonEditProperties(vgRow.Properties.EditProperties).EditMask := '[0-9]+';
        //vgRow.Properties.Value := null;
      end;
    ftInteger:
      begin
        vgRow.Properties.DataBinding.ValueType := 'Integer';
        vgRow.Properties.EditPropertiesClass := TcxMaskEditProperties;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).MaskKind := emkRegExpr;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).EditMask := '[0-9]+';
        //vgRow.Properties.Value := null;
      end;
    ftSmallint:
      begin
        vgRow.Properties.DataBinding.ValueType := 'Smallint';
        vgRow.Properties.EditPropertiesClass := TcxMaskEditProperties;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).MaskKind := emkRegExpr;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        TcxMaskEditProperties(vgRow.Properties.EditProperties).EditMask := '[0-9]+';
        //vgRow.Properties.Value := null;
      end;
    ftWideString:
      begin
        vgRow.Properties.DataBinding.ValueType := 'WideString';
        vgRow.Properties.EditPropertiesClass := TcxTextEditProperties;
        TcxTextEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        vgRow.Properties.Value := '';
      end;
    ftString:
      begin
        vgRow.Properties.DataBinding.ValueType := 'String';
        vgRow.Properties.EditPropertiesClass := TcxTextEditProperties;
        TcxTextEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        vgRow.Properties.Value := '';
      end;
    ftDate:
      begin
        vgRow.Properties.DataBinding.ValueType := 'DateTime';
        vgRow.Properties.EditPropertiesClass := TcxDateEditProperties;
        TcxDateEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        TcxDateEditProperties(vgRow.Properties.EditProperties).Kind := ckDate;
        TcxDateEditProperties(vgRow.Properties.EditProperties).ShowTime := false;
        TcxDateEditProperties(vgRow.Properties.EditProperties).SaveTime := false;
        TcxDateEditProperties(vgRow.Properties.EditProperties).DateButtons := [btnClear, btnToday];
        TcxDateEditProperties(vgRow.Properties.EditProperties).ImmediatePost := true;
        //vgRow.Properties.Value := 0;
      end;
    ftDateTime:
      begin
        vgRow.Properties.DataBinding.ValueType := 'DateTime';
        vgRow.Properties.EditPropertiesClass := TcxDateEditProperties;
        TcxDateEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
        TcxDateEditProperties(vgRow.Properties.EditProperties).Kind := ckDateTime;
        TcxDateEditProperties(vgRow.Properties.EditProperties).ShowTime := true;
        TcxDateEditProperties(vgRow.Properties.EditProperties).SaveTime := true;
        TcxDateEditProperties(vgRow.Properties.EditProperties).DateButtons := [btnClear, btnNow];
        TcxDateEditProperties(vgRow.Properties.EditProperties).ImmediatePost := true;
        //vgRow.Properties.Value := 0;
      end;
    ftBoolean:
      begin
        vgRow.Properties.DataBinding.ValueType := 'Boolean';
        vgRow.Properties.Value := false;
      end;
    ftFloat, ftCurrency, ftBCD, ftFMTBcd:
      begin
        vgRow.Properties.DataBinding.ValueType := 'Float';
        vgRow.Properties.EditPropertiesClass := TcxCalcEditProperties;
        //vgRow.Properties.Value := null;
      end;
  else
    begin
      vgRow.Properties.DataBinding.ValueType := 'String';
      vgRow.Properties.EditPropertiesClass := TcxTextEditProperties;
      TcxTextEditProperties(vgRow.Properties.EditProperties).CharCase := ecUpperCase;
      vgRow.Properties.Value := '';
    end;
  end;
end;

//Configura os campos do cxVerticalGrid de acordo com o tipo especifico do TField
class procedure TVerticalGridUtils.setEditorRowsFromDataset(const vg: TcxVerticalGrid; const vgCategory: TcxCategoryRow; const ds: TDataset; lDefaultConfiguration: Boolean = true; lUsarCaption: Boolean = false);
var vgRow: TcxEditorRow;
    i: Integer;
begin
  for i := 0 to Pred(ds.FieldCount) do
  begin
    if not(ds.Fields[i].ReadOnly) and not(ds.Fields[i] is TDatasetField) and
       not (ds.Fields[i].DataType = ftAutoInc) and (ds.Fields[i].Tag = 0) then
    begin
      vgRow := vg.AddChild(vgCategory, TcxEditorRow) as TcxEditorRow;

      if lUsarCaption then
      begin
        vgRow.Properties.Caption := ds.Fields[i].DisplayLabel;
        vgRow.Properties.Hint := ds.Fields[i].FieldName;
      end
      else
        vgRow.Properties.Caption := ds.Fields[i].FieldName;

      if lDefaultConfiguration then
        setConfigurationOfType(ds.Fields[i].DataType, vgRow);
    end;
  end;
end;

end.
