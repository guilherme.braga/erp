unit uInventario;

interface

type TInventario = class
  class function Efetivar(AIdChaveProcesso: Integer): Boolean;
  class function Reabrir(AIdChaveProcesso: Integer): Boolean;
end;

implementation

uses uInventarioProxy, uClientClasses, uDmConnection;

class function TInventario.Efetivar(AIdChaveProcesso: Integer): Boolean;
var smInventario: TSMInventarioClient;
begin
  smInventario := TsmInventarioClient.Create(DmConnection.Connection.DBXConnection);
  try
    smInventario.Efetivar(AIdChaveProcesso);
  finally
    smInventario.Free;
  end;
end;

class function TInventario.Reabrir(AIdChaveProcesso: Integer): Boolean;
var
  smInventario: TSMInventarioClient;
begin
  smInventario := TsmInventarioClient.Create(DmConnection.Connection.DBXConnection);
  try
    smInventario.Reabrir(AIdChaveProcesso);
  finally
    smInventario.Free;
  end;
end;

end.
