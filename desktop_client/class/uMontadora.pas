unit uMontadora;

interface

Uses uMontadoraProxy, SysUtils;

type TMontadora = class
  private
    class function GerarMontadora(const AMontadora: TMontadoraProxy): Integer;
    class function ExisteMontadora(const ADescricaoMontadora: String): Boolean;
  public
    class function GetMontadora(const AIdMontadora: Integer): TMontadoraProxy;
    class function InserirMontadoraCasoNaoExista(const ADescricaoMontadora: String): Boolean;
    class function GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
end;

type TModeloMontadora = class
  private
    class function GerarModeloMontadora(const AModeloMontadora: TModeloMontadoraProxy): Integer;
    class function ExisteModelo(const ADescricaoModelo: String; const ADescricaoMontadora: String): Boolean;
  public
    class function GetModeloMontadora(const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
    class function InserirModeloMontadoraCasoNaoExista(const ADescricaoModelo: String;
      const ADescricaoMontadora: String): Boolean;
end;

implementation

uses uClientClasses, uDmConnection;

{ TModeloMontadora }

class function TModeloMontadora.ExisteModelo(const ADescricaoModelo, ADescricaoMontadora: String): Boolean;
var smModeloMontadora: TSMModeloMontadoraClient;
begin
  smModeloMontadora := TSMModeloMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smModeloMontadora.ExisteModelo(ADescricaoModelo,
      ADescricaoMontadora);
  finally
    smModeloMontadora.Free;
  end;
end;

class function TModeloMontadora.GerarModeloMontadora(const AModeloMontadora: TModeloMontadoraProxy): Integer;
var smModeloMontadora: TSMModeloMontadoraClient;
begin
  smModeloMontadora := TSMModeloMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smModeloMontadora.GerarModeloMontadora(AModeloMontadora);
  finally
    smModeloMontadora.Free;
  end;
end;

class function TModeloMontadora.GetModeloMontadora(const AIdModeloMontadora: Integer): TModeloMontadoraProxy;
var smModeloMontadora: TSMModeloMontadoraClient;
begin
  smModeloMontadora := TSMModeloMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smModeloMontadora.GetModeloMontadora(AIdModeloMontadora);
  finally
    //smModeloMontadora.Free;
  end;
end;

class function TModeloMontadora.InserirModeloMontadoraCasoNaoExista(const ADescricaoModelo: String;
  const ADescricaoMontadora: String): Boolean;
var
  modelo: TModeloMontadoraProxy;
  idMontadora: Integer;
begin
  result := false;
  if not TModeloMontadora.ExisteModelo(ADescricaoModelo, ADescricaoMontadora) then
  begin
    idMontadora := TMontadora.GetIdMontadoraPorDescricao(ADescricaoMontadora);

    if idMontadora <= 0 then
    begin
      exit;
    end;

    modelo := TModeloMontadoraProxy.Create;
    try
      modelo.FDescricao := ADescricaoModelo;
      modelo.FIdMontadora := idMontadora;
      TModeloMontadora.GerarModeloMontadora(modelo);
      result := true;
    finally
      //FreeAndNil(modelo);
    end;
  end;
end;

{ TMontadora }

class function TMontadora.ExisteMontadora(const ADescricaoMontadora: String): Boolean;
var smMontadora: TSMMontadoraClient;
begin
  smMontadora := TSMMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMontadora.ExisteMontadora(ADescricaoMontadora);
  finally
    smMontadora.Free;
  end;
end;

class function TMontadora.GerarMontadora(const AMontadora: TMontadoraProxy): Integer;
var smMontadora: TSMMontadoraClient;
begin
  smMontadora := TSMMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMontadora.GerarMontadora(AMontadora);
  finally
    smMontadora.Free;
  end;
end;

class function TMontadora.GetIdMontadoraPorDescricao(const ADescricaoMontadora: String): Integer;
var
  smMontadora: TSMMontadoraClient;
begin
  smMontadora := TSMMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMontadora.GetIdMontadoraPorDescricao(ADescricaoMontadora);
  finally
    smMontadora.Free;
  end;
end;

class function TMontadora.GetMontadora(const AIdMontadora: Integer): TMontadoraProxy;
var smMontadora: TSMMontadoraClient;
begin
  smMontadora := TSMMontadoraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smMontadora.GetMontadora(AIdMontadora);
  finally
    //smMontadora.Free;
  end;
end;

class function TMontadora.InserirMontadoraCasoNaoExista(const ADescricaoMontadora: String): Boolean;
var
  montadora: TMontadoraProxy;
begin
  result := false;
  if not TMontadora.ExisteMontadora(ADescricaoMontadora) then
  begin
    montadora := TMontadoraProxy.Create;
    try
      montadora.FDescricao := ADescricaoMontadora;
      TMontadora.GerarMontadora(montadora);
      result := true;
    finally
      //FreeAndNil(montadora);
    end;
  end;
end;

end.
