unit uServico;

interface

Uses uServicoProxy;

type TServico = class
  class function GetServico(const AIdServico: Integer): TServicoProxy;
  class function GetDescricao(const AIdServico: Integer): String;
  class function GerarServico(const AServico: TServicoProxy): Integer;
  class function ExisteServico(const ADescricaoServico: String): Boolean;
  class function ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
  class function GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
  class function GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
end;

implementation

uses uClientClasses, uDmConnection, REST.JSON;

class function TServico.ExisteServico(const ADescricaoServico: String): Boolean;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.ExisteServico(ADescricaoServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.ExisteServicoTerceiro(const ADescricaoServico: String): Boolean;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.ExisteServicoTerceiro(ADescricaoServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.GerarServico(const AServico: TServicoProxy): Integer;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.GerarServico(AServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.GetDescricao(const AIdServico: Integer): String;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.GetDescricao(AIdServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.GetIdServicoPorDescricao(const ADescricaoServico: String): Integer;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.GetIdServicoPorDescricao(ADescricaoServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.GetIdServicoTerceiroPorDescricao(const ADescricaoServico: String): Integer;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smServico.GetIdServicoTerceiroPorDescricao(ADescricaoServico);
  finally
    smServico.Free;
  end;
end;

class function TServico.GetServico(const AIdServico: Integer): TServicoProxy;
var
  smServico : TSMServicoClient;
begin
  smServico := TSMServicoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TServicoProxy>(smServico.GetServico(AIdServico));
  finally
    smServico.Free;
  end;
end;

end.
