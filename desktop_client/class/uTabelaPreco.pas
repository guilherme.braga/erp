unit uTabelaPreco;

interface

Uses db, FireDAC.Comp.Client, Data.FireDACJSONReflect, uProdutoProxy, rest.json, uTabelaPrecoProxy,
  uClientClasses, Forms, SysUtils, windows;

type TTabelaPreco = class
  class
    var InstanciaServerMethod: TSMTabelaPrecoClient;
    class destructor Destroy;
  public
  class function ServerMethod: TSMTabelaPrecoClient;
  class procedure AtualizarRegistro(AIdTabelaPreco, AIdProduto: Integer; AVlCusto, APercLucro, AVlVenda: Double);
  class function GetTabelaPrecoComMaisProdutos: TFDMemTable;
  class function GetProdutosTabelaPreco(const AIdTabelaPreco: Integer): TFDMemTable;
  class function GetDescricaoTabelaPreco(const AID: Integer): String;
  class function GetProdutoTabelaPreco(const AIdTabelaPreco,
    AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
  class function BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDMemTable;
  class function GetTabelaPrecoItem(
    const AIdTabelaPreco, AIdProduto: Integer): TTabelaPrecoItemProxy;
  class function ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
end;

implementation

{ TTabelaPreco }

uses uDmConnection, uDatasetUtils;

class procedure TTabelaPreco.AtualizarRegistro(AIdTabelaPreco, AIdProduto: Integer;
  AVlCusto, APercLucro, AVlVenda: Double);
var
  smTabelaPreco: TSMTabelaPrecoClient;
begin
  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    smTabelaPreco.AtualizarRegistro(AIdTabelaPreco, AIdProduto, AVlCusto,
      APercLucro, AVlVenda);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDMemTable;
var
  smTabelaPreco : TSMTabelaPrecoClient;
begin
  Result := TFDMemTable.Create(nil);
  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    TDatasetUtils.JSONDatasetToMemTable(smTabelaPreco.BuscarProdutoEmSuasTabelasPreco(AIdProduto), Result);
  finally
    smTabelaPreco.Free;
  end;
end;

class destructor TTabelaPreco.Destroy;
begin
  if not Assigned(TTabelaPreco.InstanciaServerMethod) then
    exit;

  if TTabelaPreco.InstanciaServerMethod <> nil then
  begin
    TTabelaPreco.InstanciaServerMethod.Free;
  end;
end;

class function TTabelaPreco.ExcluirTabelaDePreco(const AIdTabelaPreco: Integer): Boolean;
var
  smTabelaPreco : TSMTabelaPrecoClient;
begin
  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smTabelaPreco.ExcluirTabelaDePreco(AIdTabelaPreco);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.GetDescricaoTabelaPreco(const AID: Integer): String;
var
  smTabelaPreco : TSMTabelaPrecoClient;
begin
  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smTabelaPreco.GetDescricaoTabelaPreco(AID);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.GetProdutosTabelaPreco(
  const AIdTabelaPreco: Integer): TFDMemTable;
var
  smTabelaPreco : TSMTabelaPrecoClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smTabelaPreco.GetProdutosTabelaPreco(AIdTabelaPreco);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.GetProdutoTabelaPreco(const AIdTabelaPreco,
  AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
begin
  result := ServerMethod.GetProdutoTabelaPreco(AIdTabelaPreco, AIdProduto, AIdFilial);
end;

class function TTabelaPreco.GetTabelaPrecoComMaisProdutos: TFDMemTable;
var
  smTabelaPreco : TSMTabelaPrecoClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smTabelaPreco.GetTabelaPrecoComMaisProdutos();
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.GetTabelaPrecoItem(const AIdTabelaPreco,
  AIdProduto: Integer): TTabelaPrecoItemProxy;
var
  smTabelaPreco : TSMTabelaPrecoClient;
begin
  result := TTabelaPrecoItemProxy.Create;

  smTabelaPreco := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smTabelaPreco.GetTabelaPrecoItem(AIdTabelaPreco, AIdProduto);
  finally
    smTabelaPreco.Free;
  end;
end;

class function TTabelaPreco.ServerMethod: TSMTabelaPrecoClient;
begin
  if TTabelaPreco.InstanciaServerMethod = nil then
  begin
    TTabelaPreco.InstanciaServerMethod := TSMTabelaPrecoClient.Create(DmConnection.Connection.DBXConnection);
  end;
  Result := TTabelaPreco.InstanciaServerMethod;
end;

end.
