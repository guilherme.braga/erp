unit uProduto;

interface

uses uProdutoProxy, db, FireDAC.Comp.Client, Data.FireDACJSONReflect, dialogs, Generics.Collections,
  uClientClasses;

Type TProdutoEtiqueta = class
  class procedure ImprimirEtiquetaArgox(AFieldIDProduto: TField; AQtdeProduto: Integer; AFieldQuantidade: TField = nil);
end;

type TProduto = class
    private
      class var InstanciaServerMethod: TSMProdutoClient;
      class destructor Destroy;
    public
    class function ServerMethod: TSMProdutoClient;
    class procedure PopularTabelasPreco(ADatasetTabelaPreco: TDataset; AIdproduto: Integer;
      AValorCustoInicial: Double);
    class procedure GerarMovimentacaoProduto
      (AProdutoEstoque: TProdutoMovimento);
    class function RemoverMovimentacaoProduto(AIdChaveProcesso: Integer): Boolean;
    class function RemoverMovimentacaoProdutoInicial(AIdProduto: Integer): Boolean;
    class function NovoCodigoBarra(): String;
    class function GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
    class function GetProximoID: Integer;
    class function CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
      const AIdProduto: Integer): Integer;
    class function PegarProdutosAtivosComPrecoDeCustoEVenda: TFDMemTable;
    class function GetProduto(AIdProduto: Integer): TFDMemTable;
    class function GetProdutoFilialProxy(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
    class function GetProdutoPeloCodigoBarra(ACodigoBarraProduto: String): TFDMemTable;
    class procedure SetProdutos(AFDMemTable: TFDMemTable; AIdsProdutos: string);
    class procedure SetProdutosCompleto(AFDMemTable: TFDMemTable;
      AIdsProdutos: string);
    class function GetProdutoFilial(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
    class function GetProdutoProxyJSON(const AIdProduto, AIdFilial: Integer): String;
    class function GetListaProduto(const AIdsProdutos: String; const AIdFilial: Integer): TList<TProdutoProxy>;
    class procedure GerarMovimentacaoProdutoInicial(ADatasetProduto, ADatasetMovimentacao: TDataset);
    class procedure BuscarMovimentacaoProdutoInicial(ADatasetMovimentacao: TDataset; AIdProduto, AIdFilial: Integer);
    class function ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto, AIdUsuario: Integer): Boolean;
    class function DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
    class function GerarProdutos(const AProdutosEmGrade: String): Boolean;
    class procedure SetVisaoProdutoGrade(const AIdGradeProduto, AIdProdutoAgrupador: Integer; fdmProdutoGrade: TFDMemTable);
    class function GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
    class procedure RefazerEAN13TodosProdutos;
end;

implementation

{ TProduto }

uses
  uDmConnection,
  System.JSON,
  REST.Json,
  DBXJSONReflect,
  System.SysUtils,
  uDatasetUtils,
  ACBrDevice,
  ACBrETQ, uTVariables, uIniFileClient, uTUsuario, uSistema, uChaveProcesso, uChaveProcessoProxy,
  uTabelaPreco;

class procedure TProduto.BuscarMovimentacaoProdutoInicial(ADatasetMovimentacao: TDataset;
  AIdProduto, AIdFilial: Integer);
var
  produto: TProdutoProxy;
begin
  produto := TProduto.GetProdutoFilial(AIdProduto, AIdFilial);
  try
    ADatasetMovimentacao.Append;

    ADatasetMovimentacao.FieldByName('VL_ULTIMO_CUSTO').AsFloat := produto.FProdutoFilial.FVlUltimoCusto;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_FRETE').AsFloat := produto.FProdutoFilial.FVlUltimoFrete;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_SEGURO').AsFloat := produto.FProdutoFilial.FVlUltimoSeguro;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_ICMS_ST').AsFloat := produto.FProdutoFilial.FVlUltimoIcmsSt;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_IPI').AsFloat := produto.FProdutoFilial.FVlUltimoIpi;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_DESCONTO').AsFloat := produto.FProdutoFilial.FVlUltimoDesconto;
    ADatasetMovimentacao.FieldByName('VL_ULTIMA_DESPESA_ACESSORIA').AsFloat :=
      produto.FProdutoFilial.FVlUltimaDespesaAcessoria;
    ADatasetMovimentacao.FieldByName('VL_ULTIMO_CUSTO_IMPOSTO').AsFloat :=
      produto.FProdutoFilial.FVlUltimoCustoImposto;
    ADatasetMovimentacao.FieldByName('VL_CUSTO_OPERACIONAL').AsFloat := produto.FProdutoFilial.FVlCustoOperacional;
    ADatasetMovimentacao.FieldByName('VL_CUSTO_IMPOSTO_OPERACIONAL').AsFloat :=
      produto.FProdutoFilial.FVlCustoImpostoOperacional;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMO_FRETE').AsFloat := produto.FProdutoFilial.FPercUltimoFrete;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMO_SEGURO').AsFloat := produto.FProdutoFilial.FPercUltimoSeguro;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMO_ICMS_ST').AsFloat := produto.FProdutoFilial.FPercUltimoIcmsSt;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMO_IPI').AsFloat := produto.FProdutoFilial.FPercUltimoIpi;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMO_DESCONTO').AsFloat := produto.FProdutoFilial.FPercUltimoDesconto;
    ADatasetMovimentacao.FieldByName('PERC_ULTIMA_DESPESA_ACESSORIA').AsFloat :=
      produto.FProdutoFilial.FPercUltimaDespesaAcessoria;
    ADatasetMovimentacao.FieldByName('PERC_CUSTO_OPERACIONAL').AsFloat := produto.FProdutoFilial.FPercCustoOperacional;

    if ADatasetMovimentacao.State in dsEditModes then
      ADatasetMovimentacao.Post;
  finally
    FreeAndNil(produto);
  end;
end;

class function TProduto.CodigoDeBarraJaCadastrado(const ACodigoBarra: String;
const AIdProduto: Integer): Integer;
var smProduto: TSMProdutoClient;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smProduto.CodigoDeBarraJaCadastrado(ACodigoBarra, AIdProduto);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.ServerMethod: TSMProdutoClient;
begin
  if TProduto.InstanciaServerMethod = nil then
  begin
    TProduto.InstanciaServerMethod := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  end;
  Result := TProduto.InstanciaServerMethod;
end;

class destructor TProduto.Destroy;
begin
  if not Assigned(TProduto.InstanciaServerMethod) then
    exit;

  if TProduto.InstanciaServerMethod <> nil then
  begin
    TProduto.InstanciaServerMethod.Free;
  end;
end;

class function TProduto.DuplicarProduto(const AIdProdutoOrigem: Integer): Integer;
begin
  result := ServerMethod.DuplicarProduto(AIdProdutoOrigem);
end;

class function TProduto.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto, AIdUsuario: Integer): Boolean;
var
  smProduto: TSMProdutoClient;
begin

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smProduto.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto, AIdUsuario);
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.GerarMovimentacaoProduto(
  AProdutoEstoque: TProdutoMovimento);
var
  smProduto            : TSMProdutoClient;
  produtoMovimentoJson : String;
begin

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    produtoMovimentoJson := TJSON.ObjectToJsonString(AProdutoEstoque);
    smProduto.GerarMovimentacaoProduto(produtoMovimentoJson);
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.GerarMovimentacaoProdutoInicial(ADatasetProduto, ADatasetMovimentacao: TDataset);
var
  produtoMovimento: TProdutoMovimento;
begin
  TProduto.RemoverMovimentacaoProdutoInicial(ADatasetProduto.FieldByName('ID').AsInteger);

  produtoMovimento := TProdutoMovimento.Create;
  try
    produtoMovimento.FTipo := TProdutoMovimento.TIPO_MOVIMENTO_ENTRADA;
    produtoMovimento.FDtMovimento := DatetoStr(Date);
    produtoMovimento.FQtMovimento := 0;
    produtoMovimento.FDocumentoOrigem := TProdutoFilialProxy.DOCUMENTO_ORIGEM_FORMACAO_PRECO_INICIAL;
    produtoMovimento.FIdProduto := ADatasetProduto.FieldByName('ID').AsInteger;
    produtoMovimento.FIdFilial := TSistema.Sistema.Filial.FId;
    produtoMovimento.FIdChaveProcesso := TChaveProcesso.NovaChaveProcesso(
      TChaveProcessoProxy.ORIGEM_FORMACAO_PRECO_INICIAL, ADatasetProduto.FieldByName('ID').AsInteger);
    produtoMovimento.FVlUltimoCusto := ADatasetMovimentacao.FieldByName('VL_ULTIMO_CUSTO').AsFloat;
    produtoMovimento.FQtEstoqueCondicional := 0;
    produtoMovimento.FVlUltimoFrete := ADatasetMovimentacao.FieldByName('VL_ULTIMO_FRETE').AsFloat;
    produtoMovimento.FVlUltimoSeguro := ADatasetMovimentacao.FieldByName('VL_ULTIMO_SEGURO').AsFloat;
    produtoMovimento.FVlUltimoIcmsSt := ADatasetMovimentacao.FieldByName('VL_ULTIMO_ICMS_ST').AsFloat;
    produtoMovimento.FVlUltimoIpi := ADatasetMovimentacao.FieldByName('VL_ULTIMO_IPI').AsFloat;
    produtoMovimento.FVlUltimoDesconto := ADatasetMovimentacao.FieldByName('VL_ULTIMO_DESCONTO').AsFloat;
    produtoMovimento.FVlUltimaDespesaAcessoria := ADatasetMovimentacao.FieldByName('VL_ULTIMA_DESPESA_ACESSORIA').AsFloat;
    produtoMovimento.FVlCustoOperacional := ADatasetMovimentacao.FieldByName('VL_CUSTO_OPERACIONAL').AsFloat;
    produtoMovimento.FPercUltimoFrete := ADatasetMovimentacao.FieldByName('PERC_ULTIMO_FRETE').AsFloat;
    produtoMovimento.FPercUltimoSeguro := ADatasetMovimentacao.FieldByName('PERC_ULTIMO_SEGURO').AsFloat;
    produtoMovimento.FPercUltimoIcmsSt := ADatasetMovimentacao.FieldByName('PERC_ULTIMO_ICMS_ST').AsFloat;
    produtoMovimento.FPercUltimoIpi := ADatasetMovimentacao.FieldByName('PERC_ULTIMO_IPI').AsFloat;
    produtoMovimento.FPercUltimoDesconto := ADatasetMovimentacao.FieldByName('PERC_ULTIMO_DESCONTO').AsFloat;
    produtoMovimento.FPercUltimaDespesaAcessoria := ADatasetMovimentacao.FieldByName('PERC_ULTIMA_DESPESA_ACESSORIA').AsFloat;
    produtoMovimento.FPercCustoOperacional := ADatasetMovimentacao.FieldByName('PERC_CUSTO_OPERACIONAL').AsFloat;

    TProduto.GerarMovimentacaoProduto(produtoMovimento);
  finally
    FreeAndNil(produtoMovimento);
  end;
end;

class function TProduto.GerarProdutos(const AProdutosEmGrade: String): Boolean;
begin
  result := ServerMethod.GerarProdutos(AProdutosEmGrade);
end;

class function TProduto.GerarCodigoBarraEAN13(const ACodigoProduto: Integer): String;
begin
  result := ServerMethod.GerarCodigoBarraEAN13(ACodigoProduto);
end;

class function TProduto.NovoCodigoBarra(): String;
begin
  result := GerarCodigoBarraEAN13(GetProximoID);
end;

class function TProduto.GetProduto(AIdProduto: Integer): TFDMemTable;
var
  smProduto : TSMProdutoClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smProduto.GetProduto(AIdProduto);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.GetProdutoFilial(const AIdProduto,
  AIdFilial: Integer): TProdutoProxy;
var
  smProduto : TSMProdutoClient;
  produtoProxy: String;
begin
  result := TProdutoProxy.Create;
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    produtoProxy := smProduto.GetProdutoFilial(AIdProduto, AIdFilial);
  finally
    smProduto.Free;
  end;
  result := TJSON.JsonToObject<TProdutoProxy>(produtoProxy);
end;

class function TProduto.GetProdutoFilialProxy(const AIdProduto, AIdFilial: Integer): TProdutoProxy;
begin
  result := TProdutoProxy.Create;
  result := ServerMethod.GetProdutoFilialProxy(AIdProduto, AIdFilial);
end;

class function TProduto.GetProdutoFiscal(const AIdProduto, AIdPessoa, AFilial: Integer): TProdutoFiscalProxy;
var
  smProduto : TSMProdutoClient;
begin
  result := TProdutoFiscalProxy.Create;

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smProduto.GetProdutoFiscal(AIdProduto, AIdPessoa, AFilial);
  finally
    //smProduto.Free;
  end;
end;

class function TProduto.GetListaProduto(const AIdsProdutos: String;
  const AIdFilial: Integer): TList<TProdutoProxy>;
var
  smProduto : TSMProdutoClient;
begin
  result := TList<TProdutoProxy>.Create;

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TList<TProdutoProxy>>(
      smProduto.GetListaProduto(AIdsProdutos, AIdFilial));
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.SetProdutos(AFDMemTable: TFDMemTable;
                                     AIdsProdutos: string);
var
  smProduto    : TSMProdutoClient;
  JSONDataset  : TFDJSONDataSets;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smProduto.GetProdutos(AIdsProdutos);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.SetProdutosCompleto(AFDMemTable: TFDMemTable;
                                     AIdsProdutos: string);
var
  smProduto    : TSMProdutoClient;
  JSONDataset  : TFDJSONDataSets;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smProduto.GetProdutosCompleto(AIdsProdutos);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.SetVisaoProdutoGrade(const AIdGradeProduto, AIdProdutoAgrupador: Integer; fdmProdutoGrade: TFDMemTable);
var
  smProduto : TSMProdutoClient;
  JSONDataset: TFDJSONDataSets;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smProduto.VisaoProdutoGrade(AIdGradeProduto, AIdProdutoAgrupador);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, fdmProdutoGrade);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.GetProdutoPeloCodigoBarra(ACodigoBarraProduto: String): TFDMemTable;
var
  smProduto : TSMProdutoClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smProduto.GetProdutoPeloCodigoBarra(ACodigoBarraProduto);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.GetProdutoProxyJSON(const AIdProduto, AIdFilial: Integer): String;
var
  smProduto : TSMProdutoClient;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smProduto.GetProdutoFilial(AIdProduto, AIdFilial);
    //produtoProxy := DmConnection.SMProdutoClient.GetProdutoFilial(AIdProduto, AIdFilial);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.GetProximoID: Integer;
var
  smProduto: TSMProdutoClient;
begin
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smProduto.GetProximoID;  //Atualizar o produto ap�s inserir o registro
  finally
    smProduto.Free;
  end;
end;

class procedure TProduto.RefazerEAN13TodosProdutos;
begin
  ServerMethod.RefazerEAN13TodosProdutos;
end;

class function TProduto.RemoverMovimentacaoProduto(AIdChaveProcesso: Integer) : Boolean;
var
  smProduto : TSMProdutoClient;
begin
  Result := False;
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smProduto.RemoverMovimentacaoProduto(AIdChaveProcesso);
  finally
    smProduto.Free;
  end;
end;

class function TProduto.RemoverMovimentacaoProdutoInicial(AIdProduto: Integer): Boolean;
var
  smProduto : TSMProdutoClient;
begin
  Result := False;
  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smProduto.RemoverMovimentacaoProduto(
      smProduto.GetChaveProcessoMovimentacaoInicial(AIdProduto));
  finally
    smProduto.Free;
  end;
end;

class function TProduto.PegarProdutosAtivosComPrecoDeCustoEVenda: TFDMemTable;
var
  smProduto : TSMProdutoClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TFDMemTable.Create(nil);

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
//    JSONDataset := smProduto.PegarProdutosAtivosComPrecoDeCustoEVenda();
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smProduto.Free;
  end;
end;


class procedure TProduto.PopularTabelasPreco(ADatasetTabelaPreco: TDataset; AIdproduto: Integer;
  AValorCustoInicial: Double);
var
  fdmTabelaPreco: TFDMemTable;
begin
  fdmTabelaPreco := TTabelaPreco.BuscarProdutoEmSuasTabelasPreco(AIdproduto);
  try
    fdmTabelaPreco.First;
    while not fdmTabelaPreco.Eof do
    begin
      ADatasetTabelaPreco.Append;
      TDatasetUtils.CopiarRegistro(fdmTabelaPreco, ADatasetTabelaPreco);

      if ADatasetTabelaPreco.FieldByName('VL_CUSTO').AsFloat <= 0 then
      begin
        ADatasetTabelaPreco.FieldByName('VL_CUSTO').AsFloat := AValorCustoInicial;
      end;

      ADatasetTabelaPreco.Post;

      fdmTabelaPreco.Next;
    end;
  finally
    FreeAndNil(fdmTabelaPreco);
  end;
end;

{ TProdutoEtiqueta }

class procedure TProdutoEtiqueta.ImprimirEtiquetaArgox(AFieldIDProduto: TField;
  AQtdeProduto: Integer; AFieldQuantidade: TField = nil);
var
  dataset: TDataset;
  smProduto: TSMProdutoClient;
  fdmProdutoImpressao: TFDMemTable;
  JSONDataset: TFDJSONDataSets;
  ACBrETQ: TACBrETQ;
  i: Integer;
  contadorDeEtiquetas: Integer;
const
  distancia1: Integer = 30;
  distancia2: Integer = 390;
  distancia3: Integer = 750;

  procedure InicializaEtiqueta;
  begin
    if contadorDeEtiquetas = 0 then
    begin
      ACBrETQ.IniciarEtiqueta;
    end;
  end;

  procedure FinalizaEtiqueta;
  begin
    //ACBrETQ.FinalizarEtiqueta(1, 0);
    contadorDeEtiquetas := 0;
    ACBrETQ.Imprimir(1, 0);
  end;

  function GetDistancia: Integer;
  begin
    case contadorDeEtiquetas of
      1:result := distancia1;
      2:result := distancia2;
      3:result := distancia3;
    end;
  end;

begin
  dataset := AFieldIDProduto.DataSet;

  if dataset.IsEmpty then
    exit;

  smProduto := TSMProdutoClient.Create(DmConnection.Connection.DBXConnection);
  try
    dataset.DisableControls;

    ACBrETQ := TACBrETQ.Create(nil);
    try
      ACBrETQ.Modelo := TACBrETQModelo(etqPpla);
      ACBrETQ.Porta := IniSystemFile.impressora_argox;
      ACBrETQ.Ativar;

      contadorDeEtiquetas := 0;

      dataset.First;
      while not dataset.Eof do
      begin
        fdmProdutoImpressao := TFDMemTable.Create(nil);
        try
          JSONDataset := smProduto.GetProduto(AFieldIDProduto.AsInteger);
          TDatasetUtils.JSONDatasetToMemTable(JSONDataset, fdmProdutoImpressao);

          if AFieldQuantidade <> nil then
            AQtdeProduto := AFieldQuantidade.AsInteger;

          for i := 0 to Pred(AQtdeProduto) do
          begin
            Inc(contadorDeEtiquetas);

            ACBrETQ.ImprimirTexto(orNormal, 1, 1, 2, 160, GetDistancia,
              Copy(fdmProdutoImpressao.FieldByName('id').AsString +' - '+
              fdmProdutoImpressao.FieldByName('descricao').AsString, 1, 28));

            ACBrETQ.ImprimirTexto(orNormal, 1, 1, 2, 130, GetDistancia,
              Copy(fdmProdutoImpressao.FieldByName('id').AsString +' - '+
              fdmProdutoImpressao.FieldByName('descricao').AsString, 29, 28));

            if contadorDeEtiquetas = 1 then
            begin
              ACBrETQ.ImprimirBarras(orNormal, 'F', '2', '2', 2, GetDistancia,
                fdmProdutoImpressao.FieldByName('codigo_barra').AsString, 70);
            end
            else
            begin
              ACBrETQ.ImprimirBarras(orNormal, 'F', '2', '2', 2, GetDistancia+25,
                fdmProdutoImpressao.FieldByName('codigo_barra').AsString, 70);
            end;

            if contadorDeEtiquetas = 3 then
            begin
              FinalizaEtiqueta;
              contadorDeEtiquetas := 0;
            end;
          end;
        finally
          FreeAndNil(fdmProdutoImpressao);
        end;

        dataset.Next;
      end;

      if contadorDeEtiquetas > 0 then
      begin
        FinalizaEtiqueta;
        contadorDeEtiquetas := 0;
      end;
    finally
      ACBrETQ.Desativar;
    end;
  finally
    dataset.EnableControls;
    smProduto.Free;
    FreeAndNil(ACBrETQ);
  end;
end;

end.

