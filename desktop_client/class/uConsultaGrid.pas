unit uConsultaGrid;

interface

Uses Classes, SysUtils, ugbClientDataset, db;

type TConsultaGrid = class
  class procedure ConsultarPorContaAnalise(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdContaAnalise: String = 'ID_CONTA_ANALISE';
    AFieldDescricaoContaAnalise: String = 'JOIN_DESCRICAO_CONTA_ANALISE';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorContaCorrente(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdContaCorrente: String = 'ID_CONTA_CORRENTE';
    AFieldDescricaoContaCorrente: String = 'JOIN_DESCRICAO_CONTA_CORRENTE';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorCentroResultado(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdCentroResultado: String = 'ID_CENTRO_RESULTADO';
    AFieldDescricaoCentroResultado: String = 'JOIN_DESCRICAO_CENTRO_RESULTADO';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorCarteira(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdCarteira: String = 'ID_CARTEIRA';
    AFieldDescricaoCarteira: String = 'JOIN_DESCRICAO_CARTEIRA';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorTipoQuitacao(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdTipoQuitacao: String = 'ID_TIPO_QUITACAO';
    AFieldDescricaoTipoQuitacao: String = 'JOIN_DESCRICAO_TIPO_QUITACAO';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorMontadora(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdMontadora: String = 'ID_MONTADORA';
    AFieldDescricaoMontadora: String = 'JOIN_DESCRICAO_MONTADORA';
    AManipularEstadoDataset: Boolean = true);

  class procedure ConsultarPorModeloMontadora(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdModeloMontadora: String = 'ID_MODELO_MONTADORA';
    AFieldDescricaoModeloMontadora: String = 'JOIN_DESCRICAO_MODELO_MONTADORA';
    AManipularEstadoDataset: Boolean = true);

end;

implementation

uses uFrmConsultaPadrao;

class procedure TConsultaGrid.ConsultarPorContaAnalise(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdContaAnalise: String = 'ID_CONTA_ANALISE';
    AFieldDescricaoContaAnalise: String = 'JOIN_DESCRICAO_CONTA_ANALISE';
    AManipularEstadoDataset: Boolean = true);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdContaAnalise);
    sourceFields.Add(AFieldDescricaoContaAnalise);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CONTA_ANALISE',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CONTA_ANALISE',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if AManipularEstadoDataset then
      ADataset.Salvar;
  end;
end;

class procedure TConsultaGrid.ConsultarPorContaCorrente(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdContaCorrente: String = 'ID_CONTA_CORRENTE';
    AFieldDescricaoContaCorrente: String = 'JOIN_DESCRICAO_CONTA_CORRENTE';
    AManipularEstadoDataset: Boolean = true);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdContaCorrente);
    sourceFields.Add(AFieldDescricaoContaCorrente);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CONTA_CORRENTE',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CONTA_CORRENTE',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if AManipularEstadoDataset then
      ADataset.Salvar;
  end;
end;

class procedure TConsultaGrid.ConsultarPorModeloMontadora(ADataset: TgbClientDataset;
  AExibirFormulario: Boolean; AValorConsultaOculta, AFieldIdModeloMontadora,
  AFieldDescricaoModeloMontadora: String; AManipularEstadoDataset: Boolean);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdModeloMontadora);
    sourceFields.Add(AFieldDescricaoModeloMontadora);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'MODELO_MONTADORA',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'MODELO_MONTADORA',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if (AManipularEstadoDataset) then
    begin
      if (not ADataset.FieldByName(AFieldIdModeloMontadora).AsString.IsEmpty)
      and (not ADataset.FieldByName(AFieldDescricaoModeloMontadora).AsString.IsEmpty) then
      begin
        ADataset.Salvar;
      end
      else
      begin
        ADataset.FieldByName(AFieldIdModeloMontadora).Clear;
        ADataset.FieldByName(AFieldDescricaoModeloMontadora).Clear;
      end;
    end;
  end;
end;

class procedure TConsultaGrid.ConsultarPorMontadora(ADataset: TgbClientDataset; AExibirFormulario: Boolean;
  AValorConsultaOculta, AFieldIdMontadora, AFieldDescricaoMontadora: String;
  AManipularEstadoDataset: Boolean);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdMontadora);
    sourceFields.Add(AFieldDescricaoMontadora);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'MONTADORA',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'MONTADORA',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if (AManipularEstadoDataset) then
    begin
      if (not ADataset.FieldByName(AFieldIdMontadora).AsString.IsEmpty)
      and (not ADataset.FieldByName(AFieldDescricaoMontadora).AsString.IsEmpty) then
      begin
        ADataset.Salvar;
      end
      else
      begin
        ADataset.FieldByName(AFieldIdMontadora).Clear;
        ADataset.FieldByName(AFieldDescricaoMontadora).Clear;
      end;
    end;
  end;
end;

class procedure TConsultaGrid.ConsultarPorCarteira(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdCarteira: String = 'ID_CARTEIRA';
    AFieldDescricaoCarteira: String = 'JOIN_DESCRICAO_CARTEIRA';
    AManipularEstadoDataset: Boolean = true);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdCarteira);
    sourceFields.Add(AFieldDescricaoCarteira);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CARTEIRA',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CARTEIRA',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if AManipularEstadoDataset then
      ADataset.Salvar;
  end;
end;

class procedure TConsultaGrid.ConsultarPorTipoQuitacao(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdTipoQuitacao: String = 'ID_TIPO_QUITACAO';
    AFieldDescricaoTipoQuitacao: String = 'JOIN_DESCRICAO_TIPO_QUITACAO';
    AManipularEstadoDataset: Boolean = true);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdTipoQuitacao);
    sourceFields.Add(AFieldDescricaoTipoQuitacao);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'TIPO_QUITACAO',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'TIPO_QUITACAO',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if AManipularEstadoDataset then
      ADataset.Salvar;
  end;
end;

class procedure TConsultaGrid.ConsultarPorCentroResultado(ADataset: TgbClientDataset;
    AExibirFormulario: Boolean = true; AValorConsultaOculta: String = '';
    AFieldIdCentroResultado: String = 'ID_CENTRO_RESULTADO';
    AFieldDescricaoCentroResultado: String = 'JOIN_DESCRICAO_CENTRO_RESULTADO';
    AManipularEstadoDataset: Boolean = true);
var
  sourceFields,
  consultaFields: TStringList;
begin
  inherited;
  sourceFields := TStringList.Create;
  consultaFields := TStringList.Create;
  try
    if AManipularEstadoDataset then
      ADataset.Alterar;

    sourceFields.Add(AFieldIdCentroResultado);
    sourceFields.Add(AFieldDescricaoCentroResultado);
    consultaFields.Add('ID');
    consultaFields.Add('DESCRICAO');

    if AExibirFormulario then
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CENTRO_RESULTADO',
        sourceFields,
        consultaFields);
    end
    else
    begin
      TFrmConsultaPadrao.Consultar(
        ADataset,
        'CENTRO_RESULTADO',
        sourceFields,
        consultaFields,
        '', nil, true, 'ID', AValorConsultaOculta);
    end;
  finally
    FreeAndNil(sourceFields);
    FreeAndNil(consultaFields);

    if AManipularEstadoDataset then
      ADataset.Salvar;
  end;
end;

end.

