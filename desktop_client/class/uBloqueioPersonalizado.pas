unit uBloqueioPersonalizado;

interface

type TBloqueioPeronalizado = class
  class function UsuarioAutorizado(const AIdBloqueioPersonalizado, AIdUsuario: Integer): Boolean;
end;

implementation

{ TBloqueioPeronalizado }

uses uClientClasses, uDmConnection;

class function TBloqueioPeronalizado.UsuarioAutorizado(const AIdBloqueioPersonalizado,
  AIdUsuario: Integer): Boolean;
var smBloqueioPersonalizado: TSMBloqueioPersonalizadoClient;
begin
  smBloqueioPersonalizado := TSMBloqueioPersonalizadoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smBloqueioPersonalizado.UsuarioAutorizado(AIdBloqueioPersonalizado, AIdUsuario);
  finally
    smBloqueioPersonalizado.Free;
  end;
end;

end.
