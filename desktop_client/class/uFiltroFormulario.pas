unit uFiltroFormulario;

interface

Uses Classes, System.SysUtils, DateUtils, StrUtils;

type TOperadorFiltroPadrao = (opAND, opOR);

type TFiltroConsultaPadrao = class
  class function RetornarColunaComAlias(const AColuna, ASQL: String): String;
end;

type TFiltroPadrao = class
  private
    FWhere: String;
    procedure SetOperador(const AOperador: TOperadorFiltroPadrao; var ACondicao: String);
    procedure SetNegacaoNaCondicao(var ACondicao: String);
    procedure AddCondicao(const ACondicao: String);
    function RetornarDataNoFormatoMYSQL(AData: TDate): String;
    function RetornarDoubleNoFormatoMYSQL(AValor: Double): String;
  public
    procedure Limpar;

    procedure AddIgual(const ACampo, AValor: String;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddIgualDate(const ACampo: String; AValor: TDate;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddIgual(const ACampo: String; AValor: Double;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

{    procedure AddIgual(const ACampo: String; AValor: Integer;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;}

    procedure AddContem(const ACampo, AValor: String;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddDiferente(const ACampo, AValor: String;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);

    procedure AddEntre(const ACampo: String; AValorInicio, AValorFim: Integer;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddEntre(const ACampo: String; AValorInicio, AValorFim: TDate;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddFaixa(const ACampo, AValor: String;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);overload;

    procedure AddFaixa(const ACampo: String; AValor: TStringList; const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
      const ANegarCondicao: Boolean);overload;

    procedure AddMaior(const ACampo: String; AValor: Double;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);

    procedure AddMenor(const ACampo: String; AValor: Double;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);

    procedure AddMaiorIgual(const ACampo: String; AValor: Double;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);

    procedure AddMenorIgual(const ACampo: String; AValor: Double;
      const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd; const ANegarCondicao: Boolean = false);

    procedure AddIsNull(const ACampo: String; const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd);

    procedure AddIsNotNull(const ACampo: String; const AOperadorJuncaoCondicional: TOperadorFiltroPadrao = opAnd);

    property where: String read FWhere write FWhere;

    Constructor Create;
end;

implementation

{ TFiltroPadrao }

function TFiltroPadrao.RetornarDataNoFormatoMYSQL(AData: TDate): String;
var dia, mes, ano: word;
begin
  DecodeDate(AData, ano, mes, dia);
  result :=  InttoStr(ano)+FormatFloat('00',mes)+FormatFloat('00',dia);
end;

function TFiltroPadrao.RetornarDoubleNoFormatoMYSQL(AValor: Double): String;
begin
  result := StringReplace(FloattoStr(AValor), ',', '.', [rfReplaceAll]);
end;

procedure TFiltroPadrao.AddCondicao(const ACondicao: String);
begin
  where := where + ' '+ ACondicao;
end;

procedure TFiltroPadrao.AddDiferente(const ACampo, AValor: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);
  whereTemp := whereTemp + ' UPPER(' + ACampo + ') <> '+ QuotedStr(UpperCase(AValor))+ ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddEntre(const ACampo: String; AValorInicio, AValorFim: TDate;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + 'Date('+ACampo+')' + ' BETWEEN '+ RetornarDataNoFormatoMYSQL(AValorInicio)
    + ' AND '+ RetornarDataNoFormatoMYSQL(AValorFim) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddEntre(const ACampo: String; AValorInicio, AValorFim: Integer;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' BETWEEN '+ InttoStr(AValorInicio) + ' AND '+ InttoStr(AValorFim) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddFaixa(const ACampo, AValor: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' UPPER(' + ACampo + ') IN ('+ UpperCase(AValor) + ') ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddFaixa(const ACampo: String; AValor: TStringList;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' UPPER(' + ACampo + ') IN ('+ UpperCase(AValor.CommaText) + ') ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddIgual(const ACampo: String; AValor: Double;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' UPPER(' + ACampo + ') = '+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddIgualDate(const ACampo: String; AValor: TDate;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao; const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + 'Date('+ACampo+')' + ' = '+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

{procedure TFiltroPadrao.AddIgual(const ACampo: String; AValor: Integer;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao; const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' = '+ InttoStr(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;      }

procedure TFiltroPadrao.AddIsNotNull(const ACampo: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' IS NOT NULL ';

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddIsNull(const ACampo: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' IS NULL ';

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddContem(const ACampo, AValor: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' UPPER(' + ACampo + ') LIKE ('+ QuotedStr('%'+UpperCase(AValor)+'%') + ') ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddIgual(const ACampo, AValor: String;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' UPPER(' + ACampo + ') = '+ QuotedStr(UpperCase(AValor)) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddMaior(const ACampo: String; AValor: Double;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' > '+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddMaiorIgual(const ACampo: String; AValor: Double;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' >='+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddMenor(const ACampo: String; AValor: Double;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' < '+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

procedure TFiltroPadrao.AddMenorIgual(const ACampo: String; AValor: Double;
  const AOperadorJuncaoCondicional: TOperadorFiltroPadrao;
  const ANegarCondicao: Boolean);
var whereTemp: String;
begin
  SetOperador(AOperadorJuncaoCondicional, whereTemp);

  whereTemp := whereTemp + ' ' + ACampo + ' <= '+ RetornarDoubleNoFormatoMYSQL(AValor) + ' ';

  if ANegarCondicao then
    SetNegacaoNaCondicao(whereTemp);

  AddCondicao(whereTemp);
end;

constructor TFiltroPadrao.Create;
begin
  inherited Create;
  Limpar;
end;

procedure TFiltroPadrao.Limpar;
begin
  where := '';
end;

procedure TFiltroPadrao.SetNegacaoNaCondicao(var ACondicao: String);
begin
  ACondicao := ' NOT ('+ACondicao+') ';
end;

procedure TFiltroPadrao.SetOperador(const AOperador: TOperadorFiltroPadrao; var ACondicao: String);
begin
  if where.IsEmpty then
  begin
    ACondicao := ' WHERE ';
  end
  else
  begin
    case AOperador of
      opAND: ACondicao := ' AND ';
      opOR: ACondicao := ' OR ';
    end;
  end;
end;

{ TFiltroConsultaPadrao }

class function TFiltroConsultaPadrao.RetornarColunaComAlias(const AColuna,
  ASQL: String): String;
var i, posicao: Integer;
begin
  posicao := Pos(uppercase(AColuna), uppercase(ASQL));
  if ASQL[posicao -1] = '.' then
  begin
    result := AColuna;
    for i := (posicao-2) downto 0 do
    begin
      if AnsiIndexStr(ASQL[i], ['.','',' ',',']) > -1 then
      begin
        result := Copy(ASQL, i+1, (posicao-1) - i)+result;
        break;
      end;
    end;
  end
  else
    result := AColuna;
end;

end.

