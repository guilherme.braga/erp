unit uTFunction;

interface

Uses
  Forms,
  Classes,
  SysUtils,
  Windows,
  DB,
  Variants,
  ActnList,
  Controls,
  SqlExpr,
  DBClient,
  StrUtils;

type TFunction = class
  // Fun��es If Then Else Tern�rio
  class function IIF(AValue: Boolean; const ATrue, AFalse: Variant): Variant;

  // Faz a leitura do parametro de dados da consulta padr�o que � enviada ao servidor
  class function ReadWhere(const AText: string; ADataSet: TDataSet; Alike: Boolean = false): string;

  //Substitui "NOT NULL" para "NULL"
  class function getSqlNull(const cSql: String): String;

  // class procedures de busca nas consultas
  class function SearchReg(ADataSet: TClientDataSet; AField, AValue: array of string; AConjuncao: Boolean = true; ALike: Boolean = false; AWhere: String = ''; AMsg: Boolean = False): Boolean;

  // Fun��es de Manipula��o de Strings
  class function getInverseString(value: String): String;

  // Valida arquivo
  class function validateFile(const value: String; lMsg: Boolean = true): Boolean;

  //Valida diret�rio, permitindo a cria��o do mesmo
  class procedure validatePath(const value: String; lCreate: Boolean = true);

  class procedure ControlsEnabledControl(AControls: array of TControl; Enable: Boolean;Visible: Boolean = false);
  class procedure ControlsEnabledAction(Actions: array of TAction; Enable: Boolean; Visible: Boolean = false);
  class procedure EnabledContainerControl(Container: TWinControl; ATag: Smallint; Enable: Boolean);
  class procedure VisibleContainerControl(Container: TWinControl; ATag: Smallint; Visible: Boolean);
  class function getFieldByName(const cSQL, cByName: String): String;
  class function getAliasByField(const cByName, cSQL: String): String;
  class function getFieldByNickname(const cSQL, cByName: String): String;

  { Devolve o valor total de um field de um dataset
    Recebe um TField, faz a itera��o somando seu Value e devolve esse resultado}
  class function getTotalValor(const field: TField): Real;

  class function getTotalValorOperacao(const field1, field2: TField; nOperacao: Integer): Real;

  class procedure ExecutarUpdate(const ASql: AnsiString; const AConnection: TSQLConnection);

  class function StringDateTime(const ADateTime: TDateTime): string;

  class function ChangeResolution(AResWidth, AResHeigth : word): Boolean;

  class function ReplaceStr(Text, oldstring, newstring: string): string;

  class function RemoveAcentos(AValue: String): String;

  class function Arredondar(Valor: Double; Dec: Integer): Double;

  class function FirstLetterUpper(AValue: String): String;

  class function ChangeUnderlineToSpace(AValue: String): String;

end;

implementation

class function TFunction.Arredondar(Valor: Double; Dec: Integer): Double;
var
  Valor1,
  Numero1,
  Numero2,
  Numero3: Double;
begin
  Valor1:=Exp(Ln(10) * (Dec + 1));
  Numero1:=Int(Valor * Valor1);
  Numero2:=(Numero1 / 10);
  Numero3:=Round(Numero2);

  Result:=(Numero3 / (Exp(Ln(10) * Dec)));
end;

class function TFunction.ReplaceStr(Text, oldstring, newstring: string): string;
var atual, strtofind, originalstr: pchar;
  NewText: string;
  lenoldstring, lennewstring, m, index: integer;
begin //ReplaceStr
  NewText := Text;
  originalstr := pchar(Text);
  strtofind := pchar(oldstring);
  lenoldstring := length(oldstring);
  lennewstring := length(newstring);
  Atual := StrPos(OriginalStr, StrtoFind);
  index := 0;
  while Atual <> nil do
    begin //Atual<>nil
      m := Atual - OriginalStr - index + 1;
      Delete(NewText, m, lenoldstring);
      Insert(newstring, NewText, m);
      inc(index, lenoldstring - lennewstring);
      Atual := StrPos(Atual + lenoldstring, StrtoFind);
    end; //Atual<>nil
  Result := NewText;
end; //ReplaceStr

class function TFunction.getTotalValor(const field: TField): Real;
var cds: Pointer;
    cField: string;
    nTotal: Real;
begin
  result := 0;
  nTotal := 0;

  cds := field.DataSet;
  cField := field.FullName;

  TDataset(cds).DisableControls;
  TDataset(cds).First;
  while not TDataset(cds).Eof do
    begin
      nTotal := nTotal + TDataset(cds).FieldByName(cField).AsFloat;
      TDataset(cds).Next;
    end;
  TDataset(cds).EnableControls;

  result := nTotal;
end;

class function TFunction.getTotalValorOperacao(const field1, field2: TField; nOperacao: Integer): Real;
var cds: Pointer;
    cField1, cField2: string;
    nTotal: Real;
begin
  { nOperacao: 1-Somar | 2-Subtrair | 3-Multiplicar | 4-Dividir}

  result := 0;
  nTotal := 0;

  cds := field1.DataSet;
  cField1 := field1.FullName;
  cField2 := field2.FullName;

  TDataset(cds).DisableControls;
  TDataset(cds).First;
  while not TDataset(cds).Eof do
    begin
      case nOperacao of
        1: nTotal := nTotal + (TDataset(cds).FieldByName(cField1).Value + TDataset(cds).FieldByName(cField2).Value);
        2: nTotal := nTotal + (TDataset(cds).FieldByName(cField1).Value - TDataset(cds).FieldByName(cField2).Value);
        3: nTotal := nTotal + (TDataset(cds).FieldByName(cField1).Value * TDataset(cds).FieldByName(cField2).Value);
        4: nTotal := nTotal + (TDataset(cds).FieldByName(cField1).Value / TDataset(cds).FieldByName(cField2).Value);
      end;
      TDataset(cds).Next;
    end;
  TDataset(cds).EnableControls;

  result := nTotal;
end;

class procedure TFunction.validatePath(const value: String; lCreate: Boolean = true);
begin
  if not DirectoryExists(value) then
    if lCreate then CreateDir(value);
end;

class function TFunction.validateFile(const value: String; lMsg: Boolean = true): Boolean;
begin
  result := true;

  if not FileExists(value) then
    begin
      result := false;
      if lMsg then
        MessageBox(0, 'Arquivo n�o encontrado!', 'Aten��o', MB_ICONERROR + MB_OK);
    end;
end;

class function TFunction.getInverseString(value: String): String;
var
  retorno: String;
  i: Integer;
begin
  for i := Length(value) downto 1 do
    retorno := retorno + value[i];
  result := retorno;
end;

class function TFunction.IIF(AValue: Boolean; const ATrue, AFalse: Variant): Variant;
begin
  if AValue then
    result := ATrue
  else
    result := AFalse;
end;

class function TFunction.getSqlNull(const cSql: String): String;
begin
  result := StringReplace(cSql, 'NOT NULL', 'NULL', [rfReplaceAll])
end;

class function TFunction.ReadWhere(const AText: string; ADataSet: TDataSet; Alike: Boolean = false): string;
var
  cFieldName, cWhere, cValue, cSql: String;
  iPos: Integer;

  function getFullName(cFieldName: String): String;
  var
    i: Integer;
  begin
    cSql := TSQLDataSet(ADataSet).CommandText;

    for i := 0 to Pred(ADataSet.Fields.Count) do
    begin
      if ADataSet.Fields[i].Fullname = cFieldName then
      begin
        result := getFieldByName(cSQL, ADataSet.Fields[i].Fullname);
        break;
      end;
    end;
  end;


  procedure ExtractFieldName(iPosition: Integer);
  var
    i, n: Integer;
  begin
    for i := iPosition to Length(AText) do
    begin
      if AText[i] = '|' then
      begin
        n := i + 2;
        break;
      end
      else
        cFieldName := cFieldName + AText[i];
    end;
    for i := n to Length(AText) do
    begin
      if AText[i] = '>' then
      begin
        iPos := i + 2;
        break;
      end
      else
        cValue := cValue + AText[i];
    end;
    cWhere := cWhere + ' AND ('+ getFullName(cFieldName) + IIF(Alike, ' CONTAINING ', ' = ') + cValue + ')';
  end;

begin
  iPos := 2;
  while iPos < Length(AText) do
    begin
      cFieldName := '';
      cValue := '';
      ExtractFieldName(iPos);
    end;
  result := cWhere;
end;

class function TFunction.RemoveAcentos(AValue: String): String;
const
  ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
var
   x: Integer;
begin;
  for x := 1 to Length(AValue) do
    if Pos(AValue[x],ComAcento) <> 0 then
      AValue[x] := SemAcento[Pos(AValue[x], ComAcento)];

  Result := AValue;
end;

class function TFunction.getFieldByNickname(const cSQL, cByName: String): String;
var
  cFieldName: String;
  position, i: Integer;
begin
  {Recebe o COD_PAIS devolve C�digo}

  position := Pos(cByName, cSQL) + Length(cByName) + 2;
  for i := position to Length(cSQL) do
    if (cSQL[i] <> ' ') and (cSQL[i] <> ',') then
    begin
      if (cSQL[i] = ' ') or (cSQL[i] = ',') or (cSQL[i] = '"') then
        break;
      cFieldName := cFieldName + cSQL[i];
    end;
  result := cFieldName;
end;

class function TFunction.getFieldByName(const cSQL, cByName: String): String;
var
  cFieldName: String;
  position, i: Integer;
begin
  {Recebe o C�digo devolve COD_PAIS}
  position := Pos(cByName, cSQL) - 3;
  for i := position downto 0 do
    begin
      cFieldName := cFieldName + cSQL[i];
      if cSQL[i] = ' ' then
        break;
    end;
  result := getInverseString(cFieldName);
end;

class function TFunction.getAliasByField(const cByName, cSQL: String): String;
var
  cAlias: String;
  position, i: Integer;
begin
  position := Pos(UpperCase('.'+cByName), UpperCase(cSQL));
  cAlias := '';

  for i := (position-1) downto 0 do
    begin
      if (cSQL[i] = ' ') or (cSQL[i] = '(') then
        begin
          cAlias := '.' + cAlias;
          break;
        end;

      cAlias := cAlias + cSQL[i];
    end;

  result := getInverseString(cAlias);
end;

class function TFunction.SearchReg(ADataSet: TClientDataSet; AField, AValue: array of string; AConjuncao: Boolean = true; ALike: Boolean = false; AWhere: String = ''; AMsg: Boolean = False): Boolean;
var nPosition, i: Integer;
    cWhere: String;
begin
  result := false;
  { TODO : Adicionar abertura do formulario wait }

  nPosition := Pos('WHERE', UpperCase(ADataSet.CommandText));

  if nPosition = 0 then exit;

  ADataSet.Close;

  ADataSet.CommandText :=
    Copy(ADataSet.CommandText, 1, Pred(nPosition))+' WHERE ';

  { TODO : Tratar consulta com iif }

  for i := 0 to Pred(Length(AField)) do
    cWhere := cWhere + TFunction.IIF(i = 0, '', TFunction.IIF(AConjuncao, ' AND ', ' OR '))+
    TFunction.GetAliasByField(AField[i], ADataSet.CommandText) + AField[i] + TFunction.IIF(ALike, ' CONTAINING('+QuotedStr(AValue[i])+') ', ' = '+QuotedStr(AValue[i]))+ ' ';

  ADataSet.CommandText := ADataSet.CommandText + cWhere + AWhere;

  ADataSet.Open;

  result := not ADataSet.IsEmpty;

  { TODO : Adicionar fechamento do formulario wait }
  if ADataSet.IsEmpty then
    begin
      if AMsg then
        { TODO : Ver possibilidade de exibicao de formulario personalizado de mensagem }
        Application.MessageBox('Nenhum registro encontrado.', Pchar(Application.Title), 64);
      ADataSet.Close;
    end;
end;

class function TFunction.StringDateTime(const ADateTime: TDateTime): string;
begin
  result := StringReplace(DateTimetoStr(ADateTime),'/','.', [rfReplaceAll]);
end;

class procedure TFunction.ControlsEnabledControl(AControls: array of TControl; Enable: Boolean;
  Visible: Boolean = false);
var
  i: Integer;
begin
  for i := Low(AControls) to High(AControls) do
  begin
    if Visible then
      AControls[i].Visible := Enable
    else
      AControls[i].Enabled := Enable;
  end;
end;

class procedure TFunction.ControlsEnabledAction(Actions: array of TAction; Enable: Boolean; Visible: Boolean = false);
var
  i: Integer;
begin
  for i := Low(Actions) to High(Actions) do
  begin
    if Visible then
      Actions[i].Visible := Enable
    else
      Actions[i].Enabled := Enable;
  end;
end;

class procedure TFunction.EnabledContainerControl(Container: TWinControl; ATag: Smallint; Enable: Boolean);
var
  i: Integer;
begin
  for i := 0 to Pred(Container.ControlCount) do
    if Container.Controls[i].Tag = ATag then
      Container.Controls[i].Enabled := Enable;
end;

class procedure TFunction.ExecutarUpdate(const ASql: AnsiString; const AConnection: TSQLConnection);
var sqlQuery: TSQLQuery;
begin
  sqlQuery := TSQLQuery.Create(AConnection);
  try
    sqlQuery.Close;
    sqlQuery.CommandText := ASql;
    sqlQuery.SQLConnection := AConnection;
    sqlQuery.ExecSQL;
  finally
    FreeAndNil(sqlQuery);
  end;
end;

class function TFunction.FirstLetterUpper(AValue: String): String;
var i: Integer;
begin
  AValue := Copy(UpperCase(AValue), 1, 1) + Copy(LowerCase(AValue), 2, Length(AValue));

  for i := 0 to Length(AValue) do
    if AValue[i] = ' ' then
      if AValue[i+1] <> '' then
        AValue[i+1] :=  Char(PChar(PAnsiString(UpperCase(AValue[i+1]))));

  result := AValue;
end;

class procedure TFunction.VisibleContainerControl(Container: TWinControl; ATag: Smallint; Visible: Boolean);
var i: Integer;
begin
  for i := 0 to Pred(Container.ControlCount) do
    if Container.Controls[i].Tag = ATag then
      Container.Controls[i].Visible := Visible
end;

class function TFunction.ChangeResolution(AResWidth, AResHeigth: word): Boolean;
var lpDevMode: TDeviceMode;
begin
  if EnumDisplaySettings(nil, 0, lpDevMode) then
    begin
      lpDevMode.dmFields := DM_PELSWIDTH Or DM_PELSHEIGHT;
      lpDevMode.dmPelsWidth := AResWidth;
      lpDevMode.dmPelsHeight:= AResHeigth;
      Result := ChangeDisplaySettings(lpDevMode, 0) = DISP_CHANGE_SUCCESSFUL;
    end;
end;

class function TFunction.ChangeUnderlineToSpace(AValue: String): String;
begin
  result := StringReplace(AValue, '_', ' ', [rfReplaceAll]);
end;

end.
