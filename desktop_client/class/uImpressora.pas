unit uImpressora;

interface

Uses Printers, Classes, SysUtils;

type TImpressora = class
  class function GetListaImpressoras: String;
  class procedure DefinirImpressoraPadrao(const ANome: String);
  class function GetNomeImpressoraPadrao :String;
  class function GetIndiceImpressoraPadrao: Integer;
  class function GetIndiceImpressora(const AImpressora: String): Integer;
  class function ImpressoraValida(const AImpressora: String): Boolean;
end;

implementation


class function TImpressora.GetIndiceImpressora(const AImpressora: String): Integer;
var
  impressoras: TStringList;
  indice: Integer;
begin
  impressoras := TStringList.Create;
  try
    impressoras.CommaText :=  TImpressora.GetListaImpressoras;
    impressoras.Find(AImpressora,indice);
    result := indice;
  finally
    FreeAndNil(impressoras);
  end;
end;

class function TImpressora.GetIndiceImpressoraPadrao: Integer;
var
  impressoras: TStringList;
  indice: Integer;
begin
  impressoras := TStringList.Create;
  try
    impressoras.CommaText :=  TImpressora.GetListaImpressoras;
    impressoras.Find(TImpressora.GetNomeImpressoraPadrao,indice);
    result := indice;
  finally
    FreeAndNil(impressoras);
  end;
end;

class function TImpressora.GetListaImpressoras: String;
Var
  I : Integer;
  impressoras: TStringList;
begin
  impressoras := TStringList.Create;
  try
    for I := 0 to Printer.Printers.Count - 1 do
    begin
      impressoras.Add(Printer.Printers.Strings[I]);
    end;
    result := impressoras.CommaText;
  finally
    FreeAndNil(impressoras);
  end;
end;

class function TImpressora.ImpressoraValida(const AImpressora: String): Boolean;
var
  impressoras: TStringList;
  indice: Integer;
begin
  impressoras := TStringList.Create;
  try
    impressoras.CommaText :=  TImpressora.GetListaImpressoras;
    impressoras.Find(AImpressora,indice);
    result := indice >= 0;
  finally
    FreeAndNil(impressoras);
  end;
end;

class procedure TImpressora.DefinirImpressoraPadrao(const ANome: String);
begin
  Printer.PrinterIndex := Printer.Printers.IndexOf(ANome);
end;

class function TImpressora.GetNomeImpressoraPadrao :String;
begin
  if(Printer.PrinterIndex > 0)then
  begin
    Result := Printer.Printers[Printer.PrinterIndex];
  end
  else
  begin
    Result := '';
  end;
end;

end.
