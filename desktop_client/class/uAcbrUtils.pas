unit uAcbrUtils;

interface

uses cxMemo, acbrTroco, SysUtils;

type TAcbrUtils = class
  public
  class procedure ExibirTroco(AValor: Double; AMemoTrocoDetalhado: TcxMemo);
end;

implementation

{ TAcbrUtils }

class procedure TAcbrUtils.ExibirTroco(AValor: Double;
  AMemoTrocoDetalhado: TcxMemo);
var
  i:integer;
  acbrTroco: TACBrTroco;
begin
  acbrTroco := TAcbrTroco.Create(nil);
  try
    acbrTroco.Troco := AValor;

    AMemoTrocoDetalhado.Clear;
    for i := 0 to Pred(acbrTroco.TrocoList.Count) do
      AMemoTrocoDetalhado.Lines.Add(acbrTroco.TrocoList[i].DescricaoCompleta);
  finally
    FreeAndNil(acbrTroco);
  end;
end;

end.
