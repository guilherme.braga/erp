unit uDateUtils;

interface

uses DateUtils, SysUtils;

type TDateUtils = class
  public
    class function NumeroMes(AMes: String): Integer;
    class function NumeroMesParaDescricaoMes(const AMes: Integer): String;
    class function TrocarDia(ANovoDia: Integer; AData: TDate): TDateTime;

    const DIAS_MES_SEPARADOS_PONTO_VIRGULA =
      '1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31';

    const DESCRICAO_MES_DO_ANO_SEPARADOS_PONTO_VIRGULA =
      'JANEIRO;FEVEREIRO;MAR�O;ABRIL;MAIO;JUNHO;JULHO;AGOSTO;SETEMBRO;OUTUBRO;NOVEMBRO;DEZEMBRO';

    const NUMERO_MES_DO_ANO_SEPARADOS_PONTO_VIRGULA =
      '1;2;3;4;5;6;7;8;9;10;11;12';
end;

implementation

class function TDateUtils.NumeroMes(AMes: String): Integer;
begin
  if Pos('JAN',UpperCase(AMes)) > 0 then
    result := 1
  else if Pos('FEV',UpperCase(AMes)) > 0 then
    result := 2
  else if Pos('MAR',UpperCase(AMes)) > 0 then
    result := 3
  else if Pos('ABR',UpperCase(AMes)) > 0 then
    result := 4
  else if Pos('MAI',UpperCase(AMes)) > 0 then
    result := 5
  else if Pos('JUN',UpperCase(AMes)) > 0 then
    result := 6
  else if Pos('JUL',UpperCase(AMes)) > 0 then
    result := 7
  else if Pos('AGO',UpperCase(AMes)) > 0 then
    result := 8
  else if Pos('SET',UpperCase(AMes)) > 0 then
    result := 9
  else if Pos('OUT',UpperCase(AMes)) > 0 then
    result := 10
  else if Pos('NOV',UpperCase(AMes)) > 0 then
    result := 11
  else if Pos('DEZ',UpperCase(AMes)) > 0 then
    result := 12;
end;

class function TDateUtils.NumeroMesParaDescricaoMes(const AMes: Integer): String;
begin
  case AMes of
     1: result := 'Janeiro';
     2: result := 'Fevereiro';
     3: result := 'Mar�o';
     4: result := 'Abril';
     5: result := 'Maio';
     6: result := 'Junho';
     7: result := 'Julho';
     8: result := 'Agosto';
     9: result := 'Setembro';
    10: result := 'Outubro';
    11: result := 'Novembro';
    12: result := 'Dezembro';
  end;
end;

class function TDateUtils.TrocarDia(ANovoDia: Integer; AData: TDate): TDateTime;
var dia, mes, ano: Word;
begin
  DecodeDate(AData, ano, mes, dia);
  try
    result := EncodeDate(ano, mes, ANovoDia);
  except
    TrocarDia(ANovoDia -1, AData);
  end;
end;

end.
