unit uTMessage;

interface

uses
  SysUtils,
  Classes,
  Windows,
  Forms,
  DBClient,
  DB,

  uFrmMessage;

type TMessage = class
  private

  public
    class function MessageDeleteAsk(cMsg : String = '') : Boolean;
    class procedure MessageFailure(cMsg : String = '');
    class procedure MessageSucess(cMsg : String = '');
    class procedure MessageFailureAddPage(cClassForm: String = ''; cMsg: String = '');
    class procedure MessageInformation( cMsg : String = '' );
end;

type TMessagePersistenceError = class
  public
    class procedure OnPostError(AMessage: String);
    class procedure OnEditError(AMessage: String);
    class procedure OnDeleteError(AMessage: String);
end;

implementation

class procedure TMessage.MessageFailureAddPage(cClassForm: String = ''; cMsg: String = '');
begin
  if not cClassForm.IsEmpty then
    cClassForm := ' :'+cClassForm;

  if cMsg = '' then
    cMsg := 'Falha ao instanciar o formul�rio'+cClassForm;

  TFrmMessage.Failure(cMsg);
end;

class function TMessage.MessageDeleteAsk( cMsg : String = '' ) : Boolean;
Begin
  if cMsg.IsEmpty then
    cMsg := 'Deseja realmente excluir o registro selecionado?';

  result := TFrmMessage.Question(cMsg) = MCONFIRMED;
End;

class procedure TMessage.MessageFailure( cMsg : String = '' );
Begin
  if cMsg = '' then
    cMsg := 'Falha ao executar o processo!';

  TFrmMessage.Failure(cMsg);
End;

class procedure TMessage.MessageInformation( cMsg : String = '' );
Begin
  if cMsg = '' then
    cMsg := 'Por favor, verifique os dados.';

  TFrmMessage.Information(cMsg);
End;

class procedure TMessage.MessageSucess( cMsg : String = '' );
Begin
  if cMsg = '' then
    cMsg := 'Processo conclu�do com sucesso!';

  TFrmMessage.Sucess(cMsg);
End;

{ TMessagePersistenceError }

class procedure TMessagePersistenceError.OnDeleteError(AMessage: String);
begin
  TFrmMessage.Failure('N�o foi poss�vel deletar esse registro, verifique se'+
    ' existem depend�ncias que impe�am essa remo��o.');
end;

class procedure TMessagePersistenceError.OnEditError(AMessage: String);
begin
  MessageBox( 0, PWideChar( AMessage ), 'Falha no Processo', MB_ICONERROR + MB_OK );
end;

class procedure TMessagePersistenceError.OnPostError(AMessage: String);
begin
  MessageBox( 0, PWideChar( AMessage ), 'Falha no Processo', MB_ICONERROR + MB_OK );
end;

end.
