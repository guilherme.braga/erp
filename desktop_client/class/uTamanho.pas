unit uTamanho;

interface

Uses uTamanhoProxy, SysUtils;

type TTamanho = class
  private
    class function GerarTamanho(const ATamanho: TTamanhoProxy): Integer;
    class function ExisteTamanho(const ADescricaoTamanho: String): Boolean;
  public
    class function GetTamanho(const AIdTamanho: Integer): TTamanhoProxy;
    class function InserirTamanhoCasoNaoExista(const ADescricaoTamanho: String; var AIdTamanho: Integer): Boolean;
    class function GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
end;

Implementation

uses uClientClasses, uDmConnection;

class function TTamanho.ExisteTamanho(const ADescricaoTamanho: String): Boolean;
var smTamanho: TSMTamanhoClient;
begin
  smTamanho := TSMTamanhoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smTamanho.ExisteTamanho(ADescricaoTamanho);
  finally
    smTamanho.Free;
  end;
end;

class function TTamanho.GerarTamanho(const ATamanho: TTamanhoProxy): Integer;
var smTamanho: TSMTamanhoClient;
begin
  smTamanho := TSMTamanhoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smTamanho.GerarTamanho(ATamanho);
  finally
    smTamanho.Free;
  end;
end;

class function TTamanho.GetIdTamanhoPorDescricao(const ADescricaoTamanho: String): Integer;
var
  smTamanho: TSMTamanhoClient;
begin
  smTamanho := TSMTamanhoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smTamanho.GetIdTamanhoPorDescricao(ADescricaoTamanho);
  finally
    smTamanho.Free;
  end;
end;

class function TTamanho.GetTamanho(const AIdTamanho: Integer): TTamanhoProxy;
var smTamanho: TSMTamanhoClient;
begin
  smTamanho := TSMTamanhoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smTamanho.GetTamanho(AIdTamanho);
  finally
    //smTamanho.Free;
  end;
end;

class function TTamanho.InserirTamanhoCasoNaoExista(const ADescricaoTamanho: String; var AIdTamanho: Integer): Boolean;
var
  tamanho: TTamanhoProxy;
begin
  result := false;
  if not TTamanho.ExisteTamanho(ADescricaoTamanho) then
  begin
    tamanho := TTamanhoProxy.Create;
    try
      tamanho.FDescricao := ADescricaoTamanho;
      AIdTamanho := TTamanho.GerarTamanho(tamanho);
      result := true;
    finally
      //FreeAndNil(montadora);
    end;
  end
  else
  begin
    AIdTamanho := TTamanho.GetIdTamanhoPorDescricao(ADescricaoTamanho);
  end;
end;

end.

