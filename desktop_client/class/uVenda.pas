unit uVenda;

interface

Uses ugbClientDataSet, SysUtils, uVendaProxy, Data.FireDACJSONReflect,
  ugbFDMemTable;

type TVenda = class
  class function Efetivar(AIdChaveProcesso: Integer; AVendaProxy: TVendaProxy): Boolean;
  class procedure LimparParcelas(AParcelas: TgbClientDataset);
  class procedure GerarParcelas(AVenda,AVendaParcela: TGbClientDataset;
    AIdFormaPagamento: Integer = 0; AIdCarteira: Integer = 0);
  class procedure Cancelar(AIdChaveProcesso: Integer);
  class function GerarNotaFiscal(AVenda, AVendaItem,
    AVendaParcela: TgbClientDataset): Boolean;
  class function GetId(AIdChaveProcesso: Integer): Integer;
  class function CancelarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
  class function EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
  class procedure ExcluirVenda(const AIdChaveProcesso: Integer);
  class function GerarDevolucaoEmDinheiroParaCliente(
    const AVendaProxy: TVendaProxy; const AValorDevolucao: Double): Boolean;
  class function GerarDevolucaoEmCreditoParaCliente(
    const AIdVenda: Integer; const AValorDevolucao: Double): Boolean;
  class function GerarDevolucaoCreditandoNoContasAReceber(const AIdVenda: Integer;
    const AValorDevolucao: Double; AIdsContasReceber: String): Boolean;
  class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
  class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
  class function GetIdNotaFiscalNFCE(AId: Integer): Integer;
  class function GetIdNotaFiscalNFE(AId: Integer): Integer;
  class function GetTotaisPorTipoVenda(const AIdsVenda: String): TgbFDMemTable;
  class procedure IncluirBloqueiosVenda(ACdsVendaBloqueioPersonalizado: TgbClientDataset;
    const AIdChaveProcesso: Integer);
end;

const
  //Campos do banco
  FIELD_CODIGO = 'VENDA.ID';
  FIELD_PESSOA = 'VENDA.ID_PESSOA';
  FIELD_DATA_CADASTRO = 'VENDA.DH_CADASTRO';
  FIELD_DATA_FECHAMENTO = 'VENDA.DH_FECHAMENTO';
  FIELD_TIPO_DOCUMENTO = 'VENDA.TIPO';
  FIELD_SITUACAO_DOCUMENTO = 'VENDA.STATUS';

  //Valores dos Campos TIPO
  TIPO_DOCUMENTO_PEDIDO: String = 'PEDIDO';
  TIPO_DOCUMENTO_VENDA: String = 'VENDA';
  TIPO_DOCUMENTO_ORCAMENTO: String = 'OR�AMENTO';
  TIPO_DOCUMENTO_CONDICIONAL: String = 'CONDICIONAL';
  TIPO_DOCUMENTO_DEVOLUCAO: String = 'DEVOLU��O';

  //Valores dos Campos STATUS
  STATUS_ABERTO: String = 'ABERTO';
  STATUS_FECHADO: String = 'FECHADO';
  STATUS_BLOQUEADO: String = 'BLOQUEADO';
  STATUS_CANCELADO: String = 'CANCELADO';

implementation

{ TVenda }

uses uClientClasses, uDmConnection, uPlanoPagamento, uMathUtils,
  uFormaPagamento, Rest.JSON, uFormaPagamentoProxy, uCarteiraProxy, uCarteira,
  uNotaFiscal, uNotaFiscalProxy, uDatasetUtils, uBloqueioPersonalizadoProxy;

class procedure TVenda.Cancelar(AIdChaveProcesso: Integer);
var smVendaFiscal: TsmVendaClient;
begin
  smVendaFiscal := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smVendaFiscal.Cancelar(AIdChaveProcesso);
  finally
    smVendaFiscal.Free;
  end;
end;

class function TVenda.CancelarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
var smVendaFiscal: TsmVendaClient;
begin
  smVendaFiscal := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smVendaFiscal.CancelarMovimentacaoVendaCondicional(AIdChaveProcesso);
  finally
    smVendaFiscal.Free;
  end;
end;

class function TVenda.Efetivar(AIdChaveProcesso: Integer; AVendaProxy: TVendaProxy): Boolean;
var smVendaFiscal: TsmVendaClient;
begin
  smVendaFiscal := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smVendaFiscal.Efetivar(AIdChaveProcesso, TJSON.ObjectToJsonString(AVendaProxy));
  finally
    smVendaFiscal.Free;
  end;
end;

class function TVenda.EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
var smVendaFiscal: TsmVendaClient;
begin
  smVendaFiscal := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smVendaFiscal.EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso);
  finally
    smVendaFiscal.Free;
  end;
end;

class procedure TVenda.ExcluirVenda(const AIdChaveProcesso: Integer);
var smVenda: TsmVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smVenda.ExcluirVenda(AIdChaveProcesso);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GerarDevolucaoCreditandoNoContasAReceber(const AIdVenda: Integer;
  const AValorDevolucao: Double; AIdsContasReceber: String): Boolean;
var smVenda: TsmVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GerarDevolucaoCreditandoNoContasAReceber(AIdVenda, AValorDevolucao, AIdsContasReceber);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GerarDevolucaoEmCreditoParaCliente(const AIdVenda: Integer;
  const AValorDevolucao: Double): Boolean;
var smVenda: TsmVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GerarDevolucaoEmCreditoParaCliente(AIdVenda, AValorDevolucao);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GerarDevolucaoEmDinheiroParaCliente(const AVendaProxy: TVendaProxy;
  const AValorDevolucao: Double): Boolean;
var smVenda: TsmVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GerarDevolucaoEmDinheiroParaCliente(AVendaProxy, AValorDevolucao);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GerarNotaFiscal(AVenda, AVendaItem,
  AVendaParcela: TgbClientDataset): Boolean;
var
  notaFiscal: TNotaFiscalProxy;
begin
  notaFiscal := TNotaFiscalProxy.Create;
  try
    notaFiscal.FDhCadastro := AVenda.GetAsString('DH_CADASTRO');
    notaFiscal.FBNnf := AVenda.GetAsInteger('B_NNF');
    notaFiscal.FBSerie := AVenda.GetAsInteger('B_SERIE');
    notaFiscal.FIdModeloNota := AVenda.GetAsInteger('ID_MODELO_NOTA');
    notaFiscal.FBDemi := AVenda.GetAsString('B_DEMI');
    notaFiscal.FBDsaient := AVenda.GetAsString('B_DSAIENT');
    notaFiscal.FBHsaient := AVenda.GetAsString('B_HSAIENT');
    notaFiscal.FWVprod := AVenda.GetAsFloat('W_VPROD');
    notaFiscal.FWVst := AVenda.GetAsFloat('W_VST');
    notaFiscal.FWVseg := AVenda.GetAsFloat('W_VSEG');
    notaFiscal.FWVdesc := AVenda.GetAsFloat('W_VDESC');
    notaFiscal.FWVoutro := AVenda.GetAsFloat('W_VOUTRO');
    notaFiscal.FWVipi := AVenda.GetAsFloat('W_VIPI');
    notaFiscal.FWVfrete := AVenda.GetAsFloat('W_VFRETE');
    notaFiscal.FWVnf := AVenda.GetAsFloat('W_VNF');
    notaFiscal.FXXnome := AVenda.GetAsString('X_XNOME');
    notaFiscal.FVlFreteTransportadora := AVenda.GetAsFloat('VL_FRETE_TRANSPORTADORA');
    notaFiscal.FDtVencimento := AVenda.GetAsString('DT_VENCIMENTO');
    notaFiscal.FIdCentroResultado := AVenda.GetAsInteger('ID_CENTRO_RESULTADO');
    notaFiscal.FIdContaAnalise := AVenda.GetAsInteger('ID_CONTA_ANALISE');
    notaFiscal.FIdChaveProcesso := AVenda.GetAsInteger('ID_CHAVE_PROCESSO');
    notaFiscal.FIdOperacao := AVenda.GetAsInteger('ID_OPERACAO');
    notaFiscal.FIdFilial := AVenda.GetAsInteger('ID_FILIAL');
    notaFiscal.FIdPessoa := AVenda.GetAsInteger('ID_PESSOA');
    notaFiscal.FPercVst := AVenda.GetAsFloat('PERC_VST');
    notaFiscal.FPercVseg := AVenda.GetAsFloat('PERC_VSEG');
    notaFiscal.FPercVdesc := AVenda.GetAsFloat('PERC_VDESC');
    notaFiscal.FPercVoutro := AVenda.GetAsFloat('PERC_VOUTRO');
    notaFiscal.FPercVipi := AVenda.GetAsFloat('PERC_VIPI');
    notaFiscal.FPercVfrete := AVenda.GetAsFloat('PERC_VFRETE');
    notaFiscal.FStatus := AVenda.GetAsString('STATUS');
    notaFiscal.FIdPlanoPagamento := AVenda.GetAsInteger('ID_PLANO_PAGAMENTO');
    notaFiscal.FIdFormaPagamento := AVenda.GetAsInteger('ID_FORMA_PAGAMENTO');
    notaFiscal.FDtCompetencia := AVenda.GetAsString('DT_COMPETENCIA');
  finally
    FreeAndNil(notaFiscal);
  end;
end;

class procedure TVenda.GerarParcelas(AVenda, AVendaParcela: TGbClientDataset;
  AIdFormaPagamento: Integer = 0; AIdCarteira: Integer = 0);
var parcelamento: TParcelamento;
    parcela: TParcela;
    valorParcela, valorTitulo, diferencaNoParcelamento, totalParcelas: Double;
    i: Integer;
    formaPagamento: TFormaPagamentoProxy;
    carteira: TCarteiraProxy;
begin
  if AVenda.FieldByName('ID_PLANO_PAGAMENTO').AsString.IsEmpty then
    exit;

  totalParcelas := 0;
  diferencaNoParcelamento := 0;
  valorTitulo := AVenda.FieldByName('VL_VENDA').AsFloat - AVenda.FieldByName('VL_PAGAMENTO').AsFloat;

  parcelamento := TParcelamento.ParcelamentoBaseCadastro(AVenda.FieldByName('ID_PLANO_PAGAMENTO').AsInteger,
    Date, 1);

  if parcelamento.quantidadeParcelas <= 0 then
    Exit;

  valorParcela := TMathUtils.Arredondar( valorTitulo / parcelamento.quantidadeParcelas);

  for i := 0 to Pred(parcelamento.parcelas.Count) do
  begin
    parcela := TParcela(parcelamento.parcelas[i]);

    AVendaParcela.Incluir;
    AVendaParcela.FieldByName('DOCUMENTO').AsString :=
      'VENDA';

    AVendaParcela.FieldByName('VL_TITULO').AsFloat := valorParcela;
    AVendaParcela.FieldByName('QT_PARCELA').AsInteger := parcela.quantidadeParcelas;
    AVendaParcela.FieldByName('NR_PARCELA').AsInteger := parcela.numeroParcela;
    AVendaParcela.FieldByName('DT_VENCIMENTO').AsDateTime := parcela.dataParcela;

    if AIdFormaPagamento > 0 then
    begin
      formaPagamento := TFormaPagamento.GetFormaPagamento(AIdFormaPagamento);
      try
        AVendaParcela.FieldByName('ID_FORMA_PAGAMENTO').AsInteger :=
          formaPagamento.FId;
        AVendaParcela.FieldByName('JOIN_DESCRICAO_FORMA_PAGAMENTO').AsString :=
          formaPagamento.FDescricao;
        AVendaParcela.FieldByName('JOIN_TIPO_FORMA_PAGAMENTO').AsString :=
          formaPagamento.FTipoTipoQuitacao;
      finally
        FreeAndNil(formaPagamento);
      end;
    end;

    if AIdCarteira > 0 then
    begin
      carteira := TCarteira.GetCarteira(AIdCarteira);
      try
        AVendaParcela.FieldByName('ID_CARTEIRA').AsInteger :=
          carteira.Fid;
        AVendaParcela.FieldByName('JOIN_DESCRICAO_CARTEIRA').AsString :=
          carteira.Fdescricao;
      finally
        FreeAndNil(carteira);
      end;
    end;

    AVendaParcela.Salvar;

    totalParcelas := totalParcelas + valorParcela;
  end;

  diferencaNoParcelamento := valorTitulo - totalParcelas;

  if diferencaNoParcelamento <> 0 then
  begin
    AVendaParcela.Alterar;
    AVendaParcela.FieldByName('VL_TITULO').AsFloat :=
      AVendaParcela.FieldByName('VL_TITULO').AsFloat + diferencaNoParcelamento;
    AVendaParcela.Salvar;
  end;
end;

class function TVenda.GetId(AIdChaveProcesso: Integer): Integer;
var smVenda: TSMVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GetId(AIdChaveProcesso);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GetIdNotaFiscalNFCE(AId: Integer): Integer;
var smVenda: TSMVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GetIdNotaFiscalNFCE(AId);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GetIdNotaFiscalNFE(AId: Integer): Integer;
var smVenda: TSMVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.GetIdNotaFiscalNFE(AId);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.GetTotaisPorTipoVenda(
  const AIdsVenda: String): TgbFDMemTable;
var
  smVenda : TSMVendaClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TgbFDMemTable.Create(nil);
  smVenda := TSMVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smVenda.GetTotaisPorTipoVenda(AIdsVenda);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smVenda.Free;
  end;
end;

class procedure TVenda.IncluirBloqueiosVenda(ACdsVendaBloqueioPersonalizado: TgbClientDataset;
  const AIdChaveProcesso: Integer);
var
  smVenda : TSMVendaClient;
  JSONDataset: TFDJSONDataSets;
  fdmBloqueiosVenda: TgbFDMemTable;
begin
  fdmBloqueiosVenda := TgbFDMemTable.Create(nil);
  smVenda := TSMVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smVenda.BuscarBloqueiosVenda(TVenda.GetId(AIdChaveProcesso));
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, fdmBloqueiosVenda);

    if fdmBloqueiosVenda.IsEmpty then
    begin
      exit;
    end;

    //ACdsVendaBloqueioPersonalizado.DeletarTodosRegistros;

    fdmBloqueiosVenda.First;
    while not fdmBloqueiosVenda.Eof do
    begin
      with ACdsVendaBloqueioPersonalizado do
      begin
        if not ACdsVendaBloqueioPersonalizado.Locate('ID_BLOQUEIO_PERSONALIZADO',
          fdmBloqueiosVenda.GetAsInteger('ID'), []) then
        begin
          Incluir;
          SetAsInteger('ID_BLOQUEIO_PERSONALIZADO', fdmBloqueiosVenda.GetAsInteger('ID'));
          //SetAsInteger('ID_VENDA', AIdVenda);
          SetAsString('STATUS', TBloqueioPersonalizadoProxy.STATUS_BLOQUEADO);
          SetAsDateTime('DH_BLOQUEIO', Now);
          SetAsString('JOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO', fdmBloqueiosVenda.GetAsString('DESCRICAO'));
          Salvar;
        end;
      end;
      fdmBloqueiosVenda.Next;
    end;
  finally
    FreeAndNil(fdmBloqueiosVenda);
    smVenda.Free;
  end;
end;

class procedure TVenda.LimparParcelas(AParcelas: TgbClientDataset);
begin
  while not AParcelas.IsEmpty do
    AParcelas.Delete;
end;

class function TVenda.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
var smVenda: TSMVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.PodeEstornar(AIdChaveProcesso);
  finally
    smVenda.Free;
  end;
end;

class function TVenda.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var smVenda: TSMVendaClient;
begin
  smVenda := TsmVendaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smVenda.PodeEstornar(AIdChaveProcesso);
  finally
    smVenda.Free;
  end;
end;

end.
