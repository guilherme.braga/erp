unit uRelatorioFRFiltroVerticalVenda;

interface

Uses uFrameFiltroVerticalPadrao;

type TRelatorioFRFiltroVerticalVenda = class
  class procedure CriarFiltrosRelatorioVenda(var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

uses uVendaProxy;

class procedure TRelatorioFRFiltroVerticalVenda.CriarFiltrosRelatorioVenda(
  var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //C�digo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Valor de Venda
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_VENDA';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TVendaProxy.LISTA_STATUS;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Tipo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TVendaProxy.LISTA_TIPO;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Fechamento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Fechamento';
  campoFiltro.campo := 'DH_FECHAMENTO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Vendedor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Vendedor';
  campoFiltro.campo := 'ID_VENDEDOR';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA_USUARIO';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);
end;

end.
