unit uOperacao;

interface

type TOperacao = class
  private
    class function ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
  public
    class function ExisteAcaoNotaFiscalEntrada(AIdOperacao: Integer): Boolean;
    class function ExisteAcaoNotaFiscalSaida(AIdOperacao: Integer): Boolean;
end;

implementation

{ TOperacao }

uses uClientClasses, uAcaoProxy, uDmConnection;

class function TOperacao.ExisteAcao(AAcao: String; AIdOperacao: Integer): Boolean;
var smOperacao: TSMOperacaoClient;
begin
  smOperacao := TsmOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smOperacao.ExisteAcao(AAcao, AIdOperacao);
  finally
    smOperacao.Free;
  end;
end;

class function TOperacao.ExisteAcaoNotaFiscalEntrada(AIdOperacao: Integer): Boolean;
begin
  result := TOperacao.ExisteAcao(uAcaoProxy.NOTA_FISCAL_ENTRADA, AIdOperacao);
end;

class function TOperacao.ExisteAcaoNotaFiscalSaida(AIdOperacao: Integer): Boolean;
begin
  result := TOperacao.ExisteAcao(uAcaoProxy.NOTA_FISCAL_SAIDA, AIdOperacao);
end;

end.
