unit uGeracaoDocumento;

interface

Uses db, ugbClientDataset, SysUtils, Data.FireDACJSONReflect, uGeracaoDocumentoProxy;

type TGeracaoDocumento = class
  private
  class procedure GerarParcelaCadastro(AGeracaoDocumento,
    AGeracaoDocumentoParcela: TgbClientDataset);
  class procedure GerarParcelaRepeticao(AGeracaoDocumento,
    AGeracaoDocumentoParcela: TgbClientDataset);

  public
  //Server
  class function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
  class function PodeAlterar(AGeracaoDocumento: TDataset): Boolean;
  class function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
  class procedure GerarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy);
  class procedure EstornarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: TGeracaoDocumentoProxy);
  class function Duplicar(const AIdGeracaoDocumento: Integer): Integer;

  //Client
  class function GetDescricaoStatus(ADataset: TDataset): String;
  class procedure GerarParcelas(AGeracaoDocumento, AGeracaoDocumentoParcela: TGbClientDataset);
  class procedure LimparParcelas(AGeracaoDocumentoParcela: TgbClientDataset);

  class function GetContasReceberAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
  class function GetContasPagarAberto(AIdChaveProcesso: Integer): TFDJSONDatasets;
end;

const STATUS_ABERTO: String = 'ABERTO';
const STATUS_GERADO: String = 'GERADO';
const STATUS_CANCELADO: String = 'CANCELADO';
const TIPO_PLANO_PAGAMENTO_CADASTRO: String = 'CADASTRADO';
const TIPO_PLANO_PAGAMENTO_REPETICAO: String = 'REPETICAO';

implementation

{ TGeracaoDocumento }

uses
  uPlanoPagamento,
  uDmConnection,
  uClientClasses,
  uMathUtils,
  DateUtils,
  rest.JSON;

class function TGeracaoDocumento.Duplicar(const AIdGeracaoDocumento: Integer): Integer;
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smGeracaoDocumento.Duplicar(AIdGeracaoDocumento);
  finally
    smGeracaoDocumento.Free;
  end;
end;

class procedure TGeracaoDocumento.EstornarDocumento(AIdChaveProcesso: Integer;
  AGeracaoDocumentoProxy: TGeracaoDocumentoProxy);
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    smGeracaoDocumento.EstornarDocumento(AIdChaveProcesso, TJson.ObjectToJsonString(AGeracaoDocumentoProxy));
  finally
    smGeracaoDocumento.Free;
  end;
end;

class procedure TGeracaoDocumento.GerarDocumento(AIdChaveProcesso: Integer;
  AGeracaoDocumentoProxy: TGeracaoDocumentoProxy);
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    smGeracaoDocumento.GerarDocumento(AIdChaveProcesso, TJson.ObjectToJsonString(AGeracaoDocumentoProxy));
  finally
    smGeracaoDocumento.Free;
  end;
end;

class procedure TGeracaoDocumento.GerarParcelaCadastro(AGeracaoDocumento,
  AGeracaoDocumentoParcela: TgbClientDataset);
var parcelamento: TParcelamento;
    parcela: TParcela;
    valorParcela, valorTitulo, diferencaNoParcelamento, totalParcelas: Double;
    i: Integer;
begin
  if AGeracaoDocumento.FieldByName('ID_PLANO_PAGAMENTO').AsString.IsEmpty then
    exit;

  totalParcelas := 0;
  diferencaNoParcelamento := 0;
  valorTitulo := AGeracaoDocumento.FieldByName('VL_TITULO').AsFloat -
    AGeracaoDocumento.FieldByName('VL_ENTRADA').AsFloat;

  parcelamento := TParcelamento.ParcelamentoBaseCadastro(AGeracaoDocumento.FieldByName('ID_PLANO_PAGAMENTO').AsInteger,
    IncMonth(AGeracaoDocumento.FieldByName('DT_BASE').AsDateTime, -1), AGeracaoDocumento.FieldByName('PARCELA_BASE').AsInteger);

  valorParcela := TMathUtils.Arredondar( valorTitulo / parcelamento.quantidadeParcelas);

  for i := 0 to Pred(parcelamento.parcelas.Count) do
  begin
    parcela := TParcela(parcelamento.parcelas[i]);

    AGeracaoDocumentoParcela.Incluir;

    AGeracaoDocumentoParcela.FieldByName('DOCUMENTO').AsString :=
      AGeracaoDocumento.FieldByName('DOCUMENTO').AsString;

    AGeracaoDocumentoParcela.FieldByName('DESCRICAO').AsString :=
      AGeracaoDocumento.FieldByName('DESCRICAO').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CENTRO_RESULTADO').AsString :=
      AGeracaoDocumento.FieldByName('ID_CENTRO_RESULTADO').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CENTRO_RESULTADO').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CENTRO_RESULTADO').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CONTA_ANALISE').AsString :=
      AGeracaoDocumento.FieldByName('ID_CONTA_ANALISE').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CONTA_CORRENTE').AsString :=
      AGeracaoDocumento.FieldByName('ID_CONTA_CORRENTE').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CONTA_CORRENTE').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CONTA_CORRENTE').AsString;

    AGeracaoDocumentoParcela.FieldByName('VL_TITULO').AsFloat := valorParcela;
    AGeracaoDocumentoParcela.FieldByName('QT_PARCELA').AsInteger := parcela.quantidadeParcelas;
    AGeracaoDocumentoParcela.FieldByName('NR_PARCELA').AsInteger := parcela.numeroParcela;
    AGeracaoDocumentoParcela.FieldByName('TOTAL_PARCELAS').AsInteger := AGeracaoDocumento.FieldByName('QT_PARCELAS').AsInteger;
    AGeracaoDocumentoParcela.FieldByName('DT_VENCIMENTO').AsDateTime := parcela.dataParcela;
    AGeracaoDocumentoParcela.FieldByName('DT_COMPETENCIA').AsDateTime :=
      AGeracaoDocumento.FieldByName('DT_COMPETENCIA').AsDateTime;

    AGeracaoDocumentoParcela.SetAsDateTime('DT_DOCUMENTO',
                                           AGeracaoDocumento.FieldByName('DT_DOCUMENTO').AsDateTime);

    AGeracaoDocumentoParcela.Salvar;

    totalParcelas := totalParcelas + valorParcela;
  end;

  diferencaNoParcelamento := valorTitulo - totalParcelas;

  if diferencaNoParcelamento <> 0 then
  begin
    AGeracaoDocumentoParcela.Alterar;
    AGeracaoDocumentoParcela.FieldByName('VL_TITULO').AsFloat :=
      AGeracaoDocumentoParcela.FieldByName('VL_TITULO').AsFloat + diferencaNoParcelamento;
    AGeracaoDocumentoParcela.Salvar;
  end;
end;

class procedure TGeracaoDocumento.GerarParcelaRepeticao(AGeracaoDocumento,
    AGeracaoDocumentoParcela: TgbClientDataset);
var
  parcelamento: TParcelamento;
  parcela: TParcela;
  valorParcela, valorTitulo: Double;
  i: Integer;
  dtBase: TDateTime;
  dtCompetencia : TDateTime;
begin

  if AGeracaoDocumento.FieldByName('QT_PARCELAS').AsString.IsEmpty then
    exit;

  valorTitulo   := AGeracaoDocumento.FieldByName('VL_TITULO').AsFloat -
    AGeracaoDocumento.FieldByName('VL_ENTRADA').AsFloat;

  dtBase        := IncMonth(AGeracaoDocumento.FieldByName('DT_BASE').AsDateTime, -1);
  dtCompetencia := AGeracaoDocumento.FieldByName('DT_COMPETENCIA').AsDateTime;

  parcelamento := TParcelamento.ParcelamentoSemCadastro(AGeracaoDocumento.FieldByName('QT_PARCELAS').AsInteger,
    dtBase, AGeracaoDocumento.FieldByName('PARCELA_BASE').AsInteger);

  valorParcela := valorTitulo;

  for i := 0 to Pred(parcelamento.parcelas.Count) do
  begin
    parcela := TParcela(parcelamento.parcelas[i]);

    AGeracaoDocumentoParcela.Incluir;
    AGeracaoDocumentoParcela.FieldByName('DOCUMENTO').AsString :=
      AGeracaoDocumento.FieldByName('DOCUMENTO').AsString;

    AGeracaoDocumentoParcela.FieldByName('DESCRICAO').AsString :=
      AGeracaoDocumento.FieldByName('DESCRICAO').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CENTRO_RESULTADO').AsString :=
      AGeracaoDocumento.FieldByName('ID_CENTRO_RESULTADO').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CENTRO_RESULTADO').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CENTRO_RESULTADO').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CONTA_ANALISE').AsString :=
      AGeracaoDocumento.FieldByName('ID_CONTA_ANALISE').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE').AsString;

    AGeracaoDocumentoParcela.FieldByName('ID_CONTA_CORRENTE').AsString :=
      AGeracaoDocumento.FieldByName('ID_CONTA_CORRENTE').AsString;

    AGeracaoDocumentoParcela.FieldByName('JOIN_DESCRICAO_CONTA_CORRENTE').AsString :=
      AGeracaoDocumento.FieldByName('JOIN_DESCRICAO_CONTA_CORRENTE').AsString;

    AGeracaoDocumentoParcela.FieldByName('VL_TITULO').AsFloat := valorParcela;

    AGeracaoDocumentoParcela.FieldByName('QT_PARCELA').AsInteger := parcela.quantidadeParcelas;

    AGeracaoDocumentoParcela.FieldByName('NR_PARCELA').AsInteger := parcela.numeroParcela;

    AGeracaoDocumentoParcela.FieldByName('TOTAL_PARCELAS').AsInteger :=
      AGeracaoDocumento.FieldByName('PARCELA_BASE').AsInteger +
      AGeracaoDocumento.FieldByName('QT_PARCELAS').AsInteger - 1;

    AGeracaoDocumentoParcela.FieldByName('DT_VENCIMENTO').AsDateTime := parcela.dataParcela;

    AGeracaoDocumentoParcela.FieldByName('DT_COMPETENCIA').AsDateTime := dtCompetencia;
    dtCompetencia := IncMonth(dtCompetencia);

    AGeracaoDocumentoParcela.SetAsDateTime('DT_DOCUMENTO',
                                           AGeracaoDocumento.FieldByName('DT_DOCUMENTO').AsDateTime);


    AGeracaoDocumentoParcela.Salvar;
  end;
end;

class procedure TGeracaoDocumento.GerarParcelas(AGeracaoDocumento,
  AGeracaoDocumentoParcela: TgbClientDataset);
begin
  if AGeracaoDocumento.FieldByName('TIPO_PLANO_PAGAMENTO').AsString.Equals(TIPO_PLANO_PAGAMENTO_CADASTRO) then
  begin
    GerarParcelaCadastro(AGeracaoDocumento,
      AGeracaoDocumentoParcela);
  end
  else if AGeracaoDocumento.FieldByName('TIPO_PLANO_PAGAMENTO').AsString.Equals(TIPO_PLANO_PAGAMENTO_REPETICAO) then
  begin
    GerarParcelaRepeticao(AGeracaoDocumento,
      AGeracaoDocumentoParcela);
  end
end;

class function TGeracaoDocumento.GetContasPagarAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smGeracaoDocumento.GetContasPagarAberto(AIdChaveProcesso);
  finally
    smGeracaoDocumento.Free;
  end;
end;

class function TGeracaoDocumento.GetContasReceberAberto(
  AIdChaveProcesso: Integer): TFDJSONDatasets;
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smGeracaoDocumento.GetContasReceberAberto(AIdChaveProcesso);
  finally
    smGeracaoDocumento.Free;
  end;
end;

class function TGeracaoDocumento.GetDescricaoStatus(ADataset: TDataset): String;
begin

end;

class procedure TGeracaoDocumento.LimparParcelas(
  AGeracaoDocumentoParcela: TgbClientDataset);
begin
  while not AGeracaoDocumentoParcela.IsEmpty do
    AGeracaoDocumentoParcela.Delete;
end;

class function TGeracaoDocumento.PodeAlterar(
  AGeracaoDocumento: TDataset): Boolean;
begin
  result :=(AGeracaoDocumento.FieldByName('status').AsString = 'GERADO') or
    (AGeracaoDocumento.FieldByName('status').AsString = 'CANCELADO');
end;

class function TGeracaoDocumento.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smGeracaoDocumento.PodeEstornar(AIdChaveProcesso);
  finally
    smGeracaoDocumento.Free;
  end;
end;

class function TGeracaoDocumento.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
var smGeracaoDocumento: TSMGeracaoDocumentoClient;
begin
  smGeracaoDocumento := TsmGeracaoDocumentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smGeracaoDocumento.PodeExcluir(AIdChaveProcesso);
  finally
    smGeracaoDocumento.Free;
  end;
end;

end.
