unit uImpostoPisCofins;

interface

uses uImpostoPisCofinsProxy;

type TImpostoPisCofins = class
  class function GetImpostoPisCofins(const AIdImpostoPisCofins: Integer): TImpostoPisCofinsProxy;
end;

implementation

{ TImpostoPisCofins }

uses uClientClasses, uDmConnection, REST.JSON;

class function TImpostoPisCofins.GetImpostoPisCofins(const AIdImpostoPisCofins: Integer): TImpostoPisCofinsProxy;
var smImpostoPisCofins: TSMImpostoPisCofinsClient;
  impostoPisCofins: TImpostoPisCofinsProxy;
begin
  smImpostoPisCofins := TSMImpostoPisCofinsClient.Create(DmConnection.Connection.DBXConnection);
  try
    impostoPisCofins := TImpostoPisCofinsProxy.Create;
    impostoPisCofins := TJson.JsonToObject<TImpostoPisCofinsProxy>(
      smImpostoPisCofins.GetImpostoPisCofins(AIdImpostoPisCofins));
    result := impostoPisCofins;
  finally
    smImpostoPisCofins.Free;
  end;
end;

end.
