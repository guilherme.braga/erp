unit uPlanoConta;

interface

uses FireDAC.Comp.Client, Data.FireDACJSONReflect, Classes;

type TPlanoConta = class
  public
    class function GetIDNivel(const ANivel: Integer; ASequencia: String): Integer;
    class procedure SetPlanoContaCompleto(AFDMemTable: TFDMemTable);
    class procedure SetPlanoContaPorCentroResultado(AFDMemTable: TFDMemTable;
      AIdCentroResultado: integer);
    class procedure SetIDPlanoContaAssociado;
    class function GetDescricaoContaAnalise(const AID: Integer): String;
    class function GetDescricaoCentroResultado(const AID: Integer): String;
end;

type TExtratoPlanoConta = class
  class function SetCentroResultados(AFDMemTable: TFDMemTable): TFDJSONDataSets;
  class function SetPrimeiroNivel(AFDMemTable: TFDMemTable; const ANivel: Integer;
    const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
  class function SetNivelAninhado(AFDMemTable: TFDMemTable; const ANivel: Integer; const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
  class function SetMovimentacao(AFDMemTable: TFDMemTable; const ACentrosResultados, APlanosContas, AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal: String): TFDJSONDataSets;
  class function SetContasCorrentes(AFDMemTable: TFDMemTable; const ACentrosResultados,
    APlanosContas, AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
  class procedure SetValores(AFDMemTable: TFDMemTable; const ANivel: Integer; const ADataInicial, ADataFinal: String; AStringAGGCentroResultado, AStringAGGContaAnalise: String);
  class function BuscarValorPrevisao(const ANivel: Integer; const APeriodoPrevisaoInicial,
    APeriodoPrevisaoFinal: String; const AIdCentroResultado, AIdContaAnalise: Integer): Double;
  class procedure SalvarPrevisao(AIdCentroResultado, AIdContaAnalise: Integer; APeriodo: String; AValor: Double);
end;

implementation

{ TPlanoConta }

uses uStringUtils, uClientClasses, uDmConnection, SysUtils, uDatasetUtils,
  uDateUtils, uTFunction;

class procedure TPlanoConta.SetIDPlanoContaAssociado;
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    smFinanceiro.SetIDPlanoContaAssociado;
  finally
    smFinanceiro.Free;
  end;
end;

class function TPlanoConta.GetDescricaoCentroResultado(
  const AID: Integer): String;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetDescricaoCentroResultado(AID);
  finally
    smFinanceiro.Free;
  end;
end;

class function TPlanoConta.GetDescricaoContaAnalise(const AID: Integer): String;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetDescricaoContaAnalise(AID);
  finally
    smFinanceiro.Free;
  end;
end;

class function TPlanoConta.GetIDNivel(const ANivel: Integer; ASequencia: String): Integer;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetIDNivel(ANivel, ASequencia);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TPlanoConta.SetPlanoContaCompleto(AFDMemTable: TFDMemTable);
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetPlanoContaCompleto;
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TPlanoConta.SetPlanoContaPorCentroResultado(
  AFDMemTable: TFDMemTable; AIdCentroResultado: integer);
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetPlanoContaPorCentroResultado(AIdCentroResultado);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

{ TExtratoPlanoConta }

class function TExtratoPlanoConta.BuscarValorPrevisao(const ANivel: Integer;
  const APeriodoPrevisaoInicial, APeriodoPrevisaoFinal: String;
  const AIdCentroResultado, AIdContaAnalise: Integer): Double;
var
  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.BuscarValorPrevisao(ANivel, APeriodoPrevisaoInicial,
      APeriodoPrevisaoFinal, AIdCentroResultado, AIdContaAnalise);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TExtratoPlanoConta.SalvarPrevisao(AIdCentroResultado, AIdContaAnalise: Integer;
  APeriodo: String; AValor: Double);
var
  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    smFinanceiro.SalvarPrevisao(AIdCentroResultado, AIdContaAnalise, APeriodo, AValor);
  finally
    smFinanceiro.Free;
  end;
end;

class function TExtratoPlanoConta.SetCentroResultados(AFDMemTable: TFDMemTable): TFDJSONDataSets;
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetCentroResultados;
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TExtratoPlanoConta.SetContasCorrentes(AFDMemTable: TFDMemTable; const ACentrosResultados,
  APlanosContas, AListaCamposPeriodo: String; ADtInicial, ADtFinal: String): TFDJSONDatasets;
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetContasCorrentes(ACentrosResultados,
      APlanosContas, AListaCamposPeriodo, ADtInicial, ADtFinal);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TExtratoPlanoConta.SetMovimentacao(AFDMemTable: TFDMemTable; const ACentrosResultados,
  APlanosContas, AContasCorrentes, AListaCamposPeriodo, ADtInicial,
  ADtFinal: String): TFDJSONDataSets;
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetMovimentacao(ACentrosResultados, APlanosContas,
      AContasCorrentes, AListaCamposPeriodo, ADtInicial, ADtFinal);

    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TExtratoPlanoConta.SetNivelAninhado(AFDMemTable: TFDMemTable; const ANivel: Integer;
  const ACentrosResultados, APlanosContas, AListaCamposPeriodo: String): TFDJSONDataSets;
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetNivelAninhado(ANivel, ACentrosResultados, APlanosContas, AListaCamposPeriodo);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TExtratoPlanoConta.SetPrimeiroNivel(AFDMemTable: TFDMemTable; const ANivel: Integer;
  const ACentrosResultados: String; const AListaCamposPeriodo: String): TFDJSONDataSets;
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetPrimeiroNivel(ANivel, ACentrosResultados, AListaCamposPeriodo);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TExtratoPlanoConta.SetValores(AFDMemTable: TFDMemTable; const ANivel: Integer;
  const ADataInicial, ADataFinal: String; AStringAGGCentroResultado, AStringAGGContaAnalise: String);
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    case ANivel of
      0:
      begin
        JSONDataset := smFinanceiro.GetTotalPorCentroResultado(AStringAGGCentroResultado, ADataInicial, ADataFinal);
      end;
      1,2,3,4:
      begin
        JSONDataset := smFinanceiro.GetTotalPorNivel(ANivel, AStringAGGCentroResultado, AStringAGGContaAnalise, ADataInicial, ADataFinal);
      end;
      5:
      begin
        JSONDataset := smFinanceiro.GetTotalPorContaCorrente(AStringAGGCentroResultado, AStringAGGContaAnalise, ADataInicial, ADataFinal);
      end;
    end;

    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

end.
