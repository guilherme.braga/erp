unit uTPessoa;

interface

Uses
  Forms,
  Classes,
  SysUtils,
  Windows,
  Dialogs,
  DB,
  Controls,
  FireDAC.Comp.Client,
  Data.FireDACJSONReflect,
  uPessoaProxy;

type TPessoa = class

  class function ValidaCnpjCeiCpf(Numero: String; ExibeMsgErro: Boolean = True): Boolean;
  class function GetNomePessoa(const AIDPessoa: Integer): String;
  class function GetPessoa(const AIdPessoa: Integer): TPessoaProxy;
  class procedure SetClienteConsumidorFinal(const AIdPessoa: Integer);
  class function VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
  class function GetPessoaEndereco(const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
end;

implementation

uses uFrmMessage, uClientClasses, uDmConnection, uDatasetUtils, rest.JSON;

class function TPessoa.VerificarConsumidorFinal(const AIdPessoa: Integer): Boolean;
var
  smPessoa: TSMPessoaClient;
begin
  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoa.VerificarConsumidorFinal(AIDPessoa);
  finally
    smPessoa.Free;
  end;
end;

class function TPessoa.GetNomePessoa(const AIDPessoa: Integer): String;
var
  smPessoa : TSMPessoaClient;
begin
  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoa.GetNomePessoa(AIDPessoa);
  finally
    smPessoa.Free;
  end;
end;

class function TPessoa.GetPessoa(const AIdPessoa: Integer): TPessoaProxy;
var
  smPessoa : TSMPessoaClient;
begin
  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TPessoaProxy>(smPessoa.GetPessoa(AIdPessoa));
  finally
    smPessoa.Free;
  end;
end;

class function TPessoa.GetPessoaEndereco(const AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
var
  smPessoa : TSMPessoaClient;
begin
  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoa.GetPessoaEndereco(AIdPessoaEndereco);
  finally
    //smPessoa.Free;
  end;
end;

class procedure TPessoa.SetClienteConsumidorFinal(const AIdPessoa: Integer);
var
  smPessoa : TSMPessoaClient;
begin
  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    smPessoa.SetClienteConsumidorFinal(AIdPessoa);
  finally
    smPessoa.Free;
  end;
end;

class function TPessoa.ValidaCnpjCeiCpf(Numero: String; ExibeMsgErro: Boolean = True): Boolean;
var
  smPessoa : TSMPessoaClient;
  MsgDialogo: String;
  DocMsg: String;
begin
  case Length(Numero) of
    11: DocMsg:= 'CPF';
    14: DocMsg:= 'CNPJ';
    12: DocMsg:= 'CEI';
  end;

  smPessoa := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smPessoa.ValidaCnpjCeiCpf(Numero);
  finally
    smPessoa.Free;
  end;

  if (not Result) and (ExibeMsgErro) then
  begin
    MsgDialogo:= 'O n�mero do '+DocMsg+' digitado � inv�lido!';
    TFrmMessage.Information(MsgDialogo);
  end;
end;

end.
