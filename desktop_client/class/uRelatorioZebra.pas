unit uRelatorioZebra;

interface

Uses db;

type TRelatorioZebra = class
  class procedure Imprimir(cdsRelatorioFR, cdsDados: TDataset);
  class function BuscarNomeArquivoTXT: String;
  class function BuscarNomeArquivoBAT: String;
  class procedure DeletarArquivosTemporarios;
end;

implementation

Uses Classes, SysUtils, StrUtils, ShellApi, Windows, Forms;

{ TRelatorioZebra }

class function TRelatorioZebra.BuscarNomeArquivoBAT: String;
begin
  result := ExtractFilePath(Application.ExeName)+'PRINTLBL.BAT';
end;

class function TRelatorioZebra.BuscarNomeArquivoTXT: String;
begin
  result := ExtractFilePath(Application.ExeName)+'IMPRESSAOZEBRA.TXT';
end;

class procedure TRelatorioZebra.DeletarArquivosTemporarios;
begin
  if fileExists(BuscarNomeArquivoTXT) then
  begin
    DeleteFile(PwideChar(BuscarNomeArquivoTXT));
  end;

  if fileExists(BuscarNomeArquivoBAT) then
  begin
    DeleteFile(PwideChar(BuscarNomeArquivoBAT));
  end;
end;

class procedure TRelatorioZebra.Imprimir(cdsRelatorioFR, cdsDados: TDataset);
Var
  vetq: TStringList;
  s,c,c1:String;
  pi,pf,i, iQuantidade:integer;
  f:textfile;
begin
  try
    cdsDados.DisableControls;
    cdsDados.First;
    while not cdsDados.Eof do
    begin
      DeletarArquivosTemporarios;

      for iQuantidade := 1 to cdsDados.FieldByName('QUANTIDADE_ETIQUETA').AsInteger do
      begin
        vetq := TStringList.create;
        try
          vetq.Add(cdsRelatorioFR.FieldByName('ARQUIVO').AsString);

          // substitui as vari�veis delimitadas por [] pelo conte�do do campo
          for i:=0 to vetq.count - 1 do
          begin
            s := vetq.strings[i];
            pi := pos('[', s);
            pf := pos(']', s);

            while pi > 0 do
            begin
              c := copy(s, pi + 1, pf - pi - 1);
              c1 := cdsDados.FieldByName(c).AsString;
              s := StringReplace(s, '[' + c + ']', c1, [rfReplaceAll]);
              pi := pos('[', s); pf := pos(']', s);
            end;

            vetq.strings[i] := s;
          end;

          // gera arquivo texto com o conte�do que ser� enviado para a impressora
          vetq.SaveToFile(BuscarNomeArquivoTXT);

          // criar um arquivo .bat para enviar o arquivo texto gerado para a impressora atraves da porta LPT1
          // usando o comando type do DOS

          if not FileExists(TRelatorioZebra.BuscarNomeArquivoBAT) then
          begin
            AssignFile(F, TRelatorioZebra.BuscarNomeArquivoBAT);
            try
              Rewrite(F);
              Writeln(F, 'TYPE '+BuscarNomeArquivoTXT+' > '+cdsRelatorioFR.FieldByName('IMPRESSORA').AsString);
            finally
              CloseFile(F);
            end
          end;

          ShellExecute(0, 'Open', PChar(BuscarNomeArquivoBAT), nil, nil, Ord(SW_HIDE));

          // Para usar USB tem que compartilhar a impressora e enviar o arquivo para o compartilhamento
          // Ex: Type c:\etiqueta.txt > \\computador\impressora end;

          //Leia mais em: Impressora Zebra TLP 2844:
          //Como integrar seu sistema com a impressora
          //http://www.devmedia.com.br/impressorazebra-tlp-2844-como-integrar-seu-sistema-com-a-impressora/
          //9510#ixzz3zg9Kfl2l
        finally
          DeletarArquivosTemporarios;
          FreeAndNil(vetq);
        end;
      end;

      cdsDados.Next;
    end;
  finally
    cdsDados.EnableControls;
  end;
end;

end.
