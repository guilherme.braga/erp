unit uRelatorioFRFiltroVerticalEstoque;

interface

Uses uFrameFiltroVerticalPadrao;

type TRelatorioFRFiltroVerticalEstoque = class
  class procedure CriarFiltrosRelatorioEstoque(var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

uses uProdutoProxy;

class procedure TRelatorioFRFiltroVerticalEstoque.CriarFiltrosRelatorioEstoque(
  var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Grupo de Produto';
  campoFiltro.campo := 'ID_GRUPO_PRODUTO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'GRUPO_PRODUTO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Sub Grupo de Produto';
  campoFiltro.campo := 'ID_SUB_GRUPO_PRODUTO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'SUB_GRUPO_PRODUTO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Marca';
  campoFiltro.campo := 'ID_MARCA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'MARCA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Categoria';
  campoFiltro.campo := 'ID_CATEGORIA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CATEGORIA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Linha';
  campoFiltro.campo := 'ID_LINHA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'LINHA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Modelo';
  campoFiltro.campo := 'ID_MODELO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'MODELO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';

  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Unidade de Estoque';
  campoFiltro.campo := 'ID_UNIDADE_ESTOQUE';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'UNIDADE_ESTOQUE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TProdutoProxy.TIPO_PRODUTO_PRODUTO+';'+
    TProdutoProxy.TIPO_PRODUTO_SERVICO+';'+TProdutoProxy.TIPO_PRODUTO_OUTROS;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo de Barra';
  campoFiltro.campo := 'CODIGO_BARRA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);
end;

end.

