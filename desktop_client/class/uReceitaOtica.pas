unit uReceitaOtica;

interface

uses uReceitaOticaProxy;

type TReceitaOtica = class
  class function GetReceitaOtica(AIdReceitaOtica: Integer): TReceitaOticaProxy;
end;

implementation

{ TReceitaOtica }

uses uClientClasses, rest.JSON, uDmConnection;

class function TReceitaOtica.GetReceitaOtica(AIdReceitaOtica: Integer): TReceitaOticaProxy;
var
  smReceitaOtica : TSMReceitaOticaClient;
begin
  result := TReceitaOticaProxy.Create;

  smReceitaOtica := TSMReceitaOticaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TReceitaOticaProxy>(
      smReceitaOtica.GetReceitaOtica(AIdReceitaOtica));
  finally
    smReceitaOtica.Free;
  end;
end;

end.
