unit uRelatorioFR;

interface

Uses ugbClientDataset, frxClass, frxVariables, frxFDComponents,
  FireDAC.Comp.Client, Data.FireDACJSONReflect, forms, uFrameFiltroVerticalPadrao,
  REST.JSON, Generics.Collections, DBClient;

type TRelatorioFR = class
  private

  public
    class procedure VisualizarRelatorio(const AIdMaster, AIdRelatorio: Integer;
      const AWhere: String; AImpressora: String = ''; ACopias: Integer = 1);

    class procedure ImprimirRelatorio(const AIdMaster,
      AIdRelatorio: Integer; const AWhere: String; AImpressora: String = ''; ACopias: Integer = 1);

    class function BuscarRelatoriosAssociados(
      const AClasseFormulario: String; AIdUsuario: Integer): TFDMemTable;

    class function BuscarFiltrosPersonalizados(
      const AFiltrosPersonalizadosJSON: String): TClientDataset;

   const
     ACAO_VISUALIZAR = 'VISUALIZAR';
     ACAO_IMPRIMIR = 'IMPRIMIR';
     TIPO_ARQUIVO_FAST_REPORT = 'FR3';
     TIPO_ARQUIVO_ZEBRA = 'PRN';
end;

implementation

{ TRelatorioFR }

uses uClientClasses, uDmConnection, uDatasetUtils, uFrmRelatorioFR, SysUtils, uFrmMessage_Process,
  uControlsUtils;

class procedure TRelatorioFR.ImprimirRelatorio(const AIdMaster, AIdRelatorio: Integer; const AWhere: String;
  AImpressora: String = ''; ACopias: Integer = 1);
var
  smRelatorio: TsmRelatorioFRClient;
  frmRelatorioFR: TFrmRelatorioFR;
begin
  try
    TFrmMessage_Process.SendMessage('Processando impressão');
    TControlsUtils.CongelarFormulario(Application.MainForm);

    smRelatorio := TsmRelatorioFRClient.Create(DmConnection.Connection.DBXConnection);
    try
      frmRelatorioFR := TFrmRelatorioFR.Create(nil);
      try
        frmRelatorioFR.frxReport.LoadFromStream(smRelatorio.BuscarRelatorio(AIdMaster, AIdRelatorio, AWhere));
        frmRelatorioFR.frxReport.PrintOptions.Printer := AImpressora;
        frmRelatorioFR.frxReport.PrintOptions.Copies := ACopias;
        frmRelatorioFR.frxReport.PrepareReport;
        frmRelatorioFR.frxReport.PrintOptions.ShowDialog := false;
        frmRelatorioFR.frxReport.Print;
      finally
        FreeAndNil(frmRelatorioFR);
      end;
    finally
      smRelatorio.Free;
    end;
  finally
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

class procedure TRelatorioFR.VisualizarRelatorio(const AIdMaster, AIdRelatorio: Integer; const AWhere: String;
  AImpressora: String = ''; ACopias: Integer = 1);
var
  smRelatorio: TsmRelatorioFRClient;
  frmRelatorioFR: TFrmRelatorioFR;
begin
  try
    TFrmMessage_Process.SendMessage('Processando visualização');
    TControlsUtils.CongelarFormulario(Application.MainForm);
    smRelatorio := TsmRelatorioFRClient.Create(DmConnection.Connection.DBXConnection);
    try
      frmRelatorioFR := TFrmRelatorioFR.Create(nil);
      try
        frmRelatorioFR.frxReport.LoadFromStream(smRelatorio.BuscarRelatorio(AIdMaster, AIdRelatorio, AWhere));
        frmRelatorioFR.frxReport.PrintOptions.Printer := AImpressora;
        frmRelatorioFR.frxReport.PrintOptions.Copies := ACopias;

        frmRelatorioFR.frxReport.PrepareReport;
        frmRelatorioFR.frxReport.ShowPreparedReport;
      finally
        FreeAndNil(frmRelatorioFR);
      end;
    finally
      smRelatorio.Free;
    end;
  finally
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

class function TRelatorioFR.BuscarFiltrosPersonalizados(
  const AFiltrosPersonalizadosJSON: String): TClientDataset;
begin
  result :=  TDatasetUtils.ClientDatasetDataStreamToClientDataset(AFiltrosPersonalizadosJSON);
end;

class function TRelatorioFR.BuscarRelatoriosAssociados(
  const AClasseFormulario: String; AIdUsuario: Integer): TFDMemTable;
var
  smRelatorioFR : TsmRelatorioFRClient;
  JSONDataset: TFDJSONDataSets;
begin
  try
    TFrmMessage_Process.SendMessage('Pesquisando relatórios disponíveis');

    result := TFDMemTable.Create(nil);

    smRelatorioFR := TsmRelatorioFRClient.Create(DmConnection.Connection.DBXConnection);
    try
      JSONDataset := smRelatorioFR.BuscarRelatoriosAssociados(AClasseFormulario, AIdUsuario);
      TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
    finally
      smRelatorioFR.Free;
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

end.
