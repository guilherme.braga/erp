unit uDLLMP2032;

interface

Uses Sysutils, classes, windows, forms, Dialogs;

type
  TIniciaPorta = function ( Porta: AnsiString ): integer; stdcall;
  TFechaPorta = function : integer; stdcall;
  TBematechTX = function ( BufTrans: string ): integer; stdcall;
  TComandoTX = function ( BufTrans: AnsiString; TamBufTrans: integer ): integer; stdcall;
  TCaracterGrafico = function ( BufTrans: AnsiString; TamBufTrans: integer ): integer; stdcall;
  TDocumentInserted = function : integer; stdcall;
  TLe_Status = function : integer; stdcall;
  TAutenticaDoc = function ( texto: AnsiString; tempo: integer ): integer; stdcall;
  TLe_Status_Gaveta = function : integer; stdcall;
  TConfiguraTamanhoExtrato = function ( NumeroLinhas: Integer ): integer; stdcall;
  THabilitaExtratoLongo = function ( Flag: Integer ): integer; stdcall;
  THabilitaEsperaImpressao = function ( Flag: Integer ): integer; stdcall;
  TEsperaImpressao = function : integer; stdcall;
  TConfiguraModeloImpressora = function ( ModeloImpressora: integer ): integer; stdcall;
  TAcionaGuilhotina = function ( Modo: integer ): integer; stdcall;
  TFormataTX = function  (BufTras: AnsiString; TpoLtra: integer; Italic: integer; Sublin: integer; expand: integer; enfat: integer ): integer; stdcall;
  THabilitaPresenterRetratil = function ( iFlag: integer ): integer; stdcall;
  TProgramaPresenterRetratil = function ( iTempo: integer ): integer; stdcall;
  TVerificaPapelPresenter = function : integer; stdcall;

  TConfiguraCodigoBarras = function ( Altura: integer; Largura: integer; PosicaoCaracteres: integer; Fonte: integer; Margem: integer ): integer; stdcall;

  TImprimeCodigoBarrasUPCA = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasUPCE = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasEAN13 = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasEAN8 = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasCODE39 = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasCODE93 = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasCODE128 = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasITF = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasCODABAR = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasISBN = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasMSI = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasPLESSEY = function ( Codigo: string ): integer; stdcall;
  TImprimeCodigoBarrasPDF417 = function ( NivelCorrecaoErros: integer; Altura: integer; Largura: integer; Colunas: integer; Codigo: string ): integer; stdcall;

  TImprimeBitmap = function ( name: AnsiString; mode: integer): integer; stdcall;
  TImprimeBmpEspecial = function ( name: AnsiString; xScale: integer; yScale: integer; angle: integer): integer; stdcall;
  TAjustaLarguraPapel = function ( width: integer): integer; stdcall;
  TSelectDithering = function ( mode: integer): integer; stdcall;

  TPrinterReset = function : integer; stdcall;
  TLeituraStatusEstendido = function ( A: array of byte ): integer; stdcall;
  TIoControl = function ( flag: Integer; mode : Boolean ): integer; stdcall;

type TBema32 = class
  private
    handleDLL: cardinal;
    fIniciaPorta: TIniciaPorta;
    fFechaPorta: TFechaPorta;
    fBematechTX: TBematechTX;
    fComandoTX: TComandoTX;
    fCaracterGrafico: TCaracterGrafico;
    fDocumentInserted: TDocumentInserted;
    fLe_Status: TLe_Status;
    fAutenticaDoc: TAutenticaDoc;
    fLe_Status_Gaveta: TLe_Status_Gaveta;
    fConfiguraTamanhoExtrato: TConfiguraTamanhoExtrato;
    fHabilitaExtratoLongo: THabilitaExtratoLongo;
    fHabilitaEsperaImpressao: THabilitaEsperaImpressao;
    fEsperaImpressao: TEsperaImpressao;
    fConfiguraModeloImpressora: TConfiguraModeloImpressora;
    fAcionaGuilhotina: TAcionaGuilhotina;
    fFormataTX: TFormataTX;
    fHabilitaPresenterRetratil: THabilitaPresenterRetratil;
    fProgramaPresenterRetratil: TProgramaPresenterRetratil;
    fVerificaPapelPresenter: TVerificaPapelPresenter;

    // Fun��o para Configura��o dos C�digos de Barras
    fConfiguraCodigoBarras: TConfiguraCodigoBarras;

    // Fun��es para impress�o dos c�digos de barras
    fImprimeCodigoBarrasUPCA: TImprimeCodigoBarrasUPCA;
    fImprimeCodigoBarrasUPCE: TImprimeCodigoBarrasUPCE;
    fImprimeCodigoBarrasEAN13: TImprimeCodigoBarrasEAN13;
    fImprimeCodigoBarrasEAN8: TImprimeCodigoBarrasEAN8;
    fImprimeCodigoBarrasCODE39: TImprimeCodigoBarrasCODE39;
    fImprimeCodigoBarrasCODE93: TImprimeCodigoBarrasCODE93;
    fImprimeCodigoBarrasCODE128: TImprimeCodigoBarrasCODE128;
    fImprimeCodigoBarrasITF: TImprimeCodigoBarrasITF;
    fImprimeCodigoBarrasCODABAR: TImprimeCodigoBarrasCODABAR;
    fImprimeCodigoBarrasISBN: TImprimeCodigoBarrasISBN;
    fImprimeCodigoBarrasMSI: TImprimeCodigoBarrasMSI;
    fImprimeCodigoBarrasPLESSEY: TImprimeCodigoBarrasPLESSEY;
    fImprimeCodigoBarrasPDF417: TImprimeCodigoBarrasPDF417;

    // Fun��es para impress�o de BitMap
    fImprimeBitmap: TImprimeBitmap;
    fImprimeBmpEspecial: TImprimeBmpEspecial;
    fAjustaLarguraPapel: TAjustaLarguraPapel;
    fSelectDithering: TSelectDithering;

    fPrinterReset: TPrinterReset;
    fLeituraStatusEstendido: TLeituraStatusEstendido;
    fIoControl: TIoControl;

  public
    procedure LoadDLL(ADLLName, APorta: AnsiString);
    procedure UnLoadDLL;
    function IniciaPorta ( Porta: AnsiString ): integer;
    function FechaPorta: integer;
    function BematechTX ( BufTrans: string ): integer;
    function ComandoTX ( BufTrans: string; TamBufTrans: integer ): integer;
    function CaracterGrafico ( BufTrans: string; TamBufTrans: integer ): integer;
    function DocumentInserted: integer;
    function Le_Status: integer;
    function AutenticaDoc ( texto: string; tempo: integer ): integer;
    function Le_Status_Gaveta: integer;
    function ConfiguraTamanhoExtrato( NumeroLinhas: Integer ): integer;
    function HabilitaExtratoLongo( Flag: Integer ): integer;
    function HabilitaEsperaImpressao( Flag: Integer ): integer;
    function EsperaImpressao: integer;
    function ConfiguraModeloImpressora( ModeloImpressora: integer ): integer;
    function AcionaGuilhotina( Modo: integer ): integer;
    function FormataTX(BufTras: string; TpoLtra: integer; Italic: integer; Sublin: integer; expand: integer; enfat: integer ): integer;
    function HabilitaPresenterRetratil( iFlag: integer ): integer;
    function ProgramaPresenterRetratil( iTempo: integer ): integer;
    function VerificaPapelPresenter: integer;

    function ConfiguraCodigoBarras( Altura: integer; Largura: integer; PosicaoCaracteres: integer; Fonte: integer; Margem: integer ): integer;

    function ImprimeCodigoBarrasUPCA( Codigo: string ): integer;
    function ImprimeCodigoBarrasUPCE( Codigo: string ): integer;
    function ImprimeCodigoBarrasEAN13( Codigo: string ): integer;
    function ImprimeCodigoBarrasEAN8( Codigo: string ): integer;
    function ImprimeCodigoBarrasCODE39( Codigo: string ): integer;
    function ImprimeCodigoBarrasCODE93( Codigo: string ): integer;
    function ImprimeCodigoBarrasCODE128( Codigo: string ): integer;
    function ImprimeCodigoBarrasITF( Codigo: string ): integer;
    function ImprimeCodigoBarrasCODABAR( Codigo: string ): integer;
    function ImprimeCodigoBarrasISBN( Codigo: string ): integer;
    function ImprimeCodigoBarrasMSI( Codigo: string ): integer;
    function ImprimeCodigoBarrasPLESSEY( Codigo: string ): integer;
    function ImprimeCodigoBarrasPDF417( NivelCorrecaoErros: integer; Altura: integer; Largura: integer; Colunas: integer; Codigo: string ): integer;

    function ImprimeBitmap( name: string; mode: integer): integer;
    function ImprimeBmpEspecial( name: string; xScale: integer; yScale: integer; angle: integer): integer;
    function AjustaLarguraPapel( width: integer): integer;
    function SelectDithering( mode: integer): integer;

    function PrinterReset: integer;
    function LeituraStatusEstendido( A: array of byte ): integer;
    function IoControl( flag: Integer; mode : Boolean ): integer;
end;

implementation

{ TBema32 }

function TBema32.AcionaGuilhotina(Modo: integer): integer;
begin
	Result := -1;
  if Assigned (fAcionaGuilhotina) then
    Result := fAcionaGuilhotina(Modo);
end;

function TBema32.AjustaLarguraPapel(width: integer): integer;
begin
	Result := -1;
  if Assigned (fAjustaLarguraPapel) then
    Result := fAjustaLarguraPapel(width);
end;

function TBema32.AutenticaDoc(texto: string; tempo: integer): integer;
begin
	Result := -1;
  if Assigned (fAutenticaDoc) then
    Result := fAutenticaDoc(texto, tempo);
end;

function TBema32.BematechTX(BufTrans: string): integer;
begin
	Result := -1;
  if Assigned (fBematechTX) then
    Result := fBematechTX(BufTrans);
end;

function TBema32.CaracterGrafico(BufTrans: string;
  TamBufTrans: integer): integer;
begin
	Result := -1;
  if Assigned (fCaracterGrafico) then
    Result := fCaracterGrafico(BufTrans, TamBufTrans);
end;

function TBema32.ComandoTX(BufTrans: string; TamBufTrans: integer): integer;
begin
	Result := -1;
  if Assigned (fComandoTX) then
    Result := fComandoTX(BufTrans, TamBufTrans);
end;

function TBema32.ConfiguraCodigoBarras(Altura, Largura, PosicaoCaracteres,
  Fonte, Margem: integer): integer;
begin
	Result := -1;
  if Assigned (fConfiguraCodigoBarras) then
    Result := fConfiguraCodigoBarras(Altura, Largura, PosicaoCaracteres, Fonte, Margem);
end;

function TBema32.ConfiguraModeloImpressora(ModeloImpressora: integer): integer;
begin
	Result := -1;
  if Assigned (fConfiguraModeloImpressora) then
    Result := fConfiguraModeloImpressora(ModeloImpressora);
end;

function TBema32.ConfiguraTamanhoExtrato(NumeroLinhas: Integer): integer;
begin
	Result := -1;
  if Assigned (fConfiguraTamanhoExtrato) then
    Result := fConfiguraTamanhoExtrato(NumeroLinhas);
end;

procedure TBema32.LoadDLL(ADLLName, APorta: AnsiString);
var nRetorno : Integer;
begin
  handleDLL := LoadLibrary(PWideChar(WideString(ADLLName)));

  if handleDLL < 32 then
    Begin
      ShowMessage( 'Retorno do HandleDLL '+IntToStr( handleDLL )+ '. Falha ao carregar a dll.' );

      abort;
    End;

  fIniciaPorta := GetProcAddress(handleDLL,'IniciaPorta');
  fFechaPorta := GetProcAddress(handleDLL,'FechaPorta');
  fBematechTX := GetProcAddress(handleDLL,'BematechTX');
  fComandoTX := GetProcAddress(handleDLL,'ComandoTX');
  fCaracterGrafico := GetProcAddress(handleDLL,'CaracterGrafico');
  fDocumentInserted := GetProcAddress(handleDLL,'DocumentInserted');
  fLe_Status := GetProcAddress(handleDLL,'Le_Status');
  fAutenticaDoc := GetProcAddress(handleDLL,'AutenticaDoc');
  fLe_Status_Gaveta := GetProcAddress(handleDLL,'Le_Status_Gaveta');
  fConfiguraTamanhoExtrato := GetProcAddress(handleDLL,'ConfiguraTamanhoExtrato');
  fHabilitaExtratoLongo := GetProcAddress(handleDLL,'HabilitaExtratoLongo');
  fHabilitaEsperaImpressao := GetProcAddress(handleDLL,'HabilitaEsperaImpressao');
  fEsperaImpressao := GetProcAddress(handleDLL,'EsperaImpressao');
  fConfiguraModeloImpressora := GetProcAddress(handleDLL,'ConfiguraModeloImpressora');
  fAcionaGuilhotina := GetProcAddress(handleDLL,'AcionaGuilhotina');
  fFormataTX := GetProcAddress(handleDLL,'FormataTX');
  fHabilitaPresenterRetratil := GetProcAddress(handleDLL,'HabilitaPresenterRetratil');
  fProgramaPresenterRetratil := GetProcAddress(handleDLL,'ProgramaPresenterRetratil');
  fVerificaPapelPresenter := GetProcAddress(handleDLL,'VerificaPapelPresenter');

  fConfiguraCodigoBarras := GetProcAddress(handleDLL,'ConfiguraCodigoBarras');

  fImprimeCodigoBarrasUPCA := GetProcAddress(handleDLL,'ImprimeCodigoBarrasUPCA');
  fImprimeCodigoBarrasUPCE := GetProcAddress(handleDLL,'ImprimeCodigoBarrasUPCE');
  fImprimeCodigoBarrasEAN13 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasEAN13');
  fImprimeCodigoBarrasEAN8 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasEAN8');
  fImprimeCodigoBarrasCODE39 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasCODE39');
  fImprimeCodigoBarrasCODE93 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasCODE93');
  fImprimeCodigoBarrasCODE128 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasCODE128');
  fImprimeCodigoBarrasITF := GetProcAddress(handleDLL,'ImprimeCodigoBarrasITF');
  fImprimeCodigoBarrasCODABAR := GetProcAddress(handleDLL,'ImprimeCodigoBarrasCODABAR');
  fImprimeCodigoBarrasISBN := GetProcAddress(handleDLL,'ImprimeCodigoBarrasISBN');
  fImprimeCodigoBarrasMSI := GetProcAddress(handleDLL,'ImprimeCodigoBarrasMSI');
  fImprimeCodigoBarrasPLESSEY := GetProcAddress(handleDLL,'ImprimeCodigoBarrasPLESSEY');
  fImprimeCodigoBarrasPDF417 := GetProcAddress(handleDLL,'ImprimeCodigoBarrasPDF417');

  fImprimeBitmap := GetProcAddress(handleDLL,'ImprimeBitmap');
  fImprimeBmpEspecial := GetProcAddress(handleDLL,'ImprimeBmpEspecial');
  fAjustaLarguraPapel := GetProcAddress(handleDLL,'AjustaLarguraPapel');
  fSelectDithering := GetProcAddress(handleDLL,'SelectDithering');

  fPrinterReset := GetProcAddress(handleDLL,'PrinterReset');
  fLeituraStatusEstendido := GetProcAddress(handleDLL,'LeituraStatusEstendido');
  fIoControl := GetProcAddress(handleDLL,'IoControl');

  nRetorno := IniciaPorta(APorta);
  if nRetorno <> 1 then
    Begin
      ShowMessage( 'Retorno do IniciaPorta '+IntToStr( nRetorno )+ '. Falha ao ler a porta '+APorta+'.' );
      abort;
    end;
end;

procedure TBema32.UnLoadDLL;
begin
  FechaPorta();

  fIniciaPorta := nil;
  fFechaPorta := nil;
  fBematechTX := nil;
  fComandoTX := nil;
  fCaracterGrafico := nil;
  fDocumentInserted := nil;
  fLe_Status := nil;
  fAutenticaDoc := nil;
  fLe_Status_Gaveta := nil;
  fConfiguraTamanhoExtrato := nil;
  fHabilitaExtratoLongo := nil;
  fHabilitaEsperaImpressao := nil;
  fEsperaImpressao := nil;
  fConfiguraModeloImpressora := nil;
  fAcionaGuilhotina := nil;
  fFormataTX := nil;
  fHabilitaPresenterRetratil := nil;
  fProgramaPresenterRetratil := nil;
  fVerificaPapelPresenter := nil;

  fConfiguraCodigoBarras := nil;

  fImprimeCodigoBarrasUPCA := nil;
  fImprimeCodigoBarrasUPCE := nil;
  fImprimeCodigoBarrasEAN13 := nil;
  fImprimeCodigoBarrasEAN8 := nil;
  fImprimeCodigoBarrasCODE39 := nil;
  fImprimeCodigoBarrasCODE93 := nil;
  fImprimeCodigoBarrasCODE128 := nil;
  fImprimeCodigoBarrasITF := nil;
  fImprimeCodigoBarrasCODABAR := nil;
  fImprimeCodigoBarrasISBN := nil;
  fImprimeCodigoBarrasMSI := nil;
  fImprimeCodigoBarrasPLESSEY := nil;
  fImprimeCodigoBarrasPDF417 := nil;

  fImprimeBitmap := nil;
  fImprimeBmpEspecial := nil;
  fAjustaLarguraPapel := nil;
  fSelectDithering := nil;

  fPrinterReset := nil;
  fLeituraStatusEstendido := nil;
  fIoControl := nil;
end;

function TBema32.DocumentInserted: integer;
begin
	Result := -1;
  if Assigned (fDocumentInserted) then
    Result := fDocumentInserted();
end;

function TBema32.EsperaImpressao: integer;
begin
	Result := -1;
  if Assigned (fEsperaImpressao) then
    Result := fEsperaImpressao();
end;

function TBema32.FechaPorta: integer;
begin
	Result := -1;
  if Assigned (fFechaPorta) then
    Result := fFechaPorta();
end;

function TBema32.FormataTX(BufTras: string; TpoLtra, Italic, Sublin, expand,
  enfat: integer): integer;
begin
	Result := -1;
  if Assigned (fFormataTX) then
    Result := fFormataTX(BufTras, TpoLtra, Italic, Sublin, expand, enfat);
end;

function TBema32.HabilitaEsperaImpressao(Flag: Integer): integer;
begin
	Result := -1;
  if Assigned (fHabilitaEsperaImpressao) then
    Result := fHabilitaEsperaImpressao(Flag);
end;

function TBema32.HabilitaExtratoLongo(Flag: Integer): integer;
begin
	Result := -1;
  if Assigned (fHabilitaExtratoLongo) then
    Result := fHabilitaExtratoLongo(Flag);
end;

function TBema32.HabilitaPresenterRetratil(iFlag: integer): integer;
begin
	Result := -1;
  if Assigned (fHabilitaPresenterRetratil) then
    Result := fHabilitaPresenterRetratil(iFlag);
end;

function TBema32.ImprimeBitmap(name: string; mode: integer): integer;
begin
	Result := -1;
  if Assigned (fImprimeBitmap) then
    Result := fImprimeBitmap(name, mode);
end;

function TBema32.ImprimeBmpEspecial(name: string; xScale, yScale,
  angle: integer): integer;
begin
	Result := -1;
  if Assigned (fImprimeBmpEspecial) then
    Result := fImprimeBmpEspecial(name, xScale, yScale, angle);
end;

function TBema32.ImprimeCodigoBarrasCODABAR(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasCODABAR) then
    Result := fImprimeCodigoBarrasCODABAR(Codigo);
end;

function TBema32.ImprimeCodigoBarrasCODE128(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasCODE128) then
    Result := fImprimeCodigoBarrasCODE128(Codigo);
end;

function TBema32.ImprimeCodigoBarrasCODE39(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasCODE39) then
    Result := fImprimeCodigoBarrasCODE39(Codigo);
end;

function TBema32.ImprimeCodigoBarrasCODE93(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasCODE93) then
    Result := fImprimeCodigoBarrasCODE93(Codigo);
end;

function TBema32.ImprimeCodigoBarrasEAN13(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasEAN13) then
    Result := fImprimeCodigoBarrasEAN13(Codigo);
end;

function TBema32.ImprimeCodigoBarrasEAN8(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasEAN8) then
    Result := fImprimeCodigoBarrasEAN8(Codigo);
end;

function TBema32.ImprimeCodigoBarrasISBN(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasISBN) then
    Result := fImprimeCodigoBarrasISBN(Codigo);
end;

function TBema32.ImprimeCodigoBarrasITF(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasITF) then
    Result := fImprimeCodigoBarrasITF(Codigo);
end;

function TBema32.ImprimeCodigoBarrasMSI(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasMSI) then
    Result := fImprimeCodigoBarrasMSI(Codigo);
end;

function TBema32.ImprimeCodigoBarrasPDF417(NivelCorrecaoErros, Altura, Largura,
  Colunas: integer; Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasPDF417) then
    Result := fImprimeCodigoBarrasPDF417(NivelCorrecaoErros, Altura, Largura, Colunas, Codigo);
end;

function TBema32.ImprimeCodigoBarrasPLESSEY(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasPLESSEY) then
    Result := fImprimeCodigoBarrasPLESSEY(Codigo);
end;

function TBema32.ImprimeCodigoBarrasUPCA(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasUPCA) then
    Result := fImprimeCodigoBarrasUPCA(Codigo);
end;

function TBema32.ImprimeCodigoBarrasUPCE(Codigo: string): integer;
begin
	Result := -1;
  if Assigned (fImprimeCodigoBarrasUPCE) then
    Result := fImprimeCodigoBarrasUPCE(Codigo);
end;

function TBema32.IniciaPorta(Porta: AnsiString): integer;
begin
	Result := -1;
  if Assigned (fIniciaPorta) then
    Result := fIniciaPorta(PWideChar(WideString(Porta)));
end;

function TBema32.IoControl(flag: Integer; mode: Boolean): integer;
begin
	Result := -1;
  if Assigned (fIoControl) then
    Result := fIoControl(flag, mode);
end;

function TBema32.LeituraStatusEstendido(A: array of byte): integer;
begin
	Result := -1;
  if Assigned (fLeituraStatusEstendido) then
    Result := fLeituraStatusEstendido(A);
end;

function TBema32.Le_Status: integer;
begin
	Result := -1;
  if Assigned (fLe_Status) then
    Result := fLe_Status();
end;

function TBema32.Le_Status_Gaveta: integer;
begin
	Result := -1;
  if Assigned (fLe_Status_Gaveta) then
    Result := fLe_Status_Gaveta();
end;

function TBema32.PrinterReset: integer;
begin
	Result := -1;
  if Assigned (fPrinterReset) then
    Result := fPrinterReset();
end;

function TBema32.ProgramaPresenterRetratil(iTempo: integer): integer;
begin
	Result := -1;
  if Assigned (fProgramaPresenterRetratil) then
    Result := fProgramaPresenterRetratil(iTempo);
end;

function TBema32.SelectDithering(mode: integer): integer;
begin
	Result := -1;
  if Assigned (fSelectDithering) then
    Result := fSelectDithering(mode);
end;

function TBema32.VerificaPapelPresenter: integer;
begin
	Result := -1;
  if Assigned (fVerificaPapelPresenter) then
    Result := fVerificaPapelPresenter();
end;

end.













































