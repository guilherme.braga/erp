unit uImpostoIPI;

interface

uses uImpostoIPIProxy;

type TImpostoIPI = class
  class function GetImpostoIPI(const AIdImpostoIPI: Integer): TImpostoIPIProxy;
end;

implementation

{ TImpostoIPI }

uses uClientClasses, uDmConnection, REST.JSON;

class function TImpostoIPI.GetImpostoIPI(const AIdImpostoIPI: Integer): TImpostoIPIProxy;
var smImpostoIPI: TSMImpostoIPIClient;
  impostoIPI: TImpostoIPIProxy;
begin
  smImpostoIPI := TSMImpostoIPIClient.Create(DmConnection.Connection.DBXConnection);
  try
    impostoIPI := TImpostoIPIProxy.Create;
    impostoIPI := TJson.JsonToObject<TImpostoIPIProxy>(smImpostoIPI.GetImpostoIPI(AIdImpostoIPI));
    result := impostoIPI;
  finally
    smImpostoIPI.Free;
  end;
end;

end.
