unit uCheque;

interface

uses uChequeProxy;

type TCheque = class
  class procedure GerarUtilizacaoCheque(const AIdCheque: Integer);
  class procedure EstornarUtilizacaoCheque(const AIdCheque: Integer);
  class function GetCheque(const AIdCheque: Integer): TChequeProxy;
  class function ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
end;

implementation

{ TCheque }

uses uClientClasses, uDmConnection, REST.json;

class function TCheque.ChequeDisponivelParaUtilizacao(const AIdCheque: Integer): Boolean;
var smCheque: TsmChequeClient;
begin
  smCheque := TsmChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCheque.ChequeDisponivelParaUtilizacao(AIdCheque);
  finally
    smCheque.Free;
  end;
end;

class procedure TCheque.EstornarUtilizacaoCheque(const AIdCheque: Integer);
var smCheque: TsmChequeClient;
begin
  smCheque := TsmChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    smCheque.EstornarUtilizacaoCheque(AIdCheque);
  finally
    smCheque.Free;
  end;
end;

class procedure TCheque.GerarUtilizacaoCheque(const AIdCheque: Integer);
var smCheque: TsmChequeClient;
begin
  smCheque := TsmChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    smCheque.GerarUtilizacaoCheque(AIdCheque);
  finally
    smCheque.Free;
  end;
end;

class function TCheque.GetCheque(const AIdCheque: Integer): TChequeProxy;
var
  smCheque: TsmChequeClient;
begin
  result := TChequeProxy.Create;

  smCheque := TsmChequeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TChequeProxy>(smCheque.GetCheque(AIdCheque));
  finally
    smCheque.Free;
  end;
end;

end.
