unit uBoletoCobreBemX;

interface

Uses uCarteiraProxy, uRetornoProxy, ugbFDMemTable;

{ Atualmente essa classe gera boletos para um mesmo cedente }

type TBoletoCobreBemX = class
    FIdCedente: Integer;
    FIdsContasReceber: String;
    FEntidadeCedente: TCarteiraProxy;
    FBoletoCBX: OleVariant;
    FContasReceber: TgbFDMemTable;

    procedure InstanciarObjetoBoleto;
    procedure InstanciarEntidadeCedente;
    procedure InstanciarEntidadeContasReceber;

    procedure CarregarInformacoes;
    procedure CarregarInformacoesCedente;
    procedure CarregarInformacoesTitulos;
    procedure CarregarInformacoesEmail;
    procedure CarregarInformacoesArquivoLicensa;
    function BuscarInstrucaoCaixa: String;
    procedure ImprimirBoletos;
    procedure EnviaBoletosPorEmail;

    const scpExecutar = $00000000;
    const feeSMTPBoletoHTML = $00000000;
    const feeSMTPMensagemBoletoHTMLAnexo = $00000001;
    const feeSMTPMensagemBoletoPDFAnexo = $00000002;

  public
    class function ValidarInformacoesParaEmissaoBoleto(const AIdCarteira: Integer): TRetornoProxy;
    class function EmitirBoleto(const AidsContasReceber: String): Boolean;
    constructor Create;
    destructor Destroy;
end;

implementation

Uses ComObj, SysUtils, System.Variants, uCarteira, uContaReceber, uPessoaProxy, uFilialProxy,
  uTPessoa, uFilial, Rest.JSON;

procedure TBoletoCobreBemX.CarregarInformacoesArquivoLicensa;
begin
  FBoletoCBX.ArquivoLicencaTexto := FEntidadeCedente.FCbxArquivoLicenca;
end;

function TBoletoCobreBemX.BuscarInstrucaoCaixa: String;
var sNewInstruction, sAlteracao: String;
    posInicial, posFinal, nPositionMaster: Integer;
begin
  sNewInstruction := FEntidadeCedente.FBoletoInstrucoes;
  posInicial := 0;
  posFinal := 0;
  repeat
    nPositionMaster := Pos('#', sNewInstruction);

    if nPositionMaster > 0 then
    begin
      posInicial := nPositionMaster;
      sNewInstruction :=
        Copy(sNewInstruction, 1, Pred(posInicial)) +
        Copy(sNewInstruction, (posInicial+1), Length(sNewInstruction));

      posFinal := Pos('#', sNewInstruction);
      sNewInstruction :=
        Copy(sNewInstruction, 1, Pred(posFinal)) +
        Copy(sNewInstruction, (posFinal+1), Length(sNewInstruction));

      sAlteracao := Copy(sNewInstruction, posInicial, posFinal - posInicial);

      sNewInstruction := Copy(sNewInstruction, 1, posInicial - 1) +
                         FContasReceber.FieldByName(sAlteracao).AsString +
                         Copy(sNewInstruction, posFinal, (Length(sNewInstruction) - (posFinal-1)));
    end;
  until nPositionMaster = 0;

  result := sNewInstruction;
end;

procedure TBoletoCobreBemX.CarregarInformacoes;
begin
  CarregarInformacoesArquivoLicensa;
  CarregarInformacoesCedente;
  CarregarInformacoesEmail;
  CarregarInformacoesTitulos;
end;

procedure TBoletoCobreBemX.CarregarInformacoesTitulos;
var
  pessoa: TPessoaProxy;
  pessoaEndereco: TPessoaEnderecoProxy;
  filial: TFilialProxy;
  Boleto: Variant;
  MDados: Variant;
  {EmailSacado: Variant;}
begin
  try
    FBoletoCBX.DocumentosCobranca.Clear;

    FContasReceber.First;
    while not FContasReceber.Eof do
    begin
      try
        Boleto := FBoletoCBX.DocumentosCobranca.Add;

        pessoa := TPessoa.GetPessoa(FContasReceber.FieldByName('ID_PESSOA').AsInteger);
        pessoaEndereco := TPessoa.GetPessoaEndereco(
          FContasReceber.FieldByName('ID_PESSOA').AsInteger);
        filial := TFilial.GetFilial(FContasReceber.FieldByName('ID_FILIAL').AsInteger);

        Boleto.NumeroDocumento := FContasReceber.FieldByName('DOCUMENTO').AsString;
        Boleto.NumeroControle := FContasReceber.FieldByName('ID').AsString;
        Boleto.NomeSacado := pessoa.FNome;

        if (pessoa.FTpPessoa.Equals(TPessoaProxy.TIPO_PESSOA_JURIDICA)) then
        begin
          Boleto.CNPJSacado := pessoa.FDocFederal;
        end
        else
        begin
          Boleto.CPFSacado := pessoa.FDocFederal;
        end;

        Boleto.EnderecoSacado := pessoaEndereco.FLogradouro;
        Boleto.BairroSacado := pessoaEndereco.FBairro;
        Boleto.CidadeSacado := pessoaEndereco.FNomeCidade;
        Boleto.EstadoSacado := pessoaEndereco.FUF;
        Boleto.CepSacado := pessoaEndereco.FCep;

        Boleto.SacadorAvalista := FBoletoCBX.NomeCedente;

        Boleto.NumeroParcelaCarnet :=FContasReceber.FieldByName('NR_PARCELA').AsInteger;

        Boleto.DiasProtesto := FEntidadeCedente.FBoletoDiasProtesto;

        if FEntidadeCedente.FBoAceiteBoleto.Equals('S') then
        begin
          Boleto.Aceite := 'Sim';
        end
        else
        begin
          Boleto.Aceite := 'N�o';
        end;

        Boleto.DataDocumento := FormatDateTime('dd/mm/yyyy',
          FContasReceber.FieldByName('DT_DOCUMENTO').AsDateTime);
        Boleto.DataVencimento  := FormatDateTime('dd/mm/yyyy',
          FContasReceber.FieldByName('DT_VENCIMENTO').AsDateTime);

        if FContasReceber.FieldByName('DH_EMISSAO_BOLETO').IsNull then
        begin
          Boleto.DataProcessamento := FormatDateTime('dd/mm/yyyy', Date)
        end
        else
        begin
          Boleto.DataProcessamento := FormatDateTime('dd/mm/yyyy',
            FContasReceber.FieldByName('DH_EMISSAO_BOLETO').AsDateTime);
        end;

        Boleto.ValorDocumento := FContasReceber.FieldByName('VL_ABERTO').AsFloat +
          TContaReceber.GetValorJuros(FContasReceber.FieldByName('ID').AsInteger) +
          TContaReceber.GetValorMulta(FContasReceber.FieldByName('ID').AsInteger);

        Boleto.PercentualJurosDiaAtraso := FEntidadeCedente.FBoletoPercMoraDiaria;
        Boleto.PercentualMultaAtraso := FEntidadeCedente.FBoletoPercMulta;
        Boleto.PercentualDesconto := FEntidadeCedente.FBoletoPercDesconto;
        Boleto.ValorOutrosAcrescimos := FEntidadeCedente.FBoletoVlAcrescimo;
        Boleto.PadroesBoleto.Demonstrativo := '';
        Boleto.PadroesBoleto.IdentificacaoCedente := filial.FLogradouro + '<br>' + filial.FUF;
        Boleto.PadroesBoleto.InstrucoesCaixa := BuscarInstrucaoCaixa;

        Boleto.TipoDocumentoCobranca := FEntidadeCedente.FBoletoTipoDocumento;

        {Boleto.BancoEmiteBoleto := true;
        Boleto.NaoGerarMensagemJuros := false;
        Boleto.NaoGerarMensagemMulta := false;
        Boleto.NaoGerarMensagemProtesto := false;}


        {EmailSacado := Boleto.EnderecosEmailSacado.Add;
        EmailSacado.Nome := Boleto.NomeSacado;
        EmailSacado.Endereco := pessoa.FLogradouro;}

        if FBoletoCBX.NumeroBanco = '748-x' then
        begin
          MDados := Boleto.MeusDados.Add;
          MDados.Nome := 'NumeroBancoXMaiusculo';
          MDados.Valor := 'X'
        end;

        Boleto.ControleProcessamentoDocumento.Imprime := scpExecutar;
        Boleto.ControleProcessamentoDocumento.EnviaEmail := scpExecutar;
        //Boleto.ControleProcessamentoDocumento.GravaRemessa := scpExecutar;

        if not FContasReceber.FieldByName('BOLETO_NOSSO_NUMERO').AsString.IsEmpty then
        begin
          Boleto.NossoNumero := FContasReceber.FieldByName('BOLETO_NOSSO_NUMERO').AsString;
        end
        else
        begin
          Boleto.NossoNumero := TCarteira.BuscarProximaSequenciaNossoNumero(FEntidadeCedente.FId);
        end;

        FBoletoCBX.CalcularDadosBoletos;

        TCarteira.IncrementarBoletoNossoNumeroProximo(FEntidadeCedente.FId);
        TContaReceber.AtualizarDadosBoletoContasReceber(FContasReceber.FieldByName('ID').AsInteger,
          Boleto.NossoNumero, Boleto.LinhaDigitavel, Boleto.CodigoBarras);
      finally
        //FreeAndNil(pessoaEndereco);
        //FreeAndNil(pessoa);
        //FreeAndNil(filial);
      end;
      FContasReceber.Next;
    end;
  finally
    FreeAndNil(FContasReceber);
  end;
end;

procedure TBoletoCobreBemX.CarregarInformacoesCedente;
begin
  FBoletoCBX.CodigoAgencia := FEntidadeCedente.FCedenteAgencia;
  FBoletoCBX.NumeroContaCorrente := FEntidadeCedente.FCedenteContaCorrente;
  FBoletoCBX.CodigoCedente := FEntidadeCedente.FCedenteCodigo;

  FBoletoCBX.InicioNossoNumero := FEntidadeCedente.FBoletoNossoNumeroInicial;
  FBoletoCBX.FimNossoNumero := FEntidadeCedente.FBoletoNossoNumeroFinal;
  FBoletoCBX.ProximoNossoNumero := FEntidadeCedente.FBoletoNossoNumeroProximo;

  FBoletoCBX.LocalPagamento := FEntidadeCedente.FBoletoLocalPagamento;

  FBoletoCBX.PadroesBoleto.PadroesBoletoImpresso.LayoutBoleto := FEntidadeCedente.FBoletoLayoutBoleto;
  //FBoletoCBX.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras := cDiretorioPrograma+gera;
  FBoletoCBX.PadroesBoleto.PadroesBoletoImpresso.MargemSuperior := 1;

  {if Pos('Personalizado', FEntidadeCedente.FCedentelayout_impressao').AsString) <> 0 then
    FBoletoCBX.PadroesBoleto.PadroesBoletoImpresso.HTMLReciboPersonalizado := FEntidadeCedente.FCedenterecibo_personalizado').AsString;}

  {    if FBoletoCBX.MascaraOutroDadoConfiguracao1 <> '' then
    FBoletoCBX.OutroDadoConfiguracao1 := FEntidadeCedente.FCedenteoutro_dado_configuracao1').AsString;

  if FBoletoCBX.MascaraOutroDadoConfiguracao2 <> '' then
    FBoletoCBX.OutroDadoConfiguracao2 := FEntidadeCedente.FCedenteoutro_dado_configuracao2').AsString;}
end;

procedure TBoletoCobreBemX.CarregarInformacoesEmail;
begin
  {if (not qEmail.FieldByName('email_port').IsNull) and ((bEnviaEmail) or
    (FEntidadeCedente.FCedentestatus_sempre_envia_email').AsString = 'S')) then
  begin
    if Pos('Personalizado', FEntidadeCedente.FCedentelayout_email').AsString) <> 0 then
      CobreBemX.PadroesBoleto.PadroesBoletoImpresso.HTMLReciboPersonalizado :=
        FEntidadeCedente.FCedentehtml_recibo_personalizado').AsString;

    CobreBemX.PadroesBoleto.PadroesBoletoEmail.SMTP.Servidor := qEmail.FieldByName('email_host').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.SMTP.Porta := StrToIntDef(qEmail.FieldByName('email_port').AsString, 1);
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.SMTP.Usuario := qEmail.FieldByName('email_user').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.SMTP.Senha := qEmail.FieldByName('email_pass').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.LayoutBoletoEmail := FEntidadeCedente.FCedentelayout_email').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.URLImagensCodigoBarras := 'http://apice.zapto.org:8024/arquivos/CobreBemX/Imagens/';
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.PadroesEmail.Assunto := FEntidadeCedente.FCedenteassunto_email').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.PadroesEmail.EmailFrom.Endereco := qEmail.FieldByName('email_from').AsString;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.PadroesEmail.EmailFrom.Nome := filial.razao_social;
    CobreBemX.PadroesBoleto.PadroesBoletoEmail.PadroesEmail.FormaEnvio := feeSMTPMensagemBoletoHTMLAnexo;
  end
  else if (bEnviaEmail) or (FEntidadeCedente.FCedentestatus_sempre_envia_email').AsString = 'S') then
  begin
    ShowMessage('Envio de Email n�o esta Configurado Corretamente!' + sLineBreak +
      'Entre em Contato com o Suporte!');
    FreeAndNil(qAuxiliar);
    FreeAndNil(qEmail);
    Abort;
  end;}
end;

constructor TBoletoCobreBemX.Create;
begin
  InstanciarObjetoBoleto;
end;

destructor TBoletoCobreBemX.Destroy;
begin
  FBoletoCBX := Unassigned;

  if Assigned(FEntidadeCedente) then
  begin
    FreeAndNil(FEntidadeCedente);
  end;

  if Assigned(FContasReceber) then
  begin
    FreeAndNil(FContasReceber);
  end;
end;

procedure TBoletoCobreBemX.EnviaBoletosPorEmail;
begin
  if not FEntidadeCedente.FBoEnviaBoletoEmail.Equals('S') then
  begin
    exit;
  end;

  FBoletoCBX.EnviaBoletosPorEmail;
end;

class function TBoletoCobreBemX.EmitirBoleto(const AidsContasReceber: String): Boolean;
var
  cbx: TBoletoCobreBemX;
begin
  cbx := TBoletoCobreBemX.Create;
  try
    cbx.FIdsContasReceber := AidsContasReceber;
    cbx.InstanciarEntidadeContasReceber;
    cbx.InstanciarEntidadeCedente;
    cbx.CarregarInformacoes;
    cbx.ImprimirBoletos;
    cbx.EnviaBoletosPorEmail;
    result := true;
  finally
    FreeAndNil(cbx);
  end;
end;

procedure TBoletoCobreBemX.InstanciarEntidadeContasReceber;
begin
  FContasReceber := TContaReceber.GetQueryVariosContasReceberJSONDatasets(FIdsContasReceber);
end;

procedure TBoletoCobreBemX.ImprimirBoletos;
begin
  FBoletoCBX.ImprimeBoletos;
end;

procedure TBoletoCobreBemX.InstanciarEntidadeCedente;
begin
  FIdCedente := FContasReceber.GetAsInteger('ID_CARTEIRA');
  FEntidadeCedente := TCarteira.GetCarteira(FIDCedente);
end;

procedure TBoletoCobreBemX.InstanciarObjetoBoleto;
begin
  FBoletoCBX := CreateOleObject('CobreBemX.ContaCorrente');
end;

class function TBoletoCobreBemX.ValidarInformacoesParaEmissaoBoleto(
  const AIdCarteira: Integer): TRetornoProxy;
begin
{        if qValida.FieldByName('boleto_nr_nosso').AsInteger > FEntidadeCedente.FCedenteprox_nosso_nr').AsFloat then
        begin
          ShowMessage('Boleto Nosso Numero esta incorreto! Corrija o Cadastro de Cedente' + sLineBreak +
                      'O Numero correto �: ' + IntToStr(qValida.FieldByName('boleto_nr_nosso').AsInteger + 1));
          Abort;
        end;


  except
    Application.MessageBox('Erro ao Preencher Instru��es do Boleto!' + sLineBreak + 'Por Favor verifique os campos exibidos nas Instru��es','Aten��o', MB_APPLMODAL + MB_OK);
  end;

        }

end;

end.
