unit uTFTP;

interface

uses
  SysUtils,
  StrUtils,
  System.Classes,
  Vcl.Dialogs,
  Vcl.Forms,
  Vcl.IdAntiFreeze,
  Vcl.Controls,
  IdFTP,
  IdFTPCommon,
  IdComponent,
  cxProgressBar,
  cxBarEditItem;

type TFTP = class

  private
    FFTP: TIdFTP;
    FProgressBar: TComponent;
    FTamanhoArquivo: Integer;
    FAntiFreeze: TIdAntiFreeze;
    FPararTransferencia: Boolean;
    FprogressBarItem: TcxBarEditItem;

    procedure onFTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure onFTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure OnFTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);

    procedure SetProgressPosition(APosition : Integer);
    procedure SetProgressMaxPosition(APosition : Integer);


  public
    constructor Create( AProgress : TComponent); overload;
    //constructor Create( AProgress : TCxProgressBar); overload;
    destructor Destroy; override;

    procedure ConfigurarFTP();
    procedure EnviarArquivo(pFilePath : String);
    procedure RemoverArquivo(pFilePath : string);
    procedure Cancelar;

    function Connected: Boolean;
    function GetDirFileNames(): TStringList;

  protected
    property FTP : TIdFTP read FFTP write FFTP;
    property AntiFreeze : TIdAntiFreeze read FAntiFreeze write FAntiFreeze;
    property ProgressBar : TComponent read FProgressBar write FProgressBar;
    property progressBarItem : TcxBarEditItem read FprogressBarItem write FprogressBarItem;

    property TamanhoArquivo : Integer read FTamanhoArquivo write FTamanhoArquivo;
    property PararTransferencia : Boolean read FPararTransferencia write FPararTransferencia;

  end;

implementation

{ TFTP }

uses
  System.IniFiles,
  IdException, uIniFileClient;

constructor TFTP.Create(AProgress : TComponent);
begin
  Inherited Create;
  FTP         := TIdFTP.Create();
  AntiFreeze  := TIdAntiFreeze.Create();
  PararTransferencia := False;

  if AProgress is TcxProgressBar then
  begin
    ProgressBar := TcxProgressBar(AProgress);
  end
  else
  if AProgress is TcxBarEditItem then
  begin
    ProgressBarItem := TcxBarEditItem(AProgress);
  end;

end;

destructor TFTP.Destroy;
begin
  FTP.Free;
  AntiFreeze.Free;
  inherited Destroy;
end;

procedure TFTP.ConfigurarFTP();
begin

  FTP.AutoLogin     := True;
  FTP.Host          := IniSystemFile.ftp_host;
  FTP.Port          := IniSystemFile.ftp_port;
  FTP.Username      := IniSystemFile.ftp_username;
  FTP.Password      := IniSystemFile.ftp_password;
  FTP.ListenTimeout := IniSystemFile.ftp_listentimeout;

  FTP.UseHOST := True;
  FTP.UseMLIS := True;
  FTP.Passive := True;
  FTP.PassiveUseControlHost     := True;
  FTP.TransferType              := ftBinary;
  FTP.NATKeepAlive.UseKeepAlive := True;
  FTP.NATKeepAlive.IdleTimeMS   := IniSystemFile.ftp_idletimems;
  FTP.NATKeepAlive.IntervalMS   := IniSystemFile.ftp_intervalms;

  FTP.OnWork      := OnFTPWork;
  FTP.OnWorkBegin := onFTPWorkBegin;
  FTP.OnWorkEnd   := onFTPWorkEnd;

end;


procedure TFTP.EnviarArquivo(pFilePath : string);
var
  vFileName : string;
  vFile     : TStream;
begin

  vFile              := TFileStream.Create(pFilePath, fmOpenRead);
  vFileName          := ExtractFileName(pFilePath);
  TamanhoArquivo     := vFile.Size;
  PararTransferencia := False;

  try
    try
      FTP.Connect;
      FTP.Put(vFile, vFileName, true);
    except
      on e : EIdSilentException do
      begin
        SetProgressPosition(0);
        Exit;
      end;
      on e : EIdException  do
      begin
        SetProgressPosition(0);
        raise Exception.Create('Falha na Conex�o com o Servidor. ' +
                               'Por favor, tente novamente.' + e.Message);
      end;
      on e : Exception do
      begin
        Raise Exception.Create(e.Message);
      end;
    end;
  finally
    FTP.Disconnect;
    FreeAndNil(vFile);
  end;
end;

procedure TFTP.RemoverArquivo(pFilePath : string);
begin
  PararTransferencia := False;
  try
    try
      FTP.Connect;
      if FTP.Size(pFilePath) > 0 then
      begin
        FTP.Delete(pFilePath);
      end;
    except
      on e : Exception do
      begin
        raise Exception.Create('Falha na Conex�o com o Servidor. '+
                               'Por favor, tente novamente.');
      end;
    end;
  finally
    FTP.Disconnect;
  end;
end;

procedure TFTP.OnFTPWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  if PararTransferencia then
  begin
    PararTransferencia := False;
    SetProgressPosition(0);
    FTP.Abort;
  end;
  SetProgressPosition(AWorkCount);
  Application.ProcessMessages;
end;

procedure TFTP.onFTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  SetProgressPosition(0);
  if AWorkCountMax > 0 then
    SetProgressMaxPosition(AWorkCountMax)
  else
    SetProgressMaxPosition(TamanhoArquivo);
  Application.ProcessMessages;
end;

procedure TFTP.onFTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  SetProgressPosition(0);
  Application.ProcessMessages;
end;

function TFTP.GetDirFileNames : TStringList;
var
  Lista : TStringList;
  I : Integer;
begin
  Lista := TStringList.Create;
  try
    try
      FTP.Connect;
      FTP.List;
      For I := 0 to FTP.DirectoryListing.Count-1 do
      Begin
        Lista.Add(FTP.DirectoryListing[I].FileName);
      End;
    except
      on e:Exception do
      begin
        Raise Exception.Create(e.Message);
      end;
    end;
  finally
    FTP.Disconnect;
  end;
  result := Lista;
end;

procedure TFTP.Cancelar;
begin
  if Assigned(FTP) then
  begin
    PararTransferencia := True;
  end;
end;

function TFTP.Connected : Boolean;
begin
  if Assigned(FTP) then
  begin
    result := FTP.Connected;
  end
  else
  begin
    Result := False;
  end;
end;


procedure TFTP.SetProgressPosition(APosition : Integer);
begin
  if Assigned(ProgressBar) then
  begin
    TcxProgressBar(ProgressBar).Position := APosition;
  end
  else
  if Assigned(progressBarItem) then
  begin
    ProgressBarItem.EditValue := APosition;
  end;
end;

procedure TFTP.SetProgressMaxPosition(APosition : Integer);
begin
  if Assigned(ProgressBar) then
  begin
    TcxProgressBar(ProgressBar).Properties.Max := APosition;
  end
  else
  if Assigned(ProgressBarItem) then
  begin
    TcxProgressBarProperties(TcxBarEditItem(ProgressBarItem).Properties).Max := APosition;
  end;
end;

end.
