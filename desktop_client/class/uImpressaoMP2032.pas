unit uImpressaoMP2032;

interface

uses Windows, Forms, SysUtils, Dialogs, Classes, Printers,
     Db, DBClient, uDLLMP2032;

type TStrArr = array of string;

type
  TImpressaoMP2032 = class
  private
    bema32: TBema32;
    Fporta: AnsiString;
    Farquivo: String;
    Ftp_impressora: Integer;
    Fvariaveis_sistema: TClientDataSet;
    Fitens: TClientDataSet;

    FTipo_Impressora : Integer;
    FnomeDLL: String; // 0 - Matricial - 1 - Termica

    procedure setPorta(const Value: AnsiString);
    procedure setArquivo(const Value: String);
    procedure setTp_Impressora(const Value: Integer);
    procedure setVariaveis_Sistema(const Value: TClientDataSet);
    procedure setItens(const Value: TClientDataSet);

    function Trata_Largura_Linha(cLinha : String; nTipoLetra, nExpandido : Integer) : String;
    function getQtdeColunas(nTipoLetra, nExpandido: Integer): Integer;
    procedure SetnomeDLL(const Value: String);

    function Split(Str, Separator: string; Trash: array of string): TStrArr;
    procedure DeleteAllChars(var Str: string; const Char: string);
    function padR(const AString: AnsiString; const nLen: Integer;
      const Caracter: AnsiChar): AnsiString;
    function padL(const AString: AnsiString; const nLen: Integer;
      const Caracter: AnsiChar): AnsiString;
    function Space(Tamanho: Integer): string;
    function IIf(pCond: Boolean; pTrue, pFalse: Variant): Variant;
    function VerificaExisteDLL(const Arquivo: string): Boolean;
  public
    constructor Create(ANomeDLL, APorta: AnsiString);
    destructor Destroy; override;
    property nomeDLL: String read FnomeDLL write SetnomeDLL;
    property porta: AnsiString read Fporta write setPorta;
    property arquivo: String read Farquivo write setArquivo;
    property tp_impressora: Integer read Ftp_impressora write setTp_Impressora;
    property variaveis_sistema: TClientDataSet read Fvariaveis_sistema write setVariaveis_Sistema;
    property itens: TClientDataSet read Fitens write setItens;

    class procedure TestarDLL(ADllName, APort: AnsiString; AModel: Integer);

    procedure Imprimir;
    procedure Aciona_Gaveta;
    function Inicia_Porta: Boolean;
    procedure Fecha_Porta;
    function ValidaDll(ANameDLL: String) : Boolean;
    function Le_Status : Integer;
    function esperaImpressao: Integer;
  end;

implementation

{ TImpressaoMP2032 }

uses uTFunction, uDatasetUtils;

procedure TImpressaoMP2032.Aciona_Gaveta;
var sComando : string;
  iRetorno : Integer;
begin
  sComando := #27 + #118 + #140;
  iRetorno := bema32.ComandoTX( sComando, Length( sComando ) );
  Case iRetorno Of
    0 : MessageBox(0, '0 (zero) : Erro na comunica��o.', 'Erro', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
  End;

  iRetorno := bema32.Le_Status_Gaveta();
  Case iRetorno Of
    0 : MessageBox(0, '0 (zero) : Erro na comunica��o.', 'Erro', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
    2 : bema32.ComandoTX( sComando, Length( sComando ) );
  End;
end;

constructor TImpressaoMP2032.Create(ANomeDLL, APorta: AnsiString);
begin
  inherited Create;

  bema32 := TBema32.Create;
  bema32.LoadDLL(ANomeDLL, APorta);

  bema32.HabilitaEsperaImpressao(1);  // Habilita para aguarda final de impressao para realizar outro comando.
  setPorta(APorta);
  SetnomeDLL(ANomeDLL);
end;

destructor TImpressaoMP2032.Destroy;
begin
  inherited Destroy;
  bema32.UnloadDLL;
  bema32.free;
end;

function TImpressaoMP2032.esperaImpressao: Integer;
begin
  result := bema32.HabilitaEsperaImpressao(1);
end;

procedure TImpressaoMP2032.Fecha_Porta;
begin
  bema32.FechaPorta();
end;

function TImpressaoMP2032.padR(const AString : AnsiString; const nLen : Integer;
   const Caracter : AnsiChar) : AnsiString ;
var
  Tam: Integer;
begin
  Tam := Length(AString);
  if Tam < nLen then
    Result := StringOfChar(Caracter, (nLen - Tam)) + AString
  else
    Result := copy(AString,1,nLen) ;
end ;

function TImpressaoMP2032.padL(const AString : AnsiString; const nLen : Integer;
   const Caracter : AnsiChar) : AnsiString ;
var
  Tam: Integer;
begin
  Tam := Length(AString);
  if Tam < nLen then
    Result := AString + StringOfChar(Caracter, (nLen - Tam))
  else
    Result := copy(AString,1,nLen) ;
end ;

function TImpressaoMP2032.Space(Tamanho: Integer): string;
begin
  Result := StringOfChar(' ', Tamanho);
end;

procedure TImpressaoMP2032.Imprimir;
Var aColunas : TStrArr;
  myFile : TextFile;
  cNome_Campo, text, cLinha, cLinhaOriginal   : string;
  i, nQtdeColunas : Integer;
  cNome_Produto : Array[1..6] of String;
begin
  try
    { // Defini na Tab_Caixa qual o modelo da impressora.
      tp_impressora - RadioGroup
      0: MP-20 TH, MP-2000 CI ou MP-2000 TH
      1: MP-20 MI, MP-20 CI ou MP-20 S
      2: Blocos t�rmicos (com comunicacao serial DTR/DSR)
      3: Bloco 112 mm
      4: ThermalKiosk
      5: MP-4000 TH

      status_aciona_gaveta S/N
      status_aciona_guilhotina S/N

      porta_impressora = VC(15)  Exemplo LPT1 ou USB
    }
    If Not (FileExists(FArquivo)) then
    Begin
      MessageBox(0, PChar( 'Arquivo '+FArquivo+' n�o encontrado!' ), 'Aten��o', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
      Exit;
    End;

    If Not Inicia_Porta Then
      Abort;

    AssignFile(myFile, Farquivo);
    Reset(myFile);
    while not Eof(myFile) do
    begin
      ReadLn(myFile, text);

      if Text <> '' then
      Begin
        aColunas := split(text, '|', []);
        Case StrToInt( aColunas[0] ) Of
          1 :
          Begin
            If Length( aColunas ) <> 7 then
            Begin
              ShowMessage('N�mero de Par�metros incorretos.');
              Abort;
            End;

            cLinha := aColunas[1];

            cLinhaOriginal := cLinha;

            If Length( cLinha ) > 0 Then
            Begin
              nQtdeColunas := getQtdeColunas( StrToInt( aColunas[2] ), StrToInt( aColunas[5] ) );
              for i := 0 to Round( Length( cLinha ) / nQtdeColunas )+1 do
              Begin
                cLinha := copy( cLinhaOriginal, (i * nQtdeColunas)+1, nQtdeColunas );
                If Length( cLinha ) > 0 Then
                  bema32.FormataTX( pchar( cLinha+#10 ), StrToInt( aColunas[2] ),
                    StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );
              End;
            End
            Else
              bema32.FormataTX( pchar( cLinha+' '+#10 ), StrToInt( aColunas[2] ),
                StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );
          end;
         2  ://Itens
          begin
            If Length( aColunas ) <> 7 then
            Begin
              ShowMessage('N�mero de Par�metros da conta n�o est�o corretos.');
              Abort;
            End;

            Fitens.First;
            While Not Fitens.Eof Do
            Begin
              cLinha := aColunas[1];
              for i := 0 to Fitens.FieldCount - 1 do
              Begin
                If FItens.Fields[i].Visible Then
                Begin
                  cNome_Campo := UpperCase(FItens.Fields[i].FieldName);

                  If copy(cNome_Campo, 1, 4) = 'QUAN' Then // Trata quando for quantidade   1234567
                    cLinha := StringReplace(cLinha, '#'+cNome_Campo+'#',
                      padR(FormatFloat('##0.0##',Fitens.FieldByName(cNome_Campo).AsFloat), 7, ' '), [rfReplaceAll])
                  Else
                  If (copy(cNome_Campo, 1, 2) = 'VL') or (StrToInt( aColunas[0] ) = 3) Then // Trata quando for valor
                  begin
                    begin
                      cLinha := StringReplace(cLinha, '#'+cNome_Campo+'#',
                        padR(FormatFloat('#,##0.00', Fitens.FieldByName(cNome_Campo).AsFloat), 8, ' '), [rfReplaceAll])
                    end;
                  end
                  Else
                  Begin
                    cNome_Produto[1] := padL(copy(Fitens.FieldByName(cNome_Campo).AsString, 1, 31 ), 31, ' ');

                    cNome_Produto[2] := '';
                    If copy( Fitens.FieldByName(cNome_Campo).AsString, 34, 31) > '' Then
                      cNome_Produto[2] := Space(8)+padL(copy( Fitens.FieldByName(cNome_Campo).AsString, 32, 31), 31, ' ');

                    cNome_Produto[3] := '';
                    If copy( Fitens.FieldByName( cNome_Campo ).AsString, 68, 31 ) > '' Then
                      cNome_Produto[3] := Space(8)+padL( copy( Fitens.FieldByName( cNome_Campo ).AsString, 66, 31 ), 31, ' ' );

                    cNome_Produto[4] := '';
                    If copy( Fitens.FieldByName( cNome_Campo ).AsString, 102, 31 ) > '' Then
                      cNome_Produto[4] := Space(8)+padL( copy( Fitens.FieldByName( cNome_Campo ).AsString, 100, 31 ), 31, ' ' );

                    cNome_Produto[5] := '';
                    If copy( Fitens.FieldByName( cNome_Campo ).AsString, 136, 31 ) > '' Then
                      cNome_Produto[5] := Space(8)+padL( copy( Fitens.FieldByName( cNome_Campo ).AsString, 134, 31 ), 31, ' ' );

                    cNome_Produto[6] := '';
                    If copy( Fitens.FieldByName( cNome_Campo ).AsString, 170, 31 ) > '' Then
                      cNome_Produto[6] := Space(8)+padL( copy( Fitens.FieldByName( cNome_Campo ).AsString, 168, 31 ), 31, ' ' );

                    cLinha := StringReplace( cLinha, '#'+cNome_Campo+'#', cNome_Produto[1], [rfReplaceAll] );
                  End;
                End;
              End;

              cLinha := Trata_Largura_Linha( cLinha, StrToInt( aColunas[2] ), StrToInt( aColunas[5] ) );
              bema32.FormataTX( pchar( cLinha+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              If cNome_Produto[2] <> '' Then
                bema32.FormataTX( pchar( cNome_Produto[2]+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              If cNome_Produto[3] <> '' Then
                bema32.FormataTX( pchar( cNome_Produto[3]+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              If cNome_Produto[4] <> '' Then
                bema32.FormataTX( pchar( cNome_Produto[4]+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              If cNome_Produto[5] <> '' Then
                bema32.FormataTX( pchar( cNome_Produto[5]+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              If cNome_Produto[6] <> '' Then
                bema32.FormataTX( pchar( cNome_Produto[6]+#10 ), StrToInt( aColunas[2] ), StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );

              Fitens.Next;
            end;
          end;
         3:
          Begin
            cLinha := aColunas[1];

            for i := 0 to Fitens.FieldCount - 1 do
            Begin
              cNome_Campo := UpperCase(FItens.Fields[i].FieldName);

              cLinha := StringReplace(cLinha, '#'+cNome_Campo+'#',
                padR(FormatFloat('#,##0.00', TDatasetUtils.SomarColuna(Fitens.FieldByName(cNome_Campo))), 8, ' '), [rfReplaceAll]);
            End;

            If Length( cLinha ) > 0 Then
              bema32.FormataTX( pchar( cLinha+' '+#10 ), StrToInt( aColunas[2] ),
                StrToInt( aColunas[3] ), StrToInt( aColunas[4] ), StrToInt( aColunas[5] ), StrToInt( aColunas[6] ) );
          End;
         9: // Comando
          Begin
            If aColunas[1] = 'ACIONA_GUILHOTINA_PARCIAL' Then bema32.AcionaGuilhotina(0);
            If aColunas[1] = 'ACIONA_GUILHOTINA_TOTAL' Then   bema32.AcionaGuilhotina(1);
            If aColunas[1] = 'ACIONA_GAVETA' Then             Aciona_Gaveta;
          End;
        End;
      End;
    end;
  finally
    CloseFile(myFile);

    Fecha_Porta;

    Sleep(1000);
  end;
end;

function TImpressaoMP2032.Inicia_Porta: Boolean;
var iRetorno : Integer;
begin
  result := true;
  If Not ValidaDll(FNomeDLL) Then
    Begin
      result := False;
      Exit;
    End;

  bema32.ConfiguraModeloImpressora( Ftp_impressora ); // MP4000 TH
  iRetorno := bema32.IniciaPorta( PWideChar(WideString(Fporta)) );
  If iRetorno <= 0 Then
    Begin
      MessageBox(0, PChar( 'Problema ao abrir a porta de comunica��o <<'+FPorta+'>>!' ), 'Aten��o', MB_APPLMODAL + MB_ICONEXCLAMATION + MB_OK);
      result := False;
    End;
end;

function TImpressaoMP2032.Le_Status: Integer;
begin
  result := Le_Status();
end;

procedure TImpressaoMP2032.setArquivo(const Value: String);
begin
  Farquivo := Value;
end;

procedure TImpressaoMP2032.setItens(const Value: TClientDataSet);
begin
  Fitens := Value;
end;

procedure TImpressaoMP2032.SetnomeDLL(const Value: String);
begin
  FnomeDLL := Value;
end;

procedure TImpressaoMP2032.setPorta(const Value: AnsiString);
begin
  Fporta := Value;
end;

procedure TImpressaoMP2032.setTp_Impressora(const Value: Integer);
begin
  Ftp_impressora := Value;
end;

procedure TImpressaoMP2032.setVariaveis_Sistema(const Value: TClientDataSet);
begin
  Fvariaveis_sistema := Value;
end;

procedure TImpressaoMP2032.DeleteAllChars(var Str: string; const Char: string);
var
  ini: Integer;
  tmp_str: string;
  terminate: boolean;
begin
  tmp_str := Str;
  while not terminate do
    begin
      ini := pos(char, tmp_str);
      if ini > 0 then
        delete(tmp_str, ini, length(char))
      else
        terminate := true;
    end;
  Str := tmp_str;
end;

function TImpressaoMP2032.Split(Str, Separator: string;
  Trash: array of string): TStrArr;
var
  tmp_str: string;
  tmp_arr: TStrArr;
  i, x: integer;
begin
  tmp_str := Str;

  // Elimina o Lixo
  for i := 0 to length(Trash) - 1 do
    begin
      while pos(Trash[i], tmp_str) > 0 do
        deleteAllChars(tmp_str, Trash[i]);
    end;

  // Quebra a linha em Array coforme o caracter de quebra
  for i := 0 to length(tmp_str) - 1 do
    begin
      if length(tmp_str) > 0 then
        begin
          x := pos(Separator, tmp_str);
          if x > 0 then
            begin
              SetLength(tmp_arr, length(tmp_arr) + 1);
              tmp_arr[i] := copy(tmp_str, 1, x - 1);
              tmp_str := copy(tmp_str, x + 1, length(tmp_str));
            end
          else
            if tmp_str <> '' then
              begin
                SetLength(tmp_arr, length(tmp_arr) + 1);
                tmp_arr[i] := copy(tmp_str, 1, length(tmp_str));
                break;
              end;
        end
      else
        break;
    end;
  result := tmp_arr;
end;

function TImpressaoMP2032.IIf(pCond: Boolean; pTrue, pFalse: Variant): Variant;
begin
  if pCond then
    Result := pTrue
  else
    Result := pFalse;
end;

function TImpressaoMP2032.getQtdeColunas( nTipoLetra, nExpandido : Integer) : Integer;
var nQtdeColunas : Integer;
Begin
  case nTipoLetra of
    1 : Begin
          nQtdeColunas := iif( FTipo_Impressora = 0, 60, 64 );
        End;
    2 : Begin
          nQtdeColunas := 48;
        End;
    3 : Begin
          nQtdeColunas := iif( FTipo_Impressora = 0, 40, 48 );
        End;
  end;
  If nExpandido = 1 Then nQtdeColunas := 24;
  result := nQtdeColunas;
End;

class procedure TImpressaoMP2032.TestarDLL(ADllName, APort: AnsiString; AModel: Integer);
var bema32: TBema32;
    lLoad: Boolean;
begin
  try
    bema32 := TBema32.Create;

    If Not(FileExists(ExtractFilePath(Application.Exename) + '\' + ADllName)) Then
      Begin
        MessageBox(0, 'N�o foi encontrado o arquivo MP2032.dll.', 'Falha no Processo', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
        exit;
      End;

    bema32.LoadDLL(ADllName, AnsiString(APort));

    lLoad := true;

    MessageBox(0, 'Impressora pronta para ser utilizada!', 'Processo Conclu�do', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
  finally
    if not lLoad then
      MessageBox(0, 'Problema na comunica��o com a impressora!', 'Falha no Processo', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);

    bema32.free;
  end;
end;

function TImpressaoMP2032.Trata_Largura_Linha(cLinha : String; nTipoLetra, nExpandido : Integer) : String;
var nQtdeColunas : Integer;
Begin
  case nTipoLetra of
    1 : Begin
          nQtdeColunas := iif( FTipo_Impressora = 0, 60, 64 );
          cLinha := copy( cLinha, 1, nQtdeColunas );
        End;
    2 : Begin
          cLinha := copy( cLinha, 1, 48 );
        End;
    3 : Begin
          nQtdeColunas := iif( FTipo_Impressora = 0, 40, 48 );
          cLinha := copy( cLinha, 1, nQtdeColunas );
        End;
  end;
  If nExpandido = 1 Then cLinha := copy( cLinha, 1, 24 );
  result := cLinha;
End;

function TImpressaoMP2032.ValidaDll(ANameDLL: String): Boolean;
var cDiretorioAplicacao : String;
    cNameDLL: String;
begin
  cDiretorioAplicacao := ExtractFileDir(Application.ExeName);

  cNameDLL := ANameDLL;

  if cNameDLL = '' then
    begin
      cNameDLL := 'mp2032.dll';
      SetnomeDLL(cNameDLL);
    end;

  If Not (VerificaExisteDLL(cDiretorioAplicacao + '\' + cNameDLL)) Then
    Begin
      MessageBox(0, 'Falta a arquivo de DLL mp2032.dll correta. Check as configura��es de setor e a exist�ncia da dll no diret�rio do sistema!', 'Erro', MB_APPLMODAL + MB_ICONINFORMATION + MB_OK);
      result := False;
      exit;
    End;

  result := True;
end;

function TImpressaoMP2032.VerificaExisteDLL(const Arquivo: string): Boolean;
begin
  Result := FileExists(Arquivo);
end;

end.
