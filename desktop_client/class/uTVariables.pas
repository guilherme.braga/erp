unit uTVariables;

interface

uses
  SysUtils,
  Classes,
  Forms,
  Windows,
  DBClient,
  System.Generics.Collections,

  // Unit de Terceiros
  cxPC,
  dxBar,
  dxTabbedMDI,

  UCBase;

type TPAFINI = Record
  Serial,
  GT,
  Modelo
    :String;
End;

type TActiveTef = Record
  gerenciador_padrao: Integer;
  status_multiplos_cartoes: Boolean;
End;

type TVariables = Record

var
  // Variavel que armazena a instancia do formulário principal
  frmMain: TCustomForm;
  // Variavel que armazena o handle do formulário principal
  hfrmMain: THandle;
  // Variavel que armazena a classtype do form principal
  tfrmMain: TPersistentClass;

  //Formulario ativo anteriormente
  classeDoFormularioAntivoAnteriormente: String;
  classeDoFormularioAtivo: String;

  //Variavel que armazena a referencia de TUserControl
  UserControl: TUserControl;

  MDIManager: TdxTabbedMDIManager;

  cdsConsultaPadrao: TClientDataset;

  formInstances: TDictionary<String, TForm>;

// variavel para controlar o Enter como Tab na aplicação, no caso de True
  EnterAsTab: Boolean;

  oldResWidth, oldResHeigth, newResWidth, newResHeigth: Word; //Resolução da tela da aplicação antes de ser alterada

{$IFNDEF SERVER}

  // Variavel do Menu Principal e PageControl Principal
  pcMainSystem: TcxPageControl;
  tbLastTabSheet: TcxTabSheet;
  barMainSystem: TdxBar;

{$ENDIF}

  //Variavel do caminho da pasta de personalização do sistema
  cPathDesign: String;
  cPathSystem: String;

  // variavel usada na passagem de parametros para o lado servidor
  vOwnerData: Olevariant;

  //Usado para Controlar a mensagem do sistema
  lMessageClose: Boolean;

  { GENERICDAO GERAL DO SISTEMA}
 // genECF, genFuncionario: TGenericDAO;
  //genConsulta: TClientDataset;

  tef: TActiveTEF;

const
  // chave raiz usada nos dados persistidos no registro
  regRootKey: Cardinal = DWORD($80000002);
  // subchave usada pelo sistema
  regKeyName: string = 'Software\Business\';
  // subchaves usadas em cada modulo
  regERPName: string = 'erp\';
  regNFeName: string = 'nfe\';
  regPDVName: string = 'pdv\';
  regSrvName: string = 'srv\';
  regSpedName: string = 'sped\';
  // subchave usada para informacoes do banco de dados
  regDBName: string = 'database\';
  // nome dos valores
  regUserValue: string = 'username';
  regPassValue: string = 'password';
  regDBPath: string = 'path';

  // Variavel para definir SQL nulo
  vEmptySQL: String = 'EmptySQL';

  //Retorno das variaveis CONFIRM/CANCEL/CLOSE
  CCONFIRM: Integer = 1;
  CCANCEL: Integer = 0;
  CCLOSE: Integer = 2;
End;

var
  Variables: TVariables;

implementation

Initialization
  Variables.EnterAsTab := true;
  Variables.cdsConsultaPadrao := TClientDataset.Create(nil);
  Variables.formInstances := TDictionary<String, TForm>.Create;

end.
