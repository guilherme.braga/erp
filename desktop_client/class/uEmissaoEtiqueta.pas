unit uEmissaoEtiqueta;

interface

type TEmissaoEtiqueta = class
  class procedure InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
  class procedure LimparRegistrosDaEmissaoEtiqueta;
end;

implementation

uses uClientClasses, uProduto;

class procedure TEmissaoEtiqueta.InserirProdutoParaEmissaoEtiqueta(const AIdProduto: Integer);
begin
  TProduto.ServerMethod.InserirProdutoParaEmissaoEtiqueta(AIdProduto);
end;

class procedure TEmissaoEtiqueta.LimparRegistrosDaEmissaoEtiqueta;
begin
  TProduto.ServerMethod.LimparRegistrosDaEmissaoEtiqueta;
end;


end.
