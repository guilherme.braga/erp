unit uFormaPagamento;

interface

Uses uFormaPagamentoProxy;

type TFormaPagamento = class
  public
    class function GetDescricaoFormaPagamento(const AID: Integer): String;
    class function GetFormaPagamento(const AID: Integer): TFormaPagamentoProxy;
    class function GetCodigosFormaPagamentoCartao: String;
    class function FormaPagamentoCartao(const AIdFormaPagamento: Integer): Boolean;
    class function FormaPagamentoCheque(const AIdFormaPagamento: Integer): Boolean;
end;

implementation

{ TFormaPagamento }

uses uClientClasses, uDmConnection, REST.JSON;

class function TFormaPagamento.FormaPagamentoCheque(const AIdFormaPagamento: Integer): Boolean;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.FormaPagamentoCheque(AIdFormaPagamento);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFormaPagamento.GetCodigosFormaPagamentoCartao: String;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetCodigosFormaPagamentoCartao;
  finally
    smFinanceiro.Free;
  end;
end;

class function TFormaPagamento.GetDescricaoFormaPagamento(
  const AID: Integer): String;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetDescricaoFormaPagamento(AID);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFormaPagamento.GetFormaPagamento(
  const AID: Integer): TFormaPagamentoProxy;
var  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TFormaPagamentoProxy>(smFinanceiro.GetFormaPagamento(AID));
  finally
    smFinanceiro.Free;
  end;
end;

class function TFormaPagamento.FormaPagamentoCartao(const AIdFormaPagamento: Integer): Boolean;
var
  smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.FormaPagamentoCartao(AIdFormaPagamento);
  finally
    smFinanceiro.Free;
  end;
end;

end.
