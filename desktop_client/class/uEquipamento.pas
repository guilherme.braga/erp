unit uEquipamento;

interface

uses uEquipamentoProxy;

type TEquipamento = class
  class function GetEquipamento(const AIdEquipamento: Integer): TEquipamentoProxy;
  class function GerarEquipamento(const AEquipamento: TEquipamentoProxy): Integer;
  class function BuscarIdEquipamentoPelaDescricao(const ADescricaoEquipamento: String): Integer;
end;

implementation

uses uClientClasses, uDmConnection, REST.JSON;

{ TEquipamento }

class function TEquipamento.BuscarIdEquipamentoPelaDescricao(const ADescricaoEquipamento: String): Integer;
var
  smEquipamento : TSMEquipamentoClient;
begin
  smEquipamento := TSMEquipamentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEquipamento.BuscarIdEquipamentoPelaDescricao(ADescricaoEquipamento);
  finally
    smEquipamento.Free;
  end;
end;

class function TEquipamento.GerarEquipamento(const AEquipamento: TEquipamentoProxy): Integer;
var
  smEquipamento : TSMEquipamentoClient;
begin
  smEquipamento := TSMEquipamentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEquipamento.GerarEquipamento(AEquipamento);
  finally
    smEquipamento.Free;
  end;
end;

class function TEquipamento.GetEquipamento(const AIdEquipamento: Integer): TEquipamentoProxy;
var
  smEquipamento : TSMEquipamentoClient;
begin
  smEquipamento := TSMEquipamentoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smEquipamento.GetEquipamento(AIdEquipamento);
  finally
    //smEquipamento.Free;
  end;
end;

end.
