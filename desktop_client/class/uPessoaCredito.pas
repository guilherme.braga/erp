unit uPessoaCredito;

interface

type TPessoaCredito = class
  class function CreditoDisponivel(AIdPessoa: Integer): Double;
  class function RealizarTransacaoCredito(APessoaCredito: String): Boolean;
  class function ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
end;

implementation

{ TPessoaCredito }

uses uClientClasses, uDmConnection;

class function TPessoaCredito.CreditoDisponivel(AIdPessoa: Integer): Double;
var
  smPessoaCredito : TSMCreditoPessoaClient;
begin
  smPessoaCredito := TSMCreditoPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoaCredito.CreditoDisponivel(AIDPessoa);
  finally
    smPessoaCredito.Free;
  end;
end;

class function TPessoaCredito.ExisteMovimentacao(const AIdPessoa: Integer): Boolean;
var
  smPessoaCredito : TSMCreditoPessoaClient;
begin
  smPessoaCredito := TSMCreditoPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoaCredito.ExisteMovimentacao(AIdPessoa);
  finally
    smPessoaCredito.Free;
  end;
end;

class function TPessoaCredito.RealizarTransacaoCredito(
  APessoaCredito: String): Boolean;
var
  smPessoaCredito : TSMCreditoPessoaClient;
begin
  smPessoaCredito := TSMCreditoPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smPessoaCredito.RealizarTransacaoCredito(APessoaCredito);
  finally
    smPessoaCredito.Free;
  end;
end;

end.
