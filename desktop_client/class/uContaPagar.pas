unit uContaPagar;

interface

uses uContaPagarProxy, db, ugbFDMemTable;

type TContaPagar = class
  private

  public
    {Client}
    class procedure SetEncargosEmAberto(
      AContaPagar, AContaPagarQuitacao: TDataset);

    {Server}
    function GerarContaPagar(AContaPagar: TContaPagarMovimento) : Boolean;
    function PodeExcluir(AChaveProcesso: Integer): Boolean;
    function RemoverContaPagar(AChaveProcesso: Integer) : Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso: Integer;
      AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean;
    class function GetTotaisContaPagar(const AIdsContaPagar: String): TgbFDMemTable;
    class function PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Boolean;
    class function QuantidadeContasPagarEmAbertoParaPessoa(const AIdPessoa: Integer): Integer;
    class function ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Boolean;
    class function QuantidadeContasPagarEmAbertoParaProcesso(const AIdChaveProcesso: Integer): Integer;
end;

const
  FIELD_DOCUMENTO= 'CONTA_PAGAR.DOCUMENTO';
  FIELD_DESCRICAO = 'CONTA_PAGAR.DESCRICAO';
  FIELD_DATA_VENCIMENTO = 'CONTA_PAGAR.DT_VENCIMENTO';
  FIELD_STATUS = 'CONTA_PAGAR.STATUS';
  FIELD_VALOR_TITULO = 'CONTA_PAGAR.VL_TITULO';

  ABERTO    = 'ABERTO';
  QUITADO   = 'QUITADO';
  CANCELADO = 'CANCELADO';

implementation

uses
  uDmConnection,
  System.JSON,
  REST.Json,
  DBXJSONReflect,
  uClientClasses,
  System.SysUtils, uCarteira, uCarteiraProxy,Data.FireDACJSONReflect, uDatasetUtils;

{ TContaPagar }

function TContaPagar.GerarContaPagar(
  AContaPagar: TContaPagarMovimento): Boolean;
var
  smContaPagar   : TSMContaPagarClient;
  ContaPagarJson : string;
begin

  Result := False;
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    ContaPagarJson := TJson.ObjectToJsonString(AContaPagar);
    Result := smContaPagar.GerarContaPagar(ContaPagarJson);
  finally
    smContaPagar.Free;
  end;

end;

class function TContaPagar.GetTotaisContaPagar(const AIdsContaPagar: String): TgbFDMemTable;
var
  smContaPagar : TSMContaPagarClient;
  JSONDataset: TFDJSONDataSets;
begin
  result := TgbFDMemTable.Create(nil);
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smContaPagar.GetTotaisContaPagar(AIdsContaPagar);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, result);
  finally
    smContaPagar.Free;
  end;
end;

class procedure TContaPagar.SetEncargosEmAberto(AContaPagar,
  AContaPagarQuitacao: TDataset);
var carteira: TCarteiraProxy;
begin
  {CONTA A PAGAR N�O SETA ENCARGOS AUTOMATICOS, SOMENTE O CONTAS A RECEBER}

  Exit;


{  if AContaPagar.FieldByName('DT_VENCIMENTO').AsDateTime <= Date then
  begin
    Exit;
  end;

  carteira := TCarteira.GetCarteira(
    AContaPagar.FieldByName('ID_CARTEIRA').AsFloat);
  try
    if AContaPagar.FieldByName('PERC_MULTA').AsFloat < carteira.FPercMulta then
    begin
      AContaPagarQuitacao.FieldByName('PERC_MULTA').AsFloat :=
        carteira.FPercMulta - AContaPagar.FieldByName('PERC_MULTA').AsFloat;
    end;

    AContaPagarQuitacao.FieldByName('PERC_JUROS').AsFloat :=
      carteira.FPercJuros;
  finally
    FreeAndNil(carteira);
  end;}
end;

class function TContaPagar.PessoaPossuiContaPagarEmAberto(const AIdPessoa: Integer): Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  Result := False;
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.PessoaPossuiContaPagarEmAberto(AIdPessoa) > 0;
  finally
    smContaPagar.Free;
  end;
end;

function TContaPagar.PodeExcluir(AChaveProcesso: Integer): Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    smContaPagar.RemoverContaPagar(AChaveProcesso);
  finally
    smContaPagar.Free;
  end;
end;

class function TContaPagar.ProcessoPossuiContaPagarEmAberto(const AIdChaveProcesso: Integer): Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.ProcessoPossuiContaPagarEmAberto(AIdChaveProcesso) > 0;
  finally
    smContaPagar.Free;
  end;
end;

class function TContaPagar.QuantidadeContasPagarEmAbertoParaPessoa(const AIdPessoa: Integer): Integer;
var
  smContaPagar : TSMContaPagarClient;
begin
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.PessoaPossuiContaPagarEmAberto(AIdPessoa);
  finally
    smContaPagar.Free;
  end;
end;

class function TContaPagar.QuantidadeContasPagarEmAbertoParaProcesso(
  const AIdChaveProcesso: Integer): Integer;
var
  smContaPagar : TSMContaPagarClient;
begin
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.ProcessoPossuiContaPagarEmAberto(AIdChaveProcesso);
  finally
    smContaPagar.Free;
  end;
end;

function TContaPagar.RemoverContaPagar(AChaveProcesso: Integer): Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    smContaPagar.PodeRemover(AChaveProcesso);
  finally
    smContaPagar.Free;
  end;
end;

function TContaPagar.AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso: Integer;
  AGerarContraPartida: Boolean) : Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  Result := False;
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.AtualizarMovimentosContaCorrente(AIdContaPagar, AIdChaveProcesso,
      AGerarContraPartida);
  finally
    smContaPagar.Free;
  end;
end;

function TContaPagar.RemoverMovimentosContaCorrente(AIdContaPagar: Integer;
  AGerarContraPartida: Boolean): Boolean;
var
  smContaPagar : TSMContaPagarClient;
begin
  Result := False;
  smContaPagar := TSMContaPagarClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smContaPagar.RemoverMovimentosContaCorrente(AIdContaPagar, AGerarContraPartida);
  finally
    smContaPagar.Free;
  end;
end;

end.
