unit uStringUtils;

interface

Uses StrUtils, SysUtils;

type TStringUtils = class
  class function OcorrenciaDeTexto(S: String; C: Char): Integer;
  class function PrimeiraLetraMaiuscula(AString: String): String;
  class function RetornarDataNoFormatoMYSQL(AData: TDate): String;
end;

implementation


{ TStringUtils }

class function TStringUtils.OcorrenciaDeTexto(S: String; C: Char): Integer;
var
  N: Integer;
begin
  Result := 0;
  N := 0;
  repeat
    N := PosEx(C, S, N + 1);
    if N <> 0 then
      Inc(Result);
  until N = 0;
end;

class function TStringUtils.PrimeiraLetraMaiuscula(AString: string): string;
var
  i: integer;
  esp: boolean;
begin
  AString := LowerCase(Trim(AString));
  for i := 1 to Length(AString) do
  begin
    if i = 1 then
      AString[i] := UpCase(AString[i])
    else
      begin
        if i <> Length(AString) then
        begin
          esp := (AString[i] = ' ');
          if esp then
            AString[i+1] := UpCase(AString[i+1]);
        end;
      end;
  end;
  Result := AString;
end;

class function TStringUtils.RetornarDataNoFormatoMYSQL(AData: TDate): String;
var dia, mes, ano: word;
begin
  DecodeDate(AData, ano, mes, dia);
  result :=  InttoStr(ano)+FormatFloat('00',mes)+FormatFloat('00',dia);
end;

end.
