unit uMathUtils;

interface

uses Math;

type TMathUtils = class
  class function ValorSobrePercentual(APercentual, AValorBase: Double): Double;
  class function PercentualSobreValor(AValor, AValorBase: Double): Double;
  class function Arredondar(AValor: Double; AQuantidadeCasasDecimais: Integer = 2): Double;
end;

implementation

uses system.SysUtils;

{ TMathUtils }

class function TMathUtils.ValorSobrePercentual(APercentual,
  AValorBase: Double): Double;
begin
  result := (APercentual * AValorBase) / 100;
end;

class function TMathUtils.Arredondar(AValor: Double; AQuantidadeCasasDecimais: Integer = 2): Double;
begin
  AQuantidadeCasasDecimais := AQuantidadeCasasDecimais * -1;
  result := RoundTo(AValor, AQuantidadeCasasDecimais);
end;

class function TMathUtils.PercentualSobreValor(AValor,
  AValorBase: Double): Double;
begin
  if AValorBase = 0 then
  begin
    result := 0;
  end
  else
  begin
    result := (100/ AValorBase) * AValor;
  end;
end;

end.
