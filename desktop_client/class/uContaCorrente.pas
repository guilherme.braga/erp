unit uContaCorrente;

interface

uses
  Data.FireDACJSONReflect,
  FireDAC.Comp.Client,
  uGBClientDataset,
  uContaCorrenteProxy;

type TContaCorrenteAtivo = (todos, ativo, inativo);

type TContaCorrente = class
  private

  public
    class procedure SetContaCorrente(AFDMemTable: TFDMemTable; AContaCorrenteAtivo: TContaCorrenteAtivo);
    class function GetTableNameMovimentacao: String;
    class procedure AtualizarSaldoMovimentacao(const AIdContaCorrente: Integer);
    class function IsTransferencia(ADados: TgbClientDataset): Boolean;
    class function EstaConciliado(ADados: TgbClientDataset): Boolean;
    class function RealizarTransferencia(const AIdMovimentoOrigem,
      AIdContaCorrenteDestino: Integer): Integer; static;
    class function RegistroEstaConciliado(const AIdMovimentacao: Integer): boolean;
    class procedure ConciliarRegistro(const AIdMovimentacao: Integer);
    class procedure DesconciliarRegistro(const AIdMovimentacao: Integer);
    class function GerarMovimentoContaCorrente
      (AMovimento: TContaCorrenteMovimento): Boolean;
    class function RemoverMovimentoContaCorrente
      (AChaveProcesso: Integer): Boolean;
    class function GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
      const ADtMovimento: String): Double;
    function GerarDocumento(ADados: TgbClientDataset): Boolean;
end;

const
  //Campos
  FIELD_ID_CONTA_CORRENTE = 'CONTA_CORRENTE_MOVIMENTO.ID_CONTA_CORRENTE';
  FIELD_DOCUMENTO = 'CONTA_CORRENTE_MOVIMENTO.DOCUMENTO';
  FIELD_DATA_EMISSAO = 'CONTA_CORRENTE_MOVIMENTO.DT_EMISSAO';
  FIELD_DATA_MOVIMENTO = 'CONTA_CORRENTE_MOVIMENTO.DT_MOVIMENTO';
  FIELD_TIPO_MOVIMENTO = 'CONTA_CORRENTE_MOVIMENTO.TP_MOVIMENTO';
  FIELD_CONCILIADO = 'CONTA_CORRENTE_MOVIMENTO.BO_CONCILIADO';
  FIELD_ID = 'CONTA_CORRENTE_MOVIMENTO.ID';
  FIELD_VL_MOVIMENTO = 'CONTA_CORRENTE_MOVIMENTO.VL_MOVIMENTO';
  FIELD_DESCRICAO = 'CONTA_CORRENTE_MOVIMENTO.DESCRICAO';
  FIELD_DATA_COMPETENCIA = 'CONTA_CORRENTE_MOVIMENTO.DT_COMPETENCIA';
  FIELD_ID_CONTA_ANALISE = 'CONTA_CORRENTE_MOVIMENTO.ID_CONTA_ANALISE';
  FIELD_ID_CENTRO_RESULTADO = 'CONTA_CORRENTE_MOVIMENTO.ID_CENTRO_RESULTADO';

  //Valores
  MOVIMENTACAO_CREDITO = 'CREDITO';
  MOVIMENTACAO_DEBITO = 'DEBITO';
  CONCILIADO = 'S';
  NAO_CONCILIADO = 'N';

implementation

{ TContaCorrente }

uses
  uClientClasses,
  uDmConnection,
  uDatasetUtils,
  REST.Json;

class procedure TContaCorrente.AtualizarSaldoMovimentacao(const AIdContaCorrente: Integer);
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    smFinanceiro.AtualizarSaldoMovimentacao(AIdContaCorrente);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TContaCorrente.ConciliarRegistro(
  const AIdMovimentacao: Integer);
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    smFinanceiro.ConciliarRegistro(AIdMovimentacao);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TContaCorrente.DesconciliarRegistro(
  const AIdMovimentacao: Integer);
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    smFinanceiro.DesconciliarRegistro(AIdMovimentacao);
  finally
    smFinanceiro.Free;
  end;
end;

class function TContaCorrente.EstaConciliado(ADados: TgbClientDataset): Boolean;
begin
  result := ADados.FieldByName('bo_conciliado').AsString = 'S';
end;

class function TContaCorrente.GetSaldoAnteriorContaCorrente(const AIdContaCorrente: Integer;
  const ADtMovimento: String): Double;
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetSaldoAnteriorContaCorrente(AIdContaCorrente, ADtMovimento);
  finally
    smFinanceiro.Free;
  end;
end;

class function TContaCorrente.GetTableNameMovimentacao: String;
begin
  result := 'CONTA_CORRENTE_MOVIMENTO'
end;

class function TContaCorrente.IsTransferencia(
  ADados: TgbClientDataset): Boolean;
begin
  result := ADados.FieldByName('bo_transferencia').AsString = 'S';
end;

class function TContaCorrente.RealizarTransferencia(const AIdMovimentoOrigem,
  AIdContaCorrenteDestino: Integer): Integer;
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.RealizarTransferencia(AIdMovimentoOrigem,
      AIdContaCorrenteDestino);
  finally
    smFinanceiro.Free;
  end;
end;

class function TContaCorrente.RegistroEstaConciliado(
  const AIdMovimentacao: Integer): boolean;
var smFinanceiro: TSMFinanceiroClient;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.RegistroEstaConciliado(AIdMovimentacao);
  finally
    smFinanceiro.Free;
  end;
end;

class procedure TContaCorrente.SetContaCorrente(AFDMemTable: TFDMemTable; AContaCorrenteAtivo: TContaCorrenteAtivo);
var smFinanceiro: TSMFinanceiroClient;
    JSONDataset: TFDJSONDataSets;
const somenteAtivos: Boolean = true;
begin
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetContaCorrente(somenteAtivos);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TContaCorrente.GerarMovimentoContaCorrente(AMovimento: TContaCorrenteMovimento) : Boolean;
var
  smFinanceiro  : TSMFinanceiroClient;
  movimentoJson : string;
begin

  Result       := False;
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    movimentoJson := TJSON.ObjectToJsonString(AMovimento);
    Result := smFinanceiro.GerarMovimentoContaCorrente(movimentoJson);
  finally
    smFinanceiro.Free;
  end;

end;

class function TContaCorrente.RemoverMovimentoContaCorrente(AChaveProcesso: Integer) : Boolean;
var
  smFinanceiro  : TSMFinanceiroClient;
begin

  Result       := False;
  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  try
    Result := smFinanceiro.RemoverMovimentoContaCorrente(AChaveProcesso);
  finally
    smFinanceiro.Free;
  end;

end;

function TContaCorrente.GerarDocumento(ADados: TgbClientDataset) : Boolean;
var
  smFinanceiro  : TSMFinanceiroClient;
  movimento     : TContaCorrenteMovimento;
  movimentoJson : string;
begin

  Result       := False;

  smFinanceiro := TSMFinanceiroClient.Create(DmConnection.Connection.DBXConnection);
  Movimento    := TContaCorrenteMovimento.Create;

  Movimento.Fid                     := ADados.FieldByName('id').AsInteger;
  Movimento.FDocumento              := ADados.FieldByName('documento').AsString;
  Movimento.FDescricao              := ADados.FieldByName('descricao').AsString;
  Movimento.FObservacao             := ADados.FieldByName('observacao').AsString;
  Movimento.FDhCadastro             := ADados.FieldByName('dh_cadastro').AsString;
  Movimento.FDtEmissao              := ADados.FieldByName('dt_emissao').AsString;
  Movimento.FDtMovimento            := ADados.FieldByName('dt_movimento').AsString;
  Movimento.FVlMovimento            := ADados.FieldByName('vl_movimento').AsFloat;
  Movimento.FTpMovimento            := ADados.FieldByName('tp_movimento').AsString;
  Movimento.FBoConciliado           := ADados.FieldByName('bo_conciliado').AsString;
  Movimento.FDhConciliado           := ADados.FieldByName('dh_conciliado').AsString;
  Movimento.FVlSaldoConciliado      := ADados.FieldByName('vl_saldo_conciliado').AsFloat;
  Movimento.FVlSaldoGeral           := ADados.FieldByName('vl_saldo_geral').AsFloat;
  Movimento.FIdContaCorrente        := ADados.FieldByName('id_conta_corrente').AsInteger;
  Movimento.FIdContaAnalise         := ADados.FieldByName('id_conta_analise').AsInteger;
  Movimento.FIdCentroResultado      := ADados.FieldByName('id_centro_resultado').AsInteger;
  Movimento.FBoMovimentoOrigem      := ADados.FieldByName('bo_movimento_origem').AsString;
  Movimento.FIdMovimentoOrigem      := ADados.FieldByName('id_movimento_origem').AsInteger;
  Movimento.FBoMovimentoDestino     := ADados.FieldByName('bo_movimento_destino').AsString;
  Movimento.FIdMovimentoDestino     := ADados.FieldByName('id_movimento_destino').AsInteger;
  Movimento.FBoTransferencia        := ADados.FieldByName('bo_transferencia').AsString;
  Movimento.FIdContaCorrenteDestino := ADados.FieldByName('id_conta_corrente_destino').AsInteger;

  Movimento.FTPDocumentoOrigem      := ADados.FieldByName('tp_documento_origem').AsString;
  Movimento.FIdDocumentoOrigem      := ADados.FieldByName('id_documento_origem').AsInteger;
  Movimento.FIdChaveProcesso        := ADados.FieldByName('id_chave_processo').AsInteger;

  try
    movimentoJson := TJSON.ObjectToJsonString(movimento);
    Result := smFinanceiro.GerarDocumento(movimentoJson);
  finally
    smFinanceiro.Free;
  end;

end;

end.
