unit uTUsuario;

interface

Uses cxGridDBBandedTableView, System.IniFiles, Windows, Classes, SysUtils, Forms,
     cxGridCustomView, db, Data.DBXJSONReflect, Contnrs, Data.DBXJSON, System.JSON,
     Data.DBXJSONCommon, System.Generics.Collections, uUsuarioProxy,
     Data.FireDACJSONReflect, FireDAC.Comp.Client;

type TUsuario = class
  class function GetUsuario(AIdUsuario: Integer): TUsuarioProxy;
  class function GetIdUsuario(const AUsuario: String): Integer;
end;

type TUsuarioPesquisaPadrao = class
  class procedure SetConsultaAutomatica(const ACodigoUsuario: Integer; const AFormName: String; const AHabilitar: Boolean);
  class function GetConsultaAutomatica(const ACodigoUsuario: Integer; const AFormName: String): Boolean;
end;

type TUsuarioGridView = class
  private
    //Seta no ponteiro do GridView os r�tulos de acordo com o que foi salvo no banco de dados
    procedure SetCaptionColumns(AGridView: TcxGridDBBandedTableView; AListFields: TStringList);

    //Devolve a string do arquivo referente aos captions dos fields
    function GetFieldsFile(AGridView: TcxGridDBBandedTableView): String;

    //Devolve o nome do formulario que sera salvo no banco de dados para recupera��o posterior
    function GetFormName(AGridView: TcxGridDBBandedTableView; AFormName: String): String;

    //Devolve o caminho do arquivo se fossem criados fisicamente, s� para que o MemIniFile funcione
    function GetFileFieldsTemp: String;
    function GetFileGridTemp: String;

    //Contantes com os nomes dos arquivos tempor�rios
    const CTempGridFile: String = 'temp_grid_view.ini';
    const CTempFieldsFile: String = 'temp_fields.ini';
    const DelimitadorSeparacaoField: String = '=';
  public
    constructor Create;
    destructor Destroy; override;

    //Salva a vis�o da grid por usuario
    class procedure SaveGridView(AGridView: TcxGridDBBandedTableView; ACodigoUsuario: Integer; AFormName: String);

    //Recupera a vis�o da grid por usuario
    class procedure LoadGridView(AGridView: TcxGridDBBandedTableView; ACodigoUsuario: Integer; AFormName: String);

    //Deleta a vis�o do banco de dados
    class procedure DeleteView(const ACodigoUsuario: Integer; AFormName: String);
end;

implementation

{ TUsuarioGridView }

uses uDmConnection, uDevExpressUtils, uClientClasses, uDatasetUtils, ugbClientDataset;

constructor TUsuarioGridView.Create;
begin
  inherited Create;
end;

class procedure TUsuarioGridView.DeleteView(const ACodigoUsuario: Integer; AFormName: String);
var usuario: TUsuarioGridView;
begin
  usuario := TUsuarioGridView.Create;
  try
    with usuario do
    begin
      DmConnection.cdsDesignGrid.Close;
      DmConnection.cdsDesignGrid.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
      DmConnection.cdsDesignGrid.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
      DmConnection.cdsDesignGrid.Open;

      if not DmConnection.cdsDesignGrid.IsEmpty then
      begin
        DmConnection.cdsDesignGrid.Delete;
        DmConnection.cdsDesignGrid.Commit;
      end;
    end;
  finally
    usuario.Free;
  end;
end;

destructor TUsuarioGridView.Destroy;
begin
  inherited Destroy;
end;

function TUsuarioGridView.GetFormName(AGridView: TcxGridDBBandedTableView;
  AFormName: String): String;
begin
  result := UpperCase(AFormName+'_'+AGridView.Name);
end;

class procedure TUsuarioGridView.LoadGridView(AGridView: TcxGridDBBandedTableView; ACodigoUsuario: Integer; AFormName: String);
var usuario: TUsuarioGridView;
    stream: TMemoryStream;
    listFields: TStringList;
    cdsUsuarioDesignGrid: TgbClientDataset;
begin
  usuario := TUsuarioGridView.Create;
  try
    with usuario do
    begin
      try
        DmConnection.cdsDesignGrid.Close;
        DmConnection.cdsDesignGrid.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
        DmConnection.cdsDesignGrid.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
        DmConnection.cdsDesignGrid.Open;

        cdsUsuarioDesignGrid := DmConnection.cdsDesignGrid;

        if DmConnection.cdsDesignGrid.IsEmpty then
        begin
          DmConnection.cdsDesignGrid_ConsultaFormulario.Close;
          DmConnection.cdsDesignGrid_ConsultaFormulario.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
          DmConnection.cdsDesignGrid_ConsultaFormulario.Open;

          cdsUsuarioDesignGrid := DmConnection.cdsDesignGrid_ConsultaFormulario;
        end;

        if not(cdsUsuarioDesignGrid.IsEmpty) and
           not(cdsUsuarioDesignGrid.FieldByName('ARQUIVO_GRID').AsString.IsEmpty) then
        begin
          stream := TMemoryStream.Create;
          try
            TBlobField(cdsUsuarioDesignGrid.FieldByName('ARQUIVO_GRID')).SaveToStream(stream);

            if Assigned(stream) then
            begin
              stream.Position := 0;
              AGridView.RestoreFromStream(stream, false, false, [{gsoUseFilter, gsoUseSummary}], InttoStr(ACodigoUsuario));

              listFields := TStringList.Create;
              try
                listFields.Text := cdsUsuarioDesignGrid.FieldByName('ARQUIVO_FIELD_CAPTION').AsString;
                SetCaptionColumns(AGridView, listFields);
              finally
                listFields.Free;
              end;
            end
            else
            begin
              AGridView.ClearItems;
              AGridView.DataController.CreateAllItems();
              TcxGridUtils.PersonalizarColunaPeloTipoCampo(AGridView);
            end;
          finally
            FreeAndNil(stream);
          end;
        end;
      finally
        cdsUsuarioDesignGrid.Close;
      end;
    end;
  finally
    usuario.Free;
  end;
end;

class procedure TUsuarioGridView.SaveGridView(AGridView: TcxGridDBBandedTableView; ACodigoUsuario: Integer; AFormName: String);
var Stream: TMemoryStream;
    usuario: TUsuarioGridView;
begin
  usuario := TUsuarioGridView.Create;
  try
    with usuario do
    begin
      Stream := TMemoryStream.Create;
      try
        AGridView.StoreToStream(Stream, [{gsoUseFilter, gsoUseSummary}], InttoStr(ACodigoUsuario));

        try
          DmConnection.cdsDesignGrid.Close;
          DmConnection.cdsDesignGrid.Params.ParamByName('id_pessoa_usuario').AsInteger := ACodigoUsuario;
          DmConnection.cdsDesignGrid.Params.ParamByName('formulario').AsString := UpperCase(AFormName);
          DmConnection.cdsDesignGrid.Open;

          if DmConnection.cdsDesignGrid.IsEmpty then
            DmConnection.cdsDesignGrid.Incluir
          else
            DmConnection.cdsDesignGrid.Alterar;

          DmConnection.cdsDesignGridID_PESSOA_USUARIO.AsInteger := ACodigoUsuario;
          DmConnection.cdsDesignGridFORMULARIO.AsString := AFormName;
          stream.Position := 0;
          DmConnection.cdsDesignGridARQUIVO_GRID.LoadFromStream(Stream);
          DmConnection.cdsDesignGridARQUIVO_FIELD_CAPTION.AsString := GetFieldsFile(AGridView);
          DmConnection.cdsDesignGrid.Salvar;

          DmConnection.cdsDesignGrid.Commit;
        finally
          DmConnection.cdsDesignGrid.Close;
        end;
      finally
        FreeAndNil(Stream);
      end;
    end;
  finally
    FreeAndNil(usuario);
  end;
end;

function TUsuarioGridView.GetFieldsFile(AGridView: TcxGridDBBandedTableView): String;
var i: Integer;
    fields: TStrings;
begin
  fields := TStringList.Create;
  try
    for i := 0 to Pred(AGridView.ColumnCount) do
      fields.Add(AGridView.Columns[i].DataBinding.FieldName+DelimitadorSeparacaoField+AGridView.Columns[i].Caption);

    result := fields.Text;
  finally
    fields.Free;
  end;
end;

procedure TUsuarioGridView.SetCaptionColumns(AGridView: TcxGridDBBandedTableView; AListFields: TStringList);
var i: Integer;
    posicao, tamanho: Integer;
    fieldSearch, caption: String;
    fieldList: TDictionary<String, String>;
begin
  fieldList := TDictionary<String, String>.Create;
  try
    for i := 0 to Pred(AListFields.Count) do
      fieldList.Add(Copy(AListFields[i], 1, Pos(DelimitadorSeparacaoField, AListFields[i])-1),
                    Copy(AListFields[i], Pos(DelimitadorSeparacaoField, AListFields[i])+1, Length(AListFields[i])));

    for i := 0 to Pred(AGridView.ColumnCount) do
    begin
      if fieldList.ContainsKey(AGridView.Columns[i].DataBinding.FieldName) then
        AGridView.Columns[i].Caption := fieldList.ExtractPair(AGridView.Columns[i].DataBinding.FieldName).Value;
    end;
  finally
    fieldList.Free;
  end;
end;

function TUsuarioGridView.GetFileFieldsTemp: String;
begin
  result := ExtractFilePath(Application.ExeName)+CTempFieldsFile;
end;

function TUsuarioGridView.GetFileGridTemp: String;
begin
  result := ExtractFilePath(Application.ExeName)+CTempGridFile;
end;


{TUsuarioPesquisaPadrao}

class procedure TUsuarioPesquisaPadrao.SetConsultaAutomatica(
  const ACodigoUsuario: Integer; const AFormName: String;
  const AHabilitar: Boolean);
begin
  DmConnection.ServerMethodsClient.HabilitarConsultaAutomatica(ACodigoUsuario, AFormName, AHabilitar);
end;

class function TUsuarioPesquisaPadrao.GetConsultaAutomatica(
  const ACodigoUsuario: Integer; const AFormName: String): Boolean;
begin
  result := DmConnection.ServerMethodsClient.EstaConsultandoAutomatico(ACodigoUsuario, AFormName);
end;

{ TUsuario }

class function TUsuario.GetIdUsuario(const AUsuario: String): Integer;
var smUsuario: TSMPessoaClient;
begin
  smUsuario := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smUsuario.GetIdUsuario(AUsuario);
  finally
    smUsuario.Free;
  end;
end;

class function TUsuario.GetUsuario(AIdUsuario: Integer): TUsuarioProxy;
var smUsuario: TSMPessoaClient;
    JSONDataset: TFDJSONDataSets;
    fdmUsuario: TFDMemTable;
begin
  result := TUsuarioProxy.Create;

  smUsuario := TSMPessoaClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smUsuario.GetUsuario(AIdUsuario);
    fdmUsuario := TFDMemTable.Create(nil);
    try
      TDatasetUtils.JSONDatasetToMemTable(JSONDataset, fdmUsuario);
      result.id := fdmUsuario.FieldByName('id').AsInteger;
      result.usuario := fdmUsuario.FieldByName('usuario').AsString;
      result.nome := fdmUsuario.FieldByName('nome').AsString;
      result.idUsuarioPerfil := fdmUsuario.FieldByName('id_usuario_perfil').AsInteger;

      if result.idUsuarioPerfil > 0 then
        result.idSeguranca := fdmUsuario.FieldByName('id_usuario_perfil').AsInteger
      else
        result.idSeguranca := fdmUsuario.FieldByName('id').AsInteger;

      if UpperCase(result.usuario) = TUsuarioProxy.USUARIO_ADMINISTRADOR then
      begin
        result.administrador := true;
      end
      else
      begin
        result.administrador := false;
      end;
    finally
      FreeAndNil(fdmUsuario);
    end;
  finally
    smUsuario.Free;
  end;
end;

end.
