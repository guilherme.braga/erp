unit uNaturezaOperacao;

interface

Uses uNaturezaOperacaoProxy, SysUtils;

type TNaturezaOperacao = class
  private
    class function GerarNaturezaOperacao(const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
    class function ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
  public
    class function GetNaturezaOperacao(const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
    class function InserirNaturezaOperacaoCasoNaoExista(const ADescricaoNaturezaOperacao: String): Boolean;
    class function GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
    class function GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
end;

Implementation

uses uClientClasses, uDmConnection;

class function TNaturezaOperacao.ExisteNaturezaOperacao(const ADescricaoNaturezaOperacao: String): Boolean;
var smNaturezaOperacao: TSMOperacaoClient;
begin
  smNaturezaOperacao := TSMOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNaturezaOperacao.ExisteNaturezaOperacao(ADescricaoNaturezaOperacao);
  finally
    smNaturezaOperacao.Free;
  end;
end;

class function TNaturezaOperacao.GerarNaturezaOperacao(const ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
var smNaturezaOperacao: TSMOperacaoClient;
begin
  smNaturezaOperacao := TSMOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNaturezaOperacao.GerarNaturezaOperacao(ANaturezaOperacao);
  finally
    smNaturezaOperacao.Free;
  end;
end;

class function TNaturezaOperacao.GetIdNaturezaOperacaoPorDescricao(const ADescricaoNaturezaOperacao: String): Integer;
var
  smNaturezaOperacao: TSMOperacaoClient;
begin
  smNaturezaOperacao := TSMOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNaturezaOperacao.GetIdNaturezaOperacaoPorDescricao(ADescricaoNaturezaOperacao);
  finally
    smNaturezaOperacao.Free;
  end;
end;

class function TNaturezaOperacao.GetNaturezaOperacao(const AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
var smNaturezaOperacao: TSMOperacaoClient;
begin
  smNaturezaOperacao := TSMOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNaturezaOperacao.GetNaturezaOperacao(AIdNaturezaOperacao);
  finally
    //smNaturezaOperacao.Free;
  end;
end;

class function TNaturezaOperacao.GetDescricaoNaturezaOperacao(const AIdNaturezaOperacao: Integer): String;
var smNaturezaOperacao: TSMOperacaoClient;
begin
  smNaturezaOperacao := TSMOperacaoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smNaturezaOperacao.GetDescricaoNaturezaOperacao(AIdNaturezaOperacao);
  finally
    smNaturezaOperacao.Free;
  end;
end;

class function TNaturezaOperacao.InserirNaturezaOperacaoCasoNaoExista(const ADescricaoNaturezaOperacao: String): Boolean;
var
  naturezaOperacao: TNaturezaOperacaoProxy;
begin
  result := false;
  if not TNaturezaOperacao.ExisteNaturezaOperacao(ADescricaoNaturezaOperacao) then
  begin
    naturezaOperacao := TNaturezaOperacaoProxy.Create;
    try
      naturezaOperacao.FDescricao := ADescricaoNaturezaOperacao;
      result := true;
    finally
      //FreeAndNil(montadora);
    end;
  end
  else
  begin
    TNaturezaOperacao.GetIdNaturezaOperacaoPorDescricao(ADescricaoNaturezaOperacao);
    result := true;
  end;
end;

end.
