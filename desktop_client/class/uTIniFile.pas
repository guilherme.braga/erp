unit uTIniFile;

interface

uses
  Classes, SysUtils, IniFiles, Forms, Windows;

type TIniSystemFile = record
  databaseERP, databaseFAST, caminho_gbak: String;
end;

type TIniOptions = class(TObject)
  private
    function FileName: String;
  public
    constructor Create;
    procedure LoadSettings(Ini: TIniFile);
    procedure SaveSettings(Ini: TIniFile);

    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);

  end;

var
  IniOptions: TIniOptions = nil;
  IniSystemFile: TIniSystemFile;

implementation

procedure TIniOptions.LoadSettings(Ini: TIniFile);
begin
  if Ini <> nil then
    begin
      with Ini do
        begin
          IniSystemFile.databaseERP := ReadString('FIREBIRD','databaseERP','db\ERP.FDB');
          IniSystemFile.databaseFAST := ReadString('FIREBIRD','databaseFAST','db\FASTREPORT.FDB');
          IniSystemFile.caminho_gbak := ReadString('BACKUP','gbak','C:\PROGRAM FILES\FIREBIRD\FIREBIRD_2_5\BIN\GBAK.EXE');
        end;
    end;
end;

procedure TIniOptions.SaveSettings(Ini: TIniFile);
begin
  if Ini <> nil then
    begin
      with Ini do
        begin
          WriteString('FIREBIRD','databaseERP','db\ERP.FDB');
          WriteString('FIREBIRD','databaseFAST','db\FASTREPORT.FDB');
          WriteString('BACKUP','gbak','C:\PROGRAM FILES\FIREBIRD\FIREBIRD_2_5\BIN\GBAK.EXE');
        end;
    end;
end;

constructor TIniOptions.Create;
begin
  inherited Create;
  LoadFromFile(FileName);
end;

function TIniOptions.FileName: String;
begin
  result :=
    ExtractFilePath(Application.ExeName)+
    StringReplace(ExtractFileName(Application.ExeName)+'.ini',
                  ExtractFileExt(ExtractFileName(Application.ExeName)),
                  '', [rfReplaceAll]);
end;

procedure TIniOptions.LoadFromFile(const FileName: string);
var
  Ini: TIniFile;
begin
  if not FileExists(FileName) then
    SaveToFile(FileName);

  Ini := TIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    Ini.Free;
  end;
end;

procedure TIniOptions.SaveToFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    SaveSettings(Ini);
  finally
    Ini.Free;
  end;
end;

end.

