unit uBoleto;

interface

type TBoleto = class
  public
    class procedure EmitirBoleto(const AIdsContasReceber: String);
end;

implementation

Uses uBoletoCobreBemX;

{ TBoleto }

//Por Enquanto emite direto com o cobrebemx
class procedure TBoleto.EmitirBoleto(const AIdsContasReceber: String);
begin
  TBoletoCobreBemX.EmitirBoleto(AIdsContasReceber);
end;

end.
