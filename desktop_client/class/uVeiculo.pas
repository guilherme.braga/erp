unit uVeiculo;

interface

Uses uVeiculoProxy;

type TVeiculo = class
  class function GetVeiculo(const AIdVeiculo: Integer): TVeiculoProxy;
end;

implementation

uses uClientClasses, uDmConnection, REST.JSON;

class function TVeiculo.GetVeiculo(const AIdVeiculo: Integer): TVeiculoProxy;
var
  smVeiculo : TSMVeiculoClient;
begin
  smVeiculo := TSMVeiculoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TVeiculoProxy>(smVeiculo.GetVeiculo(AIdVeiculo));
  finally
    smVeiculo.Free;
  end;
end;

end.
