unit uImpostoICMS;

interface

uses uImpostoICMSProxy;

type TImpostoICMS = class
  class function GetImpostoIcms(const AIdImpostoIcms: Integer): TImpostoIcmsProxy;
end;

implementation

{ TImpostoICMS }

uses uClientClasses, uDmConnection, REST.JSON;

class function TImpostoICMS.GetImpostoIcms(const AIdImpostoIcms: Integer): TImpostoIcmsProxy;
var smImpostoICMS: TSMImpostoICMSClient;
  impostoICMS: TImpostoIcmsProxy;
begin
  smImpostoICMS := TSMImpostoICMSClient.Create(DmConnection.Connection.DBXConnection);
  try
    impostoICMS := TImpostoIcmsProxy.Create;
    impostoICMS := TJson.JsonToObject<TImpostoIcmsProxy>(smImpostoICMS.GetImpostoIcms(AIdImpostoIcms));
    result := impostoICMS;
  finally
    smImpostoICMS.Free;
  end;
end;

end.
