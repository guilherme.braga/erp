unit uFechamentoCrediario;

interface

Uses Contnrs;

type TFechamentoCrediario = class
  private
    IdPessoa: Integer;
    ValorBruto: Double;
    ValorDesconto: Double;
    PercentualDesconto: Double;
    ValorAcrescimo: Double;
    PercentualAcrescimo: Double;
    ValorLiquido: Double;
    ValorEntrada: Double;
    ValorCreditoPessoaDisponivel: Double;
    ValorCreditoPessoaUtilizado: Double;
    ValorTroco: Double;
    ValorDiferenca: Double;
  public

  protected

end;

implementation

end.
