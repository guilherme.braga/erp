unit uChecklist;

interface

Uses db, ugbClientDataset, FireDAC.Comp.Client, Data.FireDACJSONReflect,
  SysUtils;

type TChecklist = class
  class procedure ConstruirVisao(ADataset: TGBClientDataset; AIdChecklist: Integer);
end;

implementation

{ TChecklist }

uses uClientClasses, uDatasetUtils, uDmConnection;

class procedure TChecklist.ConstruirVisao(ADataset: TGBClientDataset; AIdChecklist: Integer);
var fdmListaItensChecklist: TFDMemTable;
var smChecklist: TSMChecklistClient;
    JSONDataset: TFDJSONDataSets;
begin
  fdmListaItensChecklist := TFDMemTable.Create(nil);
  try
    smChecklist := TSMChecklistClient.Create(DmConnection.Connection.DBXConnection);
    try
      JSONDataset := smChecklist.GetTodosItensAtivosDoChecklist(AIdChecklist);
      TDatasetUtils.JSONDatasetToMemTable(JSONDataset, fdmListaItensChecklist);
    finally
      smChecklist.Free;
    end;

    try
      fdmListaItensChecklist.First;
      while not fdmListaItensChecklist.Eof do
      begin
        if ADataset.Locate('ID_CHECKLIST_ITEM',
          fdmListaItensChecklist.FieldByName('ID').AsInteger, []) then
        begin
          ADataset.Alterar;
        end
        else
        begin
          ADataset.Incluir;
          ADataset.FieldByName('BO_MARCADO').AsString := 'N';
        end;

        ADataset.FieldByName('ID_CHECKLIST_ITEM').AsInteger :=
          fdmListaItensChecklist.FieldByName('ID').AsInteger;

        ADataset.FieldByName('JOIN_DESCRICAO_CHECKLIST').AsString :=
          fdmListaItensChecklist.FieldByName('DESCRICAO').AsString;

        ADataset.Salvar;
        fdmListaItensChecklist.Next;
      end;
    finally
      if not ADataset.IsEmpty then
        ADataset.First;
    end;
  finally
    FreeAndNil(fdmListaItensChecklist);
  end;
end;

end.
