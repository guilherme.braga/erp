unit uFluxoCaixaResumido;

interface

uses FireDAC.Comp.Client, Data.FireDACJSONReflect, Classes, db, uFluxoCaixaResumidoProxy;

type TFluxoCaixaResumido = class
  class function SetContasCorrentes(AFDMemTable: TFDMemTable): TFDJSONDatasets;
  class procedure SetValores(AFDMemTable: TFDMemTable; const AStringAGGContaCorrente: String;
    const ADataInicial, ADataFinal, ATipoPeriodo: String);
  class function SetFluxoCaixaResumido(AFDMemTable: TFDMemTable;
    const ACentrosResultados: String; const AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;
  class function GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
  class function GerarFluxoCaixaResumido(const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
  class function ExisteOrdenacaoSalva: Boolean;
  class function GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;
  class function GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;
end;

implementation

{ TPlanoConta }

uses uStringUtils, uClientClasses, uDmConnection, SysUtils, uDatasetUtils, uDateUtils, uTFunction;

{ TFluxoCaixaResumido }

class function TFluxoCaixaResumido.ExisteOrdenacaoSalva: Boolean;
var
  smFinanceiro: TSMFluxoCaixaResumidoClient;
  fluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    fluxoCaixaResumidoProxy := smFinanceiro.GetFluxoCaixaResumidoProxy;
    result := not fluxoCaixaResumidoProxy.FOrdemContaCorrente.IsEmpty;
  finally
    smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.GerarFluxoCaixaResumido(
  const AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
var smFinanceiro: TSMFluxoCaixaResumidoClient;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GerarFluxoCaixaResumido(AFluxoCaixaResumido);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
var smFinanceiro: TSMFluxoCaixaResumidoClient;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetFluxoCaixaResumidoProxy;
  finally
    //smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.GetTotalContaPagar(ADtInicial, ADtFinal: String): Double;
var
  smFinanceiro: TSMFluxoCaixaResumidoClient;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetTotalContaPagar(ADtInicial, ADtFinal);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.GetTotalContaReceber(ADtInicial, ADtFinal: String): Double;
var
  smFinanceiro: TSMFluxoCaixaResumidoClient;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smFinanceiro.GetTotalContaReceber(ADtInicial, ADtFinal);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.SetContasCorrentes(AFDMemTable: TFDMemTable): TFDJSONDatasets;
var smFinanceiro: TSMFluxoCaixaResumidoClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetContasCorrentes;
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

class function TFluxoCaixaResumido.SetFluxoCaixaResumido(AFDMemTable: TFDMemTable;
  const ACentrosResultados: String; const AListaCamposPeriodo, ATipoPeriodo: String): TFDJSONDataSets;
var
  smFinanceiro: TSMFluxoCaixaResumidoClient;
  JSONDataset: TFDJSONDataSets;
  field: TField;
  i: Integer;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetFluxoCaixaResumido(ACentrosResultados, AListaCamposPeriodo, ATipoPeriodo);
    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);

    for i := 0 to AFDMemTable.FieldCount - 1 do
    begin
      field := AFDMemTable.Fields[i];

      field.ReadOnly := false;

      if UpperCase(field.FieldName) = 'DESCRICAO' then
      begin
        field.DisplayLabel := 'Per�odo';
      end
      else if UpperCase(field.FieldName) = 'ID_CONTA_CORRENTE' then
      begin
        field.DisplayLabel := '';
        field.Visible := false;
      end
      else if field is TNumericField then
      begin
        field.DisplayLabel := Copy(field.FieldName, 1, 2) +'/'+
          Copy(field.FieldName, 3, 2) +'/'+ Copy(field.FieldName, 5, 4);

        TNumericField(field).DisplayFormat := '###,###,###,###,##0.00';
      end;
    end;

    AFDMemTable.Append;
    AFDMemTable.FieldByName('descricao').AsString := 'TOTAL CONTAS CORRENTES';
    AFDMemTable.Post;

    AFDMemTable.Append;
    AFDMemTable.FieldByName('descricao').AsString := 'CONTAS A RECEBER';
    AFDMemTable.Post;

    AFDMemTable.Append;
    AFDMemTable.FieldByName('descricao').AsString := 'CONTAS A PAGAR';
    AFDMemTable.Post;

    AFDMemTable.Append;
    AFDMemTable.FieldByName('descricao').AsString := 'TOTAL GERAL';
    AFDMemTable.Post;
  finally
    smFinanceiro.Free;
  end;
end;

{Ver defini��o exata}
class procedure TFluxoCaixaResumido.SetValores(AFDMemTable: TFDMemTable; const AStringAGGContaCorrente: String;
  const ADataInicial, ADataFinal, ATipoPeriodo: String);
var smFinanceiro: TSMFluxoCaixaResumidoClient;
    JSONDataset: TFDJSONDataSets;
begin
  smFinanceiro := TSMFluxoCaixaResumidoClient.Create(DmConnection.Connection.DBXConnection);
  try
    JSONDataset := smFinanceiro.GetTotalPorContaCorrente(AStringAGGContaCorrente, ADataInicial,
      ADataFinal, ATipoPeriodo);

    TDatasetUtils.JSONDatasetToMemTable(JSONDataset, AFDMemTable);
  finally
    smFinanceiro.Free;
  end;
end;

end.
