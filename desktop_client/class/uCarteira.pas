unit uCarteira;

interface

Uses uCarteiraProxy;

type TCarteira = class
  class function GetCarteira(AIdCarteira: Integer): TCarteiraProxy;
  class function BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
  class function BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
  class procedure IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
end;

implementation

{ TCarteira }

uses uClientClasses, Rest.JSON, uDmConnection, SysUtils;

class function TCarteira.BuscarProximaSequenciaNossoNumero(const AIdCarteira: Integer): Integer;
var  smCarteira: TSMCarteiraClient;
begin
  smCarteira := TSMCarteiraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCarteira.BuscarProximaSequenciaNossoNumero(AIdCarteira);
  finally
    FreeAndNil(smCarteira);
  end;
end;

class function TCarteira.BuscarProximaSequenciaRemessa(const AIdCarteira: Integer): Integer;
var  smCarteira: TSMCarteiraClient;
begin
  smCarteira := TSMCarteiraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smCarteira.BuscarProximaSequenciaRemessa(AIdCarteira);
  finally
    FreeAndNil(smCarteira);
  end;
end;

class function TCarteira.GetCarteira(AIdCarteira: Integer): TCarteiraProxy;
var  smCarteira: TSMCarteiraClient;
begin
  smCarteira := TSMCarteiraClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TCarteiraProxy>(smCarteira.GetCarteira(AIdCarteira));
  finally
    //FreeAndNil(smCarteira);
  end;
end;

class procedure TCarteira.IncrementarBoletoNossoNumeroProximo(const AIdCarteira: Integer);
var smCarteira: TSMCarteiraClient;
begin
  smCarteira := TSMCarteiraClient.Create(DmConnection.Connection.DBXConnection);
  try
    smCarteira.IncrementarBoletoNossoNumeroProximo(AIdCarteira);
  finally
    FreeAndNil(smCarteira);
  end;
end;

end.
