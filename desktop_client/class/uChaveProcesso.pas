unit uChaveProcesso;

interface

type TChaveProcesso = class
  class function NovaChaveProcesso(AOrigem   : string;
    AIdOrigem : Integer): Integer;
end;

implementation

{ TChaveProcesso }

uses uClientClasses, uDmConnection;

class function TChaveProcesso.NovaChaveProcesso(AOrigem   : string;
                                                AIdOrigem : Integer): Integer;
var smChaveProcesso: TsmChaveProcessoClient;
begin
  smChaveProcesso := TsmChaveProcessoClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smChaveProcesso.NovaChaveProcesso(AOrigem, AIdOrigem);
  finally
    smChaveProcesso.Free;
  end;
end;

end.
