unit uBackup;

interface

Uses Classes, SysUtils, uBackupProxy;

type TBackup = class
  class function RealizarBackup(const AIdUsuario: Integer): Boolean;
  class function BackupDiarioRealizado: Boolean;
  class function DownloadBackup(const ACaminhoArquivoParaSalvar: String): Boolean;
end;

implementation

{ TBackup }

uses uClientClasses, uDmConnection, uSistema, uFrmMessage;

class function TBackup.BackupDiarioRealizado: Boolean;
var
  smBackup: TSMBackupClient;
begin
  smBackup := TSMBackupClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smBackup.BackupDiarioRealizado();
  finally
    smBackup.Free;
  end;
end;

class function TBackup.RealizarBackup(const AIdUsuario: Integer): Boolean;
var
  smBackup: TSMBackupClient;
begin
  smBackup := TSMBackupClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := smBackup.RealizarBackup(AIdUsuario);
  finally
    smBackup.Free;
  end;
end;

class function TBackup.DownloadBackup(const ACaminhoArquivoParaSalvar: String): Boolean;
var
    RetStream: TStream;
    Buffer: PByte;
    Mem: TMemoryStream;
    BytesRead: Integer;
    DocumentId: Int64;
    Size: Int64;
    filename: WideString;
    BufSize: Integer;
    smBackup: TSMBackupClient;
begin
  smBackup := TSMBackupClient.Create(DmConnection.Connection.DBXConnection);
  try
    BufSize := 1024;

    try
      Mem := TMemoryStream.Create;
      GetMem( Buffer, BufSize );

      try
        RetStream := smBackup.DownloadBackup(Size);
        RetStream.Position := 0;

        if ( Size <> 0 ) then
        begin
          filename := ExtractFileName(ACaminhoArquivoParaSalvar);

          repeat
            BytesRead := RetStream.Read( Pointer( Buffer )^, BufSize );

            if ( BytesRead > 0 ) then
            begin
              Mem.WriteBuffer( Pointer( Buffer )^, BytesRead );
            end;

          until ( BytesRead < BufSize );

          if ( Size <> Mem.Size ) then
          begin
            raise Exception.Create( 'Erro ao processar arquivo de backup.' );
          end;

          Mem.SaveToFile(ACaminhoArquivoParaSalvar);
        end
        else
        begin
          raise Exception.Create( 'Erro ao buscar arquivo de backup no servidor.' );
        end;
      finally
        FreeMem( Buffer, BufSize );
        FreeAndNIl(Mem);
      end;
    except
      on E: Exception do
      begin
        TFrmMessage.Failure(E.ClassName + ': ' + E.Message);
        abort;
      end;
    end;
  finally
    smBackup.Free;
  end;
end;

end.
