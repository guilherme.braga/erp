unit uTTranslate;

interface

uses
  SysUtils,
  cxEditConsts,
  cxGridStrs,
  cxFilterControlStrs,
  cxFilterConsts,
  cxGridPopUpMenuConsts,
  cxExtEditConsts,
  cxDataConsts,
  cxClasses,
  cxLibraryStrs,
  cxSchedulerStrs,
  dxBarStrs,
  cxExportStrs,
  dxPSRes,
  dxDockConsts,
  Windows,
  Consts,
  DBConsts,
  DB;

type

  { Dev Express Functions }
  TcxTranslate = class(TObject)
  public
    class procedure Translate_PTBR;
  end;

type TDelphiTranslate = class

  public
    class procedure TranslateDelphiMessages_PTBR;
    class procedure TranslateDelphiDBMessages_PTBR;

end;

implementation

{ TcsDXFunctions }

class procedure TcxTranslate.Translate_PTBR;
begin
  cxSetResourceString(@cxNavigator_DeleteRecordQuestion,'Excluir registro?');
  cxSetResourceString(@cxNavigatorHint_Append,'acrescentar registro');
  cxSetResourceString(@cxNavigatorHint_Cancel,'Cancelar editar');
  cxSetResourceString(@cxNavigatorHint_Delete,'Excluir registro');
  cxSetResourceString(@cxNavigatorHint_Edit,'Editar registro');
  cxSetResourceString(@cxNavigatorHint_Filter,'Filtrar dados');
  cxSetResourceString(@cxNavigatorHint_First,'Primeiro registro');
  cxSetResourceString(@cxNavigatorHint_Insert,'Inserir registro');
  cxSetResourceString(@cxNavigatorHint_Last,'�ltimo registro');
  cxSetResourceString(@cxNavigatorHint_Next,'Pr�ximo registro');
  cxSetResourceString(@cxNavigatorHint_NextPage,'Pr�xima p�gina');
  cxSetResourceString(@cxNavigatorHint_Post,'postar editar');
  cxSetResourceString(@cxNavigatorHint_Prior,'Antes de gravar');
  cxSetResourceString(@cxNavigatorHint_PriorPage,'Antes p�gina');
  cxSetResourceString(@cxNavigatorHint_Refresh,'Atualizar dados');
  cxSetResourceString(@cxNavigatorHint_SaveBookmark,'Salvar Bookmark');
  cxSetResourceString(@cxSBlobButtonCancel,'&Cancelar');
  cxSetResourceString(@cxSBlobButtonClose,'&Fechar');
  cxSetResourceString(@cxSBlobPicture,'(Figura)');
  cxSetResourceString(@cxSBlobPictureEmpty,'(Figura)');
  cxSetResourceString(@cxSCheckComboBoxEmptySelectionText,'Nenhum selecionado');
  cxSetResourceString(@cxSCheckComboBoxStatesItemsPropertyDlgCaption,'cxCheckComboBox - editor CheckStates');
  cxSetResourceString(@cxSCheckControlIncorrectItemCount,'O n�mero de itens n�o pode ser maior que 64, se o EditValueFormat � cvfInteger');
  cxSetResourceString(@cxSCheckGroupStatesItemsPropertyDlgCaption,'cxCheckGroup - editor CheckStates');
  cxSetResourceString(@cxSColorComboBoxDefaultDescription,'A cor n�o selecionado');
  cxSetResourceString(@cxSDataCustomDataSourceInvalidCompare,'GetInfoForCompare n�o implementado');
  cxSetResourceString(@cxSDataInvalidStreamFormat,'Stream \n formato Inv�lido');
  cxSetResourceString(@cxSDataItemExistError,'Item j� existe');
  cxSetResourceString(@cxSDataItemIndexError,'ItemIndex fora do intervalo');
  cxSetResourceString(@cxSDataProviderModeError,'Esta opera��o n�o � suportado no modo fornecedor');
  cxSetResourceString(@cxSDataReadError,'Stream erro de leitura');
  cxSetResourceString(@cxSDataRecordIndexError,'RecordIndex fora do intervalo');
  cxSetResourceString(@cxSDataRowIndexError,'RowIndex fora do intervalo');
  cxSetResourceString(@cxSDataWriteError,'Stream erro de grava��o');
  cxSetResourceString(@cxSDateError,'Data inv�lida');
  cxSetResourceString(@cxSDateFifth,'quinta');
  cxSetResourceString(@cxSDateFirst,'primeiro');
  cxSetResourceString(@cxSDateFourth,'quarto');
  cxSetResourceString(@cxSDateFriday,'Sexta-feira');
  cxSetResourceString(@cxSDateMonday,'Segunda-feira');
  cxSetResourceString(@cxSDateNow,'agora');
  cxSetResourceString(@cxSDatePopupClear,'Limpar');
  cxSetResourceString(@cxSDatePopupNow,'Agora');
  cxSetResourceString(@cxSDatePopupToday,'Hoje');
  cxSetResourceString(@cxSDateSaturday,'S�bado');
  cxSetResourceString(@cxSDateSecond,'segundo');
  cxSetResourceString(@cxSDateSeventh,'s�timo');
  cxSetResourceString(@cxSDateSixth,'sexto \n');
  cxSetResourceString(@cxSDateSunday,'Domingo');
  cxSetResourceString(@cxSDateThird,'terceiro');
  cxSetResourceString(@cxSDateThursday,'Quinta-feira');
  cxSetResourceString(@cxSDateToday,'hoje');
  cxSetResourceString(@cxSDateTomorrow,'amanh�');
  cxSetResourceString(@cxSDateTuesday,'Ter�a-feira');
  cxSetResourceString(@cxSDateWednesday,'Quarta-feira');
  cxSetResourceString(@cxSDateYesterday,'ontem');
  cxSetResourceString(@cxSDBDetailFilterControllerNotFound,'DetailFilterController n�o encontrado');
  cxSetResourceString(@cxSDBKeyFieldNotFound,'Key Field n�o encontrado');
  cxSetResourceString(@cxSDBNotInGridMode,'DataController N�o est� em GridMode');
  cxSetResourceString(@cxSEditButtonCancel,'Cancelar');
  cxSetResourceString(@cxSEditCheckBoxChecked,'VERDADEIRO');
  cxSetResourceString(@cxSEditCheckBoxUnchecked,'Falso');
  cxSetResourceString(@cxSEditCheckGroupChecked,'verificado');
  cxSetResourceString(@cxSEditCheckGroupGrayed,'Acinzentado');
  cxSetResourceString(@cxSEditCheckGroupUnchecked,'Desmarcado');
  cxSetResourceString(@cxSEditDateConvertError,'N�o foi poss�vel converter a data');
  cxSetResourceString(@cxSEditInvalidRepositoryItem,'O item reposit�rio n�o � aceit�vel');
  cxSetResourceString(@cxSEditNumericValueConvertError,'N�o foi poss�vel converter o valor num�rico');
  cxSetResourceString(@cxSEditPopupCircularReferencingError,'Referenciando Circular n�o � permitido');
  cxSetResourceString(@cxSEditPostError,'Um erro ocorreu durante o lan�amento editar o valor');
  cxSetResourceString(@cxSEditRepositoryExtLookupComboBoxItem,'ExtLookupComboBox | Representa um ultra-pesquisa avan�ada usando o QuantumGrid como seu controle drop-down');
  cxSetResourceString(@cxSEditRichEditCallBackFail,'RichEdit: Falha ao definir callback');
  cxSetResourceString(@cxSEditRichEditCopyCaption,'&copiar');
  cxSetResourceString(@cxSEditRichEditDeleteCaption,'&Deletar');
  cxSetResourceString(@cxSEditTimeConvertError,'N�o foi poss�vel converter a tempo');
  cxSetResourceString(@cxSEditValidateErrorText,'Valor de entrada inv�lido. Use Esc para abandonar as mudan�as');
  cxSetResourceString(@cxSEditValueOutOfBounds,'Valor fora dos limites');
  cxSetResourceString(@cxSFilterAddCondition,'Adicionar &Condi��es');
  cxSetResourceString(@cxSFilterAddGroup,'Adicionar &Grupo');
  cxSetResourceString(@cxSFilterAndCaption,'e');
  cxSetResourceString(@cxSFilterBlankCaption,'em branco');
  cxSetResourceString(@cxSFilterBoolOperatorAnd,'E');
  cxSetResourceString(@cxSFilterBoolOperatorNotAnd,'E N�O');
  cxSetResourceString(@cxSFilterBoolOperatorNotOr,'OU N�O');
  cxSetResourceString(@cxSFilterBoolOperatorOr,'OU');
  cxSetResourceString(@cxSFilterBoxAllCaption,'(Todos)');
  cxSetResourceString(@cxSFilterBoxBlanksCaption,'(Em branco)');
  cxSetResourceString(@cxSFilterBoxCustomCaption,'(Personalizar. ..)');
  cxSetResourceString(@cxSFilterBoxNonBlanksCaption,'(Preenchido)');
  cxSetResourceString(@cxSFilterClearAll,'Limpar &Todos');
  cxSetResourceString(@cxSFilterControlDialogActionApplyCaption,'&Aplicar');
  cxSetResourceString(@cxSFilterControlDialogActionCancelCaption,'Cancelar');
  cxSetResourceString(@cxSFilterControlDialogActionOpenCaption,'&Abrir...');
  cxSetResourceString(@cxSFilterControlDialogActionOpenHint,'Abrir | Abre um filtro existente');
  cxSetResourceString(@cxSFilterControlDialogActionSaveCaption,'&Salvar Como...');
  cxSetResourceString(@cxSFilterControlDialogActionSaveHint,'Salvar como | Salva o filtro ativo com um novo nome');
  cxSetResourceString(@cxSFilterControlDialogCaption,'Construtor do Filtro');
  cxSetResourceString(@cxSFilterControlDialogFileFilter,'Filtro (*.flt)|*.flt');
  cxSetResourceString(@cxSFilterControlDialogNewFile,'SemTitulo.flt');
  cxSetResourceString(@cxSFilterControlDialogOpenDialogCaption,'Abrir um filtro existente');
  cxSetResourceString(@cxSFilterControlDialogSaveDialogCaption,'Salve o filtro ativo para o arquivo');
  cxSetResourceString(@cxSFilterControlNullString,'<Vazio>');
  cxSetResourceString(@cxSFilterDialogCaption,'Personalizar Filtro');
  cxSetResourceString(@cxSFilterDialogCharactersSeries,'para representar qualquer seq��ncia de caracteres');
  cxSetResourceString(@cxSFilterDialogInvalidValue,'Valor inv�lido');
  cxSetResourceString(@cxSFilterDialogOperationAnd,'E');
  cxSetResourceString(@cxSFilterDialogOperationOr,'OU');
  cxSetResourceString(@cxSFilterDialogRows,'Mostrar linhas:');
  cxSetResourceString(@cxSFilterDialogSingleCharacter,'para representar qualquer caractere �nico');
  cxSetResourceString(@cxSFilterErrorBuilding,'N�o � poss�vel criar filtros da fonte');
  cxSetResourceString(@cxSFilterFooterAddCondition,'pressione o bot�o para adicionar uma nova condi��o');
  cxSetResourceString(@cxSFilterGroupCaption,'aplica-se �s seguintes condi��es');
  cxSetResourceString(@cxSFilterNotCaption,'n�o');
  cxSetResourceString(@cxSFilterOperatorBeginsWith,'come�a com');
  cxSetResourceString(@cxSFilterOperatorBetween,'entre');
  cxSetResourceString(@cxSFilterOperatorContains,'cont�m');
  cxSetResourceString(@cxSFilterOperatorDoesNotBeginWith,'n�o come�a com');
  cxSetResourceString(@cxSFilterOperatorDoesNotContain,'n�o cont�m');
  cxSetResourceString(@cxSFilterOperatorDoesNotEndWith,'n�o termina com');
  cxSetResourceString(@cxSFilterOperatorEndsWith,'termina com');
  cxSetResourceString(@cxSFilterOperatorEqual,'igual');
  cxSetResourceString(@cxSFilterOperatorFuture,'� o futuro');
  cxSetResourceString(@cxSFilterOperatorGreater,'� maior do que');
  cxSetResourceString(@cxSFilterOperatorGreaterEqual,'� maior ou igual a');
  cxSetResourceString(@cxSFilterOperatorInList,'em');
  cxSetResourceString(@cxSFilterOperatorIsNotNull,'n�o est� em branco');
  cxSetResourceString(@cxSFilterOperatorIsNull,'est� em branco');
  cxSetResourceString(@cxSFilterOperatorLast14Days,'� �ltimos 14 dias');
  cxSetResourceString(@cxSFilterOperatorLast30Days,'� �ltimos 30 dias');
  cxSetResourceString(@cxSFilterOperatorLast7Days,'� �ltimos 7 dias');
  cxSetResourceString(@cxSFilterOperatorLastMonth,'� no m�s passado');
  cxSetResourceString(@cxSFilterOperatorLastTwoWeeks,'� �ltimas duas semanas');
  cxSetResourceString(@cxSFilterOperatorLastWeek,'� na semana passada');
  cxSetResourceString(@cxSFilterOperatorLastYear,'� do ano passado');
  cxSetResourceString(@cxSFilterOperatorLess,'� inferior');
  cxSetResourceString(@cxSFilterOperatorLessEqual,'� menor ou igual a');
  cxSetResourceString(@cxSFilterOperatorLike,'parecido');
  cxSetResourceString(@cxSFilterOperatorNext14Days,'� pr�ximos 14 dias');
  cxSetResourceString(@cxSFilterOperatorNext30Days,'� pr�ximos 30 dias');
  cxSetResourceString(@cxSFilterOperatorNext7Days,'� pr�ximos 7 dias');
  cxSetResourceString(@cxSFilterOperatorNextMonth,'� no pr�ximo m�s');
  cxSetResourceString(@cxSFilterOperatorNextTwoWeeks,'� pr�ximas duas semanas');
  cxSetResourceString(@cxSFilterOperatorNextWeek,'� na pr�xima semana');
  cxSetResourceString(@cxSFilterOperatorNextYear,'� no pr�ximo ano');
  cxSetResourceString(@cxSFilterOperatorNotBetween,'n�o entre');
  cxSetResourceString(@cxSFilterOperatorNotEqual,'n�o � igual');
  cxSetResourceString(@cxSFilterOperatorNotInList,'N�o est� em');
  cxSetResourceString(@cxSFilterOperatorNotLike,'N�o gosto');
  cxSetResourceString(@cxSFilterOperatorPast,'� passado');
  cxSetResourceString(@cxSFilterOperatorThisMonth,'� este m�s');
  cxSetResourceString(@cxSFilterOperatorThisWeek,'� esta semana');
  cxSetResourceString(@cxSFilterOperatorThisYear,'� este ano');
  cxSetResourceString(@cxSFilterOperatorToday,'� hoje');
  cxSetResourceString(@cxSFilterOperatorTomorrow,'� amanh�');
  cxSetResourceString(@cxSFilterOperatorYesterday,'� tarde');
  cxSetResourceString(@cxSFilterOrCaption,'ou');
  cxSetResourceString(@cxSFilterRemoveRow,'&Remover Linha');
  cxSetResourceString(@cxSFilterRootButtonCaption,'Filtro');
  cxSetResourceString(@cxSFilterRootGroupCaption,'<raiz>');
  cxSetResourceString(@cxSGridAlignCenter,'Alinhar ao centro');
  cxSetResourceString(@cxSGridAlignLeft,'Alinhar � Esquerda');
  cxSetResourceString(@cxSGridAlignmentSubMenu,'Alinhamento');
  cxSetResourceString(@cxSGridAlignRight,'Alinhar � Direita');
  cxSetResourceString(@cxSGridAvgMenuItem,'M�dia');
  cxSetResourceString(@cxSGridBestFit,'Melhor ajuste');
  cxSetResourceString(@cxSGridBestFitAllColumns,'Melhor Ajuste (todas as colunas)');
  cxSetResourceString(@cxSGridClearSorting,'Limpar Sele��o');
  cxSetResourceString(@cxSGridCountMenuItem,'Contar');
  cxSetResourceString(@cxSGridFieldChooser,'Seletor de Campo');
  cxSetResourceString(@cxSGridGroupByBox,'Agrupar por Caixa');
  cxSetResourceString(@cxSGridGroupByThisField,'Grupo por este campo');
  cxSetResourceString(@cxSGridNone,'Nenhum');
  cxSetResourceString(@cxSGridNoneMenuItem,'Nenhum');
  cxSetResourceString(@cxSGridRemoveColumn,'Remover esta coluna');
  cxSetResourceString(@cxSGridRemoveThisGroupItem,'Retire do agrupamento');
  cxSetResourceString(@cxSGridShowFooter,'Rodap�');
  cxSetResourceString(@cxSGridShowGroupFooter,'Grupo Rodap�s');
  cxSetResourceString(@cxSGridSortByGroupValues,'Classificar por grupo Valor');
  cxSetResourceString(@cxSGridSortBySummary,'%s para %s');
  cxSetResourceString(@cxSGridSortBySummaryCaption,'Ordenar por Grupo Resumo:');
  cxSetResourceString(@cxSGridSortColumnAsc,'Ascendente');
  cxSetResourceString(@cxSGridSortColumnDesc,'Classifica��o Decrescente');
  cxSetResourceString(@cxSGridSumMenuItem,'Soma');
  cxSetResourceString(@cxSMenuItemCaptionCopy,'&Copiar');
  cxSetResourceString(@cxSMenuItemCaptionCut,'Cor&tar');
  cxSetResourceString(@cxSMenuItemCaptionDelete,'&Deletar');
  cxSetResourceString(@cxSMenuItemCaptionLoad,'&Carregar...');
  cxSetResourceString(@cxSMenuItemCaptionPaste,'&Colar');
  cxSetResourceString(@cxSMenuItemCaptionSave,'Salvar &Como...');
  cxSetResourceString(@cxSSpinEditInvalidNumericValue,'Invalid valor num�rico');
  cxSetResourceString(@cxSTextFalse,'falso');
  cxSetResourceString(@cxSTextTrue,'verdadeiro');
  cxSetResourceString(@dxSBAR_ADDEX,'Adicionar ...');
  cxSetResourceString(@dxSBAR_ADDGALLERYNAME,'Galeria');
  cxSetResourceString(@dxSBAR_ADDREMOVEBUTTONS,'&Adicionar ou Remover Bot�es');
  cxSetResourceString(@dxSBAR_ADDTOQATITEMNAME,'&Adiione %s para Quick Acesso \nToolbar');
  cxSetResourceString(@dxSBAR_BARMANAGERBADOWNER,'TdxBarManager TWinControl deve ter como seu propriet�rio');
  cxSetResourceString(@dxSBAR_BARMANAGERMORETHANONE,'Um controle deve conter apenas um �nico TdxBarManager');
  cxSetResourceString(@dxSBAR_BUTTONDEFAULTACTIONDESCRIPTION,'apertar');
  cxSetResourceString(@dxSBAR_CANCEL,'Cancelar');
  cxSetResourceString(@dxSBAR_CANTASSIGNCONTROL,'Voc� n�o pode atribuir o mesmo controle para mais de um TdxBarControlContainerItem');
  cxSetResourceString(@dxSBAR_CANTFINDBARMANAGERFORSTATUSBAR,'Um gerente de bar n�o pode ser encontrado para a barra de status');
  cxSetResourceString(@dxSBAR_CANTMERGEBARMANAGER,'Voc� n�o pode fundir-se com o gerente do bar especificado');
  cxSetResourceString(@dxSBAR_CANTMERGETOOLBAR,'Voc� n�o pode fundir-se com a barra de ferramentas especificado');
  cxSetResourceString(@dxSBAR_CANTMERGEWITHMERGEDTOOLBAR,'Voc� n�o pode fundir uma barra de ferramentas com uma barra de ferramentas que j� est� mesclado');
  cxSetResourceString(@dxSBAR_CANTPLACERIBBONGALLERY,'Voc� pode colocar TdxRibbonGalleryItem apenas na faixa');
  cxSetResourceString(@dxSBAR_CANTPLACESEPARATOR,'Um item separador n�o pode ser colocado na barra de ferramentas especificado');
  cxSetResourceString(@dxSBAR_CANTUNMERGETOOLBAR,'Voc� n�o pode desinstalar a barra de ferramentas especificado');
  cxSetResourceString(@dxSBAR_CAPTION,'Personalizar');
  cxSetResourceString(@dxSBAR_CATEGORIES,'Cate&gorias:');
  cxSetResourceString(@dxSBAR_CATEGORYADD,'Adicionar Categoria');
  cxSetResourceString(@dxSBAR_CATEGORYINSERT,'Inserir Categoria');
  cxSetResourceString(@dxSBAR_CATEGORYNAME,'&Nome da Categoria:');
  cxSetResourceString(@dxSBAR_CATEGORYRENAME,'Renomear Categoria');
  cxSetResourceString(@dxSBAR_CLEAR,'Apagar');
  cxSetResourceString(@dxSBAR_CLEARGALLERYFILTER,'Linpar Filtro');
  cxSetResourceString(@dxSBAR_CLOSE,'Fechar');
  cxSetResourceString(@dxSBAR_COLOR_STR_0,'Preto');
  cxSetResourceString(@dxSBAR_COLOR_STR_1,'Marrom');
  cxSetResourceString(@dxSBAR_COLOR_STR_10,'Lim�o');
  cxSetResourceString(@dxSBAR_COLOR_STR_11,'Amarelo \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_12,'Azul \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_15,'Branco \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_2,'Verde \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_3,'Oliva');
  cxSetResourceString(@dxSBAR_COLOR_STR_4,'Marinha \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_5,'P�rpura');
  cxSetResourceString(@dxSBAR_COLOR_STR_7,'cinza');
  cxSetResourceString(@dxSBAR_COLOR_STR_8,'Prata \n');
  cxSetResourceString(@dxSBAR_COLOR_STR_9,'Vermelho \n');
  cxSetResourceString(@dxSBAR_COLORAUTOTEXT,'(autom�tico \n)');
  cxSetResourceString(@dxSBAR_COLORCUSTOMTEXT,'(personalizado)');
  cxSetResourceString(@dxSBAR_COMMANDNAMECANNOTBEBLANK,'Um nome de comando n�o pode ser em branco. Por favor, digite um nome.');
  cxSetResourceString(@dxSBAR_COMMANDS,'Coman&dos:');
  cxSetResourceString(@dxSBAR_CP_ADDBUTTON,'Adicionar&Bot�o');
  cxSetResourceString(@dxSBAR_CP_ADDCXITEM,'Adicionar&cxEditItem');
  cxSetResourceString(@dxSBAR_CP_ADDDXITEM,'Adicionar &Item');
  cxSetResourceString(@dxSBAR_CP_ADDGROUPBUTTON,'Adicionar Gro&upButton');
  cxSetResourceString(@dxSBAR_CP_ADDLARGEBUTTON,'AdicionarL&argeButton');
  cxSetResourceString(@dxSBAR_CP_ADDSEPARATOR,'Adicionar&Separator');
  cxSetResourceString(@dxSBAR_CP_ADDSUBITEM,'Adicionar&SubItem');
  cxSetResourceString(@dxSBAR_CP_ALLVIEWLEVELS,'Todos');
  cxSetResourceString(@dxSBAR_CP_BEGINAGROUP,'Come�ar um &Grupo');
  cxSetResourceString(@dxSBAR_CP_BUTTONGROUP,'Grupo');
  cxSetResourceString(@dxSBAR_CP_BUTTONUNGROUP,'Desagrupar');
  cxSetResourceString(@dxSBAR_CP_CAPTION,'&t�tulo:');
  cxSetResourceString(@dxSBAR_CP_DELETE,'&Excluir \n');
  cxSetResourceString(@dxSBAR_CP_DELETEITEM,'Excluir Item');
  cxSetResourceString(@dxSBAR_CP_DELETELINK,'Excluir Link');
  cxSetResourceString(@dxSBAR_CP_DISTRIBUTED,'Dis&tribu�do');
  cxSetResourceString(@dxSBAR_CP_IMAGEANDTEXT,'Imagem &e Texto');
  cxSetResourceString(@dxSBAR_CP_MOSTRECENTLYUSED,'&Mais recentemente usado');
  cxSetResourceString(@dxSBAR_CP_NAME,'&Nome:');
  cxSetResourceString(@dxSBAR_CP_POSITIONMENU,'&Posi��o');
  cxSetResourceString(@dxSBAR_CP_SINGLEVIEWLEVELITEMSUFFIX,'APENAS \n');
  cxSetResourceString(@dxSBAR_CP_TEXTONLYALWAYS,'&Somente Texto (sempre)');
  cxSetResourceString(@dxSBAR_CP_TEXTONLYINMENUS,'Texto &Only (em Menus)');
  cxSetResourceString(@dxSBAR_CP_VIEWLEVELSMENU,'Ver %N�veis');
  cxSetResourceString(@dxSBAR_CP_VISIBLE,'&Vis�vel');
  cxSetResourceString(@dxSBAR_CUSTOMIZE,'&Personalizar...');
  cxSetResourceString(@dxSBAR_CUSTOMIZINGFORM,'Personaliza��o Form...');
  cxSetResourceString(@dxSBAR_CXEDITVALUEDIALOGCAPTION,'Digite o valor');
  cxSetResourceString(@dxSBAR_DATECLEAR,'Apagar \n');
  cxSetResourceString(@dxSBAR_DATEDIALOGCAPTION,'Selecione um dado');
  cxSetResourceString(@dxSBAR_DATETODAY,'Hoje \n');
  cxSetResourceString(@dxSBAR_DEFAULTCATEGORYNAME,'Padr�o');
  cxSetResourceString(@dxSBAR_DELETE,'Excluir');
  cxSetResourceString(@dxSBAR_DESCRIPTION,'Descri��o');
  cxSetResourceString(@dxSBAR_DIALOGCANCEL,'Cancelar');
  cxSetResourceString(@dxSBAR_DRAGTOMAKEMENUFLOAT,'Arraste para fazer este menu float');
  cxSetResourceString(@dxSBAR_EXPAND,'expandir(Ctrl-Down)');
  cxSetResourceString(@dxSBAR_GALLERYEMPTYFILTERCAPTION,'<vazio>');
  cxSetResourceString(@dxSBAR_HIDEALLGALLERYGROUPS,'Ocultar todos os grupos');
  cxSetResourceString(@dxSBAR_HINTOPT2,'Mostrar tecla&s de atalho em descri��es');
  cxSetResourceString(@dxSBAR_IMAGEDIALOGCAPTION,'Selecionar item');
  cxSetResourceString(@dxSBAR_IMAGEINDEX,'�ndice de Imagens');
  cxSetResourceString(@dxSBAR_IMAGETEXT,'Texo');
  cxSetResourceString(@dxSBAR_INSERTEX,'Insira ...');
  cxSetResourceString(@dxSBAR_LARGEICONS,'&�cones Grandes');
  cxSetResourceString(@dxSBAR_LOOKUPDIALOGCANCEL,'Cancelar');
  cxSetResourceString(@dxSBAR_LOOKUPDIALOGCAPTION,'Valor Selecione');
  cxSetResourceString(@dxSBAR_MDICLOSE,'Fechar Janela');
  cxSetResourceString(@dxSBAR_MDIMINIMIZE,'Minimizar janela');
  cxSetResourceString(@dxSBAR_MDIRESTORE,'Restaurar Janela');
  cxSetResourceString(@dxSBAR_MENUANIM1,'(Nenhum)');
  cxSetResourceString(@dxSBAR_MENUANIM2,'Aleat�rio');
  cxSetResourceString(@dxSBAR_MENUANIM3,'mostrar');
  cxSetResourceString(@dxSBAR_MENUANIM5,'desaparecer');
  cxSetResourceString(@dxSBAR_MENUANIMATIONS,'&Menu animados:');
  cxSetResourceString(@dxSBAR_MENUSSHOWRECENTITEMS,'Me&nus mostra os comandos recentemente utilizados primeiro');
  cxSetResourceString(@dxSBAR_MINIMIZERIBBON,'Mi&nimizar o Ribbon');
  cxSetResourceString(@dxSBAR_MODIFY,'... modificar \n');
  cxSetResourceString(@dxSBAR_MOREBUTTONS,'Mais But�es');
  cxSetResourceString(@dxSBAR_MORECOMMANDS,'&Mais Comandos...');
  cxSetResourceString(@dxSBAR_MOVEDOWN,'Descer;');
  cxSetResourceString(@dxSBAR_MOVEUP,'Subir;');
  cxSetResourceString(@dxSBAR_NEWBUTTONCAPTION,'Novo Bot�o;');
  cxSetResourceString(@dxSBAR_NEWITEMCAPTION,'Novo Item;');
  cxSetResourceString(@dxSBAR_NEWRIBBONGALLERYITEMCAPTION,'Nova Galeria;');
  cxSetResourceString(@dxSBAR_NEWSEPARATORCAPTION,'Novo Separador;');
  cxSetResourceString(@dxSBAR_NEWSUBITEMCAPTION,'Novo SubItem;');
  cxSetResourceString(@dxSBAR_NOBARMANAGERS,'N�o h� TdxBarManagers dispon�vel;');
  cxSetResourceString(@dxSBAR_OK,'OK;');
  cxSetResourceString(@dxSBAR_ONEOFTOOLBARSALREADYMERGED,'Uma das barras de ferramentas do gerente de bar especificado j� est� mesclado.');
  cxSetResourceString(@dxSBAR_ONEOFTOOLBARSHASMERGEDTOOLBARS,'Uma das barras de ferramentas do gerente de bar especificado fundiu barras de ferramentas.');
  cxSetResourceString(@dxSBAR_OTHEROPTIONS,'Outro .');
  cxSetResourceString(@dxSBAR_PERSMENUSANDTOOLBARS,'Menus personalizados e barras de ferramentas.  ');
  cxSetResourceString(@dxSBAR_PLACEFORCONTROL,'O local para a .');
  cxSetResourceString(@dxSBAR_QUICKACCESSALREADYHASGROUPBUTTON,'Barra de Acesso R�pido j� cont�m GroupButton com a mesma barra de ferramentas');
  cxSetResourceString(@dxSBAR_QUICKACCESSGROUPBUTTONTOOLBARNOTDOCKEDINRIBBON,'Barra de Ferramentas de Acesso R�pido Grupo Bot�o n�o � encaixada na Faixa');
  cxSetResourceString(@dxSBAR_RECURSIVEGROUPS,'Voc� n�o pode criar grupos recursivos');
  cxSetResourceString(@dxSBAR_RECURSIVEMENUS,'Voc� n�o pode criar menus recursiva');
  cxSetResourceString(@dxSBAR_REMOVEFROMQAT,'&Retire do Quick Access Toolbar');
  cxSetResourceString(@dxSBAR_RENAMEEX,'Renomear...');
  cxSetResourceString(@dxSBAR_RESETTOOLBAR,'&Redefinir Barra de Ferramentas');
  cxSetResourceString(@dxSBAR_RESETUSAGEDATA,'&Redefinir dados de uso');
  cxSetResourceString(@dxSBAR_RIBBONADDEMPTYGROUP,'Adicionar Grupo vaziu');
  cxSetResourceString(@dxSBAR_RIBBONADDGROUPWITHTOOLBAR,'Adicionar Grupo Com Toolbar');
  cxSetResourceString(@dxSBAR_RIBBONBADOWNER,'%s deveria ter como seu TCustomForm Propriet�rio');
  cxSetResourceString(@dxSBAR_RIBBONBADPARENT,'%s TCustomForm deve ter como seu parente');
  cxSetResourceString(@dxSBAR_RIBBONDELETEGROUP,'Apagar Grupo');
  cxSetResourceString(@dxSBAR_RIBBONDELETETAB,'Excluir Tab');
  cxSetResourceString(@dxSBAR_RIBBONMORETHANONE,'Deve haver apenas um %s inst�ncia sobre a forma');
  cxSetResourceString(@dxSBAR_SHOWABOVERIBBON,'&Show Quick Access Toolbar Acima da Faixa de Op��es');
  cxSetResourceString(@dxSBAR_SHOWALLGALLERYGROUPS,'Mostrar todos os grupos');
  cxSetResourceString(@dxSBAR_SHOWBELOWRIBBON,'&Show Quick Access Toolbar Abaixo do Friso');
  cxSetResourceString(@dxSBAR_SHOWFULLMENUSAFTERDELAY,'Mostrar menus completos ap�s um pequeno atraso');
  cxSetResourceString(@dxSBAR_TABSHEET2,'Comandos ');
  cxSetResourceString(@dxSBAR_TABSHEET3,'Op��es');
  cxSetResourceString(@dxSBAR_TDELETE,'&Excluir');
  cxSetResourceString(@dxSBAR_TNEW,'&Novo ...');
  cxSetResourceString(@dxSBAR_TOOLBAREXISTS,'Uma barra de ferramentas chamada ''%s'' j� existe. Digite outro nome.');
  cxSetResourceString(@dxSBAR_TOOLBARHASMERGEDTOOLBARS,'O ''%s'' toolbar has merged toolbars');
  cxSetResourceString(@dxSBAR_TOOLBARNEWNAME,'personalizar');
  cxSetResourceString(@dxSBAR_TOOLBARRENAME,'Renomear \nToolbar');
  cxSetResourceString(@dxSBAR_TOOLBARSALREADYMERGED,'O ''%s'' toolbarj� est� mesclado com o ''%s'' toolbar');
  cxSetResourceString(@dxSBAR_TOOLBARSARENOTMERGED,'O ''%s'' toolbarn�o � mesclado com o''%s'' toolbar');
  cxSetResourceString(@dxSBAR_TREEVIEWDIALOGCAPTION,'Selecionar item');
  cxSetResourceString(@dxSBAR_TRENAME,'R&enomear...');
  cxSetResourceString(@dxSBAR_VISIBLE,'Vis�vel');
  cxSetResourceString(@dxSBAR_WANTTOCLEARCOMMANDS,'Tem certeza de que deseja excluir todos os comandos no ''%s'' categoria?');
  cxSetResourceString(@dxSBAR_WANTTODELETECATEGORY,'Tem certeza de que deseja apagar o ''%s'' categoria \n- \ncategoria?');
  cxSetResourceString(@dxSBAR_WANTTODELETECOMPLEXITEM,'Um dos objetos selecionados � um item que tem v�rios links. Tem certeza de que deseja excluir estes links?');
  cxSetResourceString(@dxSBAR_WANTTODELETETOOLBAR,'Tem certeza de que deseja apagar o ''%s'' toolbar?');
  cxSetResourceString(@dxSBAR_WANTTODELETETOOLBARS,'Tem certeza que pretende eliminar as barras de ferramentas selecionadas?');
  cxSetResourceString(@dxSBAR_WANTTORESETTOOLBAR,'Tem certeza que pretende repor as altera��es efectuadas ao''%s'' toolbar?');
  cxSetResourceString(@dxSBAR_WANTTORESETUSAGEDATA,'Isto ir� apagar o registro dos comandos usados neste aplicativo e restaurar o conjunto predefinido de comandos vis�veis para os menus e barras de ferramentas. Ele n�o ir� desfazer todas as personaliza��es expl�cito. Tem certeza de que deseja prosseguir?');
  cxSetResourceString(@scxActualFinishField,'Concluir real');
  cxSetResourceString(@scxActualStartField,'In�cio real');
  cxSetResourceString(@scxAdd,'&Adiciona');
  cxSetResourceString(@scxAdd1,'Adiciona');
  cxSetResourceString(@scxAddedHolidaysGroupBox,'Adicionar Feriados');
  cxSetResourceString(@scxAdvance0h,'0 horas antes de Iniciar');
  cxSetResourceString(@scxAdvance10m,'10 minutos antes de Iniciar');
  cxSetResourceString(@scxAdvance15m,'15 minutos antes de Iniciar');
  cxSetResourceString(@scxAdvance5m,'5 minutos antes de Iniciar');
  cxSetResourceString(@scxAllDayEvent,'&Todo Evento do Dia');
  cxSetResourceString(@scxAllDayEventField,'Todo Evento do Dia');
  cxSetResourceString(@scxApply,'&Aplicar');
  cxSetResourceString(@scxBoolTrue,'verdadeiro');
  cxSetResourceString(@scxBusy,'Ocupado');
  cxSetResourceString(@scxCancel,'&Cancelar');
  cxSetResourceString(@scxCantCreateExportOutputFile,'N�o foi poss�vel criar o arquivo de sa�da de exporta��o');
  cxSetResourceString(@scxCantCreateRegistryKey,'N�o � Poss�vel Criar uma chave de registro: \%s');
  cxSetResourceString(@scxCantOpenRegistryKey,'N�o � poss�vel abrir a chave de registro: \%s');
  cxSetResourceString(@scxCaptionField,'Legenda');
  cxSetResourceString(@scxClose,'&Fechar');
  cxSetResourceString(@scxComplete,'Completo');
  cxSetResourceString(@scxConfirmLostExceptions,'Todas as exce��es associadas a este evento recorrente ser� perdido, Confirma?');
  cxSetResourceString(@scxConverterCantCreateStyleRepository,'N�o � poss�vel criar o reposit�rio Estilo');
  cxSetResourceString(@scxCustom,'Personalizar Ocorr�ncia');
  cxSetResourceString(@scxDaily,'&Diariamente \n');
  cxSetResourceString(@scxDate,'&Data:');
  cxSetResourceString(@scxDay,'Dia');
  cxSetResourceString(@scxDay1,'dia');
  cxSetResourceString(@scxDayCalendar,'Dia do calend�rio');
  cxSetResourceString(@scxDays,'dia(s)');
  cxSetResourceString(@scxDeferred,'Adiado');
  cxSetResourceString(@scxDelete,'&Apagar');
  cxSetResourceString(@scxDelete1,'Apagar');
  cxSetResourceString(@scxDeleteConfirmation,'Este item foi alterado. Voc� tem certeza que deseja apag�-lo?');
  cxSetResourceString(@scxDeleteRecurringEventDescription,'� um evento recorrente. Voc� quer excluir somente esta ocorr�ncia ou a s�rie?');
  cxSetResourceString(@scxDeleteTypeDialogCaption,'Confirma a Exclusao');
  cxSetResourceString(@scxDeleteTypeOccurrenceLabel,'Excluir esta ocorr�ncia');
  cxSetResourceString(@scxDeleteTypeSeriesLabel,'Excluir a s�rie');
  cxSetResourceString(@scxDown,'&Abaixar');
  cxSetResourceString(@scxDuration,'Dura��o:');
  cxSetResourceString(@scxEdit,'&Editar');
  cxSetResourceString(@scxEdit1,'Editar');
  cxSetResourceString(@scxEditRecurringEventDescription,'� um evento recorrente. Voc� quer abrir somente esta ocorr�ncia ou a s�rie?');
  cxSetResourceString(@scxEditTypeDialogCaption,'Abrir Item Peri�dico');
  cxSetResourceString(@scxEditTypeOccurrenceLabel,'Abrir esta ocorr�ncia');
  cxSetResourceString(@scxEditTypeSeriesLabel,'Abrir a s�rie');
  cxSetResourceString(@scxEmptyExportCache,'Exporta��o cache est� vazio');
  cxSetResourceString(@scxEnabledField,'Habilitado');
  cxSetResourceString(@scxEnd,'&Fim:');
  cxSetResourceString(@scxEndAfter,'ap�s o Fim:');
  cxSetResourceString(@scxEndBy,'Terminado por:');
  cxSetResourceString(@scxEndTime,'&Fim do Tempo:');
  cxSetResourceString(@scxErrorStoreObject,'Erro loja%s objeto');
  cxSetResourceString(@scxEvent,'Evento');
  cxSetResourceString(@scxEventLabel0,'Importante');
  cxSetResourceString(@scxEventLabel1,'Neg�cio');
  cxSetResourceString(@scxEventLabel2,'Pessoal');
  cxSetResourceString(@scxEventLabel3,'F�rias');
  cxSetResourceString(@scxEventLabel4,'Deve Comparecer');
  cxSetResourceString(@scxEventLabel5,'Viagem Necess�ria');
  cxSetResourceString(@scxEventLabel6,'Necessidades de prepara��o');
  cxSetResourceString(@scxEventLabel7,'Anivers�rio');
  cxSetResourceString(@scxEventLabel8,'Folga');
  cxSetResourceString(@scxEventLabel9,'Telefone');
  cxSetResourceString(@scxEventLabelNone,'Nenhum ');
  cxSetResourceString(@scxEventsConflict,'Conflitos com outro evento em sua programa��o.');
  cxSetResourceString(@scxEventTime,' Tempo do Evento');
  cxSetResourceString(@scxEventTypeField,'Tipo \n');
  cxSetResourceString(@scxEvery,'Cada');
  cxSetResourceString(@scxEveryWeekDay,'Cada dia da semana');
  cxSetResourceString(@scxException,'Ocorr�ncia de exce��o');
  cxSetResourceString(@scxExceptionEvent,'evento de exce��o');
  cxSetResourceString(@scxExitConfirmation,'Deseja salvar as altera��es??');
  cxSetResourceString(@scxExport,'&Exporta \n');
  cxSetResourceString(@scxExportToExcel,'Exportar paraMS Excel (*.xls)');
  cxSetResourceString(@scxExportToHtml,'Exportar para a p�gina da Web (*.html)');
  cxSetResourceString(@scxExportToText,'Exportar para o formato de texto(*.txt)');
  cxSetResourceString(@scxExportToXml,'Exportar para o documento XML(*.xml)');
  cxSetResourceString(@scxFindAvailableTime,'Encontre tempo dispon�vel');
  cxSetResourceString(@scxFinishField,'Concluir');
  cxSetResourceString(@scxFinishToFinish,'Concluir-para-Terminar');
  cxSetResourceString(@scxFinishToFinishLong,'Concluir-para-terminar (FF)');
  cxSetResourceString(@scxFinishToStart,'Concluir-para-Iniciar');
  cxSetResourceString(@scxFinishToStartLong,'Concluir-para-Iniciar (FS)');
  cxSetResourceString(@scxFirst,'Primeiro');
  cxSetResourceString(@scxFirstButtonHint,'Primeiro recurso');
  cxSetResourceString(@scxFourth,'quarta');
  cxSetResourceString(@scxFree,'Livre');
  cxSetResourceString(@scxFrom,'De:');
  cxSetResourceString(@scxFullYear,'O Ano  \nInteiro');
  cxSetResourceString(@scxGanttEventHint,'Terefa: %s \nCompleto: %d %% \nInicio: %s \nAcabamento: %s');
  cxSetResourceString(@scxGoToDateDialogCaption,'Ir para data');
  cxSetResourceString(@scxGridBandsQuickCustomizationHint,'Clique aqui para exibir / ocultar / bandas mover');
  cxSetResourceString(@scxGridChartAlignment,'Alinhamento');
  cxSetResourceString(@scxGridChartAlignmentCenter,'Centro');
  cxSetResourceString(@scxGridChartAlignmentDefault,'Padr�o');
  cxSetResourceString(@scxGridChartAlignmentEnd,'Fim');
  cxSetResourceString(@scxGridChartAlignmentStart,'Iniciar');
  cxSetResourceString(@scxGridChartAreaDiagramDisplayText,'Diagrama de �rea');
  cxSetResourceString(@scxGridChartBarDiagramDisplayText,'Diagrama de barras');
  cxSetResourceString(@scxGridChartBorder,'Borda');
  cxSetResourceString(@scxGridChartCategoriesDisplayText,'Dados');
  cxSetResourceString(@scxGridChartColumnDiagramDisplayText,'Diagrama de Coluna');
  cxSetResourceString(@scxGridChartCustomizationFormDataGroupsPageCaption,'Grupos de Dados');
  cxSetResourceString(@scxGridChartCustomizationFormNoSortedSeries,'<nenhuma s�rie>');
  cxSetResourceString(@scxGridChartCustomizationFormOptionsPageCaption,'Op��es');
  cxSetResourceString(@scxGridChartCustomizationFormSeriesPageCaption,'S�rie');
  cxSetResourceString(@scxGridChartCustomizationFormSortBySeries,'Ordenar por:');
  cxSetResourceString(@scxGridChartDiagramSelector,'Diagrama Selector');
  cxSetResourceString(@scxGridChartLegend,'Legenda');
  cxSetResourceString(@scxGridChartLineDiagramDisplayText,'Diagrama Line');
  cxSetResourceString(@scxGridChartNoneDiagramDisplayText,'No diagrama');
  cxSetResourceString(@scxGridChartOrientation,'Orienta��o');
  cxSetResourceString(@scxGridChartOrientationDefault,'Padr�o');
  cxSetResourceString(@scxGridChartOther,'Outro');
  cxSetResourceString(@scxGridChartPieDiagramDisplayText,'Diagrama Pie');
  cxSetResourceString(@scxGridChartPosition,'Posi��o');
  cxSetResourceString(@scxGridChartPositionBottom,'Fundo');
  cxSetResourceString(@scxGridChartPositionDefault,'Padr�o');
  cxSetResourceString(@scxGridChartPositionLeft,'Esquerdo');
  cxSetResourceString(@scxGridChartPositionNone,'Nenhum');
  cxSetResourceString(@scxGridChartPositionRight,'Direito');
  cxSetResourceString(@scxGridChartPositionTop,'Topo');
  cxSetResourceString(@scxGridChartTitle,'T�tulo');
  cxSetResourceString(@scxGridChartToolBoxCustomizeButtonCaption,'Customize Gr�fico');
  cxSetResourceString(@scxGridChartToolBoxDataLevels,'Dados dos n�veis:');
  cxSetResourceString(@scxGridChartToolBoxDataLevelSelectValue,'Selecione valor');
  cxSetResourceString(@scxGridChartValueHintFormat,'%s para %s � %s');
  cxSetResourceString(@scxGridChartValueHints,'Valor Dicas');
  cxSetResourceString(@scxGridColumnsQuickCustomizationHint,'Clique aqui para mostrar / esconder / mover colunas');
  cxSetResourceString(@scxGridConverterIntermediaryMissing,'Faltando um componente intermedi�rio! \nPor favor, adicione uma %s componente para o formul�rio.');
  cxSetResourceString(@scxGridConverterNotExistComponent,'Componente n�o existe');
  cxSetResourceString(@scxGridConverterNotExistGrid,'cxGridn�o existe');
  cxSetResourceString(@scxGridCustomizationFormBandsPageCaption,'Bandas');
  cxSetResourceString(@scxGridCustomizationFormCaption,'Personaliza��o');
  cxSetResourceString(@scxGridCustomizationFormColumnsPageCaption,'Colunas');
  cxSetResourceString(@scxGridCustomizationFormRowsPageCaption,'Filas');
  cxSetResourceString(@scxGridDeletingFocusedConfirmationText,'Deletar registro?');
  cxSetResourceString(@scxGridDeletingSelectedConfirmationText,'Excluir todos os registros selecionados?');
  cxSetResourceString(@scxGridFilterApplyButtonCaption,'Aplicar filtro');
  cxSetResourceString(@scxGridFilterCustomizeButtonCaption,'Personalizar ...');
  cxSetResourceString(@scxGridFilterIsEmpty,'<Filtro est� vazio>');
  cxSetResourceString(@scxGridFilterRowInfoText,'Clique aqui para definir um filtro');
  cxSetResourceString(@scxGridFuture,'Futuro');
  cxSetResourceString(@scxGridGroupByBoxCaption,'Arraste um cabe�alho da coluna aqui para agrupar por essa coluna');
  cxSetResourceString(@scxGridLast14Days,'�ltimos 14 dias');
  cxSetResourceString(@scxGridLast30Days,'�ltimos 30 dias');
  cxSetResourceString(@scxGridLast7Days,'�ltimos 7 dias');
  cxSetResourceString(@scxGridLastMonth,'m�s passado');
  cxSetResourceString(@scxGridLastTwoWeeks,'�ltimas duas semanas');
  cxSetResourceString(@scxGridLastWeek,'semana passada');
  cxSetResourceString(@scxGridLastYear,'ano passado');
  cxSetResourceString(@scxGridNewItemRowInfoText,'Clique aqui para adicionar uma nova linha');
  cxSetResourceString(@scxGridNext14Days,'Pr�ximos 14 dias');
  cxSetResourceString(@scxGridNext30Days,'Pr�ximos 30 dias');
  cxSetResourceString(@scxGridNext7Days,'Pr�ximos 7 dias');
  cxSetResourceString(@scxGridNextMonth,'Pr�ximo m�s \n- \npr�ximo m�s');
  cxSetResourceString(@scxGridNextTwoWeeks,'Pr�ximas duas semanas');
  cxSetResourceString(@scxGridNextWeek,'Pr�xima semana \n');
  cxSetResourceString(@scxGridNextYear,'ano seguinte');
  cxSetResourceString(@scxGridNoDataInfoText,'<N�o h� dados para mostrar>');
  cxSetResourceString(@scxGridPast,'Passado');
  cxSetResourceString(@scxGridRecursiveLevels,'Voc� n�o pode criar n�veis recursivos');
  cxSetResourceString(@scxGridThisMonth,'Este m�s');
  cxSetResourceString(@scxGridThisWeek,'Esta semana');
  cxSetResourceString(@scxGridThisYear,'Este ano');
  cxSetResourceString(@scxGridToday,'Hoje');
  cxSetResourceString(@scxGridTomorrow,'Amanh�');
  cxSetResourceString(@scxGridYesterday,'Ontem');
  cxSetResourceString(@scxHalfYear,'Semestre');
  cxSetResourceString(@scxHolidayDate,'Data:');
  cxSetResourceString(@scxHolidayName,'Nome:');
  cxSetResourceString(@scxHolidaysEditorCaption,'Editor Feriados');
  cxSetResourceString(@scxHolidaysGroupBox,'Feriados');
  cxSetResourceString(@scxHolidaysLocationEditorCaption,'Editor Local');
  cxSetResourceString(@scxHolidaysLocationHolidayEditorCaption,'Editor de feriado');
  cxSetResourceString(@scxHour,'Hora');
  cxSetResourceString(@scxHours,'Horas');
  cxSetResourceString(@scxIllegalHeight,'Illegal altura da linha');
  cxSetResourceString(@scxIllegalWidth,'Illegal largura da coluna');
  cxSetResourceString(@scxImport,'&Importa��o ');
  cxSetResourceString(@scxImportErrorCaption,'Erro de Importa��o');
  cxSetResourceString(@scxIncorrectUnion,'Uni�o incorreta de pilhas');
  cxSetResourceString(@scxInProgress,'Em Desenvolvimento');
  cxSetResourceString(@scxInvalidCellDimension,'Invalid dimens�o de c�lulas');
  cxSetResourceString(@scxInvalidColumnIndex,'O �ndice de coluna %d fora dos limites');
  cxSetResourceString(@scxInvalidColumnRowCount,'Contagem de coluna inv�lido ou linha');
  cxSetResourceString(@scxInvalidCustomField,'Inv�lido campo personalizado');
  cxSetResourceString(@scxInvalidFieldName,'Nome do Campo Inv�lido');
  cxSetResourceString(@scxInvalidNumber,'Voc� deve digitar um n�mero v�lido.');
  cxSetResourceString(@scxInvalidRecurrenceDuration,'A dura��o do evento deve ser menor do que a freq��ncia com que ocorre. Encurtar a dura��o, ou mudar o padr�o de recorr�ncia na caixa de di�logo Recorr�ncia.');
  cxSetResourceString(@scxInvalidRowIndex,'O �ndice da linha %d fora dos limites');
  cxSetResourceString(@scxInvalidStyleIndex,'Inv�lido �ndice %d estilo');
  cxSetResourceString(@scxLabel,'R�tulo:');
  cxSetResourceString(@scxLabelField,'R�tulo');
  cxSetResourceString(@scxLast,'�ltimo');
  cxSetResourceString(@scxLastButtonHint,'�ltimo Recurso');
  cxSetResourceString(@scxLinkHint,'Tarefa Link: %s (%s) \nPara: %s \nde: %s');
  cxSetResourceString(@scxLocation,'&Local:');
  cxSetResourceString(@scxLocationField,'Local');
  cxSetResourceString(@scxLocationName,'Nome:');
  cxSetResourceString(@scxLocationsGroupBox,'Locais');
  cxSetResourceString(@scxMaskEditEmptyMaskCollectionFile,'O arquivo de m�scara cole��o est� vazia');
  cxSetResourceString(@scxMaskEditIllegalFileFormat,'Ilegal formato de arquivo');
  cxSetResourceString(@scxMaskEditInvalidEditValue,'A editar o valor � inv�lido');
  cxSetResourceString(@scxMaskEditMaskCollectionFiles,'Cole��o de arquivos M�scara');
  cxSetResourceString(@scxMaskEditNoMask,'Nenhum');
  cxSetResourceString(@scxMaskEditRegExprError,'Regular erros de express�o');
  cxSetResourceString(@scxMessageField,'Mensagem');
  cxSetResourceString(@scxMinute,'Minuto');
  cxSetResourceString(@scxMinutes,'Minutos');
  cxSetResourceString(@scxMonthCalendar,'M�s do calend�rio');
  cxSetResourceString(@scxMonthly,'&Mensal');
  cxSetResourceString(@scxMonths,'Meses');
  cxSetResourceString(@scxNextAppointment,'Pr�ximo compromisso');
  cxSetResourceString(@scxNextButtonHint,'Pr�ximo Recurso');
  cxSetResourceString(@scxNextPageButtonHint,'Pr�xima P�gina');
  cxSetResourceString(@scxNoAvailableFreeTime,'Sem h� tempo livre dispon�vel.');
  cxSetResourceString(@scxNoEndDate,'&Sem data final');
  cxSetResourceString(@scxNone,'Evento Simples');
  cxSetResourceString(@scxNoneEvent,'eventos simples');
  cxSetResourceString(@scxNotExistGridLevel,'N�vel de rede Active n�o existe');
  cxSetResourceString(@scxNotExistGridView,'Grid view n�o existe');
  cxSetResourceString(@scxNotStarted,'N�o Iniciado');
  cxSetResourceString(@scxOccurenceEvent,'ocorr�ncia de eventos');
  cxSetResourceString(@scxOccurences,'ocorr�ncias');
  cxSetResourceString(@scxOccurrence,'Ocorr�ncia Simples');
  cxSetResourceString(@scxOf,'de');
  cxSetResourceString(@scxOfEvery,'de cada');
  cxSetResourceString(@scxOneDay,'Um Dia');
  cxSetResourceString(@scxOutlookFormatMismatch,'F�rias incompatibilidade de formatos');
  cxSetResourceString(@scxOutOfOffice,'Fora do escrit�rio');
  cxSetResourceString(@scxPattern,'Padr�o de Recorr�ncia');
  cxSetResourceString(@scxpm10Minutes,'10 &Minuto');
  cxSetResourceString(@scxpm15Minutes,'&15 Minuto');
  cxSetResourceString(@scxpm30Minutes,'&30 Minuto');
  cxSetResourceString(@scxpm5Minutes,'&5 Minuto');
  cxSetResourceString(@scxpm60Minutes,'6&0 Minuto');
  cxSetResourceString(@scxpm6Minutes,'&6 Minuto');
  cxSetResourceString(@scxpmBusy,'&Ocupado');
  cxSetResourceString(@scxpmDelete,'&Excluir');
  cxSetResourceString(@scxpmEditSeries,'Editar S�rie');
  cxSetResourceString(@scxpmFree,'&Livre');
  cxSetResourceString(@scxpmFullYear,'&Ano Inteiro');
  cxSetResourceString(@scxpmGoToDate,'Ir para Data...');
  cxSetResourceString(@scxpmGotoThisDay,'Ir para o Dia');
  cxSetResourceString(@scxpmHalfYear,'&Meio-do-no');
  cxSetResourceString(@scxpmLabel,'&R�tulo');
  cxSetResourceString(@scxpmNewAllDayEvent,'Evento o Dia Todo');
  cxSetResourceString(@scxpmNewEvent,'&Novo Evento');
  cxSetResourceString(@scxpmNewRecurringEvent,'Novo Evento Recorrente');
  cxSetResourceString(@scxpmOpen,'&Abrir');
  cxSetResourceString(@scxpmOutOfOffice,'&Fora do Escrit�rio');
  cxSetResourceString(@scxpmQuarter,'&Trimestre');
  cxSetResourceString(@scxpmResourcesLayout,'editor de Recurso...');
  cxSetResourceString(@scxpmShowTimeAs,'Mostrar hora');
  cxSetResourceString(@scxpmTentative,'&Tentativa');
  cxSetResourceString(@scxpmTimeZone,' Mudan�a de fuso hor�rio');
  cxSetResourceString(@scxpmToday,'Hoje');
  cxSetResourceString(@scxPrevAppointment,'Compromisso Anterior');
  cxSetResourceString(@scxPrevButtonHint,'Recursos anterior');
  cxSetResourceString(@scxPrevPageButtonHint,'P�gina Anterior');
  cxSetResourceString(@scxQuarter,'Trimestre');
  cxSetResourceString(@scxQuarterly,'&Trimestrais');
  cxSetResourceString(@scxQuarterShort,'T');
  cxSetResourceString(@scxRangeOfRecurrence,' Intervalo de recorr�ncia');
  cxSetResourceString(@scxrCaptionReminder,'1 Lembrente');
  cxSetResourceString(@scxrCaptionReminders,'%d Lembretes');
  cxSetResourceString(@scxrDismissAllButton,'Descartar &Tudo');
  cxSetResourceString(@scxrDismissButton,'&Demitir');
  cxSetResourceString(@scxrDueIn,'Devido, em');
  cxSetResourceString(@scxRecurEvery,'Repetir a cada');
  cxSetResourceString(@scxRecurrence,'&Recorr�ncia');
  cxSetResourceString(@scxRecurrenceCaption,'recorr�ncia de eventos');
  cxSetResourceString(@scxRecurrenceDailyMessage,'Diariamente');
  cxSetResourceString(@scxRecurrenceEvent,'caso de recidiva');
  cxSetResourceString(@scxRecurrenceField,'Padr�o de Recorr�ncia');
  cxSetResourceString(@scxRecurrenceIndexField,'�ndice de recorr�ncia');
  cxSetResourceString(@scxRecurrenceLabel,'Recorr�ncia:');
  cxSetResourceString(@scxRecurrenceMonthlyMessage,'Mensal');
  cxSetResourceString(@scxRecurrencePattern,' Padr�o de recorr�ncia');
  cxSetResourceString(@scxRecurrenceWeeklyMessage,'Semanal');
  cxSetResourceString(@scxRecurrenceYearlyMessage,'Anualmente');
  cxSetResourceString(@scxRegExprCantCreateEmptyAlt,'A alternativa n�o deve estar vazia');
  cxSetResourceString(@scxRegExprCantCreateEmptyBlock,'O bloco n�o deve ser vazio');
  cxSetResourceString(@scxRegExprCantCreateEmptyEnum,'N�o � poss�vel criar enumera��o vazio');
  cxSetResourceString(@scxRegExprCantUseParameterQuantifier,'O par�metro quantificador n�o pode ser aplicado aqui');
  cxSetResourceString(@scxRegExprCantUsePlusQuantifier,'O ''+'' quantificador n�o pode ser aplicado aqui');
  cxSetResourceString(@scxRegExprCantUseStarQuantifier,'O ''*'' quantificador n�o pode ser aplicado aqui');
  cxSetResourceString(@scxRegExprEmptySourceStream,'O fluxo de origem est� vazio');
  cxSetResourceString(@scxRegExprHexNumberExpected,'N�mero hexadecimal esperado, mas ''% s'' encontrado');
  cxSetResourceString(@scxRegExprHexNumberExpected0,'N�mero hexadecimal esperado');
  cxSetResourceString(@scxRegExprIllegalIntegerValue,'Illegal valor inteiro');
  cxSetResourceString(@scxRegExprIllegalQuantifier,'% Illegal quantificador ''s''');
  cxSetResourceString(@scxRegExprIllegalSymbol,'Ilegal ''%s''');
  cxSetResourceString(@scxRegExprIncorrectParameterQuantifier,'Par�metro quantificador incorreta');
  cxSetResourceString(@scxRegExprIncorrectSpace,'O car�cter de espa�o n�o � permitido ap�s ''\''');
  cxSetResourceString(@scxRegExprLine,'linha');
  cxSetResourceString(@scxRegExprMissing,'perdido ''%s''');
  cxSetResourceString(@scxRegExprNotAssignedSourceStream,'O fluxo de origem n�o � atribu�do');
  cxSetResourceString(@scxRegExprNotCompiled,'A express�o regular n�o � compilado');
  cxSetResourceString(@scxRegExprNotSupportQuantifier,'Os quantificadores par�metro n�o s�o suportados');
  cxSetResourceString(@scxRegExprSubrangeOrder,'O personagem a partir do subrange deve ser inferior a um acabamento');
  cxSetResourceString(@scxRegExprTooBigReferenceNumber,'Demasiado grande n�mero de refer�ncia');
  cxSetResourceString(@scxRegExprUnnecessary,'desnecess�rio ''%s''');
  cxSetResourceString(@scxReminder,'Lembrete:');
  cxSetResourceString(@scxReminderDateField,'LembreteData');
  cxSetResourceString(@scxReminderField,'Lembrete');
  cxSetResourceString(@scxReminderMinutesBeforeStartField,'Lembre-me minutos antes de iniciar');
  cxSetResourceString(@scxRemoveRecur,'&Remover recorr�ncia');
  cxSetResourceString(@scxReplaceOccurrenceDate,'Alguns meses t�m menos dias s%. Por estes meses, a ocorr�ncia cair�o no �ltimo dia do m�s.');
  cxSetResourceString(@scxRequiredFieldsNeeded,'Os seguintes campos obrigat�rios \n% N�o sare atribu�do!');
  cxSetResourceString(@scxResource,'Recursos');
  cxSetResourceString(@scxResourceField,'Recursos');
  cxSetResourceString(@scxResourceLayoutCaption,'Editor de Recursos');
  cxSetResourceString(@scxrOpenItemButton,'&Abrir Item');
  cxSetResourceString(@scxrSelected,'%d lembretes selecionados');
  cxSetResourceString(@scxrSnoozeButton,'&Soneca');
  cxSetResourceString(@scxrSnoozeLabel,'&Soneca');
  cxSetResourceString(@scxrStartTime,'Hora de in�cio: %s');
  cxSetResourceString(@scxrSubject,'Assunto');
  cxSetResourceString(@scxSCalcError,'erro');
  cxSetResourceString(@scxSecond,'segunda');
  cxSetResourceString(@scxSEditRepositoryBlobItem,'BlobEdit | representa o editor BLOB');
  cxSetResourceString(@scxSEditRepositoryButtonItem,'ButtonEdit | Representa um controle editar com bot�es embutidos');
  cxSetResourceString(@scxSEditRepositoryCalcItem,'CalcEdit | Representa um controle de edi��o com uma janela da calculadora suspensa');
  cxSetResourceString(@scxSEditRepositoryCheckBoxItem,'CheckBox | Representa um controle caixa de sele��o que permite selecionar uma op��o');
  cxSetResourceString(@scxSEditRepositoryComboBoxItem,'ComboBox | Representa o editor de caixa de combina��o');
  cxSetResourceString(@scxSEditRepositoryCurrencyItem,'CurrencyEdit | Representa um editor que permite a edi��o de dados de moeda');
  cxSetResourceString(@scxSEditRepositoryDateItem,'DateEdit | Representa um controle de edi��o com um calend�rio suspenso');
  cxSetResourceString(@scxSEditRepositoryHyperLinkItem,'HyperLink | Representa um editor de texto com hiperlink funcionalidade');
  cxSetResourceString(@scxSEditRepositoryImageComboBoxItem,'ImageComboBox | Representa um editor de apresenta��o da lista de imagens e seq��ncias de texto dentro da janela suspensa');
  cxSetResourceString(@scxSEditRepositoryImageItem,'Imagem | Representa um editor de imagem');
  cxSetResourceString(@scxSEditRepositoryLookupComboBoxItem,'LookupComboBox | Representa um controle caixa de combina��o de pesquisa');
  cxSetResourceString(@scxSEditRepositoryMaskItem,'MaskEdit | Representa um gen�rico mascarado editar o controle.');
  cxSetResourceString(@scxSEditRepositoryMemoItem,'Memo | Representa um controle de edi��o que permite que dados memorando de edi��o');
  cxSetResourceString(@scxSEditRepositoryMRUItem,'MRUEdit | Representa um editor de texto de apresenta��o da lista de itens utilizados mais recentemente (MRU) dentro de uma janela suspensa');
  cxSetResourceString(@scxSEditRepositoryPopupItem,'PopupEdit | Representa um controle de edi��o com uma lista suspensa');
  cxSetResourceString(@scxSEditRepositoryRadioGroupItem,'RadioGroup | Representa um grupo de bot�es de r�dio');
  cxSetResourceString(@scxSEditRepositorySpinItem,'SpinEdit | Representa um editor de spin');
  cxSetResourceString(@scxSEditRepositoryTextItem,'TextEdit | Representa um editor de texto de linha �nica');
  cxSetResourceString(@scxSEditRepositoryTimeItem,'TimeEdit | Representa um editor exibindo valores de tempo');
  cxSetResourceString(@scxSelectAll,'Selecionar todos');
  cxSetResourceString(@scxSelectNone,'Selecionar nenhum');
  cxSetResourceString(@scxShowFewerResourcesButtonHint,'Mostre Menos Recurso');
  cxSetResourceString(@scxShowIn,'&Mostrar em:');
  cxSetResourceString(@scxShowMoreResourcesButtonHint,'Mostre Mais Recursos');
  cxSetResourceString(@scxShowTimeAs,'Mostrar hor�rio como:');
  cxSetResourceString(@scxStart,'Inicio:');
  cxSetResourceString(@scxStart1,'Inicio:');
  cxSetResourceString(@scxStartField,'Inicio');
  cxSetResourceString(@scxStartTime,'Hora de in�cio:');
  cxSetResourceString(@scxStartToFinish,'Iniciar-para-Concluir');
  cxSetResourceString(@scxStartToFinishLong,'Iniciar-para-concluir (SF)');
  cxSetResourceString(@scxStateField,'Estado');
  cxSetResourceString(@scxStyleManagerCreate,'N�o � poss�vel criar gerente de estilo');
  cxSetResourceString(@scxStyleManagerKill,'O Gerente de estilo est� sendo usado em outro lugar e n�o pode ser liberada nesta fase');
  cxSetResourceString(@scxSubject,'&Assunto:');
  cxSetResourceString(@scxSuffixDay,'dia');
  cxSetResourceString(@scxSuffixDays,'Dias \n');
  cxSetResourceString(@scxSuffixHour,'hora');
  cxSetResourceString(@scxSuffixHours,'Horas');
  cxSetResourceString(@scxSuffixMinute,'minuto');
  cxSetResourceString(@scxSuffixMinutes,'minutos');
  cxSetResourceString(@scxSuffixWeek,'semana');
  cxSetResourceString(@scxSuffixWeeks,'semanas');
  cxSetResourceString(@scxTaskComplete,'Tarefa Completa:');
  cxSetResourceString(@scxTaskCompleteField,'Tarefe Completa');
  cxSetResourceString(@scxTaskDependencyEditorCaption,'Tarefa Pendente');
  cxSetResourceString(@scxTaskIndexField,'Indice da Tarefa');
  cxSetResourceString(@scxTaskLinksField,'Tarefe Links');
  cxSetResourceString(@scxTaskStatus,'Estado da Tarefa');
  cxSetResourceString(@scxTaskStatusField,'Estado da Tarefa');
  cxSetResourceString(@scxTaskWrongTimeBounds,'Uma nova data deve ser inserida dentro do per�odo de % s -% s.');
  cxSetResourceString(@scxTentative,'Tentativa');
  cxSetResourceString(@scxThe,'A');
  cxSetResourceString(@scxThird,'terceiro');
  cxSetResourceString(@scxTime0m,'0 minuto');
  cxSetResourceString(@scxTime10h,'10 horas');
  cxSetResourceString(@scxTime10m,'10 minutos');
  cxSetResourceString(@scxTime11h,'11 horas');
  cxSetResourceString(@scxTime12h,'12 horas');
  cxSetResourceString(@scxTime15m,'15 minutos');
  cxSetResourceString(@scxTime18h,'18 horas');
  cxSetResourceString(@scxTime1d,'1 dia');
  cxSetResourceString(@scxTime1h,'1 horas');
  cxSetResourceString(@scxTime1w,'1 semana');
  cxSetResourceString(@scxTime20m,'20 minutos');
  cxSetResourceString(@scxTime2d,'2 dia');
  cxSetResourceString(@scxTime2h,'2 horas');
  cxSetResourceString(@scxTime2w,'2 semanas');
  cxSetResourceString(@scxTime30m,'30 minutos');
  cxSetResourceString(@scxTime3d,'3 dia');
  cxSetResourceString(@scxTime3h,'3 horas');
  cxSetResourceString(@scxTime4d,'4 dia');
  cxSetResourceString(@scxTime4h,'4 horas');
  cxSetResourceString(@scxTime5h,'5 horas');
  cxSetResourceString(@scxTime5m,'5 minutos');
  cxSetResourceString(@scxTime6h,'6 horas');
  cxSetResourceString(@scxTime7h,'7 horas');
  cxSetResourceString(@scxTime8h,'8 horas');
  cxSetResourceString(@scxTime9h,'9 horas');
  cxSetResourceString(@scxTo,'Para:');
  cxSetResourceString(@scxType,'&Tipo:');
  cxSetResourceString(@scxUnsupportedExport,'Tipo exporta��o n�o suportada: %1');
  cxSetResourceString(@scxUntitled,'Sem T�tulo');
  cxSetResourceString(@scxUntitledEvent,'Sem Evento');
  cxSetResourceString(@scxUp,'&Pra Cima');
  cxSetResourceString(@scxWaiting,'Esperando');
  cxSetResourceString(@scxWeekCalendar,'Calend�rio da semana');
  cxSetResourceString(@scxWeekday,'Semana');
  cxSetResourceString(@scxWeekendday,'Dia da Semana \n');
  cxSetResourceString(@scxWeekly,'&Semanal');
  cxSetResourceString(@scxWeeksOn,'Semana(s) Em:');
  cxSetResourceString(@scxWorkbookWrite,'Erro ao gravar arquivo XLS');
  cxSetResourceString(@scxWorkWeekCalendar,'Trabalho Agenda da Semana');
  cxSetResourceString(@scxWrongPattern,'O padr�o de recorr�ncia n�o � v�lido.');
  cxSetResourceString(@scxWrongTimeBounds,'A data final que voc� inseriu ocorre antes da data de in�cio.');
  cxSetResourceString(@scxYearly,'&Anualmente');
  cxSetResourceString(@sdx3DEffects,'Efeitos 3D');
  cxSetResourceString(@sdx760V12Transmission,'Elec 6-velocidades autom�tica w / Steptronic');
  cxSetResourceString(@sdxAbortPrinting,'Abortar impress�o?');
  cxSetResourceString(@sdxAddAndDesignReport,'Adicionar e D&esign Report...');
  cxSetResourceString(@sdxAddItemsToComposition,'Adicionar Itens ao Composi��o');
  cxSetResourceString(@sdxAddReport,'Adicionar relat�rio');
  cxSetResourceString(@sdxAddressCaption,'Endere�o');
  cxSetResourceString(@sdxAdjustOnScale,'&Ajuste de Escala');
  cxSetResourceString(@sdxAdjustTo,'&Para ajustar');
  cxSetResourceString(@sdxAdministration,'Administra��o');
  cxSetResourceString(@sdxAggregatedLinks,'Agregados Links:');
  cxSetResourceString(@sdxAlignment,'Alinhamento');
  cxSetResourceString(@sdxAllDayMessage,'todos os dias');
  cxSetResourceString(@sdxAllRecords,'Todos os registros');
  cxSetResourceString(@sdxAncestorError,'Encaixe e opera��es escondendo autom�tica n�o est�o dispon�veis para controles declarados em um formul�rio ancestral.');
  cxSetResourceString(@sdxAppearance,'Apar�ncia');
  cxSetResourceString(@sdxApril,'Abril');
  cxSetResourceString(@sdxAprilShort,'Abril');
  cxSetResourceString(@sdxAugust,'Agosto');
  cxSetResourceString(@sdxAugustShort,'Agosto');
  cxSetResourceString(@sdxAutoCalcPreviewLineCount,'A&uto Calcular Preview Lines');
  cxSetResourceString(@sdxAutoColumnsExpand,'A&uto expandir Colunas');
  cxSetResourceString(@sdxAutoNodesExpand,'A&uto Expanda os n�s');
  cxSetResourceString(@sdxAutoRowsExpand,'Auto &Linhas expandir');
  cxSetResourceString(@sdxAutoTextDialogCaption,'Editar entradas de AutoTexto');
  cxSetResourceString(@sdxAutoWidth,'Auto &Largura');
  cxSetResourceString(@sdxAvailableItems,'Itens dispon�veis');
  cxSetResourceString(@sdxAvailableLinks,'&Dispon�vel Links');
  cxSetResourceString(@sdxAvailableReportLinks,'Dispon�vel ReportLinks');
  cxSetResourceString(@sdxAvailableSources,'&Dispon�vel Fonte(s)');
  cxSetResourceString(@sdxBackground,'&Fundo');
  cxSetResourceString(@sdxBadDatePrintRange,'A data na caixa de fim n�o pode ser anterior � data na caixa In�cio.');
  cxSetResourceString(@sdxBadTimePrintRange,'A hora de imprimir n�o s�o v�lidos. O hor�rio de in�cio deve preceder o tempo do fim.');
  cxSetResourceString(@sdxBandFont,'Band Fonte');
  cxSetResourceString(@sdxBands,'&Bandas');
  cxSetResourceString(@sdxBandsOnEveryPage,'Bandas');
  cxSetResourceString(@sdxBaseStyle,'Base Estilo');
  cxSetResourceString(@sdxBehaviors,'Comportamentos');
  cxSetResourceString(@sdxBehaviorsTab,'Comportamentos');
  cxSetResourceString(@sdxBestFit,'&Melhor Ajuste');
  cxSetResourceString(@sdxBorderColor,'&Cor da Borda:');
  cxSetResourceString(@sdxBorderLines,'&borda');
  cxSetResourceString(@sdxBorders,'Bordas');
  cxSetResourceString(@sdxBottom,'&Fundo:');
  cxSetResourceString(@sdxBottomMargin,'Margem inferior');
  cxSetResourceString(@sdxBrushColor,'Escova de cor');
  cxSetResourceString(@sdxBrushDlgCaption,'Propriedades da Escova');
  cxSetResourceString(@sdxBtnAddComposition,'Add &Composi��o');
  cxSetResourceString(@sdxBtnApply,'&Aplicar');
  cxSetResourceString(@sdxBtnAutomatic,'&Autom�tico');
  cxSetResourceString(@sdxBtnBackground,'segundo plano');
  cxSetResourceString(@sdxBtnBrowse,'&Navegue...');
  cxSetResourceString(@sdxBtnCancel,'Cancelar');
  cxSetResourceString(@sdxBtnChangeFont,'Alterar Fo&nte...');
  cxSetResourceString(@sdxBtnClose,'Fechar');
  cxSetResourceString(@sdxBtnCopy,'&Copiar...');
  cxSetResourceString(@sdxBtnDefault,'&Padr�o...');
  cxSetResourceString(@sdxBtnDefinePrintStyles,'&Definir Estilos...');
  cxSetResourceString(@sdxBtnDelete,'&Deletar...');
  cxSetResourceString(@sdxBtnDescription,'&Descri��o ...');
  cxSetResourceString(@sdxBtnEdit,'&Editar...');
  cxSetResourceString(@sdxBtnEvenFont,'igualar Fonte...');
  cxSetResourceString(@sdxBtnFillEffects,'&Efeitos de Preenchimento...');
  cxSetResourceString(@sdxBtnFix,'&fixar');
  cxSetResourceString(@sdxBtnFixedFont,'Fonte Fixa ...');
  cxSetResourceString(@sdxBtnFont,'Fo&nte...');
  cxSetResourceString(@sdxBtnGroupFont,'Gru&po Fonte ...');
  cxSetResourceString(@sdxBtnHeaderBackground,'&segundo plano');
  cxSetResourceString(@sdxBtnHeadersFont,'&Fonte cabe�alhos ...');
  cxSetResourceString(@sdxBtnHelp,'&Ajuda');
  cxSetResourceString(@sdxBtnIgnore,'&Ignorar');
  cxSetResourceString(@sdxBtnInvertColors,'I&nverter Cores');
  cxSetResourceString(@sdxBtnMoreColors,'&Mais Cores ...');
  cxSetResourceString(@sdxBtnMoveDown,'Descer');
  cxSetResourceString(@sdxBtnMoveUp,'Subir');
  cxSetResourceString(@sdxBtnNew,'&Novo...');
  cxSetResourceString(@sdxBtnNoFill,'&sem preenchimento');
  cxSetResourceString(@sdxBtnNone,'&Nenhum');
  cxSetResourceString(@sdxBtnOptions,'&Op��es ...');
  cxSetResourceString(@sdxBtnOtherTexture,'Outra Te&xtura...');
  cxSetResourceString(@sdxBtnPageSetup,'Pa&gina Configura��o...');
  cxSetResourceString(@sdxBtnPrint,'Imprimir ...');
  cxSetResourceString(@sdxBtnPrintPreview,'Imprimir Pre&view...');
  cxSetResourceString(@sdxBtnPrintStyles,'Estilos de Impress�o');
  cxSetResourceString(@sdxBtnProperties,'P&ropriedades...');
  cxSetResourceString(@sdxBtnRemoveInconsistents,'Remove desnecess�rios');
  cxSetResourceString(@sdxBtnRename,'&Renomear...');
  cxSetResourceString(@sdxBtnRestoreDefaults,'&Restaurar Padr�es');
  cxSetResourceString(@sdxBtnRestoreOriginal,'restaurar &Original');
  cxSetResourceString(@sdxBtnSaveAs,'Savar &Como...');
  cxSetResourceString(@sdxBtnSelectPicture,'Se&leciona Figura...');
  cxSetResourceString(@sdxBtnShowToolBar,'Mostrar&ToolBar');
  cxSetResourceString(@sdxBtnStyleOptions,'Op��es de Estilo ...');
  cxSetResourceString(@sdxBtnTexture,'&Textura...');
  cxSetResourceString(@sdxBtnTextureClear,'Limpar');
  cxSetResourceString(@sdxBtnTitleProperties,'T�tulo Propriedades ...');
  cxSetResourceString(@sdxBtnYes,'&Sim');
  cxSetResourceString(@sdxBtnYesToAll,'Sim para &Todos');
  cxSetResourceString(@sdxBuildingReport,'Construindo o relat�rio: Completo %d%%');
  cxSetResourceString(@sdxBuildingReportStatusText,'Construindo o relat�rio - Pressione Esc para cancelar');
  cxSetResourceString(@sdxButtons,'Bot�es');
  cxSetResourceString(@sdxByBands,'Por Bandas');
  cxSetResourceString(@sdxByColumns,'Por Colunas');
  cxSetResourceString(@sdxByRows,'Por &Linhas');
  cxSetResourceString(@sdxByTopLevelGroups,'Por grupos N�vel Superior');
  cxSetResourceString(@sdxCancel,'Cancelar');
  cxSetResourceString(@sdxCannotLoadImage,'N�o pode Carregar Imagem "%s"');
  cxSetResourceString(@sdxCannotPrintNoItemsAvailable,'N�o h� itens dispon�veis dentro do intervalo de impress�o especificado.');
  cxSetResourceString(@sdxCannotPrintNoSelectedItems,'N�o � poss�vel imprimir a n�o ser um item � selecionado. Selecione um item e tente imprimir novamente.');
  cxSetResourceString(@sdxCannotRenameFolderText,'N�o � poss�vel renomear a pasta "%s". Uma pasta com o nome "%s" j� existe. Especifique um nome diferente.');
  cxSetResourceString(@sdxCannotRenameItemText,'N�o � poss�vel renomear item "%s". Um item com o nome "%s" j� existe. Especifique um nome diferente.');
  cxSetResourceString(@sdxCannotUseOnEveryPageMode,'N�o � poss�vel usar OnEveryPage Mode \n \nVoc� deve ou (e) \n - Fechar todos os registros mestre \n - Alternar "desfazer Op��o" off on "Comportamentos" Tab');
  cxSetResourceString(@sdxCannotUseOnEveryPageModeInAggregatedState,'N�o � poss�vel usar OnEveryPage Mode \nDurante o desempenho de agregados Estado');
  cxSetResourceString(@sdxCaption,'&t�tulo:');
  cxSetResourceString(@sdxCaptionColor,'Cor de t�tulor:');
  cxSetResourceString(@sdxCaptionNodeFont,'N�vel  t�tulo Fonte');
  cxSetResourceString(@sdxCaptionStyle,'T�tulo');
  cxSetResourceString(@sdxCaptionTransparent,'T�tulo transparente');
  cxSetResourceString(@sdxCardCaptionRowStyle,'Cart�o t�tulo linha');
  cxSetResourceString(@sdxCardRowCaptionStyle,'Cart�o linha t�tulo');
  cxSetResourceString(@sdxCardsRows,'&Cart�es');
  cxSetResourceString(@sdxCardsTab,'Cart�es');
  cxSetResourceString(@sdxCarLevelCaption,'Carros');
  cxSetResourceString(@sdxCarManufacturer,'Fabricante');
  cxSetResourceString(@sdxCarManufacturerCountry1,'Alemanha');
  cxSetResourceString(@sdxCarManufacturerCountry2,'Estados Unidos');
  cxSetResourceString(@sdxCarManufacturerCountry3,'Alemanha');
  cxSetResourceString(@sdxCarManufacturerCountry4,'Reino Unido');
  cxSetResourceString(@sdxCarManufacturerCountry5,'Alemanha');
  cxSetResourceString(@sdxCarModelColumnCaption,'Modelo');
  cxSetResourceString(@sdxCarName,'Nome de carro');
  cxSetResourceString(@sdxCarPhotoColumnCaption,'Foto');
  cxSetResourceString(@sdxCarTires,'Pneus');
  cxSetResourceString(@sdxCarTransmission,'transmiss�o');
  cxSetResourceString(@sdxCashCaption,'dinheiro');
  cxSetResourceString(@sdxCategoryStyle,'Categoria');
  cxSetResourceString(@sdxCenterOnPage,'Centro, na p�gina');
  cxSetResourceString(@sdxCharts,'Gr�ficos');
  cxSetResourceString(@sdxCheckAll,'Check &Todos');
  cxSetResourceString(@sdxCheckAllChildren,'Check Todos &Filhos');
  cxSetResourceString(@sdxCheckMarks,'Check Marcas');
  cxSetResourceString(@sdxCheckMarksAsText,'&Display Checkmarks como Texto');
  cxSetResourceString(@sdxCircle,'c�rculo');
  cxSetResourceString(@sdxCloneStyleCaptionPrefix,'Copiar (%d) de ');
  cxSetResourceString(@sdxCloseExplorerHint,'Feche o Explorer');
  cxSetResourceString(@sdxColumnFields,'&Campos de Coluna');
  cxSetResourceString(@sdxColumnHeaders,'&Cabe�alhos de Coluna');
  cxSetResourceString(@sdxColumnHeadersOnEveryPage,'Coluna &Cabe�alhos');
  cxSetResourceString(@sdxCompanyCaption,'Companhia');
  cxSetResourceString(@sdxCompanyName,'Nome de Empresa');
  cxSetResourceString(@sdxComponentAlreadyExists,'Componente denominado "%s" j� existe');
  cxSetResourceString(@sdxComponentNotAssigned,'%s \nN�o designado "Componente de propriedade"');
  cxSetResourceString(@sdxComponentNotSupported,'Componente "%s" n�o � suportado pelo TdxComponentPrinter');
  cxSetResourceString(@sdxComponentNotSupportedByLink,'Componente "%s" n�o � suportado pelo TdxComponentPrinter');
  cxSetResourceString(@sdxComposition,'Composi��o');
  cxSetResourceString(@sdxCompositionDesignerCaption,'Composi��o Editor');
  cxSetResourceString(@sdxConfirmDeleteItem,'Voc� deseja excluir os itens pr�ximos %s ?');
  cxSetResourceString(@sdxConfirmOverWrite,'Arquivo "%s" j� existe. sobrescrever?');
  cxSetResourceString(@sdxConsumeSelectionStyle,'Consumir estilo Sele��o');
  cxSetResourceString(@sdxContentEvenStyle,'Mesmo conte�do Linhas');
  cxSetResourceString(@sdxContentOddStyle,'Conte�do Linhas �mpares');
  cxSetResourceString(@sdxContentStyle,'Conte�do');
  cxSetResourceString(@sdxContinuedMessage,'Continua��o');
  cxSetResourceString(@sdxControls,'&Controles');
  cxSetResourceString(@sdxControlsPlace,'Controles local');
  cxSetResourceString(@sdxControlsTab,'Controles');
  cxSetResourceString(@sdxCopy,'&Copiar');
  cxSetResourceString(@sdxCopyOfItem,'C�pia de');
  cxSetResourceString(@sdxCountCaption,'contar');
  cxSetResourceString(@sdxCountIs,'Contagem �: %d');
  cxSetResourceString(@sdxCreatedBy,'Criado de ');
  cxSetResourceString(@sdxCreatedOn,'Criado Em');
  cxSetResourceString(@sdxCurrentRecord,'Atual \nregistro');
  cxSetResourceString(@sdxCustom,'personalizado');
  cxSetResourceString(@sdxCustomSize,'Tamanho personalizado');
  cxSetResourceString(@sdxCyclicIDReferences,'Refer�ncias Cyclic ID %s e %s');
  cxSetResourceString(@sdxDataFields,'&Campos de Dados');
  cxSetResourceString(@sdxDataLoadErrorText,'N�o � poss�vel carregar dados de relat�rio');
  cxSetResourceString(@sdxDataProviderDontPresent,'N�o h� nenhuma liga��o com Assigned Componente de Composi��o');
  cxSetResourceString(@sdxDataToPrintDoesNotExist,'N�o � poss�vel ativar ReportLink porque PrintingSystem n�o encontrou nada para imprimir.');
  cxSetResourceString(@sdxDay,'Dia');
  cxSetResourceString(@sdxDBBasedExplorerItemDataLoadError,'N�o � poss�vel carregar dados do relat�rio. \nOs dados est�o corrompidos ou s�o bloqueados');
  cxSetResourceString(@sdxDecember,'Dezembro');
  cxSetResourceString(@sdxDecemberShort,'Dezembro');
  cxSetResourceString(@sdxDefaultTray,'Bandeja padr�o');
  cxSetResourceString(@sdxDefinePrintStylesCaption,'Definir estilos de impress�o');
  cxSetResourceString(@sdxDefinePrintStylesMenuItem,'Definir Print &Estilos ...');
  cxSetResourceString(@sdxDefinePrintStylesTitle,'Imprimir&Estilos:');
  cxSetResourceString(@sdxDefinePrintStylesWarningClear,'Voc� quer apagar todos os n�o-constru�da em estilos?');
  cxSetResourceString(@sdxDefinePrintStylesWarningDelete,'Voc� quer apagar "%s" ?');
  cxSetResourceString(@sdxDeleteFolderMessageText,'Excluir pasta "%s" ?');
  cxSetResourceString(@sdxDeleteItemMessageText,'Excluir Item "%s" ?');
  cxSetResourceString(@sdxDeleteNonEmptyFolderMessageText,'Pasta "%s" n�o est� vazia. Delete de qualquer maneira?');
  cxSetResourceString(@sdxDescription,'&Descri��o:');
  cxSetResourceString(@sdxDetails,'&Detalhes');
  cxSetResourceString(@sdxDisplayGraphicsAsText,'O display gr�fico Como &Texto');
  cxSetResourceString(@sdxDoubleLineEdgePattern,'dobro linha');
  cxSetResourceString(@sdxDownThenOver,'&Abaixo, em seguida, sobre');
  cxSetResourceString(@sdxDrawBorder,'&Draw borda');
  cxSetResourceString(@sdxDrawMode,'Draw &Modo:');
  cxSetResourceString(@sdxDrawModeBorrow,'Emprestar da Fonte');
  cxSetResourceString(@sdxDrawModeChess,'Modo de Xadrez');
  cxSetResourceString(@sdxDrawModeOddEven,'Odd/ Mesmo Linhas Modo');
  cxSetResourceString(@sdxDrawModeStrict,'estrito');
  cxSetResourceString(@sdxDTFormatsAutoUpdate,'&Atualizar automaticamente');
  cxSetResourceString(@sdxDTFormatsAvailableDateFormats,'&Livre Formatos de Data:');
  cxSetResourceString(@sdxDTFormatsAvailableTimeFormats,'Dispon�vel&Formato Hora:');
  cxSetResourceString(@sdxDTFormatsCaption,'Data e Hora');
  cxSetResourceString(@sdxDTFormatsChangeDefaultFormat,'Voc� quer mudar a data ea hora padr�o formatos para corresponder "%s"  - "%s" ?');
  cxSetResourceString(@sdxEast,'M�dio');
  cxSetResourceString(@sdxEditDescription,'Editar Descri��o');
  cxSetResourceString(@sdxEditReports,'Editar relat�rios');
  cxSetResourceString(@sdxInternalErrorAutoHide,'Erro interno enquanto os controles autohide.');
  cxSetResourceString(@sdxInternalErrorCreateLayout,'Erro interno ao criar um %s object layout.');
  cxSetResourceString(@sdxInternalErrorDestroyLayout,'Erro interno ao destruir um layout %s objeto.');
  cxSetResourceString(@sdxInternalErrorLayout,'Erro interno no layout %s objeto.');
  cxSetResourceString(@sdxInternalErrorPainter,'Erro interno no pintor TdxCustomDockControl.');
  cxSetResourceString(@sdxInvaldZoneOwner,'Voc� n�o pode criar um TdxZone sem possuir TdxCustomDockControl.');
  cxSetResourceString(@sdxInvalidDockSiteParent,'O pai dos TdxDockSite n�o pode ser TdxCustomDockControl.');
  cxSetResourceString(@sdxInvalidFloatingDeleting,'Voc� n�o pode excluir um TdxCustomDockSite em modo de flutua��o.');
  cxSetResourceString(@sdxInvalidFloatSiteDeleting,'Voc� n�o pode excluir um TdxFloatDockSite.');
  cxSetResourceString(@sdxInvalidFloatSiteParent,'O pai dos TdxFloatDockSite s� pode ser TdxFloatForm.');
  cxSetResourceString(@sdxInvalidLayoutSiteDeleting,'Voc� n�o pode excluir um TdxLayoutDockSite.');
  cxSetResourceString(@sdxInvalidOwner,'O Dono do TdxCustomDockControl deve ser TCustomForm.');
  cxSetResourceString(@sdxInvalidPanelChild,'Voc� n�o pode inserir um TdxCustomDockControl em TdxDockPanel (%s est� sendo inserido).');
  cxSetResourceString(@sdxInvalidParent,'O pai do% s deve ser TdxCustomDockControl.');
  cxSetResourceString(@sdxInvalidParentAssigning,'Voc� n�o pode definir o pai para este componente.');
  cxSetResourceString(@sdxInvalidSiteChild,'Voc� s� pode inserir um TdxCustomDockControl em TdxCustomDockSite (%s est� sendo inserido).');
  cxSetResourceString(@sdxManagerError,'Voc� n�o pode ter mais de uma inst�ncia TdxDockingManager em um formul�rio.');
  cxSetResourceString(@secxAllDay,'Todos os Dias');
  cxSetResourceString(@secxAlldayevent,'Todo Evento do Dia \n');
  cxSetResourceString(@secxCategories,'Categoria');
  cxSetResourceString(@secxDescription,'Descri��o');
  cxSetResourceString(@secxEndDate,'DataFim');
  cxSetResourceString(@secxEndTime,'HoraFim');
  cxSetResourceString(@secxExportStorageInvalid,'Armazenamento n�o atribu�dos');
  cxSetResourceString(@secxFalse,'FALSO');
  cxSetResourceString(@secxFinish,'Concluir');
  cxSetResourceString(@secxLocation,'Local');
  cxSetResourceString(@secxNo,'N�o');
  cxSetResourceString(@secxReminder,'');
  cxSetResourceString(@secxReminderDate,'LembreteData');
  cxSetResourceString(@secxReminderonoff,'Lembreteonoff');
  cxSetResourceString(@secxReminderTime,'LembreteTempo');
  cxSetResourceString(@secxSetDateRangeAnd,'E');
  cxSetResourceString(@secxSetDateRangeCaption,'Definir Per�odo');
  cxSetResourceString(@secxSetDateRangeText,'Exporta��o e criar ocorr�ncias individuais de compromissos ou tarefas que ocorrem entre:');
  cxSetResourceString(@secxShowtimeas,'Mostrarhorario');
  cxSetResourceString(@secxStart,'Inicio');
  cxSetResourceString(@secxStartDate,'DataInicio');
  cxSetResourceString(@secxStartTime,'Tempo do Inicio');
  cxSetResourceString(@secxState,'Status');
  cxSetResourceString(@secxSubject,'Assunto');
  cxSetResourceString(@secxTrue,'Verdadeiro');
  cxSetResourceString(@secxYes,'Sim');


  cxSetResourceString(@sdxBtnOK, 'OK');
  cxSetResourceString(@sdxBtnOKAccelerated, '&OK');
  cxSetResourceString(@sdxBtnCancel, 'Cancelar');
  cxSetResourceString(@sdxBtnClose, 'Fechar');
  cxSetResourceString(@sdxBtnApply, 'Aplicar');
  cxSetResourceString(@sdxBtnHelp, '&Help');
  cxSetResourceString(@sdxBtnFix, '&Fix');
  cxSetResourceString(@sdxBtnNew, '&New ...');
  cxSetResourceString(@sdxBtnIgnore, '&Ignorar');
  cxSetResourceString(@sdxBtnYes, '&sim');
  cxSetResourceString(@sdxBtnNo, '&N�o');
  cxSetResourceString(@sdxBtnEdit, '&Editar ...');
  cxSetResourceString(@sdxBtnReset, '&Reset ');
  cxSetResourceString(@sdxBtnAdd, '&Adicionar');
  cxSetResourceString(@sdxBtnAddComposition, 'Adicionar e Composi��o');
  cxSetResourceString(@sdxBtnDefault, '&padr�o ...');
  cxSetResourceString(@sdxBtnDelete, '&Excluir ... ');
  cxSetResourceString(@sdxBtnDescription, '&Descri��o ...');
  cxSetResourceString(@sdxBtnCopy, '&Copy ...');
  cxSetResourceString(@sdxBtnYesToAll, 'Yes To &All');
  cxSetResourceString(@sdxBtnFootnoteProperties, 'Nota de rodap� Propriedades ...');
  cxSetResourceString(@sdxBtnRestoreDefaults, '&Restore Defaults');
  cxSetResourceString(@sdxBtnRestoreOriginal, 'Restaurar &Original');
  cxSetResourceString(@sdxBtnTitleProperties, 'T�tulo Properties ... ');
  cxSetResourceString(@sdxBtnProperties, 'Propriedade');
  cxSetResourceString(@sdxBtnNetwork, 'Rede...');
  cxSetResourceString(@sdxBtnBrowse, '&Procurar ...');
  cxSetResourceString(@sdxBtnPageSetup, 'Pa&ge Configurar ... ');
  cxSetResourceString(@sdxBtnPrintPreview, 'Imprimir Pre &visualizar...');
  cxSetResourceString(@sdxBtnPreview, 'Pr� &visualizar...');
  cxSetResourceString(@sdxBtnPrint, 'Imprimir ...');
  cxSetResourceString(@sdxBtnOptions, '&Op��es ...');
  cxSetResourceString(@sdxBtnStyleOptions, 'Op��es de estilo ... ');
  cxSetResourceString(@sdxBtnDefinePrintStyles, '&Definir estilos ...');
  cxSetResourceString(@sdxBtnPrintStyles, 'Imprimir Estilos');
  cxSetResourceString(@sdxBtnBackground, 'fundo');
  cxSetResourceString(@sdxBtnShowToolBar, 'Show &ToolBar');
  cxSetResourceString(@sdxBtnDesign, 'D&esign ...');
  cxSetResourceString(@sdxBtnMoveUp, 'Move &Up');
  cxSetResourceString(@sdxBtnMoveDown, 'Mova Dow &n');

  cxSetResourceString(@sdxBtnMoreColors, '&mais cores ...');
  cxSetResourceString(@sdxBtnFillEffects, '&Efeitos de preenchimento ...');
  cxSetResourceString(@sdxBtnNoFill, '&Sem preenchimento');
  cxSetResourceString(@sdxBtnAutomatic, '&Automatic');
  cxSetResourceString(@sdxBtnNone, '&Nada');

  cxSetResourceString(@sdxBtnOtherTexture, 'Outro Te &xture ...');
  cxSetResourceString(@sdxBtnInvertColors, 'I &nvert Colors');
  cxSetResourceString(@sdxBtnSelectPicture, 'Se &lect Imagem ... ');

  cxSetResourceString(@sdxEditReports, 'Editar Reports');
  cxSetResourceString(@sdxComposition, 'Composi��o');
  cxSetResourceString(@sdxReportFootnotesDlgCaption, 'Relat�rio de Notas de rodap� ');
  cxSetResourceString(@sdxReportTitleDlgCaption, 'T�tulo do Relat�rio');
  cxSetResourceString(@sdxMode, '&Mode:');
  cxSetResourceString(@sdxText, '&texto');
  cxSetResourceString(@sdxProperties, '&Propriedades ');
  cxSetResourceString(@sdxAdjustOnScale, '&Ajuste na escala');

  // Modo T�tulo do Relat�rio
  cxSetResourceString(@sdxTitleModeNone, 'Nenhum');
  cxSetResourceString(@sdxTitleModeOnEveryTopPage, 'On Every Top Page ');
  cxSetResourceString(@sdxTitleModeOnFirstPage, 'na primeira p�gina');

  // Modo Relat�rio Footnotes
  cxSetResourceString(@sdxFootnotesModeNone, 'Nenhum');
  cxSetResourceString(@sdxFootnotesModeOnEveryBottomPage, 'On Every inferior Page ');
  cxSetResourceString(@sdxFootnotesModeOnLastPage, 'na �ltima p�gina');

  cxSetResourceString(@sdxEditDescription, 'Editar Descri��o ');
  cxSetResourceString(@sdxRename, 'Rena &me');
  cxSetResourceString(@sdxSelectAll, '&Selecionar tudo');

  cxSetResourceString(@sdxAddReport, 'Adicionar Report');
  cxSetResourceString(@sdxAddAndDesignReport, 'Adicionar e D &relat�rio esign ...');
  cxSetResourceString(@sdxNewCompositionCaption, 'New Composi��o');
  cxSetResourceString(@sdxName, '&Nome: ');
  cxSetResourceString(@sdxCaption, '&Legenda:');
  cxSetResourceString(@sdxAvailableSources, '&dispon�vel Fonte (s)');
  cxSetResourceString(@sdxOnlyComponentsInActiveForm, 'S� no Active Components &Form');
  cxSetResourceString(@sdxOnlyComponentsWithoutLinks, 'Apenas Componentes e sem ReportLinks existentes');
  cxSetResourceString(@sdxItemName, 'Name');
  cxSetResourceString(@sdxItemDescription, 'Descri��o');

  cxSetResourceString(@sdxConfirmDeleteItem, 'Voc� quer excluir itens seguintes:% s?');
  cxSetResourceString(@sdxAddItemsToComposition, 'Adicionar itens ao Composi��o');
  cxSetResourceString(@sdxHideAlreadyIncludedItems, 'Ocultar J� &Itens inclu�dos');
  cxSetResourceString(@sdxAvailableItems, 'A &Itens vailable');
  cxSetResourceString(@sdxItems, '&Itens');
  cxSetResourceString(@sdxEnable, '&Ativar ');
  cxSetResourceString(@sdxOptions, 'Op��es');
  cxSetResourceString(@sdxShow, 'Show');
  cxSetResourceString(@sdxPaintItemsGraphics, '&gr�ficos de pintura item');
  cxSetResourceString(@sdxDescription, '&Descri��o:');

  cxSetResourceString(@sdxNewReport, 'NewReport');

  cxSetResourceString(@sdxOnlySelected, 'S� &selecionado');
  cxSetResourceString(@sdxExtendedSelect, '&Select Extens�o');
  cxSetResourceString(@sdxIncludeFixed, '&Incluir Fixo');

  cxSetResourceString(@sdxFonts, 'Fontes');
  cxSetResourceString(@sdxBtnFont, '&nt Fo ...');
  cxSetResourceString(@sdxBtnEvenFont, 'E &Font ven ...');
  cxSetResourceString(@sdxBtnOddFont, 'Odd Fo &nt ...');
  cxSetResourceString(@sdxBtnFixedFont, 'F &ixed Font ...');
  cxSetResourceString(@sdxBtnGroupFont, 'Grou &p Font ...');
  cxSetResourceString(@sdxBtnChangeFont, 'Mudar Fo &nt ...');

  cxSetResourceString(@sdxFont, 'Font');
  cxSetResourceString(@sdxOddFont, 'Odd Font');
  cxSetResourceString(@sdxEvenFont, 'Mesmo Font');
  cxSetResourceString(@sdxPreviewFont, 'Preview Font');
  cxSetResourceString(@sdxCaptionNodeFont, 'N�vel Legenda Font');
  cxSetResourceString(@sdxGroupNodeFont, 'Grupo N� Font');
  cxSetResourceString(@sdxGroupFooterFont, 'Rodap� de Grupo Font ');
  cxSetResourceString(@sdxHeaderFont, 'Header Font');
  cxSetResourceString(@sdxFooterFont, 'Rodap� Font');
  cxSetResourceString(@sdxBandFont, 'Banda Font');

  cxSetResourceString(@sdxTransparent, '&Transparente');
  cxSetResourceString(@sdxFixedTransparent, 'Fi e fixa Transparente');
  cxSetResourceString(@sdxCaptionTransparent, 'Caption Transparente ');
  cxSetResourceString(@sdxGroupTransparent, 'Grupo Transparente');

  cxSetResourceString(@sdxGraphicAsTextValue, '(gr�fico)');
  cxSetResourceString(@sdxColors, 'Colors');
  cxSetResourceString(@sdxColor, 'Co &lor:');
  cxSetResourceString(@sdxOddColor, 'Odd &Co lor:');
  cxSetResourceString(@sdxEvenColor, 'E &ven Cor:');
  cxSetResourceString(@sdxPreviewColor, '&Pr�via Cor:');
  cxSetResourceString(@sdxBandColor, '&Banda Cor:');
  cxSetResourceString(@sdxLevelCaptionColor, 'Le vel &Legenda Color:');
  cxSetResourceString(@sdxHeaderColor, 'H &eader Cor:');
  cxSetResourceString(@sdxGroupNodeColor, 'Grupo N� &Cor:');
  cxSetResourceString(@sdxGroupFooterColor, '&rodap� grupo de cores:');
  cxSetResourceString(@sdxFooterColor, 'Foo &ter cor:');
  cxSetResourceString(@sdxFixedColor, 'F &ixed Cor:');
  cxSetResourceString(@sdxGroupColor, 'Grou &p Cor:');
  cxSetResourceString(@sdxCaptionColor, 'Caption Cor: ');
  cxSetResourceString(@sdxGridLinesColor, 'Gri &d Color Line:');

  cxSetResourceString(@sdxBands, '&Bands');
  cxSetResourceString(@sdxLevelCaptions, 'N�veis &rubrica');
  cxSetResourceString(@sdxHeaders, 'H &eaders');
  cxSetResourceString(@sdxFooters, 'Foote &rs');
  cxSetResourceString(@sdxGroupFooters, '&Grupo rodap�s ');
  cxSetResourceString(@sdxPreview, 'Previe &w');
  cxSetResourceString(@sdxPreviewLineCount, 'Preview Linha Coun &t:');
  cxSetResourceString(@sdxAutoCalcPreviewLineCount, 'A &uto Calcular pr�via Lines');

  cxSetResourceString(@sdxGrid, 'Grid Lines');
  cxSetResourceString(@sdxNodesGrid, 'N� Grelha');
  cxSetResourceString(@sdxGroupFooterGrid, 'GroupFooter Grelha');

  cxSetResourceString(@sdxStateImages, '&Imagens estatais �');
  cxSetResourceString(@sdxImages, '&Imagens ');

  cxSetResourceString(@sdxTextAlign, 'Texto &Alinhar');
  cxSetResourceString(@sdxTextAlignHorz, 'Hori &zontal');
  cxSetResourceString(@sdxTextAlignVert, '&verticalmente');
  cxSetResourceString(@sdxTextAlignLeft, 'Esquerda');
  cxSetResourceString(@sdxTextAlignCenter, 'Center');
  cxSetResourceString(@sdxTextAlignRight, 'direito');
  cxSetResourceString(@sdxTextAlignTop, 'Top');
  cxSetResourceString(@sdxTextAlignVCenter, 'Center');
  cxSetResourceString(@sdxTextAlignBottom, 'inferior');
  cxSetResourceString(@sdxBorderLines, '&Border');
  cxSetResourceString(@sdxHorzLines, 'Hori &linhas hori-');
  cxSetResourceString(@sdxVertLines, '&linhas verticais');
  cxSetResourceString(@sdxFixedHorzLines, 'Fi e linhas horizontais xos');
  cxSetResourceString(@sdxFixedVertLines, 'Fixe &d linhas verticais');
  cxSetResourceString(@sdxFlatCheckMarks, 'F &Checkmarks lat');
  cxSetResourceString(@sdxCheckMarksAsText, '&Exibi��o marcas de sele��o como texto');

  cxSetResourceString(@sdxRowAutoHeight, 'Ro &autoHeight w');
  cxSetResourceString(@sdxEndEllipsis, '&EndEllipsis');

  cxSetResourceString(@sdxDrawBorder, '&Desenhe Border');
  cxSetResourceString(@sdxFullExpand, 'Full &Expandir');
  cxSetResourceString(@sdxBorderColor, '&Cor da margem:');
  cxSetResourceString(@sdxAutoNodesExpand, 'A &uto Nodes Expandir');
  cxSetResourceString(@sdxExpandLevel, 'Expandir e N�vel:');
  cxSetResourceString(@sdxFixedRowOnEveryPage, 'linhas fixas ');

  cxSetResourceString(@sdxDrawMode, 'Empate &Mode:');
  cxSetResourceString(@sdxDrawModeStrict, 'Strict');
  cxSetResourceString(@sdxDrawModeOddEven, '�mpar / Par Linhas Mode');
  cxSetResourceString(@sdxDrawModeChess, 'Modo de xadrez');
  cxSetResourceString(@sdxDrawModeBorrow, 'Borrow De Fonte');

  cxSetResourceString(@sdx3DEffects, '3D Efeitos ');
  cxSetResourceString(@sdxUse3DEffects, 'Uso e 3D Efeitos');
  cxSetResourceString(@sdxSoft3D, 'Sof &T3D');

  cxSetResourceString(@sdxBehaviors, 'Comportamentos');
  cxSetResourceString(@sdxMiscellaneous, 'Diversos');
  cxSetResourceString(@sdxOnEveryPage, 'em cada p�gina');
  cxSetResourceString(@sdxNodeExpanding, 'N� Expans�o');
  cxSetResourceString(@sdxSelection, 'Sele��o');
  cxSetResourceString(@sdxNodeAutoHeight, '&N� Auto Altura');
  cxSetResourceString(@sdxTransparentGraphics, '&gr�ficos transparentes �');
  cxSetResourceString(@sdxAutoWidth, 'Auto &Largura');

  cxSetResourceString(@sdxDisplayGraphicsAsText, 'display gr�fico Como &texto');
  cxSetResourceString(@sdxTransparentColumnGraphics, 'Transparente &Graphics');

  cxSetResourceString(@sdxBandsOnEveryPage, 'Bands');
  cxSetResourceString(@sdxHeadersOnEveryPage, 'cabe�alhos');
  cxSetResourceString(@sdxFootersOnEveryPage, 'rodap�s');
  cxSetResourceString(@sdxGraphics, '&Graphics');

  cxSetResourceString(@sdxMenuFileSave ,  '&Salvar');
  cxSetResourceString(@sdxMenuFileSaveAs , 'Salvar &Como...');
  cxSetResourceString(@sdxMenuFileLoad , '&Carregar');
  cxSetResourceString(@sdxMenuFileClose , 'Descarregar');
  cxSetResourceString(@sdxHintFileSave , 'Salvar Relat�rio');
  cxSetResourceString(@sdxHintFileSaveAs , 'Salvar Relat�rio Como');
  cxSetResourceString(@sdxHintFileLoad , 'Carregar Relat�rio');
  cxSetResourceString(@sdxHintFileClose , 'Descarregar Relat�rio');

  cxSetResourceString(@sdxRibbonPrintPreviewClosePrintPreview, 'Sair');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupFormat , 'Formatar');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupInsertName , 'Nome');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupInsertPageNumber , 'N�mero de P�ginas');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupNavigation , 'Navega��o');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupOutput , 'Sa�da');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupParts , 'Partes');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupReport , 'Relat�rio');
  cxSetResourceString(@sdxRibbonPrintPreviewGroupZoom , 'Zoom');
  cxSetResourceString(@sdxRibbonPrintPreviewPagesSubItem , 'P�ginas');

  {mensagens comuns}

  cxSetResourceString(@sdxOutOfResources, 'Sem Recursos');
  cxSetResourceString(@sdxFileAlreadyExists, 'O arquivo "% s" j� existe.');
  cxSetResourceString(@sdxConfirmOverWrite, 'O arquivo "% s" j� existe. Substituir? ');
  cxSetResourceString(@sdxInvalidFileName, 'inv�lido nome do arquivo "% s"');
  cxSetResourceString(@sdxRequiredFileName, 'Digite o nome do arquivo. ');
  cxSetResourceString(@sdxOutsideMarginsMessage,
    'Um ou mais margens est�o definidas fora da �rea de impress�o da p�gina.' + #13#10 +
    'Voc� quer continuar?');
  cxSetResourceString(@sdxOutsideMarginsMessage2,
    'Um ou mais margens est�o definidas fora da �rea de impress�o da p�gina.' + #13#10 +
    'Escolha o bot�o corre��o para aumentar as margens adequadas.�');
  cxSetResourceString(@sdxInvalidMarginsMessage,
    'Um ou mais margens est�o definidas para os valores inv�lidos.' + #13#10 +
    'Escolha o bot�o Fix para corrigir esse problema.' + #13#10 +
    'Escolha o bot�o Restaurar para restaurar os valores originais. ');
  cxSetResourceString(@sdxInvalidMargins, 'Um ou mais margens t�m valores inv�lidos ');
  cxSetResourceString(@sdxOutsideMargins, 'Um ou mais margens est�o definidas fora da �rea de impress�o da p�gina');
  cxSetResourceString(@sdxThereAreNowItemsForShow, 'N�o h� itens nesta vista ');

  {Paleta de cores}

  cxSetResourceString(@sdxPageBackground, 'Fundo de p�gina');
  cxSetResourceString(@sdxPenColor, 'Pen Color');
  cxSetResourceString(@sdxFontColor, 'Cor da fonte ');
  cxSetResourceString(@sdxBrushColor, 'Pincel Color');
  cxSetResourceString(@sdxHighLight, 'destaque');

  {nomes Cor}

  cxSetResourceString(@sdxColorBlack, 'Black');
  cxSetResourceString(@sdxColorDarkRed, 'Dark Red');
  cxSetResourceString(@sdxColorRed, 'Red');
  cxSetResourceString(@sdxColorPink, 'Pink');
  cxSetResourceString(@sdxColorRose, 'Rose');
  cxSetResourceString(@sdxColorBrown, 'Brown');
  cxSetResourceString(@sdxColorOrange, 'laranja');
  cxSetResourceString(@sdxColorLightOrange, 'Light Orange');
  cxSetResourceString(@sdxColorGold, 'Gold');
  cxSetResourceString(@sdxColorTan, 'Tan');
  cxSetResourceString(@sdxColorOliveGreen, 'Olive Verde ');
  cxSetResourceString(@sdxColorDrakYellow, 'Dark Yellow');
  cxSetResourceString(@sdxColorLime, 'Lime');
  cxSetResourceString(@sdxColorYellow, 'Yellow');
  cxSetResourceString(@sdxColorLightYellow, 'luz amarela');
  cxSetResourceString(@sdxColorDarkGreen, 'Dark Green');
  cxSetResourceString(@sdxColorGreen, 'verde');
  cxSetResourceString(@sdxColorSeaGreen, 'mar verde');
  cxSetResourceString(@sdxColorBrighthGreen, 'Bright Green');
  cxSetResourceString(@sdxColorLightGreen, 'luz verde');
  cxSetResourceString(@sdxColorDarkTeal, 'Dark Teal');
  cxSetResourceString(@sdxColorTeal, 'Teal');
  cxSetResourceString(@sdxColorAqua, 'Aqua');
  cxSetResourceString(@sdxColorTurquoise, 'turquesa');
  cxSetResourceString(@sdxColorLightTurquoise, 'Light Turquoise');
  cxSetResourceString(@sdxColorDarkBlue, 'Dark Blue');
  cxSetResourceString(@sdxColorBlue, 'Blue');
  cxSetResourceString(@sdxColorLightBlue, 'Light Blue ');
  cxSetResourceString(@sdxColorSkyBlue, 'Blue Sky');
  cxSetResourceString(@sdxColorPaleBlue, 'azul p�lido');
  cxSetResourceString(@sdxColorIndigo, 'Indigo');
  cxSetResourceString(@sdxColorBlueGray, 'Blue Gray');
  cxSetResourceString(@sdxColorViolet, 'Violet');
  cxSetResourceString(@sdxColorPlum, 'Plum');
  cxSetResourceString(@sdxColorLavender, 'Lavender');
  cxSetResourceString(@sdxColorGray80, 'Gray-80% ');
  cxSetResourceString(@sdxColorGray50, 'Gray-50% ');
  cxSetResourceString(@sdxColorGray40, 'Gray-40% ');
  cxSetResourceString(@sdxColorGray25, 'Gray-25% ');
  cxSetResourceString(@sdxColorWhite, 'White');

  {FEF Dialog}

  cxSetResourceString(@sdxTexture, '&Textura');
  cxSetResourceString(@sdxPattern, '&Pattern');
  cxSetResourceString(@sdxPicture, 'P &icture');
  cxSetResourceString(@sdxForeground, '&Primeiro Plano ');
  cxSetResourceString(@sdxBackground, '&fundo');
  cxSetResourceString(@sdxSample, 'Sample:');

  cxSetResourceString(@sdxFEFCaption, 'Efeitos de Preenchimento');
  cxSetResourceString(@sdxPaintMode, 'Paint &Mode');
  cxSetResourceString(@sdxPaintModeCenter, 'Center');
  cxSetResourceString(@sdxPaintModeStretch, 'esticado');
  cxSetResourceString(@sdxPaintModeTile, 'Tile');
  cxSetResourceString(@sdxPaintModeProportional, 'Proporcional');

  {nomes Pattern}

  cxSetResourceString(@sdxPatternGray5, '5%');
  cxSetResourceString(@sdxPatternGray10, '10% ');
  cxSetResourceString(@sdxPatternGray20, '20% ');
  cxSetResourceString(@sdxPatternGray25, '25% ');
  cxSetResourceString(@sdxPatternGray30, '30% ');
  cxSetResourceString(@sdxPatternGray40, '40% ');
  cxSetResourceString(@sdxPatternGray50, '50% ');
  cxSetResourceString(@sdxPatternGray60, '60% ');
  cxSetResourceString(@sdxPatternGray70, '70% ');
  cxSetResourceString(@sdxPatternGray75, '75% ');
  cxSetResourceString(@sdxPatternGray80, '80% ');
  cxSetResourceString(@sdxPatternGray90, '90% ');
  cxSetResourceString(@sdxPatternLightDownwardDiagonal, 'Luz descendente diagonal');
  cxSetResourceString(@sdxPatternLightUpwardDiagonal, 'Luz para cima diagonal');
  cxSetResourceString(@sdxPatternDarkDownwardDiagonal, 'escuro diagonal para baixo ');
  cxSetResourceString(@sdxPatternDarkUpwardDiagonal, 'Dark ascendente diagonal');
  cxSetResourceString(@sdxPatternWideDownwardDiagonal, 'Wide descendente diagonal');
  cxSetResourceString(@sdxPatternWideUpwardDiagonal, 'Wide para cima diagonal');
  cxSetResourceString(@sdxPatternLightVertical, 'Luz vertical ');
  cxSetResourceString(@sdxPatternLightHorizontal, 'Luz horizontal');
  cxSetResourceString(@sdxPatternNarrowVertical, 'Limite vertical ');
  cxSetResourceString(@sdxPatternNarrowHorizontal, 'Narrow horizontal');
  cxSetResourceString(@sdxPatternDarkVertical, 'Dark vertical ');
  cxSetResourceString(@sdxPatternDarkHorizontal, 'Dark horizontal');
  cxSetResourceString(@sdxPatternDashedDownward, 'tracejada para baixo ');
  cxSetResourceString(@sdxPatternDashedUpward, 'tracejada para cima');
  cxSetResourceString(@sdxPatternDashedVertical, 'tracejada vertical ');
  cxSetResourceString(@sdxPatternDashedHorizontal, 'tracejada horizontal');
  cxSetResourceString(@sdxPatternSmallConfetti, 'Small confetti');
  cxSetResourceString(@sdxPatternLargeConfetti, 'Grandes confetti');
  cxSetResourceString(@sdxPatternZigZag, 'Zig zag');
  cxSetResourceString(@sdxPatternWave, 'Wave');
  cxSetResourceString(@sdxPatternDiagonalBrick, 'tijolo Diagonal ');
  cxSetResourceString(@sdxPatternHorizantalBrick, 'tijolo Horizontal ');
  cxSetResourceString(@sdxPatternWeave, 'Weave');
  cxSetResourceString(@sdxPatternPlaid, 'manta');
  cxSetResourceString(@sdxPatternDivot, 'guardi�o');
  cxSetResourceString(@sdxPatternDottedGrid, 'Dottedgrid');
  cxSetResourceString(@sdxPatternDottedDiamond, 'diamante pontilhada');
  cxSetResourceString(@sdxPatternShingle, 'Shingle');
  cxSetResourceString(@sdxPatternTrellis, 'Trellis');
  cxSetResourceString(@sdxPatternSphere, 'Sphere');
  cxSetResourceString(@sdxPatternSmallGrid, 'Small grade');
  cxSetResourceString(@sdxPatternLargeGrid, 'grid Large');
  cxSetResourceString(@sdxPatternSmallCheckedBoard, 'Small verificado board');
  cxSetResourceString(@sdxPatternLargeCheckedBoard, 'Large bordo checada');
  cxSetResourceString(@sdxPatternOutlinedDiamond, 'diamante esbo�ado');
  cxSetResourceString(@sdxPatternSolidDiamond, 'diamante s�lido ');

  {nomes textura}

  cxSetResourceString(@sdxTextureNewSprint, 'Newsprint');
  cxSetResourceString(@sdxTextureGreenMarble, 'verde m�rmore');
  cxSetResourceString(@sdxTextureBlueTissuePaper, 'papel de seda azul');
  cxSetResourceString(@sdxTexturePapyrus, 'Papyrus');
  cxSetResourceString(@sdxTextureWaterDroplets, 'As gotas de �gua ');
  cxSetResourceString(@sdxTextureCork, 'Cork');
  cxSetResourceString(@sdxTextureRecycledPaper, 'Papel reciclado');
  cxSetResourceString(@sdxTextureWhiteMarble, 'm�rmore branco');
  cxSetResourceString(@sdxTexturePinkMarble, 'm�rmore rosa');
  cxSetResourceString(@sdxTextureCanvas, 'Canvas');
  cxSetResourceString(@sdxTexturePaperBag, 'Saco de papel ');
  cxSetResourceString(@sdxTextureWalnut, 'Walnut');
  cxSetResourceString(@sdxTextureParchment, 'Pergaminho');
  cxSetResourceString(@sdxTextureBrownMarble, 'm�rmore Brown');
  cxSetResourceString(@sdxTexturePurpleMesh, 'malha roxa ');
  cxSetResourceString(@sdxTextureDenim, 'Denim');
  cxSetResourceString(@sdxTextureFishFossil, 'f�ssil Fish ');
  cxSetResourceString(@sdxTextureOak, 'Oak');
  cxSetResourceString(@sdxTextureStationary, 'estacion�ria');
  cxSetResourceString(@sdxTextureGranite, 'Granite');
  cxSetResourceString(@sdxTextureBouquet, 'Bouquet');
  cxSetResourceString(@sdxTextureWonenMat, 'Tapete de');
  cxSetResourceString(@sdxTextureSand, 'areia');
  cxSetResourceString(@sdxTextureMediumWood, 'madeira Medium');

  cxSetResourceString(@sdxFSPCaption, 'Visualiza��o da Imagem ');
  cxSetResourceString(@sdxWidth, 'Largura');
  cxSetResourceString(@sdxHeight, 'Altura');

  {Escova Dialog}

  cxSetResourceString(@sdxBrushDlgCaption, 'propriedades Pincel');
  cxSetResourceString(@sdxStyle, '&Estilo:');

  {Enter di�logo Nome do Arquivo Novo}

  cxSetResourceString(@sdxENFNCaption, 'Escolha um novo nome para o arquivo ');
  cxSetResourceString(@sdxEnterNewFileName, 'Digite o novo nome de arquivo');

  {Definir di�logo estilos}

  cxSetResourceString(@sdxDefinePrintStylesCaption, 'Definir estilos de impress�o');
  cxSetResourceString(@sdxDefinePrintStylesTitle, 'Print &Estilos:');
  cxSetResourceString(@sdxDefinePrintStylesWarningDelete, 'Voc� quer excluir "% s"?');
  cxSetResourceString(@sdxDefinePrintStylesWarningClear, 'Voc� quer apagar tudo n�o estilos internos? ');
  cxSetResourceString(@sdxClear, 'C &lear ...');

  {Imprimir dispositivo}

  cxSetResourceString(@sdxCustomSize, 'Tamanho personalizado');
  cxSetResourceString(@sdxDefaultTray, 'Tray Default');
  cxSetResourceString(@sdxInvalidPrintDevice, 'impressora selecionado n�o � v�lido ');
  cxSetResourceString(@sdxNotPrinting, 'A impressora n�o est� sendo impresso no momento');
  cxSetResourceString(@sdxPrinting, 'Printing in progress');
  cxSetResourceString(@sdxDeviceOnPort, '% s em% s');
  cxSetResourceString(@sdxPrinterIndexError, 'index Visualizar Impress�o fora de alcance ');
  cxSetResourceString(@sdxNoDefaultPrintDevice, 'N�o h� nenhuma impressora padr�o selecionada');

  {Editar texto autom�tico entradas Dialog}

  cxSetResourceString(@sdxAutoTextDialogCaption, 'Editar entradas de texto autom�tico ');
  cxSetResourceString(@sdxEnterAutoTextEntriesHere, 'Digite o A &utoText entradas aqui:');

  { De di�logo Imprimir}

  cxSetResourceString(@sdxPrintDialogCaption, 'Imprimir');
  cxSetResourceString(@sdxPrintDialogPrinter, 'Printer');
  cxSetResourceString(@sdxPrintDialogName, '&Nome: ');
  cxSetResourceString(@sdxPrintDialogStatus, 'Status:');
  cxSetResourceString(@sdxPrintDialogType, 'Tipo:');
  cxSetResourceString(@sdxPrintDialogWhere, 'Onde:');
  cxSetResourceString(@sdxPrintDialogComment, 'Coment�rio:');
  cxSetResourceString(@sdxPrintDialogPrintToFile, 'Imprimir para &arquivo');
  cxSetResourceString(@sdxPrintDialogPageRange, 'Intervalo de p�ginas ');
  cxSetResourceString(@sdxPrintDialogAll, '&Todos');
  cxSetResourceString(@sdxPrintDialogCurrentPage, 'Curr ent &p�gina');
  cxSetResourceString(@sdxPrintDialogSelection, '&Sele��o');
  cxSetResourceString(@sdxPrintDialogPages, '&P�ginas:');
  cxSetResourceString(@sdxPrintDialogRangeLegend,
    'Digite o n�mero de p�gina e / ou intervalos de p�ginas' + #13#10 +
    'Separados por v�rgulas. Por exemplo: 1,3,5-12. ');
  cxSetResourceString(@sdxPrintDialogCopies, 'c�pias');
  cxSetResourceString(@sdxPrintDialogNumberOfPages, 'N &umber de p�ginas:');
  cxSetResourceString(@sdxPrintDialogNumberOfCopies, 'N�mero de C�pias &:');
  cxSetResourceString(@sdxPrintDialogCollateCopies, 'Colla e c�pias de te');
  cxSetResourceString(@sdxPrintDialogAllPages, 'Todos');
  cxSetResourceString(@sdxPrintDialogEvenPages, 'Mesmo');
  cxSetResourceString(@sdxPrintDialogOddPages, 'estranho');
  cxSetResourceString(@sdxPrintDialogPrintStyles, 'Imprimir St &yles');

  {PrintToFile Dialog}

  cxSetResourceString(@sdxPrintDialogOpenDlgTitle, 'Escolha Nome do arquivo');
  cxSetResourceString(@sdxPrintDialogOpenDlgAllFiles, 'Todos os arquivos');
  cxSetResourceString(@sdxPrintDialogOpenDlgPrinterFiles, 'impressora Os arquivos');
  cxSetResourceString(@sdxPrintDialogInvalidPageRanges, 'p�gina inv�lida varia');
  cxSetResourceString(@sdxPrintDialogRequiredPageNumbers, 'Digite os n�meros de p�gina');
  cxSetResourceString(@sdxPrintDialogNoPrinters,
    'N�o existem impressoras instaladas. Para instalar uma impressora, '+
    'Aponte para configura��es no menu Iniciar do Windows, clique em Impressoras e, em seguida, clique duas vezes em Adicionar impressora. '+
    'Siga as instru��es do assistente. ');
  cxSetResourceString(@sdxPrintDialogInPrintingState, 'A impressora est� sendo impresso no momento. ' + #13#10 +
    'Por favor, aguarde.');

  {Estado Impressora}

  cxSetResourceString(@sdxPrintDialogPSPaused, 'Pausa');
  cxSetResourceString(@sdxPrintDialogPSPendingDeletion, 'em elimina��o pendente');
  cxSetResourceString(@sdxPrintDialogPSBusy, 'Ocupado');
  cxSetResourceString(@sdxPrintDialogPSDoorOpen, 'porta aberta');
  cxSetResourceString(@sdxPrintDialogPSError, 'erro');
  cxSetResourceString(@sdxPrintDialogPSInitializing, 'Inicializar');
  cxSetResourceString(@sdxPrintDialogPSIOActive, 'IO ativa ');
  cxSetResourceString(@sdxPrintDialogPSManualFeed, 'Manual Feed');
  cxSetResourceString(@sdxPrintDialogPSNoToner, 'Sem Toner');
  cxSetResourceString(@sdxPrintDialogPSNotAvailable, 'N�o Dispon�vel');
  cxSetResourceString(@sdxPrintDialogPSOFFLine, 'offline');
  cxSetResourceString(@sdxPrintDialogPSOutOfMemory, 'Out of Memory');
  cxSetResourceString(@sdxPrintDialogPSOutBinFull, 'Output Bin completa');
  cxSetResourceString(@sdxPrintDialogPSPagePunt, 'P�gina Punt');
  cxSetResourceString(@sdxPrintDialogPSPaperJam, 'Paper Jam');
  cxSetResourceString(@sdxPrintDialogPSPaperOut, 'Sem papel');
  cxSetResourceString(@sdxPrintDialogPSPaperProblem, 'Papel Problema');
  cxSetResourceString(@sdxPrintDialogPSPrinting, 'impress�o');
  cxSetResourceString(@sdxPrintDialogPSProcessing, 'Processamento');
  cxSetResourceString(@sdxPrintDialogPSTonerLow, 'Toner baixo');
  cxSetResourceString(@sdxPrintDialogPSUserIntervention, 'Interven��o do usu�rio ');
  cxSetResourceString(@sdxPrintDialogPSWaiting, 'Waiting');
  cxSetResourceString(@sdxPrintDialogPSWarningUp, 'Aquecendo');
  cxSetResourceString(@sdxPrintDialogPSReady, 'Ready');
  cxSetResourceString(@sdxPrintDialogPSPrintingAndWaiting, 'Impress�o: documento (s)% d espera ');

  cxSetResourceString(@sdxLeftMargin, 'Left Margin');
  cxSetResourceString(@sdxTopMargin, 'Top Margin');
  cxSetResourceString(@sdxRightMargin, 'Margem Direita');
  cxSetResourceString(@sdxBottomMargin, 'Margem inferior ');
  cxSetResourceString(@sdxGutterMargin, 'Gutter');
  cxSetResourceString(@sdxHeaderMargin, 'Header');
  cxSetResourceString(@sdxFooterMargin, 'Rodap�');

  cxSetResourceString(@sdxUnitsInches, 'in');
  cxSetResourceString(@sdxUnitsCentimeters, 'cm');
  cxSetResourceString(@sdxUnitsMillimeters, 'mm');
  cxSetResourceString(@sdxUnitsPoints, 'pt');
  cxSetResourceString(@sdxUnitsPicas, 'pi');

  cxSetResourceString(@sdxUnitsDefaultName, 'Default');
  cxSetResourceString(@sdxUnitsInchesName, 'Inches �');
  cxSetResourceString(@sdxUnitsCentimetersName, 'Cent�metros');
  cxSetResourceString(@sdxUnitsMillimetersName, 'mil�metros');
  cxSetResourceString(@sdxUnitsPointsName, 'Pontos');
  cxSetResourceString(@sdxUnitsPicasName, 'Picas');

  cxSetResourceString(@sdxPrintPreview, 'Visualizar Impress�o');
  cxSetResourceString(@sdxReportDesignerCaption, 'Formato Relat�rio');
  cxSetResourceString(@sdxCompositionDesignerCaption, 'Composi��o do Editor');
  cxSetResourceString(@sdxCompositionStartEachItemFromNewPage, '&Comece cada item da nova p�gina');

  cxSetResourceString(@sdxComponentNotSupportedByLink, 'Componente"% s "n�o � suportado pelo TdxComponentPrinter ');
  cxSetResourceString(@sdxComponentNotSupported, 'Component "% s" n�o � suportado pelo TdxComponentPrinter');
  cxSetResourceString(@sdxPrintDeviceNotReady, 'A impressora n�o foi instalado ou n�o est� pronto ');
  cxSetResourceString(@sdxUnableToGenerateReport, 'N�o foi poss�vel gerar relat�rio ');
  cxSetResourceString(@sdxPreviewNotRegistered, 'N�o existe um formul�rio de pr�-visualiza��o registado ');
  cxSetResourceString(@sdxComponentNotAssigned, '% s' + #13#10 + 'N�o atribu�do "Component" propriedade ');
  cxSetResourceString(@sdxPrintDeviceIsBusy, 'A impressora est� ocupada');
  cxSetResourceString(@sdxPrintDeviceError, 'Impressora encontrou erro!');
  cxSetResourceString(@sdxMissingComponent, 'Missing" Component "propriedade');
  cxSetResourceString(@sdxDataProviderDontPresent, 'N�o h� nenhuma liga��o com o componente atribu�do em Composi��o');
  cxSetResourceString(@sdxPrintingReport, 'relat�rio de impress�o: Conclu�do p�gina (s)% d. Pressione Esc para cancelar '); // Obsoleto
  cxSetResourceString(@sdxDefinePrintStylesMenuItem, 'Definir Print &Estilos ...');
  cxSetResourceString(@sdxAbortPrinting, 'Abortar impress�o?');
  cxSetResourceString(@sdxStandardStyle, 'Estilo Padr�o');

  cxSetResourceString(@sdxFontStyleBold, 'negrito');
  cxSetResourceString(@sdxFontStyleItalic, 'it�lico');
  cxSetResourceString(@sdxFontStyleUnderline, 'Sublinhado');
  cxSetResourceString(@sdxFontStyleStrikeOut, 'riscado');

  cxSetResourceString(@sdxNoPages, 'Ainda n�o existem p�ginas para mostrar');
  cxSetResourceString(@sdxPageWidth, 'Largura da p�gina ');
  cxSetResourceString(@sdxWholePage, 'P�gina Inteira');
  cxSetResourceString(@sdxTwoPages, 'P�ginas Dois');
  cxSetResourceString(@sdxFourPages, 'P�ginas Quatro');
  cxSetResourceString(@sdxWidenToSourceWidth, 'Widen a Fonte Largura');

  cxSetResourceString(@sdxMenuBar, 'BarraDeMenu');
  cxSetResourceString(@sdxStandardBar, 'Standard');
  cxSetResourceString(@sdxHeaderFooterBar, 'Cabe�alho e rodap�');
  cxSetResourceString(@sdxShortcutMenusBar, 'Menus de atalho');
  cxSetResourceString(@sdxAutoTextBar, 'texto autom�tico');

  cxSetResourceString(@sdxMenuFile, '&Arquivo');
  cxSetResourceString(@sdxMenuFileDesign, '&Layout...');
  cxSetResourceString(@sdxMenuFilePrint, '&Imprimir...');
  cxSetResourceString(@sdxMenuFilePrintDialog, 'Di�logo Imprimir ');
  cxSetResourceString(@sdxMenuFilePageSetup, 'Configurar P�gina...');
  cxSetResourceString(@sdxMenuPrintStyles, 'Imprimir Estilos');
  cxSetResourceString(@sdxMenuFileExit, '&Fechar ');
  cxSetResourceString(@sdxMenuExportToPDF, 'Exportar para PDF');

  cxSetResourceString(@sdxMenuEdit, '&Editar');
  cxSetResourceString(@sdxMenuEditCut, 'Recortar');
  cxSetResourceString(@sdxMenuEditCopy, '&Copiar');
  cxSetResourceString(@sdxMenuEditPaste, '&Colar');
  cxSetResourceString(@sdxMenuEditDelete, '&Apagar');
  cxSetResourceString(@sdxMenuEditFind, '&Localizar...');
  cxSetResourceString(@sdxMenuEditFindNext, 'Encontrar Pr�ximo');
  cxSetResourceString(@sdxMenuEditReplace, 'Editar e substituir...');

  cxSetResourceString(@sdxMenuLoad, '&Carregar...');
  cxSetResourceString(@sdxMenuPreview, 'Pr� &vista ...');

  cxSetResourceString(@sdxMenuInsert, '&Inserir ');
  cxSetResourceString(@sdxMenuInsertAutoText, '&texto autom�tico');
  cxSetResourceString(@sdxMenuInsertEditAutoTextEntries, 'AutoTe &xt ...');
  cxSetResourceString(@sdxMenuInsertAutoTextEntries, 'Lista de entradas de texto autom�tico');
  cxSetResourceString(@sdxMenuInsertAutoTextEntriesSubItem, 'In &sert texto autom�tico');
  cxSetResourceString(@sdxMenuInsertPageNumber, '&N�mero de p�gina');
  cxSetResourceString(@sdxMenuInsertTotalPages, '&N�mero de p�ginas');
  cxSetResourceString(@sdxMenuInsertPageOfPages, 'Pa &ge N�mero de p�ginas');
  cxSetResourceString(@sdxMenuInsertDateTime, 'Data e hora');
  cxSetResourceString(@sdxMenuInsertDate, '&Date');
  cxSetResourceString(@sdxMenuInsertTime, '&Time');
  cxSetResourceString(@sdxMenuInsertUserName, '&Nome de Usu�rio ');
  cxSetResourceString(@sdxMenuInsertMachineName, '&Machine Name');

  cxSetResourceString(@sdxMenuView, '&View');
  cxSetResourceString(@sdxMenuViewMargins, '&Margens');
  cxSetResourceString(@sdxMenuViewFlatToolBarButtons, '&bot�es da barra de flat');
  cxSetResourceString(@sdxMenuViewLargeToolBarButtons, '&Grandes barra de ferramentas Bot�es');
  cxSetResourceString(@sdxMenuViewMarginsStatusBar, 'Barra de Margem');
  cxSetResourceString(@sdxMenuViewPagesStatusBar, '&Barra de Status');
  cxSetResourceString(@sdxMenuViewToolBars, '&Barra de ferramentas ');
  cxSetResourceString(@sdxMenuViewPagesHeaders, 'P�gina &cabe�alhos ');
  cxSetResourceString(@sdxMenuViewPagesFooters, 'P�gina Rodap�s');
  cxSetResourceString(@sdxMenuViewSwitchToLeftPart, 'Mudar para a esquerda Parte');
  cxSetResourceString(@sdxMenuViewSwitchToRightPart, 'Mudar para a direita Parte');
  cxSetResourceString(@sdxMenuViewSwitchToCenterPart, 'Mudar para o Centro Parte');
  cxSetResourceString(@sdxMenuViewHFSwitchHeaderFooter, '&Mostrar cabe�alho / rodap�');
  cxSetResourceString(@sdxMenuViewSwitchToFooter, 'Rodap�');
  cxSetResourceString(@sdxMenuViewSwitchToHeader, 'Cabe�alho');
  cxSetResourceString(@sdxMenuViewHFClose, '&Fechar ');

  cxSetResourceString(@sdxMenuZoom, '&Zoom ');
  cxSetResourceString(@sdxMenuZoomPercent100, 'Percentagem &100');
  cxSetResourceString(@sdxMenuZoomPageWidth, 'P�gina &Largura ');
  cxSetResourceString(@sdxMenuZoomWholePage, 'Uma P�gina');
  cxSetResourceString(@sdxMenuZoomTwoPages, '&Duas p�ginas');
  cxSetResourceString(@sdxMenuZoomFourPages, 'P�ginas e quatro');
  cxSetResourceString(@sdxMenuZoomMultiplyPages, '&v�rias p�ginas');
  cxSetResourceString(@sdxMenuZoomWidenToSourceWidth, 'Widen Para S &c�digo fonte Largura');
  cxSetResourceString(@sdxMenuZoomSetup, '&Configurar ...');

  cxSetResourceString(@sdxMenuPages, '&Paginas');

  cxSetResourceString(@sdxMenuGotoPage, '&Ir');
  cxSetResourceString(@sdxMenuGotoPageFirst, '&Primeira P�gina');
  cxSetResourceString(@sdxMenuGotoPagePrev, '&P�gina Anterior ');
  cxSetResourceString(@sdxMenuGotoPageNext, '&P�gina Seguinte');
  cxSetResourceString(@sdxMenuGotoPageLast, '&�ltima P�gina ');
  cxSetResourceString(@sdxMenuActivePage, '&P�gina Ativa: ');

  cxSetResourceString(@sdxMenuFormat, 'F&ormatar');
  cxSetResourceString(@sdxMenuFormatHeaderAndFooter, '&Cabe�alho e rodap� ');
  cxSetResourceString(@sdxMenuFormatAutoTextEntries, '&Auto entradas de texto ...');
  cxSetResourceString(@sdxMenuFormatDateTime, 'Data &Hora ...');
  cxSetResourceString(@sdxMenuFormatPageNumbering, 'P�gina &Numera��o ... ');
  cxSetResourceString(@sdxMenuFormatPageBackground, 'Bac&kground ...');
  cxSetResourceString(@sdxMenuFormatShrinkToPage, '&Ajustar a largura da p�gina');
  cxSetResourceString(@sdxMenuFormatHFBackground, 'cabe�alho / rodap� fundo ... ');
  cxSetResourceString(@sdxMenuFormatHFClear, 'Limpar Texto');

  cxSetResourceString(@sdxMenuTools, '&Ferramentas');
  cxSetResourceString(@sdxMenuToolsCustomize, '&Personalizar ... ');
  cxSetResourceString(@sdxMenuToolsOptions, '&Op��es ...');

  cxSetResourceString(@sdxMenuHelp, '&Help');
  cxSetResourceString(@sdxMenuHelpTopics, 'Ajuda &T�picos ...');
  cxSetResourceString(@sdxMenuHelpAbout, '&About ... ');

  cxSetResourceString(@sdxMenuShortcutPreview, 'Preview');
  cxSetResourceString(@sdxMenuShortcutAutoText, 'texto autom�tico');

  cxSetResourceString(@sdxMenuBuiltInMenus, 'Built-in Menus');
  cxSetResourceString(@sdxMenuShortCutMenus, 'Menus de atalho');
  cxSetResourceString(@sdxMenuNewMenu, 'New Menu');

  { Sugest�es}

  cxSetResourceString(@sdxHintFileDesign, 'Design Report');
  cxSetResourceString(@sdxHintFilePrint, 'Imprimir');
  cxSetResourceString(@sdxHintFilePrintDialog, 'Di�logo Imprimir ');
  cxSetResourceString(@sdxHintFilePageSetup, 'Configurar p�gina');
  cxSetResourceString(@sdxHintFileExit, 'Fechar Preview ');
  cxSetResourceString(@sdxHintExportToPDF, 'Exportar para PDF');

  cxSetResourceString(@sdxHintEditFind, 'Encontrar');
  cxSetResourceString(@sdxHintEditFindNext, 'Localizar pr�xima');
  cxSetResourceString(@sdxHintEditReplace, 'Substituir');

  cxSetResourceString(@sdxHintInsertEditAutoTextEntries, 'Editar texto autom�tico entradas');
  cxSetResourceString(@sdxHintInsertPageNumber, 'Insert n�mero de p�gina');
  cxSetResourceString(@sdxHintInsertTotalPages, 'Insert N�mero de p�ginas');
  cxSetResourceString(@sdxHintInsertPageOfPages, 'Inserir p�gina N�mero de p�ginas');
  cxSetResourceString(@sdxHintInsertDateTime, 'Inserir Data e Hora ');
  cxSetResourceString(@sdxHintInsertDate, 'Insert Date');
  cxSetResourceString(@sdxHintInsertTime, 'Insert Time');
  cxSetResourceString(@sdxHintInsertUserName, 'Inserir Nome de Usu�rio ');
  cxSetResourceString(@sdxHintInsertMachineName, 'Insert Name Machine');

  cxSetResourceString(@sdxHintViewMargins, 'Ver Margens');
  cxSetResourceString(@sdxHintViewLargeButtons, 'Visualiza��o de bot�es grandes �');
  cxSetResourceString(@sdxHintViewMarginsStatusBar, 'Vista Margens Barra de status');
  cxSetResourceString(@sdxHintViewPagesStatusBar, 'Ver p�gina Barra de status');
  cxSetResourceString(@sdxHintViewPagesHeaders, 'Vista do cabe�alho da p�gina');
  cxSetResourceString(@sdxHintViewPagesFooters, 'Ver rodap� da p�gina');
  cxSetResourceString(@sdxHintViewSwitchToLeftPart, 'Mudar para a esquerda cabe�alho / rodap� Parte');
  cxSetResourceString(@sdxHintViewSwitchToRightPart, 'Mudar para o direito cabe�alho / rodap� Parte');
  cxSetResourceString(@sdxHintViewSwitchToCenterPart, 'Mudar para o Centro de cabe�alho / rodap� Parte');
  cxSetResourceString(@sdxHintViewHFSwitchHeaderFooter, 'Alternar entre cabe�alho e rodap�');
  cxSetResourceString(@sdxHintViewSwitchToFooter, 'Mudar para rodap� ');
  cxSetResourceString(@sdxHintViewSwitchToHeader,'Mudar para o cabe�alho');
  cxSetResourceString(@sdxHintViewHFClose, 'Fechar');

  cxSetResourceString(@sdxHintViewZoom, 'Zoom');
  cxSetResourceString(@sdxHintZoomPercent100, 'Zoom 100% ');
  cxSetResourceString(@sdxHintZoomPageWidth, 'Zoom Largura da p�gina');
  cxSetResourceString(@sdxHintZoomWholePage, 'P�gina Inteira');
  cxSetResourceString(@sdxHintZoomTwoPages, 'P�ginas Dois');
  cxSetResourceString(@sdxHintZoomFourPages, 'P�ginas Quatro');
  cxSetResourceString(@sdxHintZoomMultiplyPages, 'V�rias p�ginas');
  cxSetResourceString(@sdxHintZoomWidenToSourceWidth, 'Widen Para Fonte Largura');
  cxSetResourceString(@sdxHintZoomSetup, 'Configura��o Fator Zoom');

  cxSetResourceString(@sdxHintFormatDateTime, 'Formato de Data e Hora ');
  cxSetResourceString(@sdxHintFormatPageNumbering, 'Formato N�mero de p�gina');
  cxSetResourceString(@sdxHintFormatPageBackground, 'fundo');
  cxSetResourceString(@sdxHintFormatShrinkToPage, 'Ajustar � largura da p�gina');
  cxSetResourceString(@sdxHintFormatHFBackground, 'Header Background / rodap� ');
  cxSetResourceString(@sdxHintFormatHFClear, 'Limpar cabe�alho / rodap� de texto');

  cxSetResourceString(@sdxHintGotoPageFirst, 'Primeira P�gina');
  cxSetResourceString(@sdxHintGotoPagePrev, 'P�gina Anterior');
  cxSetResourceString(@sdxHintGotoPageNext, 'P�gina Seguinte');
  cxSetResourceString(@sdxHintGotoPageLast, '�ltima P�gina');
  cxSetResourceString(@sdxHintActivePage, 'P�gina Ativa');

  cxSetResourceString(@sdxHintToolsCustomize, 'Personalizar barras de ferramentas');
  cxSetResourceString(@sdxHintToolsOptions, 'Op��es');

  cxSetResourceString(@sdxHintHelpTopics, 'T�picos de Ajuda ');
  cxSetResourceString(@sdxHintHelpAbout, 'Sobre');

  cxSetResourceString(@sdxPopupMenuLargeButtons, '&bot�es grandes');
  cxSetResourceString(@sdxPopupMenuFlatButtons, '&Flat Buttons');

  cxSetResourceString(@sdxPaperSize, 'Tamanho do papel:');
  cxSetResourceString(@sdxStatus, 'Status:');
  cxSetResourceString(@sdxStatusReady, 'Ready');
  cxSetResourceString(@sdxStatusPrinting, 'Imprimindo... P�gina(s) completaram% d ');
  cxSetResourceString(@sdxStatusGenerateReport, 'Gera��o de Relat�rios. Completado% d %% ');
  cxSetResourceString(@sdxHintDoubleClickForChangePaperSize, 'Double Click for Change Tamanho do papel');
  cxSetResourceString(@sdxHintDoubleClickForChangeMargins, 'Clique duas vezes para alterar as margens');

  {Date &Time Formatos Dialog}

  cxSetResourceString(@sdxDTFormatsCaption, 'Data e hora');
  cxSetResourceString(@sdxDTFormatsAvailableDateFormats, 'Data e formatos dispon�veis:');
  cxSetResourceString(@sdxDTFormatsAvailableTimeFormats, 'dispon�veis e Hora Formatos:');
  cxSetResourceString(@sdxDTFormatsAutoUpdate, '&Atualizar automaticamente');
  cxSetResourceString(@sdxDTFormatsChangeDefaultFormat,
    'Voc� quer alterar a data e hora padr�o formatos para corresponder "% s" - "% s"?');

  {PageNumber Formatos Dialog}

  cxSetResourceString(@sdxPNFormatsCaption, 'N�mero de p�gina Format');
  cxSetResourceString(@sdxPageNumbering, 'Numera��o de p�gina');
  cxSetResourceString(@sdxPNFormatsNumberFormat, 'N�mero e de formato:');
  cxSetResourceString(@sdxPNFormatsContinueFromPrevious, '&Continue da Se��o Anterior');
  cxSetResourceString(@sdxPNFormatsStartAt, 'Start &A:');
  cxSetResourceString(@sdxPNFormatsChangeDefaultFormat,
    'Voc� quer mudar o formato de numera��o de p�gina padr�o para coincidir com"% s "? ');

  {Zoom Dialog}

  cxSetResourceString(@sdxZoomDlgCaption, 'Zoom');
  cxSetResourceString(@sdxZoomDlgZoomTo, 'Zoom To');
  cxSetResourceString(@sdxZoomDlgPageWidth, 'P�gina &Largura ');
  cxSetResourceString(@sdxZoomDlgWholePage, 'W &buraco P�gina');
  cxSetResourceString(@sdxZoomDlgTwoPages, '&Duas p�ginas');
  cxSetResourceString(@sdxZoomDlgFourPages, 'P�ginas e quatro');
  cxSetResourceString(@sdxZoomDlgManyPages, '&muitas p�ginas:');
  cxSetResourceString(@sdxZoomDlgPercent, 'P &ercent:');
  cxSetResourceString(@sdxZoomDlgPreview, 'Preview');
  cxSetResourceString(@sdxZoomDlgFontPreview, 'Times New Roman 12pt');
  cxSetResourceString(@sdxZoomDlgFontPreviewString, 'AaBbCcDdEeXxYyZz');

  {Select p�gina X x Y}

  cxSetResourceString(@sdxPages, 'P�ginas');
  cxSetResourceString(@sdxCancel, 'Cancelar');

  { De di�logo Prefer�ncias }

  cxSetResourceString(@sdxPreferenceDlgCaption, 'Op��es');
  cxSetResourceString(@sdxPreferenceDlgTab1, '&Geral');
  cxSetResourceString(@sdxPreferenceDlgTab2, '');
  cxSetResourceString(@sdxPreferenceDlgTab3, '');
  cxSetResourceString(@sdxPreferenceDlgTab4, '');
  cxSetResourceString(@sdxPreferenceDlgTab5, '');
  cxSetResourceString(@sdxPreferenceDlgTab6, '');
  cxSetResourceString(@sdxPreferenceDlgTab7, '');
  cxSetResourceString(@sdxPreferenceDlgTab8, '');
  cxSetResourceString(@sdxPreferenceDlgTab9, '');
  cxSetResourceString(@sdxPreferenceDlgTab10, '');
  cxSetResourceString(@sdxPreferenceDlgShow, '&Show');
  cxSetResourceString(@sdxPreferenceDlgMargins, '&Margens');
  cxSetResourceString(@sdxPreferenceDlgMarginsHints, 'Margens e Sugest�es ');
  cxSetResourceString(@sdxPreferenceDlgMargingWhileDragging, 'Margens Dicas Enquanto &Arrastando');
  cxSetResourceString(@sdxPreferenceDlgLargeBtns, '&bot�es grandes da barra de ferramentas ');
  cxSetResourceString(@sdxPreferenceDlgFlatBtns, '&bot�es da barra de ferramentas flat');
  cxSetResourceString(@sdxPreferenceDlgMarginsColor, 'Margens &Cor:');
  cxSetResourceString(@sdxPreferenceDlgMeasurementUnits, 'Measurement &Units:');
  cxSetResourceString(@sdxPreferenceDlgSaveForRunTimeToo, 'Salvar para &RunTime demasiado ');
  cxSetResourceString(@sdxPreferenceDlgZoomScroll, '&Zoom ao rolar com o IntelliMouse ');
  cxSetResourceString(@sdxPreferenceDlgZoomStep, 'Zoom Ste &p:');

  {Configura��o De P�gina}

  cxSetResourceString(@sdxCloneStyleCaptionPrefix, 'Copy (% d) de');
  cxSetResourceString(@sdxInvalideStyleCaption, 'O nome do estilo "% s" j� existe. Por favor, forne�a um outro nome. ');
  cxSetResourceString(@sdxHintMoreHFFunctions, 'Mais fun��es');

  cxSetResourceString(@sdxPageSetupCaption, 'Configurar p�gina');
  cxSetResourceString(@sdxStyleName, 'Estilo &Nome:');

  cxSetResourceString(@sdxPage, '&p�gina');
  cxSetResourceString(@sdxMargins, '&Margens');
  cxSetResourceString(@sdxHeaderFooter, '&cabe�alho / rodap� ');
  cxSetResourceString(@sdxScaling, '&Escala');

  cxSetResourceString(@sdxPaper, 'Papel');
  cxSetResourceString(@sdxPaperType, 'T &ipo');
  cxSetResourceString(@sdxPaperDimension, 'dimens�o');
  cxSetResourceString(@sdxPaperWidth, '&Largura:');
  cxSetResourceString(@sdxPaperHeight, 'H &oito:');
  cxSetResourceString(@sdxPaperSource, 'Papel &urce assim:');

  cxSetResourceString(@sdxOrientation, 'Orienta��o');
  cxSetResourceString(@sdxPortrait, 'P &Ortrait');
  cxSetResourceString(@sdxLandscape, '&Paisagismo ');
  cxSetResourceString(@sdxPrintOrder, 'Ordem de impress�o');
  cxSetResourceString(@sdxDownThenOver, '&Down, em seguida, sobre ');
  cxSetResourceString(@sdxOverThenDown, 'O &ver, em seguida, para baixo ');
  cxSetResourceString(@sdxShading, 'sombreamento');
  cxSetResourceString(@sdxPrintUsingGrayShading, 'Imprimir usando &cinza sombreamento ');

  cxSetResourceString(@sdxCenterOnPage, 'Centro na p�gina');
  cxSetResourceString(@sdxHorizontally, 'Hori &zontal');
  cxSetResourceString(@sdxVertically, '&verticalmente');

  cxSetResourceString(@sdxHeader, 'Header');
  cxSetResourceString(@sdxBtnHeaderFont, '&Fonte ...');
  cxSetResourceString(@sdxBtnHeaderBackground, '&fundo');
  cxSetResourceString(@sdxFooter, 'Rodap�');
  cxSetResourceString(@sdxBtnFooterFont, '&Fonte do Rodap�');
  cxSetResourceString(@sdxBtnFooterBackground, 'Fundo do &Rodap�');

  cxSetResourceString(@sdxTop, '&Topo:');
  cxSetResourceString(@sdxLeft, '&Esquerda:');
  cxSetResourceString(@sdxRight, 'Direita:');
  cxSetResourceString(@sdxBottom, '&Inferior:');
  cxSetResourceString(@sdxHeader2, 'Cabe�alho:');
  cxSetResourceString(@sdxFooter2, 'Rodap�:');

  cxSetResourceString(@sdxAlignment, 'Alinhamento');
  cxSetResourceString(@sdxVertAlignment, 'Vertical Alignment ');
  cxSetResourceString(@sdxReverseOnEvenPages, '&Reverso nas p�ginas pares');

  cxSetResourceString(@sdxAdjustTo, '&Ajuste Para:');
  cxSetResourceString(@sdxFitTo, '&Fit To:');
  cxSetResourceString(@sdxPercentOfNormalSize, '% tamanho normal ');
  cxSetResourceString(@sdxPagesWideBy, 'p�gina (s) e de largura por ');
  cxSetResourceString(@sdxTall, '&alto');

  cxSetResourceString(@sdxOf, 'Desligado');
  cxSetResourceString(@sdxLastPrinted, '�ltima Impresso');
  cxSetResourceString(@sdxFileName, 'Nome do arquivo');
  cxSetResourceString(@sdxFileNameAndPath, 'Nome do arquivo e caminho');
  cxSetResourceString(@sdxPrintedBy, 'Impresso por ');
  cxSetResourceString(@sdxPrintedOn, 'impresso em');
  cxSetResourceString(@sdxCreatedBy, 'Criado por');
  cxSetResourceString(@sdxCreatedOn, 'criado em');
  cxSetResourceString(@sdxConfidential, 'Confidencial');

  { Fun��o HF}

  cxSetResourceString(@sdxHFFunctionNameDate, 'Date');
  cxSetResourceString(@sdxHFFunctionNameDateTime, 'Data e hora');
  cxSetResourceString(@sdxHFFunctionNameImage, 'Imagem');
  cxSetResourceString(@sdxHFFunctionNameMachineName, 'Machine Name');
  cxSetResourceString(@sdxHFFunctionNamePageNumber, 'N�mero de p�gina');
  cxSetResourceString(@sdxHFFunctionNamePageOfPages, 'P�gina # de P�ginas #');
  cxSetResourceString(@sdxHFFunctionNameTime, 'Time');
  cxSetResourceString(@sdxHFFunctionNameTotalPages, 'P�ginas total �');
  cxSetResourceString(@sdxHFFunctionNameUnknown, 'Desconhecido');
  cxSetResourceString(@sdxHFFunctionNameUserName, 'Nome de usu�rio');

  cxSetResourceString(@sdxHFFunctionHintDate, 'Data de impress�o');
  cxSetResourceString(@sdxHFFunctionHintDateTime, 'Data e hora de impress�o');
  cxSetResourceString(@sdxHFFunctionHintImage, 'Imagem');
  cxSetResourceString(@sdxHFFunctionHintMachineName, 'Machine Name');
  cxSetResourceString(@sdxHFFunctionHintPageNumber, 'N�mero de p�gina');
  cxSetResourceString(@sdxHFFunctionHintPageOfPages, 'P�gina # de P�ginas #');
  cxSetResourceString(@sdxHFFunctionHintTime, 'Time Impresso');
  cxSetResourceString(@sdxHFFunctionHintTotalPages, 'P�ginas total �');
  cxSetResourceString(@sdxHFFunctionHintUserName, 'Nome de usu�rio');

  cxSetResourceString(@sdxHFFunctionTemplateDate, 'Data de impress�o');
  cxSetResourceString(@sdxHFFunctionTemplateDateTime, 'Date &Time Impresso');
  cxSetResourceString(@sdxHFFunctionTemplateImage, 'Imagem');
  cxSetResourceString(@sdxHFFunctionTemplateMachineName, 'Nome da M�quina');
  cxSetResourceString(@sdxHFFunctionTemplatePageNumber, 'P�gina #');
  cxSetResourceString(@sdxHFFunctionTemplatePageOfPages, 'P�gina # de P�ginas #');
  cxSetResourceString(@sdxHFFunctionTemplateTime, 'Time Impresso');
  cxSetResourceString(@sdxHFFunctionTemplateTotalPages, 'P�ginas total �');
  cxSetResourceString(@sdxHFFunctionTemplateUserName, 'Nome de usu�rio');

  {PDF Export Dialog}

  cxSetResourceString(@sdxPDFDialogAuthor, 'Autor');
  cxSetResourceString(@sdxPDFDialogCaption, 'Op��es de exporta��o de PDF');
  cxSetResourceString(@sdxPDFDialogCompressed, 'Comprimido');
  cxSetResourceString(@sdxPDFDialogCreator, 'Criador');
  cxSetResourceString(@sdxPDFDialogDocumentInfoTabSheet, '&Document Info');
  cxSetResourceString(@sdxPDFDialogEmbedFonts, 'Incorporar fontes');
  cxSetResourceString(@sdxPDFDialogExportSettings, 'Exportar Configura��es ');
  cxSetResourceString(@sdxPDFDialogExportTabSheet, '&Export');
  cxSetResourceString(@sdxPDFDialogKeywords, 'Palavras-chave');
  cxSetResourceString(@sdxPDFDialogMaxCompression, 'Max Compression');
  cxSetResourceString(@sdxPDFDialogMaxQuality, 'Max Qualidade ');
  cxSetResourceString(@sdxPDFDialogOpenAfterExport, 'Open Depois de Exporta��o');
  cxSetResourceString(@sdxPDFDialogPageRageTabSheet, '&Pages');
  cxSetResourceString(@sdxPDFDialogSecurityAllowChanging, 'Permitir alterar o documento ');
  cxSetResourceString(@sdxPDFDialogSecurityAllowComments, 'Permitir coment�rios ');
  cxSetResourceString(@sdxPDFDialogSecurityAllowCopy, 'Permitir c�pia de conte�do e extra��o');
  cxSetResourceString(@sdxPDFDialogSecurityAllowDocumentAssemble, 'Permitir Assembl�ia documento');
  cxSetResourceString(@sdxPDFDialogSecurityAllowPrint, 'Permitir Print');
  cxSetResourceString(@sdxPDFDialogSecurityAllowPrintHiResolution, 'Permitir Imprimir com alta resolu��o ');
  cxSetResourceString(@sdxPDFDialogSecurityEnabled, 'Ativado');
  cxSetResourceString(@sdxPDFDialogSecurityMethod, 'M�todo:');
  cxSetResourceString(@sdxPDFDialogSecurityOwnerPassword, 'Owner senha:');
  cxSetResourceString(@sdxPDFDialogSecuritySettings, 'Configura��es de Seguran�a');
  cxSetResourceString(@sdxPDFDialogSecurityUserPassword, 'senha de usu�rio:');
  cxSetResourceString(@sdxPDFDialogSubject, 'Assunto');
  cxSetResourceString(@sdxPDFDialogTitle, 'T�tulo');
  cxSetResourceString(@sdxPDFDialogUseCIDFonts, 'Use CID Fontes');
  cxSetResourceString(@sdxPDFDialogUseJPEGCompression, 'Use JPEG compacta��o das imagens');
  cxSetResourceString(@sdxPDFDialogTabDocInfo, 'Documenta��o e Informa��o ');
  cxSetResourceString(@sdxPDFDialogTabExport, '&Export');
  cxSetResourceString(@sdxPDFDialogTabPages, '&Paginas');
  cxSetResourceString(@sdxPDFDialogTabSecurity, 'Seguran�a');

  { cordas designers}

  {Meses}

  cxSetResourceString(@sdxJanuary, 'janeiro');
  cxSetResourceString(@sdxFebruary, 'de Fevereiro �');
  cxSetResourceString(@sdxMarch, 'Mar�o');
  cxSetResourceString(@sdxApril, 'April');
  cxSetResourceString(@sdxMay, 'Maio');
  cxSetResourceString(@sdxJune, '�Junho�');
  cxSetResourceString(@sdxJuly, 'de Julho �');
  cxSetResourceString(@sdxAugust, 'Agosto');
  cxSetResourceString(@sdxSeptember, 'Setembro');
  cxSetResourceString(@sdxOctober, 'outubro');
  cxSetResourceString(@sdxNovember, 'November');
  cxSetResourceString(@sdxDecember, 'Dezembro �');

  cxSetResourceString(@sdxEast, 'Oriente');
  cxSetResourceString(@sdxWest, 'Ocidente');
  cxSetResourceString(@sdxSouth, 'Sul');
  cxSetResourceString(@sdxNorth, 'Norte');

  cxSetResourceString(@sdxTotal, 'Total');

  { DxFlowChart}

  cxSetResourceString(@sdxPlan, 'Plano');
  cxSetResourceString(@sdxSwimmingPool, 'Piscina');
  cxSetResourceString(@sdxAdministration, 'Administra��o');
  cxSetResourceString(@sdxPark, 'Park');
  cxSetResourceString(@sdxCarParking, 'Car-Parking');

  { DxOrgChart}


  // -- ComboBox de Data
  cxSetResourceString(@cxSDatePopupClear, 'Limpar');
  cxSetResourceString(@cxSDatePopupToday, 'Hoje');

  // -- cxGrid
  cxSetResourceString(@scxGridGroupByBoxCaption, 'Agrupamento de colunas');
  cxSetResourceString(@scxGridNoDataInfoText, '<Nenhum Registro>');
  cxSetResourceString(@scxGridLayoutViewRecordCaptionDefaultMask,
    '[RecordIndex] de [RecordCount]');
  cxSetResourceString(@scxGridFilterCustomizeButtonCaption, 'Personalizar...');
  cxSetResourceString(@scxGridFilterRowInfoText, 'Clique aqui para definir um filtro');
  cxSetResourceString(@scxGridNewItemRowInfoText, 'Clique aqui para adicionar um novo registro');
  cxSetResourceString(@scxGridFilterIsEmpty, '<Filtro vazio>');
  cxSetResourceString(@scxGridCustomizationFormCaption, 'Personalizar');
  cxSetResourceString(@scxGridCustomizationFormColumnsPageCaption, 'Colunas');
  cxSetResourceString(@scxGridFilterApplyButtonCaption, 'Applicar filtro');
  cxSetResourceString(@scxGridColumnsQuickCustomizationHint, 'Clique aqui para exibir/ocultar/mover colunas');
  cxSetResourceString(@scxGridCustomizationFormBandsPageCaption, 'Bandas');
  cxSetResourceString(@scxGridBandsQuickCustomizationHint, 'Clique aqui para exibir/ocultar/mover bandas');
  cxSetResourceString(@scxGridCustomizationFormRowsPageCaption, 'Linhas');

  // --Componente de Filtro
  cxSetResourceString(@cxSFilterFooterAddCondition, 'Adicionar nova condi��o');
  cxSetResourceString(@cxSFilterAddCondition, 'Adicionar condi��o');
  cxSetResourceString(@cxSFilterAddGroup, 'Adicionar grupo');
  cxSetResourceString(@cxSFilterRemoveRow, 'Remover condi��o');
  cxSetResourceString(@cxSFilterClearAll, 'Limpar filtros');
  cxSetResourceString(@cxSFilterErrorBuilding, 'N�o foi poss�vel realizar o fitro');
  cxSetResourceString(@cxSFilterGroupCaption, 'Aplicar as condi��es');
  cxSetResourceString(@cxSFilterControlDialogNewFile, 'SemTitulo.flt');
  cxSetResourceString(@cxSFilterRootGroupCaption, '<raiz>');

  cxSetResourceString(@cxSFilterRootButtonCaption, 'Filtro');
  cxSetResourceString(@cxSFilterBoolOperatorAnd, 'e');
  cxSetResourceString(@cxSFilterBoolOperatorNotAnd, '&n�o');
  cxSetResourceString(@cxSFilterBoolOperatorOr, 'ou');
  cxSetResourceString(@cxSFilterBoolOperatorNotOr, 'ou n�o');

  cxSetResourceString(@cxSFilterControlDialogActionApplyCaption, 'Aplicar');
  cxSetResourceString(@cxSFilterControlDialogActionCancelCaption, 'Cancelar');
  cxSetResourceString(@cxSFilterControlDialogActionOkCaption, 'OK');

  cxSetResourceString(@cxSFilterControlDialogActionOpenCaption, 'Abrir...');
  cxSetResourceString(@cxSFilterControlDialogActionSaveCaption, 'Salvar Como...');

  cxSetResourceString(@cxSFilterControlDialogCaption, 'Filtrar');
  cxSetResourceString(@cxSFilterControlDialogFileFilter, 'Filtros');
  cxSetResourceString(@cxSFilterControlDialogOpenDialogCaption, 'Abrir um filtro existente');
  cxSetResourceString(@cxSFilterControlDialogSaveDialogCaption, 'Salvar o filtro atual para arquivo');

  cxSetResourceString(@cxSFilterControlNullString, '<Vazio>');
  cxSetResourceString(@cxSFilterDialogCaption, 'Filtro personalizado');
  cxSetResourceString(@cxSFilterDialogCharactersSeries, 'Qualquer tipo de caracter');
  cxSetResourceString(@cxSFilterDialogInvalidValue, 'Valor inv�lido!');

  cxSetResourceString(@cxSFilterDialogOperationAnd, 'e');
  cxSetResourceString(@cxSFilterDialogOperationOr, 'ou');

  cxSetResourceString(@cxSFilterDialogRows, 'Mostrar registros onde o campo:');
  cxSetResourceString(@cxSFilterDialogSingleCharacter, 'Caracter �nico');
  cxSetResourceString(@cxSFilterDialogUse, 'Usar');

  cxSetResourceString(@cxSFilterAndCaption, 'e');
  cxSetResourceString(@cxSFilterBlankCaption, 'nulo');
  cxSetResourceString(@cxSFilterBoxAllCaption, 'todos');
  cxSetResourceString(@cxSFilterBoxBlanksCaption, 'nulo');
  cxSetResourceString(@cxSFilterBoxCustomCaption, 'personalizar...');
  cxSetResourceString(@cxSFilterBoxNonBlanksCaption, 'n�o nulo');
  cxSetResourceString(@cxSFilterNotCaption, 'n�o');
  cxSetResourceString(@cxSFilterOperatorBeginsWith, 'comece com');
  cxSetResourceString(@cxSFilterOperatorBetween, 'esteja entre');
  cxSetResourceString(@cxSFilterOperatorContains, 'contenha');
  cxSetResourceString(@cxSFilterOperatorDoesNotBeginWith, 'n�o comece com');
  cxSetResourceString(@cxSFilterOperatorDoesNotContain, 'n�o contenha');
  cxSetResourceString(@cxSFilterOperatorDoesNotEndWith, 'n�o termine com');
  cxSetResourceString(@cxSFilterOperatorEndsWith, 'termine com');
  cxSetResourceString(@cxSFilterOperatorEqual, 'igual');
  cxSetResourceString(@cxSFilterOperatorGreater, 'maior que');
  cxSetResourceString(@cxSFilterOperatorGreaterEqual, 'maior ou igual a');
  cxSetResourceString(@cxSFilterOperatorInList, 'esteja na lista');
  cxSetResourceString(@cxSFilterOperatorIsNotNull, 'n�o nulo');
  cxSetResourceString(@cxSFilterOperatorIsNull, 'nulo');
  cxSetResourceString(@cxSFilterOperatorLastMonth, 'm�s anterior');
  cxSetResourceString(@cxSFilterOperatorLastWeek, 'semana anterior');
  cxSetResourceString(@cxSFilterOperatorLastYear, 'ano anterior');
  cxSetResourceString(@cxSFilterOperatorLess, 'menor');
  cxSetResourceString(@cxSFilterOperatorLessEqual, 'menor ou igual');
  cxSetResourceString(@cxSFilterOperatorLike, 'contenha');
  cxSetResourceString(@cxSFilterOperatorNextMonth, 'pr�ximo m�s');
  cxSetResourceString(@cxSFilterOperatorNextWeek, 'pr�xima semana');
  cxSetResourceString(@cxSFilterOperatorNextYear, 'pr�ximo ano');
  cxSetResourceString(@cxSFilterOperatorNotBetween, 'n�o esteja entre');
  cxSetResourceString(@cxSFilterOperatorNotEqual, 'diferente');
  cxSetResourceString(@cxSFilterOperatorNotInList, 'n�o esteja na lista');
  cxSetResourceString(@cxSFilterOperatorNotLike, 'n�o contenha');
  cxSetResourceString(@cxSFilterOperatorThisMonth, 'neste m�s');
  cxSetResourceString(@cxSFilterOperatorThisWeek, 'nesta semana');
  cxSetResourceString(@cxSFilterOperatorThisYear, 'neste ano');
  cxSetResourceString(@cxSFilterOperatorToday, 'hoje');
  cxSetResourceString(@cxSFilterOperatorTomorrow, 'amanh�');
  cxSetResourceString(@cxSFilterOperatorYesterday, 'ontem');
  cxSetResourceString(@cxSFilterOrCaption, 'ou');

  // Espa�o entre datas
  cxSetResourceString(@scxGridYesterday, 'Ontem');
  cxSetResourceString(@scxGridToday, 'Hoje');
  cxSetResourceString(@scxGridTomorrow, 'Amanha');
  cxSetResourceString(@scxGridLast30Days, 'Ultimos 30 dias');
  cxSetResourceString(@scxGridLast14Days, 'Ultimos 14 dias');
  cxSetResourceString(@scxGridLast7Days, 'Ultimos 7 dias');
  cxSetResourceString(@scxGridNext7Days, 'Proximos 7 dias');
  cxSetResourceString(@scxGridNext14Days, 'Proximos 14 dais');
  cxSetResourceString(@scxGridNext30Days, 'Proximos 30 dias');
  cxSetResourceString(@scxGridLastTwoWeeks, 'Ultimas duas semanas');
  cxSetResourceString(@scxGridLastWeek, 'Semana passada');
  cxSetResourceString(@scxGridThisWeek, 'Esta semana');
  cxSetResourceString(@scxGridNextWeek, 'Semana que vem');
  cxSetResourceString(@scxGridNextTwoWeeks, 'Proximas duas semanas');
  cxSetResourceString(@scxGridLastMonth, 'Mes passado');
  cxSetResourceString(@scxGridThisMonth, 'Este mes');
  cxSetResourceString(@scxGridNextMonth, 'Mes que vem');
  cxSetResourceString(@scxGridLastYear, 'Ultimo ano');
  cxSetResourceString(@scxGridThisYear, 'Este ano');
  cxSetResourceString(@scxGridNextYear, 'Ano que vem');
  cxSetResourceString(@scxGridPast, 'Passado');
  cxSetResourceString(@scxGridFuture, 'Futuro');

  // -- Componente Pop-up Menu
  cxSetResourceString(@cxSGridAlignCenter, 'Centralizar');
  cxSetResourceString(@cxSGridAlignLeft, 'Alinhar � esquerda');
  cxSetResourceString(@cxSGridAlignRight, 'Alinhar � direita');
  cxSetResourceString(@cxSGridAlignmentSubMenu, 'Alinhamento');
  cxSetResourceString(@cxSGridAvgMenuItem, 'M�dia');
  cxSetResourceString(@cxSGridBestFit, 'Tamanho autom�tico da coluna');
  cxSetResourceString(@cxSGridBestFitAllColumns, 'Tamanho autom�tico (Todas as colunas)');
  cxSetResourceString(@cxSGridCountMenuItem, 'Contar');
  cxSetResourceString(@cxSGridFieldChooser, 'Escolha de colunas');
  cxSetResourceString(@cxSGridGroupByBox, 'Agrupamento de colunas');
  cxSetResourceString(@cxSGridGroupByThisField, 'Agrupar por esta coluna');
  cxSetResourceString(@cxSGridMaxMenuItem, 'M�ximo');
  cxSetResourceString(@cxSGridMinMenuItem, 'M�nimo');
  cxSetResourceString(@cxSGridNone, 'Nenhum');
  cxSetResourceString(@cxSGridNoneMenuItem, 'Nenhum');
  cxSetResourceString(@cxSGridRemoveColumn, 'Remover esta coluna');
  cxSetResourceString(@cxSGridShowGroupFooter, 'Rodap� agrupado');
  cxSetResourceString(@cxSGridSortColumnAsc, 'Ordena��o ascendente');
  cxSetResourceString(@cxSGridSortColumnDesc, 'Ordena��o descendente');
  cxSetResourceString(@cxSGridClearSorting, 'Cancelar ordena��o');
  cxSetResourceString(@cxSGridSumMenuItem, 'Somar');
  cxSetResourceString(@cxSGridShowFooter, 'Rodap�');

  // -- Navigator
  cxSetResourceString(@cxNavigator_DeleteRecordQuestion, 'Excluir realmente este registro ?');
  cxSetResourceString(@cxNavigatorHint_Append, 'Adicionar registro');
  cxSetResourceString(@cxNavigatorHint_Cancel, 'Cancelar edi��o');
  cxSetResourceString(@cxNavigatorHint_Delete, 'Excluir registro');
  cxSetResourceString(@cxNavigatorHint_Edit, 'Editar registro');
  cxSetResourceString(@cxNavigatorHint_Filter, 'Filtrar dados');
  cxSetResourceString(@cxNavigatorHint_First, 'Primeiro registro');
  cxSetResourceString(@cxNavigatorHint_GotoBookmark, 'Ir para registro marcado');
  cxSetResourceString(@cxNavigatorHint_Insert, 'Inserir registro');
  cxSetResourceString(@cxNavigatorHint_Last, '�ltimo registro');
  cxSetResourceString(@cxNavigatorHint_Next, 'Pr�ximo registro');
  cxSetResourceString(@cxNavigatorHint_NextPage, 'Pr�xima p�gina');
  cxSetResourceString(@cxNavigatorHint_Post, 'Salvar altera��es');
  cxSetResourceString(@cxNavigatorHint_Prior, 'Registro anterior');
  cxSetResourceString(@cxNavigatorHint_PriorPage, 'P�gina anterior');
  cxSetResourceString(@cxNavigatorHint_Refresh, 'Atualizar');
  cxSetResourceString(@cxNavigatorHint_SaveBookmark, 'Marcar registro');
  cxSetResourceString(@cxNavigatorInfoPanelDefaultDisplayMask, '[RecordIndex] de [RecordCount]');

  // -- Blob
  cxSetResourceString(@cxSBlobButtonCancel, '&Cancela');
  cxSetResourceString(@cxSBlobButtonClose, '&Fechar');
  cxSetResourceString(@cxSBlobMemo, '(CONT�M TEXTO)');
  cxSetResourceString(@cxSBlobMemoEmpty, '(vazio)');
  cxSetResourceString(@cxSBlobPicture, '(CONT�M FIGURA)');
  cxSetResourceString(@cxSBlobPictureEmpty, '(vazio)');

  // -- Popup menu items
  cxSetResourceString(@cxSMenuItemCaptionCut, 'Recor&tar');
  cxSetResourceString(@cxSMenuItemCaptionCopy, '&Copiar');
  cxSetResourceString(@cxSMenuItemCaptionPaste, 'Co&lar');
  cxSetResourceString(@cxSMenuItemCaptionDelete, '&Limpar');
  cxSetResourceString(@cxSMenuItemCaptionLoad, '&Abrir...');
  cxSetResourceString(@cxSMenuItemCaptionSave, 'Sal&var como...');
end;

{ TDelphiTranslate }

class procedure TDelphiTranslate.TranslateDelphiDBMessages_PTBR;

  procedure SetResourceString(xOldResourceString: PResStringRec; xValueChanged: PChar);
  var POldProtect: DWORD;
  begin
    VirtualProtect(xOldResourceString, SizeOf(xOldResourceString^),
                   PAGE_EXECUTE_READWRITE, @POldProtect);
     xOldResourceString^.Identifier := Integer(xValueChanged);
     VirtualProtect(xOldResourceString,SizeOf(xOldResourceString^),POldProtect,
                    @POldProtect);
  end;

begin
  //SetResourceString(@SInvalidFieldSize, 'Invalid field size');
  //SetResourceString(@SInvalidFieldKind, 'Invalid FieldKind');
  //SetResourceString(@SInvalidFieldRegistration, 'Invalid field registration');
  //SetResourceString(@SUnknownFieldType, 'Field ''%s'' is of an unknown type');
  //SetResourceString(@SFieldNameMissing, 'Field name missing');
  //SetResourceString(@SDuplicateFieldName, 'Duplicate field name ''%s''');
  //SetResourceString(@SFieldNotFound, 'Field ''%s'' not found');
  //SetResourceString(@SFieldAccessError, 'Cannot access field ''%s'' as type %s');
  SetResourceString(@SFieldValueError, PChar('Valor Inv�lido para o campo ''%s'''));
  //SetResourceString(@SFieldRangeError, '%g is not a valid value for field ''%s''. The allowed range is %g to %g');
{  SetResourceString(@SBcdFieldRangeError, '%s is not a valid value for field ''%s''. The allowed range is %s to %s');
  SetResourceString(@SInvalidIntegerValue, '''%s'' is not a valid integer value for field ''%s''');
  SetResourceString(@SInvalidBoolValue, '''%s'' is not a valid boolean value for field ''%s''');
  SetResourceString(@SInvalidFloatValue, '''%s'' is not a valid floating point value for field ''%s''');
  SetResourceString(@SFieldTypeMismatch, 'Type mismatch for field ''%s'', expecting: %s actual: %s');
  SetResourceString(@SFieldSizeMismatch, 'Size mismatch for field ''%s'', expecting: %d actual: %d');
  SetResourceString(@SInvalidVarByteArray, 'Invalid variant type or size for field ''%s''');
  SetResourceString(@SFieldOutOfRange, 'Value of field ''%s'' is out of range');
  SetResourceString(@SCantAdjustPrecision, 'Error adjusting BCD precision');}
  SetResourceString(@SFieldRequired, PChar('O Campo ''%s'' precisa ser preenchido!'));
  {SetResourceString(@SDataSetMissing, 'Field ''%s'' has no dataset');
  SetResourceString(@SInvalidCalcType, 'Field ''%s'' cannot be a calculated or lookup field');
  SetResourceString(@SFieldReadOnly, 'Field ''%s'' cannot be modified');
  SetResourceString(@SFieldIndexError, 'Field index out of range');
  SetResourceString(@SNoFieldIndexes, 'No index currently active');
  SetResourceString(@SNotIndexField, 'Field ''%s'' is not indexed and cannot be modified');
  SetResourceString(@SIndexFieldMissing, 'Cannot access index field ''%s''');
  SetResourceString(@SDuplicateIndexName, 'Duplicate index name ''%s''');
  SetResourceString(@SNoIndexForFields, 'No index for fields ''%s''');
  SetResourceString(@SIndexNotFound, 'Index ''%s'' not found');
  SetResourceString(@SDBDuplicateName, 'Duplicate name ''%s'' in %s');
  SetResourceString(@SCircularDataLink, 'Circular datalinks are not allowed');
  SetResourceString(@SLookupInfoError, 'Lookup information for field ''%s'' is incomplete');
  SetResourceString(@SNewLookupFieldCaption, 'New Lookup Field');
  SetResourceString(@SDataSourceChange, 'DataSource cannot be changed');
  SetResourceString(@SNoNestedMasterSource, 'Nested datasets cannot have a MasterSource');
  SetResourceString(@SDataSetOpen, 'Cannot perform this operation on an open dataset');
  SetResourceString(@SNotEditing, 'Dataset not in edit or insert mode');
  SetResourceString(@SDataSetClosed, 'Cannot perform this operation on a closed dataset');
  SetResourceString(@SDataSetEmpty, 'Cannot perform this operation on an empty dataset');
  SetResourceString(@SDataSetReadOnly, 'Cannot modify a read-only dataset');
  SetResourceString(@SNestedDataSetClass, 'Nested dataset must inherit from %s');
  SetResourceString(@SExprTermination, 'Filter expression incorrectly terminated');
  SetResourceString(@SExprNameError, 'Unterminated field name');
  SetResourceString(@SExprStringError, 'Unterminated string constant');
  SetResourceString(@SExprInvalidChar, 'Invalid filter expression character: ''%s''');
  SetResourceString(@SExprNoLParen, '''('' expected but %s found');
  SetResourceString(@SExprNoRParen, ''')'' expected but %s found');
  SetResourceString(@SExprNoRParenOrComma, ''')'' or '','' expected but %s found');
  SetResourceString(@SExprExpected, 'Expression expected but %s found');
  SetResourceString(@SExprBadField, 'Field ''%s'' cannot be used in a filter expression');
  SetResourceString(@SExprBadNullTest, 'NULL only allowed with ''='' and ''<>''');
  SetResourceString(@SExprRangeError, 'Constant out of range');
  SetResourceString(@SExprNotBoolean, 'Field ''%s'' is not of type Boolean');
  SetResourceString(@SExprIncorrect, 'Incorrectly formed filter expression');
  SetResourceString(@SExprNothing, 'nothing');
  SetResourceString(@SExprTypeMis, 'Type mismatch in expression');
  SetResourceString(@SExprBadScope, 'Operation cannot mix aggregate value with record-varying value');
  SetResourceString(@SExprNoArith, 'Arithmetic in filter expressions not supported');
  SetResourceString(@SExprNotAgg, 'Expression is not an aggregate expression');
  SetResourceString(@SExprBadConst, 'Constant is not correct type %s');
  SetResourceString(@SExprNoAggFilter, 'Aggregate expressions not allowed in filters');
  SetResourceString(@SExprEmptyInList, 'IN predicate list may not be empty');
  SetResourceString(@SInvalidKeywordUse, 'Invalid use of keyword');
  SetResourceString(@STextFalse, 'False');
  SetResourceString(@STextTrue, 'True');
  SetResourceString(@SParameterNotFound, 'Parameter ''%s'' not found');
  SetResourceString(@SInvalidVersion, 'Unable to load bind parameters');
  SetResourceString(@SParamTooBig, 'Parameter ''%s'', cannot save data larger than %d bytes');
  SetResourceString(@SBadFieldType, 'Field ''%s'' is of an unsupported type');
  SetResourceString(@SAggActive, 'Property may not be modified while aggregate is active');
  SetResourceString(@SProviderSQLNotSupported, 'SQL not supported');
  SetResourceString(@SProviderExecuteNotSupported, 'Execute not supported');
  SetResourceString(@SExprNoAggOnCalcs, 'Field ''%s'' is not the correct type of calculated field to be used in an aggregate, use an internalcalc');
  SetResourceString(@SRecordChanged, 'Record not found or changed by another user');
  SetResourceString(@SDataSetUnidirectional, 'Operation not allowed on a unidirectional dataset');
  SetResourceString(@SUnassignedVar, 'Unassigned variant value');
  SetResourceString(@SRecordNotFound, 'Record not found');
  SetResourceString(@SFileNameBlank, 'FileName property cannot be blank');
  SetResourceString(@SFieldNameTooLarge, 'Fieldname %s exceeds %d chars');

{ For FMTBcd }

{  SetResourceString(@SBcdOverflow, 'BCD overflow');
  SetResourceString(@SInvalidBcdValue, '%s is not a valid BCD value');
  SetResourceString(@SInvalidFormatType, 'Invalid format type for BCD');

{ For SqlTimSt }

{  SetResourceString(@SCouldNotParseTimeStamp, 'Could not parse SQL TimeStamp string');
  SetResourceString(@SInvalidSqlTimeStamp, 'Invalid SQL date/time values');
  SetResourceString(@SCalendarTimeCannotBeRepresented, 'Calendar time cannot be represented');

  SetResourceString(@SDeleteRecordQuestion, 'Delete record?');
  SetResourceString(@SDeleteMultipleRecordsQuestion, 'Delete all selected records?');
  SetResourceString(@STooManyColumns, 'Grid requested to display more than 256 columns');

  { For reconcile error }
{  SetResourceString(@SSkip, 'Skip');
  SetResourceString(@SAbort, 'Abort');
  SetResourceString(@SMerge, 'Merge');
  SetResourceString(@SCorrect, 'Correct');
  SetResourceString(@SCancel , 'Cancel');
  SetResourceString(@SRefresh, 'Refresh');
  SetResourceString(@SModified, 'Modified');
  SetResourceString(@SInserted, 'Inserted');
  SetResourceString(@SDeleted , 'Deleted');
  SetResourceString(@SCaption, 'Update Error - %s');
  SetResourceString(@SUnchanged, '<Unchanged>');
  SetResourceString(@SBinary, '(Binary)');
  SetResourceString(@SAdt, '(ADT)');
  SetResourceString(@SArray, '(Array)');
  SetResourceString(@SFieldName, 'Field Name');
  SetResourceString(@SOriginal, 'Original Value');
  SetResourceString(@SConflict, 'Conflicting Value');
  SetResourceString(@SValue, ' Value');
  SetResourceString(@SNoData, '<No Records>');
  SetResourceString(@SNew, 'New');
}
end;

class procedure TDelphiTranslate.TranslateDelphiMessages_PTBR;

  procedure SetResourceString(xOldResourceString: PResStringRec; xValueChanged: PChar);
  var POldProtect: DWORD;
  begin
    VirtualProtect(xOldResourceString, SizeOf(xOldResourceString^),
                   PAGE_EXECUTE_READWRITE, @POldProtect);
     xOldResourceString^.Identifier := Integer(xValueChanged);
     VirtualProtect(xOldResourceString,SizeOf(xOldResourceString^),POldProtect,
                    @POldProtect);
  end;

begin
  {
  SetResourceString(@SOpenFileTitle, 'Open');
  SetResourceString(@SCantWriteResourceStreamError, 'Can''t write to a read-only resource stream');
  SetResourceString(@SDuplicateReference, 'WriteObject called twice for the same instance');
  SetResourceString(@SClassMismatch, 'Resource %s is of incorrect class');
  SetResourceString(@SInvalidTabIndex, 'Tab index out of bounds');
  SetResourceString(@SInvalidTabPosition, 'Tab position incompatible with current tab style');
  SetResourceString(@SInvalidTabStyle, 'Tab style incompatible with current tab position');
  SetResourceString(@SInvalidBitmap, 'Bitmap image is not valid');
  SetResourceString(@SInvalidIcon, 'Icon image is not valid');
  SetResourceString(@SInvalidMetafile, 'Metafile is not valid');
  SetResourceString(@SInvalidPixelFormat, 'Invalid pixel format');
  SetResourceString(@SInvalidImage, 'Invalid image');
  SetResourceString(@SBitmapEmpty, 'Bitmap is empty');
  SetResourceString(@SScanLine, 'Scan line index out of range');
  SetResourceString(@SChangeIconSize, 'Cannot change the size of an icon');
  SetResourceString(@SChangeWicSize, 'Cannot change the size of a WIC Image');
  SetResourceString(@SOleGraphic, 'Invalid operation on TOleGraphic');
  SetResourceString(@SUnknownExtension, 'Unknown picture file extension (.%s)');
  SetResourceString(@SUnknownClipboardFormat, 'Unsupported clipboard format');
  SetResourceString(@SOutOfResources, 'Out of system resources');
  SetResourceString(@SNoCanvasHandle, 'Canvas does not allow drawing');
  SetResourceString(@SInvalidImageSize, 'Invalid image size');
  SetResourceString(@STooManyImages, 'Too many images');
  SetResourceString(@SDimsDoNotMatch, 'Image dimensions do not match image list dimensions');
  SetResourceString(@SInvalidImageList, 'Invalid ImageList');
  SetResourceString(@SReplaceImage, 'Unable to Replace Image');
  SetResourceString(@SImageIndexError, 'Invalid ImageList Index');
  SetResourceString(@SImageReadFail, 'Failed to read ImageList data from stream');
  SetResourceString(@SImageWriteFail, 'Failed to write ImageList data to stream');
  SetResourceString(@SWindowDCError, 'Error creating window device context');
  SetResourceString(@SClientNotSet, 'Client of TDrag not initialized');
  SetResourceString(@SWindowClass, 'Error creating window class');
  SetResourceString(@SWindowCreate, 'Error creating window');
  SetResourceString(@SCannotFocus, 'Cannot focus a disabled or invisible window');
  SetResourceString(@SParentRequired, 'Control ''%s'' has no parent window');
  SetResourceString(@SParentGivenNotAParent, 'Parent given is not a parent of ''%s''');
  SetResourceString(@SMDIChildNotVisible, 'Cannot hide an MDI Child Form');
  SetResourceString(@SVisibleChanged, 'Cannot change Visible in OnShow or OnHide');
  SetResourceString(@SCannotShowModal, 'Cannot make a visible window modal');
  SetResourceString(@SScrollBarRange, 'Scrollbar property out of range');
  SetResourceString(@SPropertyOutOfRange, '%s property out of range');
  SetResourceString(@SMenuIndexError, 'Menu index out of range');
  SetResourceString(@SMenuReinserted, 'Menu inserted twice');
  SetResourceString(@SMenuNotFound, 'Sub-menu is not in menu');
  SetResourceString(@SNoTimers, 'Not enough timers available');
  SetResourceString(@SNotPrinting, 'Printer is not currently printing');
  SetResourceString(@SPrinting, 'Printing in progress');
  SetResourceString(@SPrinterIndexError, 'Printer index out of range');
  SetResourceString(@SInvalidPrinter, 'Printer selected is not valid');
  SetResourceString(@SDeviceOnPort, '%s on %s');
  SetResourceString(@SGroupIndexTooLow, 'GroupIndex cannot be less than a previous menu item''s GroupIndex');
  SetResourceString(@STwoMDIForms, 'Cannot have more than one MDI form per application');
  SetResourceString(@SNoMDIForm, 'Cannot create form. No MDI forms are currently active');
  SetResourceString(@SImageCanvasNeedsBitmap, 'Can only modify an image if it contains a bitmap');
  SetResourceString(@SControlParentSetToSelf, 'A control cannot have itself as its parent');
  SetResourceString(@SOKButton, 'OK');
  SetResourceString(@SCancelButton, 'Cancel');
  SetResourceString(@SYesButton, '&Yes');
  SetResourceString(@SNoButton, '&No');
  SetResourceString(@SHelpButton, '&Help');
  SetResourceString(@SCloseButton, '&Close');
  SetResourceString(@SIgnoreButton, '&Ignore');
  SetResourceString(@SRetryButton, '&Retry');
  SetResourceString(@SAbortButton, 'Abort');
  SetResourceString(@SAllButton, '&All');

  SetResourceString(@SCannotDragForm, 'Cannot drag a form');
  SetResourceString(@SPutObjectError, 'PutObject to undefined item');
  SetResourceString(@SCardDLLNotLoaded, 'Could not load CARDS.DLL');
  SetResourceString(@SDuplicateCardId, 'Duplicate CardId found');

  SetResourceString(@SDdeErr, 'An error returned from DDE  ($0%x)');
  SetResourceString(@SDdeConvErr, 'DDE Error - conversation not established ($0%x)');
  SetResourceString(@SDdeMemErr, 'Error occurred when DDE ran out of memory ($0%x)');
  SetResourceString(@SDdeNoConnect, 'Unable to connect DDE conversation');

  SetResourceString(@SFB, 'FB');
  SetResourceString(@SFG, 'FG');
  SetResourceString(@SBG, 'BG');
  SetResourceString(@SOldTShape, 'Cannot load older version of TShape');
  SetResourceString(@SVMetafiles, 'Metafiles');
  SetResourceString(@SVEnhMetafiles, 'Enhanced Metafiles');
  SetResourceString(@SVIcons, 'Icons');
  SetResourceString(@SVBitmaps, 'Bitmaps');
  SetResourceString(@SVTIFFImages, 'TIFF Images');

  SetResourceString(@SVJPGImages, 'JPEG Images');
  SetResourceString(@SVPNGImages, 'PNG Images');
  SetResourceString(@SVGIFImages, 'GIF Images');

  SetResourceString(@SGridTooLarge, 'Grid too large for operation');
  SetResourceString(@STooManyDeleted, 'Too many rows or columns deleted');
  SetResourceString(@SIndexOutOfRange, 'Grid index out of range');
  SetResourceString(@SFixedColTooBig, 'Fixed column count must be less than column count');
  SetResourceString(@SFixedRowTooBig, 'Fixed row count must be less than row count');
  SetResourceString(@SInvalidStringGridOp, 'Cannot insert or delete rows from grid');
  SetResourceString(@SInvalidEnumValue, 'Invalid Enum Value');
  SetResourceString(@SInvalidNumber, 'Invalid numeric value');
  SetResourceString(@SOutlineIndexError, 'Outline index not found');
  SetResourceString(@SOutlineExpandError, 'Parent must be expanded');
  SetResourceString(@SInvalidCurrentItem, 'Invalid value for current item');
  SetResourceString(@SMaskErr, 'Invalid input value');
  SetResourceString(@SMaskEditErr, 'Invalid input value.  Use escape key to abandon changes');
  SetResourceString(@SOutlineError, 'Invalid outline index');
  SetResourceString(@SOutlineBadLevel, 'Incorrect level assignment');
  SetResourceString(@SOutlineSelection, 'Invalid selection');
  SetResourceString(@SOutlineFileLoad, 'File load error');
  SetResourceString(@SOutlineLongLine, 'Line too long');
  SetResourceString(@SOutlineMaxLevels, 'Maximum outline depth exceeded');

  SetResourceString(@SMsgDlgWarning, 'Warning');
  SetResourceString(@SMsgDlgError, 'Error');
  SetResourceString(@SMsgDlgInformation, 'Information');
  SetResourceString(@SMsgDlgConfirm, 'Confirm');
  SetResourceString(@SMsgDlgYes, '&Yes');
  SetResourceString(@SMsgDlgNo, '&No');
  SetResourceString(@SMsgDlgOK, 'OK');
  SetResourceString(@SMsgDlgCancel, 'Cancel');
  SetResourceString(@SMsgDlgHelp, '&Help');
  SetResourceString(@SMsgDlgHelpNone, 'No help available');
  SetResourceString(@SMsgDlgHelpHelp, 'Help');
  SetResourceString(@SMsgDlgAbort, '&Abort');
  SetResourceString(@SMsgDlgRetry, '&Retry');
  SetResourceString(@SMsgDlgIgnore, '&Ignore');
  SetResourceString(@SMsgDlgAll, '&All');
  SetResourceString(@SMsgDlgNoToAll, 'N&o to All');
  SetResourceString(@SMsgDlgYesToAll, 'Yes to &All');
  SetResourceString(@SMsgDlgClose, '&Close');

  SetResourceString(@SmkcBkSp, 'BkSp');
  SetResourceString(@SmkcTab, 'Tab');
  SetResourceString(@SmkcEsc, 'Esc');
  SetResourceString(@SmkcEnter, 'Enter');
  SetResourceString(@SmkcSpace, 'Space');
  SetResourceString(@SmkcPgUp, 'PgUp');
  SetResourceString(@SmkcPgDn, 'PgDn');
  SetResourceString(@SmkcEnd, 'End');
  SetResourceString(@SmkcHome, 'Home');
  SetResourceString(@SmkcLeft, 'Left');
  SetResourceString(@SmkcUp, 'Up');
  SetResourceString(@SmkcRight, 'Right');
  SetResourceString(@SmkcDown, 'Down');
  SetResourceString(@SmkcIns, 'Ins');
  SetResourceString(@SmkcDel, 'Del');
  SetResourceString(@SmkcShift, 'Shift+');
  SetResourceString(@SmkcCtrl, 'Ctrl+');
  SetResourceString(@SmkcAlt, 'Alt+');

  SetResourceString(@srUnknown, '(Unknown)');
  SetResourceString(@srNone, '(None)');
  SetResourceString(@SOutOfRange, 'Value must be between %d and %d');

  SetResourceString(@SDateEncodeError, 'Invalid argument to date encode');
  SetResourceString(@SDefaultFilter, 'All files (*.*)|*.*');
  SetResourceString(@sAllFilter, 'All');
  SetResourceString(@SNoVolumeLabel, ': [ - no volume label - ]');
  SetResourceString(@SInsertLineError, 'Unable to insert a line');

  SetResourceString(@SConfirmCreateDir, 'The specified directory does not exist. Create it?');
  SetResourceString(@SSelectDirCap, 'Select Directory');
  SetResourceString(@SDirNameCap, 'Directory &Name:');
  SetResourceString(@SDrivesCap, 'D&rives:');
  SetResourceString(@SDirsCap, '&Directories:');
  SetResourceString(@SFilesCap, '&Files: (*.*)');
  SetResourceString(@SNetworkCap, 'Ne&twork...');

  SetResourceString(@SColorPrefix, 'Color' deprecated;          //!! obsolete - delete in 5.0
  SetResourceString(@SColorTags, 'ABCDEFGHIJKLMNOP' deprecated; //!! obsolete - delete in 5.0

  SetResourceString(@SInvalidClipFmt, 'Invalid clipboard format');
  SetResourceString(@SIconToClipboard, 'Clipboard does not support Icons');
  SetResourceString(@SCannotOpenClipboard, 'Cannot open clipboard: %s');

  SetResourceString(@SDefault, 'Default');

  SetResourceString(@SInvalidMemoSize, 'Text exceeds memo capacity');
  SetResourceString(@SCustomColors, 'Custom Colors');
  SetResourceString(@SInvalidPrinterOp, 'Operation not supported on selected printer');
  SetResourceString(@SNoDefaultPrinter, 'There is no default printer currently selected');

  SetResourceString(@SIniFileWriteError, 'Unable to write to %s');

  SetResourceString(@SBitsIndexError, 'Bits index out of range');

  SetResourceString(@SUntitled, '(Untitled)');

  SetResourceString(@SInvalidRegType, 'Invalid data type for ''%s''');

  SetResourceString(@SUnknownConversion, 'Unknown RichEdit conversion file extension (.%s)');
  SetResourceString(@SDuplicateMenus, 'Menu ''%s'' is already being used by another form');

  SetResourceString(@SPictureLabel, 'Picture:');
  SetResourceString(@SPictureDesc, ' (%dx%d)');
  SetResourceString(@SPreviewLabel, 'Preview');

  SetResourceString(@SCannotOpenAVI, 'Cannot open AVI');

  SetResourceString(@SNotOpenErr, 'No MCI device open');
  SetResourceString(@SMPOpenFilter, 'All files (*.*)|*.*|Wave files (*.wav)|*.wav|Midi files (*.mid)|*.mid|Video for Windows (*.avi)|*.avi');
  SetResourceString(@SMCINil, '');
  SetResourceString(@SMCIAVIVideo, 'AVIVideo');
  SetResourceString(@SMCICDAudio, 'CDAudio');
  SetResourceString(@SMCIDAT, 'DAT');
  SetResourceString(@SMCIDigitalVideo, 'DigitalVideo');
  SetResourceString(@SMCIMMMovie, 'MMMovie');
  SetResourceString(@SMCIOther, 'Other');
  SetResourceString(@SMCIOverlay, 'Overlay');
  SetResourceString(@SMCIScanner, 'Scanner');
  SetResourceString(@SMCISequencer, 'Sequencer');
  SetResourceString(@SMCIVCR, 'VCR');
  SetResourceString(@SMCIVideodisc, 'Videodisc');
  SetResourceString(@SMCIWaveAudio, 'WaveAudio');
  SetResourceString(@SMCIUnknownError, 'Unknown error code');

  SetResourceString(@SBoldItalicFont, 'Bold Italic');
  SetResourceString(@SBoldFont, 'Bold');
  SetResourceString(@SItalicFont, 'Italic');
  SetResourceString(@SRegularFont, 'Regular');

  SetResourceString(@SPropertiesVerb, 'Properties');

  SetResourceString(@SServiceFailed, 'Service failed on %s: %s');
  SetResourceString(@SExecute, 'execute');
  SetResourceString(@SStart, 'start');
  SetResourceString(@SStop, 'stop');
  SetResourceString(@SPause, 'pause');
  SetResourceString(@SContinue, 'continue');
  SetResourceString(@SInterrogate, 'interrogate');
  SetResourceString(@SShutdown, 'shutdown');
  SetResourceString(@SCustomError, 'Service failed in custom message(%d): %s');
  SetResourceString(@SServiceInstallOK, 'Service installed successfully');
  SetResourceString(@SServiceInstallFailed, 'Service "%s" failed to install with error: "%s"');
  SetResourceString(@SServiceUninstallOK, 'Service uninstalled successfully');
  SetResourceString(@SServiceUninstallFailed, 'Service "%s" failed to uninstall with error: "%s"');

  SetResourceString(@SInvalidActionRegistration, 'Invalid action registration');
  SetResourceString(@SInvalidActionUnregistration, 'Invalid action unregistration');
  SetResourceString(@SInvalidActionEnumeration, 'Invalid action enumeration');
  SetResourceString(@SInvalidActionCreation, 'Invalid action creation');

  SetResourceString(@SDockedCtlNeedsName, 'Docked control must have a name');
  SetResourceString(@SDockTreeRemoveError, 'Error removing control from dock tree');
  SetResourceString(@SDockZoneNotFound, ' - Dock zone not found');
  SetResourceString(@SDockZoneHasNoCtl, ' - Dock zone has no control');
  SetResourceString(@SDockZoneVersionConflict, 'Error loading dock zone from the stream. ' +
    'Expecting version %d, but found %d.');

  SetResourceString(@SAllCommands, 'All Commands');

  SetResourceString(@SDuplicateItem, 'List does not allow duplicates ($0%x)');

  SetResourceString(@STextNotFound, 'Text not found: "%s"');
  SetResourceString(@SBrowserExecError, 'No default browser is specified');

  SetResourceString(@SColorBoxCustomCaption, 'Custom...');

  SetResourceString(@SMultiSelectRequired, 'Multiselect mode must be on for this feature');

  SetResourceString(@SKeyCaption, 'Key');
  SetResourceString(@SValueCaption, 'Value');
  SetResourceString(@SKeyConflict, 'A key with the name of "%s" already exists');
  SetResourceString(@SKeyNotFound, 'Key "%s" not found');
  SetResourceString(@SNoColumnMoving, 'goColMoving is not a supported option');
  SetResourceString(@SNoEqualsInKey, 'Key may not contain equals sign ("=")');

  SetResourceString(@SSendError, 'Error sending mail');
  SetResourceString(@SAssignSubItemError, 'Cannot assign a subitem to an actionbar when one of it''s parent''s is already assigned to an actionbar');
  SetResourceString(@SDeleteItemWithSubItems, 'Item %s has subitems, delete anyway?');
  SetResourceString(@SDeleteNotAllowed, 'You are not allowed to delete this item');
  SetResourceString(@SMoveNotAllowed, 'Item %s is not allowed to be moved');
  SetResourceString(@SMoreButtons, 'More Buttons');
  SetResourceString(@SErrorDownloadingURL, 'Error downloading URL: %s');
  SetResourceString(@SUrlMonDllMissing, 'Unable to load %s');
  SetResourceString(@SAllActions, '(All Actions)');
  SetResourceString(@SNoCategory, '(No Category)');
  SetResourceString(@SExpand, 'Expand');
  SetResourceString(@SErrorSettingPath, 'Error setting path: "%s"');
  SetResourceString(@SLBPutError, 'Attempting to put items into a virtual style listbox');
  SetResourceString(@SErrorLoadingFile, 'Error loading previously saved settings file: %s'#13'Would you like to delete it?');
  SetResourceString(@SResetUsageData, 'Reset all usage data?');
  SetResourceString(@SFileRunDialogTitle, 'Run');
  SetResourceString(@SNoName, '(No Name)');
  SetResourceString(@SErrorActionManagerNotAssigned, 'ActionManager must first be assigned');
  SetResourceString(@SAddRemoveButtons, '&Add or Remove Buttons');
  SetResourceString(@SResetActionToolBar, 'Reset Toolbar');
  SetResourceString(@SCustomize, '&Customize...');
  SetResourceString(@SSeparator, 'Separator');
  SetResourceString(@SCircularReferencesNotAllowed, 'Circular references not allowed');
  SetResourceString(@SCannotHideActionBand, '%s does not allow hiding');
  SetResourceString(@SErrorSettingCount, 'Error setting %s.Count');
  SetResourceString(@SListBoxMustBeVirtual, 'Listbox (%s) style must be virtual in order to set Count');
  SetResourceString(@SUnableToSaveSettings, 'Unable to save settings');
  SetResourceString(@SRestoreDefaultSchedule, 'Would you like to reset to the default Priority Schedule?');
  SetResourceString(@SNoGetItemEventHandler, 'No OnGetItem event handler assigned');
  SetResourceString(@SInvalidColorMap, 'Invalid Colormap this ActionBand requires ColorMaps of type TCustomActionBarColorMapEx');
  SetResourceString(@SDuplicateActionBarStyleName, 'A style named %s has already been registered');
  SetResourceString(@SMissingActionBarStyleName, 'A style named %s has not been registered');
  SetResourceString(@SStandardStyleActionBars, 'Standard Style');
  SetResourceString(@SXPStyleActionBars, 'XP Style');
  SetResourceString(@SActionBarStyleMissing, 'No ActionBand style unit present in the uses clause.'#13 +
    'Your application must include either XPStyleActnCtrls, StdStyleActnCtrls or ' +
    'a third party ActionBand style unit in its uses clause');
  SetResourceString(@sParameterCannotBeNil, '%s parameter in call to %s cannot be nil');
  SetResourceString(@SInvalidColorString, 'Invalid Color string');
  SetResourceString(@SActionManagerNotAssigned, '%s ActionManager property has not been assigned');

  SetResourceString(@SInvalidPath, '"%s" is an invalid path');
  SetResourceString(@SInvalidPathCaption, 'Invalid path');

  SetResourceString(@SANSIEncoding, 'ANSI');
  SetResourceString(@SASCIIEncoding, 'ASCII');
  SetResourceString(@SUnicodeEncoding, 'Unicode');
  SetResourceString(@SBigEndianEncoding, 'Big Endian Unicode');
  SetResourceString(@SUTF8Encoding, 'UTF-8');
  SetResourceString(@SUTF7Encoding, 'UTF-7');
  SetResourceString(@SEncodingLabel, 'Encoding:');

  SetResourceString(@sCannotAddFixedSize, 'Cannot add columns or rows while expand style is fixed size');
  SetResourceString(@sInvalidSpan, '''%d'' is not a valid span');
  SetResourceString(@sInvalidRowIndex, 'Row index, %d, out of bounds');
  SetResourceString(@sInvalidColumnIndex, 'Column index, %d, out of bounds');
  SetResourceString(@sInvalidControlItem, 'ControlItem.Control cannot be set to owning GridPanel');
  SetResourceString(@sCannotDeleteColumn, 'Cannot delete a column that contains controls');
  SetResourceString(@sCannotDeleteRow, 'Cannot delete a row that contains controls');
  SetResourceString(@sCellMember, 'Member');
  SetResourceString(@sCellSizeType, 'Size Type');
  SetResourceString(@sCellValue, 'Value');
  SetResourceString(@sCellAutoSize, 'Auto');
  SetResourceString(@sCellPercentSize, 'Percent');
  SetResourceString(@sCellAbsoluteSize, 'Absolute');
  SetResourceString(@sCellColumn, 'Column%d');
  SetResourceString(@sCellRow, 'Row%d');

  SetResourceString(@STrayIconRemoveError, 'Cannot remove shell notification icon');
  SetResourceString(@STrayIconCreateError, 'Cannot create shell notification icon');

  SetResourceString(@SPageControlNotSet, 'PageControl must first be assigned');

  SetResourceString(@SWindowsVistaRequired, '%s requires Windows Vista or later');
  SetResourceString(@SXPThemesRequired, '%s requires themes to be enabled');

  SetResourceString(@STaskDlgButtonCaption, 'Button%d');
  SetResourceString(@STaskDlgRadioButtonCaption, 'RadioButton%d');
  SetResourceString(@SInvalidTaskDlgButtonCaption, 'Caption cannot be empty');

  SetResourceString(@SInvalidCategoryPanelParent, 'CategoryPanel must have a CategoryPanelGroup as its parent');
  SetResourceString(@SInvalidCategoryPanelGroupChild, 'Only CategoryPanels can be inserted into a CategoryPanelGroup');

  SetResourceString(@SInvalidCanvasOperation, 'Invalid canvas operation');
  SetResourceString(@SNoOwner, '%s has no owner');
  SetResourceString(@SRequireSameOwner, 'Source and destination require the same owner');
  SetResourceString(@SDirect2DInvalidOwner, '%s cannot be owned by a different canvas');
  SetResourceString(@SDirect2DInvalidSolidBrush, 'Not a solid color brush');
  SetResourceString(@SDirect2DInvalidBrushStyle, 'Invalid brush style');

  SetResourceString(@SKeyboardLocaleInfo, 'Error retrieving locale information');
  SetResourceString(@SKeyboardLangChange, 'Failed to change input language');

  SetResourceString(@SOnlyWinControls, 'You can only tab dock TWinControl based Controls');

  SetResourceString(@SNoKeyword, 'No help keyword specified.');

  // ColorToPrettyName strings
  SetResourceString(@SNameBlack, 'Black');
  SetResourceString(@SNameMaroon, 'Maroon');
  SetResourceString(@SNameGreen, 'Green');
  SetResourceString(@SNameOlive, 'Olive');
  SetResourceString(@SNameNavy, 'Navy');
  SetResourceString(@SNamePurple, 'Purple');
  SetResourceString(@SNameTeal, 'Teal');
  SetResourceString(@SNameGray, 'Gray');
  SetResourceString(@SNameSilver, 'Silver');
  SetResourceString(@SNameRed, 'Red');
  SetResourceString(@SNameLime, 'Lime');
  SetResourceString(@SNameYellow, 'Yellow');
  SetResourceString(@SNameBlue, 'Blue');
  SetResourceString(@SNameFuchsia, 'Fuchsia');
  SetResourceString(@SNameAqua, 'Aqua');
  SetResourceString(@SNameWhite, 'White');
  SetResourceString(@SNameMoneyGreen, 'Money Green');
  SetResourceString(@SNameSkyBlue, 'Sky Blue');
  SetResourceString(@SNameCream, 'Cream');
  SetResourceString(@SNameMedGray, 'Medium Gray');
  SetResourceString(@SNameActiveBorder, 'Active Border');
  SetResourceString(@SNameActiveCaption, 'Active Caption');
  SetResourceString(@SNameAppWorkSpace, 'Application Workspace');
  SetResourceString(@SNameBackground, 'Background');
  SetResourceString(@SNameBtnFace, 'Button Face');
  SetResourceString(@SNameBtnHighlight, 'Button Highlight');
  SetResourceString(@SNameBtnShadow, 'Button Shadow');
  SetResourceString(@SNameBtnText, 'Button Text');
  SetResourceString(@SNameCaptionText, 'Caption Text');
  SetResourceString(@SNameDefault, 'Default');
  SetResourceString(@SNameGradientActiveCaption, 'Gradient Active Caption');
  SetResourceString(@SNameGradientInactiveCaption, 'Gradient Inactive Caption');
  SetResourceString(@SNameGrayText, 'Gray Text');
  SetResourceString(@SNameHighlight, 'Highlight Background');
  SetResourceString(@SNameHighlightText, 'Highlight Text');
  SetResourceString(@SNameHotLight, 'Hot Light');
  SetResourceString(@SNameInactiveBorder, 'Inactive Border');
  SetResourceString(@SNameInactiveCaption, 'Inactive Caption');
  SetResourceString(@SNameInactiveCaptionText, 'Inactive Caption Text');
  SetResourceString(@SNameInfoBk, 'Info Background');
  SetResourceString(@SNameInfoText, 'Info Text');
  SetResourceString(@SNameMenu, 'Menu Background');
  SetResourceString(@SNameMenuBar, 'Menu Bar');
  SetResourceString(@SNameMenuHighlight, 'Menu Highlight');
  SetResourceString(@SNameMenuText, 'Menu Text');
  SetResourceString(@SNameNone, 'None');
  SetResourceString(@SNameScrollBar, 'Scroll Bar');
  SetResourceString(@SName3DDkShadow, '3D Dark Shadow');
  SetResourceString(@SName3DLight, '3D Light');
  SetResourceString(@SNameWindow, 'Window Background');
  SetResourceString(@SNameWindowFrame, 'Window Frame');
  SetResourceString(@SNameWindowText, 'Window Text');}
end;

initialization

TcxTranslate.Translate_PTBR;
TDelphiTranslate.TranslateDelphiDBMessages_PTBR;

end.
