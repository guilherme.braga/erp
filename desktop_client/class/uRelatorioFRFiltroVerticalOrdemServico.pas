unit uRelatorioFRFiltroVerticalOrdemServico;

interface

Uses uFrameFiltroVerticalPadrao;

type TRelatorioFRFiltroVerticalOrdemServico = class
  class procedure CriarFiltrosRelatorioOrdemServico(var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
end;

implementation

uses uOrdemServicoProxy;

class procedure TRelatorioFRFiltroVerticalOrdemServico.CriarFiltrosRelatorioOrdemServico(
  var AFiltroVerticalPadrao: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //C�digo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM ORDEM_SERVICO.DH_CADASTRO)';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Valor de Ordem Servico
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_TOTAL_LIQUIDO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TOrdemServicoProxy.LISTA_STATUS;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);

  //Data de Fechamento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Fechamento';
  campoFiltro.campo := 'DH_FECHAMENTO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM ORDEM_SERVICO.DH_FECHAMENTO)';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVerticalPadrao.FCampos.Add(campoFiltro);
end;

end.
