unit uCEP;

interface

Uses uCEPProxy;

type TCEP = class
  class function GetLogradouroPeloCEP(const AIdCEP: String): TCEPProxy;
end;

implementation

uses uClientClasses, uDmConnection, REST.JSON;

class function TCEP.GetLogradouroPeloCEP(const AIdCEP: String): TCEPProxy;
var
  smCEP : TSMLocalidadeClient;
begin
  smCEP := TSMLocalidadeClient.Create(DmConnection.Connection.DBXConnection);
  try
    result := TJSON.JsonToObject<TCEPProxy>(smCEP.GetLogradouroPeloCEP(AIdCEP));
  finally
    smCEP.Free;
  end;
end;

end.
