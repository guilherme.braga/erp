unit uTControl_Function;

interface

Uses
  //System
  Forms,
  Classes,
  SysUtils,
  Messages,
  Dialogs,
  ComCtrls,
  Graphics,
  Variants,
  ActnList,
  Menus,
  Controls,
  cxPC,
  Windows,
  cxImage,

  uTVariables,
  uTMessage;

type TControlFunction = class
  public
  //Devolve a Instancia de um formul�rio
  class function createForm(pForm :TPersistentClass) :TForm;

  class function GetOwnerPage(const cClassName: String): TcxTabSheet;

  //Instancia um Formul�rio e a TTabSheet dona dele
  class function addPage(cClassName: String; ACheckPage: Boolean = true): TForm;

  //Verifica se o Formul�rio ja est� instanciado em alguma page
  class Function checkPages(cClassName: String) :Boolean;

  //Seta as configura��es padr�es da Page e Form criados
  class procedure setDefaultConfigurations(form: TForm; tabSheet: TcxTabSheet; cClassName: String);

  //Desabilita as demais p�ginas e deixa apenas a p�gina enviada habilitada
  class procedure setMainControlEnabled(const value: Boolean; ts: TWinControl);

  class procedure SetMainBarEnabled(const value: Boolean);

  //Incializa as variaveis globais
  class procedure setInitialVariables;

  //Retorno do GUID
  class function GetGUID: String;

  class function CreateMDIForm(const AClassName: String; const AIdentificadorMultiplaInstancia: String = ''): TForm;
  class procedure CreateMDIChild(AClass: TComponentClass; var AReference);

  class procedure SaveInstance(AIdentifier: String; AFormInstance: TForm);
  class function GetInstance(AIdentifier: String): TForm;
  class function ExisteInstancia(AIdentifier: String): Boolean;
  class procedure RemoveInstance(AIdentifier: String);
  class procedure ExibirImagemPrincipal(AOcultar: Boolean = false);

  class function GetVersion(sFileName:string): string;
end;

implementation

var tentativasReabrirFormulario: Integer;

class function TControlFunction.GetVersion(sFileName:string): string;
var
  VerInfoSize  : DWORD;
  VerInfo      : Pointer;
  VerValueSize : DWORD;
  VerValue     : PVSFixedFileInfo;
  Dummy        : DWORD;
begin
  try
    VerInfoSize := GetFileVersionInfoSize(PChar(sFileName), Dummy);
    GetMem(VerInfo, VerInfoSize);
    GetFileVersionInfo(PChar(sFileName), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
    with VerValue^ do
    begin
      Result := IntToStr(dwFileVersionMS shr 16);
      Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
      Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
      Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
    end;
    FreeMem(VerInfo, VerInfoSize);
  except //se ocorrer um erro retorna a vers�o 1.0.0.0, vai ocoorer um erro se o arquivo n�o tiver vers�o
    Result:='Sem Vers�o';
  end;
end;

class function TControlFunction.getGUID: String;
var guid: TGUID;
    strGuid: String;
begin
  CreateGUID(guid);
  strGuid := GUIDToString(guid);
  strGuid := StringReplace(strGuid, '{', '', [rfReplaceAll]);
  strGuid := StringReplace(strGuid, '}', '', [rfReplaceAll]);
  strGuid := StringReplace(strGuid, '-', '_', [rfReplaceAll]);
  result := strGuid;
end;

class function TControlFunction.GetInstance(AIdentifier: String): TForm;
begin
  if TControlFunction.ExisteInstancia(AIdentifier) then
    result := Variables.formInstances.Items[AIdentifier]
  else
    result := nil;
end;

class procedure TControlFunction.setInitialVariables;
begin
  Variables.cPathDesign := ExtractFileDir(Application.ExeName)+'\design';
  Variables.cPathSystem := ExtractFileDir(Application.ExeName);
end;

class procedure TControlFunction.SetMainBarEnabled(const value: Boolean);
begin
  Variables.barMainSystem.Visible := value;
end;

//Seta a visibilidade e a habilita��o das p�ginas quando a consulta estiver ativa
class procedure TControlFunction.setMainControlEnabled(const value: Boolean; ts: TWinControl);
var i: Integer;
begin
  for i := 0 to Pred(Variables.pcMainSystem.PageCount) do
    if Variables.pcMainSystem.Pages[i].Name <> TcxTabSheet(ts).Name then
      Variables.pcMainSystem.Pages[i].Enabled := value;

  if value then
    Variables.pcMainSystem.ActivePage := Variables.tbLastTabSheet;
end;

class function TControlFunction.createForm(pForm :TPersistentClass): TForm;
Var frm: TForm;
Begin
  frm := (TComponentClass( pForm ).Create( Application ) As TForm);
  Result := frm;
End;

class procedure TControlFunction.CreateMDIChild(AClass: TComponentClass;
  var AReference);
begin
  try
    Application.CreateForm(AClass, AReference);
    TForm(AReference).ShowModal;
  finally
    FreeAndNil(AReference);
  end;
end;

class function TControlFunction.CreateMDIForm(const AClassName: String;
  const AIdentificadorMultiplaInstancia: String): TForm;
begin
  if not ExisteInstancia(AClassName+AIdentificadorMultiplaInstancia) then
  begin
    try
      if Variables.formInstances.Count > 0 then
      begin
        Variables.classeDoFormularioAntivoAnteriormente := Variables.classeDoFormularioAtivo;
      end;

      TControlFunction.SaveInstance(AClassName+AIdentificadorMultiplaInstancia,
        TComponentClass(GetClass(AClassName)).Create(Application) as TForm);

      Variables.classeDoFormularioAtivo := AClassName;

      result := TControlFunction.GetInstance(AClassName+AIdentificadorMultiplaInstancia);
      result.Show;
      tentativasReabrirFormulario := 0;
    except
      on e: exception do
      begin
        Inc(tentativasReabrirFormulario);
        if tentativasReabrirFormulario < 3 then
        begin
          CreateMDIForm(AClassName, AIdentificadorMultiplaInstancia);
        end
        else
        begin
          TMessage.MessageFailure(e.message);
        end;
      end;
    end;
  end
  else
  begin
    result := GetInstance(AClassName+AIdentificadorMultiplaInstancia);
    result.Show;
  end;
end;

class function TControlFunction.GetOwnerPage(const cClassName: String): TcxTabSheet;
var i :Integer;
Begin
  Result := nil;

  if (Variables.pcMainSystem.PageCount = 0) Then Exit;

  for i := 0 to Pred(Variables.pcMainSystem.PageCount) do
    if (Variables.pcMainSystem.Pages[i].Name = 'ts'+cClassName) then
      result := Variables.pcMainSystem.Pages[i];
end;

class procedure TControlFunction.RemoveInstance(AIdentifier: String);
begin
  Variables.formInstances.Remove(AIdentifier);

  if Variables.formInstances.Count = 1 then
  begin
    Variables.classeDoFormularioAntivoAnteriormente := '';
  end
  else if Variables.formInstances.Count = 0 then
  begin
    Variables.classeDoFormularioAtivo := '';
  end;

  ExibirImagemPrincipal();
end;

class procedure TControlFunction.SaveInstance(AIdentifier: String;
  AFormInstance: TForm);
begin
  ExibirImagemPrincipal(true);
  Variables.formInstances.Add(AIdentifier, AFormInstance);
end;

class procedure TControlFunction.ExibirImagemPrincipal(AOcultar: Boolean = false);
begin
  if AOcultar then
  begin
    TcxImage(Variables.frmMain.FindComponent('MainImage')).Visible := false;
    Application.processmessages;
  end
  else if Variables.formInstances.Count = 0 then
  begin
    TcxImage(Variables.frmMain.FindComponent('MainImage')).Visible := true;
    Application.processmessages;
  end;
end;

class function TControlFunction.ExisteInstancia(AIdentifier: String): Boolean;
begin
  result := Variables.formInstances.ContainsKey(AIdentifier);
end;

class function TControlFunction.checkPages(cClassName: String) :Boolean;
var i :Integer;
Begin
  Result := True;

  if (Variables.pcMainSystem.PageCount = 0) Then Exit;

  for i := 0 to Pred(Variables.pcMainSystem.PageCount) do
    if Pos('ts'+cClassName, Variables.pcMainSystem.Pages[i].Name) > 0 then
      Begin
        Variables.pcMainSystem.ActivePageIndex := i;
        Result := False;
        break;
      End;
end;

class function TControlFunction.addPage(cClassName: String; ACheckPage: Boolean = true): TForm;
Var
  TabSheet :TcxTabSheet;
  Form :TForm;
Begin
  try
    result := nil;

    if ACheckPage then
      if not(checkPages(cClassName)) then Exit;

    PostMessage(Variables.hfrmMain, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
    Application.ProcessMessages;

    Variables.tbLastTabSheet := Variables.pcMainSystem.ActivePage;

    form := TComponentClass(GetClass(cClassName)).Create(Application) As TForm;
    tabSheet := TcxTabSheet.Create(Variables.pcMainSystem);
    setDefaultConfigurations(form, tabSheet, cClassName);

    form.Show;
    result := form;
  except
    TMessage.MessageFailureAddPage();
  end;
End;

class procedure TControlFunction.setDefaultConfigurations(form: TForm; tabSheet: TcxTabSheet; cClassName: String);
begin
  With TabSheet Do
    Begin
      PageControl := Variables.pcMainSystem;
      Caption     := Form.Caption;
      Parent      := Variables.pcMainSystem;
      Margins.Top := 0;
      Name        := 'ts'+cClassName+TControlFunction.getGUID;
      tag         := Form.Handle;
    End;

  Variables.pcMainSystem.ActivePage := TabSheet;

  with Form do
    begin
      BorderStyle := bsNone;
      Height := TabSheet.Height;
      Width := TabSheet.Width;
      parent := TabSheet;
    end;
end;

initialization
  tentativasReabrirFormulario := 0;

End.
