program PrDesktop_Client;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uDmAcesso in 'modules\uDmAcesso.pas' {DmAcesso: TDataModule},
  uDmConnection in 'modules\uDmConnection.pas' {DmConnection: TDataModule},
  uFrmCadastro_Padrao in 'inheritable\uFrmCadastro_Padrao.pas' {FrmCadastro_Padrao},
  Vcl.Themes,
  Vcl.Styles,
  uTControl_Function in 'class\uTControl_Function.pas',
  uTMessage in 'class\uTMessage.pas',
  uTTranslate in 'class\uTTranslate.pas',
  uTVariables in 'class\uTVariables.pas',
  uFrmChildPadrao in 'inheritable\uFrmChildPadrao.pas' {FrmChildPadrao},
  uFrmModalPadrao in 'inheritable\uFrmModalPadrao.pas' {FrmModalPadrao},
  uTUsuario in 'class\uTUsuario.pas',
  uTPessoa in 'class\uTPessoa.pas',
  uTFunction in 'class\uTFunction.pas',
  uPlanoConta in 'class\uPlanoConta.pas',
  uStringUtils in 'class\uStringUtils.pas',
  uDatasetUtils in 'class\uDatasetUtils.pas',
  uContaCorrente in 'class\uContaCorrente.pas',
  uFiltroFormulario in 'class\uFiltroFormulario.pas',
  uProduto in 'class\uProduto.pas',
  uTFTP in 'class\uTFTP.pas',
  uControlsUtils in 'class\uControlsUtils.pas',
  uIniFileClient in 'class\uIniFileClient.pas',
  uDevExpressUtils in 'class\uDevExpressUtils.pas',
  uDateUtils in 'class\uDateUtils.pas',
  uGeracaoDocumento in 'class\uGeracaoDocumento.pas',
  uNotaFiscal in 'class\uNotaFiscal.pas',
  uContaPagar in 'class\uContaPagar.pas',
  uContaReceber in 'class\uContaReceber.pas',
  uMathUtils in 'class\uMathUtils.pas',
  uChaveProcesso in 'class\uChaveProcesso.pas',
  uPlanoPagamento in 'class\uPlanoPagamento.pas',
  uSistema in 'class\uSistema.pas',
  uFilial in 'class\uFilial.pas',
  uCadAcao in 'forms\uCadAcao.pas' {CadAcao},
  uCadCaixa in 'forms\uCadCaixa.pas' {CadCaixa},
  uCadCategoriaProduto in 'forms\uCadCategoriaProduto.pas' {CadCategoriaProduto},
  uCadCentroResultado in 'forms\uCadCentroResultado.pas' {CadCentroResultado},
  uCadCidade in 'forms\uCadCidade.pas' {CadCidade},
  uCadContaAnalise in 'forms\uCadContaAnalise.pas' {CadContaAnalise},
  uCadContaCorrente in 'forms\uCadContaCorrente.pas' {CadContaCorrente},
  uCadEmpresaFilial in 'forms\uCadEmpresaFilial.pas' {CadEmpresaFilial},
  uCadEstacao in 'forms\uCadEstacao.pas' {CadEstacao},
  uCadEstado in 'forms\uCadEstado.pas' {CadEstado},
  uCadEstruturaPlanoConta in 'forms\uCadEstruturaPlanoConta.pas' {CadEstruturaPlanoConta},
  uCadFormaPagamento in 'forms\uCadFormaPagamento.pas' {CadFormaPagamento},
  uCadGrupoCentroResultado in 'forms\uCadGrupoCentroResultado.pas' {CadGrupoCentroResultado},
  uCadGrupoProduto in 'forms\uCadGrupoProduto.pas' {CadGrupoProduto},
  uCadLinhaProduto in 'forms\uCadLinhaProduto.pas' {CadLinhaProduto},
  uCadMarcaProduto in 'forms\uCadMarcaProduto.pas' {CadMarcaProduto},
  uCadMidia in 'forms\uCadMidia.pas' {CadMidia},
  uCadModeloNotaFiscal in 'forms\uCadModeloNotaFiscal.pas' {CadModeloNotaFiscal},
  uCadModeloProduto in 'forms\uCadModeloProduto.pas' {CadModeloProduto},
  uCadOperacao in 'forms\uCadOperacao.pas' {CadOperacao},
  uCadPais in 'forms\uCadPais.pas' {CadPais},
  uCadPesquisaPadrao in 'forms\uCadPesquisaPadrao.pas' {CadPesquisaPadrao},
  uCadPessoa in 'forms\uCadPessoa.pas' {CadPessoa},
  uCadPlanoConta in 'forms\uCadPlanoConta.pas' {CadPlanoConta},
  uCadPlanoPagamento in 'forms\uCadPlanoPagamento.pas' {CadPlanoPagamento},
  uCadProduto in 'forms\uCadProduto.pas' {CadProduto},
  uCadSubCategoriaProduto in 'forms\uCadSubCategoriaProduto.pas' {CadSubCategoriaProduto},
  uCadSubGrupoProduto in 'forms\uCadSubGrupoProduto.pas' {CadSubGrupoProduto},
  uCadUnidadeEstoqueProduto in 'forms\uCadUnidadeEstoqueProduto.pas' {CadUnidadeEstoqueProduto},
  uCadUsuario in 'forms\uCadUsuario.pas' {CadUsuario},
  uFrmAlterarSQLPesquisaPadrao in 'forms\uFrmAlterarSQLPesquisaPadrao.pas' {FrmAlterarSQLPesquisaPadrao},
  uFrmApelidarColunasGrid in 'forms\uFrmApelidarColunasGrid.pas' {FrmApelidarColunasGrid},
  uFrmConfiguracaoValoresPadroes in 'forms\uFrmConfiguracaoValoresPadroes.pas' {FrmConfigurarValoresPadroes},
  uFrmConsultaPadrao in 'forms\uFrmConsultaPadrao.pas' {FrmConsultaPadrao},
  uFrmMessage in 'forms\uFrmMessage.pas' {FrmMessage},
  uFrmMessage_Process in 'forms\uFrmMessage_Process.pas' {FrmMessage_Process},
  uFrmPrincipal in 'forms\uFrmPrincipal.pas' {FrmPrincipal},
  uMovContaCorrenteMovimento in 'forms\uMovContaCorrenteMovimento.pas' {MovContaCorrenteMovimento},
  uMovContaPagar in 'forms\uMovContaPagar.pas' {MovContaPagar},
  uMovContaReceber in 'forms\uMovContaReceber.pas' {MovContaReceber},
  uMovGeracaoDocumento in 'forms\uMovGeracaoDocumento.pas' {MovGeracaoDocumento},
  uMovGerenciamentoEstacao in 'forms\uMovGerenciamentoEstacao.pas' {MovGerenciamentoEstacao},
  uMovNotaFiscal in 'forms\uMovNotaFiscal.pas' {MovNotaFiscal},
  uMovTabelaPreco in 'forms\uMovTabelaPreco.pas' {MovTabelaPreco},
  uPesqContaAnalise in 'forms\uPesqContaAnalise.pas' {PesqContaAnalise},
  uRelExtratoPlanoConta in 'forms\uRelExtratoPlanoConta.pas' {RelExtratoPlanoConta},
  uFrameDetailPadrao in 'frames\uFrameDetailPadrao.pas' {FrameDetailPadrao: TFrame},
  uFrameGerenciamentoEstacaoMidia in 'frames\uFrameGerenciamentoEstacaoMidia.pas' {FrameGerenciamentoEstacaoMidia: TFrame},
  uFramePessoaContato in 'frames\uFramePessoaContato.pas' {FramePessoaContato: TFrame},
  uFrameProdutoCodigoBarra in 'frames\uFrameProdutoCodigoBarra.pas' {FrameProdutoCodigoBarra: TFrame},
  uFrameProdutoFornecedor in 'frames\uFrameProdutoFornecedor.pas' {FrameProdutoFornecedor: TFrame},
  uFrameQuitacao in 'frames\uFrameQuitacao.pas' {FrameQuitacao: TFrame},
  uFrameTabelaPrecoItem in 'frames\uFrameTabelaPrecoItem.pas' {FrameTabelaPrecoItem: TFrame},
  uFrameOperacaoItem in 'frames\uFrameOperacaoItem.pas' {FrameOperacaoItem: TFrame},
  uMovNotaFiscalManutencaoTabelaPreco in 'forms\uMovNotaFiscalManutencaoTabelaPreco.pas' {MovNotaFiscalManutencaoTabelaPreco},
  uFrameQuitacaoContaPagar in 'frames\uFrameQuitacaoContaPagar.pas' {FrameQuitacaoContaPagar: TFrame},
  uFrameQuitacaoContaReceber in 'frames\uFrameQuitacaoContaReceber.pas' {FrameQuitacaoContaReceber: TFrame},
  uMovVendaVarejo in 'forms\uMovVendaVarejo.pas' {MovVendaVarejo},
  uVenda in 'class\uVenda.pas',
  uFrmConsultaDadosPadrao in 'inheritable\uFrmConsultaDadosPadrao.pas' {FrmConsultaDadosPadrao},
  uFrmLogin in 'forms\uFrmLogin.pas' {FrmLogin},
  uFormaPagamento in 'class\uFormaPagamento.pas',
  uTabelaPreco in 'class\uTabelaPreco.pas',
  uMovVendaVarejoRecebimento in 'forms\uMovVendaVarejoRecebimento.pas' {MovVendaVarejoRecebimento},
  uCadRelatorioFR in 'forms\uCadRelatorioFR.pas' {CadRelatorioFR},
  uFrameRelatorioFRFormulario in 'frames\uFrameRelatorioFRFormulario.pas' {FrameRelatorioFRFormulario: TFrame},
  uFrmModalComMenuPadrao in 'inheritable\uFrmModalComMenuPadrao.pas' {FrmModalComMenuPadrao},
  uFrmVisualizacaoRelatoriosAssociados in 'forms\uFrmVisualizacaoRelatoriosAssociados.pas' {FrmVisualizacaoRelatoriosAssociados},
  uRelatorioFR in 'class\uRelatorioFR.pas',
  uFrmRelatorioFRPadrao in 'inheritable\uFrmRelatorioFRPadrao.pas' {FrmRelatorioFRPadrao},
  uVerticalGridUtils in 'class\uVerticalGridUtils.pas',
  uMovVendaVarejoTouch in 'forms\uMovVendaVarejoTouch.pas' {MovVendaVarejoTouch},
  uFrmTecladoCompleto in 'inheritable\uFrmTecladoCompleto.pas' {FrmTecladoCompleto},
  uFrmTecladoNumerico in 'inheritable\uFrmTecladoNumerico.pas' {FrmTecladoNumerico},
  uFrmInputNumber in 'forms\uFrmInputNumber.pas' {Util_Input_Number},
  uFrmInputString in 'forms\uFrmInputString.pas' {FrmInputString},
  uImpressaoMP2032 in 'class\uImpressaoMP2032.pas',
  uDLLMP2032 in 'class\uDLLMP2032.pas',
  uFrameNotaFiscalItemRapido in 'frames\uFrameNotaFiscalItemRapido.pas' {FrameNotaFiscalItemRapido: TFrame},
  uUsuarioDesignControl in 'class\uUsuarioDesignControl.pas',
  uFrmChildClean in 'inheritable\uFrmChildClean.pas' {FrmChildClean},
  uCadTipoQuitacao in 'forms\uCadTipoQuitacao.pas' {CadTipoQuitacao},
  uCadCheque in 'forms\uCadCheque.pas' {CadCheque},
  uRelEstoque in 'forms\uRelEstoque.pas' {RelEstoque},
  uMovAjusteEstoque in 'forms\uMovAjusteEstoque.pas' {MovAjusteEstoque},
  uMotivo in 'class\uMotivo.pas',
  uAjusteEstoque in 'class\uAjusteEstoque.pas',
  uFrameAjusteEstoqueItem in 'frames\uFrameAjusteEstoqueItem.pas' {FrameAjusteEstoqueItem: TFrame},
  uCadMotivo in 'forms\uCadMotivo.pas' {CadMotivo},
  uRelContaReceber in 'forms\uRelContaReceber.pas' {RelContaReceber},
  uRelContaPagar in 'forms\uRelContaPagar.pas' {RelContaPagar},
  uFrameQuitacaoLotePagamento in 'frames\uFrameQuitacaoLotePagamento.pas' {FrameQuitacaoLotePagamento: TFrame},
  uCadEquipamento in 'forms\uCadEquipamento.pas' {CadEquipamento},
  uCadServico in 'forms\uCadServico.pas' {CadServico},
  uCadChecklist in 'forms\uCadChecklist.pas' {CadChecklist},
  uCadSituacao in 'forms\uCadSituacao.pas' {CadSituacao},
  uOrdemServico in 'class\uOrdemServico.pas',
  uChecklist in 'class\uChecklist.pas',
  uFrameChecklistItem in 'frames\uFrameChecklistItem.pas' {FrameChecklistItem: TFrame},
  uRelOrdemServico in 'forms\uRelOrdemServico.pas' {RelOrdemServico},
  uMovLotePagamento in 'forms\uMovLotePagamento.pas' {MovLotePagamento},
  uLotePagamento in 'class\uLotePagamento.pas',
  uMovLoteRecebimento in 'forms\uMovLoteRecebimento.pas' {MovLoteRecebimento},
  uFrameQuitacaoLoteRecebimento in 'frames\uFrameQuitacaoLoteRecebimento.pas' {FrameQuitacaoLoteRecebimento: TFrame},
  uLoteRecebimento in 'class\uLoteRecebimento.pas',
  uFrmAutenticacaoUsuario in 'forms\uFrmAutenticacaoUsuario.pas' {FrmAutenticacaoUsuario},
  uPesqProduto in 'forms\uPesqProduto.pas' {PesqProduto},
  uCadVeiculo in 'forms\uCadVeiculo.pas' {CadVeiculo},
  uCadCor in 'forms\uCadCor.pas' {CadCor},
  uCadOcupacao in 'forms\uCadOcupacao.pas' {CadOcupacao},
  uCadAreaAtuacao in 'forms\uCadAreaAtuacao.pas' {CadAreaAtuacao},
  uCadClassificacaoEmpresa in 'forms\uCadClassificacaoEmpresa.pas' {CadClassificacaoEmpresa},
  uCadCNAE in 'forms\uCadCNAE.pas' {CadCNAE},
  uFramePessoaRepresentante in 'frames\uFramePessoaRepresentante.pas' {FramePessoaRepresentante: TFrame},
  uFramePessoaSetor in 'frames\uFramePessoaSetor.pas' {FramePessoaSetor: TFrame},
  uFramePessoaAtividadeSecundaria in 'frames\uFramePessoaAtividadeSecundaria.pas' {FramePessoaAtividadeSecundaria: TFrame},
  uFramePessoaEndereco in 'frames\uFramePessoaEndereco.pas' {FramePessoaEndereco: TFrame},
  uFrameDetailTransientePadrao in 'frames\uFrameDetailTransientePadrao.pas' {FrameDetailTransientePadrao: TFrame},
  uMovOrdemServico in 'forms\uMovOrdemServico.pas',
  uFrameOrdemServicoProduto in 'frames\uFrameOrdemServicoProduto.pas' {FrameOrdemServicoProduto: TFrame},
  uConstantes in 'class\uConstantes.pas',
  uFramePadrao in 'frames\uFramePadrao.pas' {FramePadrao: TFrame},
  uFrameFechamentoCrediario in 'frames\uFrameFechamentoCrediario.pas' {FrameFechamentoCrediario: TFrame},
  uFrameTipoQuitacaoCrediario in 'frames\uFrameTipoQuitacaoCrediario.pas' {FrameTipoQuitacaoCrediario: TFrame},
  uFrameTipoQuitacaoCartao in 'frames\uFrameTipoQuitacaoCartao.pas' {FrameTipoQuitacaoCartao: TFrame},
  uFrameTipoQuitacaoCheque in 'frames\uFrameTipoQuitacaoCheque.pas' {FrameTipoQuitacaoCheque: TFrame},
  uFechamentoCrediario in 'class\uFechamentoCrediario.pas',
  uCadOperadoraCartao in 'forms\uCadOperadoraCartao.pas' {CadOperadoraCartao},
  uCadSetorComercial in 'forms\uCadSetorComercial.pas' {CadSetorComercial},
  uCadBairro in 'forms\uCadBairro.pas' {CadBairro},
  uCadCep in 'forms\uCadCep.pas' {CadCep},
  uFrmParametrosFormulario in 'forms\uFrmParametrosFormulario.pas' {FrmParametrosFormulario},
  uPessoaCredito in 'class\uPessoaCredito.pas',
  uConstParametroFormulario in 'class\uConstParametroFormulario.pas',
  uFrmInformacaoGeral in 'forms\uFrmInformacaoGeral.pas' {FrmInformacaoGeral},
  uFrmHelpImplantacaoVendaVarejo in 'forms\uFrmHelpImplantacaoVendaVarejo.pas' {FrmHelpImplantacaoVendaVarejo},
  uFrameVendaVarejoFechamentoCrediario in 'frames\uFrameVendaVarejoFechamentoCrediario.pas' {FrameVendaVarejoFechamentoCrediario: TFrame},
  uAcbrUtils in 'class\uAcbrUtils.pas',
  uFrmHelpImplantacaoVendaTouch in 'forms\uFrmHelpImplantacaoVendaTouch.pas' {FrmHelpImplantacaoVendaTouch},
  uConsultaGrid in 'class\uConsultaGrid.pas',
  uFrameQuitacaoContaPagarTipoCartao in 'frames\uFrameQuitacaoContaPagarTipoCartao.pas' {FrameQuitacaoContaPagarTipoCartao: TFrame},
  uFrameQuitacaoContaPagarTipoCheque in 'frames\uFrameQuitacaoContaPagarTipoCheque.pas' {FrameQuitacaoContaPagarTipoCheque: TFrame},
  uFrameQuitacaoContaReceberTipoCartao in 'frames\uFrameQuitacaoContaReceberTipoCartao.pas' {FrameQuitacaoContaReceberTipoCartao: TFrame},
  uFrameQuitacaoContaReceberTipoCheque in 'frames\uFrameQuitacaoContaReceberTipoCheque.pas' {FrameQuitacaoContaReceberTipoCheque: TFrame},
  uFrameOrdemServicoFechamentoCrediario in 'frames\uFrameOrdemServicoFechamentoCrediario.pas' {FrameOrdemServicoFechamentoCrediario: TFrame},
  uFrameOrdemServicoServico in 'frames\uFrameOrdemServicoServico.pas' {FrameOrdemServicoServico: TFrame},
  uMovNegociacaoContaReceber in 'forms\uMovNegociacaoContaReceber.pas' {MovNegociacaoContaReceber},
  uServico in 'class\uServico.pas',
  uVeiculo in 'class\uVeiculo.pas',
  uFrmHelpImplantacaoOrdemServico in 'forms\uFrmHelpImplantacaoOrdemServico.pas' {FrmHelpImplantacaoOrdemServico},
  uCEP in 'class\uCEP.pas',
  uFrameQuitacaoLoteRecebimentoTipoCartao in 'frames\uFrameQuitacaoLoteRecebimentoTipoCartao.pas' {FrameQuitacaoLoteRecebimentoTipoCartao: TFrame},
  uFrameQuitacaoLoteRecebimentoTipoCheque in 'frames\uFrameQuitacaoLoteRecebimentoTipoCheque.pas' {FrameQuitacaoLoteRecebimentoTipoCheque: TFrame},
  uFrameQuitacaoLotePagamentoTipoCartao in 'frames\uFrameQuitacaoLotePagamentoTipoCartao.pas' {FrameQuitacaoLotePagamentoTipoCartao: TFrame},
  uFrameQuitacaoLotePagamentoTipoCheque in 'frames\uFrameQuitacaoLotePagamentoTipoCheque.pas' {FrameQuitacaoLotePagamentoTipoCheque: TFrame},
  uFrmEmissaoDocumentoFechamento in 'forms\uFrmEmissaoDocumentoFechamento.pas' {FrmEmissaoDocumentoFechamento},
  uCarteira in 'class\uCarteira.pas',
  uCadSituacaoEspecial in 'forms\uCadSituacaoEspecial.pas' {CadSituacaoEspecial},
  uCadZoneamento in 'forms\uCadZoneamento.pas' {CadZoneamento},
  uCadRegimeEspecial in 'forms\uCadRegimeEspecial.pas' {CadRegimeEspecial},
  uCadCSOSN in 'forms\uCadCSOSN.pas' {CadCSOSN},
  uCadNCM in 'forms\uCadNCM.pas' {CadNCM},
  uFrameNCMEspecializado in 'frames\uFrameNCMEspecializado.pas' {FrameNCMEspecializado: TFrame},
  uFrameEmpresaFilial in 'frames\uFrameEmpresaFilial.pas' {FrameEmpresaFilial: TFrame},
  uCadCarteira in 'forms\uCadCarteira.pas' {CadCarteira},
  uCadMedico in 'forms\uCadMedico.pas' {CadMedico},
  uCadReceitaOtica in 'forms\uCadReceitaOtica.pas' {CadReceitaOtica},
  uFrameVendaVarejoReceitaOtica in 'frames\uFrameVendaVarejoReceitaOtica.pas' {FrameVendaVarejoReceitaOtica: TFrame},
  uCadConfiguracoesFiscais in 'forms\uCadConfiguracoesFiscais.pas' {CadConfiguracoesFiscais},
  uAcaoProxy in 'proxy datasnap\uAcaoProxy.pas',
  uAjusteEstoqueProxy in 'proxy datasnap\uAjusteEstoqueProxy.pas',
  uCarteiraProxy in 'proxy datasnap\uCarteiraProxy.pas',
  uCentroResultadoProxy in 'proxy datasnap\uCentroResultadoProxy.pas',
  uCEPProxy in 'proxy datasnap\uCEPProxy.pas',
  uChaveProcessoProxy in 'proxy datasnap\uChaveProcessoProxy.pas',
  uChequeProxy in 'proxy datasnap\uChequeProxy.pas',
  uConfiguracaoFiscalProxy in 'proxy datasnap\uConfiguracaoFiscalProxy.pas',
  uContaAnaliseProxy in 'proxy datasnap\uContaAnaliseProxy.pas',
  uContaCorrenteProxy in 'proxy datasnap\uContaCorrenteProxy.pas',
  uContaPagarProxy in 'proxy datasnap\uContaPagarProxy.pas',
  uContaReceberProxy in 'proxy datasnap\uContaReceberProxy.pas',
  uCSTCSOSNProxy in 'proxy datasnap\uCSTCSOSNProxy.pas',
  uCSTIPIProxy in 'proxy datasnap\uCSTIPIProxy.pas',
  uCSTPisCofinsProxy in 'proxy datasnap\uCSTPisCofinsProxy.pas',
  uEmpresaProxy in 'proxy datasnap\uEmpresaProxy.pas',
  uFilialProxy in 'proxy datasnap\uFilialProxy.pas',
  uFormaPagamentoProxy in 'proxy datasnap\uFormaPagamentoProxy.pas',
  uGeracaoDocumentoProxy in 'proxy datasnap\uGeracaoDocumentoProxy.pas',
  uImpostoICMSProxy in 'proxy datasnap\uImpostoICMSProxy.pas',
  uImpostoIPIProxy in 'proxy datasnap\uImpostoIPIProxy.pas',
  uImpostoPisCofinsProxy in 'proxy datasnap\uImpostoPisCofinsProxy.pas',
  uLotePagamentoProxy in 'proxy datasnap\uLotePagamentoProxy.pas',
  uLoteRecebimentoProxy in 'proxy datasnap\uLoteRecebimentoProxy.pas',
  uNCMEspecializadoProxy in 'proxy datasnap\uNCMEspecializadoProxy.pas',
  uNCMProxy in 'proxy datasnap\uNCMProxy.pas',
  uOperadoraCartaoProxy in 'proxy datasnap\uOperadoraCartaoProxy.pas',
  uOrdemServicoProxy in 'proxy datasnap\uOrdemServicoProxy.pas',
  uPessoaCreditoProxy in 'proxy datasnap\uPessoaCreditoProxy.pas',
  uPessoaProdutoSituacaoEspecialProxy in 'proxy datasnap\uPessoaProdutoSituacaoEspecialProxy.pas',
  uPessoaProxy in 'proxy datasnap\uPessoaProxy.pas',
  uProdutoProxy in 'proxy datasnap\uProdutoProxy.pas',
  uRegimeEspecialProxy in 'proxy datasnap\uRegimeEspecialProxy.pas',
  uRegraImpostoFiltroProxy in 'proxy datasnap\uRegraImpostoFiltroProxy.pas',
  uRegraImpostoProxy in 'proxy datasnap\uRegraImpostoProxy.pas',
  uServicoProxy in 'proxy datasnap\uServicoProxy.pas',
  uSituacaoEspecialProxy in 'proxy datasnap\uSituacaoEspecialProxy.pas',
  uTabelaPrecoProxy in 'proxy datasnap\uTabelaPrecoProxy.pas',
  uTipoQuitacaoProxy in 'proxy datasnap\uTipoQuitacaoProxy.pas',
  uUsuarioProxy in 'proxy datasnap\uUsuarioProxy.pas',
  uVeiculoProxy in 'proxy datasnap\uVeiculoProxy.pas',
  uVendaProxy in 'proxy datasnap\uVendaProxy.pas',
  uZoneamentoProxy in 'proxy datasnap\uZoneamentoProxy.pas',
  uCadRegraImposto in 'forms\uCadRegraImposto.pas' {CadRegraImposto},
  uCadImpostoICMS in 'forms\uCadImpostoICMS.pas' {CadImpostoICMS},
  uCadImpostoIPI in 'forms\uCadImpostoIPI.pas' {CadImpostoIPI},
  uCadImpostoPISCofins in 'forms\uCadImpostoPISCofins.pas' {CadImpostoPISCofins},
  uCadCSTPISCofins in 'forms\uCadCSTPISCofins.pas' {CadCSTPISCofins},
  uCadCSTIPI in 'forms\uCadCSTIPI.pas' {CadCSTIPI},
  uFrameRegraImpostoFiltro in 'frames\uFrameRegraImpostoFiltro.pas' {FrameRegraImpostoFiltro: TFrame},
  uNotaFiscalProxy in 'proxy datasnap\uNotaFiscalProxy.pas',
  uMovInventario in 'forms\uMovInventario.pas' {MovInventario},
  uInventarioProxy in 'proxy datasnap\uInventarioProxy.pas',
  uInventario in 'class\uInventario.pas',
  uMovInventarioDefinirContagemValida in 'forms\uMovInventarioDefinirContagemValida.pas' {MovInventarioDefinirContagemValida},
  uReceitaOtica in 'class\uReceitaOtica.pas',
  uReceitaOticaProxy in 'proxy datasnap\uReceitaOticaProxy.pas',
  uFrmRelatorioFR in 'forms\uFrmRelatorioFR.pas' {FrmRelatorioFR},
  uFastReport in '..\library\uFastReport.pas',
  uMovVendaVarejoEmitirDocumento in 'forms\uMovVendaVarejoEmitirDocumento.pas' {MovVendaVarejoEmitirDocumento},
  uMovOrdemServicoEmitirDocumento in 'forms\uMovOrdemServicoEmitirDocumento.pas' {MovOrdemServicoEmitirDocumento},
  uFrmHelpImplantacaoContaReceber in 'forms\uFrmHelpImplantacaoContaReceber.pas' {FrmHelpImplantacaoContaReceber},
  uFrmHelpImplantacaoContaPagar in 'forms\uFrmHelpImplantacaoContaPagar.pas' {FrmHelpImplantacaoContaPagar},
  uFrmEmissaoEtiqueta in 'forms\uFrmEmissaoEtiqueta.pas' {FrmEmissaoEtiqueta},
  uFrameConsultaDadosDetalhe in 'frames\uFrameConsultaDadosDetalhe.pas' {FrameConsultaDadosDetalhe: TFrame},
  uFrameConsultaDadosDetalheGrade in 'frames\uFrameConsultaDadosDetalheGrade.pas' {FrameConsultaDadosDetalheGrade: TFrame},
  uFluxoCaixaResumido in 'class\uFluxoCaixaResumido.pas',
  uRelFluxoCaixaResumido in 'forms\uRelFluxoCaixaResumido.pas' {RelFluxoCaixaResumido},
  uImpressora in 'class\uImpressora.pas',
  uFrameFiltroVerticalPadrao in 'frames\uFrameFiltroVerticalPadrao.pas' {FrameFiltroVerticalPadrao: TFrame},
  uImpMalaDireta in 'forms\uImpMalaDireta.pas' {ImpMalaDireta},
  uFrmRelatorioFRFiltroVertical in 'forms\uFrmRelatorioFRFiltroVertical.pas',
  uRelatorioFRFiltroVertical in 'class\uRelatorioFRFiltroVertical.pas',
  uFrmDevolucaoValorParaCliente in 'forms\uFrmDevolucaoValorParaCliente.pas' {FrmDevolucaoValorParaCliente},
  uPesqCheque in 'forms\uPesqCheque.pas' {PesqCheque},
  uCheque in 'class\uCheque.pas',
  uMovMovimentacaoCheque in 'forms\uMovMovimentacaoCheque.pas' {MovMovimentacaoCheque},
  uMovimentacaoCheque in 'class\uMovimentacaoCheque.pas',
  uMovimentacaoChequeProxy in 'proxy datasnap\uMovimentacaoChequeProxy.pas',
  uCadMontadora in 'forms\uCadMontadora.pas' {CadMontadora},
  uCadModeloMontadora in 'forms\uCadModeloMontadora.pas' {CadModeloMontadora},
  uFrameProdutoMontadora in 'frames\uFrameProdutoMontadora.pas' {FrameProdutoMontadora: TFrame},
  uMontadoraProxy in 'proxy datasnap\uMontadoraProxy.pas',
  uMontadora in 'class\uMontadora.pas',
  uNegociacaoContaReceberProxy in 'proxy datasnap\uNegociacaoContaReceberProxy.pas',
  uFrameNegociacaoContaReceberTitulosParaNegociacao in 'frames\uFrameNegociacaoContaReceberTitulosParaNegociacao.pas' {FrameNegociacaoContaReceberTitulosParaNegociacao: TFrame},
  uCorProxy in 'proxy datasnap\uCorProxy.pas',
  uTamanhoProxy in 'proxy datasnap\uTamanhoProxy.pas',
  uProdutoGradeProxy in 'proxy datasnap\uProdutoGradeProxy.pas',
  uCadTamanho in 'forms\uCadTamanho.pas' {CadTamanho},
  uCadGradeProduto in 'forms\uCadGradeProduto.pas' {CadGradeProduto},
  uFrameGradeProduto in 'frames\uFrameGradeProduto.pas' {FrameGradeProduto: TFrame},
  uCor in 'class\uCor.pas',
  uTamanho in 'class\uTamanho.pas',
  uFrameProdutoGrade in 'frames\uFrameProdutoGrade.pas' {FrameProdutoGrade: TFrame},
  uFrmGerarProdutoGrade in 'forms\uFrmGerarProdutoGrade.pas' {FrmGerarProdutoGrade},
  uFrmBackup in 'forms\uFrmBackup.pas' {FrmBackup},
  uWindowsUtils in '..\library\uWindowsUtils.pas',
  uBackup in 'class\uBackup.pas',
  uCFOPProxy in 'proxy datasnap\uCFOPProxy.pas',
  uCadCFOP in 'forms\uCadCFOP.pas' {CadCFOP},
  uConstantesFiscaisProxy in 'proxy datasnap\uConstantesFiscaisProxy.pas',
  uCadNaturezaOperacao in 'forms\uCadNaturezaOperacao.pas' {CadNaturezaOperacao},
  uNaturezaOperacao in 'class\uNaturezaOperacao.pas',
  uNaturezaOperacaoProxy in 'proxy datasnap\uNaturezaOperacaoProxy.pas',
  uFrameOperacaoOperacaoFiscal in 'frames\uFrameOperacaoOperacaoFiscal.pas' {FrameOperacaoOperacaoFiscal: TFrame},
  uBackupProxy in 'proxy datasnap\uBackupProxy.pas',
  uFramePessoaContaReceber in 'frames\uFramePessoaContaReceber.pas' {FramePessoaContaReceber: TFrame},
  uEmissaoEtiqueta in 'class\uEmissaoEtiqueta.pas',
  uFramePessoaContaPagar in 'frames\uFramePessoaContaPagar.pas' {FramePessoaContaPagar: TFrame},
  uEquipamentoProxy in 'proxy datasnap\uEquipamentoProxy.pas',
  uEquipamento in 'class\uEquipamento.pas',
  uRelatorioFRFiltroVerticalVenda in 'class\uRelatorioFRFiltroVerticalVenda.pas',
  uRelatorioFRFiltroVerticalContaReceber in 'class\uRelatorioFRFiltroVerticalContaReceber.pas',
  uRelatorioFRFiltroVerticalContaPagar in 'class\uRelatorioFRFiltroVerticalContaPagar.pas',
  uRelatorioFRFiltroVerticalEstoque in 'class\uRelatorioFRFiltroVerticalEstoque.pas',
  uRetornoProxy in 'proxy datasnap\uRetornoProxy.pas',
  uFrameNotaFiscalItem in 'frames\uFrameNotaFiscalItem.pas' {FrameNotaFiscalItem: TFrame},
  uOperacaoProxy in 'proxy datasnap\uOperacaoProxy.pas',
  uRelatorioFRFiltroVerticalOrdemServico in 'class\uRelatorioFRFiltroVerticalOrdemServico.pas',
  uFluxoCaixaResumidoProxy in 'proxy datasnap\uFluxoCaixaResumidoProxy.pas',
  uImpostoICMS in 'class\uImpostoICMS.pas',
  uFrmExibirListaMensagemSimples in 'inheritable\uFrmExibirListaMensagemSimples.pas' {FrmExibirListaMensagemSimples},
  uOperacao in 'class\uOperacao.pas',
  uFrmIdentificacaoDestinatarioNFCe in 'forms\uFrmIdentificacaoDestinatarioNFCe.pas' {FrmIdentificacaoDestinatarioNFCe},
  uFrameBloqueioPersonalizadoUsuarioAutorizado in 'frames\uFrameBloqueioPersonalizadoUsuarioAutorizado.pas' {FrameBloqueioPersonalizadoUsuarioAutorizado: TFrame},
  uFrameQuitacaoGeracaoDocumentoTipoCartao in 'frames\uFrameQuitacaoGeracaoDocumentoTipoCartao.pas' {FrameQuitacaoGeracaoDocumentoTipoCartao: TFrame},
  uFrameQuitacaoGeracaoDocumentoTipoCheque in 'frames\uFrameQuitacaoGeracaoDocumentoTipoCheque.pas' {FrameQuitacaoGeracaoDocumentoTipoCheque: TFrame},
  uCadEnquadramentoLegalIPI in 'forms\uCadEnquadramentoLegalIPI.pas' {CadEnquadramentoLegalIPI},
  uMotorCalculoNotaFiscal in '..\cs\uMotorCalculoNotaFiscal.pas',
  uMotorCalculoNotaFiscalICMS in '..\cs\uMotorCalculoNotaFiscalICMS.pas',
  uModeloNota in 'class\uModeloNota.pas',
  uModeloNotaProxy in 'proxy datasnap\uModeloNotaProxy.pas',
  uImpostoIPI in 'class\uImpostoIPI.pas',
  uImpostoPisCofins in 'class\uImpostoPisCofins.pas',
  uFrmSolicitarValor in 'forms\uFrmSolicitarValor.pas' {FrmSolicitarValor},
  uMotorCalculoNotaFiscalCOFINS in '..\cs\uMotorCalculoNotaFiscalCOFINS.pas',
  uMotorCalculoNotaFiscalPIS in '..\cs\uMotorCalculoNotaFiscalPIS.pas',
  uFramePessoaVenda in 'frames\uFramePessoaVenda.pas' {FramePessoaVenda: TFrame},
  uCadTipoRestricao in 'forms\uCadTipoRestricao.pas' {CadTipoRestricao},
  uFramePessoaRestricao in 'frames\uFramePessoaRestricao.pas' {FramePessoaRestricao: TFrame},
  uTipoRestricaoProxy in 'proxy datasnap\uTipoRestricaoProxy.pas',
  uRelatorioFRFiltroVerticalContaCorrenteMovimento in 'class\uRelatorioFRFiltroVerticalContaCorrenteMovimento.pas',
  uCadRelatorioFRDefinirFiltrosPersonalizados in 'forms\uCadRelatorioFRDefinirFiltrosPersonalizados.pas' {CadRelatorioFrDefinirFiltrosPersonalizados},
  uJSONUtils in '..\library\uJSONUtils.pas',
  uBoletoCobreBemX in 'class\uBoletoCobreBemX.pas',
  uBoleto in 'class\uBoleto.pas',
  uFrmComponentesFiscais in '..\cs\uFrmComponentesFiscais.pas' {FrmComponentesFiscais},
  uConfiguracaoFiscal in 'class\uConfiguracaoFiscal.pas',
  uFrmInutilizacaoNotaFiscal in 'forms\uFrmInutilizacaoNotaFiscal.pas' {FrmInutilizacaoNotaFiscal},
  uRelatorioZebra in 'class\uRelatorioZebra.pas',
  uCadBloqueioPersonalizado in 'forms\uCadBloqueioPersonalizado.pas' {CadBloqueioPersonalizado},
  uMovEmissaoDocumentoFiscalVendaAgrupada in 'forms\uMovEmissaoDocumentoFiscalVendaAgrupada.pas' {MovEmissaoDocumentoFiscalVendaAgrupada},
  uBloqueioPersonalizadoProxy in 'proxy datasnap\uBloqueioPersonalizadoProxy.pas',
  uFrmLiberacaoBloqueioPersonalizado in 'forms\uFrmLiberacaoBloqueioPersonalizado.pas' {FrmLiberacaoBloqueioPersonalizado},
  uBloqueioPersonalizado in 'class\uBloqueioPersonalizado.pas',
  uClientClasses in 'proxy datasnap\uClientClasses.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Gest�o Empresarial';
  TStyleManager.TrySetStyle('Sapphire Kamri');
  Application.CreateForm(TDmAcesso, DmAcesso);
  Application.CreateForm(TDmConnection, DmConnection);
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  //FrmPrincipal.ActMovNotaFiscal.Execute;
  Application.Run;
end.
