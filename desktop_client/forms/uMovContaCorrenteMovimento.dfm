inherited MovContaCorrenteMovimento: TMovContaCorrenteMovimento
  Caption = 'Movimenta'#231#227'o de Conta Corrente'
  ClientHeight = 593
  ClientWidth = 1341
  ExplicitWidth = 1357
  ExplicitHeight = 632
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1341
    ExplicitWidth = 1341
    inherited dxGerencia: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarNavegacaoData'
        end
        item
          ToolbarName = 'dxBarManager'
        end
        item
          ToolbarName = 'dxBarAction'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          Caption = 'Filtros'
          ToolbarName = 'dxBarFiltro'
        end
        item
          ToolbarName = 'dxBarSearch'
        end
        item
          ToolbarName = 'dxBarReport'
        end
        item
          ToolbarName = 'dxBarEnd'
        end>
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1341
    Height = 466
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1341
    ExplicitHeight = 466
    ClientRectBottom = 466
    ClientRectRight = 1341
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1341
      ExplicitHeight = 442
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1309
        Height = 442
        ExplicitWidth = 1180
        ExplicitHeight = 442
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 442
        Height = 442
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 442
        ExplicitHeight = 442
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1341
      ExplicitHeight = 442
      inherited DesignPanel: TJvDesignPanel
        Width = 1341
        Height = 442
        ExplicitWidth = 1341
        ExplicitHeight = 442
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1341
          ExplicitHeight = 442
          Height = 442
          Width = 1341
          inherited dxBevel1: TdxBevel
            Width = 1337
            ExplicitWidth = 893
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 1501
            Top = 9
            Width = 73
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Cadastrado em'
            ExplicitLeft = 1067
          end
          object Label15: TLabel
            Left = 687
            Top = 9
            Width = 34
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Origem'
            ExplicitLeft = 381
          end
          object Label16: TLabel
            Left = 191
            Top = 9
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object lbDhConciliacao: TLabel
            Left = 1135
            Top = 9
            Width = 65
            Height = 13
            Anchors = [akTop, akRight]
            AutoSize = False
            Caption = 'Conciliado em'
            FocusControl = EdtDhConciliacao
            ExplicitLeft = 702
          end
          object Label18: TLabel
            Left = 934
            Top = 8
            Width = 65
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Parcelamento'
            ExplicitLeft = 628
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 117
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 1578
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object PnDados: TcxGroupBox
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.TransparentBorder = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            DesignSize = (
              1337
              133)
            Height = 133
            Width = 1337
            object Label2: TLabel
              Left = 8
              Top = 31
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object Label3: TLabel
              Left = 8
              Top = 6
              Width = 79
              Height = 13
              Caption = 'Data de Emiss'#227'o'
            end
            object Label4: TLabel
              Left = 202
              Top = 6
              Width = 93
              Height = 13
              Caption = 'Data de Movimento'
            end
            object Label5: TLabel
              Left = 8
              Top = 109
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object Label7: TLabel
              Left = 836
              Top = 83
              Width = 24
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Valor'
              ExplicitLeft = 590
            end
            object Label8: TLabel
              Left = 591
              Top = 6
              Width = 54
              Height = 13
              Caption = 'Documento'
            end
            object Label9: TLabel
              Left = 8
              Top = 58
              Width = 99
              Height = 13
              Caption = 'Centro de Resultado'
            end
            object Label10: TLabel
              Left = 481
              Top = 58
              Width = 81
              Height = 13
              Caption = 'Conta de An'#225'lise'
            end
            object Label11: TLabel
              Left = 998
              Top = 83
              Width = 107
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Tipo de Movimenta'#231#227'o'
              ExplicitLeft = 752
            end
            object Label12: TLabel
              Left = 8
              Top = 83
              Width = 75
              Height = 13
              Caption = 'Conta Corrente'
            end
            object Label17: TLabel
              Left = 389
              Top = 6
              Width = 103
              Height = 13
              Caption = 'Data de Compet'#234'ncia'
            end
            object EdtDescricao: TgbDBTextEdit
              Left = 115
              Top = 27
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbRequired = True
              gbPassword = False
              Width = 1218
            end
            object gbDBDateEdit2: TgbDBDateEdit
              Left = 115
              Top = 2
              DataBinding.DataField = 'DT_EMISSAO'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbDateTime = False
              Width = 83
            end
            object gbDBDateEdit3: TgbDBDateEdit
              Left = 299
              Top = 2
              DataBinding.DataField = 'DT_MOVIMENTO'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              gbDateTime = False
              Width = 83
            end
            object gbDBRadioGroup1: TgbDBRadioGroup
              Left = 1109
              Top = 75
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'TP_MOVIMENTO'
              DataBinding.DataSource = dsData
              Properties.Columns = 2
              Properties.Items = <
                item
                  Caption = 'Cr'#233'dito'
                  Value = 'CREDITO'
                end
                item
                  Caption = 'D'#233'bito'
                  Value = 'DEBITO'
                end>
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 12
              Height = 25
              Width = 143
            end
            object gbDBCheckBox1: TgbDBCheckBox
              Left = 1256
              Top = 79
              Anchors = [akTop, akRight]
              Caption = 'Conciliado?'
              DataBinding.DataField = 'BO_CONCILIADO'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 13
              Transparent = True
              Width = 76
            end
            object gbDBBlobEdit1: TgbDBBlobEdit
              Left = 115
              Top = 105
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACAO'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 14
              Width = 1218
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 864
              Top = 79
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'VL_MOVIMENTO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 11
              gbRequired = True
              gbPassword = False
              Width = 130
            end
            object EdtDocumento: TgbDBTextEdit
              Left = 656
              Top = 2
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DOCUMENTO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbRequired = True
              gbPassword = False
              Width = 677
            end
            object gbDBButtonEditFK1: TgbDBButtonEditFK
              Left = 115
              Top = 54
              DataBinding.DataField = 'ID_CENTRO_RESULTADO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 5
              gbTextEdit = gbDBTextEdit3
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
              gbTableName = 'CENTRO_RESULTADO'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CENTRO_RESULTADO'
              Width = 60
            end
            object gbDBTextEdit3: TgbDBTextEdit
              Left = 174
              Top = 54
              TabStop = False
              DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 6
              gbReadyOnly = True
              gbPassword = False
              Width = 302
            end
            object gbDBTextEdit5: TgbDBTextEdit
              Left = 625
              Top = 54
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 8
              gbReadyOnly = True
              gbPassword = False
              Width = 708
            end
            object gbDBButtonEditFK3: TgbDBButtonEditFK
              Left = 115
              Top = 79
              DataBinding.DataField = 'ID_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 9
              gbTextEdit = gbDBTextEdit6
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CONTA_CORRENTE;JOIN_DESCRICAO_CONTA_CORRENTE'
              gbTableName = 'CONTA_CORRENTE'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CONTA_CORRENTE'
              Width = 60
            end
            object gbDBTextEdit6: TgbDBTextEdit
              Left = 174
              Top = 79
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 10
              gbReadyOnly = True
              gbPassword = False
              Width = 658
            end
            object EdtContaAnalise: TcxDBButtonEdit
              Left = 565
              Top = 54
              DataBinding.DataField = 'ID_CONTA_ANALISE'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ClickKey = 13
              Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
              Style.Color = 14606074
              TabOrder = 7
              OnDblClick = EdtContaAnaliseDblClick
              OnExit = EdtContaAnalisePropertiesEditValueChanged
              Width = 60
            end
            object gbDBDateEdit4: TgbDBDateEdit
              Left = 496
              Top = 2
              DataBinding.DataField = 'DT_COMPETENCIA'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbRequired = True
              gbDateTime = False
              Width = 83
            end
          end
          object PnTransferencia: TcxGroupBox
            Left = 2
            Top = 167
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.TransparentBorder = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            DesignSize = (
              1337
              26)
            Height = 26
            Width = 1337
            object Label13: TLabel
              Left = 8
              Top = 3
              Width = 83
              Height = 13
              Caption = 'Conta de Destino'
            end
            object EdtContaCorrenteDestinoTransferencia: TgbDBButtonEditFK
              Left = 115
              Top = -1
              DataBinding.DataField = 'ID_CONTA_CORRENTE_DESTINO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 0
              gbTextEdit = gbDBTextEdit7
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CONTA_CORRENTE_DESTINO;IC_DESC_CONTA_CORRENTE_DESTINO'
              gbTableName = 'CONTA_CORRENTE'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CONTA_CORRENTE'
              Width = 60
            end
            object gbDBTextEdit7: TgbDBTextEdit
              Left = 174
              Top = -1
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'IC_DESC_CONTA_CORRENTE_DESTINO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbPassword = False
              Width = 1159
            end
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 847
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 74
          end
          object gbDBTextEdit8: TgbDBTextEdit
            Left = 726
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'TP_DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 121
          end
          object gbDBTextEdit9: TgbDBTextEdit
            Left = 237
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 73
          end
          object EdtDhConciliacao: TgbDBDateEdit
            Left = 1203
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            AutoSize = False
            DataBinding.DataField = 'DH_CONCILIADO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbDateTime = True
            Height = 21
            Width = 130
          end
          object gbDBTextEdit10: TgbDBTextEdit
            Left = 1003
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'PARCELAMENTO'
            DataBinding.DataSource = dsData
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            gbReadyOnly = True
            gbPassword = False
            Width = 121
          end
          object descProcesso: TgbDBTextEdit
            Left = 309
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 364
          end
          inline FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque
            Left = 2
            Top = 241
            Width = 1337
            Height = 102
            Align = alTop
            TabOrder = 10
            ExplicitLeft = 2
            ExplicitTop = 241
            ExplicitWidth = 1337
            ExplicitHeight = 102
            inherited cxGBDadosMain: TcxGroupBox
              ExplicitWidth = 1337
              ExplicitHeight = 102
              Height = 102
              Width = 1337
              inherited PnTituloCheque: TgbPanel
                Style.IsFontAssigned = True
                ExplicitWidth = 1333
                Width = 1333
              end
              inherited PnDados: TgbPanel
                ExplicitWidth = 1333
                ExplicitHeight = 79
                Height = 79
                Width = 1333
                inherited PnDadosDireita: TgbPanel
                  Left = 1129
                  ExplicitLeft = 1129
                  ExplicitHeight = 75
                  Height = 75
                  inherited edtDocFederal: TgbDBTextEdit
                    TabStop = False
                    DataBinding.DataField = 'CHEQUE_DOC_FEDERAL'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                  inherited edtDtVencimento: TgbDBDateEdit
                    TabStop = False
                    DataBinding.DataField = 'DT_MOVIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                  inherited EdtValor: TgbDBCalcEdit
                    TabStop = False
                    DataBinding.DataField = 'VL_MOVIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                end
                inherited PnDadosEsquerda: TgbPanel
                  ExplicitWidth = 1127
                  ExplicitHeight = 75
                  Height = 75
                  Width = 1127
                  inherited PnTopo3PainelEsquerda: TgbPanel
                    ExplicitWidth = 1123
                    Width = 1123
                    inherited PnTopo3PainelEsquerdaPainel2: TgbPanel
                      Left = 978
                      ExplicitLeft = 978
                      inherited edtNumero: TgbDBTextEdit
                        Left = 66
                        TabStop = False
                        DataBinding.DataField = 'CHEQUE_NUMERO'
                        DataBinding.DataSource = dsData
                        Properties.ReadOnly = True
                        Style.Color = clGradientActiveCaption
                        gbReadyOnly = True
                        ExplicitLeft = 66
                        ExplicitWidth = 78
                        Width = 78
                      end
                    end
                    inherited PnTopo3PainelEsquerdaPainel1: TgbPanel
                      ExplicitWidth = 976
                      Width = 976
                      inherited edtAgencia: TgbDBTextEdit
                        TabStop = False
                        DataBinding.DataField = 'CHEQUE_AGENCIA'
                        DataBinding.DataSource = dsData
                        Properties.ReadOnly = True
                        Style.Color = clGradientActiveCaption
                        gbReadyOnly = True
                      end
                      inherited edtContaCorrente: TgbDBTextEdit
                        TabStop = False
                        DataBinding.DataField = 'CHEQUE_CONTA_CORRENTE'
                        DataBinding.DataSource = dsData
                        Properties.ReadOnly = True
                        Style.Color = clGradientActiveCaption
                        gbReadyOnly = True
                        ExplicitWidth = 775
                        Width = 775
                      end
                    end
                  end
                  inherited PnTopo1PainelEsquerda: TgbPanel
                    ExplicitWidth = 1123
                    Width = 1123
                    inherited PnConsultaCheque: TgbPanel
                      inherited EdtConsultaCheque: TgbDBButtonEditFK
                        TabStop = False
                        Properties.ReadOnly = True
                        Style.Color = clGradientActiveCaption
                        gbReadyOnly = True
                      end
                    end
                    inherited PnSacado: TgbPanel
                      ExplicitWidth = 1003
                      Width = 1003
                      inherited edtSacado: TgbDBTextEdit
                        TabStop = False
                        DataBinding.DataField = 'CHEQUE_SACADO'
                        DataBinding.DataSource = dsData
                        Properties.ReadOnly = True
                        Style.Color = clGradientActiveCaption
                        gbReadyOnly = True
                        ExplicitWidth = 1083
                        Width = 1083
                      end
                    end
                  end
                  inherited PnTopo2PainelEsquerda: TgbPanel
                    ExplicitWidth = 1123
                    Width = 1123
                    inherited labelDtEmissao: TLabel
                      Left = 984
                      ExplicitLeft = 892
                    end
                    inherited edtBanco: TgbDBTextEdit
                      TabStop = False
                      DataBinding.DataField = 'CHEQUE_BANCO'
                      DataBinding.DataSource = dsData
                      Properties.ReadOnly = True
                      Style.Color = clGradientActiveCaption
                      gbReadyOnly = True
                      ExplicitWidth = 931
                      Width = 931
                    end
                    inherited edtDtEmissao: TgbDBDateEdit
                      Left = 1044
                      TabStop = False
                      DataBinding.DataField = 'DT_EMISSAO'
                      DataBinding.DataSource = dsData
                      Properties.ReadOnly = True
                      Style.Color = clGradientActiveCaption
                      gbReadyOnly = True
                      ExplicitLeft = 1044
                    end
                  end
                end
              end
            end
          end
          inline FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao
            Left = 2
            Top = 193
            Width = 1337
            Height = 48
            Align = alTop
            TabOrder = 11
            ExplicitLeft = 2
            ExplicitTop = 193
            ExplicitWidth = 1337
            inherited cxGBDadosMain: TcxGroupBox
              ExplicitWidth = 1337
              Width = 1337
              inherited PnTituloCartao: TgbPanel
                Style.IsFontAssigned = True
                ExplicitWidth = 1333
                Width = 1333
              end
              inherited PnDados: TgbPanel
                ExplicitWidth = 1333
                Width = 1333
                inherited PnDtVencimento: TgbPanel
                  inherited edtDtVencimento: TgbDBDateEdit
                    TabStop = False
                    DataBinding.DataField = 'DT_MOVIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                end
                inherited PnOperadoraCartao: TgbPanel
                  ExplicitWidth = 1002
                  Width = 1002
                  inherited EdtIdOperadoraCartao: TgbDBButtonEditFK
                    TabStop = False
                    DataBinding.DataField = 'ID_OPERADORA_CARTAO'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                  inherited EdtDescricaoOperadoraCartao: TgbDBTextEdit
                    DataBinding.DataField = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
                    DataBinding.DataSource = dsData
                    ExplicitWidth = 836
                    Width = 836
                  end
                end
                inherited PnValor: TgbPanel
                  Left = 1168
                  ExplicitLeft = 1168
                  inherited EdtValor: TgbDBCalcEdit
                    TabStop = False
                    DataBinding.DataField = 'VL_MOVIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.ReadOnly = True
                    Style.Color = clGradientActiveCaption
                    gbReadyOnly = True
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    object ActExportarExcel: TAction
      Category = 'filter'
      Caption = 'Exportar para Excel'
      Hint = 'Exportar grade para Excel'
      ImageIndex = 274
      OnExecute = ActExportarExcelExecute
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Categories.Strings = (
      'Action'
      'Manager'
      'Report'
      'End'
      'Search'
      'Design'
      'ActionDesign'
      'Filtros'
      'Navegacao')
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 51
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end
        item
          Visible = True
          ItemName = 'lbMovimentacaoCheque'
        end
        item
          Visible = True
          ItemName = 'lbTransferencia'
        end>
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 406
      FloatClientWidth = 70
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 1164
      FloatClientWidth = 68
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 1243
      FloatClientWidth = 51
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 1089
      FloatClientWidth = 66
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      FloatClientWidth = 124
      FloatClientHeight = 216
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 524
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object dxBarFiltro: TdxBar [8]
      Caption = 'Filtros'
      CaptionButtons = <>
      DockedLeft = 616
      DockedTop = 0
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 216
      FloatClientHeight = 169
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemLinks = <
        item
          Visible = True
          ItemName = 'filtroLabelDataMovimentacao'
        end
        item
          Visible = True
          ItemName = 'filtroDataMovimentoInicio'
        end
        item
          Visible = True
          ItemName = 'filtroDataMovimentoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarStatic1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 252
          Visible = True
          ItemName = 'filtroContaCorrente'
        end
        item
          Visible = True
          ItemName = 'bsSaldoAnterior'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbAnterior'
        end
        item
          Visible = True
          ItemName = 'lbProximo'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = True
      Visible = True
      WholeRow = False
    end
    object dxBarContainerItem1: TdxBarContainerItem [10]
      Caption = 'New Item'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem1: TcxBarEditItem [11]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
    end
    object cxBarEditItem2: TcxBarEditItem [12]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
    end
    object lbFiltroContaCorrente: TcxBarEditItem [15]
      Align = iaCenter
      Caption = 'Conta Corrente'
      Category = 0
      Hint = 'Conta Corrente'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object dxBarStatic1: TdxBarStatic [16]
      Caption = 'Selecione a conta corrente que deseja visualizar'
      Category = 0
      Hint = 'Selecione a conta corrente que deseja visualizar'
      Visible = ivAlways
    end
    object lbTransferencia: TdxBarLargeButton [20]
      Action = ActTransferencia
      Category = 1
    end
    object lbMovimentacaoCheque: TdxBarLargeButton [23]
      Action = ActMovimentacaoCheque
      Category = 1
    end
    inherited lbNavegacaoDataPrimeiro: TdxBarLargeButton
      PaintStyle = psCaption
    end
    inherited lbNavegacaoDataAnterior: TdxBarLargeButton
      PaintStyle = psCaption
    end
    object filtroContaCorrente: TcxBarEditItem [32]
      Category = 7
      Visible = ivAlways
      Width = 190
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.CharCase = ecUpperCase
      Properties.DropDownRows = 15
      Properties.ImmediateDropDownWhenKeyPressed = False
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          FieldName = 'descricao'
        end>
      Properties.ListSource = dsContaCorrente
      Properties.OnCloseUp = filtroContaCorrentePropertiesCloseUp
    end
    object filtroDocumento: TdxBarEdit [33]
      Caption = 'Documento      '
      Category = 7
      Hint = 'Documento      '
      Visible = ivAlways
      Width = 190
    end
    object filtroRotuloDataEmissao: TdxBarStatic [34]
      Align = iaCenter
      Caption = 'Data de Emiss'#227'o'
      Category = 7
      Hint = 'Data de Emiss'#227'o'
      Visible = ivAlways
    end
    object filtroDataEmissaoInicio: TcxBarEditItem [35]
      Caption = 'In'#237'cio'
      Category = 7
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataEmissaoFim: TcxBarEditItem [36]
      Caption = 'Fim   '
      Category = 7
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroLabelDataMovimentacao: TdxBarStatic [37]
      Align = iaCenter
      Caption = 'Data de Movimenta'#231#227'o'
      Category = 7
      Hint = 'Data de Movimenta'#231#227'o'
      Visible = ivAlways
    end
    object filtroDataMovimentoInicio: TcxBarEditItem [38]
      Caption = 'In'#237'cio'
      Category = 7
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataMovimentoFim: TcxBarEditItem [39]
      Caption = 'Fim   '
      Category = 7
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroConciliado: TcxBarEditItem [40]
      Caption = 'Concilia'#231#227'o         '
      Category = 7
      Hint = 'Concilia'#231#227'o         '
      Visible = ivAlways
      Width = 80
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        'Todos'
        'Conciliado'
        'N'#227'o Conciliado')
    end
    object filtroTipoMovimentacao: TcxBarEditItem [41]
      Caption = 'Tipo Movimento'
      Category = 7
      Hint = 'Tipo Movimento'
      Visible = ivAlways
      Width = 80
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        'Todos'
        'Cr'#233'dito'
        'D'#233'bito')
    end
    inherited lbNavegacaoDataProximo: TdxBarLargeButton
      PaintStyle = psCaption
    end
    inherited lbNavegacaoDataUltimo: TdxBarLargeButton
      PaintStyle = psCaption
    end
    object bsSaldoAnterior: TdxBarStatic [44]
      Category = 7
      Visible = ivAlways
    end
    object lbProximo: TdxBarLargeButton
      Action = ActProximo
      Category = 8
    end
    object lbAnterior: TdxBarLargeButton
      Action = ActAnterior
      Category = 8
    end
  end
  inherited ActionListMain: TActionList
    object ActTransferencia: TAction [12]
      Category = 'Action'
      Caption = 'Transfer'#234'ncia'
      Hint = 'Realizar transfer'#234'ncia entre contas correntes'
      ImageIndex = 87
      ShortCut = 118
      OnExecute = ActTransferenciaExecute
    end
    object ActProximo: TAction [13]
      Category = 'Navegation'
      Caption = '&Pr'#243'ximo'
      Hint = 'Ir para a pr'#243'xima Conta Corrente'
      ImageIndex = 17
      ShortCut = 16423
      OnExecute = ActProximoExecute
    end
    object ActAnterior: TAction [14]
      Category = 'Navegation'
      Caption = 'An&terior'
      Hint = 'Ir para a Conta Corrente anterior'
      ImageIndex = 18
      ShortCut = 16421
      OnExecute = ActAnteriorExecute
    end
    object ActMovimentacaoCheque: TAction
      Category = 'Action'
      Caption = 'Movimenta'#231#227'o de Cheque'
      ImageIndex = 154
      OnExecute = ActMovimentacaoChequeExecute
    end
  end
  inherited cxGridPopupMenuPadrao: TcxGridPopupMenu
    PopupMenus = <
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtGridNone, gvhtGridTab, gvhtNone, gvhtCell, gvhtRecord]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspContaCorrenteMovimento'
    RemoteServer = DmConnection.dspFinanceiro
    AfterOpen = cdsDataAfterOpen
    AfterClose = cdsDataAfterClose
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDH_CONCILIADO: TDateTimeField
      FieldName = 'DH_CONCILIADO'
      Origin = 'DH_CONCILIADO'
    end
    object cdsDataDT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'DT_EMISSAO'
      Origin = 'DT_EMISSAO'
      Required = True
    end
    object cdsDataDT_MOVIMENTO: TDateField
      DisplayLabel = 'Dt. Movimento'
      FieldName = 'DT_MOVIMENTO'
      Origin = 'DT_MOVIMENTO'
      Required = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataTP_MOVIMENTO: TStringField
      DefaultExpression = 'CREDITO'
      DisplayLabel = 'Tipo'
      FieldName = 'TP_MOVIMENTO'
      Origin = 'TP_MOVIMENTO'
      Required = True
      Size = 10
    end
    object cdsDataBO_CONCILIADO: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Conciliado?'
      FieldName = 'BO_CONCILIADO'
      Origin = 'BO_CONCILIADO'
      OnChange = cdsDataBO_CONCILIADOChange
      FixedChar = True
      Size = 1
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataVL_MOVIMENTO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_MOVIMENTO'
      Origin = 'VL_MOVIMENTO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_SALDO_CONCILIADO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Saldo Conciliado'
      FieldName = 'VL_SALDO_CONCILIADO'
      Origin = 'VL_SALDO_CONCILIADO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_SALDO_GERAL: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Saldo Geral'
      FieldName = 'VL_SALDO_GERAL'
      Origin = 'VL_SALDO_GERAL'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataID_MOVIMENTO_ORIGEM: TIntegerField
      FieldName = 'ID_MOVIMENTO_ORIGEM'
      Origin = 'ID_MOVIMENTO_ORIGEM'
    end
    object cdsDataID_MOVIMENTO_DESTINO: TIntegerField
      FieldName = 'ID_MOVIMENTO_DESTINO'
      Origin = 'ID_MOVIMENTO_DESTINO'
    end
    object cdsDataBO_MOVIMENTO_ORIGEM: TStringField
      DefaultExpression = 'N'
      FieldName = 'BO_MOVIMENTO_ORIGEM'
      Origin = 'BO_MOVIMENTO_ORIGEM'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_MOVIMENTO_DESTINO: TStringField
      DefaultExpression = 'N'
      FieldName = 'BO_MOVIMENTO_DESTINO'
      Origin = 'BO_MOVIMENTO_DESTINO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_TRANSFERENCIA: TStringField
      DefaultExpression = 'N'
      FieldName = 'BO_TRANSFERENCIA'
      Origin = 'BO_TRANSFERENCIA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_CONTA_CORRENTE_DESTINO: TIntegerField
      DisplayLabel = 'Conta Corrente de Destino'
      FieldName = 'ID_CONTA_CORRENTE_DESTINO'
      Origin = 'ID_CONTA_CORRENTE_DESTINO'
    end
    object cdsDataIC_DESC_CONTA_CORRENTE_DESTINO: TStringField
      DisplayLabel = 'Conta Corrente de Destino'
      FieldKind = fkInternalCalc
      FieldName = 'IC_DESC_CONTA_CORRENTE_DESTINO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Id. Chave Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
    end
    object cdsDataID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object cdsDataTP_DOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object cdsDataPARCELAMENTO: TStringField
      DisplayLabel = 'Parcelamento'
      FieldName = 'PARCELAMENTO'
      Origin = 'PARCELAMENTO'
      Size = 50
    end
    object cdsDataDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Origem Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataCHEQUE_SACADO: TStringField
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsDataCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsDataCHEQUE_BANCO: TStringField
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsDataCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsDataCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsDataCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsDataCHEQUE_DT_VENCIMENTO: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object cdsDataCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsDataID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsDataJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      DisplayLabel = 'Cart'#227'o: Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataBO_CHEQUE: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Cheque'
      FieldName = 'BO_CHEQUE'
      Origin = 'BO_CHEQUE'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_CHEQUE_PROPRIO: TStringField
      DisplayLabel = 'Cheque Pr'#243'prio'
      FieldName = 'BO_CHEQUE_PROPRIO'
      Origin = 'BO_CHEQUE_PROPRIO'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_CHEQUE_TERCEIRO: TStringField
      DisplayLabel = 'Cheque de Terceiro'
      FieldName = 'BO_CHEQUE_TERCEIRO'
      Origin = 'BO_CHEQUE_TERCEIRO'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_PROCESSO_AMORTIZADO: TStringField
      DisplayLabel = 'Processo Amortizado'
      FieldName = 'BO_PROCESSO_AMORTIZADO'
      Origin = 'BO_PROCESSO_AMORTIZADO'
      FixedChar = True
      Size = 1
    end
  end
  inherited pmGridConsultaPadrao: TPopupMenu
    object ActConciliar1: TMenuItem [0]
      Action = ActConciliar
    end
    object ExportarparaExcel1: TMenuItem [1]
      Action = ActExportarExcel
    end
    object N1: TMenuItem [2]
      Caption = '-'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.551275405090000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
  object fdmContaCorrente: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 772
    Top = 64
    object fdmContaCorrenteid: TIntegerField
      FieldName = 'id'
    end
    object fdmContaCorrentedescricao: TStringField
      FieldName = 'descricao'
      Size = 255
    end
  end
  object dsContaCorrente: TDataSource
    DataSet = fdmContaCorrente
    Left = 800
    Top = 64
  end
  object ACMovimentacao: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 496
    Top = 80
    object ActConciliar: TAction
      Caption = 'Conciliar'
      Hint = 'Conciliar/Desconciliar lan'#231'amento'
      ImageIndex = 18
      OnExecute = ActConciliarExecute
    end
  end
end
