unit uCadModeloMontadora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxCheckBox, uGBDBCheckBox;

type
  TCadModeloMontadora = class(TFrmCadastro_Padrao)
    gbDBCheckBox1: TgbDBCheckBox;
    Label1: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBBlobEdit1: TgbDBBlobEdit;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_MONTADORA: TIntegerField;
    cdsDataJOIN_DESCRICAO_MONTADORA: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CadModeloMontadora: TCadModeloMontadora;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadModeloMontadora);

finalization
  UnRegisterClass(TCadModeloMontadora);

end.
