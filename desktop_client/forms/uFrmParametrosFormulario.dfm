inherited FrmParametrosFormulario: TFrmParametrosFormulario
  Caption = 'Parametriza'#231#245'es'
  ExplicitWidth = 664
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    object cxgdMain: TcxGrid
      Left = 2
      Top = 2
      Width = 654
      Height = 166
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object cxgdMainDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsColunas
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsCustomize.DataRowSizing = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        object cxgdMainDBTableView1PARAMETRO: TcxGridDBColumn
          DataBinding.FieldName = 'PARAMETRO'
          SortIndex = 0
          SortOrder = soAscending
          Width = 338
        end
        object cxgdMainDBTableView1VALOR: TcxGridDBColumn
          DataBinding.FieldName = 'VALOR'
          Width = 223
        end
        object cxgdMainDBTableView1BO_OBRIGATORIO: TcxGridDBColumn
          DataBinding.FieldName = 'BO_OBRIGATORIO'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          Properties.ValueChecked = 'S'
          Properties.ValueUnchecked = 'N'
          Options.Editing = False
          Width = 79
        end
      end
      object cxgdMainLevel1: TcxGridLevel
        GridView = cxgdMainDBTableView1
      end
    end
    object DBMemo1: TDBMemo
      Left = 2
      Top = 168
      Width = 654
      Height = 99
      Align = alBottom
      DataField = 'DESCRICAO'
      DataSource = dsColunas
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 1
    end
  end
  inherited ActionList: TActionList
    Left = 342
    Top = 270
  end
  object dsColunas: TDataSource
    DataSet = fdmParametros
    Left = 216
    Top = 272
  end
  object fdmParametros: TgbFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 244
    Top = 272
    object fdmParametrosPARAMETRO: TStringField
      DisplayLabel = 'Par'#226'metro'
      FieldName = 'PARAMETRO'
      Size = 255
    end
    object fdmParametrosVALOR: TStringField
      DisplayLabel = 'Valor'
      FieldName = 'VALOR'
      Size = 255
    end
    object fdmParametrosDESCRICAO: TBlobField
      FieldName = 'DESCRICAO'
    end
    object fdmParametrosBO_OBRIGATORIO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Obrigat'#243'rio'
      FieldName = 'BO_OBRIGATORIO'
      Size = 1
    end
  end
end
