inherited CadImpostoPISCofins: TCadImpostoPISCofins
  Caption = 'Cadastro de Imposto PIS Cofins'
  ClientWidth = 846
  ExplicitWidth = 862
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 846
    ExplicitWidth = 846
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 846
    Properties.ActivePage = cxtsData
    ExplicitWidth = 846
    ClientRectRight = 846
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 846
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 814
        ExplicitWidth = 814
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 846
      inherited DesignPanel: TJvDesignPanel
        Width = 846
        ExplicitWidth = 846
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 846
          Width = 846
          inherited dxBevel1: TdxBevel
            Width = 842
            ExplicitWidth = 842
          end
          object Label2: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 10
            Top = 40
            Width = 79
            Height = 13
            Caption = 'CST PIS COFINS'
          end
          object Label3: TLabel
            Left = 10
            Top = 66
            Width = 58
            Height = 13
            Caption = 'Al'#237'quota PIS'
          end
          object Label4: TLabel
            Left = 10
            Top = 93
            Width = 80
            Height = 13
            Caption = 'Al'#237'quota COFINS'
          end
          object Label5: TLabel
            Left = 10
            Top = 120
            Width = 73
            Height = 13
            Caption = 'Al'#237'quota PIS ST'
          end
          object Label6: TLabel
            Left = 10
            Top = 147
            Width = 95
            Height = 13
            Caption = 'Al'#237'quota COFINS ST'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 113
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 170
            Top = 36
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_CST_PIS_COFINS'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 663
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 113
            Top = 36
            DataBinding.DataField = 'JOIN_CODIGO_CST_PIS_COFINS'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 1
            gbTextEdit = gbDBTextEdit1
            gbRequired = True
            gbCampoPK = 'CODIGO_CST'
            gbCamposRetorno = 
              'ID_CST_PIS_COFINS;JOIN_CODIGO_CST_PIS_COFINS;JOIN_DESCRICAO_CST_' +
              'PIS_COFINS'
            gbTableName = 'CST_PIS_COFINS'
            gbCamposConsulta = 'ID;CODIGO_CST;DESCRICAO'
            gbIdentificadorConsulta = 'CST_PIS_COFINS'
            Width = 60
          end
          object gbDBCalcEdit1: TgbDBCalcEdit
            Left = 113
            Top = 62
            DataBinding.DataField = 'PERC_ALIQUOTA_PIS'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit2: TgbDBCalcEdit
            Left = 113
            Top = 89
            DataBinding.DataField = 'PERC_ALIQUOTA_COFINS'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit3: TgbDBCalcEdit
            Left = 113
            Top = 116
            DataBinding.DataField = 'PERC_ALIQUOTA_PIS_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit4: TgbDBCalcEdit
            Left = 113
            Top = 143
            DataBinding.DataField = 'PERC_ALIQUOTA_COFINS_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbRequired = True
            Width = 120
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspImpostoPisCofins'
    RemoteServer = DmConnection.dspImpostoPISCofins
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataPERC_ALIQUOTA_PIS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Al'#237'quota PIS'
      FieldName = 'PERC_ALIQUOTA_PIS'
      Origin = 'PERC_ALIQUOTA_PIS'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_ALIQUOTA_COFINS: TFMTBCDField
      DisplayLabel = '% Al'#237'quota Cofins'
      FieldName = 'PERC_ALIQUOTA_COFINS'
      Origin = 'PERC_ALIQUOTA_COFINS'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_ALIQUOTA_PIS_ST: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Al'#237'quota PIS ST'
      FieldName = 'PERC_ALIQUOTA_PIS_ST'
      Origin = 'PERC_ALIQUOTA_PIS_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_ALIQUOTA_COFINS_ST: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Al'#237'quota Cofins ST'
      FieldName = 'PERC_ALIQUOTA_COFINS_ST'
      Origin = 'PERC_ALIQUOTA_COFINS_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataID_CST_PIS_COFINS: TIntegerField
      DisplayLabel = 'C'#243'digo do CST PIS COFINS'
      FieldName = 'ID_CST_PIS_COFINS'
      Origin = 'ID_CST_PIS_COFINS'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_CST_PIS_COFINS: TStringField
      DisplayLabel = 'CST PIS COFINS'
      FieldName = 'JOIN_DESCRICAO_CST_PIS_COFINS'
      Origin = 'JOIN_DESCRICAO_CST_PIS_COFINS'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CODIGO_CST_PIS_COFINS: TStringField
      DisplayLabel = 'C'#243'digo CST PIS Cofins'
      FieldName = 'JOIN_CODIGO_CST_PIS_COFINS'
      Origin = 'JOIN_CODIGO_CST_PIS_COFINS'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.883658784730000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
