inherited MovContaPagar: TMovContaPagar
  Left = 342
  Top = 219
  AlphaBlend = True
  Caption = 'Conta a Pagar'
  ClientHeight = 472
  ClientWidth = 1003
  Position = poDesigned
  OnResize = FormResize
  ExplicitWidth = 1019
  ExplicitHeight = 511
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1003
    ExplicitWidth = 1003
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1003
    Height = 345
    ExplicitWidth = 1003
    ExplicitHeight = 345
    ClientRectBottom = 345
    ClientRectRight = 1003
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1003
      ExplicitHeight = 321
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 971
        Height = 275
        ExplicitWidth = 971
        ExplicitHeight = 275
        inherited Level1BandedTableView1: TcxGridDBBandedTableView
          DataController.OnDataChanged = Level1BandedTableView1DataControllerDataChanged
          FilterRow.ApplyChanges = fracOnCellExit
        end
      end
      inherited pnFiltroVertical: TgbPanel
        TabOrder = 2
        ExplicitHeight = 275
        Height = 275
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 275
        ExplicitHeight = 275
      end
      object PnTotalizadorConsulta: TFlowPanel
        Left = 0
        Top = 275
        Width = 1003
        Height = 46
        Align = alBottom
        AutoSize = True
        BevelOuter = bvNone
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 1
        object pnTotalizadorVlAberto: TgbLabel
          AlignWithMargins = True
          Left = 0
          Top = 1
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 12
        end
        object pnTotalizadorAcrecimoAberto: TgbLabel
          AlignWithMargins = True
          Left = 168
          Top = 1
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          Height = 21
          Width = 167
          AnchorY = 12
        end
        object pnTotalizadorTotalAberto: TgbLabel
          AlignWithMargins = True
          Left = 336
          Top = 1
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          Height = 21
          Width = 167
          AnchorY = 12
        end
        object pnTotalizadorVlVencido: TgbLabel
          AlignWithMargins = True
          Left = 504
          Top = 1
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clBlack
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextColor = clRed
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 12
        end
        object pnTotalizadorVlVencer: TgbLabel
          AlignWithMargins = True
          Left = 672
          Top = 1
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextColor = clNavy
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 12
        end
        object pnTotalizadorVlRecebido: TgbLabel
          AlignWithMargins = True
          Left = 0
          Top = 24
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextColor = clGreen
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 35
        end
        object pnTotalizadorAcrescimoRecebido: TgbLabel
          AlignWithMargins = True
          Left = 168
          Top = 24
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextColor = clGreen
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 35
        end
        object pnTotalizadorTotalRecebido: TgbLabel
          AlignWithMargins = True
          Left = 336
          Top = 24
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          AutoSize = False
          Caption = 'TOTALIZA R$ 999.999.999.00'
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextColor = clGreen
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 21
          Width = 167
          AnchorY = 35
        end
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1003
      ExplicitHeight = 321
      inherited DesignPanel: TJvDesignPanel
        Width = 1003
        Height = 321
        ExplicitWidth = 1003
        ExplicitHeight = 321
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1003
          ExplicitHeight = 321
          Height = 321
          Width = 1003
          inherited dxBevel1: TdxBevel
            Width = 999
            Height = 31
            Anchors = [akLeft, akTop]
            ExplicitLeft = 3
            ExplicitWidth = 1068
            ExplicitHeight = 31
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelDtCadastro: TLabel
            Left = 691
            Top = 7
            Width = 73
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Cadastrado em'
            ExplicitLeft = 760
          end
          object labelProcesso: TLabel
            Left = 89
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object labelSituacao: TLabel
            Left = 505
            Top = 7
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 574
          end
          object labelOrigem: TLabel
            Left = 232
            Top = 8
            Width = 34
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Origem'
            ExplicitLeft = 301
          end
          object labelSequencia: TLabel
            Left = 901
            Top = 7
            Width = 49
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Sequ'#234'ncia'
            ExplicitLeft = 970
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 45
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 40
          end
          object edDtCadastro: TgbDBDateEdit
            Left = 767
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object edtDescProcesso: TgbDBTextEdit
            Left = 174
            Top = 4
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 104
          end
          object edtProcesso: TgbDBTextEdit
            Left = 134
            Top = 4
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 40
          end
          object edSituacao: TgbDBTextEdit
            Left = 551
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 130
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 33
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            DesignSize = (
              999
              123)
            Height = 123
            Width = 999
            object labelDocumento: TLabel
              Left = 6
              Top = 3
              Width = 54
              Height = 13
              Caption = 'Documento'
            end
            object labelPessoa: TLabel
              Left = 6
              Top = 53
              Width = 34
              Height = 13
              Caption = 'Pessoa'
            end
            object labelCentroResultado: TLabel
              Left = 558
              Top = 29
              Width = 99
              Height = 13
              Caption = 'Centro de Resultado'
            end
            object labelContaAnalise: TLabel
              Left = 558
              Top = 54
              Width = 81
              Height = 13
              Caption = 'Conta de An'#225'lise'
            end
            object labelDescricao: TLabel
              Left = 6
              Top = 28
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object labelVlTitulo: TLabel
              Left = 362
              Top = 4
              Width = 68
              Height = 13
              Caption = 'Valor do T'#237'tulo'
            end
            object labelObservacao: TLabel
              Left = 558
              Top = 103
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object labelFormaPagamento: TLabel
              Left = 558
              Top = 5
              Width = 102
              Height = 13
              Caption = 'Forma de Pagamento'
            end
            object labelDtDocumento: TLabel
              Left = 6
              Top = 103
              Width = 72
              Height = 13
              Caption = 'Dt. Documento'
            end
            object labelDtCompetencia: TLabel
              Left = 189
              Top = 103
              Width = 80
              Height = 13
              Caption = 'Dt. Compet'#234'ncia'
            end
            object labelDtVencimento: TLabel
              Left = 379
              Top = 103
              Width = 73
              Height = 13
              Caption = 'Dt. Vencimento'
            end
            object lbContaCorrente: TLabel
              Left = 558
              Top = 78
              Width = 75
              Height = 13
              Caption = 'Conta Corrente'
            end
            object Label1: TLabel
              Left = 6
              Top = 78
              Width = 39
              Height = 13
              Caption = 'Carteira'
            end
            object edDocumento: TgbDBTextEdit
              Left = 80
              Top = 0
              DataBinding.DataField = 'DOCUMENTO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 267
            end
            object fkPessoa: TgbDBButtonEditFK
              Left = 80
              Top = 50
              DataBinding.DataField = 'ID_PESSOA'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 3
              gbTextEdit = descPessoa
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_PESSOA;JOIN_DESCRICAO_NOME_PESSOA'
              gbTableName = 'PESSOA'
              gbCamposConsulta = 'ID;NOME'
              gbIdentificadorConsulta = 'PESSOA_MOV_CONTA_PAGAR'
              Width = 57
            end
            object descPessoa: TgbDBTextEdit
              Left = 134
              Top = 50
              TabStop = False
              DataBinding.DataField = 'JOIN_DESCRICAO_NOME_PESSOA'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbReadyOnly = True
              gbPassword = False
              Width = 412
            end
            object descCentroResultado: TgbDBTextEdit
              Left = 729
              Top = 26
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 13
              gbReadyOnly = True
              gbPassword = False
              Width = 262
            end
            object edtCentroResultado: TgbDBButtonEditFK
              Left = 672
              Top = 26
              DataBinding.DataField = 'ID_CENTRO_RESULTADO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 12
              gbTextEdit = descCentroResultado
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
              gbTableName = 'CENTRO_RESULTADO'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CENTRO_RESULTADO'
              Width = 60
            end
            object descContaAnalise: TgbDBTextEdit
              Left = 729
              Top = 51
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 15
              gbReadyOnly = True
              gbPassword = False
              Width = 262
            end
            object edDescricao: TgbDBTextEdit
              Left = 80
              Top = 25
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbRequired = True
              gbPassword = False
              Width = 466
            end
            object edVlTitulo: TgbDBTextEdit
              Left = 436
              Top = 0
              DataBinding.DataField = 'VL_TITULO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              gbPassword = False
              Width = 110
            end
            object edtObservacao: TgbDBBlobEdit
              Left = 672
              Top = 100
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACAO'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Properties.PopupHeight = 300
              Properties.PopupWidth = 160
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 18
              Width = 319
            end
            object edtFormaPagamento: TgbDBButtonEditFK
              Left = 672
              Top = 2
              DataBinding.DataField = 'ID_FORMA_PAGAMENTO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 10
              gbTextEdit = descFormaPagamento
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_FORMA_PAGAMENTO;JOIN_DESCRICAO_FORMA_PAGAMENTO'
              gbTableName = 'FORMA_PAGAMENTO'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'FORMA_PAGAMENTO'
              Width = 60
            end
            object descFormaPagamento: TgbDBTextEdit
              Left = 729
              Top = 2
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 11
              gbReadyOnly = True
              gbPassword = False
              Width = 262
            end
            object EdtContaAnalise: TcxDBButtonEdit
              Left = 672
              Top = 51
              DataBinding.DataField = 'ID_CONTA_ANALISE'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ClickKey = 13
              Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
              Style.Color = 14606074
              TabOrder = 14
              OnDblClick = EdtContaAnaliseDblClick
              OnExit = EdtContaAnaliseExit
              Width = 60
            end
            object edtDtDocumento: TgbDBDateEdit
              Left = 80
              Top = 100
              DataBinding.DataField = 'DT_DOCUMENTO'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 7
              gbRequired = True
              gbDateTime = False
              Width = 89
            end
            object edtDtCompetencia: TgbDBDateEdit
              Left = 273
              Top = 100
              DataBinding.DataField = 'DT_COMPETENCIA'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 8
              gbRequired = True
              gbDateTime = False
              Width = 89
            end
            object edDtVencimento: TgbDBDateEdit
              Left = 457
              Top = 100
              DataBinding.DataField = 'DT_VENCIMENTO'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnToday]
              Properties.ImmediatePost = True
              Properties.ReadOnly = False
              Properties.SaveTime = False
              Properties.ShowTime = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 9
              gbRequired = True
              gbDateTime = False
              Width = 89
            end
            object edtContaCorrente: TgbDBButtonEditFK
              Left = 672
              Top = 75
              DataBinding.DataField = 'ID_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = clWhite
              TabOrder = 16
              gbTextEdit = descContaCorrente
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
              gbTableName = 'CONTA_CORRENTE'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CONTA_CORRENTE'
              Width = 60
            end
            object descContaCorrente: TgbDBTextEdit
              Left = 729
              Top = 75
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 17
              gbReadyOnly = True
              gbPassword = False
              Width = 262
            end
            object gbDBButtonEditFK1: TgbDBButtonEditFK
              Left = 80
              Top = 75
              DataBinding.DataField = 'ID_CARTEIRA'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 5
              gbTextEdit = gbDBTextEdit1
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CARTEIRA; JOIN_DESCRICAO_CARTEIRA'
              gbTableName = 'CARTEIRA'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CARTEIRA'
              Width = 60
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 137
              Top = 75
              TabStop = False
              DataBinding.DataField = 'JOIN_DESCRICAO_CARTEIRA'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 6
              gbReadyOnly = True
              gbPassword = False
              Width = 409
            end
          end
          object gbPanel3: TgbPanel
            Left = 2
            Top = 293
            Align = alBottom
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            Transparent = True
            Height = 26
            Width = 999
            object Label16: TLabel
              Left = 224
              Top = 7
              Width = 61
              Height = 16
              Caption = 'Total Pago'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label20: TLabel
              Left = 399
              Top = 7
              Width = 65
              Height = 16
              Caption = 'Acr'#233'scimos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label21: TLabel
              Left = 578
              Top = 7
              Width = 58
              Height = 16
              Caption = 'Descontos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label19: TLabel
              Left = 750
              Top = 7
              Width = 122
              Height = 16
              Caption = 'Pagamento L'#237'quido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label15: TLabel
              Left = 7
              Top = 7
              Width = 102
              Height = 16
              Caption = 'Total a Receber'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object edVlRecebido: TgbDBTextEdit
              Left = 289
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_QUITADO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit3: TgbDBTextEdit
              Left = 468
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_ACRESCIMO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit4: TgbDBTextEdit
              Left = 640
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_DECRESCIMO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit8: TgbDBTextEdit
              Left = 876
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_QUITADO_LIQUIDO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object edVlPagar: TgbDBTextEdit
              Left = 113
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_ABERTO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 107
            end
          end
          object edtOrigem: TgbDBTextEdit
            Left = 455
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbPassword = False
            Width = 40
          end
          object edtDescOrigem: TgbDBTextEdit
            Left = 270
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'TP_DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 186
          end
          object edtSequencia: TgbDBTextEdit
            Left = 953
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'SEQUENCIA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 40
          end
          object PnFrameQuitacao: TgbPanel
            Left = 2
            Top = 156
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameQuitacaoContaReceber'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            Transparent = True
            Height = 137
            Width = 999
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    object ActExportarExcel: TAction
      Category = 'filter'
      Caption = 'Exportar para Excel'
      Hint = 'Exportar grade para Excel'
      ImageIndex = 274
      OnExecute = ActExportarExcelExecute
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 123
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          Visible = True
          ItemName = 'lbPagamentoLote'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 755
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 926
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 356
      FloatClientWidth = 171
      FloatClientHeight = 188
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbConsultaVencimento'
        end
        item
          Visible = True
          ItemName = 'EdtConsultaVencimentoInicio'
        end
        item
          Visible = True
          ItemName = 'EdtConsultaVencimentoFim'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 120
          Visible = True
          ItemName = 'filtroPessoa'
        end
        item
          Visible = True
          ItemName = 'filtroNomePessoa'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 111
          Visible = True
          ItemName = 'EdtConsultaSituacao'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbSearch'
        end>
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 834
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object lbPagamentoLote: TdxBarLargeButton [10]
      Action = ActPagamentoLote
      Category = 0
    end
    object filtroPessoa: TcxBarEditItem [18]
      Caption = 'Pessoa'
      Category = 2
      Hint = 'Pessoa'
      Visible = ivAlways
      OnExit = filtroPessoaPropertiesEditValueChanged
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = filtroPessoaPropertiesButtonClick
    end
    object filtroNomePessoa: TdxBarStatic [19]
      Caption = 'Nome da Pessoa 40 Caracteres'
      Category = 2
      Hint = '123456789 123456789 123456789 123456789'
      Visible = ivAlways
      Alignment = taLeftJustify
    end
    object EdtConsultaDocumento: TdxBarEdit [21]
      Caption = 'Documento'
      Category = 4
      Hint = 'Documento'
      Visible = ivAlways
    end
    object EdtConsultaDescricao: TdxBarEdit [22]
      Caption = 'Descri'#231#227'o    '
      Category = 4
      Hint = 'Descri'#231#227'o    '
      Visible = ivAlways
    end
    object EdtConsultaVencimentoInicio: TcxBarEditItem [23]
      Caption = 'In'#237'cio'
      Category = 4
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ShowTime = False
      Properties.UseNullString = True
    end
    object EdtConsultaVencimentoFim: TcxBarEditItem [24]
      Caption = 'Fim   '
      Category = 4
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ShowTime = False
    end
    object EdtConsultaSituacao: TcxBarEditItem [25]
      Caption = 'Situa'#231#227'o'
      Category = 4
      Hint = 'Situa'#231#227'o'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        'TODOS'
        'ABERTO'
        'QUITADO'
        'CANCELADO')
      Properties.PostPopupValueOnTab = True
      Properties.OnEditValueChanged = EdtConsultaSituacaoPropertiesEditValueChanged
    end
    object lbConsultaVencimento: TdxBarStatic [26]
      Align = iaClient
      Caption = 'Vencimento'
      Category = 4
      Hint = 'Vencimento'
      Visible = ivAlways
    end
    object EdtConsultaValor: TcxBarEditItem [27]
      Caption = 'Valor      '
      Category = 4
      Hint = 'Valor      '
      Visible = ivAlways
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = '###,###,###,###,##0.00'
      Properties.EditFormat = '###,###,###,###,##0.00'
    end
  end
  inherited ActionListMain: TActionList
    object ActPagamentoLote: TAction
      Category = 'Conta Pagar'
      Caption = 'Pagamento em Lote'
      ImageIndex = 135
      OnExecute = ActPagamentoLoteExecute
    end
  end
  inherited cxGridPopupMenuPadrao: TcxGridPopupMenu
    PopupMenus = <
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtGridNone, gvhtGridTab, gvhtNone, gvhtTab, gvhtCell, gvhtRecord]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspContaPagar'
    RemoteServer = DmConnection.dspContaPagar
    AfterOpen = cdsDataAfterOpen
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      OnChange = AtualizarVlAberto
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_QUITADO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_ABERTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataQT_PARCELA: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsDataNR_PARCELA: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'Nr. Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsDataDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object cdsDataBO_VENCIDO: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Vencido?'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'Id. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Id. Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Id. Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Id. Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Id. Chave Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataSTATUS: TStringField
      Alignment = taCenter
      DefaultExpression = 'ABERTO'
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField
      DisplayLabel = 'Desc. Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Desc. Conta An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Desc. Centro Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Origem Chave Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Id. Origem Chave Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Desc. Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDatafdqContaPagarQuitacao: TDataSetField
      FieldName = 'fdqContaPagarQuitacao'
    end
    object cdsDataSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
      Size = 10
    end
    object cdsDataVL_ACRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_DECRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DECRESCIMO'
      Origin = 'VL_DECRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Quitado L'#237'quido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
    end
    object cdsDataID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object cdsDataTP_DOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object cdsDataDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_FILIAL: TIntegerField
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataPERC_MULTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_MULTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_JUROS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_JUROS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      ReportDocument.CreationDate = 42210.806333842590000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsContaPagarQuitacao: TGBClientDataSet
    Aggregates = <>
    AggregatesActive = True
    DataSetField = cdsDatafdqContaPagarQuitacao
    Params = <>
    RemoteServer = DmConnection.dspContaPagar
    AfterOpen = cdsContaPagarQuitacaoAfterOpen
    BeforeInsert = cdsContaPagarQuitacaoBeforeInsert
    AfterPost = cdsContaPagarQuitacaoAfterPost
    BeforeDelete = cdsContaPagarQuitacaoBeforeDelete
    AfterDelete = cdsContaPagarQuitacaoAfterDelete
    OnNewRecord = cdsContaPagarQuitacaoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 712
    Top = 80
    object cdsContaPagarQuitacaoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsContaPagarQuitacaoDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsContaPagarQuitacaoID_CONTA_PAGAR: TIntegerField
      DisplayLabel = 'Id. Conta Pagar'
      FieldName = 'ID_CONTA_PAGAR'
      Origin = 'ID_CONTA_PAGAR'
    end
    object cdsContaPagarQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      OnChange = cdsContaPagarQuitacaoVL_QUITACAOChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsContaPagarQuitacaoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'Id. Tipo Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object cdsContaPagarQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
      OnChange = cdsContaPagarQuitacaoDT_QUITACAOChange
    end
    object cdsContaPagarQuitacaoID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsContaPagarQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Id. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object cdsContaPagarQuitacaoVL_DESCONTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      OnChange = CalcularPercentual
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoVL_ACRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      OnChange = CalcularPercentual
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoVL_TOTAL: TFMTBCDField
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      OnChange = CalcularValoresAPartirDoTotalQuitacao
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsContaPagarQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      DisplayLabel = 'Desc. Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsContaPagarQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Id. Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Desc. Conta An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsContaPagarQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object cdsContaPagarQuitacaoTP_DOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      Size = 50
    end
    object cdsContaPagarQuitacaoSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsContaPagarQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField
      DisplayLabel = 'Troco'
      FieldKind = fkInternalCalc
      FieldName = 'CC_VL_TROCO_DIFERENCA'
      DisplayFormat = '###,###,###,##0.00'
    end
    object cdsContaPagarQuitacaoIC_VL_PAGAMENTO: TFloatField
      DisplayLabel = 'Vl. Pagamento'
      FieldKind = fkInternalCalc
      FieldName = 'IC_VL_PAGAMENTO'
      DisplayFormat = '###,###,###,##0.00'
    end
    object cdsContaPagarQuitacaoID_CENTRO_RESULTADO: TIntegerField
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsContaPagarQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsContaPagarQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      DisplayLabel = 'Cart'#227'o: Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsContaPagarQuitacaoID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsContaPagarQuitacaoCHEQUE_SACADO: TStringField
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsContaPagarQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsContaPagarQuitacaoCHEQUE_BANCO: TStringField
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsContaPagarQuitacaoCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsContaPagarQuitacaoCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsContaPagarQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsContaPagarQuitacaoCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      DisplayLabel = 'Tipo Quita'#231#227'o'
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      OnChange = cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAOChange
      Size = 50
    end
    object cdsContaPagarQuitacaoPERC_MULTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      OnChange = TotalizarAcrescimo
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoVL_MULTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      OnChange = TotalizarAcrescimo
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoPERC_JUROS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      OnChange = TotalizarAcrescimo
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoVL_JUROS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      OnChange = TotalizarAcrescimo
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoPERC_DESCONTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      OnChange = CalcularValor
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoPERC_ACRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoVL_TROCO: TFMTBCDField
      DisplayLabel = 'Vl. Troco'
      FieldName = 'VL_TROCO'
      Origin = 'VL_TROCO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsContaPagarQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object cdsContaPagarQuitacaoID_USUARIO_BAIXA: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object cdsContaPagarQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
  end
end
