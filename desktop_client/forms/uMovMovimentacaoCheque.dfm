inherited MovMovimentacaoCheque: TMovMovimentacaoCheque
  Caption = 'Movimenta'#231#227'o de Cheque'
  ClientHeight = 444
  ClientWidth = 870
  ExplicitWidth = 886
  ExplicitHeight = 483
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 870
    ExplicitWidth = 870
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 870
    Height = 317
    Properties.ActivePage = cxtsData
    ExplicitWidth = 870
    ExplicitHeight = 317
    ClientRectBottom = 317
    ClientRectRight = 870
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 870
      ExplicitHeight = 293
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 838
        Height = 293
        ExplicitWidth = 838
        ExplicitHeight = 293
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 293
        Height = 293
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 293
        ExplicitHeight = 293
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 870
      ExplicitHeight = 293
      inherited DesignPanel: TJvDesignPanel
        Width = 870
        Height = 293
        ExplicitWidth = 870
        ExplicitHeight = 293
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 870
          ExplicitHeight = 293
          Height = 293
          Width = 870
          inherited dxBevel1: TdxBevel
            Width = 866
            Height = 54
            Anchors = [akTop, akRight]
            ExplicitLeft = 3
            ExplicitTop = 1
            ExplicitWidth = 875
            ExplicitHeight = 54
          end
          object Label2: TLabel
            Left = 12
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 12
            Top = 33
            Width = 44
            Height = 13
            Caption = 'Cadastro'
          end
          object Label4: TLabel
            Left = 677
            Top = 9
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 679
          end
          object Label5: TLabel
            Left = 677
            Top = 33
            Width = 52
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Finaliza'#231#227'o'
            ExplicitLeft = 679
          end
          object Label3: TLabel
            Left = 195
            Top = 9
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object Label7: TLabel
            Left = 195
            Top = 33
            Width = 36
            Height = 13
            Caption = 'Usu'#225'rio'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 58
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 130
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 58
            Top = 29
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 732
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 130
          end
          object gbDBDateEdit2: TgbDBDateEdit
            Left = 732
            Top = 29
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_FECHAMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object descProcesso: TgbDBTextEdit
            Left = 296
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 375
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 242
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            Width = 55
          end
          object PnDadosMovimentacao: TgbPanel
            Left = 2
            Top = 56
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            Transparent = True
            DesignSize = (
              866
              27)
            Height = 27
            Width = 866
            object lbContaCorrente: TLabel
              Left = 270
              Top = 9
              Width = 86
              Height = 13
              Caption = 'Conta Corrente'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label6: TLabel
              Left = 10
              Top = 9
              Width = 69
              Height = 13
              Caption = 'Movimenta'#231#227'o'
            end
            object Label8: TLabel
              Left = 594
              Top = 9
              Width = 133
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Total da Movimenta'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object gbDBRadioGroup1: TgbDBRadioGroup
              AlignWithMargins = True
              Left = 83
              Top = 1
              DataBinding.DataField = 'TIPO_MOVIMENTO'
              DataBinding.DataSource = dsData
              Properties.Columns = 2
              Properties.ImmediatePost = True
              Properties.Items = <
                item
                  Caption = 'D'#233'bito'
                  Value = 'DEBITO'
                end
                item
                  Caption = 'Transfer'#234'ncia'
                  Value = 'TRANSFERENCIA'
                end>
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Height = 26
              Width = 182
            end
            object edtContaCorrente: TgbDBButtonEditFK
              Left = 357
              Top = 5
              DataBinding.DataField = 'ID_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = clWhite
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              TabOrder = 1
              gbTextEdit = descContaCorrente
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
              gbTableName = 'CONTA_CORRENTE'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CONTA_CORRENTE'
              Width = 58
            end
            object descContaCorrente: TgbDBTextEdit
              Left = 412
              Top = 5
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbReadyOnly = True
              gbPassword = False
              Width = 178
            end
            object gbDBTextEdit5: TgbDBTextEdit
              Left = 730
              Top = 5
              TabStop = False
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'VL_TOTAL_CHEQUE'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbReadyOnly = True
              gbPassword = False
              Width = 130
            end
          end
          object PnFrameConsultaDadosDetalheGradeCheque: TgbPanel
            Left = 2
            Top = 111
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameConsultaDadosDetalheGrade (Cheque)'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            Transparent = True
            Height = 180
            Width = 866
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 296
            Top = 29
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_USUARIO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 375
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 242
            Top = 29
            TabStop = False
            DataBinding.DataField = 'ID_PESSOA_USUARIO_CADASTRO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 55
          end
          object PnContaCorrenteDestino: TgbPanel
            Left = 2
            Top = 83
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            Transparent = True
            DesignSize = (
              866
              28)
            Height = 28
            Width = 866
            object Label9: TLabel
              Left = 8
              Top = 9
              Width = 114
              Height = 13
              Caption = 'Conta Corrente Destino'
            end
            object gbDBButtonEditFK1: TgbDBButtonEditFK
              Left = 125
              Top = 5
              DataBinding.DataField = 'ID_CONTA_CORRENTE_DESTINO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = clWhite
              TabOrder = 0
              gbTextEdit = gbDBTextEdit7
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CONTA_CORRENTE_DESTINO;JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
              gbTableName = 'CONTA_CORRENTE'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'CONTA_CORRENTE'
              Width = 60
            end
            object gbDBTextEdit7: TgbDBTextEdit
              Left = 182
              Top = 5
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbPassword = False
              Width = 678
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbEstornar'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 547
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 718
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 472
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 626
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object lbEstornar: TdxBarLargeButton [11]
      Action = ActEstornar
      Category = 0
    end
    object lbEfetivar: TdxBarLargeButton [12]
      Action = ActEfetivar
      Category = 0
    end
  end
  inherited ActionListMain: TActionList
    object ActEstornar: TAction
      Category = 'Action'
      Caption = 'Estornar F10'
      Hint = 'Estornar a nota fiscal'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = ActEstornarExecute
    end
    object ActEfetivar: TAction
      Category = 'Action'
      Caption = 'Efetivar F9'
      Hint = 'Efetivar a nota fiscal'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspMovimentacaoCheque'
    RemoteServer = DmConnection.dspMovimentacaoCheque
    AfterOpen = cdsDataAfterOpen
    OnNewRecord = cdsDataNewRecord
    gbUsarDefaultExpression = False
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataTIPO_MOVIMENTO: TStringField
      DisplayLabel = 'Tipo de Movimento'
      FieldName = 'TIPO_MOVIMENTO'
      Origin = 'TIPO_MOVIMENTO'
      OnChange = cdsDataTIPO_MOVIMENTOChange
      Size = 35
    end
    object cdsDataID_PESSOA_USUARIO_CADASTRO: TIntegerField
      DisplayLabel = 'Pessoa que Realizou o Cadastro'
      FieldName = 'ID_PESSOA_USUARIO_CADASTRO'
      Origin = 'ID_PESSOA_USUARIO_CADASTRO'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO_EFETIVACAO: TIntegerField
      DisplayLabel = 'Pessoa que Realizou a Efetiva'#231#227'o'
      FieldName = 'ID_PESSOA_USUARIO_EFETIVACAO'
      Origin = 'ID_PESSOA_USUARIO_EFETIVACAO'
    end
    object cdsDataID_PESSOA_USUARIO_REABERTURA: TIntegerField
      DisplayLabel = 'Pessoa que Realizou a Reabertura'
      FieldName = 'ID_PESSOA_USUARIO_REABERTURA'
      Origin = 'ID_PESSOA_USUARIO_REABERTURA'
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object cdsDataID_CONTA_CORRENTE_DESTINO: TIntegerField
      DisplayLabel = 'Conta Corrente Destino'
      FieldName = 'ID_CONTA_CORRENTE_DESTINO'
      Origin = 'ID_CONTA_CORRENTE_DESTINO'
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave do Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Data de Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsDataJOIN_NOME_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_USUARIO'
      Origin = 'JOIN_NOME_USUARIO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Origem'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      OnChange = cdsDataJOIN_DESCRICAO_CONTA_CORRENTEChange
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_DESTINO: TStringField
      DisplayLabel = 'Conta Corrente Destino'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
      Origin = 'JOIN_DESCRICAO_CONTA_CORRENTE_DESTINO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataVL_TOTAL_CHEQUE: TFMTBCDField
      DisplayLabel = 'Total Cheque'
      FieldName = 'VL_TOTAL_CHEQUE'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDatafdqMovimentacaoChequeCheque: TDataSetField
      FieldName = 'fdqMovimentacaoChequeCheque'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42276.891663101850000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsMovimentacaoChequeCheque: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqMovimentacaoChequeCheque
    Params = <>
    AfterOpen = cdsMovimentacaoChequeChequeAfterOpen
    gbUsarDefaultExpression = False
    gbValidarCamposObrigatorios = True
    Left = 768
    Top = 80
    object cdsMovimentacaoChequeChequeID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsMovimentacaoChequeChequeID_MOVIMENTACAO_CHEQUE: TIntegerField
      DisplayLabel = 'C'#243'digo da Movimenta'#231#227'o do Cheque'
      FieldName = 'ID_MOVIMENTACAO_CHEQUE'
      Origin = 'ID_MOVIMENTACAO_CHEQUE'
    end
    object cdsMovimentacaoChequeChequeID_CHEQUE: TIntegerField
      DisplayLabel = 'C'#243'digo do Cheque'
      FieldName = 'ID_CHEQUE'
      Origin = 'ID_CHEQUE'
      Required = True
    end
    object cdsMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente Movimento Anterior'
      FieldName = 'ID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR'
      Origin = 'ID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR'
    end
    object cdsMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ATUAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente Movimento Atual'
      FieldName = 'ID_CONTA_CORRENTE_MOVIMENTO_ATUAL'
      Origin = 'ID_CONTA_CORRENTE_MOVIMENTO_ATUAL'
    end
  end
end
