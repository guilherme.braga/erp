inherited CadEstado: TCadEstado
  Caption = 'Cadastro de Estado'
  ClientWidth = 902
  ExplicitWidth = 918
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 902
    ExplicitWidth = 902
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 902
    Properties.ActivePage = cxtsData
    ExplicitWidth = 902
    ClientRectRight = 902
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 902
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 902
        ExplicitWidth = 902
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 902
      inherited DesignPanel: TJvDesignPanel
        Width = 902
        ExplicitWidth = 902
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 902
          Width = 902
          inherited dxBevel1: TdxBevel
            Width = 898
            ExplicitWidth = 898
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label1: TLabel
            Left = 844
            Top = 40
            Width = 13
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'UF'
            ExplicitLeft = 876
          end
          object Label2: TLabel
            Left = 8
            Top = 65
            Width = 19
            Height = 13
            Caption = 'Pa'#237's'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 60
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 60
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 778
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 863
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'UF'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 30
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 117
            Top = 62
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_PAIS'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 776
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 60
            Top = 62
            DataBinding.DataField = 'ID_PAIS'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 3
            gbTextEdit = gbDBTextEdit3
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PAIS;JOIN_DESCRICAO_PAIS'
            gbTableName = 'PAIS'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 60
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Estado'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspEstado'
    RemoteServer = DmConnection.dspLocalidade
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataUF: TStringField
      FieldName = 'UF'
      Origin = 'UF'
      Required = True
      Size = 2
    end
    object cdsDataID_PAIS: TIntegerField
      DisplayLabel = 'C'#243'digo do Pa'#237's'
      FieldName = 'ID_PAIS'
      Origin = 'ID_PAIS'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_PAIS: TStringField
      DisplayLabel = 'Pa'#237's'
      FieldName = 'JOIN_DESCRICAO_PAIS'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
