unit uPesqCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, uFrameConsultaDadosDetalheGrade, db;

type TTipoChequeConsulta = (todos, proprio, terceiro);

type
  TPesqCheque = class(TFrmModalPadrao)
  private
    FFrameConsultaDadosDetalheGradeCheque: TFrameConsultaDadosDetalheGrade;
    procedure DefinirFiltros;
    procedure CriarFiltros;
    procedure DefinirFiltroPesquisaChequeTerceiro;
    procedure DefinirFiltroPesquisaID(AValorDefault: String = '');
    procedure AjustarRotuloDoFormularioPesquisaChequeTerceiro;
    procedure ExibirFormularioEmModal;
    class procedure InstanciarFormulario;
    class procedure LimparInstanciaFormulario;
    procedure InstanciarFrameConsultaDadosDetalheGradeCheque(ACriarTodosFiltros: Boolean = true);
  public
    class function ExecutarConsulta(AFieldCodigo: TField; ATipoCheque: TTipoChequeConsulta = todos): Boolean;
    class procedure ExecutarConsultaOculta(AFieldCodigo: TField; ATipoCheque: TTipoChequeConsulta = todos);
  end;

var
  PesqCheque: TPesqCheque;

implementation

{$R *.dfm}

uses uFrameFiltroVerticalPadrao, uContaCorrenteProxy, uChequeProxy;

{ TPesqCheque }

procedure TPesqCheque.AjustarRotuloDoFormularioPesquisaChequeTerceiro;
begin
  PesqCheque.Caption := 'Pesquisa de Cheque de Terceiro';
end;

procedure TPesqCheque.CriarFiltros;
begin
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.CriarFiltros;
end;

procedure TPesqCheque.DefinirFiltroPesquisaChequeTerceiro;
var
  campoFiltro: TcampoFiltro;
begin
  //Tipo de Cheque
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo de Cheque';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  campoFiltro.valorPadrao := TChequeProxy.TIPO_CHEQUE_TERCEIRO;
  campoFiltro.somenteLeitura := true;
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Cheque Utilizado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Cheque Utilizado';
  campoFiltro.campo := 'BO_CHEQUE_UTILIZADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  campoFiltro.valorPadrao := 'N';
  campoFiltro.somenteLeitura := true;
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TPesqCheque.DefinirFiltroPesquisaID(AValorDefault: String = '');
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);

  if not AValorDefault.IsEmpty then
  begin
    campoFiltro.valorPadrao := AValorDefault;
  end;

  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TPesqCheque.DefinirFiltros;
var
  campoFiltro: TcampoFiltro;
begin
  DefinirFiltroPesquisaID;

  //Sacado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Sacado';
  campoFiltro.campo := 'SACADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento Federal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Federal (CPF/CNPJ)';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_CHEQUE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Banco
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Banco';
  campoFiltro.campo := 'BANCO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'CONTA_CORRENTE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Emiss�o
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Emiss�o';
  campoFiltro.campo := 'DT_EMISSAO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO'; //CONFIRMAR
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //N�mero do Cheque
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'N�mero do Cheque';
  campoFiltro.campo := 'NUMERO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

   //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conciliado';
  campoFiltro.campo := 'BO_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TConstantesCampoFiltro.ITENS_BO;
  campoFiltro.campoCaixaSelecao.valores := TConstantesCampoFiltro.VALORES_BO;
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DH_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //C�digo do Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo do Documento de Origem';
  campoFiltro.campo := 'ID_DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento de Origem';
  campoFiltro.campo := 'DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TContaCorrenteMovimento.LISTA_ORIGEM;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.FCampos.Add(campoFiltro);
end;

class function TPesqCheque.ExecutarConsulta(AFieldCodigo: TField;
  ATipoCheque: TTipoChequeConsulta = todos): Boolean;
begin
  if not(AFieldCodigo.Dataset.State in dsEditModes) then
    Exit;

  try
    TPesqCheque.InstanciarFormulario;
     PesqCheque.AjustarRotuloDoFormularioPesquisaChequeTerceiro;
    PesqCheque.InstanciarFrameConsultaDadosDetalheGradeCheque;

    if ATipoCheque = TTipoChequeConsulta(terceiro) then
    begin
      PesqCheque.DefinirFiltroPesquisaChequeTerceiro;
    end;

    PesqCheque.CriarFiltros;

    PesqCheque.ShowModal;
    if (PesqCheque.result = MCONFIRMED) then
    begin
      result := true;

      if not PesqCheque.FFrameConsultaDadosDetalheGradeCheque.fdmDados.IsEmpty then
      begin
        AFieldCodigo.AsInteger :=
          PesqCheque.FFrameConsultaDadosDetalheGradeCheque.fdmDados.FieldByName('ID').AsInteger;
      end;
    end
    else
    begin
      result := false;
    end;
  finally
    TPesqCheque.LimparInstanciaFormulario;
  end;
end;

class procedure TPesqCheque.ExecutarConsultaOculta(AFieldCodigo: TField;
  ATipoCheque: TTipoChequeConsulta = todos);
begin
  if not(AFieldCodigo.Dataset.State in dsEditModes) then
    Exit;

  try
    TPesqCheque.InstanciarFormulario;
    PesqCheque.InstanciarFrameConsultaDadosDetalheGradeCheque(false);

    if ATipoCheque = TTipoChequeConsulta(terceiro) then
    begin
      PesqCheque.DefinirFiltroPesquisaChequeTerceiro;
    end;

    PesqCheque.DefinirFiltroPesquisaID(AFieldCodigo.AsString);

    PesqCheque.CriarFiltros;

    PesqCheque.FFrameConsultaDadosDetalheGradeCheque.Consultar;

    if PesqCheque.FFrameConsultaDadosDetalheGradeCheque.fdmDados.IsEmpty then
    begin
      AFieldCodigo.Clear;
    end;
  finally
    TPesqCheque.LimparInstanciaFormulario;
  end;
end;

procedure TPesqCheque.ExibirFormularioEmModal;
begin
  PesqCheque.ShowModal;
end;

class procedure TPesqCheque.InstanciarFormulario;
begin
  Application.CreateForm(TPesqCheque, PesqCheque);
end;

procedure TPesqCheque.InstanciarFrameConsultaDadosDetalheGradeCheque(ACriarTodosFiltros: Boolean = true);
begin
  FFrameConsultaDadosDetalheGradeCheque := TFrameConsultaDadosDetalheGrade.Create(
    cxGroupBox2, Self.Name, 'Cheque');

  FFrameConsultaDadosDetalheGradeCheque.OcultarTituloFrame;
  FFrameConsultaDadosDetalheGradeCheque.OcultarCampoImpressao;

  if ACriarTodosFiltros then
  begin
    PesqCheque.DefinirFiltros;
  end;

  FFrameConsultaDadosDetalheGradeCheque.Parent := cxGroupBox2;
end;

class procedure TPesqCheque.LimparInstanciaFormulario;
begin
  FreeAndNil(PesqCheque);
end;

end.

