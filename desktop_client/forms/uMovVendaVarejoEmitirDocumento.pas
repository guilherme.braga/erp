unit uMovVendaVarejoEmitirDocumento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmEmissaoDocumentoFechamento, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.StdCtrls, cxButtons, cxGroupBox, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, uVendaProxy;

type
  TMovVendaVarejoEmitirDocumento = class(TFrmEmissaoDocumentoFechamento)
  private
    FVendaProxy: TVendaProxy;
  public
    class procedure EmissaoDocumentoVenda(AIdPK: Integer; AVendaProxy: TVendaProxy);
  protected
    procedure AoCriarFormulario; override;
    procedure EmissaoNFCe; override;
    procedure EmissaoNFe; override;
  end;

implementation

{$R *.dfm}

uses uTControl_Function, uMovVendaVarejo, uNotaFiscal, uNotaFiscalProxy, uVenda;

{ TMovVendaVarejoEmitirDocumento }

procedure TMovVendaVarejoEmitirDocumento.AoCriarFormulario;
var
  frmVendaVarejo: TMovVendaVarejo;
begin
  inherited;
  frmVendaVarejo := (TControlFunction.GetInstance('TMovVendaVarejo') as TMovVendaVarejo);

  if not frmVendaVarejo.FParametroHabilitarEmissaoNFCE.ValorSim then
  begin
    Self.DesabilitarNFCe;
  end;

  if not frmVendaVarejo.FParametroHabilitarEmissaoNFE.ValorSim then
  begin
    Self.DesabilitarNFe;
  end;
end;

procedure TMovVendaVarejoEmitirDocumento.EmissaoNFCe;
begin
  inherited;
  TNotaFiscal.GerarNFCEDaVenda(FIdPK, FCPFNaNota, FVendaProxy);
  if TNotaFiscal.EmitirNFCE(TVenda.GetIdNotaFiscalNFCE(FIdPK)) then
  begin
    TNotaFiscal.ImprimirNotaFiscal(TVenda.GetIdNotaFiscalNFCE(FIdPK),
      TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
  end;
end;

procedure TMovVendaVarejoEmitirDocumento.EmissaoNFe;
begin
  inherited;
  TNotaFiscal.GerarNFEDaVenda(FIdPK, FVendaProxy);
  if TNotaFiscal.EmitirNFE(TVenda.GetIdNotaFiscalNFE(FIdPK)) then
  begin
    TNotaFiscal.ImprimirNotaFiscal(TVenda.GetIdNotaFiscalNFE(FIdPK),
      TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
  end;
end;

class procedure TMovVendaVarejoEmitirDocumento.EmissaoDocumentoVenda(AIdPK: Integer; AVendaProxy: TVendaProxy);
var
  FrmEmissaoDocumento: TMovVendaVarejoEmitirDocumento;
begin
  Application.CreateForm(TMovVendaVarejoEmitirDocumento, FrmEmissaoDocumento);
  try
    FrmEmissaoDocumento.AoCriarFormulario;
    FrmEmissaoDocumento.FIdPk := AIdPK;
    FrmEmissaoDocumento.FVendaProxy := AVendaProxy;
    FrmEmissaoDocumento.ShowModal;
  finally
    FreeAndNil(FrmEmissaoDocumento);
  end;
end;

end.
