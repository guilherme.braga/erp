inherited CadMedico: TCadMedico
  Caption = 'Cadastro de M'#233'dico'
  ClientWidth = 832
  ExplicitWidth = 848
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 832
    ExplicitWidth = 832
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 832
    Properties.ActivePage = cxtsData
    ExplicitWidth = 832
    ClientRectRight = 832
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 832
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 832
        ExplicitWidth = 832
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 832
      inherited DesignPanel: TJvDesignPanel
        Width = 832
        ExplicitWidth = 832
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 832
          Width = 832
          inherited dxBevel1: TdxBevel
            Width = 828
            ExplicitWidth = 891
          end
          object Label6: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object PnLogradouro: TcxGroupBox
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.TransparentBorder = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              828
              215)
            Height = 215
            Width = 828
            object lbComplemento: TLabel
              Left = 8
              Top = 56
              Width = 65
              Height = 13
              Caption = 'Complemento'
            end
            object lbLogradouro: TLabel
              Left = 168
              Top = 31
              Width = 55
              Height = 13
              Caption = 'Logradouro'
            end
            object lnNumero: TLabel
              Left = 442
              Top = 31
              Width = 37
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'N'#250'mero'
              ExplicitLeft = 546
            end
            object lbCEP: TLabel
              Left = 8
              Top = 31
              Width = 19
              Height = 13
              Caption = 'CEP'
            end
            object Label1: TLabel
              Left = 538
              Top = 31
              Width = 28
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Bairro'
              ExplicitLeft = 642
            end
            object lbCidadeEmpresa: TLabel
              Left = 538
              Top = 56
              Width = 33
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Cidade'
              ExplicitLeft = 642
            end
            object Label2: TLabel
              Left = 8
              Top = 6
              Width = 33
              Height = 13
              Caption = 'M'#233'dico'
            end
            object Label3: TLabel
              Left = 710
              Top = 6
              Width = 22
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'CRM'
              ExplicitLeft = 814
            end
            object Label4: TLabel
              Left = 8
              Top = 81
              Width = 42
              Height = 13
              Caption = 'Telefone'
            end
            object Label5: TLabel
              Left = 168
              Top = 81
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object edtLogradouro: TgbDBTextEdit
              Left = 227
              Top = 27
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'LOGRADOURO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbPassword = False
              Width = 210
            end
            object edtNumero: TgbDBTextEdit
              Left = 483
              Top = 27
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'NUMERO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbPassword = False
              Width = 49
            end
            object edtIDCEP: TgbDBButtonEditFK
              Left = 74
              Top = 27
              DataBinding.DataField = 'CEP'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 112
              Style.Color = clWhite
              TabOrder = 2
              gbCampoPK = 'CEP'
              gbCamposRetorno = 'CEP'
              gbTableName = 'CEP'
              gbCamposConsulta = 'CEP'
              gbDepoisDeConsultar = edtIDCEPgbDepoisDeConsultar
              Width = 90
            end
            object edtCidade: TgbDBButtonEditFK
              Left = 575
              Top = 52
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'ID_CIDADE'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Style.Color = clWhite
              TabOrder = 7
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_CIDADE_EMPRESA;JOIN_DESCRICAO_CIDADE_EMPRESA'
              gbTableName = 'CIDADE'
              gbCamposConsulta = 'ID;DESCRICAO'
              Width = 62
            end
            object edtComplemento: TgbDBTextEdit
              Left = 74
              Top = 52
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'COMPLEMENTO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 6
              gbPassword = False
              Width = 458
            end
            object gbDBTextEdit2: TgbDBTextEdit
              Left = 634
              Top = 52
              TabStop = False
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 8
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 183
            end
            object gbDBTextEdit3: TgbDBTextEdit
              Left = 74
              Top = 2
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'NOME'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 630
            end
            object gbDBTextEdit4: TgbDBTextEdit
              Left = 738
              Top = 2
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'CRM'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbPassword = False
              Width = 79
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 575
              Top = 27
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'BAIRRO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 5
              gbPassword = False
              Width = 242
            end
            object gbDBTextEdit5: TgbDBTextEdit
              Left = 74
              Top = 77
              DataBinding.DataField = 'TELEFONE'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 9
              gbPassword = False
              Width = 90
            end
            object EdtObservacao: TgbDBBlobEdit
              Left = 227
              Top = 77
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACAO'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 10
              Width = 590
            end
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 76
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspMedico'
    RemoteServer = DmConnection.dspMedico
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object cdsDataNUMERO: TIntegerField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object cdsDataCOMPLEMENTO: TStringField
      DisplayLabel = 'Complemento'
      FieldName = 'COMPLEMENTO'
      Origin = 'COMPLEMENTO'
      Size = 50
    end
    object cdsDataBAIRRO: TStringField
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      Size = 100
    end
    object cdsDataCEP: TStringField
      FieldName = 'CEP'
      Origin = 'CEP'
      Size = 15
    end
    object cdsDataLOGRADOURO: TStringField
      DisplayLabel = 'Logradouro'
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      Size = 80
    end
    object cdsDataID_CIDADE: TIntegerField
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
    end
    object cdsDataJOIN_DESCRICAO_CIDADE: TStringField
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataTELEFONE: TStringField
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE'
      Origin = 'TELEFONE'
      Size = 11
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataCRM: TStringField
      FieldName = 'CRM'
      Origin = 'CRM'
      Size = 11
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42234.098911944440000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
