unit uCadRelatorioFR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, dxBarExtItems, cxLabel, cxDBLabel,
  uFrameDetailPadrao, uFrameRelatorioFRFormulario, uGBPanel, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  cxSplitter, JvExControls, JvButton, JvTransparentButton, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter;

type
  TCadRelatorioFR = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataNOME: TStringField;
    cdsDataDESCRICAO: TStringField;
    cdsDataARQUIVO: TBlobField;
    cdsDataOBSERVACOES: TBlobField;
    cdsDatafdqRelatorioFRFormulario: TDataSetField;
    cdsRelatorioFRFormulario: TGBClientDataSet;
    cdsRelatorioFRFormularioID: TAutoIncField;
    cdsRelatorioFRFormularioNOME: TStringField;
    cdsRelatorioFRFormularioDESCRICAO: TStringField;
    cdsRelatorioFRFormularioACAO: TStringField;
    cdsRelatorioFRFormularioID_RELATORIO_FR: TIntegerField;
    cdsRelatorioFRFormularioID_PESSOA_USUARIO: TIntegerField;
    Label1: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    cdsRelatorioFRFormularioJOIN_USUARIO_PESSOA_USUARIO: TStringField;
    ActUploadRelatorio: TAction;
    ActRemoverRelatorio: TAction;
    lbAdicionarArquivoFR3: TdxBarLargeButton;
    lbRemoverArquivoFR3: TdxBarLargeButton;
    dxBarStatic1: TdxBarStatic;
    odArquivo: TFileOpenDialog;
    cxDBLabel1: TcxDBLabel;
    cdsDataCC_ARQUIVO_CARREGADO: TStringField;
    gbPanel1: TgbPanel;
    Label7: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    EdtNomeRelatorio: TgbDBTextEdit;
    gbDBBlobEdit1: TgbDBBlobEdit;
    EdtDescricaoRelatorio: TgbDBTextEdit;
    cdsRelatorioFRFormularioIMPRESSORA: TStringField;
    cdsRelatorioFRFormularioNUMERO_COPIAS: TIntegerField;
    PnFrameRelatorioFRFormulario: TgbPanel;
    cdsRelatorioFRFormularioFILTROS_PERSONALIZADOS: TBlobField;
    cdsDataTIPO_ARQUIVO: TStringField;
    procedure ActUploadRelatorioExecute(Sender: TObject);
    procedure cdsDataCalcFields(DataSet: TDataSet);
    procedure ActRemoverRelatorioExecute(Sender: TObject);
    procedure cdsRelatorioFRFormularioAfterOpen(DataSet: TDataSet);
    procedure cdsRelatorioFRFormularioNewRecord(DataSet: TDataSet);
  private
    FFrameRelatorioFRFormulario: TFrameRelatorioFRFormulario;

    procedure GerenciarVisibilidadeRelatorio;
  public
    function VerificarImpressaoZebra: Boolean;
  protected
    procedure GerenciarControles;override;
    procedure AoCriarFormulario;override;
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uTFunction, uTMessage, uRelatorioFR, uSistema;

procedure TCadRelatorioFR.ActRemoverRelatorioExecute(Sender: TObject);
begin
  inherited;
  cdsDataARQUIVO.Clear;
  cdsDataTIPO_ARQUIVO.Clear;

  GerenciarVisibilidadeRelatorio;
end;

procedure TCadRelatorioFR.ActUploadRelatorioExecute(Sender: TObject);
begin
  if odArquivo.Execute then
  begin
    if FileExists(odArquivo.FileName) then
    begin
      cdsDataARQUIVO.LoadFromFile(odArquivo.FileName);

      if Pos('.FR3', UpperCase(odArquivo.FileName)) > 0 then
      begin
        cdsDataTIPO_ARQUIVO.AsString := TRelatorioFR.TIPO_ARQUIVO_FAST_REPORT
      end
      else
      begin
        cdsDataTIPO_ARQUIVO.AsString := TRelatorioFR.TIPO_ARQUIVO_ZEBRA;
      end;

      GerenciarVisibilidadeRelatorio;
    end;
  end;
end;

procedure TCadRelatorioFR.AoCriarFormulario;
begin
  inherited;
  EdtNomeRelatorio.Properties.CharCase := ecNormal;
  EdtDescricaoRelatorio.Properties.CharCase := ecNormal;
end;

procedure TCadRelatorioFR.cdsDataCalcFields(DataSet: TDataSet);
begin
  inherited;
  cdsDataCC_ARQUIVO_CARREGADO.AsString := TFunction.IIF(cdsDataARQUIVO.AsString.IsEmpty,
    'REGISTRO SEM ARQUIVO DE RELATÓRIO ANEXADO', 'ARQUIVO ANEXADO');
end;

procedure TCadRelatorioFR.cdsRelatorioFRFormularioAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FFrameRelatorioFRFormulario.SetConfiguracoesIniciais(cdsRelatorioFRFormulario);
end;

procedure TCadRelatorioFR.cdsRelatorioFRFormularioNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsRelatorioFRFormularioID_PESSOA_USUARIO.AsInteger := TSistema.Sistema.usuario.idUsuarioPerfil;
end;

procedure TCadRelatorioFR.GerenciarControles;
begin
  inherited;
  GerenciarVisibilidadeRelatorio;
end;

procedure TCadRelatorioFR.GerenciarVisibilidadeRelatorio;
var emModoDeEdicao: Boolean;
    existeArquivoAnexado: Boolean;
begin
  emModoDeEdicao := cdsData.State in dsEditModes;
  existeArquivoAnexado := cdsData.Active and not(cdsDataARQUIVO.AsString.IsEmpty);

  ActUploadRelatorio.Visible := not(existeArquivoAnexado) and emModoDeEdicao;
  ActRemoverRelatorio.Visible := existeArquivoAnexado and emModoDeEdicao;
end;

procedure TCadRelatorioFR.InstanciarFrames;
begin
  inherited;
  //Usuario x Relatório x Formulário
  FFrameRelatorioFRFormulario := TFrameRelatorioFRFormulario.Create(PnFrameRelatorioFRFormulario);
  FFrameRelatorioFRFormulario.Parent := PnFrameRelatorioFRFormulario;
  FFrameRelatorioFRFormulario.Align := alClient;
end;

function TCadRelatorioFR.VerificarImpressaoZebra: Boolean;
begin
  result := cdsDataTIPO_ARQUIVO.AsString.Equals(TRelatorioFR.TIPO_ARQUIVO_ZEBRA);
end;

initialization
  RegisterClass(TCadRelatorioFR);
finalization
  UnRegisterClass(TCadRelatorioFR);


end.
