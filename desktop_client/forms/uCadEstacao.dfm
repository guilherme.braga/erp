inherited CadEstacao: TCadEstacao
  Caption = 'Cadastro de Esta'#231#227'o'
  ClientHeight = 451
  ClientWidth = 914
  ExplicitWidth = 930
  ExplicitHeight = 490
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 914
    ExplicitWidth = 914
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 914
    Height = 324
    ExplicitWidth = 914
    ExplicitHeight = 324
    ClientRectBottom = 324
    ClientRectRight = 914
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 914
      ExplicitHeight = 300
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 914
        Height = 300
        ExplicitWidth = 914
        ExplicitHeight = 300
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 914
      ExplicitHeight = 300
      inherited DesignPanel: TJvDesignPanel
        Width = 914
        Height = 300
        ExplicitWidth = 914
        ExplicitHeight = 300
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 914
          ExplicitHeight = 300
          DesignSize = (
            914
            300)
          Height = 300
          Width = 914
          inherited dxBevel1: TdxBevel
            Width = 910
            Height = 30
            ExplicitWidth = 910
            ExplicitHeight = 30
          end
          object Label1: TLabel
            Left = 8
            Top = 6
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 38
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
            FocusControl = cxDBTextEdit1
          end
          object Label3: TLabel
            Left = 455
            Top = 88
            Width = 97
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Sistema Operacional'
            FocusControl = cxDBTextEdit2
          end
          object Label4: TLabel
            Left = 8
            Top = 88
            Width = 38
            Height = 13
            Caption = 'Esta'#231#227'o'
            FocusControl = cxDBTextEdit3
          end
          object Label5: TLabel
            Left = 751
            Top = 88
            Width = 58
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Endere'#231'o IP'
            FocusControl = cxDBTextEdit4
          end
          object Label6: TLabel
            Left = 8
            Top = 113
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object Label7: TLabel
            Left = 8
            Top = 63
            Width = 34
            Height = 13
            Caption = 'Pessoa'
          end
          object cxDBTextEdit1: TgbDBTextEdit
            Left = 71
            Top = 34
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 834
          end
          object cxDBTextEdit2: TgbDBTextEdit
            Left = 556
            Top = 84
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'SISTEMA_OPERACIONAL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbPassword = False
            Width = 191
          end
          object cxDBTextEdit3: TgbDBTextEdit
            Left = 72
            Top = 84
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'TV'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbPassword = False
            Width = 377
          end
          object cxDBTextEdit4: TgbDBTextEdit
            Left = 813
            Top = 84
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ENDERECO_IP'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbPassword = False
            Width = 92
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 71
            Top = 3
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 71
            Top = 59
            DataBinding.DataField = 'ID_PESSOA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = cxDBTextEdit1
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
            gbTableName = 'PESSOA'
            gbCamposConsulta = 'ID;NOME'
            Width = 66
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 134
            Top = 59
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_PESSOA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 771
          end
          object gbDBBlobEdit1: TgbDBBlobEdit
            Left = 72
            Top = 109
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            Width = 833
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Esta'#231#227'o'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftWideString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspEstacao'
    RemoteServer = DmConnection.dspIndoor
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo da Esta'#231#227'o'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataSISTEMA_OPERACIONAL: TStringField
      DisplayLabel = 'Sistema Operacional'
      FieldName = 'SISTEMA_OPERACIONAL'
      Origin = 'SISTEMA_OPERACIONAL'
      Size = 255
    end
    object cdsDataTV: TStringField
      FieldName = 'TV'
      Origin = 'TV'
      Size = 255
    end
    object cdsDataENDERECO_IP: TStringField
      DisplayLabel = 'Endere'#231'o IP'
      FieldName = 'ENDERECO_IP'
      Origin = 'ENDERECO_IP'
      Size = 12
    end
    object cdsDataOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 5000
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'Id. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Desc. Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
end
