inherited CadCNAE: TCadCNAE
  Caption = 'Cadastro CNAE - Classifica'#231#227'o Nacional de Atividades Econ'#244'micas'
  ClientWidth = 1013
  ExplicitWidth = 1029
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1013
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1013
    Properties.ActivePage = cxtsData
    ClientRectRight = 1013
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 936
      ExplicitHeight = 271
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1013
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        Width = 1013
        inherited cxGBDadosMain: TcxGroupBox
          Width = 1013
          inherited dxBevel1: TdxBevel
            Width = 1009
          end
          object lbCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object lbDescricao: TLabel
            Left = 271
            Top = 43
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object lbSequencia: TLabel
            Left = 93
            Top = 43
            Width = 49
            Height = 13
            Caption = 'Sequ'#234'ncia'
          end
          object Label1: TLabel
            Left = 10
            Top = 43
            Width = 29
            Height = 13
            Caption = 'Se'#231#227'o'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 43
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edtDescricao: TgbDBTextEdit
            Left = 321
            Top = 39
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            gbPassword = False
            Width = 680
          end
          object edtSequencia: TgbDBTextEdit
            Left = 146
            Top = 39
            DataBinding.DataField = 'SEQUENCIA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 121
          end
          object gbDBComboBox1: TgbDBComboBox
            Left = 43
            Top = 39
            DataBinding.DataField = 'SECAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.DropDownListStyle = lsEditFixedList
            Properties.ImmediatePost = True
            Properties.Items.Strings = (
              'A'
              'B'
              'C'
              'D'
              'E'
              'F'
              'G'
              'H'
              'I'
              'J'
              'K'
              'L'
              'M'
              'N'
              'O'
              'P'
              'Q'
              'R'
              'S'
              'T'
              'U'
              'V'
              'X'
              'Y'
              'Z')
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            Width = 46
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspCNAE'
    RemoteServer = DmConnection.dspCNAE
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Required = True
      OnChange = cdsDataSEQUENCIAChange
    end
    object cdsDataNIVEL: TIntegerField
      DisplayLabel = 'N'#237'vel'
      FieldName = 'NIVEL'
      Origin = 'NIVEL'
      Required = True
    end
    object cdsDataSECAO: TStringField
      DefaultExpression = 'A'
      DisplayLabel = 'Se'#231#227'o'
      FieldName = 'SECAO'
      Origin = 'SECAO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
end
