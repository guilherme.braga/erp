unit uCadMidia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxDropDownEdit, cxCalendar,
  cxDBEdit, cxMaskEdit, cxSpinEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  uGBDBSpinEdit, uGBDBDateEdit, uGBDBTextEdit, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP,
  Vcl.Buttons, JvComponentBase, JvThread, JvThreadDialog, IdZLibCompressorBase,
  IdCompressorZLib, IdAntiFreezeBase, Vcl.IdAntiFreeze, cxProgressBar, Vcl.Grids,
  uTFTP, cxBarEditItem, dxBarExtItems, cxButtonEdit, Vcl.ComCtrls, IdGlobalProtocols;

type
  TCadMidia = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataNOME: TStringField;
    cdsDataDESCRICAO: TStringField;
    cdsDataTIPO: TStringField;
    cdsDataTAMANHO: TFMTBCDField;
    cdsDataDURACAO: TIntegerField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataENDERECO_FTP: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBSpinEdit1: TgbDBSpinEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    odArquivo: TFileOpenDialog;
    dxBarFTP: TdxBar;
    actListFTP: TActionList;
    actAnexarArquivo: TAction;
    actRemoverArquivo: TAction;
    actCancelarEnvio: TAction;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    progressbaritem: TcxBarEditItem;
    cxBarEditItem1: TcxBarEditItem;
    dxBarProgressItem1: TdxBarProgressItem;
    dxBarProgressItem2: TdxBarProgressItem;
    cxBarEditItem2: TcxBarEditItem;
    procedure actAnexarArquivoExecute(Sender: TObject);
    procedure actRemoverArquivoExecute(Sender: TObject);
    procedure actCancelarEnvioExecute(Sender: TObject);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsDataBeforeClose(DataSet: TDataSet);
    procedure cdsDataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FTP : TFTP;
    procedure ValidarSePossuiArquivoAnexo;
    procedure ValidarSeTransferenciaEmAndamento;

  public
    { Public declarations }

  protected
    procedure GerenciarControles; override;
    procedure AntesDeConfirmar;   override;
    procedure AntesDeCancelar;    override;
    procedure AntesDeRemover;     override;
  end;

implementation

{$R *.dfm}

uses
  uTMessage,
  uFrmMessage_Process;

procedure TCadMidia.actAnexarArquivoExecute(Sender: TObject);
begin
  inherited;

  if cdsDataENDERECO_FTP.AsString <> '' then
  begin
    TMessage.MessageFailure('Este registro j� possui um Arquivo Anexo.' +
                           'Por favor, remova o Arquivo existente antes de continuar.');
    Abort;
  end;

  if odArquivo.Execute then
  begin
    if FileExists(odArquivo.FileName) then
    begin
      cdsDataTAMANHO.AsFloat := FileSizeByName(odArquivo.FileName)/1024;
      try
        FTP.EnviarArquivo(odArquivo.FileName);
        cdsDataENDERECO_FTP.AsString := ExtractFileName(odArquivo.FileName);
      except
        on e : exception do
        begin
          TMessage.MessageFailure(e.Message);
        end;
      end;
    end;
  end;
end;

procedure TCadMidia.actCancelarEnvioExecute(Sender: TObject);
begin
  inherited;
  if FTP.Connected then
  begin
    FTP.Cancelar;
  end;
end;

procedure TCadMidia.actRemoverArquivoExecute(Sender: TObject);
begin
  inherited;
  if (cdsDataENDERECO_FTP.AsString <> '') then
  begin
    try
      FTP.RemoverArquivo(cdsDataENDERECO_FTP.AsString);
      cdsDataENDERECO_FTP.AsString := '';
    except
      on e : exception do
      begin
        TMessage.MessageFailure(e.Message);
      end;
    end;
  end;
end;

procedure TCadMidia.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FTP := TFTP.Create(progressbaritem);
  FTP.ConfigurarFTP;
end;

procedure TCadMidia.cdsDataBeforeClose(DataSet: TDataSet);
begin
  inherited;
  FreeAndNil(FTP);
end;

procedure TCadMidia.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('DH_CADASTRO').AsDateTime := Now;;
end;

procedure TCadMidia.AntesDeCancelar;
begin
  inherited;
  if cdsData.State in [dsEdit] then
  begin
    ValidarSePossuiArquivoAnexo;
  end;
  ValidarSeTransferenciaEmAndamento;
end;

procedure TCadMidia.AntesDeConfirmar;
begin
  inherited;
  ValidarSePossuiArquivoAnexo;
  ValidarSeTransferenciaEmAndamento;
end;

procedure TCadMidia.AntesDeRemover;
begin
  inherited;

  ValidarSeTransferenciaEmAndamento;
  if (cdsDataENDERECO_FTP.AsString <> '') then
  begin
    try
      FTP.RemoverArquivo(cdsDataENDERECO_FTP.AsString);
    except
      on e : exception do
      begin
        TMessage.MessageFailure(e.Message);
      end;
    end;
  end;

end;

procedure TCadMidia.ValidarSePossuiArquivoAnexo;
begin
  if cdsDataENDERECO_FTP.AsString = '' then
  begin
    TMessage.MessageFailure('� necess�rio Anexar um Arquivo de M�dia para este Registro.' +
                           'Por favor, verifique.');
    Abort;
  end;
end;

procedure TCadMidia.ValidarSeTransferenciaEmAndamento;
begin
  if FTP.Connected then
  begin
    TMessage.MessageFailure('Existe uma Conex�o Ativa com o Servidor de Arquivos.' +
                           'Por favor, cancele ou aguarde o t�rmino da opera��o.');
    Abort;
  end;
end;

procedure TCadMidia.GerenciarControles;
var
  bEdicao       : Boolean;
begin
  inherited;
  if cdsData.Active then
    bEdicao := cdsData.State in dsEditModes
  else
    bEdicao := False;

  dxBarFTP.Visible          := bEdicao;
  actAnexarArquivo.Enabled  := bEdicao;
  actRemoverArquivo.Enabled := bEdicao;
end;

initialization
  RegisterClass(TCadMidia);
finalization
  UnRegisterClass(TCadMidia);

end.
