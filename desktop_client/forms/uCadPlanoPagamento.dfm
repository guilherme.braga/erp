inherited CadPlanoPagamento: TCadPlanoPagamento
  Caption = 'Cadastro de Plano de Pagamento'
  ClientHeight = 360
  ClientWidth = 778
  ExplicitWidth = 794
  ExplicitHeight = 399
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 778
    ExplicitWidth = 778
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 778
    Height = 233
    Properties.ActivePage = cxtsData
    ExplicitWidth = 778
    ExplicitHeight = 233
    ClientRectBottom = 233
    ClientRectRight = 778
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 778
      ExplicitHeight = 209
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 778
        Height = 209
        ExplicitWidth = 778
        ExplicitHeight = 209
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 778
      ExplicitHeight = 209
      inherited DesignPanel: TJvDesignPanel
        Width = 778
        Height = 209
        ExplicitWidth = 778
        ExplicitHeight = 209
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 778
          ExplicitHeight = 209
          Height = 209
          Width = 778
          inherited dxBevel1: TdxBevel
            Width = 774
            ExplicitWidth = 881
          end
          object Label6: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label5: TLabel
            Left = 132
            Top = 9
            Width = 27
            Height = 13
            Caption = 'Prazo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 67
            Top = 5
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBRadioGroup1: TgbDBRadioGroup
            Left = 163
            Top = 1
            Hint = 
              'Define prazo para o plano de pagamento:'#13#10'Data do Movimento (DATA' +
              '_MOVIMENTO): Entende-se que o pagamento ser'#225' para o dia em que e' +
              'st'#225' ocorrendo o movimento, dessa forma '#233' composto apenas de uma ' +
              'unica parcela.'#13#10'Fixo (FIXO): Composto por uma ou mais parcelas, ' +
              'onde o dia ser'#225' fixado nas datas.'#13#10'Personalizado (PERSONALIZADO)' +
              ': Composto por uma ou mais parcelas, onde a quantidade de dias e' +
              'ntre as parcelas poder'#225' ser alterada manualmente.'
            DataBinding.DataField = 'TIPO'
            DataBinding.DataSource = dsData
            Properties.Columns = 3
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = 'Data do Movimento'
                Value = 'DATA_MOVIMENTO'
              end
              item
                Caption = 'Fixo'
                Value = 'FIXO'
              end
              item
                Caption = 'Personalizado'
                Value = 'PERSONALIZADO'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Height = 25
            Width = 398
          end
          object gbDados: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            DesignSize = (
              774
              25)
            Height = 25
            Width = 774
            object Label4: TLabel
              Left = 8
              Top = 4
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object Label2: TLabel
              Left = 668
              Top = 4
              Width = 40
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Parcelas'
              ExplicitLeft = 775
            end
            object gbDBTextEdit9: TgbDBTextEdit
              Left = 65
              Top = 0
              Hint = 'Descri'#231#227'o do plano de pagamento'
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 597
            end
            object gbDBSpinEdit1: TgbDBSpinEdit
              Left = 712
              Top = 0
              Hint = 'Quantidade de parcelas'
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'QT_PARCELA'
              DataBinding.DataSource = dsData
              Properties.MaxValue = 999.000000000000000000
              Properties.MinValue = 1.000000000000000000
              Properties.ReadOnly = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              Width = 54
            end
          end
          object gbPrazoFixo: TgbPanel
            Left = 2
            Top = 59
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Height = 24
            Width = 774
            object Label3: TLabel
              Left = 8
              Top = 3
              Width = 38
              Height = 13
              Caption = 'Dia Fixo'
            end
            object gbDBSpinEdit2: TgbDBSpinEdit
              Left = 65
              Top = -1
              Hint = 'Dia fixo mensal para as parcelas'
              DataBinding.DataField = 'DIA_PARCELA'
              DataBinding.DataSource = dsData
              Properties.AssignedValues.MinValue = True
              Properties.MaxValue = 30.000000000000000000
              Properties.ReadOnly = False
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              Width = 54
            end
          end
          object gbPrazoPersonalizado: TgbPanel
            Left = 2
            Top = 83
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Transparent = True
            DesignSize = (
              774
              22)
            Height = 22
            Width = 774
            object Label1: TLabel
              Left = 8
              Top = 3
              Width = 27
              Height = 13
              Caption = 'Prazo'
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 65
              Top = -1
              Hint = 'Exemplo: 30/45/60'
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'PRAZO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 701
            end
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 718
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            Width = 55
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Plano Pagamento'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspPlanoPagamento'
    RemoteServer = DmConnection.dspFinanceiro
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataQT_PARCELA: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsDataPRAZO: TStringField
      DisplayLabel = 'Prazo'
      FieldName = 'PRAZO'
      Origin = 'PRAZO'
      Size = 255
    end
    object cdsDataTIPO: TStringField
      DefaultExpression = 'Tipo'
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      OnChange = cdsDataTIPOChange
    end
    object cdsDataDIA_PARCELA: TIntegerField
      DisplayLabel = 'Dia Fixo'
      FieldName = 'DIA_PARCELA'
      Origin = 'DIA_PARCELA'
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
end
