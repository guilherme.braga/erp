unit uFrameFechamentoCrediarioTipoQuitacaoCrediario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrameTipoQuitacaoCrediario, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Data.DB, cxDBEdit, uGBDBTextEdit, cxCalc, uGBDBCalcEdit, uGBPanel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit, Vcl.StdCtrls,
  cxGroupBox;

type
  TFrameFechamentoCrediarioTipoQuitacaoCrediario = class(TFrameTipoQuitacaoCrediario)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrameFechamentoCrediarioTipoQuitacaoCrediario: TFrameFechamentoCrediarioTipoQuitacaoCrediario;

implementation

{$R *.dfm}

end.
