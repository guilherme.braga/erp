inherited MovAjusteEstoque: TMovAjusteEstoque
  Caption = 'Ajuste de Estoque'
  ClientWidth = 938
  ExplicitWidth = 954
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 938
    ExplicitWidth = 938
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 938
    Properties.ActivePage = cxtsData
    ExplicitWidth = 938
    ClientRectRight = 938
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 938
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 906
        ExplicitWidth = 906
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 938
      inherited DesignPanel: TJvDesignPanel
        Width = 938
        ExplicitWidth = 938
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 938
          Width = 938
          inherited dxBevel1: TdxBevel
            Width = 934
            ExplicitTop = 31
            ExplicitWidth = 934
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelDtCadastro: TLabel
            Left = 110
            Top = 8
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object labelProcesso: TLabel
            Left = 325
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object labelSituacao: TLabel
            Left = 566
            Top = 8
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 564
          end
          object lbDtFechamento: TLabel
            Left = 723
            Top = 8
            Width = 72
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Confirmado em'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 45
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDtCadastro: TgbDBDateEdit
            Left = 187
            Top = 4
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object descProcesso: TgbDBTextEdit
            Left = 432
            Top = 4
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 130
          end
          object edtChaveProcesso: TgbDBTextEdit
            Left = 370
            Top = 4
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 65
          end
          object edSituacao: TgbDBTextEdit
            Left = 612
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 106
          end
          object PnDadosCabecalho: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            DesignSize = (
              934
              30)
            Height = 30
            Width = 934
            object lbFilial: TLabel
              Left = 8
              Top = 9
              Width = 20
              Height = 13
              Caption = 'Filial'
            end
            object lbMotivo: TLabel
              Left = 332
              Top = 9
              Width = 32
              Height = 13
              Caption = 'Motivo'
            end
            object edtFilial: TgbDBButtonEditFK
              Left = 43
              Top = 5
              DataBinding.DataField = 'ID_FILIAL'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 0
              gbTextEdit = descFilial
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_FILIAL;JOIN_FANTASIA_FILIAL'
              gbTableName = 'FILIAL'
              gbCamposConsulta = 'ID;FANTASIA'
              gbIdentificadorConsulta = 'FILIAL'
              Width = 60
            end
            object descFilial: TgbDBTextEdit
              Left = 100
              Top = 5
              TabStop = False
              DataBinding.DataField = 'JOIN_FANTASIA_FILIAL'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbPassword = False
              Width = 215
            end
            object edtMotivo: TgbDBButtonEditFK
              Left = 368
              Top = 5
              DataBinding.DataField = 'ID_MOTIVO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 2
              gbTextEdit = edtDescMotivo
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_MOTIVO;JOIN_DESCRICAO_MOTIVO'
              gbTableName = 'MOTIVO'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'MOTIVO'
              Width = 65
            end
            object edtDescMotivo: TgbDBTextEdit
              Left = 430
              Top = 5
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_MOTIVO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbReadyOnly = True
              gbPassword = False
              Width = 497
            end
          end
          object EdtDtFechamento: TgbDBDateEdit
            Left = 799
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_FECHAMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          inline FrameAjusteEstoqueItem: TFrameAjusteEstoqueItem
            Left = 2
            Top = 64
            Width = 934
            Height = 267
            Align = alClient
            TabOrder = 7
            ExplicitLeft = 2
            ExplicitTop = 64
            ExplicitWidth = 934
            ExplicitHeight = 267
            inherited cxpcMain: TcxPageControl
              Width = 934
              Height = 226
              Properties.ActivePage = FrameAjusteEstoqueItem.cxtsSearch
              ExplicitWidth = 934
              ExplicitHeight = 226
              ClientRectBottom = 226
              ClientRectRight = 934
              inherited cxtsSearch: TcxTabSheet
                ExplicitTop = 24
                ExplicitWidth = 934
                ExplicitHeight = 202
                inherited cxGridPesquisaPadrao: TcxGrid
                  Width = 934
                  Height = 202
                  ExplicitWidth = 934
                  ExplicitHeight = 202
                end
              end
              inherited cxtsData: TcxTabSheet
                inherited cxGBDadosMain: TcxGroupBox
                  inherited dxBevel1: TdxBevel
                    ExplicitWidth = 812
                  end
                  inherited gbPanel1: TgbPanel
                    inherited gbTipo: TgbDBRadioGroup
                      Properties.OnChange = nil
                    end
                  end
                end
              end
            end
            inherited PnTituloFrameDetailPadrao: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 934
              Width = 934
            end
            inherited dxBarManagerPadrao: TdxBarManager
              DockControlHeights = (
                0
                0
                22
                0)
              inherited barNavegador: TdxBar
                DockedLeft = 328
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbReabrir'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 541
      FloatClientWidth = 101
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbReport'
        end
        item
          Visible = True
          ItemName = 'lbImprimirEtiqueta'
        end>
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 771
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 466
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 679
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object lbEfetivar: TdxBarLargeButton [9]
      Action = ActEfetivar
      Category = 0
    end
    object lbReabrir: TdxBarLargeButton [10]
      Action = ActReabrir
      Category = 0
    end
    object lbImprimirEtiqueta: TdxBarLargeButton [19]
      Action = ActImprimirEtiqueta
      Category = 2
    end
  end
  inherited ActionListMain: TActionList
    object ActReabrir: TAction [12]
      Category = 'AjusteEstoque'
      Caption = 'Reabrir F10'
      Hint = 'Reabrir o ajuste de estoque'
      ImageIndex = 97
      ShortCut = 121
      OnExecute = ActReabrirExecute
    end
    object ActEfetivar: TAction [13]
      Category = 'AjusteEstoque'
      Caption = 'Efetivar F9'
      Hint = 'Efetivar o ajuste de estoque'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarExecute
    end
    object ActImprimirEtiqueta: TAction
      Category = 'AjusteEstoque'
      Caption = 'Etiqueta Ctrl+F8'
      ImageIndex = 156
      ShortCut = 16503
      OnExecute = ActImprimirEtiquetaExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspAjusteEstoque'
    RemoteServer = DmConnection.dspAjusteEstoque
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Dh. Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsDataSTATUS: TStringField
      DefaultExpression = 'ABERTO'
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
      Size = 30
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDataID_MOTIVO: TIntegerField
      DisplayLabel = 'C'#243'digo do Motivo'
      FieldName = 'ID_MOTIVO'
      Origin = 'ID_MOTIVO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object cdsDataJOIN_DESCRICAO_MOTIVO: TStringField
      DisplayLabel = 'Motivo'
      FieldName = 'JOIN_DESCRICAO_MOTIVO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Processo de Origem'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDatafdqAjusteEstoqueItem: TDataSetField
      FieldName = 'fdqAjusteEstoqueItem'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.154114479170000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsAjusteEstoqueItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqAjusteEstoqueItem
    Params = <>
    AfterOpen = cdsAjusteEstoqueItemAfterOpen
    AfterPost = cdsAjusteEstoqueItemAfterPost
    AfterDelete = cdsAjusteEstoqueItemAfterDelete
    OnNewRecord = cdsAjusteEstoqueItemNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 704
    Top = 80
    object cdsAjusteEstoqueItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsAjusteEstoqueItemNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsAjusteEstoqueItemQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsAjusteEstoqueItemTIPO: TStringField
      DefaultExpression = 'ENTRADA'
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Required = True
      Size = 15
    end
    object cdsAjusteEstoqueItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsAjusteEstoqueItemID_AJUSTE_ESTOQUE: TIntegerField
      DisplayLabel = 'C'#243'digo do Ajuste de Estoque'
      FieldName = 'ID_AJUSTE_ESTOQUE'
      Origin = 'ID_AJUSTE_ESTOQUE'
    end
    object cdsAjusteEstoqueItemJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsAjusteEstoqueItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
  end
end
