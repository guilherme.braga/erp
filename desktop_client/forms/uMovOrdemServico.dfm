inherited MovOrdemServico: TMovOrdemServico
  Caption = 'Ordem de Servi'#231'o'
  ClientHeight = 642
  ClientWidth = 1157
  ExplicitWidth = 1173
  ExplicitHeight = 681
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1157
    ExplicitWidth = 1099
    inherited dxGerencia: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarNavegacaoData'
        end
        item
          ToolbarName = 'dxBarManager'
        end
        item
          ToolbarName = 'dxBarAction'
        end
        item
          ToolbarName = 'barFiltroRapido'
        end
        item
          ToolbarName = 'dxBarSearch'
        end
        item
          ToolbarName = 'dxBarReport'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'dxBarEnd'
        end>
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1157
    Height = 515
    ExplicitWidth = 1099
    ExplicitHeight = 515
    ClientRectBottom = 515
    ClientRectRight = 1157
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1157
      ExplicitHeight = 491
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1125
        Height = 491
        ExplicitWidth = 1067
        ExplicitHeight = 491
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 491
        Height = 491
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 491
        ExplicitHeight = 491
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitTop = 0
      ExplicitWidth = 1099
      ExplicitHeight = 491
      inherited DesignPanel: TJvDesignPanel
        Width = 1157
        Height = 491
        ExplicitWidth = 1099
        ExplicitHeight = 491
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1157
          ExplicitHeight = 491
          Height = 491
          Width = 1157
          inherited dxBevel1: TdxBevel
            Width = 1153
            Anchors = [akTop, akRight]
            ExplicitLeft = 1
            ExplicitWidth = 1112
          end
          object Label6: TLabel
            Left = 12
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 585
            Top = 9
            Width = 73
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Cadastrado em'
            ExplicitLeft = 547
          end
          object Label13: TLabel
            Left = 1000
            Top = 9
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 779
          end
          object Label31: TLabel
            Left = 796
            Top = 9
            Width = 64
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Finalizada em'
            ExplicitLeft = 758
          end
          object Label3: TLabel
            Left = 111
            Top = 9
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 49
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object cxPageControlDetails: TcxPageControl
            Left = 2
            Top = 34
            Width = 1153
            Height = 357
            Align = alClient
            Focusable = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            TabStop = False
            Properties.ActivePage = tsDadosGerais
            Properties.CustomButtons.Buttons = <>
            Properties.NavigatorPosition = npLeftTop
            Properties.Options = [pcoAlwaysShowGoDialogButton, pcoGradient, pcoRedrawOnResize]
            Properties.Style = 8
            OnPageChanging = cxPageControlDetailsPageChanging
            ExplicitWidth = 1095
            ClientRectBottom = 357
            ClientRectRight = 1153
            ClientRectTop = 24
            object tsDadosGerais: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 0
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object PnDados: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                Alignment = alTopCenter
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  1153
                  73)
                Height = 73
                Width = 1153
                object Label9: TLabel
                  Left = 645
                  Top = 33
                  Width = 41
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Situa'#231#227'o'
                  ExplicitLeft = 604
                end
                object Label4: TLabel
                  Left = 645
                  Top = 57
                  Width = 56
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Dt. Entrada'
                  ExplicitLeft = 604
                end
                object Label8: TLabel
                  Left = 9
                  Top = 57
                  Width = 47
                  Height = 13
                  Caption = 'Opera'#231#227'o'
                end
                object Label2: TLabel
                  Left = 9
                  Top = 33
                  Width = 61
                  Height = 13
                  Caption = 'Respons'#225'vel'
                end
                object Label32: TLabel
                  Left = 918
                  Top = 57
                  Width = 97
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Previs'#227'o do T'#233'rmino'
                  ExplicitLeft = 877
                end
                object dxBevel3: TdxBevel
                  Left = 2
                  Top = 2
                  Width = 1149
                  Height = 26
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitTop = 3
                  ExplicitWidth = 1108
                end
                object Label51: TLabel
                  Left = 8
                  Top = 7
                  Width = 111
                  Height = 13
                  Caption = 'Informa'#231#245'es Gerais'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object gbDBButtonEditFK1: TgbDBButtonEditFK
                  Left = 710
                  Top = 29
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_SITUACAO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 2
                  gbTextEdit = gbDBTextEdit3
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_SITUACAO;JOIN_DESCRICAO_SITUACAO'
                  gbTableName = 'SITUACAO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'SITUACAO'
                  ExplicitLeft = 652
                  Width = 60
                end
                object gbDBTextEdit3: TgbDBTextEdit
                  Left = 767
                  Top = 29
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_SITUACAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbPassword = False
                  ExplicitLeft = 709
                  Width = 381
                end
                object gbDBDateEdit3: TgbDBDateEdit
                  Left = 710
                  Top = 53
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'DH_ENTRADA'
                  DataBinding.DataSource = dsData
                  Properties.DateButtons = [btnClear, btnNow]
                  Properties.ImmediatePost = True
                  Properties.Kind = ckDateTime
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbRequired = True
                  gbDateTime = True
                  ExplicitLeft = 652
                  Width = 130
                end
                object gbDBButtonEditFK2: TgbDBButtonEditFK
                  Left = 72
                  Top = 53
                  DataBinding.DataField = 'ID_OPERACAO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 4
                  gbTextEdit = gbDBTextEdit2
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_OPERACAO;JOIN_DESCRICAO_OPERACAO'
                  gbTableName = 'OPERACAO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'OPERACAO'
                  Width = 60
                end
                object gbDBTextEdit2: TgbDBTextEdit
                  Left = 129
                  Top = 53
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_OPERACAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 510
                end
                object gbDBTextEdit1: TgbDBTextEdit
                  Left = 129
                  Top = 29
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_USUARIO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 510
                end
                object gbDBButtonEditFK6: TgbDBButtonEditFK
                  Left = 72
                  Top = 29
                  DataBinding.DataField = 'ID_PESSOA_USUARIO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 0
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_PESSOA_USUARIO;JOIN_USUARIO'
                  gbTableName = 'PESSOA_USUARIO'
                  gbCamposConsulta = 'ID;USUARIO'
                  gbIdentificadorConsulta = 'PESSOA_USUARIO'
                  Width = 60
                end
                object gbDBDateEdit2: TgbDBDateEdit
                  Left = 1018
                  Top = 53
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'DH_PREVISAO_TERMINO'
                  DataBinding.DataSource = dsData
                  Properties.DateButtons = [btnClear, btnNow]
                  Properties.ImmediatePost = True
                  Properties.Kind = ckDateTime
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 7
                  gbRequired = True
                  gbDateTime = True
                  ExplicitLeft = 960
                  Width = 130
                end
              end
              object pnRelatos: TgbPanel
                Left = 0
                Top = 248
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                Transparent = True
                Height = 85
                Width = 1153
                object pnSolucaoTecnica: TgbPanel
                  Left = 565
                  Top = 2
                  Align = alClient
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  Transparent = True
                  Height = 81
                  Width = 586
                  object gbDBMemo1: TgbDBMemo
                    Left = 2
                    Top = 30
                    Align = alClient
                    DataBinding.DataField = 'SOLUCAO_TECNICA'
                    DataBinding.DataSource = dsData
                    Style.BorderStyle = ebsOffice11
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 49
                    Width = 582
                  end
                  object cxLabel1: TcxLabel
                    Left = 2
                    Top = 2
                    Align = alTop
                    AutoSize = False
                    Caption = 'Solu'#231#227'o T'#233'cnica'
                    Style.BorderStyle = ebsNone
                    Style.TextStyle = [fsBold]
                    StyleDisabled.BorderStyle = ebsNone
                    Properties.Alignment.Horz = taCenter
                    Properties.Alignment.Vert = taVCenter
                    Transparent = True
                    Height = 28
                    Width = 582
                    AnchorX = 293
                    AnchorY = 16
                  end
                end
                object pnProblemaEncontrado: TgbPanel
                  Left = 380
                  Top = 2
                  Align = alLeft
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  Transparent = True
                  Height = 81
                  Width = 185
                  object gbDBMemo2: TgbDBMemo
                    Left = 2
                    Top = 30
                    Align = alClient
                    DataBinding.DataField = 'PROBLEMA_ENCONTRADO'
                    DataBinding.DataSource = dsData
                    Style.BorderStyle = ebsOffice11
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 49
                    Width = 181
                  end
                  object cxLabel2: TcxLabel
                    Left = 2
                    Top = 2
                    Align = alTop
                    AutoSize = False
                    Caption = 'Problema Encontrado'
                    Style.BorderStyle = ebsNone
                    Style.TextStyle = [fsBold]
                    StyleDisabled.BorderStyle = ebsNone
                    Properties.Alignment.Horz = taCenter
                    Properties.Alignment.Vert = taVCenter
                    Transparent = True
                    Height = 28
                    Width = 181
                    AnchorX = 93
                    AnchorY = 16
                  end
                end
                object PnProblemaRelatado: TgbPanel
                  Left = 195
                  Top = 2
                  Align = alLeft
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Transparent = True
                  Height = 81
                  Width = 185
                  object gbDBMemo3: TgbDBMemo
                    Left = 2
                    Top = 30
                    Align = alClient
                    DataBinding.DataField = 'PROBLEMA_RELATADO'
                    DataBinding.DataSource = dsData
                    Style.BorderStyle = ebsOffice11
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 49
                    Width = 181
                  end
                  object cxLabel3: TcxLabel
                    Left = 2
                    Top = 2
                    Align = alTop
                    AutoSize = False
                    Caption = 'Problema Relatado'
                    Style.BorderStyle = ebsNone
                    Style.TextStyle = [fsBold]
                    StyleDisabled.BorderStyle = ebsNone
                    Properties.Alignment.Horz = taCenter
                    Properties.Alignment.Vert = taVCenter
                    Transparent = True
                    Height = 28
                    Width = 181
                    AnchorX = 93
                    AnchorY = 16
                  end
                end
                object PnCheckList: TgbGroupBox
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Align = alLeft
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Height = 75
                  Width = 187
                  object gridChecklist: TcxGrid
                    Left = 2
                    Top = 29
                    Width = 183
                    Height = 44
                    Align = alClient
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Arial'
                    Font.Style = []
                    Images = DmAcesso.cxImage16x16
                    ParentFont = False
                    TabOrder = 0
                    LookAndFeel.Kind = lfOffice11
                    LookAndFeel.NativeStyle = False
                    object viewChecklist: TcxGridDBBandedTableView
                      OnDblClick = Level1BandedTableView1DblClick
                      Navigator.Buttons.CustomButtons = <>
                      Navigator.Buttons.Images = DmAcesso.cxImage16x16
                      Navigator.Buttons.PriorPage.Visible = False
                      Navigator.Buttons.NextPage.Visible = False
                      Navigator.Buttons.Insert.Visible = False
                      Navigator.Buttons.Delete.Visible = False
                      Navigator.Buttons.Edit.Visible = False
                      Navigator.Buttons.Post.Visible = False
                      Navigator.Buttons.Cancel.Visible = False
                      Navigator.Buttons.Refresh.Visible = False
                      Navigator.Buttons.SaveBookmark.Visible = False
                      Navigator.Buttons.GotoBookmark.Visible = False
                      Navigator.Buttons.Filter.Visible = False
                      Navigator.InfoPanel.Visible = True
                      Navigator.Visible = True
                      OnEditKeyPress = Level1BandedTableView1EditKeyPress
                      DataController.DataSource = dsChecklist
                      DataController.Filter.Options = [fcoCaseInsensitive]
                      DataController.Filter.Active = True
                      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      FilterRow.ApplyChanges = fracImmediately
                      Images = DmAcesso.cxImage16x16
                      OptionsBehavior.NavigatorHints = True
                      OptionsBehavior.ExpandMasterRowOnDblClick = False
                      OptionsCustomize.ColumnFiltering = False
                      OptionsCustomize.ColumnGrouping = False
                      OptionsCustomize.BandMoving = False
                      OptionsCustomize.NestedBands = False
                      OptionsData.CancelOnExit = False
                      OptionsData.Deleting = False
                      OptionsData.DeletingConfirmation = False
                      OptionsData.Inserting = False
                      OptionsSelection.HideSelection = True
                      OptionsView.ColumnAutoWidth = True
                      OptionsView.GroupByBox = False
                      OptionsView.GroupRowStyle = grsOffice11
                      OptionsView.Header = False
                      OptionsView.BandHeaders = False
                      Styles.ContentOdd = DmAcesso.OddColor
                      Bands = <
                        item
                        end>
                      object viewChecklistID: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID'
                        Visible = False
                        Options.Editing = False
                        Options.Sorting = False
                        Position.BandIndex = 0
                        Position.ColIndex = 0
                        Position.RowIndex = 0
                      end
                      object viewChecklistBO_MARCADO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'BO_MARCADO'
                        Options.Sorting = False
                        Width = 71
                        Position.BandIndex = 0
                        Position.ColIndex = 1
                        Position.RowIndex = 0
                        IsCaptionAssigned = True
                      end
                      object viewChecklistID_CHECKLIST_ITEM: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_CHECKLIST_ITEM'
                        Visible = False
                        Options.Editing = False
                        Options.Sorting = False
                        Position.BandIndex = 0
                        Position.ColIndex = 2
                        Position.RowIndex = 0
                      end
                      object viewChecklistID_ORDEM_SERVICO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_ORDEM_SERVICO'
                        Visible = False
                        Options.Editing = False
                        Options.Sorting = False
                        Position.BandIndex = 0
                        Position.ColIndex = 3
                        Position.RowIndex = 0
                      end
                      object viewChecklistJOIN_DESCRICAO_CHECKLIST: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'JOIN_DESCRICAO_CHECKLIST'
                        Options.Editing = False
                        Options.Sorting = False
                        Width = 1078
                        Position.BandIndex = 0
                        Position.ColIndex = 4
                        Position.RowIndex = 0
                      end
                      object viewChecklistJOIN_NR_ITEM_CHECKLIST: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'JOIN_NR_ITEM_CHECKLIST'
                        Visible = False
                        Options.Editing = False
                        SortIndex = 0
                        SortOrder = soAscending
                        Position.BandIndex = 0
                        Position.ColIndex = 5
                        Position.RowIndex = 0
                      end
                    end
                    object cxGridLevel2: TcxGridLevel
                      GridView = viewChecklist
                    end
                  end
                  object gbPanel5: TgbPanel
                    Left = 2
                    Top = 5
                    Align = alTop
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    Transparent = True
                    Height = 24
                    Width = 183
                    object gbDBButtonEditFK10: TgbDBButtonEditFK
                      Left = 63
                      Top = 2
                      Align = alLeft
                      DataBinding.DataField = 'ID_CHECKLIST'
                      DataBinding.DataSource = dsData
                      Properties.Buttons = <
                        item
                          Default = True
                          Kind = bkEllipsis
                        end>
                      Properties.CharCase = ecUpperCase
                      Properties.ClickKey = 13
                      Properties.ReadOnly = False
                      Style.Color = 14606074
                      TabOrder = 0
                      gbTextEdit = gbDBTextEdit37
                      gbRequired = True
                      gbCampoPK = 'ID'
                      gbCamposRetorno = 'ID_CHECKLIST;JOIN_DESCRICAO_CHECKLIST'
                      gbTableName = 'CHECKLIST'
                      gbCamposConsulta = 'ID;DESCRICAO'
                      gbIdentificadorConsulta = 'CHECKLIST'
                      ExplicitHeight = 21
                      Width = 60
                    end
                    object gbDBTextEdit37: TgbDBTextEdit
                      Left = 123
                      Top = 2
                      TabStop = False
                      Align = alClient
                      DataBinding.DataField = 'JOIN_DESCRICAO_CHECKLIST'
                      DataBinding.DataSource = dsData
                      Properties.CharCase = ecUpperCase
                      Properties.ReadOnly = True
                      Style.BorderStyle = ebsOffice11
                      Style.Color = clGradientActiveCaption
                      Style.LookAndFeel.Kind = lfOffice11
                      StyleDisabled.LookAndFeel.Kind = lfOffice11
                      StyleFocused.LookAndFeel.Kind = lfOffice11
                      StyleHot.LookAndFeel.Kind = lfOffice11
                      TabOrder = 1
                      gbReadyOnly = True
                      gbPassword = False
                      ExplicitHeight = 21
                      Width = 58
                    end
                    object cxLabel4: TcxLabel
                      Left = 2
                      Top = 2
                      Align = alLeft
                      Caption = 'Check List'
                      Style.BorderStyle = ebsNone
                      Style.TextStyle = [fsBold]
                      StyleDisabled.BorderStyle = ebsNone
                      Properties.Alignment.Horz = taCenter
                      Properties.Alignment.Vert = taVCenter
                      Transparent = True
                      AnchorX = 33
                      AnchorY = 12
                    end
                  end
                end
              end
              object PnInformacoesVeiculo: TgbPanel
                Left = 0
                Top = 73
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                DesignSize = (
                  1153
                  78)
                Height = 78
                Width = 1153
                object dxBevel2: TdxBevel
                  Left = 2
                  Top = 2
                  Width = 1149
                  Height = 26
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitWidth = 1108
                end
                object Label15: TLabel
                  Left = 186
                  Top = 59
                  Width = 34
                  Height = 13
                  Caption = 'Modelo'
                end
                object Label30: TLabel
                  Left = 186
                  Top = 34
                  Width = 29
                  Height = 13
                  Caption = 'Marca'
                end
                object Label46: TLabel
                  Left = 9
                  Top = 34
                  Width = 25
                  Height = 13
                  Caption = 'Placa'
                end
                object Label50: TLabel
                  Left = 645
                  Top = 34
                  Width = 17
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Cor'
                  ExplicitLeft = 604
                end
                object Label43: TLabel
                  Left = 1074
                  Top = 33
                  Width = 19
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Ano'
                  ExplicitLeft = 1033
                end
                object Label47: TLabel
                  Left = 9
                  Top = 59
                  Width = 58
                  Height = 13
                  Caption = 'Combust'#237'vel'
                end
                object Label48: TLabel
                  Left = 521
                  Top = 59
                  Width = 14
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Km'
                  ExplicitLeft = 480
                end
                object Label49: TLabel
                  Left = 645
                  Top = 59
                  Width = 36
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Tanque'
                  ExplicitLeft = 604
                end
                object lbInformacoesCliente: TLabel
                  Left = 8
                  Top = 7
                  Width = 132
                  Height = 13
                  Caption = 'Informa'#231#245'es do Ve'#237'culo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object descModelo: TgbDBTextEdit
                  Left = 223
                  Top = 55
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_MODELO_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 294
                end
                object descMarca: TgbDBTextEdit
                  Left = 223
                  Top = 30
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_MARCA_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 416
                end
                object EdtPlaca: TgbDBButtonEditFK
                  Left = 72
                  Top = 30
                  DataBinding.DataField = 'JOIN_PLACA_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Style.Color = clWhite
                  TabOrder = 0
                  gbTextEdit = descMarca
                  gbCampoPK = 'PLACA'
                  gbCamposRetorno = 'ID_VEICULO;JOIN_PLACA_VEICULO'
                  gbTableName = 'VEICULO'
                  gbCamposConsulta = 'ID;PLACA'
                  gbDepoisDeConsultar = EdtPlacagbDepoisDeConsultar
                  gbIdentificadorConsulta = 'VEICULO'
                  Width = 110
                end
                object gbDBTextEdit6: TgbDBTextEdit
                  Left = 1097
                  Top = 29
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_ANO_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbPassword = False
                  ExplicitLeft = 1039
                  Width = 51
                end
                object gbDBTextEdit9: TgbDBTextEdit
                  Left = 710
                  Top = 30
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_COR_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbReadyOnly = True
                  gbPassword = False
                  ExplicitLeft = 652
                  Width = 358
                end
                object gbDBTextEdit10: TgbDBTextEdit
                  Left = 72
                  Top = 55
                  TabStop = False
                  DataBinding.DataField = 'JOIN_COMBUSTIVEL_VEICULO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 110
                end
                object gbDBCalcEdit1: TgbDBCalcEdit
                  Left = 539
                  Top = 55
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'KM'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  ExplicitLeft = 481
                  Width = 100
                end
                object gbDBRadioGroup1: TgbDBRadioGroup
                  Left = 710
                  Top = 52
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'TANQUE'
                  DataBinding.DataSource = dsData
                  Properties.Columns = 5
                  Properties.Items = <
                    item
                      Caption = 'Completo'
                      Value = '1'
                    end
                    item
                      Caption = '3/4'
                      Value = '3/4'
                    end
                    item
                      Caption = '2/4'
                      Value = '2/4'
                    end
                    item
                      Caption = '1/4'
                      Value = '1/4'
                    end
                    item
                      Caption = 'Vazio'
                      Value = '0'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 7
                  ExplicitLeft = 652
                  Height = 26
                  Width = 438
                end
              end
              object pnInformacoesCliente: TgbPanel
                Left = 0
                Top = 200
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Transparent = True
                DesignSize = (
                  1153
                  48)
                Height = 48
                Width = 1153
                object dxBevel4: TdxBevel
                  Left = 2
                  Top = 2
                  Width = 1149
                  Height = 24
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitWidth = 1108
                end
                object Label52: TLabel
                  Left = 8
                  Top = 5
                  Width = 131
                  Height = 13
                  Caption = 'Informa'#231#245'es do Cliente'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label10: TLabel
                  Left = 9
                  Top = 32
                  Width = 33
                  Height = 13
                  Caption = 'Cliente'
                end
                object gbDBButtonEditFK3: TgbDBButtonEditFK
                  Left = 72
                  Top = 28
                  DataBinding.DataField = 'ID_PESSOA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit5
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
                  gbTableName = 'PESSOA'
                  gbCamposConsulta = 'ID;NOME'
                  gbIdentificadorConsulta = 'PESSOA_MOV_ORDEM_SERVICO'
                  Width = 60
                end
                object gbDBTextEdit5: TgbDBTextEdit
                  Left = 129
                  Top = 28
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_NOME_PESSOA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 1019
                end
              end
              object pnInformacoesEquipamento: TgbPanel
                Left = 0
                Top = 151
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                DesignSize = (
                  1153
                  49)
                Height = 49
                Width = 1153
                object dxBevel5: TdxBevel
                  Left = 2
                  Top = 2
                  Width = 1149
                  Height = 24
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitWidth = 1108
                end
                object Label53: TLabel
                  Left = 8
                  Top = 5
                  Width = 166
                  Height = 13
                  Caption = 'Informa'#231#245'es do Equipamento'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label7: TLabel
                  Left = 9
                  Top = 31
                  Width = 62
                  Height = 13
                  Caption = 'Equipamento'
                end
                object Label44: TLabel
                  Left = 864
                  Top = 31
                  Width = 24
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'S'#233'rie'
                  ExplicitLeft = 823
                end
                object Label45: TLabel
                  Left = 999
                  Top = 31
                  Width = 22
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'IMEI'
                  ExplicitLeft = 958
                end
                object EdtEquipamento: TgbDBButtonEditFK
                  Left = 72
                  Top = 27
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'EQUIPAMENTO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = clWhite
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit7
                  gbCampoPK = 'DESCRICAO'
                  gbCamposRetorno = 'EQUIPAMENTO;EQUIPAMENTO_SERIE;EQUIPAMENTO_IMEI'
                  gbTableName = 'EQUIPAMENTO'
                  gbCamposConsulta = 'DESCRICAO;SERIE;IMEI'
                  gbDepoisDeConsultar = EdtEquipamentogbDepoisDeConsultar
                  gbIdentificadorConsulta = 'EQUIPAMENTO'
                  Width = 786
                end
                object gbDBTextEdit7: TgbDBTextEdit
                  Left = 892
                  Top = 27
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'EQUIPAMENTO_SERIE'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbPassword = False
                  ExplicitLeft = 834
                  Width = 103
                end
                object gbDBTextEdit26: TgbDBTextEdit
                  Left = 1027
                  Top = 27
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'EQUIPAMENTO_IMEI'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbPassword = False
                  ExplicitLeft = 969
                  Width = 122
                end
              end
            end
            object tsProduto: TcxTabSheet
              Caption = 'Produtos'
              ImageIndex = 2
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object gbPanel7: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  1153
                  29)
                Height = 29
                Width = 1153
                object Label5: TLabel
                  Left = 7
                  Top = 8
                  Width = 77
                  Height = 13
                  Caption = 'Tabela de Pre'#231'o'
                end
                object gbDBButtonEditFK8: TgbDBButtonEditFK
                  Left = 88
                  Top = 4
                  DataBinding.DataField = 'ID_TABELA_PRECO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit25
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_TABELA_PRECO;JOIN_DESCRICAO_TABELA_PRECO'
                  gbTableName = 'TABELA_PRECO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'TABELA_PRECO'
                  Width = 60
                end
                object gbDBTextEdit25: TgbDBTextEdit
                  Left = 145
                  Top = 4
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_TABELA_PRECO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 999
                end
              end
              object PnFrameProduto: TgbPanel
                Left = 0
                Top = 29
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameOrdemSevicoProduto'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 304
                Width = 1153
              end
            end
            object tsServico: TcxTabSheet
              Caption = 'Servi'#231'os'
              ImageIndex = 3
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object PnFrameServico: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameOrdemSevicoServico'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 333
                Width = 1153
              end
            end
            object tsServicoTerceiro: TcxTabSheet
              Caption = 'Servi'#231'os de Terceiros'
              ImageIndex = 4
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object PnFrameServicoTerceiro: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameOrdemSevicoTerceiro'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 333
                Width = 1153
              end
            end
            object tsObservacao: TcxTabSheet
              Caption = 'Observa'#231#227'o'
              ImageIndex = 5
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object gbDBMemo4: TgbDBMemo
                Left = 0
                Top = 0
                Align = alClient
                DataBinding.DataField = 'OBSERVACAO'
                DataBinding.DataSource = dsData
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 333
                Width = 1153
              end
            end
            object tsFechamento: TcxTabSheet
              Caption = 'Fechamento'
              ImageIndex = 1
              ExplicitTop = 0
              ExplicitWidth = 1095
              ExplicitHeight = 0
              object PnFrameFechamento: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameOrdemSevicoFechamento'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 333
                Width = 1153
              end
            end
          end
          object PnTotalizadores: TgbPanel
            Left = 2
            Top = 391
            Align = alBottom
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Height = 98
            Width = 1153
            object gbPanel3: TgbPanel
              Left = 2
              Top = 2
              Align = alLeft
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              Height = 94
              Width = 859
              object pnTotalProduto: TgbPanel
                Left = 2
                Top = 62
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 31
                Width = 855
                object Label16: TLabel
                  Left = 300
                  Top = 8
                  Width = 45
                  Height = 13
                  Caption = 'Desconto'
                end
                object Label17: TLabel
                  Left = 486
                  Top = 8
                  Width = 48
                  Height = 13
                  Caption = 'Acr'#233'scimo'
                end
                object Label18: TLabel
                  Left = 470
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label19: TLabel
                  Left = 659
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label20: TLabel
                  Left = 8
                  Top = 8
                  Width = 121
                  Height = 13
                  Caption = 'Servi'#231'os de Terceiros'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label27: TLabel
                  Left = 680
                  Top = 8
                  Width = 72
                  Height = 13
                  Caption = 'Total L'#237'quido'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label14: TLabel
                  Left = 141
                  Top = 8
                  Width = 53
                  Height = 13
                  Caption = 'Total Bruto'
                end
                object gbDBTextEdit11: TgbDBTextEdit
                  Left = 349
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_DESCONTO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit12: TgbDBTextEdit
                  Left = 538
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit13: TgbDBTextEdit
                  Left = 420
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit14: TgbDBTextEdit
                  Left = 609
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit15: TgbDBTextEdit
                  Left = 199
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_BRUTO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
                object gbDBTextEdit22: TgbDBTextEdit
                  Left = 755
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
              end
              object gbPanel1: TgbPanel
                Left = 2
                Top = 31
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 31
                Width = 855
                object Label21: TLabel
                  Left = 300
                  Top = 8
                  Width = 45
                  Height = 13
                  Caption = 'Desconto'
                end
                object Label22: TLabel
                  Left = 486
                  Top = 8
                  Width = 48
                  Height = 13
                  Caption = 'Acr'#233'scimo'
                end
                object Label23: TLabel
                  Left = 470
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label24: TLabel
                  Left = 659
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label25: TLabel
                  Left = 8
                  Top = 8
                  Width = 48
                  Height = 13
                  Caption = 'Servi'#231'os'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label26: TLabel
                  Left = 680
                  Top = 8
                  Width = 72
                  Height = 13
                  Caption = 'Total L'#237'quido'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label12: TLabel
                  Left = 141
                  Top = 8
                  Width = 53
                  Height = 13
                  Caption = 'Total Bruto'
                end
                object gbDBTextEdit16: TgbDBTextEdit
                  Left = 349
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_DESCONTO_SERVICO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit17: TgbDBTextEdit
                  Left = 538
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_ACRESCIMO_SERVICO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit18: TgbDBTextEdit
                  Left = 420
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_DESCONTO_SERVICO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit19: TgbDBTextEdit
                  Left = 609
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_ACRESCIMO_SERVICO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit20: TgbDBTextEdit
                  Left = 199
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_BRUTO_SERVICO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
                object gbDBTextEdit21: TgbDBTextEdit
                  Left = 755
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_LIQUIDO_SERVICO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
              end
              object gbPanel2: TgbPanel
                Left = 2
                Top = 2
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 29
                Width = 855
                object Label28: TLabel
                  Left = 300
                  Top = 8
                  Width = 45
                  Height = 13
                  Caption = 'Desconto'
                end
                object Label29: TLabel
                  Left = 486
                  Top = 8
                  Width = 48
                  Height = 13
                  Caption = 'Acr'#233'scimo'
                end
                object Label33: TLabel
                  Left = 470
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label34: TLabel
                  Left = 659
                  Top = 8
                  Width = 11
                  Height = 13
                  Caption = '%'
                end
                object Label35: TLabel
                  Left = 8
                  Top = 8
                  Width = 51
                  Height = 13
                  Caption = 'Produtos'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label36: TLabel
                  Left = 680
                  Top = 8
                  Width = 72
                  Height = 13
                  Caption = 'Total L'#237'quido'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label11: TLabel
                  Left = 141
                  Top = 8
                  Width = 53
                  Height = 13
                  Caption = 'Total Bruto'
                end
                object gbDBTextEdit23: TgbDBTextEdit
                  Left = 349
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_DESCONTO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit24: TgbDBTextEdit
                  Left = 538
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_ACRESCIMO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 72
                end
                object gbDBTextEdit27: TgbDBTextEdit
                  Left = 420
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_DESCONTO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit28: TgbDBTextEdit
                  Left = 609
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'PERC_TOTAL_ACRESCIMO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 50
                end
                object gbDBTextEdit29: TgbDBTextEdit
                  Left = 199
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_BRUTO_PRODUTO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
                object gbDBTextEdit30: TgbDBTextEdit
                  Left = 755
                  Top = 4
                  TabStop = False
                  DataBinding.DataField = 'VL_TOTAL_LIQUIDO_PRODUTO'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 97
                end
              end
            end
            object gbPanel4: TgbPanel
              Left = 861
              Top = 2
              Align = alClient
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              DesignSize = (
                290
                94)
              Height = 94
              Width = 290
              object Label37: TLabel
                Left = 46
                Top = 10
                Width = 63
                Height = 13
                Anchors = [akTop, akRight]
                Caption = 'Total Bruto'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitLeft = 62
              end
              object Label38: TLabel
                Left = 46
                Top = 30
                Width = 45
                Height = 13
                Anchors = [akTop, akRight]
                Caption = 'Desconto'
                ExplicitLeft = 62
              end
              object Label39: TLabel
                Left = 268
                Top = 30
                Width = 11
                Height = 13
                Anchors = [akTop, akRight]
                Caption = '%'
                ExplicitLeft = 299
              end
              object Label40: TLabel
                Left = 46
                Top = 50
                Width = 48
                Height = 13
                Anchors = [akTop, akRight]
                Caption = 'Acr'#233'scimo'
                ExplicitLeft = 62
              end
              object Label41: TLabel
                Left = 268
                Top = 50
                Width = 11
                Height = 13
                Anchors = [akTop, akRight]
                Caption = '%'
                ExplicitLeft = 299
              end
              object Label42: TLabel
                Left = 46
                Top = 70
                Width = 72
                Height = 13
                Anchors = [akTop, akRight]
                Caption = 'Total L'#237'quido'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitLeft = 62
              end
              object gbDBTextEdit31: TgbDBTextEdit
                Left = 123
                Top = 6
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'VL_TOTAL_BRUTO'
                DataBinding.DataSource = dsData
                ParentFont = False
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.LookAndFeel.Kind = lfOffice11
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 65
                Width = 157
              end
              object gbDBTextEdit32: TgbDBTextEdit
                Left = 123
                Top = 26
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'VL_TOTAL_DESCONTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 65
                Width = 90
              end
              object gbDBTextEdit33: TgbDBTextEdit
                Left = 212
                Top = 26
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'PERC_TOTAL_DESCONTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 154
                Width = 55
              end
              object gbDBTextEdit34: TgbDBTextEdit
                Left = 123
                Top = 46
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'VL_TOTAL_ACRESCIMO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 65
                Width = 90
              end
              object gbDBTextEdit35: TgbDBTextEdit
                Left = 212
                Top = 46
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'PERC_TOTAL_ACRESCIMO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 154
                Width = 55
              end
              object gbDBTextEdit36: TgbDBTextEdit
                Left = 123
                Top = 66
                TabStop = False
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'VL_TOTAL_LIQUIDO'
                DataBinding.DataSource = dsData
                ParentFont = False
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.LookAndFeel.Kind = lfOffice11
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 5
                gbReadyOnly = True
                gbRequired = True
                gbPassword = False
                ExplicitLeft = 65
                Width = 156
              end
            end
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 662
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbDateTime = True
            ExplicitLeft = 604
            Width = 130
          end
          object gbDBTextEdit8: TgbDBTextEdit
            Left = 1045
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            ExplicitLeft = 987
            Width = 101
          end
          object gbDBDateEdit5: TgbDBDateEdit
            Left = 864
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_FECHAMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbDateTime = True
            ExplicitLeft = 806
            Width = 130
          end
          object descProcesso: TgbDBTextEdit
            Left = 210
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 369
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 158
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            Width = 55
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Categories.Strings = (
      'Action'
      'Manager'
      'Report'
      'End'
      'Search'
      'Design'
      'ActionDesign'
      'Navegacao'
      'Personalizacoes'
      'FiltroRapido')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 140
      FloatClientWidth = 82
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'LBEfetivar'
        end
        item
          Visible = True
          ItemName = 'LBEstornar'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 920
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 1091
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 845
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      FloatClientWidth = 61
      FloatClientHeight = 180
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 999
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object barFiltroRapido: TdxBar [8]
      Caption = 'Filtro R'#225'pido'
      CaptionButtons = <>
      DockedLeft = 303
      DockedTop = 0
      FloatLeft = 1133
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'filtroCodigoOrdemServico'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 158
          Visible = True
          ItemName = 'filtroPessoa'
        end
        item
          Visible = True
          ItemName = 'filtroNomePessoa'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataCadastro'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroInicial'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroFIm'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataDocumento'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoInicio'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bsLabelStatus'
        end
        item
          Visible = True
          ItemName = 'filtroSituacao'
        end
        item
          Visible = True
          ItemName = 'lbVazio'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object LBEfetivar: TdxBarLargeButton [11]
      Action = ActEfetivar
      Category = 0
    end
    object LBEstornar: TdxBarLargeButton [12]
      Action = ActEstornar
      Category = 0
    end
    object filtroLabelDataCadastro: TdxBarStatic
      Align = iaCenter
      Caption = 'Data de Cadastro'
      Category = 9
      Hint = 'Data de Cadastro'
      Visible = ivAlways
    end
    object filtroDataCadastroInicial: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 9
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataCadastroFIm: TcxBarEditItem
      Caption = 'Fim   '
      Category = 9
      Hint = 'Fim'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroLabelDataDocumento: TdxBarStatic
      Align = iaCenter
      Caption = 'Data de Efetiva'#231#227'o'
      Category = 9
      Hint = 'Data de Efetiva'#231#227'o'
      Visible = ivAlways
    end
    object filtroDataDocumentoInicio: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 9
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataDocumentoFim: TcxBarEditItem
      Caption = 'Fim   '
      Category = 9
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroSituacao: TdxBarCombo
      Category = 9
      Visible = ivAlways
      Items.Strings = (
        'TODOS'
        'ABERTO'
        'FECHADO')
      ItemIndex = -1
    end
    object filtroPessoa: TcxBarEditItem
      Caption = 'Pessoa'
      Category = 9
      Hint = 'Pessoa'
      Visible = ivAlways
      OnExit = filtroPessoaPropertiesEditValueChanged
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = filtroPessoaPropertiesButtonClick
    end
    object filtroCodigoOrdemServico: TcxBarEditItem
      Caption = 'Ordem de Servi'#231'o'
      Category = 9
      Hint = 'Ordem de Servi'#231'o'
      Visible = ivAlways
      OnExit = filtroCodigoOrdemServicoExit
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.AssignedValues.DisplayFormat = True
      Properties.DecimalPlaces = 0
      Properties.MaxLength = 8
      Properties.MaxValue = 99999999.000000000000000000
    end
    object filtroNomePessoa: TdxBarStatic
      Caption = 'Nome da Pessoa 40 Caracteres'
      Category = 9
      Hint = '123456789 123456789 123456789 123456789'
      Visible = ivAlways
      Alignment = taLeftJustify
    end
    object bsLabelStatus: TdxBarStatic
      Caption = 'Situa'#231#227'o'
      Category = 9
      Hint = 'Situa'#231#227'o'
      Visible = ivAlways
    end
    object lbVazio: TdxBarStatic
      Category = 9
      Visible = ivAlways
    end
  end
  inherited dsData: TDataSource
    OnDataChange = dsDataDataChange
  end
  inherited ActionListMain: TActionList
    object ActEstornar: TAction [12]
      Category = 'Action'
      Caption = 'Estornar F10'
      Hint = 'Estornar a nota fiscal'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = ActEstornarExecute
    end
    object ActEfetivar: TAction [13]
      Category = 'Action'
      Caption = 'Efetivar F9'
      Hint = 'Efetivar a nota fiscal'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarExecute
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Ordem de Servi'#231'o'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspOrdemServico'
    RemoteServer = DmConnection.dspOrdemServico
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Dh. Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsDataDH_ENTRADA: TDateTimeField
      DisplayLabel = 'Dh. Entrada'
      FieldName = 'DH_ENTRADA'
      Origin = 'DH_ENTRADA'
      Required = True
    end
    object cdsDataDH_PREVISAO_TERMINO: TDateTimeField
      DisplayLabel = 'Previs'#227'o do T'#233'rmino'
      FieldName = 'DH_PREVISAO_TERMINO'
      Origin = 'DH_PREVISAO_TERMINO'
    end
    object cdsDataSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataID_CHECKLIST: TIntegerField
      DisplayLabel = 'C'#243'digo do Checklist'
      FieldName = 'ID_CHECKLIST'
      Origin = 'ID_CHECKLIST'
      OnChange = cdsDataID_CHECKLISTChange
    end
    object cdsDataID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object cdsDataID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      OnChange = GerarParcela
    end
    object cdsDataID_SITUACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Situa'#231#227'o'
      FieldName = 'ID_SITUACAO'
      Origin = 'ID_SITUACAO'
      Required = True
    end
    object cdsDataID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
      Required = True
    end
    object cdsDataID_EQUIPAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Equipamento'
      FieldName = 'ID_EQUIPAMENTO'
      Origin = 'ID_EQUIPAMENTO'
    end
    object cdsDataID_USUARIO_RECEPCAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Recep'#231#227'o'
      FieldName = 'ID_USUARIO_RECEPCAO'
      Origin = 'ID_USUARIO_RECEPCAO'
      Required = True
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataVL_TOTAL_BRUTO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Produto'
      FieldName = 'VL_TOTAL_BRUTO_PRODUTO'
      Origin = 'VL_TOTAL_BRUTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_DESCONTO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Produto'
      FieldName = 'VL_TOTAL_DESCONTO_PRODUTO'
      Origin = 'VL_TOTAL_DESCONTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_DESCONTO_PRODUTO: TFMTBCDField
      DisplayLabel = '% Desconto Produto'
      FieldName = 'PERC_TOTAL_DESCONTO_PRODUTO'
      Origin = 'PERC_TOTAL_DESCONTO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Produto'
      FieldName = 'PERC_TOTAL_ACRESCIMO_PRODUTO'
      Origin = 'PERC_TOTAL_ACRESCIMO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_LIQUIDO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Produto'
      FieldName = 'VL_TOTAL_LIQUIDO_PRODUTO'
      Origin = 'VL_TOTAL_LIQUIDO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_BRUTO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Servi'#231'o'
      FieldName = 'VL_TOTAL_BRUTO_SERVICO'
      Origin = 'VL_TOTAL_BRUTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_DESCONTO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Servi'#231'o'
      FieldName = 'VL_TOTAL_DESCONTO_SERVICO'
      Origin = 'VL_TOTAL_DESCONTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_DESCONTO_SERVICO: TFMTBCDField
      DisplayLabel = '% Desconto Servi'#231'o'
      FieldName = 'PERC_TOTAL_DESCONTO_SERVICO'
      Origin = 'PERC_TOTAL_DESCONTO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Servi'#231'o'
      FieldName = 'VL_TOTAL_ACRESCIMO_SERVICO'
      Origin = 'VL_TOTAL_ACRESCIMO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Servi'#231'o'
      FieldName = 'PERC_TOTAL_ACRESCIMO_SERVICO'
      Origin = 'PERC_TOTAL_ACRESCIMO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Produto'
      FieldName = 'VL_TOTAL_ACRESCIMO_PRODUTO'
      Origin = 'VL_TOTAL_ACRESCIMO_PRODUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_LIQUIDO_SERVICO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Servi'#231'o'
      FieldName = 'VL_TOTAL_LIQUIDO_SERVICO'
      Origin = 'VL_TOTAL_LIQUIDO_SERVICO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_BRUTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_BRUTO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_BRUTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = '% Desconto Servi'#231'o Terceiro'
      FieldName = 'PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Origin = 'PERC_TOTAL_DESCONTO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo Servi'#231'o Terceiro'
      FieldName = 'PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Origin = 'PERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_LIQUIDO_SERVICO_TERCEIRO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido Servi'#231'o Terceiro'
      FieldName = 'VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO'
      Origin = 'VL_TOTAL_LIQUIDO_SERVICO_TERCEIRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_TOTAL_BRUTO'
      Origin = 'VL_TOTAL_BRUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_TOTAL_DESCONTO'
      Origin = 'VL_TOTAL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_DESCONTO: TFMTBCDField
      DisplayLabel = '% Total Desconto'
      FieldName = 'PERC_TOTAL_DESCONTO'
      Origin = 'PERC_TOTAL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_TOTAL_ACRESCIMO'
      Origin = 'VL_TOTAL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_TOTAL_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Total Acr'#233'scimo'
      FieldName = 'PERC_TOTAL_ACRESCIMO'
      Origin = 'PERC_TOTAL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.0000'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_PAGAMENTO: TFMTBCDField
      DisplayLabel = 'Vl. Pagamento'
      FieldName = 'VL_PAGAMENTO'
      Origin = 'VL_PAGAMENTO'
      OnChange = cdsDataVL_PAGAMENTOChange
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField
      DisplayLabel = 'Cr'#233'dito Utilizado'
      FieldName = 'VL_PESSOA_CREDITO_UTILIZADO'
      Origin = 'VL_PESSOA_CREDITO_UTILIZADO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_TOTAL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_TOTAL_LIQUIDO'
      Origin = 'VL_TOTAL_LIQUIDO'
      Required = True
      OnChange = cdsDataVL_TOTAL_LIQUIDOChange
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataPROBLEMA_RELATADO: TBlobField
      DisplayLabel = 'Problema Relatado'
      FieldName = 'PROBLEMA_RELATADO'
      Origin = 'PROBLEMA_RELATADO'
    end
    object cdsDataPROBLEMA_ENCONTRADO: TBlobField
      DisplayLabel = 'Problema Encontrado'
      FieldName = 'PROBLEMA_ENCONTRADO'
      Origin = 'PROBLEMA_ENCONTRADO'
    end
    object cdsDataSOLUCAO_TECNICA: TBlobField
      DisplayLabel = 'Solu'#231#227'o T'#233'cnica'
      FieldName = 'SOLUCAO_TECNICA'
      Origin = 'SOLUCAO_TECNICA'
    end
    object cdsDataJOIN_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Origem da Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_DESCRICAO_SITUACAO: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_SITUACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CHECKLIST: TStringField
      DisplayLabel = 'Checklist'
      FieldName = 'JOIN_DESCRICAO_CHECKLIST'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 45
    end
    object cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_OPERACAO: TStringField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_EQUIPAMENTO: TStringField
      DisplayLabel = 'Equipamento'
      FieldName = 'JOIN_DESCRICAO_EQUIPAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_SERIE_EQUIPAMENTO: TStringField
      FieldName = 'JOIN_SERIE_EQUIPAMENTO'
      Origin = 'SERIE'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_IMEI_EQUIPAMENTO: TStringField
      FieldName = 'JOIN_IMEI_EQUIPAMENTO'
      Origin = 'IMEI'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataCC_SALDO_PESSOA_CREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_SALDO_PESSOA_CREDITO'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsDataCC_VL_DIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_VL_DIFERENCA'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsDataCC_VL_TROCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_VL_TROCO'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsDataJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField
      DisplayLabel = 'Cr'#233'dito Dispon'#237'vel'
      FieldName = 'JOIN_VALOR_PESSOA_CREDITO'
      Origin = 'VL_CREDITO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsDataKM: TIntegerField
      FieldName = 'KM'
      Origin = 'KM'
    end
    object cdsDataTANQUE: TStringField
      FieldName = 'TANQUE'
      Origin = 'TANQUE'
      Size = 3
    end
    object cdsDataID_VEICULO: TIntegerField
      DisplayLabel = 'C'#243'digo do Ve'#237'culo'
      FieldName = 'ID_VEICULO'
      Origin = 'ID_VEICULO'
    end
    object cdsDataJOIN_PLACA_VEICULO: TStringField
      DisplayLabel = 'Placa do Ve'#237'culo'
      FieldName = 'JOIN_PLACA_VEICULO'
      Origin = 'PLACA'
      ProviderFlags = []
      Size = 7
    end
    object cdsDataJOIN_ANO_VEICULO: TIntegerField
      DisplayLabel = 'Ano do Ve'#237'culo'
      FieldName = 'JOIN_ANO_VEICULO'
      Origin = 'ANO'
      ProviderFlags = []
    end
    object cdsDataJOIN_COMBUSTIVEL_VEICULO: TStringField
      DisplayLabel = 'Combust'#237'vel do Ve'#237'culo'
      FieldName = 'JOIN_COMBUSTIVEL_VEICULO'
      Origin = 'COMBUSTIVEL'
      ProviderFlags = []
      Size = 30
    end
    object cdsDataJOIN_COR_VEICULO: TStringField
      DisplayLabel = 'Cor do Ve'#237'culo'
      FieldName = 'JOIN_COR_VEICULO'
      Origin = 'JOIN_COR_VEICULO'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_MODELO_VEICULO: TStringField
      DisplayLabel = 'Modelo do Ve'#237'culo'
      FieldName = 'JOIN_MODELO_VEICULO'
      Origin = 'JOIN_MODELO_VEICULO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_MARCA_VEICULO: TStringField
      DisplayLabel = 'Marca do Ve'#237'culo'
      FieldName = 'JOIN_MARCA_VEICULO'
      Origin = 'JOIN_MARCA_VEICULO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDatafdqOrdemServicoProduto: TDataSetField
      FieldName = 'fdqOrdemServicoProduto'
    end
    object cdsDatafdqOrdemServicoChecklist: TDataSetField
      FieldName = 'fdqOrdemServicoChecklist'
    end
    object cdsDatafdqOrdemServicoServico: TDataSetField
      FieldName = 'fdqOrdemServicoServico'
    end
    object cdsDatafdqOrdemServicoParcela: TDataSetField
      FieldName = 'fdqOrdemServicoParcela'
    end
    object cdsDataEQUIPAMENTO: TStringField
      DisplayLabel = 'Equipamento'
      FieldName = 'EQUIPAMENTO'
      Origin = 'EQUIPAMENTO'
      Size = 255
    end
    object cdsDataEQUIPAMENTO_SERIE: TStringField
      DisplayLabel = 'S'#233'rie Equipamento'
      FieldName = 'EQUIPAMENTO_SERIE'
      Origin = 'EQUIPAMENTO_SERIE'
      Size = 255
    end
    object cdsDataEQUIPAMENTO_IMEI: TStringField
      DisplayLabel = 'IMEI Equipamento'
      FieldName = 'EQUIPAMENTO_IMEI'
      Origin = 'EQUIPAMENTO_IMEI'
      Size = 100
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42209.996551284720000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsOrdemServicoChecklist: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOrdemServicoChecklist
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 736
    Top = 80
    object cdsOrdemServicoChecklistID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOrdemServicoChecklistBO_MARCADO: TStringField
      DisplayLabel = 'Selecionado'
      FieldName = 'BO_MARCADO'
      Origin = 'BO_MARCADO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsOrdemServicoChecklistID_CHECKLIST_ITEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Item do Checklist'
      FieldName = 'ID_CHECKLIST_ITEM'
      Origin = 'ID_CHECKLIST_ITEM'
      Required = True
    end
    object cdsOrdemServicoChecklistID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object cdsOrdemServicoChecklistJOIN_DESCRICAO_CHECKLIST: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_CHECKLIST'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsOrdemServicoChecklistJOIN_NR_ITEM_CHECKLIST: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'JOIN_NR_ITEM_CHECKLIST'
      Origin = 'NR_ITEM'
      ProviderFlags = []
    end
  end
  object cdsOrdemServicoServico: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOrdemServicoServico
    Params = <>
    AfterPost = cdsOrdemServicoServicoAfterPost
    AfterDelete = cdsOrdemServicoServicoAfterDelete
    OnNewRecord = cdsOrdemServicoServicoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 764
    Top = 79
    object cdsOrdemServicoServicoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOrdemServicoServicoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsOrdemServicoServicoQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_UNITARIO: TFMTBCDField
      DisplayLabel = 'Vl. Unit'#225'rio'
      FieldName = 'VL_UNITARIO'
      Origin = 'VL_UNITARIO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoVL_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object cdsOrdemServicoServicoID_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo do Servi'#231'o'
      FieldName = 'ID_SERVICO'
      Origin = 'ID_SERVICO'
      Required = True
    end
    object cdsOrdemServicoServicoBO_SERVICO_TERCEIRO: TStringField
      DisplayLabel = 'Servi'#231'o de Terceiro'
      FieldName = 'BO_SERVICO_TERCEIRO'
      Origin = 'BO_SERVICO_TERCEIRO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsOrdemServicoServicoJOIN_DESCRICAO_SERVICO: TStringField
      DisplayLabel = 'Servi'#231'o'
      FieldName = 'JOIN_DESCRICAO_SERVICO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsOrdemServicoServicoJOIN_VALOR_VENDA_SERVICO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'JOIN_VALOR_VENDA_SERVICO'
      Origin = 'VL_VENDA'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoServicoJOIN_DURACAO_SERVICO: TTimeField
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'JOIN_DURACAO_SERVICO'
      Origin = 'DURACAO'
      ProviderFlags = []
    end
  end
  object cdsOrdemServicoProduto: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOrdemServicoProduto
    Params = <>
    AfterPost = cdsOrdemServicoProdutoAfterPost
    AfterDelete = cdsOrdemServicoProdutoAfterDelete
    OnNewRecord = cdsOrdemServicoProdutoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 793
    Top = 79
    object cdsOrdemServicoProdutoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOrdemServicoProdutoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsOrdemServicoProdutoQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_UNITARIO: TFMTBCDField
      DisplayLabel = 'Vl. Unit'#225'rio'
      FieldName = 'VL_UNITARIO'
      Origin = 'VL_UNITARIO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoVL_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoProdutoID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object cdsOrdemServicoProdutoID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsOrdemServicoProdutoJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsOrdemServicoProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object cdsOrdemServicoProdutoJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
  end
  object cdsOrdemServicoParcela: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOrdemServicoParcela
    Params = <>
    BeforePost = cdsOrdemServicoParcelaBeforePost
    AfterPost = cdsOrdemServicoParcelaAfterPost
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 822
    Top = 79
    object cdsOrdemServicoParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOrdemServicoParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsOrdemServicoParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      OnChange = ModificarParcelaAposParcelamento
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsOrdemServicoParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Total de Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object cdsOrdemServicoParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsOrdemServicoParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
      OnChange = cdsOrdemServicoParcelaDT_VENCIMENTOChange
    end
    object cdsOrdemServicoParcelaIC_DIAS: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Dias'
      FieldKind = fkInternalCalc
      FieldName = 'IC_DIAS'
      OnChange = cdsOrdemServicoParcelaIC_DIASChange
    end
    object cdsOrdemServicoParcelaID_ORDEM_SERVICO: TIntegerField
      DisplayLabel = 'C'#243'digo da Ordem de Servi'#231'o'
      FieldName = 'ID_ORDEM_SERVICO'
      Origin = 'ID_ORDEM_SERVICO'
    end
    object cdsOrdemServicoParcelaOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object cdsOrdemServicoParcelaCHEQUE_SACADO: TStringField
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsOrdemServicoParcelaCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsOrdemServicoParcelaCHEQUE_BANCO: TStringField
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsOrdemServicoParcelaCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsOrdemServicoParcelaCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsOrdemServicoParcelaCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsOrdemServicoParcelaCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsOrdemServicoParcelaCHEQUE_DT_VENCIMENTO: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object cdsOrdemServicoParcelaID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsOrdemServicoParcelaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object cdsOrdemServicoParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Tipo da Forma de Pagamento'
      FieldName = 'JOIN_TIPO_FORMA_PAGAMENTO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
    object cdsOrdemServicoParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsOrdemServicoParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object cdsOrdemServicoParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsChecklist: TDataSource
    DataSet = cdsOrdemServicoChecklist
    Left = 706
    Top = 81
  end
end
