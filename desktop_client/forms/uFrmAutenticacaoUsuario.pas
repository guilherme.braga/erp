unit uFrmAutenticacaoUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  cxTextEdit, cxLabel, System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons,
  cxGroupBox, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrmAutenticacaoUsuario = class(TFrmModalPadrao)
    cxLabel1: TcxLabel;
    EdtUsuario: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    EdtSenha: TcxTextEdit;
    procedure EdtSenhaExit(Sender: TObject);
  private
    FNivelMinimoPermitido: Integer;
    FIdUsuario: Integer;
  public
    class function Autorizado(const ANivelMinimoPermitido: Integer = 0): Boolean;
    class function Autenticar(var AIdUsuario: Integer): Boolean;
  protected
    procedure Confirmar; override;
  end;

var
  FrmAutenticacaoUsuario: TFrmAutenticacaoUsuario;

implementation

{$R *.dfm}

uses uTVariables, uFrmPrincipal, uTControl_Function, uTMessage, uTUsuario;

{ TFrmAutenticacaoUsuario }

class function TFrmAutenticacaoUsuario.Autenticar(var AIdUsuario: Integer): Boolean;
begin
  Application.CreateForm(TFrmAutenticacaoUsuario, FrmAutenticacaoUsuario);
  try
    FrmAutenticacaoUsuario.ShowModal;
    result := FrmAutenticacaoUsuario.Result = MCONFIRMED;
    if result then
    begin
      AIdUsuario := FrmAutenticacaoUsuario.FIdUsuario;
    end
    else
    begin
      AIdUsuario := 0;
    end;
  finally
    FreeAndNil(FrmAutenticacaoUsuario);
  end;
end;

class function TFrmAutenticacaoUsuario.Autorizado(
  const ANivelMinimoPermitido: Integer = 0): Boolean;
begin
  Application.CreateForm(TFrmAutenticacaoUsuario, FrmAutenticacaoUsuario);
  try
    FrmAutenticacaoUsuario.FNivelMinimoPermitido := ANivelMinimoPermitido;
    FrmAutenticacaoUsuario.ShowModal;
    result := FrmAutenticacaoUsuario.Result = MCONFIRMED;
  finally
    FreeAndNil(FrmAutenticacaoUsuario);
  end;
end;

procedure TFrmAutenticacaoUsuario.Confirmar;
begin
  inherited;
  if Variables.UserControl.VerificaLogin(EdtUsuario.Text, EdtSenha.Text) = 0 then
  begin
    FIdUsuario := TUsuario.GetIdUsuario(EdtUsuario.Text);
    Self.Result := MCONFIRMED;
  end
  else
  begin
    TMessage.MessageInformation('O Usu�rio ou senha informados est�o incorretos.');
    Self.Result := MCANCELED;
    abort;
  end;
end;

procedure TFrmAutenticacaoUsuario.EdtSenhaExit(Sender: TObject);
begin
  inherited;
  if (EdtUsuario.Text <> EmptyStr) and (EdtSenha.Text <> EmptyStr) then
  begin
    ActConfirmar.Execute;
  end;
end;

end.
