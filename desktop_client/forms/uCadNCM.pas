unit uCadNCM;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEditPK,
  Vcl.StdCtrls, cxTextEdit, cxDBEdit, uGBDBTextEdit, uGBPanel,
  uFrameNCMEspecializado, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon;

type
  TCadNCM = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataCODIGO: TIntegerField;
    cdsDataDESCRICAO: TStringField;
    cdsDataUN: TStringField;
    cdsDatafdqNCMEspecializado: TDataSetField;
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsNCMEspecializado: TGBClientDataSet;
    cdsNCMEspecializadoID: TAutoIncField;
    cdsNCMEspecializadoDESCRICAO: TStringField;
    cdsNCMEspecializadoID_NCM: TIntegerField;
    cdsNCMEspecializadoJOIN_CODIGO_NCM: TIntegerField;
    cdsNCMEspecializadoJOIN_DESCRICAO_NCM: TStringField;
    PnNCMEspecializado: TgbPanel;
    gbPanel2: TgbPanel;
    Label1: TLabel;
    edDescricao: TgbDBTextEdit;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    Label4: TLabel;
  private
    FFrameNCMEspecilizado: TFrameNCMEspecializado;
  public
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection;

{ TCadNCM }

procedure TCadNCM.InstanciarFrames;
begin
  inherited;
  FFrameNCMEspecilizado := TFrameNCMEspecializado.Create(PnNCMEspecializado);
  FFrameNCMEspecilizado.Parent := PnNCMEspecializado;
  FFrameNCMEspecilizado.Align := alClient;
  FFrameNCMEspecilizado.SetConfiguracoesIniciais(cdsNCMEspecializado);
end;

initialization
  RegisterClass(TCadNCM);

finalization
  UnRegisterClass(TCadNCM);

end.
