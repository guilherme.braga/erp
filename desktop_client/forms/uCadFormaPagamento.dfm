inherited CadFormaPagamento: TCadFormaPagamento
  Caption = 'Cadastro de Forma de Pagamento'
  ClientWidth = 918
  ExplicitWidth = 934
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 918
    ExplicitWidth = 918
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 918
    Properties.ActivePage = cxtsData
    ExplicitWidth = 918
    ClientRectRight = 918
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 918
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 918
        ExplicitWidth = 918
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 918
      inherited DesignPanel: TJvDesignPanel
        Width = 918
        ExplicitWidth = 918
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 918
          Width = 918
          inherited dxBevel1: TdxBevel
            Width = 914
            ExplicitWidth = 914
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label1: TLabel
            Left = 382
            Top = 40
            Width = 81
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tipo da Quita'#231#227'o'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 68
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 310
          end
          object descTipoQuitacao: TgbDBTextEdit
            Left = 524
            Top = 36
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_TIPO_QUITACAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 385
          end
          object edtIdTipoQuitacao: TgbDBButtonEditFK
            Left = 467
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_TIPO_QUITACAO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = descTipoQuitacao
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 
              'ID_TIPO_QUITACAO;JOIN_DESCRICAO_TIPO_QUITACAO;JOIN_TIPO_TIPO_QUI' +
              'TACAO'
            gbTableName = 'TIPO_QUITACAO'
            gbCamposConsulta = 'ID;DESCRICAO;TIPO'
            Width = 60
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Forma de Pagamento'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspFormaPagamento'
    RemoteServer = DmConnection.dspFinanceiro
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsDataID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo de Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      DisplayLabel = 'Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_TIPO_TIPO_QUITACAO: TStringField
      DisplayLabel = 'Tipo do Tipo de Quita'#231#227'o'
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 50
    end
  end
end
