unit uRelContaCorrenteMovimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, dxBar, System.Actions, Vcl.ActnList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxGroupBox, dxRibbon, cxBarEditItem, cxCalendar,
  cxButtonEdit, cxDropDownEdit, uGBPanel, dxBarExtItems, cxVGrid,
  cxInplaceContainer;

type
  TRelContaCorrenteMovimento = class(TFrmRelatorioFRPadrao)
    fdmFiltrosDT_MOVIMENTO_INICIO: TDateField;
    fdmFiltrosDESCRICAO: TStringField;
    FdmFiltrosVL_MOVIMENTO: TFMTBCDField;
    FdmFiltrosDOCUMENTO: TStringField;
    FdmFiltrosDT_COMPETENCIA_INICIO: TDateField;
    FdmFiltrosDT_MOVIMENTO_TERMINO: TDateField;
    FdmFiltrosDT_COMPETENCIA_TERMINO: TDateField;
    FdmFiltrosID: TIntegerField;
    FdmFiltrosID_CONTA_CORRENTE: TLargeintField;
    FdmFiltrosID_CONTA_ANALISE: TLargeintField;
    FdmFiltrosID_CENTRO_RESULTADO: TLargeintField;
    rowCentroResultado: TcxEditorRow;
    rowContaCorrente: TcxEditorRow;
    rowContaAnalise: TcxEditorRow;
    procedure vgFiltrosEditorRow1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure vgFiltrosEditorRow2EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure vgFiltrosEditorRow3EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private

  public

  protected
    procedure SetWhereBundle; override;
    procedure PrepararFiltros; override;
  end;

implementation

{$R *.dfm}

uses uContaCorrente, uDatasetUtils, uContaCorrenteProxy, uContaAnaliseProxy,
  uCentroResultadoProxy, uFrmConsultaPadrao, uPesqContaAnalise;

{ TRelContaCorrenteMovimento }

procedure TRelContaCorrenteMovimento.PrepararFiltros;
var campo: TcxEditorRow;
begin
  inherited;
  {campo := BuscarColunaPeloNomeCampo(FdmFiltrosID_CONTA_CORRENTE.FieldName);
  FListaIndentificadoresConsultaFK.Add(campo.Properties.Hint, TContaCorrenteProxy.NOME_TABELA);
  TcxButtonEditProperties(campo.Properties).OnButtonClick := vgFiltrosEditorRow1EditPropertiesButtonClick;


  ConfigurarCampoConsultaFK(BuscarColunaPeloNomeCampo(FdmFiltrosID_CONTA_ANALISE.FieldName),
    TContaAnaliseProxy.NOME_TABELA);
  ConfigurarCampoConsultaFK(BuscarColunaPeloNomeCampo(FdmFiltrosID_CENTRO_RESULTADO.FieldName),
    TCentroResultadoProxy.NOME_TABELA);}
end;

procedure TRelContaCorrenteMovimento.SetWhereBundle;
begin
  inherited;
  AddFiltroIgual(FIELD_ID, fdmFiltrosID);
  AddFiltroIgual(FIELD_DOCUMENTO, FdmFiltrosDOCUMENTO);
  AddFiltroIgual(FIELD_DESCRICAO, fdmFiltrosDESCRICAO);

  AddFiltroEntre(FIELD_DATA_MOVIMENTO, fdmFiltrosDT_MOVIMENTO_INICIO,
    FdmFiltrosDT_MOVIMENTO_TERMINO);

  AddFiltroEntre(FIELD_DATA_COMPETENCIA, FdmFiltrosDT_COMPETENCIA_INICIO,
    FdmFiltrosDT_COMPETENCIA_TERMINO);

  AddFiltroIgual(FIELD_ID_CONTA_CORRENTE, FdmFiltrosID_CONTA_CORRENTE);
  AddFiltroIgual(FIELD_ID_CONTA_ANALISE, FdmFiltrosID_CONTA_ANALISE);
  AddFiltroIgual(FIELD_ID_CENTRO_RESULTADO, FdmFiltrosID_CENTRO_RESULTADO);
  AddFiltroIgual(FIELD_VL_MOVIMENTO, FdmFiltrosVL_MOVIMENTO);
end;

procedure TRelContaCorrenteMovimento.vgFiltrosEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TCentroResultadoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowCentroResultado.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaCorrenteMovimento.vgFiltrosEditorRow2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TContaCorrenteProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowContaCorrente.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaCorrenteMovimento.vgFiltrosEditorRow3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
  idCentroResultado: Integer;
begin
  inherited;
  idCentroResultado := StrtoIntDef(VartoStr(rowCentroResultado.Properties.Value),0);

  if idCentroResultado = 0 then
  begin
   IDConsultado := TFrmConsultaPadrao.ConsultarID(TContaAnaliseProxy.NOME_TABELA);
  end
  else
  begin
    IDConsultado := TPesqContaAnalise.IDSelecionado(idCentroResultado);
  end;

  if IDConsultado > 0 then
  begin
    rowContaAnalise.Properties.Value := IDConsultado;
  end;
end;

initialization
  RegisterClass(TRelContaCorrenteMovimento);

Finalization
  UnRegisterClass(TRelContaCorrenteMovimento);

end.
