inherited MovInventarioDefinirContagemValida: TMovInventarioDefinirContagemValida
  Caption = 'Definir Contagem'
  ClientHeight = 133
  ClientWidth = 339
  ExplicitWidth = 345
  ExplicitHeight = 162
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 339
    ExplicitHeight = 77
    Height = 77
    Width = 339
    object Label1: TLabel
      Left = 9
      Top = 18
      Width = 318
      Height = 13
      Caption = 'Qual contagem ser'#225' utilizada para corre'#231#227'o do estoque?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbConferencia: TcxComboBox
      Left = 9
      Top = 37
      ParentFont = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        'Primeira Contagem'
        'Segunda Contagem'
        'Terceira Contagem')
      Properties.PostPopupValueOnTab = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      Text = 'Primeira Contagem'
      Width = 318
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 77
    ExplicitTop = 77
    ExplicitWidth = 339
    Width = 339
    inherited BtnConfirmar: TcxButton
      Left = 187
      ExplicitLeft = 187
    end
    inherited BtnCancelar: TcxButton
      Left = 262
      ExplicitLeft = 262
    end
  end
  inherited ActionList: TActionList
    Left = 182
    Top = 14
  end
end
