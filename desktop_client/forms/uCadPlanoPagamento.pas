unit uCadPlanoPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxSpinEdit,
  cxDBEdit, uGBDBSpinEdit, uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK,
  Vcl.StdCtrls, cxRadioGroup, uGBDBRadioGroup, uGBPanel, dxBarApplicationMenu,
  cxCheckBox, uGBDBCheckBox;

type
  TCadPlanoPagamento = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataQT_PARCELA: TIntegerField;
    cdsDataPRAZO: TStringField;
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    cdsDataTIPO: TStringField;
    gbDBRadioGroup1: TgbDBRadioGroup;
    Label5: TLabel;
    gbDados: TgbPanel;
    gbPrazoFixo: TgbPanel;
    Label4: TLabel;
    Label2: TLabel;
    gbDBTextEdit9: TgbDBTextEdit;
    gbDBSpinEdit1: TgbDBSpinEdit;
    Label3: TLabel;
    gbDBSpinEdit2: TgbDBSpinEdit;
    gbPrazoPersonalizado: TgbPanel;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataDIA_PARCELA: TIntegerField;
    gbDBCheckBox1: TgbDBCheckBox;
    cdsDataBO_ATIVO: TStringField;
    procedure cdsDataTIPOChange(Sender: TField);
  private
    procedure CondicionarPrazo;
    procedure TratarVisibilidadeDosCamposPrazo;
  protected
    procedure AntesDeConfirmar;override;
    procedure GerenciarControles;override;
  public

  end;

implementation

{$R *.dfm}

uses uDmConnection, uPlanoPagamento, uDatasetUtils;

procedure TCadPlanoPagamento.AntesDeConfirmar;
begin
  inherited;
  if cdsDataTIPO.AsString.Equals(PRAZO_FIXO) then
    TValidacaoCampo.CampoPreenchido(cdsDataDIA_PARCELA);

  if cdsDataTIPO.AsString.Equals(PRAZO_PERSONALIZADO) then
    TValidacaoCampo.CampoPreenchido(cdsDataPRAZO);
end;

procedure TCadPlanoPagamento.cdsDataTIPOChange(Sender: TField);
begin
  inherited;
  CondicionarPrazo;
end;

procedure TCadPlanoPagamento.CondicionarPrazo;
begin
  if cdsData.State in dsEditModes then
  begin
    cdsDataPRAZO.Clear;
    cdsDataDIA_PARCELA.Clear;
  end;
  TratarVisibilidadeDosCamposPrazo;
end;

procedure TCadPlanoPagamento.GerenciarControles;
begin
  inherited;
  TratarVisibilidadeDosCamposPrazo;
end;

procedure TCadPlanoPagamento.TratarVisibilidadeDosCamposPrazo;
begin
  gbPrazoFixo.Visible := cdsDataTIPO.AsString.Equals(PRAZO_FIXO);
  gbPrazoPersonalizado.Visible := cdsDataTIPO.AsString.Equals(PRAZO_PERSONALIZADO);
end;

Initialization
  RegisterClass(TCadPlanoPagamento);
Finalization
  UnregisterClass(TCadPlanoPagamento);

end.
