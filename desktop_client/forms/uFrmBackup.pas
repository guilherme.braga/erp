unit uFrmBackup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, JvComponentBase, JvZlibMultiple, Vcl.ComCtrls, JvExStdCtrls, JvListBox, JvDriveCtrls,
  JvCombobox, uGBPanel, JvDialogs;

type
  TFrmBackup = class(TFrmModalPadrao)
    Label1: TLabel;
    Label2: TLabel;
    LbHT: TLabel;
    LbHI: TLabel;
    sdSalvarBackup: TJvSaveDialog;
    ActCopiarUltimoBackup: TAction;
    cxButton1: TcxButton;
    procedure CBCompacta_Backup_ManualmenteClick(Sender: TObject);
    procedure ActCopiarUltimoBackupExecute(Sender: TObject);
  private
    procedure CompactarAquivos;
    procedure ConfigurarExibicaoTermino(AExibir: Boolean);
    procedure SalvarBackup;
  public

  protected
    procedure Confirmar; override;
  end;

var
  FrmBackup: TFrmBackup;

{$R *.dfm}

implementation

uses uTVariables, uIniFileClient, uFrmMessage, uFrmMessage_Process, uWindowsUtils, uBackup, uSistema;

procedure TFrmBackup.CompactarAquivos;
var z1, z2: TJvZlibMultiple;
  TempList: TStrings;
  arquivoBack: string;
begin
 {
  try
    StaticText1.Caption := 'Compactando Arquivos XML e NFe!';
    Application.ProcessMessages;

    //arquivoBack := LabelDiretorio.Caption + '\Backup_XML_NFE_' + Dm.Database.Database + '_' + Retorna_Data_Hora_Minito_Segundo + '.z';

    TempList := TStringList.Create;
    //ListarArquivos(TempList, TWindowsUtils.DiretorioAplicacao + '\xml\', '*.xml', true, true);

    If FDiretorioNFE <> '' Then
      ListarArquivos(TempList, FDiretorioNFE, '*.xml', true, true);

    If FDiretorioNFEImportada <> '' Then
      ListarArquivos(TempList, FDiretorioNFEImportada, '*.xml', true, true);

    z1 := TJvZlibMultiple.Create(nil);
    try
      z1.CompressFiles(TempList, arquivoBack);
    finally
      z1.Free;
      Screen.Cursor := crDefault;
    end;

    TempList.Clear;

    arquivoBack := LabelDiretorio.Caption + '\Backup_BASE_' + Dm.Database.Database + '_' + Retorna_Data_Hora_Minito_Segundo + '.z';
    StaticText1.Caption := 'Compactando Arquivos Base!';
    Application.ProcessMessages;
    TempList.Add(arquivoBackSql);

    z2 := TJvZlibMultiple.Create(nil);
    try
      z2.CompressFiles(TempList, arquivoBack);
    finally
      z2.Free;
      Screen.Cursor := crDefault;
    end;

    if (CBLogs.Checked) then
      begin
        TempList.Clear;
        arquivoBack := LabelDiretorio.Caption + '\Backup_BASE_LOG' + TZConnection(DmGeral.FindComponent('DatabaseLog')).Database + '_' + Retorna_Data_Hora_Minito_Segundo + '.z';
        StaticText1.Caption := 'Compactando Arquivos Base de Log!';
        Application.ProcessMessages;
        TempList.Add(arquivoLogBackSql);

        z2 := TJvZlibMultiple.Create(nil);
        try
          z2.CompressFiles(TempList, arquivoBack);
        finally
          z2.Free;
          Screen.Cursor := crDefault;
        end;
      end;

    if (CBImagens.Checked) then
      begin
        TempList.Clear;
        arquivoBack := LabelDiretorio.Caption + '\Backup_BASE_IMAGEM' + TZConnection(Dm.FindComponent('Database_Imagens')).Database + '_' + Retorna_Data_Hora_Minito_Segundo + '.z';
        StaticText1.Caption := 'Compactando Arquivos Base de Imagens!';
        Application.ProcessMessages;
        TempList.Add(arquivoImgBackSql);

        z2 := TJvZlibMultiple.Create(nil);
        try
          z2.CompressFiles(TempList, arquivoBack);
        finally
          z2.Free;
          Screen.Cursor := crDefault;
        end;
      end;

    // Apaga o Sql.
    if FileExists(arquivoBackSql) then DeleteFile(arquivoBackSql);
    if FileExists(arquivoLogBackSql) then DeleteFile(arquivoLogBackSql);
    if FileExists(arquivoImgBackSql) then DeleteFile(arquivoImgBackSql);

    StaticText1.Caption := 'Backup Finalizado!';
    Application.ProcessMessages;

    TempList.Clear;
  finally
  end;    }
end;

procedure TFrmBackup.ConfigurarExibicaoTermino(AExibir: Boolean);
begin
  LbHT.Visible := AExibir;
end;

procedure TFrmBackup.Confirmar;
begin
  inherited;
  LbHI.Caption := '';
  ConfigurarExibicaoTermino(false);

  if TFrmMessage.Question('A opera��o de backup pode levar alguns minutos, desejar realizar o backup agora?') = MCANCELED then
  begin
    exit;
  end;

  try
    LbHI.Caption := TimeToStr(Now);
    TFrmMessage_Process.SendMessage('Realizando backup');

    if not TBackup.RealizarBackup(TSistema.Sistema.Usuario.Id) then
    begin
      exit;
    end;

    SalvarBackup;

    TFrmMessage.Sucess('Backup gerado com sucesso!');
  finally
    LbHT.Caption := TimeToStr(Now);
    ConfigurarExibicaoTermino(true);
    TFrmMessage_Process.CloseMessage;
  end;

  abort;
end;

procedure TFrmBackup.SalvarBackup;
begin
  try
    TFrmMessage_Process.SendMessage('Salvando c�pia do backup');
    if sdSalvarBackup.Execute then
    begin
      TBackup.DownloadBackup(sdSalvarBackup.FileName);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmBackup.ActCopiarUltimoBackupExecute(Sender: TObject);
begin
  inherited;
  SalvarBackup;
end;

procedure TFrmBackup.CBCompacta_Backup_ManualmenteClick(Sender: TObject);
begin
  inherited;
  //ShowMessage('Caso fa�a o Bakcup Manual do ApiceFiscal, tamb�m dever� copiar a pasta de XML das notas p/ a m�dia');
end;

end.
