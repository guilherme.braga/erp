inherited FrmConfigurarValoresPadroes: TFrmConfigurarValoresPadroes
  Caption = 'Configurar Valores Padr'#245'es'
  ClientHeight = 384
  ClientWidth = 507
  ExplicitWidth = 513
  ExplicitHeight = 413
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 507
    ExplicitHeight = 328
    Height = 328
    Width = 507
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 328
    ExplicitTop = 328
    ExplicitWidth = 507
    Width = 507
    inherited BtnConfirmar: TcxButton
      Left = 355
      ExplicitLeft = 355
    end
    inherited BtnCancelar: TcxButton
      Left = 430
      ExplicitLeft = 430
    end
    object cxButton1: TcxButton
      Left = 2
      Top = 2
      Width = 75
      Height = 52
      Align = alLeft
      Action = ActConstantes
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
      ExplicitLeft = 355
    end
  end
  object GridValoresPadrao: TcxVerticalGrid [2]
    Left = 0
    Top = 0
    Width = 507
    Height = 328
    Align = alClient
    OptionsView.RowHeaderWidth = 194
    TabOrder = 2
    Version = 1
  end
  inherited ActionList: TActionList
    Left = 238
    Top = 326
    object ActConstantes: TAction
      Caption = 'Constantes'
      Hint = 'Exibir as constantes suportadas'
      ImageIndex = 48
      OnExecute = ActConstantesExecute
    end
  end
end
