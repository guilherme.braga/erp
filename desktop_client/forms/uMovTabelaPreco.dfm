inherited MovTabelaPreco: TMovTabelaPreco
  Caption = 'Tabela de Pre'#231'o'
  ClientHeight = 522
  ClientWidth = 1061
  ExplicitWidth = 1077
  ExplicitHeight = 561
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1061
    ExplicitWidth = 1061
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1061
    Height = 395
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1061
    ExplicitHeight = 395
    ClientRectBottom = 395
    ClientRectRight = 1061
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1061
      ExplicitHeight = 371
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1029
        Height = 371
        ExplicitWidth = 1061
        ExplicitHeight = 371
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 371
        Height = 371
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 371
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1061
      ExplicitHeight = 371
      inherited DesignPanel: TJvDesignPanel
        Width = 1061
        Height = 371
        ExplicitWidth = 1061
        ExplicitHeight = 371
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1061
          ExplicitHeight = 371
          Height = 371
          Width = 1061
          inherited dxBevel1: TdxBevel
            Width = 1057
            ExplicitWidth = 1057
          end
          object lCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object EdPkCodigo: TgbDBTextEditPK
            Left = 60
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              1057
              111)
            Height = 111
            Width = 1057
            object bevelInformacoes: TdxBevel
              Left = 2
              Top = 2
              Width = 1053
              Height = 57
              Align = alTop
              Shape = dxbsLineBottom
              ExplicitWidth = 928
            end
            object lDescricao: TLabel
              Left = 8
              Top = 6
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object lFilial: TLabel
              Left = 8
              Top = 31
              Width = 20
              Height = 13
              Caption = 'Filial'
            end
            object labelAjuda: TLabel
              Left = 8
              Top = 59
              Width = 465
              Height = 13
              Caption = 
                'Os Valores preenchidos aqui ser'#227'o replicados para todos os Itens' +
                ' Filtrados na Grade de Produtos'
            end
            object edDescricao: TgbDBTextEdit
              Left = 60
              Top = 3
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 995
            end
            object EdFkFilial: TgbDBButtonEditFK
              Left = 59
              Top = 28
              DataBinding.DataField = 'ID_FILIAL'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Style.Color = 14606074
              TabOrder = 1
              gbTextEdit = descFilial
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_FILIAL;JOIN_DESCRICAO_FILIAL'
              gbTableName = 'FILIAL'
              gbCamposConsulta = 'ID;FANTASIA'
              gbIdentificadorConsulta = 'FILIAL'
              Width = 60
            end
            object descFilial: TgbDBTextEdit
              Left = 119
              Top = 28
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_FILIAL'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbReadyOnly = True
              gbPassword = False
              Width = 936
            end
            object gpAlteracao: TgbGroupBox
              AlignWithMargins = True
              Left = 2
              Top = 69
              Anchors = [akLeft, akTop, akRight]
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              Height = 36
              Width = 1053
              object labelPercLucro: TLabel
                Left = 12
                Top = 15
                Width = 40
                Height = 13
                Caption = '% Lucro'
              end
              object labelVlVenda: TLabel
                Left = 165
                Top = 15
                Width = 45
                Height = 13
                Caption = 'Vl. Venda'
              end
              object labelPercReajuste: TLabel
                Left = 323
                Top = 15
                Width = 57
                Height = 13
                Caption = '% Reajuste'
              end
              object edtPercLucro: TgbDBCalcEdit
                Left = 56
                Top = 11
                DataBinding.DataField = 'PERC_LUCRO'
                DataBinding.DataSource = dsValoresAlteracao
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Width = 97
              end
              object edtVlVenda: TgbDBCalcEdit
                Left = 214
                Top = 11
                DataBinding.DataField = 'VL_VENDA'
                DataBinding.DataSource = dsValoresAlteracao
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Width = 97
              end
              object cxButton1: TcxButton
                Left = 485
                Top = 8
                Width = 205
                Height = 25
                Caption = 'Aplicar aos Produtos Filtrados'
                SpeedButtonOptions.Flat = True
                TabOrder = 2
                OnClick = actAplicarExecute
              end
              object edtPercReajuste: TgbDBCalcEdit
                Left = 384
                Top = 11
                DataBinding.DataField = 'PERC_REAJUSTE'
                DataBinding.DataSource = dsValoresAlteracao
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Width = 97
              end
            end
          end
          inline FrameTabelaPrecoItem1: TFrameTabelaPrecoItem
            Left = 2
            Top = 145
            Width = 1057
            Height = 224
            Align = alClient
            TabOrder = 2
            ExplicitLeft = 2
            ExplicitTop = 145
            ExplicitWidth = 1057
            ExplicitHeight = 224
            inherited cxpcMain: TcxPageControl
              Width = 1057
              Height = 183
              Properties.ActivePage = FrameTabelaPrecoItem1.cxtsSearch
              ExplicitWidth = 1057
              ExplicitHeight = 183
              ClientRectBottom = 183
              ClientRectRight = 1057
              inherited cxtsSearch: TcxTabSheet
                ExplicitWidth = 1057
                ExplicitHeight = 159
                inherited cxGridPesquisaPadrao: TcxGrid
                  Width = 1057
                  Height = 159
                  ExplicitWidth = 1057
                  ExplicitHeight = 159
                end
              end
              inherited cxtsData: TcxTabSheet
                inherited cxGBDadosMain: TcxGroupBox
                  inherited dxBevel1: TdxBevel
                    ExplicitWidth = 1053
                  end
                  inherited descProduto: TgbDBTextEdit
                    ExplicitWidth = 2081
                    Width = 2081
                  end
                  inherited lbProdutoJaInserido: TcxLabel
                    Style.IsFontAssigned = True
                    AnchorY = 15
                  end
                end
              end
            end
            inherited PnTituloFrameDetailPadrao: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 1057
              Width = 1057
            end
            inherited dxBarManagerPadrao: TdxBarManager
              DockControlHeights = (
                0
                0
                22
                0)
              inherited barNavegador: TdxBar
                DockedLeft = 401
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    FetchOnDemand = False
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspTabelaPreco'
    RemoteServer = DmConnection.dspTabelaPreco
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 45
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDatafdqTabelaPrecoItem: TDataSetField
      FieldName = 'fdqTabelaPrecoItem'
    end
    object cdsDataJOIN_DESCRICAO_FILIAL: TStringField
      DisplayLabel = 'Desc. Filial'
      FieldName = 'JOIN_DESCRICAO_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42209.931946493050000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsTabelaPrecoItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqTabelaPrecoItem
    Params = <>
    AfterOpen = cdsTabelaPrecoItemAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 544
    Top = 80
    object cdsTabelaPrecoItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsTabelaPrecoItemID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
    end
    object cdsTabelaPrecoItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsTabelaPrecoItemPERC_LUCRO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      Origin = 'PERC_LUCRO'
      OnChange = CalcularValor
      DisplayFormat = '###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsTabelaPrecoItemVL_VENDA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      OnChange = CalcularValor
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemtipo: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'tipo'
      Origin = 'TIPO'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object cdsTabelaPrecoItemid_grupo_produto: TIntegerField
      DisplayLabel = 'C'#243'd. Gr. Produto'
      FieldName = 'id_grupo_produto'
      Origin = 'ID_GRUPO_PRODUTO'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      DisplayLabel = 'Grupo Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_sub_grupo_produto: TIntegerField
      DisplayLabel = 'C'#243'd. Sub. Gr. Produto'
      FieldName = 'id_sub_grupo_produto'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField
      DisplayLabel = 'Sub Grupo Produto'
      FieldName = 'JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_categoria: TIntegerField
      DisplayLabel = 'C'#243'd. Categoria'
      FieldName = 'id_categoria'
      Origin = 'ID_CATEGORIA'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_CATEGORIA: TStringField
      DisplayLabel = 'Categoria'
      FieldName = 'JOIN_DESCRICAO_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_sub_categoria: TIntegerField
      DisplayLabel = 'C'#243'd. Sub Categoria'
      FieldName = 'id_sub_categoria'
      Origin = 'ID_SUB_CATEGORIA'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_SUB_CATEGORIA: TStringField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'JOIN_DESCRICAO_SUB_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_modelo: TIntegerField
      DisplayLabel = 'C'#243'd. Modelo'
      FieldName = 'id_modelo'
      Origin = 'ID_MODELO'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_MODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_unidade_estoque: TIntegerField
      DisplayLabel = 'C'#243'd. Un. Estoque'
      FieldName = 'id_unidade_estoque'
      Origin = 'ID_UNIDADE_ESTOQUE'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'Un. Estoque'
      FieldName = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_marca: TIntegerField
      DisplayLabel = 'C'#243'd. Marca'
      FieldName = 'id_marca'
      Origin = 'ID_MARCA'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_MARCA: TStringField
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemid_linha: TIntegerField
      DisplayLabel = 'C'#243'd. Linha'
      FieldName = 'id_linha'
      Origin = 'ID_LINHA'
      ProviderFlags = []
    end
    object cdsTabelaPrecoItemJOIN_DESCRICAO_LINHA: TStringField
      DisplayLabel = 'Linha'
      FieldName = 'JOIN_DESCRICAO_LINHA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo Operacional Imposto'
      FieldName = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      OnChange = CalcularValor
      Precision = 24
      Size = 9
    end
  end
  object fdmValoresAlteracao: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 592
    Top = 289
    object fdmValoresAlteracaoVL_CUSTO: TFloatField
      FieldName = 'VL_CUSTO'
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmValoresAlteracaoVL_CUSTO_OPERACIONAL: TFloatField
      FieldName = 'VL_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmValoresAlteracaoPERC_LUCRO: TFloatField
      FieldName = 'PERC_LUCRO'
      DisplayFormat = '###,##0.00'
    end
    object fdmValoresAlteracaoVL_VENDA: TFloatField
      FieldName = 'VL_VENDA'
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmValoresAlteracaoPERC_REAJUSTE: TFloatField
      FieldName = 'PERC_REAJUSTE'
      DisplayFormat = '###,##0.00'
    end
  end
  object dsValoresAlteracao: TDataSource
    DataSet = fdmValoresAlteracao
    Left = 664
    Top = 273
  end
  object alTabelaPreco: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 528
    Top = 264
    object ActAplicarAlteracoes: TAction
      Caption = 'ActAplicarAlteracoes'
      Hint = 
        'Aplicar as altera'#231#245'es de valores para todos os produtos filtrado' +
        's abaixo'
      ImageIndex = 18
    end
  end
end
