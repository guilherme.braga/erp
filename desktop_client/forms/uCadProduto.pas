unit uCadProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxCheckBox, cxDBEdit,
  uGBDBCheckBox, uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, cxDropDownEdit,
  uFrameDetailPadrao, uFrameProdutoFornecedor, ACBrBase, ACBrETQ, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxCalc, uGBDBCalcEdit,
  cxRadioGroup, uGBDBRadioGroup, ugbDBValorComPercentual, uGBPanel, cxCurrencyEdit, uGBFDMemTable,
  uUsuarioDesignControl, cxCalendar, uGBDBDateEdit, uFrameFiltroVerticalPadrao, cxSplitter, JvExControls,
  JvButton, JvTransparentButton, uFrameConsultaDadosDetalheGrade, uFrameProdutoMontadora,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter;

type
  TCadProduto = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    Label6: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID_GRUPO_PRODUTO: TIntegerField;
    cdsDataID_SUB_GRUPO_PRODUTO: TIntegerField;
    cdsDataID_MARCA: TIntegerField;
    cdsDataID_CATEGORIA: TIntegerField;
    cdsDataID_SUB_CATEGORIA: TIntegerField;
    cdsDataID_LINHA: TIntegerField;
    cdsDataID_MODELO: TIntegerField;
    cdsDataID_UNIDADE_ESTOQUE: TIntegerField;
    cdsDataTIPO: TStringField;
    cdsDataCODIGO_BARRA: TStringField;
    cdsDataJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    cdsDataJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField;
    cdsDataJOIN_DESCRICAO_MARCA: TStringField;
    cdsDataJOIN_DESCRICAO_CATEGORIA: TStringField;
    cdsDataJOIN_DESCRICAO_SUB_CATEGORIA: TStringField;
    cdsDataJOIN_DESCRICAO_LINHA: TStringField;
    cdsDataJOIN_DESCRICAO_MODELO: TStringField;
    cdsDataJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField;
    cdsDatafdqProdutoFornecedor: TDataSetField;
    cdsDatafdqProdutoFilial: TDataSetField;
    cdsDatafdqProdutoCodigoBarra: TDataSetField;
    gbDBCheckBox1: TgbDBCheckBox;
    pgProdutoDetalhe: TcxPageControl;
    tsProdutoGeral: TcxTabSheet;
    tsProdutoFornecedor: TcxTabSheet;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    edDescricao: TgbDBTextEdit;
    descGrupo: TgbDBTextEdit;
    descSubGrupo: TgbDBTextEdit;
    descCategoria: TgbDBTextEdit;
    descSubCategoria: TgbDBTextEdit;
    descLinha: TgbDBTextEdit;
    eFkMarca: TgbDBButtonEditFK;
    descMarca: TgbDBTextEdit;
    eFkModelo: TgbDBButtonEditFK;
    eFkUnidadeEstoque: TgbDBButtonEditFK;
    descUnidadeEstoque: TgbDBTextEdit;
    descModelo: TgbDBTextEdit;
    eFkSubGrupo: TgbDBButtonEditFK;
    eFkGrupo: TgbDBButtonEditFK;
    eFkSubCategoria: TgbDBButtonEditFK;
    eFkCategoria: TgbDBButtonEditFK;
    edCodigoBarra: TgbDBTextEdit;
    eFkLinha: TgbDBButtonEditFK;
    cbTipo: TcxDBComboBox;
    cdsProdutoFornecedor: TGBClientDataSet;
    cdsProdutoFornecedorID: TAutoIncField;
    cdsProdutoFornecedorREFERENCIA: TStringField;
    cdsProdutoFornecedorID_PRODUTO: TIntegerField;
    tsProdutoFilial: TcxTabSheet;
    cdsProdutoFilial: TGBClientDataSet;
    cdsProdutoFilialID: TAutoIncField;
    cdsProdutoFilialQT_ESTOQUE: TFMTBCDField;
    cdsProdutoFilialLOCALIZACAO: TStringField;
    cdsProdutoFilialQT_ESTOQUE_MINIMO: TFMTBCDField;
    cdsProdutoFilialQT_ESTOQUE_MAXIMO: TFMTBCDField;
    cdsProdutoFilialID_PRODUTO: TIntegerField;
    cdsProdutoFilialID_FILIAL: TIntegerField;
    cdsProdutoFilialJOIN_DESCRICAO_FILIAL: TStringField;
    cxGrid1: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    dsProdutoFilial: TDataSource;
    cxGridDBBandedTableView1ID: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1QT_ESTOQUE: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1LOCALIZACAO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1QT_ESTOQUE_MINIMO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1QT_ESTOQUE_MAXIMO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1ID_PRODUTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1ID_FILIAL: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_FILIAL: TcxGridDBBandedColumn;
    lbEtiqueta: TdxBarLargeButton;
    ActImpressaoEtiqueta: TAction;
    frxReport1: TfrxReport;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    cdsDataID_COR: TIntegerField;
    cdsDataJOIN_DESCRICAO_COR: TStringField;
    cdsDataFORMA_AQUISICAO: TStringField;
    cdsDataID_NCM: TIntegerField;
    cdsDataID_NCM_ESPECIALIZADO: TIntegerField;
    cdsDataJOIN_DESCRICAO_NCM: TStringField;
    cdsDataJOIN_CODIGO_NCM: TIntegerField;
    cdsDataJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField;
    cdsDataTIPO_TRIBUTACAO_PIS_COFINS_ST: TIntegerField;
    cdsDataTIPO_TRIBUTACAO_IPI: TIntegerField;
    cdsDatafdqProdutoTributacaoPorUnidade: TDataSetField;
    cxTabSheet1: TcxTabSheet;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBRadioGroup2: TgbDBRadioGroup;
    Label3: TLabel;
    Label7: TLabel;
    Label16: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBCalcEdit1: TgbDBCalcEdit;
    gbDBRadioGroup3: TgbDBRadioGroup;
    gbDBRadioGroup4: TgbDBRadioGroup;
    Label17: TLabel;
    EdtRegraImposto: TgbDBButtonEditFK;
    gbDBTextEdit4: TgbDBTextEdit;
    cdsDataEXCECAO_IPI: TIntegerField;
    cdsDataID_REGRA_IMPOSTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_REGRA_IMPOSTO: TStringField;
    cdsDataORIGEM_DA_MERCADORIA: TIntegerField;
    cdsProdutoFilialQT_ESTOQUE_CONDICIONAL: TFMTBCDField;
    cdsProdutoFilialVL_CUSTO_MEDIO: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_CUSTO: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_FRETE: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_SEGURO: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_ICMS_ST: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_IPI: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_DESCONTO: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    cdsProdutoFilialVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField;
    cdsProdutoFilialVL_CUSTO_MEDIO_IMPOSTO: TFMTBCDField;
    cdsProdutoFilialVL_CUSTO_OPERACIONAL: TFMTBCDField;
    cdsProdutoFilialVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMO_FRETE: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMO_SEGURO: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMO_ICMS_ST: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMO_IPI: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMO_DESCONTO: TFMTBCDField;
    cdsProdutoFilialPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    cdsProdutoFilialPERC_CUSTO_OPERACIONAL: TFMTBCDField;
    PnProdutos: TgbPanel;
    PnFormacaoPreco: TgbPanel;
    Label19: TLabel;
    Label20: TLabel;
    cxDBTextEdit3: TgbDBValorComPercentual;
    Label21: TLabel;
    cxDBTextEdit4: TgbDBValorComPercentual;
    Label22: TLabel;
    cxDBTextEdit5: TgbDBValorComPercentual;
    Label23: TLabel;
    cxDBTextEdit6: TgbDBValorComPercentual;
    Label24: TLabel;
    cxDBTextEdit7: TgbDBValorComPercentual;
    Label26: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    PnTituloFrameDetailPadrao: TgbPanel;
    Label18: TLabel;
    cxDBTextEdit8: TGBDBValorComPercentual;
    PnProdutoFilial: TgbPanel;
    PnTabelaPreco: TgbPanel;
    gbPanel1: TgbPanel;
    fdmTabelaPrecoItem: TgbFDMemTable;
    fdmTabelaPrecoItemVL_CUSTO: TFloatField;
    fdmTabelaPrecoItemPERC_LUCRO: TFloatField;
    fdmTabelaPrecoItemVL_VENDA: TFloatField;
    dsTabelaPrecoItem: TDataSource;
    gridTabelaPreco: TcxGrid;
    cxGridDBBandedTableView2: TcxGridDBBandedTableView;
    Level1BandedTableView1VL_CUSTO: TcxGridDBBandedColumn;
    Level1BandedTableView1PERC_LUCRO: TcxGridDBBandedColumn;
    Level1BandedTableView1VL_VENDA: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    fdmTabelaPrecoItemID_TABELA_PRECO: TIntegerField;
    fdmTabelaPrecoItemDESCRICAO_TABELA_PRECO: TStringField;
    cxGridDBBandedTableView2ID_TABELA_PRECO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2DESCRICAO_TABELA_PRECO: TcxGridDBBandedColumn;
    fdmFormacaoPreco: TgbFDMemTable;
    dsFormacaoPreco: TDataSource;
    gbDBCalcEdit3: TgbDBCalcEdit;
    gbDBCalcEdit5: TgbDBCalcEdit;
    gbDBCalcEdit2: TgbDBCalcEdit;
    GBDBValorComPercentual1: TGBDBValorComPercentual;
    fdmFormacaoPrecoVL_ULTIMO_CUSTO: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_FRETE: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_SEGURO: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_ICMS_ST: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_IPI: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_DESCONTO: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    fdmFormacaoPrecoVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField;
    fdmFormacaoPrecoVL_CUSTO_OPERACIONAL: TFMTBCDField;
    fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMO_FRETE: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMO_SEGURO: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMO_ICMS_ST: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMO_IPI: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMO_DESCONTO: TFMTBCDField;
    fdmFormacaoPrecoPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField;
    fdmFormacaoPrecoPERC_CUSTO_OPERACIONAL: TFMTBCDField;
    cdsProdutoFilialVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    cdsProdutoFornecedorID_PESSOA: TIntegerField;
    cdsProdutoFornecedorJOIN_NOME_FORNECEDOR: TStringField;
    cdsDataDH_CADASTRO: TDateTimeField;
    gbDBDateEdit1: TgbDBDateEdit;
    Label25: TLabel;
    tsMovimentacaoProduto: TcxTabSheet;
    pnFrameMovimentacaoProduto: TgbPanel;
    cdsProdutoMontadora: TGBClientDataSet;
    cdsProdutoModeloMontadora: TGBClientDataSet;
    cdsProdutoMontadoraID: TAutoIncField;
    cdsProdutoMontadoraID_PRODUTO: TIntegerField;
    cdsProdutoModeloMontadoraID: TAutoIncField;
    tsMontadora: TcxTabSheet;
    PnFrameProdutoMontadora: TgbPanel;
    pnReferenciaFornecedor: TgbPanel;
    cdsDatafdqProdutoModeloMontadora: TDataSetField;
    cdsDatafdqProdutoMontadora: TDataSetField;
    cdsProdutoMontadoraNR_ITEM: TIntegerField;
    cdsProdutoMontadoraMONTADORA: TStringField;
    cdsProdutoModeloMontadoraID_PRODUTO: TIntegerField;
    cdsProdutoModeloMontadoraNR_ITEM_MONTADORA: TIntegerField;
    cdsProdutoModeloMontadoraMODELO: TStringField;
    cdsProdutoMontadoraESPECIFICACAO: TBlobField;
    cdsProdutoMontadoraREFERENCIA: TStringField;
    ActGerarProdutoGrade: TAction;
    lbGerarProdutoGrade: TdxBarLargeButton;
    cdsDataJOIN_DESCRICAO_TAMANHO: TStringField;
    cdsDataBO_PRODUTO_AGRUPADOR: TStringField;
    cdsDataBO_PRODUTO_AGRUPADO: TStringField;
    cdsDataID_PRODUTO_AGRUPADOR: TIntegerField;
    cdsDataID_TAMANHO: TIntegerField;
    Label27: TLabel;
    gbDBTextEdit5: TgbDBTextEdit;
    gbDBButtonEditFK5: TgbDBButtonEditFK;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsDataCODIGO_BARRAChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure ActImpressaoEtiquetaExecute(Sender: TObject);
    procedure TotalizarFormacaoPrecoInicial(Sender: TField);
    procedure HabilitarTabelaPreco(Dataset: TDataset);
    procedure CalcularPercentualTabelaPrecoItem(Sender: TField);
    procedure CalcularValorTabelaPrecoitem(Sender: TField);
    procedure fdmTabelaPrecoItemNewRecord(DataSet: TDataSet);
    procedure fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONALChange(Sender: TField);
    procedure cdsProdutoMontadoraBeforeDelete(DataSet: TDataSet);
    procedure cdsProdutoMontadoraBeforePost(DataSet: TDataSet);
    procedure cdsProdutoModeloMontadoraBeforePost(DataSet: TDataSet);
    procedure cdsProdutoModeloMontadoraNewRecord(DataSet: TDataSet);
    procedure cdsProdutoMontadoraAfterScroll(DataSet: TDataSet);
    procedure cdsProdutoModeloMontadoraBeforeInsert(DataSet: TDataSet);
    procedure cdsProdutoMontadoraNewRecord(DataSet: TDataSet);
    procedure cdsProdutoModeloMontadoraAfterPost(DataSet: TDataSet);
    procedure cdsProdutoMontadoraAfterPost(DataSet: TDataSet);
    procedure cdsProdutoMontadoraBeforeInsert(DataSet: TDataSet);
    procedure eFkSubGrupogbAntesDeConsultar(Sender: TObject);
    procedure ActGerarProdutoGradeExecute(Sender: TObject);
    procedure EdtRegraImpostogbAntesDeConsultar(Sender: TObject);
    procedure cdsDataID_NCMChange(Sender: TField);
  private
    FCodigoBarra: String;
    FCalculandoFormacaoPrecoInicial: Boolean;
    FNaoRealizarBuscaCustoIncial: Boolean;
    FCalculandoPercentualTabelaPrecoItem: Boolean;
    FCalculandoValorTabelaPrecoItem: Boolean;

    FFdmFormacaoPrecoParaRotinaDuplicacao: TgbFDMemTable;
    FFdmTabelaPrecoItemParaRotinaDuplicacao: TgbFDMemTable;

    {Parametros}
    FParametroExibirTabelaPreco: TParametroFormulario;
    FParametroExibirFormacaoPrecoInicial: TParametroFormulario;
    FParametroEmissaoEtiquetaArgox: TParametroFormulario;
    FParametroExibirMontadoraProduto: TParametroFormulario;
    FParametroExibirProdutoGrade: TParametroFormulario;

    FFrameConsultaDadosDetalheGradeMovimentacaoProduto: TFrameConsultaDadosDetalheGrade;
    FFrameProdutoMontadora: TFrameProdutoMontadora;
    FFrameProdutoFornecedor: TFrameProdutoFornecedor;

    procedure IncluirFormacaoPrecoInicial;
    procedure BuscarInformacaoPrecoInicial;
    procedure HabilitarFormacaoPrecoInicial;
    procedure IncluirTabelaPreco;
    procedure BuscarTabelaPreco;
    function ExisteMovimentacaoProduto: Boolean;
    procedure AtualizarValorCustoInicialNaTabelaPreco;
    procedure CriarFrameMovimentacaoProduto;
    procedure CriarFrameMontadora;
    procedure CriarFrameProdutoFornecedor;
    procedure AtualizarProduto;
    procedure ValidarCodigoBarraJaCadastrado;
    procedure PreencherCodigoBarra;
    procedure GerarProdutoEmGrade;
    procedure CriarFrameProdutoSimilar;
  public
    class procedure AbrirTelaFiltrando(const AIds: String);
  protected
    procedure GerenciarControles; Override;
    procedure AntesDeConfirmar; Override;
    procedure DepoisDeConfirmar; Override;
    procedure AoCriarFormulario; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;
    procedure SetIDMasterFramesConsultaDadosDetalhe;
    procedure InstanciarFrames; override;
    procedure AntesDeDuplicarRegistro; override;
    function DuplicarRegistro: Integer; override;
    procedure DepoisDeCarregarRegistroDuplicado; override;

  end;

implementation

{$R *.dfm}

uses uDmConnection, uProduto, uFilial, ACBrDevice, uIniFileClient, uFrmMessage,
  uControlsUtils, uTControl_Function, uFrmMessage_Process, uTMessage,
  uFiltroFormulario, uConstantes, uSistema, uConstParametroFormulario, uTabelaPreco, uMathUtils,
  uProdutoProxy, uFrmVisualizacaoRelatoriosAssociados, uDatasetUtils, uMontadora, uFrmGerarProdutoGrade,
  uConstantesFiscaisProxy, uRegraImpostoProxy;

class procedure TCadProduto.AbrirTelaFiltrando(
  const AIds: String);
var
  instanciaFormulario: TCadProduto;
  filtroContaCorrenteMovimento: TFiltroPadrao;
begin
  if AIds.IsEmpty then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es da movimenta��o');
    TControlsUtils.CongelarFormulario(Application.MainForm);
    instanciaFormulario := TCadProduto(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio de Produto est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TCadProduto(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.cxpcMain.ActivePage := instanciaFormulario.cxtsSearch;
    instanciaFormulario.WhereSearch.AddFaixa(TProdutoProxy.FIELD_ID, AIds);
    instanciaFormulario.Consultar;
    instanciaFormulario.ActVisualizarRegistro.Execute;
    instanciaFormulario.Show;
    Application.ProcessMessages;
  finally
    TControlsUtils.DescongelarFormulario;
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TCadProduto.ActGerarProdutoGradeExecute(Sender: TObject);
begin
  inherited;
  GerarProdutoEmGrade;
end;

procedure TCadProduto.ActImpressaoEtiquetaExecute(Sender: TObject);
var
  qtdeEtiquetas: Integer;
  i: Integer;
begin
  inherited;
  qtdeEtiquetas := StrtoIntDef(InputBox('Aten��o', 'Informe o quantidade de etiquetas que deseja imprimir:', ''), 0);

  if qtdeEtiquetas = 0 then
  begin
    Exit;
  end;

  try
    fdmSearch.DisableControls;
    fdmSearch.Filter := 'id = '+fdmSearch.FieldByName('id').AsString;
    fdmSearch.Filtered := true;
    if FParametroEmissaoEtiquetaArgox.ValorSim then
    begin
      TProdutoEtiqueta.ImprimirEtiquetaArgox(fdmSearch.FieldByName('ID'),
        qtdeEtiquetas);
    end
    else
    begin
      for i := 0 to qtdeEtiquetas do
      begin
        TFrmVisualizacaoRelatoriosAssociados.ImprimirRelatoriosAssociados(
          fdmSearch.FieldByName('ID').AsInteger, Self.Name, TSistema.Sistema.usuario.idSeguranca, '');
      end;
    end;
  finally
    fdmSearch.Filter := '';
    fdmSearch.Filtered := false;
    fdmSearch.EnableControls;
  end;
end;

procedure TCadProduto.AntesDeConfirmar;
begin
  inherited;

end;

procedure TCadProduto.AntesDeDuplicarRegistro;
begin
  inherited;
  FFdmFormacaoPrecoParaRotinaDuplicacao := TgbFDMemTable.Create(nil);
  FFdmTabelaPrecoItemParaRotinaDuplicacao := TgbFDMemTable.Create(nil);

  TProduto.BuscarMovimentacaoProdutoInicial(FdmFormacaoPreco, cdsDataID.AsInteger,
    TSistema.Sistema.filial.Fid);

  FFdmFormacaoPrecoParaRotinaDuplicacao.CopyDataset(FdmFormacaoPreco, [coRestart, coAppend, coStructure]);
  FFdmTabelaPrecoItemParaRotinaDuplicacao.CopyDataset(FdmTabelaPrecoItem, [coRestart, coAppend, coStructure]);
end;

procedure TCadProduto.AoCriarFormulario;
begin
  inherited;
  pgProdutoDetalhe.ActivePageIndex := 0;
  FNaoRealizarBuscaCustoIncial := false;

  eFkSubGrupo.FWhereInjetado := TFiltroPadrao.Create;
  EdtRegraImposto.FWhereInjetado := TFiltroPadrao.Create;
end;

procedure TCadProduto.AtualizarProduto;
begin
  AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('PRODUTO', 'ID'), 0));
end;

procedure TCadProduto.AtualizarValorCustoInicialNaTabelaPreco;
begin
  if FParametroExibirTabelaPreco.ValorSim then
  try
    if fdmTabelaPrecoItem.Active then
    begin
      fdmTabelaPrecoItem.DisableControls;
      fdmTabelaPrecoItem.First;
      while not fdmTabelaPrecoItem.Eof do
      begin
        fdmTabelaPrecoItem.Edit;
        fdmTabelaPrecoItemVL_CUSTO.AsFloat := fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat;
        fdmTabelaPrecoItem.Post;

        fdmTabelaPrecoItem.Next;
      end;
    end;
  finally
    fdmTabelaPrecoItem.EnableControls;
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TCadProduto.BuscarInformacaoPrecoInicial;
begin
  if FNaoRealizarBuscaCustoIncial then
  begin
    exit;
  end;

  try
    FNaoRealizarBuscaCustoIncial := true;
    if FParametroExibirFormacaoPrecoInicial.ValorSim then
    begin
      if PnFormacaoPreco.Visible then
      begin
        if fdmFormacaoPreco.Active then
        begin
          fdmFormacaoPreco.EmptyDataset;
          fdmFormacaoPreco.Close;
        end;

        fdmFormacaoPreco.Open;
      end;

      if (not ExisteMovimentacaoProduto) and (cdsDataID.AsInteger > 0) then
      begin
        TProduto.BuscarMovimentacaoProdutoInicial(fdmFormacaoPreco,
          cdsDataID.AsInteger, TSistema.Sistema.filial.Fid);
      end;
    end;
  finally
    FNaoRealizarBuscaCustoIncial := false;
  end;
end;

procedure TCadProduto.BuscarTabelaPreco;
var
  vlCustoInicial: Double;
begin
  if FNaoRealizarBuscaCustoIncial then
  begin
    exit;
  end;

  try
    FNaoRealizarBuscaCustoIncial := true;
    TControlsUtils.CongelarFormulario(Self);
    FCalculandoPercentualTabelaPrecoItem := true;
    FCalculandoValorTabelaPrecoItem := true;

    if FParametroExibirTabelaPreco.ValorSim then
    begin
      if PnTabelaPreco.Visible then
      begin
        if fdmTabelaPrecoItem.Active then
        begin
          fdmTabelaPrecoItem.EmptyDataset;
          fdmTabelaPrecoItem.Close;
        end;

        fdmTabelaPrecoItem.Open;
      end;

      vlCustoInicial := 0;

      if FParametroExibirFormacaoPrecoInicial.ValorSim and fdmFormacaoPreco.Active and
        (fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat > 0) then
      begin
        vlCustoInicial := fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat;
      end;

      TProduto.PopularTabelasPreco(fdmTabelaPrecoItem, cdsDataID.AsInteger, vlCustoInicial);
    end;
  finally
    FCalculandoPercentualTabelaPrecoItem := false;
    FCalculandoValorTabelaPrecoItem := false;
    TControlsUtils.DescongelarFormulario;
    FNaoRealizarBuscaCustoIncial := false;
  end;
end;

procedure TCadProduto.CalcularPercentualTabelaPrecoItem(Sender: TField);
begin
  if FCalculandoValorTabelaPrecoItem or FCalculandoPercentualTabelaPrecoItem or (fdmTabelaPrecoItemVL_VENDA.AsFloat <= 0) then
    exit;

  try
    FCalculandoValorTabelaPrecoItem := true;

    if Sender = fdmTabelaPrecoItemVL_VENDA then
    begin
      fdmTabelaPrecoItemPERC_LUCRO.AsFloat := TMathUtils.PercentualSobreValor(
        (fdmTabelaPrecoItemVL_VENDA.AsFloat - fdmTabelaPrecoItemVL_CUSTO.AsFloat), fdmTabelaPrecoItemVL_CUSTO.AsFloat);
    end
  finally
    FCalculandoValorTabelaPrecoItem := false;
  end
end;

procedure TCadProduto.CalcularValorTabelaPrecoitem(Sender: TField);
begin
  if FCalculandoPercentualTabelaPrecoItem or FCalculandoValorTabelaPrecoItem then
    exit;

  try
    FCalculandoPercentualTabelaPrecoItem := true;
    if fdmTabelaPrecoItemPERC_LUCRO.AsFloat > 0 then
    begin
      fdmTabelaPrecoItemVL_VENDA.AsFloat := fdmTabelaPrecoItemVL_CUSTO.AsFloat +
        TMathUtils.ValorSobrePercentual(fdmTabelaPrecoItemPERC_LUCRO.AsFloat,
        fdmTabelaPrecoItemVL_CUSTO.AsFloat);
    end;
    {
    else if fdmTabelaPrecoItemVL_VENDA.AsFloat > 0 then
    begin
      fdmTabelaPrecoItemPERC_LUCRO.AsFloat :=
        TMathUtils.PercentualSobreValor(fdmTabelaPrecoItemVL_CUSTO.AsFloat, fdmTabelaPrecoItemVL_VENDA.AsFloat);
    end;}
  finally
    FCalculandoPercentualTabelaPrecoItem := false;
  end
end;

procedure TCadProduto.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetIDMasterFramesConsultaDadosDetalhe;
  FCodigoBarra := '';
  HabilitarFormacaoPrecoInicial;
  BuscarInformacaoPrecoInicial;
  BuscarTabelaPreco;
end;

procedure TCadProduto.cdsDataCODIGO_BARRAChange(Sender: TField);
begin
  inherited;
  if Sender.AsString.IsEmpty then
    Sender.AsString := FCodigoBarra;

  ValidarCodigoBarraJaCadastrado;
end;

procedure TCadProduto.cdsDataID_NCMChange(Sender: TField);
begin
  inherited;
  EdtRegraImposto.ExecutarConsultaOculta;
end;

procedure TCadProduto.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  PreencherCodigoBarra;
  cdsDataFORMA_AQUISICAO.AsString := TConstantesFiscais.FORMA_AQUISICAO_ADQUIRIDA_TERCEIRO;
  cdsDataORIGEM_DA_MERCADORIA.AsInteger := TConstantesFiscais.ORIGEM_DA_MERCADORIA_NACIONAL;
  cdsDataTIPO_TRIBUTACAO_PIS_COFINS_ST.AsInteger :=
    TConstantesFiscais.TIPO_TRIBUTACAO_PIS_COFINS_SEM_COBRANCA;
  cdsDataTIPO_TRIBUTACAO_IPI.AsInteger :=
    TConstantesFiscais.TIPO_TRIBUTACAO_IPI_SEM_COBRANCA;
  cdsDataDH_CADASTRO.AsDateTime := Now;
end;

procedure TCadProduto.cdsProdutoModeloMontadoraAfterPost(DataSet: TDataSet);
begin
  inherited;
  if TModeloMontadora.InserirModeloMontadoraCasoNaoExista(cdsProdutoModeloMontadoraMODELO.AsString,
    cdsProdutoMontadoraMONTADORA.AsString) then
  begin
    FFrameProdutoMontadora.CarregarDadosModeloMontadora();
  end;
end;

procedure TCadProduto.cdsProdutoModeloMontadoraBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if (cdsProdutoMontadora.IsEmpty) then
  begin
    Abort;
  end;
end;

procedure TCadProduto.cdsProdutoModeloMontadoraBeforePost(DataSet: TDataSet);
begin
  inherited;
  cdsProdutoModeloMontadora.ValidarCamposObrigatorios;
end;

procedure TCadProduto.cdsProdutoModeloMontadoraNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsProdutoModeloMontadoraNR_ITEM_MONTADORA.AsInteger :=
    cdsProdutoMontadoraNR_ITEM.AsInteger;
end;

procedure TCadProduto.cdsProdutoMontadoraAfterPost(DataSet: TDataSet);
begin
  inherited;
  if TMontadora.InserirMontadoraCasoNaoExista(cdsProdutoMontadoraMONTADORA.AsString) then
  begin
    if Assigned(FFrameProdutoMontadora) then
    begin
      FFrameProdutoMontadora.CarregarDadosMontadora();
    end;
  end;
end;

procedure TCadProduto.cdsProdutoMontadoraAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if cdsProdutoModeloMontadora.Filtered then
  begin
    cdsProdutoModeloMontadora.Filtered := false;
  end;

  cdsProdutoModeloMontadora.Filter := 'nr_item_montadora = '+InttoStr(cdsProdutoMontadoraNR_ITEM.AsInteger);
  cdsProdutoModeloMontadora.Filtered := true;

  if Assigned(FFrameProdutoMontadora) then
  begin
    FFrameProdutoMontadora.FiltrarConsultaModelo();
  end;
end;

procedure TCadProduto.cdsProdutoMontadoraBeforeDelete(DataSet: TDataSet);
begin
  cdsProdutoModeloMontadora.DeletarTodosRegistros;
  inherited;
end;

procedure TCadProduto.cdsProdutoMontadoraBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if cdsDataID.AsInteger <= 0 then
  begin
    cdsData.ValidarCamposObrigatorios;
    cdsData.Commit;
    AtualizarProduto;
    cdsData.Alterar;
  end;
end;

procedure TCadProduto.cdsProdutoMontadoraBeforePost(DataSet: TDataSet);
begin
  inherited;
  cdsProdutoMontadora.ValidarCamposObrigatorios;
end;

procedure TCadProduto.cdsProdutoMontadoraNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsProdutoMontadoraNR_ITEM.AsFloat :=
    TDatasetUtils.MaiorValor(cdsProdutoMontadoraNR_ITEM)+1;
end;

procedure TCadProduto.CriarFrameMontadora;
begin
  FFrameProdutoMontadora := TFrameProdutoMontadora.Create(PnFrameProdutoMontadora);
  FFrameProdutoMontadora.Parent := PnFrameProdutoMontadora;
  FFrameProdutoMontadora.Align := alClient;
  FFrameProdutoMontadora.SetDatasets(cdsProdutoMontadora, cdsProdutoModeloMontadora);
end;

procedure TCadProduto.CriarFrameMovimentacaoProduto;
var
  campoFiltro: TCampoFiltro;
begin
  //Movimenta��o de Produto
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto :=
  TFrameConsultaDadosDetalheGrade.Create(pnFrameMovimentacaoProduto,
  Self.Name+'FrameConsultaDadosDetalheGradeMovimentacaoProduto',
  'Movimenta��o de Produto');
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.OcultarTituloFrame;

  //Produto
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Produto';
  campoFiltro.campo := 'ID_PRODUTO';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PRODUTO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Quantidade da Movimenta��o
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Quantidade da Movimenta��o';
  campoFiltro.campo := 'QT_MOVIMENTO';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data da Movimenta��o';
  campoFiltro.campo := 'DT_MOVIMENTO';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Tipo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TProdutoMovimento.TIPO_MOVIMENTO_ENTRADA+';'+TProdutoMovimento.TIPO_MOVIMENTO_SAIDA;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento de Origem';
  campoFiltro.campo := 'DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TProdutoFilialProxy.LISTA_ORIGEM; //CONFIRMAR
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CHAVE_PROCESSO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM';
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  //Filial
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Filial';
  campoFiltro.campo := 'ID_FILIAL';
  campoFiltro.tabela := 'PRODUTO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'FILIAL';
  campoFiltro.campoFK.camposConsulta := 'ID;FANTASIA';
  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.FCampos.Add(campoFiltro);

  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeMovimentacaoProduto.Parent := pnFrameMovimentacaoProduto;
end;

procedure TCadProduto.CriarFrameProdutoFornecedor;
begin
  FFrameProdutoFornecedor := TFrameProdutoFornecedor.Create(pnReferenciaFornecedor);
  FFrameProdutoFornecedor.Parent := pnReferenciaFornecedor;
  FFrameProdutoFornecedor.Align := alClient;
  FFrameProdutoFornecedor.SetConfiguracoesIniciais(cdsProdutoFornecedor);
  FFrameProdutoFornecedor.Parent := pnReferenciaFornecedor;
end;

procedure TCadProduto.CriarFrameProdutoSimilar;
begin
//Criar depois o frame dos produtos similares
end;

procedure TCadProduto.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Exibir Tabela Pre�o
  FParametroExibirTabelaPreco := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_TABELA_PRECO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_TABELA_PRECO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirTabelaPreco);

  //Exibir Forma��o de Pre�o Inicial
  FParametroExibirFormacaoPrecoInicial := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_FORMACAO_PRECO_INICIAL,
    TConstParametroFormulario.DESCRICAO_EXIBIR_FORMACAO_PRECO_INICIAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirFormacaoPrecoInicial);

  //Etiqueta Argox
  FParametroEmissaoEtiquetaArgox := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EMISSAO_ETIQUETA_ARGOX,
    TConstParametroFormulario.DESCRICAO_EMISSAO_ETIQUETA_ARGOX,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroEmissaoEtiquetaArgox);

  FParametroExibirMontadoraProduto:= TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_MONTADORA_PRODUTO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_MONTADORA_PRODUTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirMontadoraProduto);

  FParametroExibirProdutoGrade := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_PRODUTO_GRADE,
    TConstParametroFormulario.DESCRICAO_EXIBIR_PRODUTO_GRADE,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirProdutoGrade);
end;

procedure TCadProduto.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Grupo de Produto';
  campoFiltro.campo := 'ID_GRUPO_PRODUTO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'GRUPO_PRODUTO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Sub Grupo de Produto';
  campoFiltro.campo := 'ID_SUB_GRUPO_PRODUTO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'SUB_GRUPO_PRODUTO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Marca';
  campoFiltro.campo := 'ID_MARCA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'MARCA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Categoria';
  campoFiltro.campo := 'ID_CATEGORIA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CATEGORIA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Linha';
  campoFiltro.campo := 'ID_LINHA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'LINHA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Modelo';
  campoFiltro.campo := 'ID_MODELO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'MODELO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Unidade de Estoque';
  campoFiltro.campo := 'ID_UNIDADE_ESTOQUE';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'UNIDADE_ESTOQUE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TProdutoProxy.TIPO_PRODUTO_PRODUTO+';'+
  TProdutoProxy.TIPO_PRODUTO_SERVICO+';'+TProdutoProxy.TIPO_PRODUTO_OUTROS;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo de Barra';
  campoFiltro.campo := 'CODIGO_BARRA';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data/Hora de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'PRODUTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TCadProduto.DepoisDeConfirmar;
var idGerado: Integer;
begin
  inherited;
  try
    FNaoRealizarBuscaCustoIncial := true;

    if cdsData.FieldByName('ID').AsInteger <= 0 then
    begin
      AtualizarProduto;
    end;

    IncluirFormacaoPrecoInicial;
    IncluirTabelaPreco;
  finally
    FNaoRealizarBuscaCustoIncial := false;
  end;
end;

procedure TCadProduto.DepoisDeCarregarRegistroDuplicado;
begin
  inherited;
  try
    if Assigned(FFdmFormacaoPrecoParaRotinaDuplicacao) and (not FFdmFormacaoPrecoParaRotinaDuplicacao.IsEmpty) then
    begin
      try
        FNaoRealizarBuscaCustoIncial := true;
        fdmFormacaoPreco.EmptyDataset;
        fdmFormacaoPreco.CopyDataset(FFdmFormacaoPrecoParaRotinaDuplicacao, [coRestart, coAppend]);

        if not fdmFormacaoPreco.IsEmpty then
        begin
          fdmFormacaoPreco.Edit;
          IncluirFormacaoPrecoInicial;
        end;
      finally
        FNaoRealizarBuscaCustoIncial := false;
        FreeAndNil(FFdmFormacaoPrecoParaRotinaDuplicacao);
      end;
    end;

    if Assigned(FFdmTabelaPrecoItemParaRotinaDuplicacao) and (not FFdmTabelaPrecoItemParaRotinaDuplicacao.IsEmpty) then
    begin
      try
        FNaoRealizarBuscaCustoIncial := true;
        FdmTabelaPrecoItem.EmptyDataset;
        FdmTabelaPrecoItem.CopyDataset(FFdmTabelaPrecoItemParaRotinaDuplicacao, [coRestart, coAppend]);
        IncluirTabelaPreco;
      finally
        FNaoRealizarBuscaCustoIncial := false;
        FreeAndNil(FFdmTabelaPrecoItemParaRotinaDuplicacao);
      end;
    end;
  finally
    AtualizarRegistro;
  end;
end;

function TCadProduto.DuplicarRegistro: Integer;
begin
  inherited;
  result := TProduto.DuplicarProduto(cdsDataID.AsInteger);
end;

procedure TCadProduto.EdtRegraImpostogbAntesDeConsultar(Sender: TObject);
begin
  inherited;
  EdtRegraImposto.FWhereInjetado.Limpar;

  if cdsDataID_NCM.AsInteger > 0 then
  begin
    EdtRegraImposto.FWhereInjetado.AddIgual(TRegraImpostoProxy.FIELD_ID_NCM,
      cdsDataID_NCM.AsInteger, opOR);

    EdtRegraImposto.FWhereInjetado.AddIsNull(TRegraImpostoProxy.FIELD_ID_NCM, opOR);
  end;
end;

procedure TCadProduto.eFkSubGrupogbAntesDeConsultar(Sender: TObject);
begin
  inherited;
  eFkSubGrupo.FWhereInjetado.Limpar;

  eFkSubGrupo.FWhereInjetado.AddIgual(TSubGrupoProdutoProxy.FIELD_ID_GRUPO_PRODUTO,
    cdsDataID_GRUPO_PRODUTO.AsInteger);
end;

function TCadProduto.ExisteMovimentacaoProduto: Boolean;
begin
  result := false;

  if cdsDataID.AsInteger <= 0 then
    Exit;

  if FParametroExibirTabelaPreco.ValorSim then
  begin
    result := TProduto.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(cdsDataID.AsInteger,
      TSistema.Sistema.Usuario.ID);
  end;
end;

procedure TCadProduto.fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONALChange(Sender: TField);
begin
  inherited;
  TotalizarFormacaoPrecoInicial(Sender);
  AtualizarValorCustoInicialNaTabelaPreco;
end;

procedure TCadProduto.fdmTabelaPrecoItemNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmTabelaPrecoItemVL_CUSTO.AsFloat := 0;
  fdmTabelaPrecoItemPERC_LUCRO.AsFloat := 0;
  fdmTabelaPrecoItemVL_VENDA.AsFloat := 0;
end;

procedure TCadProduto.GerarProdutoEmGrade;
var
  componenteModoDeInclusaoFK: TgbDBButtonEditFK;
begin
  try
    componenteModoDeInclusaoFK := FComponenteModoDeInclusaoFK;
    FComponenteModoDeInclusaoFK := nil;

    if cdsData.EstadoDeAlteracao then
    begin
      ActConfirm.Execute;
    end;
  finally
    FComponenteModoDeInclusaoFK := componenteModoDeInclusaoFK;
  end;

  try
    ActUpdate.Execute;
    TFrmGerarProdutoGrade.GerarProdutoGrade(cdsDataID.AsInteger);
  finally
    ActConfirm.Execute;
  end;
end;

procedure TCadProduto.GerenciarControles;
var bEdicao: Boolean;
begin
  inherited;
  if cdsData.Active then
    bEdicao := cdsData.State in dsEditModes
  else
    bEdicao := false;

  ActImpressaoEtiqueta.Visible := not bEdicao and not(fdmSearch.IsEmpty);

  PnFormacaoPreco.Enabled := bEdicao;
  PnTabelaPreco.Enabled := bEdicao;
  ActGerarProdutoGrade.Visible := (bEdicao or (AcaoCrud = uFrmCadastro_Padrao.TAcaoCrud(visualizar))) and
    (cdsData.Active and (not cdsData.IsEmpty) and cdsDataBO_PRODUTO_AGRUPADO.AsString.Equals('N'));

  ActGerarProdutoGrade.Visible := FParametroExibirProdutoGrade.ValorSim;
  tsMontadora.TabVisible := FParametroExibirMontadoraProduto.ValorSim;
end;

procedure TCadProduto.HabilitarFormacaoPrecoInicial;
begin
  if FParametroExibirFormacaoPrecoInicial.ValorSim then
  begin
    PnFormacaoPreco.Visible := not ExisteMovimentacaoProduto;
  end;
end;

procedure TCadProduto.HabilitarTabelaPreco(Dataset: TDataset);
begin
  PnTabelaPreco.Visible := not fdmTabelaPrecoItem.IsEmpty;
end;

procedure TCadProduto.IncluirFormacaoPrecoInicial;
begin
  if FParametroExibirFormacaoPrecoInicial.ValorSim then
  begin
    if fdmFormacaoPreco.Active and (fdmFormacaoPrecoVL_ULTIMO_CUSTO.AsFloat > 0)
      and (fdmFormacaoPreco.State in dsEditModes) then
    begin
      fdmFormacaoPreco.Post;
      TProduto.GerarMovimentacaoProdutoInicial(cdsData, fdmFormacaoPreco);
    end;
  end;
end;

procedure TCadProduto.IncluirTabelaPreco;
begin
  if FParametroExibirTabelaPreco.ValorSim then
  try
    fdmTabelaPrecoItem.DisableControls;
    fdmTabelaPrecoItem.First;
    while not fdmTabelaPrecoItem.Eof do
    begin
      if fdmTabelaPrecoItemVL_VENDA.AsFloat > 0 then
      begin
        TTabelaPreco.AtualizarRegistro(fdmTabelaPrecoItemID_TABELA_PRECO.AsInteger, cdsDataID.AsInteger,
          fdmTabelaPrecoItemVL_CUSTO.AsFloat, fdmTabelaPrecoItemPERC_LUCRO.AsFloat, fdmTabelaPrecoItemVL_VENDA.AsFloat);
      end;
      fdmTabelaPrecoItem.Next;
    end;
  finally
    fdmTabelaPrecoItem.EnableControls;
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TCadProduto.InstanciarFrames;
begin
  inherited;
  CriarFrameProdutoFornecedor;
  CriarFrameMovimentacaoProduto;
  CriarFrameMontadora;
  CriarFrameProdutoSimilar;
end;

procedure TCadProduto.PreencherCodigoBarra;
begin
  if cdsDataID.AsInteger > 0 then
  begin
    FCodigoBarra := TProduto.GerarCodigoBarraEAN13(cdsDataID.AsInteger);
  end
  else
  begin
    FCodigoBarra := TProduto.NovoCodigoBarra();
  end;

  cdsDataCODIGO_BARRA.AsString := FCodigoBarra;
end;

procedure TCadProduto.SetIDMasterFramesConsultaDadosDetalhe;
begin
  if Assigned(FFrameConsultaDadosDetalheGradeMovimentacaoProduto) then
  begin
    FFrameConsultaDadosDetalheGradeMovimentacaoProduto.SetDadosMaster(
      'PRODUTO_MOVIMENTO.ID_PRODUTO', cdsDataID.AsInteger);
  end;
end;

procedure TCadProduto.TotalizarFormacaoPrecoInicial(Sender: TField);
begin
  if not FCalculandoFormacaoPrecoInicial then
  try
    FCalculandoFormacaoPrecoInicial := true;

    fdmFormacaoPrecoVL_ULTIMO_CUSTO_IMPOSTO.AsFloat :=
      fdmFormacaoPrecoVL_ULTIMO_CUSTO.AsFloat +
      fdmFormacaoPrecoVL_ULTIMO_FRETE.AsFloat +
      fdmFormacaoPrecoVL_ULTIMO_SEGURO.AsFloat +
      fdmFormacaoPrecoVL_ULTIMO_ICMS_ST.AsFloat +
      fdmFormacaoPrecoVL_ULTIMO_IPI.AsFloat -
      fdmFormacaoPrecoVL_ULTIMO_DESCONTO.AsFloat +
      fdmFormacaoPrecoVL_ULTIMA_DESPESA_ACESSORIA.AsFloat;

    fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat :=
      fdmFormacaoPrecoVL_ULTIMO_CUSTO_IMPOSTO.AsFloat + fdmFormacaoPrecoVL_CUSTO_OPERACIONAL.AsFloat;
  finally
    FCalculandoFormacaoPrecoInicial := false;
  end;
end;

procedure TCadProduto.ValidarCodigoBarraJaCadastrado;
var
  idProduto: Integer;
begin
  idProduto := TProduto.CodigoDeBarraJaCadastrado(cdsDataCODIGO_BARRA.AsString,
    cdsDataID.AsInteger);

  if idProduto > 0 then
  begin
    PreencherCodigoBarra;
  end;
end;

initialization
  RegisterClass(TCadProduto);

finalization
  UnRegisterClass(TCadProduto);

end.
