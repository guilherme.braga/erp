unit uCadChecklist;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxTextEdit, cxDBEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxCheckBox, uGBDBCheckBox, uGBDBTextEdit,
  uFrameDetailPadrao, uGBPanel, uFrameChecklistItem;

type
  TCadChecklist = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDatafdqChecklistItem: TDataSetField;
    gbDBCheckBox1: TgbDBCheckBox;
    cdsChecklistItem: TGBClientDataSet;
    cdsChecklistItemID: TAutoIncField;
    cdsChecklistItemNR_ITEM: TIntegerField;
    cdsChecklistItemDESCRICAO: TStringField;
    cdsChecklistItemID_CHECKLIST: TIntegerField;
    cdsChecklistItemBO_ATIVO: TStringField;
    gbPanel1: TgbPanel;
    Label2: TLabel;
    edDescricao: TgbDBTextEdit;
    FrameChecklistItem: TFrameChecklistItem;
    procedure cdsChecklistItemAfterOpen(DataSet: TDataSet);
    procedure cdsChecklistItemNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

uses uDmConnection, uDatasetUtils;

procedure TCadChecklist.cdsChecklistItemAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FrameChecklistItem.SetConfiguracoesIniciais(cdsChecklistItem);
end;

procedure TCadChecklist.cdsChecklistItemNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsChecklistItemNR_ITEM.AsFloat :=
    TDatasetUtils.MaiorValor(cdsChecklistItemNR_ITEM)+1;
end;

initialization
  RegisterClass(TCadChecklist);

finalization
  UnRegisterClass(TCadChecklist);

end.
