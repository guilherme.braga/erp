inherited CadCentroResultado: TCadCentroResultado
  Caption = 'Cadastro de Centro de Resultado'
  ClientWidth = 924
  ExplicitWidth = 940
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 924
    ExplicitWidth = 924
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 924
    Properties.ActivePage = cxtsData
    ExplicitWidth = 924
    ClientRectRight = 924
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 924
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 892
        ExplicitWidth = 892
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 924
      inherited DesignPanel: TJvDesignPanel
        Width = 924
        ExplicitWidth = 924
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 924
          Width = 924
          inherited dxBevel1: TdxBevel
            Width = 920
            ExplicitWidth = 920
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label2: TLabel
            Left = 406
            Top = 40
            Width = 146
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Grupo de Centro de Resultado'
          end
          object Label3: TLabel
            Left = 8
            Top = 66
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 68
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 332
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 557
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_GRUPO_CENTRO_RESULTADO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = gbDBTextEdit2
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_GRUPO_CENTRO_RESULTADO;JOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO'
            gbTableName = 'GRUPO_CENTRO_RESULTADO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'GRUPO_CENTRO_RESULTADO'
            Width = 60
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 614
            Top = 36
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 304
          end
          object gbDBBlobEdit1: TgbDBBlobEdit
            Left = 68
            Top = 62
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Properties.PopupHeight = 300
            Properties.PopupWidth = 160
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Width = 850
          end
          object gbDBCheckBox2: TgbDBCheckBox
            Left = 718
            Top = 5
            Anchors = [akTop, akRight]
            Caption = 'Exibir no Extrato de Plano de Conta?'
            DataBinding.DataField = 'BO_EXIBIR_EXTRATO_PLANO_CONTA'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            Width = 200
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Centro de Resultado'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspCentroResultado'
    RemoteServer = DmConnection.dspFinanceiro
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataID_GRUPO_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Grupo de Centro de Resultado'
      FieldName = 'ID_GRUPO_CENTRO_RESULTADO'
      Origin = 'ID_GRUPO_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Grupo de Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataBO_EXIBIR_EXTRATO_PLANO_CONTA: TStringField
      DisplayLabel = 'Exibir Extrato de Plano de Conta'
      FieldName = 'BO_EXIBIR_EXTRATO_PLANO_CONTA'
      Origin = 'BO_EXIBIR_EXTRATO_PLANO_CONTA'
      FixedChar = True
      Size = 1
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42413.921773530090000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
end
