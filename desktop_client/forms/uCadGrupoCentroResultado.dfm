inherited CadGrupoCentroResultado: TCadGrupoCentroResultado
  Caption = 'Cadastro de Grupo de Centro de Resultado'
  ClientWidth = 912
  ExplicitWidth = 928
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 912
    ExplicitWidth = 912
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 912
    Properties.ActivePage = cxtsData
    ExplicitWidth = 912
    ClientRectRight = 912
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 912
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 912
        ExplicitWidth = 912
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 912
      inherited DesignPanel: TJvDesignPanel
        Width = 912
        ExplicitWidth = 912
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 912
          Width = 912
          inherited dxBevel1: TdxBevel
            Width = 908
            ExplicitWidth = 908
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label3: TLabel
            Left = 8
            Top = 66
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 68
            Top = 36
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 835
          end
          object gbDBBlobEdit1: TgbDBBlobEdit
            Left = 68
            Top = 62
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Properties.PopupHeight = 300
            Properties.PopupWidth = 160
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Width = 835
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Centro de Resultado'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspGrupoCentroResultado'
    RemoteServer = DmConnection.dspFinanceiro
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 5000
    end
  end
end
