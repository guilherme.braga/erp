inherited CadGradeProduto: TCadGradeProduto
  Caption = 'Cadastro de Grade de Produto'
  ClientHeight = 428
  ClientWidth = 868
  ExplicitWidth = 884
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 868
    ExplicitWidth = 868
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 868
    Height = 301
    Properties.ActivePage = cxtsData
    ExplicitWidth = 868
    ExplicitHeight = 301
    ClientRectBottom = 301
    ClientRectRight = 868
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 868
      ExplicitHeight = 277
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 836
        Height = 277
        ExplicitWidth = 836
        ExplicitHeight = 277
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 277
        Height = 277
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 277
        ExplicitHeight = 277
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 868
      ExplicitHeight = 277
      inherited DesignPanel: TJvDesignPanel
        Width = 868
        Height = 277
        ExplicitWidth = 868
        ExplicitHeight = 277
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 868
          ExplicitHeight = 277
          Height = 277
          Width = 868
          inherited dxBevel1: TdxBevel
            Width = 864
            ExplicitWidth = 864
          end
          object Label1: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 74
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              864
              50)
            Height = 50
            Width = 864
            object Label2: TLabel
              Left = 8
              Top = 7
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object Label3: TLabel
              Left = 8
              Top = 30
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 72
              Top = 3
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 728
            end
            object gbDBCheckBox1: TgbDBCheckBox
              Left = 802
              Top = 3
              Anchors = [akTop, akRight]
              Caption = 'Ativo?'
              DataBinding.DataField = 'BO_ATIVO'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Width = 55
            end
            object gbDBBlobEdit1: TgbDBBlobEdit
              Left = 72
              Top = 26
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACAO'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Properties.PopupHeight = 300
              Properties.PopupWidth = 160
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              Width = 785
            end
          end
          object PnFrameGradeProdutoCor: TgbPanel
            Left = 2
            Top = 84
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameGradeProduto'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Height = 191
            Width = 864
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    Active = True
    ProviderName = 'dspGradeProduto'
    RemoteServer = DmConnection.dspGradeProduto
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDatafdqGradeProdutoCorTamanho: TDataSetField
      FieldName = 'fdqGradeProdutoCorTamanho'
    end
    object cdsDatafdqGradeProdutoCor: TDataSetField
      FieldName = 'fdqGradeProdutoCor'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42295.409605775480000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsGradeProdutoCor: TGBClientDataSet
    Active = True
    Aggregates = <>
    DataSetField = cdsDatafdqGradeProdutoCor
    Params = <>
    BeforeInsert = cdsGradeProdutoCorBeforeInsert
    BeforePost = cdsGradeProdutoCorBeforePost
    AfterPost = cdsGradeProdutoCorAfterPost
    BeforeDelete = cdsGradeProdutoCorBeforeDelete
    AfterScroll = cdsGradeProdutoCorAfterScroll
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 740
    Top = 80
    object cdsGradeProdutoCorID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsGradeProdutoCorID_GRADE_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Grade Produto'
      FieldName = 'ID_GRADE_PRODUTO'
      Origin = 'ID_GRADE_PRODUTO'
    end
    object cdsGradeProdutoCorID_COR: TIntegerField
      DisplayLabel = 'C'#243'digo da Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
      Required = True
    end
    object cdsGradeProdutoCorJOIN_DESCRICAO_COR: TStringField
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object cdsGradeProdutoCorTamanho: TGBClientDataSet
    Active = True
    Aggregates = <>
    DataSetField = cdsDatafdqGradeProdutoCorTamanho
    Params = <>
    BeforeInsert = cdsGradeProdutoCorTamanhoBeforeInsert
    BeforePost = cdsGradeProdutoCorTamanhoBeforePost
    OnNewRecord = cdsGradeProdutoCorTamanhoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 788
    Top = 80
    object cdsGradeProdutoCorTamanhoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsGradeProdutoCorTamanhoID_GRADE_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Grade de Produto'
      FieldName = 'ID_GRADE_PRODUTO'
      Origin = 'ID_GRADE_PRODUTO'
    end
    object cdsGradeProdutoCorTamanhoID_COR: TIntegerField
      DisplayLabel = 'Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
      Required = True
    end
    object cdsGradeProdutoCorTamanhoJOIN_DESCRICAO_COR: TStringField
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object cdsGradeProdutoCorTamanhoID_TAMANHO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tamanho'
      FieldName = 'ID_TAMANHO'
      Origin = 'ID_TAMANHO'
      Required = True
    end
    object cdsGradeProdutoCorTamanhoJOIN_DESCRICAO_TAMANHO: TStringField
      DisplayLabel = 'Tamanho'
      FieldName = 'JOIN_DESCRICAO_TAMANHO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
