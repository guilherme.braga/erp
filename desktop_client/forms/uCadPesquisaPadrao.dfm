inherited CadPesquisaPadrao: TCadPesquisaPadrao
  Caption = 'Cadastro de Pesquisa Padr'#227'o'
  ClientHeight = 445
  ClientWidth = 926
  ExplicitWidth = 942
  ExplicitHeight = 484
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 926
    ExplicitWidth = 926
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 926
    Height = 318
    Properties.ActivePage = cxtsData
    ExplicitWidth = 926
    ExplicitHeight = 318
    ClientRectBottom = 318
    ClientRectRight = 926
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 926
      ExplicitHeight = 294
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 894
        Height = 294
        ExplicitWidth = 894
        ExplicitHeight = 294
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 294
        Height = 294
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 294
        ExplicitHeight = 294
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 926
      ExplicitHeight = 294
      inherited DesignPanel: TJvDesignPanel
        Width = 926
        Height = 294
        ExplicitWidth = 926
        ExplicitHeight = 294
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 926
          ExplicitHeight = 294
          Height = 294
          Width = 926
          inherited dxBevel1: TdxBevel
            Width = 922
            ExplicitWidth = 754
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label2: TLabel
            Left = 294
            Top = 40
            Width = 36
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Usu'#225'rio'
            ExplicitLeft = 245
          end
          object Label3: TLabel
            Left = 8
            Top = 66
            Width = 19
            Height = 13
            Caption = 'SQL'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 57
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 57
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 233
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 862
            Top = 36
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Transparent = True
            Width = 55
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 334
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_PESSOA_USUARIO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 2
            gbTextEdit = gbDBTextEdit2
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PESSOA_USUARIO'
            gbTableName = 'PESSOA_USUARIO'
            gbCamposConsulta = 'ID'
            gbIdentificadorConsulta = 'PESSOA_USUARIO'
            Width = 60
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 391
            Top = 36
            TabStop = False
            Anchors = [akTop, akRight]
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 337
          end
          object gbDBBlobEdit1: TgbDBBlobEdit
            Left = 57
            Top = 62
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'SQL_PESQUISA'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Properties.PopupHeight = 300
            Properties.PopupWidth = 160
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbRequired = True
            Width = 860
          end
          object gbDBCheckBox3: TgbDBCheckBox
            Left = 729
            Top = 36
            Anchors = [akTop, akRight]
            Caption = 'Consulta Autom'#225'tica?'
            DataBinding.DataField = 'BO_CONSULTA_AUTOMATICA'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            Transparent = True
            Width = 129
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Pesquisa Padr'#227'o'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspPesquisaPadrao'
    RemoteServer = DmConnection.dspPesquisaPadrao
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataSQL_PESQUISA: TBlobField
      DisplayLabel = 'SQL'
      FieldName = 'SQL_PESQUISA'
      Origin = 'SQL_PESQUISA'
      Required = True
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object cdsDataBO_CONSULTA_AUTOMATICA: TStringField
      DefaultExpression = 'N'
      FieldName = 'BO_CONSULTA_AUTOMATICA'
      FixedChar = True
      Size = 1
    end
  end
  inherited pmGridConsultaPadrao: TPopupMenu
    Left = 592
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42345.921577604170000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
