unit uCadEstado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  uGBDBTextEdit, cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, System.Generics.Collections;

type
  TCadEstado = class(TFrmCadastro_Padrao)
    cdsDataDESCRICAO: TStringField;
    cdsDataUF: TStringField;
    cdsDataID_PAIS: TIntegerField;
    cdsDataJOIN_DESCRICAO_PAIS: TStringField;
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    Label1: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    Label2: TLabel;
    cdsDataID: TAutoIncField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TCadEstado);
finalization
  UnRegisterClass(TCadEstado);

end.


