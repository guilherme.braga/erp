unit uCadOperacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, uGBPanel,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, uFrameDetailPadrao,
  uFrameOperacaoItem, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, uFrameOperacaoOperacaoFiscal,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter;

type
  TCadOperacao = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    gbDados: TgbPanel;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsOperacaoItem: TGBClientDataSet;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDatafdqOperacaoItem: TDataSetField;
    cdsOperacaoItemID: TAutoIncField;
    cdsOperacaoItemID_OPERACAO: TIntegerField;
    cdsOperacaoItemID_ACAO: TIntegerField;
    cdsOperacaoItemJOIN_DESCRICAO_ACAO: TStringField;
    Label2: TLabel;
    cdsDataNATUREZA_OPERACAO: TStringField;
    cdsDatafdqOperacaoFiscal: TDataSetField;
    pcOperacao: TcxPageControl;
    tsAcao: TcxTabSheet;
    tsOperacaoFiscal: TcxTabSheet;
    pnFrameOperacaoAcao: TgbPanel;
    pnFrameOperacaoOperacaoFiscal: TgbPanel;
    cdsOperacaoFiscal: TGBClientDataSet;
    cdsOperacaoFiscalID: TAutoIncField;
    cdsOperacaoFiscalDESCRICAO: TStringField;
    cdsOperacaoFiscalID_OPERACAO: TIntegerField;
    cdsOperacaoFiscalCFOP_PRODUTO: TIntegerField;
    cdsOperacaoFiscalCFOP_PRODUTO_ST: TIntegerField;
    cdsOperacaoFiscalCFOP_MERCADORIA: TIntegerField;
    cdsOperacaoFiscalCFOP_MERCADORIA_ST: TIntegerField;
    EdtNaturezaOperacao: TgbDBButtonEditFK;
    cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO: TStringField;
    cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO_ST: TStringField;
    cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA: TStringField;
    cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA_ST: TStringField;
  private
    FFrameOperacaoItem: TFrameOperacaoItem;
    FFrameOperacaoOperacaoFiscal: TFrameOperacaoOperacaoFiscal;
    procedure PreencherNaturezaOperacao;
  public
    procedure AoCriarFormulario; override;
    procedure AntesDeConfirmar; override;
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uNaturezaOperacao;

procedure TCadOperacao.AntesDeConfirmar;
begin
  inherited;
  PreencherNaturezaOperacao;
end;

procedure TCadOperacao.AoCriarFormulario;
begin
  inherited;
  EdtNaturezaOperacao.NaoLimparCampoAposConsultaInvalida;
  pcOperacao.ActivePage := tsAcao;
end;

procedure TCadOperacao.InstanciarFrames;
begin
  inherited;
  FFrameOperacaoItem := TFrameOperacaoItem.Create(pnFrameOperacaoAcao);
  FFrameOperacaoItem.Parent := pnFrameOperacaoAcao;
  FFrameOperacaoItem.Align := alClient;
  FFrameOperacaoItem.SetConfiguracoesIniciais(cdsOperacaoItem);

  FFrameOperacaoOperacaoFiscal := TFrameOperacaoOperacaoFiscal.Create(pnFrameOperacaoOperacaoFiscal);
  FFrameOperacaoOperacaoFiscal.Parent := pnFrameOperacaoOperacaoFiscal;
  FFrameOperacaoOperacaoFiscal.Align := alClient;
  FFrameOperacaoOperacaoFiscal.SetConfiguracoesIniciais(cdsOperacaoFiscal);
end;

procedure TCadOperacao.PreencherNaturezaOperacao;
begin
  if cdsDataNATUREZA_OPERACAO.AsString.IsEmpty then
  begin
    cdsDataNATUREZA_OPERACAO.AsString := cdsDataDESCRICAO.AsString;
  end;

  TNaturezaOperacao.InserirNaturezaOperacaoCasoNaoExista(cdsDataNATUREZA_OPERACAO.AsString);
end;

Initialization
  RegisterClass(TCadOperacao);

Finalization
  UnRegisterClass(TCadOperacao);


end.
