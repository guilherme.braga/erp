unit uFrmRelatorioFR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxExportXML, frxExportXLS, frxExportRTF, frxExportODF,
  frxExportImage, frxExportHTML, frxExportCSV, frxClass, frxExportPDF, frxBarcode, frxDMPExport;

type
  TFrmRelatorioFR = class(TForm)
    frxReport: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    frxBMPExport: TfrxBMPExport;
    frxCSVExport: TfrxCSVExport;
    frxHTMLExport: TfrxHTMLExport;
    frxJPEGExport: TfrxJPEGExport;
    frxODTExport: TfrxODTExport;
    frxRTFExport: TfrxRTFExport;
    frxXLSExport: TfrxXLSExport;
    frxXMLExport: TfrxXMLExport;
    frxBarCodeObject: TfrxBarCodeObject;
    frxDotMatrixExport: TfrxDotMatrixExport;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
