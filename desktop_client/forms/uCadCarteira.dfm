inherited CadCarteira: TCadCarteira
  Caption = 'Cadastro de Carteira'
  ClientHeight = 545
  ClientWidth = 898
  ExplicitWidth = 914
  ExplicitHeight = 584
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 898
    ExplicitWidth = 896
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 898
    Height = 418
    Properties.ActivePage = cxtsData
    ExplicitWidth = 896
    ExplicitHeight = 418
    ClientRectBottom = 418
    ClientRectRight = 898
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 896
      ExplicitHeight = 394
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 866
        Height = 394
        ExplicitWidth = 864
        ExplicitHeight = 394
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 394
        Height = 394
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 394
        ExplicitHeight = 394
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 896
      ExplicitHeight = 394
      inherited DesignPanel: TJvDesignPanel
        Width = 898
        Height = 394
        ExplicitWidth = 896
        ExplicitHeight = 394
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 896
          ExplicitHeight = 394
          Height = 394
          Width = 898
          inherited dxBevel1: TdxBevel
            Width = 894
            ExplicitWidth = 919
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 69
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 833
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            ExplicitLeft = 831
            Width = 55
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            ExplicitWidth = 892
            DesignSize = (
              894
              26)
            Height = 26
            Width = 894
            object Label2: TLabel
              Left = 6
              Top = 5
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object labelPercLucro: TLabel
              Left = 600
              Top = 5
              Width = 40
              Height = 13
              Anchors = [akTop, akRight]
              Caption = '% Multa'
              ExplicitLeft = 597
            end
            object Label3: TLabel
              Left = 745
              Top = 5
              Width = 40
              Height = 13
              Anchors = [akTop, akRight]
              Caption = '% Juros'
              ExplicitLeft = 742
            end
            object Label4: TLabel
              Left = 462
              Top = 5
              Width = 80
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Dias de Car'#234'ncia'
              ExplicitLeft = 459
            end
            object edDescricao: TgbDBTextEdit
              Left = 67
              Top = 1
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              ExplicitWidth = 387
              Width = 389
            end
            object edtPercLucro: TgbDBCalcEdit
              Left = 644
              Top = 1
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'PERC_MULTA'
              DataBinding.DataSource = dsData
              Properties.DisplayFormat = '###,###,###,###,###,##0.00'
              Properties.ImmediatePost = True
              Properties.UseThousandSeparator = True
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbRequired = True
              ExplicitLeft = 642
              Width = 97
            end
            object gbDBCalcEdit1: TgbDBCalcEdit
              Left = 789
              Top = 1
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'PERC_JUROS'
              DataBinding.DataSource = dsData
              Properties.DisplayFormat = '###,###,###,###,###,##0.00'
              Properties.ImmediatePost = True
              Properties.UseThousandSeparator = True
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbRequired = True
              Width = 93
            end
            object gbDBSpinEdit1: TgbDBSpinEdit
              Left = 546
              Top = 1
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'CARENCIA'
              DataBinding.DataSource = dsData
              Properties.MaxValue = 9999.000000000000000000
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              ExplicitLeft = 544
              Width = 50
            end
          end
          object gbPanel2: TgbPanel
            Left = 2
            Top = 60
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            ExplicitWidth = 892
            Height = 332
            Width = 894
            object pcBoletoCedente: TcxPageControl
              Left = 2
              Top = 2
              Width = 890
              Height = 328
              Align = alClient
              TabOrder = 0
              Properties.ActivePage = tsCedente
              Properties.CustomButtons.Buttons = <>
              Properties.Style = 8
              ExplicitWidth = 888
              ClientRectBottom = 328
              ClientRectRight = 890
              ClientRectTop = 24
              object tsCedente: TcxTabSheet
                Caption = 'Cedente'
                ImageIndex = 0
                ExplicitWidth = 888
                DesignSize = (
                  890
                  304)
                object Label5: TLabel
                  Left = 4
                  Top = 8
                  Width = 41
                  Height = 13
                  Caption = 'Cedente'
                end
                object Label6: TLabel
                  Left = 4
                  Top = 33
                  Width = 39
                  Height = 13
                  Caption = 'Carteira'
                end
                object Label7: TLabel
                  Left = 4
                  Top = 58
                  Width = 48
                  Height = 13
                  Caption = 'CNPJ/CPF'
                end
                object Label8: TLabel
                  Left = 222
                  Top = 58
                  Width = 29
                  Height = 13
                  Caption = 'Banco'
                end
                object Label9: TLabel
                  Left = 460
                  Top = 58
                  Width = 39
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Carteira'
                  ExplicitLeft = 458
                end
                object Label10: TLabel
                  Left = 4
                  Top = 85
                  Width = 49
                  Height = 13
                  Caption = 'Descri'#231#227'o '
                end
                object Label11: TLabel
                  Left = 460
                  Top = 112
                  Width = 38
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Ag'#234'ncia'
                  ExplicitLeft = 458
                end
                object Label12: TLabel
                  Left = 670
                  Top = 112
                  Width = 29
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Conta'
                  ExplicitLeft = 668
                end
                object Label13: TLabel
                  Left = 670
                  Top = 58
                  Width = 33
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'C'#243'digo'
                end
                object lbContaCorrente: TLabel
                  Left = 460
                  Top = 85
                  Width = 75
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Conta Corrente'
                end
                object Label15: TLabel
                  Left = 4
                  Top = 112
                  Width = 54
                  Height = 13
                  Caption = 'Documento'
                end
                object EdtNomeCedente: TgbDBTextEdit
                  Left = 65
                  Top = 4
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_NOME'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbRequired = True
                  gbPassword = False
                  ExplicitWidth = 813
                  Width = 815
                end
                object EdtCarteiraCedente: TgbDBTextEdit
                  Left = 65
                  Top = 29
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_CARTEIRA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbRequired = True
                  gbPassword = False
                  ExplicitWidth = 813
                  Width = 815
                end
                object EdtDocFederalCedente: TgbDBTextEdit
                  Left = 65
                  Top = 54
                  DataBinding.DataField = 'CEDENTE_DOC_FEDERAL'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbRequired = True
                  gbPassword = False
                  Width = 151
                end
                object EdtBancoCedente: TgbDBTextEdit
                  Left = 255
                  Top = 54
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_BANCO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbRequired = True
                  gbPassword = False
                  ExplicitWidth = 197
                  Width = 199
                end
                object EdtClasseCarteiraCedente: TgbDBTextEdit
                  Left = 538
                  Top = 54
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_CARTEIRA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbRequired = True
                  gbPassword = False
                  ExplicitLeft = 536
                  Width = 126
                end
                object EdtNomeCedentePersonalizado: TgbDBTextEdit
                  Left = 65
                  Top = 81
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_NOME_PERSONALIZADO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbRequired = True
                  gbPassword = False
                  ExplicitWidth = 387
                  Width = 389
                end
                object gbDBTextEdit7: TgbDBTextEdit
                  Left = 538
                  Top = 108
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_AGENCIA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  gbRequired = True
                  gbPassword = False
                  ExplicitLeft = 536
                  Width = 126
                end
                object gbDBTextEdit8: TgbDBTextEdit
                  Left = 705
                  Top = 108
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_CONTA_CORRENTE'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 11
                  gbRequired = True
                  gbPassword = False
                  ExplicitLeft = 703
                  Width = 175
                end
                object EdtCodigoCedente: TgbDBTextEdit
                  Left = 705
                  Top = 54
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CEDENTE_CODIGO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbRequired = True
                  gbPassword = False
                  Width = 175
                end
                object descContaCorrente: TgbDBTextEdit
                  Left = 595
                  Top = 81
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbReadyOnly = True
                  gbPassword = False
                  ExplicitLeft = 593
                  Width = 285
                end
                object edtContaCorrente: TgbDBButtonEditFK
                  Left = 538
                  Top = 81
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_CONTA_CORRENTE'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 7
                  gbTextEdit = descContaCorrente
                  gbRequired = True
                  gbAtivarPopupMenu = False
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
                  gbTableName = 'CONTA_CORRENTE'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'CONTA_CORRENTE'
                  ExplicitLeft = 536
                  Width = 60
                end
                object cbTipoDocumento: TgbDBComboBox
                  Left = 65
                  Top = 108
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_TIPO_DOCUMENTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 9
                  ExplicitWidth = 387
                  Width = 389
                end
              end
              object tsBoleto: TcxTabSheet
                Caption = 'Boleto - Atribui'#231#245'es'
                ImageIndex = 1
                ExplicitWidth = 888
                object Label16: TLabel
                  Left = 8
                  Top = 7
                  Width = 89
                  Height = 13
                  Caption = 'Dias para Protesto'
                end
                object Label17: TLabel
                  Left = 8
                  Top = 32
                  Width = 63
                  Height = 13
                  Caption = 'Vl. Acr'#233'scimo'
                end
                object Label18: TLabel
                  Left = 8
                  Top = 57
                  Width = 87
                  Height = 13
                  Caption = 'Vl. Tarifa Banc'#225'ria'
                end
                object Label19: TLabel
                  Left = 208
                  Top = 7
                  Width = 40
                  Height = 13
                  Caption = '% Multa'
                end
                object Label20: TLabel
                  Left = 208
                  Top = 32
                  Width = 68
                  Height = 13
                  Caption = '% Mora Di'#225'ria'
                end
                object Label21: TLabel
                  Left = 208
                  Top = 57
                  Width = 59
                  Height = 13
                  Caption = '% Desconto'
                end
                object Label22: TLabel
                  Left = 383
                  Top = 7
                  Width = 131
                  Height = 13
                  Caption = 'N'#250'mero inicial da sequ'#234'ncia'
                end
                object Label23: TLabel
                  Left = 383
                  Top = 57
                  Width = 126
                  Height = 13
                  Caption = 'N'#250'mero final da sequ'#234'ncia'
                end
                object Label24: TLabel
                  Left = 383
                  Top = 32
                  Width = 143
                  Height = 13
                  Caption = 'Pr'#243'ximo n'#250'mero da sequ'#234'ncia'
                end
                object Label14: TLabel
                  Left = 611
                  Top = 7
                  Width = 131
                  Height = 13
                  Caption = 'Prefixo para Nosso Numero'
                end
                object gbDBCheckBox2: TgbDBCheckBox
                  Left = 8
                  Top = 179
                  Caption = 'Aceite?'
                  DataBinding.DataField = 'BO_ACEITE_BOLETO'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 12
                  Transparent = True
                  Visible = False
                  Width = 66
                end
                object gbDBCalcEdit2: TgbDBCalcEdit
                  Left = 103
                  Top = 28
                  DataBinding.DataField = 'BOLETO_VL_ACRESCIMO'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbRequired = True
                  Width = 97
                end
                object gbDBCalcEdit3: TgbDBCalcEdit
                  Left = 103
                  Top = 53
                  DataBinding.DataField = 'BOLETO_VL_TARIFA_BANCARIA'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbRequired = True
                  Width = 97
                end
                object gbDBSpinEdit2: TgbDBSpinEdit
                  Left = 103
                  Top = 3
                  DataBinding.DataField = 'BOLETO_DIAS_PROTESTO'
                  DataBinding.DataSource = dsData
                  Properties.MaxValue = 9999.000000000000000000
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbRequired = True
                  Width = 97
                end
                object gbDBCalcEdit4: TgbDBCalcEdit
                  Left = 279
                  Top = 3
                  DataBinding.DataField = 'BOLETO_PERC_MULTA'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbRequired = True
                  Width = 97
                end
                object gbDBCalcEdit5: TgbDBCalcEdit
                  Left = 279
                  Top = 28
                  DataBinding.DataField = 'BOLETO_PERC_MORA_DIARIA'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbRequired = True
                  Width = 97
                end
                object gbDBCalcEdit6: TgbDBCalcEdit
                  Left = 279
                  Top = 53
                  DataBinding.DataField = 'BOLETO_PERC_DESCONTO'
                  DataBinding.DataSource = dsData
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbRequired = True
                  Width = 97
                end
                object gbDBCheckBox3: TgbDBCheckBox
                  Left = 611
                  Top = 28
                  Caption = 'Enviar boleto por e-mail ap'#243's emiss'#227'o?'
                  DataBinding.DataField = 'BO_ENVIA_BOLETO_EMAIL'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  Transparent = True
                  Width = 212
                end
                object gbDBSpinEdit3: TgbDBSpinEdit
                  Left = 531
                  Top = 3
                  DataBinding.DataField = 'BOLETO_NOSSO_NUMERO_INICIAL'
                  DataBinding.DataSource = dsData
                  Properties.MaxValue = 9999.000000000000000000
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbRequired = True
                  Width = 77
                end
                object gbDBSpinEdit4: TgbDBSpinEdit
                  Left = 531
                  Top = 53
                  DataBinding.DataField = 'BOLETO_NOSSO_NUMERO_FINAL'
                  DataBinding.DataSource = dsData
                  Properties.MaxValue = 9999.000000000000000000
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbRequired = True
                  Width = 77
                end
                object EdtProximoNumero: TgbDBSpinEdit
                  Left = 531
                  Top = 28
                  DataBinding.DataField = 'BOLETO_NOSSO_NUMERO_PROXIMO'
                  DataBinding.DataSource = dsData
                  Properties.MaxValue = 9999.000000000000000000
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 7
                  gbRequired = True
                  Width = 77
                end
                object gbDBRadioGroup1: TgbDBRadioGroup
                  Left = 611
                  Top = 51
                  DataBinding.DataField = 'BOLETO_AMBIENTE'
                  DataBinding.DataSource = dsData
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = 'Homologa'#231#227'o'
                      Value = 'HOMOLOGACAO'
                    end
                    item
                      Caption = 'Produ'#231#227'o'
                      Value = 'PRODUCAO'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 11
                  Height = 24
                  Width = 212
                end
                object gbDBTextEdit10: TgbDBTextEdit
                  Left = 748
                  Top = 3
                  DataBinding.DataField = 'BOLETO_PREFIXO_NOSSO NUMERO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 9
                  gbRequired = True
                  gbPassword = False
                  Width = 69
                end
              end
              object cxTabSheet1: TcxTabSheet
                Caption = 'Boleto - Layout / Arquivos'
                ImageIndex = 2
                ExplicitWidth = 888
                DesignSize = (
                  890
                  304)
                object Label25: TLabel
                  Left = 4
                  Top = 7
                  Width = 191
                  Height = 13
                  Caption = 'Texto de Exibi'#231#227'o - Local de Pagamento'
                end
                object Label26: TLabel
                  Left = 4
                  Top = 32
                  Width = 146
                  Height = 13
                  Caption = 'Texto de Exibi'#231#227'o - Instru'#231#245'es'
                end
                object Label27: TLabel
                  Left = 4
                  Top = 57
                  Width = 165
                  Height = 13
                  Caption = 'Texto de Exibi'#231#227'o - Demonstrativo'
                end
                object Label28: TLabel
                  Left = 4
                  Top = 82
                  Width = 137
                  Height = 13
                  Caption = 'Layout de Emiss'#227'o do Boleto'
                end
                object Label29: TLabel
                  Left = 673
                  Top = 108
                  Width = 94
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Layout da Remessa'
                  ExplicitLeft = 679
                end
                object Label30: TLabel
                  Left = 673
                  Top = 135
                  Width = 90
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Layout do Retorno'
                  ExplicitLeft = 679
                end
                object Label31: TLabel
                  Left = 4
                  Top = 108
                  Width = 164
                  Height = 13
                  Caption = 'Diret'#243'rio para Salvar as Remessas'
                end
                object Label32: TLabel
                  Left = 439
                  Top = 108
                  Width = 110
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Sequ'#234'ncia da Remessa'
                  ExplicitLeft = 445
                end
                object Label33: TLabel
                  Left = 4
                  Top = 135
                  Width = 160
                  Height = 13
                  Caption = 'Diret'#243'rio para Salvar os Retornos'
                end
                object Label34: TLabel
                  Left = 4
                  Top = 184
                  Width = 180
                  Height = 13
                  Caption = 'Assunto para Email com Boleto Anexo'
                  Visible = False
                end
                object Label35: TLabel
                  Left = 4
                  Top = 209
                  Width = 182
                  Height = 13
                  Caption = 'Recibo Refer'#234'nte a emiss'#227'o do Boleto'
                  Visible = False
                end
                object Label36: TLabel
                  Left = 4
                  Top = 234
                  Width = 170
                  Height = 13
                  Caption = 'Recibo Refer'#234'nte ao Env'#237'o de Email'
                  Visible = False
                end
                object Label37: TLabel
                  Left = 6
                  Top = 160
                  Width = 167
                  Height = 13
                  Caption = 'Layout do Envio do Email do Boleto'
                  Visible = False
                end
                object gbDBBlobEdit1: TgbDBBlobEdit
                  Left = 201
                  Top = 3
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_LOCAL_PAGAMENTO'
                  DataBinding.DataSource = dsData
                  Properties.BlobEditKind = bekMemo
                  Properties.BlobPaintStyle = bpsText
                  Properties.ClearKey = 16430
                  Properties.ImmediatePost = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Width = 679
                end
                object gbDBBlobEdit2: TgbDBBlobEdit
                  Left = 201
                  Top = 28
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_INSTRUCOES'
                  DataBinding.DataSource = dsData
                  Properties.BlobEditKind = bekMemo
                  Properties.BlobPaintStyle = bpsText
                  Properties.ClearKey = 16430
                  Properties.ImmediatePost = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Width = 679
                end
                object gbDBBlobEdit3: TgbDBBlobEdit
                  Left = 201
                  Top = 53
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_DEMONSTRATIVO'
                  DataBinding.DataSource = dsData
                  Properties.BlobEditKind = bekMemo
                  Properties.BlobPaintStyle = bpsText
                  Properties.ClearKey = 16430
                  Properties.ImmediatePost = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  Width = 679
                end
                object cbLayoutBoleto: TgbDBComboBox
                  Left = 201
                  Top = 78
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_LAYOUT_BOLETO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbRequired = True
                  Width = 679
                end
                object cbLayoutRemessa: TgbDBComboBox
                  Left = 773
                  Top = 104
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'BOLETO_LAYOUT_REMESSA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbRequired = True
                  Width = 107
                end
                object cbLayoutRetorno: TgbDBComboBox
                  Left = 773
                  Top = 131
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'BOLETO_LAYOUT_RETORNO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbRequired = True
                  Width = 107
                end
                object EdtSequenciaRemessa: TgbDBSpinEdit
                  Left = 556
                  Top = 104
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'BOLETO_SEQUENCIA_REMESSA'
                  DataBinding.DataSource = dsData
                  Properties.MaxValue = 9999.000000000000000000
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbRequired = True
                  ExplicitLeft = 554
                  Width = 111
                end
                object EdtDiretorioRemessa: TcxDBButtonEdit
                  Left = 201
                  Top = 104
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_PATH_REMESSA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Style.Color = 14606074
                  TabOrder = 4
                  OnClick = EdtDiretorioRemessaClick
                  ExplicitWidth = 230
                  Width = 232
                end
                object EdtDiretorioRetorno: TcxDBButtonEdit
                  Left = 201
                  Top = 131
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_PATH_RETORNO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Style.Color = 14606074
                  TabOrder = 7
                  OnClick = EdtDiretorioRetornoClick
                  ExplicitWidth = 463
                  Width = 465
                end
                object gbDBBlobEdit4: TgbDBBlobEdit
                  Left = 201
                  Top = 180
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_ASSUNTO_EMAIL'
                  DataBinding.DataSource = dsData
                  Properties.BlobEditKind = bekMemo
                  Properties.BlobPaintStyle = bpsText
                  Properties.ClearKey = 16430
                  Properties.ImmediatePost = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  Visible = False
                  Width = 679
                end
                object gbDBButtonEditFK1: TgbDBButtonEditFK
                  Left = 201
                  Top = 205
                  DataBinding.DataField = 'BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Style.Color = clWhite
                  TabOrder = 11
                  Visible = False
                  Width = 64
                end
                object gbDBTextEdit12: TgbDBTextEdit
                  Left = 263
                  Top = 205
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'DESCRICAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 12
                  Visible = False
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 617
                end
                object gbDBButtonEditFK2: TgbDBButtonEditFK
                  Left = 201
                  Top = 230
                  DataBinding.DataField = 'BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Style.Color = clWhite
                  TabOrder = 13
                  Visible = False
                  Width = 64
                end
                object gbDBTextEdit13: TgbDBTextEdit
                  Left = 263
                  Top = 230
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'DESCRICAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 14
                  Visible = False
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 617
                end
                object cbLayoutEmail: TgbDBComboBox
                  Left = 201
                  Top = 156
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'BOLETO_LAYOUT_EMAIL'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 9
                  Visible = False
                  Width = 679
                end
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 233
      FloatClientHeight = 226
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbCarregarArquivoLicensaCobreBemX'
        end
        item
          Visible = True
          ItemName = 'bbRemoverArquivoLicensaCobreBemX'
        end>
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 486
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 644
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 815
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 569
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 723
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object bbCarregarArquivoLicensaCobreBemX: TdxBarLargeButton [16]
      Action = ActCarregarArquivoConfiguracaoCobreBemX
      Category = 1
    end
    object bbRemoverArquivoLicensaCobreBemX: TdxBarLargeButton [17]
      Action = ActDescarregarArquivoConfiguracaoCobreBemX
      Category = 1
    end
  end
  inherited ActionListMain: TActionList
    object ActCarregarArquivoConfiguracaoCobreBemX: TAction
      Category = 'Manager'
      Caption = 'Carregar Arquivo de Licen'#231'a CobreBemX'
      ImageIndex = 149
      OnExecute = ActCarregarArquivoConfiguracaoCobreBemXExecute
    end
    object ActDescarregarArquivoConfiguracaoCobreBemX: TAction
      Category = 'Manager'
      Caption = 'Remover Arquivo de Licen'#231'a CobreBemX'
      ImageIndex = 150
      OnExecute = ActDescarregarArquivoConfiguracaoCobreBemXExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspCarteira'
    RemoteServer = DmConnection.dspCarteira
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataPERC_JUROS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_MULTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDataCARENCIA: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Car'#234'ncia'
      FieldName = 'CARENCIA'
      Origin = 'CARENCIA'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataTIPO_BOLETO: TStringField
      FieldName = 'TIPO_BOLETO'
      Origin = 'TIPO_BOLETO'
      Size = 25
    end
    object cdsDataCBX_ARQUIVO_LICENCA: TBlobField
      DisplayLabel = 'Arquivo de Licen'#231'a'
      FieldName = 'CBX_ARQUIVO_LICENCA'
      Origin = 'CBX_ARQUIVO_LICENCA'
    end
    object cdsDataCEDENTE_NOME: TStringField
      DisplayLabel = 'Cedente - Nome'
      FieldName = 'CEDENTE_NOME'
      Origin = 'CEDENTE_NOME'
      Size = 255
    end
    object cdsDataCEDENTE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Cedente - Documento Federal'
      FieldName = 'CEDENTE_DOC_FEDERAL'
      Origin = 'CEDENTE_DOC_FEDERAL'
      Size = 11
    end
    object cdsDataCEDENTE_BANCO: TStringField
      DisplayLabel = 'Cedente - Banco'
      FieldName = 'CEDENTE_BANCO'
      Origin = 'CEDENTE_BANCO'
      Size = 25
    end
    object cdsDataCEDENTE_CARTEIRA: TStringField
      DisplayLabel = 'Cedente - Carteira'
      FieldName = 'CEDENTE_CARTEIRA'
      Origin = 'CEDENTE_CARTEIRA'
      Size = 5
    end
    object cdsDataCEDENTE_NOME_PERSONALIZADO: TStringField
      DisplayLabel = 'Cedente - Nome Personalizado'
      FieldName = 'CEDENTE_NOME_PERSONALIZADO'
      Origin = 'CEDENTE_NOME_PERSONALIZADO'
      Size = 255
    end
    object cdsDataCEDENTE_AGENCIA: TStringField
      DisplayLabel = 'Cedente - Ag'#234'ncia'
      FieldName = 'CEDENTE_AGENCIA'
      Origin = 'CEDENTE_AGENCIA'
      Size = 10
    end
    object cdsDataCEDENTE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Cedente - Conta Corrente'
      FieldName = 'CEDENTE_CONTA_CORRENTE'
      Origin = 'CEDENTE_CONTA_CORRENTE'
      Size = 10
    end
    object cdsDataBOLETO_NOSSO_NUMERO_INICIAL: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'Boleto - Nosso N'#250'mero Inicial'
      FieldName = 'BOLETO_NOSSO_NUMERO_INICIAL'
      Origin = 'BOLETO_NOSSO_NUMERO_INICIAL'
    end
    object cdsDataBOLETO_NOSSO_NUMERO_FINAL: TIntegerField
      DefaultExpression = '999999'
      DisplayLabel = 'Boleto - Nosso N'#250'mero Final'
      FieldName = 'BOLETO_NOSSO_NUMERO_FINAL'
      Origin = 'BOLETO_NOSSO_NUMERO_FINAL'
    end
    object cdsDataBOLETO_NOSSO_NUMERO_PROXIMO: TIntegerField
      DisplayLabel = 'Boleto - Nosso N'#250'mero Pr'#243'ximo'
      FieldName = 'BOLETO_NOSSO_NUMERO_PROXIMO'
      Origin = 'BOLETO_NOSSO_NUMERO_PROXIMO'
    end
    object cdsDataBOLETO_MODALIDADE: TIntegerField
      DisplayLabel = 'Boleto - Modalidade'
      FieldName = 'BOLETO_MODALIDADE'
      Origin = 'BOLETO_MODALIDADE'
    end
    object cdsDataBOLETO_CODIGO_TRANSMISSAO: TIntegerField
      DisplayLabel = 'Boleto - C'#243'digo de Transmiss'#227'o'
      FieldName = 'BOLETO_CODIGO_TRANSMISSAO'
      Origin = 'BOLETO_CODIGO_TRANSMISSAO'
    end
    object cdsDataBOLETO_LOCAL_PAGAMENTO: TStringField
      DisplayLabel = 'Boleto - Local de Pagamento'
      FieldName = 'BOLETO_LOCAL_PAGAMENTO'
      Origin = 'BOLETO_LOCAL_PAGAMENTO'
      Size = 255
    end
    object cdsDataBOLETO_INSTRUCOES: TBlobField
      DisplayLabel = 'Boleto - Instru'#231#245'es'
      FieldName = 'BOLETO_INSTRUCOES'
      Origin = 'BOLETO_INSTRUCOES'
    end
    object cdsDataBOLETO_DEMONSTRATIVO: TBlobField
      DisplayLabel = 'Boleto - Demonstrativo'
      FieldName = 'BOLETO_DEMONSTRATIVO'
      Origin = 'BOLETO_DEMONSTRATIVO'
    end
    object cdsDataBOLETO_LAYOUT_IMPRESSAO: TStringField
      DisplayLabel = 'Boleto - Layout Impress'#227'o'
      FieldName = 'BOLETO_LAYOUT_IMPRESSAO'
      Origin = 'BOLETO_LAYOUT_IMPRESSAO'
      Size = 120
    end
    object cdsDataBOLETO_LAYOUT_REMESSA: TStringField
      DisplayLabel = 'Boleto - Layout Remessa'
      FieldName = 'BOLETO_LAYOUT_REMESSA'
      Origin = 'BOLETO_LAYOUT_REMESSA'
      Size = 50
    end
    object cdsDataBOLETO_LAYOUT_RETORNO: TStringField
      DisplayLabel = 'Boleto - Layout Retorno'
      FieldName = 'BOLETO_LAYOUT_RETORNO'
      Origin = 'BOLETO_LAYOUT_RETORNO'
      Size = 50
    end
    object cdsDataBOLETO_PATH_REMESSA: TStringField
      DisplayLabel = 'Boleto - Diret'#243'rio para Salvar as Remessas'
      FieldName = 'BOLETO_PATH_REMESSA'
      Origin = 'BOLETO_PATH_REMESSA'
      Size = 255
    end
    object cdsDataBOLETO_PATH_RETORNO: TStringField
      DisplayLabel = 'Boleto - Diret'#243'rio para Salvar os Retornos'
      FieldName = 'BOLETO_PATH_RETORNO'
      Origin = 'BOLETO_PATH_RETORNO'
      Size = 255
    end
    object cdsDataBOLETO_ESPECIE_DOCUMENTO: TStringField
      DisplayLabel = 'Boleto - Esp'#233'cie do Documento'
      FieldName = 'BOLETO_ESPECIE_DOCUMENTO'
      Origin = 'BOLETO_ESPECIE_DOCUMENTO'
      Size = 50
    end
    object cdsDataBOLETO_SEQUENCIA_REMESSA: TIntegerField
      DisplayLabel = 'Boleto - Sequ'#234'ncia da Remessa'
      FieldName = 'BOLETO_SEQUENCIA_REMESSA'
      Origin = 'BOLETO_SEQUENCIA_REMESSA'
    end
    object cdsDataBOLETO_AMBIENTE: TStringField
      DefaultExpression = 'HOMOLOGACAO'
      DisplayLabel = 'Boleto - Ambiente'
      FieldName = 'BOLETO_AMBIENTE'
      Origin = 'BOLETO_AMBIENTE'
      Size = 15
    end
    object cdsDataBOLETO_DIAS_PROTESTO: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Dias de Protesto'
      FieldName = 'BOLETO_DIAS_PROTESTO'
      Origin = 'BOLETO_DIAS_PROTESTO'
    end
    object cdsDataBO_ACEITE_BOLETO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Boleto - Aceite'
      FieldName = 'BO_ACEITE_BOLETO'
      Origin = 'BO_ACEITE_BOLETO'
      FixedChar = True
      Size = 1
    end
    object cdsDataBOLETO_VL_ACRESCIMO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Acr'#233'scimo'
      FieldName = 'BOLETO_VL_ACRESCIMO'
      Origin = 'BOLETO_VL_ACRESCIMO'
      Precision = 15
      Size = 2
    end
    object cdsDataBOLETO_VL_TARIFA_BANCARIA: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Tarifa Banc'#225'ria'
      FieldName = 'BOLETO_VL_TARIFA_BANCARIA'
      Origin = 'BOLETO_VL_TARIFA_BANCARIA'
      Precision = 15
      Size = 2
    end
    object cdsDataBOLETO_PERC_MULTA: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Percentual da Multa'
      FieldName = 'BOLETO_PERC_MULTA'
      Origin = 'BOLETO_PERC_MULTA'
      Precision = 15
      Size = 2
    end
    object cdsDataBOLETO_PERC_MORA_DIARIA: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Percentual da Mora Di'#225'ria'
      FieldName = 'BOLETO_PERC_MORA_DIARIA'
      Origin = 'BOLETO_PERC_MORA_DIARIA'
      Precision = 15
      Size = 2
    end
    object cdsDataBOLETO_PERC_DESCONTO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Boleto - Percentual do Desconto'
      FieldName = 'BOLETO_PERC_DESCONTO'
      Origin = 'BOLETO_PERC_DESCONTO'
      Precision = 15
      Size = 2
    end
    object cdsDataBOLETO_MOEDA: TStringField
      DisplayLabel = 'Boleto - Moeda'
      FieldName = 'BOLETO_MOEDA'
      Origin = 'BOLETO_MOEDA'
      Size = 5
    end
    object cdsDataBO_ENVIA_BOLETO_EMAIL: TStringField
      DisplayLabel = 'Boleto - Enviar Email'
      FieldName = 'BO_ENVIA_BOLETO_EMAIL'
      Origin = 'BO_ENVIA_BOLETO_EMAIL'
      FixedChar = True
      Size = 1
    end
    object cdsDataBOLETO_ASSUNTO_EMAIL: TStringField
      DisplayLabel = 'Boleto - Assunto do Email'
      FieldName = 'BOLETO_ASSUNTO_EMAIL'
      Origin = 'BOLETO_ASSUNTO_EMAIL'
      Size = 255
    end
    object cdsDataBOLETO_LAYOUT_BOLETO: TStringField
      DisplayLabel = 'Boleto - Layout do Boleto'
      FieldName = 'BOLETO_LAYOUT_BOLETO'
      Origin = 'BOLETO_LAYOUT_BOLETO'
      Size = 50
    end
    object cdsDataBOLETO_ID_RECIBO_EMAIL_PERSONALIZADO: TIntegerField
      DisplayLabel = 'Boleto - C'#243'digo do Recibo Personalizado'
      FieldName = 'BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO'
      Origin = 'BOLETO_ID_RECIBO_EMAIL_PERSONALIZADO'
    end
    object cdsDataBOLETO_ID_RECIBO_BOLETO_PERSONALIZADO: TIntegerField
      DisplayLabel = 'Boleto - C'#243'digo do Recibo do Boleto Personalizado'
      FieldName = 'BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO'
      Origin = 'BOLETO_ID_RECIBO_BOLETO_PERSONALIZADO'
    end
    object cdsDataBOLETO_PREFIXO_NOSSONUMERO: TIntegerField
      DisplayLabel = 'Boleto - Prefixo do Nosso N'#250'mero'
      FieldName = 'BOLETO_PREFIXO_NOSSO NUMERO'
      Origin = '`BOLETO_PREFIXO_NOSSO NUMERO`'
    end
    object cdsDataBOLETO_TIPO_DOCUMENTO: TStringField
      DisplayLabel = 'Boleto - Tipo de Documento'
      FieldName = 'BOLETO_TIPO_DOCUMENTO'
      Origin = 'BOLETO_TIPO_DOCUMENTO'
      Size = 50
    end
    object cdsDataBOLETO_LAYOUT_EMAIL: TStringField
      DisplayLabel = 'Boleto - Layout E-mail'
      FieldName = 'BOLETO_LAYOUT_EMAIL'
      Origin = 'BOLETO_LAYOUT_EMAIL'
      Size = 50
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataCEDENTE_CODIGO: TStringField
      DisplayLabel = 'Cedente - C'#243'digo'
      FieldName = 'CEDENTE_CODIGO'
      Origin = 'CEDENTE_CODIGO'
      Size = 11
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.854666469910000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object odCobreBemX: TOpenDialog
    Filter = 'Arquivo de Licensa CobreBemX|*.conf'
    Left = 808
    Top = 376
  end
  object sdSelecionarDiretorio: TJvSelectDirectory
    Title = 'Local para Salvar os Arquivos'
    Left = 816
    Top = 424
  end
end
