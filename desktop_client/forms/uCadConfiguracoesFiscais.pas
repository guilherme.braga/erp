unit uCadConfiguracoesFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  cxSplitter, JvExControls, JvButton, JvTransparentButton, uGBPanel, cxVGrid, cxDBVGrid, cxInplaceContainer,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans,
  dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter, cxMaskEdit, cxBlobEdit;

type
  TCadConfiguracoesFiscais = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataLOGOMARCA: TBlobField;
    cdsDataCERTIFICADO_CAMINHO: TStringField;
    cdsDataCERTIFICADO_SENHA: TStringField;
    cdsDataCERTIFICADO_NUMERO_SERIE: TStringField;
    cdsDataNFE_MODO_IMPRESSAO: TStringField;
    cdsDataNFE_FORMA_EMISSAO: TStringField;
    cdsDataNFE_WEBSERVICE_UF: TStringField;
    cdsDataNFE_WEBSERVICE_AMBIENTE: TStringField;
    cdsDataNFE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField;
    cdsDataNFE_PROXY_HOST: TStringField;
    cdsDataNFE_PROXY_PORTA: TIntegerField;
    cdsDataNFE_PROXY_USUARIO: TStringField;
    cdsDataNFE_PROXY_SENHA: TStringField;
    cdsDataEMAIL_SMTP: TStringField;
    cdsDataEMAIL_PORTA: TStringField;
    cdsDataEMAIL_USUARIO: TStringField;
    cdsDataEMAIL_SENHA: TStringField;
    cdsDataEMAIL_ASSUNTO: TStringField;
    cdsDataEMAIL_MENSAGEM: TBlobField;
    cdsDataNFCE_MODO_IMPRESSAO: TStringField;
    cdsDataNFCE_WEBSERVICE_UF: TStringField;
    cdsDataNFCE_WEBSERVICE_AMBIENTE: TStringField;
    cdsDataNFCE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField;
    cdsDataNFCE_PROXY_HOST: TStringField;
    cdsDataNFCE_PROXY_PORTA: TIntegerField;
    cdsDataNFCE_PROXY_USUARIO: TStringField;
    cdsDataNFCE_PROXY_SENHA: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cdsDataNFE_ID_RELATORIO_FR: TIntegerField;
    cdsDataEXPANDIR_LOGOMARCAR: TStringField;
    cdsDataNFCE_ID_CSC: TStringField;
    cdsDataNFCE_CSC: TStringField;
    cdsDataARQUIVO_PATH_LOGS: TStringField;
    cdsDataARQUIVO_PATH_SCHEMAS: TStringField;
    cdsDataARQUIVO_PATH_NFE: TStringField;
    cdsDataARQUIVO_PATH_CANCELAMENTO: TStringField;
    cdsDataARQUIVO_PATH_INUTILIZACAO: TStringField;
    cdsDataARQUIVO_PATH_DPEC: TStringField;
    cdsDataARQUIVO_PATH_EVENTO: TStringField;
    cxDBVerticalGrid1LOGOMARCA: TcxDBEditorRow;
    cxDBVerticalGrid1CERTIFICADO_SENHA: TcxDBEditorRow;
    cxDBVerticalGrid1CERTIFICADO_NUMERO_SERIE: TcxDBEditorRow;
    cxDBVerticalGrid1NFE_MODO_IMPRESSAO: TcxDBEditorRow;
    cxDBVerticalGrid1NFE_FORMA_EMISSAO: TcxDBEditorRow;
    cxDBVerticalGrid1NFE_WEBSERVICE_UF: TcxDBEditorRow;
    cxDBVerticalGrid1NFE_WEBSERVICE_AMBIENTE: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_SMTP: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_PORTA: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_USUARIO: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_SENHA: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_ASSUNTO: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_MENSAGEM: TcxDBEditorRow;
    cxDBVerticalGrid1NFCE_MODO_IMPRESSAO: TcxDBEditorRow;
    cxDBVerticalGrid1NFCE_WEBSERVICE_UF: TcxDBEditorRow;
    cxDBVerticalGrid1NFCE_WEBSERVICE_AMBIENTE: TcxDBEditorRow;
    cxDBVerticalGrid1NFCE_CSC: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_LOGS: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_SCHEMAS: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_NFE: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_CANCELAMENTO: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_INUTILIZACAO: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_DPEC: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_PATH_EVENTO: TcxDBEditorRow;
    cdsDataARQUIVO_RELATORIO_NFCE_FR: TStringField;
    cdsDataARQUIVO_EVENTO_NFCE_FR: TStringField;
    cdsDataARQUIVO_RELATORIO_NFE_FR: TStringField;
    cdsDataARQUIVO_EVENTO_NFE_FR: TStringField;
    cxDBVerticalGrid1ARQUIVO_RELATORIO_NFCE_FR: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_EVENTO_NFCE_FR: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_RELATORIO_NFE_FR: TcxDBEditorRow;
    cxDBVerticalGrid1ARQUIVO_EVENTO_NFE_FR: TcxDBEditorRow;
    ActValidarStatusReceita: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    cxDBVerticalGrid1NFCE_ID_CSC: TcxDBEditorRow;
    cdsDataIMPRESSORA_NFE: TStringField;
    cdsDataIMPRESSORA_NFCE: TStringField;
    cxDBVerticalGrid1IMPRESSORA_NFE: TcxDBEditorRow;
    cxDBVerticalGrid1IMPRESSORA_NFCE: TcxDBEditorRow;
    cdsDataARQUIVO_PATH_PDF: TStringField;
    cxDBVerticalGrid1ARQUIVO_PATH_PDF: TcxDBEditorRow;
    cdsDataNFCE_PREVIEW: TStringField;
    cdsDataNFE_PREVIEW: TStringField;
    cxDBVerticalGrid1NFCE_PREVIEW: TcxDBEditorRow;
    cxDBVerticalGrid1NFE_PREVIEW: TcxDBEditorRow;
    cdsDataSALVAR_PDF_JUNTO_XML: TStringField;
    cxDBVerticalGrid1SALVAR_PDF_JUNTO_XML: TcxDBEditorRow;
    cdsDataEMAIL_ENVIAR_NFE: TStringField;
    cdsDataEMAIL_ENVIAR_NFCE: TStringField;
    cdsDataEMAIL_EMAIL_HOST: TStringField;
    cdsDataEMAIL_EMAIL_DESTINO: TStringField;
    cdsDataEMAIL_EMAIL_CC: TStringField;
    cdsDataEMAIL_EMAIL_CCO: TStringField;
    cdsDataEMAIL_SSL: TStringField;
    cdsDataEMAIL_SOLICITA_CONFIRMACAO: TStringField;
    cdsDataEMAIL_USAR_THREAD: TStringField;
    cdsDataEMAIL_NOME_ORIGEM: TStringField;
    cxDBVerticalGrid1EMAIL_ENVIAR_NFE: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_ENVIAR_NFCE: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_EMAIL_HOST: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_EMAIL_CC: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_EMAIL_CCO: TcxDBEditorRow;
    categoriaGeral: TcxCategoryRow;
    categoriaNFE: TcxCategoryRow;
    categoriaNFCE: TcxCategoryRow;
    categoriaEmail: TcxCategoryRow;
    categoriaCertificadoDigital: TcxCategoryRow;
    categoriaDiretorioArquivo: TcxCategoryRow;
    categoriaImpressora: TcxCategoryRow;
    cxDBVerticalGrid1EMAIL_SSL: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_SOLICITA_CONFIRMACAO: TcxDBEditorRow;
    cxDBVerticalGrid1EMAIL_NOME_ORIGEM: TcxDBEditorRow;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ActValidarStatusReceitaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    function DuplicarRegistro: Integer; override;
  end;

var
  CadConfiguracoesFiscais: TCadConfiguracoesFiscais;

implementation

{$R *.dfm}

uses uDmConnection, uSistema, uNotaFiscal, uConfiguracaoFiscal;

procedure TCadConfiguracoesFiscais.ActValidarStatusReceitaExecute(Sender: TObject);
begin
  inherited;
  TNotaFiscal.ChecarServidorAtivo(TSistema.Sistema.filial.FId, TSistema.Sistema.usuario.id);
end;

procedure TCadConfiguracoesFiscais.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;
  cdsDataJOIN_FANTASIA_FILIAL.AsString := TSistema.Sistema.Filial.FFantasia;
  cdsDataEXPANDIR_LOGOMARCAR.AsString := 'S';
  cdsDataNFCE_PREVIEW.AsString := 'S';
  cdsDataNFE_PREVIEW.AsString := 'S';
end;

function TCadConfiguracoesFiscais.DuplicarRegistro: Integer;
begin
  result := TConfiguracaoFiscal.DuplicarConfiguracaoFiscal(cdsDataID.AsInteger);
end;

Initialization
  RegisterClass(TCadConfiguracoesFiscais);

Finalization
  UnRegisterClass(TCadConfiguracoesFiscais);

end.
