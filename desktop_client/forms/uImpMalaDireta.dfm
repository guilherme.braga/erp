inherited ImpMalaDireta: TImpMalaDireta
  Caption = 'Mala Direta'
  ExplicitWidth = 865
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited dxBarManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 167
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbImprimirChildPadrao'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 79
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
  end
end
