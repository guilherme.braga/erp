unit uMovVendaVarejoRecebimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxLabel,
  cxTextEdit, cxDBEdit, uGBDBTextEdit, uGBPanel, db, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvComponentBase, JvEnterTab, cxMemo, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TMovVendaVarejoRecebimento = class(TFrmModalPadrao)
    PnValorBruto: TgbPanel;
    EdtTotal: TgbDBTextEdit;
    cxLabel7: TcxLabel;
    gbPanel1: TgbPanel;
    EdtRecebimento: TgbDBTextEdit;
    cxLabel1: TcxLabel;
    gbPanel2: TgbPanel;
    gbDBTextEdit2: TgbDBTextEdit;
    lbTrocoFalta: TcxLabel;
    fdmRecebimento: TFDMemTable;
    fdmRecebimentoVL_TOTAL: TFloatField;
    fdmRecebimentoVL_RECEBIDO: TFloatField;
    fdmRecebimentoVL_TROCO: TFloatField;
    dsRecebimento: TDataSource;
    EdtProblemaFoco: TEdit;
    JvEnterAsTab1: TJvEnterAsTab;
    mmTroco: TcxMemo;
    procedure ActConfirmarExecute(Sender: TObject);
    procedure ActCancelarExecute(Sender: TObject);
    procedure CalcularTroco(Sender: TField);
    procedure EdtRecebimentoPropertiesChange(Sender: TObject);
  private
    FValorRecebido: Double;
    FValorTroco: Double;
    procedure ValidarRecebimento;
  public
    class function RecebimentoAVista(const AValorTotal: Double; var AValorRecebido, AValorTroco: Double): Integer;
  end;

var
  MovVendaVarejoRecebimento: TMovVendaVarejoRecebimento;

implementation

{$R *.dfm}

uses uControlsUtils, uFrmMessage, uDatasetUtils, uTFunction, uAcbrUtils;

{ TMovVendaRecebimento }

procedure TMovVendaVarejoRecebimento.ActCancelarExecute(Sender: TObject);
begin
  fdmRecebimento.CancelUpdates;
  inherited;
end;

procedure TMovVendaVarejoRecebimento.ActConfirmarExecute(Sender: TObject);
begin
  ActiveControl := EdtProblemaFoco;
  ValidarRecebimento;
  FValorRecebido := fdmRecebimentoVL_RECEBIDO.AsFloat;
  FValorTroco := fdmRecebimentoVL_Troco.AsFloat;
  inherited;
end;

procedure TMovVendaVarejoRecebimento.CalcularTroco(Sender: TField);
begin
  if fdmRecebimento.State in dsEditModes then
  begin
    fdmRecebimentoVL_TROCO.AsFloat := StrtoFloatDef(EdtRecebimento.Text, 0) - fdmRecebimentoVL_TOTAL.AsFloat;
    lbTrocoFalta.Caption := TFunction.Iif(fdmRecebimentoVL_TROCO.AsFloat > 0, 'Troco', 'Falta');

    if fdmRecebimentoVL_TROCO.AsFloat < 0 then
    begin
      fdmRecebimentoVL_TROCO.AsFloat := -1 * fdmRecebimentoVL_TROCO.AsFloat;
    end;

    if fdmRecebimentoVL_TROCO.AsFloat > 0 then
    begin
      TAcbrUtils.ExibirTroco(fdmRecebimentoVL_TROCO.AsFloat, mmTroco);
    end
    else
    begin
      mmTroco.Clear;
    end;
  end;
end;

procedure TMovVendaVarejoRecebimento.EdtRecebimentoPropertiesChange(
  Sender: TObject);
begin
  inherited;
  CalcularTroco(nil);
end;

class function TMovVendaVarejoRecebimento.RecebimentoAVista(const AValorTotal: Double; var AValorRecebido, AValorTroco: Double): Integer;
begin
  Application.CreateForm(TMovVendaVarejoRecebimento, MovVendaVarejoRecebimento);
  try
    with MovVendaVarejoRecebimento do
    begin
      fdmRecebimento.Open;
      fdmRecebimento.Append;
      fdmRecebimentoVL_TOTAL.AsFloat := AValorTotal;
      fdmRecebimentoVL_RECEBIDO.AsFloat := AValorTotal;
      fdmRecebimentoVL_TROCO.AsFloat := 0;
      ShowModal;
      if MovVendaVarejoRecebimento.result = MCONFIRMED then
      begin
        AValorRecebido := fdmRecebimentoVL_RECEBIDO.AsFloat;
        AValorTroco := fdmRecebimentoVL_TROCO.AsFloat;
      end;
    end;
    Result := MovVendaVarejoRecebimento.result;
  finally
    FreeAndNil(MovVendaVarejoRecebimento);
  end;
end;

procedure TMovVendaVarejoRecebimento.ValidarRecebimento;
begin
  if fdmRecebimentoVL_RECEBIDO.AsFloat < fdmRecebimentoVL_TOTAL.AsFloat then
  begin
    TFrmMessage.Information('O documento ainda n�o foi quitado por completo.');
    TControlsUtils.SetFocus(EdtRecebimento);
    Abort;
  end;
end;

end.
