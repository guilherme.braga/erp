unit uFrmLiberacaoBloqueioPersonalizado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons,
  cxGroupBox, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ugbClientDataset;

type
  TFrmLiberacaoBloqueioPersonalizado = class(TFrmModalPadrao)
    cxGridPesquisaPadrao: TcxGrid;
    viewBloqueiosPersonalizados: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    dsBloqueiosPersonalizados: TDataSource;
    viewBloqueiosPersonalizadosSTATUS: TcxGridDBBandedColumn;
    viewBloqueiosPersonalizadosDH_LIBERACAO: TcxGridDBBandedColumn;
    viewBloqueiosPersonalizadosJOIN_NOME_PESSOA_USUARIO: TcxGridDBBandedColumn;
    viewBloqueiosPersonalizadosJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TcxGridDBBandedColumn;
    ActSair: TAction;
    btnSair: TcxButton;
    procedure viewBloqueiosPersonalizadosDblClick(Sender: TObject);
    procedure ActSairExecute(Sender: TObject);
  private
    FDatasetBloqueiosPersonalizados: TgbClientDataSet;
  public
    class procedure AprovarBloqueios(ADatasetBloqueio: TgbClientDataset);
  protected
    function VerificarUsuarioPodeLiberar(const AIdUsuario: Integer): Boolean; virtual;
    procedure RealizarLiberacao; virtual;
    function SolicitarAutenticacao(var AIdUsuario: Integer): Boolean; virtual;
  end;

var
  FrmLiberacaoBloqueioPersonalizado: TFrmLiberacaoBloqueioPersonalizado;

implementation

{$R *.dfm}

uses uFrmAutenticacaoUsuario, uBloqueioPersonalizadoProxy, uBloqueioPersonalizado, uFrmMessage, uUsuarioProxy,
  uTUsuario;

procedure TFrmLiberacaoBloqueioPersonalizado.ActSairExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

class procedure TFrmLiberacaoBloqueioPersonalizado.AprovarBloqueios(ADatasetBloqueio: TgbClientDataset);
begin
  Application.CreateForm(TFrmLiberacaoBloqueioPersonalizado, FrmLiberacaoBloqueioPersonalizado);
  try
    FrmLiberacaoBloqueioPersonalizado.FDatasetBloqueiosPersonalizados := ADatasetBloqueio;
    FrmLiberacaoBloqueioPersonalizado.dsBloqueiosPersonalizados.Dataset :=
      FrmLiberacaoBloqueioPersonalizado.FDatasetBloqueiosPersonalizados;
    FrmLiberacaoBloqueioPersonalizado.ShowModal;
  finally
    FreeAndNil(FrmLiberacaoBloqueioPersonalizado);
  end;
end;

procedure TFrmLiberacaoBloqueioPersonalizado.RealizarLiberacao;
var
  idUsuario: Integer;
  usuario: TUsuarioProxy;
begin
  if FDatasetBloqueiosPersonalizados.FieldByName('STATUS').AsString.Equals(
    TBloqueioPersonalizadoProxy.STATUS_LIBERADO) then
  begin
    exit;
  end;

  if not SolicitarAutenticacao(idUsuario) then
  begin
    exit;
  end;

  if VerificarUsuarioPodeLiberar(idUsuario) then
  begin
    usuario := TUsuario.GetUsuario(idUsuario);
    try
      FDatasetBloqueiosPersonalizados.Alterar;
      FDatasetBloqueiosPersonalizados.SetAsDateTime('DH_LIBERACAO', Now);
      FDatasetBloqueiosPersonalizados.SetAsInteger('ID_PESSOA_USUARIO', usuario.id);
      FDatasetBloqueiosPersonalizados.SetAsString('JOIN_NOME_PESSOA_USUARIO', usuario.nome);
      FDatasetBloqueiosPersonalizados.SetAsString('STATUS', TBloqueioPersonalizadoProxy.STATUS_LIBERADO);
      FDatasetBloqueiosPersonalizados.Salvar;

      if not FDatasetBloqueiosPersonalizados.Locate('STATUS',
        TBloqueioPersonalizadoProxy.STATUS_BLOQUEADO, []) then
      begin
        close;
      end;
    finally
      FreeAndNil(usuario);
    end;
  end
  else
  begin
    TFrmMessage.Information('O usu�rio informado n�o possui autoriza��o para esse desbloqueio!');
  end;
end;

function TFrmLiberacaoBloqueioPersonalizado.SolicitarAutenticacao(var AIdUsuario: Integer): Boolean;
begin
  result := TFrmAutenticacaoUsuario.Autenticar(AIdUsuario);
end;

function TFrmLiberacaoBloqueioPersonalizado.VerificarUsuarioPodeLiberar(const AIdUsuario: Integer): Boolean;
begin
  result := TBloqueioPeronalizado.UsuarioAutorizado(
    FDatasetBloqueiosPersonalizados.FieldByName('ID_BLOQUEIO_PERSONALIZADO').AsInteger, AIdUsuario);
end;

procedure TFrmLiberacaoBloqueioPersonalizado.viewBloqueiosPersonalizadosDblClick(Sender: TObject);
begin
  inherited;
  RealizarLiberacao;
end;

end.
