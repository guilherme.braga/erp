unit uCadCSOSN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEditPK,
  Vcl.StdCtrls, cxTextEdit, cxDBEdit, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, uGBDBBlobEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, uGBPanel;

type
  TCadCSOSN = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    EdtCST: TgbDBTextEdit;
    lbCST: TLabel;
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    EdtObservacao: TgbDBBlobEdit;
    Label3: TLabel;
    cdsDataCODIGO_CST: TStringField;
    cdsDataCODIGO_CSOSN: TStringField;
    lbCSOSN: TLabel;
    EdtCSOSN: TgbDBTextEdit;
    EdtDescricao: TgbDBTextEdit;
    Label5: TLabel;
  private
    FEmpresaOptanteDoSimples: Boolean;
    procedure ExibirCSOSN;
  public
    procedure AoCriarFormulario; override;
    procedure AntesDeConfirmar; override;
  end;


implementation

{$R *.dfm}

uses uDmConnection, uFilial, uSistema, uFrmMessage, uDatasetUtils;

{ TCadCSOSN }

procedure TCadCSOSN.AntesDeConfirmar;
begin
  inherited;
  if FEmpresaOptanteDoSimples then
  begin
    TValidacaoCampo.CampoPreenchido(cdsDataCODIGO_CSOSN);
  end
  else
  begin
    TValidacaoCampo.CampoPreenchido(cdsDataCODIGO_CST);
  end;
end;

procedure TCadCSOSN.AoCriarFormulario;
begin
  inherited;
  FEmpresaOptanteDoSimples := TFilial.OptanteSimplesNacional(TSistema.Sistema.filial.FId);
  ExibirCSOSN;
end;

procedure TCadCSOSN.ExibirCSOSN;
begin
  lbCSOSN.Visible := FEmpresaOptanteDoSimples;
  EdtCSOSN.Visible := FEmpresaOptanteDoSimples;
  lbCST.Visible := not FEmpresaOptanteDoSimples;
  EdtCST.Visible := not FEmpresaOptanteDoSimples;
end;

initialization
  RegisterClass(TCadCSOSN);

finalization
  UnRegisterClass(TCadCSOSN);

end.
