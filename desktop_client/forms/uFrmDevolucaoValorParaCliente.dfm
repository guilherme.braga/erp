inherited FrmDevolucaoValorParaCliente: TFrmDevolucaoValorParaCliente
  Caption = 'Estorno Financeiro'
  ClientWidth = 824
  ExplicitWidth = 830
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 824
    Width = 824
    object PnTotais: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 31
      Width = 820
      object PnValorPendente: TgbPanel
        Left = 565
        Top = 2
        Align = alRight
        Alignment = alRightCenter
        Caption = 'Valor Pendente R$ 999.999.999,00'
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clGreen
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Transparent = True
        Height = 27
        Width = 253
      end
      object PnTotalDevolucao: TgbPanel
        Left = 2
        Top = 2
        Align = alLeft
        Alignment = alLeftCenter
        Caption = 'Total da Devolu'#231#227'o R$ 999.999.999,00'
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clGreen
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        Height = 27
        Width = 279
      end
    end
    object PnCreditarValorCliente: TgbPanel
      Left = 2
      Top = 65
      Align = alTop
      Alignment = alCenterCenter
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      Style.TransparentBorder = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Transparent = True
      Height = 32
      Width = 820
      object gbLabel1: TgbLabel
        Left = 2
        Top = 2
        Align = alLeft
        Caption = 'Creditar valor para utiliza'#231#227'o futura'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        AnchorY = 16
      end
      object EdtCreditarPessoa: TcxCalcEdit
        Left = 215
        Top = 4
        EditValue = 0.000000000000000000
        Properties.DisplayFormat = '###,###,###,##0.00'
        Properties.OnEditValueChanged = EdtCreditarPessoaPropertiesEditValueChanged
        TabOrder = 0
        Width = 178
      end
    end
    object PnDevolverPagamentoEmDinheiro: TgbPanel
      Left = 2
      Top = 33
      Align = alTop
      Alignment = alCenterCenter
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 32
      Width = 820
      object gbLabel2: TgbLabel
        Left = 2
        Top = 2
        Align = alLeft
        Caption = 'Devolver pagamento em dinheiro'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        AnchorY = 16
      end
      object EdtDevolverPagamento: TcxCalcEdit
        Left = 215
        Top = 4
        EditValue = 0.000000000000000000
        Properties.DisplayFormat = '###,###,###,##0.00'
        Properties.OnEditValueChanged = EdtDevolverPagamentoPropertiesEditValueChanged
        TabOrder = 0
        Width = 178
      end
    end
    object PnContasReceber: TgbPanel
      Left = 2
      Top = 97
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      Transparent = True
      Height = 170
      Width = 820
      object gbLabel3: TgbLabel
        Left = 2
        Top = 2
        Align = alTop
        Caption = 'Debitar do contas a receber'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        Transparent = True
      end
      object gridContaReceber: TcxGrid
        Left = 2
        Top = 22
        Width = 816
        Height = 146
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 1
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object viewContaReceber: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          DataController.DataSource = dsContaReceber
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.ApplyChanges = fracImmediately
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.ShowColumnFilterButtons = sfbAlways
          OptionsView.BandHeaders = False
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
            end>
        end
        object gridContaReceberLevel1: TcxGridLevel
          GridView = viewContaReceber
        end
      end
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    ExplicitWidth = 824
    Width = 824
    inherited BtnConfirmar: TcxButton
      Left = 672
      ExplicitLeft = 672
    end
    inherited BtnCancelar: TcxButton
      Left = 747
      ExplicitLeft = 747
    end
  end
  object dsContaReceber: TDataSource
    DataSet = fdmContaReceber
    Left = 264
    Top = 160
  end
  object fdmContaReceber: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 192
    Top = 160
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 628
    Top = 40
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 524
    Top = 56
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object RestaurarColunasPadrao: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
  object pmGridConsultaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 696
    Top = 80
    object AlterarSQL1: TMenuItem
      Action = ActAlterarSQLPesquisaPadrao
    end
  end
  object cxGridPopupMenuPadrao: TcxGridPopupMenu
    Grid = gridContaReceber
    PopupMenus = <
      item
        GridView = viewContaReceber
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = viewContaReceber
        HitTypes = [gvhtNone]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
    Left = 420
    Top = 24
  end
end
