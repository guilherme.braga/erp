unit uFrmDevolucaoValorParaCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxLabel, uGBLabel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalc, cxDBEdit, uGBDBCalcEdit, uGBPanel, System.Actions, Vcl.ActnList,
  Vcl.StdCtrls, cxButtons, cxGroupBox, uVendaProxy, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter;

type
  TFrmDevolucaoValorParaCliente = class(TFrmModalPadrao)
    PnTotais: TgbPanel;
    PnValorPendente: TgbPanel;
    PnTotalDevolucao: TgbPanel;
    PnCreditarValorCliente: TgbPanel;
    gbLabel1: TgbLabel;
    PnDevolverPagamentoEmDinheiro: TgbPanel;
    gbLabel2: TgbLabel;
    PnContasReceber: TgbPanel;
    gbLabel3: TgbLabel;
    EdtDevolverPagamento: TcxCalcEdit;
    EdtCreditarPessoa: TcxCalcEdit;
    dsContaReceber: TDataSource;
    fdmContaReceber: TgbFDMemTable;
    ActionListAssistent: TActionList;
    ActSalvarConfiguracoes: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActAlterarColunasGrid: TAction;
    ActAlterarSQLPesquisaPadrao: TAction;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    RestaurarColunasPadrao: TMenuItem;
    SalvarConfiguraes1: TMenuItem;
    pmGridConsultaPadrao: TPopupMenu;
    AlterarSQL1: TMenuItem;
    cxGridPopupMenuPadrao: TcxGridPopupMenu;
    gridContaReceber: TcxGrid;
    viewContaReceber: TcxGridDBBandedTableView;
    gridContaReceberLevel1: TcxGridLevel;
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
    procedure EdtDevolverPagamentoPropertiesEditValueChanged(Sender: TObject);
    procedure EdtCreditarPessoaPropertiesEditValueChanged(Sender: TObject);
    procedure TotalizarRegistrosContaReceberSelecionados(Sender: TField);
  private
    FTotalContaReceberSelecionados: Double;
    FClienteConsumidorFinal: Boolean;
    FVendaProxy: TVendaProxy;
    FValorDevolucao: Double;
    FValorPendente: Double;
    procedure GerarDevolucaoEmDinheiro(const AValor: Double);
    procedure GerarCreditoCliente(const AValor: Double);
    procedure DebitarDoContasReceber;
    procedure AbrirConsultaTitulosContaReceber;
    procedure CalcularValorPendente;
    procedure CalcularTotalDevolucao;
    procedure HabilitarDevolucaoEmDinheiro(const AIdPessoa: Integer);
    procedure HabilitarCreditoCliente(const AIdPessoa: Integer);
    procedure HabilitarDebitarContasReceber(const AIdPessoa: Integer);
    procedure ValidarVendaProxy(const AVendaProxy: TVendaProxy);
    procedure InformarSeClienteConsumidorFinal(const AIdPessoa: Integer);
    function ExisteContaReceberSelecionado: Boolean;
    procedure LiberarAcaoConfirmar;
    procedure ConfigurarMetodoOnChangeBoChecked;
    procedure SugerirValorDevolucao;
  public
    class function EstornarFinanceiroVenda(const AVendaProxy: TVendaProxy;
      const AValorDevolucao: Double): boolean;
  protected
    procedure Confirmar; override;
  end;

var
  FrmDevolucaoValorParaCliente: TFrmDevolucaoValorParaCliente;

implementation

{$R *.dfm}

uses uVenda, uFrmMessage, uTPessoa, uContaReceber, uDatasetUtils, uTControl_Function, uDmConnection, uSistema,
  Data.FireDACJSONReflect, uDevExpressUtils, uUsuarioDesignControl, uTUsuario, uFrmAlterarSQLPesquisaPadrao,
  uFrmApelidarColunasGrid, uFiltroFormulario, uContaReceberProxy, uConstantes;

{ TFrmModalPadrao1 }

procedure TFrmDevolucaoValorParaCliente.AbrirConsultaTitulosContaReceber;
var
  DSList: TFDJSONDataSets;
  where: TFiltroPadrao;
begin
  try
    where := TFiltroPadrao.Create;
    try
      where.AddIgual(TContaReceberMovimento.FIELD_PESSOA, FVendaProxy.FIdPessoa);

      DSList := DmConnection.ServerMethodsClient.GetDadosComInjecaoCampos(Self.Name,
        where.where, TSistema.Sistema.Usuario.idSeguranca, TDBConstantes.FIELD_BO_UNCHECKED);
    finally
      FreeAndNil(where);
    end;
  except
    on e: Exception do
    begin
      TFrmMessage.Failure('A consulta de contas a receber n�o est� configurada corretamente.');
      Exit;
    end;
  end;

  try
    TDatasetUtils.JSONDatasetToMemTable(DSList, fdmContaReceber);

    TcxGridUtils.AdicionarTodosCamposNaView(viewContaReceber);

    TUsuarioGridView.LoadGridView(viewContaReceber,
      TSistema.Sistema.Usuario.idSeguranca, Self.Name);
  finally
    FreeAndNil(DSList);
  end;

  TcxGridUtils.ConfigurarColunasParaSomenteLeitura(viewContaReceber);
  TcxGridUtils.ConfigurarColunaSelecao(viewContaReceber.GetColumnByFieldName(TDBConstantes.ALIAS_BO_CHECKED));
end;

procedure TFrmDevolucaoValorParaCliente.ActAlterarColunasGridExecute(Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(viewContaReceber);
end;

procedure TFrmDevolucaoValorParaCliente.ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name) then
    AbrirConsultaTitulosContaReceber;
end;

procedure TFrmDevolucaoValorParaCliente.ActRestaurarColunasPadraoExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, Self.Name);
    viewContaReceber.RestoreDefaults;
end;

procedure TFrmDevolucaoValorParaCliente.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(viewContaReceber,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TFrmDevolucaoValorParaCliente.CalcularTotalDevolucao;
begin
  PnTotalDevolucao.Caption := 'Total da Devolu��o R$ '+FormatFloat('###,###,###,##0.00', FValorDevolucao);
end;

procedure TFrmDevolucaoValorParaCliente.CalcularValorPendente;
begin
  FValorPendente := FValorDevolucao - EdtDevolverPagamento.Value - EdtCreditarPessoa.Value -
    FTotalContaReceberSelecionados;

  if FValorPendente < 0 then
  begin
    FValorPendente := 0;
  end;

  PnValorPendente.Caption := 'Valor Pendente R$ '+FormatFloat('###,###,###,##0.00', FValorPendente);
  LiberarAcaoConfirmar;
end;

procedure TFrmDevolucaoValorParaCliente.ConfigurarMetodoOnChangeBoChecked;
begin
  if not fdmContaReceber.Active then
  begin
    exit;
  end;

  fdmContaReceber.FieldByName(TDBConstantes.ALIAS_BO_CHECKED).OnChange :=
    TotalizarRegistrosContaReceberSelecionados;
end;

procedure TFrmDevolucaoValorParaCliente.Confirmar;
begin
  if EdtDevolverPagamento.Value > 0 then
  begin
    GerarDevolucaoEmDinheiro(EdtDevolverPagamento.Value);
  end;

  if EdtCreditarPessoa.Value > 0 then
  begin
    GerarCreditoCliente(EdtCreditarPessoa.Value);
  end;

  if ExisteContaReceberSelecionado then
  begin
    DebitarDoContasReceber;
  end;
  inherited;
end;

procedure TFrmDevolucaoValorParaCliente.DebitarDoContasReceber;
var
  codigosContaReceberSelecionados: String;
  valorAbatimento: Double;
begin
  if not fdmContaReceber.Active then
  begin
    exit;
  end;

  codigosContaReceberSelecionados := TDatasetUtils.ConcatenarComCondicao(
    fdmContaReceber.FieldByName('ID'),
    fdmContaReceber.FieldByName(TDBConstantes.ALIAS_BO_CHECKED),
    TConstantes.BO_SIM);


  valorAbatimento := FValorDevolucao - EdtCreditarPessoa.Value - EdtDevolverPagamento.Value;

  if valorAbatimento > 0 then
  begin
    TVenda.GerarDevolucaoCreditandoNoContasAReceber(FVendaProxy.FIdPessoa, valorAbatimento,
      codigosContaReceberSelecionados);
  end;
end;

procedure TFrmDevolucaoValorParaCliente.EdtCreditarPessoaPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if EdtCreditarPessoa.Value < 0 then
  begin
    EdtCreditarPessoa.Value := 0;
  end;

  if (EdtCreditarPessoa.Value + EdtDevolverPagamento.Value + FTotalContaReceberSelecionados) > FValorDevolucao then
  begin
    EdtCreditarPessoa.Value := FValorDevolucao - FTotalContaReceberSelecionados - EdtDevolverPagamento.Value;
    viewContaReceber.Controller.ClearSelection;
  end;

  CalcularValorPendente;
end;

procedure TFrmDevolucaoValorParaCliente.EdtDevolverPagamentoPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if EdtDevolverPagamento.Value < 0 then
  begin
    EdtDevolverPagamento.Value := 0;
  end;

  if (EdtDevolverPagamento.Value + EdtCreditarPessoa.Value + FTotalContaReceberSelecionados) > FValorDevolucao then
  begin
    EdtDevolverPagamento.Value := FValorDevolucao - FTotalContaReceberSelecionados - EdtCreditarPessoa.Value;
    viewContaReceber.Controller.ClearSelection;
  end;

  CalcularValorPendente;
end;

class function TFrmDevolucaoValorParaCliente.EstornarFinanceiroVenda(const AVendaProxy: TVendaProxy;
  const AValorDevolucao: Double): Boolean;
begin
  Application.CreateForm(TFrmDevolucaoValorParaCliente, FrmDevolucaoValorParaCliente);
  try
    FrmDevolucaoValorParaCliente.FTotalContaReceberSelecionados := 0;
    FrmDevolucaoValorParaCliente.FVendaProxy := AVendaProxy;
    FrmDevolucaoValorParaCliente.FValorDevolucao := AValorDevolucao;
    FrmDevolucaoValorParaCliente.ValidarVendaProxy(AVendaProxy);

    FrmDevolucaoValorParaCliente.CalcularTotalDevolucao;
    FrmDevolucaoValorParaCliente.CalcularValorPendente;

    FrmDevolucaoValorParaCliente.AbrirConsultaTitulosContaReceber;

    FrmDevolucaoValorParaCliente.InformarSeClienteConsumidorFinal(
      FrmDevolucaoValorParaCliente.FVendaProxy.FIdPessoa);

    FrmDevolucaoValorParaCliente.HabilitarCreditoCliente(
      FrmDevolucaoValorParaCliente.FVendaProxy.FIdPessoa);
    FrmDevolucaoValorParaCliente.HabilitarDebitarContasReceber(
      FrmDevolucaoValorParaCliente.FVendaProxy.FIdPessoa);
    FrmDevolucaoValorParaCliente.HabilitarDevolucaoEmDinheiro(
      FrmDevolucaoValorParaCliente.FVendaProxy.FIdPessoa);

    FrmDevolucaoValorParaCliente.SugerirValorDevolucao;

    FrmDevolucaoValorParaCliente.ConfigurarMetodoOnChangeBoChecked;

    FrmDevolucaoValorParaCliente.ShowModal;
    result := FrmDevolucaoValorParaCliente.result = MCONFIRMED;
  finally
    FreeAndNil(FrmDevolucaoValorParaCliente);
  end;
end;

function TFrmDevolucaoValorParaCliente.ExisteContaReceberSelecionado: Boolean;
begin
  result := fdmCOntaReceber.Localizar(TDBConstantes.ALIAS_BO_CHECKED, TConstantes.BO_SIM);
end;

procedure TFrmDevolucaoValorParaCliente.GerarCreditoCliente(const AValor: Double);
begin
  TVenda.GerarDevolucaoEmCreditoParaCliente(FVendaProxy.FIdChaveProcesso, AValor);
end;

procedure TFrmDevolucaoValorParaCliente.GerarDevolucaoEmDinheiro(const AValor: Double);
begin
  TVenda.GerarDevolucaoEmDinheiroParaCliente(FVendaProxy, AValor);
end;

procedure TFrmDevolucaoValorParaCliente.HabilitarCreditoCliente(const AIdPessoa: Integer);
begin
  PnCreditarValorCliente.Visible := (not FClienteConsumidorFinal);
end;

procedure TFrmDevolucaoValorParaCliente.HabilitarDebitarContasReceber(const AIdPessoa: Integer);
begin
  PnContasReceber.Visible := (not FClienteConsumidorFinal) and
    TContaReceber.PessoaPossuiContaReceberEmAberto(AIdPessoa);
end;

procedure TFrmDevolucaoValorParaCliente.HabilitarDevolucaoEmDinheiro(const AIdPessoa: Integer);
begin
  PnDevolverPagamentoEmDinheiro.Visible := true;
end;

procedure TFrmDevolucaoValorParaCliente.InformarSeClienteConsumidorFinal(const AIdPessoa: Integer);
begin
  FClienteConsumidorFinal := TPessoa.VerificarConsumidorFinal(AIdPessoa);
end;

procedure TFrmDevolucaoValorParaCliente.LiberarAcaoConfirmar;
begin
  ActConfirmar.Enabled := FValorPendente <= 0;
end;

procedure TFrmDevolucaoValorParaCliente.SugerirValorDevolucao;
begin
  if (not PnContasReceber.Visible) and (not PnCreditarValorCliente.Visible) then
  begin
    EdtDevolverPagamento.EditValue := FValorDevolucao;
    EdtDevolverPagamento.Enabled := false;
    EdtDevolverPagamento.Style.Color := $00DEDEFA;
  end;
end;

procedure TFrmDevolucaoValorParaCliente.TotalizarRegistrosContaReceberSelecionados(Sender: TField);
begin
  if not fdmContaReceber.Active then
  begin
    exit;
  end;

  if not Assigned(fdmContaReceber.FindField('VL_ABERTO')) then
  begin
    FTotalContaReceberSelecionados := 0;
    TFrmMessage.Information('A consulta est� desconfigurada. N�o foi encontrado o campo VL_ABERTO.');
    Exit;
  end;

  FTotalContaReceberSelecionados := TDatasetUtils.SomarColunaComCondicao(fdmContaReceber.FieldByName('VL_ABERTO'),
    fdmContaReceber.FieldByName(TDBConstantes.ALIAS_BO_CHECKED), TConstantes.BO_SIM);

  CalcularValorPendente;
end;

procedure TFrmDevolucaoValorParaCliente.ValidarVendaProxy(const AVendaProxy: TVendaProxy);
begin
  if AVendaProxy.FIdChaveProcesso <= 0 then
  begin
    TFrmMessage.Information('O c�digo da chave de processo n�o foi parametrizado no proxy de comunica��o.');
    Abort;
  end;

  if AVendaProxy.FIdPessoa <= 0 then
  begin
    TFrmMessage.Information('O c�digo da pessoa n�o foi parametrizado no proxy de comunica��o.');
    Abort;
  end;

  if AVendaProxy.FIdFormaPagamentoDinheiro <= 0 then
  begin
    TFrmMessage.Information('O c�digo da forma de pagamento n�o foi parametrizado no proxy de comunica��o.');
    Abort;
  end;

  if AVendaProxy.FIdContaCorrente <= 0 then
  begin
    TFrmMessage.Information('O c�digo da conta corrente n�o foi parametrizado no proxy de comunica��o.');
    Abort;
  end;

  if AVendaProxy.FIdCarteira <= 0 then
  begin
    TFrmMessage.Information('O c�digo da carteira n�o foi parametrizado no proxy de comunica��o.');
    Abort;
  end;
end;

end.
