inherited CadRelatorioFR: TCadRelatorioFR
  Caption = 'Cadastro de Relat'#243'rio'
  ExplicitWidth = 952
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object cxDBLabel1: TcxDBLabel
            Left = 132
            Top = 7
            AutoSize = True
            DataBinding.DataField = 'CC_ARQUIVO_CARREGADO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Transparent = True
            AnchorY = 16
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            DesignSize = (
              932
              51)
            Height = 51
            Width = 932
            object Label7: TLabel
              Left = 5
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Nome'
            end
            object Label3: TLabel
              Left = 5
              Top = 30
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object Label2: TLabel
              Left = 521
              Top = 4
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object EdtNomeRelatorio: TgbDBTextEdit
              Left = 65
              Top = 0
              DataBinding.DataField = 'NOME'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 452
            end
            object gbDBBlobEdit1: TgbDBBlobEdit
              Left = 65
              Top = 26
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACOES'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Properties.PopupHeight = 300
              Properties.PopupWidth = 160
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbRequired = True
              Width = 856
            end
            object EdtDescricaoRelatorio: TgbDBTextEdit
              Left = 570
              Top = 0
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              gbPassword = False
              Width = 351
            end
          end
          object PnFrameRelatorioFRFormulario: TgbPanel
            Left = 2
            Top = 85
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameRelatorioFRFormulario'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Height = 246
            Width = 932
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 121
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbAdicionarArquivoFR3'
        end
        item
          Visible = True
          ItemName = 'lbRemoverArquivoFR3'
        end>
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 678
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 586
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object dxBarStatic1: TdxBarStatic [9]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object lbAdicionarArquivoFR3: TdxBarLargeButton [17]
      Action = ActUploadRelatorio
      Category = 2
    end
    object lbRemoverArquivoFR3: TdxBarLargeButton [18]
      Action = ActRemoverRelatorio
      Category = 2
    end
  end
  inherited ActionListMain: TActionList
    object ActUploadRelatorio: TAction [12]
      Category = 'Action'
      Caption = 'Carregar Arquivo F9'
      Hint = 'Busca o arquivo de relat'#243'rio FR3 para ser salvo'
      ImageIndex = 149
      ShortCut = 120
      OnExecute = ActUploadRelatorioExecute
    end
    object ActRemoverRelatorio: TAction [13]
      Category = 'Action'
      Caption = 'Excluir Arquivo F10'
      Hint = 'Remove o arquivo de relat'#243'rio (FR3)'
      ImageIndex = 150
      ShortCut = 121
      OnExecute = ActRemoverRelatorioExecute
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Relat'#243'rio'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspRelatorioFR'
    RemoteServer = DmConnection.dspRelatorioFR
    OnCalcFields = cdsDataCalcFields
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataARQUIVO: TBlobField
      DisplayLabel = 'Arquivo'
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
      Required = True
    end
    object cdsDataOBSERVACOES: TBlobField
      DisplayLabel = 'Observa'#231#245'es'
      FieldName = 'OBSERVACOES'
      Origin = 'OBSERVACOES'
    end
    object cdsDatafdqRelatorioFRFormulario: TDataSetField
      FieldName = 'fdqRelatorioFRFormulario'
    end
    object cdsDataCC_ARQUIVO_CARREGADO: TStringField
      FieldKind = fkCalculated
      FieldName = 'CC_ARQUIVO_CARREGADO'
      Calculated = True
    end
    object cdsDataTIPO_ARQUIVO: TStringField
      DefaultExpression = 'FR3'
      DisplayLabel = 'Tipo de Arquivo'
      FieldName = 'TIPO_ARQUIVO'
      Size = 25
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42220.009619745370000000
      BuiltInReportLink = True
    end
  end
  object cdsRelatorioFRFormulario: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqRelatorioFRFormulario
    Params = <>
    AfterOpen = cdsRelatorioFRFormularioAfterOpen
    OnNewRecord = cdsRelatorioFRFormularioNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 720
    Top = 88
    object cdsRelatorioFRFormularioID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsRelatorioFRFormularioNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 255
    end
    object cdsRelatorioFRFormularioDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsRelatorioFRFormularioACAO: TStringField
      DefaultExpression = 'VISUALIZAR'
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ACAO'
      Origin = 'ACAO'
      Size = 30
    end
    object cdsRelatorioFRFormularioID_RELATORIO_FR: TIntegerField
      DisplayLabel = 'C'#243'digo do Relat'#243'rio'
      FieldName = 'ID_RELATORIO_FR'
      Origin = 'ID_RELATORIO_FR'
    end
    object cdsRelatorioFRFormularioID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsRelatorioFRFormularioJOIN_USUARIO_PESSOA_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO_PESSOA_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object cdsRelatorioFRFormularioIMPRESSORA: TStringField
      DisplayLabel = 'Impressora'
      FieldName = 'IMPRESSORA'
      Origin = 'IMPRESSORA'
      Size = 255
    end
    object cdsRelatorioFRFormularioNUMERO_COPIAS: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'C'#243'pias'
      FieldName = 'NUMERO_COPIAS'
      Origin = 'NUMERO_COPIAS'
    end
    object cdsRelatorioFRFormularioFILTROS_PERSONALIZADOS: TBlobField
      DisplayLabel = 'Filtros Personalizados'
      FieldName = 'FILTROS_PERSONALIZADOS'
      Origin = 'FILTROS_PERSONALIZADOS'
    end
  end
  object odArquivo: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Relat'#243'rio | FR3'
        FileMask = '*.fr3'
      end
      item
        DisplayName = 'Impress'#227'o | prn'
        FileMask = '*.prn'
      end>
    FileTypeIndex = 2
    Options = []
    Left = 786
    Top = 77
  end
end
