unit uRelContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxButtonEdit, cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, uGBClientDataset, dxBar, cxBarEditItem, dxBarExtItems,
  System.Actions, Vcl.ActnList, cxVGrid, cxInplaceContainer, uGBPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxGroupBox, dxRibbon;

type
  TRelContaReceber = class(TFrmRelatorioFRPadrao)
    FdmFiltrosID: TIntegerField;
    FdmFiltrosDOCUMENTO: TStringField;
    FdmFiltrosDESCRICAO: TStringField;
    FdmFiltrosDT_COMPETENCIA_INICIO: TDateField;
    FdmFiltrosDT_COMPETENCIA_TERMINO: TDateField;
    FdmFiltrosDT_VENCIMENTO_INICIO: TDateField;
    FdmFiltrosDT_VENCIMENTO_TERMINO: TDateField;
    FdmFiltrosVL_TITULO: TFMTBCDField;
    FdmFiltrosID_CONTA_ANALISE: TLargeintField;
    FdmFiltrosID_CENTRO_RESULTADO: TLargeintField;
    rowCentroResultado: TcxEditorRow;
    rowContaAnalise: TcxEditorRow;
    FdmFiltrosSTATUS: TStringField;
    rowStatus: TcxEditorRow;
    rowPessoa: TcxEditorRow;
    FdmFiltrosID_PESSOA: TIntegerField;
    procedure rowCentroResultadoEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowContaAnaliseEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowPessoaEditPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure SetWhereBundle; override;
  end;

var
  RelContaReceber: TRelContaReceber;

implementation

{$R *.dfm}

uses uFrmConsultaPadrao, uContaCorrenteProxy, uContaAnaliseProxy,
  uCentroResultadoProxy, uContaReceberProxy, uPesqContaAnalise;

procedure TRelContaReceber.rowCentroResultadoEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TCentroResultadoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowCentroResultado.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaReceber.rowContaAnaliseEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TPesqContaAnalise.IDSelecionado(
    StrtoIntDef(VartoStr(rowCentroResultado.Properties.Value),0));

  if IDConsultado > 0 then
  begin
    rowContaAnalise.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaReceber.rowPessoaEditPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID('PESSOA');

  if IDConsultado > 0 then
  begin
    rowPessoa.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaReceber.SetWhereBundle;
begin
  inherited;
  AddFiltroIgual(fdmFiltrosID.Origin, fdmFiltrosID);
  AddFiltroIgual(FdmFiltrosDOCUMENTO.Origin, FdmFiltrosDOCUMENTO);
  AddFiltroIgual(fdmFiltrosDESCRICAO.Origin, fdmFiltrosDESCRICAO);

  AddFiltroEntre(fdmFiltrosDT_VENCIMENTO_INICIO.Origin,
    fdmFiltrosDT_VENCIMENTO_INICIO, FdmFiltrosDT_VENCIMENTO_TERMINO);

  AddFiltroEntre(FdmFiltrosDT_COMPETENCIA_INICIO.Origin, FdmFiltrosDT_COMPETENCIA_INICIO,
    FdmFiltrosDT_COMPETENCIA_TERMINO);

  AddFiltroIgual(FdmFiltrosID_CONTA_ANALISE.Origin, FdmFiltrosID_CONTA_ANALISE);
  AddFiltroIgual(FdmFiltrosID_PESSOA.Origin, FdmFiltrosID_PESSOA);
  AddFiltroIgual(FdmFiltrosID_CENTRO_RESULTADO.Origin, FdmFiltrosID_CENTRO_RESULTADO);
  AddFiltroIgual(FdmFiltrosVL_TITULO.Origin, FdmFiltrosVL_TITULO);

  AddFiltroIgual(fdmFiltrosSTATUS.Origin, fdmFiltrosSTATUS);
end;

initialization
  RegisterClass(TRelContaReceber);

Finalization
  UnRegisterClass(TRelContaReceber);

end.
