inherited CadProduto: TCadProduto
  Caption = 'Cadastro de Produto'
  ClientHeight = 442
  ClientWidth = 907
  ExplicitTop = -87
  ExplicitWidth = 923
  ExplicitHeight = 481
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 907
    ExplicitWidth = 907
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 907
    Height = 315
    Properties.ActivePage = cxtsData
    ExplicitWidth = 907
    ExplicitHeight = 315
    ClientRectBottom = 315
    ClientRectRight = 907
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 907
      ExplicitHeight = 291
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 875
        Height = 291
        ExplicitWidth = 875
        ExplicitHeight = 291
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 291
        Height = 291
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 291
        ExplicitHeight = 291
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 907
      ExplicitHeight = 291
      inherited DesignPanel: TJvDesignPanel
        Width = 907
        Height = 291
        ExplicitWidth = 907
        ExplicitHeight = 291
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 907
          ExplicitHeight = 291
          Height = 291
          Width = 907
          inherited dxBevel1: TdxBevel
            Width = 903
            ExplicitWidth = 884
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label25: TLabel
            Left = 112
            Top = 9
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 47
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 843
            Top = 5
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            Width = 55
          end
          object pgProdutoDetalhe: TcxPageControl
            Left = 2
            Top = 34
            Width = 903
            Height = 255
            Align = alClient
            Focusable = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            Properties.ActivePage = tsProdutoGeral
            Properties.CustomButtons.Buttons = <>
            Properties.NavigatorPosition = npLeftTop
            Properties.Options = [pcoAlwaysShowGoDialogButton, pcoGradientClientArea, pcoRedrawOnResize]
            Properties.Style = 8
            OnChange = cxpcMainChange
            ClientRectBottom = 255
            ClientRectRight = 903
            ClientRectTop = 24
            object tsProdutoGeral: TcxTabSheet
              Caption = 'Geral'
              ImageIndex = 0
              object PnProdutos: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  903
                  160)
                Height = 160
                Width = 903
                object Label1: TLabel
                  Left = 8
                  Top = 137
                  Width = 17
                  Height = 13
                  Caption = 'Cor'
                end
                object Label10: TLabel
                  Left = 8
                  Top = 113
                  Width = 68
                  Height = 13
                  Caption = 'Sub Categoria'
                end
                object Label11: TLabel
                  Left = 474
                  Top = 113
                  Width = 25
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Linha'
                  ExplicitLeft = 564
                end
                object Label12: TLabel
                  Left = 474
                  Top = 38
                  Width = 34
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Modelo'
                  ExplicitLeft = 564
                end
                object Label13: TLabel
                  Left = 474
                  Top = 63
                  Width = 14
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'UN'
                  ExplicitLeft = 564
                end
                object Label14: TLabel
                  Left = 473
                  Top = 15
                  Width = 20
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Tipo'
                  ExplicitLeft = 563
                end
                object Label15: TLabel
                  Left = 657
                  Top = 15
                  Width = 77
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'C'#243'digo de Barra'
                  ExplicitLeft = 638
                end
                object Label2: TLabel
                  Left = 8
                  Top = 15
                  Width = 46
                  Height = 13
                  Caption = 'Descri'#231#227'o'
                end
                object Label4: TLabel
                  Left = 8
                  Top = 38
                  Width = 29
                  Height = 13
                  Caption = 'Grupo'
                end
                object Label5: TLabel
                  Left = 8
                  Top = 63
                  Width = 50
                  Height = 13
                  Caption = 'Sub Grupo'
                end
                object Label8: TLabel
                  Left = 474
                  Top = 88
                  Width = 29
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Marca'
                  ExplicitLeft = 564
                end
                object Label9: TLabel
                  Left = 8
                  Top = 88
                  Width = 47
                  Height = 13
                  Caption = 'Categoria'
                end
                object Label27: TLabel
                  Left = 474
                  Top = 137
                  Width = 44
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Tamanho'
                  ExplicitLeft = 564
                end
                object cbTipo: TcxDBComboBox
                  Left = 518
                  Top = 11
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'TIPO'
                  DataBinding.DataSource = dsData
                  Properties.DropDownListStyle = lsFixedList
                  Properties.Items.Strings = (
                    'PRODUTO'
                    'SERVI'#199'O'
                    'OUTROS')
                  TabOrder = 1
                  Width = 125
                end
                object descCategoria: TgbDBTextEdit
                  Left = 147
                  Top = 84
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_CATEGORIA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 12
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descGrupo: TgbDBTextEdit
                  Left = 147
                  Top = 34
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descLinha: TgbDBTextEdit
                  Left = 578
                  Top = 109
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_LINHA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 18
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descMarca: TgbDBTextEdit
                  Left = 578
                  Top = 84
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_MARCA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 14
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descModelo: TgbDBTextEdit
                  Left = 578
                  Top = 34
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_MODELO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descSubCategoria: TgbDBTextEdit
                  Left = 147
                  Top = 109
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_SUB_CATEGORIA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 16
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descSubGrupo: TgbDBTextEdit
                  Left = 147
                  Top = 59
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object descUnidadeEstoque: TgbDBTextEdit
                  Left = 578
                  Top = 59
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object edCodigoBarra: TgbDBTextEdit
                  Left = 742
                  Top = 11
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CODIGO_BARRA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbRequired = True
                  gbPassword = False
                  Width = 156
                end
                object edDescricao: TgbDBTextEdit
                  Left = 86
                  Top = 11
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'DESCRICAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbRequired = True
                  gbPassword = False
                  Width = 382
                end
                object eFkCategoria: TgbDBButtonEditFK
                  Left = 86
                  Top = 84
                  DataBinding.DataField = 'ID_CATEGORIA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 11
                  gbTextEdit = descCategoria
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_CATEGORIA;JOIN_DESCRICAO_CATEGORIA'
                  gbTableName = 'CATEGORIA'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadCategoriaProduto'
                  gbIdentificadorConsulta = 'CATEGORIA'
                  Width = 60
                end
                object eFkGrupo: TgbDBButtonEditFK
                  Left = 86
                  Top = 34
                  DataBinding.DataField = 'ID_GRUPO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 3
                  gbTextEdit = descGrupo
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_GRUPO_PRODUTO;JOIN_DESCRICAO_GRUPO_PRODUTO'
                  gbTableName = 'GRUPO_PRODUTO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadGrupoProduto'
                  gbIdentificadorConsulta = 'GRUPO_PRODUTO'
                  Width = 60
                end
                object eFkLinha: TgbDBButtonEditFK
                  Left = 518
                  Top = 109
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_LINHA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 17
                  gbTextEdit = descLinha
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_LINHA;JOIN_DESCRICAO_LINHA'
                  gbTableName = 'LINHA'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadLinhaProduto'
                  gbIdentificadorConsulta = 'LINHA'
                  Width = 59
                end
                object eFkMarca: TgbDBButtonEditFK
                  Left = 518
                  Top = 84
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_MARCA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 13
                  gbTextEdit = descMarca
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_MARCA;JOIN_DESCRICAO_MARCA'
                  gbTableName = 'MARCA'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadMarcaProduto'
                  gbIdentificadorConsulta = 'MARCA'
                  Width = 59
                end
                object eFkModelo: TgbDBButtonEditFK
                  Left = 518
                  Top = 34
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_MODELO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 5
                  gbTextEdit = descModelo
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_MODELO; JOIN_DESCRICAO_MODELO'
                  gbTableName = 'MODELO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadModeloProduto'
                  gbIdentificadorConsulta = 'MODELO'
                  Width = 59
                end
                object eFkSubCategoria: TgbDBButtonEditFK
                  Left = 86
                  Top = 109
                  DataBinding.DataField = 'ID_SUB_CATEGORIA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 15
                  gbTextEdit = descSubCategoria
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_SUB_CATEGORIA; JOIN_DESCRICAO_SUB_CATEGORIA'
                  gbTableName = 'SUB_CATEGORIA'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadSubCategoriaProduto'
                  gbIdentificadorConsulta = 'SUB_CATEGORIA'
                  Width = 60
                end
                object eFkSubGrupo: TgbDBButtonEditFK
                  Left = 86
                  Top = 59
                  DataBinding.DataField = 'ID_SUB_GRUPO_PRODUTO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 7
                  gbTextEdit = descSubGrupo
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_SUB_GRUPO_PRODUTO;JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
                  gbTableName = 'SUB_GRUPO_PRODUTO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbAntesDeConsultar = eFkSubGrupogbAntesDeConsultar
                  gbClasseDoCadastro = 'TCadSubGrupoProduto'
                  gbIdentificadorConsulta = 'SUB_GRUPO_PRODUTO'
                  Width = 60
                end
                object eFkUnidadeEstoque: TgbDBButtonEditFK
                  Left = 518
                  Top = 59
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_UNIDADE_ESTOQUE'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 9
                  gbTextEdit = descUnidadeEstoque
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_UNIDADE_ESTOQUE;JOIN_DESCRICAO_UNIDADE_ESTOQUE'
                  gbTableName = 'UNIDADE_ESTOQUE'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadUnidadeEstoqueProduto'
                  gbIdentificadorConsulta = 'UNIDADE_ESTOQUE'
                  Width = 59
                end
                object gbDBButtonEditFK1: TgbDBButtonEditFK
                  Left = 86
                  Top = 133
                  DataBinding.DataField = 'ID_COR'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 19
                  gbTextEdit = gbDBTextEdit1
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_COR;JOIN_DESCRICAO_COR'
                  gbTableName = 'COR'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'COR'
                  Width = 60
                end
                object gbDBTextEdit1: TgbDBTextEdit
                  Left = 147
                  Top = 133
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_COR'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 20
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object gbDBTextEdit5: TgbDBTextEdit
                  Left = 578
                  Top = 133
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_TAMANHO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 22
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 321
                end
                object gbDBButtonEditFK5: TgbDBButtonEditFK
                  Left = 518
                  Top = 133
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'ID_TAMANHO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 21
                  gbTextEdit = gbDBTextEdit5
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_TAMANHO;JOIN_DESCRICAO_TAMANHO'
                  gbTableName = 'TAMANHO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadTamanho'
                  gbIdentificadorConsulta = 'TAMANHO'
                  Width = 59
                end
              end
              object PnFormacaoPreco: TgbPanel
                Left = 0
                Top = 160
                Align = alLeft
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 71
                Width = 609
                object PnTituloFrameDetailPadrao: TgbPanel
                  Left = 2
                  Top = 2
                  Align = alTop
                  Alignment = alCenterCenter
                  Caption = 'Forma'#231#227'o de Pre'#231'o Inicial'
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  ParentBackground = False
                  ParentFont = False
                  Style.BorderStyle = ebsOffice11
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  Style.TextStyle = [fsBold]
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Transparent = True
                  Height = 19
                  Width = 605
                end
                object PnProdutoFilial: TgbPanel
                  Left = 2
                  Top = 21
                  Align = alTop
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Transparent = True
                  Height = 148
                  Width = 605
                  object Label18: TLabel
                    Left = 6
                    Top = 33
                    Width = 105
                    Height = 13
                    Caption = 'Vl. Despesa Acessoria'
                    FocusControl = cxDBTextEdit8
                  end
                  object Label19: TLabel
                    Left = 6
                    Top = 8
                    Width = 43
                    Height = 13
                    Caption = 'Vl. Custo'
                  end
                  object Label20: TLabel
                    Left = 312
                    Top = 33
                    Width = 41
                    Height = 13
                    Caption = 'Vl. Frete'
                    FocusControl = cxDBTextEdit3
                  end
                  object Label21: TLabel
                    Left = 6
                    Top = 58
                    Width = 49
                    Height = 13
                    Caption = 'Vl. Seguro'
                    FocusControl = cxDBTextEdit4
                  end
                  object Label22: TLabel
                    Left = 312
                    Top = 58
                    Width = 55
                    Height = 13
                    Caption = 'Vl. ICMS ST'
                    FocusControl = cxDBTextEdit5
                  end
                  object Label23: TLabel
                    Left = 6
                    Top = 83
                    Width = 29
                    Height = 13
                    Caption = 'Vl. IPI'
                    FocusControl = cxDBTextEdit6
                  end
                  object Label24: TLabel
                    Left = 312
                    Top = 8
                    Width = 60
                    Height = 13
                    Caption = 'Vl. Desconto'
                    FocusControl = cxDBTextEdit7
                  end
                  object Label26: TLabel
                    Left = 312
                    Top = 83
                    Width = 96
                    Height = 13
                    Caption = 'Vl. Custo + Imposto'
                  end
                  object Label28: TLabel
                    Left = 6
                    Top = 109
                    Width = 103
                    Height = 13
                    Caption = 'Vl. Custo Operacional'
                  end
                  object Label29: TLabel
                    Left = 312
                    Top = 109
                    Width = 68
                    Height = 13
                    Caption = 'Vl. Custo Final'
                  end
                  object cxDBTextEdit3: TGBDBValorComPercentual
                    Left = 411
                    Top = 29
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 3
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMO_FRETE'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMO_FRETE'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object cxDBTextEdit4: TGBDBValorComPercentual
                    Left = 113
                    Top = 54
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 4
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMO_SEGURO'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMO_SEGURO'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object cxDBTextEdit5: TGBDBValorComPercentual
                    Left = 411
                    Top = 54
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 5
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMO_ICMS_ST'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMO_ICMS_ST'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object cxDBTextEdit6: TGBDBValorComPercentual
                    Left = 113
                    Top = 79
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 6
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMO_IPI'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMO_IPI'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object cxDBTextEdit7: TGBDBValorComPercentual
                    Left = 411
                    Top = 4
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMO_DESCONTO'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMO_DESCONTO'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object cxDBTextEdit8: TGBDBValorComPercentual
                    Left = 113
                    Top = 29
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 2
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_ULTIMA_DESPESA_ACESSORIA'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_ULTIMA_DESPESA_ACESSORIA'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 193
                  end
                  object gbDBCalcEdit3: TgbDBCalcEdit
                    Left = 411
                    Top = 79
                    TabStop = False
                    DataBinding.DataField = 'VL_ULTIMO_CUSTO_IMPOSTO'
                    DataBinding.DataSource = dsFormacaoPreco
                    ParentFont = False
                    Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                    Properties.ImmediatePost = True
                    Properties.ReadOnly = True
                    Properties.UseThousandSeparator = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.Font.Charset = DEFAULT_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Tahoma'
                    Style.Font.Style = [fsBold]
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.IsFontAssigned = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 7
                    gbReadyOnly = True
                    gbRequired = True
                    Width = 181
                  end
                  object gbDBCalcEdit5: TgbDBCalcEdit
                    Left = 411
                    Top = 105
                    TabStop = False
                    DataBinding.DataField = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
                    DataBinding.DataSource = dsFormacaoPreco
                    ParentFont = False
                    Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                    Properties.ImmediatePost = True
                    Properties.ReadOnly = True
                    Properties.UseThousandSeparator = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.Font.Charset = DEFAULT_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Tahoma'
                    Style.Font.Style = [fsBold]
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.IsFontAssigned = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 9
                    gbReadyOnly = True
                    gbRequired = True
                    Width = 181
                  end
                  object gbDBCalcEdit2: TgbDBCalcEdit
                    Left = 113
                    Top = 4
                    DataBinding.DataField = 'VL_ULTIMO_CUSTO'
                    DataBinding.DataSource = dsFormacaoPreco
                    ParentFont = False
                    Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                    Properties.ImmediatePost = True
                    Properties.ReadOnly = False
                    Properties.UseThousandSeparator = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = 14606074
                    Style.Font.Charset = DEFAULT_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Tahoma'
                    Style.Font.Style = [fsBold]
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.IsFontAssigned = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    gbRequired = True
                    Width = 181
                  end
                  object GBDBValorComPercentual1: TGBDBValorComPercentual
                    Left = 113
                    Top = 105
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 8
                    Transparent = True
                    gbFieldValorBase = 'VL_ULTIMO_CUSTO'
                    gbDataSourceValorBase = dsFormacaoPreco
                    gbFieldValor = 'VL_CUSTO_OPERACIONAL'
                    gbDataSourceValor = dsFormacaoPreco
                    gbFieldPercentual = 'PERC_CUSTO_OPERACIONAL'
                    gbDataSourcePercentual = dsFormacaoPreco
                    Height = 21
                    Width = 194
                  end
                end
              end
              object PnTabelaPreco: TgbPanel
                Left = 609
                Top = 160
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 71
                Width = 294
                object gbPanel1: TgbPanel
                  Left = 2
                  Top = 2
                  Align = alTop
                  Alignment = alCenterCenter
                  Caption = 'Manuten'#231#227'o de Tabelas de Pre'#231'o'
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  ParentBackground = False
                  ParentFont = False
                  Style.BorderStyle = ebsOffice11
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  Style.TextStyle = [fsBold]
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Transparent = True
                  Height = 19
                  Width = 290
                end
                object gridTabelaPreco: TcxGrid
                  AlignWithMargins = True
                  Left = 5
                  Top = 24
                  Width = 284
                  Height = 42
                  Align = alClient
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  Images = DmAcesso.cxImage16x16
                  ParentFont = False
                  TabOrder = 1
                  LookAndFeel.Kind = lfOffice11
                  LookAndFeel.NativeStyle = False
                  object cxGridDBBandedTableView2: TcxGridDBBandedTableView
                    Navigator.Buttons.CustomButtons = <>
                    Navigator.Buttons.Images = DmAcesso.cxImage16x16
                    Navigator.Buttons.PriorPage.Visible = False
                    Navigator.Buttons.NextPage.Visible = False
                    Navigator.Buttons.Insert.Visible = False
                    Navigator.Buttons.Delete.Visible = False
                    Navigator.Buttons.Edit.Visible = False
                    Navigator.Buttons.Post.Visible = False
                    Navigator.Buttons.Cancel.Visible = False
                    Navigator.Buttons.Refresh.Visible = False
                    Navigator.Buttons.SaveBookmark.Visible = False
                    Navigator.Buttons.GotoBookmark.Visible = False
                    Navigator.Buttons.Filter.Visible = False
                    Navigator.InfoPanel.Visible = True
                    Navigator.Visible = True
                    DataController.DataSource = dsTabelaPrecoItem
                    DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    FilterRow.ApplyChanges = fracImmediately
                    Images = DmAcesso.cxImage16x16
                    OptionsBehavior.NavigatorHints = True
                    OptionsBehavior.ExpandMasterRowOnDblClick = False
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.BandMoving = False
                    OptionsCustomize.NestedBands = False
                    OptionsData.CancelOnExit = False
                    OptionsData.Deleting = False
                    OptionsData.DeletingConfirmation = False
                    OptionsData.Inserting = False
                    OptionsView.ColumnAutoWidth = True
                    OptionsView.GroupByBox = False
                    OptionsView.GroupRowStyle = grsOffice11
                    OptionsView.ShowColumnFilterButtons = sfbAlways
                    OptionsView.BandHeaders = False
                    Styles.ContentOdd = DmAcesso.OddColor
                    Bands = <
                      item
                      end>
                    object cxGridDBBandedTableView2ID_TABELA_PRECO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'ID_TABELA_PRECO'
                      Width = 47
                      Position.BandIndex = 0
                      Position.ColIndex = 0
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView2DESCRICAO_TABELA_PRECO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'DESCRICAO_TABELA_PRECO'
                      Width = 177
                      Position.BandIndex = 0
                      Position.ColIndex = 1
                      Position.RowIndex = 0
                    end
                    object Level1BandedTableView1VL_CUSTO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'VL_CUSTO'
                      PropertiesClassName = 'TcxCurrencyEditProperties'
                      Properties.AssignedValues.MinValue = True
                      Properties.MaxValue = 1E21
                      Options.Editing = False
                      Width = 51
                      Position.BandIndex = 0
                      Position.ColIndex = 2
                      Position.RowIndex = 0
                    end
                    object Level1BandedTableView1PERC_LUCRO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'PERC_LUCRO'
                      PropertiesClassName = 'TcxCurrencyEditProperties'
                      Properties.AssignedValues.MinValue = True
                      Properties.MaxValue = 1E21
                      Width = 48
                      Position.BandIndex = 0
                      Position.ColIndex = 3
                      Position.RowIndex = 0
                    end
                    object Level1BandedTableView1VL_VENDA: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'VL_VENDA'
                      PropertiesClassName = 'TcxCurrencyEditProperties'
                      Properties.AssignedValues.MinValue = True
                      Properties.MaxValue = 1E21
                      Width = 43
                      Position.BandIndex = 0
                      Position.ColIndex = 4
                      Position.RowIndex = 0
                    end
                  end
                  object cxGridLevel2: TcxGridLevel
                    GridView = cxGridDBBandedTableView2
                  end
                end
              end
            end
            object tsMovimentacaoProduto: TcxTabSheet
              Caption = 'Movimenta'#231#227'o'
              ImageIndex = 4
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameMovimentacaoProduto: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 231
                Width = 903
              end
            end
            object tsProdutoFilial: TcxTabSheet
              Caption = 'Estoque por Filial'
              ImageIndex = 2
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object cxGrid1: TcxGrid
                Left = 0
                Top = 0
                Width = 903
                Height = 231
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Arial'
                Font.Style = []
                Images = DmAcesso.cxImage16x16
                ParentFont = False
                TabOrder = 0
                LookAndFeel.Kind = lfOffice11
                LookAndFeel.NativeStyle = False
                object cxGridDBBandedTableView1: TcxGridDBBandedTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.Images = DmAcesso.cxImage16x16
                  Navigator.Buttons.PriorPage.Visible = False
                  Navigator.Buttons.NextPage.Visible = False
                  Navigator.Buttons.Insert.Visible = False
                  Navigator.Buttons.Delete.Visible = False
                  Navigator.Buttons.Edit.Visible = False
                  Navigator.Buttons.Post.Visible = False
                  Navigator.Buttons.Cancel.Visible = False
                  Navigator.Buttons.Refresh.Visible = False
                  Navigator.Buttons.SaveBookmark.Visible = False
                  Navigator.Buttons.GotoBookmark.Visible = False
                  Navigator.Buttons.Filter.Visible = False
                  Navigator.InfoPanel.Visible = True
                  Navigator.Visible = True
                  DataController.DataSource = dsProdutoFilial
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Images = DmAcesso.cxImage16x16
                  OptionsBehavior.NavigatorHints = True
                  OptionsBehavior.ExpandMasterRowOnDblClick = False
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.BandMoving = False
                  OptionsCustomize.NestedBands = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsView.ColumnAutoWidth = True
                  OptionsView.GroupByBox = False
                  OptionsView.GroupRowStyle = grsOffice11
                  OptionsView.ShowColumnFilterButtons = sfbAlways
                  OptionsView.BandHeaders = False
                  Styles.ContentOdd = DmAcesso.OddColor
                  Bands = <
                    item
                    end>
                  object cxGridDBBandedTableView1ID: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'ID'
                    Visible = False
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 0
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1QT_ESTOQUE: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'QT_ESTOQUE'
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 1
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1LOCALIZACAO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'LOCALIZACAO'
                    Visible = False
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 2
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1QT_ESTOQUE_MINIMO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'QT_ESTOQUE_MINIMO'
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 3
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1QT_ESTOQUE_MAXIMO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'QT_ESTOQUE_MAXIMO'
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 4
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1ID_PRODUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'ID_PRODUTO'
                    Visible = False
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 5
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1ID_FILIAL: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'ID_FILIAL'
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 6
                    Position.RowIndex = 0
                  end
                  object cxGridDBBandedTableView1JOIN_DESCRICAO_FILIAL: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'JOIN_DESCRICAO_FILIAL'
                    Visible = False
                    Options.Editing = False
                    Position.BandIndex = 0
                    Position.ColIndex = 7
                    Position.RowIndex = 0
                  end
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = cxGridDBBandedTableView1
                end
              end
            end
            object cxTabSheet1: TcxTabSheet
              Caption = 'Regime Fiscal'
              ImageIndex = 3
              DesignSize = (
                903
                231)
              object Label3: TLabel
                Left = 3
                Top = 96
                Width = 22
                Height = 13
                Caption = 'NCM'
              end
              object Label7: TLabel
                Left = 3
                Top = 120
                Width = 88
                Height = 13
                Caption = 'NCM Especializado'
              end
              object Label16: TLabel
                Left = 740
                Top = 120
                Width = 72
                Height = 13
                Anchors = [akTop, akRight]
                Caption = 'Exce'#231#227'o do IPI'
                ExplicitLeft = 761
              end
              object Label17: TLabel
                Left = 484
                Top = 96
                Width = 91
                Height = 13
                Caption = 'Regra de Impostos'
              end
              object gbDBRadioGroup1: TgbDBRadioGroup
                Left = 3
                Top = 3
                Caption = 'Forma de Arquisi'#231#227'o da Mercadoria'
                DataBinding.DataField = 'FORMA_AQUISICAO'
                DataBinding.DataSource = dsData
                Properties.Items = <
                  item
                    Caption = 'Produzida'
                    Value = 'PRODUZIDA'
                  end
                  item
                    Caption = 'Importada'
                    Value = 'IMPORTADA'
                  end
                  item
                    Caption = 'Adquirida de Terceiros'
                    Value = 'ADQUIRIDA'
                  end>
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 83
                Width = 182
              end
              object gbDBRadioGroup2: TgbDBRadioGroup
                Left = 188
                Top = 3
                Anchors = [akLeft, akTop, akRight]
                Caption = 'Origem da Mercadoria'
                DataBinding.DataField = 'ORIGEM_DA_MERCADORIA'
                DataBinding.DataSource = dsData
                Properties.Items = <
                  item
                    Caption = 'Nacional'
                    Value = '0'
                  end
                  item
                    Caption = 'Importada - Diretamente'
                    Value = '1'
                  end
                  item
                    Caption = 'Importada - Adquirida no mercado interno'
                    Value = '2'
                  end>
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Height = 83
                Width = 425
              end
              object gbDBTextEdit2: TgbDBTextEdit
                Left = 128
                Top = 92
                TabStop = False
                DataBinding.DataField = 'JOIN_DESCRICAO_NCM'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 5
                gbReadyOnly = True
                gbPassword = False
                Width = 350
              end
              object gbDBButtonEditFK2: TgbDBButtonEditFK
                Left = 29
                Top = 92
                DataBinding.DataField = 'JOIN_CODIGO_NCM'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClearKey = 16430
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 4
                gbTextEdit = gbDBTextEdit2
                gbCampoPK = 'CODIGO'
                gbCamposRetorno = 'ID_NCM;JOIN_CODIGO_NCM;JOIN_DESCRICAO_NCM'
                gbTableName = 'NCM'
                gbCamposConsulta = 'ID;CODIGO;DESCRICAO'
                gbIdentificadorConsulta = 'NCM'
                Width = 102
              end
              object gbDBTextEdit3: TgbDBTextEdit
                Left = 152
                Top = 116
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 9
                gbReadyOnly = True
                gbPassword = False
                Width = 583
              end
              object gbDBButtonEditFK3: TgbDBButtonEditFK
                Left = 95
                Top = 116
                DataBinding.DataField = 'ID_NCM_ESPECIALIZADO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClearKey = 16430
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 8
                gbTextEdit = gbDBTextEdit3
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_NCM_ESPECIALIZADO;JOIN_DESCRICAO_NCM_ESPECIALIZADO'
                gbTableName = 'NCM_ESPECIALIZADO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'NCM_ESPECIALIZADO'
                Width = 60
              end
              object gbDBCalcEdit1: TgbDBCalcEdit
                Left = 820
                Top = 116
                Anchors = [akTop, akRight]
                DataBinding.DataField = 'EXCECAO_IPI'
                DataBinding.DataSource = dsData
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 10
                Width = 78
              end
              object gbDBRadioGroup3: TgbDBRadioGroup
                Left = 615
                Top = 3
                Anchors = [akTop, akRight]
                Caption = 'Tipo de Tributa'#231#227'o Pis Cofins'
                DataBinding.DataField = 'TIPO_TRIBUTACAO_PIS_COFINS_ST'
                DataBinding.DataSource = dsData
                Properties.Items = <
                  item
                    Caption = 'Sem cobran'#231'a'
                    Value = '0'
                  end
                  item
                    Caption = 'Por Al'#237'quota'
                    Value = '1'
                  end
                  item
                    Caption = 'Por valor'
                    Value = '2'
                  end>
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Height = 83
                Width = 155
              end
              object gbDBRadioGroup4: TgbDBRadioGroup
                Left = 776
                Top = 3
                Anchors = [akTop, akRight]
                Caption = 'Tipo de Tributa'#231#227'o IPI'
                DataBinding.DataField = 'TIPO_TRIBUTACAO_IPI'
                DataBinding.DataSource = dsData
                Properties.Items = <
                  item
                    Caption = 'Sem cobran'#231'a'
                    Value = '0'
                  end
                  item
                    Caption = 'Por Al'#237'quota'
                    Value = '1'
                  end
                  item
                    Caption = 'Por valor'
                    Value = '2'
                  end>
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Height = 83
                Width = 122
              end
              object EdtRegraImposto: TgbDBButtonEditFK
                Left = 579
                Top = 92
                DataBinding.DataField = 'ID_REGRA_IMPOSTO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClearKey = 16430
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 6
                gbTextEdit = gbDBTextEdit4
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_REGRA_IMPOSTO;JOIN_DESCRICAO_REGRA_IMPOSTO'
                gbTableName = 'REGRA_IMPOSTO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbAntesDeConsultar = EdtRegraImpostogbAntesDeConsultar
                gbIdentificadorConsulta = 'REGRA_IMPOSTO'
                Width = 78
              end
              object gbDBTextEdit4: TgbDBTextEdit
                Left = 654
                Top = 92
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_REGRA_IMPOSTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 7
                gbReadyOnly = True
                gbPassword = False
                Width = 244
              end
            end
            object tsProdutoFornecedor: TcxTabSheet
              Caption = 'Refer'#234'ncias nos Fornecedores'
              ImageIndex = 1
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnReferenciaFornecedor: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 231
                Width = 903
              end
            end
            object tsMontadora: TcxTabSheet
              Caption = 'Montadora'
              ImageIndex = 5
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnFrameProdutoMontadora: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 231
                Width = 903
              end
            end
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 189
            Top = 5
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 144
      FloatClientHeight = 285
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbDuplicarRegistro'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbGerarProdutoGrade'
        end>
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 398
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      Caption = 'Impress'#227'o'
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 591
      FloatClientWidth = 85
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbReport'
        end
        item
          Visible = True
          ItemName = 'lbEtiqueta'
        end>
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 821
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 516
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 729
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object lbGerarProdutoGrade: TdxBarLargeButton [16]
      Action = ActGerarProdutoGrade
      Category = 1
    end
    object lbEtiqueta: TdxBarLargeButton [17]
      Action = ActImpressaoEtiqueta
      Category = 2
    end
  end
  inherited ActionListMain: TActionList
    object ActImpressaoEtiqueta: TAction [12]
      Category = 'Report'
      Caption = 'Etiqueta F8'
      Hint = 'Imprimir etiqueta do produto'
      ImageIndex = 27
      ShortCut = 119
      OnExecute = ActImpressaoEtiquetaExecute
    end
    object ActGerarProdutoGrade: TAction
      Category = 'Manager'
      Caption = 'Gerar Produto em Grade'
      ImageIndex = 72
      OnExecute = ActGerarProdutoGradeExecute
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Produto'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspProduto'
    RemoteServer = DmConnection.dspProduto
    AfterOpen = cdsDataAfterOpen
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
    end
    object cdsDataID_SUB_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
    end
    object cdsDataID_MARCA: TIntegerField
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
    end
    object cdsDataID_CATEGORIA: TIntegerField
      DisplayLabel = 'Categoria'
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
    end
    object cdsDataID_SUB_CATEGORIA: TIntegerField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
    end
    object cdsDataID_LINHA: TIntegerField
      DisplayLabel = 'Linha'
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
    end
    object cdsDataID_MODELO: TIntegerField
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object cdsDataID_UNIDADE_ESTOQUE: TIntegerField
      DisplayLabel = 'UN'
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
    end
    object cdsDataTIPO: TStringField
      DefaultExpression = 'PRODUTO'
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 30
    end
    object cdsDataID_COR: TIntegerField
      DisplayLabel = 'C'#243'digo da Cor'
      FieldName = 'ID_COR'
      Origin = 'ID_COR'
    end
    object cdsDataCODIGO_BARRA: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO_BARRA'
      Origin = 'CODIGO_BARRA'
      Required = True
      OnChange = cdsDataCODIGO_BARRAChange
      Size = 30
    end
    object cdsDataFORMA_AQUISICAO: TStringField
      DisplayLabel = 'Forma de Aquisi'#231#227'o'
      FieldName = 'FORMA_AQUISICAO'
      Origin = 'FORMA_AQUISICAO'
      Size = 45
    end
    object cdsDataID_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'ID_NCM'
      Origin = 'ID_NCM'
      OnChange = cdsDataID_NCMChange
    end
    object cdsDataID_NCM_ESPECIALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM Especializado'
      FieldName = 'ID_NCM_ESPECIALIZADO'
      Origin = 'ID_NCM_ESPECIALIZADO'
    end
    object cdsDataJOIN_DESCRICAO_NCM: TStringField
      DisplayLabel = 'NCM'
      FieldName = 'JOIN_DESCRICAO_NCM'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CODIGO_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo NCM'
      FieldName = 'JOIN_CODIGO_NCM'
      Origin = 'CODIGO'
      ProviderFlags = []
    end
    object cdsDataJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField
      DisplayLabel = 'NCM Especializado'
      FieldName = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataTIPO_TRIBUTACAO_PIS_COFINS_ST: TIntegerField
      DisplayLabel = 'Tipo de Tributa'#231#227'o PIS/COFINS/ST'
      FieldName = 'TIPO_TRIBUTACAO_PIS_COFINS_ST'
      Origin = 'TIPO_TRIBUTACAO_PIS_COFINS_ST'
    end
    object cdsDataTIPO_TRIBUTACAO_IPI: TIntegerField
      DisplayLabel = 'Tipo de Tributa'#231#227'o IPI'
      FieldName = 'TIPO_TRIBUTACAO_IPI'
      Origin = 'TIPO_TRIBUTACAO_IPI'
    end
    object cdsDataJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_SUB_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_MARCA: TStringField
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CATEGORIA: TStringField
      DisplayLabel = 'Categoria'
      FieldName = 'JOIN_DESCRICAO_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_SUB_CATEGORIA: TStringField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'JOIN_DESCRICAO_SUB_CATEGORIA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_LINHA: TStringField
      DisplayLabel = 'Linha'
      FieldName = 'JOIN_DESCRICAO_LINHA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_MODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_COR: TStringField
      DisplayLabel = 'Cor'
      FieldName = 'JOIN_DESCRICAO_COR'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataEXCECAO_IPI: TIntegerField
      DisplayLabel = 'Exce'#231#227'o do IPI'
      FieldName = 'EXCECAO_IPI'
      Origin = 'EXCECAO_IPI'
    end
    object cdsDataID_REGRA_IMPOSTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Regra de Imposto'
      FieldName = 'ID_REGRA_IMPOSTO'
      Origin = 'ID_REGRA_IMPOSTO'
    end
    object cdsDataJOIN_DESCRICAO_REGRA_IMPOSTO: TStringField
      DisplayLabel = 'Regra Imposto'
      FieldName = 'JOIN_DESCRICAO_REGRA_IMPOSTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataORIGEM_DA_MERCADORIA: TIntegerField
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'ORIGEM_DA_MERCADORIA'
      Origin = 'ORIGEM_DA_MERCADORIA'
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object cdsDataJOIN_DESCRICAO_TAMANHO: TStringField
      DisplayLabel = 'Tamanho'
      FieldName = 'JOIN_DESCRICAO_TAMANHO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataBO_PRODUTO_AGRUPADOR: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Produto Agrupador'
      FieldName = 'BO_PRODUTO_AGRUPADOR'
      Origin = 'BO_PRODUTO_AGRUPADOR'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_PRODUTO_AGRUPADO: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Produto Agrupado'
      FieldName = 'BO_PRODUTO_AGRUPADO'
      Origin = 'BO_PRODUTO_AGRUPADO'
      FixedChar = True
      Size = 1
    end
    object cdsDataID_PRODUTO_AGRUPADOR: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'C'#243'digo do Produto Agrupador'
      FieldName = 'ID_PRODUTO_AGRUPADOR'
      Origin = 'ID_PRODUTO_AGRUPADOR'
    end
    object cdsDataID_TAMANHO: TIntegerField
      FieldName = 'ID_TAMANHO'
      Origin = 'ID_TAMANHO'
    end
    object cdsDatafdqProdutoFornecedor: TDataSetField
      FieldName = 'fdqProdutoFornecedor'
    end
    object cdsDatafdqProdutoFilial: TDataSetField
      FieldName = 'fdqProdutoFilial'
    end
    object cdsDatafdqProdutoCodigoBarra: TDataSetField
      FieldName = 'fdqProdutoCodigoBarra'
    end
    object cdsDatafdqProdutoTributacaoPorUnidade: TDataSetField
      FieldName = 'fdqProdutoTributacaoPorUnidade'
    end
    object cdsDatafdqProdutoModeloMontadora: TDataSetField
      FieldName = 'fdqProdutoModeloMontadora'
    end
    object cdsDatafdqProdutoMontadora: TDataSetField
      FieldName = 'fdqProdutoMontadora'
    end
  end
  inherited pmGridConsultaPadrao: TPopupMenu
    Left = 840
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.551147881950000000
      BuiltInReportLink = True
    end
  end
  object cdsProdutoFornecedor: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqProdutoFornecedor
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 544
    Top = 80
    object cdsProdutoFornecedorID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsProdutoFornecedorREFERENCIA: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
      Required = True
      Size = 100
    end
    object cdsProdutoFornecedorID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object cdsProdutoFornecedorID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo do Fornecedor'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsProdutoFornecedorJOIN_NOME_FORNECEDOR: TStringField
      DisplayLabel = 'Fornecedor'
      FieldName = 'JOIN_NOME_FORNECEDOR'
      Origin = 'JOIN_NOME_FORNECEDOR'
      ProviderFlags = []
      Size = 80
    end
  end
  object cdsProdutoFilial: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqProdutoFilial
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 616
    Top = 80
    object cdsProdutoFilialID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsProdutoFilialQT_ESTOQUE: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'QT_ESTOQUE'
      Origin = 'QT_ESTOQUE'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialLOCALIZACAO: TStringField
      DisplayLabel = 'Localiza'#231#227'o'
      FieldName = 'LOCALIZACAO'
      Origin = 'LOCALIZACAO'
      Size = 255
    end
    object cdsProdutoFilialQT_ESTOQUE_MINIMO: TFMTBCDField
      DisplayLabel = 'Estoque M'#237'nimo'
      FieldName = 'QT_ESTOQUE_MINIMO'
      Origin = 'QT_ESTOQUE_MINIMO'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialQT_ESTOQUE_MAXIMO: TFMTBCDField
      DisplayLabel = 'Estoque M'#225'ximo'
      FieldName = 'QT_ESTOQUE_MAXIMO'
      Origin = 'QT_ESTOQUE_MAXIMO'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsProdutoFilialID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsProdutoFilialJOIN_DESCRICAO_FILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_DESCRICAO_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsProdutoFilialQT_ESTOQUE_CONDICIONAL: TFMTBCDField
      FieldName = 'QT_ESTOQUE_CONDICIONAL'
      Origin = 'QT_ESTOQUE_CONDICIONAL'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_CUSTO_MEDIO: TFMTBCDField
      DisplayLabel = 'Vl. Custo M'#233'dio'
      FieldName = 'VL_CUSTO_MEDIO'
      Origin = 'VL_CUSTO_MEDIO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Custo'
      FieldName = 'VL_ULTIMO_CUSTO'
      Origin = 'VL_ULTIMO_CUSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_FRETE: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Frete'
      FieldName = 'VL_ULTIMO_FRETE'
      Origin = 'VL_ULTIMO_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_SEGURO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Seguro'
      FieldName = 'VL_ULTIMO_SEGURO'
      Origin = 'VL_ULTIMO_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_ICMS_ST: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo ICMS ST'
      FieldName = 'VL_ULTIMO_ICMS_ST'
      Origin = 'VL_ULTIMO_ICMS_ST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_IPI: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo IPI'
      FieldName = 'VL_ULTIMO_IPI'
      Origin = 'VL_ULTIMO_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Desconto'
      FieldName = 'VL_ULTIMO_DESCONTO'
      Origin = 'VL_ULTIMO_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltima Despesa Acessoria'
      FieldName = 'VL_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'VL_ULTIMA_DESPESA_ACESSORIA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Custo Imposto'
      FieldName = 'VL_ULTIMO_CUSTO_IMPOSTO'
      Origin = 'VL_ULTIMO_CUSTO_IMPOSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_CUSTO_MEDIO_IMPOSTO: TFMTBCDField
      DisplayLabel = 'Vl. Custo M'#233'dio Imposto'
      FieldName = 'VL_CUSTO_MEDIO_IMPOSTO'
      Origin = 'VL_CUSTO_MEDIO_IMPOSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_CUSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo Operacional'
      FieldName = 'VL_CUSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo Imposto Operacional'
      FieldName = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMO_FRETE: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Frete'
      FieldName = 'PERC_ULTIMO_FRETE'
      Origin = 'PERC_ULTIMO_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMO_SEGURO: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Seguro'
      FieldName = 'PERC_ULTIMO_SEGURO'
      Origin = 'PERC_ULTIMO_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMO_ICMS_ST: TFMTBCDField
      DisplayLabel = '% '#218'ltimo ICMS ST'
      FieldName = 'PERC_ULTIMO_ICMS_ST'
      Origin = 'PERC_ULTIMO_ICMS_ST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMO_IPI: TFMTBCDField
      DisplayLabel = '% '#218'ltimo IPI'
      FieldName = 'PERC_ULTIMO_IPI'
      Origin = 'PERC_ULTIMO_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMO_DESCONTO: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Desconto'
      FieldName = 'PERC_ULTIMO_DESCONTO'
      Origin = 'PERC_ULTIMO_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      DisplayLabel = '% '#218'ltima Despesa Acess'#243'ria'
      FieldName = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialPERC_CUSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = '% Custo Operacional'
      FieldName = 'PERC_CUSTO_OPERACIONAL'
      Origin = 'PERC_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsProdutoFilialVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo M'#233'dio Imposto Operacional'
      FieldName = 'VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
  end
  object dsProdutoFilial: TDataSource
    DataSet = cdsProdutoFilial
    Left = 648
    Top = 80
  end
  object frxReport1: TfrxReport
    Version = '4.15.10'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42090.056193634300000000
    ReportOptions.LastChange = 42090.056193634300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 584
    Top = 80
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 50.000000000000000000
      PaperHeight = 40.000000000000000000
      PaperSize = 256
      object Memo1: TfrxMemoView
        Left = 7.559060000000000000
        Top = 11.338590000000000000
        Width = 45.354360000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Memo.UTF8W = (
          'codigo')
      end
      object Memo2: TfrxMemoView
        Left = 7.559060000000000000
        Top = 37.795300000000000000
        Width = 177.637910000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Memo.UTF8W = (
          'descricao')
      end
    end
  end
  object fdmTabelaPrecoItem: TgbFDMemTable
    AfterOpen = HabilitarTabelaPreco
    AfterPost = HabilitarTabelaPreco
    AfterDelete = HabilitarTabelaPreco
    OnNewRecord = fdmTabelaPrecoItemNewRecord
    FieldOptions.PositionMode = poFirst
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 526
    Top = 336
    object fdmTabelaPrecoItemID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID_TABELA_PRECO'
    end
    object fdmTabelaPrecoItemDESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO_TABELA_PRECO'
      Size = 255
    end
    object fdmTabelaPrecoItemVL_CUSTO: TFloatField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      OnChange = CalcularValorTabelaPrecoitem
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object fdmTabelaPrecoItemPERC_LUCRO: TFloatField
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      OnChange = CalcularValorTabelaPrecoitem
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object fdmTabelaPrecoItemVL_VENDA: TFloatField
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      OnChange = CalcularPercentualTabelaPrecoItem
      DisplayFormat = '###,###,###,###,##0.00'
    end
  end
  object dsTabelaPrecoItem: TDataSource
    DataSet = fdmTabelaPrecoItem
    Left = 554
    Top = 336
  end
  object fdmFormacaoPreco: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 528
    Top = 288
    object fdmFormacaoPrecoVL_ULTIMO_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Custo'
      FieldName = 'VL_ULTIMO_CUSTO'
      Origin = 'VL_ULTIMO_CUSTO'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_FRETE: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Frete'
      FieldName = 'VL_ULTIMO_FRETE'
      Origin = 'VL_ULTIMO_FRETE'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_SEGURO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Seguro'
      FieldName = 'VL_ULTIMO_SEGURO'
      Origin = 'VL_ULTIMO_SEGURO'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_ICMS_ST: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo ICMS ST'
      FieldName = 'VL_ULTIMO_ICMS_ST'
      Origin = 'VL_ULTIMO_ICMS_ST'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_IPI: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo IPI'
      FieldName = 'VL_ULTIMO_IPI'
      Origin = 'VL_ULTIMO_IPI'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Desconto'
      FieldName = 'VL_ULTIMO_DESCONTO'
      Origin = 'VL_ULTIMO_DESCONTO'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltima Despesa Acessoria'
      FieldName = 'VL_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'VL_ULTIMA_DESPESA_ACESSORIA'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_ULTIMO_CUSTO_IMPOSTO: TFMTBCDField
      DisplayLabel = 'Vl. '#218'ltimo Custo Imposto'
      FieldName = 'VL_ULTIMO_CUSTO_IMPOSTO'
      Origin = 'VL_ULTIMO_CUSTO_IMPOSTO'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_CUSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo Operacional'
      FieldName = 'VL_CUSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_OPERACIONAL'
      OnChange = TotalizarFormacaoPrecoInicial
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = 'Vl. Custo Imposto Operacional'
      FieldName = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_IMPOSTO_OPERACIONAL'
      OnChange = fdmFormacaoPrecoVL_CUSTO_IMPOSTO_OPERACIONALChange
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMO_FRETE: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Frete'
      FieldName = 'PERC_ULTIMO_FRETE'
      Origin = 'PERC_ULTIMO_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMO_SEGURO: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Seguro'
      FieldName = 'PERC_ULTIMO_SEGURO'
      Origin = 'PERC_ULTIMO_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMO_ICMS_ST: TFMTBCDField
      DisplayLabel = '% '#218'ltimo ICMS ST'
      FieldName = 'PERC_ULTIMO_ICMS_ST'
      Origin = 'PERC_ULTIMO_ICMS_ST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMO_IPI: TFMTBCDField
      DisplayLabel = '% '#218'ltimo IPI'
      FieldName = 'PERC_ULTIMO_IPI'
      Origin = 'PERC_ULTIMO_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMO_DESCONTO: TFMTBCDField
      DisplayLabel = '% '#218'ltimo Desconto'
      FieldName = 'PERC_ULTIMO_DESCONTO'
      Origin = 'PERC_ULTIMO_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_ULTIMA_DESPESA_ACESSORIA: TFMTBCDField
      DisplayLabel = '% '#218'ltima Despesa Acess'#243'ria'
      FieldName = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      Origin = 'PERC_ULTIMA_DESPESA_ACESSORIA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object fdmFormacaoPrecoPERC_CUSTO_OPERACIONAL: TFMTBCDField
      DisplayLabel = '% Custo Operacional'
      FieldName = 'PERC_CUSTO_OPERACIONAL'
      Origin = 'PERC_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
  end
  object dsFormacaoPreco: TDataSource
    DataSet = fdmFormacaoPreco
    Left = 556
    Top = 288
  end
  object cdsProdutoMontadora: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqProdutoMontadora
    Params = <>
    BeforeInsert = cdsProdutoMontadoraBeforeInsert
    BeforePost = cdsProdutoMontadoraBeforePost
    AfterPost = cdsProdutoMontadoraAfterPost
    BeforeDelete = cdsProdutoMontadoraBeforeDelete
    AfterScroll = cdsProdutoMontadoraAfterScroll
    OnNewRecord = cdsProdutoMontadoraNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 720
    Top = 80
    object cdsProdutoMontadoraID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsProdutoMontadoraID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object cdsProdutoMontadoraNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsProdutoMontadoraMONTADORA: TStringField
      DisplayLabel = 'Montadora'
      FieldName = 'MONTADORA'
      Origin = 'MONTADORA'
      Required = True
      Size = 255
    end
    object cdsProdutoMontadoraESPECIFICACAO: TBlobField
      DisplayLabel = 'Especifica'#231#227'o'
      FieldName = 'ESPECIFICACAO'
      Origin = 'ESPECIFICACAO'
    end
    object cdsProdutoMontadoraREFERENCIA: TStringField
      DisplayLabel = 'C'#243'digo Original'
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
      Size = 255
    end
  end
  object cdsProdutoModeloMontadora: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqProdutoModeloMontadora
    Params = <>
    BeforeInsert = cdsProdutoModeloMontadoraBeforeInsert
    BeforePost = cdsProdutoModeloMontadoraBeforePost
    AfterPost = cdsProdutoModeloMontadoraAfterPost
    OnNewRecord = cdsProdutoModeloMontadoraNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 776
    Top = 80
    object cdsProdutoModeloMontadoraID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsProdutoModeloMontadoraID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
    end
    object cdsProdutoModeloMontadoraNR_ITEM_MONTADORA: TIntegerField
      DisplayLabel = 'Item Montadora'
      FieldName = 'NR_ITEM_MONTADORA'
      Origin = 'NR_ITEM_MONTADORA'
    end
    object cdsProdutoModeloMontadoraMODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Required = True
      Size = 255
    end
  end
end
