unit uCadEquipamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxTextEdit, cxDBEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxCheckBox, uGBDBCheckBox, cxMaskEdit,
  cxButtonEdit, uGBDBButtonEditFK, uGBDBTextEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon;

type
  TCadEquipamento = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataID_MARCA: TIntegerField;
    cdsDataID_MODELO: TIntegerField;
    cdsDataSERIE: TStringField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    cdsDataBO_ATIVO: TStringField;
    Label2: TLabel;
    edDescricao: TgbDBTextEdit;
    eFkModelo: TgbDBButtonEditFK;
    descModelo: TgbDBTextEdit;
    eFkMarca: TgbDBButtonEditFK;
    descMarca: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    Label10: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit4: TgbDBTextEdit;
    cdsDataJOIN_DESCRICAO_MODELO: TStringField;
    cdsDataJOIN_DESCRICAO_MARCA: TStringField;
    Label12: TLabel;
    Label8: TLabel;
    cdsDataIMEI: TStringField;
    gbDBTextEdit1: TgbDBTextEdit;
    Label1: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    Label3: TLabel;
  private

  public

  protected

  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadEquipamento);

finalization
  UnRegisterClass(TCadEquipamento);

end.
