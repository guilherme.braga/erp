inherited CadImpostoICMS: TCadImpostoICMS
  Caption = 'Cadastro de Imposto ICMS'
  ClientHeight = 469
  ClientWidth = 742
  ExplicitTop = 5
  ExplicitWidth = 758
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 742
    ExplicitWidth = 836
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 742
    Height = 342
    Properties.ActivePage = cxtsData
    ExplicitWidth = 836
    ExplicitHeight = 593
    ClientRectBottom = 342
    ClientRectRight = 742
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 836
      ExplicitHeight = 569
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 710
        Height = 318
        ExplicitWidth = 804
        ExplicitHeight = 569
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 569
        Height = 318
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 318
        ExplicitHeight = 569
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 836
      ExplicitHeight = 569
      inherited DesignPanel: TJvDesignPanel
        Width = 742
        Height = 318
        ExplicitWidth = 836
        ExplicitHeight = 569
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 836
          ExplicitHeight = 569
          Height = 318
          Width = 742
          inherited dxBevel1: TdxBevel
            Width = 738
            ExplicitWidth = 738
          end
          object Label2: TLabel
            Left = 4
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 4
            Top = 40
            Width = 56
            Height = 13
            Caption = 'CST CSOSN'
          end
          object Label3: TLabel
            Left = 444
            Top = 224
            Width = 99
            Height = 13
            Caption = '% Redu'#231#227'o ICMS ST'
          end
          object Label4: TLabel
            Left = 4
            Top = 300
            Width = 155
            Height = 13
            Caption = 'Motivo da desonera'#231#227'o do ICMS'
          end
          object Label5: TLabel
            Left = 445
            Top = 98
            Width = 84
            Height = 13
            Caption = '% Redu'#231#227'o ICMS'
          end
          object Label6: TLabel
            Left = 445
            Top = 74
            Width = 67
            Height = 13
            Caption = 'Al'#237'quota ICMS'
          end
          object Label7: TLabel
            Left = 444
            Top = 176
            Width = 82
            Height = 13
            Caption = 'Al'#237'quota ICMS ST'
          end
          object Label8: TLabel
            Left = 444
            Top = 248
            Width = 96
            Height = 13
            Caption = '% MVA Ajustado ST'
          end
          object Label9: TLabel
            Left = 445
            Top = 122
            Width = 72
            Height = 13
            Caption = '% MVA Pr'#243'prio'
          end
          object Label10: TLabel
            Left = 445
            Top = 146
            Width = 130
            Height = 13
            Caption = 'Valor Base do ICMS Pr'#243'prio'
          end
          object Label11: TLabel
            Left = 444
            Top = 200
            Width = 144
            Height = 13
            Caption = 'Al'#237'quota ICMS Pr'#243'prio para ST'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 66
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 71
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 134
            Top = 36
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_CST_CSOSN'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 583
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 66
            Top = 36
            DataBinding.DataField = 'JOIN_CODIGO_CST_CSOSN'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 1
            gbTextEdit = gbDBTextEdit1
            gbRequired = True
            gbCampoPK = 'CODIGO_CST'
            gbCamposRetorno = 'ID_CST_CSOSN;JOIN_CODIGO_CST_CSOSN;JOIN_DESCRICAO_CST_CSOSN'
            gbTableName = 'CST_CSOSN'
            gbCamposConsulta = 'ID;CODIGO_CST;DESCRICAO'
            gbIdentificadorConsulta = 'CST_CSOSN'
            Width = 71
          end
          object gbDBCalcEdit1: TgbDBCalcEdit
            Left = 597
            Top = 220
            DataBinding.DataField = 'PERC_REDUCAO_ICMS_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 11
            gbRequired = True
            Width = 120
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 3
            Top = 271
            Caption = 'Incluir frete na base do IPI?'
            DataBinding.DataField = 'BO_INCLUIR_FRETE_NA_BASE_IPI'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 13
            Transparent = True
            Width = 163
          end
          object gbDBCheckBox2: TgbDBCheckBox
            Left = 187
            Top = 271
            Caption = 'Incluir frete na base do ICMS?'
            DataBinding.DataField = 'BO_INCLUIR_FRETE_NA_BASE_ICMS'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 14
            Transparent = True
            Width = 172
          end
          object gbDBCheckBox3: TgbDBCheckBox
            Left = 387
            Top = 271
            Caption = 'Incluir IPI na base do ICMS?'
            DataBinding.DataField = 'BO_INCLUIR_IPI_NA_BASE_ICMS'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 15
            Transparent = True
            Width = 172
          end
          object gbDBCheckBox4: TgbDBCheckBox
            Left = 579
            Top = 271
            Caption = 'Destacar valor do ICMS?'
            DataBinding.DataField = 'BO_DESTACAR_VALOR_ICMS'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 16
            Transparent = True
            Width = 172
          end
          object gbDBRadioGroup1: TgbDBRadioGroup
            Left = 4
            Top = 64
            Caption = 'Modalidade da Base do ICMS'
            DataBinding.DataField = 'MODALIDADE_BASE_ICMS'
            DataBinding.DataSource = dsData
            Properties.Columns = 2
            Properties.Items = <
              item
                Caption = 'Margem de Valor Agregado (%)'
                Value = '0'
              end
              item
                Caption = 'Pauta (Valor)'
                Value = '1'
              end
              item
                Caption = 'Pre'#231'o Tabelado M'#225'ximo (Valor)'
                Value = '2'
              end
              item
                Caption = 'Valor da Opera'#231#227'o'
                Value = '3'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Height = 99
            Width = 432
          end
          object gbDBRadioGroup2: TgbDBRadioGroup
            Left = 3
            Top = 166
            Caption = 'Modalidade da Base do ICMS ST'
            DataBinding.DataField = 'MODALIDADE_BASE_ICMS_ST'
            DataBinding.DataSource = dsData
            Properties.Columns = 2
            Properties.Items = <
              item
                Caption = 'N'#227'o Calcular ICMS ST'
                Value = '-1'
              end
              item
                Caption = 'Pre'#231'o Tabelado ou M'#225'ximo Sugerido'
                Value = '0'
              end
              item
                Caption = 'Lista Negativa (valor)'
                Value = '1'
              end
              item
                Caption = 'Lista Positiva (valor)'
                Value = '2'
              end
              item
                Caption = 'Lista Neutra (valor)'
                Value = '3'
              end
              item
                Caption = 'Margem Valor Agregado (%)'
                Value = '4'
              end
              item
                Caption = 'Pauta'
                Value = '5'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            Height = 99
            Width = 433
          end
          object JvDBComboBox1: TJvDBComboBox
            Left = 166
            Top = 296
            Width = 551
            Height = 21
            DataField = 'MOTIVO_DESONERACAO_ICMS'
            DataSource = dsData
            Items.Strings = (
              'T'#225'xi'
              'Deficiente F'#237'sico'
              'Uso na agropecu'#225'ria'
              'Frotista/Locadora'
              'Diplom'#225'tico/Consular'
              
                'Utilit'#225'rios e Motocicletas da Amaz'#244'nia Ocidental e '#193'reas de Livr' +
                'e Com'#233'rcio (Resolu'#231#227'o 714/88 e 790/94 '#8211' CONTRAN e suas altera'#231#245'e' +
                's'
              'SUFRAMA'
              'Venda a '#211'rg'#227'o P'#250'blico'
              'Outros. (NT 2011/004)'
              'Deficiente Condutor (Conv'#234'nio ICMS 38/12)'
              'Deficiente N'#227'o Condutor (Conv'#234'nio ICMS 38/12)'
              #211'rg'#227'o de fomento e desenvolvimento agropecu'#225'rio')
            TabOrder = 17
            Values.Strings = (
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12')
            ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
            ListSettings.OutfilteredValueFont.Color = clRed
            ListSettings.OutfilteredValueFont.Height = -11
            ListSettings.OutfilteredValueFont.Name = 'Tahoma'
            ListSettings.OutfilteredValueFont.Style = []
          end
          object gbDBCalcEdit2: TgbDBCalcEdit
            Left = 598
            Top = 94
            DataBinding.DataField = 'PERC_REDUCAO_ICMS'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit3: TgbDBCalcEdit
            Left = 598
            Top = 70
            DataBinding.DataField = 'PERC_ALIQUOTA_ICMS'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit4: TgbDBCalcEdit
            Left = 597
            Top = 172
            DataBinding.DataField = 'PERC_ALIQUOTA_ICMS_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit5: TgbDBCalcEdit
            Left = 597
            Top = 244
            DataBinding.DataField = 'PERC_MVA_AJUSTADO_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 12
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit6: TgbDBCalcEdit
            Left = 598
            Top = 118
            DataBinding.DataField = 'PERC_MVA_PROPRIO'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit7: TgbDBCalcEdit
            Left = 598
            Top = 142
            DataBinding.DataField = 'PERC_BASE_ICMS_PROPRIO'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbRequired = True
            Width = 120
          end
          object gbDBCalcEdit8: TgbDBCalcEdit
            Left = 597
            Top = 196
            DataBinding.DataField = 'PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            gbRequired = True
            Width = 120
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspImpostoICMS'
    RemoteServer = DmConnection.dspImpostoICMS
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataBO_INCLUIR_FRETE_NA_BASE_IPI: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Incluir Frete na Base IPI'
      FieldName = 'BO_INCLUIR_FRETE_NA_BASE_IPI'
      Origin = 'BO_INCLUIR_FRETE_NA_BASE_IPI'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_INCLUIR_FRETE_NA_BASE_ICMS: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Incluir Frete na Base do ICMS'
      FieldName = 'BO_INCLUIR_FRETE_NA_BASE_ICMS'
      Origin = 'BO_INCLUIR_FRETE_NA_BASE_ICMS'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_INCLUIR_IPI_NA_BASE_ICMS: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Incluir IPI na Base do ICMS'
      FieldName = 'BO_INCLUIR_IPI_NA_BASE_ICMS'
      Origin = 'BO_INCLUIR_IPI_NA_BASE_ICMS'
      FixedChar = True
      Size = 1
    end
    object cdsDataPERC_REDUCAO_ICMS_ST: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Percentual de Redu'#231#227'o do ICMS ST'
      FieldName = 'PERC_REDUCAO_ICMS_ST'
      Origin = 'PERC_REDUCAO_ICMS_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataMODALIDADE_BASE_ICMS: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Modalidade Base ICMS'
      FieldName = 'MODALIDADE_BASE_ICMS'
      Origin = 'MODALIDADE_BASE_ICMS'
    end
    object cdsDataMODALIDADE_BASE_ICMS_ST: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Modalidade Base ICMS ST'
      FieldName = 'MODALIDADE_BASE_ICMS_ST'
      Origin = 'MODALIDADE_BASE_ICMS_ST'
    end
    object cdsDataPERC_REDUCAO_ICMS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Redu'#231#227'o ICMS'
      FieldName = 'PERC_REDUCAO_ICMS'
      Origin = 'PERC_REDUCAO_ICMS'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_ALIQUOTA_ICMS: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Al'#237'quota ICMS'
      FieldName = 'PERC_ALIQUOTA_ICMS'
      Origin = 'PERC_ALIQUOTA_ICMS'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_ALIQUOTA_ICMS_ST: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Al'#237'quota ICMS ST'
      FieldName = 'PERC_ALIQUOTA_ICMS_ST'
      Origin = 'PERC_ALIQUOTA_ICMS_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_MVA_AJUSTADO_ST: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% MVA Ajustado ST'
      FieldName = 'PERC_MVA_AJUSTADO_ST'
      Origin = 'PERC_MVA_AJUSTADO_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_MVA_PROPRIO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% MVA Pr'#243'prio'
      FieldName = 'PERC_MVA_PROPRIO'
      Origin = 'PERC_MVA_PROPRIO'
      Precision = 24
      Size = 9
    end
    object cdsDataMOTIVO_DESONERACAO_ICMS: TIntegerField
      DisplayLabel = 'Motivo de Desonera'#231#227'o do ICMS'
      FieldName = 'MOTIVO_DESONERACAO_ICMS'
      Origin = 'MOTIVO_DESONERACAO_ICMS'
    end
    object cdsDataPERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST: TFMTBCDField
      DisplayLabel = '% Al'#237'quota ICMS Pr'#243'prio para ST'
      FieldName = 'PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST'
      Origin = 'PERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST'
      Precision = 24
      Size = 9
    end
    object cdsDataBO_DESTACAR_VALOR_ICMS: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Destacar Valor do ICMS'
      FieldName = 'BO_DESTACAR_VALOR_ICMS'
      Origin = 'BO_DESTACAR_VALOR_ICMS'
      FixedChar = True
      Size = 1
    end
    object cdsDataPERC_BASE_ICMS_PROPRIO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Base ICMS Pr'#243'prio'
      FieldName = 'PERC_BASE_ICMS_PROPRIO'
      Origin = 'PERC_BASE_ICMS_PROPRIO'
      Precision = 24
      Size = 9
    end
    object cdsDataID_CST_CSOSN: TIntegerField
      DisplayLabel = 'C'#243'digo do CST CSOSN'
      FieldName = 'ID_CST_CSOSN'
      Origin = 'ID_CST_CSOSN'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_CST_CSOSN: TStringField
      DisplayLabel = 'CST CSOSN'
      FieldName = 'JOIN_DESCRICAO_CST_CSOSN'
      Origin = 'JOIN_DESCRICAO_CST_CSOSN'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CODIGO_CST_CSOSN: TStringField
      DisplayLabel = 'C'#243'digo CST CSOSN'
      FieldName = 'JOIN_CODIGO_CST_CSOSN'
      Origin = 'JOIN_CODIGO_CST_CSOSN'
      ProviderFlags = []
      Size = 5
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.880539837960000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
end
