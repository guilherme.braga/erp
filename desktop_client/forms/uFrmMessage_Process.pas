unit uFrmMessage_Process;

interface

uses
  //System
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,

  //DevExpress
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  cxGroupBox,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  ExtCtrls,

  //JVCL
  JvExControls,
  JvButton,
  JvTransparentButton,

  //Project
  uDmAcesso,
  uTVariables;

type
  TFrmMessage_Process = class(TForm)
    cxPnMain: TcxGroupBox;
    cxPnText: TcxGroupBox;
    image: TJvTransparentButton;
  private

  public
    class procedure SendMessage(const text: string);
    class procedure SendMessageTime(const text: string; const time: Integer);
    class procedure CloseMessage;
  end;

var
  FrmMessage_Process: TFrmMessage_Process;

implementation

{$R *.dfm}

class procedure TFrmMessage_Process.CloseMessage;
begin
  if FrmMessage_Process <> nil then
  begin
    FrmMessage_Process.Close;
    FreeAndNil(FrmMessage_Process);
    Variables.lMessageClose := false;

    try
      LockWindowUpdate(0);
    except
    end;
  end;
end;

class procedure TFrmMessage_Process.SendMessage(const text: string);
begin
  if not Variables.lMessageClose then
  begin
    Variables.lMessageClose := true;
    Application.CreateForm(TFrmMessage_Process, FrmMessage_Process);
    FrmMessage_Process.Width := 390;
    FrmMessage_Process.Height := 106;
    FrmMessage_Process.cxPnText.Caption := text;
    FrmMessage_Process.Show;
  end;
  Application.ProcessMessages;

  try
    LockWindowUpdate(Application.MainForm.Handle);
  except
    LockWindowUpdate(0);
  end;
end;

class procedure TFrmMessage_Process.SendMessageTime(const text: string; const time: Integer);
begin
  if not Variables.lMessageClose then
    begin
      Application.CreateForm(TFrmMessage_Process, FrmMessage_Process);
      FrmMessage_Process.Width := 390;
      FrmMessage_Process.Height := 106;
      FrmMessage_Process.cxPnText.Caption := text;
      FrmMessage_Process.Show;
      Application.ProcessMessages;
      Sleep(time);
      FrmMessage_Process.Close;
    end;
  Application.ProcessMessages;
end;

end.
