unit uRelContaPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxButtonEdit, cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, uGBClientDataset, dxBar, cxBarEditItem, dxBarExtItems,
  System.Actions, Vcl.ActnList, cxVGrid, cxInplaceContainer, uGBPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxGroupBox, dxRibbon;

type
  TRelContaPagar = class(TFrmRelatorioFRPadrao)
    FdmFiltrosID: TIntegerField;
    FdmFiltrosDOCUMENTO: TStringField;
    FdmFiltrosDESCRICAO: TStringField;
    FdmFiltrosDT_COMPETENCIA_INICIO: TDateField;
    FdmFiltrosDT_COMPETENCIA_TERMINO: TDateField;
    FdmFiltrosVL_TITULO: TFMTBCDField;
    FdmFiltrosID_CONTA_ANALISE: TLargeintField;
    FdmFiltrosID_CENTRO_RESULTADO: TLargeintField;
    FdmFiltrosDT_VENCIMENTO_INICIO: TDateField;
    FdmFiltrosDT_VENCIMENTO_TERMINO: TDateField;
    rowCentroResultado: TcxEditorRow;
    rowContaAnalise: TcxEditorRow;
    FdmFiltrosSTATUS: TStringField;
    rowStatus: TcxEditorRow;
    procedure rowCentroResultadoEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowContaAnaliseEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure SetWhereBundle; override;
  end;

var
  RelContaPagar: TRelContaPagar;

implementation

{$R *.dfm}

uses uFrmConsultaPadrao, uCentroResultadoProxy, uContaCorrenteProxy,
  uContaAnaliseProxy, uContaPagarProxy, uPesqContaAnalise;

procedure TRelContaPagar.rowCentroResultadoEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TCentroResultadoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowCentroResultado.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaPagar.rowContaAnaliseEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TPesqContaAnalise.IDSelecionado(
    StrtoIntDef(VartoStr(rowCentroResultado.Properties.Value),0));

  if IDConsultado > 0 then
  begin
    rowContaAnalise.Properties.Value := IDConsultado;
  end;
end;

procedure TRelContaPagar.SetWhereBundle;
begin
  inherited;
  AddFiltroIgual(fdmFiltrosID.Origin, fdmFiltrosID);
  AddFiltroIgual(FdmFiltrosDOCUMENTO.Origin, FdmFiltrosDOCUMENTO);
  AddFiltroIgual(fdmFiltrosDESCRICAO.Origin, fdmFiltrosDESCRICAO);

  AddFiltroEntre(fdmFiltrosDT_VENCIMENTO_INICIO.Origin,
    fdmFiltrosDT_VENCIMENTO_INICIO, FdmFiltrosDT_VENCIMENTO_TERMINO);

  AddFiltroEntre(FdmFiltrosDT_COMPETENCIA_INICIO.Origin, FdmFiltrosDT_COMPETENCIA_INICIO,
    FdmFiltrosDT_COMPETENCIA_TERMINO);

  AddFiltroIgual(FdmFiltrosSTATUS.Origin, FdmFiltrosSTATUS);
  AddFiltroIgual(FdmFiltrosID_CONTA_ANALISE.Origin, FdmFiltrosID_CONTA_ANALISE);
  AddFiltroIgual(FdmFiltrosID_CENTRO_RESULTADO.Origin, FdmFiltrosID_CENTRO_RESULTADO);
  AddFiltroIgual(FdmFiltrosVL_TITULO.Origin, FdmFiltrosVL_TITULO);
end;

initialization
  RegisterClass(TRelContaPagar);

Finalization
  UnRegisterClass(TRelContaPagar);

end.
