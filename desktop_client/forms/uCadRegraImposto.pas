unit uCadRegraImposto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxButtonEdit, uGBDBButtonEditFK, cxCalc,
  uGBDBCalcEdit, uGBPanel, uFrameRegraImpostoFiltro, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  cxSplitter, JvExControls, JvButton, JvTransparentButton, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TCadRegraImposto = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataID_NCM: TIntegerField;
    cdsDataDESCRICAO: TStringField;
    cdsDataEXCECAO_IPI: TIntegerField;
    cdsDataID_NCM_ESPECIALIZADO: TIntegerField;
    cdsDataJOIN_DESCRICAO_NCM: TStringField;
    cdsDataJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField;
    cdsDatafdqRegraImpostoFiltro: TDataSetField;
    cdsDataJOIN_CODIGO_NCM: TIntegerField;
    gbPanel1: TgbPanel;
    Label1: TLabel;
    edDescricao: TgbDBTextEdit;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    Label4: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    Label5: TLabel;
    gbDBCalcEdit1: TgbDBCalcEdit;
    PnFrameRegraImpostoFiltro: TgbPanel;
    cdsRegraImpostoFiltro: TGBClientDataSet;
    cdsRegraImpostoFiltroID: TAutoIncField;
    cdsRegraImpostoFiltroTIPO_EMPRESA_PESSOA: TStringField;
    cdsRegraImpostoFiltroFORMA_AQUISICAO: TStringField;
    cdsRegraImpostoFiltroREGIME_TRIBUTARIO_DESTINATARIO: TIntegerField;
    cdsRegraImpostoFiltroREGIME_TRIBUTARIO_EMITENTE: TIntegerField;
    cdsRegraImpostoFiltroCRT_EMITENTE: TIntegerField;
    cdsRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_EMITENTE: TStringField;
    cdsRegraImpostoFiltroBO_CONTRIBUINTE_IPI_EMITENTE: TStringField;
    cdsRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_DESTINATARIO: TStringField;
    cdsRegraImpostoFiltroBO_CONTRIBUINTE_IPI_DESTINATARIO: TStringField;
    cdsRegraImpostoFiltroID_ZONEAMENTO_EMITENTE: TIntegerField;
    cdsRegraImpostoFiltroID_ZONEAMENTO_DESTINATARIO: TIntegerField;
    cdsRegraImpostoFiltroID_REGIME_ESPECIAL_EMITENTE: TIntegerField;
    cdsRegraImpostoFiltroID_REGIME_ESPECIAL_DESTINATARIO: TIntegerField;
    cdsRegraImpostoFiltroID_SITUACAO_ESPECIAL: TIntegerField;
    cdsRegraImpostoFiltroID_REGRA_IMPOSTO: TIntegerField;
    cdsRegraImpostoFiltroID_ESTADO_ORIGEM: TIntegerField;
    cdsRegraImpostoFiltroID_ESTADO_DESTINO: TIntegerField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_EMITENTE: TStringField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO: TStringField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE: TStringField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO: TStringField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_SITUACAO_ESPECIAL: TStringField;
    cdsRegraImpostoFiltroJOIN_UF_ESTADO_ORIGEM: TStringField;
    cdsRegraImpostoFiltroJOIN_UF_ESTADO_DESTINO: TStringField;
    cdsRegraImpostoFiltroDH_INICIO_VIGENCIA: TDateTimeField;
    cdsRegraImpostoFiltroDH_FIM_VIGENCIA: TDateTimeField;
    cdsRegraImpostoFiltroORIGEM_DA_MERCADORIA: TIntegerField;
    cdsRegraImpostoFiltroID_IMPOSTO_PIS_COFINS: TIntegerField;
    cdsRegraImpostoFiltroID_IMPOSTO_ICMS: TIntegerField;
    cdsRegraImpostoFiltroID_IMPOSTO_IPI: TIntegerField;
    cdsRegraImpostoFiltroJOIN_CST_CSOSN: TStringField;
    cdsRegraImpostoFiltroJOIN_CST_IPI: TStringField;
    cdsRegraImpostoFiltroJOIN_CST_PIS_COFINS: TStringField;
    cdsRegraImpostoFiltroID_OPERACAO_FISCAL: TIntegerField;
    cdsRegraImpostoFiltroJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField;
    procedure cdsRegraImpostoFiltroNewRecord(DataSet: TDataSet);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
  private
    FFrameRegraImpostoFiltro: TFrameRegraImpostoFiltro;
  public
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uConstantes, uConstantesFiscaisProxy;

{ TCadRegraImposto }

procedure TCadRegraImposto.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FFrameRegraImpostoFiltro.SetConfiguracoesIniciais(cdsRegraImpostoFiltro);
end;

procedure TCadRegraImposto.cdsRegraImpostoFiltroNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsRegraImpostoFiltroDH_INICIO_VIGENCIA.AsDateTime := Now;
end;

procedure TCadRegraImposto.InstanciarFrames;
begin
  inherited;
  FFrameRegraImpostoFiltro := TFrameRegraImpostoFiltro.Create(PnFrameRegraImpostoFiltro);
  FFrameRegraImpostoFiltro.Parent := PnFrameRegraImpostoFiltro;
  FFrameRegraImpostoFiltro.Align := alClient;
end;

initialization
  RegisterClass(TCadRegraImposto);
finalization
  UnRegisterClass(TCadRegraImposto);

end.
