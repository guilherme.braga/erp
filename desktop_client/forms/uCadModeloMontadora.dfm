inherited CadModeloMontadora: TCadModeloMontadora
  Caption = 'Cadastro de Modelos da Montadora'
  ExplicitWidth = 952
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label4: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label2: TLabel
            Left = 406
            Top = 40
            Width = 52
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Montadora'
          end
          object Label3: TLabel
            Left = 8
            Top = 66
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 863
            Top = 36
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Transparent = True
            Width = 55
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 68
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 332
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 461
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_MONTADORA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = gbDBTextEdit2
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_MONTADORA;JOIN_DESCRICAO_MONTADORA'
            gbTableName = 'MONTADORA'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'MONTADORA'
            Width = 60
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 518
            Top = 36
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_MONTADORA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 343
          end
          object gbDBBlobEdit1: TgbDBBlobEdit
            Left = 68
            Top = 62
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Properties.PopupHeight = 300
            Properties.PopupWidth = 160
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Width = 850
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspModeloMontadora'
    RemoteServer = DmConnection.dspModeloMontadora
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataID_MONTADORA: TIntegerField
      DisplayLabel = 'C'#243'digo da Montadora'
      FieldName = 'ID_MONTADORA'
      Origin = 'ID_MONTADORA'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_MONTADORA: TStringField
      DisplayLabel = 'Montadora'
      FieldName = 'JOIN_DESCRICAO_MONTADORA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42285.628337326390000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
end
