unit uMovVendaVarejoTouch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, dxBarBuiltInMenu, uGBPanel,
  cxPC, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxLabel, cxTextEdit, cxDBEdit, uGBDBTextEdit, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxCheckBox, cxGridDBTableView,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, Vcl.StdCtrls, DateUtils,
  dxBarExtItems, cxBarEditItem, System.Generics.Collections, uFiltroFormulario,
  cxCalendar, cxCurrencyEdit, StrUtils, Vcl.Menus, cxGridCustomPopupMenu,
  cxGridPopupMenu, JvComponentBase, JvEnterTab, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, JvExControls, JvButton,
  JvTransparentButton, Vcl.ImgList, cxGridCardView, cxGridDBCardView,
  cxGridCustomLayoutView, cxDBLabel, uUsuarioDesignCOntrol;

type TEStadoProduto = (Incluindo, Alterando, Exluindo, Nenhum);

type
  TMovVendaVarejoTouch = class(TFrmChildPadrao)
    cxpcMain: TcxPageControl;
    tsVenda: TcxTabSheet;
    tsFechamento: TcxTabSheet;
    PnTopo: TgbPanel;
    tsPesquisa: TcxTabSheet;
    cxGridPesquisaPadrao: TcxGrid;
    viewPesquisa: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    PnCentro: TgbPanel;
    PnProdutos: TgbPanel;
    cxGridProdutos: TcxGrid;
    cxGridLevel1: TcxGridLevel;
    cxStyleRepository: TcxStyleRepository;
    cxStyleYellowStrong: TcxStyle;
    cxStyleYellowTooStrong: TcxStyle;
    cdsVenda: TGBClientDataSet;
    dsVenda: TDataSource;
    cdsVendaID: TAutoIncField;
    cdsVendaDH_CADASTRO: TDateTimeField;
    cdsVendaVL_DESCONTO: TFMTBCDField;
    cdsVendaVL_ACRESCIMO: TFMTBCDField;
    cdsVendaVL_TOTAL_PRODUTO: TFMTBCDField;
    cdsVendaVL_VENDA: TFMTBCDField;
    cdsVendaSTATUS: TStringField;
    cdsVendaID_PESSOA: TIntegerField;
    cdsVendaID_FILIAL: TIntegerField;
    cdsVendaID_OPERACAO: TIntegerField;
    cdsVendaID_CHAVE_PROCESSO: TIntegerField;
    cdsVendaID_CENTRO_RESULTADO: TIntegerField;
    cdsVendaID_CONTA_ANALISE: TIntegerField;
    cdsVendaID_PLANO_PAGAMENTO: TIntegerField;
    cdsVendaPERC_DESCONTO: TFMTBCDField;
    cdsVendaPERC_ACRESCIMO: TFMTBCDField;
    cdsVendaJOIN_NOME_PESSOA: TStringField;
    cdsVendaJOIN_DESCRICAO_OPERACAO: TStringField;
    cdsVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField;
    cdsVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    cdsVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsVendafdqVendaParcela: TDataSetField;
    cdsVendafdqVendaItem: TDataSetField;
    cdsVendaItem: TGBClientDataSet;
    cdsVendaParcela: TGBClientDataSet;
    cdsVendaItemID: TAutoIncField;
    cdsVendaItemID_VENDA: TIntegerField;
    cdsVendaItemID_PRODUTO: TIntegerField;
    cdsVendaItemNR_ITEM: TIntegerField;
    cdsVendaItemQUANTIDADE: TFMTBCDField;
    cdsVendaItemVL_DESCONTO: TFMTBCDField;
    cdsVendaItemVL_ACRESCIMO: TFMTBCDField;
    cdsVendaItemVL_BRUTO: TFMTBCDField;
    cdsVendaItemVL_LIQUIDO: TFMTBCDField;
    cdsVendaItemPERC_DESCONTO: TFMTBCDField;
    cdsVendaItemPERC_ACRESCIMO: TFMTBCDField;
    cdsVendaItemJOIN_DESCRICAO_PRODUTO: TStringField;
    cdsVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    cdsVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    cdsVendaParcelaID: TAutoIncField;
    cdsVendaParcelaID_VENDA: TIntegerField;
    cdsVendaParcelaDOCUMENTO: TStringField;
    cdsVendaParcelaVL_TITULO: TFMTBCDField;
    cdsVendaParcelaQT_PARCELA: TIntegerField;
    cdsVendaParcelaNR_PARCELA: TIntegerField;
    cdsVendaParcelaDT_VENCIMENTO: TDateField;
    dsVendaItem: TDataSource;
    dsVendaParcela: TDataSource;
    cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    cdsVendaID_TABELA_PRECO: TIntegerField;
    cdsVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    ActVenda: TAction;
    ActEfetivarVenda: TAction;
    ActVoltar: TAction;
    lbVoltar: TdxBarLargeButton;
    lbFechamentoVenda: TdxBarLargeButton;
    lbEfetivar: TdxBarLargeButton;
    PnParcela: TgbPanel;
    cxGridFechamento: TcxGrid;
    viewFechamento: TcxGridDBBandedTableView;
    viewFechamentoNR_PARCELA: TcxGridDBBandedColumn;
    viewFechamentoDOCUMENTO: TcxGridDBBandedColumn;
    viewFechamentoDT_VENCIMENTO: TcxGridDBBandedColumn;
    viewFechamentoIC_DIAS: TcxGridDBBandedColumn;
    viewFechamentoVL_TITULO: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    PnPlanoPagamento: TgbPanel;
    Label4: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    EdtPlanoPagamento: TgbDBButtonEditFK;
    cdsVendaParcelaIC_DIAS: TIntegerField;
    ActPesquisar: TAction;
    ActDevolucao: TAction;
    ActOrcamento: TAction;
    ActPedido: TAction;
    ActCondicional: TAction;
    lbDevolucao: TdxBarLargeButton;
    lbOrcamento: TdxBarLargeButton;
    lbPedido: TdxBarLargeButton;
    lbCondicional: TdxBarLargeButton;
    lbPesquisar: TdxBarLargeButton;
    barConsultar: TdxBar;
    ActCancelarVenda: TAction;
    lbCancelarDocumento: TdxBarLargeButton;
    filtroDataCadastroInicial: TcxBarEditItem;
    filtroDataCadastroFIm: TcxBarEditItem;
    filtroDataDocumentoInicio: TcxBarEditItem;
    filtroDataDocumentoFim: TcxBarEditItem;
    filtroTipoDocumento: TdxBarCombo;
    filtroSituacao: TdxBarCombo;
    filtroPessoa: TcxBarEditItem;
    filtroLabelDataCadastro: TdxBarStatic;
    filtroLabelDataDocumento: TdxBarStatic;
    fdmSearch: TFDMemTable;
    dsSearch: TDataSource;
    ActImprimir: TAction;
    filtroCodigoVenda: TcxBarEditItem;
    filtroNomePessoa: TdxBarStatic;
    BarInformacoes: TdxBar;
    informacoesCodigo: TdxBarStatic;
    informacoesSituacao: TdxBarStatic;
    cdsVendaTIPO: TStringField;
    cdsVendaDH_FECHAMENTO: TDateTimeField;
    lbImpressao: TdxBarLargeButton;
    lbCancelar: TdxBarLargeButton;
    ActionListAssistent: TActionList;
    ActSalvarConfiguracoes: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActAlterarColunasGrid: TAction;
    ActExibirAgrupamento: TAction;
    ActAbrirConsultando: TAction;
    ActAlterarSQLPesquisaPadrao: TAction;
    ActFullExpand: TAction;
    ActConfigurarValoresDefault: TAction;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    ExibirAgrupamento2: TMenuItem;
    RestaurarColunasPadrao: TMenuItem;
    SalvarConfiguraes1: TMenuItem;
    pmGridConsultaPadrao: TPopupMenu;
    AlterarSQL1: TMenuItem;
    ExibirAgrupamento1: TMenuItem;
    PopUpControllerPesquisa: TcxGridPopupMenu;
    ActExcluir: TAction;
    lbExcluir: TdxBarLargeButton;
    JvEnterAsTab1: TJvEnterAsTab;
    viewProduto: TcxGridDBBandedTableView;
    viewProdutoID_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoNR_ITEM: TcxGridDBBandedColumn;
    viewProdutoQUANTIDADE: TcxGridDBBandedColumn;
    viewProdutoVL_DESCONTO: TcxGridDBBandedColumn;
    viewProdutoVL_ACRESCIMO: TcxGridDBBandedColumn;
    viewProdutoVL_BRUTO: TcxGridDBBandedColumn;
    viewProdutoVL_LIQUIDO: TcxGridDBBandedColumn;
    viewProdutoPERC_DESCONTO: TcxGridDBBandedColumn;
    viewProdutoPERC_ACRESCIMO: TcxGridDBBandedColumn;
    viewProdutoJOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TcxGridDBBandedColumn;
    viewProdutoJOIN_ESTOQUE_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoJOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn;
    PopUpControllerFechamento: TcxGridPopupMenu;
    PopUpControllerProduto: TcxGridPopupMenu;
    gbPanel1: TgbPanel;
    cxImageList32x32: TcxImageList;
    ActMainMenu: TActionList;
    ActAlterarQuantidade: TAction;
    ActCancelarItem: TAction;
    ActAdicionar1: TAction;
    ActAdicionar2: TAction;
    ActSubtrair1: TAction;
    ActSubtrair2: TAction;
    cxGroupBox18: TcxGroupBox;
    cxGroupBox27: TcxGroupBox;
    JvTransparentButton21: TJvTransparentButton;
    JvTransparentButton22: TJvTransparentButton;
    cxGroupBox28: TcxGroupBox;
    JvTransparentButton23: TJvTransparentButton;
    JvTransparentButton24: TJvTransparentButton;
    cxImageList22x22: TcxImageList;
    cxGBGrupoProduto: TcxGroupBox;
    PnPaginacaoGrupo: TcxGroupBox;
    BtnGridDownGrupoProdutos: TJvTransparentButton;
    BtnGridUpGrupoProdutos: TJvTransparentButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel9: TcxLabel;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBCardView1: TcxGridDBCardView;
    cxGridDBCardViewRow1: TcxGridDBCardViewRow;
    cxGridGrupoProduto: TcxGridLevel;
    cxGBProduto: TcxGroupBox;
    PnPaginacaoProduto: TcxGroupBox;
    BtnGridDownProdutos: TJvTransparentButton;
    BtnGridUpProdutos: TJvTransparentButton;
    cxGroupBox13: TcxGroupBox;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGrid2DBCardView1: TcxGridDBCardView;
    cxGrid2DBCardView1Row1: TcxGridDBCardViewRow;
    cxGridProduto: TcxGridLevel;
    cxGroupBox14: TcxGroupBox;
    cxLabel10: TcxLabel;
    cdsTodosGruposProduto: TGBClientDataSet;
    dsTodosGruposProduto: TDataSource;
    cdsTodosGruposProdutoID: TAutoIncField;
    cdsTodosGruposProdutoDESCRICAO: TStringField;
    cxGroupBox3: TcxGroupBox;
    JvTransparentButton1: TJvTransparentButton;
    cxGroupBox4: TcxGroupBox;
    JvTransparentButton2: TJvTransparentButton;
    cxGroupBox6: TcxGroupBox;
    JvTransparentButton3: TJvTransparentButton;
    JvTransparentButton4: TJvTransparentButton;
    cxGroupBox7: TcxGroupBox;
    JvTransparentButton5: TJvTransparentButton;
    JvTransparentButton6: TJvTransparentButton;
    cdsProdutoGrupoTabelaPreco: TGBClientDataSet;
    dsProdutoGrupoTabelaPreco: TDataSource;
    cdsProdutoGrupoTabelaPrecoID: TAutoIncField;
    cdsProdutoGrupoTabelaPrecoDESCRICAO: TStringField;
    cdsProdutoGrupoTabelaPrecoBO_ATIVO: TStringField;
    cdsProdutoGrupoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_MARCA: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_CATEGORIA: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_SUB_CATEGORIA: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_LINHA: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_MODELO: TIntegerField;
    cdsProdutoGrupoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField;
    cdsProdutoGrupoTabelaPrecoTIPO: TStringField;
    cdsProdutoGrupoTabelaPrecoCODIGO_BARRA: TStringField;
    cdsProdutoGrupoTabelaPrecoVL_VENDA: TFMTBCDField;
    cxGrid2DBCardView1Row2: TcxGridDBCardViewRow;
    cxStyleYellowStrongOdd: TcxStyle;
    gbPanel2: TgbPanel;
    cxDBLabel1: TcxDBLabel;
    cxLabel1: TcxLabel;
    cxStyleYellowStrongNegrito: TcxStyle;
    BtnGridUpComandaItem: TJvTransparentButton;
    BtnGridDownComandaItem: TJvTransparentButton;
    pmTela: TPopupMenu;
    ConfigurarValoresPadres1: TMenuItem;
    cdsVendaVL_PAGAMENTO: TFMTBCDField;
    cdsVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField;
    cdsVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField;
    cdsVendaID_VENDEDOR: TIntegerField;
    cdsVendaJOIN_NOME_VENDEDOR: TStringField;
    procedure CalcularDescontoPeloPercentual(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure cxGridProdutosViewDblClick(Sender: TObject);
    procedure cxGridProdutosViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsVendaItemAfterDelete(DataSet: TDataSet);
    procedure ActVendaExecute(Sender: TObject);
    procedure ActEfetivarVendaExecute(Sender: TObject);
    procedure ActVoltarExecute(Sender: TObject);
    procedure AlterandoDiasParcelamento(Sender: TField);
    procedure AlterandoDataParcelamento(Sender: TField);
    procedure cdsVendaNewRecord(DataSet: TDataSet);
    procedure GerarParcela(Sender: TField);
    procedure AoAlterarQuantidade(Sender: TField);
    procedure cdsVendaItemAfterPost(DataSet: TDataSet);
    procedure ActDevolucaoExecute(Sender: TObject);
    procedure ActOrcamentoExecute(Sender: TObject);
    procedure ActPedidoExecute(Sender: TObject);
    procedure ActCondicionalExecute(Sender: TObject);
    procedure ActCancelarVendaExecute(Sender: TObject);
    procedure fdmSearchAfterClose(DataSet: TDataSet);
    procedure fdmSearchAfterDelete(DataSet: TDataSet);
    procedure fdmSearchAfterOpen(DataSet: TDataSet);
    procedure cdsVendaAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure viewPesquisaDblClick(Sender: TObject);
    procedure viewPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ActCancelExecute(Sender: TObject);
    procedure ActPesquisarExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure ActAbrirConsultandoExecute(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure pmGridConsultaPadraoPopup(Sender: TObject);
    procedure pmTituloGridPesquisaPadraoPopup(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActExcluirExecute(Sender: TObject);
    procedure ActAlterarQuantidadeExecute(Sender: TObject);
    procedure ActAdicionar1Execute(Sender: TObject);
    procedure ActAdicionar2Execute(Sender: TObject);
    procedure ActSubtrair1Execute(Sender: TObject);
    procedure ActSubtrair2Execute(Sender: TObject);
    procedure ActCancelarItemExecute(Sender: TObject);
    procedure cxGridDBCardView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MoveGridUpDown(Sender: TObject);
    procedure cxGrid2DBCardView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ActImprimirExecute(Sender: TObject);
    procedure cdsVendaItemBeforePost(DataSet: TDataSet);
    procedure ActConfigurarValoresDefaultExecute(Sender: TObject);
    procedure InserirValoresPadrao(DataSet: TDataSet);
  private
    const CID_NULL: Integer = -999;
    const FILTRO_TODOS: String = 'TODOS';

    var
    FWhereSearch: TFiltroPadrao;
    FPodeExecutarGeracaoParcela: Boolean;
    FCalculandoPeriodoParcelamento: Boolean;
    FFdmTabelaPrecoItem: TFDMemTable;
    FEstadoProduto: TEStadoProduto;
    FCalculandoValor: Boolean;
    FConsultaAutomatica: Boolean;
    FPodeExecutarConsultaPorCodigoBarra: Boolean;

    FParametroFormaPagamentoAVista: TParametroFormulario;
    FParametroCentroResultadoAVista: TParametroFormulario;
    FParametroContaAnaliseAVista: TParametroFormulario;
    FParametroPlanoPagamentoAVista: TParametroFormulario;
    FParametroContaCorrente: TParametroFormulario;
    FParametroCarteira: TParametroFormulario;

    procedure ConfigurarEventoInserirValoresPadrao(ADataset: TgbClientDataset);
    procedure ProcessarProdutosPorGrupo;
    procedure MostrarTodosGruposProduto;
    function ViewAtivaNaTela: TcxGridDBBandedTableView;
    procedure SalvarProduto;
    procedure AlterarProduto;
    procedure AtualizarPrecoProdutos;
    procedure TotalizarValoresVenda;
    procedure NovaVenda;
    procedure AtualizarNumeroItemDosItensDaVenda;
    procedure GerenciarControles;
    procedure AoInicializarFormulario;
    procedure HabilitarBotaoEfetivar;
    procedure SalvarConfiguracaoGrid;
    procedure PesquisarVenda;
    procedure ImprimirDocumento;
    procedure MostrarProdutosPorGrupo;

    procedure CriarTotalizadorComanda;

    procedure PreencherPlanoContaAVista;

    procedure AdicionarQuantidade(AIdProduto: Integer; AQuantidade: Double);
    procedure SubtrairQuantidade(AIdProduto: Integer; AQuantidade: Double);
    procedure AlterarQuantidadePeloTeclado(AIdProduto: Integer);

    procedure GerarParcelamento;
    procedure PrepararFiltros;
    procedure Consultar;
    procedure LimparVenda;
    procedure SetWhereBundle;
    procedure ClearWhereBundle;
    procedure ExibirFiltroDeMemoriaDaGrid(const AExibir: Boolean);
    procedure CarregarDocumento(AIdVenda: Integer);
    procedure AcaoDaTelaAposCarregarDocumento;
    procedure PreencherNomePessoaNoFiltro;
    procedure FechamentoVenda;
    procedure ValidarCamposObrigatorios;

  public

  protected
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure MostrarInformacoesImplantacao; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uControlsUtils, uMathUtils, uTabelaPreco, uProduto,
  uFrmMessage, uFilial, uTPessoa, uFrmMessage_Process, uVenda, uChaveProcesso,
  uDatasetUtils, uDevExpressUtils, uTUsuario, Data.FireDACJSONReflect,
  uStringUtils, uFrmAlterarSQLPesquisaPadrao, uFrmApelidarColunasGrid,
  uTFunction, uTMessage, uPlanoConta, uFormaPagamento,
  uMovVendaVarejoRecebimento, uFrmInputNumber, uImpressaoMP2032,
  uIniFileClient, uChaveProcessoProxy, uSistema, uFrmConfiguracaoValoresPadroes, uConstParametroFormulario,
  uVendaProxy, uFrmHelpImplantacaoVendaTouch, uTControl_Function,
  uPesqContaAnalise, uPlanoPagamento;

{ TMovVendaVarejoTouchTouch }

procedure TMovVendaVarejoTouch.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Forma de Pagamento a Vista
  FParametroFormaPagamentoAVista := TParametroFormulario.Create;
  FParametroFormaPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_AVISTA;
  FParametroFormaPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_AVISTA;
  FParametroFormaPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroFormaPagamentoAVista);

  //Parametro Conta Corrente
  FParametroContaCorrente := TParametroFormulario.Create;
  FParametroContaCorrente.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FParametroContaCorrente.descricao :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FParametroContaCorrente.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaCorrente);

  //Parametro Centro de Resultado A Vista
  FParametroCentroResultadoAVista := TParametroFormulario.Create;
  FParametroCentroResultadoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroCentroResultadoAVista);

  //Parametro Conta de Analise A Vista
  FParametroContaAnaliseAVista := TParametroFormulario.Create;
  FParametroContaAnaliseAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaAnaliseAVista);

  //Parametro Plano de Pagamento a Vista
  FparametroPlanoPagamentoAVista := TParametroFormulario.Create;
  FparametroPlanoPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroPlanoPagamentoAVista);

  //Carteira
  FParametroCarteira := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCarteira);
end;

procedure TMovVendaVarejoTouch.AcaoDaTelaAposCarregarDocumento;
begin
  if not cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO) then
    exit;

  if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_PEDIDO) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_VENDA) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_ORCAMENTO) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
  begin

  end;
end;

procedure TMovVendaVarejoTouch.ActCancelarItemExecute(Sender: TObject);
begin
  inherited;
  if cdsVendaItem.IsEmpty then
    Exit;

  SubtrairQuantidade(cdsVendaItemID_PRODUTO.AsInteger, cdsVendaItemQUANTIDADE.AsInteger);
end;

procedure TMovVendaVarejoTouch.ActCancelarVendaExecute(Sender: TObject);
begin
  inherited;
  if cdsVendaID.AsInteger > 0 then
  begin
    TVenda.Cancelar(cdsVendaID_CHAVE_PROCESSO.AsInteger);
    NovaVenda;
  end
  else
  begin
    LimparVenda;
    NovaVenda;
  end;
end;

procedure TMovVendaVarejoTouch.ActCancelExecute(Sender: TObject);
begin
  //inherited;
  LimparVenda;
  NovaVenda;
end;

procedure TMovVendaVarejoTouch.ActCondicionalExecute(Sender: TObject);
begin
  inherited;
  try
    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_CONDICIONAL;
    ImprimirDocumento;
  finally
    cdsVenda.Commit;
    NovaVenda;
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejoTouch.ActConfigurarValoresDefaultExecute(
  Sender: TObject);
begin
  inherited;
  TFrmConfigurarValoresPadroes.ConfigurarValoresPadroes(Self.Name,
    TSistema.Sistema.usuario.idSeguranca, cdsVenda);
end;

procedure TMovVendaVarejoTouch.ActDevolucaoExecute(Sender: TObject);
begin
  inherited;
  cdsVendaTIPO.AsString := TIPO_DOCUMENTO_DEVOLUCAO;
  FechamentoVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.ActEfetivarVendaExecute(Sender: TObject);
var
  vendaProxy: TVendaProxy;
begin
  inherited;
  //TValidacaoCampo.CampoPreenchido(cdsVendaID_PLANO_PAGAMENTO);
  cdsVendaDH_FECHAMENTO.AsDateTime := Now;
  cdsVendaSTATUS.AsString := STATUS_FECHADO;
  cdsVenda.Commit;

  TFrmMessage_Process.SendMessage('Efetivando a venda');
  try
    vendaProxy := TVendaProxy.Create;
    try
      vendaProxy.FIdContaCorrente :=
        FParametroContaCorrente.AsInteger;

      vendaProxy.FIdFormaPagamentoDinheiro :=
        FParametroFormaPagamentoAVista.AsInteger;

      vendaProxy.FIdCarteira := FParametroCarteira.AsInteger;

      TVenda.Efetivar(cdsVendaID_CHAVE_PROCESSO.AsInteger, vendaProxy);
    finally
      FreeAndNil(vendaProxy);
    end;
    ImprimirDocumento;
  finally
    NovaVenda;
    GerenciarControles;
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovVendaVarejoTouch.ActExcluirExecute(Sender: TObject);
begin
  if TMessage.MessageDeleteAsk() then
  try
    cdsVenda.Cancelar;
    cdsVenda.Excluir;
  finally
    NovaVenda;
    GerenciarControles;
  end
end;

procedure TMovVendaVarejoTouch.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  inherited;

  ViewAtivaNaTela.OptionsView.GroupByBox := not ViewAtivaNaTela.OptionsView.GroupByBox;
end;

procedure TMovVendaVarejoTouch.ActImprimirExecute(Sender: TObject);
begin
  inherited;
  ImprimirDocumento;
end;

procedure TMovVendaVarejoTouch.ActOrcamentoExecute(Sender: TObject);
begin
  inherited;
  try
    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_ORCAMENTO;
    cdsVenda.Commit;
    ImprimirDocumento;
    NovaVenda;
  finally
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejoTouch.ActPedidoExecute(Sender: TObject);
begin
  inherited;
  try
    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_PEDIDO;
    cdsVenda.Commit;
    ImprimirDocumento;
    NovaVenda;
  finally
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejoTouch.ActPesquisarExecute(Sender: TObject);
begin
  inherited;
  if cxpcMain.ActivePage = tsVenda then
  begin
    if cdsVenda.ChangeCount > 0 then
    begin
      if TFrmMessage.Question('Existe altera��es que n�o est�o salvas, '+
        'deseja cancela-las e continuar a pesquisa?') = MCANCELED then
        exit;
    end;

    cxpcMain.ActivePage := tsPesquisa;

    GerenciarControles;

    if FConsultaAutomatica then
      Consultar;
  end
  else if cxpcMain.ActivePage = tsPesquisa then
  begin
    Consultar;
  end;
end;

procedure TMovVendaVarejoTouch.ActVendaExecute(Sender: TObject);
begin
  inherited;
  cdsVendaTIPO.AsString := TIPO_DOCUMENTO_VENDA;
  FechamentoVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.ActVoltarExecute(Sender: TObject);
begin
  inherited;
  cxpcMain.ActivePage := tsVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.AdicionarQuantidade(AIdProduto: Integer; AQuantidade: Double);
var savePlace: TBookMark;
begin
  if cdsVendaItem.IsEmpty then
    Exit;

  savePlace := cdsVendaItem.GetBookmark;

  //Adiciona/Remove Quantidade
  cdsVendaItem.Edit;
  cdsVendaItemQUANTIDADE.AsFloat := cdsVendaItemQUANTIDADE.AsFloat + AQuantidade;
  cdsVendaItem.Post;

  if Assigned(savePlace) and cdsVendaItem.BookMarkValid(savePlace) then
    cdsVendaItem.GotoBookmark(savePlace);
end;

procedure TMovVendaVarejoTouch.AlterandoDataParcelamento(Sender: TField);
begin
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsVendaParcelaIC_DIAS.AsInteger := DaysBetween(Date,
      cdsVendaParcelaDT_VENCIMENTO.AsDateTime);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovVendaVarejoTouch.AlterandoDiasParcelamento(Sender: TField);
begin
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsVendaParcelaDT_VENCIMENTO.AsDateTime := Date +
      cdsVendaParcelaIC_DIAS.AsInteger;
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovVendaVarejoTouch.AlterarProduto;
begin

end;

procedure TMovVendaVarejoTouch.AlterarQuantidadePeloTeclado(
  AIdProduto: Integer);
var nQuantidade: Real;
    savePlace: TBookMark;
    lGotoBookMark: Boolean;
begin
  if cdsVendaItem.IsEmpty then
    Exit;

  lGotoBookMark := true;

  nQuantidade := TFrmInputNumber.buildKeyboard('Digite uma quantidade');

  savePlace := cdsVendaItem.GetBookmark;

  cdsVendaItem.Edit;
  cdsVendaItemQUANTIDADE.AsFloat :=
    TFunction.iif(nQuantidade <= 0, cdsVendaItemQUANTIDADE.AsFloat, nQuantidade);
  cdsVendaItem.Post;

  //Se a quantidade do item for 0 ou negativo ent�o exclui o item da comanda
  if cdsVendaItemQUANTIDADE.AsFloat <= 0 then
  begin
    cdsVendaItem.Delete;
    lGotoBookMark := false;
  end;

  if lGotoBookMark then
    cdsVendaItem.GotoBookmark(savePlace);
end;

procedure TMovVendaVarejoTouch.AoAlterarQuantidade(Sender: TField);
begin
{  CalcularValorTotalItem;}
end;

procedure TMovVendaVarejoTouch.AoInicializarFormulario;
begin
  TFrmMessage_Process.SendMessage('Carregando informa��es');
  try
    PrepararFiltros;

    tsPesquisa.Visible := False;
    cxpcMain.ActivePage := tsVenda;
    cxpcMain.Properties.HideTabs := true;

    FFdmTabelaPrecoItem := TFDMemTable.Create(nil);

    FPodeExecutarGeracaoParcela := true;
    FCalculandoPeriodoParcelamento := false;
    FEstadoProduto := nenhum;
    FCalculandoValor := false;
    FPodeExecutarConsultaPorCodigoBarra := true;

    FConsultaAutomatica := TUsuarioPesquisaPadrao.GetConsultaAutomatica(
      TSistema.Sistema.Usuario.idSeguranca, Self.Name);

    NovaVenda;

    MostrarTodosGruposProduto;
    MostrarProdutosPorGrupo;

    TUsuarioGridView.LoadGridView(viewFechamento,
      TSistema.Sistema.Usuario.idSeguranca, Self.Name+viewFechamento.Name);
    TUsuarioGridView.LoadGridView(viewProduto,
      TSistema.Sistema.Usuario.idSeguranca, Self.Name+viewProduto.Name);
    CriarTotalizadorComanda;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovVendaVarejoTouch.MostrarTodosGruposProduto;
begin
  cdsTodosGruposProduto.Close;
  cdsTodosGruposProduto.Open;

  cxGBGrupoProduto.Visible := (cdsTodosGruposProduto.RecordCount > 1);
end;

procedure TMovVendaVarejoTouch.AtualizarNumeroItemDosItensDaVenda;
var registroAtual: TBookMark;
begin
  registroAtual := cdsVendaItem.GetBookMark;
  try
    cdsVendaItem.DisableControls;
    cdsVendaItem.First;
    while not cdsVendaItem.Eof do
    begin
      cdsVendaItem.Edit;
      cdsVendaItemNR_ITEM.AsInteger := cdsVendaItem.Recno;
      cdsVendaItem.Post;
      cdsVendaItem.Next;
    end;
  finally
    if (registroAtual <> nil) and cdsVendaItem.BookmarkValid(registroAtual) then
      cdsVendaItem.GotoBookmark(registroAtual);

    cdsVendaItem.EnableControls;
  end;
end;

procedure TMovVendaVarejoTouch.AtualizarPrecoProdutos;
begin
  //Depois, quando alterar a tabela de pre�o recalcular os produtos
end;

procedure TMovVendaVarejoTouch.CalcularDescontoPeloPercentual(Sender: TField);
begin
  if FCalculandoValor then
    Exit;

  try
    FCalculandoValor := true;

  {  if Sender = fdmProdutoPERC_DESCONTO then
    begin
      fdmProdutoVL_DESCONTO.AsFloat := TMathUtils.ValorSobrePercentual(
        fdmProdutoPERC_DESCONTO.AsFloat, fdmProdutoVALOR.AsFloat);

      CalcularValorTotalItem;
    end}
  finally
    FCalculandoValor := false;
  end;
end;

procedure TMovVendaVarejoTouch.CarregarDocumento(AIdVenda: Integer);
begin
  cdsVenda.Fechar;
  cdsVenda.Params.Clear;
  cdsVenda.FetchParams;
  cdsVenda.Params.ParamByName('id').AsInteger := AIdVenda;
  cdsVenda.Abrir;

  if not Assigned(cdsVenda.AfterInsert) then
    ConfigurarEventoInserirValoresPadrao(cdsVenda);

  if not cdsVenda.IsEmpty then
  begin
    cxpcMain.ActivePage := tsVenda;
    cdsVenda.Alterar;

    GerenciarControles;

    AcaoDaTelaAposCarregarDocumento;
  end
  else
  begin
    TFrmMessage.Information('Venda n�o encontrada. Possivelmente foi exclu�da por outro usu�rio');
  end;
end;

procedure TMovVendaVarejoTouch.cdsVendaAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if cdsVendaID.AsInteger > 0 then
  begin
    informacoesCodigo.Caption :=
      TStringUtils.PrimeiraLetraMaiuscula(cdsVendaTIPO.AsString) + ':' +
      cdsVendaID.AsString;

    informacoesSituacao.Caption := TStringUtils.PrimeiraLetraMaiuscula(
      cdsVendaSTATUS.AsString);
  end;
end;

procedure TMovVendaVarejoTouch.cdsVendaItemAfterDelete(DataSet: TDataSet);
begin
  inherited;
  AtualizarNumeroItemDosItensDaVenda;
  TotalizarValoresVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.cdsVendaItemAfterPost(DataSet: TDataSet);
begin
  inherited;
  TotalizarValoresVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.cdsVendaItemBeforePost(DataSet: TDataSet);
begin
  inherited;
  cdsVendaItemVL_LIQUIDO.AsFloat :=
    cdsVendaItemQUANTIDADE.AsFloat * cdsVendaItemVL_BRUTO.AsFloat;
end;

procedure TMovVendaVarejoTouch.cdsVendaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsVendaDH_CADASTRO.AsDateTime := Now;
  cdsVendaID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_VENDA_VAREJO, 0);
  cdsVendaID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;
  cdsVendaID_VENDEDOR.AsInteger := TSistema.Sistema.Usuario.id;
  cdsVendaVL_TOTAL_PRODUTO.AsFloat := 0;
  cdsVendaVL_VENDA.AsFloat := 0;
  cdsVendaVL_DESCONTO.AsFloat := 0;
  cdsVendaPERC_DESCONTO.AsFloat := 0;
  cdsVendaVL_ACRESCIMO.AsFloat := 0;
  cdsVendaPERC_ACRESCIMO.AsFloat := 0;

  cdsVendaSTATUS.AsString := STATUS_ABERTO;
  cdsVendaTIPO.AsString := TIPO_DOCUMENTO_PEDIDO;

  PreencherPlanoContaAVista;

  //Plano de Pagamento
  cdsVendaID_PLANO_PAGAMENTO.AsInteger := FParametroPlanoPagamentoAVista.AsInteger;
  cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO.AsString := TPlanoPagamento.GetDescricaoPlanoPagamento(cdsVendaID_PLANO_PAGAMENTO.AsInteger);
end;

procedure TMovVendaVarejoTouch.cxGrid2DBCardView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  SalvarProduto;
  cxGridProdutos.SetFocus;
end;

procedure TMovVendaVarejoTouch.cxGridDBCardView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  ProcessarProdutosPorGrupo;
end;

procedure TMovVendaVarejoTouch.ProcessarProdutosPorGrupo;
begin
  try
    TFrmMessage_Process.SendMessage('Carregando produtos do grupo selecionado');
    MostrarProdutosPorGrupo;
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  if cdsProdutoGrupoTabelaPreco.IsEmpty then
    TFrmMessage_Process.SendMessageTime('O grupo n�o possui produtos cadastrados!', 1500);
end;

procedure TMovVendaVarejoTouch.cxGridProdutosViewDblClick(Sender: TObject);
begin
  inherited;
  if not cdsVendaItem.IsEmpty then
    AlterarProduto;
end;

procedure TMovVendaVarejoTouch.cxGridProdutosViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
  begin
    cdsVendaItem.Delete;
  end;
end;

procedure TMovVendaVarejoTouch.FechamentoVenda;
var AValorRecebido, AValorTroco: Double;
begin
  ValidarCamposObrigatorios;

  cdsVendaVL_PAGAMENTO.AsFloat := cdsVendaVL_VENDA.AsFloat;
  ActEfetivarVenda.Execute;
end;

procedure TMovVendaVarejoTouch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SalvarConfiguracaoGrid;
  FWhereSearch.Free;
  inherited;
end;

procedure TMovVendaVarejoTouch.FormCreate(Sender: TObject);
begin
  inherited;
  AoInicializarFormulario;
end;

procedure TMovVendaVarejoTouch.GerarParcela(Sender: TField);
var
  planoPagamentoValido: Boolean;
  valorTituloValido: Boolean;
begin
  {if not(FPodeExecutarGeracaoParcela) then
    exit;

  planoPagamentoValido := (not(cdsVendaID_PLANO_PAGAMENTO.AsString.IsEmpty) or
    (TDatasetUtils.ValorFoiAlterado(cdsVendaID_PLANO_PAGAMENTO)));

  valorTituloValido := (cdsVendaVL_VENDA.AsFloat > 0) or
    TDatasetUtils.ValorFoiAlterado(cdsVendaVL_VENDA);

  if (planoPagamentoValido) and
     (valorTituloValido) then
  begin
    GerarParcelamento;
  end;}
end;

procedure TMovVendaVarejoTouch.GerarParcelamento;
begin
  {FPodeExecutarGeracaoParcela := false;
  try
    TVenda.LimparParcelas(cdsVendaParcela);
    TVenda.GerarParcelas(cdsVenda, cdsVendaParcela);
  finally
    FPodeExecutarGeracaoParcela := true;
    HabilitarBotaoEfetivar;
  end;    }
end;

procedure TMovVendaVarejoTouch.GerenciarControles;
var
  possuiProdutos: boolean;
  estaVendendo: boolean;
  estaFaturando: boolean;
  estaPesquisando: boolean;
  documentoAberto: Boolean;
  documentoCancelado: Boolean;
  documentoFechado: Boolean;
  possuiAlteracoes: Boolean;
  documentoEstaSalvoNoBancoDados: Boolean;
begin
  TControlsUtils.CongelarFormulario(Self);
  try
    estaVendendo := (cxpcMain.ActivePage = tsVenda);
    estaFaturando := (cxpcMain.ActivePage = tsFechamento);
    estaPesquisando := (cxpcMain.ActivePage = tsPesquisa);
    possuiProdutos := not(cdsVendaItem.IsEmpty);
    documentoAberto := cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO);
    documentoCancelado := cdsVendaSTATUS.AsString.Equals(STATUS_CANCELADO);
    documentoFechado := cdsVendaSTATUS.AsString.Equals(STATUS_FECHADO);
    possuiAlteracoes := cdsVenda.ChangeCount > 0;
    documentoEstaSalvoNoBancoDados := cdsVendaID.AsInteger > 0;

    ActVoltar.Visible := estaFaturando or estaPesquisando;
    ActVenda.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActDevolucao.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActOrcamento.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActPedido.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActCondicional.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActCancelarVenda.Visible := estaVendendo and documentoFechado;
    ActCancel.Visible := not(estaPesquisando) and possuiAlteracoes;

    ActExcluir.Visible := not(estaPesquisando) and documentoAberto
      and documentoEstaSalvoNoBancoDados;

    ActExcluir.Enabled := ActExcluir.Visible;

    ActImprimir.Visible := not(documentoAberto) or estaPesquisando;

    barConsultar.Visible := estaPesquisando;

    HabilitarBotaoEfetivar;

    BarAcao.Visible := ActVoltar.Visible or ActPedido.Visible or ActOrcamento.Visible or
      ActCondicional.Visible or ActVenda.Visible or ActDevolucao.Visible or
      ActEfetivarVenda.Visible or ActCancelarVenda.Visible or ActPesquisar.Visible or
      ActCancel.Visible or ActExcluir.Visible;

    BarInformacoes.Visible := cdsVendaID.AsInteger > 0;
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovVendaVarejoTouch.HabilitarBotaoEfetivar;
var estaFaturando, possuiParcelamento: Boolean;
begin
  estaFaturando := (cxpcMain.ActivePage = tsFechamento);
  possuiParcelamento := not cdsVendaParcela.IsEmpty;

  ActEfetivarVenda.Visible := estaFaturando and possuiParcelamento;
end;

procedure TMovVendaVarejoTouch.ImprimirDocumento;
var tempList : TStrings;
    impressaoMp2032 : TImpressaoMP2032;
    cTipo : String;
    I: Integer;
begin
  if IniSystemFile.dll_bematech = '' then
    exit;

  TempList := TStringList.Create;
  try
    impressaoMp2032 := TImpressaoMP2032.Create(IniSystemFile.dll_bematech, AnsiString(IniSystemFile.porta_bematech));
    Try
      try
        With impressaoMp2032 Do
        Begin
          arquivo:= ExtractFilePath(Application.ExeName)+IniSystemFile.arquivo_impressao;
          tp_impressora:= IniSystemFile.tipo_impressora;
          itens:= cdsVendaItem;

          Imprimir;
        End;
      except
        FrmMessage.Failure('Problema na comunica��o com a impressora. '+
            'Verifique se o dispositivo est� devidamente instalado e configurado.');
      end;
    finally
      try
       FreeAndNil( impressaoMp2032 );
      except
      end;
    end;
    Application.ProcessMessages;
  finally
    FreeAndNil(TempList);
  end;
end;

procedure TMovVendaVarejoTouch.InserirValoresPadrao(DataSet: TDataSet);
begin
  TUsuarioDesignControl.CarregarValoresPadrao(DataSet,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TMovVendaVarejoTouch.viewPesquisaDblClick(Sender: TObject);
begin
  inherited;
  CarregarDocumento(fdmSearch.FieldByName('id').AsInteger);
end;

procedure TMovVendaVarejoTouch.viewPesquisaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  CarregarDocumento(fdmSearch.FieldByName('id').AsInteger);
end;

procedure TMovVendaVarejoTouch.LimparVenda;
begin
  cdsVenda.CancelUpdates;
end;

procedure TMovVendaVarejoTouch.MostrarInformacoesImplantacao;
begin
  inherited;
  TControlFunction.CreateMDIChild(TFrmHelpImplantacaoVendaTouch,
    FrmHelpImplantacaoVendaTouch);
end;

procedure TMovVendaVarejoTouch.MostrarProdutosPorGrupo;
begin
  cdsProdutoGrupoTabelaPreco.Close;
  cdsProdutoGrupoTabelaPreco.Params.ParamByName('id_grupo_produto').AsInteger :=
    cdsTodosGruposProdutoID.AsInteger;
  cdsProdutoGrupoTabelaPreco.Params.ParamByName('id_tabela_preco').AsInteger :=
    cdsVendaID_TABELA_PRECO.AsInteger;
  cdsProdutoGrupoTabelaPreco.Open;
end;

procedure TMovVendaVarejoTouch.NovaVenda;
begin
  cxpcMain.ActivePage := tsVenda;
  cdsVenda.Fechar;
  cdsVenda.Params.Clear;
  cdsVenda.FetchParams;
  cdsVenda.Params.ParamByName('id').AsInteger := CID_NULL;

  cdsVenda.Abrir;

  if not Assigned(cdsVenda.AfterInsert) then
    ConfigurarEventoInserirValoresPadrao(cdsVenda);

  cdsVenda.Incluir;

  GerenciarControles;
end;

procedure TMovVendaVarejoTouch.PesquisarVenda;
begin
  Consultar;
end;

procedure TMovVendaVarejoTouch.SalvarConfiguracaoGrid;
begin
  TUsuarioGridView.SaveGridView(viewPesquisa,
    TSistema.Sistema.usuario.idSeguranca, Self.Name+viewPesquisa.Name);

  TUsuarioGridView.SaveGridView(viewProduto,
    TSistema.Sistema.usuario.idSeguranca, Self.Name+viewProduto.Name);

  TUsuarioGridView.SaveGridView(viewFechamento,
    TSistema.Sistema.usuario.idSeguranca, Self.Name+viewFechamento.Name);
end;

procedure TMovVendaVarejoTouch.SalvarProduto;
var produtoExiste: Boolean;
begin
  produtoExiste := cdsVendaItem.Locate('ID_PRODUTO',
    cdsProdutoGrupoTabelaPrecoID.AsInteger,[]);

  if produtoExiste then
  begin
    AdicionarQuantidade(cdsProdutoGrupoTabelaPrecoID.AsInteger, 1);
  end
  else
  begin
    cdsVendaItem.Incluir;

    cdsVendaItemID_PRODUTO.AsInteger := cdsProdutoGrupoTabelaPrecoID.AsInteger;
    cdsVendaItemNR_ITEM.AsInteger := cdsVendaItem.RecordCount + 1;
    cdsVendaItemQUANTIDADE.AsFloat := 1;

    cdsVendaItemVL_DESCONTO.AsFloat := 0;
    cdsVendaItemVL_ACRESCIMO.AsFloat := 0;

    cdsVendaItemVL_BRUTO.AsFloat := cdsProdutoGrupoTabelaPrecoVL_VENDA.AsFloat;

    cdsVendaItemVL_LIQUIDO.AsFloat :=
      cdsProdutoGrupoTabelaPrecoVL_VENDA.AsFloat * cdsVendaItemQUANTIDADE.AsFloat;

    cdsVendaItemPERC_DESCONTO.AsFloat := 0;
    cdsVendaItemPERC_ACRESCIMO.AsFloat := 0;
    cdsVendaItemJOIN_DESCRICAO_PRODUTO.AsString := cdsProdutoGrupoTabelaPrecoDESCRICAO.AsString;
    cdsVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE.AsString := 'UN';
    cdsVendaItemJOIN_ESTOQUE_PRODUTO.AsFloat := 0;
    cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO.AsString := cdsProdutoGrupoTabelaPrecoCODIGO_BARRA.AsString;

    cdsVendaItem.Salvar;
  end;
end;

procedure TMovVendaVarejoTouch.TotalizarValoresVenda;
var
  cdsClone: TClientDataset;
  valorDesconto: Double;
  valorAcrescimo: Double;
  valorBruto: Double;
  valorLiquido: Double;
begin
  valorDesconto := 0;
  valorAcrescimo := 0;
  valorBruto := 0;
  valorLiquido := 0;

  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(cdsVendaItem), true);
  try
    cdsClone.First;
    while not cdsClone.Eof do
    begin
      valorDesconto := valorDesconto + cdsClone.FieldByName('VL_DESCONTO').AsFloat;
      valorAcrescimo := valorAcrescimo + cdsClone.FieldByName('VL_ACRESCIMO').AsFloat;

      valorBruto := valorBruto + (cdsClone.FieldByName('QUANTIDADE').AsFloat *
        cdsClone.FieldByName('VL_BRUTO').AsFloat);

      valorLiquido := valorLiquido + cdsClone.FieldByName('VL_LIQUIDO').AsFloat;

      cdsClone.Next;
    end;

    cdsVendaVL_DESCONTO.AsFloat := valorDesconto;
    cdsVendaVL_ACRESCIMO.AsFloat := valorAcrescimo;
    cdsVendaVL_TOTAL_PRODUTO.AsFloat := valorBruto;
    cdsVendaVL_VENDA.AsFloat := valorLiquido;
  finally
    FreeAndNil(cdsClone);
  end;
end;

procedure TMovVendaVarejoTouch.ValidarCamposObrigatorios;
begin
  TValidacaoCampo.CampoPreenchido(cdsVendaID_PESSOA);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_CENTRO_RESULTADO);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_CONTA_ANALISE);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_TABELA_PRECO);
end;

function TMovVendaVarejoTouch.ViewAtivaNaTela: TcxGridDBBandedTableView;
begin
  if cxpcMain.ActivePage = tsVenda then
    result := viewProduto
  else if cxpcMain.ActivePage = tsFechamento then
    result := viewFechamento
  else if cxpcMain.ActivePage = tsPesquisa then
    result := viewPesquisa;
end;

procedure TMovVendaVarejoTouch.ConfigurarEventoInserirValoresPadrao(
  ADataset: TgbClientDataset);
var i: Integer;
begin
  ADataset.AfterInsert := InserirValoresPadrao;
  for i := 0 to Pred(ADataset.FListaDetail.Count) do
  begin
    ADataset.FListaDetail[i].AfterInsert := InserirValoresPadrao;
    ConfigurarEventoInserirValoresPadrao(ADataset.FListaDetail[i]);
  end;
end;

procedure TMovVendaVarejoTouch.Consultar;
var DSList: TFDJSONDataSets;
begin
  inherited;
  try
    SetWhereBundle;
    if fdmSearch.Active then
      fdmSearch.EmptyDataSet;

    DSList := DmConnection.ServerMethodsClient.GetDados(Self.Name,
      FWhereSearch.where, TSistema.Sistema.Usuario.idSeguranca);
    try
      TDatasetUtils.JSONDatasetToMemTable(DSList, fdmSearch);

       TcxGridUtils.AdicionarTodosCamposNaView(viewPesquisa);

      TUsuarioGridView.LoadGridView(viewPesquisa,
        TSistema.Sistema.Usuario.idSeguranca, Self.Name);
    finally
      FreeAndNil(DSList);
    end;
    ClearWhereBundle;

    ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
  Except
    TFrmMessage.Failure('Falha ao consultar os dados, verifique o SQL associado a esta tela.');
    abort;
  end;
end;

procedure TMovVendaVarejoTouch.CriarTotalizadorComanda;
begin
{  with ViewProduto.GetColumnByFieldName(cdsVendaItemVL_LIQUIDO.FieldName) do
  begin
    Width := 245;
    Options.Editing := false;
    Options.Sorting := false;
    Summary.GroupFooterKind := skSum;
    Summary.GroupFooterFormat := '###,###,###,###,##0.00';
  end;}

  ViewProduto.OptionsView.Footer := false;
  ViewProduto.OptionsView.FooterMultiSummaries := false;
end;

procedure TMovVendaVarejoTouch.SetWhereBundle;
var AItemList: TcxFilterCriteriaItemList;
    BetweenCondicional: array of string;
    textoBetween: String;
begin
  inherited;
  ClearWhereBundle;

  //Numero do Documento
  if not VartoStr(filtroCodigoVenda.EditValue).IsEmpty then
    FWhereSearch.AddIgual(FIELD_CODIGO, StrtoIntDef(VartoStr(filtroCodigoVenda.EditValue), 0));

  //Data de Emissao
  if not(VartoStr(filtroDataCadastroInicial.EditValue).IsEmpty) and
     not(VartoStr(filtroDataCadastroFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataCadastroInicial.EditValue) <= VarToDateTime(filtroDataCadastroFIm.EditValue)) then
    FWhereSearch.AddEntre(FIELD_DATA_CADASTRO, StrtoDate(filtroDataCadastroInicial.EditValue),
      StrtoDate(filtroDataCadastroFIm.EditValue));

  //Data de Fechamento
  if not(VartoStr(filtroDataDocumentoInicio.EditValue).IsEmpty) and
     not(VartoStr(filtroDataDocumentoFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataDocumentoInicio.EditValue) <= VarToDateTime(filtroDataDocumentoFIm.EditValue)) then
   FWhereSearch.AddEntre(FIELD_DATA_FECHAMENTO, StrtoDate(filtroDataDocumentoInicio.EditValue),
     StrtoDate(filtroDataDocumentoFIm.EditValue));

  //Tipo do Documento
  if not VartoStr(filtroTipoDocumento.Text).Equals(FILTRO_TODOS) then
    FWhereSearch.AddIgual(FIELD_TIPO_DOCUMENTO,VartoStr(filtroTipoDocumento.Text));

  //Status
  if not VartoStr(filtroSituacao.Text).Equals(FILTRO_TODOS) then
    FWhereSearch.AddIgual(FIELD_SITUACAO_DOCUMENTO,VartoStr(filtroSituacao.Text));
end;

procedure TMovVendaVarejoTouch.SubtrairQuantidade(AIdProduto: Integer;
  AQuantidade: Double);
var savePlace: TBookMark;
   lGotoBookMark: Boolean;
begin
  if cdsVendaItem.IsEmpty then
    Exit;

  savePlace := cdsVendaItem.GetBookmark;

  //Adiciona/Remove Quantidade
  cdsVendaItem.Edit;
  cdsVendaItemQUANTIDADE.AsFloat := cdsVendaItemQUANTIDADE.AsFloat - AQuantidade;
  cdsVendaItem.Post;

  //Se a quantidade do item for 0 ou negativo ent�o exclui o item da comanda
  if cdsVendaItemQUANTIDADE.AsFloat <= 0 then
  begin
    cdsVendaItem.Delete;
    lGotoBookMark := false;
  end;

  if lGotoBookMark and Assigned(savePlace) and cdsVendaItem.BookMarkValid(savePlace) then
    cdsVendaItem.GotoBookmark(savePlace);
end;

procedure TMovVendaVarejoTouch.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TMovVendaVarejoTouch.PreencherNomePessoaNoFiltro;
begin

end;

procedure TMovVendaVarejoTouch.PrepararFiltros;
begin
  FWhereSearch := TFiltroPadrao.Create;

  filtroCodigoVenda.EditValue := null;
  filtroPessoa.EditValue := null;
  filtroNomePessoa.Caption := '';
  filtroDataCadastroInicial.EditValue := Date;
  filtroDataCadastroFIm.EditValue := Date;
  filtroDataDocumentoInicio.EditValue := null;
  filtroDataDocumentoFIm.EditValue := null;
  filtroTipoDocumento.Text := FILTRO_TODOS;
  filtroSituacao.Text := FILTRO_TODOS;
end;

procedure TMovVendaVarejoTouch.ExibirFiltroDeMemoriaDaGrid(
  const AExibir: Boolean);
begin
  viewPesquisa.FilterRow.Visible := AExibir;
end;

procedure TMovVendaVarejoTouch.fdmSearchAfterClose(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(false);
end;

procedure TMovVendaVarejoTouch.fdmSearchAfterDelete(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
end;

procedure TMovVendaVarejoTouch.fdmSearchAfterOpen(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
  viewPesquisa.DataController.Groups.FullExpand;
  fdmSearch.First;
end;

procedure TMovVendaVarejoTouch.ActRestaurarColunasPadraoExecute(Sender: TObject);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+ViewAtivaNaTela.Name);

  ViewAtivaNaTela.RestoreDefaults;
end;

procedure TMovVendaVarejoTouch.ActAbrirConsultandoExecute(Sender: TObject);
var consultandoAutomaticamente: boolean;
begin
  consultandoAutomaticamente := FConsultaAutomatica;
  TUsuarioPesquisaPadrao.SetConsultaAutomatica(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name,
    not consultandoAutomaticamente);
  FConsultaAutomatica := not consultandoAutomaticamente;
end;

procedure TMovVendaVarejoTouch.ActAdicionar1Execute(Sender: TObject);
begin
  inherited;
  if cdsVendaItem.IsEmpty then
    Exit;

  AdicionarQuantidade(cdsVendaItemID_PRODUTO.AsInteger, 1);
end;

procedure TMovVendaVarejoTouch.ActAdicionar2Execute(Sender: TObject);
begin
  inherited;
  AdicionarQuantidade(cdsVendaItemID_PRODUTO.AsInteger, 2);
end;

procedure TMovVendaVarejoTouch.ActAlterarColunasGridExecute(Sender: TObject);
begin
  TFrmApelidarColunasGrid.SetColumns(ViewAtivaNaTela);
end;

procedure TMovVendaVarejoTouch.ActAlterarQuantidadeExecute(Sender: TObject);
var registroAtual: TBookmark;
begin
  inherited;
  if cdsVendaItem.IsEmpty then
    Exit;

  registroAtual := cdsVendaItem.GetBookmark;
  AlterarQuantidadePeloTeclado(cdsVendaItem.FieldByName('id_produto').AsInteger);
  cdsVendaItem.GotoBookmark(registroAtual);
end;

procedure TMovVendaVarejoTouch.ActAlterarSQLPesquisaPadraoExecute(
  Sender: TObject);
begin
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name) then
    Consultar;
end;

procedure TMovVendaVarejoTouch.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  TUsuarioGridView.SaveGridView(ViewAtivaNaTela,
    TSistema.Sistema.usuario.idSeguranca, Self.Name+ViewAtivaNaTela.Name);
end;

procedure TMovVendaVarejoTouch.ActSubtrair1Execute(Sender: TObject);
begin
  inherited;
  SubtrairQuantidade(cdsVendaItemID_PRODUTO.AsInteger, 1);
end;

procedure TMovVendaVarejoTouch.ActSubtrair2Execute(Sender: TObject);
begin
  inherited;
  SubtrairQuantidade(cdsVendaItemID_PRODUTO.AsInteger, 2);
end;

procedure TMovVendaVarejoTouch.pmGridConsultaPadraoPopup(Sender: TObject);
begin
  ActAbrirConsultando.Caption := TFunction.Iif(FConsultaAutomatica,
    'Desabilitar Consulta Autom�tica', 'Habilitar Consulta Autom�tica');
end;

procedure TMovVendaVarejoTouch.pmTituloGridPesquisaPadraoPopup(Sender: TObject);
begin
  ActExibirAgrupamento.Caption := TFunction.Iif(ViewAtivaNaTela.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');
end;

procedure TMovVendaVarejoTouch.MoveGridUpDown(Sender: TObject);
begin
  if Sender = BtnGridUpComandaItem then
  begin
    cxGridProdutos.SetFocus;
    cdsVendaItem.Prior;
    // keybd_event (VK_PRIOR, 0, 0, 0);
    //keybd_event (VK_PRIOR, 0, KEYEVENTF_KEYUP, 0);
  end
  else if Sender = BtnGridDownComandaItem then
  begin
    cxGridProdutos.SetFocus;
    cdsVendaItem.Next;
    //keybd_event(VK_NEXT, 0, 0, 0);
    //keybd_event(VK_NEXT, 0, KEYEVENTF_KEYUP, 0);
  end
  else if Sender = BtnGridUpProdutos then
  begin
    cxGrid2.SetFocus;
    //cdsProduto.Prior;
    keybd_event(VK_PRIOR, 0, 0, 0);
    keybd_event(VK_PRIOR, 0, KEYEVENTF_KEYUP, 0);
  end
  else if Sender = BtnGridDownProdutos then
  begin
    cxGrid2.SetFocus;
    //cdsProduto.Next;
    keybd_event(VK_NEXT, 0, 0, 0);
    keybd_event(VK_NEXT, 0, KEYEVENTF_KEYUP, 0);
  end
  else if Sender = BtnGridUpGrupoProdutos then
  begin
    cxGrid1.SetFocus;
    //cdsGrupoProduto.Prior;
    keybd_event(VK_PRIOR, 0, 0, 0);
    keybd_event(VK_PRIOR, 0, KEYEVENTF_KEYUP, 0);
  end
  else if Sender = BtnGridDownGrupoProdutos then
  begin
    cxGrid1.SetFocus;
    //cdsGrupoProduto.Next;
    keybd_event(VK_NEXT, 0, 0, 0);
    keybd_event(VK_NEXT, 0, KEYEVENTF_KEYUP, 0);
  end;

  cxGridProdutos.SetFocus;
end;

procedure TMovVendaVarejoTouch.PreencherPlanoContaAVista;
begin
  //Centro de Resultado
  cdsVendaID_CENTRO_RESULTADO.AsInteger := StrtoIntDef(FParametroCentroResultadoAVista.valor,0);
  cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO.AsString := TPlanoConta.GetDescricaoCentroResultado(cdsVendaID_CENTRO_RESULTADO.AsInteger);

  //Dados Conta de Analise
  cdsVendaID_CONTA_ANALISE.AsInteger := StrtoIntDef(FParametroContaAnaliseAVista.valor,0);
  TPesqContaAnalise.ExecutarConsultaOculta(cdsVendaID_CENTRO_RESULTADO.AsInteger,
  cdsVendaID_CONTA_ANALISE, cdsVendaJOIN_DESCRICAO_CONTA_ANALISE);
end;

Initialization
  RegisterClass(TMovVendaVarejoTouch);

Finalization
  UnRegisterClass(TMovVendaVarejoTouch);

end.


