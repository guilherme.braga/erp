unit uRelEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxButtonEdit, cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, uGBClientDataset, dxBar, cxBarEditItem, dxBarExtItems,
  System.Actions, Vcl.ActnList, cxVGrid, cxInplaceContainer, uGBPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxGroupBox, dxRibbon;

type
  TRelEstoque = class(TFrmRelatorioFRPadrao)
    FdmFiltrosID_GRUPO_PRODUTO: TIntegerField;
    FdmFiltrosID_SUB_GRUPO_PRODUTO: TIntegerField;
    FdmFiltrosID_MARCA: TIntegerField;
    FdmFiltrosID_CATEGORIA: TIntegerField;
    FdmFiltrosID_SUB_CATEGORIA: TIntegerField;
    FdmFiltrosID_LINHA: TIntegerField;
    FdmFiltrosID_MODELO: TIntegerField;
    FdmFiltrosID_UNIDADE_ESTOQUE: TIntegerField;
    FdmFiltrosTIPO: TStringField;
    FdmFiltrosCODIGO_BARRA: TIntegerField;
    FdmFiltrosDESCRICAO: TStringField;
    FdmFiltrosID: TIntegerField;
    rowGrupoProduto: TcxEditorRow;
    rowSubGrupoProduto: TcxEditorRow;
    rowMarca: TcxEditorRow;
    rowCategoria: TcxEditorRow;
    rowSubCategoria: TcxEditorRow;
    rowLinha: TcxEditorRow;
    rowModelo: TcxEditorRow;
    rowUnidadeEstoque: TcxEditorRow;
    rowTipo: TcxEditorRow;
    rowProduto: TcxEditorRow;
    procedure rowGrupoProdutoEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowSubGrupoProdutoEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowMarcaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowCategoriaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowSubCategoriaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowLinhaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowModeloEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowUnidadeEstoqueEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure rowProdutoEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private

  public

  protected
    procedure SetWhereBundle; override;
  end;

var
  RelEstoque: TRelEstoque;

implementation

{$R *.dfm}

uses uProdutoProxy, uCadCategoriaProduto, uProduto, uFrmConsultaPadrao;

{ TRelEstoque }

procedure TRelEstoque.rowCategoriaEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TCategoriaProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowCategoria.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowGrupoProdutoEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TGrupoProdutoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowGrupoProduto.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowLinhaEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TLinhaProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowLinha.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowMarcaEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TMarcaProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowMarca.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowModeloEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TModeloProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowModelo.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowProdutoEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TProdutoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowProduto.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowSubCategoriaEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TSubCategoriaProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowSubCategoria.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowSubGrupoProdutoEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TGrupoProdutoProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowGrupoProduto.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.rowUnidadeEstoqueEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TUnidadeEstoqueProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowUnidadeEstoque.Properties.Value := IDConsultado;
  end;
end;

procedure TRelEstoque.SetWhereBundle;
begin
  inherited;
  AddFiltroIgual(TProdutoProxy.FIELD_ID_GRUPO_PRODUTO, FdmFiltrosID_GRUPO_PRODUTO);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_SUB_GRUPO_PRODUTO, FdmFiltrosID_SUB_GRUPO_PRODUTO);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_MARCA, FdmFiltrosID_MARCA);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_CATEGORIA, FdmFiltrosID_CATEGORIA);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_SUB_CATEGORIA, FdmFiltrosID_SUB_CATEGORIA);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_LINHA, FdmFiltrosID_LINHA);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_MODELO, FdmFiltrosID_MODELO);
  AddFiltroIgual(TProdutoProxy.FIELD_ID_UNIDADE_ESTOQUE, FdmFiltrosID_UNIDADE_ESTOQUE);
  AddFiltroIgual(TProdutoProxy.FIELD_TIPO, FdmFiltrosTIPO);
  AddFiltroIgual(TProdutoProxy.FIELD_CODIGO_BARRA, FdmFiltrosCODIGO_BARRA);
  AddFiltroIgual(TProdutoProxy.FIELD_DESCRICAO, FdmFiltrosDESCRICAO);
  AddFiltroIgual(TProdutoProxy.FIELD_ID, FdmFiltrosID);
end;

initialization
  RegisterClass(TRelEstoque);

Finalization
  UnRegisterClass(TRelEstoque);

end.
