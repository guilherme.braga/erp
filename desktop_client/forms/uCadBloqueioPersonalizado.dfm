inherited CadBloqueioPersonalizado: TCadBloqueioPersonalizado
  Caption = 'Bloqueio Personalizado'
  ClientHeight = 422
  ClientWidth = 848
  ExplicitWidth = 864
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 848
    ExplicitWidth = 848
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 848
    Height = 295
    Properties.ActivePage = cxtsData
    ExplicitWidth = 848
    ExplicitHeight = 295
    ClientRectBottom = 295
    ClientRectRight = 848
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 848
      ExplicitHeight = 271
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 816
        Height = 271
        ExplicitWidth = 816
        ExplicitHeight = 271
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 271
        Height = 271
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 271
        ExplicitHeight = 271
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 848
      ExplicitHeight = 271
      inherited DesignPanel: TJvDesignPanel
        Width = 848
        Height = 271
        ExplicitWidth = 848
        ExplicitHeight = 271
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 848
          ExplicitHeight = 271
          Height = 271
          Width = 848
          inherited dxBevel1: TdxBevel
            Width = 844
            ExplicitWidth = 868
          end
          object Label4: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 57
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object PnFrameBloqueioPersonalizadoUsuarioLiberacao: TgbPanel
            Left = 351
            Top = 63
            Align = alRight
            Alignment = alCenterCenter
            Caption = 'TFrameBloqueioPersonalizadoUsuarioLiberacao'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            ExplicitLeft = 352
            Height = 206
            Width = 495
            object Label3: TLabel
              AlignWithMargins = True
              Left = 5
              Top = 5
              Width = 485
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Usu'#225'rios Autorizados'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 120
            end
          end
          object gbPanel2: TgbPanel
            Left = 2
            Top = 63
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            ExplicitWidth = 471
            Height = 206
            Width = 349
            object Label2: TLabel
              AlignWithMargins = True
              Left = 5
              Top = 5
              Width = 339
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Bloqueio SQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 72
            end
            object gbDBMemo1: TgbDBMemo
              Left = 2
              Top = 21
              Align = alClient
              DataBinding.DataField = 'SQL'
              DataBinding.DataSource = dsData
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              ExplicitWidth = 344
              Height = 183
              Width = 345
            end
          end
          object gbPanel3: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            DesignSize = (
              844
              29)
            Height = 29
            Width = 844
            object Label1: TLabel
              Left = 6
              Top = 8
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 55
              Top = 4
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 293
            end
            object gbDBCheckBox1: TgbDBCheckBox
              Left = 783
              Top = 4
              Anchors = [akTop, akRight]
              Caption = 'Ativo?'
              DataBinding.DataField = 'BO_ATIVO'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              Transparent = True
              Width = 55
            end
            object gbDBCheckBox2: TgbDBCheckBox
              Left = 664
              Top = 4
              Anchors = [akTop, akRight]
              Caption = 'Aplicar para Venda?'
              DataBinding.DataField = 'BO_VENDA'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              Transparent = True
              Width = 122
            end
            object gbDBCheckBox3: TgbDBCheckBox
              Left = 490
              Top = 4
              Anchors = [akTop, akRight]
              Caption = 'Aplicar para Ordem de Servi'#231'o?'
              DataBinding.DataField = 'BO_ORDEM_SERVICO'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              Transparent = True
              Width = 176
            end
            object gbDBCheckBox4: TgbDBCheckBox
              Left = 348
              Top = 4
              Anchors = [akTop, akRight]
              Caption = 'Aplicar para Nota Fiscal?'
              DataBinding.DataField = 'BO_NOTA_FISCAL'
              DataBinding.DataSource = dsData
              Properties.ImmediatePost = True
              Properties.ValueChecked = 'S'
              Properties.ValueUnchecked = 'N'
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Width = 144
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspBloqueioPersonalizado'
    RemoteServer = DmConnection.dspBloqueioPersonalizado
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataSQL: TBlobField
      FieldName = 'SQL'
      Origin = '`SQL`'
      Required = True
    end
    object cdsDataBO_VENDA: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Utilizar na Venda'
      FieldName = 'BO_VENDA'
      Origin = 'BO_VENDA'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_NOTA_FISCAL: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Utilizar na Nota Fiscal'
      FieldName = 'BO_NOTA_FISCAL'
      Origin = 'BO_NOTA_FISCAL'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_ORDEM_SERVICO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Utilizar na Ordem de Servi'#231'o'
      FieldName = 'BO_ORDEM_SERVICO'
      Origin = 'BO_ORDEM_SERVICO'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDatafdqBloqueioPersonalizadoLib: TDataSetField
      FieldName = 'fdqBloqueioPersonalizadoLib'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42345.917350520830000000
      BuiltInReportLink = True
    end
  end
  object cdsBloqueioPersonalizadoUsuarioLiberacao: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqBloqueioPersonalizadoLib
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 736
    Top = 80
    object cdsBloqueioPersonalizadoUsuarioLiberacaoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsBloqueioPersonalizadoUsuarioLiberacaoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsBloqueioPersonalizadoUsuarioLiberacaoID_BLOQUEIO_PERSONALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bloqueio Personalizado'
      FieldName = 'ID_BLOQUEIO_PERSONALIZADO'
      Origin = 'ID_BLOQUEIO_PERSONALIZADO'
    end
    object cdsBloqueioPersonalizadoUsuarioLiberacaoJOIN_NOME_PESSOA_USUARIO: TStringField
      DisplayLabel = 'Nome do Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
end
