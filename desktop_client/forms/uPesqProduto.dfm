inherited PesqProduto: TPesqProduto
  Caption = 'Pesquisa de Produto'
  ClientHeight = 424
  ClientWidth = 933
  ExplicitWidth = 939
  ExplicitHeight = 453
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 933
    ExplicitHeight = 368
    Height = 368
    Width = 933
    object gbFiltrosPesquisa: TgbGroupBox
      AlignWithMargins = True
      Left = 5
      Top = 5
      Align = alLeft
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      DesignSize = (
        316
        358)
      Height = 358
      Width = 316
      object labelDescricao: TLabel
        Left = 8
        Top = 86
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
      end
      object labelGrupo: TLabel
        Left = 8
        Top = 133
        Width = 29
        Height = 13
        Caption = 'Grupo'
      end
      object labelSubgrupo: TLabel
        Left = 8
        Top = 158
        Width = 50
        Height = 13
        Caption = 'Sub Grupo'
      end
      object labelCategoria: TLabel
        Left = 8
        Top = 183
        Width = 47
        Height = 13
        Caption = 'Categoria'
      end
      object labelSubCategoria: TLabel
        Left = 8
        Top = 208
        Width = 68
        Height = 13
        Caption = 'Sub Categoria'
      end
      object labelMarcar: TLabel
        Left = 8
        Top = 281
        Width = 29
        Height = 13
        Caption = 'Marca'
      end
      object labelLinha: TLabel
        Left = 8
        Top = 306
        Width = 25
        Height = 13
        Caption = 'Linha'
      end
      object labelModelo: TLabel
        Left = 8
        Top = 231
        Width = 34
        Height = 13
        Caption = 'Modelo'
      end
      object labelUniddeEstoque: TLabel
        Left = 8
        Top = 256
        Width = 59
        Height = 13
        Caption = 'Un. Estoque'
      end
      object labelTipo: TLabel
        Left = 8
        Top = 110
        Width = 20
        Height = 13
        Caption = 'Tipo'
      end
      object Label1: TLabel
        Left = 8
        Top = 38
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
      end
      object Label2: TLabel
        Left = 8
        Top = 13
        Width = 62
        Height = 13
        Caption = 'Dt. Cadastro'
      end
      object Label3: TLabel
        Left = 8
        Top = 63
        Width = 82
        Height = 13
        Caption = 'C'#243'digo de Barras'
      end
      object edDescricao: TgbDBTextEdit
        Left = 94
        Top = 83
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'DESCRICAO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        gbPassword = False
        Width = 219
      end
      object descGrupo: TgbDBTextEdit
        Left = 138
        Top = 130
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 6
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descSubGrupo: TgbDBTextEdit
        Left = 138
        Top = 155
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_SUB_GRUPO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 16
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descCategoria: TgbDBTextEdit
        Left = 138
        Top = 180
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_CATEGORIA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 17
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descSubCategoria: TgbDBTextEdit
        Left = 138
        Top = 205
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_SUB_CATEGORIA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 18
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descLinha: TgbDBTextEdit
        Left = 138
        Top = 303
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_LINHA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 19
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descMarca: TgbDBTextEdit
        Left = 138
        Top = 278
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_MARCA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 20
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descUnidadeEstoque: TgbDBTextEdit
        Left = 138
        Top = 253
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 21
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object descModelo: TgbDBTextEdit
        Left = 138
        Top = 228
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_MODELO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 22
        gbReadyOnly = True
        gbPassword = False
        Width = 175
      end
      object cbTipo: TcxDBComboBox
        Left = 94
        Top = 105
        DataBinding.DataField = 'TIPO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'PRODUTO'
          'SERVI'#199'O'
          'OUTROS')
        TabOrder = 4
        Width = 219
      end
      object btnPesquisar: TcxButton
        Left = 8
        Top = 329
        Width = 180
        Height = 25
        Action = actFiltrarSelecaoTitulos
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Pesquisar F7'
        TabOrder = 14
      end
      object btnLimparFiltro: TcxButton
        Left = 188
        Top = 329
        Width = 122
        Height = 25
        Action = actLimparFiltro
        Anchors = [akTop, akRight]
        TabOrder = 15
      end
      object EdtCodigo: TgbDBTextEdit
        Left = 94
        Top = 35
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'ID'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbPassword = False
        Width = 219
      end
      object EdtDataCadastro: TgbDBDateEdit
        Left = 94
        Top = 10
        DataBinding.DataField = 'DT_CADASTRO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.DateButtons = [btnClear, btnToday]
        Properties.ImmediatePost = True
        Properties.SaveTime = False
        Properties.ShowTime = False
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        gbDateTime = False
        Width = 83
      end
      object EdtCodigoBarras: TgbDBTextEdit
        Left = 94
        Top = 59
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'CODIGO_BARRAS'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.CharCase = ecUpperCase
        Style.BorderStyle = ebsOffice11
        Style.Color = clWhite
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 2
        gbPassword = False
        Width = 219
      end
      object eFkLinha: TgbDBButtonEditFK
        Left = 94
        Top = 303
        DataBinding.DataField = 'ID_LINHA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 13
        gbTextEdit = descLinha
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_LINHA;JOIN_DESCRICAO_LINHA'
        gbTableName = 'LINHA'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkUnidadeEstoque: TgbDBButtonEditFK
        Left = 94
        Top = 253
        DataBinding.DataField = 'ID_UNIDADE_ESTOQUE'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 11
        gbTextEdit = descUnidadeEstoque
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_UNIDADE_ESTOQUE;JOIN_DESCRICAO_UNIDADE_ESTOQUE'
        gbTableName = 'UNIDADE_ESTOQUE'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkModelo: TgbDBButtonEditFK
        Left = 94
        Top = 228
        DataBinding.DataField = 'ID_MODELO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 10
        gbTextEdit = descModelo
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_MODELO; JOIN_DESCRICAO_MODELO'
        gbTableName = 'MODELO'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkMarca: TgbDBButtonEditFK
        Left = 94
        Top = 278
        DataBinding.DataField = 'ID_MARCA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 12
        gbTextEdit = descMarca
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_MARCA;JOIN_DESCRICAO_MARCA'
        gbTableName = 'MARCA'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkCategoria: TgbDBButtonEditFK
        Left = 94
        Top = 180
        DataBinding.DataField = 'ID_CATEGORIA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 8
        gbTextEdit = descCategoria
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_CATEGORIA;JOIN_DESCRICAO_CATEGORIA'
        gbTableName = 'CATEGORIA'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkSubCategoria: TgbDBButtonEditFK
        Left = 94
        Top = 205
        DataBinding.DataField = 'ID_SUB_CATEGORIA'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 9
        gbTextEdit = descSubCategoria
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_SUB_CATEGORIA; JOIN_DESCRICAO_SUB_CATEGORIA'
        gbTableName = 'SUB_CATEGORIA'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkGrupo: TgbDBButtonEditFK
        Left = 94
        Top = 130
        DataBinding.DataField = 'ID_GRUPO_PRODUTO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 5
        gbTextEdit = descGrupo
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_GRUPO_PRODUTO;JOIN_DESCRICAO_GRUPO_PRODUTO'
        gbTableName = 'GRUPO_PRODUTO'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
      object eFkSubGrupo: TgbDBButtonEditFK
        Left = 94
        Top = 155
        DataBinding.DataField = 'ID_SUB_GRUPO_PRODUTO'
        DataBinding.DataSource = dsCamposPesquisa
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Style.Color = clWhite
        TabOrder = 7
        gbTextEdit = descSubGrupo
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_SUB_GRUPO_PRODUTO;JOIN_DESCRICAO_SUB_GRUPO'
        gbTableName = 'SUB_GRUPO_PRODUTO'
        gbCamposConsulta = 'ID;DESCRICAO'
        Width = 47
      end
    end
    object gbPanel3: TgbPanel
      Left = 324
      Top = 2
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 364
      Width = 607
      object gbGridPesquisa: TgbGroupBox
        AlignWithMargins = True
        Left = 5
        Top = 5
        Align = alClient
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Height = 354
        Width = 597
        object gridProdutos: TcxGrid
          Left = 2
          Top = 5
          Width = 593
          Height = 347
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Images = DmAcesso.cxImage16x16
          ParentFont = False
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object ViewSelecaoProdutos: TcxGridDBBandedTableView
            OnDblClick = ViewSelecaoProdutosDblClick
            OnKeyDown = ViewSelecaoProdutosKeyDown
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Images = DmAcesso.cxImage16x16
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsSelecaoTitulos
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.Active = True
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            Images = DmAcesso.cxImage16x16
            OptionsBehavior.NavigatorHints = True
            OptionsBehavior.ExpandMasterRowOnDblClick = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsCustomize.NestedBands = False
            OptionsData.CancelOnExit = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.ShowColumnFilterButtons = sfbAlways
            OptionsView.BandHeaders = False
            Styles.ContentOdd = DmAcesso.OddColor
            Bands = <
              item
              end>
          end
          object cxGridLevel1: TcxGridLevel
            GridView = ViewSelecaoProdutos
          end
        end
      end
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 368
    ExplicitTop = 368
    ExplicitWidth = 933
    Width = 933
    inherited BtnConfirmar: TcxButton
      Left = 781
      ExplicitLeft = 781
    end
    inherited BtnCancelar: TcxButton
      Left = 856
      ExplicitLeft = 856
    end
    object BtnSair: TcxButton
      Left = 706
      Top = 2
      Width = 75
      Height = 52
      Align = alRight
      Action = ActClosePesqProduto
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
    end
    object cxButton1: TcxButton
      Left = 552
      Top = 2
      Width = 154
      Height = 52
      Align = alRight
      Action = ActSelecionarTodosProdutos
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 3
    end
  end
  inherited ActionList: TActionList
    Top = 78
    object ActClosePesqProduto: TAction
      Caption = 'Sair Esc'
      ImageIndex = 12
      ShortCut = 27
      Visible = False
      OnExecute = ActClosePesqProdutoExecute
    end
    object ActSelecionarTodosProdutos: TAction
      Caption = 'Adicionar Todos Produtos F5'
      ImageIndex = 3
      OnExecute = ActSelecionarTodosProdutosExecute
    end
  end
  object fdmCamposPesquisa: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 384
    Top = 143
    object fdmCamposPesquisaDT_CADASTRO: TDateField
      FieldName = 'DT_CADASTRO'
    end
    object fdmCamposPesquisaID: TIntegerField
      FieldName = 'ID'
    end
    object fdmCamposPesquisaCODIGO_BARRAS: TStringField
      FieldName = 'CODIGO_BARRAS'
      Size = 50
    end
    object fdmCamposPesquisaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object fdmCamposPesquisaTIPO: TStringField
      FieldName = 'TIPO'
      Size = 200
    end
    object fdmCamposPesquisaID_GRUPO_PRODUTO: TIntegerField
      FieldName = 'ID_GRUPO_PRODUTO'
    end
    object fdmCamposPesquisaID_SUB_GRUPO_PRODUTO: TIntegerField
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
    end
    object fdmCamposPesquisaID_CATEGORIA: TIntegerField
      FieldName = 'ID_CATEGORIA'
    end
    object fdmCamposPesquisaID_SUB_CATEGORIA: TIntegerField
      FieldName = 'ID_SUB_CATEGORIA'
    end
    object fdmCamposPesquisaID_MODELO: TIntegerField
      FieldName = 'ID_MODELO'
    end
    object fdmCamposPesquisaID_UNIDADE_ESTOQUE: TIntegerField
      FieldName = 'ID_UNIDADE_ESTOQUE'
    end
    object fdmCamposPesquisaID_MARCA: TIntegerField
      FieldName = 'ID_MARCA'
    end
    object fdmCamposPesquisaID_LINHA: TIntegerField
      FieldName = 'ID_LINHA'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Size = 200
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_SUB_GRUPO: TStringField
      FieldName = 'JOIN_DESCRICAO_SUB_GRUPO'
      Size = 200
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_CATEGORIA: TStringField
      FieldName = 'JOIN_DESCRICAO_CATEGORIA'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_SUB_CATEGORIA: TStringField
      FieldName = 'JOIN_DESCRICAO_SUB_CATEGORIA'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_MODELO: TStringField
      FieldName = 'JOIN_DESCRICAO_MODELO'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField
      FieldName = 'JOIN_DESCRICAO_UNIDADE_ESTOQUE'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_MARCA: TStringField
      FieldName = 'JOIN_DESCRICAO_MARCA'
    end
    object fdmCamposPesquisaJOIN_DESCRICAO_LINHA: TStringField
      FieldName = 'JOIN_DESCRICAO_LINHA'
    end
  end
  object dsCamposPesquisa: TDataSource
    DataSet = fdmCamposPesquisa
    Left = 480
    Top = 111
  end
  object gpmSelecaoTitulos: TcxGridPopupMenu
    Grid = gridProdutos
    PopupMenus = <
      item
        GridView = ViewSelecaoProdutos
        HitTypes = [gvhtGridNone, gvhtNone, gvhtTab, gvhtCell, gvhtRecord, gvhtColumnHeader]
        Index = 0
        PopupMenu = pmSelecaoTitulos
      end>
    Left = 591
    Top = 56
  end
  object alSelecaoTitulos: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 681
    Top = 55
    object ActSalvarConfiguracoesTitulos: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesTitulosExecute
    end
    object actRestaurarColunasTitulos: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = actRestaurarColunasTitulosExecute
    end
    object actAlterarColunasGridTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = actAlterarColunasGridTitulosExecute
    end
    object actExibirAgrupamentoTitulos: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = actExibirAgrupamentoTitulosExecute
    end
    object actAlterarSQLPesquisaPadraoTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = actAlterarSQLPesquisaPadraoTitulosExecute
    end
    object actFiltrarSelecaoTitulos: TAction
      Category = 'filter'
      Caption = 'Pesquisar'
      ImageIndex = 11
      ShortCut = 118
      OnExecute = actFiltrarSelecaoTitulosExecute
    end
    object actLimparFiltro: TAction
      Category = 'filter'
      Caption = 'Limpar Filtro'
      ImageIndex = 21
      OnExecute = actLimparFiltroExecute
    end
  end
  object pmSelecaoTitulos: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 764
    Top = 54
    object MenuItem1: TMenuItem
      Action = actAlterarColunasGridTitulos
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object MenuItem2: TMenuItem
      Action = actExibirAgrupamentoTitulos
    end
    object MenuItem3: TMenuItem
      Action = actRestaurarColunasTitulos
    end
    object MenuItem4: TMenuItem
      Action = ActSalvarConfiguracoesTitulos
    end
    object AlterarSQL2: TMenuItem
      Action = actAlterarSQLPesquisaPadraoTitulos
    end
  end
  object dsSelecaoTitulos: TDataSource
    DataSet = fdmSelecaoTitulos
    Left = 682
    Top = 104
  end
  object fdmSelecaoTitulos: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 593
    Top = 104
  end
end
