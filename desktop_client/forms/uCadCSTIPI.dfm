inherited CadCSTIPI: TCadCSTIPI
  Caption = 'Cadastro de CST IPI'
  ClientHeight = 272
  ExplicitWidth = 952
  ExplicitHeight = 311
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Height = 145
    Properties.ActivePage = cxtsData
    ExplicitHeight = 145
    ClientRectBottom = 145
    inherited cxtsSearch: TcxTabSheet
      ExplicitHeight = 121
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 121
        ExplicitHeight = 121
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 121
        Height = 121
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 121
        ExplicitHeight = 121
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 121
      inherited DesignPanel: TJvDesignPanel
        Height = 121
        ExplicitHeight = 121
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitHeight = 121
          Height = 121
          object Label2: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 207
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label3: TLabel
            Left = 10
            Top = 40
            Width = 36
            Height = 13
            Caption = 'CST IPI'
          end
          object Label4: TLabel
            Left = 707
            Top = 40
            Width = 155
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'C'#243'digo do Enquadramento Legal'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 55
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 55
            Top = 36
            DataBinding.DataField = 'CODIGO_CST'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 146
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 259
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 442
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 868
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'CENQ'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Style.Color = clWhite
            TabOrder = 3
            gbCampoPK = 'CODIGO'
            gbCamposRetorno = 'CENQ'
            gbTableName = 'ENQUADRAMENTO_LEGAL_IPI'
            gbCamposConsulta = 'CODIGO'
            gbClasseDoCadastro = 'TCadEnquadramentoLegalPI'
            gbIdentificadorConsulta = 'ENQUADRAMENTO_LEGAL_IPI'
            Width = 60
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspCSTIPI'
    RemoteServer = DmConnection.dspCSTIPI
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataCODIGO_CST: TStringField
      DisplayLabel = 'C'#243'digo CST'
      FieldName = 'CODIGO_CST'
      Origin = 'CODIGO_CST'
      Required = True
      FixedChar = True
      Size = 2
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataCENQ: TStringField
      DisplayLabel = 'C'#243'digo do Enquadramento Legal do IPI'
      FieldName = 'CENQ'
      Origin = 'CENQ'
      Size = 3
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42358.563361145830000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
