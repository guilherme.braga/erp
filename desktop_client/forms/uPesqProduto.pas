unit uPesqProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, uGBGrid, cxDropDownEdit, cxDBEdit,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxGridBandedTableView,
  cxGridDBBandedTableView, uGBPanel, uGBGroupBox,
  uFiltroFormulario, cxCalendar, uGBDBDateEdit, JvExControls, JvButton, JvTransparentButton;

type
  TPesqProduto = class(TFrmModalPadrao)
    fdmCamposPesquisa: TgbFDMemTable;
    fdmCamposPesquisaDESCRICAO: TStringField;
    fdmCamposPesquisaTIPO: TStringField;
    fdmCamposPesquisaID_GRUPO_PRODUTO: TIntegerField;
    fdmCamposPesquisaID_SUB_GRUPO_PRODUTO: TIntegerField;
    fdmCamposPesquisaID_CATEGORIA: TIntegerField;
    fdmCamposPesquisaID_SUB_CATEGORIA: TIntegerField;
    fdmCamposPesquisaID_MODELO: TIntegerField;
    fdmCamposPesquisaID_UNIDADE_ESTOQUE: TIntegerField;
    fdmCamposPesquisaID_MARCA: TIntegerField;
    fdmCamposPesquisaID_LINHA: TIntegerField;
    dsCamposPesquisa: TDataSource;
    fdmCamposPesquisaJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_SUB_GRUPO: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_CATEGORIA: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_SUB_CATEGORIA: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_MODELO: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_MARCA: TStringField;
    fdmCamposPesquisaJOIN_DESCRICAO_LINHA: TStringField;
    gbFiltrosPesquisa: TgbGroupBox;
    labelDescricao: TLabel;
    labelGrupo: TLabel;
    labelSubgrupo: TLabel;
    labelCategoria: TLabel;
    labelSubCategoria: TLabel;
    edDescricao: TgbDBTextEdit;
    descGrupo: TgbDBTextEdit;
    descSubGrupo: TgbDBTextEdit;
    descCategoria: TgbDBTextEdit;
    descSubCategoria: TgbDBTextEdit;
    eFkSubGrupo: TgbDBButtonEditFK;
    eFkGrupo: TgbDBButtonEditFK;
    eFkSubCategoria: TgbDBButtonEditFK;
    eFkCategoria: TgbDBButtonEditFK;
    labelMarcar: TLabel;
    labelLinha: TLabel;
    labelModelo: TLabel;
    labelUniddeEstoque: TLabel;
    descLinha: TgbDBTextEdit;
    eFkMarca: TgbDBButtonEditFK;
    descMarca: TgbDBTextEdit;
    eFkModelo: TgbDBButtonEditFK;
    eFkUnidadeEstoque: TgbDBButtonEditFK;
    descUnidadeEstoque: TgbDBTextEdit;
    descModelo: TgbDBTextEdit;
    eFkLinha: TgbDBButtonEditFK;
    labelTipo: TLabel;
    cbTipo: TcxDBComboBox;
    btnPesquisar: TcxButton;
    btnLimparFiltro: TcxButton;
    gbPanel3: TgbPanel;
    gpmSelecaoTitulos: TcxGridPopupMenu;
    alSelecaoTitulos: TActionList;
    ActSalvarConfiguracoesTitulos: TAction;
    actRestaurarColunasTitulos: TAction;
    actAlterarColunasGridTitulos: TAction;
    actExibirAgrupamentoTitulos: TAction;
    actAlterarSQLPesquisaPadraoTitulos: TAction;
    actFiltrarSelecaoTitulos: TAction;
    pmSelecaoTitulos: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    AlterarSQL2: TMenuItem;
    dsSelecaoTitulos: TDataSource;
    fdmSelecaoTitulos: TFDMemTable;
    gbGridPesquisa: TgbGroupBox;
    gridProdutos: TcxGrid;
    ViewSelecaoProdutos: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    actLimparFiltro: TAction;
    Label1: TLabel;
    EdtCodigo: TgbDBTextEdit;
    Label2: TLabel;
    EdtDataCadastro: TgbDBDateEdit;
    fdmCamposPesquisaDT_CADASTRO: TDateField;
    fdmCamposPesquisaID: TIntegerField;
    fdmCamposPesquisaCODIGO_BARRAS: TStringField;
    Label3: TLabel;
    EdtCodigoBarras: TgbDBTextEdit;
    BtnSair: TcxButton;
    ActClosePesqProduto: TAction;
    ActSelecionarTodosProdutos: TAction;
    cxButton1: TcxButton;
    procedure ActSalvarConfiguracoesTitulosExecute(Sender: TObject);
    procedure actRestaurarColunasTitulosExecute(Sender: TObject);
    procedure actAlterarColunasGridTitulosExecute(Sender: TObject);
    procedure actExibirAgrupamentoTitulosExecute(Sender: TObject);
    procedure actAlterarSQLPesquisaPadraoTitulosExecute(Sender: TObject);
    procedure actFiltrarSelecaoTitulosExecute(Sender: TObject);
    procedure actLimparFiltroExecute(Sender: TObject);
    procedure ViewSelecaoProdutosDblClick(Sender: TObject);
    procedure ActClosePesqProdutoExecute(Sender: TObject);
    procedure ActSelecionarTodosProdutosExecute(Sender: TObject);
    procedure ViewSelecaoProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    function GetWhereBundleSelecao: TFiltroPadrao;
    procedure Consultar(AFiltro: TFiltroPadrao);
    function GetListaIDs: String;
    procedure AdicionarProdutoNoDatasetDestino(const ASolicitaQuantidade: Boolean = true);
  protected
    FIDs : string;
    FDatasetRetorno: TFDMemTable;
    procedure Confirmar; override;
    procedure Cancelar; override;
  public
    const NOME_CONSULTA_PRODUTO = 'PesqProdutoViewSelecaoProdutos';
    class function BuscarProdutos(): string;
    class function BuscarDatasetProdutos(var ACodigosProdutos: String): TFDMemTable;
    class procedure BuscarProdutosComRecursoDeSelecao(ADataset: TFDMemTable;
      AViewProdutos: TcxGridDBBandedTableView = nil);
    class function BuscarDatasetProdutosOculto(ACodigosProdutos: String): TFDMemTable;
  end;

var
  PesqProduto: TPesqProduto;

implementation

{$R *.dfm}

uses
  uTUsuario,
  uFrmAlterarSQLPesquisaPadrao,
  uFrmApelidarColunasGrid,
  uDatasetUtils,
  uVerticalGridUtils,
  uDmConnection,
  uDevExpressUtils,
  Data.FireDACJSONReflect, uTMessage, uFrmMessage, uProdutoProxy, uSistema, uFrmMessage_Process,
  uControlsUtils;

{ TPesqProduto }

procedure TPesqProduto.actAlterarColunasGridTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewSelecaoProdutos);
end;

procedure TPesqProduto.actAlterarSQLPesquisaPadraoTitulosExecute(
  Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(TSistema.Sistema.Usuario.idSeguranca,
    NOME_CONSULTA_PRODUTO) then
    //AbrirPesquisaPadrao;
end;

procedure TPesqProduto.ActClosePesqProdutoExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TPesqProduto.actExibirAgrupamentoTitulosExecute(
  Sender: TObject);
begin
  inherited;
  ViewSelecaoProdutos.OptionsView.GroupByBox :=
    not ViewSelecaoProdutos.OptionsView.GroupByBox;
end;

procedure TPesqProduto.actFiltrarSelecaoTitulosExecute(
  Sender: TObject);
var
  filtrosSelecaoTitulo: TFiltroPadrao;
begin
  inherited;
  Self.SelectNext(Self, true, false);
  filtrosSelecaoTitulo := GetWhereBundleSelecao;
  try
    Consultar(filtrosSelecaoTitulo);
  finally
    FreeAndNil(filtrosSelecaoTitulo);
  end;
end;

procedure TPesqProduto.actLimparFiltroExecute(Sender: TObject);
begin
  inherited;
  fdmCamposPesquisa.ClearFields;
end;

function TPesqProduto.GetWhereBundleSelecao: TFiltroPadrao;
begin
  Result := TFiltroPadrao.Create;

  if not (fdmCamposPesquisaID.AsString).IsEmpty then
    Result.AddIgual('ID', fdmCamposPesquisaID.AsInteger);

  if not (fdmCamposPesquisaCODIGO_BARRAS.AsString).IsEmpty then
    Result.AddIgual('CODIGO_BARRA', fdmCamposPesquisaCODIGO_BARRAS.AsString);

  if not (fdmCamposPesquisaDT_CADASTRO.AsString).IsEmpty then
    Result.AddIgualDate('DH_CADASTRO', fdmCamposPesquisaDT_CADASTRO.AsDateTime);

  if not (fdmCamposPesquisaDESCRICAO.AsString).IsEmpty then
    Result.AddContem('DESCRICAO', fdmCamposPesquisaDESCRICAO.AsString);

  if not (fdmCamposPesquisaTIPO.AsString).IsEmpty then
    Result.AddIgual('TIPO', fdmCamposPesquisaTIPO.AsString);

  if not (fdmCamposPesquisaID_GRUPO_PRODUTO.AsString).IsEmpty then
    Result.AddIgual('ID_GRUPO_PRODUTO', fdmCamposPesquisaID_GRUPO_PRODUTO.AsString);

  if not (fdmCamposPesquisaID_SUB_GRUPO_PRODUTO.AsString).IsEmpty then
    Result.AddIgual('ID_SUB_GRUPO_PRODUTO', fdmCamposPesquisaID_SUB_GRUPO_PRODUTO.AsString);

  if not (fdmCamposPesquisaID_CATEGORIA.AsString).IsEmpty then
    Result.AddIgual('ID_CATEGORIA', fdmCamposPesquisaID_CATEGORIA.AsString);

  if not (fdmCamposPesquisaID_SUB_CATEGORIA.AsString).IsEmpty then
    Result.AddIgual('ID_SUB_CATEGORIA', fdmCamposPesquisaID_SUB_CATEGORIA.AsString);

  if not (fdmCamposPesquisaID_MODELO.AsString).IsEmpty then
    Result.AddIgual('ID_MODELO', fdmCamposPesquisaID_MODELO.AsString);

  if not (fdmCamposPesquisaID_UNIDADE_ESTOQUE.AsString).IsEmpty then
    Result.AddIgual('ID_UNIDADE_ESTOQUE', fdmCamposPesquisaID_UNIDADE_ESTOQUE.AsString);

  if not (fdmCamposPesquisaID_MARCA.AsString).IsEmpty then
    Result.AddIgual('ID_MARCA', fdmCamposPesquisaID_MARCA.AsString);

  if not (fdmCamposPesquisaID_LINHA.AsString).IsEmpty then
    Result.AddIgual('ID_LINHA', fdmCamposPesquisaID_LINHA.AsString);
end;

procedure TPesqProduto.ViewSelecaoProdutosDblClick(Sender: TObject);
begin
  inherited;
  AdicionarProdutoNoDatasetDestino;
end;

procedure TPesqProduto.ViewSelecaoProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    AdicionarProdutoNoDatasetDestino;
  end;
end;

procedure TPesqProduto.AdicionarProdutoNoDatasetDestino(const ASolicitaQuantidade: Boolean = true);
begin
  if Assigned(FDatasetRetorno) then
  try
    TControlsUtils.CongelarFormulario(Application.MainForm);
    FDatasetRetorno.DisableControls;
    fdmSelecaoTitulos.DisableControls;
    if Assigned(FDatasetRetorno) then
    begin
      if not FDatasetRetorno.Locate('ID', fdmSelecaoTitulos.FieldByName('ID').AsInteger, []) then
      begin
        FDatasetRetorno.Append;
        FDatasetRetorno.CopyRecord(fdmSelecaoTitulos);

        if ASolicitaQuantidade and Assigned(FDatasetRetorno.FindField('QUANTIDADE_ETIQUETA')) then
        begin
          FDatasetRetorno.FieldByName('QUANTIDADE_ETIQUETA').AsInteger :=
            StrtoIntDef(InputBox('Aten��o', 'Informe o quantidade de etiquetas que deseja imprimir:', '1'), 1);
        end;

        FDatasetRetorno.Post;
      end;
    end;
  finally
    FDatasetRetorno.EnableControls;
    fdmSelecaoTitulos.EnableControls;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TPesqProduto.Consultar(AFiltro: TFiltroPadrao);
var
  DSList: TFDJSONDataSets;
begin
  inherited;
  try
    if fdmSelecaoTitulos.Active then
      fdmSelecaoTitulos.EmptyDataSet;

    DSList := DmConnection.ServerMethodsClient.GetDados(
      NOME_CONSULTA_PRODUTO, AFiltro.where, TSistema.Sistema.Usuario.idSeguranca);

    try
      TDatasetUtils.JSONDatasetToMemTable(DSList, fdmSelecaoTitulos);

      TcxGridUtils.AdicionarTodosCamposNaView(ViewSelecaoProdutos);

      TUsuarioGridView.LoadGridView(ViewSelecaoProdutos, TSistema.Sistema.Usuario.idSeguranca, // TSistema.Sistema.Usuario.IdSeguranca
        NOME_CONSULTA_PRODUTO);
    finally
      FreeAndNil(DSList);
    end;
  Except
    TFrmMessage.Failure('Falha ao consultar os dados, verifique o SQL associado a esta Tela.');
  end;
end;

procedure TPesqProduto.actRestaurarColunasTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, NOME_CONSULTA_PRODUTO);
  ViewSelecaoProdutos.RestoreDefaults;
end;

procedure TPesqProduto.ActSalvarConfiguracoesTitulosExecute(
  Sender: TObject);
begin
  inherited;
    TUsuarioGridView.SaveGridView(ViewSelecaoProdutos, TSistema.Sistema.Usuario.idSeguranca,
    NOME_CONSULTA_PRODUTO);
end;

procedure TPesqProduto.ActSelecionarTodosProdutosExecute(Sender: TObject);
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Adicionando Produtos');
    fdmSelecaoTitulos.DisableControls;
    try
      fdmSelecaoTitulos.First;
      while not fdmSelecaoTitulos.Eof do
      begin
        AdicionarProdutoNoDatasetDestino(false);
        fdmSelecaoTitulos.Next;
      end;
    except
      TFrmMessage.Failure('O SQL associado a esta Tela deve retornar o campo ID do Produto.');
      abort;
    end;
  finally
    TFrmMessage_Process.CloseMessage;
    fdmSelecaoTitulos.EnableControls;
  end;
end;

procedure TPesqProduto.Cancelar;
begin
  inherited;
  fdmCamposPesquisa.CancelUpdates;
end;

procedure TPesqProduto.Confirmar;
begin
  FIDs := GetListaIDs();
  inherited;
end;

function TPesqProduto.GetListaIDs: String;
var
  ListaIDs : TStringList;
begin
  if not fdmSelecaoTitulos.Active then
  begin
    FIDs := '';
    Exit;
  end;

  ListaIDs := TStringList.Create;
  try
    fdmSelecaoTitulos.DisableControls;
    try
      fdmSelecaoTitulos.First;
      while not fdmSelecaoTitulos.Eof do
      begin
        ListaIDs.Add(fdmSelecaoTitulos.FieldByName('ID').AsString);
        fdmSelecaoTitulos.Next;
      end;
    except
      TFrmMessage.Failure('O SQL associado a esta Tela deve retornar o campo ID do Produto.');
      abort;
    end;
    result := ListaIDs.CommaText;
  finally
    FreeAndNil(ListaIDs);
    fdmSelecaoTitulos.EnableControls;
  end;
end;

class function TPesqProduto.BuscarDatasetProdutos(var ACodigosProdutos: String): TFDMemTable;
begin
  Application.CreateForm(TPesqProduto, PesqProduto);
  try
    PesqProduto.fdmCamposPesquisa.Open;
    PesqProduto.fdmCamposPesquisa.Append;

    PesqProduto.ActConfirmar.Visible := true;
    PesqProduto.ActCancelar.Visible := true;
    PesqProduto.ActConfirmar.Enabled := true;
    PesqProduto.ActCancelar.Enabled := true;

    PesqProduto.ActClosePesqProduto.Visible := false;
    PesqProduto.ActClosePesqProduto.Enabled := false;
    PesqProduto.ActSelecionarTodosProdutos.Visible := false;
    PesqProduto.ActSelecionarTodosProdutos.Enabled := false;

    PesqProduto.ShowModal;
    if PesqProduto.result = MCONFIRMED then
    begin
      ACodigosProdutos := PesqProduto.FIDs;
      Result := TFDMemTable.Create(nil);
      Result.CopyDataSet(PesqProduto.fdmSelecaoTitulos, [coStructure, coRestart, coAppend]);
    end
    else
    begin
      Result := nil;
    end;
  finally
    FreeAndNil(PesqProduto);
  end;
end;

class function TPesqProduto.BuscarDatasetProdutosOculto(ACodigosProdutos: String): TFDMemTable;
var
  filtro: TFiltroPadrao;
begin
  Application.CreateForm(TPesqProduto, PesqProduto);
  try
    PesqProduto.fdmCamposPesquisa.Open;
    PesqProduto.fdmCamposPesquisa.Append;

    PesqProduto.ActConfirmar.Visible := true;
    PesqProduto.ActCancelar.Visible := true;
    PesqProduto.ActConfirmar.Enabled := true;
    PesqProduto.ActCancelar.Enabled := true;

    PesqProduto.ActClosePesqProduto.Visible := false;
    PesqProduto.ActClosePesqProduto.Enabled := false;
    PesqProduto.ActSelecionarTodosProdutos.Visible := false;
    PesqProduto.ActSelecionarTodosProdutos.Enabled := false;

    filtro := TFiltroPadrao.Create;
    try
      filtro.AddFaixa(TProdutoProxy.FIELD_ID, ACodigosProdutos);
      PesqProduto.Consultar(filtro);
      Result := TFDMemTable.Create(nil);
      Result.CopyDataSet(PesqProduto.fdmSelecaoTitulos, [coStructure, coRestart, coAppend]);
    finally
      FreeAndNil(filtro);
    end;
  finally
    FreeAndNil(PesqProduto);
  end;
end;

class function TPesqProduto.BuscarProdutos: string;
begin
  Application.CreateForm(TPesqProduto, PesqProduto);
  try
    PesqProduto.fdmCamposPesquisa.Open;
    PesqProduto.fdmCamposPesquisa.Append;

    PesqProduto.ActConfirmar.Visible := true;
    PesqProduto.ActCancelar.Visible := true;
    PesqProduto.ActConfirmar.Enabled := true;
    PesqProduto.ActCancelar.Enabled := true;

    PesqProduto.ActClosePesqProduto.Visible := false;
    PesqProduto.ActClosePesqProduto.Enabled := false;
    PesqProduto.ActSelecionarTodosProdutos.Visible := false;
    PesqProduto.ActSelecionarTodosProdutos.Enabled := false;

    PesqProduto.ShowModal;
    if PesqProduto.result = MCONFIRMED then
    begin
      Result := PesqProduto.FIDs;
    end
    else
    begin
      Result := '';
    end;
  finally
    FreeAndNil(PesqProduto);
  end;
end;

class procedure TPesqProduto.BuscarProdutosComRecursoDeSelecao(ADataset: TFDMemTable;
  AViewProdutos: TcxGridDBBandedTableView = nil);
var
  filtro: TFiltroPadrao;
begin
  Application.CreateForm(TPesqProduto, PesqProduto);
  try
    PesqProduto.fdmCamposPesquisa.Open;
    PesqProduto.FDatasetRetorno := ADataset;

    PesqProduto.ActConfirmar.Visible := false;
    PesqProduto.ActCancelar.Visible := false;
    PesqProduto.ActConfirmar.Enabled := false;
    PesqProduto.ActCancelar.Enabled := false;

    PesqProduto.ActClosePesqProduto.Visible := true;
    PesqProduto.ActClosePesqProduto.Enabled := true;
    PesqProduto.ActSelecionarTodosProdutos.Visible := true;
    PesqProduto.ActSelecionarTodosProdutos.Enabled := true;

    filtro := TFiltroPadrao.Create;
    try
      PesqProduto.Consultar(filtro);
    finally
      FreeAndNil(filtro);
    end;

    if not ADataset.Active then
    begin
      ADataset.CopyDataset(PesqProduto.fdmSelecaoTitulos, [coStructure, coRestart]);

      if Assigned(ADataset.FindField('QUANTIDADE_ETIQUETA')) then
      begin
        ADataset.FieldByName('QUANTIDADE_ETIQUETA').ReadOnly := false;
      end;
    end;

    if Assigned(AViewProdutos) then
    begin
      TcxGridUtils.AdicionarTodosCamposNaView(AViewProdutos);

      TUsuarioGridView.LoadGridView(AViewProdutos,
        TSistema.Sistema.Usuario.idSeguranca, TPesqProduto.NOME_CONSULTA_PRODUTO);
    end;

    PesqProduto.fdmCamposPesquisa.Append;

    PesqProduto.ShowModal;
  finally
    FreeAndNil(PesqProduto);
  end;
end;

end.
