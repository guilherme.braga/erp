unit uFrmRelatorioFRFiltroVertical;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, Datasnap.DBClient, uGBClientDataset, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uFiltroFormulario, cxBarEditItem, cxCalendar, cxButtonEdit,
  dxBarExtItems, cxDropDownEdit, uGBPanel, cxInplaceContainer, cxVGrid,
  cxDBVGrid, System.Generics.Collections, cxTextEdit, uFrameFiltroVerticalPadrao, cxSplitter,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin,
  dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter,
  dxSkinscxPCPainter;

type
  TFrmRelatorioFRFiltroVertical = class(TFrmChildPadrao)
    dsConsultaRelatorioFR: TDataSource;
    cdsConsultaRelatorioFR: TGBClientDataSet;
    cdsConsultaRelatorioFRID: TAutoIncField;
    cdsConsultaRelatorioFRDESCRICAO: TStringField;
    cdsConsultaRelatorioFRNOME: TStringField;
    cxGridRelatorios: TcxGrid;
    cxGridRelatoriosView: TcxGridDBTableView;
    cxGridRelatoriosViewID: TcxGridDBColumn;
    cxGridRelatoriosViewNOME: TcxGridDBColumn;
    cxGridRelatoriosViewDESCRICAO: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    ActImprimir: TAction;
    ActVisualizar: TAction;
    lbImprimir: TdxBarLargeButton;
    lbVisualizar: TdxBarLargeButton;
    BarFiltro: TdxBar;
    filtroDataTermino: TcxBarEditItem;
    filtroDataInicio: TcxBarEditItem;
    filtroGrupoCentroResultado: TcxBarEditItem;
    filtroCentroResultado: TcxBarEditItem;
    filtroContaAnalise: TcxBarEditItem;
    filtroLabelDataMovimento: TdxBarStatic;
    filtroContaCorrente: TcxBarEditItem;
    filtroConciliado: TcxBarEditItem;
    filtroTipoMovimento: TcxBarEditItem;
    filtroDescricaoCentroResultado: TdxBarStatic;
    filtroDescricaoGrupoCentroResultado: TdxBarStatic;
    filtroDescricaoContaAnalise: TdxBarStatic;
    filtroDescricaoContaCorrente: TdxBarStatic;
    FdmFiltros: TFDMemTable;
    pnFrameFiltroVerticalPadrao: TgbPanel;
    spFiltroVertical: TcxSplitter;
    cdsConsultaRelatorioFRIMPRESSORA: TStringField;
    cdsConsultaRelatorioFRACAO: TStringField;
    cdsConsultaRelatorioFRNUMERO_COPIAS: TIntegerField;
    cdsConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField;
    procedure ActImprimirExecute(Sender: TObject);
    procedure ActVisualizarExecute(Sender: TObject);
    procedure cdsConsultaRelatorioFRAfterScroll(DataSet: TDataSet);
  private
    FWhereSearch: TFiltroPadrao;
    FBuscandoRelatoriosAssociados: Boolean;
    procedure ClearWhereBundle;
    procedure BuscarRelatoriosAssociados;
    procedure InstanciarFiltroVertical;
    procedure FinalizarFiltroVertical;
    procedure AplicarPersonalizacaoFiltro;
  public
    FFiltroVertical: TFrameFiltroVerticalPadrao;
    FListaIndentificadoresConsultaFK: TDictionary<String, String>;
    FIdentificador: String;
    class function CriarFormularioRelatorio(AIdentificarFormulario, ARotuloFormulario: String): TFrmRelatorioFRFiltroVertical;

  protected
    procedure SetWhereBundle; virtual;
    procedure PrepararFiltros; virtual;
    procedure AntesDeImprimir; virtual;
    procedure Imprimir; virtual;
    procedure DepoisDeImprimir; virtual;
    procedure AntesDeVisualizar; virtual;
    procedure Visualizar; virtual;
    procedure DepoisDeVisualizar; virtual;
    procedure AoCriarFormulario; override;
    procedure AoFecharFormulario; override;
    procedure AoDestruirFormulario; override;
  end;

implementation

{$R *.dfm}

uses uTControl_Function, uRelatorioFR, uSistema, uVerticalGridUtils,
  uDatasetUtils, uFrmMessage_Process, uFrmConsultaPadrao, uRelatorioFRFiltroVertical, uControlsUtils;

{ TFrmRelatorioFRFiltroVertical }

procedure TFrmRelatorioFRFiltroVertical.ActImprimirExecute(Sender: TObject);
begin
  inherited;
  Imprimir;
end;

procedure TFrmRelatorioFRFiltroVertical.ActVisualizarExecute(Sender: TObject);
begin
  inherited;
  Visualizar;
end;

procedure TFrmRelatorioFRFiltroVertical.AntesDeImprimir;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRFiltroVertical.AntesDeVisualizar;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRFiltroVertical.AoCriarFormulario;
begin
  InstanciarFiltroVertical;
  FWhereSearch := TFiltroPadrao.Create;
  FBuscandoRelatoriosAssociados := false;
end;

procedure TFrmRelatorioFRFiltroVertical.AoDestruirFormulario;
begin
  inherited;
  TControlFunction.RemoveInstance(Self.ClassName+FIdentificador);
end;

procedure TFrmRelatorioFRFiltroVertical.AoFecharFormulario;
begin
  if Assigned(FWhereSearch) then
   FreeAndNil(FWhereSearch);

  FinalizarFiltroVertical;
end;

procedure TFrmRelatorioFRFiltroVertical.AplicarPersonalizacaoFiltro;
var
  cdsFiltrosPersonalizados: TClientDataset;
begin
  cdsFiltrosPersonalizados := TRelatorioFR.BuscarFiltrosPersonalizados(
      cdsConsultaRelatorioFRFILTROS_PERSONALIZADOS.AsString);
  try
    FFiltroVertical.AplicarPersonalizacaoFiltro(cdsFiltrosPersonalizados);
  finally
    FreeAndNil(cdsFiltrosPersonalizados);
  end;
end;

procedure TFrmRelatorioFRFiltroVertical.BuscarRelatoriosAssociados;
begin
  cdsConsultaRelatorioFR.Close;
  cdsConsultaRelatorioFR.Params.Clear;
  cdsConsultaRelatorioFR.FetchParams;
  cdsConsultaRelatorioFR.Params.ParamByName('NOME').AsString := FIdentificador;
  cdsConsultaRelatorioFR.Params.ParamByName('ID_PESSOA_USUARIO').AsInteger :=
    TSistema.Sistema.usuario.idSeguranca;
  cdsConsultaRelatorioFR.Open;
end;

procedure TFrmRelatorioFRFiltroVertical.cdsConsultaRelatorioFRAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if FBuscandoRelatoriosAssociados then
  begin
    exit;
  end;

  FinalizarFiltroVertical;
  InstanciarFiltroVertical;
  PrepararFiltros;
end;

procedure TFrmRelatorioFRFiltroVertical.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TFrmRelatorioFRFiltroVertical.Imprimir;
begin
  try
    TFrmMessage_Process.SendMessage('Carregando relatório');
    ActiveControl := nil;

    if cdsConsultaRelatorioFR.isEmpty then
      exit;

    SetWhereBundle;

    TRelatorioFR.ImprimirRelatorio(0, cdsConsultaRelatorioFRID.AsInteger,
      FWhereSearch.where);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmRelatorioFRFiltroVertical.InstanciarFiltroVertical;
begin
  FFiltroVertical := TFrameFiltroVerticalPadrao.Create(pnFrameFiltroVerticalPadrao);
  FFiltroVertical.Parent := pnFrameFiltroVerticalPadrao;
end;

class function TFrmRelatorioFRFiltroVertical.CriarFormularioRelatorio(
  AIdentificarFormulario, ARotuloFormulario: String): TFrmRelatorioFRFiltroVertical;
begin
  if TControlFunction.ExisteInstancia(Self.ClassName+AIdentificarFormulario) then
  begin
    result := TFrmRelatorioFRFiltroVertical(
      TControlFunction.GetInstance(Self.ClassName+AIdentificarFormulario));

    result.Show;
    exit;
  end;

  result := TFrmRelatorioFRFiltroVertical(TControlFunction.CreateMDIForm(Self.ClassName,
    AIdentificarFormulario));

  if result = nil then
  begin
    Exit;
  end;

  with result do
  begin
    FIdentificador := AIdentificarFormulario;
    Name := AIdentificarFormulario;
    Caption := ARotuloFormulario;

    try
      FBuscandoRelatoriosAssociados := true;
      BuscarRelatoriosAssociados;
    finally
      FBuscandoRelatoriosAssociados := false;
    end;

    if not cdsConsultaRelatorioFR.IsEmpty then
    begin
      PrepararFiltros;
    end;
  end;
end;

procedure TFrmRelatorioFRFiltroVertical.DepoisDeImprimir;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRFiltroVertical.DepoisDeVisualizar;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRFiltroVertical.FinalizarFiltroVertical;
begin
  if Assigned(FFiltroVertical) then
    FreeAndNil(FFiltroVertical);
end;

procedure TFrmRelatorioFRFiltroVertical.PrepararFiltros;
begin
  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Carregando configuração de filtro');
    TRelatorioFRFiltroVertical.AdicionarFiltros(FIdentificador, FFiltroVertical);

    if not cdsConsultaRelatorioFR.FieldByName('FILTROS_PERSONALIZADOS').AsString.IsEmpty then
    begin
      AplicarPersonalizacaoFiltro;
    end;

    FFiltroVertical.CriarFiltros;
  finally
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TFrmRelatorioFRFiltroVertical.SetWhereBundle;
begin
  ClearWhereBundle;
  FFiltroVertical.ValidarFiltrosObrigatorios;
  FFiltroVertical.SetWhere(FWhereSearch);
end;

procedure TFrmRelatorioFRFiltroVertical.Visualizar;
begin
  try
    TFrmMessage_Process.SendMessage('Carregando relatório');

    ActiveControl := nil;

    if cdsConsultaRelatorioFR.isEmpty then
      exit;

    SetWhereBundle;

    TRelatorioFR.VisualizarRelatorio(0, cdsConsultaRelatorioFRID.AsInteger,
      FWhereSearch.where);

  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

initialization
  RegisterClass(TFrmRelatorioFRFiltroVertical);

Finalization
  UnRegisterClass(TFrmRelatorioFRFiltroVertical);


end.

