inherited CadVeiculo: TCadVeiculo
  Caption = 'Cadastro de Ve'#237'culo'
  ClientWidth = 912
  ExplicitWidth = 928
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 912
    ExplicitWidth = 912
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 912
    Properties.ActivePage = cxtsData
    ExplicitWidth = 912
    ClientRectRight = 912
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 912
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 880
        ExplicitWidth = 912
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 912
      inherited DesignPanel: TJvDesignPanel
        Width = 912
        ExplicitWidth = 912
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 912
          Width = 912
          inherited dxBevel1: TdxBevel
            Width = 908
            ExplicitWidth = 908
          end
          object Label1: TLabel
            Left = 8
            Top = 62
            Width = 34
            Height = 13
            Caption = 'Modelo'
          end
          object Label2: TLabel
            Left = 668
            Top = 62
            Width = 29
            Height = 13
            Caption = 'Marca'
          end
          object Label3: TLabel
            Left = 668
            Top = 37
            Width = 34
            Height = 13
            Caption = 'Pessoa'
          end
          object lbFilial: TLabel
            Left = 668
            Top = 87
            Width = 20
            Height = 13
            Caption = 'Filial'
          end
          object Label5: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label6: TLabel
            Left = 8
            Top = 37
            Width = 25
            Height = 13
            Caption = 'Placa'
          end
          object Label4: TLabel
            Left = 121
            Top = 37
            Width = 19
            Height = 13
            Caption = 'Ano'
          end
          object Label7: TLabel
            Left = 504
            Top = 37
            Width = 58
            Height = 13
            Caption = 'Combust'#237'vel'
          end
          object Label8: TLabel
            Left = 199
            Top = 37
            Width = 31
            Height = 13
            Caption = 'Chassi'
          end
          object Label9: TLabel
            Left = 8
            Top = 87
            Width = 17
            Height = 13
            Caption = 'Cor'
          end
          object Label10: TLabel
            Left = 348
            Top = 37
            Width = 45
            Height = 13
            Caption = 'Renavam'
          end
          object eFkModelo: TgbDBButtonEditFK
            Left = 43
            Top = 58
            DataBinding.DataField = 'ID_MODELO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = clWhite
            TabOrder = 8
            gbTextEdit = descModelo
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_MODELO; JOIN_DESCRICAO_MODELO'
            gbTableName = 'MODELO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbClasseDoCadastro = 'TCadModeloProduto'
            gbIdentificadorConsulta = 'MODELO'
            Width = 59
          end
          object descModelo: TgbDBTextEdit
            Left = 99
            Top = 58
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_MODELO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbReadyOnly = True
            gbPassword = False
            Width = 566
          end
          object eFkMarca: TgbDBButtonEditFK
            Left = 706
            Top = 58
            DataBinding.DataField = 'ID_MARCA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = clWhite
            TabOrder = 10
            gbTextEdit = descMarca
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_MARCA;JOIN_DESCRICAO_MARCA'
            gbTableName = 'MARCA'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbClasseDoCadastro = 'TCadMarcaProduto'
            gbIdentificadorConsulta = 'MARCA'
            Width = 59
          end
          object descMarca: TgbDBTextEdit
            Left = 761
            Top = 58
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_MARCA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 11
            gbReadyOnly = True
            gbPassword = False
            Width = 146
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 706
            Top = 33
            DataBinding.DataField = 'ID_PESSOA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 6
            gbTextEdit = gbDBTextEdit1
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
            gbTableName = 'PESSOA'
            gbCamposConsulta = 'ID;NOME'
            gbIdentificadorConsulta = 'PESSOA'
            Width = 59
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 761
            Top = 33
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_PESSOA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbPassword = False
            Width = 146
          end
          object edtFilial: TgbDBButtonEditFK
            Left = 706
            Top = 83
            DataBinding.DataField = 'ID_FILIAL'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 14
            gbTextEdit = descFilial
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_FILIAL;JOIN_FANTASIA_FILIAL'
            gbTableName = 'FILIAL'
            gbCamposConsulta = 'ID;FANTASIA'
            gbClasseDoCadastro = 'TCadEmpresaFilial'
            gbIdentificadorConsulta = 'FILIAL'
            Width = 60
          end
          object descFilial: TgbDBTextEdit
            Left = 761
            Top = 83
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_FANTASIA_FILIAL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 15
            gbReadyOnly = True
            gbPassword = False
            Width = 146
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 43
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 43
            Top = 33
            DataBinding.DataField = 'PLACA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbPassword = False
            Width = 74
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 852
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 16
            Transparent = True
            Width = 55
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 144
            Top = 33
            DataBinding.DataField = 'ANO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbPassword = False
            Width = 51
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 234
            Top = 33
            DataBinding.DataField = 'CHASSI'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbPassword = False
            Width = 110
          end
          object gbDBComboBox1: TgbDBComboBox
            Left = 566
            Top = 33
            DataBinding.DataField = 'COMBUSTIVEL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.DropDownListStyle = lsEditFixedList
            Properties.ImmediatePost = True
            Properties.Items.Strings = (
              'DIESEL'
              'ETANOL'
              'GAS'
              'GASOLINA')
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Width = 99
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 99
            Top = 83
            TabStop = False
            DataBinding.DataField = 'JOIN_DESCRICAO_COR'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 13
            gbReadyOnly = True
            gbPassword = False
            Width = 566
          end
          object gbDBButtonEditFK2: TgbDBButtonEditFK
            Left = 43
            Top = 83
            DataBinding.DataField = 'ID_COR'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 12
            gbTextEdit = gbDBTextEdit3
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_COR;JOIN_DESCRICAO_COR'
            gbTableName = 'COR'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'COR'
            Width = 60
          end
          object gbDBTextEdit5: TgbDBTextEdit
            Left = 397
            Top = 33
            DataBinding.DataField = 'RENAVAM'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbPassword = False
            Width = 103
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Ve'#237'culo'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspVeiculo'
    RemoteServer = DmConnection.dspVeiculo
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataPLACA: TStringField
      FieldName = 'PLACA'
      Size = 7
    end
    object cdsDataANO: TIntegerField
      FieldName = 'ANO'
    end
    object cdsDataCOMBUSTIVEL: TStringField
      FieldName = 'COMBUSTIVEL'
      Size = 30
    end
    object cdsDataCHASSI: TStringField
      FieldName = 'CHASSI'
      Size = 30
    end
    object cdsDataRENAVAM: TStringField
      FieldName = 'RENAVAM'
      Size = 30
    end
    object cdsDataID_MODELO: TIntegerField
      FieldName = 'ID_MODELO'
    end
    object cdsDataID_MARCA: TIntegerField
      FieldName = 'ID_MARCA'
    end
    object cdsDataID_COR: TIntegerField
      FieldName = 'ID_COR'
    end
    object cdsDataID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
    end
    object cdsDataID_FILIAL: TIntegerField
      FieldName = 'ID_FILIAL'
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      FieldName = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataJOIN_DESCRICAO_MARCA: TStringField
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_MODELO: TStringField
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_COR: TStringField
      FieldName = 'JOIN_DESCRICAO_COR'
      Size = 100
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      FieldName = 'JOIN_NOME_PESSOA'
      Size = 80
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Size = 80
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42223.950180057870000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
