inherited RelContaPagar: TRelContaPagar
  Caption = 'Relat'#243'rio de Conta a Pagar'
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    inherited gbPanel1: TgbPanel
      inherited vgFiltros: TcxVerticalGrid
        Version = 1
        inherited vcatFiltrosPadrao: TcxCategoryRow
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object rowStatus: TcxEditorRow
          Properties.Caption = 'Situa'#231#227'o'
          Properties.Hint = 'STATUS'
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsEditFixedList
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.Items.Strings = (
            'ABERTO'
            'QUITADO'
            'CANCELADO')
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object rowCentroResultado: TcxEditorRow
          Properties.Caption = 'Centro de Resultado'
          Properties.Hint = 'ID_CENTRO_RESULTADO'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowCentroResultadoEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object rowContaAnalise: TcxEditorRow
          Properties.Caption = 'Conta de An'#225'lise'
          Properties.Hint = 'ID_CONTA_ANALISE'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowContaAnaliseEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 236
    Top = 235
  end
  inherited dxBarManager: TdxBarManager
    Left = 208
    Top = 235
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarFiltro: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited FdmFiltros: TFDMemTable
    Left = 448
    Top = 224
    object FdmFiltrosID: TIntegerField
      FieldName = 'ID'
      Origin = 'CONTA_PAGAR.ID'
    end
    object FdmFiltrosDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'CONTA_PAGAR.DOCUMENTO'
      Size = 50
    end
    object FdmFiltrosDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'CONTA_PAGAR.DESCRICAO'
      Size = 255
    end
    object FdmFiltrosDT_COMPETENCIA_INICIO: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia - In'#237'cio'
      FieldName = 'DT_COMPETENCIA_INICIO'
      Origin = 'CONTA_PAGAR.DT_COMPETENCIA'
    end
    object FdmFiltrosDT_COMPETENCIA_TERMINO: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia - T'#233'rmino'
      FieldName = 'DT_COMPETENCIA_TERMINO'
      Origin = 'CONTA_PAGAR.DT_COMPETENCIA'
    end
    object FdmFiltrosDT_VENCIMENTO_INICIO: TDateField
      DisplayLabel = 'Dt. Vencimento - In'#237'cio'
      FieldName = 'DT_VENCIMENTO_INICIO'
      Origin = 'CONTA_PAGAR.DT_VENCIMENTO'
    end
    object FdmFiltrosDT_VENCIMENTO_TERMINO: TDateField
      DisplayLabel = 'Dt. Vencimento - T'#233'rmino'
      FieldName = 'DT_VENCIMENTO_TERMINO'
      Origin = 'CONTA_PAGAR.DT_VENCIMENTO'
    end
    object FdmFiltrosVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'CONTA_PAGAR.VL_TITULO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FdmFiltrosID_CONTA_ANALISE: TLargeintField
      Tag = 1
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'CONTA_PAGAR.ID_CONTA_ANALISE'
    end
    object FdmFiltrosID_CENTRO_RESULTADO: TLargeintField
      Tag = 1
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'CONTA_PAGAR.ID_CENTRO_RESULTADO'
    end
    object FdmFiltrosSTATUS: TStringField
      Tag = 1
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = 'CONTA_PAGAR.STATUS'
      Size = 15
    end
  end
end
