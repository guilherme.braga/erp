unit uMovLoteRecebimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBPanel, Vcl.StdCtrls,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, uFrameDetailPadrao, uFrameQuitacao,
   uframeQuitacaoLoteRecebimento, cxBlobEdit, uGBDBBlobEdit,
  uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit,
  cxCurrencyEdit, dxBarExtItems, cxBarEditItem, JvExControls, JvButton,
  JvTransparentButton, cxButtonEdit, uGBDBButtonEditFK, uFiltroFormulario,
  cxSplitter, cxLabel, Vcl.DBCtrls, Data.FireDACJSONReflect,
  uUsuarioDesignControl, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, cxCalc, Generics.Collections, uGBLabel, uGBFDMemTable,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter;

type
  TMovLoteRecebimento = class(TFrmCadastro_Padrao)
    pkCodigo: TgbDBTextEditPK;
    labelCodigo: TLabel;
    edtDtCadastro: TgbDBDateEdit;
    edtDtEfetivacao: TgbDBDateEdit;
    edtSituacao: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    descProcesso: TgbDBTextEdit;
    edtChaveProcesso: TgbDBTextEdit;
    pmSelecaoTitulos: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    gpmSelecaoTitulos: TcxGridPopupMenu;
    alSelecaoTitulos: TActionList;
    ActSalvarConfiguracoesTitulos: TAction;
    actRestaurarColunasTitulos: TAction;
    actAlterarColunasGridTitulos: TAction;
    actExibirAgrupamentoTitulos: TAction;
    actAlterarSQLPesquisaPadraoTitulos: TAction;
    AlterarSQL2: TMenuItem;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataSTATUS: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataDH_EFETIVACAO: TDateTimeField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_USUARIO: TStringField;
    cdsDataJOIN_CHAVE_PROCESSO: TStringField;
    cdsDatafdqLoteRecebimentoQuitacao: TDataSetField;
    cdsDatafdqLoteRecebimentoTitulo: TDataSetField;
    dsLoteRecebimentoTitulo: TDataSource;
    cdsLoteRecebimentoTitulo: TGBClientDataSet;
    cdsLoteRecebimentoQuitacao: TGBClientDataSet;
    dsLoteRecebimentoQuitacao: TDataSource;
    cdsLoteRecebimentoTituloID: TAutoIncField;
    cdsLoteRecebimentoTituloVL_ACRESCIMO: TFMTBCDField;
    cdsLoteRecebimentoTituloVL_DESCONTO: TFMTBCDField;
    cdsLoteRecebimentoTituloID_LOTE_Recebimento: TIntegerField;
    cdsLoteRecebimentoTituloID_CONTA_Receber: TIntegerField;
    cdsLoteRecebimentoTituloDH_CADASTRO: TDateTimeField;
    cdsLoteRecebimentoTituloDOCUMENTO: TStringField;
    cdsLoteRecebimentoTituloDESCRICAO: TStringField;
    cdsLoteRecebimentoTituloVL_TITULO: TFMTBCDField;
    cdsLoteRecebimentoTituloVL_QUITADO: TFMTBCDField;
    cdsLoteRecebimentoTituloVL_ABERTO: TFMTBCDField;
    cdsLoteRecebimentoTituloQT_PARCELA: TIntegerField;
    cdsLoteRecebimentoTituloNR_PARCELA: TIntegerField;
    cdsLoteRecebimentoTituloDT_VENCIMENTO: TDateField;
    cdsLoteRecebimentoTituloSEQUENCIA: TStringField;
    cdsLoteRecebimentoTituloBO_VENCIDO: TStringField;
    cdsLoteRecebimentoTituloOBSERVACAO: TBlobField;
    cdsLoteRecebimentoTituloID_PESSOA: TIntegerField;
    cdsLoteRecebimentoTituloID_CONTA_ANALISE: TIntegerField;
    cdsLoteRecebimentoTituloID_CENTRO_RESULTADO: TIntegerField;
    cdsLoteRecebimentoTituloSTATUS: TStringField;
    cdsLoteRecebimentoTituloID_CHAVE_PROCESSO: TIntegerField;
    cdsLoteRecebimentoTituloID_FORMA_PAGAMENTO: TIntegerField;
    cdsLoteRecebimentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField;
    cdsLoteRecebimentoTituloDT_COMPETENCIA: TDateField;
    cdsLoteRecebimentoTituloID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsLoteRecebimentoTituloTP_DOCUMENTO_ORIGEM: TStringField;
    cdsLoteRecebimentoQuitacaoID: TAutoIncField;
    cdsLoteRecebimentoQuitacaoOBSERVACAO: TBlobField;
    cdsLoteRecebimentoQuitacaoID_TIPO_QUITACAO: TIntegerField;
    cdsLoteRecebimentoQuitacaoDT_QUITACAO: TDateField;
    cdsLoteRecebimentoQuitacaoID_CONTA_CORRENTE: TIntegerField;
    cdsLoteRecebimentoQuitacaoVL_DESCONTO: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoVL_ACRESCIMO: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoVL_QUITACAO: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoNR_ITEM: TIntegerField;
    cdsLoteRecebimentoQuitacaoPERC_ACRESCIMO: TBCDField;
    cdsLoteRecebimentoQuitacaoPERC_DESCONTO: TBCDField;
    cdsLoteRecebimentoQuitacaoVL_TOTAL: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoID_CONTA_ANALISE: TIntegerField;
    cdsLoteRecebimentoQuitacaoID_LOTE_Recebimento: TIntegerField;
    labelProcesso: TLabel;
    labelSituacao: TLabel;
    labelDtEfetivacao: TLabel;
    labelDtCadastro: TLabel;
    labelObservacao: TLabel;
    cdsLoteRecebimentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    cdsLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cdsLoteRecebimentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    actEfetivar: TAction;
    actReabrir: TAction;
    lbEfetivar: TdxBarLargeButton;
    lbReabrir: TdxBarLargeButton;
    cdsLoteRecebimentoTituloSUM_VL_ABERTO_CR: TAggregateField;
    cdsLoteRecebimentoQuitacaoSUM_VL_TOTAL_QUITACAO: TAggregateField;
    barFiltrosLote: TdxBar;
    filtrosTitulos: TFDMemTable;
    filtrosTitulosID_PESSOA: TIntegerField;
    filtrosTitulosDT_VENCIMENTO_INICIAL: TDateField;
    filtrosTitulosDT_VENCIMENTO_FINAL: TDateField;
    actSelecionarTodos: TAction;
    actRemoverTodos: TAction;
    panelTotalizadores: TgbPanel;
    labelVlAbertoCR: TLabel;
    edtVlAbertoTitulos: TgbDBTextEdit;
    EdtConsultaEfetivacaoInicio: TcxBarEditItem;
    EdtConsultaEfetivacaoFim: TcxBarEditItem;
    EdtConsultaSituacao: TcxBarEditItem;
    lbConsultaVencimento: TdxBarStatic;
    EdtConsultaValor: TcxBarEditItem;
    fdmSelecaoTitulos: TFDMemTable;
    dsSelecaoTitulos: TDataSource;
    actFiltrarSelecaoTitulos: TAction;
    dsFiltrosTitulos: TDataSource;
    filtrosTitulosDOCUMENTO: TStringField;
    filtrosTitulosJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    pmLoteRecebimentoTitulos: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    alLoteRecebimentoTitulo: TActionList;
    actSalvarConfiguracaoLoteTitulos: TAction;
    actRestaurarColunasLoteTitulos: TAction;
    actAlterarRotuloLoteTitulos: TAction;
    actExibirAgrupamentoLoteTitulos: TAction;
    actAlterarSQLLoteTitulos: TAction;
    gpmLoteRecebimentoTitulo: TcxGridPopupMenu;
    cdsLoteRecebimentoQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    cdsLoteRecebimentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsLoteRecebimentoQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField;
    cdsLoteRecebimentoQuitacaoIC_VL_Recebimento: TFloatField;
    cdsDataVL_QUITADO: TFMTBCDField;
    cdsDataVL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_DESCONTO: TFMTBCDField;
    cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField;
    cdsDataVL_ABERTO: TFMTBCDField;
    cdsDataCC_VL_TROCO_DIFERENCA: TStringField;
    lbTrocoDiferencaLote: TDBText;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label19: TLabel;
    edVlRecebido: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    cdsLoteRecebimentoQuitacaoDOCUMENTO: TStringField;
    cdsLoteRecebimentoQuitacaoDESCRICAO: TStringField;
    cdsLoteRecebimentoTituloID_CONTA_CORRENTE: TIntegerField;
    cdsLoteRecebimentoTituloID_FILIAL: TIntegerField;
    cdsLoteRecebimentoQuitacaoCHEQUE_NUMERO: TIntegerField;
    cdsLoteRecebimentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    cdsLoteRecebimentoQuitacaoCHEQUE_AGENCIA: TStringField;
    cdsLoteRecebimentoQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    cdsLoteRecebimentoQuitacaoCHEQUE_BANCO: TStringField;
    cdsLoteRecebimentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    cdsLoteRecebimentoQuitacaoCHEQUE_SACADO: TStringField;
    cdsLoteRecebimentoQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    cdsLoteRecebimentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    pageControlPrincipal: TcxPageControl;
    tsTitulos: TcxTabSheet;
    tsQuitacao: TcxTabSheet;
    PnFrameQuitacao: TgbPanel;
    pnTitulos: TgbPanel;
    panelFiltro: TgbPanel;
    labelPessoa: TLabel;
    LabelDtVencimento: TLabel;
    labelDocumento: TLabel;
    btnFiltrar: TJvTransparentButton;
    Label1: TLabel;
    Label2: TLabel;
    edtDtVencimentoInicio: TgbDBDateEdit;
    edtDescPessoa: TgbDBTextEdit;
    edtDocumento: TgbDBTextEdit;
    edtVencimentoFim: TgbDBDateEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbPanel2: TgbPanel;
    gridTitulos: TcxGrid;
    ViewSelecaoTitulos: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    gbPanel1: TgbPanel;
    GridLoteRecebimentoTitulos: TcxGrid;
    ViewLoteRecebimentoTitulos: TcxGridDBBandedTableView;
    ViewLoteRecebimentoTitulosID: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_ACRESCIMO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_DESCONTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_LOTE_Recebimento: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_CONTA_Receber: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosDH_CADASTRO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosDOCUMENTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosDESCRICAO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_TITULO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_QUITADO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_ABERTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosQT_PARCELA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosNR_PARCELA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosDT_VENCIMENTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosSEQUENCIA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosBO_VENCIDO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosOBSERVACAO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_PESSOA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_CONTA_ANALISE: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_CENTRO_RESULTADO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosSTATUS: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_CHAVE_PROCESSO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_FORMA_PAGAMENTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_QUITADO_LIQUIDO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosDT_COMPETENCIA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosTP_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    ViewLoteRecebimentoTitulosDT_DOCUMENTO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_CONTA_CORRENTE: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosID_FILIAL: TcxGridDBBandedColumn;
    cdsLoteRecebimentoTituloVL_JUROS: TFMTBCDField;
    cdsLoteRecebimentoTituloPERC_JUROS: TFMTBCDField;
    cdsLoteRecebimentoTituloPERC_MULTA: TFMTBCDField;
    cdsLoteRecebimentoTituloVL_MULTA: TFMTBCDField;
    cdsLoteRecebimentoTituloBO_NEGOCIADO: TStringField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_NOME_PESSOA_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_ANALISE_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_CENTRO_RESULTADO_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_ORIGEM_CHAVE_PROCESSO_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_ID_ORIGEM_CHAVE_PROCESSO_CR: TIntegerField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_FORMA_PAGAMENTO_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_CONTA_CORRENTE_CR: TStringField;
    cdsLoteRecebimentoTituloJOIN_DESCRICAO_CARTEIRA_CR: TStringField;
    ViewLoteRecebimentoTitulosVL_JUROS: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosPERC_JUROS: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosPERC_MULTA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosVL_MULTA: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosBO_NEGOCIADO: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_NOME_PESSOA_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_CONTA_ANALISE_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_CENTRO_RESULTADO_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_ORIGEM_CHAVE_PROCESSO_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_ID_ORIGEM_CHAVE_PROCESSO_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_FORMA_PAGAMENTO_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_CONTA_CORRENTE_CR: TcxGridDBBandedColumn;
    ViewLoteRecebimentoTitulosJOIN_DESCRICAO_CARTEIRA_CR: TcxGridDBBandedColumn;
    cdsLoteRecebimentoTituloDT_DOCUMENTO: TDateField;
    cdsLoteRecebimentoQuitacaoVL_JUROS: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoPERC_JUROS: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoVL_MULTA: TFMTBCDField;
    cdsLoteRecebimentoQuitacaoPERC_MULTA: TFMTBCDField;
    cdsLoteRecebimentoTituloIC_VL_ABERTO: TFloatField;
    ViewLoteRecebimentoTitulosCC_VL_ABERTO: TcxGridDBBandedColumn;
    cdsLoteRecebimentoTituloID_CARTEIRA: TIntegerField;
    cdsLoteRecebimentoTituloJOIN_NOME_PESSOA: TStringField;
    gbPanel3: TgbPanel;
    cxLabel1: TcxLabel;
    PnSelecionarTodos: TgbPanel;
    BtnSelecionarTodosRegistros: TJvTransparentButton;
    gbPanel4: TgbPanel;
    cxLabel3: TcxLabel;
    gbPanel5: TgbPanel;
    JvTransparentButton1: TJvTransparentButton;
    cdsLoteRecebimentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    cdsLoteRecebimentoQuitacaoID_USUARIO_BAIXA: TIntegerField;
    cdsLoteRecebimentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    PnTotalizadorConsulta: TFlowPanel;
    pnTotalizadorVlAberto: TgbLabel;
    pnTotalizadorAcrecimoAberto: TgbLabel;
    pnTotalizadorTotalAberto: TgbLabel;
    pnTotalizadorVlVencido: TgbLabel;
    pnTotalizadorVlVencer: TgbLabel;
    pnTotalizadorVlRecebido: TgbLabel;
    pnTotalizadorAcrescimoRecebido: TgbLabel;
    pnTotalizadorTotalRecebido: TgbLabel;
    PnTotalizadorConsultaSelecionados: TFlowPanel;
    pnTotalizadorVlAbertoSelecionado: TgbLabel;
    pnTotalizadorAcrecimoAbertoSelecionado: TgbLabel;
    pnTotalizadorTotalAbertoSelecionado: TgbLabel;
    pnTotalizadorVlVencidoSelecionado: TgbLabel;
    pnTotalizadorVlVencerSelecionado: TgbLabel;
    pnTotalizadorVlRecebidoSelecionado: TgbLabel;
    pnTotalizadorAcrescimoRecebidoSelecionado: TgbLabel;
    pnTotalizadorTotalRecebidoSelecionado: TgbLabel;
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsLoteRecebimentoQuitacaoNewRecord(DataSet: TDataSet);
    procedure ActSalvarConfiguracoesTitulosExecute(Sender: TObject);
    procedure actRestaurarColunasTitulosExecute(Sender: TObject);
    procedure actAlterarColunasGridTitulosExecute(Sender: TObject);
    procedure actExibirAgrupamentoTitulosExecute(Sender: TObject);
    procedure actAlterarSQLPesquisaPadraoTitulosExecute(Sender: TObject);
    procedure actEfetivarExecute(Sender: TObject);
    procedure actReabrirExecute(Sender: TObject);
    procedure actSelecionarTodosExecute(Sender: TObject);
    procedure actFiltrarSelecaoTitulosExecute(Sender: TObject);
    procedure actSalvarConfiguracaoLoteTitulosExecute(Sender: TObject);
    procedure actRestaurarColunasLoteTitulosExecute(Sender: TObject);
    procedure actAlterarRotuloLoteTitulosExecute(Sender: TObject);
    procedure actExibirAgrupamentoLoteTitulosExecute(Sender: TObject);
    procedure actAlterarSQLLoteTitulosExecute(Sender: TObject);
    procedure cdsLoteRecebimentoQuitacaoVL_QUITACAOChange(Sender: TField);
    procedure CalcularPercentual(Sender: TField);
    procedure CalcularValor(Sender: TField);
    procedure cdsLoteRecebimentoQuitacaoAfterEdit(DataSet: TDataSet);
    procedure cdsLoteRecebimentoQuitacaoAfterDelete(DataSet: TDataSet);
    procedure cdsLoteRecebimentoQuitacaoAfterPost(DataSet: TDataSet);
    procedure actRemoverTodosExecute(Sender: TObject);
    procedure ViewLoteRecebimentoTitulosEditKeyDown(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure ViewSelecaoTitulosEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewSelecaoTitulosDblClick(Sender: TObject);
    procedure CalcularValoresAPartirDoTotalQuitacao(Sender: TField);
    procedure cdsDataCalcFields(DataSet: TDataSet);
    procedure cdsDataAfterEdit(DataSet: TDataSet);
    procedure cdsLoteRecebimentoTituloAfterDelete(DataSet: TDataSet);
    procedure cdsLoteRecebimentoTituloAfterPost(DataSet: TDataSet);
    procedure cdsLoteRecebimentoTituloBeforeDelete(DataSet: TDataSet);
    procedure TotalizarAcrescimo(Sender: TField);
    procedure cdsLoteRecebimentoTituloVL_DESCONTOChange(Sender: TField);
    procedure TotalizarAcrescimoQuitacao(Sender: TField);
    procedure cdsLoteRecebimentoTituloAfterOpen(DataSet: TDataSet);
    procedure cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
    procedure ViewSelecaoTitulosDataControllerDataChanged(Sender: TObject);
    procedure ViewLoteRecebimentoTitulosDataControllerDataChanged(Sender: TObject);
    procedure cxSplitter1Moved(Sender: TObject);
  private
    FCalculandoTotalQuitacaoValor: Boolean;
    FCalculandoPercentual : Boolean;
    FCalculandoValor      : Boolean;
    FTotalizandoAcrescimo: Boolean;

    FParametroLancarQuitacaoAutomaticaPorTitulo: TParametroFormulario;
    FParametroTipoQuitacaoDinheiro: TParametroFormulario;

    procedure InserirQuitacaoAutomaticaPorTitulo;
    procedure HabilitarBotaoReabrir;
    procedure HabilitarBotaoEfetivar;
    function GetWhereBundleSelecao: TFiltroPadrao;
    procedure AbrirDatasetSelecaoTitulo;
    function PegarIdContaReceberSelecionados: String;
    procedure InserirTituloNoLoteRecebimento;
    procedure RemoverTituloDoLoteRecebimento;
    procedure RemoverDaConsultaOsTitulosSelecionados;
    procedure Consultar(AFiltro: TFiltroPadrao);
    procedure PopularConsultaComJSONDataset(ADados: TFDJSONDatasets);
    procedure CalcularTamanhoDasVisoesDeTitulos;
    procedure ValidacoesAntesDeEfetivar;
    procedure AtualizarTotalItem;
    procedure HabilitarFiltrosSelecaoTitulo;
    procedure RemoverRegistroInserido;
    procedure SelecionarTitulo;
    procedure RemoverTituloSelecionado;
    procedure AtualizarTotalLote;
    procedure ExibirTrocoLote;
    procedure CalcularValorEmAberto;
    procedure RecalcularValorEmAberto;
    procedure SugerirDataVencimentoCheque;
  public
    FFrameQuitacaoLoteRecebimento: TFrameQuitacaoLoteRecebimento;
    FFormularioAbertoPorOutroFormulario: Boolean;
    FParametroGerarContraPartidaContaCorrenteMovimento: TParametroFormulario;
    FCalculandoTotalRodape: Boolean;
    FFiltroRegistroGradeAnterior: String;
    FExibirBarraTotalizadora: TParametroFormulario;

    procedure RatearAcrescimos;
    procedure RatearDescontos;
    class procedure AbrirTelaFiltrando(AIdChaveProcesso: Integer);
    class procedure AbrirTelaFiltrandoPorCodigosDosTitulos(const ACodigos: String;
      const AFiltrosDaTela: TDictionary<String, String> = nil);

    function QuitacaoEmCartao: Boolean;

    procedure CalcularTotaisDoRodape;
    procedure CalcularTotaisDoRodapeSelecionados;
    procedure AlinharBarraTotalizadoraConsulta;
    procedure ExibirBarraTotalizadora;

    {Usados em AfiltrosDaTela}
    const FILTRO_CODIGO_PESSOA = 'FILTRO_CODIGO_PESSOA';
    const FILTRO_NOME_PESSOA = 'FILTRO_NOME_PESSOA';
    const FILTRO_INICIO_VENCIMENTO = 'FILTRO_INICIO_VENCIMENTO';
    const FILTRO_FIM_VENCIMENTO = 'FILTRO_FIM_VENCIMENTO';
    const FILTRO_DOCUMENTO = 'FILTRO_DOCUMENTO';

  protected
    procedure GerenciarControles; override;
    procedure SetWhereBundle; override;
    procedure PrepararFiltros; override;
    procedure AoCriarFormulario; override;
    procedure DepoisDeInserir; override;
    procedure DepoisDeAlterar; override;
    procedure DepoisDeCancelar; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeRemover; override;
    procedure AntesDeConfirmar; override;
    procedure BloquearEdicao; override;
    procedure InstanciarFrames; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

var
  MovLoteRecebimento: TMovLoteRecebimento;

implementation

{$R *.dfm}

uses uFilial, uChaveProcesso, uChaveProcessoProxy, uLoteRecebimentoProxy,
  uTUsuario, uDatasetUtils, uFrmApelidarColunasGrid,
  uFrmAlterarSQLPesquisaPadrao, System.DateUtils,
  uDmConnection, uDevExpressUtils, uFrmMessage, uLoteRecebimento, uTMessage,
  uMathUtils, uFrmMessage_Process, uSistema, uPesqContaAnalise, uControlsUtils,
  uTControl_Function, uConstParametroFormulario, uContaReceber, uConsultaGrid, uTPessoa, uTipoQuitacaoProxy;

procedure TMovLoteRecebimento.BloquearEdicao;
begin
  inherited;
  cdsData.SomenteLeitura := not cdsDataSTATUS.AsString.Equals(TLoteRecebimentoProxy.STATUS_ABERTO);
end;

function TMovLoteRecebimento.PegarIdContaReceberSelecionados: String;
begin
  result := fdmSelecaoTitulos.FieldByName('id').AsString;
end;

class procedure TMovLoteRecebimento.AbrirTelaFiltrando(
  AIdChaveProcesso: Integer);
var
  instanciaFormulario: TMovLoteRecebimento;
  filtroChaveProcesso: TFiltroPadrao;
begin
  if AIdChaveProcesso <= 0 then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es sobre o lote');
    TControlsUtils.CongelarFormulario(Application.MainForm);
    instanciaFormulario := TMovLoteRecebimento(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio de Lote de Recebimento est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TMovLoteRecebimento(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.ActInsert.Execute;

    filtroChaveProcesso := TFiltroPadrao.Create;
    try
      filtroChaveProcesso.AddIgual('CONTA_RECEBER.ID_CHAVE_PROCESSO', AIdChaveProcesso);
      instanciaFormulario.Consultar(filtroChaveProcesso)
    finally
      FreeAndNil(filtroChaveProcesso);
    end;

    instanciaFormulario.Show;
    Application.ProcessMessages;
    instanciaFormulario.FFormularioAbertoPorOutroFormulario := true;
  finally
    TControlsUtils.DescongelarFormulario;
    TFrmMessage_Process.CloseMessage;
  end;
end;

class procedure TMovLoteRecebimento.AbrirTelaFiltrandoPorCodigosDosTitulos(const ACodigos: String;
  const AFiltrosDaTela: TDictionary<String, String> = nil);
var
  instanciaFormulario: TMovLoteRecebimento;
  filtroChaveProcesso: TFiltroPadrao;
begin
  if ACodigos.IsEmpty then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es sobre o lote');
    instanciaFormulario := TMovLoteRecebimento(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio de Lote de Recebimento est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TMovLoteRecebimento(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.ActInsert.Execute;

    filtroChaveProcesso := TFiltroPadrao.Create;
    try
      filtroChaveProcesso.AddFaixa('CONTA_RECEBER.ID', ACodigos);
      instanciaFormulario.Consultar(filtroChaveProcesso)
    finally
      FreeAndNil(filtroChaveProcesso);
    end;

    instanciaFormulario.Show;
    Application.ProcessMessages;
    instanciaFormulario.FFormularioAbertoPorOutroFormulario := true;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovLoteRecebimento.PopularConsultaComJSONDataset(ADados: TFDJSONDatasets);
begin
  try
    TDatasetUtils.JSONDatasetToMemTable(ADados, fdmSelecaoTitulos);

    TcxGridUtils.AdicionarTodosCamposNaView(ViewSelecaoTitulos);

    TUsuarioGridView.LoadGridView(ViewSelecaoTitulos,
      TSistema.Sistema.Usuario.idSeguranca,
      Self.Name+ViewSelecaoTitulos.Name);

    RemoverDaConsultaOsTitulosSelecionados;
  finally
    FreeAndNil(ADados);
  end;
end;

procedure TMovLoteRecebimento.DepoisDeConfirmar;
begin
  inherited;
  if fdmSelecaoTitulos.Active then
    fdmSelecaoTitulos.close;

  if cdsData.FieldByName('ID').AsInteger <= 0 then
  begin
    AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('LOTE_RECEBIMENTO', 'ID'), 0));
  end;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLoteRecebimento.DepoisDeCancelar;
begin
  inherited;
  if fdmSelecaoTitulos.Active then
    fdmSelecaoTitulos.close;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLoteRecebimento.PrepararFiltros;
begin
  inherited;
  //EdtConsultaEfetivacaoInicio.EditValue := StartOfTheMonth(Date);
  //EdtConsultaEfetivacaoFim.EditValue    := EndOfTheMonth(Date+30);
  EdtConsultaSituacao.EditValue         := TLoteRecebimentoProxy.CONSTANTE_TODOS;
  EdtConsultaValor.EditValue            := null;
end;

function TMovLoteRecebimento.QuitacaoEmCartao: Boolean;
begin
  result := cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovLoteRecebimento.RatearAcrescimos;
var
  SomaJuros : Double;
  SomaMulta : Double;
  SomaVlAberto   : Double;
  Percentual     : Double;
  Diferenca      : Double;
begin

  if cdsLoteRecebimentoTitulo.IsEmpty then
    Exit;

  if cdsLoteRecebimentoQuitacao.IsEmpty then
    Exit;

  SomaJuros := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_JUROS);
  SomaMulta := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_MULTA);
  SomaVlAberto   := TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_ABERTO);

  cdsLoteRecebimentoTitulo.DisableControls;
  cdsLoteRecebimentoTitulo.First;
  try
    while not cdsLoteRecebimentoTitulo.Eof do
    begin
      if not (cdsLoteRecebimentoTitulo.State in dsEditModes) then
        cdsLoteRecebimentoTitulo.Edit;

      Percentual := TMathUtils.PercentualSobreValor(
        cdsLoteRecebimentoTituloVL_ABERTO.AsFloat, SomaVlAberto);

      cdsLoteRecebimentoTituloVL_JUROS.AsFloat := TMathUtils.Arredondar(
        TMathUtils.ValorSobrePercentual(Percentual, SomaJuros));

      cdsLoteRecebimentoTituloVL_MULTA.AsFloat := TMathUtils.Arredondar(
        TMathUtils.ValorSobrePercentual(Percentual, SomaMulta));

      cdsLoteRecebimentoTitulo.Next;
    end;

    if not (cdsLoteRecebimentoTitulo.State in dsEditModes) then
      cdsLoteRecebimentoTitulo.Edit;

    Diferenca := (SomaJuros) - TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_JUROS);
    cdsLoteRecebimentoTituloVL_JUROS.AsFloat := TMathUtils.Arredondar(
      cdsLoteRecebimentoTituloVL_JUROS.AsFloat + Diferenca);

    Diferenca := (SomaMulta) - TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_MULTA);
    cdsLoteRecebimentoTituloVL_MULTA.AsFloat := TMathUtils.Arredondar(
      cdsLoteRecebimentoTituloVL_MULTA.AsFloat + Diferenca);
  finally
    cdsLoteRecebimentoTitulo.First;
    cdsLoteRecebimentoTitulo.EnableControls;
  end;
end;

procedure TMovLoteRecebimento.RatearDescontos;
var
  SomaDescontos  : Double;
  SomaVlAberto   : Double;
  Percentual     : Double;
  Diferenca      : Double;
begin

  if cdsLoteRecebimentoTitulo.IsEmpty then
    Exit;

  if cdsLoteRecebimentoQuitacao.IsEmpty then
    Exit;

  SomaDescontos  := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_DESCONTO);
  SomaVlAberto   := TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_ABERTO);

  cdsLoteRecebimentoTitulo.DisableControls;
  cdsLoteRecebimentoTitulo.First;
  try

    while not cdsLoteRecebimentoTitulo.Eof do
    begin

      if not (cdsLoteRecebimentoTitulo.State in dsEditModes) then
        cdsLoteRecebimentoTitulo.Edit;

      Percentual := TMathUtils.PercentualSobreValor(cdsLoteRecebimentoTituloVL_ABERTO.AsFloat
                                                   ,SomaVlAberto);

      cdsLoteRecebimentoTituloVL_DESCONTO.AsFloat := TMathUtils.Arredondar(
                                                      TMathUtils.ValorSobrePercentual(Percentual
                                                                                     ,SomaDescontos));

      cdsLoteRecebimentoTitulo.Next;
    end;


    if not (cdsLoteRecebimentoTitulo.State in dsEditModes) then
      cdsLoteRecebimentoTitulo.Edit;

    Diferenca := SomaDescontos - TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_DESCONTO);
    cdsLoteRecebimentoTituloVL_DESCONTO.AsFloat := TMathUtils.Arredondar(cdsLoteRecebimentoTituloVL_DESCONTO.AsFloat
                                                                     + Diferenca);

  finally
    cdsLoteRecebimentoTitulo.First;
    cdsLoteRecebimentoTitulo.EnableControls;
  end;

end;

procedure TMovLoteRecebimento.RemoverDaConsultaOsTitulosSelecionados;
begin
  if not cdsLoteRecebimentoTitulo.Active then
  begin
    Exit;
  end;

  cdsLoteRecebimentoTitulo.DisableControls;
  cdsLoteRecebimentoTitulo.First;
  fdmSelecaoTitulos.DisableControls;
  try
    while not cdsLoteRecebimentoTitulo.Eof do
    begin
      if fdmSelecaoTitulos.Locate('ID', cdsLoteRecebimentoTituloID_CONTA_Receber.AsInteger, []) then
        fdmSelecaoTitulos.Delete;

      cdsLoteRecebimentoTitulo.Next;

    end;
  finally
    cdsLoteRecebimentoTitulo.First;
    cdsLoteRecebimentoTitulo.EnableControls;
    fdmSelecaoTitulos.First;
    fdmSelecaoTitulos.EnableControls;
  end;

end;

procedure TMovLoteRecebimento.RemoverTituloDoLoteRecebimento;
begin
  if not cdsLoteRecebimentoTitulo.IsEmpty then
  begin
    cdsLoteRecebimentoTitulo.Delete;
  end;
end;

procedure TMovLoteRecebimento.CalcularValorEmAberto;
begin
  cdsLoteRecebimentoTituloIC_VL_ABERTO.AsFloat := cdsLoteRecebimentoTituloVL_ABERTO.AsFloat +
    cdsLoteRecebimentoTituloVL_ACRESCIMO.AsFloat - cdsLoteRecebimentoTituloVL_DESCONTO.AsFloat;
end;

procedure TMovLoteRecebimento.SetWhereBundle;
begin
  inherited;

  //Vencimento
  if not(VartoStr(EdtConsultaEfetivacaoInicio.editvalue).isEmpty) and
     not(VartoStr(EdtConsultaEfetivacaoFim.EditValue).IsEmpty) and
     (VarToDateTime(EdtConsultaEfetivacaoInicio.EditValue) <= VarToDateTime(EdtConsultaEfetivacaoFim.EditValue)) then
    WhereSearch.AddEntre(TLoteRecebimentoProxy.FIELD_DATA_EFETIVACAO,
      StrtoDate(EdtConsultaEfetivacaoInicio.EditValue), StrtoDate(EdtConsultaEfetivacaoFim.EditValue));

  //Situa��o
  if not VartoStr(EdtConsultaSituacao.EditValue).Equals(TLoteRecebimentoProxy.CONSTANTE_TODOS) then
    WhereSearch.AddIgual(TLoteRecebimentoProxy.FIELD_STATUS,VartoStr(EdtConsultaSituacao.EditValue));

  //Valor
  if StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0) > 0 then
    //WhereSearch.AddIgual(FIELD_VALOR_LOTE_Recebimento, StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0));
end;

procedure TMovLoteRecebimento.SugerirDataVencimentoCheque;
begin
  if not cdsLoteRecebimentoQuitacao.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsLoteRecebimentoQuitacaoCHEQUE_DT_VENCIMENTO.AsDateTime := cdsLoteRecebimentoQuitacaoDT_QUITACAO.AsDateTime;
  end
  else
  begin
    cdsLoteRecebimentoQuitacaoCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

function TMovLoteRecebimento.GetWhereBundleSelecao: TFiltroPadrao;
begin
  Result := TFiltroPadrao.Create;

  if not (filtrosTitulosDOCUMENTO.AsString).IsEmpty then
    Result.AddContem('Upper(DOCUMENTO)', UpperCase(filtrosTitulosDOCUMENTO.AsString));

  if not (filtrosTitulosID_PESSOA.AsString).IsEmpty then
    Result.AddIgual('ID_PESSOA', filtrosTitulosID_PESSOA.AsString);


  if not(filtrosTitulosDT_VENCIMENTO_INICIAL.AsString.isEmpty) and
     not(filtrosTitulosDT_VENCIMENTO_FINAL.AsString.IsEmpty) and
     (filtrosTitulosDT_VENCIMENTO_INICIAL.AsDateTime <= filtrosTitulosDT_VENCIMENTO_FINAL.AsDateTime)
  then
    Result.AddEntre('DT_VENCIMENTO',
      StrtoDate(filtrosTitulosDT_VENCIMENTO_INICIAL.AsString), StrtoDate(filtrosTitulosDT_VENCIMENTO_FINAL.AsString));

end;

procedure TMovLoteRecebimento.AbrirDatasetSelecaoTitulo;
var filtroVazio: TFiltroPadrao;
begin
  filtroVazio := TFiltroPadrao.Create;
  try
    filtroVazio.AddIgual('1', '2');
    Consultar(filtroVazio)
  finally
    FreeAndNil(filtroVazio);
  end;
end;

procedure TMovLoteRecebimento.actAlterarColunasGridTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewSelecaoTitulos);
end;

procedure TMovLoteRecebimento.actAlterarRotuloLoteTitulosExecute(Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewLoteRecebimentoTitulos);
end;

procedure TMovLoteRecebimento.actAlterarSQLLoteTitulosExecute(Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+ViewLoteRecebimentoTitulos.Name) then
    AbrirPesquisaPadrao;
end;

procedure TMovLoteRecebimento.actAlterarSQLPesquisaPadraoTitulosExecute(
  Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+ViewSelecaoTitulos.Name) then
    AbrirPesquisaPadrao;
end;

procedure TMovLoteRecebimento.RemoverTituloSelecionado;
begin
  RemoverTituloDoLoteRecebimento;
  RatearAcrescimos;
  RatearDescontos;
end;

procedure TMovLoteRecebimento.actEfetivarExecute(Sender: TObject);
var
  IDChaveProcesso : Integer;
  fecharAoConfirmar: Boolean;
begin
  inherited;
  if (cdsLoteRecebimentoQuitacao.State in dsEditmodes) then
  begin
    FFrameQuitacaoLoteRecebimento.ActConfirm.Execute;
  end;

  ValidacoesAntesDeEfetivar;

  IDChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

  if (cdsData.EstadoDeAlteracao) then
  try
    fecharAoConfirmar := FFormularioAbertoPorOutroFormulario;

    if fecharAoConfirmar then
    begin
      FFormularioAbertoPorOutroFormulario := false;
    end;

    ActConfirm.Execute;
  finally
    if fecharAoConfirmar then
    begin
      FFormularioAbertoPorOutroFormulario := true;
    end;
  end;

  if TFrmMessage.Question('Deseja Efetivar o Lote de Recebimento?') = MCONFIRMED then
  begin
    TFrmMessage_Process.SendMessage('Efetivando o Lote de Recebimento');
    try
      TLoteRecebimento.Efetivar(IDChaveProcesso);
      AtualizarRegistro;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLoteRecebimento.ValidacoesAntesDeEfetivar;
var
  VlSomaQuitacao : Double;
  VlSomaDesconto : Double;
  VlSomaAberto   : Double;
begin
  VlSomaQuitacao := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_QUITACAO));
  VlSomaDesconto := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_DESCONTO));
  VlSomaAberto   := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloIC_VL_ABERTO));

{  if (VlSomaQuitacao) > VlSomaAberto then
  begin
    TMessage.MessageInformation('O valor total da quita��o n�o pode ser maior que o valor em aberto.');
    Abort;
  end;

  if (VlSomaQuitacao) <= 0 then
  begin
    TMessage.MessageInformation('O valor total da quita��o deve possuir um valor maior do que zero.');
    Abort;
  end;}
end;

procedure TMovLoteRecebimento.ViewLoteRecebimentoTitulosDataControllerDataChanged(Sender: TObject);
var
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not ViewLoteRecebimentoTitulos.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodapeSelecionados;
  end;

  FFiltroRegistroGradeAnterior := ViewLoteRecebimentoTitulos.DataController.Filter.FilterText;
end;

procedure TMovLoteRecebimento.ViewLoteRecebimentoTitulosEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    RemoverTituloSelecionado;
end;

procedure TMovLoteRecebimento.ViewSelecaoTitulosDataControllerDataChanged(Sender: TObject);
var
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not ViewSelecaoTitulos.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodape;
  end;

  FFiltroRegistroGradeAnterior := ViewSelecaoTitulos.DataController.Filter.FilterText;
end;

procedure TMovLoteRecebimento.ViewSelecaoTitulosDblClick(Sender: TObject);
begin
  inherited;
  SelecionarTitulo;
end;

procedure TMovLoteRecebimento.ViewSelecaoTitulosEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RIGHT then
    SelecionarTitulo;
end;

procedure TMovLoteRecebimento.actExibirAgrupamentoLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  ViewLoteRecebimentoTitulos.OptionsView.GroupByBox :=
    not ViewLoteRecebimentoTitulos.OptionsView.GroupByBox;
end;

procedure TMovLoteRecebimento.actExibirAgrupamentoTitulosExecute(Sender: TObject);
begin
  inherited;
  ViewSelecaoTitulos.OptionsView.GroupByBox :=
    not ViewSelecaoTitulos.OptionsView.GroupByBox;
end;

procedure TMovLoteRecebimento.actFiltrarSelecaoTitulosExecute(Sender: TObject);
var
  filtrosSelecaoTitulo: TFiltroPadrao;
begin
  Self.SelectNext(Self, true, true);
  filtrosSelecaoTitulo := GetWhereBundleSelecao;
  try
    Consultar(filtrosSelecaoTitulo);
  finally
    FreeAndNil(filtrosSelecaoTitulo);
  end;
end;

procedure TMovLoteRecebimento.actReabrirExecute(Sender: TObject);
begin
  inherited;
  if TFrmMessage.Question('Deseja Reabrir o Lote de Recebimento?') = MCONFIRMED then
  begin
    TFrmMessage_Process.SendMessage('Reabrindo o Lote de Recebimento');
    try
      TLoteRecebimento.Reabrir(cdsDataID.AsInteger,
        FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
    finally
      TFrmMessage_Process.CloseMessage;
    end;
    AtualizarRegistro;
  end;
end;

procedure TMovLoteRecebimento.actRemoverTodosExecute(Sender: TObject);
begin
  inherited;
  if TFrmMessage.Question('Deseja remover todos os registros selecionados'+
    ' deste lote de Recebimento?') = MCONFIRMED then
  try
    TControlsUtils.CongelarFormulario(Self);
    cdsLoteRecebimentoTitulo.DisableControls;

    While not cdsLoteRecebimentoTitulo.IsEmpty do
    begin
      RemoverTituloSelecionado;
    end;
  finally
    cdsLoteRecebimentoTitulo.EnableControls;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovLoteRecebimento.actRestaurarColunasLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca,
    Self.Name + ViewLoteRecebimentoTitulos.Name);

  ViewLoteRecebimentoTitulos.RestoreDefaults;
end;

procedure TMovLoteRecebimento.actRestaurarColunasTitulosExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca,
    Self.Name + ViewSelecaoTitulos.Name);

  ViewSelecaoTitulos.RestoreDefaults;
end;

procedure TMovLoteRecebimento.actSalvarConfiguracaoLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(ViewLoteRecebimentoTitulos,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+ViewLoteRecebimentoTitulos.Name);
end;

procedure TMovLoteRecebimento.ActSalvarConfiguracoesTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(ViewSelecaoTitulos,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+ViewSelecaoTitulos.Name);
end;

procedure TMovLoteRecebimento.actSelecionarTodosExecute(Sender: TObject);
var
  i: Integer;
  codigosContaReceber: TStringList;
  fieldCodigo: TField;
begin
  inherited;
  if fdmSelecaoTitulos.IsEmpty then
    exit;
  if TFrmMessage.Question('Deseja adicionar todos os registros filtrados'+
    ' a este lote de Recebimento?') = MCONFIRMED then
  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Adicionando todos os t�tulos');
    cdsLoteRecebimentoTitulo.DisableControls;
    fdmSelecaoTitulos.DisableControls;

    fieldCodigo := fdmSelecaoTitulos.FieldByName('ID');
    codigosContaReceber := TStringList.Create;
    try
      for i := 0 to ViewSelecaoTitulos.DataController.FilteredRecordCount - 1 do
      begin
        fdmSelecaoTitulos.Recno :=  ViewSelecaoTitulos.DataController.FilteredRecordIndex[i] + 1;
        codigosContaReceber.Add(fieldCodigo.AsString);
      end;

      for i := 0 to codigosContaReceber.Count - 1 do
      begin
        fdmSelecaoTitulos.Locate('ID', codigosContaReceber[I], []);
        SelecionarTitulo;
      end;
    finally
      FreeAndNil(codigosContaReceber);
    end;
  finally
    fdmSelecaoTitulos.EnableControls;
    cdsLoteRecebimentoTitulo.EnableControls;
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovLoteRecebimento.SelecionarTitulo;
begin
  if fdmSelecaoTitulos.IsEmpty then
    exit;

  InserirTituloNoLoteRecebimento;
  RemoverRegistroInserido;
  RatearAcrescimos;
  RatearDescontos;
end;

procedure TMovLoteRecebimento.RemoverRegistroInserido;
begin
  fdmSelecaoTitulos.Delete;
end;

procedure TMovLoteRecebimento.AntesDeRemover;
begin
  inherited;
  if (cdsDataSTATUS.AsString = TLoteRecebimentoProxy.STATUS_EFETIVADO)
  then
  begin
    TMessage.MessageInformation('N�o � poss�vel Remover um Lote de Recebimento Efetivado.');
    Abort;
  end;
end;

procedure TMovLoteRecebimento.AlinharBarraTotalizadoraConsulta;
begin
  PnTotalizadorConsulta.AutoWrap := false;
  PnTotalizadorConsulta.AutoWrap := true;

  PnTotalizadorConsultaSelecionados.AutoWrap := false;
  PnTotalizadorConsultaSelecionados.AutoWrap := true;
end;

procedure TMovLoteRecebimento.AntesDeConfirmar;
begin
  inherited;
  if cdsLoteRecebimentoQuitacao.State in dsEditModes then
  begin
    fframeQuitacaoLoteRecebimento.ActConfirm.Execute;
  end;
end;

procedure TMovLoteRecebimento.AoCriarFormulario;
begin
  inherited;
  pageControlPrincipal.ActivePage := tsTitulos;
  AbrirDatasetSelecaoTitulo;

  FTotalizandoAcrescimo := false;
  FCalculandoTotalRodape := false;
  FFormularioAbertoPorOutroFormulario := false;
  FFiltroRegistroGradeAnterior := '';

  TUsuarioGridView.LoadGridView(ViewLoteRecebimentoTitulos,
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name + ViewLoteRecebimentoTitulos.Name);

  ExibirBarraTotalizadora;
  AlinharBarraTotalizadoraConsulta;
end;

procedure TMovLoteRecebimento.cdsDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
end;

procedure TMovLoteRecebimento.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  fframeQuitacaoLoteRecebimento.SetConfiguracoesIniciais(cdsLoteRecebimentoQuitacao);
  RecalcularValorEmAberto;
end;

procedure TMovLoteRecebimento.cdsDataCalcFields(DataSet: TDataSet);
begin
  inherited;
  if cdsDataVL_ABERTO.AsFloat > 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Diferen�a R$ '+FormatFloat('###,###,###,###,##0.00',
     cdsDataVL_ABERTO.AsFloat);
  end
  else if cdsDataVL_ABERTO.AsFloat < 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Troco R$ '+FormatFloat('###,###,###,###,##0.00',
     -1*cdsDataVL_ABERTO.AsFloat);
  end;
end;

procedure TMovLoteRecebimento.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime       := Now;
  cdsDataSTATUS.AsString              := TLoteRecebimentoProxy.STATUS_ABERTO;
  cdsDataID_FILIAL.AsInteger          := TFilial.GetIdFilial;
  cdsDataID_CHAVE_PROCESSO.AsInteger  :=
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_LOTE_Recebimento, 0);
  cdsDataJOIN_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_LOTE_Recebimento;
  cdsDataID_PESSOA_USUARIO.AsInteger  := TSistema.Sistema.usuario.id;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  RatearAcrescimos;
  RatearDescontos;
  AtualizarTotalLote
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoAfterEdit(
  DataSet: TDataSet);
begin
  inherited;
  //RatearAcrescimos;
  //RatearDescontos;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoAfterPost(
  DataSet: TDataSet);
begin
  inherited;
  RatearAcrescimos;
  RatearDescontos;
  AtualizarTotalLote;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoNewRecord(
  DataSet: TDataSet);
var
  vlMulta: Double;
  vlJuros: Double;
  vlAberto: Double;
begin
  inherited;
  try
    cdsLoteRecebimentoTitulo.DisableControls;
    cdsLoteRecebimentoTitulo.GuardarBookmark;
    cdsLoteRecebimentoTitulo.First;
    while not cdsLoteRecebimentoTitulo.Eof do
    begin
      vlJuros := vlJuros + cdsLoteRecebimentoTituloVL_JUROS.AsFloat;
      vlMulta := vlMulta + cdsLoteRecebimentoTituloVL_MULTA.AsFloat;
      vlAberto := vlAberto + cdsLoteRecebimentoTituloVL_ABERTO.AsFloat;
      cdsLoteRecebimentoTitulo.Next;
    end;
  finally
    cdsLoteRecebimentoTitulo.PosicionarBookmark;
    cdsLoteRecebimentoTitulo.EnableControls;
  end;

  cdsLoteRecebimentoQuitacaoID_USUARIO_BAIXA.AsInteger := TSistema.Sistema.Usuario.Id;
  cdsLoteRecebimentoQuitacaoJOIN_NOME_USUARIO_BAIXA.AsString := TSistema.Sistema.Usuario.Nome;
  cdsLoteRecebimentoQuitacaoVL_JUROS.AsFloat := vlJuros;
  cdsLoteRecebimentoQuitacaoVL_MULTA.AsFloat := vlMulta;
  cdsLoteRecebimentoQuitacaoPERC_JUROS.AsFloat :=
    TMathUtils.PercentualSobreValor(vlJuros, vlAberto);
  cdsLoteRecebimentoQuitacaoPERC_MULTA.AsFloat :=
    TMathUtils.PercentualSobreValor(vlMulta, vlAberto);

  cdsLoteRecebimentoQuitacaoDT_QUITACAO.AsDateTime := Now;
  cdsLoteRecebimentoQuitacaoNR_ITEM.AsFloat :=
    TDatasetUtils.MaiorValor(cdsLoteRecebimentoQuitacaoNR_ITEM) + 1;

  cdsLoteRecebimentoQuitacaoVL_QUITACAO.AsFloat :=
    cdsDataVL_ABERTO.AsFloat;

  if FParametroTipoQuitacaoDinheiro.ValorNumericoValido then
  begin
    TConsultaGrid.ConsultarPorTipoQuitacao(cdsLoteRecebimentoQuitacao,
      false, FParametroTipoQuitacaoDinheiro.AsString,
      'ID_TIPO_QUITACAO', 'JOIN_DESCRICAO_TIPO_QUITACAO', false);
  end;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoQuitacaoVL_QUITACAOChange(
  Sender: TField);
begin
  inherited;

  if not (cdsLoteRecebimentoQuitacao.State in dsEditModes) then
    Exit;

  AtualizarTotalItem;

end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoTituloAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
  CalcularTotaisDoRodapeSelecionados;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoTituloAfterOpen(DataSet: TDataSet);
begin
  inherited;
  RecalcularValorEmAberto;
  CalcularTotaisDoRodapeSelecionados;
end;

procedure TMovLoteRecebimento.RecalcularValorEmAberto;
var bloquearEdicao: Boolean;
begin
  if (not cdsLoteRecebimentoTitulo.Active) or (not cdsData.Active) then
      Exit;

  try
    TControlsUtils.CongelarFormulario(Self);
    bloquearEdicao := cdsData.somenteLeitura or cdsLoteRecebimentoTitulo.somenteLeitura;

    if bloquearEdicao then
    begin
      cdsData.somenteLeitura := false;
      cdsLoteRecebimentoTitulo.somenteLeitura := false;
    end;

    cdsLoteRecebimentoTitulo.DisableControls;
    try
      cdsLoteRecebimentoTitulo.First;
      while not cdsLoteRecebimentoTitulo.Eof do
      begin
        cdsLoteRecebimentoTitulo.Edit;
        CalcularValorEmAberto;
        cdsLoteRecebimentoTitulo.Post;
        cdsLoteRecebimentoTitulo.Next;
      end;
    finally
      cdsLoteRecebimentoTitulo.EnableControls;
    end;
  finally
    if cdsData.State in dsEditModes then
      cdsData.Salvar;

    if bloquearEdicao then
    begin
      cdsData.somenteLeitura := true;
      cdsLoteRecebimentoTitulo.somenteLeitura := true;
    end;

    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoTituloAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
  CalcularTotaisDoRodapeSelecionados;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoTituloBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  if FParametroLancarQuitacaoAutomaticaPorTitulo.ValorSim then
  begin
    if cdsLoteRecebimentoQuitacao.Locate('VL_TOTAL', cdsLoteRecebimentoTituloVL_ABERTO.AsFloat,[]) then
    begin
      cdsLoteRecebimentoQuitacao.Delete;
    end;
  end;
end;

procedure TMovLoteRecebimento.cdsLoteRecebimentoTituloVL_DESCONTOChange(Sender: TField);
begin
  inherited;
  CalcularValorEmAberto;
end;

procedure TMovLoteRecebimento.CalcularPercentual(Sender: TField);

  function PegarPercentualSobreValor: Double;
  var
    VlSomaAberto : Double;
  begin

    VlSomaAberto := TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_ABERTO);

    result := TMathUtils.PercentualSobreValor(
      Sender.AsFloat,
      VlSomaAberto);
  end;
begin
  if FCalculandoValor then
    exit;

  FCalculandoValor := True;

  if Sender.Dataset = cdsLoteRecebimentoQuitacao then
  begin
    if Sender = cdsLoteRecebimentoQuitacaoVL_DESCONTO then
    begin
      cdsLoteRecebimentoQuitacaoPERC_DESCONTO.AsFloat := PegarPercentualSobreValor;
    end
    else if Sender = cdsLoteRecebimentoQuitacaoVL_ACRESCIMO then
    begin
      cdsLoteRecebimentoQuitacaoPERC_ACRESCIMO.AsFloat := PegarPercentualSobreValor;
    end;

    AtualizarTotalItem;
  end;

  FCalculandoValor := False;
end;

procedure TMovLoteRecebimento.CalcularValor(Sender: TField);

  function PegarValorSobrePercentual: Double;
  var
    VlSomaAberto : Double;
  begin

    VlSomaAberto := TDatasetUtils.SomarColuna(cdsLoteRecebimentoTituloVL_ABERTO);

    result := TMathUtils.ValorSobrePercentual(
      Sender.AsFloat,
      VlSomaAberto);
  end;
begin
  if FCalculandoPercentual then
    exit;

  FCalculandoPercentual := True;

  if Sender.Dataset = cdsLoteRecebimentoQuitacao then
  begin
    if Sender = cdsLoteRecebimentoQuitacaoPERC_DESCONTO then
    begin
      cdsLoteRecebimentoQuitacaoVL_DESCONTO.AsFloat := PegarValorSobrePercentual;
    end
    else if Sender = cdsLoteRecebimentoQuitacaoPERC_ACRESCIMO then
    begin
      cdsLoteRecebimentoQuitacaoVL_ACRESCIMO.AsFloat := PegarValorSobrePercentual;
    end;

    AtualizarTotalItem;
  end;

  FCalculandoPercentual := False;
end;

procedure TMovLoteRecebimento.CalcularValoresAPartirDoTotalQuitacao(
  Sender: TField);
begin
  if FCalculandoTotalQuitacaoValor then
    exit;

  try
    FCalculandoTotalQuitacaoValor := true;
    cdsLoteRecebimentoQuitacaoVL_QUITACAO.AsFloat :=
      cdsLoteRecebimentoQuitacaoVL_TOTAL.AsFloat +
      cdsLoteRecebimentoQuitacaoVL_DESCONTO.AsFloat -
      cdsLoteRecebimentoQuitacaoVL_ACRESCIMO.AsFloat;

    fframeQuitacaoLoteRecebimento.PreencherValorRecebimento;
  finally
    FCalculandoTotalQuitacaoValor := false;
  end;

end;

procedure TMovLoteRecebimento.Consultar(AFiltro: TFiltroPadrao);
var
  DSList: TFDJSONDataSets;
begin
  try
    if filtrosTitulos.State in dsEditModes then
      filtrosTitulos.Post;

    try
      DSList := DmConnection.ServerMethodsClient.GetDados(
        Self.Name+ViewSelecaoTitulos.Name, AFiltro.where,
        TSistema.Sistema.Usuario.idSeguranca);

      PopularConsultaComJSONDataset(DSList);
      CalcularTotaisDoRodape;

      ClearWhereBundle;
    Except
      TFrmMessage.Failure('Falha ao consultar os dados, verifique o SQL associado a esta tela.');
      abort;
    end;
  finally
    HabilitarFiltrosSelecaoTitulo;
  end;
end;

procedure TMovLoteRecebimento.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Incluir Quita��o Por T�tulo
  FParametroLancarQuitacaoAutomaticaPorTitulo := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_LANCAR_QUITACAO_AUTOMATICA_POR_TITULO,
    TConstParametroFormulario.DESCRICAO_LANCAR_QUITACAO_AUTOMATICA_POR_TITULO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO,
    TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroLancarQuitacaoAutomaticaPorTitulo);

  //Parametro Tipo de Quita��o Dinheiro
  FParametroTipoQuitacaoDinheiro := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_TIPO_QUITACAO_DINHEIRO,
    TConstParametroFormulario.DESCRICAO_TIPO_QUITACAO_DINHEIRO);
  AParametros.AdicionarParametro(FParametroTipoQuitacaoDinheiro);

  //Gerar contra partida no conta corrente movimento
  FParametroGerarContraPartidaContaCorrenteMovimento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TConstParametroFormulario.DESCRICAO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroGerarContraPartidaContaCorrenteMovimento);

  //Exibir totalizadores
  FExibirBarraTotalizadora := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_BARRA_TOTALIZADORA,
    TConstParametroFormulario.DESCRICAO_EXIBIR_BARRA_TOTALIZADORA,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO,
    TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FExibirBarraTotalizadora);
end;

procedure TMovLoteRecebimento.cxSplitter1Moved(Sender: TObject);
begin
  inherited;
  AlinharBarraTotalizadoraConsulta;
end;

procedure TMovLoteRecebimento.DepoisDeAlterar;
begin
  inherited;

end;

procedure TMovLoteRecebimento.DepoisDeInserir;
begin
  inherited;

end;

procedure TMovLoteRecebimento.ExibirBarraTotalizadora;
begin
  PnTotalizadorConsulta.Visible := FExibirBarraTotalizadora.ValorSim;
  PnTotalizadorConsultaSelecionados.Visible := FExibirBarraTotalizadora.ValorSim;
end;

procedure TMovLoteRecebimento.ExibirTrocoLote;
var
  vlAberto: Double;
begin
  vlAberto := TDatasetUtils.GetAggregateAsFloat(cdsLoteRecebimentoTituloSUM_VL_ABERTO_CR) -
    cdsDataVL_QUITADO.AsFloat;

  lbTrocoDiferencaLote.Visible := false;
  if vlAberto > 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Diferen�a R$ '+FormatFloat('###,###,###,###,##0.00',
     vlAberto);
     lbTrocoDiferencaLote.Visible := true;
  end;
  {else if (cdsDataVL_ABERTO.AsFloat = 0) and
    not(cdsLoteRecebimentoQuitacao.IsEmpty) then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Troco R$ '+FormatFloat('###,###,###,###,##0.00',
     TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoIC_VL_Recebimento) -
     (cdsDataVL_QUITADO_LIQUIDO.AsFloat));
    lbTrocoDiferencaLote.Visible := true;
  end;}
  Application.ProcessMessages;
end;

procedure TMovLoteRecebimento.GerenciarControles;
var
  bEstaPesquisando: Boolean;
begin
  inherited;
  bEstaPesquisando := acaoCrud in [uFrmCadastro_Padrao.Pesquisar, uFrmCadastro_Padrao.none];
  barFiltrosLote.Visible := bEstaPesquisando;

  fframeQuitacaoLoteRecebimento.cxGBDadosMain.Enabled := (cdsDataSTATUS.AsString = TLoteRecebimentoProxy.STATUS_ABERTO);

  btnFiltrar.Enabled    := (cdsDataSTATUS.AsString = TLoteRecebimentoProxy.STATUS_ABERTO);

  HabilitarBotaoReabrir;
  HabilitarBotaoEfetivar;
  CalcularTamanhoDasVisoesDeTitulos;

  HabilitarFiltrosSelecaoTitulo;
end;

procedure TMovLoteRecebimento.HabilitarBotaoReabrir;
begin
  ActReabrir.Visible := cdsDataSTATUS.AsString = TLoteRecebimentoProxy.STATUS_EFETIVADO;
end;

procedure TMovLoteRecebimento.HabilitarFiltrosSelecaoTitulo;
begin
  if not filtrosTitulos.Active then
  begin
    filtrosTitulos.open;
  end;

  if filtrosTitulos.State in dsEditModes then
  begin
    filtrosTitulos.Post;
  end;

  filtrosTitulos.EmptyDataSet;
  filtrosTitulos.Append;

end;

procedure TMovLoteRecebimento.InserirQuitacaoAutomaticaPorTitulo;
const
  NAO_EXIBIR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = false;
begin
  FFrameQuitacaoLoteRecebimento.ActInsert.Execute;

  TContaReceber.SetEncargosEmAberto(cdsLoteRecebimentoTitulo, cdsLoteRecebimentoQuitacao);

  TConsultaGrid.ConsultarPorCentroResultado(cdsLoteRecebimentoQuitacao,
    false, cdsLoteRecebimentoTituloID_CENTRO_RESULTADO.AsString,
    'ID_CENTRO_RESULTADO', 'JOIN_DESCRICAO_CENTRO_RESULTADO', false);

  TConsultaGrid.ConsultarPorContaAnalise(cdsLoteRecebimentoQuitacao,
    false, cdsLoteRecebimentoTituloID_CONTA_ANALISE.AsString, 'ID_CONTA_ANALISE',
    'JOIN_DESCRICAO_CONTA_ANALISE', false);

  TConsultaGrid.ConsultarPorContaCorrente(cdsLoteRecebimentoQuitacao,
    false, cdsLoteRecebimentoTituloID_CONTA_CORRENTE.AsString,
    'ID_CONTA_CORRENTE', 'JOIN_DESCRICAO_CONTA_CORRENTE', false);

  cdsLoteRecebimentoQuitacaoDOCUMENTO.AsString :=
    cdsLoteRecebimentoTituloDOCUMENTO.AsString;

  cdsLoteRecebimentoQuitacaoDESCRICAO.AsString :=
    cdsLoteRecebimentoTituloDESCRICAO.AsString;

  if cdsLoteRecebimentoQuitacao.ValidarCamposObrigatorios(
    NAO_EXIBIR_MENSAGEM, NAO_ABORTAR) then
  begin
    FFrameQuitacaoLoteRecebimento.ActConfirm.Execute;
  end
  else
  begin
    FFrameQuitacaoLoteRecebimento.ActCancel.Execute;
  end;
end;

procedure TMovLoteRecebimento.InserirTituloNoLoteRecebimento;
var
  fdmTitulosSelecionados: TfdMemTable;
begin
  try
    fdmTitulosSelecionados := TFDMemTable.Create(nil);
    TLoteRecebimento.SetContasAReceber(fdmTitulosSelecionados, PegarIdContaReceberSelecionados);

    fdmTitulosSelecionados.First;
    while not fdmTitulosSelecionados.Eof do
    begin

      if cdsLoteRecebimentoTitulo.Locate('ID_CONTA_Receber', fdmTitulosSelecionados.FieldByName('ID').AsInteger, []) then
      begin
        fdmTitulosSelecionados.Next;
        continue;
      end;

      cdsLoteRecebimentoTitulo.Incluir;

      TDatasetUtils.CopiarRegistroComCondicoes(
      fdmTitulosSelecionados, //Origem
      cdsLoteRecebimentoTitulo, //Destino
      ['id', 'id_conta_Receber', 'vl_acrescimo', 'vl_decrescimo'], //No Copy
      [], //Campos para mudar
      []); //Valores para Mudar

      cdsLoteRecebimentoTituloID_CONTA_Receber.AsInteger := fdmSelecaoTitulos.FieldByName('id').AsInteger;
      cdsLoteRecebimentoTituloJOIN_NOME_PESSOA.AsString :=
        TPessoa.GetNomePessoa(cdsLoteRecebimentoTituloID_PESSOA.AsInteger);

      //� o mesmo dataset pq nele se encontra as informa��es do CR e dos encargos
      TContaReceber.SetEncargosEmAberto(cdsLoteRecebimentoTitulo, cdsLoteRecebimentoTitulo);

      cdsLoteRecebimentoTituloVL_DESCONTO.AsFloat := 0;

      CalcularValorEmAberto;

      cdsLoteRecebimentoTitulo.Salvar;

      if FParametroLancarQuitacaoAutomaticaPorTitulo.EstaPreenchido and
        FParametroTipoQuitacaoDinheiro.EstaPreenchido then
      begin
        InserirQuitacaoAutomaticaPorTitulo;
      end;

      fdmTitulosSelecionados.Next;
    end;
  finally
    fdmTitulosSelecionados.Free;
  end;
end;

procedure TMovLoteRecebimento.TotalizarAcrescimo(Sender: TField);
begin
  if (FTotalizandoAcrescimo) or (Sender.Dataset <> cdsLoteRecebimentoTitulo) then
    exit;

  try
    FTotalizandoAcrescimo := True;

    cdsLoteRecebimentoTituloVL_MULTA.AsFloat := TMathUtils.ValorSobrePercentual(
      cdsLoteRecebimentoTituloPERC_MULTA.AsFloat, cdsLoteRecebimentoTituloVL_ABERTO.AsFloat);

    cdsLoteRecebimentoTituloVL_JUROS.AsFloat := TMathUtils.ValorSobrePercentual(
      cdsLoteRecebimentoTituloPERC_JUROS.AsFloat, cdsLoteRecebimentoTituloVL_ABERTO.AsFloat);

    cdsLoteRecebimentoTituloVL_ACRESCIMO.AsFloat :=
      cdsLoteRecebimentoTituloVL_MULTA.AsFloat +
      cdsLoteRecebimentoTituloVL_JUROS.AsFloat;
  finally
    CalcularValorEmAberto;
    FTotalizandoAcrescimo := False;
  end;
end;

procedure TMovLoteRecebimento.InstanciarFrames;
begin
  inherited;
  FFrameQuitacaoLoteRecebimento := TFrameQuitacaoLoteRecebimento.Create(PnFrameQuitacao);
  FFrameQuitacaoLoteRecebimento.Parent := PnFrameQuitacao;
  FFrameQuitacaoLoteRecebimento.Align := alClient;
  FFrameQuitacaoLoteRecebimento.ExibirDadosDeBaixaPorTitulo(
    FParametroLancarQuitacaoAutomaticaPorTitulo.ValorSim);
end;

procedure TMovLoteRecebimento.AtualizarTotalItem;
begin
  cdsLoteRecebimentoQuitacaoVL_TOTAL.AsFloat := cdsLoteRecebimentoQuitacaoVL_QUITACAO.AsFloat
                                            + cdsLoteRecebimentoQuitacaoVL_ACRESCIMO.AsFloat
                                            - cdsLoteRecebimentoQuitacaoVL_DESCONTO.AsFloat;
end;

procedure TMovLoteRecebimento.AtualizarTotalLote;
var
  vlTotal, vlLiquido  : Double;
  vlDesconto : Double;
  vlAcrescimo  : Double;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  vlLiquido    := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_TOTAL);
  vlTotal      := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_QUITACAO);
  vlDesconto   := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_DESCONTO);
  vlAcrescimo  := TDatasetUtils.SomarColuna(cdsLoteRecebimentoQuitacaoVL_ACRESCIMO);

  cdsDataVL_ACRESCIMO.AsFloat  := vlAcrescimo;
  cdsDataVL_DESCONTO.AsFloat := vlDesconto;

  cdsDataVL_QUITADO.AsFloat         := vlTotal;
  cdsDataVL_QUITADO_LIQUIDO.AsFloat := vlLiquido;

  cdsDataVL_ABERTO.AsFloat :=
    TDatasetUtils.SomarColuna(
    cdsLoteRecebimentoTituloVL_ABERTO) - vlTotal;

  ExibirTrocoLote;
end;

procedure TMovLoteRecebimento.CalcularTamanhoDasVisoesDeTitulos;
var
  tamanhoDoPainel: Integer;
const
  QUANTIDADE_PAINEIS: Integer = 2;
begin
  tamanhoDoPainel := PnTitulos.Width div QUANTIDADE_PAINEIS;
  gridTitulos.Width := tamanhoDoPainel;
  cxSplitter1.Left := tamanhoDoPainel;
end;

procedure TMovLoteRecebimento.CalcularTotaisDoRodape;
var
  fdmTotais: TgbFDMemTable;
  idsContaReceber: String;
begin
  if not FExibirBarraTotalizadora.ValorSim then
  begin
    exit;
  end;

  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    idsContaReceber := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
      fdmSelecaoTitulos.FieldByName('ID'), ViewSelecaoTitulos);

    if idsContaReceber.IsEmpty then
    begin
      idsContaReceber := '-9999999';
    end;

    fdmTotais := TContaReceber.GetTotaisContaReceber(idsContaReceber);

    pnTotalizadorVlAberto.Caption := 'Aberto R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto'));
    pnTotalizadorAcrecimoAberto.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_aberto'));
    pnTotalizadorTotalAberto.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_liquido'));
    pnTotalizadorVlVencido.Caption := 'Vencido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencido'));
    pnTotalizadorVlVencer.Caption := 'A Vencer R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencer'));
    pnTotalizadorVlRecebido.Caption := 'Recebido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido'));
    pnTotalizadorAcrescimoRecebido.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_recebido'));
    pnTotalizadorTotalRecebido.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido_liquido'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;

procedure TMovLoteRecebimento.CalcularTotaisDoRodapeSelecionados;
var
  fdmTotais: TgbFDMemTable;
  idsContaReceber: String;
begin
  if not FExibirBarraTotalizadora.ValorSim then
  begin
    exit;
  end;

  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    idsContaReceber := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
      cdsLoteRecebimentoTitulo.FieldByName('ID_CONTA_RECEBER'), ViewLoteRecebimentoTitulos);

    if idsContaReceber.IsEmpty then
    begin
      idsContaReceber := '-9999999';
    end;

    fdmTotais := TContaReceber.GetTotaisContaReceber(idsContaReceber);

    pnTotalizadorVlAbertoSelecionado.Caption := 'Aberto R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto'));
    pnTotalizadorAcrecimoAbertoSelecionado.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_aberto'));
    pnTotalizadorTotalAbertoSelecionado.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_liquido'));
    pnTotalizadorVlVencidoSelecionado.Caption := 'Vencido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencido'));
    pnTotalizadorVlVencerSelecionado.Caption := 'A Vencer R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencer'));
    pnTotalizadorVlRecebidoSelecionado.Caption := 'Recebido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido'));
    pnTotalizadorAcrescimoRecebidoSelecionado.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_recebido'));
    pnTotalizadorTotalRecebidoSelecionado.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido_liquido'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;

procedure TMovLoteRecebimento.HabilitarBotaoEfetivar;
begin
  actEfetivar.Visible := cdsDataSTATUS.AsString = TLoteRecebimentoProxy.STATUS_ABERTO
end;

procedure TMovLoteRecebimento.TotalizarAcrescimoQuitacao(Sender: TField);
begin
  if (FTotalizandoAcrescimo) or (Sender.Dataset <> cdsLoteRecebimentoQuitacao) then
    exit;

  try
    FTotalizandoAcrescimo := True;

    cdsLoteRecebimentoQuitacaoVL_ACRESCIMO.AsFloat :=
      cdsLoteRecebimentoQuitacaoVL_MULTA.AsFloat +
      cdsLoteRecebimentoQuitacaoVL_JUROS.AsFloat;
  finally
    FTotalizandoAcrescimo := False;
  end;
end;

Initialization
  RegisterClass(TMovLoteRecebimento);

Finalization
  UnRegisterClass(TMovLoteRecebimento);

end.
