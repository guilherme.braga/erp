unit uMovAjusteEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxDBEdit, uGBDBDateEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, uGBPanel, cxRadioGroup, uGBDBRadioGroup,
  cxButtonEdit, uGBDBButtonEditFK, uFrameDetailPadrao, uFrameAjusteEstoqueItem,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton;

type
  TMovAjusteEstoque = class(TFrmCadastro_Padrao)
    labelCodigo: TLabel;
    labelDtCadastro: TLabel;
    labelProcesso: TLabel;
    labelSituacao: TLabel;
    pkCodigo: TgbDBTextEditPK;
    edDtCadastro: TgbDBDateEdit;
    descProcesso: TgbDBTextEdit;
    edtChaveProcesso: TgbDBTextEdit;
    edSituacao: TgbDBTextEdit;
    PnDadosCabecalho: TgbPanel;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDH_FECHAMENTO: TDateTimeField;
    cdsDataSTATUS: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_MOTIVO: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_USUARIO: TStringField;
    cdsDataJOIN_DESCRICAO_MOTIVO: TStringField;
    cdsDataJOIN_CHAVE_PROCESSO: TStringField;
    cdsDatafdqAjusteEstoqueItem: TDataSetField;
    cdsAjusteEstoqueItem: TGBClientDataSet;
    cdsAjusteEstoqueItemID: TAutoIncField;
    cdsAjusteEstoqueItemQUANTIDADE: TFMTBCDField;
    cdsAjusteEstoqueItemID_PRODUTO: TIntegerField;
    cdsAjusteEstoqueItemID_AJUSTE_ESTOQUE: TIntegerField;
    cdsAjusteEstoqueItemJOIN_DESCRICAO_PRODUTO: TStringField;
    lbDtFechamento: TLabel;
    EdtDtFechamento: TgbDBDateEdit;
    lbFilial: TLabel;
    edtFilial: TgbDBButtonEditFK;
    descFilial: TgbDBTextEdit;
    lbMotivo: TLabel;
    edtMotivo: TgbDBButtonEditFK;
    edtDescMotivo: TgbDBTextEdit;
    FrameAjusteEstoqueItem: TFrameAjusteEstoqueItem;
    cdsAjusteEstoqueItemNR_ITEM: TIntegerField;
    cdsAjusteEstoqueItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    ActReabrir: TAction;
    ActEfetivar: TAction;
    lbEfetivar: TdxBarLargeButton;
    lbReabrir: TdxBarLargeButton;
    cdsAjusteEstoqueItemTIPO: TStringField;
    ActImprimirEtiqueta: TAction;
    lbImprimirEtiqueta: TdxBarLargeButton;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ActEfetivarExecute(Sender: TObject);
    procedure ActReabrirExecute(Sender: TObject);
    procedure cdsAjusteEstoqueItemAfterOpen(DataSet: TDataSet);
    procedure cdsAjusteEstoqueItemNewRecord(DataSet: TDataSet);
    procedure cdsAjusteEstoqueItemAfterPost(DataSet: TDataSet);
    procedure cdsAjusteEstoqueItemAfterDelete(DataSet: TDataSet);
    procedure ActImprimirEtiquetaExecute(Sender: TObject);
  private
    FCalculandoEstoqueDesejado: Boolean;
    procedure HabilitarBotaoCancelar;
    procedure HabilitarBotaoEfetivar;
    procedure HabilitarBotaoRemover;
    procedure HabilitarCampoDataFechamento;
  public
    { Public declarations }
  protected
    procedure DepoisDeConfirmar; override;
    procedure GerenciarControles; override;
    procedure AoCriarFormulario; override;
    procedure BloquearEdicao; override;
    procedure AntesDeRemover; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uSistema, uChaveProcesso, uChaveProcessoProxy,
  uAjusteEstoqueProxy, uFrmMessage, uFrmMessage_Process, uAjusteEstoque,
  uDatasetUtils, uFrmAutenticacaoUsuario, uFrmEmissaoEtiqueta;

procedure TMovAjusteEstoque.ActEfetivarExecute(Sender: TObject);
var idChaveProcesso: Integer;
begin
  inherited;
  if TFrmMessage.Question('Deseja Efetivar o Ajuste de Estoque?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    if not (cdsData.State in dsEditModes) then
      cdsData.Edit;

    cdsDataDH_FECHAMENTO.AsDateTime := Now;
    cdsDataSTATUS.AsString := TAjusteEstoqueProxy.STATUS_CONFIRMADO;

    ActConfirm.Execute;

    TFrmMessage_Process.SendMessage('Efetivando o Ajuste de Estoque');
    try
      TAjusteEstoque.Efetivar(idChaveProcesso);
      AtualizarRegistro;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovAjusteEstoque.ActImprimirEtiquetaExecute(Sender: TObject);
begin
  inherited;
  TFrmEmissaoEtiqueta.EmitirEtiquetaProduto(cdsAjusteEstoqueItemID_PRODUTO, cdsAjusteEstoqueItemQUANTIDADE);
end;

procedure TMovAjusteEstoque.ActReabrirExecute(Sender: TObject);
var idChaveProcesso: Integer;
begin
  inherited;
  if not cdsDataJOIN_CHAVE_PROCESSO.AsString.Equals(TChaveProcessoProxy.ORIGEM_AJUSTE_ESTOQUE) then
  begin
    TFrmMessage.Information('N�o � poss�vel reabrir um ajuste de estoque'+
    ' que foi gerado por outro processo.'+#13+
    'Processo de Origem: '+cdsDataJOIN_CHAVE_PROCESSO.AsString);
    exit;
  end;

  if TFrmMessage.Question('Deseja Reabrir o Ajuste de Estoque?') = MCONFIRMED then
  begin
    if TFrmAutenticacaoUsuario.Autorizado then
    begin
      idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

      if (cdsData.Alterando) then
      begin
        ActConfirm.Execute;
      end;

      TFrmMessage_Process.SendMessage('Reabrindo o Ajuste de Estoque');
      try
        TAjusteEstoque.Cancelar(idChaveProcesso);
        AtualizarRegistro;
      finally
        TFrmMessage_Process.CloseMessage;
      end;
    end
    else
    begin
      TFrmMessage.Information('Usu�rio n�o possui Autoriza��o para realizar esta Opera��o.');
    end;
  end;
end;

procedure TMovAjusteEstoque.AntesDeRemover;
begin
  inherited;
  TFrmMessage.Information('N�o � permitido Remover um Ajuste de Estoque.');
  Abort;
end;

procedure TMovAjusteEstoque.AoCriarFormulario;
begin
  inherited;
  FCalculandoEstoqueDesejado := false;
end;

procedure TMovAjusteEstoque.BloquearEdicao;
begin
  inherited;
  cdsData.SomenteLeitura := not cdsDataSTATUS.AsString.Equals(TAjusteEstoqueProxy.STATUS_ABERTO);
end;

procedure TMovAjusteEstoque.cdsAjusteEstoqueItemAfterDelete(DataSet: TDataSet);
begin
  inherited;
  HabilitarBotaoEfetivar;
end;

procedure TMovAjusteEstoque.cdsAjusteEstoqueItemAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FrameAjusteEstoqueItem.SetConfiguracoesIniciais(cdsAjusteEstoqueItem);
end;

procedure TMovAjusteEstoque.cdsAjusteEstoqueItemAfterPost(DataSet: TDataSet);
begin
  inherited;
  HabilitarBotaoEfetivar;
end;

procedure TMovAjusteEstoque.cdsAjusteEstoqueItemNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsAjusteEstoqueItemNR_ITEM.AsFloat :=
    (TDatasetUtils.MaiorValor(cdsAjusteEstoqueItemNR_ITEM)+1);
end;

procedure TMovAjusteEstoque.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.filial.Fid;
  cdsDataJOIN_FANTASIA_FILIAL.AsString := TSistema.Sistema.filial.Ffantasia;
  cdsDataID_PESSOA_USUARIO.AsInteger := TSistema.Sistema.usuario.id;
  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(
    TChaveProcessoProxy.ORIGEM_AJUSTE_ESTOQUE, cdsDataID.AsInteger);
  cdsDataJOIN_CHAVE_PROCESSO.AsString :=
    TChaveProcessoProxy.ORIGEM_AJUSTE_ESTOQUE;
end;

procedure TMovAjusteEstoque.HabilitarCampoDataFechamento;
begin
  lbDtFechamento.Visible :=
    cdsDataSTATUS.AsString.Equals(TAjusteEstoqueProxy.STATUS_CONFIRMADO);
  EdtDtFechamento.Visible :=
    cdsDataSTATUS.AsString.Equals(TAjusteEstoqueProxy.STATUS_CONFIRMADO);
end;

procedure TMovAjusteEstoque.DepoisDeConfirmar;
begin
  inherited;

end;

procedure TMovAjusteEstoque.GerenciarControles;
begin
  inherited;
  HabilitarBotaoRemover;
  HabilitarBotaoCancelar;
  HabilitarBotaoEfetivar;
  HabilitarCampoDataFechamento;

  ActImprimirEtiqueta.Visible := cdsData.Active and
    cdsAjusteEstoqueItem.Active and not(cdsAjusteEstoqueItem.IsEmpty);
end;

procedure TMovAjusteEstoque.HabilitarBotaoCancelar;
begin
  ActReabrir.Visible := cdsDataSTATUS.AsString =
    TAjusteEstoqueProxy.STATUS_CONFIRMADO;
end;

procedure TMovAjusteEstoque.HabilitarBotaoEfetivar;
begin
  ActEfetivar.Visible :=
    (cdsDataSTATUS.AsString = TAjusteEstoqueProxy.STATUS_ABERTO)
    and not(cdsAjusteEstoqueItem.IsEmpty);
end;

procedure TMovAjusteEstoque.HabilitarBotaoRemover;
begin
  ActRemove.Visible :=
    cdsDataSTATUS.AsString.Equals(TAjusteEstoqueProxy.STATUS_ABERTO);
end;

initialization
  RegisterClass(TMovAjusteEstoque);

Finalization
  UnRegisterClass(TMovAjusteEstoque);

end.
