unit uFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DBXDataSnap, IPPeerClient,
  Data.DBXCommon, Data.DB, Data.SqlExpr, Datasnap.Provider, Datasnap.DBClient,
  Datasnap.DSConnect, cxPC, dxBarBuiltInMenu, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, UCDataConnector, UCDBXConn,
  UCBase, dxBar, dxBarApplicationMenu, dxRibbon, cxClasses, Vcl.AppEvnts,
  dxTabbedMDI, uDmAcesso, JvComponentBase, JvAnimTitle, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxGDIPlusClasses,
  cxImage, Vcl.ComCtrls, Vcl.CustomizeDlg, Vcl.ToolWin, Vcl.ActnCtrls,
  UCSettings, Vcl.Menus, UCMidasConn, frxClass, Vcl.StdCtrls, Vcl.ExtCtrls,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TFrmPrincipal = class(TForm)
    AppEvents: TApplicationEvents;
    dxBarManagerMain: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton13: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    dxBarButton19: TdxBarButton;
    dxBarButton20: TdxBarButton;
    dxBarButton21: TdxBarButton;
    dxBarButton22: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarButton24: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarButton26: TdxBarButton;
    dxBarButton27: TdxBarButton;
    dxBarButton28: TdxBarButton;
    dxBarButton29: TdxBarButton;
    dxBarButton30: TdxBarButton;
    siPessoa: TdxBarSubItem;
    dxBarButton31: TdxBarButton;
    dxBarButton32: TdxBarButton;
    dxBarButton33: TdxBarButton;
    dxBarButton34: TdxBarButton;
    dxBarSeparator1: TdxBarSeparator;
    dxBarSeparator2: TdxBarSeparator;
    siIBGE: TdxBarSubItem;
    dxBarSeparator3: TdxBarSeparator;
    dxBarSubItem11: TdxBarSubItem;
    dxBarSeparator4: TdxBarSeparator;
    bbCadProduto: TdxBarButton;
    dxBarSubItem12: TdxBarSubItem;
    dxBarSeparator5: TdxBarSeparator;
    dxBarButton36: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarSeparator6: TdxBarSeparator;
    dxBarSubItem5: TdxBarSubItem;
    dxBarSubItem13: TdxBarSubItem;
    dxBarSubItem14: TdxBarSubItem;
    dxBarSubItem15: TdxBarSubItem;
    dxBarSubItem16: TdxBarSubItem;
    dxBarSeparator7: TdxBarSeparator;
    dxBarButton37: TdxBarButton;
    dxBarButton38: TdxBarButton;
    dxBarButton39: TdxBarButton;
    dxBarButton40: TdxBarButton;
    dxBarButton41: TdxBarButton;
    dxBarSubItem17: TdxBarSubItem;
    dxBarButton42: TdxBarButton;
    dxBarButton43: TdxBarButton;
    dxBarButton48: TdxBarButton;
    dxBarButton49: TdxBarButton;
    dxBarButton50: TdxBarButton;
    dxBarButton51: TdxBarButton;
    dxBarButton52: TdxBarButton;
    dxBarButton53: TdxBarButton;
    dxBarSubItem18: TdxBarSubItem;
    dxBarSubItem19: TdxBarSubItem;
    dxBarSubItem20: TdxBarSubItem;
    dxBarButton54: TdxBarButton;
    siPlanoContas: TdxBarSubItem;
    dxBarButton55: TdxBarButton;
    dxBarSeparator8: TdxBarSeparator;
    dxBarSeparator9: TdxBarSeparator;
    dxBarSeparator10: TdxBarSeparator;
    dxBarSeparator11: TdxBarSeparator;
    dxBarSubItem22: TdxBarSubItem;
    dxBarSeparator12: TdxBarSeparator;
    dxBarButton56: TdxBarButton;
    dxBarButton57: TdxBarButton;
    dxBarSeparator13: TdxBarSeparator;
    dxBarSubItem23: TdxBarSubItem;
    dxBarSubItem24: TdxBarSubItem;
    dxBarSeparator14: TdxBarSeparator;
    dxBarSubItem25: TdxBarSubItem;
    dxBarSeparator15: TdxBarSeparator;
    dxBarSubItem26: TdxBarSubItem;
    dxBarButton58: TdxBarButton;
    dxBarButton59: TdxBarButton;
    dxBarButton60: TdxBarButton;
    dxBarButton61: TdxBarButton;
    dxBarButton62: TdxBarButton;
    dxBarButton63: TdxBarButton;
    dxBarButton64: TdxBarButton;
    dxBarSubItem27: TdxBarSubItem;
    dxBarSeparator16: TdxBarSeparator;
    dxBarButton65: TdxBarButton;
    dxBarButton66: TdxBarButton;
    dxBarButton67: TdxBarButton;
    dxBarButton68: TdxBarButton;
    dxBarButton69: TdxBarButton;
    dxBarButton70: TdxBarButton;
    dxBarSubItem28: TdxBarSubItem;
    dxBarButton71: TdxBarButton;
    dxBarButton72: TdxBarButton;
    dxBarButton73: TdxBarButton;
    dxBarButton74: TdxBarButton;
    dxBarButton75: TdxBarButton;
    dxBarSubItem29: TdxBarSubItem;
    dxBarButton77: TdxBarButton;
    dxBarButton78: TdxBarButton;
    dxBarButton76: TdxBarButton;
    dxBarButton79: TdxBarButton;
    dxBarButton80: TdxBarButton;
    dxBarButton81: TdxBarButton;
    dxBarButton82: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarSubItem6: TdxBarSubItem;
    dxBarSubItem8: TdxBarSubItem;
    dxBarButton23: TdxBarButton;
    dxBarSubItem9: TdxBarSubItem;
    dxBarButton6: TdxBarButton;
    dxBarButton44: TdxBarButton;
    Estado: TdxBarButton;
    dxBarButton45: TdxBarButton;
    dxBarButton46: TdxBarButton;
    dxBarButton47: TdxBarButton;
    dxBarApplicationMenu1: TdxBarApplicationMenu;
    actmgrMain: TActionManager;
    ActSair: TAction;
    ActCadPais: TAction;
    ActCadEstado: TAction;
    ActCadCidade: TAction;
    ActCadPessoa: TAction;
    ActCadProduto: TAction;
    ActCadCentroResultado: TAction;
    ActCadGrupoCentroResultado: TAction;
    ActCadContaAnalise: TAction;
    ActCadPesquisaPadrao: TAction;
    actSubItemIBGE: TAction;
    actSubItemPessoa: TAction;
    actSubItemPlanoContas: TAction;
    actSubItemProduto: TAction;
    ActMovNotaFiscal: TAction;
    MDIManager: TdxTabbedMDIManager;
    ActCadPlanoConta: TAction;
    ActCadContaCorrente: TAction;
    dxBarSeparator17: TdxBarSeparator;
    dxBarButtonContaCorrente: TdxBarButton;
    ActCadCaixa: TAction;
    dxBarButtonCaixa: TdxBarButton;
    actCadEstacao: TAction;
    bsiIndoor: TdxBarSubItem;
    bbEstacao: TdxBarButton;
    bbMidia: TdxBarButton;
    actCadMidia: TAction;
    ActMovGerenciamentoEstacao: TAction;
    dxBarButton87: TdxBarButton;
    ActMovContaCorrenteMovimento: TAction;
    dxBarButton88: TdxBarButton;
    MainImage: TcxImage;
    CustomizeDlg1: TCustomizeDlg;
    dxBarButton89: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    ActRelExtratoPlanoConta: TAction;
    dxBarButton90: TdxBarButton;
    dxBarButton91: TdxBarButton;
    dxBarButton92: TdxBarButton;
    dxBarButton93: TdxBarButton;
    ActMovContaReceber: TAction;
    ActMovGeracaoDocumento: TAction;
    dxBarSubItem21: TdxBarSubItem;
    dxBarButton94: TdxBarButton;
    dxBarButton95: TdxBarButton;
    dxBarButton96: TdxBarButton;
    dxBarButton97: TdxBarButton;
    dxBarButton98: TdxBarButton;
    dxBarButton99: TdxBarButton;
    dxBarButton100: TdxBarButton;
    dxBarButton101: TdxBarButton;
    actCadGrupoProduto: TAction;
    actCadSubGrupoProduto: TAction;
    actCadCategoriaProduto: TAction;
    actCadSubCategoriaProduto: TAction;
    actCadModeloProduto: TAction;
    actCadLinhaProduto: TAction;
    actCadMarcaProduto: TAction;
    actCadUnidadeEstoqueProduto: TAction;
    ActCadPlanoPagamento: TAction;
    bbActCadPlanoPagamento: TdxBarButton;
    dxBarButton102: TdxBarButton;
    dxBarSubItem30: TdxBarSubItem;
    dxBarButton103: TdxBarButton;
    ActCadFormaPagamento: TAction;
    ActMovTabelaPreco: TAction;
    ActMovContaPagar: TAction;
    dxBarButton104: TdxBarButton;
    dxBarButton105: TdxBarButton;
    ActCadOperacao: TAction;
    dxBarButton106: TdxBarButton;
    dxBarSubItem31: TdxBarSubItem;
    ActCadAcao: TAction;
    dxBarButton107: TdxBarButton;
    dxBarButton108: TdxBarButton;
    UCMenuPrincipal: TUCControls;
    UserControl: TUserControl;
    ActMovVendaVarejo: TAction;
    dxBarButton109: TdxBarButton;
    UCSettings: TUCSettings;
    UCMidasConn: TUCMidasConn;
    dxBarButton110: TdxBarButton;
    dxBarButton111: TdxBarButton;
    dxBarButton112: TdxBarButton;
    dxBarButton113: TdxBarButton;
    ActLogin: TAction;
    ActMudarSenha: TAction;
    ActLogoff: TAction;
    dxBarSubItem32: TdxBarSubItem;
    dxBarButton114: TdxBarButton;
    dxBarButton115: TdxBarButton;
    dxBarButton116: TdxBarButton;
    dxBarButton117: TdxBarButton;
    ActMenuCadastro: TAction;
    ActMenuOperacoes: TAction;
    ActMenuRelatorios: TAction;
    ActMenuSeguranca: TAction;
    ActMenuConfiguracoes: TAction;
    ActSubMenuIBGE: TAction;
    ActSubMenuPlanoContas: TAction;
    ActSubMenuAcao: TAction;
    ActSubMenuProduto: TAction;
    dxBarButton118: TdxBarButton;
    dxBarSeparator18: TdxBarSeparator;
    ActCadRelatorioFR: TAction;
    ActMovVendaVarejoTouch: TAction;
    dxBarButton119: TdxBarButton;
    dxBarButton120: TdxBarButton;
    actCadTipoQuitacao: TAction;
    actCadCheque: TAction;
    dxBarSubItem33: TdxBarSubItem;
    dxBarButton121: TdxBarButton;
    SBPrincipal: TStatusBar;
    dxBarButton122: TdxBarButton;
    ActRelContaCorrenteMovimento: TAction;
    ActRelVenda: TAction;
    ActRelEstoque: TAction;
    dxBarButton123: TdxBarButton;
    dxBarButton124: TdxBarButton;
    dxBarButton125: TdxBarButton;
    ActCadMotivo: TAction;
    ActMovAjusteEstoque: TAction;
    dxBarButton126: TdxBarButton;
    dxBarButton127: TdxBarButton;
    ActRelContaReceber: TAction;
    ActRelContaPagar: TAction;
    dxBarButton128: TdxBarButton;
    dxBarButton129: TdxBarButton;
    ActCadEquipamento: TAction;
    ActCadChecklist: TAction;
    ActCadSituacao: TAction;
    ActCadServico: TAction;
    ActMovOrdemServico: TAction;
    ActRelOrdemServico: TAction;
    dxBarButton130: TdxBarButton;
    dxBarButton131: TdxBarButton;
    dxBarButton132: TdxBarButton;
    dxBarButton133: TdxBarButton;
    ActMovLotePagamento: TAction;
    dxBarButton134: TdxBarButton;
    ActMovLoteRecebimento: TAction;
    dxBarButton135: TdxBarButton;
    ActCadCor: TAction;
    ActCadVeiculo: TAction;
    dxBarButton136: TdxBarButton;
    dxBarButton137: TdxBarButton;
    ActCadOcupacao: TAction;
    dxBarButton138: TdxBarButton;
    dxBarSubItem34: TdxBarSubItem;
    ActCadAreaAtuacao: TAction;
    ActCadCNAE: TAction;
    ActCadClassificacaoEmpresa: TAction;
    dxBarButton139: TdxBarButton;
    dxBarButton140: TdxBarButton;
    dxBarButton141: TdxBarButton;
    dxBarSeparator19: TdxBarSeparator;
    ActCadOperadoraCartao: TAction;
    dxBarButton142: TdxBarButton;
    ActCadSetorComercial: TAction;
    ActCadBairro: TAction;
    ActCadCEP: TAction;
    dxBarButton143: TdxBarButton;
    dxBarButton144: TdxBarButton;
    dxBarButton145: TdxBarButton;
    dxBarButton146: TdxBarButton;
    dxBarButton147: TdxBarButton;
    ActCadEmpresaFilial: TAction;
    ActCadSituacaoEspecial: TAction;
    ActCadZoneamento: TAction;
    ActCadRegimeEspecial: TAction;
    ActCadCSOSN: TAction;
    ActCadNCM: TAction;
    ActCadCarteira: TAction;
    dxBarButton148: TdxBarButton;
    dxBarButton149: TdxBarButton;
    dxBarButton150: TdxBarButton;
    dxBarButton151: TdxBarButton;
    dxBarButton152: TdxBarButton;
    dxBarButton153: TdxBarButton;
    dxBarButton154: TdxBarButton;
    ActCadMedico: TAction;
    ActCadReceitaOtica: TAction;
    dxBarButton155: TdxBarButton;
    dxBarButton156: TdxBarButton;
    ActCadConfiguracoesFiscais: TAction;
    bbConfiguracoesFiscais: TdxBarButton;
    ActCadCSTIPI: TAction;
    ActCadCSTPisCofins: TAction;
    ActCadImpostoIPI: TAction;
    ActCadImpostoPISCofins: TAction;
    ActCadRegraImposto: TAction;
    ActCadImpostoICMS: TAction;
    dxBarSubItem35: TdxBarSubItem;
    ActSubMenuAtribuicoesFiscais: TAction;
    dxBarButton157: TdxBarButton;
    dxBarButton158: TdxBarButton;
    dxBarButton159: TdxBarButton;
    dxBarButton160: TdxBarButton;
    dxBarButton161: TdxBarButton;
    dxBarButton162: TdxBarButton;
    dxBarButton163: TdxBarButton;
    ActMovInventario: TAction;
    bbInventario: TdxBarButton;
    ActFrmEmissaoEtiqueta: TAction;
    ActMenuImpressoes: TAction;
    dxBarSubItem36: TdxBarSubItem;
    dxBarButton164: TdxBarButton;
    dxBarButton165: TdxBarButton;
    ActRelFluxoCaixaResumido: TAction;
    bbSalvarConfiguracoes: TdxBarButton;
    bbRestaurarColunasPadrao: TdxBarButton;
    bbAlterarColunasGrid: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    bbAlterarSQLPesquisaPadrao: TdxBarButton;
    bbImpMalaDireta: TdxBarButton;
    ActImpMalaDireta: TAction;
    ActMovMovimentacaoCheque: TAction;
    dxBarButton166: TdxBarButton;
    ActCadModeloMontadora: TAction;
    ActCadMontadora: TAction;
    ActSubMenuMontadora: TAction;
    siMontadora: TdxBarSubItem;
    bbMontadora: TdxBarButton;
    bbModeloMontadora: TdxBarButton;
    ActMovNegociacaoContasReceber: TAction;
    dxBarButton35: TdxBarButton;
    ActCadTamanho: TAction;
    ActCadGradeProduto: TAction;
    dxBarButton167: TdxBarButton;
    dxBarButton168: TdxBarButton;
    dxBarSeparator20: TdxBarSeparator;
    dxBarSeparator21: TdxBarSeparator;
    bbBackup: TdxBarButton;
    ActBackup: TAction;
    ActCadCFOP: TAction;
    bbCFOP: TdxBarButton;
    ActCadNaturezaOperacao: TAction;
    bbNaturezaOperacao: TdxBarButton;
    bbRelOrdemServico: TdxBarButton;
    bbModeloNotaFiscal: TdxBarButton;
    ActCadModeloNotaFiscal: TAction;
    ActCadBloqueioPersonalizado: TAction;
    bbBloqueioPersonalizado: TdxBarButton;
    ActCadEnquadramentoLegalIPI: TAction;
    bbCadEnquadramentoLegalIPI: TdxBarButton;
    bsICMS: TdxBarSeparator;
    bsIPI: TdxBarSeparator;
    dxBarSeparator22: TdxBarSeparator;
    bsAtribuicoesEspeciais: TdxBarSeparator;
    bsOutrasAtribuicoes: TdxBarSeparator;
    ActCadTipoRestricao: TAction;
    dxBarButton169: TdxBarButton;
    ActFrmInutilizacaoNotaFiscal: TAction;
    dxBarButton170: TdxBarButton;
    ActMovEmissaoDocumentoFiscalVendaAgrupada: TAction;
    bbEmissaoDocumentoFiscalVendasAgrupadas: TdxBarButton;
    dxBarButton171: TdxBarButton;
    dxBarSubItem37: TdxBarSubItem;
    ActMenuManutencao: TAction;
    dxBarButton172: TdxBarButton;
    ActReprocessarCodigosEAN13: TAction;
    procedure addNewPage(Sender: TObject);
    procedure addNewPageRelatorioFR(Sender: TObject);
    procedure SemAcao(Sender: TObject);
    procedure ActSairExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActLoginExecute(Sender: TObject);
    procedure ActMudarSenhaExecute(Sender: TObject);
    procedure ActLogoffExecute(Sender: TObject);
    procedure UserControlLoginSucess(Sender: TObject; IdUser: Integer; Usuario,
      Nome, Senha, Email: string; Privileged: Boolean);
    procedure ActSubMenuAtribuicoesFiscaisExecute(Sender: TObject);
    procedure ActBackupExecute(Sender: TObject);
    procedure ActReprocessarCodigosEAN13Execute(Sender: TObject);
  private
    procedure DefinirPropriedadesUserControl;
    procedure SetVersaoDaAplicacao;
    procedure SetUsuarioLogado;
    procedure SetNomeFilial;
    procedure ExibirStatusBar;
    procedure ChecarBackup;
    procedure RemoverBackgroud;
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

uses uTFunction, uTControl_Function, uTVariables, uFrmMessage_Process,
  uFrmMessage, uDevExpressUtils, uSistema, uFrmRelatorioFRFiltroVertical,
  uControlsUtils, uFrmBackup, uBackup, uFrmInutilizacaoNotaFiscal, uProduto;

{ TFrmPrincipal }

procedure TFrmPrincipal.ActBackupExecute(Sender: TObject);
begin
  TControlFunction.CreateMDIChild(TFrmBackup, FrmBackup);
end;

procedure TFrmPrincipal.ActLoginExecute(Sender: TObject);
begin
  //Implementado pelo UserControl
end;

procedure TFrmPrincipal.ActLogoffExecute(Sender: TObject);
begin
  //Implementado pelo UserControl
end;

procedure TFrmPrincipal.ActMudarSenhaExecute(Sender: TObject);
begin
  //Implementado pelo UserControl
end;

procedure TFrmPrincipal.ActReprocessarCodigosEAN13Execute(Sender: TObject);
begin
  if TFrmMessage.Question(
    'Deseja realmente reprocessar todos os c�digos de barra EAN13 de todos os produtos?') = MCONFIRMED then
  begin
    try
      TFrmMessage_Process.SendMessage('Atualizando codigos de barras EAN13');
      TProduto.RefazerEAN13TodosProdutos;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TFrmPrincipal.ActSairExecute(Sender: TObject);
begin
  ChecarBackup;

  if TFrmMessage.Question('Deseja realmente sair?') = MCONFIRMED then
  begin
    Close;
  end;
end;

procedure TFrmPrincipal.ActSubMenuAtribuicoesFiscaisExecute(Sender: TObject);
begin
//Sub Menu Atribui��es Fiscais
end;

procedure TFrmPrincipal.addNewPage(Sender: TObject);
var className: String;
begin
  TFrmMessage_Process.SendMessage('Carregando formul�rio');
  try
    className := 'T'+Copy(TAction(Sender).Name, 4, Length(TAction(Sender).Name)-3);
    TControlFunction.CreateMDIForm(className);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmPrincipal.addNewPageRelatorioFR(Sender: TObject);
var className: String;
begin
  TFrmMessage_Process.SendMessage('Carregando formul�rio');
  try
    className := Copy(TAction(Sender).Name, 4, Length(TAction(Sender).Name)-3);
    TFrmRelatorioFRFiltroVertical.CriarFormularioRelatorio(className, 'Relat�rio de '+TAction(Sender).Caption);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmPrincipal.ChecarBackup;
begin
  if ActBackup.Visible and not(TBackup.BackupDiarioRealizado) then
  begin
    if TFrmMessage.Question('Deseja realizar backup antes de sair do sistema?') = MCONFIRMED then
    begin
      ActBackup.Execute;
      abort;
    end;
  end;
end;

procedure TFrmPrincipal.DefinirPropriedadesUserControl;
const ativo: Boolean = true;
begin
  UserControl.AutoStart := ativo;
  UserControl.LogControl.Active := ativo;
  UserControl.UserProfile.Active := ativo;
end;

procedure TFrmPrincipal.ExibirStatusBar;
begin
  SBPrincipal.Visible := true;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
  //RemoverBackgroud;

  Variables.frmMain := FrmPrincipal;
  Variables.MDIManager := MDIManager;
  Variables.UserControl := UserControl;

  DefinirPropriedadesUserControl;
  SetVersaoDaAplicacao;
end;

procedure TFrmPrincipal.RemoverBackgroud;
begin
  MainImage.Picture.Bitmap.FreeImage;
end;

procedure TFrmPrincipal.SemAcao(Sender: TObject);
begin
  // Para os menus sem a��o
end;

procedure TFrmPrincipal.SetNomeFilial;
begin
  Self.Caption := 'Gest�o Empresarial - '+TSistema.Sistema.filial.FFantasia;
end;

procedure TFrmPrincipal.SetUsuarioLogado;
begin
  SBPrincipal.Panels[1].Text := 'Usuario '+TSistema.Sistema.Usuario.usuario;
end;

procedure TFrmPrincipal.SetVersaoDaAplicacao;
begin
  SBPrincipal.Panels[0].Text := 'Vers�o '+TControlFunction.GetVersion(Application.ExeName);
end;

procedure TFrmPrincipal.UserControlLoginSucess(Sender: TObject; IdUser: Integer;
  Usuario, Nome, Senha, Email: string; Privileged: Boolean);
begin
  try
    TControlsUtils.CongelarFormulario(Self);
    TSistema.Sistema.SetUsuario(IdUser);
    SetUsuarioLogado;
    SetNomeFilial;
    ExibirStatusBar;
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

end.
