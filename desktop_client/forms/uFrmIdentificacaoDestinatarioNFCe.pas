unit uFrmIdentificacaoDestinatarioNFCe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, cxLabel, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, cxTextEdit, cxMaskEdit;

type
  TFrmIdentificacaoDestinatarioNFCe = class(TFrmModalPadrao)
    cxLabel1: TcxLabel;
    EdtCPF: TcxMaskEdit;
    procedure EdtCPFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    class function CPFNaNota: String;
    procedure Confirmar; override;
  end;

var
  FrmIdentificacaoDestinatarioNFCe: TFrmIdentificacaoDestinatarioNFCe;

implementation

{$R *.dfm}

uses uTPessoa;

{ TFrmIdentificacaoDestinatarioNFCe }

procedure TFrmIdentificacaoDestinatarioNFCe.Confirmar;
begin
  if TPessoa.ValidaCnpjCeiCpf(EdtCPF.Text) then
  begin
    inherited;
  end
  else
  begin
    abort;
  end;
end;

class function TFrmIdentificacaoDestinatarioNFCe.CPFNaNota: String;
begin
  try
    Application.CreateForm(TFrmIdentificacaoDestinatarioNFCe, FrmIdentificacaoDestinatarioNFCe);
    FrmIdentificacaoDestinatarioNFCe.ShowModal;
    if FrmIdentificacaoDestinatarioNFCe.result = MCONFIRMED then
    begin
      result := FrmIdentificacaoDestinatarioNFCe.EdtCPF.Text;
    end
    else
    begin
      result := '';
    end;
  finally
    FreeAndNil(FrmIdentificacaoDestinatarioNFCe);
  end;
end;

procedure TFrmIdentificacaoDestinatarioNFCe.EdtCPFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    ActConfirmar.Execute;
  end;
end;

end.
