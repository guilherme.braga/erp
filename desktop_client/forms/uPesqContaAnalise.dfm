inherited PesqContaAnalise: TPesqContaAnalise
  Caption = 'Consulta de Conta de An'#225'lise'
  ClientHeight = 454
  ClientWidth = 585
  OnCreate = FormCreate
  ExplicitWidth = 591
  ExplicitHeight = 483
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 585
    ExplicitHeight = 398
    Height = 398
    Width = 585
    object TreeListPlanoConta: TcxDBTreeList
      AlignWithMargins = True
      Left = 5
      Top = 5
      Width = 575
      Height = 388
      Align = alClient
      Bands = <
        item
        end>
      DataController.DataSource = dsPlanoContaItem
      DataController.ImageIndexField = 'BO_HABILITADO'
      DataController.ParentField = 'ID_ASSOCIACAO'
      DataController.KeyField = 'ID'
      DataController.StateIndexField = 'BO_HABILITADO'
      Images = DmAcesso.cxImage16x16
      LookAndFeel.Kind = lfOffice11
      Navigator.Buttons.CustomButtons = <>
      OptionsCustomizing.BandCustomizing = False
      OptionsCustomizing.BandHorzSizing = False
      OptionsCustomizing.BandMoving = False
      OptionsCustomizing.BandVertSizing = False
      OptionsData.CancelOnExit = False
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsData.CheckHasChildren = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.DynamicIndent = True
      OptionsView.TreeLineStyle = tllsSolid
      RootValue = -1
      TabOrder = 0
      OnDblClick = TreeListPlanoContaDblClick
      object cxDBTreeList1SEQUENCIA: TcxDBTreeListColumn
        Caption.AlignVert = vaTop
        DataBinding.FieldName = 'SEQUENCIA'
        Width = 88
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object cxDBTreeList1DESCRICAO: TcxDBTreeListColumn
        Caption.AlignVert = vaTop
        DataBinding.FieldName = 'DESCRICAO'
        Width = 809
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ColumnIcon: TcxDBTreeListColumn
        PropertiesClassName = 'TcxImageProperties'
        DataBinding.FieldName = 'BO_HABILITADO'
        Width = 27
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 398
    ExplicitTop = 398
    ExplicitWidth = 585
    Width = 585
    inherited BtnConfirmar: TcxButton
      Left = 433
      ExplicitLeft = 433
    end
    inherited BtnCancelar: TcxButton
      Left = 508
      ExplicitLeft = 508
    end
  end
  inherited ActionList: TActionList
    Left = 142
    Top = 182
  end
  object fdmVisaoPlanoConta: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 294
    Top = 40
    object fdmVisaoPlanoContaID: TIntegerField
      FieldName = 'ID'
    end
    object fdmVisaoPlanoContaSEQUENCIA: TStringField
      FieldName = 'SEQUENCIA'
    end
    object fdmVisaoPlanoContaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object fdmVisaoPlanoContaNIVEL: TIntegerField
      FieldName = 'NIVEL'
    end
    object fdmVisaoPlanoContaID_ASSOCIACAO: TIntegerField
      FieldName = 'ID_ASSOCIACAO'
    end
    object fdmVisaoPlanoContaBO_HABILITADO: TIntegerField
      FieldName = 'BO_HABILITADO'
    end
  end
  object dsPlanoContaItem: TDataSource
    DataSet = fdmVisaoPlanoConta
    Left = 222
    Top = 56
  end
end
