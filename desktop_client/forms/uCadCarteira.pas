unit uCadCarteira;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxCheckBox, cxDBEdit,
  uGBDBCheckBox, uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  cxMaskEdit, cxDropDownEdit, cxCalc, uGBDBCalcEdit, cxSpinEdit, uGBDBSpinEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinscxPCPainter, dxSkinsdxBarPainter, cxSplitter, JvExControls, JvButton, JvTransparentButton, uGBPanel,
  cxButtonEdit, uGBDBButtonEditFK, cxRadioGroup, uGBDBRadioGroup, cxBlobEdit, uGBDBBlobEdit, JvExStdCtrls,
  JvCombobox, ugbDBComboBox, uUsuarioDesignControl, JvDBCombobox, JvBaseDlg, JvSelectDirectory;

type
  TCadCarteira = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    ePkCodig: TgbDBTextEditPK;
    gbDBCheckBox1: TgbDBCheckBox;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataPERC_JUROS: TFMTBCDField;
    cdsDataPERC_MULTA: TFMTBCDField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataCARENCIA: TIntegerField;
    cdsDataOBSERVACAO: TBlobField;
    gbPanel1: TgbPanel;
    Label2: TLabel;
    labelPercLucro: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edDescricao: TgbDBTextEdit;
    edtPercLucro: TgbDBCalcEdit;
    gbDBCalcEdit1: TgbDBCalcEdit;
    gbDBSpinEdit1: TgbDBSpinEdit;
    gbPanel2: TgbPanel;
    pcBoletoCedente: TcxPageControl;
    tsCedente: TcxTabSheet;
    tsBoleto: TcxTabSheet;
    Label5: TLabel;
    EdtNomeCedente: TgbDBTextEdit;
    Label6: TLabel;
    EdtCarteiraCedente: TgbDBTextEdit;
    Label7: TLabel;
    EdtDocFederalCedente: TgbDBTextEdit;
    Label8: TLabel;
    EdtBancoCedente: TgbDBTextEdit;
    Label9: TLabel;
    EdtClasseCarteiraCedente: TgbDBTextEdit;
    Label10: TLabel;
    EdtNomeCedentePersonalizado: TgbDBTextEdit;
    Label11: TLabel;
    gbDBTextEdit7: TgbDBTextEdit;
    Label12: TLabel;
    gbDBTextEdit8: TgbDBTextEdit;
    Label13: TLabel;
    EdtCodigoCedente: TgbDBTextEdit;
    lbContaCorrente: TLabel;
    descContaCorrente: TgbDBTextEdit;
    edtContaCorrente: TgbDBButtonEditFK;
    Label15: TLabel;
    Label16: TLabel;
    gbDBCheckBox2: TgbDBCheckBox;
    Label17: TLabel;
    Label18: TLabel;
    gbDBCalcEdit2: TgbDBCalcEdit;
    gbDBCalcEdit3: TgbDBCalcEdit;
    gbDBSpinEdit2: TgbDBSpinEdit;
    Label19: TLabel;
    gbDBCalcEdit4: TgbDBCalcEdit;
    Label20: TLabel;
    gbDBCalcEdit5: TgbDBCalcEdit;
    gbDBCalcEdit6: TgbDBCalcEdit;
    Label21: TLabel;
    gbDBCheckBox3: TgbDBCheckBox;
    Label22: TLabel;
    gbDBSpinEdit3: TgbDBSpinEdit;
    Label23: TLabel;
    gbDBSpinEdit4: TgbDBSpinEdit;
    Label24: TLabel;
    EdtProximoNumero: TgbDBSpinEdit;
    cxTabSheet1: TcxTabSheet;
    gbDBRadioGroup1: TgbDBRadioGroup;
    Label25: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    Label26: TLabel;
    gbDBBlobEdit2: TgbDBBlobEdit;
    Label27: TLabel;
    gbDBBlobEdit3: TgbDBBlobEdit;
    cbLayoutBoleto: TgbDBComboBox;
    Label28: TLabel;
    cbLayoutRemessa: TgbDBComboBox;
    Label29: TLabel;
    cbLayoutRetorno: TgbDBComboBox;
    Label30: TLabel;
    Label31: TLabel;
    EdtSequenciaRemessa: TgbDBSpinEdit;
    Label32: TLabel;
    EdtDiretorioRemessa: TcxDBButtonEdit;
    Label33: TLabel;
    EdtDiretorioRetorno: TcxDBButtonEdit;
    Label34: TLabel;
    gbDBBlobEdit4: TgbDBBlobEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit12: TgbDBTextEdit;
    Label35: TLabel;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit13: TgbDBTextEdit;
    Label36: TLabel;
    cdsDataTIPO_BOLETO: TStringField;
    cdsDataCBX_ARQUIVO_LICENCA: TBlobField;
    cdsDataCEDENTE_NOME: TStringField;
    cdsDataCEDENTE_DOC_FEDERAL: TStringField;
    cdsDataCEDENTE_BANCO: TStringField;
    cdsDataCEDENTE_CARTEIRA: TStringField;
    cdsDataCEDENTE_NOME_PERSONALIZADO: TStringField;
    cdsDataCEDENTE_AGENCIA: TStringField;
    cdsDataCEDENTE_CONTA_CORRENTE: TStringField;
    cdsDataBOLETO_NOSSO_NUMERO_INICIAL: TIntegerField;
    cdsDataBOLETO_NOSSO_NUMERO_FINAL: TIntegerField;
    cdsDataBOLETO_NOSSO_NUMERO_PROXIMO: TIntegerField;
    cdsDataBOLETO_MODALIDADE: TIntegerField;
    cdsDataBOLETO_CODIGO_TRANSMISSAO: TIntegerField;
    cdsDataBOLETO_LOCAL_PAGAMENTO: TStringField;
    cdsDataBOLETO_INSTRUCOES: TBlobField;
    cdsDataBOLETO_DEMONSTRATIVO: TBlobField;
    cdsDataBOLETO_LAYOUT_IMPRESSAO: TStringField;
    cdsDataBOLETO_LAYOUT_REMESSA: TStringField;
    cdsDataBOLETO_LAYOUT_RETORNO: TStringField;
    cdsDataBOLETO_PATH_REMESSA: TStringField;
    cdsDataBOLETO_PATH_RETORNO: TStringField;
    cdsDataBOLETO_ESPECIE_DOCUMENTO: TStringField;
    cdsDataBOLETO_SEQUENCIA_REMESSA: TIntegerField;
    cdsDataBOLETO_AMBIENTE: TStringField;
    cdsDataBOLETO_DIAS_PROTESTO: TIntegerField;
    cdsDataBO_ACEITE_BOLETO: TStringField;
    cdsDataBOLETO_VL_ACRESCIMO: TBCDField;
    cdsDataBOLETO_VL_TARIFA_BANCARIA: TBCDField;
    cdsDataBOLETO_PERC_MULTA: TBCDField;
    cdsDataBOLETO_PERC_MORA_DIARIA: TBCDField;
    cdsDataBOLETO_PERC_DESCONTO: TBCDField;
    cdsDataBOLETO_MOEDA: TStringField;
    cdsDataBO_ENVIA_BOLETO_EMAIL: TStringField;
    cdsDataBOLETO_ASSUNTO_EMAIL: TStringField;
    cdsDataBOLETO_LAYOUT_BOLETO: TStringField;
    cdsDataBOLETO_ID_RECIBO_EMAIL_PERSONALIZADO: TIntegerField;
    cdsDataBOLETO_ID_RECIBO_BOLETO_PERSONALIZADO: TIntegerField;
    cdsDataBOLETO_PREFIXO_NOSSONUMERO: TIntegerField;
    odCobreBemX: TOpenDialog;
    ActCarregarArquivoConfiguracaoCobreBemX: TAction;
    ActDescarregarArquivoConfiguracaoCobreBemX: TAction;
    bbCarregarArquivoLicensaCobreBemX: TdxBarLargeButton;
    bbRemoverArquivoLicensaCobreBemX: TdxBarLargeButton;
    cdsDataBOLETO_TIPO_DOCUMENTO: TStringField;
    cdsDataBOLETO_LAYOUT_EMAIL: TStringField;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cbTipoDocumento: TgbDBComboBox;
    Label37: TLabel;
    cbLayoutEmail: TgbDBComboBox;
    cdsDataCEDENTE_CODIGO: TStringField;
    sdSelecionarDiretorio: TJvSelectDirectory;
    Label14: TLabel;
    gbDBTextEdit10: TgbDBTextEdit;
    procedure ActCarregarArquivoConfiguracaoCobreBemXExecute(Sender: TObject);
    procedure ActDescarregarArquivoConfiguracaoCobreBemXExecute(Sender: TObject);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure EdtDiretorioRetornoClick(Sender: TObject);
    procedure EdtDiretorioRemessaClick(Sender: TObject);
  private
    FCobreBemX: OleVariant;
    FParametroExibirDadosBoleto: TParametroFormulario;
    FSalvandoInternamente: Boolean;

    procedure ExibirDadosBoleto;
    procedure ExibirBotaoCarregarArquivoLicensaCobreBemX;
    procedure ExibirBotaoRemoverArquivoLicensaCobreBemX;
    procedure ConfigurarCamposLayoutCobreBemX;

    procedure InstanciarCobreBemX;
    procedure PreencherDadosAtravesArquivoLicenca;
    procedure LimparInstanciaCobreBemX;

    procedure ConfigurarOpenDialogCobreBemX;
    procedure ConfigurarSelectDirectoryArquivosRemessaRetorno;

    procedure CarregarArquivoLicensaCobreBemX;
    procedure CarregarArquivoConfiguracaoCobreBemX;
    procedure CarregarComboBoxLayoutBoletoCobreBemX;
    procedure CarregarComboBoxLayoutRemessaCobreBemX;
    procedure CarregarComboBoxLayoutRetornoCobreBemX;
    procedure CarregarComboBoxTipoDocumentoCobreBemX;
    procedure CarregarComboBoxLayoutBoletoEmail;

    procedure ValidarCamposObrigatorios;

    procedure FocarPrimeiraAbaBoleto;
  public

  protected
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;

    procedure AoCriarFormulario; override;
    procedure GerenciarControles; override;
    procedure AoDestruirFormulario; override;
    procedure AntesDeConfirmar; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uConstParametroFormulario, uFrmMessage, ComObj, uDatasetUtils, uCarteira, uControlsUtils;

{ TCadCarteira }

procedure TCadCarteira.ActCarregarArquivoConfiguracaoCobreBemXExecute(Sender: TObject);
var
  estaEmEdicao: Boolean;
begin
  inherited;
  if odCobreBemX.Execute then
  begin
    try
      TControlsUtils.CongelarFormulario(Self);

      estaEmEdicao := cdsData.EstadoDeAlteracao;

      if not estaEmEdicao then
      begin
        ActUpdate.Execute;
      end;

      InstanciarCobreBemX;
      FCobreBemX.ArquivoLicenca := odCobreBemX.FileName;

      cdsDataCBX_ARQUIVO_LICENCA.AsString := FCobreBemX.ArquivoLicencaTexto;

      CarregarArquivoConfiguracaoCobreBemX;
      PreencherDadosAtravesArquivoLicenca;

      FSalvandoInternamente := true;
      ActConfirm.Execute;

      if estaEmEdicao then
      begin
        ActUpdate.Execute;
      end;
    finally
      FSalvandoInternamente := false;
      TControlsUtils.DescongelarFormulario;
    end;
  end;
end;

procedure TCadCarteira.ActDescarregarArquivoConfiguracaoCobreBemXExecute(Sender: TObject);
var
  estaEmEdicao: Boolean;
begin
  inherited;
  if TFrmMessage.Question('Deseja realmente remover o arquivo de licensa do CobreBemX?') = MCONFIRMED then
  begin
    try
      TControlsUtils.CongelarFormulario(Self);
      estaEmEdicao := cdsData.EstadoDeAlteracao;

      if not estaEmEdicao then
      begin
        ActUpdate.Execute;
      end;

      cdsDataCBX_ARQUIVO_LICENCA.Clear;

      FSalvandoInternamente := true;
      ActConfirm.Execute;

      if estaEmEdicao then
      begin
        ActUpdate.Execute;
      end;

      ExibirDadosBoleto;
      LimparInstanciaCobreBemX;
    finally
      FSalvandoInternamente := false;
      TControlsUtils.DescongelarFormulario;
    end;
  end;
end;

procedure TCadCarteira.AntesDeConfirmar;
begin
  inherited;
  ValidarCamposObrigatorios;
end;

procedure TCadCarteira.AoCriarFormulario;
begin
  inherited;
  ConfigurarOpenDialogCobreBemX;
  ConfigurarSelectDirectoryArquivosRemessaRetorno;
  ConfigurarCamposLayoutCobreBemX;
  FSalvandoInternamente := false;
end;

procedure TCadCarteira.AoDestruirFormulario;
begin
  inherited;
  LimparInstanciaCobreBemX;
end;

procedure TCadCarteira.CarregarArquivoConfiguracaoCobreBemX;
begin
  InstanciarCobreBemX;
  FCobreBemX.ArquivoLicencaTexto := cdsDataCBX_ARQUIVO_LICENCA.AsString;
end;

procedure TCadCarteira.CarregarArquivoLicensaCobreBemX;
begin
  ExibirDadosBoleto;
  ExibirBotaoCarregarArquivoLicensaCobreBemX;
  ExibirBotaoRemoverArquivoLicensaCobreBemX;

  if cdsDataCBX_ARQUIVO_LICENCA.AsString.IsEmpty then
  begin
    Exit;
  end;

  CarregarArquivoConfiguracaoCobreBemX;
  CarregarComboBoxLayoutBoletoCobreBemX;
  CarregarComboBoxLayoutRemessaCobreBemX;
  CarregarComboBoxLayoutRetornoCobreBemX;
  CarregarComboBoxTipoDocumentoCobreBemX;
  CarregarComboBoxLayoutBoletoEmail;
end;

procedure TCadCarteira.CarregarComboBoxLayoutBoletoCobreBemX;
var
  i: Integer;
begin
  cbLayoutBoleto.Properties.Items.Clear;
  for i := 0 to FCobreBemX.LayoutsBoleto.Count - 1 do
  begin
    cbLayoutBoleto.Properties.Items.Add(FCobreBemX.LayoutsBoleto[i]);
  end;
end;

procedure TCadCarteira.CarregarComboBoxLayoutBoletoEmail;
var
  i: Integer;
begin
  cbLayoutEmail.Properties.Items.Clear;
  for i := 0 to FCobreBemX.LayoutsBoletoEmail.Count - 1 do
    cbLayoutEmail.Properties.Items.Add(FCobreBemX.LayoutsBoletoEmail[i]);
end;

procedure TCadCarteira.CarregarComboBoxLayoutRemessaCobreBemX;
var
  i: Integer;
begin
  cbLayoutRemessa.Properties.Items.Clear;
  for i := 0 to FCobreBemX.LayoutsArquivoRemessa.Count - 1 do
    cbLayoutRemessa.Properties.Items.Add(FCobreBemX.LayoutsArquivoRemessa[i]);
end;

procedure TCadCarteira.CarregarComboBoxLayoutRetornoCobreBemX;
var
  i: Integer;
begin
  cbLayoutRetorno.Properties.Items.Clear;
  for i := 0 to FCobreBemX.LayoutsArquivoRetorno.Count - 1 do
    cbLayoutRetorno.Properties.Items.Add(FCobreBemX.LayoutsArquivoRetorno[i]);
end;

procedure TCadCarteira.CarregarComboBoxTipoDocumentoCobreBemX;
var
  i: Integer;
begin
  cbTipoDocumento.Properties.Items.Clear;
  for i := 0 to FCobreBemX.TiposDocumentosCobranca.Count - 1 do
    cbTipoDocumento.Properties.Items.Add(FCobreBemX.TiposDocumentosCobranca[i].Codigo +' <> '+ FCobreBemX.TiposDocumentosCobranca[i].Nome );
end;

procedure TCadCarteira.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataBO_ENVIA_BOLETO_EMAIL.AsString := 'N';
end;

procedure TCadCarteira.ConfigurarCamposLayoutCobreBemX;
var
  i: Integer;
begin
  EdtNomeCedente.gbReadyOnly := true;
  EdtCarteiraCedente.gbReadyOnly := true;
  EdtDocFederalCedente.gbReadyOnly := true;
  EdtBancoCedente.gbReadyOnly := true;
  EdtClasseCarteiraCedente.gbReadyOnly := true;
  EdtCodigoCedente.gbReadyOnly := true;
  EdtProximoNumero.gbReadyOnly := true;
  EdtProximoNumero.gbReadyOnly := true;
  EdtSequenciaRemessa.gbReadyOnly := true;
end;

procedure TCadCarteira.ConfigurarOpenDialogCobreBemX;
begin
  odCobreBemX.InitialDir := ExtractFilePath(Application.ExeName);
end;

procedure TCadCarteira.ConfigurarSelectDirectoryArquivosRemessaRetorno;
begin
  sdSelecionarDiretorio.InitialDir := ExtractFilePath(Application.ExeName);
end;

procedure TCadCarteira.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Exibir Dados do Boleto / Cedente
  FParametroExibirDadosBoleto := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_BOLETO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_BOLETO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirDadosBoleto);
end;

procedure TCadCarteira.EdtDiretorioRemessaClick(Sender: TObject);
begin
  inherited;
  if sdSelecionarDiretorio.Execute then
  begin
    cdsDataBOLETO_PATH_REMESSA.AsString := sdSelecionarDiretorio.Directory;
  end;
end;

procedure TCadCarteira.EdtDiretorioRetornoClick(Sender: TObject);
begin
  inherited;
  if sdSelecionarDiretorio.Execute then
  begin
    cdsDataBOLETO_PATH_RETORNO.AsString := sdSelecionarDiretorio.Directory;
  end;
end;

procedure TCadCarteira.ExibirBotaoCarregarArquivoLicensaCobreBemX;
begin
  ActCarregarArquivoConfiguracaoCobreBemX.Visible := (acaoCrud <> TAcaoCrud(Pesquisar))
    and FParametroExibirDadosBoleto.ValorSim and cdsDataCBX_ARQUIVO_LICENCA.AsString.IsEmpty;
end;

procedure TCadCarteira.ExibirBotaoRemoverArquivoLicensaCobreBemX;
begin
  ActDescarregarArquivoConfiguracaoCobreBemX.Visible := (acaoCrud <> TAcaoCrud(Pesquisar))
    and FParametroExibirDadosBoleto.ValorSim and (not cdsDataCBX_ARQUIVO_LICENCA.AsString.IsEmpty);
end;

procedure TCadCarteira.ExibirDadosBoleto;
begin
  FocarPrimeiraAbaBoleto;

  pcBoletoCedente.Visible := FParametroExibirDadosBoleto.ValorSim and
    (not cdsDataCBX_ARQUIVO_LICENCA.AsString.IsEmpty);
end;

procedure TCadCarteira.FocarPrimeiraAbaBoleto;
begin
  pcBoletoCedente.ActivepageIndex := 0;
end;

procedure TCadCarteira.GerenciarControles;
begin
  inherited;
  CarregarArquivoLicensaCobreBemX;
end;

procedure TCadCarteira.InstanciarCobreBemX;
begin
  LimparInstanciaCobreBemX;

  FCobreBemX := CreateOleObject('CobreBemX.ContaCorrente');
end;

procedure TCadCarteira.LimparInstanciaCobreBemX;
begin
  FCobreBemX := Unassigned;
end;

procedure TCadCarteira.PreencherDadosAtravesArquivoLicenca;
begin
  cdsDataCEDENTE_NOME.AsString :=  FCobreBemX.NomeCedente;
  cdsDataCEDENTE_NOME_PERSONALIZADO.AsString := FCobreBemX.NomeCedente;
  cdsDataCEDENTE_DOC_FEDERAL.AsString := FCobreBemX.CnpjCpfCedente;
  cdsDataCEDENTE_BANCO.AsString := FCobreBemX.NumeroBanco;
  cdsDataCEDENTE_CODIGO.AsString := FCobreBemX.CodigoCarteira;
  cdsDataCEDENTE_CARTEIRA.AsString := FCobreBemX.NomeCarteira;
  cdsDataBOLETO_NOSSO_NUMERO_PROXIMO.AsInteger :=
    TCarteira.BuscarProximaSequenciaNossoNumero(cdsDataID.AsInteger);
  cdsDataBOLETO_SEQUENCIA_REMESSA.AsInteger := TCarteira.BuscarProximaSequenciaRemessa(cdsDataID.AsInteger);
  cdsDataBOLETO_PERC_MULTA.AsFloat := cdsDataPERC_MULTA.AsFloat;
  cdsDataBOLETO_PERC_MORA_DIARIA.AsFloat := cdsDataPERC_JUROS.AsFloat;
end;

procedure TCadCarteira.ValidarCamposObrigatorios;
begin
  if FSalvandoInternamente then
  begin
    exit;
  end;

  if not cdsDataCBX_ARQUIVO_LICENCA.AsString.IsEmpty then
  begin
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_NOME);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_NOME_PERSONALIZADO);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_CARTEIRA);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_DOC_FEDERAL);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_BANCO);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_AGENCIA);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_CONTA_CORRENTE);
    TValidacaoCampo.CampoPreenchido(cdsDataCEDENTE_CODIGO);
    TValidacaoCampo.CampoPreenchido(cdsDataBOLETO_LAYOUT_BOLETO);
    TValidacaoCampo.CampoPreenchido(cdsDataBOLETO_LAYOUT_REMESSA);
    TValidacaoCampo.CampoPreenchido(cdsDataBOLETO_LAYOUT_RETORNO);
    TValidacaoCampo.CampoPreenchido(cdsDataBOLETO_PATH_REMESSA);
    TValidacaoCampo.CampoPreenchido(cdsDataBOLETO_PATH_RETORNO);
  end;
end;

initialization
  RegisterClass(TCadCarteira);

finalization
  UnRegisterClass(TCadCarteira);

end.
