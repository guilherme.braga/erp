unit uFrmSolicitarValor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, cxLabel, cxTextEdit, cxDBEdit, uGBDBTextEdit, uGBPanel,
  cxMaskEdit, cxDropDownEdit, cxCalc;

type
  TFrmSolicitarValor = class(TFrmModalPadrao)
    gbPanel1: TgbPanel;
    lbDigiteValor: TcxLabel;
    EdtProblemaFoco: TEdit;
    EdtValor: TcxCalcEdit;
    EdtFoco: TcxCalcEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    class function SolicitarValor(const ATituloFormulario: String;
      var AValorRetorno: Double): Boolean;
  protected
    procedure Confirmar; override;
  end;

var
  FrmSolicitarValor: TFrmSolicitarValor;

implementation

{$R *.dfm}

{ TFrmSolicitarValor }

procedure TFrmSolicitarValor.Confirmar;
begin
  EdtFoco.SetFocus;
  inherited;
end;

procedure TFrmSolicitarValor.FormShow(Sender: TObject);
begin
  inherited;
  FrmSolicitarValor.EdtValor.SetFocus;
end;

class function TFrmSolicitarValor.SolicitarValor(
  const ATituloFormulario: String; var AValorRetorno: Double): Boolean;
begin
  try
    Application.CreateForm(TFrmSolicitarValor, FrmSolicitarValor);
    FrmSolicitarValor.lbDigiteValor.Caption := ATituloFormulario;

    FrmSolicitarValor.ShowModal;

    result := FrmSolicitarValor.Result = MCONFIRMED;

    if result then
    begin
      AValorRetorno := StrtoFloatDef(VartoStr(FrmSolicitarValor.EdtValor.EditValue), 0);
    end;
  finally
    FreeAndNil(FrmSolicitarValor);
  end;
end;

end.
