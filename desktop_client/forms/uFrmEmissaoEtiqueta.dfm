inherited FrmEmissaoEtiqueta: TFrmEmissaoEtiqueta
  Caption = 'Emiss'#227'o de Etiquetas'
  ClientWidth = 845
  ExplicitWidth = 861
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 845
    ExplicitWidth = 845
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    ExplicitWidth = 845
    Width = 845
    object cxGridPesquisaPadrao: TcxGrid
      Left = 2
      Top = 31
      Width = 841
      Height = 305
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object viewProdutos: TcxGridDBBandedTableView
        PopupMenu = dxRibbonRadialMenu
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        DataController.DataSource = dsProdutos
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.CancelOnExit = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.ShowColumnFilterButtons = sfbAlways
        OptionsView.BandHeaders = False
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
          end>
      end
      object cxGridPesquisaPadraoLevel1: TcxGridLevel
        GridView = viewProdutos
      end
    end
    object gbPanel1: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      DesignSize = (
        841
        29)
      Height = 29
      Width = 841
      object Label1: TLabel
        Left = 7
        Top = 8
        Width = 77
        Height = 13
        Caption = 'Tabela de Pre'#231'o'
      end
      object gbDBButtonEditFK1: TgbDBButtonEditFK
        Left = 88
        Top = 4
        DataBinding.DataField = 'ID_TABELA_PRECO'
        DataBinding.DataSource = dsTabelaPreco
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Properties.ReadOnly = False
        Style.Color = 14606074
        TabOrder = 0
        gbTextEdit = gbDBTextEdit1
        gbRequired = True
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_TABELA_PRECO;JOIN_DESCRICAO_TABELA_PRECO'
        gbTableName = 'TABELA_PRECO'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbIdentificadorConsulta = 'TABELA_PRECO'
        Width = 60
      end
      object gbDBTextEdit1: TgbDBTextEdit
        Left = 145
        Top = 4
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'JOIN_DESCRICAO_TABELA_PRECO'
        DataBinding.DataSource = dsTabelaPreco
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbReadyOnly = True
        gbPassword = False
        Width = 693
      end
    end
  end
  inherited ActionListMain: TActionList
    object ActBuscarProduto: TAction
      Category = 'Action'
      Caption = 'Buscar Produtos F1'
      Hint = 'Buscar produtos para emiss'#227'o de etiquetas'
      ImageIndex = 9
      ShortCut = 112
      OnExecute = ActBuscarProdutoExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 254
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 117
      FloatClientHeight = 113
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbBuscarProduto'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbImprimirChildPadrao'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 166
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object dxBarButton1: TdxBarButton [6]
      Action = ActBuscarProdutoSelecionado
      Category = 0
    end
    object dxBarButton2: TdxBarButton [7]
      Action = ActBuscarTodosProdutos
      Category = 0
    end
    object dxBarButton3: TdxBarButton [8]
      Action = ActExcluirProdutoSelecionado
      Category = 0
    end
    object dxBarButton4: TdxBarButton [9]
      Action = ActExcluirTodosRegistros
      Category = 0
    end
    object dxBarButton5: TdxBarButton [10]
      Action = ActAlterarSQL
      Category = 0
    end
    object lbBuscarProduto: TdxBarLargeButton [14]
      Action = ActBuscarProduto
      Category = 1
    end
    inherited siPersonalizar: TdxBarSubItem
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'bbParametrosFormulario'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbHelpImplantacao'
        end>
    end
  end
  inherited alAcoes16x16: TActionList
    object ActExcluirTodosRegistros: TAction
      Category = 'EmissaoEtiqueta'
      Caption = 'Remover Todos Produtos'
      Hint = 'Remover todos os produtos deste invent'#225'rio'
      ImageIndex = 21
      OnExecute = ActExcluirTodosRegistrosExecute
    end
    object ActBuscarProdutoSelecionado: TAction
      Category = 'EmissaoEtiqueta'
      Caption = 'Explorar Registro Selecionado'
      ImageIndex = 11
      OnExecute = ActBuscarProdutoSelecionadoExecute
    end
    object ActBuscarTodosProdutos: TAction
      Category = 'EmissaoEtiqueta'
      Caption = 'Explorar Todos os Registros'
      ImageIndex = 11
      OnExecute = ActBuscarTodosProdutosExecute
    end
    object ActExcluirProdutoSelecionado: TAction
      Category = 'EmissaoEtiqueta'
      Caption = 'Excluir o Registro Selecionado'
      ImageIndex = 52
      OnExecute = ActExcluirProdutoSelecionadoExecute
    end
    object ActAlterarSQL: TAction
      Caption = 'Alterar SQL'
      ImageIndex = 13
      OnExecute = ActAlterarSQLExecute
    end
  end
  object dsProdutos: TDataSource
    DataSet = fdmProdutos
    Left = 416
    Top = 224
  end
  object fdmProdutos: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 387
    Top = 224
  end
  object dxRibbonRadialMenu: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
        ItemName = 'dxBarButton1'
      end
      item
        Visible = True
        ItemName = 'dxBarButton2'
      end
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end
      item
        Visible = True
        ItemName = 'dxBarButton4'
      end>
    Images = DmAcesso.cxImage16x16
    Ribbon = dxRibbon1
    UseOwnFont = False
    Left = 560
    Top = 200
  end
  object fdmTabelaPreco: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 380
    Top = 288
    object fdmTabelaPrecoID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
    end
    object fdmTabelaPrecoJOIN_DESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Size = 255
    end
  end
  object dsTabelaPreco: TDataSource
    DataSet = fdmTabelaPreco
    Left = 408
    Top = 288
  end
  object gbFDMemTable1: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 523
    Top = 256
    object gbFDMemTable1ID_PRODUTO: TIntegerField
      FieldName = 'ID_PRODUTO'
    end
    object gbFDMemTable1QUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
  end
end
