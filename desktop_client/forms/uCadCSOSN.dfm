inherited CadCSOSN: TCadCSOSN
  Caption = 'Cadastro de ICMS/CSOSN'
  ClientWidth = 919
  ExplicitWidth = 935
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 919
    ExplicitWidth = 919
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 919
    Properties.ActivePage = cxtsData
    ExplicitWidth = 919
    ClientRectRight = 919
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 919
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 887
        ExplicitWidth = 887
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 919
      inherited DesignPanel: TJvDesignPanel
        Width = 919
        ExplicitWidth = 919
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 919
          Width = 919
          inherited dxBevel1: TdxBevel
            Width = 915
            ExplicitWidth = 915
          end
          object lbCST: TLabel
            Left = 10
            Top = 38
            Width = 19
            Height = 13
            Caption = 'CST'
          end
          object Label2: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label3: TLabel
            Left = 10
            Top = 85
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object lbCSOSN: TLabel
            Left = 10
            Top = 38
            Width = 34
            Height = 13
            Caption = 'CSOSN'
          end
          object Label5: TLabel
            Left = 10
            Top = 62
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object EdtCST: TgbDBTextEdit
            Left = 71
            Top = 34
            DataBinding.DataField = 'CODIGO_CST'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 90
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 71
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object EdtObservacao: TgbDBBlobEdit
            Left = 71
            Top = 82
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbRequired = True
            Width = 836
          end
          object EdtCSOSN: TgbDBTextEdit
            Left = 71
            Top = 34
            DataBinding.DataField = 'CODIGO_CSOSN'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 90
          end
          object EdtDescricao: TgbDBTextEdit
            Left = 71
            Top = 58
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            gbPassword = False
            Width = 836
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspCSOSN'
    RemoteServer = DmConnection.dspCSOSN
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataCODIGO_CST: TStringField
      FieldName = 'CODIGO_CST'
      Size = 5
    end
    object cdsDataCODIGO_CSOSN: TStringField
      FieldName = 'CODIGO_CSOSN'
      Size = 5
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.875182604170000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
