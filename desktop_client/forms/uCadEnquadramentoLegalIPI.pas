unit uCadEnquadramentoLegalIPI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, cxMemo, cxDBEdit, uGBDBMemo, uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls;

type
  TCadEnquadramentoLegalIPI = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    gbPanel1: TgbPanel;
    Label3: TLabel;
    edDescricao: TgbDBTextEdit;
    Label4: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    gbPanel2: TgbPanel;
    PnTituloFrameDetailPadrao: TgbPanel;
    gbDBMemo1: TgbDBMemo;
    cdsDataID: TAutoIncField;
    cdsDataCODIGO: TStringField;
    cdsDataGRUPO_CST: TStringField;
    cdsDataDESCRICAO: TBlobField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

Initialization
  RegisterClass(TCadEnquadramentoLegalIPI);

Finalization
  UnRegisterClass(TCadEnquadramentoLegalIPI)

end.
