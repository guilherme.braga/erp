unit uImpMalaDireta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, uFrameConsultaDadosDetalheGrade, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter;

type
  TImpMalaDireta = class(TFrmChildPadrao)
  private
    FFrameConsultaDadosDetalheGradeMalaDireta: TFrameConsultaDadosDetalheGrade;
    function GetContaReceberSelecionados: String;
  public
    procedure InstanciarFrames; override;
    procedure Imprimir; override;
  end;

implementation

{$R *.dfm}

uses uFrameFiltroVerticalPadrao, uFrmVisualizacaoRelatoriosAssociados, uSistema, uFiltroFormulario,
  uDateUtils, uConstantes, uDatasetUtils, db, uFrmMessage, uPessoaProxy;

{ TImpEmissaoMalaDireta }

function TImpMalaDireta.GetContaReceberSelecionados: String;
var
  fieldChecked: TField;
  fieldID: TField;
begin
  fieldChecked :=
    FFrameConsultaDadosDetalheGradeMalaDireta.fdmDados.FieldByName(TDBConstantes.ALIAS_BO_CHECKED);

  fieldID := FFrameConsultaDadosDetalheGradeMalaDireta.fdmDados.FieldByName('ID');

  result := TDatasetUtils.ConcatenarComCondicao(fieldID, fieldChecked, TConstantes.BO_SIM);
end;

procedure TImpMalaDireta.Imprimir;
var
  filtro: TFiltroPadrao;
  contasReceberSelecionados: String;
begin
  inherited;
  contasReceberSelecionados := GetContaReceberSelecionados;

  if contasReceberSelecionados.IsEmpty then
  begin
    TFrmMessage.Information('N�o foi poss�vel localizar os registros selecionados, '+
      'possivelmente sua consulta est� desconfigurada.');
    exit;
  end;

  filtro := TFiltroPadrao.Create;
  try
    //FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.SetWhere(filtro);
    filtro.AddFaixa(TPessoaProxy.FIELD_ID, contasReceberSelecionados);
    TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
      0, Self.Name, TSistema.Sistema.usuario.idSeguranca, filtro.where);
  finally
    FreeAndNil(filtro);
  end;
end;

procedure TImpMalaDireta.InstanciarFrames;
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
  FFrameConsultaDadosDetalheGradeMalaDireta := TFrameConsultaDadosDetalheGrade.Create(
    cxPanelMain, Self.Name, 'Mala Direta');

  FFrameConsultaDadosDetalheGradeMalaDireta.OcultarTituloFrame;
  FFrameConsultaDadosDetalheGradeMalaDireta.OcultarCampoImpressao;
  FFrameConsultaDadosDetalheGradeMalaDireta.SetInjecaoCamposNaConsulta(TDBConstantes.FIELD_BO_UNCHECKED);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Cliente';
  campoFiltro.campo := 'BO_CLIENTE';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := 'Sim;N�o';
  campoFiltro.campoCaixaSelecao.valores := 'S;N';
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Fornecedor';
  campoFiltro.campo := 'BO_FORNECEDOR';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := 'Sim;N�o';
  campoFiltro.campoCaixaSelecao.valores := 'S;N';
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Ativo';
  campoFiltro.campo := 'BO_ATIVO';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := 'Sim;N�o';
  campoFiltro.campoCaixaSelecao.valores := 'S;N';
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Dia de Anivers�rio';
  campoFiltro.campo := 'DT_NASCIMENTO_DIA';//FICITICIO
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM PESSOA.DT_NASCIMENTO)';
  campoFiltro.condicao := TCondicaoFiltro(entre);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TDateUtils.DIAS_MES_SEPARADOS_PONTO_VIRGULA;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'M�s de Anivers�rio';
  campoFiltro.campo := 'DT_NASCIMENTO_MES';//FICTICIO
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicaoAberta := 'EXTRACT(MONTH FROM PESSOA.DT_NASCIMENTO)';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TDateUtils.DESCRICAO_MES_DO_ANO_SEPARADOS_PONTO_VIRGULA;
  campoFiltro.campoCaixaSelecao.valores := TDateUtils.NUMERO_MES_DO_ANO_SEPARADOS_PONTO_VIRGULA;
  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.FCampos.Add(campoFiltro);

  FFrameConsultaDadosDetalheGradeMalaDireta.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeMalaDireta.Parent := cxPanelMain;
end;

initialization
  RegisterClass(TImpMalaDireta);

finalization
  UnRegisterClass(TImpMalaDireta);

end.
