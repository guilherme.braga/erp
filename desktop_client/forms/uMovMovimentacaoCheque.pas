unit uMovMovimentacaoCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, uFrameConsultaDadosDetalheGrade, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit,
  uGBDBDateEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxButtonEdit, uGBDBButtonEditFK, cxRadioGroup,
  uGBDBRadioGroup;

type TTipoMovimentacaoCheque = (debito, transferencia);

type TFrameConsultaDadosDetalheGradeMovimentacaoCheque = class (TFrameConsultaDadosDetalheGrade)
  public
    procedure DepoisConsultar; override;
end;

type
  TMovMovimentacaoCheque = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataTIPO_MOVIMENTO: TStringField;
    cdsDataID_PESSOA_USUARIO_CADASTRO: TIntegerField;
    cdsDataID_PESSOA_USUARIO_EFETIVACAO: TIntegerField;
    cdsDataID_PESSOA_USUARIO_REABERTURA: TIntegerField;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataID_CONTA_CORRENTE_DESTINO: TIntegerField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDH_FECHAMENTO: TDateTimeField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataSTATUS: TStringField;
    cdsDataJOIN_NOME_USUARIO: TStringField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_DESTINO: TStringField;
    cdsDatafdqMovimentacaoChequeCheque: TDataSetField;
    cdsMovimentacaoChequeCheque: TGBClientDataSet;
    cdsMovimentacaoChequeChequeID: TAutoIncField;
    cdsMovimentacaoChequeChequeID_MOVIMENTACAO_CHEQUE: TIntegerField;
    cdsMovimentacaoChequeChequeID_CHEQUE: TIntegerField;
    cdsMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ANTERIOR: TIntegerField;
    cdsMovimentacaoChequeChequeID_CONTA_CORRENTE_MOVIMENTO_ATUAL: TIntegerField;
    cdsDataVL_TOTAL_CHEQUE: TFMTBCDField;
    ActEstornar: TAction;
    ActEfetivar: TAction;
    lbEstornar: TdxBarLargeButton;
    lbEfetivar: TdxBarLargeButton;
    Label2: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    ePkCodig: TgbDBTextEditPK;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBDateEdit2: TgbDBDateEdit;
    descProcesso: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    PnDadosMovimentacao: TgbPanel;
    PnFrameConsultaDadosDetalheGradeCheque: TgbPanel;
    gbDBRadioGroup1: TgbDBRadioGroup;
    lbContaCorrente: TLabel;
    edtContaCorrente: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    Label6: TLabel;
    Label7: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    PnContaCorrenteDestino: TgbPanel;
    Label9: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    Label8: TLabel;
    gbDBTextEdit5: TgbDBTextEdit;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ActEstornarExecute(Sender: TObject);
    procedure ActEfetivarExecute(Sender: TObject);
    procedure cdsDataTIPO_MOVIMENTOChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsMovimentacaoChequeChequeAfterOpen(DataSet: TDataSet);
    procedure cdsDataJOIN_DESCRICAO_CONTA_CORRENTEChange(Sender: TField);
    procedure AoSelecionarChequeNaConsulta(Sender: TField);
  private
    FFrameConsultaDadosDetalheGradeCheque: TFrameConsultaDadosDetalheGradeMovimentacaoCheque;

    procedure MostrarChequesDessaMovimentacao;
    procedure SalvarChequesDessaMovimentacao;
    procedure ConfigurarEventoDeTotalizarChequesSelecionados;
    procedure TotalizarChequesSelecionados;
    procedure HabilitarBotaoEstornar;
    procedure HabilitarBotaoEfetivar;
    procedure DefinirFiltrosChequesMovimentacao(
      AFrameConsultaDadosDetalheGrade: TFrameConsultaDadosDetalheGrade);
    procedure ExibirContaCorrenteDestino;
    function GetChequesSelecionados: String;
    function GetChequesDaMovimentacaoCheque: String;
    procedure DeletarChequesInvalidos;
    procedure InserirChequeNaMovimentacaoCheque(const AIdCheque: Integer);
    procedure MudarValorFiltroDeContaCorrente(const AIdContaCorrente: Integer;
      const ADescricaoContaCorrente: String);
    procedure PreencherContaCorrente(const AIdContaCorrente: Integer);
    procedure PreencherTipoMovimentacao(const ATipoMovimentacao: TTipoMovimentacaoCheque);
    procedure ExecutarConsultaFiltro;
    procedure HabilitarBotaoPesquisar;
    procedure HabilitarEdicaoCampoSelecaoConsultaCheque;
    procedure HabilitarBotaoExcluir;
    procedure LimparDadosDaConsultaCheque;
  public
    class procedure IncluirMovimentacao(const AIdContaCorrente: Integer;
      const ATipoMovimentacao: TTipoMovimentacaoCheque);
  protected
    procedure GerenciarControles; override;
    procedure InstanciarFrames; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeInserir; override;
    procedure BloquearEdicao; override;
    procedure AntesDeRemover; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uMovimentacaoChequeProxy, uSistema, uChaveProcesso, uChaveProcessoProxy,
  uFrameFiltroVerticalPadrao, uConstantes, uDateUtils, uChequeProxy, uContaCorrenteProxy, uDatasetUtils,
  uMovimentacaoCheque, uCheque, uFrmMessage, uFiltroFormulario, uFrmMessage_Process, uControlsUtils,
  uTControl_Function, uTMessage;

{ TMovimentacaoCheque }

procedure TMovMovimentacaoCheque.ActEfetivarExecute(Sender: TObject);
var
  idChaveProcesso: Integer;
  idMovimentacaoCheque: Integer;
begin
  if TFrmMessage.Question('Deseja efetivar a movimenta��o de cheque?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    DeletarChequesInvalidos;
    ActConfirm.Execute;

    TFrmMessage_Process.SendMessage('Efetivando a movimenta��o de cheque');
    try
      TMovimentacaoCheque.Efetivar(idChaveProcesso);

      idMovimentacaoCheque := TMovimentacaoCheque.BuscarIdPelaChaveProcesso(idChaveProcesso);
      if idMovimentacaoCheque > 0 then
      begin
        AtualizarRegistro(idMovimentacaoCheque);
      end;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovMovimentacaoCheque.ActEstornarExecute(Sender: TObject);
begin
  if TFrmMessage.Question('Deseja realmente estornar essa movimenta��o?') = MCONFIRMED then
  begin
    TMovimentacaoCheque.Estornar(cdsDataID_CHAVE_PROCESSO.AsInteger);
  end;
end;

procedure TMovMovimentacaoCheque.AntesDeConfirmar;
begin
  inherited;
  if cdsDataTIPO_MOVIMENTO.AsString.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA) then
  begin
    TValidacaoCampo.CampoPreenchido(cdsDataID_CONTA_CORRENTE_DESTINO);
  end;

  SalvarChequesDessaMovimentacao;
end;

procedure TMovMovimentacaoCheque.AntesDeRemover;
begin
  inherited;
  if not TMovimentacaoCheque.PodeExcluir(cdsDataID.AsInteger) then
  begin
    Abort;
  end;
end;

procedure TMovMovimentacaoCheque.AoSelecionarChequeNaConsulta(Sender: TField);
begin
  TotalizarChequesSelecionados;
end;

procedure TMovMovimentacaoCheque.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados :=
    not cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO);
end;

procedure TMovMovimentacaoCheque.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  ExibirContaCorrenteDestino;

  if cdsDataID_CONTA_CORRENTE.AsInteger > 0 then
  begin
    MudarValorFiltroDeContaCorrente(cdsDataID_CONTA_CORRENTE.AsInteger,
      cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString);
  end;
end;

procedure TMovMovimentacaoCheque.cdsDataJOIN_DESCRICAO_CONTA_CORRENTEChange(Sender: TField);
begin
  inherited;
  MudarValorFiltroDeContaCorrente(cdsDataID_CONTA_CORRENTE.AsInteger,
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString);

  HabilitarBotaoPesquisar;

  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.LimparTodosFiltros;
  FFrameConsultaDadosDetalheGradeCheque.Consultar;
  FFrameConsultaDadosDetalheGradeCheque.DepoisConsultar;
end;

procedure TMovMovimentacaoCheque.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataTIPO_MOVIMENTO.AsString := TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA;

  cdsDataID_PESSOA_USUARIO_CADASTRO.AsInteger := TSistema.Sistema.Usuario.Id;
  cdsDataJOIN_NOME_USUARIO.AsString := TSistema.Sistema.Usuario.Nome;

  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.FId;
  cdsDataJOIN_FANTASIA_FILIAL.AsString := TSistema.Sistema.Filial.FFantasia;

  cdsDataID_CHAVE_PROCESSO.AsInteger :=
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_MOVIMENTACAO_CHEQUE, cdsDataID.AsInteger);
  cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_MOVIMENTACAO_CHEQUE;

  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataSTATUS.AsString := TMovimentacaoChequeProxy.STATUS_ABERTO;

  cdsDataVL_TOTAL_CHEQUE.AsFloat := 0;
end;

procedure TMovMovimentacaoCheque.cdsDataTIPO_MOVIMENTOChange(Sender: TField);
begin
  inherited;
  ExibirContaCorrenteDestino;
end;

procedure TMovMovimentacaoCheque.cdsMovimentacaoChequeChequeAfterOpen(DataSet: TDataSet);
begin
  inherited;
  MostrarChequesDessaMovimentacao;
end;

procedure TMovMovimentacaoCheque.ConfigurarEventoDeTotalizarChequesSelecionados;
var
  fieldBoChecked: TField;
begin
  fieldBoChecked := FFrameConsultaDadosDetalheGradeCheque.fdmDados.FindField(TDBConstantes.ALIAS_BO_CHECKED);
  if Assigned(fieldBoChecked) then
  begin
    if not Assigned(fieldBoChecked.OnChange) then
    begin
      fieldBoChecked.OnChange := AoSelecionarChequeNaConsulta;
    end;
  end;
end;

procedure TMovMovimentacaoCheque.GerenciarControles;
begin
  inherited;
  HabilitarBotaoEfetivar;
  HabilitarBotaoEstornar;
  HabilitarBotaoPesquisar;
  HabilitarBotaoExcluir;
  HabilitarEdicaoCampoSelecaoConsultaCheque;
end;

function TMovMovimentacaoCheque.GetChequesDaMovimentacaoCheque: String;
begin
  result := TDatasetUtils.Concatenar(cdsMovimentacaoChequeChequeID_CHEQUE);
end;

function TMovMovimentacaoCheque.GetChequesSelecionados: String;
var
  fieldChecked: TField;
  fieldID: TField;
begin
  fieldChecked :=
    FFrameConsultaDadosDetalheGradeCheque.fdmDados.FindField(TDBConstantes.ALIAS_BO_CHECKED);

  if not Assigned(fieldChecked) then
  begin
    result := '';
    exit;
  end;

  fieldID := FFrameConsultaDadosDetalheGradeCheque.fdmDados.FieldByName('ID');

  result := TDatasetUtils.ConcatenarComCondicao(fieldID, fieldChecked, TConstantes.BO_SIM);
end;

procedure TMovMovimentacaoCheque.HabilitarBotaoEfetivar;
begin
  ActEfetivar.Visible := cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO) and
    (cdsDataVL_TOTAL_CHEQUE.AsFloat > 0);
end;

procedure TMovMovimentacaoCheque.HabilitarBotaoEstornar;
begin
  ActEstornar.Visible := cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_FECHADO) and
    (cdsDataVL_TOTAL_CHEQUE.AsFloat > 0) and
    (cdsDataTIPO_MOVIMENTO.AsString.Equals(TMovimentacaoChequeProxy.TIPO_MOVIMENTO_DEBITO));
end;

procedure TMovMovimentacaoCheque.HabilitarBotaoExcluir;
begin
  ActRemove.Visible := cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO);
end;

procedure TMovMovimentacaoCheque.HabilitarBotaoPesquisar;
begin
  if Assigned(FFrameConsultaDadosDetalheGradeCheque) then
  begin
    if cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO) and
      (cdsData.Active) and (cdsDataID_CONTA_CORRENTE.AsInteger > 0) then
    begin
      FFrameConsultaDadosDetalheGradeCheque.HabilitarBotaoPesquisar;
    end
    else
    begin
      FFrameConsultaDadosDetalheGradeCheque.DesabilitarBotaoPesquisar;
    end;
  end;
end;

procedure TMovMovimentacaoCheque.InserirChequeNaMovimentacaoCheque(const AIdCheque: Integer);
begin
  cdsMovimentacaoChequeCheque.Incluir;
  cdsMovimentacaoChequeChequeID_CHEQUE.AsInteger := AIdCheque;
  cdsMovimentacaoChequeCheque.Salvar;
end;

procedure TMovMovimentacaoCheque.InstanciarFrames;
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
  FFrameConsultaDadosDetalheGradeCheque := TFrameConsultaDadosDetalheGradeMovimentacaoCheque.Create(
    PnFrameConsultaDadosDetalheGradeCheque, Self.Name+'ConsultaCheques', 'Cheque');

  FFrameConsultaDadosDetalheGradeCheque.OcultarTituloFrame;
  FFrameConsultaDadosDetalheGradeCheque.OcultarCampoImpressao;
  FFrameConsultaDadosDetalheGradeCheque.SetInjecaoCamposNaConsulta(TDBConstantes.FIELD_BO_UNCHECKED);

  DefinirFiltrosChequesMovimentacao(FFrameConsultaDadosDetalheGradeCheque);

  FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeCheque.Parent := PnFrameConsultaDadosDetalheGradeCheque;
end;

procedure TMovMovimentacaoCheque.LimparDadosDaConsultaCheque;
begin
  if Assigned(FFrameConsultaDadosDetalheGradeCheque) then
  begin
    FFrameConsultaDadosDetalheGradeCheque.LimparDatasetConsulta;
  end;
end;

procedure TMovMovimentacaoCheque.DefinirFiltrosChequesMovimentacao(
  AFrameConsultaDadosDetalheGrade: TFrameConsultaDadosDetalheGrade);
var
  campoFiltro: TcampoFiltro;
begin
  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.SomenteLeitura := true;
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Tipo de Cheque TERCEIRO (FIXO)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo de Cheque';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  campoFiltro.valorPadrao := TChequeProxy.TIPO_CHEQUE_TERCEIRO;
  campoFiltro.somenteLeitura := true;
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Cheque Utilizado N (FIXO)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Cheque Utilizado';
  campoFiltro.campo := 'BO_CHEQUE_UTILIZADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);
  campoFiltro.valorPadrao := 'N';
  campoFiltro.somenteLeitura := true;
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Sacado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Sacado';
  campoFiltro.campo := 'SACADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento Federal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Federal (CPF/CNPJ)';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_CHEQUE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Banco
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Banco';
  campoFiltro.campo := 'BANCO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Emiss�o
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Emiss�o';
  campoFiltro.campo := 'DT_EMISSAO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //N�mero do Cheque
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'N�mero do Cheque';
  campoFiltro.campo := 'NUMERO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

   //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conciliado';
  campoFiltro.campo := 'BO_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TConstantesCampoFiltro.ITENS_BO;
  campoFiltro.campoCaixaSelecao.valores := TConstantesCampoFiltro.VALORES_BO;
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DH_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //C�digo do Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo do Documento de Origem';
  campoFiltro.campo := 'ID_DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);

  //Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento de Origem';
  campoFiltro.campo := 'DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TContaCorrenteMovimento.LISTA_ORIGEM;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFrameConsultaDadosDetalheGrade.FFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TMovMovimentacaoCheque.ExecutarConsultaFiltro;
begin
  if not Assigned(FFrameConsultaDadosDetalheGradeCheque) then
  begin
    Exit;
  end;

  FFrameConsultaDadosDetalheGradeCheque.Consultar;
end;

procedure TMovMovimentacaoCheque.ExibirContaCorrenteDestino;
begin
  PnContaCorrenteDestino.Visible := cdsDataTIPO_MOVIMENTO.AsString.Equals(
    TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA);
end;

procedure TMovMovimentacaoCheque.MostrarChequesDessaMovimentacao;
var
  filtroPadrao: TFiltroPadrao;
  idsChequesDaMovimentacaoCheque: String;
begin
  idsChequesDaMovimentacaoCheque := GetChequesDaMovimentacaoCheque;
  if not idsChequesDaMovimentacaoCheque.IsEmpty then
  begin
    filtroPadrao := TFiltroPadrao.Create;
    try
      filtroPadrao.AddFaixa(TChequeProxy.FIELD_ID, GetChequesDaMovimentacaoCheque);
      FFrameConsultaDadosDetalheGradeCheque.ConsultarInjetandoWhere(filtroPadrao);
      FFrameConsultaDadosDetalheGradeCheque.DepoisConsultar;
      FFrameConsultaDadosDetalheGradeCheque.ExecutarClickColunaSelecao;
    finally
      FreeAndNil(filtroPadrao);
    end;
  end;
end;

procedure TMovMovimentacaoCheque.MudarValorFiltroDeContaCorrente(const AIdContaCorrente: Integer;
  const ADescricaoContaCorrente: String);
var
  fieldIdContaCorrente: TField;
  fieldDescricaoContaCorrente: TField;
begin
  fieldIdContaCorrente :=
    FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.fdmFiltro.FieldByName('ID_CONTA_CORRENTE');

  fieldDescricaoContaCorrente :=
    FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.fdmFiltro.FieldByName(
    FFrameConsultaDadosDetalheGradeCheque.FFiltroVertical.GetNomeFieldDetalhe('ID_CONTA_CORRENTE'));

  fieldIdContaCorrente.AsInteger := AIdContaCorrente;

  fieldDescricaoContaCorrente.AsString := ADescricaoContaCorrente;
end;

procedure TMovMovimentacaoCheque.PreencherContaCorrente(const AIdContaCorrente: Integer);
begin
  edtContaCorrente.ExecutarOnEnter;
  edtContaCorrente.EditValue := AIdContaCorrente;
  edtContaCorrente.ExecutarOnExit;
end;

procedure TMovMovimentacaoCheque.PreencherTipoMovimentacao(const ATipoMovimentacao: TTipoMovimentacaoCheque);
begin
  case ATipoMovimentacao of
    debito:
    begin
      cdsDataTIPO_MOVIMENTO.AsString := TMovimentacaoChequeProxy.TIPO_MOVIMENTO_DEBITO;
    end;

    transferencia:
    begin
      cdsDataTIPO_MOVIMENTO.AsString := TMovimentacaoChequeProxy.TIPO_MOVIMENTO_TRANSFERENCIA;
    end;
  end;
end;

procedure TMovMovimentacaoCheque.SalvarChequesDessaMovimentacao;
var
  cheques: TStringList;
  i: Integer;
begin
  cheques := TStringList.Create;
  try
    cheques.Delimiter := ',';
    cheques.DelimitedText := GetChequesSelecionados;

    cdsMovimentacaoChequeCheque.DeletarTodosRegistros;

    for i := 0 to Pred(cheques.Count) do
    begin
      InserirChequeNaMovimentacaoCheque(StrToInt(cheques[i]));
    end;
  finally
    FreeAndNil(cheques);
  end;
end;

procedure TMovMovimentacaoCheque.TotalizarChequesSelecionados;
var
  fieldChecked: TField;
  fieldValorCheque: TField;
begin
  if not cdsData.EstadoDeAlteracao then
  begin
    Exit;
  end;

  fieldChecked := FFrameConsultaDadosDetalheGradeCheque.fdmDados.FieldByName(TDBConstantes.ALIAS_BO_CHECKED);

  fieldValorCheque := FFrameConsultaDadosDetalheGradeCheque.fdmDados.FindField('VL_CHEQUE');

  if not Assigned(fieldValorCheque) then
  begin
    TFrmMessage.Information('N�o foi localizado o campo "VL_CHEQUE" na consulta,'+
      ' isso comprometer� a informa��o "Total de Cheques Selecionados".');
    exit;
  end;

  cdsDataVL_TOTAL_CHEQUE.AsFloat :=
    TDatasetUtils.SomarColunaComCondicao(fieldValorCheque, fieldChecked, TConstantes.BO_SIM);

  HabilitarBotaoEfetivar;
end;

procedure TMovMovimentacaoCheque.HabilitarEdicaoCampoSelecaoConsultaCheque;
var
  podeEditar: Boolean;
begin
  podeEditar := cdsData.EstadoDeAlteracao and
    cdsDataSTATUS.AsString.Equals(TMovimentacaoChequeProxy.STATUS_ABERTO);

  FFrameConsultaDadosDetalheGradeCheque.HabilitarEdicaoColunaSelecao(podeEditar);
end;

procedure TMovMovimentacaoCheque.DeletarChequesInvalidos;
var
  listaChequesInvalidos: TStringList;
  i: Integer;
begin
  listaChequesInvalidos := TStringList.Create;
  try
    cdsMovimentacaoChequeCheque.DisableControls;

    cdsMovimentacaoChequeCheque.First;
    while not cdsMovimentacaoChequeCheque.Eof do
    begin
      if not TCheque.ChequeDisponivelParaUtilizacao(cdsMovimentacaoChequeChequeID_CHEQUE.AsInteger) then
      begin
        listaChequesInvalidos.Add(cdsMovimentacaoChequeChequeID_CHEQUE.AsString);
      end;
      cdsMovimentacaoChequeCheque.Next;
    end;

    if listaChequesInvalidos.Count > 0 then
    begin
      if TFrmMessage.Question('Os cheques '+listaChequesInvalidos.text+' n�o est�o mais dispon�veis para uso: '+
        'Deseja remove-los dessa movimenta��o e continuar o processo de efetiva��o?') = MCONFIRMED then
      begin
        for i := 0 to (listaChequesInvalidos.Count - 1) do
        begin
          if cdsMovimentacaoChequeCheque.Locate('ID_CHEQUE', listaChequesInvalidos[i], []) then
          begin
            cdsMovimentacaoChequeCheque.Delete;
          end;
        end;
      end;
    end;
  finally
    cdsMovimentacaoChequeCheque.EnableControls;
    FreeAndNil(listaChequesInvalidos);
  end;
end;

procedure TMovMovimentacaoCheque.DepoisDeInserir;
begin
  inherited;
  LimparDadosDaConsultaCheque;
end;

class procedure TMovMovimentacaoCheque.IncluirMovimentacao(const AIdContaCorrente: Integer;
  const ATipoMovimentacao: TTipoMovimentacaoCheque);
var
  instanciaFormulario: TMovMovimentacaoCheque;
begin
  try
    try
      TFrmMessage_Process.SendMessage('Preparando modo de inclus�o');
      TControlsUtils.CongelarFormulario(Application.MainForm);
      instanciaFormulario := TMovMovimentacaoCheque(TControlFunction.GetInstance('TMovMovimentacaoCheque'));
      if Assigned(instanciaFormulario) then
      begin
        if instanciaFormulario.cdsData.EstadoDeAlteracao then
        begin
          TMessage.MessageInformation('O formul�rio est� em edi��o, salve ou cancele as altera��es pendentes'+
           ' para poder executar esta a��o.');

          exit;
        end;
      end
      else
      begin
        instanciaFormulario := TMovMovimentacaoCheque(
          TControlFunction.CreateMDIForm('TMovMovimentacaoCheque'));
      end;
      instanciaFormulario.ActInsert.Execute;
      instanciaFormulario.Show;
      instanciaFormulario.PreencherContaCorrente(AIdContaCorrente);
      instanciaFormulario.PreencherTipoMovimentacao(ATipoMovimentacao);
      Application.ProcessMessages;
      instanciaFormulario.ExecutarConsultaFiltro;
    finally
      TControlsUtils.DescongelarFormulario;
      TFrmMessage_Process.CloseMessage;
    end;
  except
    on e: exception do
    begin
      TFrmMessage.Information(e.message);
    end;
  end;
end;

{ TFrameConsultaDadosDetalheGradeMovimentacaoCheque }

procedure TFrameConsultaDadosDetalheGradeMovimentacaoCheque.DepoisConsultar;
var
  formularioMovimentacaoCheque: TMovMovimentacaoCheque;
begin
  inherited;
  formularioMovimentacaoCheque :=
    TMovMovimentacaoCheque(TControlFunction.GetInstance('TMovMovimentacaoCheque'));

  if Assigned(formularioMovimentacaoCheque) then
  begin
    formularioMovimentacaoCheque.ConfigurarEventoDeTotalizarChequesSelecionados;
    formularioMovimentacaoCheque.TotalizarChequesSelecionados;
    formularioMovimentacaoCheque.HabilitarEdicaoCampoSelecaoConsultaCheque;
  end;
end;

initialization
  RegisterClass(TMovMovimentacaoCheque);

Finalization
  UnRegisterClass(TMovMovimentacaoCheque);

end.
