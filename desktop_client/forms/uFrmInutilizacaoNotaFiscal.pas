unit uFrmInutilizacaoNotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm, cxContainer,
  cxEdit, dxSkinsdxBarPainter, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, dxBar, System.Actions, Vcl.ActnList, cxGroupBox,
  dxRibbon, cxLabel, cxCalendar, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus;

type
  TFrmInutilizacaoNotaFiscal = class(TFrmChildPadrao)
    fdmNotasFiscaisInutilizadas: TgbFDMemTable;
    dsNotasFiscaisInutilizadas: TDataSource;
    gridCentroResultado: TcxGrid;
    ViewConsultaDados: TcxGridDBBandedTableView;
    cxGridLevel12: TcxGridLevel;
    ActRealizarInutilizacao: TAction;
    lbInutilizarNotaFiscal: TdxBarLargeButton;
    bbGrupoFiltro: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    EdtMes: TdxBarCombo;
    EdtAno: TdxBarCombo;
    PopUpControllerPesquisa: TcxGridPopupMenu;
    ActionListAssistent: TActionList;
    ActSalvarConfiguracoes: TAction;
    ActAlterarColunasGrid: TAction;
    ActPesquisar: TAction;
    lbConsultar: TdxBarLargeButton;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    SalvarConfiguraes1: TMenuItem;
    procedure ActInutilizarNotaFiscalExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActPesquisarExecute(Sender: TObject);
  private
    procedure BuscarNotasFiscaisParaInutilizacao;
    function ExistirNotasFiscaisSelecionadas: Boolean;
    function BuscarMes: Integer;
    function BuscarAno: Integer;
    procedure ConstruirComboBoxAno;
    procedure ConfigurarConsultaNotaFiscal;
  public
    procedure RealizarInutilizacao;
  end;

implementation

{$R *.dfm}

uses uFrmMessage, Data.FireDACJSONReflect, uDatasetUtils, uSistema, uNotaFiscal, uVerticalGridUtils,
  uDevExpressUtils, uConstantes, uFrmApelidarColunasGrid, uUsuarioDesignControl, uTUsuario, uControlsUtils,
  DateUtils, uFrmMessage_Process;

procedure TFrmInutilizacaoNotaFiscal.ActAlterarColunasGridExecute(Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewConsultaDados);
end;

procedure TFrmInutilizacaoNotaFiscal.ActInutilizarNotaFiscalExecute(Sender: TObject);
begin
  inherited;
  RealizarInutilizacao;
end;

procedure TFrmInutilizacaoNotaFiscal.ActPesquisarExecute(Sender: TObject);
begin
  inherited;
  BuscarNotasFiscaisParaInutilizacao;
end;

procedure TFrmInutilizacaoNotaFiscal.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(ViewConsultaDados,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

function TFrmInutilizacaoNotaFiscal.BuscarAno: Integer;
begin
  result := StrToIntDef(VartoStr(EdtAno.Text), 0);
end;

function TFrmInutilizacaoNotaFiscal.BuscarMes: Integer;
begin
  result := StrToIntDef(VartoStr(EdtMes.Text), 0);
end;

procedure TFrmInutilizacaoNotaFiscal.BuscarNotasFiscaisParaInutilizacao;
begin
  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Consultando Notas Fiscais');
    TNotaFiscal.BuscarNotasFiscaisParaInutilizacao(
      TSistema.Sistema.filial.FId, BuscarMes, BuscarAno, fdmNotasFiscaisInutilizadas);
  finally
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;

  ConfigurarConsultaNotaFiscal;
end;

procedure TFrmInutilizacaoNotaFiscal.ConfigurarConsultaNotaFiscal;
var
  field: TField;
begin
  try
    TControlsUtils.CongelarFormulario(Self);

    TcxGridUtils.AdicionarTodosCamposNaView(ViewConsultaDados);

    TcxGridUtils.ConfigurarColunasParaSomenteLeitura(ViewConsultaDados);

    ViewConsultaDados.OptionsData.Editing := true;
    ViewConsultaDados.OptionsSelection.CellSelect := true;

    TcxGridUtils.ConfigurarColunaSelecao(
      ViewConsultaDados.GetColumnByFieldName(TDBConstantes.ALIAS_BO_CHECKED));

    TcxGridUtils.OcultarTodosCamposView(ViewConsultaDados);

    TUsuarioGridView.LoadGridView(ViewConsultaDados,
      TSistema.Sistema.Usuario.idSeguranca, Self.Name);
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TFrmInutilizacaoNotaFiscal.ConstruirComboBoxAno;
begin
  EdtAno.Items.Clear;
  EdtAno.Items.Add('2015');
  EdtAno.Items.Add('2016');
  EdtAno.Items.Add('2017');
  EdtAno.Items.Add('2018');
  EdtAno.Items.Add('2019');
  EdtAno.Items.Add('2020');
end;

function TFrmInutilizacaoNotaFiscal.ExistirNotasFiscaisSelecionadas: Boolean;
begin
  try
    fdmNotasFiscaisInutilizadas.DisableControls;
    fdmNotasFiscaisInutilizadas.Filter := TDBConstantes.ALIAS_BO_CHECKED +' = ''S''';
    fdmNotasFiscaisInutilizadas.Filtered := true;

    result := not fdmNotasFiscaisInutilizadas.IsEmpty;
  finally
    fdmNotasFiscaisInutilizadas.Filter := '';
    fdmNotasFiscaisInutilizadas.Filtered := false;
    fdmNotasFiscaisInutilizadas.EnableControls;
  end;
end;

procedure TFrmInutilizacaoNotaFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  ConstruirComboBoxAno;
  EdtAno.Text := InttoStr(YearOf(Now));
  EdtMes.Text := InttoStr(MonthOf(Now));
end;

procedure TFrmInutilizacaoNotaFiscal.RealizarInutilizacao;
var
  fieldChecked: TField;
  fieldID: TField;
begin
  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Realizando Inutilização');

    if not ExistirNotasFiscaisSelecionadas then
    begin
      TFrmMessage.Information('Selecione as notas fiscais que deseja inutilizar.');
      Abort;
    end;

    fieldChecked := fdmNotasFiscaisInutilizadas.FieldByName(TDBConstantes.ALIAS_BO_CHECKED);

    fieldID := fdmNotasFiscaisInutilizadas.FieldByName('ID');

    TNotaFiscal.InutilizarDocumentoFiscal(
      TDatasetUtils.ConcatenarComCondicao(fieldID, fieldChecked, TConstantes.BO_SIM));

    TFrmMessage.Information('Inutilização realizada com sucesso!');
    BuscarNotasFiscaisParaInutilizacao;
  finally
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

Initialization
  RegisterClass(TFrmInutilizacaoNotaFiscal);

Finalization
  UnRegisterClass(TFrmInutilizacaoNotaFiscal);

end.
