unit uCadMedico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxButtonEdit,
  cxDBEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls,
  cxDropDownEdit, cxBlobEdit, uGBDBBlobEdit, uGBDBTextEditPK, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon;

type
  TCadMedico = class(TFrmCadastro_Padrao)
    PnLogradouro: TcxGroupBox;
    lbComplemento: TLabel;
    lbLogradouro: TLabel;
    lnNumero: TLabel;
    lbCEP: TLabel;
    Label1: TLabel;
    lbCidadeEmpresa: TLabel;
    edtLogradouro: TgbDBTextEdit;
    edtNumero: TgbDBTextEdit;
    edtIDCEP: TgbDBButtonEditFK;
    edtCidade: TgbDBButtonEditFK;
    edtComplemento: TgbDBTextEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    Label2: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    Label3: TLabel;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataID: TAutoIncField;
    cdsDataNOME: TStringField;
    cdsDataNUMERO: TIntegerField;
    cdsDataCOMPLEMENTO: TStringField;
    cdsDataBAIRRO: TStringField;
    cdsDataCEP: TStringField;
    cdsDataLOGRADOURO: TStringField;
    cdsDataID_CIDADE: TIntegerField;
    cdsDataJOIN_DESCRICAO_CIDADE: TStringField;
    cdsDataTELEFONE: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    Label4: TLabel;
    gbDBTextEdit5: TgbDBTextEdit;
    Label5: TLabel;
    EdtObservacao: TgbDBBlobEdit;
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    cdsDataCRM: TStringField;
    procedure edtIDCEPgbDepoisDeConsultar(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CadMedico: TCadMedico;

implementation

{$R *.dfm}

uses uDmConnection, uCEPProxy, uCEP;

procedure TCadMedico.edtIDCEPgbDepoisDeConsultar(Sender: TObject);
var
  CEP: TCEPProxy;
begin
  inherited;
  if (not cdsDataCEP.AsString.IsEmpty) then
  begin
    CEP := TCEP.GetLogradouroPeloCEP(cdsDataCEP.AsString);
    try
      cdsDataLOGRADOURO.AsString :=  CEP.FRua;
      cdsDataBAIRRO.AsString := CEP.FBairro;
      if CEP.FIdCidade > 0 then
      begin
        cdsDataID_CIDADE.AsInteger := CEP.FIdCidade;
      end
      else
      begin
        cdsDataID_CIDADE.Clear;
      end;
      cdsDataJOIN_DESCRICAO_CIDADE.AsString := CEP.FCidade;
    finally
      FreeAndNil(CEP);
    end;
  end;
end;

initialization
  RegisterClass(TCadMedico);

finalization
  UnRegisterClass(TCadMedico);

end.
