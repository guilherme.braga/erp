unit uCadCheque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxTextEdit,
  cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxCheckBox, uGBDBCheckBox,
  cxRadioGroup, uGBDBRadioGroup, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, cxButtonEdit, uGBDBButtonEditFK, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  uFrameFiltroVerticalPadrao, cxSplitter, JvExControls, JvButton, JvTransparentButton, uGBPanel;

type
  TCadCheque = class(TFrmCadastro_Padrao)
    labelCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    labelSacado: TLabel;
    labelDocFederal: TLabel;
    labelVlCheque: TLabel;
    labelNumero: TLabel;
    labelBanco: TLabel;
    labelAgencia: TLabel;
    labelContaCorrente: TLabel;
    labelDtEmissao: TLabel;
    labelDtVencimento: TLabel;
    edtSacado: TgbDBTextEdit;
    edtDocFederal: TgbDBTextEdit;
    edtVlCheque: TgbDBTextEdit;
    edtNumero: TgbDBTextEdit;
    edtBanco: TgbDBTextEdit;
    edtAgencia: TgbDBTextEdit;
    edtContaCorrente: TgbDBTextEdit;
    edtDtEmissao: TgbDBDateEdit;
    edtDtVencimento: TgbDBDateEdit;
    cdsDataID: TAutoIncField;
    cdsDataSACADO: TStringField;
    cdsDataDOC_FEDERAL: TStringField;
    cdsDataVL_CHEQUE: TFMTBCDField;
    cdsDataNUMERO: TIntegerField;
    cdsDataBANCO: TStringField;
    cdsDataAGENCIA: TStringField;
    cdsDataCONTA_CORRENTE: TStringField;
    cdsDataDT_EMISSAO: TDateField;
    cdsDataDT_VENCIMENTO: TDateField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataBO_CONCILIADO: TStringField;
    cdsDataDH_CONCILIADO: TDateTimeField;
    cdsDataID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsDataDOCUMENTO_ORIGEM: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    labelProcesso: TLabel;
    labelOrigem: TLabel;
    edtDescProcesso: TgbDBTextEdit;
    edtProcesso: TgbDBTextEdit;
    edtOrigem: TgbDBTextEdit;
    edtDescOrigem: TgbDBTextEdit;
    EdtDhConciliacao: TgbDBDateEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    lbContaCorrente: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure edtDocFederalExit(Sender: TObject);
  private
    procedure ValidarDocumentoFederal(AField: TField);
    { Private declarations }
  public

  protected
    procedure BloquearEdicao; override;
    procedure GerenciarControles; override;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uChaveProcesso, uChaveProcessoProxy, uTPessoa, uContaCorrenteProxy;

procedure TCadCheque.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados := cdsDataID_DOCUMENTO_ORIGEM.AsInteger > 0;
end;

procedure TCadCheque.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_CHEQUE, 0);
  cdsDataDT_EMISSAO.AsDateTime       := Date;
  cdsDataDT_VENCIMENTO.AsDateTime    := Date;
  cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString  := TChaveProcessoProxy.ORIGEM_CHEQUE;
end;

procedure TCadCheque.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
      //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Sacado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Sacado';
  campoFiltro.campo := 'SACADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento Federal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Federal (CPF/CNPJ)';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_CHEQUE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Banco
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Banco';
  campoFiltro.campo := 'BANCO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'CONTA_CORRENTE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Emiss�o
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Emiss�o';
  campoFiltro.campo := 'DT_EMISSAO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO'; //CONFIRMAR
  AFiltroVertical.FCampos.Add(campoFiltro);

  //N�mero do Cheque
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'N�mero do Cheque';
  campoFiltro.campo := 'NUMERO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

   //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conciliado';
  campoFiltro.campo := 'BO_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TConstantesCampoFiltro.ITENS_BO;
  campoFiltro.campoCaixaSelecao.valores := TConstantesCampoFiltro.VALORES_BO;
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Conciliado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DH_CONCILIADO';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //C�digo do Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo do Documento de Origem';
  campoFiltro.campo := 'ID_DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento de Origem
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento de Origem';
  campoFiltro.campo := 'DOCUMENTO_ORIGEM';
  campoFiltro.tabela := 'CHEQUE';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TContaCorrenteMovimento.LISTA_ORIGEM;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TCadCheque.edtDocFederalExit(Sender: TObject);
begin
  inherited;
  ValidarDocumentoFederal(cdsDataDOC_FEDERAL);
end;

procedure TCadCheque.GerenciarControles;
begin
  inherited;
  ActRemove.Visible := cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString = TChaveProcessoProxy.ORIGEM_CHEQUE;
end;

procedure TCadCheque.ValidarDocumentoFederal(AField: TField);
begin
  if not(AField.AsString.IsEmpty) and
   not(TPessoa.ValidaCnpjCeiCpf(AField.AsString))
  then
  begin
    AField.Clear;
    AField.FocusControl;
  end;
end;

initialization
  RegisterClass(TCadCheque);

finalization
  UnRegisterClass(TCadCheque);

end.
