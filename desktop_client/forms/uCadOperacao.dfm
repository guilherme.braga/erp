inherited CadOperacao: TCadOperacao
  Caption = 'Cadastro de Opera'#231#227'o'
  ClientWidth = 930
  ExplicitWidth = 946
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 930
    ExplicitWidth = 930
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 930
    Properties.ActivePage = cxtsData
    ExplicitWidth = 930
    ClientRectRight = 930
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 930
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 898
        ExplicitWidth = 898
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 930
      inherited DesignPanel: TJvDesignPanel
        Width = 930
        ExplicitWidth = 930
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 930
          Width = 930
          inherited dxBevel1: TdxBevel
            Width = 926
            ExplicitWidth = 926
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 45
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDados: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              926
              50)
            Height = 50
            Width = 926
            object Label7: TLabel
              Left = 5
              Top = 5
              Width = 96
              Height = 13
              Caption = 'Opera'#231#227'o Comercial'
            end
            object Label2: TLabel
              Left = 5
              Top = 29
              Width = 109
              Height = 13
              Caption = 'Natureza de Opera'#231#227'o'
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 120
              Top = 1
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 798
            end
            object EdtNaturezaOperacao: TgbDBButtonEditFK
              Left = 120
              Top = 25
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'NATUREZA_OPERACAO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 112
              Style.Color = 14606074
              TabOrder = 1
              gbRequired = True
              gbCampoPK = 'DESCRICAO'
              gbCamposRetorno = 'NATUREZA_OPERACAO'
              gbTableName = 'NATUREZA_OPERACAO'
              gbCamposConsulta = 'DESCRICAO'
              gbClasseDoCadastro = 'TCadNaturezaOperacao'
              gbIdentificadorConsulta = 'NATUREZA_OPERACAO'
              Width = 798
            end
          end
          object pcOperacao: TcxPageControl
            Left = 2
            Top = 84
            Width = 926
            Height = 247
            Align = alClient
            TabOrder = 2
            Properties.ActivePage = tsAcao
            Properties.CustomButtons.Buttons = <>
            Properties.Style = 8
            ClientRectBottom = 247
            ClientRectRight = 926
            ClientRectTop = 24
            object tsAcao: TcxTabSheet
              Caption = 'A'#231#245'es'
              ImageIndex = 0
              object pnFrameOperacaoAcao: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'pnFrameOperacaoAcao'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 223
                Width = 926
              end
            end
            object tsOperacaoFiscal: TcxTabSheet
              Caption = 'Opera'#231#245'es Fiscais'
              ImageIndex = 1
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameOperacaoOperacaoFiscal: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'gbPanel1'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 223
                Width = 926
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Opera'#231#227'o'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspOperacao'
    RemoteServer = DmConnection.dspOperacao
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataNATUREZA_OPERACAO: TStringField
      DisplayLabel = 'Natureza de Opera'#231#227'o'
      FieldName = 'NATUREZA_OPERACAO'
      Origin = 'NATUREZA_OPERACAO'
      Required = True
      Size = 255
    end
    object cdsDatafdqOperacaoItem: TDataSetField
      FieldName = 'fdqOperacaoItem'
    end
    object cdsDatafdqOperacaoFiscal: TDataSetField
      FieldName = 'fdqOperacaoFiscal'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42308.817100844920000000
      BuiltInReportLink = True
    end
  end
  object cdsOperacaoItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOperacaoItem
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 728
    Top = 80
    object cdsOperacaoItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOperacaoItemID_OPERACAO: TIntegerField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object cdsOperacaoItemID_ACAO: TIntegerField
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ID_ACAO'
      Origin = 'ID_ACAO'
      Required = True
    end
    object cdsOperacaoItemJOIN_DESCRICAO_ACAO: TStringField
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_ACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object cdsOperacaoFiscal: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqOperacaoFiscal
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 760
    Top = 80
    object cdsOperacaoFiscalID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsOperacaoFiscalDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object cdsOperacaoFiscalID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object cdsOperacaoFiscalCFOP_PRODUTO: TIntegerField
      DisplayLabel = 'CFOP de Produto'
      FieldName = 'CFOP_PRODUTO'
      Origin = 'CFOP_PRODUTO'
    end
    object cdsOperacaoFiscalCFOP_PRODUTO_ST: TIntegerField
      DisplayLabel = 'CFOP de Produto ST'
      FieldName = 'CFOP_PRODUTO_ST'
      Origin = 'CFOP_PRODUTO_ST'
    end
    object cdsOperacaoFiscalCFOP_MERCADORIA: TIntegerField
      DisplayLabel = 'CFOP da Mercadoria'
      FieldName = 'CFOP_MERCADORIA'
      Origin = 'CFOP_MERCADORIA'
    end
    object cdsOperacaoFiscalCFOP_MERCADORIA_ST: TIntegerField
      DisplayLabel = 'CFOP da Mercadoria ST'
      FieldName = 'CFOP_MERCADORIA_ST'
      Origin = 'CFOP_MERCADORIA_ST'
    end
    object cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO: TStringField
      DisplayLabel = 'CFOP Produto'
      FieldName = 'JOIN_DESCRICAO_CFOP_PRODUTO'
      Origin = 'JOIN_DESCRICAO_CFOP_PRODUTO'
      ProviderFlags = []
      Size = 255
    end
    object cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_PRODUTO_ST: TStringField
      DisplayLabel = 'CFOP Produto ST'
      FieldName = 'JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      Origin = 'JOIN_DESCRICAO_CFOP_PRODUTO_ST'
      ProviderFlags = []
      Size = 255
    end
    object cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA: TStringField
      DisplayLabel = 'CFOP Mercadoria'
      FieldName = 'JOIN_DESCRICAO_CFOP_MERCADORIA'
      Origin = 'JOIN_DESCRICAO_CFOP_MERCADORIA'
      ProviderFlags = []
      Size = 255
    end
    object cdsOperacaoFiscalJOIN_DESCRICAO_CFOP_MERCADORIA_ST: TStringField
      DisplayLabel = 'CFOP Mercadoria ST'
      FieldName = 'JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      Origin = 'JOIN_DESCRICAO_CFOP_MERCADORIA_ST'
      ProviderFlags = []
      Size = 255
    end
  end
end
