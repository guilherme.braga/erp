inherited CadMotivo: TCadMotivo
  Caption = 'Cadastro de Motivo'
  ClientHeight = 265
  ClientWidth = 868
  ExplicitWidth = 884
  ExplicitHeight = 304
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 868
    ExplicitWidth = 868
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 868
    Height = 138
    Properties.ActivePage = cxtsData
    ExplicitWidth = 868
    ExplicitHeight = 138
    ClientRectBottom = 138
    ClientRectRight = 868
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 868
      ExplicitHeight = 114
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 868
        Height = 114
        ExplicitWidth = 868
        ExplicitHeight = 114
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 868
      ExplicitHeight = 114
      inherited DesignPanel: TJvDesignPanel
        Width = 868
        Height = 114
        ExplicitWidth = 868
        ExplicitHeight = 114
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 868
          ExplicitHeight = 114
          Height = 114
          Width = 868
          inherited dxBevel1: TdxBevel
            Width = 864
            ExplicitWidth = 864
          end
          object Label6: TLabel
            Left = 8
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 39
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label1: TLabel
            Left = 678
            Top = 39
            Width = 20
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tipo'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 58
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 58
            Top = 35
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbRequired = True
            gbPassword = False
            Width = 615
          end
          object gbDBComboBox1: TgbDBComboBox
            Left = 702
            Top = 35
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'TIPO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.Items.Strings = (
              'NOTA FISCAL'
              'AJUSTE DE ESTOQUE'
              'INVENTARIO')
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Width = 157
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Motivo'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspMotivo'
    RemoteServer = DmConnection.dspMotivo
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 45
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.448590104170000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
