inherited CadServico: TCadServico
  Caption = 'Cadastro de Servi'#231'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 37
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label3: TLabel
            Left = 8
            Top = 63
            Width = 43
            Height = 13
            Caption = 'Vl. Custo'
          end
          object Label5: TLabel
            Left = 158
            Top = 63
            Width = 40
            Height = 13
            Caption = '% Lucro'
          end
          object Label1: TLabel
            Left = 302
            Top = 63
            Width = 45
            Height = 13
            Caption = 'Vl. Venda'
          end
          object Label4: TLabel
            Left = 451
            Top = 63
            Width = 40
            Height = 13
            Caption = 'Dura'#231#227'o'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 58
            Top = 7
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 58
            Top = 34
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 868
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 871
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            Transparent = True
            Width = 55
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 58
            Top = 59
            DataBinding.DataField = 'VL_CUSTO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 96
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 202
            Top = 59
            DataBinding.DataField = 'PERC_LUCRO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            gbPassword = False
            Width = 96
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 353
            Top = 59
            DataBinding.DataField = 'VL_VENDA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbRequired = True
            gbPassword = False
            Width = 96
          end
          object gbDBCheckBox2: TgbDBCheckBox
            Left = 620
            Top = 59
            Caption = 'Servi'#231'o de Terceiro?'
            DataBinding.DataField = 'BO_SERVICO_TERCEIRO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            Transparent = True
            Width = 126
          end
          object cxDBTimeEdit1: TcxDBTimeEdit
            Left = 495
            Top = 59
            DataBinding.DataField = 'DURACAO'
            DataBinding.DataSource = dsData
            TabOrder = 5
            Width = 121
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Servi'#231'o'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspServico'
    RemoteServer = DmConnection.dspServico
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataVL_CUSTO: TFMTBCDField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      OnChange = CalcularValor
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataPERC_LUCRO: TFMTBCDField
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      Origin = 'PERC_LUCRO'
      OnChange = CalcularValor
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_VENDA: TFMTBCDField
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      Required = True
      OnChange = CalcularValor
      EditFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataDURACAO: TTimeField
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'DURACAO'
      Origin = 'DURACAO'
    end
    object cdsDataBO_SERVICO_TERCEIRO: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Servi'#231'o de Terceiro'
      FieldName = 'BO_SERVICO_TERCEIRO'
      Origin = 'BO_SERVICO_TERCEIRO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.456620011570000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
end
