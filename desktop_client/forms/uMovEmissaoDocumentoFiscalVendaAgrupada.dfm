inherited MovEmissaoDocumentoFiscalVendaAgrupada: TMovEmissaoDocumentoFiscalVendaAgrupada
  Caption = 'Emiss'#227'o de Documento Fiscal de Vendas Agrupadas'
  ExplicitWidth = 865
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited ActionListMain: TActionList
    object ActEmitirNFE: TAction
      Category = 'Action'
      Caption = 'Emitir NFe'
      ImageIndex = 157
      OnExecute = ActEmitirNFEExecute
    end
    object ActEmitirNFCe: TAction
      Category = 'Action'
      Caption = 'Emitir NFCe'
      ImageIndex = 158
      OnExecute = ActEmitirNFCeExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 191
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 79
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbEmitirNFE'
        end
        item
          Visible = True
          ItemName = 'lbEmitirNFCe'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 103
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object lbEmitirNFE: TdxBarLargeButton [9]
      Action = ActEmitirNFE
      Category = 1
    end
    object lbEmitirNFCe: TdxBarLargeButton [10]
      Action = ActEmitirNFCe
      Category = 1
    end
  end
end
