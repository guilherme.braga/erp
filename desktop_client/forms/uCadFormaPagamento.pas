unit uCadFormaPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  uGBDBTextEdit, cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls,
  cxRadioGroup, uGBDBRadioGroup, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK;

type
  TCadFormaPagamento = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataID_TIPO_QUITACAO: TIntegerField;
    cdsDataJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    cdsDataJOIN_TIPO_TIPO_QUITACAO: TStringField;
    Label1: TLabel;
    descTipoQuitacao: TgbDBTextEdit;
    edtIdTipoQuitacao: TgbDBButtonEditFK;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadFormaPagamento);

finalization
  UnRegisterClass(TCadFormaPagamento);

end.
