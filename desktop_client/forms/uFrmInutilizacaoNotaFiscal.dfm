inherited FrmInutilizacaoNotaFiscal: TFrmInutilizacaoNotaFiscal
  Caption = 'Inutiliza'#231#227'o de Documento Fiscal'
  ClientHeight = 398
  ClientWidth = 848
  ExplicitWidth = 864
  ExplicitHeight = 437
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 848
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'bbGrupoFiltro'
        end
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    Height = 296
    Width = 848
    object gridCentroResultado: TcxGrid
      Left = 2
      Top = 2
      Width = 844
      Height = 292
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      ExplicitLeft = 1
      ExplicitTop = 6
      object ViewConsultaDados: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        DataController.DataSource = dsNotasFiscaisInutilizadas
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Images = DmAcesso.cxImage16x16
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.ShowColumnFilterButtons = sfbAlways
        Bands = <
          item
            Caption = 'Selecione as notas fiscais que deseja inutilizar'
          end>
      end
      object cxGridLevel12: TcxGridLevel
        GridView = ViewConsultaDados
      end
    end
  end
  inherited ActionListMain: TActionList
    object ActRealizarInutilizacao: TAction
      Category = 'Action'
      Caption = 'Realizar Inutiliza'#231#227'o F5'
      ImageIndex = 146
      ShortCut = 116
      OnExecute = ActInutilizarNotaFiscalExecute
    end
    object ActPesquisar: TAction
      Category = 'Action'
      Caption = 'Consultar F7'
      ImageIndex = 16
      ShortCut = 118
      OnExecute = ActPesquisarExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 383
      FloatClientWidth = 51
      FloatClientHeight = 54
      Row = 1
    end
    inherited BarAcao: TdxBar
      DockedLeft = 120
      FloatClientWidth = 119
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbConsultar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbInutilizarNotaFiscal'
        end>
      Row = 1
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 295
      FloatClientWidth = 100
      FloatClientHeight = 22
      Row = 1
    end
    object bbGrupoFiltro: TdxBar [3]
      Caption = 'Filtros'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 883
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 80
          Visible = True
          ItemName = 'EdtMes'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 80
          Visible = True
          ItemName = 'EdtAno'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object cxBarEditItem1: TcxBarEditItem [7]
      Align = iaCenter
      Caption = 'Per'#237'odo de Emiss'#227'o'
      Category = 0
      Hint = 'Per'#237'odo de Emiss'#227'o'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
    end
    object cxBarEditItem2: TcxBarEditItem [8]
      Caption = 'In'#237'cio'
      Category = 0
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
    end
    object cxBarEditItem3: TcxBarEditItem [9]
      Caption = 'Fim   '
      Category = 0
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object EdtMes: TdxBarCombo [10]
      Caption = 'M'#234's'
      Category = 0
      Hint = 'M'#234's'
      Visible = ivAlways
      DropDownCount = 12
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12')
      ItemIndex = -1
    end
    object EdtAno: TdxBarCombo [11]
      Caption = 'Ano'
      Category = 0
      Hint = 'Ano'
      Visible = ivAlways
      ItemIndex = -1
    end
    object lbInutilizarNotaFiscal: TdxBarLargeButton [15]
      Action = ActRealizarInutilizacao
      Category = 1
    end
    object lbConsultar: TdxBarLargeButton [16]
      Action = ActPesquisar
      Category = 1
    end
  end
  object fdmNotasFiscaisInutilizadas: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evDetailOptimize]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailOptimize = False
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 272
    Top = 216
  end
  object dsNotasFiscaisInutilizadas: TDataSource
    DataSet = fdmNotasFiscaisInutilizadas
    Left = 300
    Top = 216
  end
  object PopUpControllerPesquisa: TcxGridPopupMenu
    Grid = gridCentroResultado
    PopupMenus = <
      item
        GridView = ViewConsultaDados
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end>
    Left = 508
    Top = 184
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 478
    Top = 184
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 148
    Top = 256
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
end
