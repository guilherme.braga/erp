inherited MovGeracaoDocumento: TMovGeracaoDocumento
  Caption = 'Gera'#231#227'o de Documento'
  ClientHeight = 694
  ClientWidth = 1185
  ExplicitWidth = 1201
  ExplicitHeight = 733
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1185
    ExplicitWidth = 1185
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1185
    Height = 567
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1185
    ExplicitHeight = 567
    ClientRectBottom = 567
    ClientRectRight = 1185
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1185
      ExplicitHeight = 543
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1153
        Height = 543
        ExplicitWidth = 1153
        ExplicitHeight = 543
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 543
        Height = 543
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 543
        ExplicitHeight = 543
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1185
      ExplicitHeight = 543
      inherited DesignPanel: TJvDesignPanel
        Width = 1185
        Height = 543
        ExplicitWidth = 1185
        ExplicitHeight = 543
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1185
          ExplicitHeight = 543
          Height = 543
          Width = 1185
          inherited dxBevel1: TdxBevel
            Width = 1181
            ExplicitLeft = 1
            ExplicitTop = 3
            ExplicitWidth = 1180
          end
          object Label6: TLabel
            Left = 6
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 149
            Top = 8
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object Label7: TLabel
            Left = 991
            Top = 8
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 990
          end
          object Label20: TLabel
            Left = 362
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 83
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 62
          end
          object edtDtCadastro: TgbDBDateEdit
            Left = 226
            Top = 4
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object edtSituacao: TgbDBTextEdit
            Left = 1036
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 140
          end
          object PnDadosGeral: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Height = 362
            Width = 1181
            object dxBevel3: TdxBevel
              Left = 2
              Top = 124
              Width = 1177
              Height = 7
              Align = alTop
              Shape = dxbsLineBottom
              ExplicitLeft = 3
              ExplicitTop = 131
              ExplicitWidth = 1176
            end
            object gbDados: TgbPanel
              Left = 2
              Top = 2
              Align = alTop
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              DesignSize = (
                1177
                122)
              Height = 122
              Width = 1177
              object Label11: TLabel
                Left = 656
                Top = 7
                Width = 99
                Height = 13
                Caption = 'Centro de Resultado'
              end
              object Label12: TLabel
                Left = 656
                Top = 31
                Width = 81
                Height = 13
                Caption = 'Conta de An'#225'lise'
              end
              object Label10: TLabel
                Left = 4
                Top = 79
                Width = 34
                Height = 13
                Caption = 'Pessoa'
              end
              object Label4: TLabel
                Left = 4
                Top = 55
                Width = 46
                Height = 13
                Caption = 'Descri'#231#227'o'
              end
              object Label14: TLabel
                Left = 500
                Top = 31
                Width = 24
                Height = 13
                Caption = 'Valor'
              end
              object Label3: TLabel
                Left = 166
                Top = 102
                Width = 45
                Height = 13
                Caption = 'Dt. Inicial'
              end
              object Label5: TLabel
                Left = 4
                Top = 6
                Width = 24
                Height = 13
                Caption = 'Tipo'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label9: TLabel
                Left = 4
                Top = 31
                Width = 54
                Height = 13
                Caption = 'Documento'
              end
              object Label15: TLabel
                Left = 656
                Top = 55
                Width = 102
                Height = 13
                Caption = 'Forma de Pagamento'
              end
              object Label18: TLabel
                Left = 302
                Top = 102
                Width = 80
                Height = 13
                Caption = 'Dt. Compet'#234'ncia'
              end
              object Label21: TLabel
                Left = 4
                Top = 102
                Width = 72
                Height = 13
                Caption = 'Dt. Documento'
              end
              object lbContaCorrente: TLabel
                Left = 656
                Top = 102
                Width = 75
                Height = 13
                Caption = 'Conta Corrente'
              end
              object Label22: TLabel
                Left = 656
                Top = 79
                Width = 39
                Height = 13
                Caption = 'Carteira'
              end
              object Label23: TLabel
                Left = 473
                Top = 102
                Width = 53
                Height = 13
                Caption = 'Vl. Entrada'
              end
              object edtDescCentroResultado: TgbDBTextEdit
                Left = 824
                Top = 4
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 15
                gbReadyOnly = True
                gbPassword = False
                Width = 348
              end
              object edtCentroResultado: TgbDBButtonEditFK
                Left = 767
                Top = 4
                DataBinding.DataField = 'ID_CENTRO_RESULTADO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 9
                gbTextEdit = edtDescCentroResultado
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
                gbTableName = 'CENTRO_RESULTADO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CENTRO_RESULTADO'
                Width = 60
              end
              object edtDescContaAnalise: TgbDBTextEdit
                Left = 824
                Top = 27
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 16
                gbReadyOnly = True
                gbPassword = False
                Width = 348
              end
              object edtPessoa: TgbDBButtonEditFK
                Left = 79
                Top = 75
                DataBinding.DataField = 'ID_PESSOA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 4
                gbTextEdit = edtDescPessoa
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_PESSOA;JOIN_DESCRICAO_NOME_PESSOA'
                gbTableName = 'PESSOA'
                gbCamposConsulta = 'ID;NOME'
                gbAntesDeConsultar = edtPessoagbAntesDeConsultar
                gbIdentificadorConsulta = 'PESSOA'
                Width = 63
              end
              object edtDescPessoa: TgbDBTextEdit
                Left = 139
                Top = 75
                TabStop = False
                DataBinding.DataField = 'JOIN_DESCRICAO_NOME_PESSOA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 14
                gbReadyOnly = True
                gbPassword = False
                Width = 512
              end
              object edtDtInicial: TgbDBDateEdit
                Left = 215
                Top = 98
                DataBinding.DataField = 'DT_BASE'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 6
                gbRequired = True
                gbDateTime = False
                Width = 83
              end
              object gbDBRadioGroup1: TgbDBRadioGroup
                Left = 79
                Top = -4
                DataBinding.DataField = 'TIPO'
                DataBinding.DataSource = dsData
                ParentFont = False
                Properties.Columns = 2
                Properties.ImmediatePost = True
                Properties.Items = <
                  item
                    Caption = 'Conta a Pagar'
                    Value = 'CONTAPAGAR'
                  end
                  item
                    Caption = 'Conta a Receber'
                    Value = 'CONTARECEBER'
                  end>
                Style.BorderStyle = ebsOffice11
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.LookAndFeel.Kind = lfOffice11
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                OnExit = gbDBRadioGroup1Exit
                Height = 26
                Width = 571
              end
              object edtFormaPagamento: TgbDBButtonEditFK
                Left = 767
                Top = 51
                DataBinding.DataField = 'ID_FORMA_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 11
                gbTextEdit = edtDescFormaPagamento
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_FORMA_PAGAMENTO;JOIN_DESCRICAO_FORMA_PAGAMENTO'
                gbTableName = 'FORMA_PAGAMENTO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'FORMA_PAGAMENTO'
                Width = 60
              end
              object edtDescFormaPagamento: TgbDBTextEdit
                Left = 824
                Top = 51
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 17
                gbReadyOnly = True
                gbPassword = False
                Width = 348
              end
              object EdtContaAnalise: TcxDBButtonEdit
                Left = 767
                Top = 27
                DataBinding.DataField = 'ID_CONTA_ANALISE'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ClickKey = 13
                Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
                Style.Color = 14606074
                TabOrder = 10
                OnDblClick = EdtContaAnaliseDblClick
                OnExit = EdtContaAnalisePropertiesEditValueChanged
                OnKeyDown = EdtContaAnaliseKeyDown
                Width = 60
              end
              object EdtDocumento: TgbDBTextEdit
                Left = 79
                Top = 27
                DataBinding.DataField = 'DOCUMENTO'
                DataBinding.DataSource = dsData
                ParentFont = False
                Properties.CharCase = ecUpperCase
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.LookAndFeel.Kind = lfOffice11
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                gbRequired = True
                gbPassword = False
                Width = 415
              end
              object EdtDescricao: TgbDBTextEdit
                Left = 79
                Top = 51
                DataBinding.DataField = 'DESCRICAO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                gbRequired = True
                gbPassword = False
                Width = 572
              end
              object edtDtCompetencia: TgbDBDateEdit
                Left = 386
                Top = 98
                DataBinding.DataField = 'DT_COMPETENCIA'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 7
                gbRequired = True
                gbDateTime = False
                Width = 83
              end
              object edDtDocumento: TgbDBDateEdit
                Left = 79
                Top = 98
                DataBinding.DataField = 'DT_DOCUMENTO'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 5
                gbRequired = True
                gbDateTime = False
                Width = 83
              end
              object edtContaCorrente: TgbDBButtonEditFK
                Left = 767
                Top = 98
                DataBinding.DataField = 'ID_CONTA_CORRENTE'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = clWhite
                TabOrder = 13
                gbTextEdit = descContaCorrente
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
                gbTableName = 'CONTA_CORRENTE'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CONTA_CORRENTE'
                Width = 60
              end
              object descContaCorrente: TgbDBTextEdit
                Left = 824
                Top = 98
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 18
                gbReadyOnly = True
                gbPassword = False
                Width = 348
              end
              object gbDBButtonEditFK2: TgbDBButtonEditFK
                Left = 767
                Top = 75
                DataBinding.DataField = 'ID_CARTEIRA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 12
                gbTextEdit = gbDBTextEdit2
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CARTEIRA; JOIN_DESCRICAO_CARTEIRA'
                gbTableName = 'CARTEIRA'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CARTEIRA'
                Width = 60
              end
              object gbDBTextEdit2: TgbDBTextEdit
                Left = 824
                Top = 75
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CARTEIRA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 19
                gbReadyOnly = True
                gbPassword = False
                Width = 348
              end
              object edtEntradaQuitacao: TgbDBCalcEdit
                Left = 530
                Top = 98
                Hint = 
                  'Quando informado algum valor de entrada, ao efetivar a gera'#231#227'o d' +
                  'e documento, esse valor ser'#225' creditado/debitado da movimenta'#231#227'o ' +
                  'de conta corrente'
                DataBinding.DataField = 'VL_ENTRADA'
                DataBinding.DataSource = dsData
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 8
                gbRequired = True
                Width = 121
              end
              object edtValorTitulo: TgbDBCalcEdit
                Left = 530
                Top = 27
                DataBinding.DataField = 'VL_TITULO'
                DataBinding.DataSource = dsData
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                OnExit = edtValorTituloExit
                gbRequired = True
                Width = 121
              end
            end
            object PnDadosEntrada: TgbPanel
              Left = 2
              Top = 131
              Align = alTop
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Height = 206
              Width = 1177
              object PnQuitacaoCartao: TgbPanel
                Left = 2
                Top = 44
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 53
                Width = 1173
              end
              object PnQuitacaoCheque: TgbPanel
                Left = 2
                Top = 97
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Transparent = True
                ExplicitTop = 93
                Height = 110
                Width = 1173
              end
              object PnTituloCheque: TgbPanel
                Left = 2
                Top = 2
                Align = alTop
                Alignment = alCenterCenter
                Caption = 'Dados da Entrada'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                ParentBackground = False
                ParentFont = False
                Style.BorderStyle = ebsOffice11
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 19
                Width = 1173
              end
              object PnDadosGeralEntrada: TgbPanel
                Left = 2
                Top = 21
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                DesignSize = (
                  1173
                  23)
                Height = 23
                Width = 1173
                object Label24: TLabel
                  Left = 654
                  Top = 7
                  Width = 102
                  Height = 13
                  Caption = 'Forma de Pagamento'
                end
                object Label25: TLabel
                  Left = 2
                  Top = 7
                  Width = 75
                  Height = 13
                  Caption = 'Conta Corrente'
                end
                object gbDBButtonEditFK3: TgbDBButtonEditFK
                  Left = 765
                  Top = 3
                  DataBinding.DataField = 'ID_FORMA_PAGAMENTO_ENTRADA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 2
                  gbTextEdit = gbDBTextEdit3
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 
                    'ID_FORMA_PAGAMENTO_ENTRADA;JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRAD' +
                    'A'
                  gbTableName = 'FORMA_PAGAMENTO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'FORMA_PAGAMENTO'
                  Width = 60
                end
                object gbDBTextEdit3: TgbDBTextEdit
                  Left = 822
                  Top = 3
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 348
                end
                object gbDBButtonEditFK4: TgbDBButtonEditFK
                  Left = 77
                  Top = 3
                  DataBinding.DataField = 'ID_CONTA_CORRENTE_ENTRADA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = clWhite
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit4
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_CONTA_CORRENTE_ENTRADA;JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
                  gbTableName = 'CONTA_CORRENTE'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'CONTA_CORRENTE'
                  Width = 63
                end
                object gbDBTextEdit4: TgbDBTextEdit
                  Left = 137
                  Top = 3
                  TabStop = False
                  DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 511
                end
              end
            end
          end
          object gridParcela: TcxGrid
            AlignWithMargins = True
            Left = 5
            Top = 448
            Width = 1175
            Height = 65
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = []
            Images = DmAcesso.cxImage16x16
            ParentFont = False
            TabOrder = 4
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            ExplicitTop = 418
            ExplicitHeight = 95
            object cxGridDBBandedTableView1: TcxGridDBBandedTableView
              PopupMenu = pmAtualizarParcelas
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.Images = DmAcesso.cxImage16x16
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Insert.Visible = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Post.Visible = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Buttons.Filter.Visible = False
              Navigator.InfoPanel.Visible = True
              Navigator.Visible = True
              DataController.DataSource = dsGeracaoDocumentoParcela
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Images = DmAcesso.cxImage16x16
              OptionsBehavior.NavigatorHints = True
              OptionsBehavior.ExpandMasterRowOnDblClick = False
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.BandMoving = False
              OptionsCustomize.NestedBands = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Inserting = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GroupByBox = False
              OptionsView.GroupRowStyle = grsOffice11
              OptionsView.ShowColumnFilterButtons = sfbAlways
              OptionsView.BandHeaders = False
              Styles.ContentOdd = DmAcesso.OddColor
              Bands = <
                item
                end>
              object cxGridDBBandedTableView1ID: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Width = 54
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1DOCUMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DOCUMENTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 170
                Position.BandIndex = 0
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1DESCRICAO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DESCRICAO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Width = 192
                Position.BandIndex = 0
                Position.ColIndex = 4
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_TITULO'
                PropertiesClassName = 'TcxCurrencyEditProperties'
                Properties.AssignedValues.MinValue = True
                Properties.MaxValue = 999999999999999.000000000000000000
                Properties.Nullstring = '0'
                Options.Sorting = False
                Width = 141
                Position.BandIndex = 0
                Position.ColIndex = 7
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1QT_PARCELA: TcxGridDBBandedColumn
                DataBinding.FieldName = 'QT_PARCELA'
                Options.Editing = False
                Options.Sorting = False
                Width = 73
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1NR_PARCELA: TcxGridDBBandedColumn
                DataBinding.FieldName = 'NR_PARCELA'
                Options.Editing = False
                Options.Sorting = False
                Width = 87
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DT_VENCIMENTO'
                PropertiesClassName = 'TcxDateEditProperties'
                Properties.ImmediatePost = True
                Properties.PostPopupValueOnTab = True
                Properties.SaveTime = False
                Properties.ShowTime = False
                Options.Sorting = False
                Width = 115
                Position.BandIndex = 0
                Position.ColIndex = 6
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1ID_GERACAO_DOCUMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID_GERACAO_DOCUMENTO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Width = 111
                Position.BandIndex = 0
                Position.ColIndex = 8
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1TOTAL_PARCELAS: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TOTAL_PARCELAS'
                Visible = False
                Width = 71
                Position.BandIndex = 0
                Position.ColIndex = 9
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1ID_CONTA_ANALISE: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID_CONTA_ANALISE'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.OnButtonClick = cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesButtonClick
                Properties.OnEditValueChanged = cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesEditValueChanged
                Visible = False
                Width = 166
                Position.BandIndex = 0
                Position.ColIndex = 10
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1ID_CENTRO_RESULTADO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.OnButtonClick = cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesButtonClick
                Properties.OnEditValueChanged = cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesEditValueChanged
                Visible = False
                Width = 168
                Position.BandIndex = 0
                Position.ColIndex = 11
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1JOIN_DESCRICAO_CENTRO_RESULTADO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ReadOnly = True
                Properties.OnButtonClick = cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesButtonClick
                Properties.OnEditValueChanged = cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesEditValueChanged
                Visible = False
                Position.BandIndex = 0
                Position.ColIndex = 12
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_ANALISE: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ReadOnly = True
                Properties.OnButtonClick = cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesButtonClick
                Properties.OnEditValueChanged = EdtContaAnalisePropertiesEditValueChanged
                Visible = False
                Position.BandIndex = 0
                Position.ColIndex = 13
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1ID_CONTA_CORRENTE: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID_CONTA_CORRENTE'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.OnButtonClick = cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesButtonClick
                Properties.OnEditValueChanged = cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesEditValueChanged
                Position.BandIndex = 0
                Position.ColIndex = 15
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTE: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ReadOnly = True
                Properties.OnButtonClick = cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesButtonClick
                Properties.OnEditValueChanged = cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesEditValueChanged
                Position.BandIndex = 0
                Position.ColIndex = 16
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1DT_COMPETENCIA: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DT_COMPETENCIA'
                Width = 132
                Position.BandIndex = 0
                Position.ColIndex = 5
                Position.RowIndex = 0
              end
              object cxGridDBBandedTableView1DT_DOCUMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DT_DOCUMENTO'
                Position.BandIndex = 0
                Position.ColIndex = 14
                Position.RowIndex = 0
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBBandedTableView1
            end
          end
          object pnParcelas: TgbPanel
            AlignWithMargins = True
            Left = 5
            Top = 399
            Align = alTop
            Alignment = alCenterCenter
            Caption = 'Parcelas'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            ParentBackground = False
            ParentFont = False
            Style.BorderStyle = ebsOffice11
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            ExplicitLeft = 3
            ExplicitTop = 367
            Height = 19
            Width = 1175
          end
          object edtProcesso: TgbDBTextEdit
            Left = 408
            Top = 4
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 578
          end
          object gbPanel4: TgbPanel
            Left = 2
            Top = 516
            Align = alBottom
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            Transparent = True
            DesignSize = (
              1181
              25)
            Height = 25
            Width = 1181
            object Label8: TLabel
              Left = 4
              Top = 8
              Width = 58
              Height = 13
              Caption = 'Observa'#231#227'o'
            end
            object edtObservacao: TgbDBBlobEdit
              Left = 78
              Top = 4
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OBSERVACAO'
              DataBinding.DataSource = dsData
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ClearKey = 16430
              Properties.ImmediatePost = True
              Properties.PopupHeight = 300
              Properties.PopupWidth = 160
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Width = 1094
            end
          end
          object pnGeracaoParcelas: TgbPanel
            Left = 2
            Top = 421
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            Transparent = True
            ExplicitTop = 335
            Height = 24
            Width = 1181
            object gbPlanoPagamentoRepeticao: TgbPanel
              Left = 901
              Top = 2
              Align = alLeft
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              Transparent = True
              ExplicitLeft = 929
              Height = 20
              Width = 280
              object Label16: TLabel
                Left = 0
                Top = 4
                Width = 40
                Height = 13
                Caption = 'Parcelas'
              end
              object Label17: TLabel
                Left = 108
                Top = 4
                Width = 65
                Height = 13
                Caption = 'Parcela Inicial'
              end
              object gbDBSpinEdit1: TgbDBSpinEdit
                Left = 44
                Top = 0
                Hint = 'Quantidade de parcelas'
                DataBinding.DataField = 'QT_PARCELAS'
                DataBinding.DataSource = dsData
                Properties.MaxValue = 999.000000000000000000
                Properties.MinValue = 1.000000000000000000
                Properties.ReadOnly = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                gbRequired = True
                Width = 60
              end
              object gbDBSpinEdit2: TgbDBSpinEdit
                Left = 177
                Top = 0
                DataBinding.DataField = 'PARCELA_BASE'
                DataBinding.DataSource = dsData
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                gbRequired = True
                Width = 65
              end
            end
            object gbPlanoPagamentoCadastro: TgbPanel
              Left = 441
              Top = 2
              Align = alLeft
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              ExplicitLeft = 369
              Height = 20
              Width = 460
              object Label2: TLabel
                Left = 0
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Plano'
              end
              object Label19: TLabel
                Left = 326
                Top = 4
                Width = 65
                Height = 13
                Caption = 'Parcela Inicial'
              end
              object gbDBButtonEditFK1: TgbDBButtonEditFK
                Left = 30
                Top = 0
                DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 0
                gbTextEdit = gbDBTextEdit1
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO;QT_PARCELAS'
                gbTableName = 'PLANO_PAGAMENTO'
                gbCamposConsulta = 'ID;DESCRICAO;QT_PARCELA'
                gbIdentificadorConsulta = 'PLANO_PAGAMENTO'
                Width = 67
              end
              object gbDBTextEdit1: TgbDBTextEdit
                Left = 94
                Top = 0
                TabStop = False
                DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                gbReadyOnly = True
                gbPassword = False
                Width = 229
              end
              object gbDBSpinEdit3: TgbDBSpinEdit
                Left = 395
                Top = 0
                DataBinding.DataField = 'PARCELA_BASE'
                DataBinding.DataSource = dsData
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                gbRequired = True
                Width = 60
              end
            end
            object gbPanel1: TgbPanel
              Left = 2
              Top = 2
              Align = alLeft
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              Height = 20
              Width = 439
              object Label13: TLabel
                Left = 210
                Top = 3
                Width = 47
                Height = 13
                Caption = 'Gera'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label26: TLabel
                Left = 4
                Top = 3
                Width = 65
                Height = 13
                Caption = 'Parcelamento'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object gbDBRadioGroup2: TgbDBRadioGroup
                Left = 263
                Top = -6
                Hint = 
                  '|Parcelamento:'#13#10'- O Valor ser'#225' dividido entre as Parcelas gerada' +
                  's.'#13#10'- A Dt. Compet'#234'ncia ser'#225' a mesma para todas as Parcelas.'#13#10#13#10 +
                  'Repeti'#231#227'o:'#13#10'- O Valor ser'#225' repetido em todas as Parcelas geradas' +
                  '.'#13#10'- A Dt. Compet'#234'ncia ser'#225' sequenciada M'#234's a M'#234's em cada Parcel' +
                  'a.'
                DataBinding.DataField = 'TIPO_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                ParentFont = False
                ParentShowHint = False
                Properties.Columns = 2
                Properties.ImmediatePost = True
                Properties.Items = <
                  item
                    Caption = 'Plano'
                    Value = 'CADASTRADO'
                  end
                  item
                    Caption = 'Parcelas'
                    Value = 'REPETICAO'
                  end>
                ShowHint = True
                Style.BorderStyle = ebsOffice11
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.LookAndFeel.Kind = lfOffice11
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 25
                Width = 168
              end
              object gbDBCalcEdit1: TgbDBCalcEdit
                Left = 79
                Top = -1
                Hint = 
                  'Quando informado algum valor de entrada, ao efetivar a gera'#231#227'o d' +
                  'e documento, esse valor ser'#225' creditado/debitado da movimenta'#231#227'o ' +
                  'de conta corrente'
                TabStop = False
                DataBinding.DataField = 'CC_VL_PARCELAMENTO'
                DataBinding.DataSource = dsData
                Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                Properties.ImmediatePost = True
                Properties.ReadOnly = True
                Properties.UseThousandSeparator = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                Style.TextStyle = [fsBold]
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                gbReadyOnly = True
                gbRequired = True
                Width = 121
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 102
      FloatClientHeight = 280
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbDuplicarRegistro'
        end>
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 300
      FloatClientWidth = 132
      FloatClientHeight = 329
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnEstornarDocumento'
        end
        item
          Visible = True
          ItemName = 'btnGerarDocumento'
        end
        item
          Visible = True
          ItemName = 'lbLotePagamento'
        end
        item
          Visible = True
          ItemName = 'lbLoteRecebimento'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 822
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 993
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 747
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      FloatClientWidth = 61
      FloatClientHeight = 180
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 901
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object btnEstornarDocumento: TdxBarLargeButton [9]
      Action = ActEstornarDocumento
      Category = 0
    end
    object btnGerarDocumento: TdxBarLargeButton [10]
      Action = ActGerarDocumento
      Category = 0
    end
    object lbLotePagamento: TdxBarLargeButton [12]
      Action = ActLotePagamento
      Category = 0
    end
    object lbLoteRecebimento: TdxBarLargeButton [13]
      Action = ActLoteRecebimento
      Category = 0
    end
  end
  inherited ActionListMain: TActionList
    object ActGerarDocumento: TAction [12]
      Category = 'Action'
      Caption = 'Gerar Documento'
      Hint = 'Efetiva a gera'#231#227'o dos documentos'
      ImageIndex = 92
      OnExecute = ActGerarDocumentoExecute
    end
    object ActEstornarDocumento: TAction [13]
      Category = 'Action'
      Caption = 'Estornar Documento'
      Hint = 'Estorna a gera'#231#227'o dos documentos'
      ImageIndex = 95
      OnExecute = ActEstornarDocumentoExecute
    end
    object ActLotePagamento: TAction
      Category = 'Action'
      Caption = 'Pagamento em Lote'
      ImageIndex = 135
      OnExecute = ActLotePagamentoExecute
    end
    object ActLoteRecebimento: TAction
      Category = 'Action'
      Caption = 'Recebimento em Lote'
      ImageIndex = 133
      OnExecute = ActLoteRecebimentoExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspGeracaoDocumento'
    RemoteServer = DmConnection.dspGeracaoDocumento
    AfterOpen = cdsDataAfterOpen
    OnCalcFields = cdsDataCalcFields
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 80
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      OnChange = GerarParcela
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataSTATUS: TStringField
      DefaultExpression = 'ABERTO'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsDataTIPO: TStringField
      DefaultExpression = 'CONTAPAGAR'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      OnChange = GerarParcela
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      OnChange = GerarParcela
    end
    object cdsDataDT_BASE: TDateField
      DisplayLabel = 'Dt. Base'
      FieldName = 'DT_BASE'
      Origin = 'DT_BASE'
      Required = True
      OnChange = GerarParcela
    end
    object cdsDataTIPO_PLANO_PAGAMENTO: TStringField
      DefaultExpression = 'CADASTRADO'
      DisplayLabel = 'Tipo do Plano de Pagamento'
      FieldName = 'TIPO_PLANO_PAGAMENTO'
      Origin = 'TIPO_PLANO_PAGAMENTO'
      OnChange = cdsDataTIPO_PLANO_PAGAMENTOChange
    end
    object cdsDataQT_PARCELAS: TIntegerField
      DisplayLabel = 'Qt. Parcelas'
      FieldName = 'QT_PARCELAS'
      Origin = 'QT_PARCELAS'
      OnChange = GerarParcela
    end
    object cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDatafdqGeracaoDocumentoParcela: TDataSetField
      FieldName = 'fdqGeracaoDocumentoParcela'
    end
    object cdsDataPARCELA_BASE: TIntegerField
      DisplayLabel = 'Parcela Inicial'
      FieldName = 'PARCELA_BASE'
      Origin = 'PARCELA_BASE'
      OnChange = GerarParcela
    end
    object cdsDataDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      OnChange = GerarParcela
    end
    object cdsDataDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
      OnChange = GerarParcela
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'd. Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Desc. Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataVL_ENTRADA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Entrada'
      FieldName = 'VL_ENTRADA'
      Origin = 'VL_ENTRADA'
      OnChange = cdsDataVL_ENTRADAChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataID_FORMA_PAGAMENTO_ENTRADA: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento da Entrada'
      FieldName = 'ID_FORMA_PAGAMENTO_ENTRADA'
      Origin = 'ID_FORMA_PAGAMENTO_ENTRADA'
      OnChange = cdsDataID_FORMA_PAGAMENTO_ENTRADAChange
    end
    object cdsDataID_CONTA_CORRENTE_ENTRADA: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente da Entrada'
      FieldName = 'ID_CONTA_CORRENTE_ENTRADA'
      Origin = 'ID_CONTA_CORRENTE_ENTRADA'
    end
    object cdsDataCHEQUE_SACADO_ENTRADA: TStringField
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO_ENTRADA'
      Origin = 'CHEQUE_SACADO_ENTRADA'
      Size = 255
    end
    object cdsDataCHEQUE_DOC_FEDERAL_ENTRADA: TStringField
      DisplayLabel = 'Cheque: Documento Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL_ENTRADA'
      Origin = 'CHEQUE_DOC_FEDERAL_ENTRADA'
      Size = 14
    end
    object cdsDataCHEQUE_BANCO_ENTRADA: TStringField
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO_ENTRADA'
      Origin = 'CHEQUE_BANCO_ENTRADA'
      Size = 100
    end
    object cdsDataCHEQUE_DT_EMISSAO_ENTRADA: TDateField
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO_ENTRADA'
      Origin = 'CHEQUE_DT_EMISSAO_ENTRADA'
    end
    object cdsDataCHEQUE_AGENCIA_ENTRADA: TStringField
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA_ENTRADA'
      Origin = 'CHEQUE_AGENCIA_ENTRADA'
      Size = 15
    end
    object cdsDataCHEQUE_CONTA_CORRENTE_ENTRADA: TStringField
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE_ENTRADA'
      Origin = 'CHEQUE_CONTA_CORRENTE_ENTRADA'
      Size = 15
    end
    object cdsDataCHEQUE_NUMERO_ENTRADA: TIntegerField
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO_ENTRADA'
      Origin = 'CHEQUE_NUMERO_ENTRADA'
    end
    object cdsDataCHEQUE_DT_VENCIMENTO_ENTRADA: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO_ENTRADA'
      Origin = 'CHEQUE_DT_VENCIMENTO_ENTRADA'
    end
    object cdsDataID_OPERADORA_CARTAO_ENTRADA: TIntegerField
      DisplayLabel = 'C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO_ENTRADA'
      Origin = 'ID_OPERADORA_CARTAO_ENTRADA'
    end
    object cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA: TStringField
      DisplayLabel = 'Forma de Pagamento Entrada'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA'
      Origin = 'JOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA: TStringField
      DisplayLabel = 'Conta Corrente da Entrada'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
      Origin = 'JOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA: TStringField
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA'
      Origin = 'JOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataCC_VL_PARCELAMENTO: TFloatField
      DisplayLabel = 'Vl. Parcelamento'
      FieldKind = fkCalculated
      FieldName = 'CC_VL_PARCELAMENTO'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      ReportDocument.CreationDate = 42210.571164918980000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsGeracaoDocumentoParcela: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqGeracaoDocumentoParcela
    Params = <>
    AfterPost = cdsGeracaoDocumentoParcelaAfterPost
    AfterDelete = cdsGeracaoDocumentoParcelaAfterDelete
    OnDeleteError = cdsDataDeleteError
    OnEditError = cdsDataEditError
    OnPostError = cdsDataPostError
    AfterApplyUpdates = cdsDataAfterApplyUpdates
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 752
    Top = 80
    object cdsGeracaoDocumentoParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsGeracaoDocumentoParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsGeracaoDocumentoParcelaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsGeracaoDocumentoParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsGeracaoDocumentoParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'Nr. Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaID_GERACAO_DOCUMENTO: TIntegerField
      DisplayLabel = 'Gera'#231#227'o de Documento'
      FieldName = 'ID_GERACAO_DOCUMENTO'
      Origin = 'ID_GERACAO_DOCUMENTO'
    end
    object cdsGeracaoDocumentoParcelaTOTAL_PARCELAS: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'TOTAL_PARCELAS'
      Origin = 'TOTAL_PARCELAS'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsGeracaoDocumentoParcelaDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      Required = True
    end
    object cdsGeracaoDocumentoParcelaDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
  end
  object dsGeracaoDocumentoParcela: TDataSource
    DataSet = cdsGeracaoDocumentoParcela
    Left = 782
    Top = 80
  end
  object pmAtualizarParcelas: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 880
    Top = 79
    object itemAtualizarParcelas: TMenuItem
      Caption = 'Atualizar Parcelas'
      ImageIndex = 111
      OnClick = itemAtualizarParcelasClick
    end
    object SalvarConfiguraes2: TMenuItem
      Action = ActSalvarConfigGrid
    end
  end
  object ActionListGrid: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 878
    Top = 32
    object ActSalvarConfigGrid: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
  end
end
