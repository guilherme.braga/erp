unit uCadEstacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxButtonEdit,
  cxDBEdit, uGBDBButtonEditFK, uGBDBTextEditPK, cxTextEdit, Vcl.StdCtrls,
  uGBDBTextEdit, System.Generics.Collections, Vcl.ComCtrls, Vcl.Buttons,
  cxDropDownEdit, cxBlobEdit, uGBDBBlobEdit;

type
  TCadEstacao = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataSISTEMA_OPERACIONAL: TStringField;
    cdsDataTV: TStringField;
    cdsDataENDERECO_IP: TStringField;
    cdsDataOBSERVACAO: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    cxDBTextEdit1: TgbDBTextEdit;
    Label3: TLabel;
    cxDBTextEdit2: TgbDBTextEdit;
    Label4: TLabel;
    cxDBTextEdit3: TgbDBTextEdit;
    Label5: TLabel;
    cxDBTextEdit4: TgbDBTextEdit;
    Label6: TLabel;
    Label7: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBBlobEdit1: TgbDBBlobEdit;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TCadEstacao);
finalization
  UnRegisterClass(TCadEstacao);

end.
