unit uFrmMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsDefaultPainters, cxLabel,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, ActnList, cxImage,
  StdCtrls, System.Actions, Vcl.Menus, cxButtons, Vcl.ImgList, cxClasses,
  clipbrd;

type
  TFrmMessage = class(TForm)
    ActionList: TActionList;
    ActConfirmar: TAction;
    ActCancelar: TAction;
    cxGroupBox4: TcxGroupBox;
    image: TJvTransparentButton;
    LbMessage: TLabel;
    cxGroupBox1: TcxGroupBox;
    BtnConfirmar: TcxButton;
    BtnCancelar: TcxButton;
    cxImageList48x48_Messages: TcxImageList;
    cxImage32x32: TcxImageList;
    cxDefaultEditStyleController1: TcxDefaultEditStyleController;
    procedure ActConfirmarExecute(Sender: TObject);
    procedure ActCancelarExecute(Sender: TObject);
    procedure cxGroupBox1DblClick(Sender: TObject);
  private
    nResult: Integer;
    procedure setDefauyltConfigurations(const typeForm: Integer; const text: string);
  public
    class function Question(const text: string): Integer;
    class procedure Sucess(const text: string);
    class procedure Failure(const text: string);
    class procedure Information(const text: string);
  end;

var
  FrmMessage: TFrmMessage;

const
  //Results
  MCONFIRMED: Integer = 1;
  MCANCELED:  Integer = 0;

  //Icons
  MINFORMATION: Integer = 3;
  MQUESTION:    Integer = 2;
  MFAILURE:     Integer = 1;
  MSUCESS:      Integer = 0;

implementation

{$R *.dfm}

procedure TFrmMessage.ActCancelarExecute(Sender: TObject);
begin
  nResult := MCANCELED;
  Close;
end;

procedure TFrmMessage.ActConfirmarExecute(Sender: TObject);
begin
  nResult := MCONFIRMED;
  Close;
end;

procedure TFrmMessage.cxGroupBox1DblClick(Sender: TObject);
begin
    Clipboard.AsText := LbMessage.Caption;
end;

class procedure TFrmMessage.failure(const text: string);
begin
  try
    Application.CreateForm(TFrmMessage, FrmMessage);
    FrmMessage.setDefauyltConfigurations(MFAILURE, text);
    FrmMessage.ShowModal;
  finally
    FreeAndNil(FrmMessage);
  end;
end;

class procedure TFrmMessage.information(const text: string);
begin
  try
    Application.CreateForm(TFrmMessage, FrmMessage);
    FrmMessage.setDefauyltConfigurations(MINFORMATION, text);
    FrmMessage.ShowModal;
  finally
    FreeAndNil(FrmMessage);
  end;
end;

class function TFrmMessage.question(const text: string): Integer;
begin
  try
    Application.CreateForm(TFrmMessage, FrmMessage);
    FrmMessage.setDefauyltConfigurations(MQUESTION, text);
    FrmMessage.ShowModal;
    result := FrmMessage.nResult;
  finally
    FreeAndNil(FrmMessage);
  end;
end;

class procedure TFrmMessage.sucess(const text: string);
begin
  try
    Application.CreateForm(TFrmMessage, FrmMessage);
    FrmMessage.setDefauyltConfigurations(MSUCESS, text);
    FrmMessage.ShowModal;
  finally
    FreeAndNil(FrmMessage);
  end;
end;

procedure TFrmMessage.setDefauyltConfigurations(const typeForm: Integer; const text: string);
var
  nInformation,
  nQuestion,
  nFailure,
  nSucess
    :Integer;

  cMessage, cTempMessage, cTempWords: String;

  i,j,nLinhas: Integer;

  function getInverseString(value: String): String;
  var
    retorno: String;
    i: Integer;
  begin
    for i := Length(value) downto 1 do
      retorno := retorno + value[i];
    result := retorno;
  end;

begin
  image.Images.ActiveIndex := typeForm;
 { cMessage := '';
  nLinhas := 2;
  for i := 1 to Length(text) do
    begin
      if (i = 50)   or (i = 100)  or (i = 150)  or (i = 200)  or
         (i = 300)  or (i = 350)  or (i = 400)  or (i = 450)  or
         (i = 500)  or (i = 550)  or (i = 600)  or (i = 650)  or
         (i = 700)  or (i = 750)  or (i = 800)  or (i = 900)  or
         (i = 950)  or (i = 1000) or (i = 1050) or (i = 1100) or
         (i = 1150) or (i = 1200) or (i = 1250) or (i = 1300) then
        begin
          cMessage := cMessage + cTempMessage;
          if (text[i] = ' ') then
            begin
              cMessage := cMessage + #13;
              J := I;
            end
          else
            begin
              cTempWords := '';
              for J := (I) downto 1 do
                begin
                  cTempWords := cTempWords + text[J];
                  cMessage := Copy(cMessage, 1, J);

                  if text[J-1] = ' ' then
                    begin
                      cMessage := Copy(cMessage, 1, J-1);
                      cMessage := cMessage + #13 +getInverseString(cTempWords);
                      break;
                    end;
                end;
            end;

          cTempMessage := '';
        end
      else
        cTempMessage := cTempMessage + text[i];
    end;

  cMessage := cMessage + cTempMessage;

  for i := 1 to Length(text) do
    if cMessage[i] = #13 then Inc(nLinhas);

  case nLinhas of
    24: Self.Height := 500;
    23: Self.Height := 480;
    22: Self.Height := 460;
    21: Self.Height := 440;
    20: Self.Height := 430;
    19: Self.Height := 415;
    18: Self.Height := 400;
    17: Self.Height := 380;
    16: Self.Height := 365;
    15: Self.Height := 350;
    14: Self.Height := 335;
    13: Self.Height := 320;
    12: Self.Height := 300;
    11: Self.Height := 300;
    10: Self.Height := 285;
    09: Self.Height := 260;
    08: Self.Height := 245;
    07: Self.Height := 230;
    06: Self.Height := 215;
    05: Self.Height := 200;
    04: Self.Height := 200;
    02,03:
      begin
        cMessage := #13+cMessage;
        Self.Height := 160;
      end;
  end;  }

  if (typeForm = MINFORMATION) or
     (typeForm = MQUESTION) then
     Self.Caption := 'Aten��o'
  else if (typeForm = MSUCESS) then
    Self.Caption := 'Processo Conclu�do'
  else if (typeForm = MFAILURE) then
    Self.Caption := 'Falha no Processo';

  ActCancelar.Visible := typeForm = MQUESTION;
  ActConfirmar.Visible := true;

  LbMessage.Caption := text;

  Self.AutoSize := true;
  Application.ProcessMessages;
end;

end.
