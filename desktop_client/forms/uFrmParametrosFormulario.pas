unit uFrmParametrosFormulario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDMemTable, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMemo, cxTextEdit, cxDBEdit, uGBDBMemo, Vcl.DBCtrls,
  cxCheckBox;

type
  TFrmParametrosFormulario = class(TFrmModalPadrao)
    dsColunas: TDataSource;
    cxgdMain: TcxGrid;
    cxgdMainDBTableView1: TcxGridDBTableView;
    cxgdMainLevel1: TcxGridLevel;
    fdmParametros: TgbFDMemTable;
    fdmParametrosPARAMETRO: TStringField;
    cxgdMainDBTableView1PARAMETRO: TcxGridDBColumn;
    cxgdMainDBTableView1VALOR: TcxGridDBColumn;
    fdmParametrosVALOR: TStringField;
    fdmParametrosDESCRICAO: TBlobField;
    DBMemo1: TDBMemo;
    fdmParametrosBO_OBRIGATORIO: TStringField;
    cxgdMainDBTableView1BO_OBRIGATORIO: TcxGridDBColumn;
  private
    FFormulario: String;
    FIdUsuarioSeguranca: Integer;
    procedure CarregarParametros;
    procedure SalvarParametros;
  public
    class procedure Parametrizar(AFormName: String; AIdUsuario: Integer);
  protected
    procedure Confirmar;override;
  end;

var
  FrmParametrosFormulario: TFrmParametrosFormulario;

implementation

{$R *.dfm}

uses uUsuarioDesignControl, uSistema;

{ TFrmParametrosFormulario }

procedure TFrmParametrosFormulario.CarregarParametros;
begin
  TUsuarioParametrosFormulario.BuscarParametros(
    FIdUsuarioSeguranca, FFormulario, fdmParametros);
end;

procedure TFrmParametrosFormulario.Confirmar;
begin
  inherited;
  SalvarParametros;
end;

class procedure TFrmParametrosFormulario.Parametrizar(AFormName: String;
  AIdUsuario: Integer);
begin
  Application.CreateForm(TFrmParametrosFormulario, FrmParametrosFormulario);
  try
    FrmParametrosFormulario.fdmParametros.Open;
    FrmParametrosFormulario.FFormulario := AFormName;
    FrmParametrosFormulario.FIdUsuarioSeguranca := AIdUsuario;
    FrmParametrosFormulario.CarregarParametros;
    FrmParametrosFormulario.ShowModal;
  finally
    FreeAndNil(FrmParametrosFormulario);
  end;
end;

procedure TFrmParametrosFormulario.SalvarParametros;
begin
  TUsuarioParametrosFormulario.SalvarParametros(
    FIdUsuarioSeguranca, FFormulario, fdmParametros);
end;

end.
