unit uFrmGerarProdutoGrade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, uGBPanel, uFrameProdutoGrade, FireDAC.Comp.DataSet, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.Client, Generics.Collections, uProdutoGradeProxy;

type
  TFrmGerarProdutoGrade = class(TFrmModalPadrao)
    PnFrameProdutoGrade: TgbPanel;
  private
    FFrameProdutoGrade: TFrameProdutoGrade;
    FIdProdutoAgrupador: Integer;
    procedure CarregarFrameProdutoGrade;
    procedure DimencionarFormulario;
  public
    class procedure GerarProdutoGrade(const AIdProdutoAgrupador: Integer);
    class function SelecionarProdutoGrade(const AIdProdutoAgrupador: Integer): TList<TProdutoGradeProxy>;
  protected
    procedure Confirmar; override;
    procedure Cancelar; override;
  end;

var
  FrmGerarProdutoGrade: TFrmGerarProdutoGrade;

implementation

{$R *.dfm}

uses uProduto, Rest.JSON, uFrmMessage;

{ TFrmGerarProdutoGrade }

procedure TFrmGerarProdutoGrade.Cancelar;
begin
  inherited;

end;

procedure TFrmGerarProdutoGrade.CarregarFrameProdutoGrade;
begin
  FFrameProdutoGrade := TFrameProdutoGrade.Create(PnFrameProdutoGrade);
  FFrameProdutoGrade.Parent := PnFrameProdutoGrade;
  FFrameProdutoGrade.Align := alClient;
end;

procedure TFrmGerarProdutoGrade.Confirmar;
var
  jsonProdutosGrade: String;
begin
  ActiveControl := nil;
  case FFrameProdutoGrade.FModo of
    modoInclusao:
    begin
      if not FFrameProdutoGrade.ExisteRegistro then
      begin
        TFrmMessage.Information('N�o existem registros para a gera��o de novos produtos!');
        abort;
      end;

      jsonProdutosGrade := FFrameProdutoGrade.ProdutosEmJSON;
      TProduto.GerarProdutos(jsonProdutosGrade);
      TFrmMessage.Information('Produtos gerados com sucesso!');
    end;
    modoConsulta:
    begin
      if not FFrameProdutoGrade.ExisteRegistrosValorados then
      begin
        TFrmMessage.Information('N�o existem quantidades informadas!');
        abort;
      end;
    end;
  end;

  inherited;
end;

procedure TFrmGerarProdutoGrade.DimencionarFormulario;
begin
  Self.Height := 600;
  Self.Width := 800;
end;

class procedure TFrmGerarProdutoGrade.GerarProdutoGrade(const AIdProdutoAgrupador: Integer);
begin
  try
    Application.CreateForm(TFrmGerarProdutoGrade, FrmGerarProdutoGrade);
    FrmGerarProdutoGrade.FIdProdutoAgrupador := AIdProdutoAgrupador;
    FrmGerarProdutoGrade.CarregarFrameProdutoGrade;
    FrmGerarProdutoGrade.FFrameProdutoGrade.ModoDeInclusao(AIdProdutoAgrupador);
    FrmGerarProdutoGrade.DimencionarFormulario;
    FrmGerarProdutoGrade.ShowModal;
  finally
    FreeAndNil(FrmGerarProdutoGrade);
  end;
end;

class function TFrmGerarProdutoGrade.SelecionarProdutoGrade(
  const AIdProdutoAgrupador: Integer): TList<TProdutoGradeProxy>;
var listaProdutoGrade: TList<TProdutoGradeProxy>;
begin
  try
    Application.CreateForm(TFrmGerarProdutoGrade, FrmGerarProdutoGrade);
    FrmGerarProdutoGrade.FIdProdutoAgrupador := AIdProdutoAgrupador;
    FrmGerarProdutoGrade.CarregarFrameProdutoGrade;
    FrmGerarProdutoGrade.FFrameProdutoGrade.ModoDeConsulta(AIdProdutoAgrupador);
    FrmGerarProdutoGrade.DimencionarFormulario;
    FrmGerarProdutoGrade.ShowModal;

    if FrmGerarProdutoGrade.Result = MCONFIRMED then
    begin
      listaProdutoGrade := TList<TProdutoGradeProxy>.Create;
      listaProdutoGrade := TJSON.JsonToObject<TList<TProdutoGradeProxy>>(
        FrmGerarProdutoGrade.FFrameProdutoGrade.ProdutosEmJSON);
    end;

    result := listaProdutoGrade;
  finally
    FreeAndNil(FrmGerarProdutoGrade);
  end;
end;

end.
