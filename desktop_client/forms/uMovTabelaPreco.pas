unit uMovTabelaPreco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxTextEdit,
  cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK, uFrameDetailPadrao, uFrameTabelaPrecoItem, uGBPanel,
  JvExControls, JvButton, JvTransparentButton, cxDropDownEdit, cxCalc,
  uGBDBCalcEdit, uGBGroupBox, uGBFDMemTable, Vcl.Buttons, JvExButtons, JvBitBtn,
  cxLabel, cxButtons, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, Generics.Collections, cxSplitter;

type
  TMovTabelaPreco = class(TFrmCadastro_Padrao)
    lCodigo: TLabel;
    EdPkCodigo: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDatafdqTabelaPrecoItem: TDataSetField;
    cdsDataJOIN_DESCRICAO_FILIAL: TStringField;
    cdsTabelaPrecoItem: TGBClientDataSet;
    cdsTabelaPrecoItemID: TAutoIncField;
    cdsTabelaPrecoItemID_PRODUTO: TIntegerField;
    cdsTabelaPrecoItemID_TABELA_PRECO: TIntegerField;
    cdsTabelaPrecoItemPERC_LUCRO: TFMTBCDField;
    cdsTabelaPrecoItemVL_VENDA: TFMTBCDField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_PRODUTO: TStringField;
    gbPanel1: TgbPanel;
    lDescricao: TLabel;
    edDescricao: TgbDBTextEdit;
    lFilial: TLabel;
    EdFkFilial: TgbDBButtonEditFK;
    descFilial: TgbDBTextEdit;
    cdsTabelaPrecoItemtipo: TStringField;
    cdsTabelaPrecoItemid_grupo_produto: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
    cdsTabelaPrecoItemid_sub_grupo_produto: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_SUB_GRUPO_PRODUTO: TStringField;
    cdsTabelaPrecoItemid_categoria: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_CATEGORIA: TStringField;
    cdsTabelaPrecoItemid_sub_categoria: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_SUB_CATEGORIA: TStringField;
    cdsTabelaPrecoItemid_modelo: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_MODELO: TStringField;
    cdsTabelaPrecoItemid_unidade_estoque: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_UNIDADE_ESTOQUE: TStringField;
    cdsTabelaPrecoItemid_marca: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_MARCA: TStringField;
    cdsTabelaPrecoItemid_linha: TIntegerField;
    cdsTabelaPrecoItemJOIN_DESCRICAO_LINHA: TStringField;
    gpAlteracao: TgbGroupBox;
    edtPercLucro: TgbDBCalcEdit;
    edtVlVenda: TgbDBCalcEdit;
    labelPercLucro: TLabel;
    labelVlVenda: TLabel;
    bevelInformacoes: TdxBevel;
    fdmValoresAlteracao: TgbFDMemTable;
    dsValoresAlteracao: TDataSource;
    fdmValoresAlteracaoVL_CUSTO: TFloatField;
    fdmValoresAlteracaoVL_CUSTO_OPERACIONAL: TFloatField;
    fdmValoresAlteracaoPERC_LUCRO: TFloatField;
    fdmValoresAlteracaoVL_VENDA: TFloatField;
    cxButton1: TcxButton;
    labelAjuda: TLabel;
    labelPercReajuste: TLabel;
    edtPercReajuste: TgbDBCalcEdit;
    fdmValoresAlteracaoPERC_REAJUSTE: TFloatField;
    FrameTabelaPrecoItem1: TFrameTabelaPrecoItem;
    alTabelaPreco: TActionList;
    ActAplicarAlteracoes: TAction;
    cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL: TFMTBCDField;
    procedure cdsTabelaPrecoItemAfterOpen(DataSet: TDataSet);
    procedure CalcularValor(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure actAplicarExecute(Sender: TObject);
    procedure CalcularCamposAlteracaoEmLote(Sender: TField);
    procedure cxButton2Click(Sender: TObject);

  private
    FCalculandoValores : Boolean;
    FCalculandoValoresAlteracao : Boolean;
    procedure HabilitarCamposValoresAlteracao;
    procedure AplicarAlteracaoAosItens;


  protected
    procedure GerenciarControles;override;
    procedure AoExcluir; override;

  public
    procedure InserirProdutos(AProdutos: string);
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uMathUtils, uPesqProduto, uProduto, uDatasetUtils, uControlsUtils,
  uTMessage, uFrmMessage_Process, uDmAcesso, uProdutoProxy, uTabelaPreco;

procedure TMovTabelaPreco.cdsTabelaPrecoItemAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FrameTabelaPrecoItem1.SetConfiguracoesIniciais(cdsTabelaPrecoItem);
end;

procedure TMovTabelaPreco.cxButton2Click(Sender: TObject);
var
  IDs: string;
begin
  inherited;
  IDs := TPesqProduto.BuscarProdutos;
  if IDs <> EmptyStr then
    InserirProdutos(IDs);
end;

procedure TMovTabelaPreco.InserirProdutos(AProdutos: string);
var
  produtos: TList<TprodutoProxy>;
  produto: TProdutoProxy;
  i: Integer;
begin
  try
    TFrmMessage_Process.SendMessage('Inserindo produtos');
    cdsTabelaPrecoItem.DisableControls;
    cdsTabelaPrecoItem.GuardarBookmark;
    produtos := TProduto.GetListaProduto(AProdutos, cdsDataID_FILIAL.AsInteger);
    try
      for i := 0 to Pred(produtos.Count) do
      begin
        produto := produtos[i];

        if cdsTabelaPrecoItem.Locate('ID_PRODUTO', produto.FId, []) then
        begin
          continue;
        end;

        cdsTabelaPrecoItem.Incluir;

        cdsTabelaPrecoItemID_PRODUTO.AsFloat := produto.FId;
        cdsTabelaPrecoItemJOIN_DESCRICAO_PRODUTO.AsString := produto.FDescricao;
        cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat :=
          produto.FProdutoFilial.FVlCustoImpostoOperacional;
        cdsTabelaPrecoItemVL_VENDA.AsFloat                := 0;
        cdsTabelaPrecoItemPERC_LUCRO.AsFloat              := 0;
        cdsTabelaPrecoItem.Salvar;
      end;
    finally
      FreeAndNil(produtos);
      cdsTabelaPrecoItem.First;
    end;
  finally
    cdsTabelaPrecoItem.PosicionarBookmark;
    cdsTabelaPrecoItem.EnableControls;
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovTabelaPreco.FormCreate(Sender: TObject);
begin
  inherited;
  FCalculandoValores          := False;
  FCalculandoValoresAlteracao := False;
end;

procedure TMovTabelaPreco.GerenciarControles;
begin
  inherited;
  HabilitarCamposValoresAlteracao;
end;

procedure TMovTabelaPreco.actAplicarExecute(Sender: TObject);
begin
  inherited;
  AplicarAlteracaoAosItens;
  HabilitarCamposValoresAlteracao;
end;

procedure TMovTabelaPreco.AoExcluir;
begin
  //inherited;
  TTabelaPreco.ExcluirTabelaDePreco(cdsData.FieldByName('ID').AsInteger);
  fdmSearch.Delete;
end;

procedure TMovTabelaPreco.AplicarAlteracaoAosItens;
var
  i: Integer;
begin
  if cdsTabelaPrecoItem.IsEmpty then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Aplicando altera��es');

    TControlsUtils.CongelarFormulario(Self);
    cdsTabelaPrecoItem.DisableControls;
    cdsTabelaPrecoItem.GuardarBookMark;

    for i := 0 to FrameTabelaPrecoItem1.Level1BandedTableView1.DataController.FilteredRecordCount - 1 do
    begin
      (cdsTabelaPrecoItem as TDataSet).RecNo :=  FrameTabelaPrecoItem1.Level1BandedTableView1.DataController.FilteredRecordIndex[i] + 1;

      cdsTabelaPrecoItem.Alterar;

      if fdmValoresAlteracaoPERC_LUCRO.AsString <> EmptyStr then
      begin
        cdsTabelaPrecoItemPERC_LUCRO.AsFloat := fdmValoresAlteracaoPERC_LUCRO.AsFloat;
      end;

      if fdmValoresAlteracaoVL_VENDA.AsString <> EmptyStr then
      begin
        cdsTabelaPrecoItemVL_VENDA.AsFloat := fdmValoresAlteracaoVL_VENDA.AsFloat;
      end;

      if fdmValoresAlteracaoPERC_REAJUSTE.AsString <> EmptyStr then
      begin
        cdsTabelaPrecoItemVL_VENDA.AsFloat :=
          cdsTabelaPrecoItemVL_VENDA.AsFloat
          + TMathUtils.ValorSobrePercentual(fdmValoresAlteracaoPERC_REAJUSTE.AsFloat
                                                                             ,cdsTabelaPrecoItemVL_VENDA.AsFloat)
      end;

      cdsTabelaPrecoItem.Salvar;
    end;

    cdsTabelaPrecoItem.First;
  finally
    cdsTabelaPrecoItem.EnableControls;
    cdsTabelaPrecoItem.PosicionarBookmark;
    TControlsUtils.DescongelarFormulario;
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovTabelaPreco.CalcularValor(Sender: TField);
var
  vlCusto : Double;
begin
  if FCalculandoValores then
    Exit;

  FCalculandoValores := True;

  if Sender = cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL then
  begin
    vlCusto := cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat;
    cdsTabelaPrecoItemVL_VENDA.AsFloat := vlCusto
                                        + TMathUtils.ValorSobrePercentual(cdsTabelaPrecoItemPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = cdsTabelaPrecoItemPERC_LUCRO then
  begin
    vlCusto := cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat;
    cdsTabelaPrecoItemVL_VENDA.AsFloat := vlCusto
                                        + TMathUtils.ValorSobrePercentual(cdsTabelaPrecoItemPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = cdsTabelaPrecoItemVL_VENDA then
  begin
    vlCusto := cdsTabelaPrecoItemVL_CUSTO_IMPOSTO_OPERACIONAL.AsFloat;
    if vlCusto > 0 then
      cdsTabelaPrecoItemPERC_LUCRO.AsFloat := (cdsTabelaPrecoItemVL_VENDA.AsFloat - vlCusto)/vlCusto * 100;
  end;

  FCalculandoValores := False;
end;

procedure TMovTabelaPreco.CalcularCamposAlteracaoEmLote(Sender: TField);
var
  vlCusto : Double;
begin

  Exit;
  // N�o utilizado temporariamente
  // Dever� ser chamado no evento onchange dos valores do fdmValoresAlteracao

  if FCalculandoValoresAlteracao then
    Exit;

  FCalculandoValoresAlteracao := True;

  if Sender = fdmValoresAlteracaoVL_CUSTO then
  begin
    vlCusto := fdmValoresAlteracaoVL_CUSTO.AsFloat + fdmValoresAlteracaoVL_CUSTO_OPERACIONAL.AsFloat;
    fdmValoresAlteracaoVL_VENDA.AsFloat := vlCusto
                                        + TMathUtils.ValorSobrePercentual(fdmValoresAlteracaoPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = fdmValoresAlteracaoVL_CUSTO_OPERACIONAL then
  begin
    vlCusto := fdmValoresAlteracaoVL_CUSTO.AsFloat + fdmValoresAlteracaoVL_CUSTO_OPERACIONAL.AsFloat;
    fdmValoresAlteracaoVL_VENDA.AsFloat := vlCusto
                                        + TMathUtils.ValorSobrePercentual(fdmValoresAlteracaoPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = fdmValoresAlteracaoPERC_LUCRO then
  begin
    vlCusto := fdmValoresAlteracaoVL_CUSTO.AsFloat + fdmValoresAlteracaoVL_CUSTO_OPERACIONAL.AsFloat;
    fdmValoresAlteracaoVL_VENDA.AsFloat := vlCusto
                                        + TMathUtils.ValorSobrePercentual(fdmValoresAlteracaoPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = fdmValoresAlteracaoVL_VENDA then
  begin
    vlCusto := fdmValoresAlteracaoVL_CUSTO.AsFloat + fdmValoresAlteracaoVL_CUSTO_OPERACIONAL.AsFloat;
    if vlCusto > 0 then
      fdmValoresAlteracaoPERC_LUCRO.AsFloat := (fdmValoresAlteracaoVL_VENDA.AsFloat - vlCusto)/vlCusto * 100;
  end;

  FCalculandoValoresAlteracao := False;
end;

procedure TMovTabelaPreco.HabilitarCamposValoresAlteracao;
begin

  if not fdmValoresAlteracao.Active then
  begin
    fdmValoresAlteracao.open;
  end;

  if fdmValoresAlteracao.State in dsEditModes then
  begin
    fdmValoresAlteracao.Post;
  end;

  fdmValoresAlteracao.EmptyDataSet;
  fdmValoresAlteracao.Append;
end;

initialization
  RegisterClass(TMovTabelaPreco);

finalization
  UnRegisterClass(TMovTabelaPreco);

end.
