inherited CadPlanoConta: TCadPlanoConta
  Caption = 'Cadastro de Plano de Conta'
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 113
            Top = 9
            Width = 99
            Height = 13
            Caption = 'Centro de Resultado'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 45
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object TreeListPlanoConta: TcxDBTreeList
            AlignWithMargins = True
            Left = 5
            Top = 37
            Width = 926
            Height = 229
            Align = alClient
            Bands = <
              item
              end>
            DataController.DataSource = dsPlanoContaItem
            DataController.ImageIndexField = 'BO_HABILITADO'
            DataController.ParentField = 'ID_ASSOCIACAO'
            DataController.KeyField = 'ID'
            DataController.StateIndexField = 'BO_HABILITADO'
            Images = DmAcesso.cxImage16x16
            LookAndFeel.Kind = lfOffice11
            Navigator.Buttons.CustomButtons = <>
            OptionsCustomizing.BandCustomizing = False
            OptionsCustomizing.BandHorzSizing = False
            OptionsCustomizing.BandMoving = False
            OptionsCustomizing.BandVertSizing = False
            OptionsData.CancelOnExit = False
            OptionsData.Editing = False
            OptionsData.Deleting = False
            OptionsData.CheckHasChildren = False
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRect = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.DynamicIndent = True
            OptionsView.TreeLineStyle = tllsSolid
            PopupMenu = ViewPopUpMenu
            RootValue = -1
            TabOrder = 1
            object cxDBTreeList1SEQUENCIA: TcxDBTreeListColumn
              Caption.AlignVert = vaTop
              DataBinding.FieldName = 'SEQUENCIA'
              Width = 88
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeList1DESCRICAO: TcxDBTreeListColumn
              Caption.AlignVert = vaTop
              DataBinding.FieldName = 'DESCRICAO'
              Width = 809
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object ColumnIcon: TcxDBTreeListColumn
              PropertiesClassName = 'TcxImageProperties'
              DataBinding.FieldName = 'BO_HABILITADO'
              Width = 27
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 216
            Top = 5
            DataBinding.DataField = 'ID_CENTRO_RESULTADO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 2
            OnEnter = gbDBButtonEditFK1Enter
            gbTextEdit = gbDBTextEdit2
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_CENTRO_RESULTADO'
            gbTableName = 'CENTRO_RESULTADO'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 60
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 273
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_CENTRO_RESULTADO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 658
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    object ActHabilitarConta: TAction
      Category = 'filter'
      Caption = 'Habilitar'
      Hint = 'Habilitar esta conta para este plano de conta'
      ImageIndex = 19
      ShortCut = 16397
      OnExecute = ExecutarAcaoNaVisao
    end
    object ActDesabilitarConta: TAction
      Category = 'filter'
      Caption = 'Desabilitar'
      Hint = 'Desabilitar esta conta deste plano de conta'
      ImageIndex = 16
      ShortCut = 16430
      OnExecute = ExecutarAcaoNaVisao
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Plano de Conta'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspPlanoConta'
    RemoteServer = DmConnection.dspFinanceiro
    AfterOpen = cdsDataAfterOpen
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataJOIN_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDatafdqPlanoContaItem: TDataSetField
      FieldName = 'fdqPlanoContaItem'
    end
  end
  object fdmVisaoPlanoConta: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 784
    Top = 80
    object fdmVisaoPlanoContaID: TIntegerField
      FieldName = 'ID'
    end
    object fdmVisaoPlanoContaSEQUENCIA: TStringField
      FieldName = 'SEQUENCIA'
    end
    object fdmVisaoPlanoContaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object fdmVisaoPlanoContaNIVEL: TIntegerField
      FieldName = 'NIVEL'
    end
    object fdmVisaoPlanoContaID_ASSOCIACAO: TIntegerField
      FieldName = 'ID_ASSOCIACAO'
    end
    object fdmVisaoPlanoContaBO_HABILITADO: TIntegerField
      FieldName = 'BO_HABILITADO'
    end
  end
  object dsPlanoContaItem: TDataSource
    DataSet = fdmVisaoPlanoConta
    Left = 812
    Top = 80
  end
  object cdsPlanoContaItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPlanoContaItem
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 632
    Top = 80
    object cdsPlanoContaItemID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsPlanoContaItemID_CONTA_ANALISE: TIntegerField
      FieldName = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsPlanoContaItemID_PLANO_CONTA: TIntegerField
      FieldName = 'ID_PLANO_CONTA'
    end
  end
  object ViewPopUpMenu: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 360
    Top = 232
    object Habilitar1: TMenuItem
      Action = ActHabilitarConta
      SubMenuImages = DmAcesso.cxImage16x16
      ImageIndex = 18
      ShortCut = 16429
    end
    object Desabilitar1: TMenuItem
      Action = ActDesabilitarConta
      ImageIndex = 17
    end
  end
end
