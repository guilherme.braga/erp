inherited CadRegraImposto: TCadRegraImposto
  Caption = 'Cadastro de Regra de Imposto'
  ClientHeight = 459
  ClientWidth = 764
  ExplicitWidth = 780
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 764
    ExplicitWidth = 764
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 764
    Height = 332
    Properties.ActivePage = cxtsData
    ExplicitWidth = 764
    ExplicitHeight = 332
    ClientRectBottom = 332
    ClientRectRight = 764
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 764
      ExplicitHeight = 308
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 732
        Height = 308
        ExplicitWidth = 732
        ExplicitHeight = 308
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 308
        Height = 308
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 308
        ExplicitHeight = 308
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 764
      ExplicitHeight = 308
      inherited DesignPanel: TJvDesignPanel
        Width = 764
        Height = 308
        ExplicitWidth = 764
        ExplicitHeight = 308
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 764
          ExplicitHeight = 308
          Height = 308
          Width = 764
          inherited dxBevel1: TdxBevel
            Width = 760
            ExplicitWidth = 760
          end
          object Label2: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 63
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            DesignSize = (
              760
              55)
            Height = 55
            Width = 760
            object Label1: TLabel
              Left = 8
              Top = 8
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object Label3: TLabel
              Left = 8
              Top = 34
              Width = 22
              Height = 13
              Caption = 'NCM'
            end
            object Label4: TLabel
              Left = 226
              Top = 34
              Width = 88
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'NCM Especializado'
            end
            object Label5: TLabel
              Left = 588
              Top = 34
              Width = 72
              Height = 13
              Anchors = [akTop, akRight]
              Caption = 'Exce'#231#227'o do IPI'
            end
            object edDescricao: TgbDBTextEdit
              Left = 61
              Top = 5
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 691
            end
            object gbDBTextEdit1: TgbDBTextEdit
              Left = 160
              Top = 30
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_NCM'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbReadyOnly = True
              gbPassword = False
              Width = 63
            end
            object gbDBButtonEditFK1: TgbDBButtonEditFK
              Left = 61
              Top = 30
              DataBinding.DataField = 'JOIN_CODIGO_NCM'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClearKey = 16430
              Properties.ClickKey = 112
              Style.Color = clWhite
              TabOrder = 1
              gbTextEdit = gbDBTextEdit1
              gbCampoPK = 'CODIGO'
              gbCamposRetorno = 'ID_NCM;JOIN_CODIGO_NCM;JOIN_DESCRICAO_NCM'
              gbTableName = 'NCM'
              gbCamposConsulta = 'ID;CODIGO;DESCRICAO'
              gbIdentificadorConsulta = 'NCM'
              Width = 102
            end
            object gbDBTextEdit2: TgbDBTextEdit
              Left = 376
              Top = 30
              TabStop = False
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbReadyOnly = True
              gbPassword = False
              Width = 207
            end
            object gbDBButtonEditFK2: TgbDBButtonEditFK
              Left = 319
              Top = 30
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'ID_NCM_ESPECIALIZADO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClearKey = 16430
              Properties.ClickKey = 112
              Style.Color = clWhite
              TabOrder = 3
              gbTextEdit = gbDBTextEdit2
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_NCM_ESPECIALIZADO;JOIN_DESCRICAO_NCM_ESPECIALIZADO'
              gbTableName = 'NCM_ESPECIALIZADO'
              gbCamposConsulta = 'ID;DESCRICAO'
              gbIdentificadorConsulta = 'NCM_ESPECIALIZADO'
              Width = 60
            end
            object gbDBCalcEdit1: TgbDBCalcEdit
              Left = 666
              Top = 30
              Anchors = [akTop, akRight]
              DataBinding.DataField = 'EXCECAO_IPI'
              DataBinding.DataSource = dsData
              Properties.DisplayFormat = '###,###,###,###,###,##0.00'
              Properties.ImmediatePost = True
              Properties.UseThousandSeparator = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 5
              Width = 86
            end
          end
          object PnFrameRegraImpostoFiltro: TgbPanel
            Left = 2
            Top = 89
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameRegraImpostoFiltro'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Height = 217
            Width = 760
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspRegraImposto'
    RemoteServer = DmConnection.dspRegraImposto
    AfterOpen = cdsDataAfterOpen
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataID_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM'
      FieldName = 'ID_NCM'
      Origin = 'ID_NCM'
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataEXCECAO_IPI: TIntegerField
      DisplayLabel = 'Exce'#231#227'o IPI'
      FieldName = 'EXCECAO_IPI'
      Origin = 'EXCECAO_IPI'
    end
    object cdsDataID_NCM_ESPECIALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do NCM Especializado'
      FieldName = 'ID_NCM_ESPECIALIZADO'
      Origin = 'ID_NCM_ESPECIALIZADO'
    end
    object cdsDataJOIN_DESCRICAO_NCM: TStringField
      DisplayLabel = 'NCM'
      FieldName = 'JOIN_DESCRICAO_NCM'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_NCM_ESPECIALIZADO: TStringField
      DisplayLabel = 'NCM Especializado'
      FieldName = 'JOIN_DESCRICAO_NCM_ESPECIALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CODIGO_NCM: TIntegerField
      DisplayLabel = 'C'#243'digo NCM'
      FieldName = 'JOIN_CODIGO_NCM'
      Origin = 'CODIGO'
      ProviderFlags = []
    end
    object cdsDatafdqRegraImpostoFiltro: TDataSetField
      FieldName = 'fdqRegraImpostoFiltro'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.927839375000000000
      BuiltInReportLink = True
    end
  end
  object cdsRegraImpostoFiltro: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqRegraImpostoFiltro
    Params = <>
    OnNewRecord = cdsRegraImpostoFiltroNewRecord
    gbUsarDefaultExpression = False
    gbValidarCamposObrigatorios = True
    Left = 664
    Top = 80
    object cdsRegraImpostoFiltroID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsRegraImpostoFiltroTIPO_EMPRESA_PESSOA: TStringField
      DisplayLabel = 'Tipo de Empresa'
      FieldName = 'TIPO_EMPRESA_PESSOA'
      Origin = 'TIPO_EMPRESA_PESSOA'
      Size = 15
    end
    object cdsRegraImpostoFiltroFORMA_AQUISICAO: TStringField
      DisplayLabel = 'Forma de Aquisi'#231#227'o'
      FieldName = 'FORMA_AQUISICAO'
      Origin = 'FORMA_AQUISICAO'
      Size = 45
    end
    object cdsRegraImpostoFiltroREGIME_TRIBUTARIO_DESTINATARIO: TIntegerField
      DisplayLabel = 'Regime Tribut'#225'rio do Destinat'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO_DESTINATARIO'
      Origin = 'REGIME_TRIBUTARIO_DESTINATARIO'
    end
    object cdsRegraImpostoFiltroREGIME_TRIBUTARIO_EMITENTE: TIntegerField
      DisplayLabel = 'Regime Tribut'#225'rio do Emitente'
      FieldName = 'REGIME_TRIBUTARIO_EMITENTE'
      Origin = 'REGIME_TRIBUTARIO_EMITENTE'
    end
    object cdsRegraImpostoFiltroCRT_EMITENTE: TIntegerField
      DisplayLabel = 'CRT Emitente'
      FieldName = 'CRT_EMITENTE'
      Origin = 'CRT_EMITENTE'
    end
    object cdsRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_EMITENTE: TStringField
      DisplayLabel = 'Contribuinte do ICMS Emitente'
      FieldName = 'BO_CONTRIBUINTE_ICMS_EMITENTE'
      Origin = 'BO_CONTRIBUINTE_ICMS_EMITENTE'
      FixedChar = True
      Size = 1
    end
    object cdsRegraImpostoFiltroBO_CONTRIBUINTE_IPI_EMITENTE: TStringField
      DisplayLabel = 'Contribuinte do IPI Emitente'
      FieldName = 'BO_CONTRIBUINTE_IPI_EMITENTE'
      Origin = 'BO_CONTRIBUINTE_IPI_EMITENTE'
      FixedChar = True
      Size = 1
    end
    object cdsRegraImpostoFiltroBO_CONTRIBUINTE_ICMS_DESTINATARIO: TStringField
      DisplayLabel = 'Contribuinte do ICMS Destinat'#225'rio'
      FieldName = 'BO_CONTRIBUINTE_ICMS_DESTINATARIO'
      Origin = 'BO_CONTRIBUINTE_ICMS_DESTINATARIO'
      FixedChar = True
      Size = 1
    end
    object cdsRegraImpostoFiltroBO_CONTRIBUINTE_IPI_DESTINATARIO: TStringField
      DisplayLabel = 'Contribuinte do IPI Destinat'#225'rio'
      FieldName = 'BO_CONTRIBUINTE_IPI_DESTINATARIO'
      Origin = 'BO_CONTRIBUINTE_IPI_DESTINATARIO'
      FixedChar = True
      Size = 1
    end
    object cdsRegraImpostoFiltroID_ZONEAMENTO_EMITENTE: TIntegerField
      DisplayLabel = 'Zoneamento do Emitente'
      FieldName = 'ID_ZONEAMENTO_EMITENTE'
      Origin = 'ID_ZONEAMENTO_EMITENTE'
    end
    object cdsRegraImpostoFiltroID_ZONEAMENTO_DESTINATARIO: TIntegerField
      DisplayLabel = 'Zoneamento do Destinat'#225'rio'
      FieldName = 'ID_ZONEAMENTO_DESTINATARIO'
      Origin = 'ID_ZONEAMENTO_DESTINATARIO'
    end
    object cdsRegraImpostoFiltroID_REGIME_ESPECIAL_EMITENTE: TIntegerField
      DisplayLabel = 'C'#243'digo do Regime Especial do Emitente'
      FieldName = 'ID_REGIME_ESPECIAL_EMITENTE'
      Origin = 'ID_REGIME_ESPECIAL_EMITENTE'
    end
    object cdsRegraImpostoFiltroID_REGIME_ESPECIAL_DESTINATARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Regime Especial do Destinat'#225'rio'
      FieldName = 'ID_REGIME_ESPECIAL_DESTINATARIO'
      Origin = 'ID_REGIME_ESPECIAL_DESTINATARIO'
    end
    object cdsRegraImpostoFiltroID_SITUACAO_ESPECIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Situa'#231#227'o Especial'
      FieldName = 'ID_SITUACAO_ESPECIAL'
      Origin = 'ID_SITUACAO_ESPECIAL'
    end
    object cdsRegraImpostoFiltroID_REGRA_IMPOSTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Regra de Imposto'
      FieldName = 'ID_REGRA_IMPOSTO'
      Origin = 'ID_REGRA_IMPOSTO'
    end
    object cdsRegraImpostoFiltroID_ESTADO_ORIGEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado de Origem'
      FieldName = 'ID_ESTADO_ORIGEM'
      Origin = 'ID_ESTADO_ORIGEM'
    end
    object cdsRegraImpostoFiltroID_ESTADO_DESTINO: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado de Destino'
      FieldName = 'ID_ESTADO_DESTINO'
      Origin = 'ID_ESTADO_DESTINO'
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_EMITENTE: TStringField
      DisplayLabel = 'Zoneamento do Emitente'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
      Origin = 'JOIN_DESCRICAO_ZONEAMENTO_EMITENTE'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO: TStringField
      DisplayLabel = 'Zoneamento do Destinat'#225'rio'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
      Origin = 'JOIN_DESCRICAO_ZONEAMENTO_DESTINATARIO'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE: TStringField
      DisplayLabel = 'Regime Especial do Emitente'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
      Origin = 'JOIN_DESCRICAO_REGIME_ESPECIAL_EMITENTE'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO: TStringField
      DisplayLabel = 'Regime Especial do Destinat'#225'rio'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
      Origin = 'JOIN_DESCRICAO_REGIME_ESPECIAL_DESTINATARIO'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_SITUACAO_ESPECIAL: TStringField
      DisplayLabel = 'Situa'#231#227'o Especial'
      FieldName = 'JOIN_DESCRICAO_SITUACAO_ESPECIAL'
      Origin = 'JOIN_DESCRICAO_SITUACAO_ESPECIAL'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_UF_ESTADO_ORIGEM: TStringField
      DisplayLabel = 'Estado de Origem'
      FieldName = 'JOIN_UF_ESTADO_ORIGEM'
      Origin = 'JOIN_UF_ESTADO_ORIGEM'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroJOIN_UF_ESTADO_DESTINO: TStringField
      DisplayLabel = 'Estado de Destino'
      FieldName = 'JOIN_UF_ESTADO_DESTINO'
      Origin = 'JOIN_UF_ESTADO_DESTINO'
      ProviderFlags = []
      Size = 255
    end
    object cdsRegraImpostoFiltroDH_INICIO_VIGENCIA: TDateTimeField
      DisplayLabel = 'Dh do In'#237'cio da Vig'#234'ncia'
      FieldName = 'DH_INICIO_VIGENCIA'
      Origin = 'DH_INICIO_VIGENCIA'
      Required = True
    end
    object cdsRegraImpostoFiltroDH_FIM_VIGENCIA: TDateTimeField
      DisplayLabel = 'Dh do T'#233'rmino da Vig'#234'ncia'
      FieldName = 'DH_FIM_VIGENCIA'
      Origin = 'DH_FIM_VIGENCIA'
    end
    object cdsRegraImpostoFiltroORIGEM_DA_MERCADORIA: TIntegerField
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'ORIGEM_DA_MERCADORIA'
      Origin = 'ORIGEM_DA_MERCADORIA'
    end
    object cdsRegraImpostoFiltroID_IMPOSTO_PIS_COFINS: TIntegerField
      DisplayLabel = 'Imposto PIS Cofins'
      FieldName = 'ID_IMPOSTO_PIS_COFINS'
      Origin = 'ID_IMPOSTO_PIS_COFINS'
    end
    object cdsRegraImpostoFiltroID_IMPOSTO_ICMS: TIntegerField
      DisplayLabel = 'Imposto ICMS'
      FieldName = 'ID_IMPOSTO_ICMS'
      Origin = 'ID_IMPOSTO_ICMS'
    end
    object cdsRegraImpostoFiltroID_IMPOSTO_IPI: TIntegerField
      DisplayLabel = 'Imposto IPI'
      FieldName = 'ID_IMPOSTO_IPI'
      Origin = 'ID_IMPOSTO_IPI'
    end
    object cdsRegraImpostoFiltroJOIN_CST_CSOSN: TStringField
      FieldName = 'JOIN_CST_CSOSN'
      Origin = 'JOIN_CST_CSOSN'
      ProviderFlags = []
      Size = 5
    end
    object cdsRegraImpostoFiltroJOIN_CST_IPI: TStringField
      FieldName = 'JOIN_CST_IPI'
      Origin = 'JOIN_CST_IPI'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object cdsRegraImpostoFiltroJOIN_CST_PIS_COFINS: TStringField
      FieldName = 'JOIN_CST_PIS_COFINS'
      Origin = 'JOIN_CST_PIS_COFINS'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object cdsRegraImpostoFiltroID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
      Required = True
    end
    object cdsRegraImpostoFiltroJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField
      DisplayLabel = 'Descri'#231#227'o da Opera'#231#227'o Fiscal'
      FieldName = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      Origin = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      ProviderFlags = []
      Size = 255
    end
  end
end
