unit uCadImpostoICMS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxCalc, uGBDBCalcEdit, cxButtonEdit,
  uGBDBButtonEditFK, cxRadioGroup, uGBDBRadioGroup, cxCheckBox, uGBDBCheckBox,
  JvExStdCtrls, JvCombobox, JvDBCombobox, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, uGBPanel, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TCadImpostoICMS = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataBO_INCLUIR_FRETE_NA_BASE_IPI: TStringField;
    cdsDataBO_INCLUIR_FRETE_NA_BASE_ICMS: TStringField;
    cdsDataBO_INCLUIR_IPI_NA_BASE_ICMS: TStringField;
    cdsDataPERC_REDUCAO_ICMS_ST: TFMTBCDField;
    cdsDataMODALIDADE_BASE_ICMS: TIntegerField;
    cdsDataMODALIDADE_BASE_ICMS_ST: TIntegerField;
    cdsDataPERC_REDUCAO_ICMS: TFMTBCDField;
    cdsDataPERC_ALIQUOTA_ICMS: TFMTBCDField;
    cdsDataPERC_ALIQUOTA_ICMS_ST: TFMTBCDField;
    cdsDataPERC_MVA_AJUSTADO_ST: TFMTBCDField;
    cdsDataPERC_MVA_PROPRIO: TFMTBCDField;
    cdsDataMOTIVO_DESONERACAO_ICMS: TIntegerField;
    cdsDataPERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST: TFMTBCDField;
    cdsDataBO_DESTACAR_VALOR_ICMS: TStringField;
    cdsDataPERC_BASE_ICMS_PROPRIO: TFMTBCDField;
    cdsDataID_CST_CSOSN: TIntegerField;
    cdsDataJOIN_DESCRICAO_CST_CSOSN: TStringField;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    Label3: TLabel;
    gbDBCalcEdit1: TgbDBCalcEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBCheckBox2: TgbDBCheckBox;
    gbDBCheckBox3: TgbDBCheckBox;
    gbDBCheckBox4: TgbDBCheckBox;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBRadioGroup2: TgbDBRadioGroup;
    JvDBComboBox1: TJvDBComboBox;
    Label4: TLabel;
    gbDBCalcEdit2: TgbDBCalcEdit;
    Label5: TLabel;
    Label6: TLabel;
    gbDBCalcEdit3: TgbDBCalcEdit;
    Label7: TLabel;
    gbDBCalcEdit4: TgbDBCalcEdit;
    Label8: TLabel;
    gbDBCalcEdit5: TgbDBCalcEdit;
    Label9: TLabel;
    gbDBCalcEdit6: TgbDBCalcEdit;
    Label10: TLabel;
    gbDBCalcEdit7: TgbDBCalcEdit;
    Label11: TLabel;
    gbDBCalcEdit8: TgbDBCalcEdit;
    cdsDataJOIN_CODIGO_CST_CSOSN: TStringField;
    procedure cdsDataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

procedure TCadImpostoICMS.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataPERC_ALIQUOTA_ICMS_PROPRIO_PARA_ST.AsFloat := 0;
end;

initialization
  RegisterClass(TCadImpostoICMS);
finalization
  UnRegisterClass(TCadImpostoICMS);

end.
