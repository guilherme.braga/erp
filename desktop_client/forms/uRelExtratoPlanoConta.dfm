inherited RelExtratoPlanoConta: TRelExtratoPlanoConta
  Caption = 'Extrato do Plano de Conta'
  ExplicitWidth = 865
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxPeriodo'
        end
        item
          ToolbarName = 'dxBarOperacao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    object PageControlRelatorios: TcxPageControl
      Left = 2
      Top = 2
      Width = 845
      Height = 334
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheet1
      Properties.ActivateFocusedTab = False
      Properties.CustomButtons.Buttons = <>
      Properties.Options = [pcoRedrawOnResize]
      Properties.Style = 8
      Properties.TabsScroll = False
      ClientRectBottom = 334
      ClientRectRight = 845
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Caption = 'H'#237'brido'
        ImageIndex = 12
        object cxPageControlHibrido: TcxPageControl
          Left = 0
          Top = 0
          Width = 845
          Height = 310
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          PopupMenu = dxRibbonRadialMenu
          TabOrder = 0
          Properties.ActivePage = cxPrimeiroNivelH
          Properties.ActivateFocusedTab = False
          Properties.CustomButtons.Buttons = <>
          Properties.Style = 8
          LookAndFeel.Kind = lfOffice11
          ClientRectBottom = 310
          ClientRectRight = 845
          ClientRectTop = 24
          object cxCentroResultadoH: TcxTabSheet
            Caption = 'Centro de Resultado'
            ImageIndex = 0
            object gridCentroResultado: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewCentroResultado: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsCentroResultado
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Centros de Resultado'
                  end>
              end
              object cxGridLevel12: TcxGridLevel
                GridView = ViewCentroResultado
              end
            end
          end
          object cxPrimeiroNivelH: TcxTabSheet
            Caption = 'Plano de Conta (1'#186' N'#237'vel)'
            ImageIndex = 1
            object gridPrimeiroNivel: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewPrimeiroNivel: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsPrimeiroNivel
                DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.FooterMultiSummaries = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupFooterMultiSummaries = True
                OptionsView.GroupFooters = gfAlwaysVisible
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Planos de Contas (1'#186' N'#237'vel)'
                  end>
              end
              object cxGridLevel20: TcxGridLevel
                GridView = ViewPrimeiroNivel
              end
            end
          end
          object cxSegundoNivelH: TcxTabSheet
            Caption = 'Plano de Conta (2'#186' N'#237'vel)'
            ImageIndex = 2
            object gridSegundoNivel: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewSegundoNivel: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsSegundoNivel
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupFooterMultiSummaries = True
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Plano de Contas (2'#186' N'#237'vel)'
                  end>
              end
              object cxGridLevel21: TcxGridLevel
                GridView = ViewSegundoNivel
              end
            end
          end
          object cxTerceiroNivelH: TcxTabSheet
            Caption = 'Plano de Conta (3'#186' e 4'#176' N'#237'vel)'
            ImageIndex = 3
            object gridTerceiroNivel: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewTerceiroNivel: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsTerceiroNivel
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.FooterMultiSummaries = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupFooterMultiSummaries = True
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Plano de Contas (3'#186' e 4'#176' N'#237'vel)'
                  end>
              end
              object cxGridLevel22: TcxGridLevel
                GridView = ViewTerceiroNivel
              end
            end
          end
          object cxQuartoNivelH: TcxTabSheet
            Caption = 'Plano de Conta (4'#186' N'#237'vel)'
            ImageIndex = 4
            object gridQuartoNivel: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewQuartoNivel: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsQuartoNivel
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupFooterMultiSummaries = True
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Plano de Contas (4'#186' N'#237'vel)'
                  end>
              end
              object cxGridLevel23: TcxGridLevel
                GridView = ViewQuartoNivel
              end
            end
          end
          object cxContaCorrenteH: TcxTabSheet
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Caption = 'Conta Corrente'
            ImageIndex = 5
            object gridContaCorrente: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object ViewContaCorrente: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsContaCorrente
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupFooterMultiSummaries = True
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Contas Correntes'
                  end>
              end
              object cxGridLevel24: TcxGridLevel
                GridView = ViewContaCorrente
              end
            end
          end
          object cxMovimentacaoH: TcxTabSheet
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Caption = 'Movimenta'#231#227'o'
            ImageIndex = 6
            object gridMovimentacao: TcxGrid
              Left = 0
              Top = 0
              Width = 845
              Height = 286
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object cxGridDBTableView124: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.KeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_JAN'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_FEV'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_MAR'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_ABR'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_MAI'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_JUN'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_JUL'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_AGO'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_SET'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_OUT'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_NOV'
                  end
                  item
                    Format = '###,###,###,###,##0.00'
                    Kind = skSum
                    FieldName = 'VL_SALDO_CONCILIADO_DEZ'
                  end>
                DataController.Summary.SummaryGroups = <
                  item
                    Links = <
                      item
                      end>
                    SummaryItems = <
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_JAN'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_FEV'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_MAR'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_ABR'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_MAI'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_JUN'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_JUL'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_AGO'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_SET'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_OUT'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_NOV'
                      end
                      item
                        Format = '###,###,###,###,##0.00'
                        Kind = skSum
                        Position = spFooter
                        FieldName = 'VL_SALDO_CONCILIADO_DEZ'
                      end>
                  end>
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsCustomize.ColumnHiding = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
                OptionsCustomize.DataRowSizing = True
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfAlwaysVisible
                OptionsView.Indicator = True
                OptionsView.ShowColumnFilterButtons = sfbAlways
                object cxGridDBColumn983: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                end
                object cxGridDBColumn984: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
              end
              object cxGridDBTableView125: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                object cxGridDBColumn985: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn986: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn987: TcxGridDBColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                end
                object cxGridDBColumn988: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                end
                object cxGridDBColumn989: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                end
                object cxGridDBColumn990: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                end
                object cxGridDBColumn991: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                end
              end
              object cxGridDBTableView126: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                object cxGridDBColumn992: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn993: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn994: TcxGridDBColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                end
                object cxGridDBColumn995: TcxGridDBColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                end
                object cxGridDBColumn996: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                end
                object cxGridDBColumn997: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                end
                object cxGridDBColumn998: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                end
                object cxGridDBColumn999: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                end
              end
              object cxGridDBTableView127: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                object cxGridDBColumn1000: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn1001: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn1002: TcxGridDBColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                end
                object cxGridDBColumn1003: TcxGridDBColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                end
                object cxGridDBColumn1004: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                end
                object cxGridDBColumn1005: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                end
                object cxGridDBColumn1006: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                end
                object cxGridDBColumn1007: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                end
              end
              object cxGridDBTableView128: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
              end
              object cxGridDBTableView129: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                object cxGridDBColumn1008: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn1009: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn1010: TcxGridDBColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                end
                object cxGridDBColumn1011: TcxGridDBColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                end
                object cxGridDBColumn1012: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                end
                object cxGridDBColumn1013: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                end
                object cxGridDBColumn1014: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                end
                object cxGridDBColumn1015: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                end
              end
              object cxGridDBTableView130: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ExpandButtonsForEmptyDetails = False
                OptionsView.GroupByBox = False
                object cxGridDBColumn1016: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn1017: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn1018: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA'
                end
                object cxGridDBColumn1019: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                end
                object cxGridDBColumn1020: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                end
                object cxGridDBColumn1021: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                end
              end
              object cxGridDBTableView131: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_CONTA_CORRENTE;ID_PLANO_CONTA'
                DataController.KeyFieldNames = 'ID;ID_PLANO_CONTA'
                DataController.MasterKeyFieldNames = 'ID;ID_PLANO_CONTA'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                object cxGridDBColumn1022: TcxGridDBColumn
                  DataBinding.FieldName = 'ID'
                end
                object cxGridDBColumn1023: TcxGridDBColumn
                  DataBinding.FieldName = 'DESCRICAO'
                end
                object cxGridDBColumn1024: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE'
                end
                object cxGridDBColumn1025: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA'
                end
                object cxGridDBColumn1026: TcxGridDBColumn
                  DataBinding.FieldName = 'DT_CADASTRO'
                end
                object cxGridDBColumn1027: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_MOVIMENTO'
                end
                object cxGridDBColumn1028: TcxGridDBColumn
                  DataBinding.FieldName = 'TP_MOVIMENTO'
                end
                object cxGridDBColumn1029: TcxGridDBColumn
                  DataBinding.FieldName = 'BO_CONCILIADO'
                end
                object cxGridDBColumn1030: TcxGridDBColumn
                  DataBinding.FieldName = 'DT_CONCILIACAO'
                end
                object cxGridDBColumn1031: TcxGridDBColumn
                  DataBinding.FieldName = 'DT_EMISSAO'
                end
                object cxGridDBColumn1032: TcxGridDBColumn
                  DataBinding.FieldName = 'OBSERVACAO'
                end
                object cxGridDBColumn1033: TcxGridDBColumn
                  DataBinding.FieldName = 'CHEQUE_AGENCIA'
                end
                object cxGridDBColumn1034: TcxGridDBColumn
                  DataBinding.FieldName = 'CHEQUE_CONTA'
                end
                object cxGridDBColumn1035: TcxGridDBColumn
                  DataBinding.FieldName = 'FORMA_PAGAMENTO'
                end
                object cxGridDBColumn1036: TcxGridDBColumn
                  DataBinding.FieldName = 'CHEQUE_TITULAR'
                end
                object cxGridDBColumn1037: TcxGridDBColumn
                  DataBinding.FieldName = 'CHEQUE_DT_DEPOSITO'
                end
                object cxGridDBColumn1038: TcxGridDBColumn
                  DataBinding.FieldName = 'CHEQUE_NUMERO'
                end
                object cxGridDBColumn1039: TcxGridDBColumn
                  DataBinding.FieldName = 'DT_MOVIMENTO'
                end
                object cxGridDBColumn1040: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_SALDO_CONCILIADO'
                end
                object cxGridDBColumn1041: TcxGridDBColumn
                  DataBinding.FieldName = 'VL_SALDO_GERAL'
                end
                object cxGridDBColumn1042: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_MOVIMENTO_ORIGEM'
                end
                object cxGridDBColumn1043: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                end
                object cxGridDBColumn1044: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_USUARIO'
                end
                object cxGridDBColumn1045: TcxGridDBColumn
                  DataBinding.FieldName = 'NOME_CONTA_CORRENTE'
                end
                object cxGridDBColumn1046: TcxGridDBColumn
                  DataBinding.FieldName = 'NOME_PLANO_CONTA'
                end
                object cxGridDBColumn1047: TcxGridDBColumn
                  DataBinding.FieldName = 'TP_MOVIMENTO_SIGLA'
                end
                object cxGridDBColumn1048: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE_ORIGEM'
                end
                object cxGridDBColumn1049: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_MOVIMENTO_DESTINO'
                end
                object cxGridDBColumn1050: TcxGridDBColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE_DESTINO'
                end
              end
              object cxGridDBLayoutView27: TcxGridDBLayoutView
                Navigator.Buttons.CustomButtons = <>
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                object cxGridDBLayoutViewItem131: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'DESCRICAO'
                  LayoutItem = cxGridLayoutItem131
                end
                object cxGridDBLayoutViewItem132: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                  LayoutItem = cxGridLayoutItem132
                end
                object dxLayoutGroup27: TdxLayoutGroup
                  AlignHorz = ahLeft
                  AlignVert = avTop
                  ButtonOptions.Buttons = <>
                  Hidden = True
                  ShowBorder = False
                  Index = -1
                end
                object cxGridLayoutItem131: TcxGridLayoutItem
                  Parent = dxLayoutGroup27
                  Index = 0
                end
                object cxGridLayoutItem132: TcxGridLayoutItem
                  Parent = dxLayoutGroup27
                  Index = 1
                end
              end
              object cxGridDBLayoutView28: TcxGridDBLayoutView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                object cxGridDBLayoutViewItem133: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'ID'
                  LayoutItem = cxGridLayoutItem133
                end
                object cxGridDBLayoutViewItem134: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'DESCRICAO'
                  LayoutItem = cxGridLayoutItem134
                end
                object cxGridDBLayoutViewItem135: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'SEQUENCIA'
                  LayoutItem = cxGridLayoutItem135
                end
                object cxGridDBLayoutViewItem136: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'BO_ATIVO'
                  LayoutItem = cxGridLayoutItem136
                end
                object cxGridDBLayoutViewItem137: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                  LayoutItem = cxGridLayoutItem137
                end
                object cxGridDBLayoutViewItem138: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'VL_CREDITO'
                  LayoutItem = cxGridLayoutItem138
                end
                object cxGridDBLayoutViewItem139: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'VL_DEBITO'
                  LayoutItem = cxGridLayoutItem139
                end
                object cxGridDBLayoutViewItem140: TcxGridDBLayoutViewItem
                  DataBinding.FieldName = 'VL_TOTAL'
                  LayoutItem = cxGridLayoutItem140
                end
                object dxLayoutGroup28: TdxLayoutGroup
                  AlignHorz = ahLeft
                  AlignVert = avTop
                  ButtonOptions.Buttons = <>
                  Hidden = True
                  ShowBorder = False
                  Index = -1
                end
                object cxGridLayoutItem133: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 0
                end
                object cxGridLayoutItem134: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 1
                end
                object cxGridLayoutItem135: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 2
                end
                object cxGridLayoutItem136: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 3
                end
                object cxGridLayoutItem137: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 4
                end
                object cxGridLayoutItem138: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 5
                end
                object cxGridLayoutItem139: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 6
                end
                object cxGridLayoutItem140: TcxGridLayoutItem
                  Parent = dxLayoutGroup28
                  Index = 7
                end
              end
              object ViewMovimentacao: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsMovimentacao
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.FooterAutoHeight = True
                OptionsView.FooterMultiSummaries = True
                OptionsView.GroupByHeaderLayout = ghlHorizontal
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                Bands = <
                  item
                    Caption = 'Movimenta'#231#245'es'
                  end>
              end
              object cxGridDBBandedTableView106: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID_CENTRO_RESULTADO'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                    Caption = 'Primeiro Nivel'
                  end>
                object cxGridDBBandedColumn1037: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1038: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1039: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1040: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1041: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1042: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1043: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView107: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Bands = <
                  item
                    Caption = 'Segundo N'#237'vel'
                  end>
                object cxGridDBBandedColumn1044: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1045: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1046: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1047: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1048: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1049: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1050: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1051: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView108: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                object cxGridDBBandedColumn1052: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1053: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1054: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1055: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1056: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1057: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1058: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1059: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView109: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                object cxGridDBBandedColumn1060: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1061: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1062: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1063: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1064: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1065: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1066: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1067: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView110: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA_ASSOCIADO'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                object cxGridDBBandedColumn1068: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1069: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1070: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'SEQUENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1071: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'BO_ATIVO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1072: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA_ASSOCIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1073: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1074: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1075: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView111: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_PLANO_CONTA'
                DataController.KeyFieldNames = 'ID'
                DataController.MasterKeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                object cxGridDBBandedColumn1076: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1077: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1078: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1079: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_CREDITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1080: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DEBITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1081: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TOTAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
              end
              object cxGridDBBandedTableView112: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DetailKeyFieldNames = 'ID_CONTA_CORRENTE;ID_PLANO_CONTA'
                DataController.KeyFieldNames = 'ID;ID_PLANO_CONTA'
                DataController.MasterKeyFieldNames = 'ID;ID_PLANO_CONTA'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                object cxGridDBBandedColumn1082: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1083: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DESCRICAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1084: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE'
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1085: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PLANO_CONTA'
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1086: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_CADASTRO'
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1087: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_MOVIMENTO'
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1088: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'TP_MOVIMENTO'
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1089: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'BO_CONCILIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1090: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_CONCILIACAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 8
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1091: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_EMISSAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 9
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1092: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'OBSERVACAO'
                  Position.BandIndex = 0
                  Position.ColIndex = 10
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1093: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'CHEQUE_AGENCIA'
                  Position.BandIndex = 0
                  Position.ColIndex = 11
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1094: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'CHEQUE_CONTA'
                  Position.BandIndex = 0
                  Position.ColIndex = 12
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1095: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'FORMA_PAGAMENTO'
                  Position.BandIndex = 0
                  Position.ColIndex = 13
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1096: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'CHEQUE_TITULAR'
                  Position.BandIndex = 0
                  Position.ColIndex = 14
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1097: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'CHEQUE_DT_DEPOSITO'
                  Position.BandIndex = 0
                  Position.ColIndex = 15
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1098: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'CHEQUE_NUMERO'
                  Position.BandIndex = 0
                  Position.ColIndex = 16
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1099: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_MOVIMENTO'
                  Position.BandIndex = 0
                  Position.ColIndex = 17
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1100: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_SALDO_CONCILIADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 18
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1101: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_SALDO_GERAL'
                  Position.BandIndex = 0
                  Position.ColIndex = 19
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1102: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_MOVIMENTO_ORIGEM'
                  Position.BandIndex = 0
                  Position.ColIndex = 20
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1103: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                  Position.BandIndex = 0
                  Position.ColIndex = 21
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1104: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_USUARIO'
                  Position.BandIndex = 0
                  Position.ColIndex = 22
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1105: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'NOME_CONTA_CORRENTE'
                  Position.BandIndex = 0
                  Position.ColIndex = 23
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1106: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'NOME_PLANO_CONTA'
                  Position.BandIndex = 0
                  Position.ColIndex = 24
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1107: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'TP_MOVIMENTO_SIGLA'
                  Position.BandIndex = 0
                  Position.ColIndex = 25
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1108: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE_ORIGEM'
                  Position.BandIndex = 0
                  Position.ColIndex = 26
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1109: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_MOVIMENTO_DESTINO'
                  Position.BandIndex = 0
                  Position.ColIndex = 27
                  Position.RowIndex = 0
                end
                object cxGridDBBandedColumn1110: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_CONTA_CORRENTE_DESTINO'
                  Position.BandIndex = 0
                  Position.ColIndex = 28
                  Position.RowIndex = 0
                end
              end
              object cxGridLevel25: TcxGridLevel
                GridView = ViewMovimentacao
              end
            end
          end
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    inherited ActConfirm: TAction
      Visible = False
    end
    inherited ActCancel: TAction
      Visible = False
    end
    object ActAnterior: TAction
      Category = 'Action'
      Caption = '&Anterior <-'
      Hint = 'Clique sobre o bot'#227'o para visualizar o n'#237'vel anterior'
      ImageIndex = 18
      ShortCut = 37
      OnExecute = ActAnteriorExecute
    end
    object ActProximo: TAction
      Category = 'Action'
      Caption = '&Pr'#243'ximo ->'
      Hint = 'Visualizar o pr'#243'ximo n'#237'vel'
      ImageIndex = 17
      ShortCut = 39
      OnExecute = ActProximoExecute
    end
    object ActAtualizar: TAction
      Category = 'Action'
      Caption = 'A&tualizar F5'
      ImageIndex = 88
      ShortCut = 116
      OnExecute = ActAtualizarExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Operacoes'
      'AcoesGrids')
    Categories.ItemsVisibles = (
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 617
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockControl = nil
      DockedDockControl = nil
      DockingStyle = dsNone
      FloatLeft = 785
      FloatTop = 198
      FloatClientWidth = 70
      FloatClientHeight = 108
      Visible = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 529
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object dxBarOperacao: TdxBar [3]
      Caption = 'Opera'#231#245'es'
      CaptionButtons = <>
      DockedLeft = 339
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarAtualizar'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxPeriodo: TdxBar [4]
      Caption = 'Filtros'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'filtroPeriodoRotulo'
        end
        item
          Visible = True
          ItemName = 'FiltroPeriodoInicio'
        end
        item
          Visible = True
          ItemName = 'FiltroPeriodoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroExibirPrevisao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSeparator1: TdxBarSeparator [8]
      Caption = 'New Separator'
      Category = 0
      Hint = 'New Separator'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem [9]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object dxBarStatic1: TdxBarStatic [10]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object filtroNone: TdxBarStatic [11]
      Category = 0
      Visible = ivAlways
    end
    object filtroExibirPrevisao: TcxBarEditItem [12]
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = 'Exibir colunas do previsto'
          Value = 'P'
        end
        item
          Caption = 'Exibir colunas do realizado'
          Value = 'R'
        end
        item
          Caption = 'Exibir previsto e realizado'
          Value = 'A'
        end>
      Properties.OnEditValueChanged = filtroExibirPrevisaoPropertiesEditValueChanged
    end
    object dxBarStatic2: TdxBarStatic [13]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = ActAnterior
      Category = 2
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = ActProximo
      Category = 2
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = ActConfirm
      Category = 2
    end
    object dxBarAtualizar: TdxBarLargeButton
      Action = ActAtualizar
      Category = 2
    end
    object FiltroPeriodoInicio: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object FiltroPeriodoFim: TcxBarEditItem
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object filtroPeriodoRotulo: TdxBarStatic
      Caption = 'Per'#237'odo de Movimenta'#231#227'o'
      Category = 2
      Hint = 'Per'#237'odo de Movimenta'#231#227'o'
      Visible = ivAlways
    end
    object bbExplorarRegistroSelecionado: TdxBarButton
      Action = ActAbrirMovimentoSelecionado
      Category = 3
    end
    object bbExplorarTodosRegistros: TdxBarButton
      Action = ActAbrirTodosRegistros
      Category = 3
    end
    object bbExportarExcel: TdxBarButton
      Action = ActExportarExcel
      Category = 3
    end
    object bbExibirAgrupamento: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 3
    end
    object bbExpandirAgrupamentos: TdxBarButton
      Action = ActExpandirTudo
      Category = 3
    end
    object bbRecolherAgrupamentos: TdxBarButton
      Action = ActRecolherAgrupamento
      Category = 3
    end
    object bbAlterarRotulo: TdxBarButton
      Action = ActAlterarColunasGrid
      Category = 3
    end
    object bbRestaurarConfiguracoes: TdxBarButton
      Action = ActRestaurarColunasPadrao
      Category = 3
    end
  end
  inherited alAcoes16x16: TActionList
    object ActExportarExcel: TAction
      Caption = 'Exportar para Excel'
      Hint = 'Exportar grade para Excel'
      ImageIndex = 274
      OnExecute = ActExportarExcelExecute
    end
    object ActExibirAgrupamento: TAction
      Caption = 'Exibir Agrupamento'
      Hint = 'Mostrar barra de agrupamento'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActExpandirTudo: TAction
      Caption = 'Expandir Agrupamentos'
      Hint = 'Expandir agrupamentos'
      ImageIndex = 117
      OnExecute = ActExpandirTudoExecute
    end
    object ActRecolherAgrupamento: TAction
      Caption = 'Recolher Agrupamento'
      Hint = 'Oculta o detalhamento dos agrupamentos'
      ImageIndex = 116
      OnExecute = ActRecolherAgrupamentoExecute
    end
    object ActExportarPDF: TAction
      Caption = 'Exportar para PDF'
      Hint = 'Exportar grade para PDF'
      ImageIndex = 273
      OnExecute = ActExportarPDFExecute
    end
    object ActAbrirMovimentoSelecionado: TAction
      Caption = 'Explorar Registro Selecionado'
      ImageIndex = 11
      Visible = False
      OnExecute = ActAbrirMovimentoSelecionadoExecute
    end
    object ActAbrirTodosRegistros: TAction
      Caption = 'Explorar Todos os Registros'
      ImageIndex = 11
      Visible = False
      OnExecute = ActAbrirTodosRegistrosExecute
    end
  end
  object fdmCentroResultado: TFDMemTable
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 72
    Top = 264
  end
  object fdmPrimeiroNivel: TFDMemTable
    BeforePost = AoSalvarAlteracaoPrevisao
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 184
    Top = 264
  end
  object fdmSegundoNivel: TFDMemTable
    BeforePost = AoSalvarAlteracaoPrevisao
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 280
    Top = 264
  end
  object fdmTerceiroNivel: TFDMemTable
    BeforePost = AoSalvarAlteracaoPrevisao
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 376
    Top = 264
  end
  object fdmQuartoNivel: TFDMemTable
    BeforePost = AoSalvarAlteracaoPrevisao
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 464
    Top = 264
  end
  object dsCentroResultado: TDataSource
    DataSet = fdmCentroResultado
    Left = 72
    Top = 312
  end
  object dsPrimeiroNivel: TDataSource
    DataSet = fdmPrimeiroNivel
    Left = 184
    Top = 312
  end
  object dsSegundoNivel: TDataSource
    DataSet = fdmSegundoNivel
    Left = 280
    Top = 312
  end
  object dsTerceiroNivel: TDataSource
    DataSet = fdmTerceiroNivel
    Left = 376
    Top = 312
  end
  object dsQuartoNivel: TDataSource
    DataSet = fdmQuartoNivel
    Left = 464
    Top = 312
  end
  object fdmContaCorrente: TFDMemTable
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 560
    Top = 264
  end
  object dsContaCorrente: TDataSource
    DataSet = fdmContaCorrente
    Left = 560
    Top = 312
  end
  object dsMovimentacao: TDataSource
    DataSet = fdmMovimentacao
    Left = 664
    Top = 312
  end
  object fdmMovimentacao: TFDMemTable
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 664
    Top = 264
  end
  object fdmConsultaDados: TFDMemTable
    FetchOptions.AssignedValues = [evMode, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 440
    Top = 232
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 700
    Top = 16
    object ExplorarTodososRegistros2: TMenuItem
      Action = ActAbrirMovimentoSelecionado
    end
    object ExplorarTodososRegistros1: TMenuItem
      Action = ActAbrirTodosRegistros
    end
    object ExportarparaExcel1: TMenuItem
      Action = ActExportarExcel
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ExportarparaPDF1: TMenuItem
      Action = ActExibirAgrupamento
    end
    object ExpandirAgrupamentos1: TMenuItem
      Action = ActExpandirTudo
    end
    object RecolherAgrupamento1: TMenuItem
      Action = ActRecolherAgrupamento
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ActRestaurarColunasPadrao1: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
  end
  object ACExtratoConta16x16: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 568
    Top = 16
    object ActAlterarColunasGrid: TAction
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Caption = 'Restaurar Configura'#231#245'es Iniciais'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 664
    Top = 72
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = gridCentroResultado
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.GrayShading = True
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object dxRibbonRadialMenu: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbExplorarRegistroSelecionado'
      end
      item
        Visible = True
        ItemName = 'bbExplorarTodosRegistros'
      end
      item
        Visible = True
        ItemName = 'bbExportarExcel'
      end
      item
        Visible = True
        ItemName = 'bbExibirAgrupamento'
      end
      item
        Visible = True
        ItemName = 'bbExpandirAgrupamentos'
      end
      item
        Visible = True
        ItemName = 'bbRecolherAgrupamentos'
      end
      item
        Visible = True
        ItemName = 'bbAlterarRotulo'
      end
      item
        Visible = True
        ItemName = 'bbRestaurarConfiguracoes'
      end>
    Images = DmAcesso.cxImage16x16
    Ribbon = dxRibbon1
    UseOwnFont = False
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 456
    Top = 216
  end
end
