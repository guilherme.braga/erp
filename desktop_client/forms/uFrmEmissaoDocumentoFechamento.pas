unit uFrmEmissaoDocumentoFechamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TFrmEmissaoDocumentoFechamento = class(TFrmModalPadrao)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    ActEmitirComanda: TAction;
    ActEmitirDuplicata: TAction;
    ActEmitirCarne: TAction;
    ActEmitirNFCe: TAction;
    ActEmitirNFE: TAction;
    ActSairEmissaoDocumentos: TAction;
    cxButton6: TcxButton;
    procedure ActSairEmissaoDocumentosExecute(Sender: TObject);
    procedure ActEmitirNFEExecute(Sender: TObject);
    procedure ActEmitirNFCeExecute(Sender: TObject);
    procedure ActEmitirCarneExecute(Sender: TObject);
    procedure ActEmitirDuplicataExecute(Sender: TObject);
    procedure ActEmitirComandaExecute(Sender: TObject);
  private

  protected
    FCPFNaNota: String;
    FIdPK: Integer;
    procedure EmissaoCarne; virtual;
    procedure EmissaoComanda; virtual;
    procedure EmissaoDuplicata; virtual;
    procedure EmissaoNFCe; virtual;
    procedure EmissaoNFe; virtual;
    procedure AoCriarFormulario; virtual;
  public
    procedure DesabilitarCarne;
    procedure DesabilitarComanda;
    procedure DesabilitarDuplicata;
    procedure DesabilitarNFCe;
    procedure DesabilitarNFe;

    class procedure EmissaoDocumento(AIdPK: Integer);
  end;

implementation

{$R *.dfm}

uses uFrmMessage, uFrmVisualizacaoRelatoriosAssociados, uSistema, uFrmIdentificacaoDestinatarioNFCe,
  uFrmMessage_Process;

procedure TFrmEmissaoDocumentoFechamento.ActEmitirCarneExecute(Sender: TObject);
begin
  inherited;
  EmissaoCarne;
end;

procedure TFrmEmissaoDocumentoFechamento.ActEmitirComandaExecute(Sender: TObject);
begin
  inherited;
  EmissaoComanda;
end;

procedure TFrmEmissaoDocumentoFechamento.ActEmitirDuplicataExecute(Sender: TObject);
begin
  inherited;
  EmissaoDuplicata;
end;

procedure TFrmEmissaoDocumentoFechamento.ActEmitirNFCeExecute(Sender: TObject);
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Gerando NFC-e');
    //FCPFNaNota := TFrmIdentificacaoDestinatarioNFCe.CPFNaNota;

    EmissaoNFCe;
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  Close;
end;

procedure TFrmEmissaoDocumentoFechamento.ActEmitirNFEExecute(Sender: TObject);
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Gerando NF-e');

    EmissaoNFE;
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  Close;
end;

procedure TFrmEmissaoDocumentoFechamento.ActSairEmissaoDocumentosExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFrmEmissaoDocumentoFechamento.AoCriarFormulario;
begin
// implementar na heran�a
end;

procedure TFrmEmissaoDocumentoFechamento.DesabilitarCarne;
begin
  ActEmitirCarne.Enabled := false;
  ActEmitirCarne.Visible := false;
end;

procedure TFrmEmissaoDocumentoFechamento.DesabilitarComanda;
begin
  ActEmitirComanda.Enabled := false;
  ActEmitirComanda.Visible := false;
end;

procedure TFrmEmissaoDocumentoFechamento.DesabilitarDuplicata;
begin
  ActEmitirDuplicata.Enabled := false;
  ActEmitirDuplicata.Visible := false;
end;

procedure TFrmEmissaoDocumentoFechamento.DesabilitarNFCe;
begin
  ActEmitirNFCe.Enabled := false;
  ActEmitirNFCe.Visible := false;
end;

procedure TFrmEmissaoDocumentoFechamento.DesabilitarNFe;
begin
  ActEmitirNFe.Enabled := false;
  ActEmitirNFe.Visible := false;
end;

procedure TFrmEmissaoDocumentoFechamento.EmissaoCarne;
begin
  //Implementar na heran�a
end;

procedure TFrmEmissaoDocumentoFechamento.EmissaoComanda;
begin
  TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
    FIdPK, Self.Name+'Comanda', TSistema.Sistema.usuario.idSeguranca);

  Close;
end;

class procedure TFrmEmissaoDocumentoFechamento.EmissaoDocumento(AIdPK: Integer);
var
  FrmEmissaoDocumento: TFrmEmissaoDocumentoFechamento;
begin
  Application.CreateForm(TFrmEmissaoDocumentoFechamento, FrmEmissaoDocumento);
  try
    FrmEmissaoDocumento.AoCriarFormulario;
    FrmEmissaoDocumento.FIdPk := AIdPK;
    FrmEmissaoDocumento.ShowModal;
  finally
    FreeAndNil(FrmEmissaoDocumento);
  end;
end;

procedure TFrmEmissaoDocumentoFechamento.EmissaoDuplicata;
begin
  TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
    FIdPK, Self.Name+'Duplicata', TSistema.Sistema.usuario.idSeguranca);

  Close;
end;

procedure TFrmEmissaoDocumentoFechamento.EmissaoNFCe;
begin
  //Implementar na heran�a
end;

procedure TFrmEmissaoDocumentoFechamento.EmissaoNFe;
begin
  //
end;

end.
