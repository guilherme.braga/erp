unit uMovGeracaoDocumento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEditPK, cxBlobEdit,
  cxDBEdit, uGBDBBlobEdit, cxButtonEdit, uGBDBButtonEditFK, uGBDBTextEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit,
  Vcl.StdCtrls, cxSpinEdit, uGBDBSpinEdit, uGBPanel, cxRadioGroup,
  uGBDBRadioGroup, uGBDBComboBox, cxCurrencyEdit, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, uUsuarioDesignControl, cxCalc, uGBDBCalcEdit, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, uFrameFiltroVerticalPadrao, uFrameQuitacaoGeracaoDocumentoTipoCheque,
  uFrameQuitacaoGeracaoDocumentoTipoCartao;

type
  TMovGeracaoDocumento = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    edtCodigo: TgbDBTextEditPK;
    Label1: TLabel;
    edtDtCadastro: TgbDBDateEdit;
    Label7: TLabel;
    edtSituacao: TgbDBTextEdit;
    PnDadosGeral: TgbPanel;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cdsGeracaoDocumentoParcela: TGBClientDataSet;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDESCRICAO: TStringField;
    cdsDataVL_TITULO: TFMTBCDField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataSTATUS: TStringField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsDatafdqGeracaoDocumentoParcela: TDataSetField;
    cdsGeracaoDocumentoParcelaID: TAutoIncField;
    cdsGeracaoDocumentoParcelaDOCUMENTO: TStringField;
    cdsGeracaoDocumentoParcelaDESCRICAO: TStringField;
    cdsGeracaoDocumentoParcelaVL_TITULO: TFMTBCDField;
    cdsGeracaoDocumentoParcelaQT_PARCELA: TIntegerField;
    cdsGeracaoDocumentoParcelaNR_PARCELA: TIntegerField;
    cdsGeracaoDocumentoParcelaDT_VENCIMENTO: TDateField;
    cdsGeracaoDocumentoParcelaID_GERACAO_DOCUMENTO: TIntegerField;
    dsGeracaoDocumentoParcela: TDataSource;
    cxGridDBBandedTableView1ID: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DOCUMENTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DESCRICAO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1QT_PARCELA: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1NR_PARCELA: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1ID_GERACAO_DOCUMENTO: TcxGridDBBandedColumn;
    cdsDataID_PLANO_PAGAMENTO: TIntegerField;
    cdsDataTIPO: TStringField;
    cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    ActGerarDocumento: TAction;
    ActEstornarDocumento: TAction;
    btnEstornarDocumento: TdxBarLargeButton;
    btnGerarDocumento: TdxBarLargeButton;
    cdsDataDT_BASE: TDateField;
    cdsDataDOCUMENTO: TStringField;
    cdsDataID_FORMA_PAGAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsGeracaoDocumentoParcelaTOTAL_PARCELAS: TIntegerField;
    gbDados: TgbPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    edtDescCentroResultado: TgbDBTextEdit;
    edtCentroResultado: TgbDBButtonEditFK;
    edtDescContaAnalise: TgbDBTextEdit;
    edtPessoa: TgbDBButtonEditFK;
    edtDescPessoa: TgbDBTextEdit;
    edtDtInicial: TgbDBDateEdit;
    gbDBRadioGroup1: TgbDBRadioGroup;
    edtFormaPagamento: TgbDBButtonEditFK;
    edtDescFormaPagamento: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    cdsDataTIPO_PLANO_PAGAMENTO: TStringField;
    cdsDataQT_PARCELAS: TIntegerField;
    pmAtualizarParcelas: TPopupMenu;
    itemAtualizarParcelas: TMenuItem;
    cdsGeracaoDocumentoParcelaID_CONTA_ANALISE: TIntegerField;
    cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO: TIntegerField;
    cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cxGridDBBandedTableView1TOTAL_PARCELAS: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1ID_CONTA_ANALISE: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1ID_CENTRO_RESULTADO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_CENTRO_RESULTADO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_ANALISE: TcxGridDBBandedColumn;
    cdsDataPARCELA_BASE: TIntegerField;
    EdtDocumento: TgbDBTextEdit;
    EdtDescricao: TgbDBTextEdit;
    Label18: TLabel;
    edtDtCompetencia: TgbDBDateEdit;
    cdsDataDT_COMPETENCIA: TDateField;
    cdsGeracaoDocumentoParcelaDT_COMPETENCIA: TDateField;
    cxGridDBBandedTableView1DT_COMPETENCIA: TcxGridDBBandedColumn;
    pnParcelas: TgbPanel;
    Label20: TLabel;
    edtProcesso: TgbDBTextEdit;
    ActionListGrid: TActionList;
    ActSalvarConfigGrid: TAction;
    SalvarConfiguraes2: TMenuItem;
    Label21: TLabel;
    edDtDocumento: TgbDBDateEdit;
    cdsDataDT_DOCUMENTO: TDateField;
    cdsGeracaoDocumentoParcelaDT_DOCUMENTO: TDateField;
    cxGridDBBandedTableView1DT_DOCUMENTO: TcxGridDBBandedColumn;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    lbContaCorrente: TLabel;
    edtContaCorrente: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    cdsGeracaoDocumentoParcelaID_CONTA_CORRENTE: TIntegerField;
    cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cxGridDBBandedTableView1ID_CONTA_CORRENTE: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTE: TcxGridDBBandedColumn;
    cdsDataID_FILIAL: TIntegerField;
    lbLotePagamento: TdxBarLargeButton;
    lbLoteRecebimento: TdxBarLargeButton;
    ActLotePagamento: TAction;
    ActLoteRecebimento: TAction;
    Label22: TLabel;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    cdsDataID_CARTEIRA: TIntegerField;
    cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField;
    cdsDataVL_ENTRADA: TFMTBCDField;
    edtEntradaQuitacao: TgbDBCalcEdit;
    Label23: TLabel;
    edtValorTitulo: TgbDBCalcEdit;
    cdsDataID_FORMA_PAGAMENTO_ENTRADA: TIntegerField;
    cdsDataID_CONTA_CORRENTE_ENTRADA: TIntegerField;
    cdsDataCHEQUE_SACADO_ENTRADA: TStringField;
    cdsDataCHEQUE_DOC_FEDERAL_ENTRADA: TStringField;
    cdsDataCHEQUE_BANCO_ENTRADA: TStringField;
    cdsDataCHEQUE_DT_EMISSAO_ENTRADA: TDateField;
    cdsDataCHEQUE_AGENCIA_ENTRADA: TStringField;
    cdsDataCHEQUE_CONTA_CORRENTE_ENTRADA: TStringField;
    cdsDataCHEQUE_NUMERO_ENTRADA: TIntegerField;
    cdsDataCHEQUE_DT_VENCIMENTO_ENTRADA: TDateField;
    cdsDataID_OPERADORA_CARTAO_ENTRADA: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA: TStringField;
    PnDadosEntrada: TgbPanel;
    PnQuitacaoCartao: TgbPanel;
    PnQuitacaoCheque: TgbPanel;
    PnTituloCheque: TgbPanel;
    PnDadosGeralEntrada: TgbPanel;
    Label24: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    Label25: TLabel;
    gbDBButtonEditFK4: TgbDBButtonEditFK;
    gbDBTextEdit4: TgbDBTextEdit;
    dxBevel3: TdxBevel;
    gbPanel4: TgbPanel;
    Label8: TLabel;
    edtObservacao: TgbDBBlobEdit;
    cdsDataJOIN_DESCRICAO_OPERADORA_CARTAO_ENTRADA: TStringField;
    pnGeracaoParcelas: TgbPanel;
    gbPlanoPagamentoRepeticao: TgbPanel;
    Label16: TLabel;
    Label17: TLabel;
    gbDBSpinEdit1: TgbDBSpinEdit;
    gbDBSpinEdit2: TgbDBSpinEdit;
    gbPlanoPagamentoCadastro: TgbPanel;
    Label2: TLabel;
    Label19: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBSpinEdit3: TgbDBSpinEdit;
    gbPanel1: TgbPanel;
    Label13: TLabel;
    gbDBRadioGroup2: TgbDBRadioGroup;
    gbDBCalcEdit1: TgbDBCalcEdit;
    Label26: TLabel;
    cdsDataCC_VL_PARCELAMENTO: TFloatField;
    procedure ActGerarDocumentoExecute(Sender: TObject);
    procedure ActEstornarDocumentoExecute(Sender: TObject);
    procedure GerarParcela(Sender: TField);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsGeracaoDocumentoParcelaAfterPost(DataSet: TDataSet);
    procedure cdsGeracaoDocumentoParcelaAfterDelete(DataSet: TDataSet);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsDataTIPO_PLANO_PAGAMENTOChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure itemAtualizarParcelasClick(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesEditValueChanged(
      Sender: TObject);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesEditValueChanged(
      Sender: TObject);
    procedure ActLoteRecebimentoExecute(Sender: TObject);
    procedure ActLotePagamentoExecute(Sender: TObject);
    procedure edtValorTituloExit(Sender: TObject);
    procedure gbDBRadioGroup1Exit(Sender: TObject);
    procedure edtPessoagbAntesDeConsultar(Sender: TObject);
    procedure cdsDataVL_ENTRADAChange(Sender: TField);
    procedure cdsDataID_FORMA_PAGAMENTO_ENTRADAChange(Sender: TField);
    procedure cdsDataCalcFields(DataSet: TDataSet);
  private
    FPodeExecutarGeracaoParcelamento: Boolean;
    FParametroGerarNumeroDocumentoAutomaticamente: TParametroFormulario;
    FParametroGerarVencimentoComMesmoDiaBase: TParametroFormulario;
    FParametroGerarContraPartidaContaCorrenteMovimento: TParametroFormulario;

    procedure HabilitarBotaoEstornarDocumento;
    procedure HabilitarBotaoGerarDocumento;
    procedure HabilitarBotaoLotePagamento;
    procedure HabilitarBotaoLoteRecebimento;
    procedure AtualizarValorDocumento;
    procedure PesquisarContaAnalise;
    procedure CondicionarPlanoPagamento;
    procedure LimparCamposDoTipoDePagamento;
    procedure ValidarDadosEntradaQuitacao;
    procedure ExibirDadosEntrada;
    procedure AtribuirDadosEntrada;
  protected
    procedure GerenciarControles; override;
    procedure AntesDeRemover; override;
    procedure AoCriarFormulario; override;
    procedure DepoisDeAlterar;override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeConfirmar; override;
    procedure BloquearEdicao; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;
    function DuplicarRegistro: Integer; override;
    procedure InstanciarFrames; override;
  public
    FFrameQuitacaoGeracaoDocumentoTipoCheque: TFrameQuitacaoGeracaoDocumentoTipoCheque;
    FFrameQuitacaoGeracaoDocumentoTipoCartao: TFrameQuitacaoGeracaoDocumentoTipoCartao;
  end;

implementation

{$R *.dfm}

uses
  uDmConnection,
  uGeracaoDocumento,
  uChaveProcesso,
  uDatasetUtils,
  uFrmMessage_Process,
  uFrmMessage,
  uPesqContaAnalise,
  uDmAcesso, uChaveProcessoProxy, uGeracaoDocumentoProxy, uTUsuario, uSistema,
  uFrmConsultaPadrao, uControlsUtils, uConsultaGrid, uMovLotePagamento,
  uMovLoteRecebimento, uConstParametroFormulario, uPlanoPagamento, DateUtils,
  uFormaPagamento;

procedure TMovGeracaoDocumento.ActEstornarDocumentoExecute(Sender: TObject);
const MSG_NAO_PODE_ESTORNAR: String =
  'N�o � poss�vel estornar o documento pois existe quita��o realizada.';
var idChaveProcesso: Integer;
    tipoDocumento: String;
    geracaoDocumentoProxy: TGeracaoDocumentoProxy;
begin
  inherited;
  if TFrmMessage.Question('Deseja Estornar os Documentos Gerados?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;
    tipoDocumento := cdsDataTIPO.AsString;

    if (cdsData.Alterando) then
    begin
      ActConfirm.Execute;
    end;

    if TGeracaoDocumento.PodeEstornar(idChaveProcesso) then
    begin
      TFrmMessage_Process.SendMessage('Estornando os Documentos Gerados');
      try
        geracaoDocumentoProxy := TGeracaoDocumentoProxy.Create;
        try
          geracaoDocumentoProxy.FGerarContraPartidaContaCorrenteMovimento :=
            FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim;

          geracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos := tipoDocumento;

          TGeracaoDocumento.EstornarDocumento(idChaveProcesso, geracaoDocumentoProxy);
          AtualizarRegistro;
        finally
          FreeAndNil(geracaoDocumentoProxy);
        end;
      finally
        TFrmMessage_Process.CloseMessage;
      end;
    end
    else
      TFrmMessage.Information(MSG_NAO_PODE_ESTORNAR);
  end;
end;

procedure TMovGeracaoDocumento.AntesDeConfirmar;
begin
  inherited;
  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
  ValidarDadosEntradaQuitacao;
end;

procedure TMovGeracaoDocumento.ActGerarDocumentoExecute(Sender: TObject);
var idChaveProcesso: Integer;
  tipoDocumento: String;
  geracaoDocumentoProxy: TGeracaoDocumentoProxy;
begin
  inherited;
  if TFrmMessage.Question('Deseja efetivar a Gera��o de Documentos?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;
    tipoDocumento   := cdsDataTIPO.AsString;

    if (cdsData.Alterando) then
    begin
      ActConfirm.Execute;
    end;

    TFrmMessage_Process.SendMessage('Efetivando a Gera��o de Documento');
    try
      geracaoDocumentoProxy := TGeracaoDocumentoProxy.Create;
      try
        geracaoDocumentoProxy.FTipoDocumentoParaGeracaoTitulos := tipoDocumento;
        TGeracaoDocumento.GerarDocumento(idChaveProcesso, geracaoDocumentoProxy);
        AtualizarRegistro;
      finally
        FreeAndNil(geracaoDocumentoProxy);
      end;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovGeracaoDocumento.ActLotePagamentoExecute(Sender: TObject);
begin
  inherited;
  TMovLotePagamento.AbrirTelaFiltrando(cdsDataID_CHAVE_PROCESSO.AsInteger);
end;

procedure TMovGeracaoDocumento.ActLoteRecebimentoExecute(Sender: TObject);
begin
  inherited;
  TMovLoteRecebimento.AbrirTelaFiltrando(cdsDataID_CHAVE_PROCESSO.AsInteger);
end;

procedure TMovGeracaoDocumento.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(cxGridDBBandedTableView1,
    TSistema.Sistema.usuario.idSeguranca,
    self.Name + cxGridDBBandedTableView1.Name);
end;

procedure TMovGeracaoDocumento.AntesDeRemover;
const
  MSG_NAO_PODE_EXCLUIR: String =
    'N�o � poss�vel excluir o Documento pois existe Quita��o realizada.';
  MSG_DOCUMENTO_GERADO: String =
    'N�o � poss�vel excluir o Documento, para isso este deve ser Estornado.';
begin
  inherited;

  if cdsDataSTATUS.AsString = TGeracaoDocumentoProxy.STATUS_GERADO then
  begin
    TFrmMessage.Information(MSG_DOCUMENTO_GERADO);
    abort;
  end;

  if not TGeracaoDocumento.PodeExcluir(cdsDataID_CHAVE_PROCESSO.AsInteger) then
  begin
    TFrmMessage.Information(MSG_NAO_PODE_EXCLUIR);
    abort;
  end;
end;

procedure TMovGeracaoDocumento.AoCriarFormulario;
begin
  inherited;
  FPodeExecutarGeracaoParcelamento := true;
  EdtDescricao.Properties.CharCase := ecNormal;
  EdtDocumento.Properties.CharCase := ecNormal;

  PnTituloCheque.Transparent := false;
  PnTituloCheque.PanelStyle.OfficeBackgroundKind := pobkGradient;

  pnParcelas.Transparent := false;
  pnParcelas.PanelStyle.OfficeBackgroundKind := pobkGradient;

  TUsuarioGridView.LoadGridView(cxGridDBBandedTableView1,
    TSistema.Sistema.usuario.idSeguranca,
    self.Name + cxGridDBBandedTableView1.Name);
end;

procedure TMovGeracaoDocumento.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  CondicionarPlanoPagamento;
end;

procedure TMovGeracaoDocumento.cdsDataCalcFields(DataSet: TDataSet);
begin
  inherited;
  if cdsDataVL_TITULO.AsFloat > 0 then
  begin
    cdsDataCC_VL_PARCELAMENTO.AsFloat := cdsDataVL_TITULO.AsFloat - cdsDataVL_ENTRADA.AsFloat;
  end
  else
  begin
    cdsDataCC_VL_PARCELAMENTO.AsFloat := 0;
  end;
end;

procedure TMovGeracaoDocumento.cdsDataID_FORMA_PAGAMENTO_ENTRADAChange(Sender: TField);
begin
  inherited;
  ExibirDadosEntrada;
end;

procedure TMovGeracaoDocumento.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(
    TChaveProcessoProxy.ORIGEM_GERACAO_DOCUMENTO, cdsDataID.AsInteger);
  cdsDataDH_CADASTRO.AsDateTime      := Now;
  cdsDataDT_DOCUMENTO.AsDateTime     := Date;
  cdsDataDT_BASE.AsDateTime          := Date;
  cdsDataDT_COMPETENCIA.AsDateTime   := Date;
  cdsDataPARCELA_BASE.AsInteger      := 1;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;

  if FParametroGerarNumeroDocumentoAutomaticamente.ValorSim then
  begin
    cdsDataDocumento.AsString := InttoStr(StrtoIntDef(UltimoRegistroGerado('GERACAO_DOCUMENTO', 'ID'), 0)+ 1);
  end;
end;

procedure TMovGeracaoDocumento.cdsDataTIPO_PLANO_PAGAMENTOChange(
  Sender: TField);
begin
  inherited;
  LimparCamposDoTipoDePagamento;
  TGeracaoDocumento.LimparParcelas(cdsGeracaoDocumentoParcela);
  CondicionarPlanoPagamento;
  GerarParcela(nil);
end;

procedure TMovGeracaoDocumento.cdsDataVL_ENTRADAChange(Sender: TField);
begin
  inherited;
  ExibirDadosEntrada;
  GerarParcela(nil);
  AtribuirDadosEntrada;
end;

procedure TMovGeracaoDocumento.cdsGeracaoDocumentoParcelaAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  HabilitarBotaoGerarDocumento;
end;

procedure TMovGeracaoDocumento.cdsGeracaoDocumentoParcelaAfterPost(
  DataSet: TDataSet);
begin
  inherited;
  HabilitarBotaoGerarDocumento;
  //AtualizarValorDocumento;
end;

procedure TMovGeracaoDocumento.CondicionarPlanoPagamento;
begin
  gbPlanoPagamentoCadastro.Visible :=
    cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_CADASTRO);

  gbPlanoPagamentoRepeticao.Visible :=
    cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_REPETICAO);
end;

procedure TMovGeracaoDocumento.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Gera��o automatica do numero do documento
  FParametroGerarNumeroDocumentoAutomaticamente := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_NUMERO_DOCUMENTO_AUTOMATICAMENTE,
    TConstParametroFormulario.DESCRICAO_GERAR_NUMERO_DOCUMENTO_AUTOMATICAMENTE,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, 'N');
  AParametros.AdicionarParametro(FParametroGerarNumeroDocumentoAutomaticamente);

  //Gera��o dos vencimentos com o mesmo dia da base
  FParametroGerarVencimentoComMesmoDiaBase := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_VENCIMENTO_COM_MESMO_DIA_DA_BASE,
    TConstParametroFormulario.DESCRICAO_GERAR_VENCIMENTO_COM_MESMO_DIA_DA_BASE,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, 'N');
  AParametros.AdicionarParametro(FParametroGerarVencimentoComMesmoDiaBase);

  //Gerar contra partida no conta corrente movimento
  FParametroGerarContraPartidaContaCorrenteMovimento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TConstParametroFormulario.DESCRICAO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroGerarContraPartidaContaCorrenteMovimento);
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  cxGBDadosMain.SetFocus;
  TConsultaGrid.ConsultarPorCentroResultado(cdsGeracaoDocumentoParcela);
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1ID_CENTRO_RESULTADOPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  cxGBDadosMain.SetFocus;
  TConsultaGrid.ConsultarPorCentroResultado(cdsGeracaoDocumentoParcela,
    false, cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO.AsString);
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  cxGBDadosMain.SetFocus;
  if not cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    cdsGeracaoDocumentoParcela.Alterar;
    TPesqContaAnalise.ExecutarConsulta(cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO.AsInteger,
      cdsGeracaoDocumentoParcelaID_CONTA_ANALISE, cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE);
    cdsGeracaoDocumentoParcela.Salvar;
  end;
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1ID_CONTA_ANALISEPropertiesEditValueChanged(
  Sender: TObject);
begin
  cxGBDadosMain.SetFocus;
  if not cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    cdsGeracaoDocumentoParcela.Alterar;
    TPesqContaAnalise.ExecutarConsulta(cdsGeracaoDocumentoParcelaID_CENTRO_RESULTADO.AsInteger,
      cdsGeracaoDocumentoParcelaID_CONTA_ANALISE, cdsGeracaoDocumentoParcelaJOIN_DESCRICAO_CONTA_ANALISE);
    cdsGeracaoDocumentoParcela.Salvar;
  end;
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  cxGBDadosMain.SetFocus;
  TConsultaGrid.ConsultarPorContaCorrente(cdsGeracaoDocumentoParcela);
end;

procedure TMovGeracaoDocumento.cxGridDBBandedTableView1JOIN_DESCRICAO_CONTA_CORRENTEPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  cxGBDadosMain.SetFocus;
  TConsultaGrid.ConsultarPorContaCorrente(cdsGeracaoDocumentoParcela,
    false, cdsGeracaoDocumentoParcelaID_CONTA_CORRENTE.AsString);
end;

procedure TMovGeracaoDocumento.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Documento';
  campoFiltro.campo := 'DT_DOCUMENTO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Centro de Resultado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Centro de Resultado';
  campoFiltro.campo := 'ID_CENTRO_RESULTADO';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CENTRO_RESULTADO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta de Analise
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta de Analise';
  campoFiltro.campo := 'ID_CONTA_ANALISE';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_ANALISE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Carteira
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Carteira';
  campoFiltro.campo := 'ID_CARTEIRA';
  campoFiltro.tabela := 'GERACAO_DOCUMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CARTEIRA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TMovGeracaoDocumento.DepoisDeAlterar;
begin
  inherited;
  //cdsData.ReadOnly := cdsDataSTATUS.AsString.Equals(STATUS_CANCELADO);
end;

procedure TMovGeracaoDocumento.DepoisDeConfirmar;
begin
  inherited;
  if cdsData.FieldByName('ID').AsInteger <= 0 then
  begin
    AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('GERACAO_DOCUMENTO', 'ID'), 0));
  end
end;

function TMovGeracaoDocumento.DuplicarRegistro: Integer;
begin
  result := TGeracaoDocumento.Duplicar(cdsDataID.AsInteger);
end;

procedure TMovGeracaoDocumento.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovGeracaoDocumento.EdtContaAnaliseKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    PesquisarContaAnalise;
end;

procedure TMovGeracaoDocumento.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovGeracaoDocumento.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovGeracaoDocumento.edtPessoagbAntesDeConsultar(Sender: TObject);
begin
  inherited;
  if cdsDataTIPO.AsString.Equals(TGeracaoDocumentoProxy.TIPO_CONTAPAGAR) then
  begin
    edtPessoa.gbIdentificadorConsulta := 'PESSOA_MOV_CONTA_PAGAR';
  end
  else if cdsDataTIPO.AsString.Equals(TGeracaoDocumentoProxy.TIPO_CONTARECEBER) then
  begin
    edtPessoa.gbIdentificadorConsulta := 'PESSOA_MOV_CONTA_RECEBER';
  end;
end;

procedure TMovGeracaoDocumento.edtValorTituloExit(Sender: TObject);
begin
  inherited;
  TControlsUtils.SetFocus(EdtDescricao);
end;

procedure TMovGeracaoDocumento.ExibirDadosEntrada;
begin
  if TFormaPagamento.FormaPagamentoCheque(cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger) then
  begin
    PnDadosEntrada.Visible := true;
    PnQuitacaoCheque.Visible := true;
    PnQuitacaoCartao.Visible := false;
    PnDadosEntrada.Height := 150;
    PnDadosGeral.Height := 275;
    exit;
  end;

  if TFormaPagamento.FormaPagamentoCartao(cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger) then
  begin
    PnDadosEntrada.Visible := true;
    PnQuitacaoCartao.Visible := true;
    PnQuitacaoCheque.Visible := false;
    PnDadosEntrada.Height := 100;
    PnDadosGeral.Height := 225;
    exit;
  end;

  if cdsDataVL_ENTRADA.AsFloat > 0 then
  begin
    PnDadosEntrada.Height := 54;
    PnDadosEntrada.Visible := true;
    PnQuitacaoCartao.Visible := false;
    PnQuitacaoCheque.Visible := false;
    PnDadosGeral.Height := 177;
    exit;
  end;

  PnDadosGeral.Height := 127;
  PnDadosEntrada.Visible := false;
end;

procedure TMovGeracaoDocumento.gbDBRadioGroup1Exit(Sender: TObject);
begin
  inherited;
  TControlsUtils.SetFocus(EdtDocumento);
end;

procedure TMovGeracaoDocumento.AtribuirDadosEntrada;
begin
  if cdsDataVL_ENTRADA.AsFloat > 0 then
  begin
    if cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger <= 0 then
    begin
      cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger := cdsDataID_FORMA_PAGAMENTO.AsInteger;
      cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA.AsString := cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO.AsString;
    end;

    if cdsDataID_CONTA_CORRENTE_ENTRADA.AsInteger <= 0 then
    begin
      cdsDataID_CONTA_CORRENTE_ENTRADA.AsInteger := cdsDataID_CONTA_CORRENTE.AsInteger;
      cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA.AsString := cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString;
    end;
  end
  else
  begin
    cdsDataID_FORMA_PAGAMENTO_ENTRADA.Clear;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO_ENTRADA.Clear;
    cdsDataID_CONTA_CORRENTE_ENTRADA.Clear;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE_ENTRADA.Clear;
  end;
end;

procedure TMovGeracaoDocumento.AtualizarValorDocumento;
begin
  if cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_CADASTRO) then
  begin
    FPodeExecutarGeracaoParcelamento := false;
    try
      cdsDataVL_TITULO.AsFloat :=
        TDatasetUtils.SomarColuna(cdsGeracaoDocumentoParcelaVL_TITULO);
    finally
      FPodeExecutarGeracaoParcelamento := true;
    end;
  end;
end;

procedure TMovGeracaoDocumento.BloquearEdicao;
begin
  inherited;
  cdsData.SomenteLeitura := not cdsDataSTATUS.AsString.Equals('ABERTO');
end;

procedure TMovGeracaoDocumento.GerarParcela(Sender: TField);
var
  planoPagamentoValido: Boolean;
  valorTituloValido: Boolean;
  tipoValido: Boolean;
  dataBaseValida: Boolean;
  IdPessoaValido: Boolean;
begin
  if cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_CADASTRO) then
  begin
    planoPagamentoValido := (not(cdsDataID_PLANO_PAGAMENTO.AsString.IsEmpty) or
    (not(cdsDataID_PLANO_PAGAMENTO.AsString.IsEmpty) and TDatasetUtils.ValorFoiAlterado(cdsDataID_PLANO_PAGAMENTO)));
  end
  else if cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_REPETICAO) then
  begin
    planoPagamentoValido :=
      (not(cdsDataQT_PARCELAS.AsString.IsEmpty) or
      (not(cdsDataQT_PARCELAS.AsString.IsEmpty) and TDatasetUtils.ValorFoiAlterado(cdsDataQT_PARCELAS)));
  end
  else
  begin
    planoPagamentoValido := false;
  end;

  valorTituloValido := (cdsDataVL_TITULO.AsFloat > 0) or
    TDatasetUtils.ValorFoiAlterado(cdsDataVL_TITULO);

  tipoValido := not(cdsDataTIPO.AsString.IsEmpty);

  dataBaseValida := not(cdsDataDT_BASE.AsString.IsEmpty) or
    TDatasetUtils.ValorFoiAlterado(cdsDataDT_BASE);

  IdPessoaValido := TValidacaoCampo.CampoPreenchido(cdsDataID_PESSOA, False, false);

  if (FPodeExecutarGeracaoParcelamento) and
     (planoPagamentoValido) and
     (valorTituloValido) and
     (tipoValido) and
     (dataBaseValida) and
     (IdPessoaValido)
  then
  begin
    FPodeExecutarGeracaoParcelamento := false;
    try
      TGeracaoDocumento.LimparParcelas(cdsGeracaoDocumentoParcela);
      TGeracaoDocumento.GerarParcelas(cdsData, cdsGeracaoDocumentoParcela);

      if FParametroGerarVencimentoComMesmoDiaBase.ValorSim then
      begin
        cdsGeracaoDocumentoParcela.First;
        TParcelamentoUtils.TrocarDiaDeVencimento(cdsGeracaoDocumentoParcelaDT_VENCIMENTO,
          DayOf(cdsDataDT_BASE.AsDateTime));
        TParcelamentoUtils.ReplicarDiaDeVencimento(cdsGeracaoDocumentoParcelaDT_VENCIMENTO);
      end;
    finally
      FPodeExecutarGeracaoParcelamento := true;
    end;
  end;
end;

procedure TMovGeracaoDocumento.GerenciarControles;
begin
  inherited;
  HabilitarBotaoGerarDocumento;
  HabilitarBotaoEstornarDocumento;
  HabilitarBotaoLotePagamento;
  HabilitarBotaoLoteRecebimento;
  ExibirDadosEntrada;
end;

procedure TMovGeracaoDocumento.HabilitarBotaoGerarDocumento;
begin
  ActGerarDocumento.Visible := (cdsDataSTATUS.AsString = STATUS_ABERTO)
    and not(cdsGeracaoDocumentoParcela.IsEmpty);
end;

procedure TMovGeracaoDocumento.HabilitarBotaoLotePagamento;
begin
  ActLotePagamento.Visible := (cdsDataSTATUS.AsString = STATUS_GERADO) and
  (cdsDataTIPO.AsString.Equals(TGeracaoDocumentoProxy.TIPO_CONTAPAGAR));
end;

procedure TMovGeracaoDocumento.HabilitarBotaoLoteRecebimento;
begin
  ActLoteRecebimento.Visible := (cdsDataSTATUS.AsString = STATUS_GERADO) and
  (cdsDataTIPO.AsString.Equals(TGeracaoDocumentoProxy.TIPO_CONTARECEBER));
end;

procedure TMovGeracaoDocumento.InstanciarFrames;
begin
  inherited;
  if not Assigned(FFrameQuitacaoGeracaoDocumentoTipoCheque) then
  begin
    FFrameQuitacaoGeracaoDocumentoTipoCheque :=
      TFrameQuitacaoGeracaoDocumentoTipoCheque.Create(PnQuitacaoCheque);
    FFrameQuitacaoGeracaoDocumentoTipoCheque.Parent := PnQuitacaoCheque;
    FFrameQuitacaoGeracaoDocumentoTipoCheque.Align := alClient;
    FFrameQuitacaoGeracaoDocumentoTipoCheque.SetDataset(cdsData);
  end;

  if not Assigned(FFrameQuitacaoGeracaoDocumentoTipoCartao) then
  begin
    FFrameQuitacaoGeracaoDocumentoTipoCartao :=
      TFrameQuitacaoGeracaoDocumentoTipoCartao.Create(PnQuitacaoCartao);
    FFrameQuitacaoGeracaoDocumentoTipoCartao.Parent := PnQuitacaoCartao;
    FFrameQuitacaoGeracaoDocumentoTipoCartao.Align := alClient;
    FFrameQuitacaoGeracaoDocumentoTipoCartao.SetDataset(cdsData);
  end;
end;

procedure TMovGeracaoDocumento.itemAtualizarParcelasClick(Sender: TObject);
begin
  inherited;
  GerarParcela(nil);
end;

procedure TMovGeracaoDocumento.LimparCamposDoTipoDePagamento;
begin
  cdsDataQT_PARCELAS.Clear;
  cdsDataID_PLANO_PAGAMENTO.Clear;
  cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO.Clear;

  if cdsDataTIPO_PLANO_PAGAMENTO.AsString.Equals(TIPO_PLANO_PAGAMENTO_REPETICAO) then
  begin
    cdsDataQT_PARCELAS.AsInteger := 1;
  end
end;

procedure TMovGeracaoDocumento.HabilitarBotaoEstornarDocumento;
begin
  ActEstornarDocumento.Visible := cdsDataSTATUS.AsString = STATUS_GERADO;
end;

procedure TMovGeracaoDocumento.PesquisarContaAnalise;
begin
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovGeracaoDocumento.ValidarDadosEntradaQuitacao;
begin
  if cdsDataVL_ENTRADA.AsFloat <= 0 then
  begin
    exit;
  end;

  if cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger <= 0 then
  begin
    TFrmMessage.Information('� necess�rio informar a forma de pagamento para o valor da entrada.');
    Abort;
  end;

  if cdsDataID_CONTA_CORRENTE_ENTRADA.AsInteger <= 0 then
  begin
    TFrmMessage.Information('� necess�rio informar a conta corrente para o valor da entrada.');
    Abort;
  end;

  if TFormaPagamento.FormaPagamentoCartao(cdsDataID_FORMA_PAGAMENTO_ENTRADA.AsInteger) and
    (cdsDataID_OPERADORA_CARTAO_ENTRADA.AsInteger <= 0) then
  begin
    TFrmMessage.Information('Com a ENTRADA sendo realizada em cart�o, '+
      '� necess�rio informar a operadora do cart�o.');
    Abort;
  end;
end;

initialization
  RegisterClass(TMovGeracaoDocumento);

Finalization
  UnRegisterClass(TMovGeracaoDocumento);

end.
