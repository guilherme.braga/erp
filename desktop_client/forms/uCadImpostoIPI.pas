unit uCadImpostoIPI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxCalc, uGBDBCalcEdit, cxButtonEdit,
  uGBDBButtonEditFK, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, uGBPanel;

type
  TCadImpostoIPI = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    Label1: TLabel;
    cdsDataID: TAutoIncField;
    cdsDataPERC_ALIQUOTA: TFMTBCDField;
    cdsDataID_CST_IPI: TIntegerField;
    cdsDataJOIN_DESCRICAO_CST_IPI: TStringField;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    Label3: TLabel;
    gbDBCalcEdit1: TgbDBCalcEdit;
    cdsDataJOIN_CODIGO_CST_IPI: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadImpostoIPI);
finalization
  UnRegisterClass(TCadImpostoIPI);

end.
