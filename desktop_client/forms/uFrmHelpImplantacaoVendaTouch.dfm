inherited FrmHelpImplantacaoVendaTouch: TFrmHelpImplantacaoVendaTouch
  Caption = 'Help Implanta'#231#227'o - Venda Touch'
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    inherited Memo1: TMemo
      Lines.Strings = (
        'CONFIGURA'#199#195'O OBRIGAT'#211'RIA:'
        
          'Atrav'#233's do Insert Padr'#227'o, configure valores para os seguintes ca' +
          'mpos:'
        '- cdsVenda ID_PESSOA '
        '- cdsVenda JOIN_NOME_PESSOA'
        '- cdsVenda ID_TABELA_PRECO'
        '- cdsVenda JOIN_DESCRICAO_TABELA_PRECO'
        ''
        'Atrav'#233's dos par'#226'metros de tela configure:'
        
          '- PARAMETRO_FORMA_PAGAMENTO_AVISTA: FORMA DE PAGAMENTO QUE SER'#193' ' +
          'UTILIZADA QUANDO HOUVER ENTRADA OU '
        'QUITA'#199#195'O EM DINHEIRO.'
        
          '- PARAMETRO_CONTA_CORRENTE: CONTA CORRENTE EM QUE SER'#193' GERADO O ' +
          'REGISTRO REFERENTE A '
        'QUITA'#199#195'O DE CR OU CP GERADO.'
        
          '- PARAMETRO_CENTRO_RESULTADO_AVISTA: CENTRO DE RESULTADO DE FORM' +
          'A DE PAGAMENTO A VISTA.'
        
          '- PARAMETRO_CONTA_ANALISE_AVISTA: CONTA DE ANALISE DE FORMA DE P' +
          'AGAMENTO A VISTA.'
        
          '- PARAMETRO_CARTEIRA: CARTEIRA QUE SER'#193' USADA PARA GERA'#199#195'O DO CO' +
          'NTA A RECEBER E ENTRADA/QUITACAO.')
      ReadOnly = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 654
      ExplicitHeight = 265
    end
  end
end
