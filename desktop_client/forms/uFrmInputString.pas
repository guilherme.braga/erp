unit uFrmInputString;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFrmTecladoCompleto, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, ExtCtrls, Keyboard, cxGroupBox;

type
  TFrmInputString = class(TFrmTecladoCompleto)
    PnCaption: TPanel;
  private

  public
    class function buildKeyboard(const Atext: string): String; overload;
    class function buildKeyboard(const Atext: string; const AValue: String): String; overload;
  end;

var
  FrmInputString: TFrmInputString;

implementation

{$R *.dfm}

class function TFrmInputString.buildKeyboard(const Atext: string): String;
begin
  try
    Application.CreateForm(TFrmInputString, FrmInputString);
    FrmInputString.PnCaption.Caption := Atext;
    FrmInputString.ShowModal;
    result := FrmInputString.EdtVisor.Text;
  finally
    FreeAndNil(FrmInputString);
  end;
end;

class function TFrmInputString.buildKeyboard(const Atext,
  AValue: String): String;
begin
  try
    Application.CreateForm(TFrmInputString, FrmInputString);
    FrmInputString.PnCaption.Caption := Atext;
    FrmInputString.EdtVisor.Text := AValue;
    FrmInputString.ShowModal;
    result := FrmInputString.EdtVisor.Text;
  finally
    FreeAndNil(FrmInputString);
  end;
end;

end.
