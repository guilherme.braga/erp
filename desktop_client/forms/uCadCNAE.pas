unit uCadCNAE;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxTextEdit,
  cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxMaskEdit, cxSpinEdit, uGBDBSpinEdit,
  cxDropDownEdit, uGBDBComboBox;

type
  TCadCNAE = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataSEQUENCIA: TStringField;
    cdsDataNIVEL: TIntegerField;
    lbCodigo: TLabel;
    pkCodigo: TgbDBTextEditPK;
    lbDescricao: TLabel;
    edtDescricao: TgbDBTextEdit;
    lbSequencia: TLabel;
    edtSequencia: TgbDBTextEdit;
    gbDBComboBox1: TgbDBComboBox;
    Label1: TLabel;
    cdsDataSECAO: TStringField;
    procedure cdsDataSEQUENCIAChange(Sender: TField);
  private
    procedure setNivel;
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

{ TCadCNAE }

procedure TCadCNAE.cdsDataSEQUENCIAChange(Sender: TField);
begin
  inherited;
  setNivel;
end;

procedure TCadCNAE.setNivel;
const
  CARACTER_PONTO: Char = '.';
var
  i,
  nivel: Integer;
begin
  nivel := 1;
  for i := 0 to Length(cdsDataSEQUENCIA.AsString) do
  begin
    if cdsDataSEQUENCIA.AsString[i] = CARACTER_PONTO then
      inc(nivel);

    cdsDataNIVEL.AsInteger := nivel;
  end;
end;

initialization
  RegisterClass(TCadCNAE);

Finalization
  UnRegisterClass(TCadCNAE);

end.
