inherited CadPais: TCadPais
  Caption = 'Cadastro de Pa'#237's'
  ClientHeight = 469
  ClientWidth = 929
  ExplicitWidth = 945
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 929
    ExplicitWidth = 929
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 929
    Height = 342
    Properties.ActivePage = cxtsData
    ExplicitWidth = 929
    ExplicitHeight = 342
    ClientRectBottom = 342
    ClientRectRight = 929
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 929
      ExplicitHeight = 318
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 929
        Height = 318
        ExplicitWidth = 929
        ExplicitHeight = 318
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 929
      ExplicitHeight = 318
      inherited DesignPanel: TJvDesignPanel
        Width = 929
        Height = 318
        ExplicitWidth = 929
        ExplicitHeight = 318
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 929
          ExplicitHeight = 318
          Height = 318
          Width = 929
          inherited dxBevel1: TdxBevel
            Width = 925
            ExplicitWidth = 925
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 60
            Top = 5
            TabStop = False
            DataBinding.DataField = 'id'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 60
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'descricao'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 861
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Left = 599
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Pa'#237's'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftWideString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspPais'
    RemoteServer = DmConnection.dspLocalidade
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDatadescricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'descricao'
      Origin = 'descricao'
      Size = 80
    end
  end
end
