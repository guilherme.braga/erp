unit uMovNotaFiscalManutencaoTabelaPreco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxDBEdit, uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxCurrencyEdit,
  dbClient;

type
  TMovNotaFiscalManutencaoTabelaPreco = class(TFrmModalPadrao)
    cxGridPesquisaPadrao: TcxGrid;
    Level1BandedTableView1: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    gbPanel1: TgbPanel;
    Label8: TLabel;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    dsTabelaPrecoItem: TDataSource;
    fdmTabelaPrecoItem: TFDMemTable;
    fdmTabelaPrecoItemID_PRODUTO: TIntegerField;
    fdmTabelaPrecoItemDESCRICAO: TStringField;
    fdmTabelaPrecoItemVL_CUSTO: TFloatField;
    fdmTabelaPrecoItemPERC_LUCRO: TFloatField;
    fdmTabelaPrecoItemVL_VENDA: TFloatField;
    Level1BandedTableView1ID_PRODUTO: TcxGridDBBandedColumn;
    Level1BandedTableView1DESCRICAO: TcxGridDBBandedColumn;
    Level1BandedTableView1VL_CUSTO: TcxGridDBBandedColumn;
    Level1BandedTableView1PERC_LUCRO: TcxGridDBBandedColumn;
    Level1BandedTableView1VL_VENDA: TcxGridDBBandedColumn;
    fdmTabelaPreco: TFDMemTable;
    fdmTabelaPrecoID_TABELA_PRECO: TIntegerField;
    fdmTabelaPrecoDESCRICAO_TABELA_PRECO: TStringField;
    dsTabelaPreco: TDataSource;
    procedure CalcularPercentual(Sender: TField);
    procedure CalcularValor(Sender: TField);
    procedure ActConfirmarExecute(Sender: TObject);
  private
    FCalculandoPercentual: Boolean;
    FCalculandoValor: Boolean;

    procedure AtualizarTabelaPreco;
  public
    class procedure RealizarManutencaoNaTabelaPreco
      (ANotaFiscalItem: TClientDataset);
  end;

var
  MovNotaFiscalManutencaoTabelaPreco: TMovNotaFiscalManutencaoTabelaPreco;

implementation

{$R *.dfm}

uses uTabelaPreco, uMathUtils, uDatasetUtils, uFrmMessage_Process, uTabelaPrecoProxy, uProdutoProxy,
  uCadCategoriaProduto, uProduto, uSistema;

{ TMovNotaFiscalManutencaoTabelaPreco }

procedure TMovNotaFiscalManutencaoTabelaPreco.ActConfirmarExecute(
  Sender: TObject);
begin
  TValidacaoCampo.CampoPreenchido(fdmTabelaPrecoID_TABELA_PRECO);
  AtualizarTabelaPreco;
  inherited;
end;

procedure TMovNotaFiscalManutencaoTabelaPreco.AtualizarTabelaPreco;
begin
  TFrmMessage_Process.SendMessage('Atualizando tabela de pre�o');
  try
    fdmTabelaPrecoItem.DisableControls;
    fdmTabelaPrecoItem.First;
    while not fdmTabelaPrecoItem.Eof do
    begin
      TTabelaPreco.AtualizarRegistro(fdmTabelaPrecoID_TABELA_PRECO.AsInteger, fdmTabelaPrecoItemID_PRODUTO.AsInteger,
        fdmTabelaPrecoItemVL_CUSTO.AsFloat, fdmTabelaPrecoItemPERC_LUCRO.AsFloat, fdmTabelaPrecoItemVL_VENDA.AsFloat);
      fdmTabelaPrecoItem.Next;
    end;
  finally
    fdmTabelaPrecoItem.EnableControls;
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TMovNotaFiscalManutencaoTabelaPreco.CalcularPercentual(Sender: TField);
begin
  if FCalculandoValor or FCalculandoPercentual or (fdmTabelaPrecoItemVL_VENDA.AsFloat <= 0) then
    exit;

  try
    FCalculandoValor := true;

    if Sender = fdmTabelaPrecoItemVL_VENDA then
    begin
      fdmTabelaPrecoItemPERC_LUCRO.AsFloat := TMathUtils.PercentualSobreValor(
        (fdmTabelaPrecoItemVL_VENDA.AsFloat - fdmTabelaPrecoItemVL_CUSTO.AsFloat), fdmTabelaPrecoItemVL_CUSTO.AsFloat);
    end
  finally
    FCalculandoValor := false;
  end
end;

procedure TMovNotaFiscalManutencaoTabelaPreco.CalcularValor(Sender: TField);
begin
  if FCalculandoPercentual or FCalculandoValor then
    exit;

  try
    FCalculandoPercentual := true;

    if Sender = fdmTabelaPrecoItemPERC_LUCRO then
    begin
      fdmTabelaPrecoItemVL_VENDA.AsFloat := fdmTabelaPrecoItemVL_CUSTO.AsFloat +
      TMathUtils.ValorSobrePercentual(fdmTabelaPrecoItemPERC_LUCRO.AsFloat, fdmTabelaPrecoItemVL_CUSTO.AsFloat);
    end
  finally
    FCalculandoPercentual := false;
  end
end;

class procedure TMovNotaFiscalManutencaoTabelaPreco.RealizarManutencaoNaTabelaPreco(
  ANotaFiscalItem: TClientDataset);
var
  tabelaPrecoMaisUtilizada: TFDMemTable;
  tabelaPrecoItem: TTabelaPrecoItemProxy;
  produtoFilial: TProdutoProxy;
begin
  TFrmMessage_Process.SendMessage('Carregando itens da nota fiscal');
  try
    tabelaPrecoMaisUtilizada := nil;
    Application.CreateForm(TMovNotaFiscalManutencaoTabelaPreco, MovNotaFiscalManutencaoTabelaPreco);
    try
      tabelaPrecoMaisUtilizada := TTabelaPreco.GetTabelaPrecoComMaisProdutos;
      with MovNotaFiscalManutencaoTabelaPreco do
      begin
        fdmTabelaPreco.Open;

        fdmTabelaPreco.Append;
        fdmTabelaPrecoID_TABELA_PRECO.AsInteger := tabelaPrecoMaisUtilizada.FieldByName('ID').AsInteger;
        fdmTabelaPrecoDESCRICAO_TABELA_PRECO.AsString := tabelaPrecoMaisUtilizada.FieldByName('DESCRICAO').AsString;

        try
          fdmTabelaPrecoItem.Open;

          ANotaFiscalItem.DisableControls;
          ANotaFiscalItem.First;
          while not ANotaFiscalItem.Eof do
          begin
            fdmTabelaPrecoItem.Append;
            fdmTabelaPrecoItemID_PRODUTO.AsInteger := ANotaFiscalItem.FieldByName('ID_PRODUTO').AsInteger;
            fdmTabelaPrecoItemDESCRICAO.AsString := ANotaFiscalItem.FieldByName('JOIN_DESCRICAO_PRODUTO').AsString;

            tabelaPrecoItem := TTabelaPreco.GetTabelaPrecoItem(
              fdmTabelaPrecoID_TABELA_PRECO.AsInteger, fdmTabelaPrecoItemID_PRODUTO.AsInteger);

            if tabelaPrecoItem.FId > 0 then
            begin
              fdmTabelaPrecoItemVL_CUSTO.AsFloat := tabelaPrecoItem.FVlCustoImpostoOperacional;
              fdmTabelaPrecoItemPERC_LUCRO.AsFloat := tabelaPrecoItem.FPercLucro;
              fdmTabelaPrecoItemVL_VENDA.AsFloat := tabelaPrecoItem.FVlVenda;
            end
            else
            begin
              produtoFilial := TProduto.GetProdutoFilial(fdmTabelaPrecoItemID_PRODUTO.AsInteger,
                TSistema.Sistema.Filial.FID);
              try
                fdmTabelaPrecoItemVL_CUSTO.AsFloat := produtoFilial.FProdutoFilial.FVlCustoMedioImpostoOperacional;
              finally
                FreeAndNil(produtoFilial);
              end;
            end;

            fdmTabelaPrecoItem.Post;

            ANotaFiscalItem.Next;
          end;
        finally
          ANotaFiscalItem.EnableControls;
        end;
      end;
      MovNotaFiscalManutencaoTabelaPreco.ShowModal;
    finally
      FreeAndNil(MovNotaFiscalManutencaoTabelaPreco);
      FreeAndNil(tabelaPrecoMaisUtilizada);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

end.
