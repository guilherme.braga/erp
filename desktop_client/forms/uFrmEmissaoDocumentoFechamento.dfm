inherited FrmEmissaoDocumentoFechamento: TFrmEmissaoDocumentoFechamento
  Caption = 'Emiss'#227'o de Documento'
  ClientHeight = 62
  ClientWidth = 452
  ExplicitWidth = 458
  ExplicitHeight = 91
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 452
    ExplicitHeight = 0
    Height = 0
    Width = 452
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = -2
    ExplicitTop = -2
    ExplicitWidth = 452
    ExplicitHeight = 64
    Height = 64
    Width = 452
    inherited BtnConfirmar: TcxButton
      Left = 450
      Width = 0
      Height = 60
      Visible = False
      ExplicitLeft = 450
      ExplicitWidth = 0
      ExplicitHeight = 60
    end
    inherited BtnCancelar: TcxButton
      Left = 450
      Width = 0
      Height = 60
      Visible = False
      ExplicitLeft = 450
      ExplicitWidth = 0
      ExplicitHeight = 60
    end
    object cxButton1: TcxButton
      Left = 377
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActSairEmissaoDocumentos
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
    end
    object cxButton2: TcxButton
      Left = 227
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActEmitirNFCe
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 3
    end
    object cxButton3: TcxButton
      Left = 77
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActEmitirDuplicata
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 4
    end
    object cxButton4: TcxButton
      Left = 152
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActEmitirCarne
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 5
    end
    object cxButton5: TcxButton
      Left = 2
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActEmitirComanda
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 6
    end
    object cxButton6: TcxButton
      Left = 302
      Top = 2
      Width = 75
      Height = 60
      Align = alLeft
      Action = ActEmitirNFE
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 7
    end
  end
  inherited ActionList: TActionList
    Left = 206
    Top = 6
    object ActEmitirComanda: TAction
      Caption = 'Comanda F2'
      ImageIndex = 115
      OnExecute = ActEmitirComandaExecute
    end
    object ActEmitirDuplicata: TAction
      Caption = 'Duplicata F3'
      ImageIndex = 40
      OnExecute = ActEmitirDuplicataExecute
    end
    object ActEmitirCarne: TAction
      Caption = 'Carn'#234' F4'
      ImageIndex = 131
      OnExecute = ActEmitirCarneExecute
    end
    object ActEmitirNFCe: TAction
      Caption = 'NFC-e F5'
      ImageIndex = 158
      OnExecute = ActEmitirNFCeExecute
    end
    object ActEmitirNFE: TAction
      Caption = 'NF-e F6'
      ImageIndex = 157
      OnExecute = ActEmitirNFEExecute
    end
    object ActSairEmissaoDocumentos: TAction
      Caption = 'Sair Esc'
      ImageIndex = 12
      OnExecute = ActSairEmissaoDocumentosExecute
    end
  end
end
