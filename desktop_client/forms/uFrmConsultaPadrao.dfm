inherited FrmConsultaPadrao: TFrmConsultaPadrao
  BorderStyle = bsSizeable
  Caption = 'Consultar'
  ClientHeight = 516
  ClientWidth = 784
  PopupMenu = pmGridConsultaPadrao
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  ExplicitWidth = 800
  ExplicitHeight = 555
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    Top = 31
    TabOrder = 1
    ExplicitTop = 31
    ExplicitWidth = 784
    ExplicitHeight = 429
    Height = 429
    Width = 784
    object cxGridPesquisaPadrao: TcxGrid
      Left = 2
      Top = 2
      Width = 780
      Height = 425
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object Level1BandedTableView1: TcxGridDBBandedTableView
        OnDblClick = Level1BandedTableView1DblClick
        OnKeyPress = Level1BandedTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        OnEditKeyPress = Level1BandedTableView1EditKeyPress
        DataController.DataSource = dsSearch
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.ShowColumnFilterButtons = sfbAlways
        OptionsView.BandHeaders = False
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
          end>
      end
      object cxGridPesquisaPadraoLevel1: TcxGridLevel
        GridView = Level1BandedTableView1
      end
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 460
    TabOrder = 2
    ExplicitTop = 460
    ExplicitWidth = 784
    Width = 784
    inherited BtnConfirmar: TcxButton
      Left = 632
      ExplicitLeft = 632
    end
    inherited BtnCancelar: TcxButton
      Left = 707
      ExplicitLeft = 707
    end
  end
  object cxGroupBox3: TcxGroupBox [2]
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Height = 31
    Width = 784
    object cxGroupBox4: TcxGroupBox
      Left = 756
      Top = 2
      Align = alRight
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Height = 27
      Width = 26
      object cxButton1: TcxButton
        Left = 2
        Top = 2
        Width = 22
        Height = 23
        Align = alClient
        Action = ActPesquisar
        OptionsImage.Images = DmAcesso.cxImage16x16
        OptionsImage.Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        SpeedButtonOptions.CanBeFocused = False
        SpeedButtonOptions.Flat = True
        SpeedButtonOptions.Transparent = True
        TabOrder = 0
      end
    end
    object cxGroupBox5: TcxGroupBox
      Left = 2
      Top = 2
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Height = 27
      Width = 754
      object EdtConsultar: TcxTextEdit
        Left = 2
        Top = 2
        Align = alClient
        ParentFont = False
        Properties.CharCase = ecUpperCase
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 0
        OnKeyDown = EdtConsultarKeyDown
        Width = 750
      end
    end
  end
  inherited ActionList: TActionList
    Left = 94
    Top = 334
  end
  object fdmSearch: TFDMemTable
    AfterOpen = fdmSearchAfterOpen
    AfterClose = fdmSearchAfterClose
    AfterDelete = fdmSearchAfterDelete
    FieldOptions.PositionMode = poFirst
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 150
    Top = 333
  end
  object dsSearch: TDataSource
    DataSet = fdmSearch
    Left = 178
    Top = 333
  end
  object ActionList16x16: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 32
    Top = 336
    object ActPesquisar: TAction
      Hint = 'Clique sobre o bot'#227'o ou pressione Ctrl+ENTER para "Pesquisar"'
      ImageIndex = 11
      OnExecute = ActPesquisarExecute
    end
  end
  object cxGridPopupMenuPadrao: TcxGridPopupMenu
    Grid = cxGridPesquisaPadrao
    PopupMenus = <
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = PMGridPesquisa
      end>
    Left = 276
    Top = 336
  end
  object PMGridPesquisa: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = PMGridPesquisaPopup
    Left = 304
    Top = 336
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterar_Colunas_Grid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ExibirAgrupamento2: TMenuItem
      Action = ActExibirAgrupamento
    end
    object RestaurarColunas1: TMenuItem
      Action = ActRestaurarColunas
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 332
    Top = 336
    object ActAlterar_Colunas_Grid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterar_Colunas_GridExecute
    end
    object ActRestaurarColunas: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasExecute
    end
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActAbrirConsultando: TAction
      Category = 'filter'
      Caption = 'Consultar Autom'#225'ticamente'
      Hint = 'Realiza a consulta de dados ao abrir o formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActAbrirConsultandoExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
  end
  object pmGridConsultaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmGridConsultaPadraoPopup
    Left = 457
    Top = 168
    object AlterarSQL1: TMenuItem
      Action = ActAlterarSQLPesquisaPadrao
    end
    object ExibirAgrupamento1: TMenuItem
      Action = ActAbrirConsultando
    end
  end
end
