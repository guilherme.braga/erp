inherited FrmIdentificacaoDestinatarioNFCe: TFrmIdentificacaoDestinatarioNFCe
  Caption = 'Identifica'#231#227'o do Destinat'#225'rio na NFC-e'
  ClientHeight = 123
  ClientWidth = 290
  ExplicitWidth = 296
  ExplicitHeight = 152
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    PanelStyle.OfficeBackgroundKind = pobkOffice11Color
    ParentBackground = False
    ExplicitWidth = 290
    ExplicitHeight = 67
    Height = 67
    Width = 290
    object cxLabel1: TcxLabel
      Left = 13
      Top = 24
      Caption = 'CPF na Nota?'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -15
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object EdtCPF: TcxMaskEdit
      Left = 123
      Top = 22
      ParentFont = False
      Properties.EditMask = '000\.000\.000\-00;0;_'
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -15
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 1
      Text = '           '
      OnKeyDown = EdtCPFKeyDown
      Width = 155
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 67
    ExplicitTop = 67
    ExplicitWidth = 290
    Width = 290
    inherited BtnConfirmar: TcxButton
      Left = 138
      ExplicitLeft = 138
    end
    inherited BtnCancelar: TcxButton
      Left = 213
      ExplicitLeft = 213
    end
  end
  inherited ActionList: TActionList
    Left = 38
    Top = 70
  end
end
