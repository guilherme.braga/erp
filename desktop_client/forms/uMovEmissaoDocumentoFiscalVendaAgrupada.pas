unit uMovEmissaoDocumentoFiscalVendaAgrupada;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, uFrameConsultaDadosDetalheGrade, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, uVendaProxy, uUsuarioDesignControl;

type
  TMovEmissaoDocumentoFiscalVendaAgrupada = class(TFrmChildPadrao)
    ActEmitirNFE: TAction;
    ActEmitirNFCe: TAction;
    lbEmitirNFE: TdxBarLargeButton;
    lbEmitirNFCe: TdxBarLargeButton;
    procedure ActEmitirNFEExecute(Sender: TObject);
    procedure ActEmitirNFCeExecute(Sender: TObject);
  private
    FFrameConsultaDadosDetalheGradeVenda: TFrameConsultaDadosDetalheGrade;
    FParametroContaCorrente: TParametroFormulario;
    FParametroFormaPagamentoAVista: TParametroFormulario;
    FParametroCarteira: TParametroFormulario;
    FParametroHabilitarEmissaoNFCE: TParametroFormulario;
    FParametroHabilitarEmissaoNFE: TParametroFormulario;
    function GetVendasSelecionadas: String;
    function GetVendaProxy: TVendaProxy;
    procedure HabilitarBotaoEmissaoNFE;
    procedure HabilitarBotaoEmissaoNFCE;
  public
    procedure InstanciarFrames; override;
    procedure EmitirDocumentoFiscal(const ATipoDocumentoFiscal: String);
  protected
    procedure CriarParametrosFormulario(var AParametros: TListaParametroFormulario); override;
    procedure AoCriarFormulario; override;
  end;

implementation

{$R *.dfm}

uses uFrameFiltroVerticalPadrao, uFrmVisualizacaoRelatoriosAssociados, uSistema, uFiltroFormulario,
  uDateUtils, uConstantes, uDatasetUtils, db, uFrmMessage, uPessoaProxy, uRelatorioFRFiltroVerticalVenda,
  uFramePessoaVenda, uVenda, uNotaFiscalProxy, uNotaFiscal, uConstParametroFormulario, uControlsUtils,
  uFrmMessage_Process;

{ TImpEmissaoMalaDireta }



procedure TMovEmissaoDocumentoFiscalVendaAgrupada.ActEmitirNFCeExecute(Sender: TObject);
begin
  inherited;
  EmitirDocumentoFiscal(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.ActEmitirNFEExecute(Sender: TObject);
begin
  inherited;
  EmitirDocumentoFiscal(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.AoCriarFormulario;
begin
  inherited;
  HabilitarBotaoEmissaoNFE;
  HabilitarBotaoEmissaoNFCE;
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Forma de Pagamento a Vista
  FparametroFormaPagamentoAVista := TParametroFormulario.Create;
  FparametroFormaPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoAVista);

  //Parametro Conta Corrente
  FparametroContaCorrente := TParametroFormulario.Create;
  FparametroContaCorrente.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FparametroContaCorrente.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_CORRENTE;
  FparametroContaCorrente.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroContaCorrente);

  //Carteira
  FParametroCarteira := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCarteira);

  //Habilitar Emiss�o de NFCE
  FParametroHabilitarEmissaoNFCE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFCE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFCE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFCE);

  //Habilitar Emiss�o de NFE
  FParametroHabilitarEmissaoNFE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFE);
end;

function TMovEmissaoDocumentoFiscalVendaAgrupada.GetVendaProxy: TVendaProxy;
begin
  result := TVendaProxy.Create;

  result.FIdContaCorrente := FParametroContaCorrente.AsInteger;

  result.FIdFormaPagamentoDinheiro :=
    FParametroFormaPagamentoAVista.AsInteger;

  result.FIdCarteira := FParametroCarteira.AsInteger;
end;

function TMovEmissaoDocumentoFiscalVendaAgrupada.GetVendasSelecionadas: String;
var
  fieldChecked: TField;
  fieldID: TField;
begin
  fieldChecked :=
    FFrameConsultaDadosDetalheGradeVenda.fdmDados.FieldByName(TDBConstantes.ALIAS_BO_CHECKED);

  fieldID := FFrameConsultaDadosDetalheGradeVenda.fdmDados.FieldByName('ID');

  result := TDatasetUtils.ConcatenarComCondicao(fieldID, fieldChecked, TConstantes.BO_SIM);
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.HabilitarBotaoEmissaoNFCE;
begin
  ActEmitirNFCE.Visible := FParametroHabilitarEmissaoNFCE.ValorSim;
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.HabilitarBotaoEmissaoNFE;
begin
  ActEmitirNFE.Visible := FParametroHabilitarEmissaoNFE.ValorSim;
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.EmitirDocumentoFiscal(const ATipoDocumentoFiscal: String);
var
  filtro: TFiltroPadrao;
  vendasSelecionadas: String;
  idPrimeiraVendaSelecionada: Integer;
begin
  inherited;
  vendasSelecionadas := GetVendasSelecionadas;

  if vendasSelecionadas.IsEmpty then
  begin
    TFrmMessage.Information('Selecione ao menos uma venda para a emiss�o do documento fiscal.');
    exit;
  end;

  if Pos(',', vendasSelecionadas) > 0 then
  begin
    idPrimeiraVendaSelecionada := StrtoIntDef(Copy(vendasSelecionadas, 1, Pos(',', vendasSelecionadas)-1), 0);
  end
  else
  begin
    idPrimeiraVendaSelecionada := StrtoIntDef(vendasSelecionadas, 0);
  end;

  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Realizando Emiss�o da '+ATipoDocumentoFiscal);

    if ATipoDocumentoFiscal.Equals(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE) then
    begin
      TNotaFiscal.GerarNFEDeVendaAgrupada(vendasSelecionadas, GetVendaProxy);
      if TNotaFiscal.EmitirNFE(TVenda.GetIdNotaFiscalNFE(idPrimeiraVendaSelecionada)) then
      begin
        TNotaFiscal.ImprimirNotaFiscal(TVenda.GetIdNotaFiscalNFE(idPrimeiraVendaSelecionada),
          TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
      end;
    end
    else if ATipoDocumentoFiscal.Equals(TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE) then
    begin
      TNotaFiscal.GerarNFCEDeVendaAgrupada(vendasSelecionadas, '', GetVendaProxy);
      if TNotaFiscal.EmitirNFCE(TVenda.GetIdNotaFiscalNFCE(idPrimeiraVendaSelecionada)) then
      begin
        TNotaFiscal.ImprimirNotaFiscal(TVenda.GetIdNotaFiscalNFCE(idPrimeiraVendaSelecionada),
          TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
      end;
    end;
  finally
    FFrameConsultaDadosDetalheGradeVenda.ActConsultar.Execute;
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovEmissaoDocumentoFiscalVendaAgrupada.InstanciarFrames;
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
  FFrameConsultaDadosDetalheGradeVenda := TFrameConsultaDadosDetalheGrade.Create(
    cxPanelMain, Self.Name, 'Mala Direta');

  FFrameConsultaDadosDetalheGradeVenda.OcultarTituloFrame;
  FFrameConsultaDadosDetalheGradeVenda.OcultarCampoImpressao;
  FFrameConsultaDadosDetalheGradeVenda.SetInjecaoCamposNaConsulta(TDBConstantes.FIELD_BO_UNCHECKED);

  TRelatorioFRFiltroVerticalVenda.CriarFiltrosRelatorioVenda(
    FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical);

  //C�digo de NFCE
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Fiscal';
  campoFiltro.campo := 'DOCUMENTO_FISCAL';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(texto);

  campoFiltro.condicaoAberta := 'IFNULL((SELECT IF(AR_CSTAT = '''', ''A ENVIAR'', AR_CSTAT) FROM NOTA_FISCAL WHERE ID = '+
    'IF(IF(ID_NOTA_FISCAL_NFE = 0, 0, ID_NOTA_FISCAL_NFE) = 0, ID_NOTA_FISCAL_NFCE, ID_NOTA_FISCAL_NFE)), ''A ENVIAR'')';

  campoFiltro.valorPadrao := TNotaFiscalProxy.STATUS_NFE_AENVIAR;
  campoFiltro.visivel := false;
  campoFiltro.somenteLeitura := true;
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  campoFiltro :=
    FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.BuscarCampoFiltroPorNomeCampo('STATUS');
  if Assigned(campoFiltro) then
  begin
    campoFiltro.valorPadrao := STATUS_FECHADO;
    campoFiltro.visivel := false;
    campoFiltro.somenteLeitura := true;
  end;

  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeVenda.Parent := cxPanelMain;
end;

initialization
  RegisterClass(TMovEmissaoDocumentoFiscalVendaAgrupada);

finalization
  UnRegisterClass(TMovEmissaoDocumentoFiscalVendaAgrupada);

end.
