inherited CadEmpresaFilial: TCadEmpresaFilial
  Caption = 'Cadastro de Empresa/Filial'
  ExplicitWidth = 952
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object lbCodigo: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object PnDadosEmpresa: TcxGroupBox
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.TransparentBorder = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            DesignSize = (
              932
              25)
            Height = 25
            Width = 932
            object lbRazaoSocial: TLabel
              Left = 8
              Top = 4
              Width = 41
              Height = 13
              Caption = 'Empresa'
            end
            object edRazaoSocial: TgbDBTextEdit
              Left = 55
              Top = 1
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbPassword = False
              Width = 870
            end
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 57
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Width = 58
          end
          object PnFrameEmpresaFilial: TgbPanel
            Left = 2
            Top = 59
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameEmpresaFilial'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Height = 272
            Width = 932
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Empresa/Filial'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspEmpresa'
    RemoteServer = DmConnection.dspEmpresaFilial
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsDatafdqFilial: TDataSetField
      FieldName = 'fdqFilial'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42284.383730208330000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsFilial: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqFilial
    Params = <>
    OnNewRecord = cdsFilialNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 784
    Top = 80
    object cdsFilialID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsFilialFANTASIA: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'FANTASIA'
      Origin = 'FANTASIA'
      Required = True
      Size = 80
    end
    object cdsFilialRAZAO_SOCIAL: TStringField
      DisplayLabel = 'Raz'#227'o Social'
      FieldName = 'RAZAO_SOCIAL'
      Origin = 'RAZAO_SOCIAL'
      Required = True
      Size = 80
    end
    object cdsFilialCNPJ: TStringField
      FieldName = 'CNPJ'
      Origin = 'CNPJ'
      Required = True
      Size = 14
    end
    object cdsFilialIE: TStringField
      DisplayLabel = 'Inscri'#231#227'o Estadual'
      FieldName = 'IE'
      Origin = 'IE'
      Required = True
    end
    object cdsFilialDT_FUNDACAO: TDateField
      DisplayLabel = 'Dt. Funda'#231#227'o'
      FieldName = 'DT_FUNDACAO'
      Origin = 'DT_FUNDACAO'
    end
    object cdsFilialID_EMPRESA: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'ID_EMPRESA'
      Origin = 'ID_EMPRESA'
    end
    object cdsFilialNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
    end
    object cdsFilialID_ZONEAMENTO: TIntegerField
      DisplayLabel = 'C'#243'dido do Zoneamento'
      FieldName = 'ID_ZONEAMENTO'
      Origin = 'ID_ZONEAMENTO'
    end
    object cdsFilialJOIN_DESCRICAO_ZONEAMENTO: TStringField
      DisplayLabel = 'Zoneamento'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsFilialID_CIDADE: TIntegerField
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
    end
    object cdsFilialLOGRADOURO: TStringField
      DisplayLabel = 'Logradouro'
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      Size = 100
    end
    object cdsFilialNUMERO: TStringField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object cdsFilialCOMPLEMENTO: TStringField
      DisplayLabel = 'Complemento'
      FieldName = 'COMPLEMENTO'
      Origin = 'COMPLEMENTO'
      Size = 50
    end
    object cdsFilialBAIRRO: TStringField
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      Size = 100
    end
    object cdsFilialCEP: TStringField
      FieldName = 'CEP'
      Origin = 'CEP'
      Size = 15
    end
    object cdsFilialJOIN_DESCRICAO_CIDADE: TStringField
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsFilialREGIME_TRIBUTARIO: TStringField
      DefaultExpression = 'SIMPLES'
      DisplayLabel = 'Regime Tribut'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO'
      Origin = 'REGIME_TRIBUTARIO'
      Size = 45
    end
    object cdsFilialTELEFONE: TStringField
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE'
      Origin = 'TELEFONE'
      Size = 11
    end
    object cdsFilialTIPO_EMPRESA: TStringField
      DisplayLabel = 'Tipo da Empresa'
      FieldName = 'TIPO_EMPRESA'
      Origin = 'TIPO_EMPRESA'
      Size = 15
    end
    object cdsFilialCRT: TIntegerField
      FieldName = 'CRT'
      Origin = 'CRT'
    end
    object cdsFilialBO_MATRIZ: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Matriz'
      FieldName = 'BO_MATRIZ'
      Origin = 'BO_MATRIZ'
      FixedChar = True
      Size = 1
    end
    object cdsFilialBO_CONTRIBUINTE_ICMS: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Contribuinte ICMS'
      FieldName = 'BO_CONTRIBUINTE_ICMS'
      Origin = 'BO_CONTRIBUINTE_ICMS'
      FixedChar = True
      Size = 1
    end
    object cdsFilialBO_CONTRIBUINTE_IPI: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Contribuinte IPI'
      FieldName = 'BO_CONTRIBUINTE_IPI'
      Origin = 'BO_CONTRIBUINTE_IPI'
      FixedChar = True
      Size = 1
    end
    object cdsFilialEMAIL: TStringField
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 255
    end
    object cdsFilialSITE: TStringField
      DisplayLabel = 'Site'
      FieldName = 'SITE'
      Origin = 'SITE'
      Size = 255
    end
    object cdsFilialID_CNAE: TIntegerField
      DisplayLabel = 'C'#243'digo CNAE'
      FieldName = 'ID_CNAE'
      Origin = 'ID_CNAE'
    end
    object cdsFilialIM: TStringField
      DisplayLabel = 'Inscri'#231#227'o Municipal'
      FieldName = 'IM'
      Origin = 'IM'
      Size = 15
    end
    object cdsFilialJOIN_SEQUENCIA_CNAE: TStringField
      DisplayLabel = 'CNAE'
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object cdsFilialJOIN_DESCRICAO_CNAE: TStringField
      DisplayLabel = 'Descri'#231#227'o CNAE'
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
