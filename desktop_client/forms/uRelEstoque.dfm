inherited RelEstoque: TRelEstoque
  Caption = 'Relat'#243'rio de Estoque'
  ClientHeight = 426
  ClientWidth = 928
  ExplicitWidth = 944
  ExplicitHeight = 465
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 928
    ExplicitWidth = 928
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    ExplicitWidth = 928
    ExplicitHeight = 324
    Height = 324
    Width = 928
    inherited cxGridRelatorios: TcxGrid
      Width = 543
      Height = 314
      ExplicitWidth = 543
      ExplicitHeight = 314
    end
    inherited gbPanel1: TgbPanel
      ExplicitHeight = 320
      Height = 320
      inherited vgFiltros: TcxVerticalGrid
        Height = 316
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitHeight = 316
        Version = 1
        inherited vcatFiltrosPadrao: TcxCategoryRow
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object rowProduto: TcxEditorRow
          Properties.Caption = 'Produto'
          Properties.Hint = 'ID'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowProdutoEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object rowTipo: TcxEditorRow
          Properties.Caption = 'Tipo'
          Properties.Hint = 'TIPO'
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsEditFixedList
          Properties.EditProperties.Items.Strings = (
            'ENTRADA'
            'SAIDA')
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object rowUnidadeEstoque: TcxEditorRow
          Properties.Caption = 'Unidade de Estoque'
          Properties.Hint = 'ID_UNIDADE_ESTOQUE'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowUnidadeEstoqueEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
        object rowModelo: TcxEditorRow
          Properties.Caption = 'Modelo'
          Properties.Hint = 'ID_MODELO'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowModeloEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 4
          ParentID = 0
          Index = 3
          Version = 1
        end
        object rowLinha: TcxEditorRow
          Properties.Caption = 'Linha'
          Properties.Hint = 'ID_LINHA'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowLinhaEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 5
          ParentID = 0
          Index = 4
          Version = 1
        end
        object rowSubCategoria: TcxEditorRow
          Properties.Caption = 'Sub Categoria'
          Properties.Hint = 'ID_SUB_CATEGORIA'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowSubCategoriaEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 6
          ParentID = 0
          Index = 5
          Version = 1
        end
        object rowCategoria: TcxEditorRow
          Properties.Caption = 'Categoria'
          Properties.Hint = 'ID_CATEGORIA'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowCategoriaEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 7
          ParentID = 0
          Index = 6
          Version = 1
        end
        object rowMarca: TcxEditorRow
          Properties.Caption = 'Marca'
          Properties.Hint = 'ID_MARCA'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowMarcaEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 8
          ParentID = 0
          Index = 7
          Version = 1
        end
        object rowSubGrupoProduto: TcxEditorRow
          Properties.Caption = 'Sub Grupo Produto'
          Properties.Hint = 'ID_SUB_GRUPO_PRODUTO'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowSubGrupoProdutoEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 9
          ParentID = 0
          Index = 8
          Version = 1
        end
        object rowGrupoProduto: TcxEditorRow
          Properties.Caption = 'Grupo de Produto'
          Properties.Hint = 'ID_GRUPO_PRODUTO'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = rowGrupoProdutoEditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 10
          ParentID = 0
          Index = 9
          Version = 1
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 292
    Top = 267
  end
  inherited dxBarManager: TdxBarManager
    Left = 264
    Top = 267
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarFiltro: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited FdmFiltros: TFDMemTable
    object FdmFiltrosID_GRUPO_PRODUTO: TIntegerField
      Tag = 1
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
      Required = True
    end
    object FdmFiltrosID_SUB_GRUPO_PRODUTO: TIntegerField
      Tag = 1
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      Required = True
    end
    object FdmFiltrosID_MARCA: TIntegerField
      Tag = 1
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
      Required = True
    end
    object FdmFiltrosID_CATEGORIA: TIntegerField
      Tag = 1
      DisplayLabel = 'Categoria'
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
      Required = True
    end
    object FdmFiltrosID_SUB_CATEGORIA: TIntegerField
      Tag = 1
      DisplayLabel = 'Sub Categoria'
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
      Required = True
    end
    object FdmFiltrosID_LINHA: TIntegerField
      Tag = 1
      DisplayLabel = 'Linha'
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
      Required = True
    end
    object FdmFiltrosID_MODELO: TIntegerField
      Tag = 1
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
      Required = True
    end
    object FdmFiltrosID_UNIDADE_ESTOQUE: TIntegerField
      Tag = 1
      DisplayLabel = 'UN'
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
      Required = True
    end
    object FdmFiltrosID: TIntegerField
      Tag = 1
      DisplayLabel = 'Produto'
      FieldName = 'ID'
    end
    object FdmFiltrosTIPO: TStringField
      Tag = 1
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 30
    end
    object FdmFiltrosCODIGO_BARRA: TIntegerField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO_BARRA'
    end
    object FdmFiltrosDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
    end
  end
end
