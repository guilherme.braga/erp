unit uRelOrdemServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxButtonEdit, cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, uGBClientDataset, dxBar, cxBarEditItem, dxBarExtItems,
  System.Actions, Vcl.ActnList, cxVGrid, cxInplaceContainer, uGBPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxGroupBox, dxRibbon;

type
  TRelOrdemServico = class(TFrmRelatorioFRPadrao)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RelOrdemServico: TRelOrdemServico;

implementation

{$R *.dfm}

end.
