inherited FrmBackup: TFrmBackup
  Caption = 'Backup'
  ClientHeight = 114
  ClientWidth = 230
  ExplicitWidth = 236
  ExplicitHeight = 143
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 195
    ExplicitHeight = 43
    Height = 48
    Width = 230
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 53
      Height = 13
      Caption = 'Hr. Inicio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 4
      Top = 23
      Width = 69
      Height = 13
      Caption = 'Hr. Termino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbHT: TLabel
      Left = 82
      Top = 23
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbHI: TLabel
      Left = 82
      Top = 4
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 48
    ExplicitTop = 43
    ExplicitWidth = 195
    ExplicitHeight = 66
    Height = 66
    Width = 230
    inherited BtnConfirmar: TcxButton
      Left = 78
      Height = 62
      ExplicitLeft = 43
      ExplicitHeight = 62
    end
    inherited BtnCancelar: TcxButton
      Left = 153
      Height = 62
      ExplicitLeft = 118
      ExplicitHeight = 62
    end
    object cxButton1: TcxButton
      Left = 3
      Top = 2
      Width = 75
      Height = 62
      Align = alRight
      Action = ActCopiarUltimoBackup
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
      ExplicitLeft = 4
    end
  end
  inherited ActionList: TActionList
    Left = 150
    Top = 65534
    inherited ActConfirmar: TAction
      Caption = 'Gerar Backup'
      Hint = 'Executa a gera'#231#227'o de backup do sistema'
      ImageIndex = 149
    end
    inherited ActCancelar: TAction
      Caption = 'Sair Esc'
      ImageIndex = 12
    end
    object ActCopiarUltimoBackup: TAction
      Caption = 'Salvar C'#243'pia'
      Hint = 
        'Possibilita salvar uma c'#243'pia do backup no diret'#243'rio de sua prefe' +
        'r'#234'ncia'
      ImageIndex = 19
      OnExecute = ActCopiarUltimoBackupExecute
    end
  end
  object sdSalvarBackup: TJvSaveDialog
    FileName = 'Backup.sql'
    Filter = 'Backup|*.sql'
    Height = 0
    Width = 0
    Left = 208
  end
end
