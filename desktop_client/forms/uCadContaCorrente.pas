unit uCadContaCorrente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  uGBDBTextEditPK, cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit,
  cxMaskEdit, cxButtonEdit, cxTextEdit, uGBDBTextEdit,
  Vcl.StdCtrls, cxCheckBox, uGBDBCheckBox, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, uGBPanel;

type
  TCadContaCorrente = class(TFrmCadastro_Padrao)
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    gbDBCheckBox1: TgbDBCheckBox;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataDH_CADASTRO: TDateTimeField;
    gbDBBlobEdit1: TgbDBBlobEdit;
    Label3: TLabel;
    cdsDataBO_VISUALIZA_FLUXO_CAIXA: TStringField;
    gbDBCheckBox2: TgbDBCheckBox;
    procedure cdsDataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

procedure TCadContaCorrente.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
end;

initialization
  RegisterClass(TCadContaCorrente);
finalization
  UnRegisterClass(TCadContaCorrente);

end.
