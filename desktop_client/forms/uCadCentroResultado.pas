unit uCadCentroResultado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  cxMaskEdit, cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit,
  uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxButtonEdit,
  uGBDBButtonEditFK, System.Generics.Collections, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, cxCheckBox, uGBDBCheckBox, dxPSCore, dxPScxCommon, cxSplitter, JvExControls,
  JvButton, JvTransparentButton, uGBPanel;

type
  TCadCentroResultado = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_GRUPO_CENTRO_RESULTADO: TIntegerField;
    Label1: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label7: TLabel;
    Label2: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    Label3: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    cdsDataJOIN_DESCRICAO_GRUPO_CENTRO_RESULTADO: TStringField;
    gbDBCheckBox2: TgbDBCheckBox;
    cdsDataBO_EXIBIR_EXTRATO_PLANO_CONTA: TStringField;
  private

  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TCadCentroResultado);
finalization
  UnRegisterClass(TCadCentroResultado);

end.
