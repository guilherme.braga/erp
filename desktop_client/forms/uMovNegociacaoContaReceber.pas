unit uMovNegociacaoContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxLabel,
  JvExControls, JvButton, JvTransparentButton, cxButtonEdit, cxDBEdit,
  uGBDBButtonEditFK, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, uGBPanel,
  Vcl.DBCtrls, cxCurrencyEdit, uGBDBValorComPercentual, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  cxBlobEdit, uGBDBBlobEdit, cxSplitter;

type
  TMovNegociacaoContaReceber = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataVL_TOTAL: TFMTBCDField;
    cdsDataVL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_DESCONTO: TFMTBCDField;
    cdsDataVL_LIQUIDO: TFMTBCDField;
    cdsDataVL_ENTRADA: TFMTBCDField;
    cdsDataDT_PARCELA_INICIAL: TDateField;
    cdsDataDT_COMPETENCIA_INICIAL: TDateField;
    cdsDataSTATUS: TStringField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataID_FORMA_PAGAMENTO: TIntegerField;
    cdsDataID_PLANO_PAGAMENTO: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_CARTEIRA: TIntegerField;
    cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField;
    cdsDataJOIN_NOME_USUARIO: TStringField;
    cdsNegociacaoContaReceberParcela: TGBClientDataSet;
    cdsDatafdqNegociacaoContaReceberAnteri: TDataSetField;
    cdsDatafdqNegociacaoContaReceberGerado: TDataSetField;
    cdsDatafdqNegociacaoContaReceberParcel: TDataSetField;
    cdsNegociacaoContaReceberTituloGerado: TGBClientDataSet;
    cdsNegociacaoContaReceberTituloAnterior: TGBClientDataSet;
    cdsNegociacaoContaReceberParcelaID: TAutoIncField;
    cdsNegociacaoContaReceberParcelaDOCUMENTO: TStringField;
    cdsNegociacaoContaReceberParcelaDESCRICAO: TStringField;
    cdsNegociacaoContaReceberParcelaVL_TITULO: TFMTBCDField;
    cdsNegociacaoContaReceberParcelaQT_PARCELA: TIntegerField;
    cdsNegociacaoContaReceberParcelaNR_PARCELA: TIntegerField;
    cdsNegociacaoContaReceberParcelaSEQUENCIA: TStringField;
    cdsNegociacaoContaReceberParcelaDT_VENCIMENTO: TDateField;
    cdsNegociacaoContaReceberParcelaDT_COMPETENCIA: TDateField;
    cdsNegociacaoContaReceberParcelaDT_DOCUMENTO: TDateField;
    cdsNegociacaoContaReceberParcelaID_NEGOCIACAO_CONTA_RECEBER: TIntegerField;
    cdsNegociacaoContaReceberTituloGeradoID: TAutoIncField;
    cdsNegociacaoContaReceberTituloGeradoID_NEGOCIACAO_CONTA_RECEBER: TIntegerField;
    cdsNegociacaoContaReceberTituloGeradoID_CONTA_RECEBER: TIntegerField;
    cdsNegociacaoContaReceberTituloAnteriorID: TAutoIncField;
    cdsNegociacaoContaReceberTituloAnteriorID_NEGOCIACAO_CONTA_RECEBER: TIntegerField;
    cdsNegociacaoContaReceberTituloAnteriorID_CONTA_RECEBER: TIntegerField;
    PnTitulosParaNegociacao: TgbPanel;
    PnSelecionarTodos: TgbPanel;
    JvTransparentButton1: TJvTransparentButton;
    PnInformacoesGeral: TgbPanel;
    PnParcelas: TgbPanel;
    gridParcela: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedTableView1ID: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DT_COMPETENCIA: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DT_DOCUMENTO: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    gbPanel2: TgbPanel;
    gbPanel1: TgbPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lbContaCorrente: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    labelObservacao: TLabel;
    edtDescCentroResultado: TgbDBTextEdit;
    edtCentroResultado: TgbDBButtonEditFK;
    edtDescContaAnalise: TgbDBTextEdit;
    edtPessoa: TgbDBButtonEditFK;
    edtDescPessoa: TgbDBTextEdit;
    edtFormaPagamento: TgbDBButtonEditFK;
    edtDescFormaPagamento: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    edtContaCorrente: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit6: TgbDBTextEdit;
    edtDtInicial: TgbDBDateEdit;
    edtDtCompetencia: TgbDBDateEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    Label13: TLabel;
    gbDBTextEdit7: TgbDBTextEdit;
    Label14: TLabel;
    edDtDocumento: TgbDBDateEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    gbPanel3: TgbPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    ePkCodig: TgbDBTextEditPK;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBDateEdit2: TgbDBDateEdit;
    descProcesso: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    gbDBTextEdit9: TgbDBTextEdit;
    gbDBTextEdit10: TgbDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

Initialization
  RegisterClass(TMovNegociacaoContaReceber);

Finalization
  UnRegisterClass(TMovNegociacaoContaReceber);

end.
