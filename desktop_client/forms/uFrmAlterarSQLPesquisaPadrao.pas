unit uFrmAlterarSQLPesquisaPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxTextEdit,
  cxMemo, cxDBEdit, uGBDBMemo;

type
  TFrmAlterarSQLPesquisaPadrao = class(TFrmModalPadrao)
    mmSQLPesquisaPadrao: TcxMemo;
    procedure ActConfirmarExecute(Sender: TObject);
  private
    FIdentificador: String;
    FCodigoUsuario: Integer;
    procedure MostrarSQLNoFormulario;
  public
    class function AlterarSQLPesquisaPadrao(const ACodigoUsuario: Integer; AIdentificador: String): Boolean;
  protected
    property codigoUsuario: Integer read FCodigoUsuario write FCodigoUsuario;
    property identificador: String read FIdentificador write FIdentificador;
  end;

var
  FrmAlterarSQLPesquisaPadrao: TFrmAlterarSQLPesquisaPadrao;

implementation

{$R *.dfm}

uses uDmConnection;

procedure TFrmAlterarSQLPesquisaPadrao.ActConfirmarExecute(Sender: TObject);
begin
  DmConnection.ServerMethodsClient.SetSQLPesquisaPadrao(Identificador, CodigoUsuario, mmSQLPesquisaPadrao.Lines.Text);
  inherited;
end;

class function TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(const ACodigoUsuario: Integer; AIdentificador: String): Boolean;
begin
  Application.CreateForm(TFrmAlterarSQLPesquisaPadrao, FrmAlterarSQLPesquisaPadrao);
  try
    FrmAlterarSQLPesquisaPadrao.Identificador := AIdentificador;
    FrmAlterarSQLPesquisaPadrao.CodigoUsuario := ACodigoUsuario;
    FrmAlterarSQLPesquisaPadrao.MostrarSQLNoFormulario;
    FrmAlterarSQLPesquisaPadrao.ShowModal;
    result := FrmAlterarSQLPesquisaPadrao.result = MCONFIRMED;
  finally
    FreeAndNil(FrmAlterarSQLPesquisaPadrao);
  end;
end;

procedure TFrmAlterarSQLPesquisaPadrao.MostrarSQLNoFormulario;
begin
  mmSQLPesquisaPadrao.Lines.Text := DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(Identificador, CodigoUsuario);
end;

end.
