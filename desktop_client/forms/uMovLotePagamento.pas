unit uMovLotePagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBPanel, Vcl.StdCtrls,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, uFrameDetailPadrao, uFrameQuitacao,
   uFrameQuitacaoLotePagamento, cxBlobEdit, uGBDBBlobEdit,
  uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit,
  cxCurrencyEdit, dxBarExtItems, cxBarEditItem, JvExControls, JvButton,
  JvTransparentButton, cxButtonEdit, uGBDBButtonEditFK, uFiltroFormulario,
  cxSplitter, cxLabel, Vcl.DBCtrls, Data.FireDACJSONReflect, uUsuarioDesignControl,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, Generics.Collections;

type
  TMovLotePagamento = class(TFrmCadastro_Padrao)
    pkCodigo: TgbDBTextEditPK;
    labelCodigo: TLabel;
    edtDtCadastro: TgbDBDateEdit;
    edtDtEfetivacao: TgbDBDateEdit;
    edtSituacao: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    descProcesso: TgbDBTextEdit;
    edtChaveProcesso: TgbDBTextEdit;
    pmSelecaoTitulos: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    gpmSelecaoTitulos: TcxGridPopupMenu;
    alSelecaoTitulos: TActionList;
    ActSalvarConfiguracoesTitulos: TAction;
    actRestaurarColunasTitulos: TAction;
    actAlterarColunasGridTitulos: TAction;
    actExibirAgrupamentoTitulos: TAction;
    actAlterarSQLPesquisaPadraoTitulos: TAction;
    AlterarSQL2: TMenuItem;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataSTATUS: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataDH_EFETIVACAO: TDateTimeField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_USUARIO: TStringField;
    cdsDataJOIN_CHAVE_PROCESSO: TStringField;
    cdsDatafdqLotePagamentoQuitacao: TDataSetField;
    cdsDatafdqLotePagamentoTitulo: TDataSetField;
    dsLotePagamentoTitulo: TDataSource;
    cdsLotePagamentoTitulo: TGBClientDataSet;
    cdsLotePagamentoQuitacao: TGBClientDataSet;
    dsLotePagamentoQuitacao: TDataSource;
    cdsLotePagamentoTituloID: TAutoIncField;
    cdsLotePagamentoTituloVL_ACRESCIMO: TFMTBCDField;
    cdsLotePagamentoTituloVL_DESCONTO: TFMTBCDField;
    cdsLotePagamentoTituloID_LOTE_PAGAMENTO: TIntegerField;
    cdsLotePagamentoTituloID_CONTA_PAGAR: TIntegerField;
    cdsLotePagamentoTituloDH_CADASTRO: TDateTimeField;
    cdsLotePagamentoTituloDOCUMENTO: TStringField;
    cdsLotePagamentoTituloDESCRICAO: TStringField;
    cdsLotePagamentoTituloVL_TITULO: TFMTBCDField;
    cdsLotePagamentoTituloVL_QUITADO: TFMTBCDField;
    cdsLotePagamentoTituloVL_ABERTO: TFMTBCDField;
    cdsLotePagamentoTituloQT_PARCELA: TIntegerField;
    cdsLotePagamentoTituloNR_PARCELA: TIntegerField;
    cdsLotePagamentoTituloDT_VENCIMENTO: TDateField;
    cdsLotePagamentoTituloSEQUENCIA: TStringField;
    cdsLotePagamentoTituloBO_VENCIDO: TStringField;
    cdsLotePagamentoTituloOBSERVACAO: TBlobField;
    cdsLotePagamentoTituloID_PESSOA: TIntegerField;
    cdsLotePagamentoTituloID_CONTA_ANALISE: TIntegerField;
    cdsLotePagamentoTituloID_CENTRO_RESULTADO: TIntegerField;
    cdsLotePagamentoTituloSTATUS: TStringField;
    cdsLotePagamentoTituloID_CHAVE_PROCESSO: TIntegerField;
    cdsLotePagamentoTituloID_FORMA_PAGAMENTO: TIntegerField;
    cdsLotePagamentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField;
    cdsLotePagamentoTituloDT_COMPETENCIA: TDateField;
    cdsLotePagamentoTituloID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsLotePagamentoTituloTP_DOCUMENTO_ORIGEM: TStringField;
    cdsLotePagamentoTituloVL_ACRESCIMO_CP: TFMTBCDField;
    cdsLotePagamentoTituloVL_DESCONTO_CP: TFMTBCDField;
    cdsLotePagamentoQuitacaoID: TAutoIncField;
    cdsLotePagamentoQuitacaoOBSERVACAO: TBlobField;
    cdsLotePagamentoQuitacaoID_TIPO_QUITACAO: TIntegerField;
    cdsLotePagamentoQuitacaoDT_QUITACAO: TDateField;
    cdsLotePagamentoQuitacaoID_CONTA_CORRENTE: TIntegerField;
    cdsLotePagamentoQuitacaoVL_DESCONTO: TFMTBCDField;
    cdsLotePagamentoQuitacaoVL_ACRESCIMO: TFMTBCDField;
    cdsLotePagamentoQuitacaoVL_QUITACAO: TFMTBCDField;
    cdsLotePagamentoQuitacaoNR_ITEM: TIntegerField;
    cdsLotePagamentoQuitacaoPERC_ACRESCIMO: TBCDField;
    cdsLotePagamentoQuitacaoPERC_DESCONTO: TBCDField;
    cdsLotePagamentoQuitacaoVL_TOTAL: TFMTBCDField;
    cdsLotePagamentoQuitacaoID_CONTA_ANALISE: TIntegerField;
    cdsLotePagamentoQuitacaoID_LOTE_PAGAMENTO: TIntegerField;
    labelProcesso: TLabel;
    labelSituacao: TLabel;
    labelDtEfetivacao: TLabel;
    labelDtCadastro: TLabel;
    labelObservacao: TLabel;
    cdsLotePagamentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    actEfetivar: TAction;
    actReabrir: TAction;
    lbEfetivar: TdxBarLargeButton;
    lbReabrir: TdxBarLargeButton;
    cdsLotePagamentoTituloSUM_VL_ABERTO_CP: TAggregateField;
    cdsLotePagamentoQuitacaoSUM_VL_TOTAL_QUITACAO: TAggregateField;
    barFiltrosLote: TdxBar;
    filtrosTitulos: TFDMemTable;
    filtrosTitulosID_PESSOA: TIntegerField;
    filtrosTitulosDT_VENCIMENTO_INICIAL: TDateField;
    filtrosTitulosDT_VENCIMENTO_FINAL: TDateField;
    actSelecionarTodos: TAction;
    actRemoverTodos: TAction;
    panelTotalizadores: TgbPanel;
    labelVlAbertoCP: TLabel;
    edtVlAbertoTitulos: TgbDBTextEdit;
    EdtConsultaEfetivacaoInicio: TcxBarEditItem;
    EdtConsultaEfetivacaoFim: TcxBarEditItem;
    EdtConsultaSituacao: TcxBarEditItem;
    lbConsultaVencimento: TdxBarStatic;
    EdtConsultaValor: TcxBarEditItem;
    fdmSelecaoTitulos: TFDMemTable;
    dsSelecaoTitulos: TDataSource;
    actFiltrarSelecaoTitulos: TAction;
    dsFiltrosTitulos: TDataSource;
    filtrosTitulosDOCUMENTO: TStringField;
    filtrosTitulosJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    pmLotePagamentoTitulos: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    alLotePagamentoTitulo: TActionList;
    actSalvarConfiguracaoLoteTitulos: TAction;
    actRestaurarColunasLoteTitulos: TAction;
    actAlterarRotuloLoteTitulos: TAction;
    actExibirAgrupamentoLoteTitulos: TAction;
    actAlterarSQLLoteTitulos: TAction;
    gpmLotePagamentoTitulo: TcxGridPopupMenu;
    cdsLotePagamentoQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsLotePagamentoQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField;
    cdsLotePagamentoQuitacaoIC_VL_PAGAMENTO: TFloatField;
    cdsDataVL_QUITADO: TFMTBCDField;
    cdsDataVL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_DESCONTO: TFMTBCDField;
    cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField;
    cdsDataVL_ABERTO: TFMTBCDField;
    cdsDataCC_VL_TROCO_DIFERENCA: TStringField;
    lbTrocoDiferencaLote: TDBText;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label19: TLabel;
    edVlRecebido: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    cdsLotePagamentoQuitacaoDOCUMENTO: TStringField;
    cdsLotePagamentoQuitacaoDESCRICAO: TStringField;
    cdsLotePagamentoTituloDT_DOCUMENTO: TDateField;
    cdsLotePagamentoTituloID_CONTA_CORRENTE: TIntegerField;
    cdsLotePagamentoTituloID_FILIAL: TIntegerField;
    cdsLotePagamentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    cdsLotePagamentoQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    cdsLotePagamentoQuitacaoCHEQUE_SACADO: TStringField;
    cdsLotePagamentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    cdsLotePagamentoQuitacaoCHEQUE_BANCO: TStringField;
    cdsLotePagamentoQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    cdsLotePagamentoQuitacaoCHEQUE_AGENCIA: TStringField;
    cdsLotePagamentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    cdsLotePagamentoQuitacaoCHEQUE_NUMERO: TIntegerField;
    pageControlPrincipal: TcxPageControl;
    tsTitulos: TcxTabSheet;
    tsQuitacao: TcxTabSheet;
    JvDesignPanel1: TJvDesignPanel;
    cxGroupBox1: TcxGroupBox;
    PnFrameQuitacao: TgbPanel;
    pnTitulos: TgbPanel;
    panelFiltro: TgbPanel;
    labelPessoa: TLabel;
    LabelDtVencimento: TLabel;
    labelDocumento: TLabel;
    btnFiltrar: TJvTransparentButton;
    Label1: TLabel;
    Label2: TLabel;
    edtDtVencimentoInicio: TgbDBDateEdit;
    edtDescPessoa: TgbDBTextEdit;
    edtDocumento: TgbDBTextEdit;
    edtVencimentoFim: TgbDBDateEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbPanel2: TgbPanel;
    gridTitulos: TcxGrid;
    ViewSelecaoTitulos: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    gbPanel1: TgbPanel;
    GridLotePagamentoTitulos: TcxGrid;
    ViewLotePagamentoTitulos: TcxGridDBBandedTableView;
    ViewLotePagamentoTitulosID: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_ACRESCIMO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_DESCONTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_LOTE_PAGAMENTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_CONTA_PAGAR: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosDH_CADASTRO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosDOCUMENTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosDESCRICAO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_TITULO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_QUITADO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_ABERTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosQT_PARCELA: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosNR_PARCELA: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosDT_VENCIMENTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosSEQUENCIA: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosBO_VENCIDO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosOBSERVACAO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_PESSOA: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_CONTA_ANALISE: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_CENTRO_RESULTADO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosSTATUS: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_CHAVE_PROCESSO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_FORMA_PAGAMENTO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_QUITADO_LIQUIDO: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosDT_COMPETENCIA: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosID_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosTP_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_ACRESCIMO_CP: TcxGridDBBandedColumn;
    ViewLotePagamentoTitulosVL_DESCONTO_CP: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    cdsLotePagamentoTituloVL_JUROS: TFMTBCDField;
    cdsLotePagamentoTituloPERC_JUROS: TFMTBCDField;
    cdsLotePagamentoTituloVL_MULTA: TFMTBCDField;
    cdsLotePagamentoTituloPERC_MULTA: TFMTBCDField;
    cdsLotePagamentoTituloJOIN_NOME_PESSOA: TStringField;
    gbPanel3: TgbPanel;
    cxLabel3: TcxLabel;
    gbPanel4: TgbPanel;
    BtnSelecionarTodosRegistros: TJvTransparentButton;
    gbPanel5: TgbPanel;
    cxLabel1: TcxLabel;
    gbPanel6: TgbPanel;
    JvTransparentButton3: TJvTransparentButton;
    cdsLotePagamentoQuitacaoVL_JUROS: TFMTBCDField;
    cdsLotePagamentoQuitacaoPERC_JUROS: TFMTBCDField;
    cdsLotePagamentoQuitacaoVL_MULTA: TFMTBCDField;
    cdsLotePagamentoQuitacaoPERC_MULTA: TFMTBCDField;
    cdsLotePagamentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    cdsLotePagamentoQuitacaoID_USUARIO_BAIXA: TIntegerField;
    cdsLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsLotePagamentoQuitacaoNewRecord(DataSet: TDataSet);
    procedure ActSalvarConfiguracoesTitulosExecute(Sender: TObject);
    procedure actRestaurarColunasTitulosExecute(Sender: TObject);
    procedure actAlterarColunasGridTitulosExecute(Sender: TObject);
    procedure actExibirAgrupamentoTitulosExecute(Sender: TObject);
    procedure actAlterarSQLPesquisaPadraoTitulosExecute(Sender: TObject);
    procedure actEfetivarExecute(Sender: TObject);
    procedure actReabrirExecute(Sender: TObject);
    procedure actSelecionarTodosExecute(Sender: TObject);
    procedure actFiltrarSelecaoTitulosExecute(Sender: TObject);
    procedure actSalvarConfiguracaoLoteTitulosExecute(Sender: TObject);
    procedure actRestaurarColunasLoteTitulosExecute(Sender: TObject);
    procedure actAlterarRotuloLoteTitulosExecute(Sender: TObject);
    procedure actExibirAgrupamentoLoteTitulosExecute(Sender: TObject);
    procedure actAlterarSQLLoteTitulosExecute(Sender: TObject);
    procedure cdsLotePagamentoQuitacaoVL_QUITACAOChange(Sender: TField);
    procedure CalcularPercentual(Sender: TField);
    procedure CalcularValor(Sender: TField);
    procedure cdsLotePagamentoQuitacaoAfterEdit(DataSet: TDataSet);
    procedure cdsLotePagamentoQuitacaoAfterDelete(DataSet: TDataSet);
    procedure cdsLotePagamentoQuitacaoAfterPost(DataSet: TDataSet);
    procedure actRemoverTodosExecute(Sender: TObject);
    procedure ViewLotePagamentoTitulosEditKeyDown(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure ViewSelecaoTitulosEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewLotePagamentoTitulosDblClick(Sender: TObject);
    procedure ViewSelecaoTitulosDblClick(Sender: TObject);
    procedure CalcularValoresAPartirDoTotalQuitacao(Sender: TField);
    procedure cdsDataCalcFields(DataSet: TDataSet);
    procedure cdsDataAfterEdit(DataSet: TDataSet);
    procedure cdsLotePagamentoTituloAfterDelete(DataSet: TDataSet);
    procedure cdsLotePagamentoTituloAfterPost(DataSet: TDataSet);
    procedure cdsLotePagamentoTituloBeforeDelete(DataSet: TDataSet);
    procedure cdsLotePagamentoQuitacaoDT_QUITACAOChange(Sender: TField);
    procedure cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
  private
    FCalculandoTotalQuitacaoValor: Boolean;
    FCalculandoPercentual : Boolean;
    FCalculandoValor      : Boolean;

    FParametroLancarQuitacaoAutomaticaPorTitulo: TParametroFormulario;
    FParametroTipoQuitacaoDinheiro: TParametroFormulario;

    procedure HabilitarBotaoReabrir;
    procedure HabilitarBotaoEfetivar;
    function GetWhereBundleSelecao: TFiltroPadrao;
    procedure AbrirDatasetSelecaoTitulo;
    function PegarIdContaPagarSelecionados: String;
    procedure InserirTituloNoLotePagamento;
    procedure RemoverTituloDoLotePagamento;
    procedure RemoverDaConsultaOsTitulosSelecionados;
    procedure Consultar(AFiltro: TFiltroPadrao); overload;
    procedure PopularConsultaComJSONDataset(ADados: TFDJSONDatasets);
    procedure CalcularTamanhoDasVisoesDeTitulos;
    procedure ValidacoesAntesDeEfetivar;
    procedure AtualizarTotalItem;
    procedure HabilitarFiltrosSelecaoTitulo;
    procedure RemoverRegistroInserido;
    procedure SelecionarTitulo;
    procedure RemoverTituloSelecionado;
    procedure AtualizarTotalLote;
    procedure ExibirTrocoLote;
    procedure InserirQuitacaoAutomaticaPorTitulo;
    procedure SugerirDataVencimentoCheque;
  public
    FFrameQuitacaoLotePagamento: TFrameQuitacaoLotePagamento;
    FFormularioAbertoPorOutroFormulario: Boolean;
    FParametroGerarContraPartidaContaCorrenteMovimento: TParametroFormulario;

    procedure RatearAcrescimos;
    procedure RatearDescontos;
    class procedure AbrirTelaFiltrando(const AIdChaveProcesso: Integer);

    class procedure AbrirTelaFiltrandoPorCodigosDosTitulos(const ACodigos: String;
      const AFiltrosDaTela: TDictionary<String, String> = nil);

    {Usados em AfiltrosDaTela}
    const FILTRO_CODIGO_PESSOA = 'FILTRO_CODIGO_PESSOA';
    const FILTRO_NOME_PESSOA = 'FILTRO_NOME_PESSOA';
    const FILTRO_INICIO_VENCIMENTO = 'FILTRO_INICIO_VENCIMENTO';
    const FILTRO_FIM_VENCIMENTO = 'FILTRO_FIM_VENCIMENTO';
    const FILTRO_DOCUMENTO = 'FILTRO_DOCUMENTO';

    function QuitacaoEmCartao: Boolean;
  protected
    procedure GerenciarControles; override;
    procedure SetWhereBundle; override;
    procedure PrepararFiltros; override;
    procedure AoCriarFormulario; override;
    procedure DepoisDeInserir; override;
    procedure DepoisDeAlterar; override;
    procedure DepoisDeCancelar; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeRemover; override;
    procedure AntesDeConfirmar; override;
    procedure BloquearEdicao; override;
    procedure InstanciarFrames; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

var
  MovLotePagamento: TMovLotePagamento;

implementation

{$R *.dfm}

uses uFilial, uChaveProcesso, uChaveProcessoProxy, uLotePagamentoProxy,
  uTUsuario, uDatasetUtils, uFrmApelidarColunasGrid,
  uFrmAlterarSQLPesquisaPadrao, System.DateUtils,
  uDmConnection, uDevExpressUtils, uFrmMessage, uLotePagamento, uTMessage,
  uMathUtils, uFrmMessage_Process, uSistema, uPesqContaAnalise, uControlsUtils,
  uTControl_Function, uContaPagarProxy, uConstantes, uConstParametroFormulario,
  uTFunction, uConsultaGrid, uCadPessoa, uTPessoa, uTipoQuitacaoProxy;

procedure TMovLotePagamento.BloquearEdicao;
begin
  inherited;
  cdsData.SomenteLeitura := not cdsDataSTATUS.AsString.Equals(TLotePagamentoProxy.STATUS_ABERTO);
end;

function TMovLotePagamento.PegarIdContaPagarSelecionados: String;
begin
  result := fdmSelecaoTitulos.FieldByName('id').AsString;
end;

procedure TMovLotePagamento.DepoisDeConfirmar;
begin
  inherited;
  if fdmSelecaoTitulos.Active then
    fdmSelecaoTitulos.close;

  if cdsData.FieldByName('ID').AsInteger <= 0 then
  begin
    AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('LOTE_PAGAMENTO', 'ID'), 0));
  end;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLotePagamento.DepoisDeCancelar;
begin
  inherited;
  if fdmSelecaoTitulos.Active then
    fdmSelecaoTitulos.close;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLotePagamento.PrepararFiltros;
begin
  inherited;
  //EdtConsultaEfetivacaoInicio.EditValue := StartOfTheMonth(Date);
  //EdtConsultaEfetivacaoFim.EditValue    := EndOfTheMonth(Date+30);
  EdtConsultaSituacao.EditValue         := TLotePagamentoProxy.CONSTANTE_TODOS;
  EdtConsultaValor.EditValue            := null;
end;

function TMovLotePagamento.QuitacaoEmCartao: Boolean;
begin
  result := cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovLotePagamento.RatearAcrescimos;
var
  SomaAcrescimos : Double;
  SomaVlAberto   : Double;
  Percentual     : Double;
  Diferenca      : Double;
begin

  if cdsLotePagamentoTitulo.IsEmpty then
    Exit;

  if cdsLotePagamentoQuitacao.IsEmpty then
    Exit;

  SomaAcrescimos := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_ACRESCIMO);
  SomaVlAberto   := TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ABERTO);

  cdsLotePagamentoTitulo.DisableControls;
  cdsLotePagamentoTitulo.First;
  try

    while not cdsLotePagamentoTitulo.Eof do
    begin

      if not (cdsLotePagamentoTitulo.State in dsEditModes) then
        cdsLotePagamentoTitulo.Edit;

      Percentual := TMathUtils.PercentualSobreValor(cdsLotePagamentoTituloVL_ABERTO.AsFloat
                                                   ,SomaVlAberto);

      cdsLotePagamentoTituloVL_ACRESCIMO.AsFloat := TMathUtils.Arredondar(
                                                      TMathUtils.ValorSobrePercentual(Percentual
                                                                                     ,SomaAcrescimos));

      cdsLotePagamentoTitulo.Next;
    end;

     if not (cdsLotePagamentoTitulo.State in dsEditModes) then
        cdsLotePagamentoTitulo.Edit;

    Diferenca := SomaAcrescimos - TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ACRESCIMO);
    cdsLotePagamentoTituloVL_ACRESCIMO.AsFloat := TMathUtils.Arredondar(cdsLotePagamentoTituloVL_ACRESCIMO.AsFloat
                                                                      + Diferenca);

  finally
    cdsLotePagamentoTitulo.First;
    cdsLotePagamentoTitulo.EnableControls;
  end;

end;

procedure TMovLotePagamento.RatearDescontos;
var
  SomaDescontos  : Double;
  SomaVlAberto   : Double;
  Percentual     : Double;
  Diferenca      : Double;
begin

  if cdsLotePagamentoTitulo.IsEmpty then
    Exit;

  if cdsLotePagamentoQuitacao.IsEmpty then
    Exit;

  SomaDescontos  := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_DESCONTO);
  SomaVlAberto   := TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ABERTO);

  cdsLotePagamentoTitulo.DisableControls;
  cdsLotePagamentoTitulo.First;
  try

    while not cdsLotePagamentoTitulo.Eof do
    begin

      if not (cdsLotePagamentoTitulo.State in dsEditModes) then
        cdsLotePagamentoTitulo.Edit;

      Percentual := TMathUtils.PercentualSobreValor(cdsLotePagamentoTituloVL_ABERTO.AsFloat
                                                   ,SomaVlAberto);

      cdsLotePagamentoTituloVL_DESCONTO.AsFloat := TMathUtils.Arredondar(
                                                      TMathUtils.ValorSobrePercentual(Percentual
                                                                                     ,SomaDescontos));

      cdsLotePagamentoTitulo.Next;
    end;


    if not (cdsLotePagamentoTitulo.State in dsEditModes) then
      cdsLotePagamentoTitulo.Edit;

    Diferenca := SomaDescontos - TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_DESCONTO);
    cdsLotePagamentoTituloVL_DESCONTO.AsFloat := TMathUtils.Arredondar(cdsLotePagamentoTituloVL_DESCONTO.AsFloat
                                                                     + Diferenca);

  finally
    cdsLotePagamentoTitulo.First;
    cdsLotePagamentoTitulo.EnableControls;
  end;

end;

procedure TMovLotePagamento.RemoverDaConsultaOsTitulosSelecionados;
begin
  if not cdsLotePagamentoTitulo.Active then
  begin
    Exit;
  end;

  cdsLotePagamentoTitulo.DisableControls;
  cdsLotePagamentoTitulo.First;
  fdmSelecaoTitulos.DisableControls;
  try
    while not cdsLotePagamentoTitulo.Eof do
    begin
      if fdmSelecaoTitulos.Locate('ID', cdsLotePagamentoTituloID_CONTA_PAGAR.AsInteger, []) then
        fdmSelecaoTitulos.Delete;

      cdsLotePagamentoTitulo.Next;

    end;
  finally
    cdsLotePagamentoTitulo.First;
    cdsLotePagamentoTitulo.EnableControls;
    fdmSelecaoTitulos.First;
    fdmSelecaoTitulos.EnableControls;
  end;

end;

procedure TMovLotePagamento.RemoverTituloDoLotePagamento;
begin
  if not cdsLotePagamentoTitulo.IsEmpty then
  begin
    cdsLotePagamentoTitulo.Delete;
  end;
end;

procedure TMovLotePagamento.SetWhereBundle;
begin
  inherited;

  //Vencimento
  if not(VartoStr(EdtConsultaEfetivacaoInicio.editvalue).isEmpty) and
     not(VartoStr(EdtConsultaEfetivacaoFim.EditValue).IsEmpty) and
     (VarToDateTime(EdtConsultaEfetivacaoInicio.EditValue) <= VarToDateTime(EdtConsultaEfetivacaoFim.EditValue)) then
    WhereSearch.AddEntre(TLotePagamentoProxy.FIELD_DATA_EFETIVACAO,
      StrtoDate(EdtConsultaEfetivacaoInicio.EditValue), StrtoDate(EdtConsultaEfetivacaoFim.EditValue));

  //Situa��o
  if not VartoStr(EdtConsultaSituacao.EditValue).Equals(TLotePagamentoProxy.CONSTANTE_TODOS) then
    WhereSearch.AddIgual(TLotePagamentoProxy.FIELD_STATUS,VartoStr(EdtConsultaSituacao.EditValue));

  //Valor
  if StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0) > 0 then
    //WhereSearch.AddIgual(FIELD_VALOR_LOTE_PAGAMENTO, StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0));
end;


procedure TMovLotePagamento.SugerirDataVencimentoCheque;
begin
  if not cdsLotePagamentoQuitacao.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO.AsDateTime := cdsLotePagamentoQuitacaoDT_QUITACAO.AsDateTime;
  end
  else
  begin
    cdsLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

function TMovLotePagamento.GetWhereBundleSelecao: TFiltroPadrao;
begin
  Result := TFiltroPadrao.Create;

  if not (filtrosTitulosDOCUMENTO.AsString).IsEmpty then
    Result.AddContem('Upper(DOCUMENTO)', UpperCase(filtrosTitulosDOCUMENTO.AsString));

  if not (filtrosTitulosID_PESSOA.AsString).IsEmpty then
    Result.AddIgual('ID_PESSOA', filtrosTitulosID_PESSOA.AsString);


  if not(filtrosTitulosDT_VENCIMENTO_INICIAL.AsString.isEmpty) and
     not(filtrosTitulosDT_VENCIMENTO_FINAL.AsString.IsEmpty) and
     (filtrosTitulosDT_VENCIMENTO_INICIAL.AsDateTime <= filtrosTitulosDT_VENCIMENTO_FINAL.AsDateTime)
  then
    Result.AddEntre('DT_VENCIMENTO',
      StrtoDate(filtrosTitulosDT_VENCIMENTO_INICIAL.AsString), StrtoDate(filtrosTitulosDT_VENCIMENTO_FINAL.AsString));

end;

procedure TMovLotePagamento.AbrirDatasetSelecaoTitulo;
var filtroVazio: TFiltroPadrao;
begin
  filtroVazio := TFiltroPadrao.Create;
  try
    filtroVazio.AddIgual('1', '2');
    Consultar(filtroVazio)
  finally
    FreeAndNil(filtroVazio);
  end;
end;

class procedure TMovLotePagamento.AbrirTelaFiltrando(const AIdChaveProcesso: Integer);
var
  instanciaFormulario: TMovLotePagamento;
  filtroChaveProcesso: TFiltroPadrao;
begin
  if AIdChaveProcesso <= 0 then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es sobre o lote');
    TControlsUtils.CongelarFormulario(Application.MainForm);
    instanciaFormulario := TMovLotePagamento(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio de Lote de Pagamento est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TMovLotePagamento(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.ActInsert.Execute;

    filtroChaveProcesso := TFiltroPadrao.Create;
    try
      filtroChaveProcesso.AddIgual('CONTA_PAGAR.ID_CHAVE_PROCESSO', AIdChaveProcesso);
      instanciaFormulario.Consultar(filtroChaveProcesso);
    finally
      FreeAndNil(filtroChaveProcesso);
    end;

    instanciaFormulario.Show;
    Application.ProcessMessages;
    instanciaFormulario.FFormularioAbertoPorOutroFormulario := true;
  finally
    TControlsUtils.DescongelarFormulario;
    TFrmMessage_Process.CloseMessage;
  end;
end;

class procedure TMovLotePagamento.AbrirTelaFiltrandoPorCodigosDosTitulos(const ACodigos: String;
  const AFiltrosDaTela: TDictionary<String, String>);
var
  instanciaFormulario: TMovLotePagamento;
  filtroChaveProcesso: TFiltroPadrao;
begin
  if ACodigos.IsEmpty then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es sobre o lote');
    instanciaFormulario := TMovLotePagamento(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio de Lote de Pagamento est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TMovLotePagamento(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.ActInsert.Execute;

    filtroChaveProcesso := TFiltroPadrao.Create;
    try
      filtroChaveProcesso.AddFaixa('CONTA_PAGAR.ID', ACodigos);
      instanciaFormulario.Consultar(filtroChaveProcesso)
    finally
      FreeAndNil(filtroChaveProcesso);
    end;

    instanciaFormulario.Show;
    Application.ProcessMessages;
    instanciaFormulario.FFormularioAbertoPorOutroFormulario := true;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovLotePagamento.actAlterarColunasGridTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewSelecaoTitulos);
end;

procedure TMovLotePagamento.actAlterarRotuloLoteTitulosExecute(Sender: TObject);
begin
  inherited;
  TFrmApelidarColunasGrid.SetColumns(ViewLotePagamentoTitulos);
end;

procedure TMovLotePagamento.actAlterarSQLLoteTitulosExecute(Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+ViewLotePagamentoTitulos.Name) then
    AbrirPesquisaPadrao;
end;

procedure TMovLotePagamento.actAlterarSQLPesquisaPadraoTitulosExecute(
  Sender: TObject);
begin
  inherited;
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+ViewSelecaoTitulos.Name) then
    AbrirPesquisaPadrao;
end;

procedure TMovLotePagamento.RemoverTituloSelecionado;
begin
  RemoverTituloDoLotePagamento;
  RatearAcrescimos;
  RatearDescontos;
end;

procedure TMovLotePagamento.actEfetivarExecute(Sender: TObject);
var
  IDChaveProcesso : Integer;
  fecharAoConfirmar: Boolean;
begin
  inherited;
  if (cdsLotePagamentoQuitacao.State in dsEditmodes) then
  begin
    FFrameQuitacaoLotePagamento.ActConfirm.Execute;
  end;

  ValidacoesAntesDeEfetivar;

  IDChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

  if (cdsData.EstadoDeAlteracao) then
  try
    fecharAoConfirmar := FFormularioAbertoPorOutroFormulario;

    if fecharAoConfirmar then
    begin
      FFormularioAbertoPorOutroFormulario := false;
    end;

    ActConfirm.Execute;
  finally
    if fecharAoConfirmar then
    begin
      FFormularioAbertoPorOutroFormulario := true;
    end;
  end;

  if TFrmMessage.Question('Deseja Efetivar o Lote de Pagamento?') = MCONFIRMED then
  begin
    TFrmMessage_Process.SendMessage('Efetivando o Lote de Pagamento');
    try
      TLotePagamento.Efetivar(IDChaveProcesso);
      AtualizarRegistro;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;

  if FFormularioAbertoPorOutroFormulario then
    FecharFormulario;
end;

procedure TMovLotePagamento.ValidacoesAntesDeEfetivar;
var
  VlSomaQuitacao : Double;
  VlSomaDesconto : Double;
  VlSomaAberto   : Double;
begin
  VlSomaQuitacao := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_QUITACAO));
  VlSomaDesconto := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_DESCONTO));
  VlSomaAberto   := TMathUtils.Arredondar(
                      TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ABERTO));

  if (VlSomaQuitacao) > VlSomaAberto then
  begin
    TMessage.MessageInformation('O valor total da quita��o n�o pode ser maior que o valor em aberto.');
    Abort;
  end;

  if (VlSomaQuitacao) <= 0 then
  begin
    TMessage.MessageInformation('O valor total da quita��o deve possuir um valor maior do que zero.');
    Abort;
  end;
end;

procedure TMovLotePagamento.ViewLotePagamentoTitulosDblClick(Sender: TObject);
begin
  inherited;
  RemoverTituloSelecionado;
end;

procedure TMovLotePagamento.ViewLotePagamentoTitulosEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    RemoverTituloSelecionado;
end;

procedure TMovLotePagamento.ViewSelecaoTitulosDblClick(Sender: TObject);
begin
  inherited;
  SelecionarTitulo;
end;

procedure TMovLotePagamento.ViewSelecaoTitulosEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RIGHT then
    SelecionarTitulo;
end;

procedure TMovLotePagamento.actExibirAgrupamentoLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  ViewLotePagamentoTitulos.OptionsView.GroupByBox :=
    not ViewLotePagamentoTitulos.OptionsView.GroupByBox;
end;

procedure TMovLotePagamento.actExibirAgrupamentoTitulosExecute(Sender: TObject);
begin
  inherited;
  ViewSelecaoTitulos.OptionsView.GroupByBox :=
    not ViewSelecaoTitulos.OptionsView.GroupByBox;
end;

procedure TMovLotePagamento.actFiltrarSelecaoTitulosExecute(Sender: TObject);
var
  filtrosSelecaoTitulo: TFiltroPadrao;
begin
  Self.SelectNext(Self, true, true);
  filtrosSelecaoTitulo := GetWhereBundleSelecao;
  try
    Consultar(filtrosSelecaoTitulo);
  finally
    FreeAndNil(filtrosSelecaoTitulo);
  end;
end;

procedure TMovLotePagamento.actReabrirExecute(Sender: TObject);
begin
  inherited;

  if TFrmMessage.Question('Deseja Reabrir o Lote de Pagamento?') = MCONFIRMED then
  begin

    TFrmMessage_Process.SendMessage('Reabrindo o Lote de Pagamento');
    try
      TLotePagamento.Reabrir(cdsDataID.AsInteger,
        FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
    finally
      TFrmMessage_Process.CloseMessage;
    end;
    AtualizarRegistro;
  end;
end;

procedure TMovLotePagamento.actRemoverTodosExecute(Sender: TObject);
begin
  inherited;
  if TFrmMessage.Question('Deseja remover todos os registros selecionados'+
    ' deste lote de pagamento?') = MCONFIRMED then
  try
    TControlsUtils.CongelarFormulario(Self);
    cdsLotePagamentoTitulo.DisableControls;

    While not cdsLotePagamentoTitulo.IsEmpty do
    begin
      RemoverTituloSelecionado;
    end;
  finally
    cdsLotePagamentoTitulo.EnableControls;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovLotePagamento.actRestaurarColunasLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name + ViewLotePagamentoTitulos.Name);
  ViewLotePagamentoTitulos.RestoreDefaults;
end;

procedure TMovLotePagamento.actRestaurarColunasTitulosExecute(Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name + ViewSelecaoTitulos.Name);
  ViewSelecaoTitulos.RestoreDefaults;
end;

procedure TMovLotePagamento.actSalvarConfiguracaoLoteTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(ViewLotePagamentoTitulos,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+ViewLotePagamentoTitulos.Name);
end;

procedure TMovLotePagamento.ActSalvarConfiguracoesTitulosExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.SaveGridView(ViewSelecaoTitulos,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+ViewSelecaoTitulos.Name);
end;

procedure TMovLotePagamento.actSelecionarTodosExecute(Sender: TObject);
var
  i: Integer;
  codigosContaPagar: TStringList;
  fieldCodigo: TField;
begin
  inherited;
  if fdmSelecaoTitulos.IsEmpty then
    exit;
  if TFrmMessage.Question('Deseja adicionar todos os registros filtrados'+
    ' a este lote de pagamento?') = MCONFIRMED then
  try
    TControlsUtils.CongelarFormulario(Self);
    TFrmMessage_Process.SendMessage('Adicionando todos os t�tulos');
    cdsLotePagamentoTitulo.DisableControls;
    fdmSelecaoTitulos.DisableControls;

    fieldCodigo := fdmSelecaoTitulos.FieldByName('ID');
    codigosContaPagar := TStringList.Create;
    try
      for i := 0 to ViewSelecaoTitulos.DataController.FilteredRecordCount - 1 do
      begin
        fdmSelecaoTitulos.Recno :=  ViewSelecaoTitulos.DataController.FilteredRecordIndex[i] + 1;
        codigosContaPagar.Add(fieldCodigo.AsString);
      end;

      for i := 0 to codigosContaPagar.Count - 1 do
      begin
        fdmSelecaoTitulos.Locate('ID', codigosContaPagar[I], []);
        SelecionarTitulo;
      end;
    finally
      FreeAndNil(codigosContaPagar);
    end;
  finally
    fdmSelecaoTitulos.EnableControls;
    cdsLotePagamentoTitulo.EnableControls;
    TFrmMessage_Process.CloseMessage;
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovLotePagamento.SelecionarTitulo;
begin
  if fdmSelecaoTitulos.IsEmpty then
    exit;

  InserirTituloNoLotePagamento;
  RemoverRegistroInserido;
  RatearAcrescimos;
  RatearDescontos;
end;

procedure TMovLotePagamento.RemoverRegistroInserido;
begin
  fdmSelecaoTitulos.Delete;
end;

procedure TMovLotePagamento.AntesDeRemover;
begin
  inherited;
  if (cdsDataSTATUS.AsString = TLotePagamentoProxy.STATUS_EFETIVADO)
  then
  begin
    TMessage.MessageInformation('N�o � poss�vel Remover um Lote de Pagamento Efetivado.');
    Abort;
  end;
end;

procedure TMovLotePagamento.AntesDeConfirmar;
begin
  inherited;

  if cdsLotePagamentoQuitacao.State in dsEditModes then
  begin
    FFrameQuitacaoLotePagamento.ActConfirm.Execute;
  end;
end;

procedure TMovLotePagamento.AoCriarFormulario;
begin
  inherited;
  AbrirDatasetSelecaoTitulo;
  pageControlPrincipal.ActivePage := tsTitulos;

  FFormularioAbertoPorOutroFormulario := false;

  TUsuarioGridView.LoadGridView(ViewLotePagamentoTitulos,
    TSistema.Sistema.Usuario.idSeguranca,
    Self.Name + ViewLotePagamentoTitulos.Name);
end;

procedure TMovLotePagamento.cdsDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
end;

procedure TMovLotePagamento.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FframeQuitacaoLotePagamento.SetConfiguracoesIniciais(cdsLotePagamentoQuitacao);
end;

procedure TMovLotePagamento.cdsDataCalcFields(DataSet: TDataSet);
begin
  inherited;
  if cdsDataVL_ABERTO.AsFloat > 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Diferen�a R$ '+FormatFloat('###,###,###,###,##0.00',
     cdsDataVL_ABERTO.AsFloat);
  end
  else if cdsDataVL_ABERTO.AsFloat < 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Troco R$ '+FormatFloat('###,###,###,###,##0.00',
     -1*cdsDataVL_ABERTO.AsFloat);
  end;
end;

procedure TMovLotePagamento.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime       := Now;
  cdsDataSTATUS.AsString              := TLotePagamentoProxy.STATUS_ABERTO;
  cdsDataID_FILIAL.AsInteger          := TFilial.GetIdFilial;
  cdsDataID_CHAVE_PROCESSO.AsInteger  :=
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_LOTE_PAGAMENTO, 0);
  cdsDataJOIN_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_LOTE_PAGAMENTO;
  cdsDataID_PESSOA_USUARIO.AsInteger  := TSistema.Sistema.usuario.id;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  RatearAcrescimos;
  RatearDescontos;
  AtualizarTotalLote
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoAfterEdit(
  DataSet: TDataSet);
begin
  inherited;
  //RatearAcrescimos;
  //RatearDescontos;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoAfterPost(
  DataSet: TDataSet);
begin
  inherited;
  RatearAcrescimos;
  RatearDescontos;
  AtualizarTotalLote;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoDT_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsLotePagamentoQuitacaoID_USUARIO_BAIXA.AsInteger := TSistema.Sistema.Usuario.Id;
  cdsLotePagamentoQuitacaoJOIN_NOME_USUARIO_BAIXA.AsString := TSistema.Sistema.Usuario.Nome;
  cdsLotePagamentoQuitacaoVL_DESCONTO.AsFloat    := 0;
  cdsLotePagamentoQuitacaoVL_ACRESCIMO.AsFloat   := 0;
  cdsLotePagamentoQuitacaoPERC_ACRESCIMO.AsFloat := 0;
  cdsLotePagamentoQuitacaoPERC_DESCONTO.AsFloat  := 0;

  cdsLotePagamentoQuitacaoDT_QUITACAO.AsDateTime :=
    TFunction.Iif(FParametroLancarQuitacaoAutomaticaPorTitulo.ValorSim,
    cdsLotePagamentoTituloDT_VENCIMENTO.AsDateTime, Now);

  cdsLotePagamentoQuitacaoNR_ITEM.AsFloat := TDatasetUtils.MaiorValor(cdsLotePagamentoQuitacaoNR_ITEM) + 1;

  cdsLotePagamentoQuitacaoVL_QUITACAO.AsFloat :=
    cdsDataVL_ABERTO.AsFloat;

  if FParametroTipoQuitacaoDinheiro.ValorNumericoValido then
  begin
    TConsultaGrid.ConsultarPorTipoQuitacao(cdsLotePagamentoQuitacao,
      false, FParametroTipoQuitacaoDinheiro.AsString,
      'ID_TIPO_QUITACAO', 'JOIN_DESCRICAO_TIPO_QUITACAO', false);
  end;
end;

procedure TMovLotePagamento.cdsLotePagamentoQuitacaoVL_QUITACAOChange(
  Sender: TField);
begin
  inherited;

  if not (cdsLotePagamentoQuitacao.State in dsEditModes) then
  begin
    Exit;
  end;

  AtualizarTotalItem;
end;

procedure TMovLotePagamento.cdsLotePagamentoTituloAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
end;

procedure TMovLotePagamento.cdsLotePagamentoTituloAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalLote;
end;

procedure TMovLotePagamento.cdsLotePagamentoTituloBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  if FParametroLancarQuitacaoAutomaticaPorTitulo.ValorSim then
  begin
    if cdsLotePagamentoQuitacao.Locate('VL_TOTAL', cdsLotePagamentoTituloVL_ABERTO.AsFloat,[]) then
    begin
      cdsLotePagamentoQuitacao.Delete;
    end;
  end;
end;

procedure TMovLotePagamento.CalcularPercentual(Sender: TField);

  function PegarPercentualSobreValor: Double;
  var
    VlSomaAberto : Double;
  begin

    VlSomaAberto := TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ABERTO);

    result := TMathUtils.PercentualSobreValor(
      Sender.AsFloat,
      VlSomaAberto);
  end;
begin
  if FCalculandoValor then
    exit;

  FCalculandoValor := True;

  if Sender.Dataset = cdsLotePagamentoQuitacao then
  begin
    if Sender = cdsLotePagamentoQuitacaoVL_DESCONTO then
    begin
      cdsLotePagamentoQuitacaoPERC_DESCONTO.AsFloat := PegarPercentualSobreValor;
    end
    else if Sender = cdsLotePagamentoQuitacaoVL_ACRESCIMO then
    begin
      cdsLotePagamentoQuitacaoPERC_ACRESCIMO.AsFloat := PegarPercentualSobreValor;
    end;

    AtualizarTotalItem;
  end;

  FCalculandoValor := False;
end;

procedure TMovLotePagamento.CalcularValor(Sender: TField);

  function PegarValorSobrePercentual: Double;
  var
    VlSomaAberto : Double;
  begin

    VlSomaAberto := TDatasetUtils.SomarColuna(cdsLotePagamentoTituloVL_ABERTO);

    result := TMathUtils.ValorSobrePercentual(
      Sender.AsFloat,
      VlSomaAberto);
  end;
begin
  if FCalculandoPercentual then
    exit;

  FCalculandoPercentual := True;

  if Sender.Dataset = cdsLotePagamentoQuitacao then
  begin
    if Sender = cdsLotePagamentoQuitacaoPERC_DESCONTO then
    begin
      cdsLotePagamentoQuitacaoVL_DESCONTO.AsFloat := PegarValorSobrePercentual;
    end
    else if Sender = cdsLotePagamentoQuitacaoPERC_ACRESCIMO then
    begin
      cdsLotePagamentoQuitacaoVL_ACRESCIMO.AsFloat := PegarValorSobrePercentual;
    end;

    AtualizarTotalItem;
  end;

  FCalculandoPercentual := False;
end;

procedure TMovLotePagamento.CalcularValoresAPartirDoTotalQuitacao(
  Sender: TField);
begin
  if FCalculandoTotalQuitacaoValor then
    exit;

  try
    FCalculandoTotalQuitacaoValor := true;
    cdsLotePagamentoQuitacaoVL_QUITACAO.AsFloat :=
      cdsLotePagamentoQuitacaoVL_TOTAL.AsFloat +
      cdsLotePagamentoQuitacaoVL_DESCONTO.AsFloat -
      cdsLotePagamentoQuitacaoVL_ACRESCIMO.AsFloat;

    FFrameQuitacaoLotePagamento.PreencherValorPagamento;
  finally
    FCalculandoTotalQuitacaoValor := false;
  end;

end;

procedure TMovLotePagamento.Consultar(AFiltro: TFiltroPadrao);
var
  DSList: TFDJSONDataSets;
begin
  try
    if filtrosTitulos.State in dsEditModes then
      filtrosTitulos.Post;

    try
      DSList := DmConnection.ServerMethodsClient.GetDados(
        Self.Name+ViewSelecaoTitulos.Name, AFiltro.where,
        TSistema.Sistema.Usuario.idSeguranca);

      PopularConsultaComJSONDataset(DSList);
      ClearWhereBundle;
    Except
      TFrmMessage.Failure('Falha ao consultar os dados, verifique o SQL associado a esta tela.');
      abort;
    end;
  finally
    HabilitarFiltrosSelecaoTitulo;
  end;
end;

procedure TMovLotePagamento.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Incluir Quita��o Por T�tulo
  FParametroLancarQuitacaoAutomaticaPorTitulo := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_LANCAR_QUITACAO_AUTOMATICA_POR_TITULO,
    TConstParametroFormulario.DESCRICAO_LANCAR_QUITACAO_AUTOMATICA_POR_TITULO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO,
    TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroLancarQuitacaoAutomaticaPorTitulo);

  //Parametro Tipo de Quita��o Dinheiro
  FParametroTipoQuitacaoDinheiro := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_TIPO_QUITACAO_DINHEIRO,
    TConstParametroFormulario.DESCRICAO_TIPO_QUITACAO_DINHEIRO);
  AParametros.AdicionarParametro(FParametroTipoQuitacaoDinheiro);

    //Gerar contra partida no conta corrente movimento
  FParametroGerarContraPartidaContaCorrenteMovimento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TConstParametroFormulario.DESCRICAO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroGerarContraPartidaContaCorrenteMovimento);
end;

procedure TMovLotePagamento.PopularConsultaComJSONDataset(ADados: TFDJSONDatasets);
begin
  try
    TDatasetUtils.JSONDatasetToMemTable(ADados, fdmSelecaoTitulos);

    TcxGridUtils.AdicionarTodosCamposNaView(ViewSelecaoTitulos);

    TUsuarioGridView.LoadGridView(ViewSelecaoTitulos,
      TSistema.Sistema.Usuario.idSeguranca,
      Self.Name+ViewSelecaoTitulos.Name);

    RemoverDaConsultaOsTitulosSelecionados;
  finally
    FreeAndNil(ADados);
  end;
end;

procedure TMovLotePagamento.DepoisDeAlterar;
begin
  inherited;

end;

procedure TMovLotePagamento.DepoisDeInserir;
begin
  inherited;

end;

procedure TMovLotePagamento.ExibirTrocoLote;
begin
  lbTrocoDiferencaLote.Visible := false;
  if cdsDataVL_ABERTO.AsFloat > 0 then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Diferen�a R$ '+FormatFloat('###,###,###,###,##0.00',
     cdsDataVL_ABERTO.AsFloat);
     lbTrocoDiferencaLote.Visible := true;
  end;
  {else if (cdsDataVL_ABERTO.AsFloat = 0) and
    not(cdsLotePagamentoQuitacao.IsEmpty) then
  begin
    cdsDataCC_VL_TROCO_DIFERENCA.AsString :=
    'Troco R$ '+FormatFloat('###,###,###,###,##0.00',
     TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoIC_VL_PAGAMENTO) -
     (cdsDataVL_QUITADO_LIQUIDO.AsFloat));
    lbTrocoDiferencaLote.Visible := true;
  end;}
  Application.ProcessMessages;
end;

procedure TMovLotePagamento.GerenciarControles;
var
  bEstaPesquisando: Boolean;
begin
  inherited;
  bEstaPesquisando := acaoCrud in [uFrmCadastro_Padrao.Pesquisar, uFrmCadastro_Padrao.none];
  barFiltrosLote.Visible := bEstaPesquisando;

  FframeQuitacaoLotePagamento.cxGBDadosMain.Enabled := (cdsDataSTATUS.AsString = TLotePagamentoProxy.STATUS_ABERTO);

  btnFiltrar.Enabled    := (cdsDataSTATUS.AsString = TLotePagamentoProxy.STATUS_ABERTO);

  HabilitarBotaoReabrir;
  HabilitarBotaoEfetivar;
  CalcularTamanhoDasVisoesDeTitulos;

  HabilitarFiltrosSelecaoTitulo;
end;

procedure TMovLotePagamento.HabilitarBotaoReabrir;
begin
  ActReabrir.Visible := cdsDataSTATUS.AsString = TLotePagamentoProxy.STATUS_EFETIVADO;
end;

procedure TMovLotePagamento.HabilitarFiltrosSelecaoTitulo;
begin
  if not filtrosTitulos.Active then
  begin
    filtrosTitulos.open;
  end;

  if filtrosTitulos.State in dsEditModes then
  begin
    filtrosTitulos.Post;
  end;

  filtrosTitulos.EmptyDataSet;
  filtrosTitulos.Append;

end;

procedure TMovLotePagamento.InserirQuitacaoAutomaticaPorTitulo;
const
  NAO_EXIBIR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = false;
begin
  FFrameQuitacaoLotePagamento.ActInsert.Execute;

  TConsultaGrid.ConsultarPorCentroResultado(cdsLotePagamentoQuitacao,
    false, cdsLotePagamentoTituloID_CENTRO_RESULTADO.AsString,
    'ID_CENTRO_RESULTADO', 'JOIN_DESCRICAO_CENTRO_RESULTADO', false);

  TConsultaGrid.ConsultarPorContaAnalise(cdsLotePagamentoQuitacao,
    false, cdsLotePagamentoTituloID_CONTA_ANALISE.AsString, 'ID_CONTA_ANALISE',
    'JOIN_DESCRICAO_CONTA_ANALISE', false);

  TConsultaGrid.ConsultarPorContaCorrente(cdsLotePagamentoQuitacao,
    false, cdsLotePagamentoTituloID_CONTA_CORRENTE.AsString,
    'ID_CONTA_CORRENTE', 'JOIN_DESCRICAO_CONTA_CORRENTE', false);

  cdsLotePagamentoQuitacaoDOCUMENTO.AsString :=
    cdsLotePagamentoTituloDOCUMENTO.AsString;

  cdsLotePagamentoQuitacaoDESCRICAO.AsString :=
    cdsLotePagamentoTituloDESCRICAO.AsString;

  if cdsLotePagamentoQuitacao.ValidarCamposObrigatorios(
    NAO_EXIBIR_MENSAGEM, NAO_ABORTAR) then
  begin
    FFrameQuitacaoLotePagamento.ActConfirm.Execute;
  end
  else
  begin
    FFrameQuitacaoLotePagamento.ActCancel.Execute;
  end;
end;

procedure TMovLotePagamento.InserirTituloNoLotePagamento;
var
  fdmTitulosSelecionados: TfdMemTable;
begin
  try
    fdmTitulosSelecionados := TFDMemTable.Create(nil);
    TLotePagamento.SetContasAPagar(fdmTitulosSelecionados, PegarIdContaPagarSelecionados);

    fdmTitulosSelecionados.First;
    while not fdmTitulosSelecionados.Eof do
    begin

      if cdsLotePagamentoTitulo.Locate('ID_CONTA_PAGAR', fdmTitulosSelecionados.FieldByName('ID').AsInteger, []) then
      begin
        fdmTitulosSelecionados.Next;
        continue;
      end;

      cdsLotePagamentoTitulo.Incluir;

      TDatasetUtils.CopiarRegistroComCondicoes(
      fdmTitulosSelecionados, //Origem
      cdsLotePagamentoTitulo, //Destino
      ['id', 'id_conta_pagar', 'vl_acrescimo', 'vl_decrescimo'], //No Copy
      [], //Campos para mudar
      []); //Valores para Mudar

      cdsLotePagamentoTituloVL_ACRESCIMO_CP.AsFloat  := fdmSelecaoTitulos.FieldByName('vl_acrescimo').AsFloat;
      cdsLotePagamentoTituloVL_DESCONTO_CP.AsFloat   := fdmSelecaoTitulos.FieldByName('vl_decrescimo').AsFloat;
      cdsLotePagamentoTituloID_CONTA_PAGAR.AsInteger := fdmSelecaoTitulos.FieldByName('id').AsInteger;
      cdsLotePagamentoTituloVL_ACRESCIMO.AsFloat     := 0;
      cdsLotePagamentoTituloVL_DESCONTO.AsFloat      := 0;

      cdsLotePagamentoTituloJOIN_NOME_PESSOA.AsString :=
        TPessoa.GetNomePessoa(cdsLotePagamentoTituloID_PESSOA.AsInteger);

      cdsLotePagamentoTitulo.Salvar;

      if FParametroLancarQuitacaoAutomaticaPorTitulo.EstaPreenchido and
        FParametroTipoQuitacaoDinheiro.EstaPreenchido then
      begin
        InserirQuitacaoAutomaticaPorTitulo;
      end;

      fdmTitulosSelecionados.Next;
    end;
  finally
    fdmTitulosSelecionados.Free;
  end;

end;

procedure TMovLotePagamento.InstanciarFrames;
begin
  inherited;
  FFrameQuitacaoLotePagamento := TFrameQuitacaoLotePagamento.Create(PnFrameQuitacao);
  FFrameQuitacaoLotePagamento.Parent := PnFrameQuitacao;
  FFrameQuitacaoLotePagamento.Align := alClient;
  FFrameQuitacaoLotePagamento.ExibirDadosDeBaixaPorTitulo(
    FParametroLancarQuitacaoAutomaticaPorTitulo.ValorSim);
end;

procedure TMovLotePagamento.AtualizarTotalItem;
begin
  cdsLotePagamentoQuitacaoVL_TOTAL.AsFloat := cdsLotePagamentoQuitacaoVL_QUITACAO.AsFloat
                                            + cdsLotePagamentoQuitacaoVL_ACRESCIMO.AsFloat
                                            - cdsLotePagamentoQuitacaoVL_DESCONTO.AsFloat;
end;

procedure TMovLotePagamento.AtualizarTotalLote;
var
  vlTotal, vlLiquido  : Double;
  vlDesconto : Double;
  vlAcrescimo  : Double;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  vlLiquido    := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_TOTAL);
  vlTotal      := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_QUITACAO);
  vlDesconto   := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_DESCONTO);
  vlAcrescimo  := TDatasetUtils.SomarColuna(cdsLotePagamentoQuitacaoVL_ACRESCIMO);

  cdsDataVL_ACRESCIMO.AsFloat  := vlAcrescimo;
  cdsDataVL_DESCONTO.AsFloat := vlDesconto;

  cdsDataVL_QUITADO.AsFloat         := vlTotal;
  cdsDataVL_QUITADO_LIQUIDO.AsFloat := vlLiquido;

  cdsDataVL_ABERTO.AsFloat :=
    TDatasetUtils.SomarColuna(
    cdsLotePagamentoTituloVL_ABERTO) - vlTotal;

  ExibirTrocoLote;
end;

procedure TMovLotePagamento.CalcularTamanhoDasVisoesDeTitulos;
var
  tamanhoDoPainel: Integer;
const
  QUANTIDADE_PAINEIS: Integer = 2;
begin
  tamanhoDoPainel := PnTitulos.Width div QUANTIDADE_PAINEIS;
  gridTitulos.Width := tamanhoDoPainel;
end;

procedure TMovLotePagamento.HabilitarBotaoEfetivar;
begin
  actEfetivar.Visible := cdsDataSTATUS.AsString = TLotePagamentoProxy.STATUS_ABERTO
end;

Initialization
  RegisterClass(TMovLotePagamento);

Finalization
  UnRegisterClass(TMovLotePagamento);

end.
