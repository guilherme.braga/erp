inherited FrmVisualizacaoRelatoriosAssociados: TFrmVisualizacaoRelatoriosAssociados
  Caption = 'Visualiza'#231#227'o de Impress'#245'es'
  ExplicitWidth = 685
  ExplicitHeight = 472
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'barPropriedades'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    object cxGridRelatorios: TcxGrid
      Left = 2
      Top = 2
      Width = 675
      Height = 337
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object cxGridRelatoriosView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.CustomizeDialog = False
        DataController.DataSource = dsConsultaRelatorioFR
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        DateTimeHandling.UseLongDateFormat = False
        OptionsBehavior.PostponedSynchronization = False
        OptionsBehavior.CopyCaptionsToClipboard = False
        OptionsBehavior.DragHighlighting = False
        OptionsBehavior.DragOpening = False
        OptionsBehavior.DragScrolling = False
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.ColumnHeaderHints = False
        OptionsBehavior.CopyPreviewToClipboard = False
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnHidingOnGrouping = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrDisabled
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.FocusRect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GridLineColor = clInfoBk
        OptionsView.GroupByBox = False
        object cxGridRelatoriosViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 61
        end
        object cxGridRelatoriosViewNOME: TcxGridDBColumn
          DataBinding.FieldName = 'NOME'
          Width = 236
        end
        object cxGridRelatoriosViewDESCRICAO: TcxGridDBColumn
          DataBinding.FieldName = 'DESCRICAO'
          Width = 364
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridRelatoriosView
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 316
    Top = 155
    object ActImprimir: TAction
      Category = 'Action'
      Caption = 'Imprimir F9'
      Hint = 'Imprimir arquivo selecionado'
      ImageIndex = 27
      ShortCut = 120
      OnExecute = ActImprimirExecute
    end
    object ActVisualizarRelatorio: TAction
      Category = 'Action'
      Caption = 'Visualizar F10'
      Hint = 'Visualizar arquivo selecionado'
      ImageIndex = 28
      ShortCut = 121
      OnExecute = ActVisualizarRelatorioExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Left = 288
    Top = 155
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 489
      FloatClientWidth = 55
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      FloatClientWidth = 86
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbImprimir'
        end
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end>
    end
    object barPropriedades: TdxBar [2]
      Caption = 'Propriedades'
      CaptionButtons = <>
      DockedLeft = 136
      DockedTop = 0
      FloatLeft = 703
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 170
          Visible = True
          ItemName = 'EdtImpressora'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 51
          Visible = True
          ItemName = 'EdtCopias'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object EdtImpressora: TdxBarCombo [4]
      Caption = 'Impressora '
      Category = 0
      Hint = 'Impressora '
      Visible = ivAlways
      ShowCaption = True
      ItemIndex = -1
    end
    object EdtCopias: TdxBarSpinEdit [5]
      Caption = 'C'#243'pias'
      Category = 0
      Hint = 'C'#243'pias'
      Visible = ivAlways
      ShowCaption = True
      MaxValue = 99999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
    end
    object dxBarCombo2: TdxBarCombo [6]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ItemIndex = -1
    end
    object lbVisualizar: TdxBarLargeButton
      Action = ActVisualizarRelatorio
      Category = 1
    end
    object lbImprimir: TdxBarLargeButton
      Action = ActImprimir
      Category = 1
    end
  end
  object cdsConsultaRelatorioFR: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspConsultaRelatorioFR'
    RemoteServer = DmConnection.dspRelatorioFR
    AfterOpen = cdsConsultaRelatorioFRAfterOpen
    AfterScroll = cdsConsultaRelatorioFRAfterScroll
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 408
    Top = 152
    object cdsConsultaRelatorioFRID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsConsultaRelatorioFRDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsConsultaRelatorioFRNOME: TStringField
      DisplayLabel = 'Relat'#243'rio'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object cdsConsultaRelatorioFRIMPRESSORA: TStringField
      DisplayLabel = 'Impressora'
      FieldName = 'IMPRESSORA'
      Origin = 'IMPRESSORA'
      Size = 255
    end
    object cdsConsultaRelatorioFRACAO: TStringField
      DisplayLabel = 'A'#231#227'o'
      FieldName = 'ACAO'
      Origin = 'ACAO'
      Size = 30
    end
    object cdsConsultaRelatorioFRNUMERO_COPIAS: TIntegerField
      DisplayLabel = 'C'#243'pias'
      FieldName = 'NUMERO_COPIAS'
    end
    object cdsConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField
      DisplayLabel = 'Filtros Personalizados'
      FieldName = 'FILTROS_PERSONALIZADOS'
    end
    object cdsConsultaRelatorioFRTIPO_ARQUIVO: TStringField
      DisplayLabel = 'Tipo de Arquivo'
      FieldName = 'TIPO_ARQUIVO'
      Size = 25
    end
    object cdsConsultaRelatorioFRARQUIVO: TBlobField
      FieldName = 'ARQUIVO'
      Required = True
    end
  end
  object dsConsultaRelatorioFR: TDataSource
    DataSet = cdsConsultaRelatorioFR
    Left = 436
    Top = 152
  end
end
