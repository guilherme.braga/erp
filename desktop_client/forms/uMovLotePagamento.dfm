inherited MovLotePagamento: TMovLotePagamento
  Caption = 'Lote de Pagamento'
  ClientHeight = 713
  ClientWidth = 1222
  Position = poDesigned
  ExplicitLeft = -297
  ExplicitWidth = 1238
  ExplicitHeight = 752
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1222
    ExplicitWidth = 1222
    inherited dxGerencia: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarNavegacaoData'
        end
        item
          ToolbarName = 'dxBarManager'
        end
        item
          ToolbarName = 'dxBarAction'
        end
        item
          ToolbarName = 'barFiltrosLote'
        end
        item
          ToolbarName = 'dxBarSearch'
        end
        item
          ToolbarName = 'dxBarReport'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'dxBarEnd'
        end>
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1222
    Height = 586
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1222
    ExplicitHeight = 586
    ClientRectBottom = 586
    ClientRectRight = 1222
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1222
      ExplicitHeight = 562
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1190
        Height = 562
        ExplicitLeft = 32
        ExplicitWidth = 1190
        ExplicitHeight = 562
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 562
        Height = 562
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 562
        ExplicitLeft = 24
        ExplicitHeight = 562
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1222
      ExplicitHeight = 562
      inherited DesignPanel: TJvDesignPanel
        Width = 1222
        Height = 562
        ExplicitWidth = 1222
        ExplicitHeight = 562
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1222
          ExplicitHeight = 562
          Height = 562
          Width = 1222
          inherited dxBevel1: TdxBevel
            Width = 1218
            Height = 27
            ExplicitWidth = 1012
            ExplicitHeight = 27
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 6
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelDtEfetivacao: TLabel
            Left = 705
            Top = 6
            Width = 69
            Height = 13
            Caption = 'Dt. Efetiva'#231#227'o'
          end
          object labelDtCadastro: TLabel
            Left = 505
            Top = 6
            Width = 62
            Height = 13
            Caption = 'Dt. Cadastro'
          end
          object labelProcesso: TLabel
            Left = 107
            Top = 6
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object labelSituacao: TLabel
            Left = 349
            Top = 6
            Width = 41
            Height = 13
            Caption = 'Situa'#231#227'o'
          end
          object labelObservacao: TLabel
            Left = 912
            Top = 5
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 45
            Top = 2
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Width = 58
          end
          object edtDtCadastro: TgbDBDateEdit
            Left = 571
            Top = 2
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnToday]
            Properties.ImmediatePost = True
            Properties.ReadOnly = True
            Properties.SaveTime = False
            Properties.ShowTime = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbDateTime = False
            Width = 130
          end
          object edtDtEfetivacao: TgbDBDateEdit
            Left = 778
            Top = 2
            TabStop = False
            DataBinding.DataField = 'DH_EFETIVACAO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnToday]
            Properties.ImmediatePost = True
            Properties.ReadOnly = True
            Properties.SaveTime = False
            Properties.ShowTime = False
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbDateTime = False
            Width = 130
          end
          object edtSituacao: TgbDBTextEdit
            Left = 394
            Top = 2
            TabStop = False
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 107
          end
          object edtObservacao: TgbDBBlobEdit
            Left = 974
            Top = 2
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OBSERVACAO'
            DataBinding.DataSource = dsData
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ClearKey = 16430
            Properties.ImmediatePost = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Width = 241
          end
          object descProcesso: TgbDBTextEdit
            Left = 200
            Top = 2
            TabStop = False
            DataBinding.DataField = 'JOIN_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 144
          end
          object edtChaveProcesso: TgbDBTextEdit
            Left = 154
            Top = 2
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 49
          end
          object panelTotalizadores: TgbPanel
            Left = 2
            Top = 532
            Align = alBottom
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            Transparent = True
            Height = 28
            Width = 1218
            object labelVlAbertoCP: TLabel
              Left = 8
              Top = 7
              Width = 83
              Height = 14
              Caption = 'Total do Lote'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lbTrocoDiferencaLote: TDBText
              Left = 958
              Top = 6
              Width = 138
              Height = 16
              AutoSize = True
              DataField = 'CC_VL_TROCO_DIFERENCA'
              DataSource = dsData
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label16: TLabel
              Left = 196
              Top = 7
              Width = 61
              Height = 16
              Caption = 'Total Pago'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label20: TLabel
              Left = 371
              Top = 7
              Width = 65
              Height = 16
              Caption = 'Acr'#233'scimos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label21: TLabel
              Left = 550
              Top = 7
              Width = 58
              Height = 16
              Caption = 'Descontos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label19: TLabel
              Left = 722
              Top = 7
              Width = 122
              Height = 16
              Caption = 'Pagamento L'#237'quido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object edtVlAbertoTitulos: TgbDBTextEdit
              Left = 95
              Top = 3
              TabStop = False
              DataBinding.DataField = 'SUM_VL_ABERTO_CP'
              DataBinding.DataSource = dsLotePagamentoTitulo
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 97
            end
            object edVlRecebido: TgbDBTextEdit
              Left = 261
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_QUITADO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit3: TgbDBTextEdit
              Left = 440
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_ACRESCIMO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit4: TgbDBTextEdit
              Left = 612
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_DESCONTO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
            object gbDBTextEdit8: TgbDBTextEdit
              Left = 848
              Top = 3
              TabStop = False
              DataBinding.DataField = 'VL_QUITADO_LIQUIDO'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.Alignment.Horz = taRightJustify
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 106
            end
          end
          object pageControlPrincipal: TcxPageControl
            Left = 2
            Top = 29
            Width = 1218
            Height = 503
            Align = alClient
            Focusable = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            TabStop = False
            Properties.ActivePage = tsTitulos
            Properties.CustomButtons.Buttons = <>
            Properties.NavigatorPosition = npLeftTop
            Properties.Style = 8
            OnChange = cxpcMainChange
            ClientRectBottom = 503
            ClientRectRight = 1218
            ClientRectTop = 24
            object tsTitulos: TcxTabSheet
              Caption = 'Titulos'
              ImageIndex = 0
              object pnTitulos: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 479
                Width = 1218
                object panelFiltro: TgbPanel
                  Left = 2
                  Top = 2
                  Align = alTop
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  ParentBackground = False
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Transparent = True
                  DesignSize = (
                    1214
                    29)
                  Height = 29
                  Width = 1214
                  object labelPessoa: TLabel
                    Left = 6
                    Top = 8
                    Width = 34
                    Height = 13
                    Caption = 'Pessoa'
                  end
                  object LabelDtVencimento: TLabel
                    Left = 610
                    Top = 8
                    Width = 109
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Per'#237'odo de Vencimento'
                    ExplicitLeft = 404
                  end
                  object labelDocumento: TLabel
                    Left = 942
                    Top = 8
                    Width = 54
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Documento'
                    ExplicitLeft = 736
                  end
                  object btnFiltrar: TJvTransparentButton
                    Left = 1172
                    Top = 2
                    Width = 40
                    Height = 25
                    Action = actFiltrarSelecaoTitulos
                    Align = alRight
                    FrameStyle = fsMono
                    TextAlign = ttaTopRight
                    Images.ActiveImage = DmAcesso.cxImage16x16
                    Images.ActiveIndex = 11
                    Images.GrayIndex = 11
                    Images.DisabledIndex = 11
                    Images.DownIndex = 11
                    Images.HotIndex = 11
                    ExplicitLeft = 964
                    ExplicitTop = 3
                    ExplicitHeight = 23
                  end
                  object Label1: TLabel
                    Left = 723
                    Top = 8
                    Width = 13
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'De'
                    ExplicitLeft = 517
                  end
                  object Label2: TLabel
                    Left = 830
                    Top = 8
                    Width = 17
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'At'#233
                    ExplicitLeft = 624
                  end
                  object edtDtVencimentoInicio: TgbDBDateEdit
                    Left = 738
                    Top = 4
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DT_VENCIMENTO_INICIAL'
                    DataBinding.DataSource = dsFiltrosTitulos
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 2
                    gbDateTime = False
                    Width = 83
                  end
                  object edtDescPessoa: TgbDBTextEdit
                    Left = 102
                    Top = 4
                    TabStop = False
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_NOME_PESSOA'
                    DataBinding.DataSource = dsFiltrosTitulos
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 502
                  end
                  object edtDocumento: TgbDBTextEdit
                    Left = 997
                    Top = 4
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DOCUMENTO'
                    DataBinding.DataSource = dsFiltrosTitulos
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 4
                    gbPassword = False
                    Width = 167
                  end
                  object edtVencimentoFim: TgbDBDateEdit
                    Left = 854
                    Top = 4
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DT_VENCIMENTO_FINAL'
                    DataBinding.DataSource = dsFiltrosTitulos
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 3
                    gbDateTime = False
                    Width = 83
                  end
                  object gbDBButtonEditFK1: TgbDBButtonEditFK
                    Left = 44
                    Top = 4
                    DataBinding.DataField = 'ID_PESSOA'
                    DataBinding.DataSource = dsFiltrosTitulos
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Properties.ReadOnly = False
                    Style.Color = clWhite
                    TabOrder = 1
                    gbTextEdit = edtDescPessoa
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_PESSOA;JOIN_DESCRICAO_NOME_PESSOA'
                    gbTableName = 'PESSOA'
                    gbCamposConsulta = 'ID;NOME'
                    gbIdentificadorConsulta = 'PESSOA'
                    Width = 60
                  end
                end
                object gbPanel2: TgbPanel
                  Left = 2
                  Top = 31
                  Align = alLeft
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Transparent = True
                  Height = 446
                  Width = 679
                  object gridTitulos: TcxGrid
                    Left = 2
                    Top = 30
                    Width = 675
                    Height = 414
                    Align = alClient
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Arial'
                    Font.Style = []
                    Images = DmAcesso.cxImage16x16
                    ParentFont = False
                    TabOrder = 0
                    LookAndFeel.Kind = lfOffice11
                    LookAndFeel.NativeStyle = False
                    object ViewSelecaoTitulos: TcxGridDBBandedTableView
                      PopupMenu = pmSelecaoTitulos
                      OnDblClick = ViewSelecaoTitulosDblClick
                      Navigator.Buttons.CustomButtons = <>
                      Navigator.Buttons.Images = DmAcesso.cxImage16x16
                      Navigator.Buttons.PriorPage.Visible = False
                      Navigator.Buttons.NextPage.Visible = False
                      Navigator.Buttons.Insert.Visible = False
                      Navigator.Buttons.Delete.Visible = False
                      Navigator.Buttons.Edit.Visible = False
                      Navigator.Buttons.Post.Visible = False
                      Navigator.Buttons.Cancel.Visible = False
                      Navigator.Buttons.Refresh.Visible = False
                      Navigator.Buttons.SaveBookmark.Visible = False
                      Navigator.Buttons.GotoBookmark.Visible = False
                      Navigator.Buttons.Filter.Visible = False
                      Navigator.InfoPanel.Visible = True
                      Navigator.Visible = True
                      OnEditKeyDown = ViewSelecaoTitulosEditKeyDown
                      OnEditKeyPress = Level1BandedTableView1EditKeyPress
                      DataController.DataSource = dsSelecaoTitulos
                      DataController.Filter.Options = [fcoCaseInsensitive]
                      DataController.Filter.Active = True
                      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      FilterRow.Visible = True
                      FilterRow.ApplyChanges = fracImmediately
                      Images = DmAcesso.cxImage16x16
                      OptionsBehavior.NavigatorHints = True
                      OptionsBehavior.ExpandMasterRowOnDblClick = False
                      OptionsCustomize.ColumnsQuickCustomization = True
                      OptionsCustomize.BandMoving = False
                      OptionsCustomize.NestedBands = False
                      OptionsData.CancelOnExit = False
                      OptionsData.Deleting = False
                      OptionsData.DeletingConfirmation = False
                      OptionsData.Editing = False
                      OptionsData.Inserting = False
                      OptionsSelection.CellSelect = False
                      OptionsView.ColumnAutoWidth = True
                      OptionsView.GroupByBox = False
                      OptionsView.GroupRowStyle = grsOffice11
                      OptionsView.ShowColumnFilterButtons = sfbAlways
                      OptionsView.BandHeaders = False
                      Styles.ContentOdd = DmAcesso.OddColor
                      Bands = <
                        item
                        end>
                    end
                    object cxGridLevel1: TcxGridLevel
                      GridView = ViewSelecaoTitulos
                    end
                  end
                  object gbPanel3: TgbPanel
                    Left = 2
                    Top = 2
                    Align = alTop
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    Transparent = True
                    Height = 28
                    Width = 675
                    object cxLabel3: TcxLabel
                      Left = 2
                      Top = 2
                      Align = alClient
                      Caption = 
                        'Selecione os t'#237'tulos abaixo que deseja adicionar ao Lote de Rece' +
                        'bimento'
                      ParentFont = False
                      Style.BorderStyle = ebsNone
                      Style.Font.Charset = DEFAULT_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -13
                      Style.Font.Name = 'Tahoma'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      Properties.Alignment.Horz = taLeftJustify
                      Properties.Alignment.Vert = taVCenter
                      Transparent = True
                      AnchorY = 14
                    end
                    object gbPanel4: TgbPanel
                      Left = 492
                      Top = 2
                      Align = alRight
                      PanelStyle.Active = True
                      PanelStyle.OfficeBackgroundKind = pobkGradient
                      Style.BorderStyle = ebsNone
                      Style.LookAndFeel.Kind = lfOffice11
                      Style.Shadow = False
                      StyleDisabled.LookAndFeel.Kind = lfOffice11
                      StyleFocused.LookAndFeel.Kind = lfOffice11
                      StyleHot.LookAndFeel.Kind = lfOffice11
                      TabOrder = 1
                      Transparent = True
                      Height = 24
                      Width = 181
                      object BtnSelecionarTodosRegistros: TJvTransparentButton
                        Left = 2
                        Top = 2
                        Width = 177
                        Height = 20
                        Action = actSelecionarTodos
                        Align = alClient
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        HotTrackFont.Charset = DEFAULT_CHARSET
                        HotTrackFont.Color = clWindowText
                        HotTrackFont.Height = -11
                        HotTrackFont.Name = 'Tahoma'
                        HotTrackFont.Style = []
                        FrameStyle = fsNone
                        ParentFont = False
                        TextAlign = ttaRight
                        Images.ActiveImage = DmAcesso.cxImage16x16
                        Images.ActiveIndex = 3
                        Images.GrayImage = DmAcesso.cxImage16x16
                        Images.GrayIndex = 3
                        Images.DisabledImage = DmAcesso.cxImage16x16
                        Images.DisabledIndex = 3
                        Images.DownImage = DmAcesso.cxImage16x16
                        Images.DownIndex = 3
                        Images.HotImage = DmAcesso.cxImage16x16
                        Images.HotIndex = 3
                        ExplicitLeft = 72
                        ExplicitWidth = 147
                      end
                    end
                  end
                end
                object cxSplitter1: TcxSplitter
                  Left = 681
                  Top = 31
                  Width = 8
                  Height = 446
                  HotZoneClassName = 'TcxXPTaskBarStyle'
                  Control = gbPanel2
                end
                object gbPanel1: TgbPanel
                  Left = 689
                  Top = 31
                  Align = alClient
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  Transparent = True
                  Height = 446
                  Width = 527
                  object GridLotePagamentoTitulos: TcxGrid
                    Left = 2
                    Top = 30
                    Width = 523
                    Height = 414
                    Align = alClient
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Arial'
                    Font.Style = []
                    Images = DmAcesso.cxImage16x16
                    ParentFont = False
                    TabOrder = 0
                    LookAndFeel.Kind = lfOffice11
                    LookAndFeel.NativeStyle = False
                    object ViewLotePagamentoTitulos: TcxGridDBBandedTableView
                      PopupMenu = pmLotePagamentoTitulos
                      OnDblClick = ViewLotePagamentoTitulosDblClick
                      Navigator.Buttons.CustomButtons = <>
                      Navigator.Buttons.Images = DmAcesso.cxImage16x16
                      Navigator.Buttons.PriorPage.Visible = False
                      Navigator.Buttons.NextPage.Visible = False
                      Navigator.Buttons.Insert.Visible = False
                      Navigator.Buttons.Delete.Visible = False
                      Navigator.Buttons.Edit.Visible = False
                      Navigator.Buttons.Post.Visible = False
                      Navigator.Buttons.Cancel.Visible = False
                      Navigator.Buttons.Refresh.Visible = False
                      Navigator.Buttons.SaveBookmark.Visible = False
                      Navigator.Buttons.GotoBookmark.Visible = False
                      Navigator.Buttons.Filter.Visible = False
                      Navigator.InfoPanel.Visible = True
                      Navigator.Visible = True
                      OnEditKeyDown = ViewLotePagamentoTitulosEditKeyDown
                      OnEditKeyPress = Level1BandedTableView1EditKeyPress
                      DataController.DataSource = dsLotePagamentoTitulo
                      DataController.Filter.Options = [fcoCaseInsensitive]
                      DataController.Filter.Active = True
                      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      FilterRow.ApplyChanges = fracImmediately
                      Images = DmAcesso.cxImage16x16
                      OptionsBehavior.NavigatorHints = True
                      OptionsBehavior.ExpandMasterRowOnDblClick = False
                      OptionsCustomize.ColumnsQuickCustomization = True
                      OptionsCustomize.BandMoving = False
                      OptionsCustomize.NestedBands = False
                      OptionsData.CancelOnExit = False
                      OptionsData.Deleting = False
                      OptionsData.DeletingConfirmation = False
                      OptionsData.Editing = False
                      OptionsData.Inserting = False
                      OptionsSelection.CellSelect = False
                      OptionsView.ColumnAutoWidth = True
                      OptionsView.GroupByBox = False
                      OptionsView.GroupRowStyle = grsOffice11
                      OptionsView.ShowColumnFilterButtons = sfbAlways
                      OptionsView.BandHeaders = False
                      Styles.ContentOdd = DmAcesso.OddColor
                      Bands = <
                        item
                        end>
                      object ViewLotePagamentoTitulosID: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID'
                        Position.BandIndex = 0
                        Position.ColIndex = 0
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_ACRESCIMO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_ACRESCIMO'
                        Position.BandIndex = 0
                        Position.ColIndex = 1
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_DESCONTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_DESCONTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 2
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_LOTE_PAGAMENTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_LOTE_PAGAMENTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 3
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_CONTA_PAGAR: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_CONTA_PAGAR'
                        Position.BandIndex = 0
                        Position.ColIndex = 4
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosDH_CADASTRO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'DH_CADASTRO'
                        Position.BandIndex = 0
                        Position.ColIndex = 5
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosDOCUMENTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'DOCUMENTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 6
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosDESCRICAO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'DESCRICAO'
                        Position.BandIndex = 0
                        Position.ColIndex = 7
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_TITULO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_TITULO'
                        Position.BandIndex = 0
                        Position.ColIndex = 8
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_QUITADO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_QUITADO'
                        Position.BandIndex = 0
                        Position.ColIndex = 9
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_ABERTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_ABERTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 10
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosQT_PARCELA: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'QT_PARCELA'
                        Position.BandIndex = 0
                        Position.ColIndex = 11
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosNR_PARCELA: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'NR_PARCELA'
                        Position.BandIndex = 0
                        Position.ColIndex = 12
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosDT_VENCIMENTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'DT_VENCIMENTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 13
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosSEQUENCIA: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'SEQUENCIA'
                        Position.BandIndex = 0
                        Position.ColIndex = 14
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosBO_VENCIDO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'BO_VENCIDO'
                        Position.BandIndex = 0
                        Position.ColIndex = 15
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosOBSERVACAO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'OBSERVACAO'
                        Position.BandIndex = 0
                        Position.ColIndex = 16
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_PESSOA: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_PESSOA'
                        Position.BandIndex = 0
                        Position.ColIndex = 17
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_CONTA_ANALISE: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_CONTA_ANALISE'
                        Position.BandIndex = 0
                        Position.ColIndex = 18
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_CENTRO_RESULTADO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_CENTRO_RESULTADO'
                        Position.BandIndex = 0
                        Position.ColIndex = 19
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosSTATUS: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'STATUS'
                        Position.BandIndex = 0
                        Position.ColIndex = 20
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_CHAVE_PROCESSO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_CHAVE_PROCESSO'
                        Position.BandIndex = 0
                        Position.ColIndex = 21
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_FORMA_PAGAMENTO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_FORMA_PAGAMENTO'
                        Position.BandIndex = 0
                        Position.ColIndex = 22
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_QUITADO_LIQUIDO: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_QUITADO_LIQUIDO'
                        Position.BandIndex = 0
                        Position.ColIndex = 23
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosDT_COMPETENCIA: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'DT_COMPETENCIA'
                        Position.BandIndex = 0
                        Position.ColIndex = 24
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosID_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'ID_DOCUMENTO_ORIGEM'
                        Position.BandIndex = 0
                        Position.ColIndex = 25
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosTP_DOCUMENTO_ORIGEM: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'TP_DOCUMENTO_ORIGEM'
                        Position.BandIndex = 0
                        Position.ColIndex = 26
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_ACRESCIMO_CP: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_ACRESCIMO_CP'
                        Position.BandIndex = 0
                        Position.ColIndex = 27
                        Position.RowIndex = 0
                      end
                      object ViewLotePagamentoTitulosVL_DESCONTO_CP: TcxGridDBBandedColumn
                        DataBinding.FieldName = 'VL_DESCONTO_CP'
                        Position.BandIndex = 0
                        Position.ColIndex = 28
                        Position.RowIndex = 0
                      end
                    end
                    object cxGridLevel2: TcxGridLevel
                      GridView = ViewLotePagamentoTitulos
                    end
                  end
                  object gbPanel5: TgbPanel
                    Left = 2
                    Top = 2
                    Align = alTop
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    Transparent = True
                    Height = 28
                    Width = 523
                    object cxLabel1: TcxLabel
                      Left = 2
                      Top = 2
                      Align = alClient
                      Caption = 'T'#237'tulos selecionados para este Lote de Recebimento:'
                      ParentFont = False
                      Style.BorderStyle = ebsNone
                      Style.Font.Charset = DEFAULT_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -13
                      Style.Font.Name = 'Tahoma'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      Properties.Alignment.Horz = taLeftJustify
                      Properties.Alignment.Vert = taVCenter
                      Transparent = True
                      AnchorY = 14
                    end
                    object gbPanel6: TgbPanel
                      Left = 348
                      Top = 2
                      Align = alRight
                      PanelStyle.Active = True
                      PanelStyle.OfficeBackgroundKind = pobkGradient
                      Style.BorderStyle = ebsNone
                      Style.LookAndFeel.Kind = lfOffice11
                      Style.Shadow = False
                      StyleDisabled.LookAndFeel.Kind = lfOffice11
                      StyleFocused.LookAndFeel.Kind = lfOffice11
                      StyleHot.LookAndFeel.Kind = lfOffice11
                      TabOrder = 1
                      Transparent = True
                      Height = 24
                      Width = 173
                      object JvTransparentButton3: TJvTransparentButton
                        Left = 2
                        Top = 2
                        Width = 169
                        Height = 20
                        Action = actRemoverTodos
                        Align = alClient
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        HotTrackFont.Charset = DEFAULT_CHARSET
                        HotTrackFont.Color = clWindowText
                        HotTrackFont.Height = -11
                        HotTrackFont.Name = 'Tahoma'
                        HotTrackFont.Style = []
                        FrameStyle = fsNone
                        ParentFont = False
                        TextAlign = ttaRight
                        Images.ActiveImage = DmAcesso.cxImage16x16
                        Images.ActiveIndex = 21
                        Images.GrayImage = DmAcesso.cxImage16x16
                        Images.GrayIndex = 21
                        Images.DisabledImage = DmAcesso.cxImage16x16
                        Images.DisabledIndex = 21
                        Images.DownImage = DmAcesso.cxImage16x16
                        Images.DownIndex = 21
                        Images.HotImage = DmAcesso.cxImage16x16
                        Images.HotIndex = 21
                        ExplicitLeft = 40
                        ExplicitWidth = 179
                      end
                    end
                  end
                end
              end
            end
            object tsQuitacao: TcxTabSheet
              Caption = 'Quita'#231#227'o'
              ImageIndex = 1
              object JvDesignPanel1: TJvDesignPanel
                Left = 0
                Top = 0
                Width = 1218
                Height = 479
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object cxGroupBox1: TcxGroupBox
                  Left = 0
                  Top = 0
                  Align = alClient
                  PanelStyle.Active = True
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  Style.TransparentBorder = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Height = 479
                  Width = 1218
                  object PnFrameQuitacao: TgbPanel
                    Left = 2
                    Top = 2
                    Align = alClient
                    Alignment = alCenterCenter
                    Caption = 'TFrameQuitacaoLotePagamento'
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    ParentBackground = False
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Transparent = True
                    Height = 475
                    Width = 1214
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    Left = 958
    Top = 136
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Categories.Strings = (
      'Action'
      'Manager'
      'Report'
      'End'
      'Search'
      'Design'
      'ActionDesign'
      'FiltrosLote')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True)
    Left = 760
    Top = 136
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbReabrir'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 850
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 1021
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 775
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      FloatClientWidth = 124
      FloatClientHeight = 216
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 929
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object barFiltrosLote: TdxBar [8]
      Caption = 'Filtros'
      CaptionButtons = <>
      DockedLeft = 466
      DockedTop = 0
      FloatLeft = 1050
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbConsultaVencimento'
        end
        item
          Visible = True
          ItemName = 'EdtConsultaEfetivacaoInicio'
        end
        item
          Visible = True
          ItemName = 'EdtConsultaEfetivacaoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'EdtConsultaSituacao'
        end
        item
          Visible = True
          ItemName = 'EdtConsultaValor'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object lbEfetivar: TdxBarLargeButton [9]
      Action = actEfetivar
      Category = 0
    end
    object lbReabrir: TdxBarLargeButton [10]
      Action = actReabrir
      Category = 0
    end
    object EdtConsultaEfetivacaoInicio: TcxBarEditItem [27]
      Caption = 'In'#237'cio'
      Category = 7
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ShowTime = False
      Properties.UseNullString = True
    end
    object EdtConsultaEfetivacaoFim: TcxBarEditItem [28]
      Caption = 'Fim   '
      Category = 7
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ShowTime = False
      Properties.UseNullString = True
    end
    object EdtConsultaSituacao: TcxBarEditItem [29]
      Caption = 'Situa'#231#227'o'
      Category = 7
      Hint = 'Situa'#231#227'o'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        'TODOS'
        'ABERTO'
        'EFETIVADO')
      Properties.PostPopupValueOnTab = True
    end
    object lbConsultaVencimento: TdxBarStatic [30]
      Align = iaClient
      Caption = 'Efetiva'#231#227'o'
      Category = 7
      Hint = 'Efetiva'#231#227'o'
      Visible = ivAlways
    end
    object EdtConsultaValor: TcxBarEditItem [31]
      Caption = 'Valor      '
      Category = 7
      Hint = 'Valor      '
      Visible = ivAlways
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = '###,###,###,###,##0.00'
      Properties.EditFormat = '###,###,###,###,##0.00'
    end
  end
  inherited dsSearch: TDataSource
    Left = 902
    Top = 136
  end
  inherited dsData: TDataSource
    Left = 846
    Top = 136
  end
  inherited ActionListMain: TActionList
    Left = 930
    Top = 136
    object actEfetivar: TAction
      Category = 'Action'
      Caption = 'Efetivar F9'
      Hint = 'Efetivar'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = actEfetivarExecute
    end
    object actReabrir: TAction
      Category = 'Action'
      Caption = 'Reabrir F10'
      Hint = 'Reabrir'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = actReabrirExecute
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    Left = 704
    Top = 136
  end
  inherited pmTituloGridPesquisaPadrao: TPopupMenu
    Left = 788
    Top = 136
  end
  inherited cxGridPopupMenuPadrao: TcxGridPopupMenu
    Left = 732
    Top = 136
  end
  inherited cdsData: TGBClientDataSet
    AggregatesActive = True
    ProviderName = 'dspLotePagamento'
    RemoteServer = DmConnection.dspLotePagamento
    AfterOpen = cdsDataAfterOpen
    OnCalcFields = cdsDataCalcFields
    OnNewRecord = cdsDataNewRecord
    Left = 816
    Top = 136
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object cdsDataSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'd. Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataDH_EFETIVACAO: TDateTimeField
      DisplayLabel = 'Dh. Efetiva'#231#227'o'
      FieldName = 'DH_EFETIVACAO'
      Origin = 'DH_EFETIVACAO'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataVL_QUITADO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_ACRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_DESCONTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Quitado L'#237'quido'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_ABERTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Fantasia Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object cdsDataJOIN_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Processo'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDatafdqLotePagamentoQuitacao: TDataSetField
      FieldName = 'fdqLotePagamentoQuitacao'
    end
    object cdsDatafdqLotePagamentoTitulo: TDataSetField
      FieldName = 'fdqLotePagamentoTitulo'
    end
    object cdsDataCC_VL_TROCO_DIFERENCA: TStringField
      FieldKind = fkCalculated
      FieldName = 'CC_VL_TROCO_DIFERENCA'
      Size = 50
      Calculated = True
    end
  end
  inherited fdmSearch: TFDMemTable
    Left = 874
    Top = 136
  end
  inherited pmGridConsultaPadrao: TPopupMenu
    Left = 744
    Top = 184
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42210.571244386570000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object pmSelecaoTitulos: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 76
    Top = 280
    object MenuItem1: TMenuItem
      Action = actAlterarColunasGridTitulos
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object MenuItem2: TMenuItem
      Action = actExibirAgrupamentoTitulos
    end
    object MenuItem3: TMenuItem
      Action = actRestaurarColunasTitulos
    end
    object MenuItem4: TMenuItem
      Action = ActSalvarConfiguracoesTitulos
    end
    object AlterarSQL2: TMenuItem
      Action = actAlterarSQLPesquisaPadraoTitulos
    end
  end
  object gpmSelecaoTitulos: TcxGridPopupMenu
    PopupMenus = <
      item
        HitTypes = [gvhtGridNone, gvhtNone, gvhtTab, gvhtCell, gvhtRecord, gvhtColumnHeader]
        Index = 0
        PopupMenu = pmSelecaoTitulos
      end>
    Left = 20
    Top = 280
  end
  object alSelecaoTitulos: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 46
    Top = 280
    object ActSalvarConfiguracoesTitulos: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesTitulosExecute
    end
    object actRestaurarColunasTitulos: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = actRestaurarColunasTitulosExecute
    end
    object actAlterarColunasGridTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = actAlterarColunasGridTitulosExecute
    end
    object actExibirAgrupamentoTitulos: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = actExibirAgrupamentoTitulosExecute
    end
    object actAlterarSQLPesquisaPadraoTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = actAlterarSQLPesquisaPadraoTitulosExecute
    end
    object actSelecionarTodos: TAction
      Category = 'filter'
      Caption = 'Adicionar todos os t'#237'tulos'
      Hint = 'Adicionar todos os t'#237'tulos ao lote'
      ImageIndex = 3
      OnExecute = actSelecionarTodosExecute
    end
    object actRemoverTodos: TAction
      Category = 'filter'
      Caption = 'Remover todos os t'#237'tulos'
      Hint = 'Remover todos os t'#237'tulos do lote'
      ImageIndex = 21
      OnExecute = actRemoverTodosExecute
    end
    object actFiltrarSelecaoTitulos: TAction
      Category = 'filter'
      ImageIndex = 11
      OnExecute = actFiltrarSelecaoTitulosExecute
    end
  end
  object dsLotePagamentoTitulo: TDataSource
    DataSet = cdsLotePagamentoTitulo
    Left = 870
    Top = 184
  end
  object cdsLotePagamentoTitulo: TGBClientDataSet
    Aggregates = <>
    AggregatesActive = True
    DataSetField = cdsDatafdqLotePagamentoTitulo
    Params = <>
    AfterPost = cdsLotePagamentoTituloAfterPost
    BeforeDelete = cdsLotePagamentoTituloBeforeDelete
    AfterDelete = cdsLotePagamentoTituloAfterDelete
    OnDeleteError = cdsDataDeleteError
    OnEditError = cdsDataEditError
    OnPostError = cdsDataPostError
    OnReconcileError = cdsDataReconcileError
    AfterApplyUpdates = cdsDataAfterApplyUpdates
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 840
    Top = 184
    object cdsLotePagamentoTituloID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsLotePagamentoTituloVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloID_LOTE_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'd. Lote Pagamento'
      FieldName = 'ID_LOTE_PAGAMENTO'
      Origin = 'ID_LOTE_PAGAMENTO'
    end
    object cdsLotePagamentoTituloID_CONTA_PAGAR: TIntegerField
      DisplayLabel = 'C'#243'd. Conta PAgar'
      FieldName = 'ID_CONTA_PAGAR'
      Origin = 'ID_CONTA_PAGAR'
      Required = True
    end
    object cdsLotePagamentoTituloDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      ProviderFlags = []
      Size = 50
    end
    object cdsLotePagamentoTituloDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsLotePagamentoTituloVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloVL_QUITADO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado'
      FieldName = 'VL_QUITADO'
      Origin = 'VL_QUITADO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloVL_ABERTO: TFMTBCDField
      DisplayLabel = 'Vl. Aberto'
      FieldName = 'VL_ABERTO'
      Origin = 'VL_ABERTO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloQT_PARCELA: TIntegerField
      DisplayLabel = 'Qt. Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloNR_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
      Size = 10
    end
    object cdsLotePagamentoTituloBO_VENCIDO: TStringField
      DisplayLabel = 'Vencido?'
      FieldName = 'BO_VENCIDO'
      Origin = 'BO_VENCIDO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object cdsLotePagamentoTituloOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloVL_QUITADO_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. Quitado T'#237'tulo'
      FieldName = 'VL_QUITADO_LIQUIDO'
      Origin = 'VL_QUITADO_LIQUIDO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'Id. Documento Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloTP_DOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Tp. Documento Origem'
      FieldName = 'TP_DOCUMENTO_ORIGEM'
      Origin = 'TP_DOCUMENTO_ORIGEM'
      ProviderFlags = []
      Size = 50
    end
    object cdsLotePagamentoTituloVL_ACRESCIMO_CP: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo CP'
      FieldName = 'VL_ACRESCIMO_CP'
      Origin = 'VL_ACRESCIMO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloVL_DESCONTO_CP: TFMTBCDField
      DisplayLabel = 'Vl. Desconto CP'
      FieldName = 'VL_DESCONTO_CP'
      Origin = 'VL_DECRESCIMO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloID_FILIAL: TIntegerField
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      ProviderFlags = []
    end
    object cdsLotePagamentoTituloVL_JUROS: TFMTBCDField
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloPERC_JUROS: TFMTBCDField
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloVL_MULTA: TFMTBCDField
      DisplayLabel = 'Vl. Multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloPERC_MULTA: TFMTBCDField
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoTituloJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'JOIN_NOME_PESSOA'
      ProviderFlags = []
      Size = 80
    end
    object cdsLotePagamentoTituloSUM_VL_ABERTO_CP: TAggregateField
      FieldName = 'SUM_VL_ABERTO_CP'
      Active = True
      DisplayName = ''
      DisplayFormat = '###,###,###,##0.00'
      Expression = 'SUM(VL_ABERTO)'
    end
  end
  object cdsLotePagamentoQuitacao: TGBClientDataSet
    Aggregates = <>
    AggregatesActive = True
    DataSetField = cdsDatafdqLotePagamentoQuitacao
    Params = <>
    AfterEdit = cdsLotePagamentoQuitacaoAfterEdit
    AfterPost = cdsLotePagamentoQuitacaoAfterPost
    AfterDelete = cdsLotePagamentoQuitacaoAfterDelete
    OnDeleteError = cdsDataDeleteError
    OnEditError = cdsDataEditError
    OnNewRecord = cdsLotePagamentoQuitacaoNewRecord
    OnPostError = cdsDataPostError
    OnReconcileError = cdsDataReconcileError
    AfterApplyUpdates = cdsDataAfterApplyUpdates
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 928
    Top = 184
    object cdsLotePagamentoQuitacaoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsLotePagamentoQuitacaoOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsLotePagamentoQuitacaoID_TIPO_QUITACAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo Quita'#231#227'o'
      FieldName = 'ID_TIPO_QUITACAO'
      Origin = 'ID_TIPO_QUITACAO'
      Required = True
    end
    object cdsLotePagamentoQuitacaoDT_QUITACAO: TDateField
      DisplayLabel = 'Dt. Quita'#231#227'o'
      FieldName = 'DT_QUITACAO'
      Origin = 'DT_QUITACAO'
      Required = True
      OnChange = cdsLotePagamentoQuitacaoDT_QUITACAOChange
    end
    object cdsLotePagamentoQuitacaoID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object cdsLotePagamentoQuitacaoVL_DESCONTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      OnChange = CalcularPercentual
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoVL_ACRESCIMO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      OnChange = CalcularPercentual
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoVL_QUITACAO: TFMTBCDField
      DisplayLabel = 'Vl. Quita'#231#227'o'
      FieldName = 'VL_QUITACAO'
      Origin = 'VL_QUITACAO'
      Required = True
      OnChange = cdsLotePagamentoQuitacaoVL_QUITACAOChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoNR_ITEM: TIntegerField
      DisplayLabel = 'Nr. Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsLotePagamentoQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField
      DisplayLabel = 'Troco'
      FieldKind = fkInternalCalc
      FieldName = 'CC_VL_TROCO_DIFERENCA'
      DisplayFormat = '###,###,###,##0.00'
    end
    object cdsLotePagamentoQuitacaoIC_VL_PAGAMENTO: TFloatField
      DisplayLabel = 'Vl. Pagamento'
      FieldKind = fkInternalCalc
      FieldName = 'IC_VL_PAGAMENTO'
      DisplayFormat = '###,###,###,##0.00'
    end
    object cdsLotePagamentoQuitacaoPERC_ACRESCIMO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      OnChange = CalcularValor
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsLotePagamentoQuitacaoPERC_DESCONTO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      OnChange = CalcularValor
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsLotePagamentoQuitacaoVL_TOTAL: TFMTBCDField
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      OnChange = CalcularValoresAPartirDoTotalQuitacao
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsLotePagamentoQuitacaoID_LOTE_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Lote Pagamento'
      FieldName = 'ID_LOTE_PAGAMENTO'
      Origin = 'ID_LOTE_PAGAMENTO'
    end
    object cdsLotePagamentoQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'JOIN_DESCRICAO_TIPO_QUITACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsLotePagamentoQuitacaoID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsLotePagamentoQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsLotePagamentoQuitacaoDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object cdsLotePagamentoQuitacaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object cdsLotePagamentoQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField
      FieldName = 'JOIN_TIPO_TIPO_QUITACAO'
      Origin = 'TIPO'
      ProviderFlags = []
      OnChange = cdsLotePagamentoQuitacaoJOIN_TIPO_TIPO_QUITACAOChange
      Size = 50
    end
    object cdsLotePagamentoQuitacaoID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Cart'#227'o: C'#243'digo da Operadora de Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsLotePagamentoQuitacaoCHEQUE_SACADO: TStringField
      DisplayLabel = 'Cheque: Sacado'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsLotePagamentoQuitacaoCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Cheque: Doc. Federal'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsLotePagamentoQuitacaoCHEQUE_BANCO: TStringField
      DisplayLabel = 'Cheque: Banco'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsLotePagamentoQuitacaoCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Cheque: Dt. Emiss'#227'o'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsLotePagamentoQuitacaoCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Cheque: Ag'#234'ncia'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsLotePagamentoQuitacaoCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Cheque: Conta Corrente'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsLotePagamentoQuitacaoCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'Cheque: N'#250'mero'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsLotePagamentoQuitacaoVL_JUROS: TFMTBCDField
      DisplayLabel = 'Vl. Juros'
      FieldName = 'VL_JUROS'
      Origin = 'VL_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoPERC_JUROS: TFMTBCDField
      DisplayLabel = '% Juros'
      FieldName = 'PERC_JUROS'
      Origin = 'PERC_JUROS'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoVL_MULTA: TFMTBCDField
      DisplayLabel = 'Vl. multa'
      FieldName = 'VL_MULTA'
      Origin = 'VL_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoPERC_MULTA: TFMTBCDField
      DisplayLabel = '% Multa'
      FieldName = 'PERC_MULTA'
      Origin = 'PERC_MULTA'
      DisplayFormat = '###,###,###,###,#0.00'
      Precision = 24
      Size = 9
    end
    object cdsLotePagamentoQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField
      DisplayLabel = 'Usu'#225'rio da Baixa'
      FieldName = 'JOIN_NOME_USUARIO_BAIXA'
      Origin = 'JOIN_NOME_USUARIO_BAIXA'
      ProviderFlags = []
      Size = 80
    end
    object cdsLotePagamentoQuitacaoID_USUARIO_BAIXA: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio da Baixa'
      FieldName = 'ID_USUARIO_BAIXA'
      Origin = 'ID_USUARIO_BAIXA'
    end
    object cdsLotePagamentoQuitacaoCHEQUE_DT_VENCIMENTO: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object cdsLotePagamentoQuitacaoSUM_VL_TOTAL_QUITACAO: TAggregateField
      FieldName = 'SUM_VL_TOTAL_QUITACAO'
      Active = True
      DisplayName = ''
      DisplayFormat = '###,###,###,##0.00'
      Expression = 'SUM(VL_TOTAL)'
    end
  end
  object dsLotePagamentoQuitacao: TDataSource
    DataSet = cdsLotePagamentoQuitacao
    Left = 958
    Top = 184
  end
  object filtrosTitulos: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 296
    Top = 184
    object filtrosTitulosID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
    end
    object filtrosTitulosDT_VENCIMENTO_INICIAL: TDateField
      FieldName = 'DT_VENCIMENTO_INICIAL'
    end
    object filtrosTitulosDT_VENCIMENTO_FINAL: TDateField
      FieldName = 'DT_VENCIMENTO_FINAL'
    end
    object filtrosTitulosDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
    end
    object filtrosTitulosJOIN_DESCRICAO_NOME_PESSOA: TStringField
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
    end
  end
  object fdmSelecaoTitulos: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 18
    Top = 328
  end
  object dsSelecaoTitulos: TDataSource
    DataSet = fdmSelecaoTitulos
    Left = 56
    Top = 327
  end
  object dsFiltrosTitulos: TDataSource
    DataSet = filtrosTitulos
    Left = 356
    Top = 189
  end
  object pmLotePagamentoTitulos: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 604
    Top = 288
    object MenuItem5: TMenuItem
      Action = actAlterarRotuloLoteTitulos
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object MenuItem6: TMenuItem
      Action = actExibirAgrupamentoLoteTitulos
    end
    object MenuItem7: TMenuItem
      Action = actRestaurarColunasLoteTitulos
    end
    object MenuItem8: TMenuItem
      Action = actSalvarConfiguracaoLoteTitulos
    end
  end
  object alLotePagamentoTitulo: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 566
    Top = 288
    object actSalvarConfiguracaoLoteTitulos: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = actSalvarConfiguracaoLoteTitulosExecute
    end
    object actRestaurarColunasLoteTitulos: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = actRestaurarColunasLoteTitulosExecute
    end
    object actAlterarRotuloLoteTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = actAlterarRotuloLoteTitulosExecute
    end
    object actExibirAgrupamentoLoteTitulos: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = actExibirAgrupamentoLoteTitulosExecute
    end
    object actAlterarSQLLoteTitulos: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = actAlterarSQLLoteTitulosExecute
    end
  end
  object gpmLotePagamentoTitulo: TcxGridPopupMenu
    PopupMenus = <
      item
        HitTypes = [gvhtGridNone, gvhtNone, gvhtTab, gvhtCell, gvhtRecord, gvhtColumnHeader]
        Index = 0
        PopupMenu = pmLotePagamentoTitulos
      end>
    Left = 524
    Top = 288
  end
end
