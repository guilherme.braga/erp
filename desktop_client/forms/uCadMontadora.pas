unit uCadMontadora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, uGBDBTextEditPK, cxCheckBox, cxDBEdit, uGBDBCheckBox, cxMaskEdit, cxDropDownEdit, cxBlobEdit,
  uGBDBBlobEdit, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls;

type
  TCadMontadora = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBBlobEdit1: TgbDBBlobEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadMontadora);

finalization
  UnRegisterClass(TCadMontadora);


end.
