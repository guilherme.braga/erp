inherited CadCheque: TCadCheque
  Caption = 'Cadastro de Cheque'
  ClientHeight = 481
  ClientWidth = 910
  ExplicitWidth = 926
  ExplicitHeight = 520
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 910
    ExplicitWidth = 910
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 910
    Height = 354
    Properties.ActivePage = cxtsData
    ExplicitWidth = 910
    ExplicitHeight = 354
    ClientRectBottom = 354
    ClientRectRight = 910
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 910
      ExplicitHeight = 330
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 878
        Height = 330
        ExplicitWidth = 910
        ExplicitHeight = 330
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 330
        Height = 330
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 330
        ExplicitHeight = 330
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 910
      ExplicitHeight = 330
      inherited DesignPanel: TJvDesignPanel
        Width = 910
        Height = 330
        ExplicitWidth = 910
        ExplicitHeight = 330
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 910
          ExplicitHeight = 330
          DesignSize = (
            910
            330)
          Height = 330
          Width = 910
          inherited dxBevel1: TdxBevel
            Width = 906
            ExplicitLeft = -182
            ExplicitTop = 1
            ExplicitWidth = 919
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelSacado: TLabel
            Left = 213
            Top = 42
            Width = 35
            Height = 13
            Caption = 'Sacado'
          end
          object labelDocFederal: TLabel
            Left = 8
            Top = 67
            Width = 61
            Height = 13
            Caption = 'Doc. Federal'
          end
          object labelVlCheque: TLabel
            Left = 712
            Top = 92
            Width = 52
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Vl. Cheque'
            ExplicitLeft = 725
          end
          object labelNumero: TLabel
            Left = 712
            Top = 67
            Width = 37
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'N'#250'mero'
            ExplicitLeft = 725
          end
          object labelBanco: TLabel
            Left = 213
            Top = 67
            Width = 29
            Height = 13
            Caption = 'Banco'
          end
          object labelAgencia: TLabel
            Left = 8
            Top = 92
            Width = 38
            Height = 13
            Caption = 'Ag'#234'ncia'
          end
          object labelContaCorrente: TLabel
            Left = 213
            Top = 92
            Width = 75
            Height = 13
            Caption = 'Conta Corrente'
          end
          object labelDtEmissao: TLabel
            Left = 8
            Top = 42
            Width = 56
            Height = 13
            Caption = 'Dt. Emiss'#227'o'
          end
          object labelDtVencimento: TLabel
            Left = 712
            Top = 42
            Width = 55
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Vencimento'
            ExplicitLeft = 725
          end
          object labelProcesso: TLabel
            Left = 149
            Top = 9
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object labelOrigem: TLabel
            Left = 549
            Top = 8
            Width = 34
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Origem'
            ExplicitLeft = 562
          end
          object lbContaCorrente: TLabel
            Left = 8
            Top = 118
            Width = 75
            Height = 13
            Caption = 'Conta Corrente'
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 86
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edtSacado: TgbDBTextEdit
            Left = 289
            Top = 38
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'SACADO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbRequired = True
            gbPassword = False
            Width = 411
          end
          object edtDocFederal: TgbDBTextEdit
            Left = 86
            Top = 63
            DataBinding.DataField = 'DOC_FEDERAL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            OnExit = edtDocFederalExit
            gbRequired = True
            gbPassword = False
            Width = 121
          end
          object edtVlCheque: TgbDBTextEdit
            Left = 777
            Top = 88
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'VL_CHEQUE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 13
            gbRequired = True
            gbPassword = False
            Width = 130
          end
          object edtNumero: TgbDBTextEdit
            Left = 777
            Top = 63
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'NUMERO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            gbRequired = True
            gbPassword = False
            Width = 130
          end
          object edtBanco: TgbDBTextEdit
            Left = 291
            Top = 63
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'BANCO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbRequired = True
            gbPassword = False
            Width = 409
          end
          object edtAgencia: TgbDBTextEdit
            Left = 86
            Top = 88
            DataBinding.DataField = 'AGENCIA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 11
            gbRequired = True
            gbPassword = False
            Width = 121
          end
          object edtContaCorrente: TgbDBTextEdit
            Left = 291
            Top = 88
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'CONTA_CORRENTE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 12
            gbRequired = True
            gbPassword = False
            Width = 409
          end
          object edtDtEmissao: TgbDBDateEdit
            Left = 86
            Top = 38
            DataBinding.DataField = 'DT_EMISSAO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnToday]
            Properties.ImmediatePost = True
            Properties.SaveTime = False
            Properties.ShowTime = False
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbRequired = True
            gbDateTime = False
            Width = 121
          end
          object edtDtVencimento: TgbDBDateEdit
            Left = 777
            Top = 38
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DT_VENCIMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnToday]
            Properties.ImmediatePost = True
            Properties.SaveTime = False
            Properties.ShowTime = False
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbRequired = True
            gbDateTime = False
            Width = 130
          end
          object edtDescProcesso: TgbDBTextEdit
            Left = 263
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 282
          end
          object edtProcesso: TgbDBTextEdit
            Left = 195
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbPassword = False
            Width = 71
          end
          object edtOrigem: TgbDBTextEdit
            Left = 584
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 61
          end
          object edtDescOrigem: TgbDBTextEdit
            Left = 642
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DOCUMENTO_ORIGEM'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 265
          end
          object EdtDhConciliacao: TgbDBDateEdit
            Left = 777
            Top = 114
            TabStop = False
            Anchors = [akTop, akRight]
            AutoSize = False
            DataBinding.DataField = 'DH_CONCILIADO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 16
            gbReadyOnly = True
            gbDateTime = True
            Height = 21
            Width = 130
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 707
            Top = 114
            Anchors = [akTop, akRight]
            Caption = 'Conciliado'
            DataBinding.DataField = 'BO_CONCILIADO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 17
            Transparent = True
            Width = 72
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 86
            Top = 114
            DataBinding.DataField = 'ID_CONTA_CORRENTE'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 14
            gbTextEdit = descContaCorrente
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
            gbTableName = 'CONTA_CORRENTE'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbIdentificadorConsulta = 'CONTA_CORRENTE'
            Width = 60
          end
          object descContaCorrente: TgbDBTextEdit
            Left = 143
            Top = 114
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 15
            gbReadyOnly = True
            gbPassword = False
            Width = 558
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Cheque'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspCheque'
    RemoteServer = DmConnection.dspCheque
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataSACADO: TStringField
      DisplayLabel = 'Sacado'
      FieldName = 'SACADO'
      Origin = 'SACADO'
      Size = 80
    end
    object cdsDataDOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal'
      FieldName = 'DOC_FEDERAL'
      Origin = 'DOC_FEDERAL'
      Size = 14
    end
    object cdsDataVL_CHEQUE: TFMTBCDField
      DisplayLabel = 'Vl. Cheque'
      FieldName = 'VL_CHEQUE'
      Origin = 'VL_CHEQUE'
      Required = True
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataNUMERO: TIntegerField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object cdsDataBANCO: TStringField
      DisplayLabel = 'Banco'
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 80
    end
    object cdsDataAGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object cdsDataCONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'CONTA_CORRENTE'
      Origin = 'CONTA_CORRENTE'
      Size = 10
    end
    object cdsDataDT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'DT_EMISSAO'
      Origin = 'DT_EMISSAO'
    end
    object cdsDataDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
      Required = True
    end
    object cdsDataBO_CONCILIADO: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Conciliado'
      FieldName = 'BO_CONCILIADO'
      Origin = 'BO_CONCILIADO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataDH_CONCILIADO: TDateTimeField
      DisplayLabel = 'Dh. Concilia'#231#227'o'
      FieldName = 'DH_CONCILIADO'
      Origin = 'DH_CONCILIADO'
    end
    object cdsDataID_DOCUMENTO_ORIGEM: TIntegerField
      DisplayLabel = 'C'#243'digo do Documento de Origem'
      FieldName = 'ID_DOCUMENTO_ORIGEM'
      Origin = 'ID_DOCUMENTO_ORIGEM'
    end
    object cdsDataDOCUMENTO_ORIGEM: TStringField
      DisplayLabel = 'Documento de Origem'
      FieldName = 'DOCUMENTO_ORIGEM'
      Origin = 'DOCUMENTO_ORIGEM'
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Processo de Origem'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42246.987946064830000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
