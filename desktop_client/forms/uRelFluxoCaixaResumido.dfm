inherited RelFluxoCaixaResumido: TRelFluxoCaixaResumido
  Caption = 'Fluxo de Caixa - Saldo de Movimenta'#231#227'o'
  ExplicitWidth = 865
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxPeriodo'
        end
        item
          ToolbarName = 'dxBarOperacao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    object pgFluxoCaixaResumido: TcxPageControl
      Left = 2
      Top = 2
      Width = 845
      Height = 334
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      PopupMenu = dxRibbonRadialMenu
      TabOrder = 0
      Properties.ActivePage = tsEstruturaFluxoCaixaResumido
      Properties.ActivateFocusedTab = False
      Properties.CustomButtons.Buttons = <>
      Properties.Style = 8
      LookAndFeel.Kind = lfOffice11
      ClientRectBottom = 334
      ClientRectRight = 845
      ClientRectTop = 24
      object tsContaCorrente: TcxTabSheet
        Caption = 'Conta Corrente'
        ImageIndex = 0
        object gridContaCorrente: TcxGrid
          Left = 0
          Top = 0
          Width = 845
          Height = 310
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Images = DmAcesso.cxImage16x16
          ParentFont = False
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object ViewContaCorrente: TcxGridDBBandedTableView
            PopupMenu = dxRibbonRadialMenu
            OnKeyDown = ViewContaCorrenteKeyDown
            Navigator.Buttons.OnButtonClick = ViewContaCorrenteNavigatorButtonsButtonClick
            Navigator.Buttons.CustomButtons = <
              item
                Hint = 'Ordenar para cima (Ctrl + Seta para Cima)'
                ImageIndex = 113
              end
              item
                Hint = 'Ordenar para baixo (Ctrl + Seta para Baixo)'
                ImageIndex = 112
              end>
            Navigator.Buttons.Images = DmAcesso.cxImage16x16
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsContaCorrente
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Images = DmAcesso.cxImage16x16
            OptionsBehavior.ExpandMasterRowOnDblClick = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsCustomize.NestedBands = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.ShowColumnFilterButtons = sfbAlways
            Bands = <
              item
                Caption = 'Contas Correntes'
              end>
          end
          object cxGridLevel12: TcxGridLevel
            GridView = ViewContaCorrente
          end
        end
      end
      object tsEstruturaFluxoCaixaResumido: TcxTabSheet
        Caption = 'Fluxo de Caixa'
        ImageIndex = 1
        object ViewEstruturaFluxoCaixaResumido: TcxDBVerticalGrid
          Left = 0
          Top = 0
          Width = 845
          Height = 310
          Align = alClient
          LayoutStyle = lsMultiRecordView
          LookAndFeel.Kind = lfOffice11
          OptionsView.CellAutoHeight = True
          OptionsView.RowHeaderWidth = 97
          OptionsData.CancelOnExit = False
          OptionsData.Editing = False
          OptionsData.Appending = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          Navigator.Buttons.CustomButtons = <>
          Styles.ContentOdd = DmAcesso.OddColor
          TabOrder = 0
          DataController.DataSource = dsEstruturaFluxoCaixaResumido
          Version = 1
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    inherited ActConfirm: TAction
      Visible = False
    end
    inherited ActCancel: TAction
      Visible = False
    end
    object ActAnterior: TAction
      Category = 'Action'
      Caption = '&Anterior Ctrl+A'
      Hint = 'Clique sobre o bot'#227'o para visualizar o n'#237'vel anterior'
      ImageIndex = 18
      ShortCut = 16449
      OnExecute = ActAnteriorExecute
    end
    object ActProximo: TAction
      Category = 'Action'
      Caption = '&Pr'#243'ximo Ctrl+P'
      Hint = 'Visualizar o pr'#243'ximo n'#237'vel'
      ImageIndex = 17
      ShortCut = 16464
      OnExecute = ActProximoExecute
    end
    object ActAtualizar: TAction
      Category = 'Action'
      Caption = 'A&tualizar F5'
      ImageIndex = 88
      ShortCut = 116
      OnExecute = ActAtualizarExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Operacoes'
      'AcoesGrids')
    Categories.ItemsVisibles = (
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 425
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockControl = nil
      DockedDockControl = nil
      DockingStyle = dsNone
      FloatLeft = 785
      FloatTop = 198
      FloatClientWidth = 70
      FloatClientHeight = 108
      Visible = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 337
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object dxBarOperacao: TdxBar [3]
      Caption = 'Opera'#231#245'es'
      CaptionButtons = <>
      DockedLeft = 147
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarAtualizar'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxPeriodo: TdxBar [4]
      Caption = 'Per'#237'odo de Compet'#234'ncia'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'FiltroPeriodoInicio'
        end
        item
          Visible = True
          ItemName = 'FiltroPeriodoFim'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSeparator1: TdxBarSeparator [8]
      Caption = 'New Separator'
      Category = 0
      Hint = 'New Separator'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem [9]
      Caption = 'Apura'#231#227'o'
      Category = 0
      Hint = 'Apura'#231#227'o'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object FiltroTipoPeriodo: TcxBarEditItem [10]
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = 'Di'#225'rio'
          Value = 'DIARIO'
        end
        item
          Caption = 'Mensal'
          Value = 'MENSAL'
        end>
      Properties.OnEditValueChanged = FiltroTipoPeriodoPropertiesEditValueChanged
    end
    object dxBarButton1: TdxBarButton [11]
      Action = ActExportarPDF
      Category = 0
    end
    object dxBarButton2: TdxBarButton [12]
      Action = ActImprimirGrade
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = ActAnterior
      Category = 2
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = ActProximo
      Category = 2
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = ActConfirm
      Category = 2
    end
    object dxBarAtualizar: TdxBarLargeButton
      Action = ActAtualizar
      Category = 2
    end
    object FiltroPeriodoInicio: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object FiltroPeriodoFim: TcxBarEditItem
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object filtroPeriodoRotulo: TdxBarStatic
      Caption = 'Per'#237'odo de Movimenta'#231#227'o'
      Category = 2
      Hint = 'Per'#237'odo de Movimenta'#231#227'o'
      Visible = ivAlways
    end
    object bbExplorarRegistroSelecionado: TdxBarButton
      Action = ActAbrirMovimentoSelecionado
      Category = 3
    end
    object bbExplorarTodosRegistros: TdxBarButton
      Action = ActAbrirTodosRegistros
      Category = 3
    end
    object bbExportarExcel: TdxBarButton
      Action = ActExportarExcel
      Category = 3
    end
    object bbExibirAgrupamento: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 3
    end
    object bbExpandirAgrupamentos: TdxBarButton
      Action = ActExpandirTudo
      Category = 3
    end
    object bbRecolherAgrupamentos: TdxBarButton
      Action = ActRecolherAgrupamento
      Category = 3
    end
    object bbAlterarRotulo: TdxBarButton
      Action = ActAlterarColunasGrid
      Category = 3
    end
    object bbRestaurarConfiguracoes: TdxBarButton
      Action = ActRestaurarColunasPadrao
      Category = 3
    end
  end
  inherited alAcoes16x16: TActionList
    object ActExportarExcel: TAction
      Caption = 'Exportar para Excel'
      Hint = 'Exportar grade para Excel'
      ImageIndex = 274
      OnExecute = ActExportarExcelExecute
    end
    object ActExibirAgrupamento: TAction
      Caption = 'Exibir Agrupamento'
      Hint = 'Mostrar barra de agrupamento'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActExpandirTudo: TAction
      Caption = 'Expandir Agrupamentos'
      Hint = 'Expandir agrupamentos'
      ImageIndex = 117
      OnExecute = ActExpandirTudoExecute
    end
    object ActRecolherAgrupamento: TAction
      Caption = 'Recolher Agrupamento'
      Hint = 'Oculta o detalhamento dos agrupamentos'
      ImageIndex = 116
      OnExecute = ActRecolherAgrupamentoExecute
    end
    object ActExportarPDF: TAction
      Caption = 'Exportar para PDF'
      Hint = 'Exportar grade para PDF'
      ImageIndex = 273
      OnExecute = ActExportarPDFExecute
    end
    object ActAbrirMovimentoSelecionado: TAction
      Caption = 'Explorar Registro Selecionado'
      ImageIndex = 11
      Visible = False
    end
    object ActAbrirTodosRegistros: TAction
      Caption = 'Explorar Todos os Registros'
      ImageIndex = 11
      Visible = False
    end
    object ActImprimirGrade: TAction
      Caption = 'Imprimir Fluxo de Caixa'
      ImageIndex = 12
      OnExecute = ActImprimirGradeExecute
    end
  end
  object fdmContaCorrente: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 72
    Top = 264
  end
  object fdmEstruturaFluxoCaixaResumido: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 184
    Top = 264
  end
  object dsContaCorrente: TDataSource
    DataSet = fdmContaCorrente
    Left = 72
    Top = 312
  end
  object dsEstruturaFluxoCaixaResumido: TDataSource
    DataSet = fdmEstruturaFluxoCaixaResumido
    Left = 184
    Top = 312
  end
  object fdmConsultaDados: TgbFDMemTable
    FetchOptions.AssignedValues = [evMode, evRecordCountMode]
    FetchOptions.Mode = fmAll
    FetchOptions.RecordCountMode = cmTotal
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 440
    Top = 232
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 604
    Top = 16
    object ExplorarTodososRegistros2: TMenuItem
      Action = ActAbrirMovimentoSelecionado
    end
    object ExplorarTodososRegistros1: TMenuItem
      Action = ActAbrirTodosRegistros
    end
    object ExportarparaExcel1: TMenuItem
      Action = ActExportarExcel
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ExportarparaPDF1: TMenuItem
      Action = ActExibirAgrupamento
    end
    object ExpandirAgrupamentos1: TMenuItem
      Action = ActExpandirTudo
    end
    object RecolherAgrupamento1: TMenuItem
      Action = ActRecolherAgrupamento
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ActRestaurarColunasPadrao1: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
  end
  object ACExtratoConta16x16: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 472
    Top = 16
    object ActAlterarColunasGrid: TAction
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Caption = 'Restaurar Configura'#231#245'es Iniciais'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 664
    Top = 72
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = gridContaCorrente
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
  object dxRibbonRadialMenu: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbAlterarRotulo'
      end
      item
        Visible = True
        ItemName = 'bbRestaurarConfiguracoes'
      end
      item
        Visible = True
        ItemName = 'bbExibirAgrupamento'
      end
      item
        Visible = True
        ItemName = 'bbExpandirAgrupamentos'
      end
      item
        Visible = True
        ItemName = 'bbRecolherAgrupamentos'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'bbExportarExcel'
      end
      item
        Visible = True
        ItemName = 'dxBarButton1'
      end
      item
        Visible = True
        ItemName = 'dxBarButton2'
      end>
    Images = DmAcesso.cxImage16x16
    Ribbon = dxRibbon1
    UseOwnFont = False
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 352
    Top = 224
  end
end
