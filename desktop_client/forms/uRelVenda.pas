unit uRelVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmRelatorioFRPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxButtonEdit, cxDropDownEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, uGBClientDataset, dxBar, cxBarEditItem, dxBarExtItems,
  System.Actions, Vcl.ActnList, cxVGrid, cxInplaceContainer, uGBPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxGroupBox, dxRibbon;

type
  TRelVenda = class(TFrmRelatorioFRPadrao)
    FdmFiltrosID: TIntegerField;
    FdmFiltrosDT_CADASTRO_INICIO: TDateField;
    FdmFiltrosDT_CADASTRO_TERMINO: TDateField;
    FdmFiltrosDT_FECHAMENTO_INICIO: TDateField;
    FdmFiltrosDT_FECHAMENTO_TERMINO: TDateField;
    FdmFiltrosVL_VENDA: TFloatField;
    FdmFiltrosSTATUS: TStringField;
    FdmFiltrosID_PESSOA: TIntegerField;
    FdmFiltrosTIPO: TStringField;
    rowPessoa: TcxEditorRow;
    vgFiltrosEditorRow2: TcxEditorRow;
    vgFiltrosEditorRow3: TcxEditorRow;
    procedure vgFiltrosEditorRow1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure SetWhereBundle; override;
  end;

var
  RelVenda: TRelVenda;

implementation

{$R *.dfm}

uses uVendaProxy, uFrmConsultaPadrao, uPessoaProxy;

{ TRelVenda }

procedure TRelVenda.SetWhereBundle;
begin
  inherited;
  AddFiltroIgual(TVendaProxy.FIELD_ID,FdmFiltrosID);

  AddFiltroEntre(TVendaProxy.FIELD_DATA_CADASTRO,
    FdmFiltrosDT_CADASTRO_INICIO, FdmFiltrosDT_CADASTRO_TERMINO);

  AddFiltroEntre(TVendaProxy.FIELD_DATA_CADASTRO,
    FdmFiltrosDT_FECHAMENTO_INICIO, FdmFiltrosDT_FECHAMENTO_TERMINO);

  AddFiltroIgual(TVendaProxy.FIELD_VALOR_VENDA,FdmFiltrosVL_VENDA);
  AddFiltroIgual(TVendaProxy.FIELD_STATUS,FdmFiltrosSTATUS);
  AddFiltroIgual(TVendaProxy.FIELD_ID_PESSOA,FdmFiltrosID_PESSOA);
  AddFiltroIgual(TVendaProxy.FIELD_TIPO,FdmFiltrosTIPO);
end;

procedure TRelVenda.vgFiltrosEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(TPessoaProxy.NOME_TABELA);

  if IDConsultado > 0 then
  begin
    rowPessoa.Properties.Value := IDConsultado;
  end;
end;

initialization
  RegisterClass(TRelVenda);

Finalization
  UnRegisterClass(TRelVenda);

end.

