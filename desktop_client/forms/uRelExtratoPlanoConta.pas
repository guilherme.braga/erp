unit uRelExtratoPlanoConta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxCalendar, dxBarExtItems,
  cxBarEditItem, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, dxLayoutContainer, cxGridViewLayoutContainer,
  cxGridLayoutView, cxGridDBLayoutView, cxGridCustomLayoutView,
  cxGridDBTableView, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid, cxPC, DateUtils,
  Vcl.Menus, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, cxDBLookupComboBox, cxDropDownEdit, dxRibbonRadialMenu, cxLabel,
  uUsuarioDesignControl, cxRadioGroup, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TRelExtratoPlanoConta = class(TFrmChildPadrao)
    ActProximo: TAction;
    ActAnterior: TAction;
    ActAtualizar: TAction;
    dxBarOperacao: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxPeriodo: TdxBar;
    dxBarAtualizar: TdxBarLargeButton;
    FiltroPeriodoInicio: TcxBarEditItem;
    FiltroPeriodoFim: TcxBarEditItem;
    filtroPeriodoRotulo: TdxBarStatic;
    PageControlRelatorios: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControlHibrido: TcxPageControl;
    cxCentroResultadoH: TcxTabSheet;
    gridCentroResultado: TcxGrid;
    ViewCentroResultado: TcxGridDBBandedTableView;
    cxGridLevel12: TcxGridLevel;
    cxPrimeiroNivelH: TcxTabSheet;
    gridPrimeiroNivel: TcxGrid;
    ViewPrimeiroNivel: TcxGridDBBandedTableView;
    cxGridLevel20: TcxGridLevel;
    cxSegundoNivelH: TcxTabSheet;
    gridSegundoNivel: TcxGrid;
    ViewSegundoNivel: TcxGridDBBandedTableView;
    cxGridLevel21: TcxGridLevel;
    cxTerceiroNivelH: TcxTabSheet;
    gridTerceiroNivel: TcxGrid;
    ViewTerceiroNivel: TcxGridDBBandedTableView;
    cxGridLevel22: TcxGridLevel;
    cxQuartoNivelH: TcxTabSheet;
    gridQuartoNivel: TcxGrid;
    ViewQuartoNivel: TcxGridDBBandedTableView;
    cxGridLevel23: TcxGridLevel;
    cxContaCorrenteH: TcxTabSheet;
    gridContaCorrente: TcxGrid;
    ViewContaCorrente: TcxGridDBBandedTableView;
    cxGridLevel24: TcxGridLevel;
    cxMovimentacaoH: TcxTabSheet;
    gridMovimentacao: TcxGrid;
    cxGridDBTableView124: TcxGridDBTableView;
    cxGridDBColumn983: TcxGridDBColumn;
    cxGridDBColumn984: TcxGridDBColumn;
    cxGridDBTableView125: TcxGridDBTableView;
    cxGridDBColumn985: TcxGridDBColumn;
    cxGridDBColumn986: TcxGridDBColumn;
    cxGridDBColumn987: TcxGridDBColumn;
    cxGridDBColumn988: TcxGridDBColumn;
    cxGridDBColumn989: TcxGridDBColumn;
    cxGridDBColumn990: TcxGridDBColumn;
    cxGridDBColumn991: TcxGridDBColumn;
    cxGridDBTableView126: TcxGridDBTableView;
    cxGridDBColumn992: TcxGridDBColumn;
    cxGridDBColumn993: TcxGridDBColumn;
    cxGridDBColumn994: TcxGridDBColumn;
    cxGridDBColumn995: TcxGridDBColumn;
    cxGridDBColumn996: TcxGridDBColumn;
    cxGridDBColumn997: TcxGridDBColumn;
    cxGridDBColumn998: TcxGridDBColumn;
    cxGridDBColumn999: TcxGridDBColumn;
    cxGridDBTableView127: TcxGridDBTableView;
    cxGridDBColumn1000: TcxGridDBColumn;
    cxGridDBColumn1001: TcxGridDBColumn;
    cxGridDBColumn1002: TcxGridDBColumn;
    cxGridDBColumn1003: TcxGridDBColumn;
    cxGridDBColumn1004: TcxGridDBColumn;
    cxGridDBColumn1005: TcxGridDBColumn;
    cxGridDBColumn1006: TcxGridDBColumn;
    cxGridDBColumn1007: TcxGridDBColumn;
    cxGridDBTableView128: TcxGridDBTableView;
    cxGridDBTableView129: TcxGridDBTableView;
    cxGridDBColumn1008: TcxGridDBColumn;
    cxGridDBColumn1009: TcxGridDBColumn;
    cxGridDBColumn1010: TcxGridDBColumn;
    cxGridDBColumn1011: TcxGridDBColumn;
    cxGridDBColumn1012: TcxGridDBColumn;
    cxGridDBColumn1013: TcxGridDBColumn;
    cxGridDBColumn1014: TcxGridDBColumn;
    cxGridDBColumn1015: TcxGridDBColumn;
    cxGridDBTableView130: TcxGridDBTableView;
    cxGridDBColumn1016: TcxGridDBColumn;
    cxGridDBColumn1017: TcxGridDBColumn;
    cxGridDBColumn1018: TcxGridDBColumn;
    cxGridDBColumn1019: TcxGridDBColumn;
    cxGridDBColumn1020: TcxGridDBColumn;
    cxGridDBColumn1021: TcxGridDBColumn;
    cxGridDBTableView131: TcxGridDBTableView;
    cxGridDBColumn1022: TcxGridDBColumn;
    cxGridDBColumn1023: TcxGridDBColumn;
    cxGridDBColumn1024: TcxGridDBColumn;
    cxGridDBColumn1025: TcxGridDBColumn;
    cxGridDBColumn1026: TcxGridDBColumn;
    cxGridDBColumn1027: TcxGridDBColumn;
    cxGridDBColumn1028: TcxGridDBColumn;
    cxGridDBColumn1029: TcxGridDBColumn;
    cxGridDBColumn1030: TcxGridDBColumn;
    cxGridDBColumn1031: TcxGridDBColumn;
    cxGridDBColumn1032: TcxGridDBColumn;
    cxGridDBColumn1033: TcxGridDBColumn;
    cxGridDBColumn1034: TcxGridDBColumn;
    cxGridDBColumn1035: TcxGridDBColumn;
    cxGridDBColumn1036: TcxGridDBColumn;
    cxGridDBColumn1037: TcxGridDBColumn;
    cxGridDBColumn1038: TcxGridDBColumn;
    cxGridDBColumn1039: TcxGridDBColumn;
    cxGridDBColumn1040: TcxGridDBColumn;
    cxGridDBColumn1041: TcxGridDBColumn;
    cxGridDBColumn1042: TcxGridDBColumn;
    cxGridDBColumn1043: TcxGridDBColumn;
    cxGridDBColumn1044: TcxGridDBColumn;
    cxGridDBColumn1045: TcxGridDBColumn;
    cxGridDBColumn1046: TcxGridDBColumn;
    cxGridDBColumn1047: TcxGridDBColumn;
    cxGridDBColumn1048: TcxGridDBColumn;
    cxGridDBColumn1049: TcxGridDBColumn;
    cxGridDBColumn1050: TcxGridDBColumn;
    cxGridDBLayoutView27: TcxGridDBLayoutView;
    cxGridDBLayoutViewItem131: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem132: TcxGridDBLayoutViewItem;
    dxLayoutGroup27: TdxLayoutGroup;
    cxGridLayoutItem131: TcxGridLayoutItem;
    cxGridLayoutItem132: TcxGridLayoutItem;
    cxGridDBLayoutView28: TcxGridDBLayoutView;
    cxGridDBLayoutViewItem133: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem134: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem135: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem136: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem137: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem138: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem139: TcxGridDBLayoutViewItem;
    cxGridDBLayoutViewItem140: TcxGridDBLayoutViewItem;
    dxLayoutGroup28: TdxLayoutGroup;
    cxGridLayoutItem133: TcxGridLayoutItem;
    cxGridLayoutItem134: TcxGridLayoutItem;
    cxGridLayoutItem135: TcxGridLayoutItem;
    cxGridLayoutItem136: TcxGridLayoutItem;
    cxGridLayoutItem137: TcxGridLayoutItem;
    cxGridLayoutItem138: TcxGridLayoutItem;
    cxGridLayoutItem139: TcxGridLayoutItem;
    cxGridLayoutItem140: TcxGridLayoutItem;
    ViewMovimentacao: TcxGridDBBandedTableView;
    cxGridDBBandedTableView106: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1037: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1038: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1039: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1040: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1041: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1042: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1043: TcxGridDBBandedColumn;
    cxGridDBBandedTableView107: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1044: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1045: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1046: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1047: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1048: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1049: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1050: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1051: TcxGridDBBandedColumn;
    cxGridDBBandedTableView108: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1052: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1053: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1054: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1055: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1056: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1057: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1058: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1059: TcxGridDBBandedColumn;
    cxGridDBBandedTableView109: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1060: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1061: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1062: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1063: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1064: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1065: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1066: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1067: TcxGridDBBandedColumn;
    cxGridDBBandedTableView110: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1068: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1069: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1070: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1071: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1072: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1073: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1074: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1075: TcxGridDBBandedColumn;
    cxGridDBBandedTableView111: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1076: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1077: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1078: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1079: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1080: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1081: TcxGridDBBandedColumn;
    cxGridDBBandedTableView112: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1082: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1083: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1084: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1085: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1086: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1087: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1088: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1089: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1090: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1091: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1092: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1093: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1094: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1095: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1096: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1097: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1098: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1099: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1100: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1101: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1102: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1103: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1104: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1105: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1106: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1107: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1108: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1109: TcxGridDBBandedColumn;
    cxGridDBBandedColumn1110: TcxGridDBBandedColumn;
    cxGridLevel25: TcxGridLevel;
    fdmCentroResultado: TFDMemTable;
    fdmPrimeiroNivel: TFDMemTable;
    fdmSegundoNivel: TFDMemTable;
    fdmTerceiroNivel: TFDMemTable;
    fdmQuartoNivel: TFDMemTable;
    dsCentroResultado: TDataSource;
    dsPrimeiroNivel: TDataSource;
    dsSegundoNivel: TDataSource;
    dsTerceiroNivel: TDataSource;
    dsQuartoNivel: TDataSource;
    fdmContaCorrente: TFDMemTable;
    dsContaCorrente: TDataSource;
    dsMovimentacao: TDataSource;
    fdmMovimentacao: TFDMemTable;
    fdmConsultaDados: TFDMemTable;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    ACExtratoConta16x16: TActionList;
    ActAlterarColunasGrid: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActRestaurarColunasPadrao1: TMenuItem;
    ActExportarExcel: TAction;
    ActExibirAgrupamento: TAction;
    ExportarparaExcel1: TMenuItem;
    ActExpandirTudo: TAction;
    ActRecolherAgrupamento: TAction;
    ActExportarPDF: TAction;
    ExpandirAgrupamentos1: TMenuItem;
    RecolherAgrupamento1: TMenuItem;
    ExportarparaPDF1: TMenuItem;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    N1: TMenuItem;
    N2: TMenuItem;
    ActAbrirMovimentoSelecionado: TAction;
    ActAbrirTodosRegistros: TAction;
    ExplorarTodososRegistros1: TMenuItem;
    ExplorarTodososRegistros2: TMenuItem;
    dxRibbonRadialMenu: TdxRibbonRadialMenu;
    bbExplorarRegistroSelecionado: TdxBarButton;
    bbExplorarTodosRegistros: TdxBarButton;
    bbExportarExcel: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    bbExpandirAgrupamentos: TdxBarButton;
    bbRecolherAgrupamentos: TdxBarButton;
    bbAlterarRotulo: TdxBarButton;
    bbRestaurarConfiguracoes: TdxBarButton;
    dxBarSeparator1: TdxBarSeparator;
    cxBarEditItem1: TcxBarEditItem;
    dxBarStatic1: TdxBarStatic;
    filtroNone: TdxBarStatic;
    filtroExibirPrevisao: TcxBarEditItem;
    dxBarStatic2: TdxBarStatic;
    procedure ActProximoExecute(Sender: TObject);
    procedure ActAnteriorExecute(Sender: TObject);
    procedure ActAtualizarExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure ActExportarExcelExecute(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActExpandirTudoExecute(Sender: TObject);
    procedure ActRecolherAgrupamentoExecute(Sender: TObject);
    procedure pmTituloGridPesquisaPadraoPopup(Sender: TObject);
    procedure ActExportarPDFExecute(Sender: TObject);
    procedure ActAbrirMovimentoSelecionadoExecute(Sender: TObject);
    procedure ActAbrirTodosRegistrosExecute(Sender: TObject);
    procedure AoAlterarPrevisao(Sender: TField);
    procedure AoSalvarAlteracaoPrevisao(Sender: TDataset);
    procedure filtroExibirPrevisaoPropertiesEditValueChanged(Sender: TObject);
    procedure PintarColunaPrevisto(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    FDtInicial, FDtFinal: String;
    FListaPeriodo: TStringList;
    FUsuarioAlterou: Boolean;

    FParametroFiltroDataMovimentoInicio: TParametroFormulario;
    FParametroFiltroDataMovimentoFim: TParametroFormulario;
    FParametroExibirPrevisao: TParametroFormulario;

    procedure ValidarPreenchimentoDoPeriodo;
    function CriarCampoMesAno(const AQuery: TFDMemTable; AFieldName,
      ADisplayLabel: AnsiString): TFloatField;
    function GetDisplayLabelFromPeriodoColumn(const AMes: String): String;
    function GetFieldNameFromPeriodoColumn(const AMes: String): String;
    procedure CriarCamposPersonalizados(const AQueryNivel: array of TFDMemTable;
      const AView: array of TcxGridDBBandedTableView);
    procedure DeletarCamposPersonalizados(const AQueryNivel: array of TFDMemTable;
      const AView: array of TcxGridDBBandedTableView);
    procedure MarcarTodos(Sender: TObject);
    function SetListaPeriodo: TStringList;
    procedure PrepararFiltros;
    procedure SmartOpen(Sender: TObject);
    function String_AGG(AQuery: TFDMemTable; const AFieldName: String; const AStringAgg: String = ''): String;
    procedure SetValorPeriodo(const AQuery: TFDMemTable; const ANivel: Integer;
      const AStringAGGCentroResultado: String; AStringAGGContaAnalise: String = '');
    procedure ConfigurarColunas(AView: TcxGridDBBandedTableView);
    procedure ConfigurarTotalizadorMovimentacao;

    procedure CarregarPersonalizacaoGrid(AView: TcxGridDBBandedTableView);
    procedure SalvarPersonalizacaoGrid;
    function String_AGG_ID_Conta_Corrente(AQuery1, AQuery2, AQuery3,
      AQuery4: TFDMemTable; const AFieldName: String): String;
    function ViewAtivaNaTela: TcxGridDBBandedTableView;
    procedure ExibirColunasPrevisao(ADataset: TDataset; AView: TcxGridDBBandedTableView);
    procedure ExibirColunasRealizado(ADataset: TDataset; AView: TcxGridDBBandedTableView);

    const FSUMPERIODOLIST: String = 'SUM/9999';
    const FSUMPERIODO: String = 'SUM9999';
    const FSUFIXO_COLUNA_PREVISAO: String = 'PREV';
  public
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

implementation

{$R *.dfm}

uses uDateUtils, uFrmMessage, uTFunction, uFrmMessage_Process, uPlanoConta,
  uDevExpressUtils, uTUsuario, uDatasetUtils, uFrmApelidarColunasGrid,
  uDmAcesso, uSistema, uMovContaCorrenteMovimento, uConstParametroFormulario, uTControl_Function,
  uStringUtils;

procedure TRelExtratoPlanoConta.ActAbrirMovimentoSelecionadoExecute(
  Sender: TObject);
begin
  inherited;
  TMovContaCorrenteMovimento.AbrirTelaFiltrando(
    fdmMovimentacao.FieldByName('ID').AsString)
end;

procedure TRelExtratoPlanoConta.ActAbrirTodosRegistrosExecute(Sender: TObject);
begin
  inherited;
  TMovContaCorrenteMovimento.AbrirTelaFiltrando(
    TDatasetUtils.Concatenar(fdmMovimentacao.FieldByName('ID')));
end;

procedure TRelExtratoPlanoConta.ActAlterarColunasGridExecute(Sender: TObject);
var view: TcxGridDBBandedTableView;
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    view := ViewCentroResultado
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    view := ViewPrimeiroNivel
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    view := ViewSegundoNivel
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    view := ViewTerceiroNivel
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    view := ViewQuartoNivel
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    view := ViewContaCorrente
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    view := ViewMovimentacao;

  TFrmApelidarColunasGrid.SetColumns(view);
end;

procedure TRelExtratoPlanoConta.ActAnteriorExecute(Sender: TObject);
begin
  inherited;
  cxPageControlHibrido.SelectNextPage(false);
  SmartOpen(Sender);
end;

function TRelExtratoPlanoConta.String_AGG(AQuery: TFDMemTable; const AFieldName: String; const AStringAgg: String = ''): String;
begin
  result := AStringAgg;
  if AQuery.Active then
  try
    AQuery.DisableControls;
    AQuery.First;
    while not AQuery.Eof do
    begin
      if AQuery.FieldByName('BO_CHECKED').AsString = 'S' then
      begin
        if Pos(','+AQuery.FieldByName(AFieldName).AsString, result) = 0 then
        begin
          if result <> '' then
            result := result + ',';
          result := result + AQuery.FieldByName(AFieldName).AsString;
        end;
      end;
      AQuery.Next;
    end;
  finally
    AQuery.EnableControls;
  end;
end;

procedure TRelExtratoPlanoConta.ActAtualizarExecute(Sender: TObject);
var AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise,
    AnsiStringAGGContaCorrente, AnsiStringAGGWhere: String;
const whereIDPlanoConta: String = 'id_plano_conta_associado in (:id)';
const whereFieldIDPlanoConta: String = 'id_plano_conta_associado';

  function GetDataFirebirdFormat(ADate: String): String;
  const dataContraida: Integer = 14;
  var dia, mes, ano: String;
  begin
    dia := Copy(ADate, 1, 2);
    mes := Copy(ADate, 4, 2);
    ano := Copy(ADate, 7, 4);

    if Length(ADate) = 14 then
      ano := '20'+ano;

    result := QuotedStr(mes+'-'+dia+'-'+ano);
  end;

  procedure DeleteRegistroNulo(AQuery: TFDMemTable);
  var listaNulos: TStringList;
      i: Integer;
  begin
    AQuery.DisableControls;
    listaNulos := TStringList.Create;
    try
      AQuery.Filter := 'mes_ano is null';
      AQuery.Filtered := true;

      AQuery.First;
      while not AQuery.Eof do
      begin
        listaNulos.Add(AQuery.FieldByName('id').AsString);
        AQuery.Next;
      end;

      for i := 0 to Pred(listaNulos.Count) do
      begin
        AQuery.Filtered := false;
        AQuery.Filter := 'id = '+listaNulos[i];
        AQuery.Filtered := true;

        if AQuery.RecordCount > 1 then
        begin
          if AQuery.Locate('mes_ano', null,[]) then
            AQuery.Delete;
        end;
      end;
    finally
      AQuery.Filter := '';
      AQuery.Filtered := false;
      AQuery.EnableControls;
      FreeAndNil(listaNulos);
    end;
  end;
begin
  inherited;
  try
    FUsuarioAlterou := false;

    TFrmMessage_Process.SendMessage('Carregando dados');

    ValidarPreenchimentoDoPeriodo;

    FDtInicial := VartoStr(FiltroPeriodoInicio.EditValue);
    FDtFinal := VartoStr(FiltroPeriodoFim.EditValue);

    if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    begin
      TExtratoPlanoConta.SetCentroResultados(fdmCentroResultado);
      TcxGridUtils.AdicionarTodosCamposNaView(ViewCentroResultado);
      ConfigurarColunas(ViewCentroResultado);
      CarregarPersonalizacaoGrid(ViewCentroResultado);
      ViewCentroResultado.DataController.Groups.FullExpand;
      fdmCentroResultado.First;
    end
    else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    begin
      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');
      if not AnsiStringAGGCentroResultado.IsEmpty then
      begin
        TExtratoPlanoConta.SetPrimeiroNivel(fdmPrimeiroNivel, 1, AnsiStringAGGCentroResultado, FListaPeriodo.CommaText);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewPrimeiroNivel);
        ConfigurarColunas(ViewPrimeiroNivel);
        CarregarPersonalizacaoGrid(ViewPrimeiroNivel);
        ExibirColunasPrevisao(fdmPrimeiroNivel, ViewPrimeiroNivel);
        ExibirColunasRealizado(fdmPrimeiroNivel, ViewPrimeiroNivel);
        TDatasetUtils.ConfigurarMascaraFloat(fdmPrimeiroNivel);

        if not fdmPrimeiroNivel.IsEmpty then
        begin
          SetValorPeriodo(fdmPrimeiroNivel, 1, AnsiStringAGGCentroResultado);
          ViewPrimeiroNivel.DataController.Groups.FullExpand;
          fdmPrimeiroNivel.First;
        end;
      end;
    end
    else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    begin
      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');
      AnsiStringAGGContaAnalise := String_Agg(fdmPrimeiroNivel, 'ID');

      if not AnsiStringAGGContaAnalise.IsEmpty then
      begin
        TExtratoPlanoConta.SetNivelAninhado(fdmSegundoNivel, 2, AnsiStringAGGCentroResultado,
          AnsiStringAGGContaAnalise, FListaPeriodo.CommaText);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewSegundoNivel);
        ConfigurarColunas(ViewSegundoNivel);
        CarregarPersonalizacaoGrid(ViewSegundoNivel);
        ExibirColunasPrevisao(fdmSegundoNivel, ViewSegundoNivel);
        ExibirColunasRealizado(fdmSegundoNivel, ViewSegundoNivel);
        TDatasetUtils.ConfigurarMascaraFloat(fdmSegundoNivel);

        if not fdmSegundoNivel.IsEmpty then
        begin
          SetValorPeriodo(fdmSegundoNivel, 2, AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise);
          ViewSegundoNivel.DataController.Groups.FullExpand;
          fdmSegundoNivel.First;
        end;
      end
    end
    else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    begin
      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');
      AnsiStringAGGContaAnalise := String_Agg(fdmSegundoNivel, 'ID');

      if not AnsiStringAGGContaAnalise.IsEmpty then
      begin
        TExtratoPlanoConta.SetNivelAninhado(fdmTerceiroNivel, 3,
          AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise, FListaPeriodo.CommaText);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewTerceiroNivel);
        ConfigurarColunas(ViewTerceiroNivel);
        CarregarPersonalizacaoGrid(ViewTerceiroNivel);
        ExibirColunasPrevisao(fdmTerceiroNivel, ViewTerceiroNivel);
        ExibirColunasRealizado(fdmTerceiroNivel, ViewTerceiroNivel);
        TDatasetUtils.ConfigurarMascaraFloat(fdmTerceiroNivel);

        if not fdmTerceiroNivel.IsEmpty then
        begin
          SetValorPeriodo(fdmTerceiroNivel, 3, AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise);
          ViewTerceiroNivel.DataController.Groups.FullExpand;
          fdmTerceiroNivel.First;
        end;
      end
    end
    else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    begin
      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');
      AnsiStringAGGContaAnalise := String_Agg(fdmTerceiroNivel, 'ID');

      if not AnsiStringAGGContaAnalise.IsEmpty then
      begin
        TExtratoPlanoConta.SetNivelAninhado(fdmQuartoNivel, 4,
          AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise, FListaPeriodo.CommaText);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewQuartoNivel);
        ConfigurarColunas(ViewQuartoNivel);
        CarregarPersonalizacaoGrid(ViewQuartoNivel);
        ExibirColunasPrevisao(fdmQuartoNivel, ViewQuartoNivel);
        ExibirColunasRealizado(fdmQuartoNivel, ViewQuartoNivel);
        TDatasetUtils.ConfigurarMascaraFloat(fdmQuartoNivel);

        if not fdmQuartoNivel.IsEmpty then
        begin
          SetValorPeriodo(fdmQuartoNivel, 4, AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise);
          ViewQuartoNivel.DataController.Groups.FullExpand;
          fdmQuartoNivel.First;
        end;
      end
    end
    else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    begin
      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');
      AnsiStringAGGContaAnalise := String_AGG_ID_Conta_Corrente(
          fdmPrimeiroNivel,
          fdmSegundoNivel,
          fdmTerceiroNivel,
          fdmQuartoNivel, 'ID');

      if not AnsiStringAGGContaAnalise.IsEmpty then
      begin
        TExtratoPlanoConta.SetContasCorrentes(fdmContaCorrente, AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise,
          FListaPeriodo.CommaText, FDtInicial, FDtFinal);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewContaCorrente);
        ConfigurarColunas(ViewContaCorrente);
        CarregarPersonalizacaoGrid(ViewContaCorrente);

        if not fdmContaCorrente.IsEmpty then
        begin
          SetValorPeriodo(fdmContaCorrente, 5,AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise);
          ViewContaCorrente.DataController.Groups.FullExpand;
          fdmContaCorrente.First;
        end;
      end;
    end
    else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    begin
      AnsiStringAGGContaAnalise := String_AGG_ID_Conta_Corrente(
        fdmPrimeiroNivel,
        fdmSegundoNivel,
        fdmTerceiroNivel,
        fdmQuartoNivel, 'ID');

      AnsiStringAGGCentroResultado := String_Agg(fdmCentroResultado, 'ID_CENTRO_RESULTADO');

      //AnsiStringAGGContaCorrente := String_AGG(fdmContaCorrente, 'ID');

      {if not AnsiStringAGGContaCorrente.IsEmpty then}
      if not AnsiStringAGGCentroResultado.IsEmpty then
      begin
        TExtratoPlanoConta.SetMovimentacao(fdmMovimentacao,
          AnsiStringAGGCentroResultado, AnsiStringAGGContaAnalise, AnsiStringAGGContaCorrente,
          FListaPeriodo.CommaText, FDtInicial, FDtFinal);
        TcxGridUtils.AdicionarTodosCamposNaView(ViewMovimentacao);
        ConfigurarColunas(ViewMovimentacao);
        CarregarPersonalizacaoGrid(ViewMovimentacao);
        ConfigurarTotalizadorMovimentacao;

        if not fdmMovimentacao.IsEmpty then
        begin
          ViewMovimentacao.DataController.Groups.FullExpand;
          fdmMovimentacao.First;
        end;
      end;
    end;
  finally
    TFrmMessage_Process.CloseMessage;
    FUsuarioAlterou := true;
  end;
end;

procedure TRelExtratoPlanoConta.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    ViewCentroResultado.OptionsView.GroupByBox :=
    not ViewCentroResultado.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    ViewPrimeiroNivel.OptionsView.GroupByBox :=
    not ViewPrimeiroNivel.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    ViewSegundoNivel.OptionsView.GroupByBox :=
    not ViewSegundoNivel.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    ViewTerceiroNivel.OptionsView.GroupByBox :=
    not ViewTerceiroNivel.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    ViewQuartoNivel.OptionsView.GroupByBox :=
    not ViewQuartoNivel.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    ViewContaCorrente.OptionsView.GroupByBox :=
    not ViewContaCorrente.OptionsView.GroupByBox
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    ViewMovimentacao.OptionsView.GroupByBox :=
    not ViewMovimentacao.OptionsView.GroupByBox;
end;

procedure TRelExtratoPlanoConta.ActExpandirTudoExecute(Sender: TObject);
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    ViewCentroResultado.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    ViewPrimeiroNivel.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    ViewSegundoNivel.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    ViewTerceiroNivel.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    ViewQuartoNivel.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    ViewContaCorrente.DataController.Groups.FullExpand
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    ViewMovimentacao.DataController.Groups.FullExpand;
end;

procedure TRelExtratoPlanoConta.ActExportarExcelExecute(Sender: TObject);
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    TcxGridUtils.ExportarParaExcel(gridCentroResultado, 'Extrato de Plano de Conta - Centros de Resultado')
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    TcxGridUtils.ExportarParaExcel(gridPrimeiroNivel, 'Extrato de Plano de Conta - Primeiro N�vel')
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    TcxGridUtils.ExportarParaExcel(gridSegundoNivel, 'Extrato de Plano de Conta - Segundo N�vel')
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    TcxGridUtils.ExportarParaExcel(gridTerceiroNivel, 'Extrato de Plano de Conta - Terceiro N�vel')
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    TcxGridUtils.ExportarParaExcel(gridQuartoNivel, 'Extrato de Plano de Conta - Quarto N�vel')
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    TcxGridUtils.ExportarParaExcel(gridContaCorrente, 'Extrato de Plano de Conta - Contas Correntes')
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    TcxGridUtils.ExportarParaExcel(gridMovimentacao, 'Extrato de Plano de Conta - Movimenta��es');
end;

procedure TRelExtratoPlanoConta.ActExportarPDFExecute(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1Link1.ExportToPDF;
  exit;
    if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    TcxGridUtils.ExportarParaPDF(gridCentroResultado, 'Extrato de Plano de Conta - Centros de Resultado')
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    TcxGridUtils.ExportarParaPDF(gridPrimeiroNivel, 'Extrato de Plano de Conta - Primeiro N�vel')
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    TcxGridUtils.ExportarParaPDF(gridSegundoNivel, 'Extrato de Plano de Conta - Segundo N�vel')
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    TcxGridUtils.ExportarParaPDF(gridTerceiroNivel, 'Extrato de Plano de Conta - Terceiro N�vel')
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    TcxGridUtils.ExportarParaPDF(gridQuartoNivel, 'Extrato de Plano de Conta - Quarto N�vel')
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    TcxGridUtils.ExportarParaPDF(gridContaCorrente, 'Extrato de Plano de Conta - Contas Correntes')
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    TcxGridUtils.ExportarParaPDF(gridMovimentacao, 'Extrato de Plano de Conta - Movimenta��es');
end;

procedure TRelExtratoPlanoConta.ActProximoExecute(Sender: TObject);
begin
  inherited;
  cxPageControlHibrido.SelectNextPage(true);
  SmartOpen(Sender);
end;

procedure TRelExtratoPlanoConta.ActRecolherAgrupamentoExecute(Sender: TObject);
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    ViewCentroResultado.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    ViewPrimeiroNivel.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    ViewSegundoNivel.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    ViewTerceiroNivel.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    ViewQuartoNivel.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    ViewContaCorrente.DataController.Groups.FullCollapse
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    ViewMovimentacao.DataController.Groups.FullCollapse;
end;

procedure TRelExtratoPlanoConta.ActRestaurarColunasPadraoExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+'.'+ViewAtivaNaTela.Name);

  ViewAtivaNaTela.RestoreDefaults;
end;

procedure TRelExtratoPlanoConta.AoAlterarPrevisao(Sender: TField);
begin
  if not FUsuarioAlterou then
  begin
    Exit;
  end;

  Sender.Dataset.Post;

  TExtratoPlanoConta.SalvarPrevisao(Sender.Dataset.FieldByName('id_centro_resultado').AsInteger,
    Sender.Dataset.FieldByName('id').AsInteger, Sender.FieldName, Sender.AsFloat);
end;

procedure TRelExtratoPlanoConta.AoSalvarAlteracaoPrevisao(Sender: TDataset);
var
  fieldTotal: TField;
  fieldPrev: TField;
  valorTotal: Double;
  i: Integer;
begin
  fieldTotal := Sender.FindField(FSUMPERIODO+FSUFIXO_COLUNA_PREVISAO);
  if not Assigned(fieldTotal) then
  begin
    exit;
  end;

  valorTotal := 0;

  for i := 0 to Pred(FListaPeriodo.Count) do
  begin
    fieldPrev := Sender.FindField(GetFieldNameFromPeriodoColumn(
      FListaPeriodo[i])+FSUFIXO_COLUNA_PREVISAO);

    //Total do prefixo n�o pode ser somado
    if fieldPrev.FieldName = (FSUMPERIODO+FSUFIXO_COLUNA_PREVISAO) then
    begin
      Continue;
    end;

    if Assigned(fieldPrev) then
    begin
      valorTotal := valorTotal + fieldPrev.AsFloat;
    end;
  end;

  fieldTotal.AsFloat := valorTotal;
end;

function TRelExtratoPlanoConta.ViewAtivaNaTela: TcxGridDBBandedTableView;
begin
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    result := ViewCentroResultado
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    result := ViewPrimeiroNivel
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    result := ViewSegundoNivel
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    result := ViewTerceiroNivel
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    result := ViewQuartoNivel
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    result := ViewContaCorrente
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    result := ViewMovimentacao;
end;

procedure TRelExtratoPlanoConta.CarregarPersonalizacaoGrid(AView: TcxGridDBBandedTableView);
begin
  TUsuarioGridView.LoadGridView(AView, TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+'.'+ViewAtivaNaTela.Name);
end;

procedure TRelExtratoPlanoConta.ConfigurarColunas(
  AView: TcxGridDBBandedTableView);
var i: Integer;
    field: TField;
begin
  for i := Pred(AView.ColumnCount) downto 0 do
  begin
    if AView.Columns[i].DataBinding.FieldName = 'BO_CHECKED' then
    begin
      AView.Columns[i].DataBinding.Field.ReadOnly := false;
      AView.Columns[i].Options.Editing := true;
      AView.Columns[i].Options.Sorting := false;
      AView.Columns[i].OnHeaderClick := MarcarTodos;
    end
    else
    begin
      AView.Columns[i].Options.Editing := true;
      AView.Columns[i].Options.Sorting := true;
    end;
  end;

  for i := 0 to Pred(FListaPeriodo.Count) do
  begin
    field := AView.DataController.DataSource.DataSet.FindField(GetFieldNameFromPeriodoColumn(FListaPeriodo[i]));
    if (field <> nil) then
    begin
      field.displayLabel := GetDisplayLabelFromPeriodoColumn(FListaPeriodo[i]);
      TFMTBCDField(field).DisplayFormat := '###,###,###,###,##0.00';
      field.ReadOnly := false;

      with AView.GetColumnByFieldName(field.FieldName) do
      begin
        Width := 245;
        Options.Editing := false;
        Options.Sorting := false;
        Summary.GroupFooterKind := skSum;
        Summary.GroupFooterFormat := '###,###,###,###,##0.00';
      end;
    end;

    field := AView.DataController.DataSource.DataSet.FindField(GetFieldNameFromPeriodoColumn(
      FListaPeriodo[i])+FSUFIXO_COLUNA_PREVISAO);
    if (field <> nil) then
    begin
      field.displayLabel := 'Prev '+GetDisplayLabelFromPeriodoColumn(FListaPeriodo[i]);
      TFMTBCDField(field).DisplayFormat := '###,###,###,###,##0.00';
      field.ReadOnly := false;
      field.OnChange := AoAlterarPrevisao;

      with AView.GetColumnByFieldName(field.FieldName) do
      begin
        Width := 245;
        Options.Editing := true;
        Options.Sorting := false;
        Summary.GroupFooterKind := skSum;
        Summary.GroupFooterFormat := '###,###,###,###,##0.00';

        OnCustomDrawCell := PintarColunaPrevisto;

        //Total do prefixo n�o pode ser alterado
        if field.FieldName = (FSUMPERIODO+FSUFIXO_COLUNA_PREVISAO) then
        begin
          Options.Editing := false;
          field.OnChange := nil;
        end;
      end;
    end;
  end;
end;

procedure TRelExtratoPlanoConta.ConfigurarTotalizadorMovimentacao;
begin
  with ViewMovimentacao.GetColumnByFieldName('VL_MOVIMENTO') do
  begin
    Width := 245;
    Options.Editing := false;
    Options.Sorting := false;
    Summary.GroupFooterKind := skSum;
    Summary.GroupFooterFormat := '###,###,###,###,##0.00';
  end;
end;

function TRelExtratoPlanoConta.CriarCampoMesAno(const AQuery: TFDMemTable;
  AFieldName, ADisplayLabel: AnsiString): TFloatField;
var newField: TFloatField;
begin
  newField := TFloatField.Create(AQuery);
  newField.FieldName := AFieldName;
  newField.DisplayLabel := ADisplayLabel;
  newField.DisplayFormat := '###,###,###,##0.00';
  newField.Name := AQuery.Name + newField.FieldName;
  newField.Index := AQuery.FieldCount;
  newField.DataSet := AQuery;
  newField.FieldKind := fkInternalCalc;
  //AQuery.FieldDefs.UpDate;
  result := newField;
end;

function TRelExtratoPlanoConta.GetFieldNameFromPeriodoColumn(const AMes: String): String;
begin
  result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes)+1, 4);
end;

function TRelExtratoPlanoConta.GetDisplayLabelFromPeriodoColumn(const AMes: String): String;
begin
  if Copy(AMes, 1, 3) = Copy(FSUMPERIODOLIST, 1, 3) then
    result := 'Total'
  else
    result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes), 5);
end;

procedure TRelExtratoPlanoConta.CriarCamposPersonalizados(const AQueryNivel: array of TFDMemTable; const AView: array of TcxGridDBBandedTableView);
var i,q: integer;
    fieldName, displayLabel: String;
    field: TFloatField;
begin
  DeletarCamposPersonalizados(AQueryNivel, AView);
  for q := 0 to Pred(Length(AQueryNivel)) do
  begin
    AView[q].BeginUpdate;
    for i := 0 to Pred(FListaPeriodo.Count) do
    begin
      fieldName := GetFieldNameFromPeriodoColumn(FListaPeriodo[i]);
      displayLabel := GetDisplayLabelFromPeriodoColumn(FListaPeriodo[i]);
      field := CriarCampoMesAno( AQueryNivel[q], fieldName, displayLabel);

      with AView[q].CreateColumn do
      begin
        DataBinding.FieldName := fieldName;
        Width := 245;
        Options.Editing := false;
        Options.Sorting := false;
        Summary.GroupFooterKind := skSum;
        Summary.GroupFooterFormat := '###,###,###,###,##0.00';
      end;
    end;
    AView[q].EndUpdate;
  end;
end;

procedure TRelExtratoPlanoConta.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Data de Movimento Inicio
  FParametroFiltroDataMovimentoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_MOVIMENTO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_MOVIMENTO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataMovimentoInicio);

  //Data de Movimento Fim
  FParametroFiltroDataMovimentoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_MOVIMENTO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_MOVIMENTO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataMovimentoFim);

  //Exibir Atalho da Movimenta��o de Cheque
  FParametroExibirPrevisao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_PREVISAO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_PREVISAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirPrevisao);
end;

procedure TRelExtratoPlanoConta.DeletarCamposPersonalizados(const AQueryNivel: array of TFDMemTable; const AView: array of TcxGridDBBandedTableView);
var i,c,q: integer;
    fieldName: String;
    fieldNamePrevisao: String;
    field : TField;
begin
  if Assigned(FListaPeriodo) then
  try
    for i := 0 to Pred(FListaPeriodo.Count) do
    begin
      for q := 0 to Pred(Length(AQueryNivel)) do
      begin
        fieldName := GetFieldNameFromPeriodoColumn(FListaPeriodo[i]);
        fieldNamePrevisao := fieldName+FSUFIXO_COLUNA_PREVISAO;

        for c := Pred(AView[q].ColumnCount) downto 0 do
          if (AView[q].Columns[c].DataBinding.FieldName = fieldName) or
            (AView[q].Columns[c].DataBinding.FieldName = fieldNamePrevisao) then
          begin
            AView[q].BeginUpdate;
            AView[q].Columns[c].Destroy;
            AView[q].EndUpdate;
          end;
      end;
    end;
  except
  end;
end;

procedure TRelExtratoPlanoConta.ExibirColunasPrevisao(ADataset: TDataset; AView: TcxGridDBBandedTableView);
var
  exibir: boolean;
  i: Integer;
  fieldPrev: TField;
  coluna: TcxGridDBBandedColumn;
begin
  exibir := VartoStr(filtroExibirPrevisao.EditValue).Equals('P') or
    VartoStr(filtroExibirPrevisao.EditValue).Equals('A');

  for i := 0 to Pred(FListaPeriodo.Count) do
  begin
    fieldPrev := ADataset.FindField(GetFieldNameFromPeriodoColumn(
      FListaPeriodo[i])+FSUFIXO_COLUNA_PREVISAO);

    if Assigned(fieldPrev) then
    begin
      coluna := AView.GetColumnByFieldName(fieldPrev.FieldName);
      if Assigned(coluna) then
      begin
        coluna.Visible := exibir;
      end;
    end;
  end;
end;

procedure TRelExtratoPlanoConta.ExibirColunasRealizado(ADataset: TDataset;
  AView: TcxGridDBBandedTableView);
var
  exibir: boolean;
  i: Integer;
  fieldPrev: TField;
  coluna: TcxGridDBBandedColumn;
begin
  exibir := VartoStr(filtroExibirPrevisao.EditValue).Equals('R') or
    VartoStr(filtroExibirPrevisao.EditValue).Equals('A');

  for i := 0 to Pred(FListaPeriodo.Count) do
  begin
    fieldPrev := ADataset.FindField(GetFieldNameFromPeriodoColumn(
      FListaPeriodo[i]));

    if Assigned(fieldPrev) then
    begin
      coluna := AView.GetColumnByFieldName(fieldPrev.FieldName);
      if Assigned(coluna) then
      begin
        coluna.Visible := exibir;
      end;
    end;
  end;
end;

procedure TRelExtratoPlanoConta.filtroExibirPrevisaoPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  ActAtualizar.Execute;
end;

procedure TRelExtratoPlanoConta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DeletarCamposPersonalizados(
    [fdmPrimeiroNivel,
     fdmSegundoNivel,
     fdmTerceiroNivel,
     fdmQuartoNivel,
     fdmContaCorrente]
  ,
    [ViewPrimeiroNivel,
     ViewSegundoNivel,
     ViewTerceiroNivel,
     ViewQuartoNivel,
     ViewContaCorrente]
  );

  SalvarPersonalizacaoGrid;
  inherited;
end;

procedure TRelExtratoPlanoConta.FormCreate(Sender: TObject);
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Construindo plano de contas');
    TPlanoConta.SetIDPlanoContaAssociado;
    cxPageControlHibrido.Properties.HideTabs := true;
    PageControlRelatorios.Properties.HideTabs := true;
    cxQuartoNivelH.TabVisible := false;
    cxContaCorrenteH.TabVisible := false;
    cxPageControlHibrido.ActivePage := cxCentroResultadoH;
    PrepararFiltros;
  finally
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TRelExtratoPlanoConta.SalvarPersonalizacaoGrid;
begin
  if fdmCentroResultado.Active then
    TUsuarioGridView.SaveGridView(ViewCentroResultado,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewCentroResultado.Name);

  if fdmPrimeiroNivel.Active then
    TUsuarioGridView.SaveGridView(ViewPrimeiroNivel,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewPrimeiroNivel.Name);

  if fdmSegundoNivel.Active then
    TUsuarioGridView.SaveGridView(ViewSegundoNivel,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewSegundoNivel.Name);

  if fdmTerceiroNivel.Active then
    TUsuarioGridView.SaveGridView(ViewTerceiroNivel,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewTerceiroNivel.Name);

  if fdmQuartoNivel.Active then
    TUsuarioGridView.SaveGridView(ViewQuartoNivel,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewQuartoNivel.Name);

  if fdmContaCorrente.Active then
    TUsuarioGridView.SaveGridView(ViewContaCorrente,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewContaCorrente.Name);

  if fdmMovimentacao.Active then
    TUsuarioGridView.SaveGridView(ViewMovimentacao,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewMovimentacao.Name);
end;

function TRelExtratoPlanoConta.SetListaPeriodo: TStringList;
var listaPeriodo: TStringList;
    mesInicial, anoInicial, mesFinal, anoFinal: Integer;
begin
  if not Assigned(FListaPeriodo) then
    FListaPeriodo := TStringList.Create
  else
    FListaPeriodo.Clear;

  listaPeriodo := TStringList.Create;
  try
    mesInicial := MonthOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    anoInicial := YearOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    mesFinal := MonthOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));
    anoFinal := YearOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));

    repeat
      listaPeriodo.Add(TDateUtils.NumeroMesParaDescricaoMes(mesInicial)+'/'+IntToStr(anoInicial));

      if (mesInicial = 12) then
      begin
        mesInicial := 01;
        inc(anoInicial);
      end
      else
        inc(mesInicial);

    until (((mesInicial > mesFinal) or ((mesInicial = 1) and (anoInicial > anoFinal))) and (anoInicial >= anoFinal));

    listaPeriodo.Add(FSUMPERIODOLIST);

    FListaPeriodo.Text := listaPeriodo.Text;
  finally
    listaPeriodo.Free;
  end;
end;

procedure TRelExtratoPlanoConta.PrepararFiltros;
begin
  FiltroPeriodoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoInicio.AsString);

  FiltroPeriodoFim.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoFim.AsString);

  filtroExibirPrevisao.EditValue := 'R';

  if FParametroExibirPrevisao.ValorSim then
  begin
    filtroExibirPrevisao.Visible := ivAlways;
  end
  else
  begin
    filtroExibirPrevisao.Visible := ivNever;
  end;
end;

procedure TRelExtratoPlanoConta.SmartOpen(Sender: TObject);
var query: TFDMemTable;
begin
  if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
  begin
    ActAtualizar.Execute;

    query := fdmPrimeiroNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
  begin
    ActAtualizar.Execute;

    query := fdmSegundoNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
  begin
    ActAtualizar.Execute;

    query := fdmTerceiroNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
  begin
    ActAtualizar.Execute;

    query := fdmQuartoNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
  begin
    ActAtualizar.Execute;

    query := fdmContaCorrente;
  end
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
  begin
    ActAtualizar.Execute;

    query := fdmMovimentacao;
  end;

  if Assigned(query) and query.IsEmpty then
  begin
    if Sender = ActProximo then
      ActProximo.Execute
    else
      ActAnterior.Execute;
  end;

  ActAbrirMovimentoSelecionado.Visible := cxPageControlHibrido.ActivePage = cxMovimentacaoH;
  ActAbrirTodosRegistros.Visible := cxPageControlHibrido.ActivePage = cxMovimentacaoH;
end;

procedure TRelExtratoPlanoConta.SetValorPeriodo(const AQuery: TFDMemTable; const ANivel: Integer; const AStringAGGCentroResultado: String; AStringAGGContaAnalise: String = '');
var i: integer;
    diaInicial, diaFinal, mes, ano: Integer;
    field: TFloatField;
    fieldColumnPrevisao: TField;
    fieldColumn: String;
    stringAGGCentroResultado: String;
    whereIDPlanoConta, whereFieldIDPlanoConta: String;
    sumPeriodo: Double;
    fdmConsulta: TFDMemTable;
begin
  try
    fdmConsulta := TFDMemTable.Create(nil);
    fdmConsulta.FetchOptions.RecordCountMode := cmTotal;
    TExtratoPlanoConta.SetValores(fdmConsulta, ANivel, FDtInicial, FDtFinal, AStringAGGCentroResultado, AStringAGGContaAnalise);

    AQuery.DisableControls;
    try
      AQuery.First;
      while not AQuery.Eof do
      begin
        sumPeriodo := 0;
        AQuery.Edit;
        for i := 0 to Pred(FListaPeriodo.Count) do
        begin
          mes := TDateUtils.NumeroMes(Copy(FListaPeriodo[i],1,3));
          ano := StrtoInt(Copy(FListaPeriodo[i], Pos('/',FListaPeriodo[i])+1, 4));
          fieldColumn := GetFieldNameFromPeriodoColumn(FListaPeriodo[i]);

          case ANivel of
            0:
            begin

            end;
            1,2,3,4:
            begin
              if fdmConsulta.locate('id_centro_resultado;id_conta_analise;periodo',VarArrayOf([AQuery.FieldByName('ID_CENTRO_RESULTADO').AsInteger, AQuery.FieldByName('ID').AsInteger, InttoStr(mes)+'/'+InttoStr(ano)]),[]) then
                AQuery.FieldByName(fieldColumn).AsFloat :=
                  fdmConsulta.FieldByName('vl_total').AsFloat
              else
              begin
                AQuery.FieldByName(fieldColumn).AsFloat := 0;
              end;

              {Previs�o}
              if ano < 3000 then //Workaround
              begin
                fieldColumnPrevisao := AQuery.FindField(fieldColumn+FSUFIXO_COLUNA_PREVISAO);
                if Assigned(fieldColumnPrevisao)  then
                begin
                  fieldColumnPrevisao.AsFloat := TExtratoPlanoConta.BuscarValorPrevisao(ANivel,
                    FormatFloat('0000',ano)+FormatFloat('00',mes)+'01', TStringUtils.RetornarDataNoFormatoMYSQL(EndOfAMonth(ano, mes)),
                    AQuery.FieldByName('ID_CENTRO_RESULTADO').AsInteger, AQuery.FieldByName('ID').AsInteger);
                end;
              end;
            end;
            5:
            begin
              if fdmConsulta.locate('id_centro_resultado;id_conta_corrente;periodo',
                VarArrayOf([AQuery.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
                AQuery.FieldByName('ID').AsInteger, InttoStr(mes)+'/'+InttoStr(ano)]),[]) then
                AQuery.FieldByName(fieldColumn).AsFloat :=
                  fdmConsulta.FieldByName('vl_total').AsFloat
              else
                AQuery.FieldByName(fieldColumn).AsFloat := 0;
            end;
          end;

          sumPeriodo := sumPeriodo + AQuery.FieldByName(fieldColumn).AsFloat;
        end;

        AQuery.FieldByName(FSUMPERIODO).AsFloat := sumPeriodo;

        AQuery.Post;
        AQuery.Next;
      end;
    finally
      AQuery.EnableControls;
    end;
  finally
    FreeAndNil(fdmConsulta);
  end;
end;

procedure TRelExtratoPlanoConta.MarcarTodos(Sender: TObject);
var query: TFDMemTable;
    value: String;
begin
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
  begin
    query := fdmCentroResultado;
  end
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
  begin
    query := fdmPrimeiroNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
  begin
    query := fdmSegundoNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
  begin
    query := fdmTerceiroNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
  begin
    query := fdmQuartoNivel;
  end
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
  begin
    query := fdmContaCorrente;
  end;

  if (query <> nil) and (query.Active) then
  try
    query.DisableControls;
    query.First;
    value := TFunction.IIF(query.FieldByName('BO_CHECKED').AsString = 'S', 'N', 'S');
    while not query.Eof do
    begin
      query.Edit;
      query.FieldByName('BO_CHECKED').AsString := value;
      query.Post;
      query.Next;
    end;
  finally
    query.First;
    query.EnableControls;
  end;
end;

procedure TRelExtratoPlanoConta.PintarColunaPrevisto(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  ACanvas.Brush.Color := clbtnface;
  ACanvas.Font.Color := clwindowtext;
  ACanvas.FillRect(AViewInfo.Bounds);
end;

procedure TRelExtratoPlanoConta.pmTituloGridPesquisaPadraoPopup(
  Sender: TObject);
var
  view: TcxGridDBBandedTableView;
begin
  inherited;
  if cxPageControlHibrido.ActivePage = cxCentroResultadoH then
    view := ViewCentroResultado
  else if cxPageControlHibrido.ActivePage = cxPrimeiroNivelH then
    view := ViewPrimeiroNivel
  else if cxPageControlHibrido.ActivePage = cxSegundoNivelH then
    view := ViewSegundoNivel
  else if cxPageControlHibrido.ActivePage = cxTerceiroNivelH then
    view := ViewTerceiroNivel
  else if cxPageControlHibrido.ActivePage = cxQuartoNivelH then
    view := ViewQuartoNivel
  else if cxPageControlHibrido.ActivePage = cxContaCorrenteH then
    view := ViewContaCorrente
  else if cxPageControlHibrido.ActivePage = cxMovimentacaoH then
    view := ViewMovimentacao;

  ActExibirAgrupamento.Caption := TFunction.Iif(view.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');
end;

procedure TRelExtratoPlanoConta.ValidarPreenchimentoDoPeriodo;
begin
  if (VartoStr(FiltroPeriodoInicio.EditValue).IsEmpty) or
     (VartoStr(FiltroPeriodoFim.EditValue).IsEmpty) then
  begin
    TFrmMessage.Information('Informe o per�odo desejado!');
    Abort;
  end;

  if (FDtInicial <> VartoStr(FiltroPeriodoInicio.EditValue)) or
     (FDtFinal <> VartoStr(FiltroPeriodoFim.EditValue)) then
  begin
    fdmCentroResultado.Close;
    fdmPrimeiroNivel.Close;
    fdmSegundoNivel.Close;
    fdmTerceiroNivel.Close;
    fdmQuartoNivel.Close;
    fdmContaCorrente.Close;
    fdmMovimentacao.Close;
    cxPageControlHibrido.ActivePage := cxCentroResultadoH;
{
    DeletarCamposPersonalizados(
      [fdmPrimeiroNivel,
       fdmSegundoNivel,
       fdmTerceiroNivel,
       fdmQuartoNivel,
       fdmContaCorrente]
    ,
      [ViewPrimeiroNivel,
       ViewSegundoNivel,
       ViewTerceiroNivel,
       ViewQuartoNivel,
       ViewContaCorrente]
    ); }

 {   DeletarCamposPersonalizados(
      [fdmPrimeiroNivel], [ViewPrimeiroNivel]
    );   }

    SetListaPeriodo;

  {  CriarCamposPersonalizados(
      [fdmPrimeiroNivel], [ViewPrimeiroNivel]);

    CriarCamposPersonalizados(
      [fdmPrimeiroNivel,
       fdmSegundoNivel,
       fdmTerceiroNivel,
       fdmQuartoNivel,
       fdmContaCorrente]
    ,
      [ViewPrimeiroNivel,
       ViewSegundoNivel,
       ViewTerceiroNivel,
       ViewQuartoNivel,
       ViewContaCorrente]
    );                            }
  end;
end;

function TRelExtratoPlanoConta.String_AGG_ID_Conta_Corrente(AQuery1,AQuery2,AQuery3,AQuery4: TFDMemTable; const AFieldName: String): String;
var resultAGG: String;
  procedure SetResult;
  begin
    if not(resultAGG.IsEmpty) and not(result.IsEmpty) then
      result := result + ','+resultAGG
    else
      result := result + resultAGG;
  end;
begin
  resultAGG := String_AGG(AQuery1, AFieldName);
  SetResult;
  resultAGG := String_AGG(AQuery2, AFieldName, resultAGG);
  SetResult;
  resultAGG := String_AGG(AQuery3, AFieldName, resultAGG);
  SetResult;
  resultAGG := String_AGG(AQuery4, AFieldName, resultAGG);
  SetResult;
end;

Initialization
  RegisterClass(TRelExtratoPlanoConta);

Finalization
  UnRegisterClass(TRelExtratoPlanoConta);

end.
