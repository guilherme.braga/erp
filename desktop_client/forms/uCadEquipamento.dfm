inherited CadEquipamento: TCadEquipamento
  Caption = 'Cadastro de Equipamento'
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          inherited dxBevel1: TdxBevel
            ExplicitLeft = 4
            ExplicitTop = 0
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 37
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label10: TLabel
            Left = 8
            Top = 112
            Width = 34
            Height = 13
            Caption = 'Pessoa'
          end
          object Label12: TLabel
            Left = 8
            Top = 62
            Width = 34
            Height = 13
            Caption = 'Modelo'
          end
          object Label8: TLabel
            Left = 8
            Top = 87
            Width = 29
            Height = 13
            Caption = 'Marca'
          end
          object Label1: TLabel
            Left = 470
            Top = 37
            Width = 24
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'S'#233'rie'
          end
          object Label3: TLabel
            Left = 613
            Top = 37
            Width = 22
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'IMEI'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 58
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 58
            Top = 33
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 408
          end
          object eFkModelo: TgbDBButtonEditFK
            Left = 58
            Top = 58
            DataBinding.DataField = 'ID_MODELO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = 14606074
            TabOrder = 4
            gbTextEdit = descModelo
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_MODELO; JOIN_DESCRICAO_MODELO'
            gbTableName = 'MODELO'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbClasseDoCadastro = 'TCadModeloProduto'
            Width = 59
          end
          object descModelo: TgbDBTextEdit
            Left = 114
            Top = 58
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_MODELO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 812
          end
          object eFkMarca: TgbDBButtonEditFK
            Left = 58
            Top = 83
            DataBinding.DataField = 'ID_MARCA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = 14606074
            TabOrder = 6
            gbTextEdit = descMarca
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_MARCA;JOIN_DESCRICAO_MARCA'
            gbTableName = 'MARCA'
            gbCamposConsulta = 'ID;DESCRICAO'
            gbClasseDoCadastro = 'TCadMarcaProduto'
            Width = 59
          end
          object descMarca: TgbDBTextEdit
            Left = 114
            Top = 83
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_MARCA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbPassword = False
            Width = 812
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 871
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            Transparent = True
            Width = 55
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 58
            Top = 108
            DataBinding.DataField = 'ID_PESSOA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 8
            gbTextEdit = gbDBTextEdit4
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
            gbTableName = 'PESSOA'
            gbCamposConsulta = 'ID;NOME'
            Width = 59
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 114
            Top = 108
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_PESSOA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbReadyOnly = True
            gbPassword = False
            Width = 812
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 498
            Top = 33
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'SERIE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 111
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 641
            Top = 33
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'IMEI'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            gbPassword = False
            Width = 285
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Equipamento'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspEquipamento'
    RemoteServer = DmConnection.dspEquipamento
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataID_MARCA: TIntegerField
      DisplayLabel = 'C'#243'digo da Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
    end
    object cdsDataID_MODELO: TIntegerField
      DisplayLabel = 'C'#243'digo do Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object cdsDataSERIE: TStringField
      DisplayLabel = 'S'#233'rie'
      FieldName = 'SERIE'
      Origin = 'SERIE'
      Size = 255
    end
    object cdsDataIMEI: TStringField
      FieldName = 'IMEI'
      Size = 100
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataJOIN_DESCRICAO_MODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_MARCA: TStringField
      DisplayLabel = 'Marca'
      FieldName = 'JOIN_DESCRICAO_MARCA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42223.951042094910000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
