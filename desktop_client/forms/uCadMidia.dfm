inherited CadMidia: TCadMidia
  Caption = 'Cadastro de M'#237'dia'
  ClientHeight = 414
  ExplicitWidth = 952
  ExplicitHeight = 453
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarNavegacaoData'
        end
        item
          ToolbarName = 'dxBarManager'
        end
        item
          ToolbarName = 'dxBarAction'
        end
        item
          ToolbarName = 'dxBarSearch'
        end
        item
          ToolbarName = 'dxBarReport'
        end
        item
          ToolbarName = 'dxBarEnd'
        end
        item
          Caption = 'Servidor'
          ToolbarName = 'dxBarFTP'
        end>
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Height = 287
    ExplicitHeight = 287
    ClientRectBottom = 287
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 936
      ExplicitHeight = 263
      inherited cxGridPesquisaPadrao: TcxGrid
        Height = 263
        ExplicitHeight = 263
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitHeight = 263
      inherited DesignPanel: TJvDesignPanel
        Height = 263
        ExplicitHeight = 263
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitHeight = 263
          Height = 263
          inherited dxBevel1: TdxBevel
            ExplicitLeft = 10
          end
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 41
            Width = 27
            Height = 13
            Caption = 'Nome'
          end
          object Label3: TLabel
            Left = 8
            Top = 66
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label4: TLabel
            Left = 8
            Top = 91
            Width = 62
            Height = 13
            Caption = 'Tipo de M'#237'dia'
          end
          object Label5: TLabel
            Left = 738
            Top = 116
            Width = 67
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tamanho (KB)'
          end
          object Label6: TLabel
            Left = 765
            Top = 66
            Width = 40
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Dura'#231#227'o'
          end
          object Label7: TLabel
            Left = 140
            Top = 8
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object Label8: TLabel
            Left = 8
            Top = 116
            Width = 37
            Height = 13
            Caption = 'Arquivo'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 76
            Top = 4
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 217
            Top = 4
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbRequired = True
            gbDateTime = True
            Width = 130
          end
          object gbDBSpinEdit1: TgbDBSpinEdit
            Left = 809
            Top = 62
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DURACAO'
            DataBinding.DataSource = dsData
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Width = 121
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 76
            Top = 37
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'NOME'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 854
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 76
            Top = 62
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbPassword = False
            Width = 683
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 76
            Top = 87
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'TIPO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbPassword = False
            Width = 854
          end
          object gbDBTextEdit4: TgbDBTextEdit
            Left = 76
            Top = 112
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'ENDERECO_FTP'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 657
          end
          object gbDBTextEdit5: TgbDBTextEdit
            Left = 809
            Top = 112
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'TAMANHO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbPassword = False
            Width = 121
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    Left = 904
    Top = 34
  end
  inherited dxBarManagerPadrao: TdxBarManager
    Categories.Strings = (
      'Action'
      'Manager'
      'Report'
      'End'
      'Search'
      'Design'
      'ActionDesign'
      'FTP')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True)
    Left = 650
    Top = 34
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedLeft = 292
      FloatClientWidth = 51
      Row = 1
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 524
      FloatClientWidth = 70
      Row = 1
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 682
      FloatClientWidth = 68
      Row = 1
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 761
      FloatClientWidth = 51
      Row = 1
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 607
      FloatClientWidth = 66
      FloatClientHeight = 54
      Row = 1
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      FloatClientWidth = 124
      FloatClientHeight = 216
      Visible = True
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockControl = nil
      DockedDockControl = nil
      DockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object dxBarFTP: TdxBar [8]
      Caption = 'FTP'
      CaptionButtons = <>
      DockedLeft = 813
      DockedTop = 0
      FloatLeft = 970
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'progressbaritem'
        end>
      MultiLine = True
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton6: TdxBarButton [10]
      Action = actAnexarArquivo
      Category = 0
      Glyph.Data = {
        F6030000424DF6030000000000003600000028000000100000000F0000000100
        200000000000C003000000000000000000000000000000000000000000000000
        000500000077000000E2000000E1000000930000001700000000000000000000
        0000000000000000000000000000000000000000000000000000000000040000
        00CA000000DF0000006A00000075000000D6000000EA0000002B000000000000
        0000000000000000000000000000000000000000000000000000000000710000
        00DC00000006000000020000004E00000040000000A7000000E8000000280000
        0000000000000000000000000000000000000000000000000000000000E00000
        006700000002000000C2000000F6000000FE00000087000000A0000000E60000
        0026000000000000000000000000000000000000000000000000000000E20000
        006300000055000000EF0000000E0000003C000000EE0000008E000000A30000
        00E40000002300000000000000000000000000000000000000000000009D0000
        00BE00000047000000F9000000220000000000000033000000EF0000008A0000
        00A6000000E200000021000000000000000000000000000000000000001F0000
        00F30000007F000000A8000000DC0000001A0000000000000035000000F00000
        0086000000A8000000E00000001F000000000000000000000000000000000000
        003E000000F400000073000000B4000000D90000001800000000000000370000
        00F100000082000000AB000000DD0000001C0000000000000000000000000000
        000000000042000000F500000070000000B8000000D600000016000000000000
        0039000000F20000007E000000AE000000DB0000001A00000000000000000000
        00000000000000000045000000F60000006D000000BB000000D3000000140000
        00000000003B000000F10000001C000000B0000000D900000017000000000000
        0000000000000000000000000049000000F70000006B000000BE000000D00000
        001200000000000000030000000000000006000000D0000000A0000000000000
        00000000000000000000000000000000004D000000F700000068000000C10000
        00CD0000001000000000000000000000000000000064000000EC000000000000
        00000000000000000000000000000000000000000051000000F80000004D0000
        00C4000000CA00000010000000000000000000000091000000CF000000000000
        000000000000000000000000000000000000000000000000003B0000001D0000
        000D000000C7000000DE0000006F00000093000000FA00000042000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000E00000095000000EB000000D20000004300000000}
    end
    object dxBarButton7: TdxBarButton [11]
      Action = actRemoverArquivo
      Category = 0
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000012000000600000006000000060000000600000
        0060000000600000001200000000000000000000000000000000000000000000
        000000000000000000000000009D000000FF000000FF000000FF000000FF0000
        00FF000000FF0000009D00000000000000000000000000000000000000000000
        00000000000000000000000000AF000000EB000000C5000000D8000000D80000
        00C5000000EB000000AF00000000000000000000000000000000000000000000
        00000000000000000000000000BC000000B80000005800000088000000880000
        0058000000B8000000BC00000000000000000000000000000000000000000000
        00000000000000000000000000C8000000B80000005800000088000000880000
        0058000000B8000000C800000000000000000000000000000000000000000000
        00000000000000000000000000D4000000B80000005800000088000000880000
        0058000000B8000000D400000000000000000000000000000000000000000000
        00000000000000000000000000E1000000B80000005800000088000000880000
        0058000000B8000000E100000000000000000000000000000000000000000000
        00000000000000000000000000ED000000B80000005800000088000000880000
        0058000000B8000000ED00000000000000000000000000000000000000000000
        00000000000000000000000000F9000000F3000000D9000000E6000000E60000
        00D9000000F3000000F900000000000000000000000000000000000000000000
        00000000000000000006000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF00000006000000000000000000000000000000000000
        0000000000000000000400000048000000480000004800000048000000480000
        0048000000480000004800000004000000000000000000000000000000000000
        000000000000000000B4000000F0000000F0000000F0000000F0000000F00000
        00F0000000F0000000F0000000B4000000000000000000000000000000000000
        00000000000000000074000000B8000000B8000000E2000000FF000000FF0000
        00E2000000B8000000B800000074000000000000000000000000000000000000
        0000000000000000000000000000000000000000003600000060000000600000
        0036000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
    end
    object progressbaritem: TcxBarEditItem [12]
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxProgressBarProperties'
      Properties.PeakValue = 50.000000000000000000
      Properties.ShowTextStyle = cxtsText
      InternalEditValue = 0
    end
    object cxBarEditItem1: TcxBarEditItem [13]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxProgressBarProperties'
    end
    object dxBarProgressItem1: TdxBarProgressItem [14]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarProgressItem2: TdxBarProgressItem [15]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem [16]
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxProgressBarProperties'
    end
    object dxBBEnviar: TdxBarButton [28]
      Caption = 'Enviar'
      Category = 7
      Hint = 'Enviar'
      Visible = ivAlways
    end
    object dcBBRemover: TdxBarButton [29]
      Caption = 'Remover'
      Category = 7
      Hint = 'Remover'
      Visible = ivAlways
    end
  end
  inherited dsSearch: TDataSource
    Left = 848
    Top = 34
  end
  inherited dsData: TDataSource
    Left = 792
    Top = 34
  end
  inherited ActionListMain: TActionList
    Left = 876
    Top = 34
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de M'#237'dia'
    Left = 594
    Top = 34
  end
  inherited pmTituloGridPesquisaPadrao: TPopupMenu
    Left = 678
    Top = 34
  end
  inherited cxGridPopupMenuPadrao: TcxGridPopupMenu
    Left = 622
    Top = 34
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
      end>
    ProviderName = 'dspMidia'
    RemoteServer = DmConnection.dspIndoor
    AfterOpen = cdsDataAfterOpen
    BeforeClose = cdsDataBeforeClose
    OnNewRecord = cdsDataNewRecord
    Left = 762
    Top = 34
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo M'#237'dia'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 255
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object cdsDataTIPO: TStringField
      DisplayLabel = 'Tipo de M'#237'dia'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 45
    end
    object cdsDataTAMANHO: TFMTBCDField
      DisplayLabel = 'Tamanho'
      FieldName = 'TAMANHO'
      Origin = 'TAMANHO'
      Precision = 24
      Size = 2
    end
    object cdsDataDURACAO: TIntegerField
      DisplayLabel = 'Dura'#231#227'o'
      FieldName = 'DURACAO'
      Origin = 'DURACAO'
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
    end
    object cdsDataENDERECO_FTP: TStringField
      DisplayLabel = 'Endere'#231'o FTP'
      FieldName = 'ENDERECO_FTP'
      Origin = 'ENDERECO_FTP'
      Size = 255
    end
  end
  inherited fdmSearch: TFDMemTable
    Left = 820
    Top = 34
  end
  object odArquivo: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = []
    Left = 594
    Top = 77
  end
  object actListFTP: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 623
    Top = 76
    object actAnexarArquivo: TAction
      Caption = 'Anexar'
      ImageIndex = 22
      OnExecute = actAnexarArquivoExecute
    end
    object actRemoverArquivo: TAction
      Caption = 'Remover'
      ImageIndex = 18
      OnExecute = actRemoverArquivoExecute
    end
    object actCancelarEnvio: TAction
      Caption = 'Cancelar'
      ImageIndex = 17
      OnExecute = actCancelarEnvioExecute
    end
  end
end
