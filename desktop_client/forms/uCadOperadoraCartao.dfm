inherited CadOperadoraCartao: TCadOperadoraCartao
  Caption = 'Operadora de Cart'#227'o'
  ClientWidth = 947
  ExplicitWidth = 963
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 947
    ExplicitWidth = 947
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 947
    Properties.ActivePage = cxtsData
    ExplicitWidth = 947
    ClientRectRight = 947
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 947
      ExplicitHeight = 271
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 947
        ExplicitWidth = 947
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 947
      inherited DesignPanel: TJvDesignPanel
        Width = 947
        ExplicitWidth = 947
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 947
          Width = 947
          inherited dxBevel1: TdxBevel
            Width = 943
            ExplicitWidth = 943
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 37
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label3: TLabel
            Left = 8
            Top = 61
            Width = 34
            Height = 13
            Caption = 'Pessoa'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 58
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 58
            Top = 33
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 879
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 58
            Top = 57
            DataBinding.DataField = 'ID_PESSOA'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = gbDBTextEdit1
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
            gbTableName = 'PESSOA'
            gbCamposConsulta = 'ID;NOME'
            Width = 59
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 114
            Top = 57
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_PESSOA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 823
          end
          object gbDBCheckBox1: TgbDBCheckBox
            Left = 882
            Top = 6
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Transparent = True
            Width = 55
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspOperadoraCartao'
    RemoteServer = DmConnection.dspOperadoraCartao
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
  end
end
