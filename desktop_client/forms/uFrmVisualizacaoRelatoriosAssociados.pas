unit uFrmVisualizacaoRelatoriosAssociados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalComMenuPadrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  Datasnap.DBClient, uGBClientDataset, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, FireDAC.Comp.Client, dxBarExtItems,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans,
  dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TFrmVisualizacaoRelatoriosAssociados = class(TFrmModalComMenuPadrao)
    cdsConsultaRelatorioFR: TGBClientDataSet;
    dsConsultaRelatorioFR: TDataSource;
    cxGridRelatorios: TcxGrid;
    cxGridRelatoriosView: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cdsConsultaRelatorioFRID: TAutoIncField;
    cdsConsultaRelatorioFRDESCRICAO: TStringField;
    cdsConsultaRelatorioFRNOME: TStringField;
    cxGridRelatoriosViewID: TcxGridDBColumn;
    cxGridRelatoriosViewDESCRICAO: TcxGridDBColumn;
    cxGridRelatoriosViewNOME: TcxGridDBColumn;
    ActImprimir: TAction;
    ActVisualizarRelatorio: TAction;
    lbVisualizar: TdxBarLargeButton;
    lbImprimir: TdxBarLargeButton;
    barPropriedades: TdxBar;
    EdtImpressora: TdxBarCombo;
    EdtCopias: TdxBarSpinEdit;
    dxBarCombo2: TdxBarCombo;
    cdsConsultaRelatorioFRIMPRESSORA: TStringField;
    cdsConsultaRelatorioFRACAO: TStringField;
    cdsConsultaRelatorioFRNUMERO_COPIAS: TIntegerField;
    cdsConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField;
    cdsConsultaRelatorioFRTIPO_ARQUIVO: TStringField;
    cdsConsultaRelatorioFRARQUIVO: TBlobField;
    procedure ActVisualizarRelatorioExecute(Sender: TObject);
    procedure ActImprimirExecute(Sender: TObject);
    procedure cdsConsultaRelatorioFRAfterOpen(DataSet: TDataSet);
    procedure cdsConsultaRelatorioFRAfterScroll(DataSet: TDataSet);
  private
      FPK: Integer;
      FWhere: String;
      FDatasetGerarImpressao: TDataset;
      procedure MostrarRelatoriosAssociados(ANomeFormulario: String; AIdUsuario: Integer);
      procedure GerenciarControles;
      procedure SetImpressoraDaImpressao;
      procedure SetNumeroCopiasDaImpressao;
      procedure CarregarListaImpressoras;
  public
    class procedure VisualizarRelatoriosAssociados(AIdPK: Integer; ANomeFormulario: String;
       AIdUsuario: Integer; AWhere: String = ''; ADatasetGerarImpressao: TDataset = nil);
    class procedure ImprimirRelatoriosAssociados(AIdPK: Integer; ANomeFormulario: String;
       AIdUsuario: Integer; AWhere: String = ''; ADatasetGerarImpressao: TDataset = nil);
  end;

var
  FrmVisualizacaoRelatoriosAssociados: TFrmVisualizacaoRelatoriosAssociados;

implementation

{$R *.dfm}

uses uDmConnection, uRelatorioFR, uSistema, uTVariables, uFrmMessage, uImpressora, uRelatorioZebra;

{ TFrmVisualizacaoRelatoriosAssociados }

procedure TFrmVisualizacaoRelatoriosAssociados.ActImprimirExecute(
  Sender: TObject);
begin
  inherited;
  if (cdsConsultaRelatorioFRTIPO_ARQUIVO.AsString.Equals(TRelatorioFR.TIPO_ARQUIVO_FAST_REPORT)) then
  begin
    TRelatorioFR.ImprimirRelatorio(FPK, cdsConsultaRelatorioFRID.AsInteger, FWhere, EdtImpressora.Text,
      StrtoIntDef(EdtCopias.Text, 1));
  end
  else if (cdsConsultaRelatorioFRTIPO_ARQUIVO.AsString.Equals(TRelatorioFR.TIPO_ARQUIVO_ZEBRA)) then
  begin
    TRelatorioZebra.Imprimir(cdsConsultaRelatorioFR, FDatasetGerarImpressao);
  end;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.ActVisualizarRelatorioExecute(
  Sender: TObject);
begin
  inherited;
  TRelatorioFR.VisualizarRelatorio(FPK, cdsConsultaRelatorioFRID.AsInteger, FWhere, EdtImpressora.Text,
    StrtoIntDef(EdtCopias.Text, 1));
end;

procedure TFrmVisualizacaoRelatoriosAssociados.cdsConsultaRelatorioFRAfterOpen(
  DataSet: TDataSet);
begin
  inherited;
  GerenciarControles;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.cdsConsultaRelatorioFRAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetImpressoraDaImpressao;
  SetNumeroCopiasDaImpressao;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.GerenciarControles;
begin
  ActVisualizarRelatorio.Enabled := (not cdsConsultaRelatorioFR.IsEmpty) and
    (cdsConsultaRelatorioFRTIPO_ARQUIVO.AsString.Equals(TRelatorioFR.TIPO_ARQUIVO_FAST_REPORT));

  ActImprimir.Enabled := not cdsConsultaRelatorioFR.IsEmpty;
end;

class procedure TFrmVisualizacaoRelatoriosAssociados.ImprimirRelatoriosAssociados(AIdPK: Integer;
  ANomeFormulario: String; AIdUsuario: Integer; AWhere: String = ''; ADatasetGerarImpressao: TDataset = nil);
begin
  FrmVisualizacaoRelatoriosAssociados :=
    TFrmVisualizacaoRelatoriosAssociados.Create(Variables.frmMain);
  try
    with FrmVisualizacaoRelatoriosAssociados do
    begin
      Visible := false;
      FPK := AIdPk;
      FWhere := AWhere;
      FDatasetGerarImpressao := ADatasetGerarImpressao;
      CarregarListaImpressoras;
      MostrarRelatoriosAssociados(ANomeFormulario, AIdUsuario);

      if cdsConsultaRelatorioFR.IsEmpty then
      begin
        TFrmMessage.Information('N�o existem impress�es associadas a esta tela.');
      end
      else if cdsConsultaRelatorioFR.RecordCount = 1 then
      begin
        ActImprimir.Execute;
      end
      else
      if cdsConsultaRelatorioFR.RecordCount > 1 then
      begin
        FrmVisualizacaoRelatoriosAssociados.ShowModal;
      end;
    end;
  finally
    FreeAndNil(FrmVisualizacaoRelatoriosAssociados);
  end;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.MostrarRelatoriosAssociados(
  ANomeFormulario: String; AIdUsuario: Integer);
begin
  cdsConsultaRelatorioFR.Close;
  cdsConsultaRelatorioFR.Params.Clear;
  cdsConsultaRelatorioFR.FetchParams;
  cdsConsultaRelatorioFR.Params.ParamByName('NOME').AsString := ANomeFormulario;
  cdsConsultaRelatorioFR.Params.ParamByName('ID_PESSOA_USUARIO').AsInteger := AIdUsuario;
  cdsConsultaRelatorioFR.Open;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.SetImpressoraDaImpressao;
var
  indice: Integer;
begin
  if (not cdsConsultaRelatorioFR.Active) or (cdsConsultaRelatorioFR.IsEmpty) or
    (cdsConsultaRelatorioFRIMPRESSORA.AsString.IsEmpty) then
  begin
    EdtImpressora.Text := TImpressora.GetNomeImpressoraPadrao;
  end
  else if (TImpressora.ImpressoraValida(cdsConsultaRelatorioFRIMPRESSORA.AsString)) then
  begin
    EdtImpressora.Text := cdsConsultaRelatorioFRIMPRESSORA.AsString;
  end;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.SetNumeroCopiasDaImpressao;
begin
  if cdsConsultaRelatorioFRNUMERO_COPIAS.AsInteger > 0 then
  begin
    EdtCopias.Text := cdsConsultaRelatorioFRNUMERO_COPIAS.AsString;
  end
  else
  begin
    EdtCopias.Text := '1';
  end;
end;

procedure TFrmVisualizacaoRelatoriosAssociados.CarregarListaImpressoras;
begin
  EdtImpressora.Items.Clear;
  EdtImpressora.Items.CommaText:= TImpressora.GetListaImpressoras;
end;

class procedure TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
  AIdPK: Integer; ANomeFormulario: String; AIdUsuario: Integer; AWhere: String = '';
  ADatasetGerarImpressao: TDataset = nil);
begin
  FrmVisualizacaoRelatoriosAssociados :=
    TFrmVisualizacaoRelatoriosAssociados.Create(Variables.frmMain);
  try
    with FrmVisualizacaoRelatoriosAssociados do
    begin
      Visible := false;
      FPK := AIdPk;
      FWhere := AWhere;
      FDatasetGerarImpressao := ADatasetGerarImpressao;
      CarregarListaImpressoras;
      MostrarRelatoriosAssociados(ANomeFormulario, AIdUsuario);
      if cdsConsultaRelatorioFR.IsEmpty then
      begin
        TFrmMessage.Information('N�o existem impress�es associadas a esta tela.');
      end
      else if cdsConsultaRelatorioFR.RecordCount = 1 then
      begin
        if cdsConsultaRelatorioFRACAO.AsString.Equals(TRelatorioFR.ACAO_VISUALIZAR) then
        begin
          ActVisualizarRelatorio.Execute;
        end
        else
        begin
          ActImprimir.Execute;
        end;
      end
      else
      if cdsConsultaRelatorioFR.RecordCount > 1 then
      begin
        FrmVisualizacaoRelatoriosAssociados.ShowModal;
      end;
    end;
  finally
    FreeAndNil(FrmVisualizacaoRelatoriosAssociados);
  end;
end;

end.
