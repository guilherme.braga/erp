unit uCadGrupoCentroResultado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit, cxMaskEdit, cxButtonEdit,
  uGBDBTextEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls;

type
  TCadGrupoCentroResultado = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    Label3: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataOBSERVACAO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadGrupoCentroResultado);
finalization
  UnRegisterClass(TCadGrupoCentroResultado);

end.

