unit uMovContaCorrenteMovimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  cxCheckBox, cxDBEdit, uGBDBCheckBox, cxRadioGroup, uGBDBRadioGroup,
  uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit,
  cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxButtonEdit, uGBDBButtonEditFK,
  cxBlobEdit, uGBDBBlobEdit, cxLabel, dxBarExtItems, cxBarEditItem,
  cxDBLookupComboBox, uFrameTipoQuitacaoCheque, uFramePadrao,
  uFrameTipoQuitacaoCartao, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, uUsuarioDesignControl, cxSplitter, JvExControls, JvButton, JvTransparentButton,
  uGBPanel, uFrameFiltroVerticalPadrao, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TMovContaCorrenteMovimento = class(TFrmCadastro_Padrao)
    cdsDataDESCRICAO: TStringField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDH_CONCILIADO: TDateTimeField;
    cdsDataDT_EMISSAO: TDateField;
    cdsDataDT_MOVIMENTO: TDateField;
    cdsDataTP_MOVIMENTO: TStringField;
    cdsDataBO_CONCILIADO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataVL_MOVIMENTO: TFMTBCDField;
    cdsDataVL_SALDO_CONCILIADO: TFMTBCDField;
    cdsDataVL_SALDO_GERAL: TFMTBCDField;
    cdsDataDOCUMENTO: TStringField;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataID_MOVIMENTO_ORIGEM: TIntegerField;
    cdsDataID_MOVIMENTO_DESTINO: TIntegerField;
    cdsDataBO_MOVIMENTO_ORIGEM: TStringField;
    cdsDataBO_MOVIMENTO_DESTINO: TStringField;
    cdsDataBO_TRANSFERENCIA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    gbDBDateEdit1: TgbDBDateEdit;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    dxBarFiltro: TdxBar;
    dxBarContainerItem1: TdxBarContainerItem;
    ActTransferencia: TAction;
    filtroDocumento: TdxBarEdit;
    filtroRotuloDataEmissao: TdxBarStatic;
    filtroLabelDataMovimentacao: TdxBarStatic;
    cxBarEditItem1: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    filtroContaCorrente: TcxBarEditItem;
    filtroDataEmissaoInicio: TcxBarEditItem;
    filtroDataEmissaoFim: TcxBarEditItem;
    filtroDataMovimentoInicio: TcxBarEditItem;
    filtroDataMovimentoFim: TcxBarEditItem;
    filtroConciliado: TcxBarEditItem;
    filtroTipoMovimentacao: TcxBarEditItem;
    fdmContaCorrente: TFDMemTable;
    ActProximo: TAction;
    ActAnterior: TAction;
    lbTransferencia: TdxBarLargeButton;
    PnDados: TcxGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EdtDescricao: TgbDBTextEdit;
    gbDBDateEdit2: TgbDBDateEdit;
    gbDBDateEdit3: TgbDBDateEdit;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBBlobEdit1: TgbDBBlobEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    EdtDocumento: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit6: TgbDBTextEdit;
    PnTransferencia: TcxGroupBox;
    Label13: TLabel;
    EdtContaCorrenteDestinoTransferencia: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    lbProximo: TdxBarLargeButton;
    lbAnterior: TdxBarLargeButton;
    cdsDataID: TAutoIncField;
    dsContaCorrente: TDataSource;
    fdmContaCorrenteid: TIntegerField;
    fdmContaCorrentedescricao: TStringField;
    cdsDataIC_DESC_CONTA_CORRENTE_DESTINO: TStringField;
    N1: TMenuItem;
    ACMovimentacao: TActionList;
    ActConciliar: TAction;
    ActConciliar1: TMenuItem;
    cdsDataID_CONTA_CORRENTE_DESTINO: TIntegerField;
    EdtContaAnalise: TcxDBButtonEdit;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsDataTP_DOCUMENTO_ORIGEM: TStringField;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    Label15: TLabel;
    gbDBTextEdit9: TgbDBTextEdit;
    Label16: TLabel;
    lbDhConciliacao: TLabel;
    EdtDhConciliacao: TgbDBDateEdit;
    Label17: TLabel;
    gbDBDateEdit4: TgbDBDateEdit;
    cdsDataDT_COMPETENCIA: TDateField;
    gbDBTextEdit10: TgbDBTextEdit;
    Label18: TLabel;
    descProcesso: TgbDBTextEdit;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataCHEQUE_SACADO: TStringField;
    cdsDataCHEQUE_DOC_FEDERAL: TStringField;
    cdsDataCHEQUE_BANCO: TStringField;
    cdsDataCHEQUE_DT_EMISSAO: TDateField;
    cdsDataCHEQUE_AGENCIA: TStringField;
    cdsDataCHEQUE_CONTA_CORRENTE: TStringField;
    cdsDataCHEQUE_NUMERO: TIntegerField;
    cdsDataID_OPERADORA_CARTAO: TIntegerField;
    cdsDataJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    FrameTipoQuitacaoCheque: TFrameTipoQuitacaoCheque;
    FrameTipoQuitacaoCartao: TFrameTipoQuitacaoCartao;
    ActExportarExcel: TAction;
    ExportarparaExcel1: TMenuItem;
    cdsDataBO_CHEQUE: TStringField;
    lbFiltroContaCorrente: TcxBarEditItem;
    dxBarStatic1: TdxBarStatic;
    cdsDataBO_CHEQUE_PROPRIO: TStringField;
    cdsDataBO_CHEQUE_TERCEIRO: TStringField;
    cdsDataCHEQUE_DT_VENCIMENTO: TDateField;
    cdsDataBO_PROCESSO_AMORTIZADO: TStringField;
    ActMovimentacaoCheque: TAction;
    lbMovimentacaoCheque: TdxBarLargeButton;
    bsSaldoAnterior: TdxBarStatic;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ActProximoExecute(Sender: TObject);
    procedure ActAnteriorExecute(Sender: TObject);
    procedure ActTransferenciaExecute(Sender: TObject);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsDataAfterClose(DataSet: TDataSet);
    procedure cdsDataBO_CONCILIADOChange(Sender: TField);
    procedure filtroContaCorrentePropertiesCloseUp(Sender: TObject);
    procedure ActConciliarExecute(Sender: TObject);
    procedure pmGridConsultaPadraoPopup(Sender: TObject);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure EdtContaAnalisePropertiesEditValueChanged(Sender: TObject);
    procedure ActExportarExcelExecute(Sender: TObject);
    procedure ActMovimentacaoChequeExecute(Sender: TObject);
  private
    FRegistroEstaConciliado: Boolean;
    FModoTransferencia: Boolean;
    FEstaConciliado: boolean;

    {Parametros}
    FParametroFiltroDataEmissaoInicio: TParametroFormulario;
    FParametroFiltroDataEmissaoFim: TParametroFormulario;
    FParametroFiltroDataMovimentoInicio: TParametroFormulario;
    FParametroFiltroDataMovimentoFim: TParametroFormulario;
    FParametroExibirAtalhoMovimentacaoCheque: TParametroFormulario;
    FParametroExibirTransferenciaMovimentacao: TParametroFormulario;

    procedure SetModoTransferencia(const Value: boolean);
    procedure SetEstaConciliado(const Value: boolean);
    const ArrFiltroTipoMovimentacao: array[0..2] of String = ('Todos','Cr�dito','D�bito');
    const ArrFiltroConciliacao: array[0..2] of String = ('Todos','Conciliado','N�o Conciliado');

    procedure SetContaCorrente;
    procedure AtualizarSaldoMovimentacao;
    procedure ExibirContaCorrenteDestino;
    procedure ExibirDadosCheque;
    procedure ExibirDadosCartao;
    procedure RealizarMovimentacaoCheque;
    procedure ExibirTransferenciaDeMovimentacao;
    procedure ExibirMovimentacaoCheque;
    procedure ExibirSaldoAnterior;

  public
    class procedure AbrirTelaFiltrando(const AIdsContaCorrenteMovimento: String);
  protected
    procedure PosicionarGradeDePesquisaNoPrimeiroRegistro; override;
    procedure SetWhereBundle;override;
    procedure GerenciarControles; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeCancelar;  override;
    procedure DepoisDeCancelar; override;
    procedure AntesDeRemover; override;
    procedure AntesDePesquisar; override;
    procedure DepoisDePesquisar; override;
    procedure AoCriarFormulario; override;
    procedure BloquearEdicao; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;

    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;

    procedure PrepararFiltros; override;

    property modoTransferencia: boolean read FModoTransferencia write SetModoTransferencia;
    property estaConciliado: boolean read FEstaConciliado write SetEstaConciliado;

  end;

implementation

{$R *.dfm}

uses uDmConnection, uContaCorrente, uFrmMessage_Process, uTFunction,
  uTVariables, System.Generics.Collections, uDatasetUtils, uDmAcesso,
  uPesqContaAnalise, uChaveProcesso, uContaCorrenteProxy, uChaveProcessoProxy,
  uTMessage, uSistema, uDevExpressUtils, uFiltroFormulario, uControlsUtils,
  uTControl_Function, uConstParametroFormulario, uProdutoProxy, uMovMovimentacaoCheque, uFluxoCaixaResumido;

{ TMovContaCorrenteMovimento }

procedure TMovContaCorrenteMovimento.ActAnteriorExecute(Sender: TObject);
begin
  inherited;
  fdmContaCorrente.Prior;
  FiltroContaCorrente.EditValue := fdmContaCorrente.FieldByName('ID').AsInteger;
  ExecutarConsulta;
end;

procedure TMovContaCorrenteMovimento.ActConciliarExecute(Sender: TObject);
var idRegistroAnterior: Integer;
begin
  inherited;
  try
    idRegistroAnterior := fdmSearch.FieldByName('id').AsInteger;
    if FRegistroEstaConciliado then
      TContaCorrente.DesconciliarRegistro(fdmSearch.FieldByName('id').AsInteger)
    else
      TContaCorrente.ConciliarRegistro(fdmSearch.FieldByName('id').AsInteger);

    ExecutarConsulta;
  finally
    fdmSearch.Locate('id',idRegistroAnterior, []);
  end;
end;

procedure TMovContaCorrenteMovimento.ActExportarExcelExecute(Sender: TObject);
begin
  inherited;
  TcxGridUtils.ExportarParaExcel(cxGridPesquisaPadrao,
    'Movimenta��o de Conta Corrente')
end;

procedure TMovContaCorrenteMovimento.ActMovimentacaoChequeExecute(Sender: TObject);
begin
  inherited;
  RealizarMovimentacaoCheque;
end;

procedure TMovContaCorrenteMovimento.ActProximoExecute(Sender: TObject);
begin
  inherited;
  fdmContaCorrente.Next;
  FiltroContaCorrente.EditValue := fdmContaCorrente.FieldByName('ID').AsInteger;
  ExecutarConsulta;
end;

procedure TMovContaCorrenteMovimento.ActTransferenciaExecute(Sender: TObject);
begin
  inherited;
  modoTransferencia := true;
  ExecutarIncluir;
  cdsDataDOCUMENTO.AsString := 'Transfer�ncia';
end;

procedure TMovContaCorrenteMovimento.AntesDeCancelar;
begin
  inherited;
end;

procedure TMovContaCorrenteMovimento.AntesDeConfirmar;
begin
  inherited;
  if modoTransferencia then
    TValidacaoCampo.CampoPreenchido(cdsDataID_CONTA_CORRENTE_DESTINO);

  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
  //TValidacaoCampo.CampoMaiorQueZero(cdsDataVL_MOVIMENTO);

  if modoTransferencia then
    cdsDataBO_TRANSFERENCIA.AsString := 'S';
end;

procedure TMovContaCorrenteMovimento.AntesDeRemover;
begin
  inherited;
  if (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> TContaCorrenteMovimento.ORIGEM_MANUAL) and
     (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> '')
  then
  begin
    TMessage.MessageInformation('Este Documento deve ser Removido apenas a partir de sua Origem.');
    Abort;
  end;
  //Tratar Transferencia, pensar em criar before e after no servidor
end;

procedure TMovContaCorrenteMovimento.AoCriarFormulario;
begin
  inherited;
  EdtDescricao.Properties.CharCase := ecNormal;
  EdtDocumento.Properties.CharCase := ecNormal;
  modoTransferencia := false;
  estaConciliado := false;
end;

procedure TMovContaCorrenteMovimento.AtualizarSaldoMovimentacao;
begin
  TFrmMessage_Process.SendMessage('Atualizando saldo da movimenta��o');
  try
    TContaCorrente.AtualizarSaldoMovimentacao(fdmContaCorrente.FieldByName('ID').AsInteger);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovContaCorrenteMovimento.BloquearEdicao;
begin
  inherited;
  Self.BloquearEdicaoDados :=
    (cdsDataID_DOCUMENTO_ORIGEM.AsInteger > 0);
end;

procedure TMovContaCorrenteMovimento.cdsDataAfterClose(DataSet: TDataSet);
begin
  modoTransferencia := false;
end;

procedure TMovContaCorrenteMovimento.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  estaConciliado := TContaCorrente.EstaConciliado(cdsData);
end;

procedure TMovContaCorrenteMovimento.cdsDataBO_CONCILIADOChange(Sender: TField);
begin
  inherited;
  estaConciliado := TContaCorrente.EstaConciliado(cdsData);
end;

procedure TMovContaCorrenteMovimento.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataID_CHAVE_PROCESSO.AsInteger            := TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_CONTA_CORRENTE, 0);
  cdsDataID_CONTA_CORRENTE.AsString             := fdmContaCorrenteid.AsString;
  cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString := fdmContaCorrentedescricao.AsString;
  cdsDataDH_CADASTRO.AsDateTime                 := Now;
  cdsDataDT_EMISSAO.AsDateTime                  := Date;
  cdsDataDT_MOVIMENTO.AsDateTime                := Date;
  cdsDataDT_COMPETENCIA.AsDateTime              := Date;
  cdsDataID_DOCUMENTO_ORIGEM.AsInteger          := 0;
  cdsDataTP_DOCUMENTO_ORIGEM.AsString           := TContaCorrenteMovimento.ORIGEM_MANUAL;
  cdsDataID_FILIAL.AsInteger                    := TSistema.Sistema.Filial.Fid;
end;

procedure TMovContaCorrenteMovimento.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Data de Emissao Inicio
  FParametroFiltroDataEmissaoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_EMISSAO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_EMISSAO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataEmissaoInicio);

  //Data de Emissao Fim
  FParametroFiltroDataEmissaoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_EMISSAO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_EMISSAO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataEmissaoFim);

  //Data de Movimento Inicio
  FParametroFiltroDataMovimentoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_MOVIMENTO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_MOVIMENTO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataMovimentoInicio);

  //Data de Movimento Fim
  FParametroFiltroDataMovimentoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_MOVIMENTO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_MOVIMENTO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataMovimentoFim);

  //Exibir Atalho da Movimenta��o de Cheque
  FParametroExibirAtalhoMovimentacaoCheque := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_ATALHO_MOVIMENTACAO_CHEQUE,
    TConstParametroFormulario.DESCRICAO_EXIBIR_ATALHO_MOVIMENTACAO_CHEQUE,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirAtalhoMovimentacaoCheque);

  //Exibir Trasnfer�ncia de Movimenta��o
  FParametroExibirTransferenciaMovimentacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_TRANSFERENCIA_MOVIMENTACAO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_TRANSFERENCIA_MOVIMENTACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirTransferenciaMovimentacao);
end;

procedure TMovContaCorrenteMovimento.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Emiss�o
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Emiss�o';
  campoFiltro.campo := 'DT_EMISSAO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  campoFiltro.valorPadraoInicial :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataEmissaoInicio.AsString);
  campoFiltro.valorPadraoFinal :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataEmissaoFim.AsString);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Movimento
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Movimento';
  campoFiltro.campo := 'DT_MOVIMENTO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  campoFiltro.valorPadraoInicial :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoInicio.AsString);
  campoFiltro.valorPadraoFinal :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoFim.AsString);
  AFiltroVertical.FCampos.Add(campoFiltro); }

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA'; ///VERIFICAR SE EXISTE COMPETENCIA
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo de Documento';
  campoFiltro.campo := 'TIPO'; //VERIFICAR
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TContaCorrenteMovimento.TIPO_MOVIMENTO_CREDITO+';'+
  TContaCorrenteMovimento.TIPO_MOVIMENTO_DEBITO; ///VERIFICAR
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Centro de Resultado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Centro de Resultado';
  campoFiltro.campo := 'ID_CENTRO_RESULTADO';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CENTRO_RESULTADO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta de Analise
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta de Analise';
  campoFiltro.campo := 'ID_CONTA_ANALISE';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_ANALISE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Carteira
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Carteira';
  campoFiltro.campo := 'ID_CARTEIRA';
  campoFiltro.tabela := 'CONTA_CORRENTE_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CARTEIRA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TMovContaCorrenteMovimento.DepoisDeCancelar;
begin
  inherited;
end;

procedure TMovContaCorrenteMovimento.DepoisDeConfirmar;
var
  Movimento : TContaCorrente;
begin
  inherited;

//  TFrmMessage_Process.SendMessage('Gerando Documentos ... ');
//  Movimento := TContaCorrente.Create;
//  try
//    Movimento.GerarDocumento(cdsData);
//  finally
//    TFrmMessage_Process.CloseMessage;
//  end;

  ExecutarConsulta;
end;

procedure TMovContaCorrenteMovimento.DepoisDePesquisar;
begin
  inherited;
  fdmSearch.RecNo := fdmSearch.RecordCount;
  ExibirSaldoAnterior;
end;

procedure TMovContaCorrenteMovimento.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovContaCorrenteMovimento.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovContaCorrenteMovimento.EdtContaAnalisePropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovContaCorrenteMovimento.ExibirContaCorrenteDestino;
begin
  PnTransferencia.Visible :=
    FModoTransferencia or cdsDataBO_TRANSFERENCIA.AsString.Equals('S');
end;

procedure TMovContaCorrenteMovimento.ExibirDadosCartao;
begin
  FrameTipoQuitacaoCartao.Visible :=
    cdsData.Active and (not cdsDataID_OPERADORA_CARTAO.AsString.IsEmpty);
end;

procedure TMovContaCorrenteMovimento.ExibirDadosCheque;
begin
  FrameTipoQuitacaoCheque.Visible :=
    cdsData.Active and (cdsDataCHEQUE_NUMERO.AsInteger > 0);
end;

procedure TMovContaCorrenteMovimento.ExibirMovimentacaoCheque;
var
  bEstaVisualizando: Boolean;
  bEstaPesquisando: Boolean;
begin
  bEstaVisualizando := acaoCrud = TAcaoCrud(Visualizar);
  bEstaPesquisando := acaoCrud = TAcaoCrud(Pesquisar);
  ActTransferencia.Visible := FParametroExibirTransferenciaMovimentacao.ValorSim and
    (bEstaVisualizando or bEstaPesquisando);
end;

procedure TMovContaCorrenteMovimento.ExibirSaldoAnterior;
var
  saldoAnterior: Double;
begin
  //Data de Movimento
  if not(VartoStr(filtroDataMovimentoInicio.EditValue).IsEmpty) and
     not(VartoStr(filtroDataMovimentoFim.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataMovimentoInicio.EditValue) <= VarToDateTime(filtroDataMovimentoFim.EditValue)) and
     not(fdmSearch.IsEmpty) then
  begin
    saldoAnterior := TContaCorrente.GetSaldoAnteriorContaCorrente(
      StrtoIntDef(VartoStr(FiltroContaCorrente.EditValue), 0), VartoStr(FiltroDataMovimentoInicio.EditValue));

    bsSaldoAnterior.Caption := 'Saldo Anterior R$ ' + FormatFloat('###,###,###,###,##0.00', saldoAnterior);
  end
  else
  begin
    bsSaldoAnterior.Caption := '';
  end;
end;

procedure TMovContaCorrenteMovimento.ExibirTransferenciaDeMovimentacao;
begin
  ActMovimentacaoCheque.Visible := FParametroExibirAtalhoMovimentacaoCheque.ValorSim;
end;

procedure TMovContaCorrenteMovimento.filtroContaCorrentePropertiesCloseUp(
  Sender: TObject);
begin
  inherited;
  ExecutarConsulta;
end;

procedure TMovContaCorrenteMovimento.AntesDePesquisar;
begin
  inherited;
  AtualizarSaldoMovimentacao;
end;

procedure TMovContaCorrenteMovimento.PosicionarGradeDePesquisaNoPrimeiroRegistro;
begin
  //iherited;
end;

procedure TMovContaCorrenteMovimento.PrepararFiltros;
begin
  inherited;
  FiltroContaCorrente.EditValue := null;
  FiltroDocumento.Text := '';
  FiltroTipoMovimentacao.EditValue := ArrFiltroTipoMovimentacao[0];
  FiltroConciliado.EditValue := ArrFiltroConciliacao[0];

  SetContaCorrente;

  if not fdmContaCorrente.IsEmpty then
    FiltroContaCorrente.EditValue := fdmContaCorrente.FieldByName('ID').AsInteger;

  FiltroDataMovimentoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoInicio.AsString);
  FiltroDataMovimentoFim.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataMovimentoFim.AsString);
end;

procedure TMovContaCorrenteMovimento.RealizarMovimentacaoCheque;
begin
  TMovMovimentacaoCheque.IncluirMovimentacao(fdmContaCorrente.FieldByName('ID').AsInteger,
    TTipoMovimentacaoCheque(transferencia));
end;

procedure TMovContaCorrenteMovimento.SetContaCorrente;
begin
  TContaCorrente.SetContaCorrente(fdmContaCorrente,TContaCorrenteAtivo(ativo));
end;

procedure TMovContaCorrenteMovimento.SetEstaConciliado(const Value: boolean);
begin
  FEstaConciliado := Value;
  EdtDhConciliacao.Visible := FEstaConciliado;
  lbDhConciliacao.Visible := FEstaConciliado;

  if cdsData.Active and (cdsData.State in DsEditModes) then
  begin
    if Value then
      cdsDataDH_CONCILIADO.AsDateTime := Now
    else
      cdsDataDH_CONCILIADO.Clear;
  end;
end;

procedure TMovContaCorrenteMovimento.SetModoTransferencia(const Value: boolean);
begin
  FModoTransferencia := Value;
  ExibirContaCorrenteDestino;
end;

procedure TMovContaCorrenteMovimento.SetWhereBundle;
var AItemList: TcxFilterCriteriaItemList;
    BetweenCondicional: array of string;
    textoBetween: String;
begin
  inherited;
  //Conta Corrente
  if not VartoStr(FiltroContaCorrente.EditValue).IsEmpty then
  begin
    if fdmContaCorrente.Locate('ID', FiltroContaCorrente.EditValue, []) then
      WhereSearch.AddIgual(FIELD_ID_CONTA_CORRENTE, fdmContaCorrente.FieldByName('id').AsInteger);
  end;

  //Numero do Documento
  if not VartoStr(FiltroDocumento.Text).IsEmpty then
    WhereSearch.AddIgual(FIELD_DOCUMENTO, UpperCase(FiltroDocumento.Text));

  //Data de Emissao
  if not(VartoStr(FiltroDataEmissaoInicio.EditValue).IsEmpty) and
     not(VartoStr(FiltroDataEmissaoFim.EditValue).IsEmpty) and
     (VarToDateTime(FiltroDataEmissaoInicio.EditValue) <= VarToDateTime(FiltroDataEmissaoFim.EditValue)) then
    WhereSearch.AddEntre(FIELD_DATA_EMISSAO, StrtoDate(FiltroDataEmissaoInicio.EditValue), StrtoDate(FiltroDataEmissaoFim.EditValue));

  //Data de Movimento
  if not(VartoStr(filtroDataMovimentoInicio.EditValue).IsEmpty) and
     not(VartoStr(filtroDataMovimentoFim.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataMovimentoInicio.EditValue) <= VarToDateTime(filtroDataMovimentoFim.EditValue)) then
   WhereSearch.AddEntre(FIELD_DATA_MOVIMENTO, StrtoDate(FiltroDataMovimentoInicio.EditValue), StrtoDate(FiltroDataMovimentoFim.EditValue));

  //Tipo de Movimentacao
  if not VartoStr(FiltroTipoMovimentacao.EditValue).Equals(ArrFiltroTipoMovimentacao[0]) then
    WhereSearch.AddIgual(FIELD_TIPO_MOVIMENTO,VartoStr(TFunction.Iif(FiltroTipoMovimentacao.EditValue = ArrFiltroTipoMovimentacao[1],
      MOVIMENTACAO_CREDITO, MOVIMENTACAO_DEBITO)));

  //Conciliacao
  if not VartoStr(FiltroConciliado.EditValue).Equals(ArrFiltroConciliacao[0]) then
    WhereSearch.AddIgual(FIELD_CONCILIADO,VartoStr(TFunction.Iif(FiltroConciliado.EditValue = ArrFiltroConciliacao[1], CONCILIADO, NAO_CONCILIADO)));
end;

procedure TMovContaCorrenteMovimento.GerenciarControles;
var
  bEdicao: Boolean;
  bInsercao : Boolean;
  bEstaPesquisando: Boolean;
begin
  inherited;
  bEdicao := cdsData.State in dsEditModes;
  bInsercao := cdsData.State in [dsInsert];
  bEstaPesquisando := acaoCrud in [Pesquisar, none];

  ActTransferencia.Visible := not bEdicao;
  dxBarFiltro.Visible := bEstaPesquisando;
  ActProximo.Visible := bEstaPesquisando;
  ActAnterior.Visible := bEstaPesquisando;

  ExibirContaCorrenteDestino;
  ExibirDadosCartao;
  ExibirDadosCheque;
  ExibirTransferenciaDeMovimentacao;
  ExibirMovimentacaoCheque;
end;

procedure TMovContaCorrenteMovimento.pmGridConsultaPadraoPopup(Sender: TObject);
const ICONE_CONCILIAR: Integer = 18;
const ICONE_DESCONCILIAR: Integer = 21;
const LABEL_CONCILIAR: String = 'Conciliar';
const LABEL_DESCONCILIAR: String = 'Desconciliar';
begin
  inherited;
  if not fdmSearch.IsEmpty then
  begin
    FregistroEstaConciliado := TContaCorrente.RegistroEstaConciliado(fdmSearch.FieldByName('id').AsInteger);
    ActConciliar.Caption :=  TFunction.IIF(FregistroEstaConciliado, LABEL_DESCONCILIAR, LABEL_CONCILIAR);
    ActConciliar.ImageIndex :=  TFunction.IIF(FregistroEstaConciliado, ICONE_DESCONCILIAR, ICONE_CONCILIAR);
  end;
end;

class procedure TMovContaCorrenteMovimento.AbrirTelaFiltrando(
  const AIdsContaCorrenteMovimento: String);
var
  instanciaFormulario: TMovContaCorrenteMovimento;
  filtroContaCorrenteMovimento: TFiltroPadrao;
begin
  if AIdsContaCorrenteMovimento.IsEmpty then
    Exit;

  try
    TFrmMessage_Process.SendMessage('Carregando informa��es da movimenta��o');
    TControlsUtils.CongelarFormulario(Application.MainForm);
    instanciaFormulario := TMovContaCorrenteMovimento(TControlFunction.GetInstance(Self.ClassName));
    if Assigned(instanciaFormulario) then
    begin
      if instanciaFormulario.cdsData.EstadoDeAlteracao then
      begin
        TMessage.MessageInformation('O formul�rio movimenta��o de conta corrente est� em'+
         ' edi��o, salve ou cancele as altera��es pendentes antes de continuar.');

        exit;
      end;
    end
    else
    begin
      instanciaFormulario := TMovContaCorrenteMovimento(
        TControlFunction.CreateMDIForm(Self.ClassName));
    end;
    instanciaFormulario.cxpcMain.ActivePage := instanciaFormulario.cxtsSearch;
    instanciaFormulario.WhereSearch.AddFaixa('CONTA_CORRENTE_MOVIMENTO.ID',
      AIdsContaCorrenteMovimento);
    instanciaFormulario.Consultar;
    instanciaFormulario.ActVisualizarRegistro.Execute;
    instanciaFormulario.Show;
    Application.ProcessMessages;
  finally
    TControlsUtils.DescongelarFormulario;
    TFrmMessage_Process.CloseMessage;
  end;
end;

initialization
  RegisterClass(TMovContaCorrenteMovimento);

Finalization
  UnRegisterClass(TMovContaCorrenteMovimento);

end.
