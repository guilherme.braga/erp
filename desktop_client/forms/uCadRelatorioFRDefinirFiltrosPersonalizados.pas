unit uCadRelatorioFRDefinirFiltrosPersonalizados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Datasnap.DBClient, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, Generics.Collections,
  uFrameFiltroVerticalPadrao, cxCheckBox;

type
  TCadRelatorioFrDefinirFiltrosPersonalizados = class(TFrmModalPadrao)
    cdsColunas: TClientDataSet;
    dsColunas: TDataSource;
    cxgdMain: TcxGrid;
    ViewDefinicaoCampos: TcxGridDBTableView;
    cxgdMainLevel1: TcxGridLevel;
    cdsColunasTABELA: TStringField;
    cdsColunasCAMPO: TStringField;
    cdsColunasVISIVEL: TBooleanField;
    cdsColunasVALOR_PADRAO: TStringField;
    cdsColunasOBRIGATORIO: TBooleanField;
    cdsColunasDESCRICAO: TStringField;
    ViewDefinicaoCamposVISIVEL: TcxGridDBColumn;
    ViewDefinicaoCamposOBRIGATORIO: TcxGridDBColumn;
    ViewDefinicaoCamposDESCRICAO: TcxGridDBColumn;
  private
    FListaFiltrosPersonalizados: TList<TCampoFiltro>;
    procedure ConstruirVisao(
      const AListaFiltrosPersonalizados: TList<TCampoFiltro>);
    function BuscarVisao: TList<TCampoFiltro>;
  public
    class function DefinirPersonalizacao(
      const AListaFiltrosPersonalizados: TList<TCampoFiltro>): String;
  end;

var
  CadRelatorioFrDefinirFiltrosPersonalizados: TCadRelatorioFrDefinirFiltrosPersonalizados;

implementation

{$R *.dfm}

uses uDevExpressUtils, dbxjson, dbxjsonreflect, Contnrs, System.JSON, uJSONUtils, uDatasetUtils;

{ TFrmModalPadrao1 }

function TCadRelatorioFrDefinirFiltrosPersonalizados.BuscarVisao: TList<TCampoFiltro>;
var
  i: Integer;
begin
  for i := 0 to FListaFiltrosPersonalizados.Count -1 do
  begin
    if cdsColunas.Locate('TABELA;CAMPO', VarArrayOf([FListaFiltrosPersonalizados[i].Tabela,
      FListaFiltrosPersonalizados[i].Campo]), []) then
    begin
      //FListaFiltrosPersonalizados[i].Tabela      := cdsColunasTABELA.AsString;
      //FListaFiltrosPersonalizados[i].Campo       := cdsColunasCAMPO.AsString;
      FListaFiltrosPersonalizados[i].Visivel     := cdsColunasVISIVEL.AsBoolean;
      //FListaFiltrosPersonalizados[i].ValorPadrao := cdsColunasVALOR_PADRAO.AsString;
      FListaFiltrosPersonalizados[i].Obrigatorio := cdsColunasOBRIGATORIO.AsBoolean;
      //FListaFiltrosPersonalizados[i].Rotulo      := cdsColunasDESCRICAO.AsString;
    end;
  end;
end;

procedure TCadRelatorioFrDefinirFiltrosPersonalizados.ConstruirVisao(
  const AListaFiltrosPersonalizados: TList<TCampoFiltro>);
var
  i: Integer;
begin
  for i := 0 to AListaFiltrosPersonalizados.Count -1 do
  begin
    cdsColunas.Append;
    cdsColunasTABELA.AsString := AListaFiltrosPersonalizados[i].Tabela;
    cdsColunasCAMPO.AsString := AListaFiltrosPersonalizados[i].Campo;
    cdsColunasVISIVEL.AsBoolean := AListaFiltrosPersonalizados[i].Visivel;
    cdsColunasVALOR_PADRAO.AsString := AListaFiltrosPersonalizados[i].ValorPadrao;
    cdsColunasOBRIGATORIO.AsBoolean := AListaFiltrosPersonalizados[i].Obrigatorio;
    cdsColunasDESCRICAO.AsString := AListaFiltrosPersonalizados[i].Rotulo;
    cdsColunas.Post;
  end;
end;

class function TCadRelatorioFrDefinirFiltrosPersonalizados.DefinirPersonalizacao(
  const AListaFiltrosPersonalizados: TList<TCampoFiltro>): String;
var
  m: TJSONMarshal;
  json: TJSONObject;
  list: TList;
  i: Integer;
begin
  Application.CreateForm(TCadRelatorioFrDefinirFiltrosPersonalizados,
    CadRelatorioFrDefinirFiltrosPersonalizados);
  try
    CadRelatorioFrDefinirFiltrosPersonalizados.FListaFiltrosPersonalizados :=
      AListaFiltrosPersonalizados;
    CadRelatorioFrDefinirFiltrosPersonalizados.ConstruirVisao(
      AListaFiltrosPersonalizados);
    CadRelatorioFrDefinirFiltrosPersonalizados.ShowModal;

    if CadRelatorioFrDefinirFiltrosPersonalizados.Result = MCONFIRMED then
    begin
      result := TDatasetUtils.ClientDatasetToClientDatasetDataStream(
        CadRelatorioFrDefinirFiltrosPersonalizados.cdsColunas);
      {result := TJSONUtil.ListToJSONArray<TCampoFiltro>(
        CadRelatorioFrDefinirFiltrosPersonalizados.FListaFiltrosPersonalizados).ToString;}

      {list := TList.Create;
      try
        for i := 0 to AListaFiltrosPersonalizados.Count - 1 do
        begin
          list.Add(AListaFiltrosPersonalizados[i]);
        end;

        m := TJSONMarshal.Create;
        try
          // Register a specific converter for field FList
          m.RegisterConverter(TList, 'FList', function(Data: TObject; Field: String): TListOfObjects
            var
              l: TList;
              j: integer;
            begin
              l := Data as TList;
              SetLength(Result, l.Count);
              for j := 0 to l.Count - 1 do
                Result[j] := TObject(l[j]); // HardCast from pointer
            end);

          json := m.Marshal(AListaFiltrosPersonalizados) as TJSONObject;
          try
            result := json.tostring;
          finally
            json.free;
          end;
        finally
          m.free;
        end;
      finally
        list.free;
      end;}
    end
    else
    begin
      result := '';
    end;
  finally
    FreeAndNil(CadRelatorioFrDefinirFiltrosPersonalizados);
  end;
end;

end.
