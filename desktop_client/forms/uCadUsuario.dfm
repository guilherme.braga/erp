inherited CadUsuario: TCadUsuario
  Caption = 'Cadastro de Usu'#225'rio'
  ClientWidth = 870
  ExplicitWidth = 886
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 870
    ExplicitWidth = 870
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 870
    ExplicitWidth = 870
    ClientRectRight = 870
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 870
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 870
        ExplicitWidth = 870
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 870
      inherited DesignPanel: TJvDesignPanel
        Width = 870
        ExplicitWidth = 870
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 870
          Width = 870
          inherited dxBevel1: TdxBevel
            Width = 866
            ExplicitWidth = 866
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 38
            Width = 36
            Height = 13
            Caption = 'Usu'#225'rio'
          end
          object Label1: TLabel
            Left = 500
            Top = 38
            Width = 30
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Senha'
            ExplicitLeft = 566
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 52
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edNome: TgbDBTextEdit
            Left = 52
            Top = 34
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'USUARIO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 442
          end
          object gbDBCheckBox7: TgbDBCheckBox
            Left = 804
            Top = 34
            Anchors = [akTop, akRight]
            Caption = 'Ativo?'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Width = 63
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 534
            Top = 34
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'SENHA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.EchoMode = eemPassword
            Properties.PasswordChar = '*'
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = True
            Width = 266
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Usu'#225'rio'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspPessoaUsuario'
    RemoteServer = DmConnection.dspPessoa
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataUSUARIO: TStringField
      FieldName = 'USUARIO'
      Required = True
      Size = 50
    end
    object cdsDataSENHA: TStringField
      FieldName = 'SENHA'
      Required = True
      Size = 255
    end
    object cdsDataDT_EXPIRACAO: TDateField
      FieldName = 'DT_EXPIRACAO'
    end
    object cdsDataUSUARIO_EXPIRADO: TIntegerField
      FieldName = 'USUARIO_EXPIRADO'
    end
    object cdsDataUSUARIO_DIAS_EXPIRAR_SENHA: TIntegerField
      FieldName = 'USUARIO_DIAS_EXPIRAR_SENHA'
    end
    object cdsDataEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object cdsDataPRIVILEGIADO: TIntegerField
      FieldName = 'PRIVILEGIADO'
    end
    object cdsDataSENHA_KEY: TStringField
      FieldName = 'SENHA_KEY'
      Size = 255
    end
    object cdsDataBO_ATIVO: TStringField
      FieldName = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDataID_USUARIO_PERFIL: TIntegerField
      FieldName = 'ID_USUARIO_PERFIL'
    end
    object cdsDataUSUARIO_TIPO: TStringField
      FieldName = 'USUARIO_TIPO'
      FixedChar = True
      Size = 1
    end
    object cdsDataESTACAO_ID: TIntegerField
      FieldName = 'ESTACAO_ID'
    end
  end
end
