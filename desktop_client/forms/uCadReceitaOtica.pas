unit uCadReceitaOtica;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxDBEdit, uGBDBTextEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, uGBDBTextEditPK,
  cxBlobEdit, uGBDBBlobEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit,
  Vcl.StdCtrls, cxLabel, dxGDIPlusClasses, cxImage, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon;

type
  TCadReceitaOtica = class(TFrmCadastro_Padrao)
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    Label7: TLabel;
    Label8: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    dxBevel2: TdxBevel;
    dxBevel3: TdxBevel;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataDH_PREVISAO_ENTREGA: TDateTimeField;
    cdsDataID_MEDICO: TIntegerField;
    cdsDataESPECIFICACAO: TBlobField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_VENDA: TIntegerField;
    cdsDataLONGE_DIREITO_ESFERICO: TStringField;
    cdsDataLONGE_DIREITO_CILINDRICO: TStringField;
    cdsDataLONGE_DIREITO_EIXO: TStringField;
    cdsDataLONGE_ESQUERDO_ESFERICO: TStringField;
    cdsDataLONGE_ESQUERDO_CILINDRICO: TStringField;
    cdsDataLONGE_ESQUERDO_EIXO: TStringField;
    cdsDataPERTO_DIREITO_ESFERICO: TStringField;
    cdsDataPERTO_DIREITO_CILINDRICO: TStringField;
    cdsDataPERTO_DIREITO_EIXO: TStringField;
    cdsDataPERTO_ESQUERDO_ESFERICO: TStringField;
    cdsDataPERTO_ESQUERDO_CILINDRICO: TStringField;
    cdsDataPERTO_ESQUERDO_EIXO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataJOIN_NOME_USUARIO: TStringField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_NOME_MEDICO: TStringField;
    cxImage1: TcxImage;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    EdtEsfericoDireito: TgbDBTextEdit;
    cxLabel8: TcxLabel;
    cxLabel4: TcxLabel;
    EdtCilindricoDireito: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit5: TgbDBTextEdit;
    cxLabel5: TcxLabel;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBTextEdit7: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    cxLabel6: TcxLabel;
    EdtObservacao: TgbDBBlobEdit;
    Label6: TLabel;
    gbDBDateEdit2: TgbDBDateEdit;
    Label4: TLabel;
    cxLabel17: TcxLabel;
    gbDBTextEdit18: TgbDBTextEdit;
    gbDBTextEdit19: TgbDBTextEdit;
    cxImage2: TcxImage;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    gbDBTextEdit9: TgbDBTextEdit;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    gbDBTextEdit10: TgbDBTextEdit;
    gbDBTextEdit11: TgbDBTextEdit;
    gbDBTextEdit12: TgbDBTextEdit;
    cxLabel13: TcxLabel;
    gbDBTextEdit13: TgbDBTextEdit;
    gbDBTextEdit14: TgbDBTextEdit;
    gbDBTextEdit15: TgbDBTextEdit;
    cxLabel14: TcxLabel;
    cxLabel15: TcxLabel;
    gbDBTextEdit16: TgbDBTextEdit;
    gbDBTextEdit17: TgbDBTextEdit;
    cxLabel16: TcxLabel;
    cxLabel18: TcxLabel;
    gbDBTextEdit20: TgbDBTextEdit;
    gbDBTextEdit21: TgbDBTextEdit;
    cdsDataPERTO_DNP_DIREITO: TBCDField;
    cdsDataPERTO_DNP_ESQUERDO: TBCDField;
    cdsDataPERTO_DNP_GERAL: TBCDField;
    cdsDataLONGE_DNP_DIREITO: TBCDField;
    cdsDataLONGE_DNP_ESQUERDO: TBCDField;
    cdsDataLONGE_DNP_GERAL: TBCDField;
    Label3: TLabel;
    Label5: TLabel;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBTextEditPK1: TgbDBTextEditPK;
    lbVenda: TLabel;
    EdtNumeroVenda: TgbDBTextEdit;
    cdsDataALTURA: TStringField;
    cdsDataADICAO: TStringField;
    procedure CalcularDNP(AField: TField);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure gbDBTextEdit18KeyPress(Sender: TObject; var Key: Char);
    procedure gbDBTextEdit19KeyPress(Sender: TObject; var Key: Char);
    procedure gbDBTextEdit16KeyPress(Sender: TObject; var Key: Char);
    procedure gbDBTextEdit17KeyPress(Sender: TObject; var Key: Char);
  private
    FCalculandoDNP: Boolean;
    procedure ExibirInformacoesVenda;
    function KeyNumber(CharKey: Char): Char;
  public
    procedure AoCriarFormulario; override;
    procedure GerenciarControles; override;
  end;

var
  CadReceitaOtica: TCadReceitaOtica;

implementation

{$R *.dfm}

uses uDmConnection, uSistema;

{ TCadReceitaOtica }

function TCadReceitaOtica.KeyNumber(CharKey: Char): Char;
begin
  if CharKey in ['0'..'9', '-'] then
    result := CharKey
  else
    result := #0;
end;

procedure TCadReceitaOtica.AoCriarFormulario;
begin
  inherited;
  FCalculandoDNP := false;
end;

procedure TCadReceitaOtica.CalcularDNP(AField: TField);
begin
  if FCalculandoDNP then
    Exit;

  try
    FCalculandoDNP := true;

    if AField = cdsDataPERTO_DNP_DIREITO then
    begin
      cdsDataPERTO_DNP_GERAL.AsFloat := cdsDataPERTO_DNP_DIREITO.AsFloat +
        cdsDataPERTO_DNP_ESQUERDO.AsFloat;
    end
    else if AField = cdsDataPERTO_DNP_ESQUERDO then
    begin
      cdsDataPERTO_DNP_GERAL.AsFloat := cdsDataPERTO_DNP_DIREITO.AsFloat +
        cdsDataPERTO_DNP_ESQUERDO.AsFloat;
    end
    else if AField = cdsDataPERTO_DNP_GERAL then
    begin
      cdsDataPERTO_DNP_DIREITO.AsFloat := cdsDataPERTO_DNP_GERAL.AsFloat / 2;
      cdsDataPERTO_DNP_ESQUERDO.AsFloat := cdsDataPERTO_DNP_GERAL.AsFloat / 2;
    end
    else if AField = cdsDataLONGE_DNP_DIREITO then
    begin
      cdsDataLONGE_DNP_GERAL.AsFloat := cdsDataLONGE_DNP_DIREITO.AsFloat +
        cdsDataLONGE_DNP_ESQUERDO.AsFloat;
    end
    else if AField = cdsDataLONGE_DNP_ESQUERDO then
    begin
      cdsDataLONGE_DNP_GERAL.AsFloat := cdsDataLONGE_DNP_DIREITO.AsFloat +
        cdsDataLONGE_DNP_ESQUERDO.AsFloat;
    end
    else if AField = cdsDataLONGE_DNP_GERAL then
    begin
      cdsDataLONGE_DNP_DIREITO.AsFloat := cdsDataLONGE_DNP_GERAL.AsFloat / 2;
      cdsDataLONGE_DNP_ESQUERDO.AsFloat := cdsDataLONGE_DNP_GERAL.AsFloat / 2;
    end;
  finally
    FCalculandoDNP := false;
  end;
end;

procedure TCadReceitaOtica.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;
end;

procedure TCadReceitaOtica.GerenciarControles;
begin
  inherited;
  ExibirInformacoesVenda;
end;

procedure TCadReceitaOtica.ExibirInformacoesVenda;
var
  exiteVendaVinculada: Boolean;
begin
  exiteVendaVinculada := (cdsData.Active) and (cdsDataID_VENDA.AsInteger > 0);

  lbVenda.Visible := exiteVendaVinculada;
  EdtNumeroVenda.Visible := exiteVendaVinculada;
end;

procedure TCadReceitaOtica.gbDBTextEdit16KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key <> #8 then
    Key := keyNumber(Key);
end;

procedure TCadReceitaOtica.gbDBTextEdit17KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key <> #8 then
    Key := keyNumber(Key);
end;

procedure TCadReceitaOtica.gbDBTextEdit18KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key <> #8 then
    Key := keyNumber(Key);
end;

procedure TCadReceitaOtica.gbDBTextEdit19KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key <> #8 then
    Key := keyNumber(Key);
end;

initialization
  RegisterClass(TCadReceitaOtica);

finalization
  UnRegisterClass(TCadReceitaOtica);

end.
