unit uCadCep;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEditPK, cxDBEdit,
  uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK,
  Vcl.StdCtrls;

type
  TCadCep = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    eFkModelo: TgbDBButtonEditFK;
    descModelo: TgbDBTextEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    Label2: TLabel;
    Label3: TLabel;
    ePkCodig: TgbDBTextEditPK;
    Label4: TLabel;
    edDescricao: TgbDBTextEdit;
    Label5: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    cdsDataID: TAutoIncField;
    cdsDataCEP: TStringField;
    cdsDataID_CIDADE: TIntegerField;
    cdsDataRUA: TStringField;
    cdsDataID_BAIRRO: TIntegerField;
    cdsDataJOIN_CIDADE_CIDADE: TStringField;
    cdsDataJOIN_DESCRICAO_BAIRRO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

Initialization
  RegisterClass(TCadCep);

Finalization
  UnRegisterClass(TCadCep);

end.
