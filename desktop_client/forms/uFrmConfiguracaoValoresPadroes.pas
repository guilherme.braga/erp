unit uFrmConfiguracaoValoresPadroes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxDropDownEdit, Datasnap.DBClient, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxInplaceContainer, cxVGrid, cxDBVGrid, cxCurrencyEdit, cxMaskEdit, cxCalendar;

type
  TFrmConfigurarValoresPadroes = class(TFrmModalPadrao)
    GridValoresPadrao: TcxVerticalGrid;
    ActConstantes: TAction;
    cxButton1: TcxButton;
    procedure ActConstantesExecute(Sender: TObject);
  private
    FIdentificador: String;
    function ExisteRegistro: Boolean;
    procedure ConstruirVisaoDatasetPrincipal(ADataset: TDataset);
    procedure ConstruirVisaoDatasetDetail(ADataset: TDataset);
    procedure PreencherValoresJaSalvos;
    function AdicionarCategoria(ADataset: TDataset): TcxCategoryRow;
  public
    class procedure ConfigurarValoresPadroes(
      AIdentificador: String; AIdUsuario: Integer; ADataset: TClientDataset);

   protected
     procedure Confirmar; override;
     procedure Cancelar; override;
  end;

var
  FrmConfigurarValoresPadroes: TFrmConfigurarValoresPadroes;

implementation

{$R *.dfm}

uses uVerticalGridUtils, uUsuarioDesignControl, uSistema, uTMessage;

{ TFrmConfigurarValoresPadroes }

procedure TFrmConfigurarValoresPadroes.ActConstantesExecute(Sender: TObject);
begin
  inherited;
  ShowMessage(
  '       DATA ATUAL = 01/01/1001'+#13+
  '        DATA HORA = 01/01/1001 00:00:00'+#13+
  '       DATA VAZIA = 31/01/1001 00:00:00'+#13+
  '  DATA HORA VAZIA = 31/01/1001 00:00:00'+#13+
  '            VAZIO = #VAZIO#');
end;

function TFrmConfigurarValoresPadroes.AdicionarCategoria(
  ADataset: TDataset): TcxCategoryRow;
begin
  result := TcxCategoryRow(GridValoresPadrao.Add(TcxCategoryRow));
  result.Properties.Caption := ADataset.Name;
end;

procedure TFrmConfigurarValoresPadroes.Cancelar;
begin
  inherited;

end;

class procedure TFrmConfigurarValoresPadroes.ConfigurarValoresPadroes(
  AIdentificador: String; AIdUsuario: Integer; ADataset: TClientDataset);
begin
  Application.CreateForm(TFrmConfigurarValoresPadroes, FrmConfigurarValoresPadroes);
  try
    with FrmConfigurarValoresPadroes do
    begin
      FIdentificador := AIdentificador;
      ConstruirVisaoDatasetPrincipal(ADataset);
      ConstruirVisaoDatasetDetail(ADataset);

      if ExisteRegistro then
      begin
        PreencherValoresJaSalvos;
      end;

      ShowModal;
    end;
  finally
    FreeAndNil(FrmConfigurarValoresPadroes);
  end;
end;

procedure TFrmConfigurarValoresPadroes.Confirmar;
begin
  TUsuarioDesignControl.SalvarValoresPadrao(GridValoresPadrao,
    TSistema.Sistema.usuario.idSeguranca, FIdentificador);
  inherited;
end;

procedure TFrmConfigurarValoresPadroes.ConstruirVisaoDatasetDetail(
  ADataset: TDataset);
var
  i: Integer;
  datasetDetail: TDataset;
begin
  //Adicionar categoria e campos dos datasets detail
  for i := 0 to Pred(ADataset.Fields.Count) do
  begin
    if ADataset.Fields[i] is TDatasetField then
    begin
      datasetDetail := TDatasetField(ADataset.Fields[i]).NestedDataSet;
      TVerticalGridUtils.setEditorRowsFromDataset(GridValoresPadrao,
        AdicionarCategoria(datasetDetail), datasetDetail);
      ConstruirVisaoDatasetDetail(datasetDetail);
    end;
  end;
end;

procedure TFrmConfigurarValoresPadroes.ConstruirVisaoDatasetPrincipal(ADataset: TDataset);
begin
  //Adicionar categoria e campos referente ao dataset master
  TVerticalGridUtils.setEditorRowsFromDataset(GridValoresPadrao,
        AdicionarCategoria(ADataset), ADataset);
end;

function TFrmConfigurarValoresPadroes.ExisteRegistro: Boolean;
begin
  TUsuarioDesignControl.ExisteArquivoValoresPadrao(
    TSistema.Sistema.usuario.idSeguranca, FIdentificador)
end;

procedure TFrmConfigurarValoresPadroes.PreencherValoresJaSalvos;
begin
  TUsuarioDesignControl.CarregarValoresPadraoNaGrid(GridValoresPadrao,
    TSistema.Sistema.usuario.idSeguranca, FIdentificador);
end;

end.
