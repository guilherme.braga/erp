inherited MovNegociacaoContaReceber: TMovNegociacaoContaReceber
  Caption = 'Negocia'#231#227'o de Contas a Receber'
  ClientHeight = 457
  ClientWidth = 1280
  ExplicitWidth = 1296
  ExplicitHeight = 496
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1280
    ExplicitWidth = 1211
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1280
    Height = 330
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1211
    ExplicitHeight = 369
    ClientRectBottom = 330
    ClientRectRight = 1280
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1211
      ExplicitHeight = 345
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1248
        Height = 306
        ExplicitWidth = 1179
        ExplicitHeight = 345
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 345
        Height = 306
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 306
        ExplicitHeight = 345
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1211
      ExplicitHeight = 345
      inherited DesignPanel: TJvDesignPanel
        Width = 1280
        Height = 306
        ExplicitWidth = 1211
        ExplicitHeight = 345
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1211
          ExplicitHeight = 345
          Height = 306
          Width = 1280
          inherited dxBevel1: TdxBevel
            Width = 1276
            Height = 34
            ExplicitWidth = 1276
            ExplicitHeight = 34
          end
          object Label2: TLabel
            Left = 12
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label3: TLabel
            Left = 108
            Top = 9
            Width = 44
            Height = 13
            Caption = 'Cadastro'
          end
          object Label16: TLabel
            Left = 1099
            Top = 9
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
            ExplicitLeft = 1107
          end
          object Label17: TLabel
            Left = 909
            Top = 9
            Width = 52
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Finaliza'#231#227'o'
            ExplicitLeft = 917
          end
          object Label18: TLabel
            Left = 609
            Top = 9
            Width = 43
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Processo'
            ExplicitLeft = 617
          end
          object Label19: TLabel
            Left = 291
            Top = 9
            Width = 36
            Height = 13
            Caption = 'Usu'#225'rio'
          end
          object PnTitulosParaNegociacao: TgbPanel
            Left = 2
            Top = 36
            Align = alClient
            Alignment = alCenterCenter
            Caption = 'TFrameNegociacaoContaReceberTitulosParaNegociacao'
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            ExplicitTop = 34
            ExplicitWidth = 307
            ExplicitHeight = 335
            DesignSize = (
              562
              268)
            Height = 268
            Width = 562
            object PnSelecionarTodos: TgbPanel
              Left = 506
              Top = 334
              Anchors = [akRight, akBottom]
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              ExplicitLeft = 580
              ExplicitTop = 375
              Height = 33
              Width = 39
              object JvTransparentButton1: TJvTransparentButton
                Left = 2
                Top = 2
                Width = 35
                Height = 29
                Align = alClient
                FrameStyle = fsNone
                Images.ActiveImage = DmAcesso.cxImage16x16
                Images.ActiveIndex = 32
                Images.GrayImage = DmAcesso.cxImage16x16
                Images.GrayIndex = 32
                Images.DisabledImage = DmAcesso.cxImage16x16
                Images.DisabledIndex = 32
                Images.DownImage = DmAcesso.cxImage16x16
                Images.DownIndex = 32
                Images.HotImage = DmAcesso.cxImage16x16
                Images.HotIndex = 32
                ExplicitLeft = 1
              end
            end
          end
          object PnInformacoesGeral: TgbPanel
            Left = 564
            Top = 36
            Align = alRight
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            ExplicitLeft = 309
            ExplicitTop = 34
            ExplicitHeight = 335
            Height = 268
            Width = 410
            object gbPanel1: TgbPanel
              AlignWithMargins = True
              Left = 5
              Top = 24
              Align = alTop
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              ExplicitLeft = 0
              ExplicitTop = 26
              ExplicitWidth = 406
              DesignSize = (
                400
                303)
              Height = 303
              Width = 400
              object Label1: TLabel
                Left = 5
                Top = 100
                Width = 99
                Height = 13
                Caption = 'Centro de Resultado'
              end
              object Label5: TLabel
                Left = 5
                Top = 123
                Width = 81
                Height = 13
                Caption = 'Conta de An'#225'lise'
              end
              object Label6: TLabel
                Left = 5
                Top = 29
                Width = 34
                Height = 13
                Caption = 'Pessoa'
              end
              object Label7: TLabel
                Left = 5
                Top = 146
                Width = 102
                Height = 13
                Caption = 'Forma de Pagamento'
              end
              object lbContaCorrente: TLabel
                Left = 5
                Top = 192
                Width = 75
                Height = 13
                Caption = 'Conta Corrente'
              end
              object Label4: TLabel
                Left = 5
                Top = 6
                Width = 20
                Height = 13
                Caption = 'Filial'
              end
              object Label8: TLabel
                Left = 5
                Top = 169
                Width = 39
                Height = 13
                Caption = 'Carteira'
              end
              object labelObservacao: TLabel
                Left = 5
                Top = 284
                Width = 58
                Height = 13
                Caption = 'Observa'#231#227'o'
              end
              object Label9: TLabel
                Left = 5
                Top = 214
                Width = 98
                Height = 13
                Caption = 'Plano de Pagamento'
              end
              object Label10: TLabel
                Left = 199
                Top = 238
                Width = 72
                Height = 13
                Caption = 'Dt. Documento'
              end
              object Label11: TLabel
                Left = 5
                Top = 238
                Width = 45
                Height = 13
                Caption = 'Dt. Inicial'
              end
              object Label12: TLabel
                Left = 5
                Top = 261
                Width = 80
                Height = 13
                Caption = 'Dt. Compet'#234'ncia'
              end
              object Label15: TLabel
                Left = 199
                Top = 261
                Width = 48
                Height = 13
                Caption = 'Vl. L'#237'quido'
              end
              object Label13: TLabel
                Left = 5
                Top = 52
                Width = 54
                Height = 13
                Caption = 'Documento'
              end
              object Label14: TLabel
                Left = 5
                Top = 75
                Width = 46
                Height = 13
                Caption = 'Descri'#231#227'o'
              end
              object edtDescCentroResultado: TgbDBTextEdit
                Left = 169
                Top = 96
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object edtCentroResultado: TgbDBButtonEditFK
                Left = 112
                Top = 96
                DataBinding.DataField = 'ID_CENTRO_RESULTADO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 1
                gbTextEdit = edtDescCentroResultado
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
                gbTableName = 'CENTRO_RESULTADO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CENTRO_RESULTADO'
                Width = 60
              end
              object edtDescContaAnalise: TgbDBTextEdit
                Left = 169
                Top = 119
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object edtPessoa: TgbDBButtonEditFK
                Left = 112
                Top = 25
                DataBinding.DataField = 'ID_PESSOA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 3
                gbTextEdit = edtDescPessoa
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_PESSOA;JOIN_DESCRICAO_NOME_PESSOA'
                gbTableName = 'PESSOA'
                gbCamposConsulta = 'ID;NOME'
                gbIdentificadorConsulta = 'PESSOA'
                Width = 60
              end
              object edtDescPessoa: TgbDBTextEdit
                Left = 169
                Top = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_NOME_PESSOA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object edtFormaPagamento: TgbDBButtonEditFK
                Left = 112
                Top = 142
                DataBinding.DataField = 'ID_FORMA_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 5
                gbTextEdit = edtDescFormaPagamento
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_FORMA_PAGAMENTO;JOIN_DESCRICAO_FORMA_PAGAMENTO'
                gbTableName = 'FORMA_PAGAMENTO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'FORMA_PAGAMENTO'
                Width = 60
              end
              object edtDescFormaPagamento: TgbDBTextEdit
                Left = 169
                Top = 142
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 6
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object EdtContaAnalise: TcxDBButtonEdit
                Left = 112
                Top = 119
                DataBinding.DataField = 'ID_CONTA_ANALISE'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ClickKey = 13
                Style.Color = 14606074
                TabOrder = 7
                Width = 60
              end
              object edtContaCorrente: TgbDBButtonEditFK
                Left = 112
                Top = 188
                DataBinding.DataField = 'ID_CONTA_CORRENTE'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = clWhite
                TabOrder = 8
                gbTextEdit = descContaCorrente
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CONTA_CORRENTE; JOIN_DESCRICAO_CONTA_CORRENTE'
                gbTableName = 'CONTA_CORRENTE'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CONTA_CORRENTE'
                Width = 60
              end
              object descContaCorrente: TgbDBTextEdit
                Left = 169
                Top = 188
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_CORRENTE'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 9
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object gbDBButtonEditFK1: TgbDBButtonEditFK
                Left = 112
                Top = 2
                DataBinding.DataField = 'ID_PESSOA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 10
                gbTextEdit = gbDBTextEdit1
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_PESSOA;JOIN_DESCRICAO_NOME_PESSOA'
                gbTableName = 'PESSOA'
                gbCamposConsulta = 'ID;NOME'
                gbIdentificadorConsulta = 'PESSOA'
                Width = 60
              end
              object gbDBTextEdit1: TgbDBTextEdit
                Left = 169
                Top = 2
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_NOME_PESSOA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 11
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object edtObservacao: TgbDBBlobEdit
                Left = 111
                Top = 280
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'OBSERVACAO'
                DataBinding.DataSource = dsData
                Properties.BlobEditKind = bekMemo
                Properties.BlobPaintStyle = bpsText
                Properties.ClearKey = 16430
                Properties.ImmediatePost = True
                Properties.PopupHeight = 300
                Properties.PopupWidth = 160
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 12
                ExplicitWidth = 295
                Width = 289
              end
              object gbDBTextEdit3: TgbDBTextEdit
                Left = 169
                Top = 165
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CARTEIRA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 13
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object gbDBButtonEditFK2: TgbDBButtonEditFK
                Left = 112
                Top = 165
                DataBinding.DataField = 'ID_CARTEIRA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 14
                gbTextEdit = gbDBTextEdit3
                gbRequired = True
                gbAtivarPopupMenu = False
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CARTEIRA; JOIN_DESCRICAO_CARTEIRA'
                gbTableName = 'CARTEIRA'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CARTEIRA'
                Width = 60
              end
              object gbDBButtonEditFK3: TgbDBButtonEditFK
                Left = 112
                Top = 211
                DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 13
                Properties.ReadOnly = False
                Style.Color = 14606074
                TabOrder = 15
                gbTextEdit = gbDBTextEdit6
                gbRequired = True
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO'
                gbTableName = 'PLANO_PAGAMENTO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'PLANO_PAGAMENTO'
                Width = 60
              end
              object gbDBTextEdit6: TgbDBTextEdit
                Left = 169
                Top = 211
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 16
                gbReadyOnly = True
                gbPassword = False
                ExplicitWidth = 237
                Width = 231
              end
              object edtDtInicial: TgbDBDateEdit
                Left = 112
                Top = 234
                DataBinding.DataField = 'DT_BASE'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 17
                gbRequired = True
                gbDateTime = False
                Width = 83
              end
              object edtDtCompetencia: TgbDBDateEdit
                Left = 112
                Top = 257
                DataBinding.DataField = 'DT_COMPETENCIA'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 18
                gbRequired = True
                gbDateTime = False
                Width = 83
              end
              object gbDBTextEdit4: TgbDBTextEdit
                Left = 112
                Top = 48
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 19
                gbRequired = True
                gbPassword = False
                ExplicitWidth = 294
                Width = 288
              end
              object gbDBTextEdit7: TgbDBTextEdit
                Left = 112
                Top = 71
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 20
                gbRequired = True
                gbPassword = False
                ExplicitWidth = 294
                Width = 288
              end
              object edDtDocumento: TgbDBDateEdit
                Left = 274
                Top = 234
                DataBinding.DataField = 'DT_DOCUMENTO'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.ReadOnly = False
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = 14606074
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 21
                gbRequired = True
                gbDateTime = False
                Width = 126
              end
              object gbDBTextEdit5: TgbDBTextEdit
                Left = 274
                Top = 257
                TabStop = False
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 22
                gbReadyOnly = True
                gbPassword = False
                Width = 126
              end
            end
            object gbPanel3: TgbPanel
              Left = 2
              Top = 2
              Align = alTop
              Alignment = alCenterCenter
              Caption = 'Informe os dados para o novo parcelamento'
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              ParentBackground = False
              ParentFont = False
              Style.BorderStyle = ebsOffice11
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TextStyle = [fsBold]
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              ExplicitLeft = 4
              ExplicitTop = 10
              Height = 19
              Width = 406
            end
          end
          object PnParcelas: TgbPanel
            Left = 974
            Top = 36
            Align = alRight
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            ExplicitLeft = 976
            ExplicitTop = 34
            ExplicitHeight = 335
            Height = 268
            Width = 304
            object gridParcela: TcxGrid
              AlignWithMargins = True
              Left = 5
              Top = 24
              Width = 294
              Height = 239
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              ExplicitLeft = 2
              ExplicitTop = 132
              ExplicitWidth = 78
              ExplicitHeight = 158
              object cxGridDBBandedTableView1: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.NavigatorHints = True
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Styles.ContentOdd = DmAcesso.OddColor
                Bands = <
                  item
                  end>
                object cxGridDBBandedTableView1ID: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 54
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_TITULO'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.AssignedValues.MinValue = True
                  Properties.MaxValue = 999999999999999.000000000000000000
                  Properties.Nullstring = '0'
                  Options.Sorting = False
                  Width = 124
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_VENCIMENTO'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Properties.ImmediatePost = True
                  Properties.PostPopupValueOnTab = True
                  Properties.SaveTime = False
                  Properties.ShowTime = False
                  Options.Sorting = False
                  Width = 101
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1DT_COMPETENCIA: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_COMPETENCIA'
                  Width = 117
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1DT_DOCUMENTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'DT_DOCUMENTO'
                  Width = 56
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = cxGridDBBandedTableView1
              end
            end
            object gbPanel2: TgbPanel
              Left = 2
              Top = 2
              Align = alTop
              Alignment = alCenterCenter
              Caption = 'Novo parcelamento gerado'
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              ParentBackground = False
              ParentFont = False
              Style.BorderStyle = ebsOffice11
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TextStyle = [fsBold]
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              ExplicitTop = 113
              ExplicitWidth = 78
              Height = 19
              Width = 300
            end
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 49
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Width = 56
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 156
            Top = 5
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 1144
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 129
          end
          object gbDBDateEdit2: TgbDBDateEdit
            Left = 965
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_FECHAMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbDateTime = True
            ExplicitLeft = 973
            Width = 130
          end
          object descProcesso: TgbDBTextEdit
            Left = 702
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'JOIN_ORIGEM_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            ExplicitLeft = 710
            Width = 201
          end
          object gbDBTextEdit8: TgbDBTextEdit
            Left = 656
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            gbReadyOnly = True
            gbPassword = False
            ExplicitLeft = 664
            Width = 49
          end
          object gbDBTextEdit9: TgbDBTextEdit
            Left = 381
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_NOME_USUARIO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 9
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 224
          end
          object gbDBTextEdit10: TgbDBTextEdit
            Left = 329
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID_PESSOA_USUARIO_CADASTRO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 10
            gbReadyOnly = True
            gbPassword = False
            Width = 55
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspNegociacaoContaReceber'
    RemoteServer = DmConnection.dspNegociacaoContaReceber
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataVL_TOTAL: TFMTBCDField
      DisplayLabel = 'Vl. Total'
      FieldName = 'VL_TOTAL'
      Origin = 'VL_TOTAL'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_ENTRADA: TFMTBCDField
      DisplayLabel = 'Vl. Entrada'
      FieldName = 'VL_ENTRADA'
      Origin = 'VL_ENTRADA'
      Precision = 24
      Size = 9
    end
    object cdsDataDT_PARCELA_INICIAL: TDateField
      DisplayLabel = 'Dt. Parcela Inicial'
      FieldName = 'DT_PARCELA_INICIAL'
      Origin = 'DT_PARCELA_INICIAL'
      Required = True
    end
    object cdsDataDT_COMPETENCIA_INICIAL: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia Inicial'
      FieldName = 'DT_COMPETENCIA_INICIAL'
      Origin = 'DT_COMPETENCIA_INICIAL'
      Required = True
    end
    object cdsDataSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Size = 15
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
      Required = True
    end
    object cdsDataID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      Required = True
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
      Required = True
    end
    object cdsDataID_CONTA_CORRENTE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta Corrente'
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_DESCRICAO_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Origem da Chave de Processo'
      FieldName = 'JOIN_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Origem da Chave de Processo'
      FieldName = 'JOIN_ID_ORIGEM_CHAVE_PROCESSO'
      Origin = 'ID_ORIGEM'
      ProviderFlags = []
    end
    object cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'JOIN_DESCRICAO_CONTA_CORRENTE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_NOME_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDatafdqNegociacaoContaReceberAnteri: TDataSetField
      FieldName = 'fdqNegociacaoContaReceberAnteri'
    end
    object cdsDatafdqNegociacaoContaReceberGerado: TDataSetField
      FieldName = 'fdqNegociacaoContaReceberGerado'
    end
    object cdsDatafdqNegociacaoContaReceberParcel: TDataSetField
      FieldName = 'fdqNegociacaoContaReceberParcel'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42243.606676296290000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsNegociacaoContaReceberParcela: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqNegociacaoContaReceberParcel
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 792
    Top = 80
    object cdsNegociacaoContaReceberParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsNegociacaoContaReceberParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsNegociacaoContaReceberParcelaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsNegociacaoContaReceberParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Vl. T'#237'tulo'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      Precision = 24
      Size = 9
    end
    object cdsNegociacaoContaReceberParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsNegociacaoContaReceberParcelaNR_PARCELA: TIntegerField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsNegociacaoContaReceberParcelaSEQUENCIA: TStringField
      DisplayLabel = 'Sequ'#234'ncia'
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
      Size = 10
    end
    object cdsNegociacaoContaReceberParcelaDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
    end
    object cdsNegociacaoContaReceberParcelaDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      Required = True
    end
    object cdsNegociacaoContaReceberParcelaDT_DOCUMENTO: TDateField
      DisplayLabel = 'Dt. Documento'
      FieldName = 'DT_DOCUMENTO'
      Origin = 'DT_DOCUMENTO'
      Required = True
    end
    object cdsNegociacaoContaReceberParcelaID_NEGOCIACAO_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Recener'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
  end
  object cdsNegociacaoContaReceberTituloGerado: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqNegociacaoContaReceberGerado
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 832
    Top = 80
    object cdsNegociacaoContaReceberTituloGeradoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsNegociacaoContaReceberTituloGeradoID_NEGOCIACAO_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Receber'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
    object cdsNegociacaoContaReceberTituloGeradoID_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo do Conta a Receber'
      FieldName = 'ID_CONTA_RECEBER'
      Origin = 'ID_CONTA_RECEBER'
      Required = True
    end
  end
  object cdsNegociacaoContaReceberTituloAnterior: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqNegociacaoContaReceberAnteri
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 880
    Top = 80
    object cdsNegociacaoContaReceberTituloAnteriorID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsNegociacaoContaReceberTituloAnteriorID_NEGOCIACAO_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo da Negocia'#231#227'o de Conta a Receber'
      FieldName = 'ID_NEGOCIACAO_CONTA_RECEBER'
      Origin = 'ID_NEGOCIACAO_CONTA_RECEBER'
    end
    object cdsNegociacaoContaReceberTituloAnteriorID_CONTA_RECEBER: TIntegerField
      DisplayLabel = 'C'#243'digo do Conta a Receber'
      FieldName = 'ID_CONTA_RECEBER'
      Origin = 'ID_CONTA_RECEBER'
      Required = True
    end
  end
end
