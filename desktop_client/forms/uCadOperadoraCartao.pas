unit uCadOperadoraCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxCheckBox, cxDBEdit,
  uGBDBCheckBox, cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, uGBDBTextEdit,
  cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls;

type
  TCadOperadoraCartao = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    ePkCodig: TgbDBTextEditPK;
    Label2: TLabel;
    edDescricao: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    Label3: TLabel;
    gbDBCheckBox1: TgbDBCheckBox;
    cdsDataDESCRICAO: TStringField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    cdsDataID: TAutoIncField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

Initialization
  RegisterClass(TCadOperadoraCartao);

Finalization
  UnRegisterClass(TCadOperadoraCartao);

end.
