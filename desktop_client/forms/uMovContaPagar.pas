unit uMovContaPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxButtonEdit, cxDBEdit,
  uGBDBButtonEditFK, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxBlobEdit,
  uGBDBBlobEdit, uGBPanel, uFrameDetailPadrao, uFrameQuitacao,
  uFrameQuitacaoContaPagar, cxBarEditItem, dxBarExtItems, cxCurrencyEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  uUsuarioDesignControl, cxLabel, uGBLabel, uFrameFiltroVerticalPadrao, cxSplitter, JvExControls, JvButton,
  JvTransparentButton;

type
  TMovContaPagar = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDOCUMENTO: TStringField;
    cdsDataDESCRICAO: TStringField;
    cdsDataVL_TITULO: TFMTBCDField;
    cdsDataVL_QUITADO: TFMTBCDField;
    cdsDataVL_ABERTO: TFMTBCDField;
    cdsDataQT_PARCELA: TIntegerField;
    cdsDataNR_PARCELA: TIntegerField;
    cdsDataDT_VENCIMENTO: TDateField;
    cdsDataBO_VENCIDO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataSTATUS: TStringField;
    cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsDatafdqContaPagarQuitacao: TDataSetField;
    cdsContaPagarQuitacao: TGBClientDataSet;
    cdsContaPagarQuitacaoID: TAutoIncField;
    cdsContaPagarQuitacaoDH_CADASTRO: TDateTimeField;
    cdsContaPagarQuitacaoVL_QUITACAO: TFMTBCDField;
    cdsContaPagarQuitacaoOBSERVACAO: TBlobField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    labelCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    labelDtCadastro: TLabel;
    edDtCadastro: TgbDBDateEdit;
    edtDescProcesso: TgbDBTextEdit;
    cdsDataID_FORMA_PAGAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsContaPagarQuitacaoNR_ITEM: TIntegerField;
    cdsContaPagarQuitacaoID_TIPO_QUITACAO: TIntegerField;
    cdsContaPagarQuitacaoDT_QUITACAO: TDateField;
    cdsContaPagarQuitacaoID_FILIAL: TIntegerField;
    cdsContaPagarQuitacaoID_CONTA_CORRENTE: TIntegerField;
    cdsContaPagarQuitacaoVL_DESCONTO: TFMTBCDField;
    cdsContaPagarQuitacaoVL_ACRESCIMO: TFMTBCDField;
    cdsContaPagarQuitacaoVL_TOTAL: TFMTBCDField;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    labelProcesso: TLabel;
    edtProcesso: TgbDBTextEdit;
    cdsDataSEQUENCIA: TStringField;
    cdsDataVL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_DECRESCIMO: TFMTBCDField;
    cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField;
    labelSituacao: TLabel;
    edSituacao: TgbDBTextEdit;
    cdsContaPagarQuitacaoID_CONTA_PAGAR: TIntegerField;
    EdtConsultaDocumento: TdxBarEdit;
    EdtConsultaDescricao: TdxBarEdit;
    EdtConsultaVencimentoInicio: TcxBarEditItem;
    EdtConsultaVencimentoFim: TcxBarEditItem;
    EdtConsultaSituacao: TcxBarEditItem;
    lbConsultaVencimento: TdxBarStatic;
    EdtConsultaValor: TcxBarEditItem;
    gbPanel1: TgbPanel;
    labelDocumento: TLabel;
    labelPessoa: TLabel;
    labelCentroResultado: TLabel;
    labelContaAnalise: TLabel;
    labelDescricao: TLabel;
    labelVlTitulo: TLabel;
    labelObservacao: TLabel;
    labelFormaPagamento: TLabel;
    edDocumento: TgbDBTextEdit;
    fkPessoa: TgbDBButtonEditFK;
    descPessoa: TgbDBTextEdit;
    descCentroResultado: TgbDBTextEdit;
    edtCentroResultado: TgbDBButtonEditFK;
    descContaAnalise: TgbDBTextEdit;
    edDescricao: TgbDBTextEdit;
    edVlTitulo: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    edtFormaPagamento: TgbDBButtonEditFK;
    descFormaPagamento: TgbDBTextEdit;
    gbPanel3: TgbPanel;
    cdsDataDT_COMPETENCIA: TDateField;
    cdsContaPagarQuitacaoID_CONTA_ANALISE: TIntegerField;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsDataTP_DOCUMENTO_ORIGEM: TStringField;
    labelOrigem: TLabel;
    edtOrigem: TgbDBTextEdit;
    edtDescOrigem: TgbDBTextEdit;
    cdsContaPagarQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsContaPagarQuitacaoTP_DOCUMENTO_ORIGEM: TStringField;
    cdsContaPagarQuitacaoSTATUS: TStringField;
    cdsContaPagarQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField;
    cdsContaPagarQuitacaoIC_VL_PAGAMENTO: TFloatField;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label19: TLabel;
    Label15: TLabel;
    edVlRecebido: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    edVlPagar: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    cdsDataDT_DOCUMENTO: TDateField;
    labelSequencia: TLabel;
    edtSequencia: TgbDBTextEdit;
    labelDtDocumento: TLabel;
    edtDtDocumento: TgbDBDateEdit;
    labelDtCompetencia: TLabel;
    edtDtCompetencia: TgbDBDateEdit;
    labelDtVencimento: TLabel;
    edDtVencimento: TgbDBDateEdit;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    lbContaCorrente: TLabel;
    edtContaCorrente: TgbDBButtonEditFK;
    descContaCorrente: TgbDBTextEdit;
    PnFrameQuitacao: TgbPanel;
    cdsContaPagarQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    cdsContaPagarQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    cdsContaPagarQuitacaoCHEQUE_SACADO: TStringField;
    cdsContaPagarQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    cdsContaPagarQuitacaoCHEQUE_BANCO: TStringField;
    cdsContaPagarQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    cdsContaPagarQuitacaoCHEQUE_AGENCIA: TStringField;
    cdsContaPagarQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    cdsContaPagarQuitacaoCHEQUE_NUMERO: TIntegerField;
    cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    ActExportarExcel: TAction;
    Label1: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataPERC_MULTA: TFMTBCDField;
    cdsDataVL_MULTA: TFMTBCDField;
    cdsDataPERC_JUROS: TFMTBCDField;
    cdsDataVL_JUROS: TFMTBCDField;
    cdsDataID_CARTEIRA: TIntegerField;
    cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField;
    cdsContaPagarQuitacaoPERC_MULTA: TFMTBCDField;
    cdsContaPagarQuitacaoVL_MULTA: TFMTBCDField;
    cdsContaPagarQuitacaoPERC_JUROS: TFMTBCDField;
    cdsContaPagarQuitacaoVL_JUROS: TFMTBCDField;
    cdsContaPagarQuitacaoPERC_DESCONTO: TFMTBCDField;
    cdsContaPagarQuitacaoPERC_ACRESCIMO: TFMTBCDField;
    ActPagamentoLote: TAction;
    lbPagamentoLote: TdxBarLargeButton;
    PnTotalizadorConsulta: TFlowPanel;
    pnTotalizadorVlAberto: TgbLabel;
    pnTotalizadorAcrecimoAberto: TgbLabel;
    pnTotalizadorTotalAberto: TgbLabel;
    pnTotalizadorVlVencido: TgbLabel;
    pnTotalizadorVlVencer: TgbLabel;
    pnTotalizadorVlRecebido: TgbLabel;
    pnTotalizadorAcrescimoRecebido: TgbLabel;
    pnTotalizadorTotalRecebido: TgbLabel;
    filtroPessoa: TcxBarEditItem;
    filtroNomePessoa: TdxBarStatic;
    cdsContaPagarQuitacaoVL_TROCO: TFMTBCDField;
    cdsContaPagarQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    cdsContaPagarQuitacaoID_USUARIO_BAIXA: TIntegerField;
    cdsContaPagarQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    procedure cdsContaPagarQuitacaoAfterOpen(DataSet: TDataSet);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsContaPagarQuitacaoNewRecord(DataSet: TDataSet);
    procedure CalcularPercentual(Sender: TField);
    procedure CalcularValor(Sender: TField);
    procedure TotalizarAcrescimo(Sender: TField);
    procedure AtualizarVlAberto(Sender: TField);
    procedure AtualizarSituacao;
    procedure AtualizarTotalCapa;
    procedure AtualizarTotalItem;
    procedure cdsContaPagarQuitacaoAfterPost(DataSet: TDataSet);
    procedure cdsContaPagarQuitacaoAfterDelete(DataSet: TDataSet);
    procedure cdsContaPagarQuitacaoVL_QUITACAOChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsContaPagarQuitacaoBeforeInsert(DataSet: TDataSet);
    procedure CalcularValoresAPartirDoTotalQuitacao(Sender: TField);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure EdtContaAnaliseExit(Sender: TObject);
    procedure cdsContaPagarQuitacaoBeforeDelete(DataSet: TDataSet);
    procedure ActExportarExcelExecute(Sender: TObject);
    procedure ActPagamentoLoteExecute(Sender: TObject);
    procedure Level1BandedTableView1DataControllerDataChanged(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure filtroPessoaPropertiesEditValueChanged(Sender: TObject);
    procedure cdsContaPagarQuitacaoDT_QUITACAOChange(Sender: TField);
    procedure cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
    procedure EdtConsultaSituacaoPropertiesEditValueChanged(Sender: TObject);
  private
    FFrameQuitacaoContaPagar: TFrameQuitacaoContaPagar;
    FCalculandoPercentual: Boolean;
    FCalculandoValor: Boolean;
    FCalculandoTotalQuitacaoValor: Boolean;
    FTotalizandoAcrescimo: Boolean;
    FIdChaveProcesso : Integer;
    FIdContaPagar : Integer;
    FCalculandoTotalRodape: Boolean;
    FFiltroRegistroGradeAnterior: String;

    FParametroFiltroDataVencimentoInicio: TParametroFormulario;
    FParametroFiltroDataVencimentoFim: TParametroFormulario;
    FParametroFiltroStatus: TParametroFormulario;
    FParametroImprimirComprovanteAposRealizarQuitacao: TParametroFormulario;
    FParametroPermiteAlterarBaixaDoTitulo: TParametroFormulario;
    FParametroGerarContraPartidaContaCorrenteMovimento: TParametroFormulario;
    FParametroSugerirDataVencimentoNaDataQuitacao: TParametroFormulario;

    procedure HabilitarPainelPrincipal;
    procedure ValidarDataDeCompetencia;
    procedure ValidarCamposObrigatorios;

    procedure CalcularTotaisDoRodape;
    procedure AlinharBarraTotalizadoraConsulta;
    procedure SugerirDataVencimentoCheque;

    function PodeUtilizarFiltroStatusNaPesquisaPadrao: Boolean;
    procedure DefinirIdentificadorDaPesquisaPadrao;
    procedure ChecarConsultaPreenchida;
    function ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
    function ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
    procedure ExibirFiltroPeriodoQuitacao;

    const FIDENTIFICADOR_SQL_ABERTO = 'MOVCONTAPAGARPESQUISAABERTO';
    const FIDENTIFICADOR_SQL_QUITADO = 'MOVCONTAPAGARPESQUISAQUITADO';
    const FIDENTIFICADOR_SQL_GERAL = 'MOVCONTAPAGAR';

  protected
    procedure GerenciarControles; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeConfirmar; override;
    procedure AntesDeRemover; override;
    procedure DepoisDePesquisar; override;
    procedure AntesDePesquisar; override;

    procedure SetWhereBundle;override;
    procedure PrepararFiltros;override;
    procedure AoCriarFormulario;override;

    procedure BloquearEdicao; override;
    procedure InstanciarFrames; override;

    procedure MostrarInformacoesImplantacao; override;

    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;

  public
    FAtualizarTotalItem: Boolean;

    FParametroBloquearDataQuitacao: TParametroFormulario;

    const TODOS: String = 'TODOS';

    function QuitacaoEmCartao: Boolean;

    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

implementation

{$R *.dfm}

uses
  uDmConnection,
  uMathUtils,
  System.Math,
  uChaveProcesso,
  uDatasetUtils,
  uFilial, uContaPagar, uFrmMessage_Process, uTFunction, DateUtils,
  uContaPagarProxy, uChaveProcessoProxy, uFrmMessage, uTMessage,
  uPesqContaAnalise, uSistema, uDevExpressUtils, uConstParametroFormulario, uControlsUtils,
  uTControl_Function, uFrmHelpImplantacaoContaPagar, uMovLotePagamento, ugbFDMemTable, uFrmConsultaPadrao,
  uTPessoa, uPessoaProxy, uTipoQuitacaoProxy;

procedure TMovContaPagar.CalcularPercentual(Sender: TField);

  function PegarPercentualSobreValor: Double;
  begin
    result := TMathUtils.PercentualSobreValor(
      Sender.AsFloat,
      cdsContaPagarQuitacaoVL_QUITACAO.AsFloat);
  end;
begin
  if FCalculandoValor then
    exit;

  FCalculandoValor := True;

  if Sender.Dataset = cdsContaPagarQuitacao then
  begin
    if Sender = cdsContaPagarQuitacaoVL_DESCONTO then
    begin
      cdsContaPagarQuitacaoPERC_DESCONTO.AsFloat := PegarPercentualSobreValor;
    end
    else if Sender = cdsContaPagarQuitacaoVL_ACRESCIMO then
    begin
      cdsContaPagarQuitacaoPERC_ACRESCIMO.AsFloat := PegarPercentualSobreValor;
    end;

    AtualizarTotalItem;

  end;
  FCalculandoValor := False;
end;

procedure TMovContaPagar.CalcularTotaisDoRodape;
var
  fdmTotais: TgbFDMemTable;
  idsContaPagar: String;
begin
  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    idsContaPagar := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
      fdmSearch.FieldByName('ID'), Level1BandedTableView1);

    if idsContaPagar.IsEmpty then
    begin
      idsContaPagar := '-9999999';
    end;

    fdmTotais := TContaPagar.GetTotaisContaPagar(idsContaPagar);

    pnTotalizadorVlAberto.Caption := 'Aberto R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto'));
    pnTotalizadorAcrecimoAberto.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_aberto'));
    pnTotalizadorTotalAberto.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_liquido'));
    pnTotalizadorVlVencido.Caption := 'Vencido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencido'));
    pnTotalizadorVlVencer.Caption := 'A Vencer R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencer'));
    pnTotalizadorVlRecebido.Caption := 'Recebido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido'));
    pnTotalizadorAcrescimoRecebido.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_recebido'));
    pnTotalizadorTotalRecebido.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido_liquido'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;

procedure TMovContaPagar.InstanciarFrames;
begin
  inherited;
  FFrameQuitacaoContaPagar := TFrameQuitacaoContaPagar.Create(PnFrameQuitacao);
  FFrameQuitacaoContaPagar.Parent := PnFrameQuitacao;
  FFrameQuitacaoContaPagar.Align := alClient;
  FFrameQuitacaoContaPagar.permiteAlterar := FParametroPermiteAlterarBaixaDoTitulo.ValorSim;
end;

procedure TMovContaPagar.Level1BandedTableView1DataControllerDataChanged(Sender: TObject);
var
  strings: string;
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not Level1BandedTableView1.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodape;
  end;

  FFiltroRegistroGradeAnterior := Level1BandedTableView1.DataController.Filter.FilterText;
end;

procedure TMovContaPagar.MostrarInformacoesImplantacao;
begin
  inherited;
  TControlFunction.CreateMDIChild(TFrmHelpImplantacaoContaPagar,
    FrmHelpImplantacaoContaPagar);
end;

procedure TMovContaPagar.CalcularValor(Sender: TField);

  function PegarValorSobrePercentual: Double;
  begin
    result := TMathUtils.ValorSobrePercentual(
      Sender.AsFloat,
      cdsContaPagarQuitacaoVL_QUITACAO.AsFloat);
  end;
begin
  if FCalculandoPercentual then
    exit;

  FCalculandoPercentual := True;

  if Sender.Dataset = cdsContaPagarQuitacao then
  begin
    if Sender = cdsContaPagarQuitacaoPERC_DESCONTO then
    begin
      cdsContaPagarQuitacaoVL_DESCONTO.AsFloat := PegarValorSobrePercentual;
    end
    else if Sender = cdsContaPagarQuitacaoPERC_ACRESCIMO then
    begin
      cdsContaPagarQuitacaoVL_ACRESCIMO.AsFloat := PegarValorSobrePercentual;
    end;

    AtualizarTotalItem;

  end;

  FCalculandoPercentual := False;
end;

procedure TMovContaPagar.CalcularValoresAPartirDoTotalQuitacao(Sender: TField);
begin
  if FCalculandoTotalQuitacaoValor then
    exit;

  try
    FCalculandoTotalQuitacaoValor := true;
    cdsContaPagarQuitacaoVL_QUITACAO.AsFloat :=
      cdsContaPagarQuitacaoVL_TOTAL.AsFloat +
      cdsContaPagarQuitacaoVL_DESCONTO.AsFloat -
      cdsContaPagarQuitacaoVL_ACRESCIMO.AsFloat;

    FFrameQuitacaoContaPagar.PreencherValorPagamento;
  finally
    FCalculandoTotalQuitacaoValor := false;
  end;
end;

procedure TMovContaPagar.AtualizarVlAberto(Sender: TField);
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  if Sender = cdsDataVL_TITULO then
    cdsDataVL_ABERTO.AsFloat := cdsDataVL_TITULO.AsFloat;
end;

procedure TMovContaPagar.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados := cdsDataSTATUS.AsString.Equals(
    TContaPagarMovimento.CANCELADO);
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalCapa;
  AtualizarSituacao;
  HabilitarPainelPrincipal;
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FFrameQuitacaoContaPagar.SetConfiguracoesIniciais(cdsContaPagarQuitacao);
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalCapa;
  AtualizarSituacao;
  HabilitarPainelPrincipal
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  cdsData.Alterar;
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ValidarCamposObrigatorios;
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoDT_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;
end;

procedure TMovContaPagar.AntesDeRemover;
var
  ContaPagar : TContaPagar;
begin
  inherited;

  if (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> TContaPagarMovimento.ORIGEM_MANUAL) and
     (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> '') and
     (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> TContaPagarMovimento.ORIGEM_GERACAO_DOCUMENTO)
  then
  begin
    TMessage.MessageInformation('Este Documento deve ser removido apenas a partir de sua origem.');
    Abort;
  end;

  TFrmMessage_Process.SendMessage('Removendo Movimenta��es');
  ContaPagar := TContaPagar.Create;
  try
    ContaPagar.RemoverMovimentosContaCorrente(cdsDataID.AsInteger,
      FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovContaPagar.ActExportarExcelExecute(Sender: TObject);
begin
  inherited;
  TcxGridUtils.ExportarParaExcel(cxGridPesquisaPadrao,
    'Conta a Pagar');
end;

procedure TMovContaPagar.ActPagamentoLoteExecute(Sender: TObject);
var
  fieldPessoa: TField;
begin
  inherited;
  fieldPessoa := fdmSearch.FindField('ID_PESSOA');

  if Assigned(fieldPessoa) then
  begin
    TMovLotePagamento.AbrirTelaFiltrandoPorCodigosDosTitulos(
    TDatasetUtils.ConcatenarRegistrosFiltradosNaViewComCriterios(fdmSearch.FieldByName('ID'),
      Level1BandedTableView1, fieldPessoa, fieldPessoa.AsString));
  end
  else
  begin
    TMovLotePagamento.AbrirTelaFiltrandoPorCodigosDosTitulos(
    TDatasetUtils.ConcatenarRegistrosFiltradosNaView(fdmSearch.FieldByName('ID'), Level1BandedTableView1));
  end;
end;

procedure TMovContaPagar.AlinharBarraTotalizadoraConsulta;
begin
  PnTotalizadorConsulta.AutoWrap := false;
  PnTotalizadorConsulta.AutoWrap := true;
end;

procedure TMovContaPagar.AntesDeConfirmar;
begin
  if cdsContaPagarQuitacao.State in dsEditModes then
  begin
    FFrameQuitacaoContaPagar.ActConfirm.Execute;
  end;

  inherited;
  FIdChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;
  FIdContaPagar    := cdsDataID.AsInteger;

  ValidarDataDeCompetencia;
end;

procedure TMovContaPagar.AntesDePesquisar;
begin
  inherited;
  ChecarConsultaPreenchida;
end;

procedure TMovContaPagar.ValidarCamposObrigatorios;
begin
  cdsData.ValidarCamposObrigatorios;
  ValidarDataDeCompetencia;
end;

procedure TMovContaPagar.ValidarDataDeCompetencia;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
end;

procedure TMovContaPagar.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Documento';
  campoFiltro.campo := 'DT_DOCUMENTO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

{  //Data de Vencimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

{  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TContaPagarMovimento.ABERTO+';'+
  TContaPagarMovimento.QUITADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Centro de Resultado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Centro de Resultado';
  campoFiltro.campo := 'ID_CENTRO_RESULTADO';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CENTRO_RESULTADO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta de Analise
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta de Analise';
  campoFiltro.campo := 'ID_CONTA_ANALISE';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_ANALISE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Carteira
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Carteira';
  campoFiltro.campo := 'ID_CARTEIRA';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CARTEIRA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CONTA_PAGAR';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Per�odo de Quita��o (Dt de Pagamento)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Dt. Pagamento';
  campoFiltro.campo := 'DT_QUITACAO';
  campoFiltro.tabela := 'CONTA_PAGAR_QUITACAO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TMovContaPagar.DepoisDeConfirmar;
var
  ContaPagar : TContaPagar;
begin
  inherited;
  TFrmMessage_Process.SendMessage('Gerando Movimenta��es');
  ContaPagar := TContaPagar.Create;
  try
    ContaPagar.AtualizarMovimentosContaCorrente(FIdContaPagar, FIdChaveProcesso,
      FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  AtualizarRegistro;
end;

procedure TMovContaPagar.DepoisDePesquisar;
begin
  inherited;
  CalcularTotaisDoRodape;
  DefinirIdentificadorDaPesquisaPadrao;
end;

procedure TMovContaPagar.EdtConsultaSituacaoPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  DefinirIdentificadorDaPesquisaPadrao;
  ExibirFiltroPeriodoQuitacao;
end;

procedure TMovContaPagar.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  if not cdsData.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsData.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      cdsData.FieldByName('ID_CONTA_ANALISE'), cdsData.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TMovContaPagar.EdtContaAnaliseExit(Sender: TObject);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovContaPagar.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  if not cdsData.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsData.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      cdsData.FieldByName('ID_CONTA_ANALISE'), cdsData.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TMovContaPagar.ExibirFiltroPeriodoQuitacao;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) then
  begin
    FFiltroVertical.ExibirFiltro('DT_QUITACAO');
  end
  else
  begin
    FFiltroVertical.OcultarFiltro('DT_QUITACAO');
  end;
end;

procedure TMovContaPagar.filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := TFrmConsultaPadrao.ConsultarID('PESSOA');
  if idPessoa > 0 then
  begin
    filtroPessoa.EditValue := idPessoa;
    filtroPessoaPropertiesEditValueChanged(filtroPessoa);
  end;
end;

procedure TMovContaPagar.filtroPessoaPropertiesEditValueChanged(Sender: TObject);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0);
  if idPessoa > 0 then
  begin
    filtroNomePessoa.Caption := Copy(TPessoa.GetNomePessoa(idPessoa),1,40);
  end
  else
  begin
    filtroNomePessoa.Caption := '';
  end;

  abort;
end;

procedure TMovContaPagar.FormResize(Sender: TObject);
begin
  inherited;
  AlinharBarraTotalizadoraConsulta;
end;

procedure TMovContaPagar.GerenciarControles;
var
  estaConsultando: Boolean;
begin
  inherited;
  estaConsultando := (acaoCrud = uFrmCadastro_Padrao.TAcaoCrud(Pesquisar)) and
    (Level1BandedTableView1.DataController.FilteredRecordCount > 0);

  ActPagamentoLote.Visible := estaConsultando;
end;

procedure TMovContaPagar.AoCriarFormulario;
begin
  inherited;
  FTotalizandoAcrescimo := false;
  FAtualizarTotalItem := true;
  EdDescricao.Properties.CharCase := ecNormal;
  EdDocumento.Properties.CharCase := ecNormal;
  FCalculandoTotalRodape := false;
  FFiltroRegistroGradeAnterior := '';
end;

procedure TMovContaPagar.AtualizarSituacao;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  if TMathUtils.Arredondar(cdsDataVL_ABERTO.AsFloat) = 0 then
  begin
    cdsDataSTATUS.AsString := QUITADO;
  end
  else
  begin
    cdsDataSTATUS.AsString := ABERTO;
  end;
end;

procedure TMovContaPagar.AtualizarTotalCapa;
var
  vlTotal, vlLiquido  : Double;
  vlDesconto : Double;
  vlAcrescimo  : Double;
  vlMulta: Double;
  vlJuros: Double;
  percMulta: Double;
  percJuros: Double;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  vlLiquido    := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_TOTAL);
  vlTotal      := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_QUITACAO);
  vlDesconto   := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_DESCONTO);
  vlAcrescimo  := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_ACRESCIMO);
  vlMulta := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_MULTA);
  vlJuros := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoVL_JUROS);
  percMulta := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoPERC_MULTA);
  percJuros := TDatasetUtils.SomarColuna(cdsContaPagarQuitacaoPERC_JUROS);

  cdsDataVL_JUROS.AsFloat  := vlJuros;
  cdsDataVL_MULTA.AsFloat  := vlMulta;
  cdsDataVL_ACRESCIMO.AsFloat  := vlAcrescimo;
  cdsDataVL_DECRESCIMO.AsFloat := vlDesconto;

  cdsDataPERC_JUROS.AsFloat  := percJuros;
  cdsDataPERC_MULTA.AsFloat  := percMulta;

  cdsDataVL_QUITADO.AsFloat         := vlTotal;
  cdsDataVL_QUITADO_LIQUIDO.AsFloat := vlLiquido;

  cdsDataVL_ABERTO.AsFloat  := cdsDataVL_TITULO.AsFloat - vlTotal;
end;

procedure TMovContaPagar.AtualizarTotalItem;
begin
  if not FAtualizarTotalItem then
    exit;

  cdsContaPagarQuitacaoVL_TOTAL.AsFloat := cdsContaPagarQuitacaoVL_QUITACAO.AsFloat
                                          + cdsContaPagarQuitacaoVL_ACRESCIMO.AsFloat
                                          - cdsContaPagarQuitacaoVL_DESCONTO.AsFloat;
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsContaPagarQuitacaoDH_CADASTRO.AsDateTime     := Now;

  if FParametroSugerirDataVencimentoNaDataQuitacao.ValorSim then
  begin
    cdsContaPagarQuitacaoDT_QUITACAO.AsDateTime := cdsDataDT_VENCIMENTO.AsDateTime;
  end
  else
  begin
    cdsContaPagarQuitacaoDT_QUITACAO.AsDateTime := Now;
  end;

  cdsContaPagarQuitacaoID_USUARIO_BAIXA.AsInteger := TSistema.Sistema.Usuario.Id;
  cdsContaPagarQuitacaoJOIN_NOME_USUARIO_BAIXA.AsString := TSistema.Sistema.Usuario.Nome;
  cdsContaPagarQuitacaoNR_ITEM.AsFloat            := TDatasetUtils.MaiorValor(cdsContaPagarQuitacaoNR_ITEM)+1;
  cdsContaPagarQuitacaoID_FILIAL.AsInteger        := TFilial.GetIdFilial;
  cdsContaPagarQuitacaoVL_QUITACAO.AsFloat        := cdsDataVL_ABERTO.AsFloat;
  cdsContaPagarQuitacaoVL_TOTAL.AsFloat           := cdsDataVL_ABERTO.AsFloat;
  cdsContaPagarQuitacaoID_CONTA_ANALISE.AsInteger := cdsDataID_CONTA_ANALISE.AsInteger;
  cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_ANALISE.AsString := cdsDataJOIN_DESCRICAO_CONTA_ANALISE.AsString;
  cdsContaPagarQuitacaoID_CENTRO_RESULTADO.AsInteger := cdsDataID_CENTRO_RESULTADO.AsInteger;
  cdsContaPagarQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO.AsString := cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO.AsString;

  if cdsDataID_CONTA_CORRENTE.AsInteger > 0 then
  begin
    cdsContaPagarQuitacaoID_CONTA_CORRENTE.AsInteger := cdsDataID_CONTA_CORRENTE.AsInteger;
    cdsContaPagarQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE.AsString := cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString;
  end;

  cdsContaPagarQuitacaoID_DOCUMENTO_ORIGEM.AsInteger := 0;
  cdsContaPagarQuitacaoTP_DOCUMENTO_ORIGEM.AsString  := TContaPagarQuitacaoMovimento.ORIGEM_MANUAL;
  cdsContaPagarQuitacaoSTATUS.AsString               := TContaPagarQuitacaoMovimento.STATUS_CONFIRMADA;

  //Atribui��es do juros e multa
  TContaPagar.SetEncargosEmAberto(cdsData, cdsContaPagarQuitacao);
end;

procedure TMovContaPagar.cdsContaPagarQuitacaoVL_QUITACAOChange(
  Sender: TField);
begin
  inherited;
  AtualizarTotalItem;
end;

procedure TMovContaPagar.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  HabilitarPainelPrincipal;
end;

procedure TMovContaPagar.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;

  cdsDataDH_CADASTRO.AsDateTime      := Now;
  cdsDataDT_DOCUMENTO.AsDateTime     := Date;
  cdsDataNR_PARCELA.AsInteger        := 1;

  cdsDataID_CHAVE_PROCESSO.AsInteger :=
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_CONTA_PAGAR,
      cdsDataID.AsInteger);

  cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString :=
    TChaveProcessoProxy.ORIGEM_CONTA_PAGAR;

  cdsDataSEQUENCIA.AsString          := '1/1';

  cdsDataID_DOCUMENTO_ORIGEM.AsInteger := 0;
  cdsDataTP_DOCUMENTO_ORIGEM.AsString  := TContaPagarMovimento.ORIGEM_MANUAL;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;
end;

procedure TMovContaPagar.ChecarConsultaPreenchida;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) and ExistePesquisaConfiguradaParaSituacaoQuitado then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(EdtConsultaSituacao.EditValue).Equals(ABERTO) and ExistePesquisaConfiguradaParaSituacaoAberto then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_GERAL;
end;

procedure TMovContaPagar.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Data de Vencimento Inicio
  FParametroFiltroDataVencimentoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataVencimentoInicio);

  //Data de Vencimento Fim
  FParametroFiltroDataVencimentoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataVencimentoFim);

  //Status
  FParametroFiltroStatus := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_STATUS,
    TConstParametroFormulario.DESCRICAO_FILTRO_STATUS,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroStatus);

  //Imprimir comprovante apos incluir quita��o
  FParametroImprimirComprovanteAposRealizarQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_IMPRIMIR_COMPROVANTE_APOS_REALIZAR_QUITACAO,
    TConstParametroFormulario.DESCRICAO_IMPRIMIR_COMPROVANTE_APOS_REALIZAR_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroImprimirComprovanteAposRealizarQuitacao);

  //Permite alterar baixa do t�tulo
  FParametroPermiteAlterarBaixaDoTitulo := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_PERMITE_ALTERAR_BAIXA_DO_TITULO,
    TConstParametroFormulario.DESCRICAO_PERMITE_ALTERAR_BAIXA_DO_TITULO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroPermiteAlterarBaixaDoTitulo);

  //Gerar contra partida no conta corrente movimento
  FParametroGerarContraPartidaContaCorrenteMovimento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TConstParametroFormulario.DESCRICAO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroGerarContraPartidaContaCorrenteMovimento);

  //Sugerir Data de Vencimento na Data de Quita��o
  FParametroSugerirDataVencimentoNaDataQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_SUGERIR_DATA_VENCIMENTO_NA_DATA_QUITACAO,
    TConstParametroFormulario.DESCRICAO_SUGERIR_DATA_VENCIMENTO_NA_DATA_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroSugerirDataVencimentoNaDataQuitacao);

  //Bloquear data de quita��o
  FParametroBloquearDataQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_BLOQUEAR_DATA_QUITACAO,
    TConstParametroFormulario.DESCRICAO_BLOQUEAR_DATA_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroBloquearDataQuitacao);
end;

procedure TMovContaPagar.HabilitarPainelPrincipal;
begin
  gbPanel1.Enabled := (cdsContaPagarQuitacao.IsEmpty);
end;

procedure TMovContaPagar.PrepararFiltros;
begin
  inherited;
  filtroPessoa.EditValue := null;
  filtroNomePessoa.Caption := '';
  EdtConsultaDocumento.Text := '';
  EdtConsultaDescricao.Text := '';
  EdtConsultaVencimentoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataVencimentoInicio.AsString);
  EdtConsultaVencimentoFim.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataVencimentoFim.AsString);
  EdtConsultaSituacao.EditValue :=
    TFunction.Iif(FParametroFiltroStatus.EstaPreenchido, FParametroFiltroStatus.AsString, TODOS);
  EdtConsultaValor.EditValue := null;

  DefinirIdentificadorDaPesquisaPadrao;
  ExibirFiltroPeriodoQuitacao;
end;

function TMovContaPagar.QuitacaoEmCartao: Boolean;
begin
  result := cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovContaPagar.SetWhereBundle;
var AItemList: TcxFilterCriteriaItemList;
    BetweenCondicional: array of string;
    textoBetween: String;
begin
  inherited;
  //Pessoa
  if not VartoStr(filtroPessoa.EditValue).IsEmpty then
    WhereSearch.AddIgual(TContaPagarMovimento.FIELD_PESSOA, StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0));

  //Descri��o
  if not VartoStr(EdtConsultaDocumento.Text).IsEmpty then
    WhereSearch.AddContem(FIELD_DOCUMENTO, UpperCase(EdtConsultaDocumento.Text));

  //Documento
  if not VartoStr(EdtConsultaDescricao.Text).IsEmpty then
    WhereSearch.AddContem(FIELD_DOCUMENTO, UpperCase(EdtConsultaDescricao.Text));

  //Vencimento
  if not(VartoStr(EdtConsultaVencimentoInicio.EditValue).IsEmpty) and
     not(VartoStr(EdtConsultaVencimentoFim.EditValue).IsEmpty) and
     (VarToDateTime(EdtConsultaVencimentoInicio.EditValue) <= VarToDateTime(EdtConsultaVencimentoFim.EditValue)) then
    WhereSearch.AddEntre(FIELD_DATA_VENCIMENTO, StrtoDate(EdtConsultaVencimentoInicio.EditValue), StrtoDate(EdtConsultaVencimentoFim.EditValue));

  //Situa��o
  if not VartoStr(EdtConsultaSituacao.EditValue).Equals(TODOS) then
    WhereSearch.AddIgual(FIELD_STATUS,VartoStr(EdtConsultaSituacao.EditValue));

  //Valor
  if StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0) > 0 then
    WhereSearch.AddIgual(FIELD_VALOR_TITULO, StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0));
end;

procedure TMovContaPagar.SugerirDataVencimentoCheque;
begin
  if not cdsContaPagarQuitacao.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsContaPagarQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsContaPagarQuitacaoCHEQUE_DT_VENCIMENTO.AsDateTime := cdsContaPagarQuitacaoDT_QUITACAO.AsDateTime;
  end
  else
  begin
    cdsContaPagarQuitacaoCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

procedure TMovContaPagar.TotalizarAcrescimo(Sender: TField);
begin
  if (FTotalizandoAcrescimo) or (Sender.Dataset <> cdsContaPagarQuitacao) then
    exit;

  try
    FTotalizandoAcrescimo := True;

    cdsContaPagarQuitacaoVL_ACRESCIMO.AsFloat :=
      cdsContaPagarQuitacaoVL_MULTA.AsFloat +
      cdsContaPagarQuitacaoVL_JUROS.AsFloat;
  finally
    FTotalizandoAcrescimo := False;
  end;
end;

function TMovContaPagar.PodeUtilizarFiltroStatusNaPesquisaPadrao: Boolean;
begin
  result := ExistePesquisaConfiguradaParaSituacaoAberto and ExistePesquisaConfiguradaParaSituacaoQuitado;
end;

procedure TMovContaPagar.DefinirIdentificadorDaPesquisaPadrao;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(EdtConsultaSituacao.EditValue).Equals(ABERTO) then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_GERAL;
end;

function TMovContaPagar.ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_ABERTO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

function TMovContaPagar.ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_QUITADO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

Initialization
  RegisterClass(TMovContaPagar);

Finalization
  UnRegisterClass(TMovContaPagar);

end.
