unit uCadGradeProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, cxCheckBox, cxDBEdit, uGBDBCheckBox, uGBDBTextEdit, Vcl.StdCtrls, cxTextEdit, uGBDBTextEditPK,
  cxMaskEdit, cxDropDownEdit, cxBlobEdit, uGBDBBlobEdit, uFrameGradeProduto;

type
  TCadGradeProduto = class(TFrmCadastro_Padrao)
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    gbPanel1: TgbPanel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    Label2: TLabel;
    PnFrameGradeProdutoCor: TgbPanel;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataBO_ATIVO: TStringField;
    cdsDatafdqGradeProdutoCorTamanho: TDataSetField;
    cdsDatafdqGradeProdutoCor: TDataSetField;
    Label3: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    cdsGradeProdutoCor: TGBClientDataSet;
    cdsGradeProdutoCorTamanho: TGBClientDataSet;
    cdsGradeProdutoCorID: TAutoIncField;
    cdsGradeProdutoCorID_GRADE_PRODUTO: TIntegerField;
    cdsGradeProdutoCorID_COR: TIntegerField;
    cdsGradeProdutoCorJOIN_DESCRICAO_COR: TStringField;
    cdsGradeProdutoCorTamanhoID: TAutoIncField;
    cdsGradeProdutoCorTamanhoID_GRADE_PRODUTO: TIntegerField;
    cdsGradeProdutoCorTamanhoID_COR: TIntegerField;
    cdsGradeProdutoCorTamanhoJOIN_DESCRICAO_COR: TStringField;
    cdsGradeProdutoCorTamanhoID_TAMANHO: TIntegerField;
    cdsGradeProdutoCorTamanhoJOIN_DESCRICAO_TAMANHO: TStringField;
    procedure cdsGradeProdutoCorAfterPost(DataSet: TDataSet);
    procedure cdsGradeProdutoCorAfterScroll(DataSet: TDataSet);
    procedure cdsGradeProdutoCorBeforeDelete(DataSet: TDataSet);
    procedure cdsGradeProdutoCorBeforeInsert(DataSet: TDataSet);
    procedure cdsGradeProdutoCorBeforePost(DataSet: TDataSet);
    procedure cdsGradeProdutoCorTamanhoBeforeInsert(DataSet: TDataSet);
    procedure cdsGradeProdutoCorTamanhoBeforePost(DataSet: TDataSet);
    procedure cdsGradeProdutoCorTamanhoNewRecord(DataSet: TDataSet);
  private
    FFrameGradeProdutoCor: TFrameGradeProduto;
    procedure AtualizarGradeProduto;
    procedure CriarFrameGradeProduto;
  public
    { Public declarations }
  protected
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uCor, uTamanho;

procedure TCadGradeProduto.AtualizarGradeProduto;
begin
  AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('GRADE_PRODUTO', 'ID'), 0));
end;

procedure TCadGradeProduto.cdsGradeProdutoCorAfterPost(DataSet: TDataSet);
begin
  inherited;
  if Assigned(FFrameGradeProdutoCor) then
  begin
    FFrameGradeProdutoCor.CarregarDadosCor();
  end;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if cdsGradeProdutoCorTamanho.Filtered then
  begin
    cdsGradeProdutoCorTamanho.Filtered := false;
  end;

  cdsGradeProdutoCorTamanho.Filter := 'id_cor = '+InttoStr(cdsGradeProdutoCorID_COR.AsInteger);
  cdsGradeProdutoCorTamanho.Filtered := true;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  cdsGradeProdutoCorTamanho.DeletarTodosRegistros;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if cdsDataID.AsInteger <= 0 then
  begin
    cdsData.ValidarCamposObrigatorios;
    cdsData.Commit;
    AtualizarGradeProduto;
    cdsData.Alterar;
  end;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorBeforePost(DataSet: TDataSet);
var
  idCor: Integer;
begin
  inherited;
  TCor.InserirCorCasoNaoExista(cdsGradeProdutoCorJOIN_DESCRICAO_COR.AsString, idCor);
  cdsGradeProdutoCorID_COR.AsInteger := idCor;
  cdsGradeProdutoCor.ValidarCamposObrigatorios;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorTamanhoBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if (cdsGradeProdutoCor.IsEmpty) then
  begin
    Abort;
  end;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorTamanhoBeforePost(DataSet: TDataSet);
var
  idTamanho: Integer;
begin
  inherited;
  TTamanho.InserirTamanhoCasoNaoExista(cdsGradeProdutoCorTamanhoJOIN_DESCRICAO_TAMANHO.AsString, idTamanho);
  cdsGradeProdutoCorTamanhoID_TAMANHO.AsInteger := idTamanho;
  cdsGradeProdutoCorTamanho.ValidarCamposObrigatorios;
end;

procedure TCadGradeProduto.cdsGradeProdutoCorTamanhoNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsGradeProdutoCorTamanhoID_COR.AsInteger :=
    cdsGradeProdutoCORID_COR.AsInteger;
end;

procedure TCadGradeProduto.CriarFrameGradeProduto;
begin
  FFrameGradeProdutoCor := TFrameGradeProduto.Create(PnFrameGradeProdutoCor);
  FFrameGradeProdutoCor.Parent := PnFrameGradeProdutoCor;
  FFrameGradeProdutoCor.Align := alClient;
  FFrameGradeProdutoCor.SetDatasets(cdsGradeProdutoCor, cdsGradeProdutoCorTamanho);
end;

procedure TCadGradeProduto.InstanciarFrames;
begin
  inherited;
  CriarFrameGradeProduto;
end;

initialization
  RegisterClass(TCadGradeProduto);

finalization
  UnRegisterClass(TCadGradeProduto);

end.
