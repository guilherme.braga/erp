inherited FrmApelidarColunasGrid: TFrmApelidarColunasGrid
  Caption = 'Renomear Colunas'
  ExplicitWidth = 664
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    object cxgdMain: TcxGrid
      Left = 2
      Top = 2
      Width = 654
      Height = 265
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object cxgdMainDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsColunas
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsCustomize.DataRowSizing = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        object cxgdMainDBTableView1Column1: TcxGridDBColumn
          Caption = 'Coluna'
          DataBinding.FieldName = 'name'
          Options.Editing = False
          Width = 216
        end
        object cxgdMainDBTableView1Column2: TcxGridDBColumn
          Caption = 'R'#243'tulo'
          DataBinding.FieldName = 'nickname'
          Width = 231
        end
        object cxgdMainDBTableView1Column3: TcxGridDBColumn
          Caption = 'Alinhamento'
          DataBinding.FieldName = 'alinhamento'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediatePost = True
          Properties.Items.Strings = (
            'Centro'
            'Direita'
            'Esquerda')
          Width = 84
        end
        object cxgdMainDBTableView1Column4: TcxGridDBColumn
          Caption = 'Ordena'#231#227'o'
          DataBinding.FieldName = 'ordenacao'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediatePost = True
          Properties.Items.Strings = (
            'Nenhuma'
            'Crescente'
            'Descrescente')
          Width = 109
        end
      end
      object cxgdMainLevel1: TcxGridLevel
        GridView = cxgdMainDBTableView1
      end
    end
  end
  object cdsColunas: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 300
    Top = 144
    object cdsColunasname: TStringField
      FieldName = 'name'
      Size = 80
    end
    object cdsColunasnickname: TStringField
      FieldName = 'nickname'
      Size = 100
    end
    object cdsColunasalinhamento: TStringField
      FieldName = 'alinhamento'
      Size = 10
    end
    object cdsColunasordenacao: TStringField
      FieldName = 'ordenacao'
    end
  end
  object dsColunas: TDataSource
    DataSet = cdsColunas
    Left = 272
    Top = 144
  end
end
