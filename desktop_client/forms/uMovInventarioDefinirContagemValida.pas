unit uMovInventarioDefinirContagemValida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  Vcl.StdCtrls, cxTextEdit, cxMaskEdit, cxDropDownEdit, System.Actions,
  Vcl.ActnList, cxButtons, cxGroupBox;

type
  TMovInventarioDefinirContagemValida = class(TFrmModalPadrao)
    cbConferencia: TcxComboBox;
    Label1: TLabel;
  private
    { Private declarations }
  public
    class function BuscarContagem: Integer;
  end;

var
  MovInventarioDefinirContagemValida: TMovInventarioDefinirContagemValida;

implementation

{$R *.dfm}

{ TMovInventarioDefinirContagemValida }

class function TMovInventarioDefinirContagemValida.BuscarContagem: Integer;
begin
  try
    Application.CreateForm(TMovInventarioDefinirContagemValida,
      MovInventarioDefinirContagemValida);
    MovInventarioDefinirContagemValida.ShowModal;
    if MovInventarioDefinirContagemValida.result = MCONFIRMED then
    begin
      Result :=
        MovInventarioDefinirContagemValida.cbConferencia.ItemIndex+1;
    end
    else
    begin
      Result := 0;
    end;
  finally
    FreeAndNil(MovInventarioDefinirContagemValida);
  end;
end;

end.
