inherited MovVendaVarejo: TMovVendaVarejo
  Caption = 'Venda Varejo'
  ClientHeight = 453
  ClientWidth = 1190
  PopupMenu = pmTela
  OnResize = FormResize
  ExplicitWidth = 1206
  ExplicitHeight = 492
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 1190
    PopupMenuItems = []
    ExplicitWidth = 1190
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'BarInformacoes'
        end
        item
          ToolbarName = 'barConsultar'
        end
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    ExplicitWidth = 1190
    ExplicitHeight = 351
    Height = 351
    Width = 1190
    object cxpcMain: TcxPageControl
      Left = 2
      Top = 2
      Width = 1186
      Height = 347
      Align = alClient
      Focusable = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = tsVenda
      Properties.CustomButtons.Buttons = <>
      Properties.NavigatorPosition = npLeftTop
      Properties.Style = 8
      ClientRectBottom = 347
      ClientRectRight = 1186
      ClientRectTop = 24
      object tsPesquisa: TcxTabSheet
        Caption = 'Pesquisa'
        ImageIndex = 2
        object cxGridPesquisaPadrao: TcxGrid
          Left = 0
          Top = 0
          Width = 1186
          Height = 300
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Images = DmAcesso.cxImage16x16
          ParentFont = False
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object viewPesquisa: TcxGridDBBandedTableView
            OnDblClick = viewPesquisaDblClick
            OnKeyDown = viewPesquisaKeyDown
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Images = DmAcesso.cxImage16x16
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsSearch
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            DataController.OnDataChanged = viewPesquisaDataControllerDataChanged
            Images = DmAcesso.cxImage16x16
            OptionsBehavior.NavigatorHints = True
            OptionsBehavior.ExpandMasterRowOnDblClick = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsCustomize.NestedBands = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.ShowColumnFilterButtons = sfbAlways
            OptionsView.BandHeaders = False
            Styles.ContentOdd = DmAcesso.OddColor
            Bands = <
              item
              end>
          end
          object cxGridPesquisaPadraoLevel1: TcxGridLevel
            GridView = viewPesquisa
          end
        end
        object PnTotalizadorConsulta: TFlowPanel
          Left = 0
          Top = 300
          Width = 1186
          Height = 23
          Align = alBottom
          AutoSize = True
          BevelOuter = bvNone
          DoubleBuffered = True
          ParentDoubleBuffered = False
          TabOrder = 1
          object PnTotalVenda: TgbLabel
            AlignWithMargins = True
            Left = 0
            Top = 1
            Margins.Left = 0
            Margins.Top = 1
            Margins.Right = 1
            Margins.Bottom = 1
            Align = alLeft
            AutoSize = False
            Caption = 'TOTALIZA R$ 999.999.999.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 21
            Width = 167
            AnchorY = 12
          end
          object PnTotalPedido: TgbLabel
            AlignWithMargins = True
            Left = 168
            Top = 1
            Margins.Left = 0
            Margins.Top = 1
            Margins.Right = 1
            Margins.Bottom = 1
            Align = alLeft
            AutoSize = False
            Caption = 'TOTALIZA R$ 999.999.999.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 21
            Width = 167
            AnchorY = 12
          end
          object PnTotalOrcamento: TgbLabel
            AlignWithMargins = True
            Left = 336
            Top = 1
            Margins.Left = 0
            Margins.Top = 1
            Margins.Right = 1
            Margins.Bottom = 1
            Align = alLeft
            AutoSize = False
            Caption = 'TOTALIZA R$ 999.999.999.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 21
            Width = 167
            AnchorY = 12
          end
          object PnTotalCondicional: TgbLabel
            AlignWithMargins = True
            Left = 504
            Top = 1
            Margins.Left = 0
            Margins.Top = 1
            Margins.Right = 1
            Margins.Bottom = 1
            Align = alLeft
            AutoSize = False
            Caption = 'TOTALIZA R$ 999.999.999.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 21
            Width = 167
            AnchorY = 12
          end
          object PnTotalDevolucao: TgbLabel
            AlignWithMargins = True
            Left = 672
            Top = 1
            Margins.Left = 0
            Margins.Top = 1
            Margins.Right = 1
            Margins.Bottom = 1
            Align = alLeft
            AutoSize = False
            Caption = 'TOTALIZA R$ 999.999.999.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 21
            Width = 167
            AnchorY = 12
          end
        end
      end
      object tsVenda: TcxTabSheet
        Caption = 'Venda'
        ImageIndex = 1
        object PnTopo: TgbPanel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -16
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 63
          Width = 1180
          object PnProduto: TgbPanel
            Left = 148
            Top = 2
            Align = alLeft
            Anchors = [akLeft, akTop, akRight, akBottom]
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            Height = 59
            Width = 574
            object EdtDescricaoProduto: TgbDBTextEdit
              Left = 2
              Top = 22
              TabStop = False
              Align = alClient
              DataBinding.DataField = 'PRODUTO'
              DataBinding.DataSource = dsProduto
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -16
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbReadyOnly = True
              gbPassword = False
              Width = 570
            end
            object cxLabel1: TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = 'Produto'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
          end
          object PnDesconto: TgbPanel
            Left = 949
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            Transparent = True
            Height = 59
            Width = 89
            object EdtPercentualDesconto: TgbDBTextEdit
              Left = 2
              Top = 22
              Align = alClient
              DataBinding.DataField = 'PERC_DESCONTO'
              DataBinding.DataSource = dsProduto
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -16
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              OnExit = EdtPercentualDescontoExit
              gbPassword = False
              Width = 85
            end
            object TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = '% Desconto'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
          end
          object PnCodigo: TgbPanel
            Left = 2
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            Height = 59
            Width = 146
            object cxLabel3: TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = 'C'#243'digo'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
            object EdtCodigoProduto: TgbDBButtonEditFK
              Left = 2
              Top = 22
              Align = alClient
              DataBinding.DataField = 'CODIGO'
              DataBinding.DataSource = dsProduto
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 112
              Style.Color = clWhite
              TabOrder = 1
              OnExit = EdtCodigoProdutoExit
              gbCampoPK = 'CODIGO_BARRA'
              gbCamposRetorno = 'ID_PRODUTO;PRODUTO;CODIGO'
              gbTableName = 'PRODUTO'
              gbCamposConsulta = 'ID;DESCRICAO;CODIGO_BARRA'
              gbDepoisDeConsultar = EdtCodigoProdutogbDepoisDeConsultar
              gbIdentificadorConsulta = 'PRODUTO'
              gbEnviaValorCampoParaConsulta = True
              gbUsarLikeConsulta = False
              Width = 142
            end
          end
          object PnValor: TgbPanel
            Left = 809
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Height = 59
            Width = 140
            object EdtValorUnitarioProduto: TgbDBTextEdit
              Left = 2
              Top = 22
              TabStop = False
              Align = alClient
              DataBinding.DataField = 'VALOR'
              DataBinding.DataSource = dsProduto
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -16
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              OnKeyDown = EdtValorUnitarioProdutoKeyDown
              gbReadyOnly = True
              gbPassword = False
              Width = 136
            end
            object cxLabel4: TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = 'Valor'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
          end
          object PnQuantidade: TgbPanel
            Left = 722
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Height = 59
            Width = 87
            object EdtQuantidadeProduto: TgbDBTextEdit
              Left = 2
              Top = 22
              Align = alClient
              DataBinding.DataField = 'QUANTIDADE'
              DataBinding.DataSource = dsProduto
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -16
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              OnEnter = EdtQuantidadeProdutoEnter
              gbPassword = False
              Width = 83
            end
            object TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = 'Quantidade'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
          end
          object PnTotal: TgbPanel
            Left = 1038
            Top = 2
            Align = alLeft
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            Transparent = True
            Height = 59
            Width = 140
            object EdtValorTotalProduto: TgbDBTextEdit
              Left = 2
              Top = 22
              TabStop = False
              Align = alClient
              DataBinding.DataField = 'TOTAL'
              DataBinding.DataSource = dsProduto
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -16
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbReadyOnly = True
              gbPassword = False
              Width = 136
            end
            object cxLabel2: TcxLabel
              Left = 2
              Top = 2
              Align = alTop
              Caption = 'Total'
              ParentFont = False
              Style.BorderStyle = ebsNone
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              Transparent = True
            end
          end
        end
        object PnCentro: TgbPanel
          AlignWithMargins = True
          Left = 3
          Top = 72
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 248
          Width = 1180
          object PnProdutos: TgbPanel
            Left = 2
            Top = 2
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            Height = 244
            Width = 1176
            object PnInformacoesLateraisDireita: TgbPanel
              Left = 848
              Top = 2
              Align = alRight
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              Height = 240
              Width = 326
              object PnValorBruto: TgbPanel
                Left = 2
                Top = 282
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 5
                Transparent = True
                Height = 56
                Width = 322
                object gbDBTextEdit13: TgbDBTextEdit
                  Left = 32
                  Top = 22
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'VL_TOTAL_PRODUTO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 288
                end
                object cxLabel7: TcxLabel
                  Left = 2
                  Top = 2
                  Align = alTop
                  Caption = 'Valor Bruto'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Transparent = True
                end
                object cxLabel11: TcxLabel
                  Left = 2
                  Top = 22
                  Align = alLeft
                  Caption = 'R$'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taCenter
                  Properties.Alignment.Vert = taVCenter
                  Transparent = True
                  AnchorX = 17
                  AnchorY = 38
                end
              end
              object PnTotalDesconto: TgbPanel
                Left = 2
                Top = 338
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 6
                Transparent = True
                Height = 56
                Width = 322
                object gbDBTextEdit12: TgbDBTextEdit
                  Left = 32
                  Top = 22
                  Align = alClient
                  DataBinding.DataField = 'VL_DESCONTO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  OnEnter = gbDBTextEdit12Enter
                  gbPassword = False
                  Width = 197
                end
                object cxLabel6: TcxLabel
                  Left = 2
                  Top = 2
                  Align = alTop
                  Caption = 'Desconto'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Transparent = True
                end
                object gbDBTextEdit4: TgbDBTextEdit
                  Left = 229
                  Top = 22
                  Align = alRight
                  DataBinding.DataField = 'PERC_DESCONTO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  OnEnter = gbDBTextEdit4Enter
                  gbPassword = False
                  Width = 64
                end
                object lbRSDesconto: TcxLabel
                  Left = 2
                  Top = 22
                  Align = alLeft
                  Caption = 'R$'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taCenter
                  Properties.Alignment.Vert = taVCenter
                  Transparent = True
                  AnchorX = 17
                  AnchorY = 38
                end
                object cxLabel10: TcxLabel
                  Left = 293
                  Top = 22
                  Align = alRight
                  Caption = '%'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taCenter
                  Properties.Alignment.Vert = taVCenter
                  Transparent = True
                  AnchorX = 307
                  AnchorY = 38
                end
              end
              object PnValorLiquido: TgbPanel
                Left = 2
                Top = 394
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 7
                Transparent = True
                Height = 56
                Width = 322
                object gbDBTextEdit11: TgbDBTextEdit
                  Left = 32
                  Top = 22
                  Align = alClient
                  DataBinding.DataField = 'VL_VENDA'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  OnEnter = gbDBTextEdit11Enter
                  gbPassword = False
                  Width = 288
                end
                object cxLabel5: TcxLabel
                  Left = 2
                  Top = 2
                  Align = alTop
                  Caption = 'Valor L'#237'quido'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Transparent = True
                end
                object cxLabel12: TcxLabel
                  Left = 2
                  Top = 22
                  Align = alLeft
                  Caption = 'R$'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taCenter
                  Properties.Alignment.Vert = taVCenter
                  Transparent = True
                  AnchorX = 17
                  AnchorY = 38
                end
              end
              object gbPanel2: TgbPanel
                Left = 2
                Top = 170
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Transparent = True
                Height = 56
                Width = 322
                object Label2: TLabel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Width = 312
                  Height = 16
                  Align = alTop
                  Caption = 'Tabela de Pre'#231'o'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 103
                end
                object gbDBTextEdit1: TgbDBTextEdit
                  Left = 60
                  Top = 24
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'JOIN_DESCRICAO_TABELA_PRECO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 260
                end
                object gbDBButtonEditFK2: TgbDBButtonEditFK
                  Left = 2
                  Top = 24
                  Align = alLeft
                  DataBinding.DataField = 'ID_TABELA_PRECO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit1
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_TABELA_PRECO;JOIN_DESCRICAO_TABELA_PRECO'
                  gbTableName = 'TABELA_PRECO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TMovTabelaPreco'
                  gbIdentificadorConsulta = 'TABELA_PRECO'
                  gbUsarLikeConsulta = False
                  Width = 58
                end
              end
              object gbPanel3: TgbPanel
                Left = 2
                Top = 114
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 56
                Width = 322
                object Label5: TLabel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Width = 312
                  Height = 16
                  Align = alTop
                  Caption = 'Vendedor'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 63
                end
                object gbDBTextEdit2: TgbDBTextEdit
                  Left = 60
                  Top = 24
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'JOIN_NOME_VENDEDOR'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 260
                end
                object EdtVendedor: TgbDBButtonEditFK
                  Left = 2
                  Top = 24
                  Align = alLeft
                  DataBinding.DataField = 'ID_VENDEDOR'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit2
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_VENDEDOR;JOIN_NOME_VENDEDOR'
                  gbTableName = 'PESSOA_USUARIO'
                  gbCamposConsulta = 'ID;NOME'
                  gbIdentificadorConsulta = 'PESSOA_USUARIO'
                  gbUsarLikeConsulta = False
                  Width = 58
                end
              end
              object PnPlanoPagamento: TgbPanel
                Left = 2
                Top = 226
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                Transparent = True
                Height = 56
                Width = 322
                object Label1: TLabel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Width = 312
                  Height = 16
                  Align = alTop
                  Caption = 'Plano de Pagamento'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 132
                end
                object gbDBTextEdit3: TgbDBTextEdit
                  Left = 60
                  Top = 24
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -16
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 260
                end
                object EdtPlanoPagamento: TgbDBButtonEditFK
                  Left = 2
                  Top = 24
                  Align = alLeft
                  DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -16
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit3
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO'
                  gbTableName = 'PLANO_PAGAMENTO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'PLANO_PAGAMENTO'
                  gbUsarLikeConsulta = False
                  Width = 58
                end
              end
              object gbPanel4: TgbPanel
                Left = 2
                Top = 58
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 56
                Width = 322
                object Label3: TLabel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Width = 312
                  Height = 16
                  Align = alTop
                  Caption = 'Cliente'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 44
                end
                object gbDBTextEdit5: TgbDBTextEdit
                  Left = 60
                  Top = 24
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'JOIN_NOME_PESSOA'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 260
                end
                object EdtCliente: TgbDBButtonEditFK
                  Left = 2
                  Top = 24
                  Align = alLeft
                  DataBinding.DataField = 'ID_PESSOA'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit5
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
                  gbTableName = 'PESSOA'
                  gbCamposConsulta = 'ID;NOME'
                  gbIdentificadorConsulta = 'PESSOA_MOV_VENDA_VAREJO'
                  gbUsarLikeConsulta = False
                  Width = 58
                end
              end
              object PnOperacao: TgbPanel
                Left = 2
                Top = 2
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 56
                Width = 322
                object lbOperacao: TLabel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Width = 312
                  Height = 16
                  Align = alTop
                  Caption = 'Opera'#231#227'o Comercial'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 128
                end
                object EdtDescricaoOperacao: TgbDBTextEdit
                  Left = 60
                  Top = 24
                  TabStop = False
                  Align = alClient
                  DataBinding.DataField = 'JOIN_DESCRICAO_OPERACAO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 260
                end
                object EdtCodigoOperacao: TgbDBButtonEditFK
                  Left = 2
                  Top = 24
                  Align = alLeft
                  DataBinding.DataField = 'ID_OPERACAO'
                  DataBinding.DataSource = dsVenda
                  ParentFont = False
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -19
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  gbTextEdit = EdtDescricaoOperacao
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_OPERACAO;JOIN_DESCRICAO_OPERACAO'
                  gbTableName = 'OPERACAO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadOperacao'
                  gbIdentificadorConsulta = 'OPERACAO'
                  gbUsarLikeConsulta = False
                  Width = 58
                end
              end
            end
            object pnProdutosEsquerda: TgbPanel
              Left = 2
              Top = 2
              Align = alClient
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Height = 240
              Width = 846
              object cxGridProdutos: TcxGrid
                Left = 2
                Top = 2
                Width = 842
                Height = 236
                Align = alClient
                BevelInner = bvNone
                BevelOuter = bvNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                LookAndFeel.Kind = lfOffice11
                LookAndFeel.NativeStyle = False
                object viewProduto: TcxGridDBBandedTableView
                  OnDblClick = cxGridProdutosViewDblClick
                  OnKeyDown = cxGridProdutosViewKeyDown
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = False
                  Navigator.Buttons.PriorPage.Visible = False
                  Navigator.Buttons.Prior.Visible = False
                  Navigator.Buttons.Next.Visible = False
                  Navigator.Buttons.NextPage.Visible = False
                  Navigator.Buttons.Last.Visible = False
                  Navigator.Buttons.Insert.Visible = False
                  Navigator.Buttons.Delete.Visible = False
                  Navigator.Buttons.Edit.Visible = False
                  Navigator.Buttons.Post.Visible = False
                  Navigator.Buttons.Cancel.Visible = False
                  Navigator.Buttons.Refresh.Visible = False
                  Navigator.Buttons.SaveBookmark.Visible = False
                  Navigator.Buttons.GotoBookmark.Visible = False
                  Navigator.Buttons.Filter.Visible = False
                  Navigator.InfoPanel.Visible = True
                  Navigator.Visible = True
                  FilterBox.CustomizeDialog = False
                  DataController.DataSource = dsVendaItem
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '###,###,###,##0.00'
                      Kind = skSum
                      FieldName = 'QUANTIDADE'
                      Column = viewProdutoQUANTIDADE
                    end
                    item
                      Format = '###,###,###,###,##0.00'
                      Kind = skSum
                      FieldName = 'VL_LIQUIDO'
                      Column = viewProdutoVL_LIQUIDO
                    end>
                  DataController.Summary.SummaryGroups = <>
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideSelection = True
                  OptionsSelection.InvertSelect = False
                  OptionsView.FocusRect = False
                  OptionsView.ColumnAutoWidth = True
                  OptionsView.Footer = True
                  OptionsView.GridLineColor = clInfoBk
                  OptionsView.GroupByBox = False
                  OptionsView.RowSeparatorColor = clBlack
                  OptionsView.BandHeaders = False
                  Styles.Background = cxStyleYellowStrong
                  Styles.Content = cxStyleYellowStrong
                  Styles.ContentEven = cxStyleYellowStrong
                  Styles.ContentOdd = cxStyleYellowStrong
                  Styles.Selection = cxStyleYellowTooStrong
                  Bands = <
                    item
                    end>
                  object viewProdutoID_PRODUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'ID_PRODUTO'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 62
                    Position.BandIndex = 0
                    Position.ColIndex = 0
                    Position.RowIndex = 0
                  end
                  object viewProdutoNR_ITEM: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'NR_ITEM'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 1
                    Position.RowIndex = 0
                  end
                  object viewProdutoQUANTIDADE: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'QUANTIDADE'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 101
                    Position.BandIndex = 0
                    Position.ColIndex = 3
                    Position.RowIndex = 0
                  end
                  object viewProdutoVL_DESCONTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'VL_DESCONTO'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 89
                    Position.BandIndex = 0
                    Position.ColIndex = 5
                    Position.RowIndex = 0
                  end
                  object viewProdutoVL_ACRESCIMO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'VL_ACRESCIMO'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 6
                    Position.RowIndex = 0
                  end
                  object viewProdutoVL_BRUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'VL_BRUTO'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 88
                    Position.BandIndex = 0
                    Position.ColIndex = 4
                    Position.RowIndex = 0
                  end
                  object viewProdutoVL_LIQUIDO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'VL_LIQUIDO'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 157
                    Position.BandIndex = 0
                    Position.ColIndex = 7
                    Position.RowIndex = 0
                  end
                  object viewProdutoPERC_DESCONTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'PERC_DESCONTO'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 8
                    Position.RowIndex = 0
                  end
                  object viewProdutoPERC_ACRESCIMO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'PERC_ACRESCIMO'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 9
                    Position.RowIndex = 0
                  end
                  object viewProdutoJOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'DESCRICAO_PRODUTO'
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 194
                    Position.BandIndex = 0
                    Position.ColIndex = 2
                    Position.RowIndex = 0
                  end
                  object viewProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 10
                    Position.RowIndex = 0
                  end
                  object viewProdutoJOIN_ESTOQUE_PRODUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'JOIN_ESTOQUE_PRODUTO'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Position.BandIndex = 0
                    Position.ColIndex = 11
                    Position.RowIndex = 0
                  end
                  object viewProdutoJOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn
                    DataBinding.FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
                    Visible = False
                    Options.Editing = False
                    Options.Sorting = False
                    Width = 144
                    Position.BandIndex = 0
                    Position.ColIndex = 12
                    Position.RowIndex = 0
                  end
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = viewProduto
                end
              end
            end
          end
        end
      end
      object tsFechamento: TcxTabSheet
        Caption = 'Fechamento'
        ImageIndex = 1
        object PnParcela: TgbPanel
          Left = 0
          Top = 0
          Align = alClient
          Alignment = alCenterCenter
          Caption = 'FrameVendaVarejoFechamentoCrediario'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 323
          Width = 1186
        end
      end
      object tsReceitaOtica: TcxTabSheet
        Caption = 'Receitas '#211'ticas'
        ImageIndex = 3
        object PnFrameVendaVarejoReceitaOtica: TgbPanel
          Left = 0
          Top = 0
          Align = alClient
          Alignment = alCenterCenter
          Caption = 'TFrameVendaVarejoReceitaOtica'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 323
          Width = 1186
        end
      end
      object tsDevolucaoCondicional: TcxTabSheet
        Caption = 'Devolu'#231#227'o de Produtos da Condicional'
        ImageIndex = 4
        object gbPanel12: TgbPanel
          Left = 0
          Top = 19
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 304
          Width = 1186
          object gridDevolucaoCondicional: TcxGrid
            Left = 2
            Top = 71
            Width = 1182
            Height = 231
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            object viewDevolucaoCondicional: TcxGridDBBandedTableView
              OnKeyDown = viewDevolucaoCondicionalKeyDown
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.First.Visible = False
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.Prior.Visible = False
              Navigator.Buttons.Next.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Last.Visible = False
              Navigator.Buttons.Insert.Visible = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Post.Visible = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Buttons.Filter.Visible = False
              Navigator.InfoPanel.Visible = True
              Navigator.Visible = True
              FilterBox.CustomizeDialog = False
              DataController.DataSource = dsVendaItemDevolucao
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '###,###,###,##0.00'
                  Kind = skSum
                  FieldName = 'QUANTIDADE'
                  Column = cxGridDBBandedColumn3
                end
                item
                  Format = '###,###,###,##0.00'
                  Kind = skSum
                  FieldName = 'VL_LIQUIDO'
                  Column = cxGridDBBandedColumn7
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideSelection = True
              OptionsSelection.InvertSelect = False
              OptionsView.FocusRect = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.Footer = True
              OptionsView.GridLineColor = clInfoBk
              OptionsView.GroupByBox = False
              OptionsView.RowSeparatorColor = clBlack
              OptionsView.BandHeaders = False
              Styles.Background = cxStyleYellowStrong
              Styles.Content = cxStyleYellowStrong
              Styles.ContentEven = cxStyleYellowStrong
              Styles.ContentOdd = cxStyleYellowStrong
              Styles.Selection = cxStyleYellowTooStrong
              Bands = <
                item
                end>
              object cxGridDBBandedColumn1: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID_PRODUTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 62
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn2: TcxGridDBBandedColumn
                DataBinding.FieldName = 'NR_ITEM'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn3: TcxGridDBBandedColumn
                DataBinding.FieldName = 'QUANTIDADE'
                Options.Editing = False
                Options.Sorting = False
                Width = 101
                Position.BandIndex = 0
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn4: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_DESCONTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 89
                Position.BandIndex = 0
                Position.ColIndex = 5
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn5: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_ACRESCIMO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 6
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn6: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_BRUTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 88
                Position.BandIndex = 0
                Position.ColIndex = 4
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn7: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_LIQUIDO'
                Options.Editing = False
                Options.Sorting = False
                Width = 157
                Position.BandIndex = 0
                Position.ColIndex = 7
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn8: TcxGridDBBandedColumn
                DataBinding.FieldName = 'PERC_DESCONTO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 8
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn9: TcxGridDBBandedColumn
                DataBinding.FieldName = 'PERC_ACRESCIMO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 9
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn10: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_DESCRICAO_PRODUTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 194
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn11: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 10
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn12: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_ESTOQUE_PRODUTO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Position.BandIndex = 0
                Position.ColIndex = 11
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn13: TcxGridDBBandedColumn
                DataBinding.FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
                Visible = False
                Options.Editing = False
                Options.Sorting = False
                Width = 144
                Position.BandIndex = 0
                Position.ColIndex = 12
                Position.RowIndex = 0
              end
            end
            object cxGridLevel2: TcxGridLevel
              GridView = viewDevolucaoCondicional
            end
          end
          object gbPanel5: TgbPanel
            AlignWithMargins = True
            Left = 5
            Top = 5
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            Height = 63
            Width = 1176
            object gbPanel6: TgbPanel
              Left = 148
              Top = 2
              Align = alClient
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Height = 59
              Width = 1026
              object gbDBTextEdit6: TgbDBTextEdit
                Left = 2
                Top = 22
                TabStop = False
                Align = alClient
                DataBinding.DataField = 'PRODUTO'
                DataBinding.DataSource = dsProduto
                ParentFont = False
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -16
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.LookAndFeel.Kind = lfOffice11
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                gbReadyOnly = True
                gbPassword = False
                Width = 1022
              end
              object cxLabel8: TcxLabel
                Left = 2
                Top = 2
                Align = alTop
                Caption = 'Produto'
                ParentFont = False
                Style.BorderStyle = ebsNone
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -13
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                Transparent = True
              end
            end
            object PnCodigoProdutoDevolucaoCondicional: TgbPanel
              Left = 2
              Top = 2
              Align = alLeft
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              Height = 59
              Width = 146
              object cxLabel9: TcxLabel
                Left = 2
                Top = 2
                Align = alTop
                Caption = 'C'#243'digo'
                ParentFont = False
                Style.BorderStyle = ebsNone
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -13
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                Transparent = True
              end
              object EdtCodigoProdutoDevolucaoCondicional: TcxDBButtonEdit
                Left = 2
                Top = 22
                AutoSize = False
                DataBinding.DataField = 'CODIGO'
                DataBinding.DataSource = dsProduto
                ParentFont = False
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -16
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.IsFontAssigned = True
                TabOrder = 1
                OnExit = EdtCodigoProdutoDevolucaoCondicionalExit
                Height = 35
                Width = 142
              end
            end
          end
        end
        object PnTituloDevolucaoCondicional: TgbPanel
          Left = 0
          Top = 0
          Align = alTop
          Alignment = alCenterCenter
          Caption = 'Devolu'#231#227'o de Produtos da Condicional'
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentBackground = False
          ParentFont = False
          Style.BorderStyle = ebsOffice11
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 19
          Width = 1186
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 212
    Top = 251
    inherited ActCancel: TAction
      Caption = 'Cancelar F11'
      Hint = 'Cancelar as altera'#231#245'es'
      ShortCut = 122
    end
    object ActEfetivarVenda: TAction [3]
      Category = 'Action'
      Caption = 'Efetivar F9'
      Hint = 'Encerrar a venda lan'#231'ada'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarVendaExecute
    end
    object ActVoltar: TAction [4]
      Category = 'Action'
      Caption = 'Voltar'
      Hint = 'Voltar para o lan'#231'amento dos produtos'
      ImageIndex = 18
      OnExecute = ActVoltarExecute
    end
    object ActPesquisar: TAction [5]
      Category = 'Action'
      Caption = 'Pesquisar F7'
      ImageIndex = 16
      ShortCut = 118
      OnExecute = ActPesquisarExecute
    end
    object ActVenda: TAction [6]
      Category = 'Action'
      Caption = 'Venda F6'
      Hint = 'Realizar uma venda'
      ImageIndex = 133
      ShortCut = 117
      OnExecute = ActVendaExecute
    end
    object ActDevolucao: TAction [7]
      Category = 'Action'
      Caption = 'Devolu'#231#227'o F8'
      Hint = 'Realizar uma devolu'#231#227'o'
      ImageIndex = 136
      ShortCut = 119
      OnExecute = ActDevolucaoExecute
    end
    object ActOrcamento: TAction [8]
      Category = 'Action'
      Caption = 'Or'#231'amento F3'
      Hint = 'Realizar um or'#231'amento'
      ImageIndex = 134
      OnExecute = ActOrcamentoExecute
    end
    object ActPedido: TAction [9]
      Category = 'Action'
      Caption = 'Pedido F2'
      Hint = 'Realizar um pedido'
      ImageIndex = 135
      ShortCut = 113
      OnExecute = ActPedidoExecute
    end
    object ActCondicional: TAction [10]
      Category = 'Action'
      Caption = 'Condicional F4'
      Hint = 'Realizar uma condicional'
      ImageIndex = 137
      ShortCut = 115
      OnExecute = ActCondicionalExecute
    end
    object ActCancelarVenda: TAction [11]
      Category = 'Action'
      Caption = 'Estornar F10'
      Hint = 'Estornar o documento'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = ActCancelarVendaExecute
    end
    object ActImprimir: TAction [12]
      Category = 'Action'
      Caption = 'Imprimir F12'
      Hint = 'Imprimir documento'
      ImageIndex = 115
      ShortCut = 123
      OnExecute = ActImprimirExecute
    end
    object ActExcluir: TAction [13]
      Category = 'Action'
      Caption = 'Excluir Ctrl+Del'
      Hint = 'Excluir documento'
      ImageIndex = 8
      ShortCut = 16430
      OnExecute = ActExcluirExecute
    end
    object ActReceitaOtica: TAction
      Category = 'Action'
      Caption = 'Receita '#211'tica'
      Hint = 'Vincular receitas '#243'ticas na venda'
      ImageIndex = 152
      OnExecute = ActReceitaOticaExecute
    end
    object ActNovaVenda: TAction
      Category = 'Action'
      Caption = 'Nova Venda'
      Hint = 'Iniciar nova venda'
      ImageIndex = 2
      OnExecute = ActNovaVendaExecute
    end
    object ActDevolucaoCondicional: TAction
      Category = 'Action'
      Caption = 'Devolu'#231#227'o de Condicional'
      Hint = 'Realizar a devolu'#231#227'o de produtos da condicional'
      ImageIndex = 136
      ShortCut = 16499
      OnExecute = ActDevolucaoCondicionalExecute
    end
    object ActLiberarBloqueios: TAction
      Category = 'Action'
      Caption = 'Liberar Bloqueios'
      Hint = 'Realiza a libera'#231#227'o dos bloqueios da venda'
      ImageIndex = 160
      OnExecute = ActLiberarBloqueiosExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Consultar'
      'Informacoes'
      'Personalizar')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    Left = 184
    Top = 251
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 1131
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedLeft = 75
      FloatClientWidth = 95
      FloatClientHeight = 378
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbPesquisar'
        end
        item
          Visible = True
          ItemName = 'lbVoltar'
        end
        item
          Visible = True
          ItemName = 'lbNovaVenda'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbReceitaOtica'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbPedido'
        end
        item
          Visible = True
          ItemName = 'lbOrcamento'
        end
        item
          Visible = True
          ItemName = 'lbCondicional'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'lbFechamentoVenda'
        end
        item
          Visible = True
          ItemName = 'lbDevolucao'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbCancelarDocumento'
        end
        item
          Visible = True
          ItemName = 'lbLiberarBloqueioPersonalizado'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbCancelar'
        end
        item
          Visible = True
          ItemName = 'lbExcluir'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbImpressao'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 1043
    end
    object barConsultar: TdxBar [3]
      Caption = 'Consultar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 75
      DockedTop = 0
      FloatLeft = 1259
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 102
          Visible = True
          ItemName = 'filtroCodigoVenda'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 129
          Visible = True
          ItemName = 'filtroPessoa'
        end
        item
          Visible = True
          ItemName = 'filtroNomePessoa'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataCadastro'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroInicial'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroFIm'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataDocumento'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoInicio'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroTipoDocumento'
        end
        item
          Visible = True
          ItemName = 'filtroSituacao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object BarInformacoes: TdxBar [4]
      Caption = 'Informa'#231#245'es'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 974
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'informacoesCodigo'
        end
        item
          Visible = True
          ItemName = 'informacoesSituacao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bbInserirValoresPadrao: TdxBarButton [8]
      Action = ActConfigurarValoresDefault
      Category = 0
    end
    object lbVoltar: TdxBarLargeButton [9]
      Action = ActVoltar
      Category = 1
    end
    object lbFechamentoVenda: TdxBarLargeButton [10]
      Action = ActVenda
      Category = 1
    end
    object lbEfetivar: TdxBarLargeButton [11]
      Action = ActEfetivarVenda
      Category = 1
    end
    object lbDevolucao: TdxBarLargeButton [12]
      Action = ActDevolucao
      Category = 1
    end
    object lbOrcamento: TdxBarLargeButton [13]
      Action = ActOrcamento
      Category = 1
    end
    object lbPedido: TdxBarLargeButton [14]
      Action = ActPedido
      Category = 1
    end
    object lbCondicional: TdxBarLargeButton [15]
      Action = ActCondicional
      Category = 1
    end
    object lbPesquisar: TdxBarLargeButton [16]
      Action = ActPesquisar
      Category = 1
    end
    object lbCancelarDocumento: TdxBarLargeButton [17]
      Action = ActCancelarVenda
      Category = 1
    end
    object lbImpressao: TdxBarLargeButton [18]
      Action = ActImprimir
      Category = 1
    end
    object lbCancelar: TdxBarLargeButton [19]
      Action = ActCancel
      Category = 1
    end
    object lbExcluir: TdxBarLargeButton [20]
      Action = ActExcluir
      Category = 1
    end
    object lbReceitaOtica: TdxBarLargeButton [23]
      Action = ActReceitaOtica
      Category = 1
    end
    object lbNovaVenda: TdxBarLargeButton [24]
      Action = ActNovaVenda
      Category = 1
    end
    object dxBarLargeButton3: TdxBarLargeButton [26]
      Action = ActDevolucaoCondicional
      Category = 1
    end
    object lbLiberarBloqueioPersonalizado: TdxBarLargeButton [27]
      Action = ActLiberarBloqueios
      Category = 1
    end
    object filtroLabelDataCadastro: TdxBarStatic [28]
      Align = iaCenter
      Caption = 'Data de Cadastro'
      Category = 2
      Hint = 'Data de Cadastro'
      Visible = ivAlways
    end
    object filtroDataCadastroInicial: TcxBarEditItem [29]
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataCadastroFIm: TcxBarEditItem [30]
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroLabelDataDocumento: TdxBarStatic [31]
      Align = iaCenter
      Caption = 'Data de Efetiva'#231#227'o'
      Category = 2
      Hint = 'Data de Efetiva'#231#227'o'
      Visible = ivAlways
    end
    object filtroDataDocumentoInicio: TcxBarEditItem [32]
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataDocumentoFim: TcxBarEditItem [33]
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroTipoDocumento: TdxBarCombo [34]
      Caption = 'Documento'
      Category = 2
      Hint = 'Documento'
      Visible = ivAlways
      Items.Strings = (
        'CONDICIONAL'
        'DEVOLU'#199#195'O'
        'PEDIDO'
        'OR'#199'AMENTO'
        'VENDA'
        'TODOS')
      ItemIndex = -1
    end
    object filtroSituacao: TdxBarCombo [35]
      Caption = 'Situa'#231#227'o      '
      Category = 2
      Hint = 'Situa'#231#227'o      '
      Visible = ivAlways
      Items.Strings = (
        'ABERTO'
        'FECHADO'
        'CANCELADO')
      ItemIndex = -1
    end
    object filtroPessoa: TcxBarEditItem [36]
      Caption = 'Pessoa'
      Category = 2
      Hint = 'Pessoa'
      Visible = ivAlways
      OnExit = filtroPessoaPropertiesEditValueChanged
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = filtroPessoaPropertiesButtonClick
    end
    object filtroCodigoVenda: TcxBarEditItem [37]
      Caption = 'Documento'
      Category = 2
      Hint = 'Documento'
      Visible = ivAlways
      OnExit = filtroCodigoVendaExit
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.AssignedValues.DisplayFormat = True
      Properties.DecimalPlaces = 0
      Properties.MaxLength = 8
      Properties.MaxValue = 99999999.000000000000000000
    end
    object filtroNomePessoa: TdxBarStatic [38]
      Caption = 'Nome da Pessoa 40 Caracteres'
      Category = 2
      Hint = '123456789 123456789 123456789 123456789'
      Visible = ivAlways
      Alignment = taLeftJustify
    end
    inherited siPersonalizar: TdxBarSubItem
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bbInserirValoresPadrao'
        end
        item
          Visible = True
          ItemName = 'bbParametrosFormulario'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbHelpImplantacao'
        end>
    end
    object informacoesCodigo: TdxBarStatic
      Align = iaCenter
      Caption = 'Venda: 000'
      Category = 3
      Hint = 'Venda: 000'
      Visible = ivAlways
    end
    object informacoesSituacao: TdxBarStatic
      Align = iaCenter
      Caption = 'ABERTA'
      Category = 3
      Hint = 'ABERTA'
      Visible = ivAlways
      BorderStyle = sbsEtched
    end
  end
  inherited alAcoes16x16: TActionList
    Left = 537
    Top = 274
  end
  object pmTela: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 448
    Top = 344
    object ConfigurarValoresPadres1: TMenuItem
      Action = ActConfigurarValoresDefault
    end
  end
  object PopUpControllerPesquisa: TcxGridPopupMenu
    Grid = cxGridPesquisaPadrao
    PopupMenus = <
      item
        GridView = viewPesquisa
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = viewPesquisa
        HitTypes = [gvhtNone]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
    Left = 204
    Top = 368
  end
  object JvEnterAsTab1: TJvEnterAsTab
    Left = 520
    Top = 336
  end
  object PopUpControllerFechamento: TcxGridPopupMenu
    PopupMenus = <
      item
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end>
    Left = 76
    Top = 368
  end
  object PopUpControllerProduto: TcxGridPopupMenu
    Grid = cxGridProdutos
    PopupMenus = <
      item
        GridView = viewProduto
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end>
    Left = 204
    Top = 312
  end
  object dsSearch: TDataSource
    DataSet = fdmSearch
    Left = 278
    Top = 320
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 334
    Top = 320
    object ActRestaurarColunasPadrao: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActAbrirConsultando: TAction
      Category = 'filter'
      Caption = 'Consultar Autom'#225'ticamente'
      Hint = 'Realiza a consulta de dados ao abrir o formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActAbrirConsultandoExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
    object ActFullExpand: TAction
      Category = 'filter'
      Caption = 'Expandir Grupos'
      Hint = 'Expandir/Contrair agrupamentos'
      ImageIndex = 13
    end
    object ActConfigurarValoresDefault: TAction
      Category = 'filter'
      Caption = 'Configurar Valores Padr'#245'es'
      Hint = 'Configurar valores que ser'#227'o preenchidos em um novo registro'
      ImageIndex = 13
      OnExecute = ActConfigurarValoresDefaultExecute
    end
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 148
    Top = 256
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ExibirAgrupamento2: TMenuItem
      Action = ActExibirAgrupamento
    end
    object RestaurarColunasPadrao: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
  object pmGridConsultaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmGridConsultaPadraoPopup
    Left = 120
    Top = 256
    object AlterarSQL1: TMenuItem
      Action = ActAlterarSQLPesquisaPadrao
    end
    object ExibirAgrupamento1: TMenuItem
      Action = ActAbrirConsultando
    end
  end
  object cdsVendaParcela: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsVendafdqVendaParcela
    Params = <>
    BeforeInsert = GerenciarSomenteLeitura
    BeforeEdit = GerenciarSomenteLeitura
    BeforePost = cdsVendaParcelaBeforePost
    AfterPost = cdsVendaParcelaAfterPost
    BeforeDelete = GerenciarSomenteLeitura
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 144
    Top = 320
    object cdsVendaParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaParcelaID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object cdsVendaParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsVendaParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsVendaParcelaNR_PARCELA: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Parcelas'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsVendaParcelaDT_VENCIMENTO: TDateField
      Alignment = taCenter
      DisplayLabel = 'Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
      OnChange = AlterandoDataParcelamento
    end
    object cdsVendaParcelaIC_DIAS: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Dias'
      FieldKind = fkInternalCalc
      FieldName = 'IC_DIAS'
      OnChange = AlterandoDiasParcelamento
    end
    object cdsVendaParcelaID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsVendaParcelaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object cdsVendaParcelaCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsVendaParcelaCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsVendaParcelaCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsVendaParcelaCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsVendaParcelaCHEQUE_BANCO: TStringField
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsVendaParcelaCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsVendaParcelaCHEQUE_SACADO: TStringField
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsVendaParcelaCHEQUE_DT_VENCIMENTO: TDateField
      DisplayLabel = 'Cheque: Dt. Vencimento'
      FieldName = 'CHEQUE_DT_VENCIMENTO'
      Origin = 'CHEQUE_DT_VENCIMENTO'
    end
    object cdsVendaParcelaOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object cdsVendaParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Tipo da Forma de Pagamento'
      FieldName = 'JOIN_TIPO_FORMA_PAGAMENTO'
      Origin = 'TIPO'
      ProviderFlags = []
      Size = 30
    end
    object cdsVendaParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField
      DisplayLabel = 'Operadora de Cart'#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERADORA_CARTAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
    end
    object cdsVendaParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsVendaItem: TDataSource
    DataSet = cdsVendaItem
    Left = 52
    Top = 320
  end
  object dsVendaParcela: TDataSource
    DataSet = cdsVendaParcela
    Left = 116
    Top = 320
  end
  object fdmSearch: TFDMemTable
    AfterOpen = fdmSearchAfterOpen
    AfterClose = fdmSearchAfterClose
    AfterDelete = fdmSearchAfterDelete
    FieldOptions.PositionMode = poFirst
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 250
    Top = 320
  end
  object dsProduto: TDataSource
    DataSet = fdmProduto
    Left = 272
    Top = 240
  end
  object cdsVenda: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspVenda'
    RemoteServer = DmConnection.dspVenda
    AfterOpen = cdsVendaAfterOpen
    AfterInsert = InserirValoresPadrao
    BeforeEdit = GerenciarSomenteLeitura
    BeforeDelete = GerenciarSomenteLeitura
    OnCalcFields = cdsVendaCalcFields
    OnNewRecord = cdsVendaNewRecord
    OnPostError = cdsVendaPostError
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 104
    Top = 264
    object cdsVendaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsVendaDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsVendaDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsVendaVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      OnChange = cdsVendaVL_DESCONTOChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_TOTAL_PRODUTO: TFMTBCDField
      DisplayLabel = 'Total dos Produtos'
      FieldName = 'VL_TOTAL_PRODUTO'
      Origin = 'VL_TOTAL_PRODUTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_VENDA: TFMTBCDField
      DisplayLabel = 'Valor L'#237'quido'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      OnChange = cdsVendaVL_VENDAChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object cdsVendaTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object cdsVendaID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
      OnChange = cdsVendaID_PESSOAChange
    end
    object cdsVendaID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsVendaID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object cdsVendaID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsVendaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object cdsVendaID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object cdsVendaID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      OnChange = GerarParcela
    end
    object cdsVendaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object cdsVendaID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
      Required = True
      OnChange = BuscarProdutosDaTabelaPreco
    end
    object cdsVendaPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      OnChange = cdsVendaPERC_DESCONTOChange
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_PAGAMENTO: TFMTBCDField
      DisplayLabel = 'Vl. Pagamento'
      FieldName = 'VL_PAGAMENTO'
      Origin = 'VL_PAGAMENTO'
      OnChange = GerarParcela
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Utilizado'
      FieldName = 'VL_PESSOA_CREDITO_UTILIZADO'
      Origin = 'VL_PESSOA_CREDITO_UTILIZADO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaJOIN_DESCRICAO_OPERACAO: TStringField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField
      DisplayLabel = 'Sequ'#234'ncia da Conta de An'#225'lise'
      FieldName = 'JOIN_SEQUENCIA_CONTA_ANALISE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object cdsVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 45
    end
    object cdsVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Dispon'#237'vel'
      FieldName = 'JOIN_VALOR_PESSOA_CREDITO'
      Origin = 'VL_CREDITO'
      ProviderFlags = []
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaID_VENDEDOR: TIntegerField
      DisplayLabel = 'C'#243'digo do Vendedor'
      FieldName = 'ID_VENDEDOR'
      Origin = 'ID_VENDEDOR'
    end
    object cdsVendaJOIN_NOME_VENDEDOR: TStringField
      DisplayLabel = 'Vendedor'
      FieldName = 'JOIN_NOME_VENDEDOR'
      Origin = 'JOIN_NOME_VENDEDOR'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaCC_SALDO_PESSOA_CREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_SALDO_PESSOA_CREDITO'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsVendaCC_VL_DIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_VL_DIFERENCA'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsVendaCC_VL_TROCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CC_VL_TROCO'
      DisplayFormat = '###,###,###,##0.00'
      Calculated = True
    end
    object cdsVendafdqVendaParcela: TDataSetField
      FieldName = 'fdqVendaParcela'
    end
    object cdsVendafdqVendaItem: TDataSetField
      FieldName = 'fdqVendaItem'
    end
    object cdsVendafdqVendaReceitaOtica: TDataSetField
      FieldName = 'fdqVendaReceitaOtica'
    end
    object cdsVendafdqVendaItemDevolucao: TDataSetField
      FieldName = 'fdqVendaItemDevolucao'
    end
    object cdsVendaID_NOTA_FISCAL_NFE: TIntegerField
      DisplayLabel = 'C'#243'digo da NFE'
      FieldName = 'ID_NOTA_FISCAL_NFE'
      Origin = 'ID_NOTA_FISCAL_NFE'
    end
    object cdsVendaID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object cdsVendaID_NOTA_FISCAL_NFSE: TIntegerField
      DisplayLabel = 'C'#243'digo da NFSE'
      FieldName = 'ID_NOTA_FISCAL_NFSE'
      Origin = 'ID_NOTA_FISCAL_NFSE'
    end
    object cdsVendaID_NOTA_FISCAL_NFCE: TIntegerField
      DisplayLabel = 'C'#243'digo da NFCE'
      FieldName = 'ID_NOTA_FISCAL_NFCE'
      Origin = 'ID_NOTA_FISCAL_NFCE'
    end
    object cdsVendafdqVendaBloqueioPersonalizado: TDataSetField
      FieldName = 'fdqVendaBloqueioPersonalizado'
    end
  end
  object dsVenda: TDataSource
    DataSet = cdsVenda
    Left = 76
    Top = 264
  end
  object cdsVendaItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsVendafdqVendaItem
    Params = <>
    BeforeInsert = GerenciarSomenteLeitura
    BeforeEdit = GerenciarSomenteLeitura
    AfterPost = cdsVendaItemAfterPost
    BeforeDelete = cdsVendaItemBeforeDelete
    AfterDelete = cdsVendaItemAfterDelete
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 80
    Top = 320
    object cdsVendaItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaItemID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaItemID_PRODUTO: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      ProviderFlags = [pfInWhere, pfInKey]
      Required = True
    end
    object cdsVendaItemNR_ITEM: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      ProviderFlags = [pfInWhere, pfInKey]
      Required = True
    end
    object cdsVendaItemQUANTIDADE: TFMTBCDField
      Alignment = taCenter
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object cdsVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField
      Alignment = taCenter
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
    object cdsVendaItemDESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO_PRODUTO'
      Size = 120
    end
  end
  object fdmProduto: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 296
    Top = 240
    object fdmProdutoCODIGO: TStringField
      FieldName = 'CODIGO'
      OnChange = BuscarDadosProdutoPorCodigoBarra
      Size = 255
    end
    object fdmProdutoPRODUTO: TStringField
      FieldName = 'PRODUTO'
      Size = 80
    end
    object fdmProdutoVALOR: TFloatField
      FieldName = 'VALOR'
      OnChange = AoAlterarQuantidade
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmProdutoQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
      OnChange = AoAlterarQuantidade
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmProdutoPERC_DESCONTO: TFloatField
      FieldName = 'PERC_DESCONTO'
      OnChange = CalcularDescontoPeloPercentual
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmProdutoTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmProdutoVL_DESCONTO: TFloatField
      FieldName = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
    end
    object fdmProdutoNR_ITEM: TIntegerField
      FieldName = 'NR_ITEM'
    end
    object fdmProdutoID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
    end
    object fdmProdutoVL_VENDA: TFloatField
      FieldName = 'VL_VENDA'
    end
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 324
    Top = 373
    PixelsPerInch = 96
    object cxStyleYellowStrong: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyleYellowTooStrong: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12713983
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBtnText
    end
  end
  object UCCadastro_Padrao: TUCControls
    GroupName = 'Venda Varejo'
    UserControl = FrmPrincipal.UserControl
    Components = ''
    Left = 376
    Top = 264
  end
  object cdsVendaReceitaOtica: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsVendafdqVendaReceitaOtica
    Params = <>
    AfterOpen = AtualizarContadorReceitaOtica
    BeforeInsert = GerenciarSomenteLeitura
    BeforeEdit = GerenciarSomenteLeitura
    AfterPost = AtualizarContadorReceitaOtica
    BeforeDelete = GerenciarSomenteLeitura
    AfterDelete = AtualizarContadorReceitaOtica
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 152
    Top = 368
    object cdsVendaReceitaOticaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaReceitaOticaID_RECEITA_OTICA: TIntegerField
      DisplayLabel = 'C'#243'digo da Receita '#211'tica'
      FieldName = 'ID_RECEITA_OTICA'
      Origin = 'ID_RECEITA_OTICA'
      Required = True
    end
    object cdsVendaReceitaOticaID_VENDA: TIntegerField
      DisplayLabel = 'C'#243'digo da Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object cdsVendaReceitaOticaJOIN_ID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'JOIN_ID_PESSOA'
      Origin = 'ID_PESSOA'
      ProviderFlags = []
    end
    object cdsVendaReceitaOticaJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'JOIN_NOME_PESSOA'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaReceitaOticaJOIN_ID_MEDICO: TIntegerField
      DisplayLabel = 'C'#243'digo do M'#233'dico'
      FieldName = 'JOIN_ID_MEDICO'
      Origin = 'ID_MEDICO'
      ProviderFlags = []
    end
    object cdsVendaReceitaOticaJOIN_NOME_MEDICO: TStringField
      DisplayLabel = 'M'#233'dico'
      FieldName = 'JOIN_NOME_MEDICO'
      Origin = 'JOIN_NOME_MEDICO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaReceitaOticaJOIN_ESPECIFICACAO_RECEITA_OTICA: TBlobField
      DisplayLabel = 'Especifica'#231#227'o'
      FieldName = 'JOIN_ESPECIFICACAO_RECEITA_OTICA'
      Origin = 'ESPECIFICACAO'
      ProviderFlags = []
    end
  end
  object cdsVendaItemDevolucao: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsVendafdqVendaItemDevolucao
    Params = <>
    BeforeInsert = GerenciarSomenteLeitura
    BeforeEdit = cdsVendaItemDevolucaoBeforeEdit
    AfterPost = cdsVendaItemDevolucaoAfterPost
    BeforeDelete = cdsVendaItemDevolucaoBeforeDelete
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 80
    Top = 424
    object cdsVendaItemDevolucaoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
    end
    object cdsVendaItemDevolucaoID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsVendaItemDevolucaoID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsVendaItemDevolucaoNR_ITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsVendaItemDevolucaoQUANTIDADE: TFMTBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaItemDevolucaoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object cdsVendaItemDevolucaoJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsVendaItemDevolucaoJOIN_CODIGO_BARRA_PRODUTO: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
  end
  object dsVendaItemDevolucao: TDataSource
    DataSet = cdsVendaItemDevolucao
    Left = 52
    Top = 424
  end
  object cdsVendaBloqueioPersonalizado: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsVendafdqVendaBloqueioPersonalizado
    Params = <>
    BeforeInsert = GerenciarSomenteLeitura
    BeforeEdit = cdsVendaItemDevolucaoBeforeEdit
    AfterPost = cdsVendaItemDevolucaoAfterPost
    BeforeDelete = cdsVendaItemDevolucaoBeforeDelete
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 168
    Top = 401
    object cdsVendaBloqueioPersonalizadoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaBloqueioPersonalizadoID_BLOQUEIO_PERSONALIZADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bloqueio Personalizado'
      FieldName = 'ID_BLOQUEIO_PERSONALIZADO'
      Origin = 'ID_BLOQUEIO_PERSONALIZADO'
      Required = True
    end
    object cdsVendaBloqueioPersonalizadoID_VENDA: TIntegerField
      DisplayLabel = 'C'#243'digo da Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object cdsVendaBloqueioPersonalizadoSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Size = 15
    end
    object cdsVendaBloqueioPersonalizadoDH_BLOQUEIO: TDateTimeField
      DisplayLabel = 'Dt. Hora do Bloqueio'
      FieldName = 'DH_BLOQUEIO'
      Origin = 'DH_BLOQUEIO'
    end
    object cdsVendaBloqueioPersonalizadoDH_LIBERACAO: TDateTimeField
      DisplayLabel = 'Dh. da Libera'#231#227'o'
      FieldName = 'DH_LIBERACAO'
      Origin = 'DH_LIBERACAO'
    end
    object cdsVendaBloqueioPersonalizadoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object cdsVendaBloqueioPersonalizadoJOIN_NOME_PESSOA_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaBloqueioPersonalizadoJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TStringField
      DisplayLabel = 'Bloqueio'
      FieldName = 'JOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
