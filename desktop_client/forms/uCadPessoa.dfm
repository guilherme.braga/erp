inherited CadPessoa: TCadPessoa
  Caption = 'Cadastro de Pessoa'
  ClientHeight = 712
  ClientWidth = 1049
  ExplicitWidth = 1065
  ExplicitHeight = 751
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1049
    ExplicitWidth = 1049
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1049
    Height = 585
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1049
    ExplicitHeight = 585
    ClientRectBottom = 585
    ClientRectRight = 1049
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1049
      ExplicitHeight = 561
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1017
        Height = 561
        ExplicitWidth = 1017
        ExplicitHeight = 561
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 561
        Height = 561
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 561
        ExplicitHeight = 561
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitTop = 0
      ExplicitWidth = 1049
      ExplicitHeight = 561
      inherited DesignPanel: TJvDesignPanel
        Width = 1049
        Height = 561
        ExplicitWidth = 1049
        ExplicitHeight = 561
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1049
          ExplicitHeight = 561
          Height = 561
          Width = 1049
          inherited dxBevel1: TdxBevel
            Width = 1045
            Height = 33
            ExplicitWidth = 927
            ExplicitHeight = 33
          end
          object lbDtCadastro: TLabel
            Left = 120
            Top = 9
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object lbCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 47
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edtDtCadastro: TgbDBDateEdit
            Left = 200
            Top = 6
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object cxPageControlDetails: TcxPageControl
            Left = 2
            Top = 35
            Width = 1045
            Height = 524
            Align = alClient
            Focusable = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            Properties.ActivePage = tsDadosGeral
            Properties.CustomButtons.Buttons = <>
            Properties.NavigatorPosition = npLeftTop
            Properties.Style = 8
            ClientRectBottom = 524
            ClientRectRight = 1045
            ClientRectTop = 24
            object tsDadosGeral: TcxTabSheet
              Caption = 'Geral'
              ImageIndex = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnPessoa: TcxGroupBox
                AlignWithMargins = True
                Left = 3
                Top = 3
                Align = alTop
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 0
                DesignSize = (
                  1039
                  494)
                Height = 494
                Width = 1039
                object dxBevel3: TdxBevel
                  Left = 2
                  Top = 5
                  Width = 1035
                  Height = 34
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitWidth = 917
                end
                object lTipoPessoa: TLabel
                  Left = 8
                  Top = 15
                  Width = 57
                  Height = 13
                  Caption = 'Tipo Pessoa'
                end
                object cbTipoPessoa: TgbDBComboBox
                  Left = 74
                  Top = 12
                  DataBinding.DataField = 'TP_PESSOA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.DropDownListStyle = lsFixedList
                  Properties.ImmediatePost = True
                  Properties.Items.Strings = (
                    'FISICA'
                    'JURIDICA')
                  Properties.ReadOnly = False
                  Properties.Sorted = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbRequired = True
                  Width = 121
                end
                object cbCliente: TgbDBCheckBox
                  Left = 209
                  Top = 13
                  Caption = 'Cliente?'
                  DataBinding.DataField = 'BO_CLIENTE'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Transparent = True
                  Width = 62
                end
                object cbFornecedor: TgbDBCheckBox
                  Left = 282
                  Top = 13
                  Caption = 'Fornecedor?'
                  DataBinding.DataField = 'BO_FORNECEDOR'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  Transparent = True
                  Width = 83
                end
                object cbTransportadora: TgbDBCheckBox
                  Left = 379
                  Top = 13
                  Caption = 'Transportadora?'
                  DataBinding.DataField = 'BO_TRANSPORTADORA'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  Transparent = True
                  Width = 103
                end
                object gbDBCheckBox7: TgbDBCheckBox
                  Left = 969
                  Top = 12
                  Anchors = [akTop, akRight]
                  Caption = 'Ativo?'
                  DataBinding.DataField = 'BO_ATIVO'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  Transparent = True
                  Width = 63
                end
                object pnInfoCliente: TgbPanel
                  Left = 2
                  Top = 39
                  Align = alTop
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  Transparent = True
                  DesignSize = (
                    1035
                    232)
                  Height = 232
                  Width = 1035
                  object dxBevel8: TdxBevel
                    Left = 2
                    Top = 2
                    Width = 1031
                    Height = 22
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitWidth = 995
                  end
                  object dxBevel2: TdxBevel
                    Left = 2
                    Top = 24
                    Width = 1031
                    Height = 82
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitLeft = 3
                    ExplicitTop = 28
                    ExplicitWidth = 995
                  end
                  object dxBevel4: TdxBevel
                    Left = 2
                    Top = 106
                    Width = 1031
                    Height = 103
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitWidth = 1063
                  end
                  object lbNomeCliente: TLabel
                    Left = 8
                    Top = 31
                    Width = 27
                    Height = 13
                    Caption = 'Nome'
                  end
                  object lbCPFCliente: TLabel
                    Left = 8
                    Top = 55
                    Width = 19
                    Height = 13
                    Caption = 'CPF'
                  end
                  object lbRG: TLabel
                    Left = 168
                    Top = 55
                    Width = 14
                    Height = 13
                    Caption = 'RG'
                  end
                  object lbGrupoPessoa: TLabel
                    Left = 516
                    Top = 31
                    Width = 29
                    Height = 13
                    Caption = 'Grupo'
                  end
                  object ldDtNascimento: TLabel
                    Left = 846
                    Top = 31
                    Width = 73
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Dt. Nascimento'
                    ExplicitLeft = 810
                  end
                  object lbOrgaoEmissor: TLabel
                    Left = 283
                    Top = 57
                    Width = 69
                    Height = 13
                    Caption = 'Org'#227'o Emissor'
                  end
                  object lbUFEmissor: TLabel
                    Left = 404
                    Top = 57
                    Width = 52
                    Height = 13
                    Caption = 'UF Emissor'
                  end
                  object lbNaturalidade: TLabel
                    Left = 516
                    Top = 57
                    Width = 61
                    Height = 13
                    Caption = 'Naturalidade'
                  end
                  object lbNomePai: TLabel
                    Left = 168
                    Top = 81
                    Width = 14
                    Height = 13
                    Caption = 'Pai'
                  end
                  object lbNomeMae: TLabel
                    Left = 516
                    Top = 81
                    Width = 20
                    Height = 13
                    Caption = 'M'#227'e'
                  end
                  object lbEmpresa: TLabel
                    Left = 8
                    Top = 111
                    Width = 41
                    Height = 13
                    Caption = 'Empresa'
                  end
                  object lbCNPJEmpresa: TLabel
                    Left = 507
                    Top = 112
                    Width = 25
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'CNPJ'
                    ExplicitLeft = 539
                  end
                  object lbDtAdmissao: TLabel
                    Left = 663
                    Top = 112
                    Width = 63
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Dt. Admiss'#227'o'
                    ExplicitLeft = 656
                  end
                  object lbRendaMensal: TLabel
                    Left = 841
                    Top = 112
                    Width = 67
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Renda Mensal'
                    ExplicitLeft = 834
                  end
                  object lbOcupacao: TLabel
                    Left = 8
                    Top = 135
                    Width = 48
                    Height = 13
                    Caption = 'Ocupa'#231#227'o'
                  end
                  object lbEnderecoEmpresa: TLabel
                    Left = 8
                    Top = 159
                    Width = 55
                    Height = 13
                    Caption = 'Logradouro'
                  end
                  object lbBairroEmpresa: TLabel
                    Left = 8
                    Top = 183
                    Width = 28
                    Height = 13
                    Caption = 'Bairro'
                  end
                  object lbCidadeEmpresa: TLabel
                    Left = 175
                    Top = 183
                    Width = 33
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Cidade'
                    ExplicitLeft = 207
                  end
                  object ldCEPEmpresa: TLabel
                    Left = 841
                    Top = 135
                    Width = 19
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'CEP'
                    ExplicitLeft = 873
                  end
                  object lbInformacoesCliente: TLabel
                    Left = 8
                    Top = 7
                    Width = 149
                    Height = 13
                    Caption = 'Informa'#231#245'es Pessoa F'#237'sica'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label18: TLabel
                    Left = 663
                    Top = 135
                    Width = 29
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Ramal'
                    ExplicitLeft = 695
                  end
                  object Label17: TLabel
                    Left = 507
                    Top = 135
                    Width = 42
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Telefone'
                    ExplicitLeft = 539
                  end
                  object Label2: TLabel
                    Left = 8
                    Top = 213
                    Width = 58
                    Height = 13
                    Caption = 'Observa'#231#227'o'
                  end
                  object lbEstadoCivil: TLabel
                    Left = 847
                    Top = 55
                    Width = 55
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Estado Civil'
                    ExplicitLeft = 811
                  end
                  object Label4: TLabel
                    Left = 841
                    Top = 159
                    Width = 37
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'N'#250'mero'
                    ExplicitLeft = 869
                  end
                  object Label5: TLabel
                    Left = 308
                    Top = 112
                    Width = 92
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Tempo de Trabalho'
                    ExplicitLeft = 336
                  end
                  object Label6: TLabel
                    Left = 545
                    Top = 183
                    Width = 72
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Outras Rendas'
                    ExplicitLeft = 573
                  end
                  object Label7: TLabel
                    Left = 841
                    Top = 183
                    Width = 60
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Renda Extra'
                    ExplicitLeft = 869
                  end
                  object Label8: TLabel
                    Left = 848
                    Top = 81
                    Width = 27
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Filhos'
                    ExplicitLeft = 880
                  end
                  object Label9: TLabel
                    Left = 841
                    Top = 214
                    Width = 42
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Vl. Limite'
                    ExplicitLeft = 873
                  end
                  object Label13: TLabel
                    Left = 8
                    Top = 81
                    Width = 51
                    Height = 13
                    Caption = 'Resid'#234'ncia'
                  end
                  object descGrupoPessoa: TgbDBTextEdit
                    Left = 637
                    Top = 28
                    TabStop = False
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_PESSOA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 2
                    gbReadyOnly = True
                    gbRequired = True
                    gbPassword = False
                    Width = 41
                  end
                  object edNome: TgbDBTextEdit
                    Left = 76
                    Top = 27
                    DataBinding.DataField = 'NOME'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = 14606074
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    gbRequired = True
                    gbPassword = False
                    Width = 432
                  end
                  object fkIdGrupoPessoa: TgbDBButtonEditFK
                    Left = 584
                    Top = 28
                    DataBinding.DataField = 'ID_GRUPO_PESSOA'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = clWhite
                    TabOrder = 1
                    gbTextEdit = descGrupoPessoa
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_GRUPO_PESSOA;JOIN_DESCRICAO_GRUPO_PESSOA'
                    gbTableName = 'GRUPO_PESSOA'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'GRUPO_PESSOA'
                    Width = 62
                  end
                  object edDocEstadual: TgbDBTextEdit
                    Left = 188
                    Top = 52
                    DataBinding.DataField = 'DOC_ESTADUAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 6
                    gbPassword = False
                    Width = 90
                  end
                  object deDtNascimento: TgbDBDateEdit
                    Left = 923
                    Top = 28
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DT_NASCIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 4
                    gbDateTime = False
                    Width = 106
                  end
                  object edDocFederal: TgbDBTextEdit
                    Left = 76
                    Top = 52
                    DataBinding.DataField = 'DOC_FEDERAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = 14606074
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 5
                    gbRequired = True
                    gbPassword = False
                    Width = 88
                  end
                  object edtOrgaoEmissor: TgbDBTextEdit
                    Left = 355
                    Top = 52
                    DataBinding.DataField = 'ORGAO_EMISSOR'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 7
                    gbPassword = False
                    Width = 45
                  end
                  object descNaturalidade: TgbDBTextEdit
                    Left = 643
                    Top = 52
                    TabStop = False
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 10
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 158
                  end
                  object edtNaturalidade: TgbDBButtonEditFK
                    Left = 584
                    Top = 52
                    DataBinding.DataField = 'ID_NATURALIDADE'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = clWhite
                    TabOrder = 9
                    gbTextEdit = descNaturalidade
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_NATURALIDADE;JOIN_DESCRICAO_CIDADE_NATURALIDADE'
                    gbTableName = 'CIDADE'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'CIDADE'
                    Width = 62
                  end
                  object edtNomePai: TgbDBTextEdit
                    Left = 188
                    Top = 77
                    DataBinding.DataField = 'NOME_PAI'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 14
                    gbPassword = False
                    Width = 320
                  end
                  object edtNomeMae: TgbDBTextEdit
                    Left = 584
                    Top = 77
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'NOME_MAE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 15
                    gbPassword = False
                    Width = 253
                  end
                  object edtEmpresa: TgbDBTextEdit
                    Left = 76
                    Top = 108
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 17
                    gbPassword = False
                    Width = 229
                  end
                  object EdtCNPJEmpresa: TgbDBTextEdit
                    Left = 552
                    Top = 108
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'CNPJ_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 19
                    gbPassword = False
                    Width = 106
                  end
                  object edtDtAdmissao: TgbDBDateEdit
                    Left = 730
                    Top = 109
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DT_ADMISSAO'
                    DataBinding.DataSource = dsData
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 20
                    gbDateTime = False
                    Width = 107
                  end
                  object edtRendaMensal: TgbDBTextEdit
                    Left = 912
                    Top = 109
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'RENDA_MENSAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 21
                    gbPassword = False
                    Width = 117
                  end
                  object edtEnderecoEmpresa: TgbDBTextEdit
                    Left = 76
                    Top = 155
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'ENDERECO_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 26
                    gbPassword = False
                    Width = 761
                  end
                  object edtBairro: TgbDBButtonEditFK
                    Left = 76
                    Top = 179
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'BAIRRO_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 112
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 28
                    gbCampoPK = 'DESCRICAO'
                    gbCamposRetorno = 'BAIRRO_EMPRESA'
                    gbTableName = 'BAIRRO'
                    gbCamposConsulta = 'DESCRICAO'
                    gbIdentificadorConsulta = 'BAIRRO'
                    Width = 94
                  end
                  object edtCidade: TgbDBButtonEditFK
                    Left = 212
                    Top = 179
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'ID_CIDADE_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = clWhite
                    TabOrder = 29
                    gbTextEdit = descCidadeEmpresa
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_CIDADE_EMPRESA;JOIN_DESCRICAO_CIDADE_EMPRESA;JOIN_UF_ESTADO'
                    gbTableName = 'CIDADE'
                    gbCamposConsulta = 'ID;DESCRICAO;UF'
                    gbIdentificadorConsulta = 'CIDADE'
                    Width = 62
                  end
                  object descCidadeEmpresa: TgbDBTextEdit
                    Left = 271
                    Top = 179
                    TabStop = False
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 30
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 234
                  end
                  object edtUF: TgbDBTextEdit
                    Left = 502
                    Top = 179
                    TabStop = False
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'JOIN_UF_ESTADO'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 31
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 40
                  end
                  object rgSexo: TgbDBRadioGroup
                    Left = 684
                    Top = 22
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'SEXO'
                    DataBinding.DataSource = dsData
                    ParentFont = False
                    Properties.Columns = 2
                    Properties.ImmediatePost = True
                    Properties.Items = <
                      item
                        Caption = 'Masculino'
                        Value = 'MASCULINO'
                      end
                      item
                        Caption = 'Feminino'
                        Value = 'FEMININO'
                      end>
                    Style.BorderStyle = ebsOffice11
                    Style.Font.Charset = DEFAULT_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Tahoma'
                    Style.Font.Style = []
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.IsFontAssigned = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 3
                    Height = 27
                    Width = 153
                  end
                  object edtRamal: TgbDBTextEdit
                    Left = 730
                    Top = 131
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'RAMAL_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 24
                    gbPassword = False
                    Width = 107
                  end
                  object edtTelefone: TgbDBTextEdit
                    Left = 552
                    Top = 131
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'TELEFONE_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 23
                    gbPassword = False
                    Width = 107
                  end
                  object gbDBButtonEditFK1: TgbDBButtonEditFK
                    Left = 460
                    Top = 52
                    DataBinding.DataField = 'JOIN_UF_ESTADO'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = clWhite
                    TabOrder = 8
                    gbCampoPK = 'UF'
                    gbCamposRetorno = 'ID_UF_EMISSOR;JOIN_UF_ESTADO'
                    gbTableName = 'ESTADO'
                    gbCamposConsulta = 'ID;UF'
                    gbIdentificadorConsulta = 'ESTADO'
                    Width = 43
                  end
                  object EdtOcupacao: TgbDBButtonEditFK
                    Left = 76
                    Top = 131
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'OCUPACAO'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = clWhite
                    TabOrder = 22
                    gbCampoPK = 'DESCRICAO'
                    gbCamposRetorno = 'OCUPACAO'
                    gbTableName = 'OCUPACAO'
                    gbCamposConsulta = 'DESCRICAO'
                    gbIdentificadorConsulta = 'OCUPACAO'
                    Width = 427
                  end
                  object edtDescUFNaturalidade: TgbDBTextEdit
                    Left = 798
                    Top = 52
                    TabStop = False
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'JOIN_UF_ESTADO_NATURALIDADE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 11
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 39
                  end
                  object gbDBBlobEdit1: TgbDBBlobEdit
                    Left = 76
                    Top = 210
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'OBSERVACAO'
                    DataBinding.DataSource = dsData
                    Properties.BlobEditKind = bekMemo
                    Properties.BlobPaintStyle = bpsText
                    Properties.ClearKey = 16430
                    Properties.ImmediatePost = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 34
                    Width = 761
                  end
                  object cbEstadoCivil: TgbDBComboBox
                    Left = 923
                    Top = 52
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'ESTADO_CIVIL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.DropDownListStyle = lsFixedList
                    Properties.ImmediatePost = True
                    Properties.ImmediateUpdateText = True
                    Properties.Items.Strings = (
                      'CASADO'
                      'DIVORCIADO'
                      'SEPARADO'
                      'SOLTEIRO'
                      'VIUVO')
                    Properties.ReadOnly = False
                    Properties.Sorted = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 12
                    Width = 106
                  end
                  object edtIDCEP: TgbDBButtonEditFK
                    Left = 912
                    Top = 131
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'CEP_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 112
                    Style.Color = clWhite
                    TabOrder = 25
                    gbCampoPK = 'CEP'
                    gbCamposRetorno = 'CEP_EMPRESA'
                    gbTableName = 'CEP'
                    gbCamposConsulta = 'CEP'
                    gbDepoisDeConsultar = edtIDCEPgbDepoisDeConsultar
                    gbIdentificadorConsulta = 'CEP'
                    Width = 106
                  end
                  object gbDBTextEdit1: TgbDBTextEdit
                    Left = 912
                    Top = 155
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'NUMERO_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 27
                    gbPassword = False
                    Width = 117
                  end
                  object gbDBTextEdit2: TgbDBTextEdit
                    Left = 404
                    Top = 108
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'TEMPO_TRABALHO_EMPRESA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 18
                    gbPassword = False
                    Width = 99
                  end
                  object gbDBTextEdit3: TgbDBTextEdit
                    Left = 621
                    Top = 179
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'RENDA_EXTRA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 32
                    gbPassword = False
                    Width = 216
                  end
                  object gbDBTextEdit4: TgbDBTextEdit
                    Left = 912
                    Top = 179
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'VL_RENDA_EXTRA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 33
                    gbPassword = False
                    Width = 117
                  end
                  object gbDBSpinEdit1: TgbDBSpinEdit
                    Left = 923
                    Top = 77
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'QUANTIDADE_FILHOS'
                    DataBinding.DataSource = dsData
                    Properties.MaxValue = 999.000000000000000000
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 16
                    Width = 106
                  end
                  object EdtLimiteCreditoPF: TgbDBTextEdit
                    Left = 912
                    Top = 210
                    Hint = 'Valor do limite de cr'#233'dito aprovado'
                    TabStop = False
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'VL_LIMITE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 35
                    OnDblClick = EdtLimiteCreditoPFDblClick
                    OnExit = EdtLimiteCreditoPFExit
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 117
                  end
                  object JvDBComboBox1: TJvDBComboBox
                    Left = 76
                    Top = 77
                    Width = 88
                    Height = 21
                    Items.Strings = (
                      'PROPRIA'
                      'ALUGADA'
                      'FAMILIAR/AMIGO')
                    TabOrder = 13
                    Values.Strings = (
                      'PROPRIA'
                      'ALUGADA'
                      'FAMILIAR')
                    ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                    ListSettings.OutfilteredValueFont.Color = clRed
                    ListSettings.OutfilteredValueFont.Height = -11
                    ListSettings.OutfilteredValueFont.Name = 'Tahoma'
                    ListSettings.OutfilteredValueFont.Style = []
                  end
                end
                object pnInfoEmpresa: TgbPanel
                  Left = 2
                  Top = 271
                  Align = alTop
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  Transparent = True
                  DesignSize = (
                    1035
                    112)
                  Height = 112
                  Width = 1035
                  object dxBevel10: TdxBevel
                    Left = 2
                    Top = 2
                    Width = 1031
                    Height = 22
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitWidth = 995
                  end
                  object dxBevel7: TdxBevel
                    Left = 2
                    Top = 24
                    Width = 1031
                    Height = 57
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitTop = 23
                    ExplicitWidth = 995
                  end
                  object lbInformacoesEmpresa: TLabel
                    Left = 8
                    Top = 7
                    Width = 162
                    Height = 13
                    Caption = 'Informa'#231#245'es Pessoa Jur'#237'dica'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object lbRazaoSocial: TLabel
                    Left = 8
                    Top = 31
                    Width = 60
                    Height = 13
                    Caption = 'Raz'#227'o Social'
                  end
                  object lbDtFundacao: TLabel
                    Left = 855
                    Top = 31
                    Width = 65
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Dt. Funda'#231#227'o'
                    ExplicitLeft = 819
                  end
                  object lbNomeFantasia: TLabel
                    Left = 404
                    Top = 31
                    Width = 71
                    Height = 13
                    Caption = 'Nome Fantasia'
                  end
                  object lbCNPJ: TLabel
                    Left = 344
                    Top = 55
                    Width = 25
                    Height = 13
                    Caption = 'CNPJ'
                  end
                  object lbInscricaoEstadual: TLabel
                    Left = 467
                    Top = 55
                    Width = 68
                    Height = 13
                    Caption = 'Insc. Estadual'
                  end
                  object lbGrupoPessoaPJ: TLabel
                    Left = 8
                    Top = 55
                    Width = 66
                    Height = 13
                    Caption = 'Grupo Pessoa'
                  end
                  object lbInscricaoMunicipal: TLabel
                    Left = 640
                    Top = 55
                    Width = 70
                    Height = 13
                    Caption = 'Insc. Municipal'
                  end
                  object lbSuframa: TLabel
                    Left = 836
                    Top = 55
                    Width = 48
                    Height = 13
                    Caption = 'SUFRAMA'
                  end
                  object lbObservacao: TLabel
                    Left = 6
                    Top = 85
                    Width = 58
                    Height = 13
                    Caption = 'Observa'#231#227'o'
                  end
                  object Label16: TLabel
                    Left = 841
                    Top = 86
                    Width = 42
                    Height = 13
                    Anchors = [akTop, akRight]
                    Caption = 'Vl. Limite'
                  end
                  object edRazaoSocial: TgbDBTextEdit
                    Left = 76
                    Top = 27
                    DataBinding.DataField = 'RAZAO_SOCIAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    gbPassword = False
                    Width = 323
                  end
                  object edtDtFundacao: TgbDBDateEdit
                    Left = 923
                    Top = 27
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'DT_NASCIMENTO'
                    DataBinding.DataSource = dsData
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 2
                    gbDateTime = False
                    Width = 107
                  end
                  object edtNomeFantasia: TgbDBTextEdit
                    Left = 476
                    Top = 27
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'NOME'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    gbPassword = False
                    Width = 364
                  end
                  object edtCNPJ: TgbDBTextEdit
                    Left = 371
                    Top = 52
                    DataBinding.DataField = 'DOC_FEDERAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 5
                    gbPassword = False
                    Width = 90
                  end
                  object edtInscricaoEstadual: TgbDBTextEdit
                    Left = 539
                    Top = 52
                    DataBinding.DataField = 'DOC_ESTADUAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 6
                    gbPassword = False
                    Width = 90
                  end
                  object edtGrupoPessoaPJ: TgbDBButtonEditFK
                    Left = 76
                    Top = 52
                    DataBinding.DataField = 'ID_GRUPO_PESSOA'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Style.Color = 14606074
                    TabOrder = 3
                    gbTextEdit = edtDescGrupoPessoaPJ
                    gbRequired = True
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_GRUPO_PESSOA;JOIN_DESCRICAO_GRUPO_PESSOA'
                    gbTableName = 'GRUPO_PESSOA'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'GRUPO_PESSOA'
                    Width = 62
                  end
                  object edtDescGrupoPessoaPJ: TgbDBTextEdit
                    Left = 137
                    Top = 52
                    TabStop = False
                    DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_PESSOA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 4
                    gbReadyOnly = True
                    gbRequired = True
                    gbPassword = False
                    Width = 201
                  end
                  object edtInscricaoMunicipal: TgbDBTextEdit
                    Left = 714
                    Top = 52
                    DataBinding.DataField = 'DOC_MUNICIPAL'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 7
                    gbPassword = False
                    Width = 90
                  end
                  object edtSuframa: TgbDBTextEdit
                    Left = 887
                    Top = 52
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'SUFRAMA'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 8
                    gbPassword = False
                    Width = 143
                  end
                  object edtObservacao: TgbDBBlobEdit
                    Left = 76
                    Top = 82
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'OBSERVACAO'
                    DataBinding.DataSource = dsData
                    Properties.BlobEditKind = bekMemo
                    Properties.BlobPaintStyle = bpsText
                    Properties.ClearKey = 16430
                    Properties.ImmediatePost = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 9
                    Width = 761
                  end
                  object EdtLimiteCreditoPJ: TgbDBTextEdit
                    Left = 913
                    Top = 82
                    Hint = 'Valor do limite de cr'#233'dito aprovado'
                    TabStop = False
                    Anchors = [akTop, akRight]
                    DataBinding.DataField = 'VL_LIMITE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 10
                    OnDblClick = EdtLimiteCreditoPFDblClick
                    OnExit = EdtLimiteCreditoPFExit
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 117
                  end
                end
                object pcContatoEndereco: TcxPageControl
                  Left = 2
                  Top = 383
                  Width = 1035
                  Height = 109
                  Align = alClient
                  Focusable = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                  TabStop = False
                  Properties.ActivePage = tsContato
                  Properties.CustomButtons.Buttons = <>
                  Properties.NavigatorPosition = npLeftTop
                  Properties.Style = 8
                  OnChange = cxpcMainChange
                  ClientRectBottom = 109
                  ClientRectRight = 1035
                  ClientRectTop = 24
                  object tsContato: TcxTabSheet
                    Caption = 'Contatos'
                    ImageIndex = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object PnFramePessoaContato: TgbPanel
                      Left = 0
                      Top = 0
                      Align = alClient
                      Alignment = alCenterCenter
                      Caption = 'TFramePessoaConta'
                      PanelStyle.Active = True
                      PanelStyle.OfficeBackgroundKind = pobkGradient
                      Style.BorderStyle = ebsNone
                      Style.LookAndFeel.Kind = lfOffice11
                      Style.Shadow = False
                      StyleDisabled.LookAndFeel.Kind = lfOffice11
                      StyleFocused.LookAndFeel.Kind = lfOffice11
                      StyleHot.LookAndFeel.Kind = lfOffice11
                      TabOrder = 0
                      Transparent = True
                      Height = 85
                      Width = 1035
                    end
                  end
                  object cxTabSheet4: TcxTabSheet
                    Caption = 'Endere'#231'os'
                    ImageIndex = 1
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object PnFramePessoaEndereco: TcxGroupBox
                      Left = 0
                      Top = 0
                      Align = alClient
                      Alignment = alCenterCenter
                      Caption = 'TFramePessoaEndereco'
                      PanelStyle.Active = True
                      Style.BorderStyle = ebsNone
                      Style.LookAndFeel.Kind = lfOffice11
                      Style.Shadow = False
                      Style.TransparentBorder = True
                      StyleDisabled.LookAndFeel.Kind = lfOffice11
                      StyleFocused.LookAndFeel.Kind = lfOffice11
                      StyleHot.LookAndFeel.Kind = lfOffice11
                      TabOrder = 0
                      Height = 85
                      Width = 1035
                    end
                  end
                end
              end
            end
            object tsConjuge: TcxTabSheet
              Caption = 'Conjug'#234
              ImageIndex = 2
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnInfoConjuge: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderColor = clWindowFrame
                Style.BorderStyle = ebsNone
                Style.Edges = [bLeft, bTop, bRight, bBottom]
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  1045
                  500)
                Height = 500
                Width = 1045
                object dxBevel5: TdxBevel
                  Left = 2
                  Top = 15
                  Width = 1041
                  Height = 9
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitWidth = 1005
                end
                object dxBevel13: TdxBevel
                  Left = 2
                  Top = 24
                  Width = 1041
                  Height = 58
                  Align = alTop
                  Shape = dxbsLineBottom
                  ExplicitTop = 23
                  ExplicitWidth = 1005
                end
                object dxBevel16: TdxBevel
                  Left = 2
                  Top = 82
                  Width = 1041
                  Height = 83
                  Align = alTop
                  Shape = dxbsLineBottom
                end
                object Label24: TLabel
                  Left = 8
                  Top = 30
                  Width = 27
                  Height = 13
                  Caption = 'Nome'
                end
                object Label25: TLabel
                  Left = 8
                  Top = 56
                  Width = 19
                  Height = 13
                  Caption = 'CPF'
                end
                object Label26: TLabel
                  Left = 140
                  Top = 56
                  Width = 14
                  Height = 13
                  Caption = 'RG'
                end
                object Label27: TLabel
                  Left = 264
                  Top = 56
                  Width = 69
                  Height = 13
                  Caption = 'Org'#227'o Emissor'
                end
                object Label28: TLabel
                  Left = 386
                  Top = 56
                  Width = 52
                  Height = 13
                  Caption = 'UF Emissor'
                end
                object Label30: TLabel
                  Left = 508
                  Top = 56
                  Width = 61
                  Height = 13
                  Caption = 'Naturalidade'
                end
                object Label31: TLabel
                  Left = 846
                  Top = 29
                  Width = 73
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Dt. Nascimento'
                  ExplicitLeft = 810
                end
                object lbEmpresaConjuge: TLabel
                  Left = 8
                  Top = 88
                  Width = 41
                  Height = 13
                  Caption = 'Empresa'
                end
                object lbCNPJEmpresaConjuge: TLabel
                  Left = 511
                  Top = 88
                  Width = 25
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'CNPJ'
                  ExplicitLeft = 567
                end
                object lbDtAdmissaoConjuge: TLabel
                  Left = 667
                  Top = 88
                  Width = 63
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Dt. Admiss'#227'o'
                end
                object lbRendaMensalConjuge: TLabel
                  Left = 847
                  Top = 88
                  Width = 67
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Renda Mensal'
                end
                object lbOcupacaoConjuge: TLabel
                  Left = 511
                  Top = 138
                  Width = 48
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Ocupa'#231#227'o'
                end
                object lbEnderecoEmpresaConjuge: TLabel
                  Left = 173
                  Top = 113
                  Width = 55
                  Height = 13
                  Caption = 'Logradouro'
                end
                object lbBairroEmpresaConjuge: TLabel
                  Left = 667
                  Top = 113
                  Width = 28
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Bairro'
                end
                object lbCidadeEmpresaConjuge: TLabel
                  Left = 8
                  Top = 138
                  Width = 33
                  Height = 13
                  Caption = 'Cidade'
                end
                object lbCEPEmpresaConjuge: TLabel
                  Left = 8
                  Top = 113
                  Width = 19
                  Height = 13
                  Caption = 'CEP'
                end
                object lbTelefoneEmpresaConjuge: TLabel
                  Left = 777
                  Top = 137
                  Width = 42
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Telefone'
                end
                object lbRamalEmpresaConjuge: TLabel
                  Left = 955
                  Top = 138
                  Width = 29
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'Ramal'
                end
                object Label1: TLabel
                  Left = 2
                  Top = 2
                  Width = 1041
                  Height = 13
                  Align = alTop
                  Caption = '  Informa'#231#245'es C'#244'njuge'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ExplicitWidth = 127
                end
                object Label11: TLabel
                  Left = 511
                  Top = 113
                  Width = 37
                  Height = 13
                  Anchors = [akTop, akRight]
                  Caption = 'N'#250'mero'
                end
                object gbDBTextEdit21: TgbDBTextEdit
                  Left = 39
                  Top = 26
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'NOME'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbPassword = False
                  Width = 646
                end
                object gbDBTextEdit22: TgbDBTextEdit
                  Left = 39
                  Top = 51
                  DataBinding.DataField = 'CPF'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  gbPassword = False
                  Width = 90
                end
                object gbDBRadioGroup2: TgbDBRadioGroup
                  Left = 691
                  Top = 20
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'SEXO'
                  DataBinding.DataSource = dsConjuge
                  ParentFont = False
                  Properties.Columns = 2
                  Properties.ImmediatePost = True
                  Properties.Items = <
                    item
                      Caption = 'Masculino'
                      Value = 'MASCULINO'
                    end
                    item
                      Caption = 'Feminino'
                      Value = 'FEMININO'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Height = 27
                  Width = 150
                end
                object gbDBTextEdit23: TgbDBTextEdit
                  Left = 158
                  Top = 51
                  DataBinding.DataField = 'RG'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbPassword = False
                  Width = 90
                end
                object gbDBTextEdit24: TgbDBTextEdit
                  Left = 337
                  Top = 51
                  DataBinding.DataField = 'ORGAO_EMISSOR'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbPassword = False
                  Width = 45
                end
                object edtIdNaturatildadeConjuge: TgbDBButtonEditFK
                  Left = 572
                  Top = 51
                  DataBinding.DataField = 'ID_NATURALIDADE'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 7
                  gbTextEdit = descNaturalidadeConjuge
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_NATURALIDADE;JOIN_DESCRICAO_CIDADE_NATURALIDADE'
                  gbTableName = 'CIDADE'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'CIDADE'
                  Width = 62
                end
                object descNaturalidadeConjuge: TgbDBTextEdit
                  Left = 633
                  Top = 51
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 357
                end
                object edtDtNascimentoConjuge: TgbDBDateEdit
                  Left = 923
                  Top = 26
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'DT_NASCIMENTO'
                  DataBinding.DataSource = dsConjuge
                  Properties.DateButtons = [btnClear, btnToday]
                  Properties.ImmediatePost = True
                  Properties.SaveTime = False
                  Properties.ShowTime = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  gbDateTime = False
                  Width = 107
                end
                object edtEmpresaConjuge: TgbDBTextEdit
                  Left = 58
                  Top = 84
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 9
                  gbPassword = False
                  Width = 446
                end
                object edtCNPJEmpresaConjuge: TgbDBTextEdit
                  Left = 565
                  Top = 84
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'CNPJ_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  gbPassword = False
                  Width = 100
                end
                object edtDtAdmissaoConjuge: TgbDBDateEdit
                  Left = 736
                  Top = 84
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'DT_ADMISSAO'
                  DataBinding.DataSource = dsConjuge
                  Properties.DateButtons = [btnClear, btnToday]
                  Properties.ImmediatePost = True
                  Properties.SaveTime = False
                  Properties.ShowTime = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 11
                  gbDateTime = False
                  Width = 107
                end
                object edtRendaMensalConjuge: TgbDBTextEdit
                  Left = 919
                  Top = 84
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'RENDA_MENSAL'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 12
                  gbPassword = False
                  Width = 112
                end
                object edtEnderecoEmpresaConjuge: TgbDBTextEdit
                  Left = 234
                  Top = 109
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'ENDERECO_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 13
                  gbPassword = False
                  Width = 270
                end
                object edtBairroEmpresaConjuge: TgbDBButtonEditFK
                  Left = 736
                  Top = 109
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'BAIRRO_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 15
                  gbCampoPK = 'DESCRICAO'
                  gbCamposRetorno = 'BAIRRO_EMPRESA'
                  gbTableName = 'BAIRRO'
                  gbCamposConsulta = 'DESCRICAO'
                  gbIdentificadorConsulta = 'BAIRRO'
                  Width = 295
                end
                object edtCidadeEmpresaConjuge: TgbDBButtonEditFK
                  Left = 58
                  Top = 134
                  DataBinding.DataField = 'ID_CIDADE_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 18
                  gbTextEdit = descCidadeEmpresaConjuge
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_CIDADE_EMPRESA;JOIN_DESCRICAO_CIDADE_EMPRESA'
                  gbTableName = 'CIDADE'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'CIDADE'
                  Width = 62
                end
                object descCidadeEmpresaConjuge: TgbDBTextEdit
                  Left = 117
                  Top = 134
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 19
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 356
                end
                object descUFEmpresaConjuge: TgbDBTextEdit
                  Left = 466
                  Top = 134
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'JOIN_UF_ESTADO'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 20
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 38
                end
                object edtCEPEmpresaConjuge: TgbDBButtonEditFK
                  Left = 58
                  Top = 109
                  DataBinding.DataField = 'CEP_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 112
                  Properties.EditMask = '00\.000\-999;0;'
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 17
                  gbCampoPK = 'CEP'
                  gbCamposRetorno = 'CEP_EMPRESA'
                  gbTableName = 'CEP'
                  gbCamposConsulta = 'CEP'
                  gbDepoisDeConsultar = edtCEPEmpresaConjugegbDepoisDeConsultar
                  gbIdentificadorConsulta = 'CEP'
                  Width = 111
                end
                object edtTelefoneEmpresaConjuge: TgbDBTextEdit
                  Left = 821
                  Top = 134
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'TELEFONE_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 21
                  gbPassword = False
                  Width = 121
                end
                object edtRamalEmpresaConjuge: TgbDBTextEdit
                  Left = 985
                  Top = 134
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'RAMAL_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 22
                  gbPassword = False
                  Width = 45
                end
                object gbDBButtonEditFK2: TgbDBButtonEditFK
                  Left = 442
                  Top = 51
                  DataBinding.DataField = 'JOIN_UF_ESTADO'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 6
                  gbCampoPK = 'UF'
                  gbCamposRetorno = 'ID_UF_EMISSOR;JOIN_UF_ESTADO'
                  gbTableName = 'ESTADO'
                  gbCamposConsulta = 'ID;UF'
                  gbIdentificadorConsulta = 'ESTADO'
                  Width = 62
                end
                object descUFNaturalidadeConjuge: TgbDBTextEdit
                  Left = 990
                  Top = 51
                  TabStop = False
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'DOC_ESTADUAL'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 23
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 40
                end
                object EdtOcupacaoConjuge: TgbDBButtonEditFK
                  Left = 565
                  Top = 134
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'OCUPACAO_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 16
                  gbCampoPK = 'DESCRICAO'
                  gbCamposRetorno = 'OCUPACAO_EMPRESA'
                  gbTableName = 'OCUPACAO'
                  gbCamposConsulta = 'DESCRICAO'
                  gbIdentificadorConsulta = 'OCUPACAO'
                  Width = 206
                end
                object gbDBTextEdit6: TgbDBTextEdit
                  Left = 565
                  Top = 109
                  Anchors = [akTop, akRight]
                  DataBinding.DataField = 'NUMERO_EMPRESA'
                  DataBinding.DataSource = dsConjuge
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clWhite
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 14
                  gbPassword = False
                  Width = 100
                end
              end
            end
            object tsInfoEmpresa: TcxTabSheet
              Caption = 'Classifica'#231#227'o Empresarial'
              ImageIndex = 3
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              DesignSize = (
                1045
                500)
              object dxBevel6: TdxBevel
                Left = 0
                Top = 0
                Width = 1045
                Height = 24
                Align = alTop
                Shape = dxbsLineBottom
                ExplicitWidth = 1009
              end
              object dxBevel9: TdxBevel
                Left = 0
                Top = 24
                Width = 1045
                Height = 100
                Align = alTop
                Shape = dxbsLineBottom
                ExplicitWidth = 1077
              end
              object Label3: TLabel
                Left = 8
                Top = 7
                Width = 72
                Height = 13
                Caption = 'Classifica'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object lbClassificacao: TLabel
                Left = 7
                Top = 51
                Width = 61
                Height = 13
                Caption = 'Classifica'#231#227'o'
              end
              object lbAreaAtuacao: TLabel
                Left = 7
                Top = 75
                Width = 81
                Height = 13
                Caption = #193'rea de Atua'#231#227'o'
              end
              object lbAtividadePrincipal: TLabel
                Left = 7
                Top = 99
                Width = 87
                Height = 13
                Caption = 'Atividade Principal'
              end
              object Label10: TLabel
                Left = 7
                Top = 27
                Width = 71
                Height = 13
                Caption = 'Dt. Associa'#231#227'o'
              end
              object edtIdClassificacaoEmpresa: TgbDBButtonEditFK
                Left = 96
                Top = 48
                DataBinding.DataField = 'ID_CLASSIFICACAO_EMPRESA'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 1
                gbTextEdit = descClassificacao
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_CLASSIFICACAO_EMPRESA;JOIN_DESCRICAO_CLASSIFICAO_EMPRESA'
                gbTableName = 'CLASSIFICACAO_EMPRESA'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'CLASSIFICACAO_EMPRESA'
                Width = 106
              end
              object descClassificacao: TgbDBTextEdit
                Left = 199
                Top = 48
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CLASSIFICAO_EMPRESA'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                gbReadyOnly = True
                gbPassword = False
                Width = 842
              end
              object edtIdAreaAtuacao: TgbDBButtonEditFK
                Left = 96
                Top = 72
                DataBinding.DataField = 'ID_AREA_ATUACAO'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 3
                gbTextEdit = descAreaAtuacao
                gbCampoPK = 'ID'
                gbCamposRetorno = 'ID_AREA_ATUACAO;JOIN_DESCRICAO_AREA_ATUACAO'
                gbTableName = 'AREA_ATUACAO'
                gbCamposConsulta = 'ID;DESCRICAO'
                gbIdentificadorConsulta = 'AREA_ATUACAO'
                Width = 106
              end
              object descAreaAtuacao: TgbDBTextEdit
                Left = 199
                Top = 72
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_AREA_ATUACAO'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                gbReadyOnly = True
                gbPassword = False
                Width = 842
              end
              object edtAtividadePrincipal: TgbDBButtonEditFK
                Left = 96
                Top = 96
                DataBinding.DataField = 'JOIN_SEQUENCIA_CNAE'
                DataBinding.DataSource = dsData
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.CharCase = ecUpperCase
                Properties.ClickKey = 112
                Style.Color = clWhite
                TabOrder = 5
                gbTextEdit = descAtividadePrincipal
                gbCampoPK = 'SEQUENCIA'
                gbCamposRetorno = 'ID_ATIVIDADE_PRINCIPAL;JOIN_DESCRICAO_CNAE;JOIN_SEQUENCIA_CNAE'
                gbTableName = 'CNAE'
                gbCamposConsulta = 'ID;DESCRICAO;SEQUENCIA'
                gbIdentificadorConsulta = 'CNAE'
                Width = 106
              end
              object descAtividadePrincipal: TgbDBTextEdit
                Left = 199
                Top = 96
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                DataBinding.DataField = 'JOIN_DESCRICAO_CNAE'
                DataBinding.DataSource = dsData
                Properties.CharCase = ecUpperCase
                Properties.ReadOnly = True
                Style.BorderStyle = ebsOffice11
                Style.Color = clGradientActiveCaption
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 6
                gbReadyOnly = True
                gbPassword = False
                Width = 842
              end
              object cxPageControl1: TcxPageControl
                Left = 0
                Top = 124
                Width = 1045
                Height = 376
                Align = alClient
                Focusable = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 7
                TabStop = False
                Properties.ActivePage = tsAtividadeSecundaria
                Properties.CustomButtons.Buttons = <>
                Properties.NavigatorPosition = npLeftTop
                Properties.Style = 8
                OnChange = cxpcMainChange
                ClientRectBottom = 376
                ClientRectRight = 1045
                ClientRectTop = 24
                object tsAtividadeSecundaria: TcxTabSheet
                  Caption = 'Atividades Secund'#225'rias'
                  ImageIndex = 2
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object PnFrameAtividadeSecundaria: TgbPanel
                    Left = 0
                    Top = 0
                    Align = alClient
                    Alignment = alCenterCenter
                    Caption = 'TFramePessoaAtividadeSecundaria'
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Transparent = True
                    Height = 352
                    Width = 1045
                  end
                end
                object cxTabSheet1: TcxTabSheet
                  Caption = 'Representantes'
                  ImageIndex = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object PnFrameRepresentantes: TcxGroupBox
                    Left = 0
                    Top = 0
                    Align = alClient
                    Alignment = alCenterCenter
                    Caption = 'TFramePessoaRepresentantes'
                    PanelStyle.Active = True
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    Style.TransparentBorder = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 352
                    Width = 1045
                  end
                end
                object cxTabSheet2: TcxTabSheet
                  Caption = 'Setores'
                  ImageIndex = 1
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object PnFrameSetor: TcxGroupBox
                    Left = 0
                    Top = 0
                    Align = alClient
                    Alignment = alCenterCenter
                    Caption = 'TFramePessoaSetor'
                    PanelStyle.Active = True
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    Style.TransparentBorder = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 352
                    Width = 1045
                  end
                end
              end
              object gbDBDateEdit1: TgbDBDateEdit
                Left = 96
                Top = 24
                DataBinding.DataField = 'DT_ASSOCIACAO'
                DataBinding.DataSource = dsData
                Properties.DateButtons = [btnClear, btnToday]
                Properties.ImmediatePost = True
                Properties.SaveTime = False
                Properties.ShowTime = False
                Style.BorderStyle = ebsOffice11
                Style.Color = clWhite
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                gbDateTime = False
                Width = 106
              end
            end
            object tsFiscal: TcxTabSheet
              Caption = 'Regime Fiscal'
              ImageIndex = 3
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnInformacoesRegimeFiscal: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  1045
                  113)
                Height = 113
                Width = 1045
                object Label12: TLabel
                  Left = 695
                  Top = 33
                  Width = 60
                  Height = 13
                  Caption = 'Zoneamento'
                end
                object Label15: TLabel
                  Left = 695
                  Top = 9
                  Width = 76
                  Height = 13
                  Caption = 'Regime Especial'
                end
                object gbDBButtonEditFK3: TgbDBButtonEditFK
                  Left = 777
                  Top = 29
                  DataBinding.DataField = 'ID_ZONEAMENTO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 5
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_ZONEAMENTO;JOIN_DESCRICAO_ZONEAMENTO'
                  gbTableName = 'ZONEAMENTO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'ZONEAMENTO'
                  Width = 62
                end
                object gbDBTextEdit7: TgbDBTextEdit
                  Left = 836
                  Top = 29
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_ZONEAMENTO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 6
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 200
                end
                object gbDBButtonEditFK4: TgbDBButtonEditFK
                  Left = 777
                  Top = 5
                  DataBinding.DataField = 'ID_REGIME_ESPECIAL'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Style.Color = clWhite
                  TabOrder = 3
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_REGIME_ESPECIAL;JOIN_DESCRICAO_REGIME_ESPECIAL'
                  gbTableName = 'REGIME_ESPECIAL'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbIdentificadorConsulta = 'REGIME_ESPECIAL'
                  Width = 62
                end
                object gbDBTextEdit8: TgbDBTextEdit
                  Left = 836
                  Top = 5
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_REGIME_ESPECIAL'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 200
                end
                object gbDBCheckBox1: TgbDBCheckBox
                  Left = 691
                  Top = 56
                  Caption = 'Contribuinte ICMS'
                  DataBinding.DataField = 'BO_CONTRIBUINTE_ICMS'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 7
                  Transparent = True
                  Width = 115
                end
                object gbDBCheckBox2: TgbDBCheckBox
                  Left = 691
                  Top = 83
                  Caption = 'Contribuinte IPI'
                  DataBinding.DataField = 'BO_CONTRIBUINTE_IPI'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 'S'
                  Properties.ValueUnchecked = 'N'
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  Transparent = True
                  Width = 102
                end
                object gbDBRadioGroup1: TgbDBRadioGroup
                  Left = 3
                  Top = 3
                  Caption = 'Tipo da Empresa do Destinat'#225'rio'
                  DataBinding.DataField = 'TIPO_EMPRESA'
                  DataBinding.DataSource = dsData
                  Properties.Items = <
                    item
                      Caption = 'Empresa Privada'
                      Value = 'PRIVADA'
                    end
                    item
                      Caption = 'Empresa P'#250'blica'
                      Value = 'PUBLICA'
                    end
                    item
                      Caption = 'Empresa Mista'
                      Value = 'MISTA'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Height = 101
                  Width = 193
                end
                object gbDBRadioGroup3: TgbDBRadioGroup
                  Left = 202
                  Top = 5
                  Caption = 'C'#243'digo do Regime Tribut'#225'rio (CRT)'
                  DataBinding.DataField = 'CRT'
                  DataBinding.DataSource = dsData
                  Properties.Items = <
                    item
                      Caption = 'Simples Nacional'
                      Value = '1'
                    end
                    item
                      Caption = 'Simples Nacional - Excesso de sublimite de receita bruta'
                      Value = '2'
                    end
                    item
                      Caption = 'Regime Normal'
                      Value = '3'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Height = 99
                  Width = 299
                end
                object gbDBRadioGroup4: TgbDBRadioGroup
                  Left = 507
                  Top = 5
                  Caption = 'Regime Tribut'#225'rio do Destinat'#225'rio'
                  DataBinding.DataField = 'REGIME_TRIBUTARIO'
                  DataBinding.DataSource = dsData
                  Properties.Items = <
                    item
                      Caption = 'RPA'
                      Value = '0'
                    end
                    item
                      Caption = 'Simples Nacional'
                      Value = '1'
                    end>
                  Style.BorderStyle = ebsOffice11
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 2
                  Height = 99
                  Width = 174
                end
              end
              object cxPageControl2: TcxPageControl
                Left = 0
                Top = 113
                Width = 1045
                Height = 387
                Align = alClient
                Focusable = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                TabStop = False
                Visible = False
                Properties.ActivePage = tsPessoaProdutoSituacaoEspecial
                Properties.CustomButtons.Buttons = <>
                Properties.NavigatorPosition = npLeftTop
                Properties.Style = 8
                OnChange = cxpcMainChange
                ClientRectBottom = 387
                ClientRectRight = 1045
                ClientRectTop = 24
                object tsPessoaProdutoSituacaoEspecial: TcxTabSheet
                  Caption = 'Situa'#231#227'o Especial'
                  ImageIndex = 1
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object PnFramePessoaProdutoSituacaoEspecial: TcxGroupBox
                    Left = 0
                    Top = 0
                    Align = alClient
                    Alignment = alCenterCenter
                    Caption = 'TFramePessoaProdutoSituacaoEspecial'
                    PanelStyle.Active = True
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    Style.TransparentBorder = True
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 363
                    Width = 1045
                  end
                end
              end
            end
            object tsObservacao: TcxTabSheet
              Caption = 'Observa'#231#227'o'
              ImageIndex = 4
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object gbDBMemo1: TgbDBMemo
                Left = 0
                Top = 0
                Align = alClient
                DataBinding.DataField = 'OBSERVACAO'
                DataBinding.DataSource = dsData
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 500
                Width = 1045
              end
            end
            object tsContaReceber: TcxTabSheet
              Caption = 'Contas a Receber'
              ImageIndex = 5
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameContaReceber: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameConsultaDadosDetalheContaReceber'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 500
                Width = 1045
              end
            end
            object tsContaPagar: TcxTabSheet
              Caption = 'Contas a Pagar'
              ImageIndex = 6
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameContaPagar: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameConsultaDadosDetalheContaPagar'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 500
                Width = 1045
              end
            end
            object tsVenda: TcxTabSheet
              Caption = 'Venda'
              ImageIndex = 7
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameVenda: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameConsultaDadosDetalheVenda'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 500
                Width = 1045
              end
            end
            object tsOrdemServico: TcxTabSheet
              Caption = 'Ordem Servi'#231'o'
              ImageIndex = 8
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFrameOrdemServico: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameConsultaDadosDetalheOrdemServico'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsOffice11
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 500
                Width = 1045
              end
            end
            object tsPessoaCredito: TcxTabSheet
              Caption = 'Cr'#233'dito'
              ImageIndex = 9
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object pnFramePessoaCreditoMovimento: TgbPanel
                Left = 0
                Top = 27
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameConsultaDadosDetalhePessoaCreditoMovimento'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 473
                Width = 1045
              end
              object pnResumoPessoaCredito: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 27
                Width = 1045
                object Label14: TLabel
                  Left = 6
                  Top = 11
                  Width = 157
                  Height = 13
                  Caption = 'Limite de Cr'#233'dito Dispon'#237'vel'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object gbDBCalcEdit1: TgbDBCalcEdit
                  Left = 169
                  Top = 7
                  TabStop = False
                  DataBinding.DataField = 'VL_CREDITO'
                  DataBinding.DataSource = dsPessoaCredito
                  Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                  Properties.ImmediatePost = True
                  Properties.ReadOnly = True
                  Properties.UseThousandSeparator = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.TextStyle = [fsBold]
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  gbReadyOnly = True
                  Width = 136
                end
              end
            end
            object tsRestricao: TcxTabSheet
              Caption = 'Restri'#231#227'o'
              ImageIndex = 10
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnFramePessoaRestricao: TgbPanel
                Left = 0
                Top = 0
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFramePessoaRestricao'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 500
                Width = 1045
              end
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspPessoa'
    RemoteServer = DmConnection.dspPessoa
    AfterOpen = cdsDataAfterOpen
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Dh. Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataDT_NASCIMENTO: TDateField
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object cdsDataTP_PESSOA: TStringField
      DefaultExpression = 'JURIDICA'
      DisplayLabel = 'Tp. Pessoa'
      FieldName = 'TP_PESSOA'
      Origin = 'TP_PESSOA'
      Required = True
      OnChange = cdsDataTP_PESSOAChange
      FixedChar = True
      Size = 10
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_CLIENTE: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Cliente?'
      FieldName = 'BO_CLIENTE'
      Origin = 'BO_CLIENTE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_FORNECEDOR: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Fornecedor?'
      FieldName = 'BO_FORNECEDOR'
      Origin = 'BO_FORNECEDOR'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_TRANSPORTADORA: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Transportadora?'
      FieldName = 'BO_TRANSPORTADORA'
      Origin = 'BO_TRANSPORTADORA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataDOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal'
      FieldName = 'DOC_FEDERAL'
      Origin = 'DOC_FEDERAL'
      OnChange = ValidarDocumentoFederal
      Size = 14
    end
    object cdsDataDOC_ESTADUAL: TStringField
      DisplayLabel = 'Doc. Estadual'
      FieldName = 'DOC_ESTADUAL'
      Origin = 'DOC_ESTADUAL'
      Size = 25
    end
    object cdsDataID_GRUPO_PESSOA: TIntegerField
      DisplayLabel = 'Gr. Pessoa'
      FieldName = 'ID_GRUPO_PESSOA'
      Origin = 'ID_GRUPO_PESSOA'
    end
    object cdsDataJOIN_DESCRICAO_GRUPO_PESSOA: TStringField
      DisplayLabel = 'Desc. Grupo Pessoa'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PESSOA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataRAZAO_SOCIAL: TStringField
      DisplayLabel = 'Raz'#227'o Social'
      FieldName = 'RAZAO_SOCIAL'
      Origin = 'RAZAO_SOCIAL'
      Size = 80
    end
    object cdsDataNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 80
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataDOC_MUNICIPAL: TStringField
      DisplayLabel = 'Inscri'#231#227'o Municipal'
      FieldName = 'DOC_MUNICIPAL'
      Origin = 'DOC_MUNICIPAL'
      Size = 45
    end
    object cdsDataNOME_PAI: TStringField
      DisplayLabel = 'Nome do Pai'
      FieldName = 'NOME_PAI'
      Origin = 'NOME_PAI'
      Size = 80
    end
    object cdsDataNOME_MAE: TStringField
      DisplayLabel = 'Nome da M'#227'e'
      FieldName = 'NOME_MAE'
      Origin = 'NOME_MAE'
      Size = 80
    end
    object cdsDataORGAO_EMISSOR: TStringField
      DisplayLabel = 'Org'#227'o Emissor'
      FieldName = 'ORGAO_EMISSOR'
      Origin = 'ORGAO_EMISSOR'
      Size = 25
    end
    object cdsDataID_UF_EMISSOR: TIntegerField
      DisplayLabel = 'C'#243'd. UF Emissor'
      FieldName = 'ID_UF_EMISSOR'
      Origin = 'ID_UF_EMISSOR'
    end
    object cdsDataSEXO: TStringField
      DisplayLabel = 'Sexo'
      FieldName = 'SEXO'
      Origin = 'SEXO'
      Size = 10
    end
    object cdsDataEMPRESA: TStringField
      DisplayLabel = 'Nome Empresa'
      FieldName = 'EMPRESA'
      Origin = 'EMPRESA'
      Size = 80
    end
    object cdsDataENDERECO_EMPRESA: TStringField
      DisplayLabel = 'Endere'#231'o Empresa'
      FieldName = 'ENDERECO_EMPRESA'
      Origin = 'ENDERECO_EMPRESA'
      Size = 80
    end
    object cdsDataID_CIDADE_EMPRESA: TIntegerField
      DisplayLabel = 'C'#243'd. Cidade Empresa'
      FieldName = 'ID_CIDADE_EMPRESA'
      Origin = 'ID_CIDADE_EMPRESA'
    end
    object cdsDataCEP_EMPRESA: TStringField
      DisplayLabel = 'CEP Empresa'
      FieldName = 'CEP_EMPRESA'
      Origin = 'CEP_EMPRESA'
      Size = 8
    end
    object cdsDataCNPJ_EMPRESA: TStringField
      DisplayLabel = 'CNPJ Empresa'
      FieldName = 'CNPJ_EMPRESA'
      Origin = 'CNPJ_EMPRESA'
      OnChange = ValidarDocumentoFederal
      Size = 14
    end
    object cdsDataBAIRRO_EMPRESA: TStringField
      DisplayLabel = 'Bairro Empresa'
      FieldName = 'BAIRRO_EMPRESA'
      Origin = 'BAIRRO_EMPRESA'
      Size = 45
    end
    object cdsDataNUMERO_EMPRESA: TIntegerField
      FieldName = 'NUMERO_EMPRESA'
      Origin = 'NUMERO_EMPRESA'
    end
    object cdsDataDT_ADMISSAO: TDateField
      DisplayLabel = 'Dt. Admiss'#227'o'
      FieldName = 'DT_ADMISSAO'
      Origin = 'DT_ADMISSAO'
    end
    object cdsDataTELEFONE_EMPRESA: TStringField
      DisplayLabel = 'Telefone Empresa'
      FieldName = 'TELEFONE_EMPRESA'
      Origin = 'TELEFONE_EMPRESA'
    end
    object cdsDataRAMAL_EMPRESA: TStringField
      DisplayLabel = 'Ramal Empresa'
      FieldName = 'RAMAL_EMPRESA'
      Origin = 'RAMAL_EMPRESA'
      Size = 10
    end
    object cdsDataRENDA_MENSAL: TFMTBCDField
      DisplayLabel = 'Renda Mensal'
      FieldName = 'RENDA_MENSAL'
      Origin = 'RENDA_MENSAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataSUFRAMA: TStringField
      FieldName = 'SUFRAMA'
      Origin = 'SUFRAMA'
      Size = 45
    end
    object cdsDataID_NATURALIDADE: TIntegerField
      DisplayLabel = 'C'#243'd. Naturalidade'
      FieldName = 'ID_NATURALIDADE'
      Origin = 'ID_NATURALIDADE'
    end
    object cdsDataESTADO_CIVIL: TStringField
      DisplayLabel = 'Estado Civil'
      FieldName = 'ESTADO_CIVIL'
      Origin = 'ESTADO_CIVIL'
      OnChange = cdsDataESTADO_CIVILChange
      Size = 10
    end
    object cdsDataJOIN_UF_ESTADO: TStringField
      DisplayLabel = 'UF Estado'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'UF'
      ProviderFlags = []
      Size = 2
    end
    object cdsDataJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField
      DisplayLabel = 'Descri'#231#227'o Cidade Naturalidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField
      DisplayLabel = 'Descri'#231#227'o Cidade Empresa'
      FieldName = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataID_AREA_ATUACAO: TIntegerField
      DisplayLabel = #193'rea de Atua'#231#227'o'
      FieldName = 'ID_AREA_ATUACAO'
      Origin = 'ID_AREA_ATUACAO'
    end
    object cdsDataID_CLASSIFICACAO_EMPRESA: TIntegerField
      DisplayLabel = 'Classifica'#231#227'o Empresa'
      FieldName = 'ID_CLASSIFICACAO_EMPRESA'
      Origin = 'ID_CLASSIFICACAO_EMPRESA'
    end
    object cdsDataID_ATIVIDADE_PRINCIPAL: TIntegerField
      DisplayLabel = 'Atividade Principal'
      FieldName = 'ID_ATIVIDADE_PRINCIPAL'
      Origin = 'ID_ATIVIDADE_PRINCIPAL'
    end
    object cdsDataTEMPO_TRABALHO_EMPRESA: TStringField
      DisplayLabel = 'Tempo de Trabalho'
      FieldName = 'TEMPO_TRABALHO_EMPRESA'
      Origin = 'TEMPO_TRABALHO_EMPRESA'
      Size = 100
    end
    object cdsDataQUANTIDADE_FILHOS: TIntegerField
      DisplayLabel = 'Filhos'
      FieldName = 'QUANTIDADE_FILHOS'
      Origin = 'QUANTIDADE_FILHOS'
    end
    object cdsDataTIPO_RESIDENCIA: TStringField
      DisplayLabel = 'Tempo de Resid'#234'ncia'
      FieldName = 'TIPO_RESIDENCIA'
      Origin = 'TIPO_RESIDENCIA'
      Size = 100
    end
    object cdsDataRENDA_EXTRA: TStringField
      DisplayLabel = 'Renda Extra'
      FieldName = 'RENDA_EXTRA'
      Origin = 'RENDA_EXTRA'
      Size = 255
    end
    object cdsDataVL_RENDA_EXTRA: TFMTBCDField
      DisplayLabel = 'Renda Extra'
      FieldName = 'VL_RENDA_EXTRA'
      Origin = 'VL_RENDA_EXTRA'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVL_LIMITE: TFMTBCDField
      DisplayLabel = 'Vl. Limite de Cr'#233'dito'
      FieldName = 'VL_LIMITE'
      Origin = 'VL_LIMITE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataOCUPACAO: TStringField
      DisplayLabel = 'Ocupa'#231#227'o'
      FieldName = 'OCUPACAO'
      Origin = 'OCUPACAO'
      Size = 255
    end
    object cdsDataDT_ASSOCIACAO: TDateField
      DisplayLabel = 'Dt. Associa'#231#227'o'
      FieldName = 'DT_ASSOCIACAO'
      Origin = 'DT_ASSOCIACAO'
    end
    object cdsDataID_ZONEAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Zoneamento'
      FieldName = 'ID_ZONEAMENTO'
      Origin = 'ID_ZONEAMENTO'
    end
    object cdsDataCRT: TIntegerField
      DisplayLabel = 'C'#243'digo do Regime Tribut'#225'rio'
      FieldName = 'CRT'
      Origin = 'CRT'
    end
    object cdsDataID_REGIME_ESPECIAL: TIntegerField
      DisplayLabel = 'C'#243'digo do Regime Especial'
      FieldName = 'ID_REGIME_ESPECIAL'
      Origin = 'ID_REGIME_ESPECIAL'
    end
    object cdsDataREGIME_TRIBUTARIO: TIntegerField
      DisplayLabel = 'Regime Tribut'#225'rio'
      FieldName = 'REGIME_TRIBUTARIO'
      Origin = 'REGIME_TRIBUTARIO'
    end
    object cdsDataBO_CONTRIBUINTE_ICMS: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Contribuinte ICMS'
      FieldName = 'BO_CONTRIBUINTE_ICMS'
      Origin = 'BO_CONTRIBUINTE_ICMS'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_CONTRIBUINTE_IPI: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Contribuinte IPI'
      FieldName = 'BO_CONTRIBUINTE_IPI'
      Origin = 'BO_CONTRIBUINTE_IPI'
      FixedChar = True
      Size = 1
    end
    object cdsDataBO_CONTRIBUINTE: TStringField
      DefaultExpression = 'N'
      DisplayLabel = 'Contribuinte'
      FieldName = 'BO_CONTRIBUINTE'
      Origin = 'BO_CONTRIBUINTE'
      FixedChar = True
      Size = 1
    end
    object cdsDataJOIN_DESCRICAO_ZONEAMENTO: TStringField
      DisplayLabel = 'Zoneamento'
      FieldName = 'JOIN_DESCRICAO_ZONEAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_REGIME_ESPECIAL: TStringField
      DisplayLabel = 'Regime Especial'
      FieldName = 'JOIN_DESCRICAO_REGIME_ESPECIAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CLASSIFICAO_EMPRESA: TStringField
      DisplayLabel = 'Desc. Classifica'#231#227'o Empresa'
      FieldName = 'JOIN_DESCRICAO_CLASSIFICAO_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_SEQUENCIA_CNAE: TStringField
      DisplayLabel = 'Sequ'#234'ncia CNAE'
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object cdsDataJOIN_DESCRICAO_CNAE: TStringField
      DisplayLabel = 'Desc. Atividade Principal'
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_UF_ESTADO_NATURALIDADE: TStringField
      DisplayLabel = 'UF Naturalidade'
      FieldName = 'JOIN_UF_ESTADO_NATURALIDADE'
      Origin = 'JOIN_UF_ESTADO_NATURALIDADE'
      ProviderFlags = []
      ReadOnly = True
      Size = 2
    end
    object cdsDataJOIN_DESCRICAO_AREA_ATUACAO: TStringField
      DisplayLabel = #193'rea de Atua'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_AREA_ATUACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataTIPO_EMPRESA: TStringField
      DisplayLabel = 'Tipo da Empresa'
      FieldName = 'TIPO_EMPRESA'
      Origin = 'TIPO_EMPRESA'
      Size = 15
    end
    object cdsDatafdqPessoaContato: TDataSetField
      FieldName = 'fdqPessoaContato'
    end
    object cdsDatafdqPessoaEndereco: TDataSetField
      FieldName = 'fdqPessoaEndereco'
    end
    object cdsDatafdqPessoaSetor: TDataSetField
      FieldName = 'fdqPessoaSetor'
    end
    object cdsDatafdqPessoaRepresentante: TDataSetField
      FieldName = 'fdqPessoaRepresentante'
    end
    object cdsDatafdqPessoaAtividadeSecundaria: TDataSetField
      FieldName = 'fdqPessoaAtividadeSecundaria'
    end
    object cdsDatafdqPessoaConjuge: TDataSetField
      FieldName = 'fdqPessoaConjuge'
    end
    object cdsDatafdqPessoaCredito: TDataSetField
      FieldName = 'fdqPessoaCredito'
    end
    object cdsDatafdqPessoaRestricao: TDataSetField
      FieldName = 'fdqPessoaRestricao'
    end
  end
  inherited pmGridConsultaPadrao: TPopupMenu
    Left = 544
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42216.040628923610000000
      BuiltInReportLink = True
    end
  end
  object cdsPessoaSetor: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaSetor
    Params = <>
    AfterOpen = cdsPessoaSetorAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 800
    Top = 82
    object cdsPessoaSetorID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaSetorQUANTIDADE_FUNCIONARIOS: TIntegerField
      DisplayLabel = 'Qtde. Funcion'#225'rios'
      FieldName = 'QUANTIDADE_FUNCIONARIOS'
      Origin = 'QUANTIDADE_FUNCIONARIOS'
    end
    object cdsPessoaSetorID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaSetorID_SETOR_COMERCIAL: TIntegerField
      DisplayLabel = 'C'#243'digo do Setor Comercial'
      FieldName = 'ID_SETOR_COMERCIAL'
      Origin = 'ID_SETOR_COMERCIAL'
      Required = True
    end
    object cdsPessoaSetorJOIN_DESCRICAO_SETOR_COMERCIAL: TStringField
      DisplayLabel = 'Setor Comercial'
      FieldName = 'JOIN_DESCRICAO_SETOR_COMERCIAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object cdsPessoaAtividadeSecundaria: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaAtividadeSecundaria
    Params = <>
    AfterOpen = cdsPessoaAtividadeSecundariaAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 848
    Top = 79
    object cdsPessoaAtividadeSecundariaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaAtividadeSecundariaID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaAtividadeSecundariaID_CNAE: TIntegerField
      DisplayLabel = 'C'#243'd. CNAE'
      FieldName = 'ID_CNAE'
      Origin = 'ID_CNAE'
      Required = True
    end
    object cdsPessoaAtividadeSecundariaJOIN_DESCRICAO_CNAE: TStringField
      DisplayLabel = 'Descri'#231#227'o CNAE'
      FieldName = 'JOIN_DESCRICAO_CNAE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsPessoaAtividadeSecundariaJOIN_SEQUENCIA_CNAE: TStringField
      DisplayLabel = 'Sequ'#234'ncia CNAE'
      FieldName = 'JOIN_SEQUENCIA_CNAE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
  end
  object cdsPessoaEndereco: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaEndereco
    Params = <>
    AfterOpen = cdsPessoaEnderecoAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 749
    Top = 80
    object cdsPessoaEnderecoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaEnderecoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 35
    end
    object cdsPessoaEnderecoLOGRADOURO: TStringField
      DisplayLabel = 'Logradouro'
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      Required = True
      Size = 100
    end
    object cdsPessoaEnderecoCOMPLEMENTO: TStringField
      DisplayLabel = 'Complemento'
      FieldName = 'COMPLEMENTO'
      Origin = 'COMPLEMENTO'
      Size = 50
    end
    object cdsPessoaEnderecoOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsPessoaEnderecoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaEnderecoNUMERO: TStringField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object cdsPessoaEnderecoCEP: TStringField
      FieldName = 'CEP'
      Origin = 'CEP'
      Size = 15
    end
    object cdsPessoaEnderecoBAIRRO: TStringField
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      Size = 100
    end
    object cdsPessoaEnderecoID_CIDADE: TIntegerField
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
    end
    object cdsPessoaEnderecoJOIN_DESCRICAO_CIDADE: TStringField
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object cdsPessoaConjuge: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaConjuge
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 893
    Top = 30
    object cdsPessoaConjugeID: TAutoIncField
      DisplayLabel = 'C'#243'd. Conjug'#234
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaConjugeID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaConjugeNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 80
    end
    object cdsPessoaConjugeCPF: TStringField
      FieldName = 'CPF'
      Origin = 'CPF'
      OnChange = ValidarDocumentoFederal
      Size = 11
    end
    object cdsPessoaConjugeSEXO: TStringField
      DisplayLabel = 'Sexo'
      FieldName = 'SEXO'
      Origin = 'SEXO'
    end
    object cdsPessoaConjugeRG: TStringField
      FieldName = 'RG'
      Origin = 'RG'
      Size = 25
    end
    object cdsPessoaConjugeORGAO_EMISSOR: TStringField
      DisplayLabel = 'Org'#227'o Emissor'
      FieldName = 'ORGAO_EMISSOR'
      Origin = 'ORGAO_EMISSOR'
      Size = 3
    end
    object cdsPessoaConjugeID_UF_EMISSOR: TIntegerField
      DisplayLabel = 'C'#243'd. UF Emissor'
      FieldName = 'ID_UF_EMISSOR'
      Origin = 'ID_UF_EMISSOR'
    end
    object cdsPessoaConjugeDT_NASCIMENTO: TDateField
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object cdsPessoaConjugeEMPRESA: TStringField
      DisplayLabel = 'Nome Empresa'
      FieldName = 'EMPRESA'
      Origin = 'EMPRESA'
      Size = 80
    end
    object cdsPessoaConjugeENDERECO_EMPRESA: TStringField
      DisplayLabel = 'Endere'#231'o Empresa'
      FieldName = 'ENDERECO_EMPRESA'
      Origin = 'ENDERECO_EMPRESA'
      Size = 80
    end
    object cdsPessoaConjugeCEP_EMPRESA: TStringField
      DisplayLabel = 'CEP Empresa'
      FieldName = 'CEP_EMPRESA'
      Origin = 'CEP_EMPRESA'
      EditMask = '00\.000\-999;0;'
      Size = 8
    end
    object cdsPessoaConjugeID_CIDADE_EMPRESA: TIntegerField
      DisplayLabel = 'C'#243'd. Cidade Empresa'
      FieldName = 'ID_CIDADE_EMPRESA'
      Origin = 'ID_CIDADE_EMPRESA'
    end
    object cdsPessoaConjugeCNPJ_EMPRESA: TStringField
      DisplayLabel = 'CNPJ Empresa'
      FieldName = 'CNPJ_EMPRESA'
      Origin = 'CNPJ_EMPRESA'
      OnChange = ValidarDocumentoFederal
      Size = 14
    end
    object cdsPessoaConjugeBAIRRO_EMPRESA: TStringField
      DisplayLabel = 'Bairro Empresa'
      FieldName = 'BAIRRO_EMPRESA'
      Origin = 'BAIRRO_EMPRESA'
      Size = 45
    end
    object cdsPessoaConjugeDT_ADMISSAO: TDateField
      DisplayLabel = 'Dt. Admiss'#227'o'
      FieldName = 'DT_ADMISSAO'
      Origin = 'DT_ADMISSAO'
    end
    object cdsPessoaConjugeRENDA_MENSAL: TFMTBCDField
      DisplayLabel = 'Renda Mensal'
      FieldName = 'RENDA_MENSAL'
      Origin = 'RENDA_MENSAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsPessoaConjugeTELEFONE_EMPRESA: TStringField
      DisplayLabel = 'Telefone Empresa'
      FieldName = 'TELEFONE_EMPRESA'
      Origin = 'TELEFONE_EMPRESA'
    end
    object cdsPessoaConjugeRAMAL_EMPRESA: TStringField
      DisplayLabel = 'Ramal Empresa'
      FieldName = 'RAMAL_EMPRESA'
      Origin = 'RAMAL_EMPRESA'
      Size = 10
    end
    object cdsPessoaConjugeID_NATURALIDADE: TIntegerField
      DisplayLabel = 'C'#243'd. Naturalidade'
      FieldName = 'ID_NATURALIDADE'
      Origin = 'ID_NATURALIDADE'
    end
    object cdsPessoaConjugeJOIN_UF_ESTADO: TStringField
      DisplayLabel = 'UF Estado'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'UF'
      ProviderFlags = []
      Size = 2
    end
    object cdsPessoaConjugeJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField
      DisplayLabel = 'Desc. Cidade Naturalidade'
      FieldName = 'JOIN_DESCRICAO_CIDADE_NATURALIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsPessoaConjugeJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField
      DisplayLabel = 'Desc. Cidade Empresa'
      FieldName = 'JOIN_DESCRICAO_CIDADE_EMPRESA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsPessoaConjugeNUMERO_EMPRESA: TIntegerField
      FieldName = 'NUMERO_EMPRESA'
      Origin = 'NUMERO_EMPRESA'
    end
    object cdsPessoaConjugeTEMPO_TRABALHO: TStringField
      DisplayLabel = 'Tempo de Trabalho'
      FieldName = 'TEMPO_TRABALHO'
      Origin = 'TEMPO_TRABALHO'
      Size = 100
    end
    object cdsPessoaConjugeOCUPACAO_EMPRESA: TStringField
      DisplayLabel = 'Ocupa'#231#227'o Empresa'
      FieldName = 'OCUPACAO_EMPRESA'
      Origin = 'OCUPACAO_EMPRESA'
      Size = 255
    end
  end
  object dsConjuge: TDataSource
    DataSet = cdsPessoaConjuge
    Left = 944
    Top = 31
  end
  object cdsPessoaRepresentante: TGBClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsPessoaRepresentanteAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 894
    Top = 79
    object cdsPessoaRepresentanteID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaRepresentanteNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 255
    end
    object cdsPessoaRepresentanteCPF: TStringField
      FieldName = 'CPF'
      Origin = 'CPF'
      OnChange = ValidarDocumentoFederal
      Size = 11
    end
    object cdsPessoaRepresentanteRG: TStringField
      FieldName = 'RG'
      Origin = 'RG'
      Size = 9
    end
    object cdsPessoaRepresentanteDT_NASCIMENTO: TDateField
      DisplayLabel = 'Dt. Nascimento'
      FieldName = 'DT_NASCIMENTO'
      Origin = 'DT_NASCIMENTO'
    end
    object cdsPessoaRepresentanteESTADO_CIVIL: TStringField
      DisplayLabel = 'Estado Civil'
      FieldName = 'ESTADO_CIVIL'
      Origin = 'ESTADO_CIVIL'
      Size = 10
    end
    object cdsPessoaRepresentanteID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'd. Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
  end
  object cdsPessoaContato: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaContato
    Params = <>
    AfterOpen = cdsPessoaContatoAfterOpen
    OnNewRecord = cdsPessoaContatoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 651
    Top = 80
    object cdsPessoaContatoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaContatoTIPO: TStringField
      DefaultExpression = 'TELEFONE'
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object cdsPessoaContatoCONTATO: TStringField
      DisplayLabel = 'Contato'
      FieldName = 'CONTATO'
      Origin = 'CONTATO'
      Required = True
      Size = 255
    end
    object cdsPessoaContatoOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object cdsPessoaContatoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaContatoBO_EMAIL_ENVIO_DOCUMENTO_FISCAL: TStringField
      DisplayLabel = 'Email para Envio de Documento Fiscal'
      FieldName = 'BO_EMAIL_ENVIO_DOCUMENTO_FISCAL'
      Origin = 'BO_EMAIL_ENVIO_DOCUMENTO_FISCAL'
      FixedChar = True
      Size = 1
    end
  end
  object cdsPessoaCredito: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaCredito
    Params = <>
    AfterOpen = cdsPessoaRepresentanteAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 942
    Top = 79
    object cdsPessoaCreditoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaCreditoVL_CREDITO: TFMTBCDField
      DisplayLabel = 'Valor Cr'#233'dito'
      FieldName = 'VL_CREDITO'
      Origin = 'VL_CREDITO'
      Precision = 24
      Size = 9
    end
    object cdsPessoaCreditoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
  end
  object dsPessoaCredito: TDataSource
    DataSet = cdsPessoaCredito
    Left = 970
    Top = 78
  end
  object cdsPessoaRestricao: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqPessoaRestricao
    Params = <>
    AfterOpen = cdsPessoaRepresentanteAfterOpen
    OnNewRecord = cdsPessoaRestricaoNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 918
    Top = 79
    object cdsPessoaRestricaoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsPessoaRestricaoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsPessoaRestricaoDH_INCLUSAO: TDateTimeField
      DisplayLabel = 'Dh. Inclus'#227'o'
      FieldName = 'DH_INCLUSAO'
      Origin = 'DH_INCLUSAO'
      Required = True
    end
    object cdsPessoaRestricaoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsPessoaRestricaoID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
    end
    object cdsPessoaRestricaoID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsPessoaRestricaoID_TIPO_RESTRICAO: TIntegerField
      DisplayLabel = 'C'#243'digo do Tipo da Restri'#231#227'o'
      FieldName = 'ID_TIPO_RESTRICAO'
      Origin = 'ID_TIPO_RESTRICAO'
      Required = True
    end
    object cdsPessoaRestricaoJOIN_DESCRICAO_RESTRICAO: TStringField
      DisplayLabel = 'Tipo de Restri'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_RESTRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
