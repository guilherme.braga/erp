unit uCadVeiculo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxCheckBox, cxDBEdit,
  uGBDBCheckBox, uGBDBTextEditPK, uGBDBTextEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, uGBDBButtonEditFK, Vcl.StdCtrls, cxDropDownEdit, uGBDBComboBox, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton, JvTransparentButton,
  uGBPanel;

type
  TCadVeiculo = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    eFkModelo: TgbDBButtonEditFK;
    descModelo: TgbDBTextEdit;
    Label2: TLabel;
    eFkMarca: TgbDBButtonEditFK;
    descMarca: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    Label3: TLabel;
    lbFilial: TLabel;
    edtFilial: TgbDBButtonEditFK;
    descFilial: TgbDBTextEdit;
    Label5: TLabel;
    ePkCodig: TgbDBTextEditPK;
    Label6: TLabel;
    edDescricao: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    Label4: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    Label7: TLabel;
    gbDBTextEdit4: TgbDBTextEdit;
    Label8: TLabel;
    gbDBComboBox1: TgbDBComboBox;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    Label9: TLabel;
    cdsDataID: TAutoIncField;
    cdsDataPLACA: TStringField;
    cdsDataANO: TIntegerField;
    cdsDataCOMBUSTIVEL: TStringField;
    cdsDataCHASSI: TStringField;
    cdsDataRENAVAM: TStringField;
    cdsDataID_MODELO: TIntegerField;
    cdsDataID_MARCA: TIntegerField;
    cdsDataID_COR: TIntegerField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataJOIN_DESCRICAO_MARCA: TStringField;
    cdsDataJOIN_DESCRICAO_MODELO: TStringField;
    cdsDataJOIN_DESCRICAO_COR: TStringField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    gbDBTextEdit5: TgbDBTextEdit;
    Label10: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

Initialization
  RegisterClass(TCadVeiculo);

Finalization
  UnRegisterClass(TCadVeiculo);

end.
