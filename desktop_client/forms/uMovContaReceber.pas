unit uMovContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxButtonEdit, cxDBEdit,
  uGBDBButtonEditFK, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, cxTextEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxBlobEdit,
  uGBDBBlobEdit, uGBPanel, uFrameDetailPadrao, uFrameQuitacao,
  uFrameQuitacaoContaReceber, cxCurrencyEdit, dxBarExtItems, cxBarEditItem, DateUtils,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  uUsuarioDesignControl, dxStatusBar, ugbLabel, cxLabel, JvComponentBase, cxGridDBDataDefinitions,
  uFrameFiltroVerticalPadrao, cxSplitter, JvExControls, JvButton, JvTransparentButton,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter;

type
  TMovContaReceber = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDOCUMENTO: TStringField;
    cdsDataDESCRICAO: TStringField;
    cdsDataVL_TITULO: TFMTBCDField;
    cdsDataVL_QUITADO: TFMTBCDField;
    cdsDataVL_ABERTO: TFMTBCDField;
    cdsDataQT_PARCELA: TIntegerField;
    cdsDataNR_PARCELA: TIntegerField;
    cdsDataDT_VENCIMENTO: TDateField;
    cdsDataBO_VENCIDO: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataSTATUS: TStringField;
    cdsDataJOIN_DESCRICAO_NOME_PESSOA: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsDatafdqContaReceberQuitacao: TDataSetField;
    cdsContaReceberQuitacao: TGBClientDataSet;
    cdsContaReceberQuitacaoID: TAutoIncField;
    cdsContaReceberQuitacaoDH_CADASTRO: TDateTimeField;
    cdsContaReceberQuitacaoVL_QUITACAO: TFMTBCDField;
    cdsContaReceberQuitacaoOBSERVACAO: TBlobField;
    cdsContaReceberQuitacaoID_CONTA_RECEBER: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_ID_ORIGEM_CHAVE_PROCESSO: TIntegerField;
    labelCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    edDtCadastro: TgbDBDateEdit;
    edtDescProcesso: TgbDBTextEdit;
    gbPanel1: TgbPanel;
    cdsDataID_FORMA_PAGAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsContaReceberQuitacaoNR_ITEM: TIntegerField;
    cdsContaReceberQuitacaoID_TIPO_QUITACAO: TIntegerField;
    cdsContaReceberQuitacaoDT_QUITACAO: TDateField;
    cdsContaReceberQuitacaoID_FILIAL: TIntegerField;
    cdsContaReceberQuitacaoID_CONTA_CORRENTE: TIntegerField;
    cdsContaReceberQuitacaoVL_DESCONTO: TFMTBCDField;
    cdsContaReceberQuitacaoVL_ACRESCIMO: TFMTBCDField;
    cdsContaReceberQuitacaoVL_TOTAL: TFMTBCDField;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_TIPO_QUITACAO: TStringField;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    labelProcesso: TLabel;
    edtProcesso: TgbDBTextEdit;
    cdsDataSEQUENCIA: TStringField;
    cdsDataVL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_DECRESCIMO: TFMTBCDField;
    cdsDataVL_QUITADO_LIQUIDO: TFMTBCDField;
    EdtConsultaDocumento: TdxBarEdit;
    EdtConsultaDescricao: TdxBarEdit;
    EdtConsultaVencimentoInicio: TcxBarEditItem;
    EdtConsultaVencimentoFim: TcxBarEditItem;
    EdtConsultaSituacao: TcxBarEditItem;
    lbConsultaVencimento: TdxBarStatic;
    EdtConsultaValor: TcxBarEditItem;
    gbPanel2: TgbPanel;
    labelSituacao: TLabel;
    edSituacao: TgbDBTextEdit;
    labelDtVencimento: TLabel;
    labelDocumento: TLabel;
    labelCentroResultado: TLabel;
    labelContaAnalise: TLabel;
    labelVlTitulo: TLabel;
    labelObservacao: TLabel;
    labelFormaPagamento: TLabel;
    labelDescricao: TLabel;
    labelPessoa: TLabel;
    edDtVencimento: TgbDBDateEdit;
    edDocumento: TgbDBTextEdit;
    descCentroResultado: TgbDBTextEdit;
    edtCentroResultado: TgbDBButtonEditFK;
    descContaAnalise: TgbDBTextEdit;
    edVlTitulo: TgbDBTextEdit;
    edtObservacao: TgbDBBlobEdit;
    edtFormaPagamento: TgbDBButtonEditFK;
    descFormaPagamento: TgbDBTextEdit;
    EdDescricao: TgbDBTextEdit;
    edtPessoa: TgbDBButtonEditFK;
    edtDescPessoa: TgbDBTextEdit;
    cdsDataDT_COMPETENCIA: TDateField;
    labelDtCompetencia: TLabel;
    edtDtCompetencia: TgbDBDateEdit;
    cdsContaReceberQuitacaoID_CONTA_ANALISE: TIntegerField;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsDataTP_DOCUMENTO_ORIGEM: TStringField;
    labelOrigem: TLabel;
    edtOrigem: TgbDBTextEdit;
    edtDescOrigem: TgbDBTextEdit;
    cdsContaReceberQuitacaoID_DOCUMENTO_ORIGEM: TIntegerField;
    cdsContaReceberQuitacaoTP_DOCUMENTO_ORIGEM: TStringField;
    cdsContaReceberQuitacaoSTATUS: TStringField;
    cdsContaReceberQuitacaoCC_VL_TROCO_DIFERENCA: TFloatField;
    cdsContaReceberQuitacaoIC_VL_PAGAMENTO: TFloatField;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    edVlRecebido: TgbDBTextEdit;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBTextEdit7: TgbDBTextEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    edVlPagar: TgbDBTextEdit;
    edtContaAnalise: TcxDBButtonEdit;
    cdsDataDT_DOCUMENTO: TDateField;
    labelSequencia: TLabel;
    edtSequencia: TgbDBTextEdit;
    labelDtDocumento: TLabel;
    edtDtDocumento: TgbDBDateEdit;
    labelDtCadastro: TLabel;
    cdsDataID_CONTA_CORRENTE: TIntegerField;
    cdsDataJOIN_DESCRICAO_CONTA_CORRENTE: TStringField;
    descContaCorrente: TgbDBTextEdit;
    edtContaCorrente: TgbDBButtonEditFK;
    lbContaCorrente: TLabel;
    PnFrameQuitacao: TgbPanel;
    cdsContaReceberQuitacaoID_CENTRO_RESULTADO: TIntegerField;
    cdsContaReceberQuitacaoCHEQUE_NUMERO: TIntegerField;
    cdsContaReceberQuitacaoCHEQUE_CONTA_CORRENTE: TStringField;
    cdsContaReceberQuitacaoCHEQUE_AGENCIA: TStringField;
    cdsContaReceberQuitacaoCHEQUE_DT_EMISSAO: TDateField;
    cdsContaReceberQuitacaoCHEQUE_BANCO: TStringField;
    cdsContaReceberQuitacaoCHEQUE_DOC_FEDERAL: TStringField;
    cdsContaReceberQuitacaoCHEQUE_SACADO: TStringField;
    cdsContaReceberQuitacaoID_OPERADORA_CARTAO: TIntegerField;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    ActExportarExcel: TAction;
    Label1: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataBO_NEGOCIADO: TStringField;
    cdsDataBO_GERADO_POR_NEGOCIACAO: TStringField;
    cdsDataVL_JUROS: TFMTBCDField;
    cdsDataPERC_JUROS: TFMTBCDField;
    cdsDataVL_MULTA: TFMTBCDField;
    cdsDataPERC_MULTA: TFMTBCDField;
    cdsDataID_CARTEIRA: TIntegerField;
    cdsDataJOIN_DESCRICAO_CARTEIRA: TStringField;
    cdsContaReceberQuitacaoVL_JUROS: TFMTBCDField;
    cdsContaReceberQuitacaoVL_MULTA: TFMTBCDField;
    cdsContaReceberQuitacaoPERC_JUROS: TFMTBCDField;
    cdsContaReceberQuitacaoPERC_MULTA: TFMTBCDField;
    cdsContaReceberQuitacaoPERC_ACRESCIMO: TFMTBCDField;
    cdsContaReceberQuitacaoPERC_DESCONTO: TFMTBCDField;
    ActLoteRecebimento: TAction;
    lbLoteRecebimento: TdxBarLargeButton;
    cdsContaReceberQuitacaoVL_TROCO: TFMTBCDField;
    PnTotalizadorConsulta: TFlowPanel;
    pnTotalizadorVlAberto: TgbLabel;
    pnTotalizadorAcrecimoAberto: TgbLabel;
    pnTotalizadorTotalAberto: TgbLabel;
    pnTotalizadorVlVencido: TgbLabel;
    pnTotalizadorVlVencer: TgbLabel;
    pnTotalizadorVlRecebido: TgbLabel;
    pnTotalizadorAcrescimoRecebido: TgbLabel;
    pnTotalizadorTotalRecebido: TgbLabel;
    filtroPessoa: TcxBarEditItem;
    filtroNomePessoa: TdxBarStatic;
    cdsContaReceberQuitacaoCHEQUE_DT_VENCIMENTO: TDateField;
    cdsContaReceberQuitacaoID_USUARIO_BAIXA: TIntegerField;
    cdsContaReceberQuitacaoJOIN_NOME_USUARIO_BAIXA: TStringField;
    cdsDataVL_NEGOCIADO: TFMTBCDField;
    cdsDataVL_TITULO_ANTES_NEGOCIACAO: TFMTBCDField;
    cdsDataBO_BOLETO_EMITIDO: TStringField;
    cdsDataBOLETO_NOSSO_NUMERO: TIntegerField;
    cdsDataDH_EMISSAO_BOLETO: TDateTimeField;
    cdsDataBOLETO_LINHA_DIGITAVEL: TStringField;
    cdsDataBOLETO_CODIGO_BARRAS: TStringField;
    ActEmitirBoleto: TAction;
    bbEmissaoBoleto: TdxBarLargeButton;
    ActCancelarEmissaoBoleto: TAction;
    dxBarLargeButton4: TdxBarLargeButton;

    procedure cdsContaReceberQuitacaoAfterOpen(DataSet: TDataSet);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsContaReceberQuitacaoNewRecord(DataSet: TDataSet);
    procedure CalcularPercentual(Sender: TField);
    procedure CalcularValor(Sender: TField);
    procedure AtualizarVlReceber(Sender: TField);
    procedure AtualizarSituacao;
    procedure AtualizarTotalCapa;
    procedure AtualizarTotalItem;
    procedure cdsContaReceberQuitacaoAfterPost(DataSet: TDataSet);
    procedure cdsContaReceberQuitacaoAfterDelete(DataSet: TDataSet);
    procedure cdsContaReceberQuitacaoVL_QUITACAOChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsContaReceberQuitacaoBeforeInsert(DataSet: TDataSet);
    procedure CalcularValoresAPartirDoTotalQuitacao(Sender: TField);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure edtContaAnaliseDblClick(Sender: TObject);
    procedure edtContaAnaliseExit(Sender: TObject);
    procedure cdsContaReceberQuitacaoBeforeDelete(DataSet: TDataSet);
    procedure ActExportarExcelExecute(Sender: TObject);
    procedure TotalizarAcrescimo(Sender: TField);
    procedure ActLoteRecebimentoExecute(Sender: TObject);
    procedure Level1BandedTableView1DataControllerDataChanged(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure filtroPessoaPropertiesEditValueChanged(Sender: TObject);
    procedure cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
    procedure EdtConsultaSituacaoPropertiesEditValueChanged(Sender: TObject);
    procedure ActFocarFiltroExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActEmitirBoletoExecute(Sender: TObject);
    procedure ActCancelarEmissaoBoletoExecute(Sender: TObject);
  private
    FFrameQuitacaoContaReceber: TFrameQuitacaoContaReceber;
    FCalculandoPercentual: Boolean;
    FCalculandoValor: Boolean;
    FCalculandoTotalQuitacaoValor: Boolean;
    FIdChaveProcesso : Integer;
    FIdContaReceber : Integer;
    FIdUltimaQuitacao: Integer;
    FTotalizandoAcrescimo: Boolean;
    FCalculandoTotalRodape: Boolean;
    FFiltroRegistroGradeAnterior: String;

    FParametroFiltroDataVencimentoInicio: TParametroFormulario;
    FParametroFiltroDataVencimentoFim: TParametroFormulario;
    FParametroFiltroStatus: TParametroFormulario;
    FParametroImprimirComprovanteAposRealizarQuitacao: TParametroFormulario;
    FParametroPermiteAlterarBaixaDoTitulo: TParametroFormulario;
    FParametroGerarContraPartidaContaCorrenteMovimento: TParametroFormulario;
    FParametroSugerirDataVencimentoNaDataQuitacao: TParametroFormulario;
    FParametroFocoFiltroRapidoContaReceber: TParametroFormulario;
    FParametroExibirDadosBoleto: TParametroFormulario;

    procedure HabilitarPainelPrincipal;
    procedure ValidarDataDeCompetencia;
    procedure ValidarCamposObrigatorios;
    procedure ArmazenarMaiorIDQuitacao;
    procedure ImprimirComprovanteQuitacao;

    procedure CalcularTotaisDoRodape;
    procedure AlinharBarraTotalizadoraConsulta;
    procedure SugerirDataVencimentoCheque;
    procedure SugerirDadosPessoaNoCheque;

    function PodeUtilizarFiltroStatusNaPesquisaPadrao: Boolean;
    procedure DefinirIdentificadorDaPesquisaPadrao;
    procedure ChecarConsultaPreenchida;
    function ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
    function ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
    procedure ExibirFiltroPeriodoQuitacao;
    procedure FocarFiltroRapido;

    procedure ExibirDadosBoleto;
    procedure ExibirBotaoEmitirBoleto;
    procedure ExibirBotaoCancelarBoleto;

    procedure ConfigurarAcaoEmitirBoleto;

    const FIDENTIFICADOR_SQL_ABERTO = 'MOVCONTARECEBERPESQUISAABERTO';
    const FIDENTIFICADOR_SQL_QUITADO = 'MOVCONTARECEBERPESQUISAQUITADO';
    const FIDENTIFICADOR_SQL_GERAL = 'MOVCONTARECEBER';
    const FOCO_FILTRO_RAPIDO_PESSOA = 'PESSOA';
    const FOCO_FILTRO_RAPIDO_SITUACAO = 'SITUA��O';
    const FOCO_FILTRO_RAPIDO_DATA_VENCIMENTO = 'VENCIMENTO';
  protected
    procedure GerenciarControles; override;
    procedure DepoisDeConfirmar; override;
    procedure AntesDeConfirmar; override;
    procedure AntesDeRemover; override;

    procedure SetWhereBundle;override;
    procedure PrepararFiltros;override;
    procedure AoCriarFormulario; override;

    procedure BloquearEdicao; override;
    procedure InstanciarFrames;override;
    procedure DepoisDeInserir;override;
    procedure DepoisDeAlterar;override;
    procedure DepoisDePesquisar; override;
    procedure AntesDePesquisar; override;
    procedure DepoisDeSelecionarRegistro; override;

    procedure MostrarInformacoesImplantacao; override;

    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;


  public
    FAtualizarTotalItem: Boolean;

    FParametroBloquearEdicaoJuros: TParametroFormulario;
    FParametroBloquearEdicaoMulta: TParametroFormulario;
    FParametroBloquearDataQuitacao: TParametroFormulario;

  const TODOS: String = 'TODOS';
  const ABERTO    = 'ABERTO';
  const QUITADO   = 'QUITADO';
  const CANCELADO = 'CANCELADO';

  procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;

    function QuitacaoEmCartao: Boolean;
  end;

implementation

{$R *.dfm}

uses
  uDmConnection,
  uMathUtils,
  System.Math,
  uChaveProcesso,
  uDatasetUtils,
  uFilial,
  uFrmMessage_Process, uContaReceber, uChaveProcessoProxy, uContaReceberProxy,
  uTMessage, uPesqContaAnalise, uSistema, uDevExpressUtils, uConstParametroFormulario, uTFunction,
  uFrmMessage, Generics.Collections, uFrmVisualizacaoRelatoriosAssociados, uTControl_Function,
  uFrmHelpImplantacaoContaReceber, uFrameQuitacaoLoteRecebimento, uMovLoteRecebimento, ugbFDMemTable,
  uFrmConsultaPadrao, uTPessoa, uPessoaProxy, uTipoQuitacaoProxy, uFormaPagamento, uControlsUtils,
  uBoleto;

procedure TMovContaReceber.ValidarCamposObrigatorios;
begin
  cdsData.ValidarCamposObrigatorios;
  ValidarDataDeCompetencia;
end;

procedure TMovContaReceber.ValidarDataDeCompetencia;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
end;

procedure TMovContaReceber.CalcularPercentual(Sender: TField);

  function PegarPercentualSobreValor: Double;
  begin
    result := TMathUtils.PercentualSobreValor(
      Sender.AsFloat,
      cdsContaReceberQuitacaoVL_QUITACAO.AsFloat);
  end;
begin
  if FCalculandoValor then
    exit;

  FCalculandoValor := True;

  if Sender.Dataset = cdsContaReceberQuitacao then
  begin
    if Sender = cdsContaReceberQuitacaoVL_DESCONTO then
    begin
      cdsContaReceberQuitacaoPERC_DESCONTO.AsFloat := PegarPercentualSobreValor;
    end
    else if Sender = cdsContaReceberQuitacaoVL_ACRESCIMO then
    begin
      cdsContaReceberQuitacaoPERC_ACRESCIMO.AsFloat := PegarPercentualSobreValor;
    end;

    AtualizarTotalItem;

  end;
  FCalculandoValor := False;
end;

procedure TMovContaReceber.CalcularTotaisDoRodape;
var
  fdmTotais: TgbFDMemTable;
  idsContaReceber: String;
begin
  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    idsContaReceber := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
      fdmSearch.FieldByName('ID'), Level1BandedTableView1);

    if idsContaReceber.IsEmpty then
    begin
      idsContaReceber := '-9999999';
    end;

    fdmTotais := TContaReceber.GetTotaisContaReceber(idsContaReceber);

    pnTotalizadorVlAberto.Caption := 'Aberto R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto'));
    pnTotalizadorAcrecimoAberto.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_aberto'));
    pnTotalizadorTotalAberto.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_liquido'));
    pnTotalizadorVlVencido.Caption := 'Vencido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencido'));
    pnTotalizadorVlVencer.Caption := 'A Vencer R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_aberto_vencer'));
    pnTotalizadorVlRecebido.Caption := 'Recebido R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido'));
    pnTotalizadorAcrescimoRecebido.Caption := 'Acr�scimo R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_acrescimo_recebido'));
    pnTotalizadorTotalRecebido.Caption := 'Total R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vl_recebido_liquido'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;

procedure TMovContaReceber.AlinharBarraTotalizadoraConsulta;
begin
  PnTotalizadorConsulta.AutoWrap := false;
  PnTotalizadorConsulta.AutoWrap := true;
end;

procedure TMovContaReceber.CalcularValor(Sender: TField);

  function PegarValorSobrePercentual: Double;
  begin
    result := TMathUtils.ValorSobrePercentual(
      Sender.AsFloat,
      cdsContaReceberQuitacaoVL_QUITACAO.AsFloat);
  end;
begin
  if FCalculandoPercentual then
    exit;

  FCalculandoPercentual := True;

  if Sender.Dataset = cdsContaReceberQuitacao then
  begin
    if Sender = cdsContaReceberQuitacaoPERC_DESCONTO then
    begin
      cdsContaReceberQuitacaoVL_DESCONTO.AsFloat := PegarValorSobrePercentual;
    end
    else if Sender = cdsContaReceberQuitacaoPERC_ACRESCIMO then
    begin
      cdsContaReceberQuitacaoVL_ACRESCIMO.AsFloat := PegarValorSobrePercentual;
    end;

    AtualizarTotalItem;

  end;

  FCalculandoPercentual := False;
end;

procedure TMovContaReceber.CalcularValoresAPartirDoTotalQuitacao(
  Sender: TField);
begin
  if FCalculandoTotalQuitacaoValor then
    exit;

  try
    FCalculandoTotalQuitacaoValor := true;
    cdsContaReceberQuitacaoVL_QUITACAO.AsFloat :=
      cdsContaReceberQuitacaoVL_TOTAL.AsFloat +
      cdsContaReceberQuitacaoVL_DESCONTO.AsFloat -
      cdsContaReceberQuitacaoVL_ACRESCIMO.AsFloat;

    FFrameQuitacaoContaReceber.PreencherValorPagamento
  finally
    FCalculandoTotalQuitacaoValor := false;
  end;
end;

procedure TMovContaReceber.AtualizarVlReceber(Sender: TField);
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  if Sender = cdsDataVL_TITULO then
    cdsDataVL_ABERTO.AsFloat := cdsDataVL_TITULO.AsFloat;

end;

procedure TMovContaReceber.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados := cdsDataSTATUS.AsString.Equals(
    TContaReceberMovimento.CANCELADO);
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalCapa;
  AtualizarSituacao;
  HabilitarPainelPrincipal;
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FFrameQuitacaoContaReceber.SetConfiguracoesIniciais(cdsContaReceberQuitacao);
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarTotalCapa;
  AtualizarSituacao;
  HabilitarPainelPrincipal;
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  cdsData.Alterar;
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoBeforeInsert(
  DataSet: TDataSet);
begin
  inherited;
  ValidarCamposObrigatorios;
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAOChange(Sender: TField);
begin
  inherited;
  SugerirDadosPessoaNoCheque;
  SugerirDataVencimentoCheque;
end;

procedure TMovContaReceber.ActCancelarEmissaoBoletoExecute(Sender: TObject);
begin
  inherited;
  if TFrmMessage.Question('Deseja realmente cancelar o boleto emitido?') = MCONFIRMED then
  begin
    TContaReceber.CancelarBoletoEmitido(cdsDataID.AsString);
    AtualizarRegistro;
  end;
end;

procedure TMovContaReceber.ActEmitirBoletoExecute(Sender: TObject);
begin
  inherited;
  TBoleto.EmitirBoleto(cdsDataID.AsString);
  AtualizarRegistro;
end;

procedure TMovContaReceber.ActExportarExcelExecute(Sender: TObject);
begin
  inherited;
  TcxGridUtils.ExportarParaExcel(cxGridPesquisaPadrao,
    'Conta a Receber');
end;

procedure TMovContaReceber.ActFocarFiltroExecute(Sender: TObject);
begin
  inherited;
  FocarFiltroRapido;
end;

procedure TMovContaReceber.ActLoteRecebimentoExecute(Sender: TObject);
var
  fieldPessoa: TField;
begin
  inherited;
  fieldPessoa := fdmSearch.FindField('ID_PESSOA');

  if Assigned(fieldPessoa) then
  begin
    TMovLoteRecebimento.AbrirTelaFiltrandoPorCodigosDosTitulos(
      TDatasetUtils.ConcatenarRegistrosFiltradosNaViewComCriterios(fdmSearch.FieldByName('ID'),
      Level1BandedTableView1, fieldPessoa, fieldPessoa.AsString));
  end
  else
  begin
    TMovLoteRecebimento.AbrirTelaFiltrandoPorCodigosDosTitulos(
      TDatasetUtils.ConcatenarRegistrosFiltradosNaView(fdmSearch.FieldByName('ID'), Level1BandedTableView1));
  end;
end;

procedure TMovContaReceber.AntesDeConfirmar;
begin
  if cdsContaReceberQuitacao.State in dsEditModes then
  begin
    FFrameQuitacaoContaReceber.ActConfirm.Execute;
  end;

  inherited;
  FIdChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;
  FIdContaReceber  := cdsDataID.AsInteger;
  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
end;

procedure TMovContaReceber.AntesDePesquisar;
begin
  inherited;
  ChecarConsultaPreenchida;
end;

procedure TMovContaReceber.ChecarConsultaPreenchida;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) and ExistePesquisaConfiguradaParaSituacaoQuitado then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(EdtConsultaSituacao.EditValue).Equals(ABERTO) and ExistePesquisaConfiguradaParaSituacaoAberto then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_GERAL;
end;

procedure TMovContaReceber.ConfigurarAcaoEmitirBoleto;
begin
  if cdsDataBOLETO_NOSSO_NUMERO.AsInteger <= 0 then
  begin
    ActEmitirBoleto.Caption := 'Emitir Boleto';
    ActEmitirBoleto.Hint := 'Realiza a emiss�o do boleto refer�nte a esse t�tulo de contas a receber';
  end
  else
  begin
    ActEmitirBoleto.Caption := 'Re-imprimir Boleto';
    ActEmitirBoleto.Hint := 'Realiza a re-emiss�o do boleto refer�nte a esse t�tulo de contas a receber';
  end;
end;

procedure TMovContaReceber.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento';
  campoFiltro.campo := 'DOCUMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Descricao
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Descri��o';
  campoFiltro.campo := 'DESCRICAO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Documento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Documento';
  campoFiltro.campo := 'DT_DOCUMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Vencimento
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Vencimento';
  campoFiltro.campo := 'DT_VENCIMENTO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Data de Compet�ncia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Compet�ncia';
  campoFiltro.campo := 'DT_COMPETENCIA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

{  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TContaReceberMovimento.ABERTO+';'+
  TContaReceberMovimento.QUITADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor do Documento';
  campoFiltro.campo := 'VL_TITULO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Pessoa
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Centro de Resultado
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Centro de Resultado';
  campoFiltro.campo := 'ID_CENTRO_RESULTADO';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CENTRO_RESULTADO';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta de Analise
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta de Analise';
  campoFiltro.campo := 'ID_CONTA_ANALISE';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_ANALISE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Carteira
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Carteira';
  campoFiltro.campo := 'ID_CARTEIRA';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CARTEIRA';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Conta Corrente
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Conta Corrente';
  campoFiltro.campo := 'ID_CONTA_CORRENTE';
  campoFiltro.tabela := 'CONTA_RECEBER';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CONTA_CORRENTE';
  campoFiltro.campoFK.camposConsulta := 'ID;DESCRICAO';
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Per�odo de Quita��o (Dt de Recebimento)
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Dt. Recebimento';
  campoFiltro.campo := 'DT_QUITACAO';
  campoFiltro.tabela := 'CONTA_RECEBER_QUITACAO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TMovContaReceber.DepoisDeAlterar;
begin
  inherited;
  ArmazenarMaiorIDQuitacao;
end;

procedure TMovContaReceber.DepoisDeConfirmar;
var
  ContaReceber : TContaReceber;
begin
  inherited;
  TFrmMessage_Process.SendMessage('Gerando Movimenta��es ... ');
  ContaReceber := TContaReceber.Create;
  try
    ContaReceber.AtualizarMovimentosContaCorrente(FIdContaReceber, FIdChaveProcesso,
      FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  ImprimirComprovanteQuitacao;
  AtualizarRegistro;
  ArmazenarMaiorIDQuitacao;

  if FIdContaReceber > 0 then
  begin
    ActSearch.Execute;
    FocarFiltroRapido;
  end;
end;

procedure TMovContaReceber.DepoisDePesquisar;
begin
  inherited;
  CalcularTotaisDoRodape;
  DefinirIdentificadorDaPesquisaPadrao;
end;

procedure TMovContaReceber.DepoisDeSelecionarRegistro;
begin
  inherited;
  ConfigurarAcaoEmitirBoleto;
end;

procedure TMovContaReceber.EdtConsultaSituacaoPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  DefinirIdentificadorDaPesquisaPadrao;
  ExibirFiltroPeriodoQuitacao;
end;

procedure TMovContaReceber.DepoisDeInserir;
begin
  inherited;
  ArmazenarMaiorIDQuitacao;
end;

procedure TMovContaReceber.ImprimirComprovanteQuitacao;
const
  MENSAGEM_CONFIRMACAO_SINGULAR =
    'Foi realizada uma nova quita��o neste t�tulo, deseja imprimir o comprovante?';
  MENSAGEM_CONFIRMACAO_PLURAL =
    'Foi realizada novas quita��es neste t�tulo, deseja imprimir os comprovantes?';
var
  idsNovasQuitacoes: TList<Integer>;
  i: Integer;
  mensagem: String;
begin
  if (FParametroImprimirComprovanteAposRealizarQuitacao.ValorSim) and not(cdsContaReceberQuitacao.IsEmpty) and
    (cdsDataID.AsInteger > 0) then
  begin
    idsNovasQuitacoes := TList<Integer>.Create;
    try
      TContaReceber.GetIdsNovasQuitacoes(FIdUltimaQuitacao, cdsDataID.AsInteger, idsNovasQuitacoes);

      if idsNovasQuitacoes.Count = 0 then
      begin
        exit;
      end
      else
      if idsNovasQuitacoes.Count = 1 then
      begin
        mensagem := MENSAGEM_CONFIRMACAO_SINGULAR;
      end
      else
      if idsNovasQuitacoes.Count > 1 then
      begin
        mensagem := MENSAGEM_CONFIRMACAO_PLURAL;
      end;

      if TFrmMessage.Question(mensagem) = MCONFIRMED then
      begin
        for i := 0 to Pred(idsNovasQuitacoes.Count) do
        begin
          TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
            idsNovasQuitacoes[i], TFrameQuitacaoContaReceber.NOME_FRAME, TSistema.Sistema.usuario.idSeguranca);
        end;
      end;
    finally
      FreeAndNil(idsNovasQuitacoes);
    end;
  end;
end;

procedure TMovContaReceber.edtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  if not cdsData.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsData.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      cdsData.FieldByName('ID_CONTA_ANALISE'), cdsData.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TMovContaReceber.edtContaAnaliseExit(Sender: TObject);
begin
  inherited;
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsultaOculta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovContaReceber.EdtContaAnalisePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  if not cdsData.FieldByName('ID_CENTRO_RESULTADO').AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsData.FieldByName('ID_CENTRO_RESULTADO').AsInteger,
      cdsData.FieldByName('ID_CONTA_ANALISE'), cdsData.FieldByName('JOIN_DESCRICAO_CONTA_ANALISE'));
  end;
end;

procedure TMovContaReceber.filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := TFrmConsultaPadrao.ConsultarID('PESSOA');
  if idPessoa > 0 then
  begin
    filtroPessoa.EditValue := idPessoa;
    filtroPessoaPropertiesEditValueChanged(filtroPessoa);
  end;
end;

procedure TMovContaReceber.filtroPessoaPropertiesEditValueChanged(Sender: TObject);
var
  idPessoa: Integer;
begin
  inherited;
  if VartoStr(filtroPessoa.EditValue).IsEmpty then
  begin
    Exit;
  end;

  idPessoa := StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0);
  if idPessoa > 0 then
  begin
    filtroNomePessoa.Caption := Copy(TPessoa.GetNomePessoa(idPessoa),1,24);
  end
  else
  begin
    filtroNomePessoa.Caption := '';
  end;

  abort;
end;

procedure TMovContaReceber.FormResize(Sender: TObject);
begin
  inherited;
  AlinharBarraTotalizadoraConsulta;
end;

procedure TMovContaReceber.FormShow(Sender: TObject);
begin
  inherited;
  FocarFiltroRapido;
end;

procedure TMovContaReceber.GerenciarControles;
var
  estaConsultando: Boolean;
begin
  inherited;
    estaConsultando := (acaoCrud = uFrmCadastro_Padrao.TAcaoCrud(Pesquisar)) and
    (Level1BandedTableView1.DataController.FilteredRecordCount > 0);

  ExibirBotaoCancelarBoleto;
  ExibirBotaoEmitirBoleto;

  ActLoteRecebimento.Visible := estaConsultando;
end;

procedure TMovContaReceber.AntesDeRemover;
var
  ContaReceber : TContaReceber;
begin
  inherited;

  if (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> TContaReceberMovimento.ORIGEM_MANUAL) and
     (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> '') and
     (cdsDataTP_DOCUMENTO_ORIGEM.AsString <> TContaReceberMovimento.ORIGEM_GERACAO_DOCUMENTO)
  then
  begin
    TMessage.MessageInformation('Este Documento deve ser Removido apenas a partir de sua Origem.');
    Abort;
  end;

  TFrmMessage_Process.SendMessage('Removendo Movimenta��es ... ');
  ContaReceber := TContaReceber.Create;
  try
    ContaReceber.RemoverMovimentosContaCorrente(cdsDataID.AsInteger,
      FParametroGerarContraPartidaContaCorrenteMovimento.ValorSim);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovContaReceber.AoCriarFormulario;
begin
  inherited;
  FTotalizandoAcrescimo := false;
  FAtualizarTotalItem := true;
  EdDescricao.Properties.CharCase := ecNormal;
  EdDocumento.Properties.CharCase := ecNormal;
  FCalculandoTotalRodape := false;
  FFiltroRegistroGradeAnterior := '';
end;

procedure TMovContaReceber.ArmazenarMaiorIDQuitacao;
begin
  FIdUltimaQuitacao := Trunc(TDatasetUtils.MaiorValor(cdsContaReceberQuitacaoID));
end;

procedure TMovContaReceber.AtualizarSituacao;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  if TMathUtils.Arredondar(cdsDataVL_ABERTO.AsFloat) = 0 then
  begin
    cdsDataSTATUS.AsString := QUITADO;
  end
  else
  begin
    cdsDataSTATUS.AsString := ABERTO;
  end;

end;

procedure TMovContaReceber.AtualizarTotalCapa;
var
  vlTotal, vlLiquido  : Double;
  vlDesconto : Double;
  vlAcrescimo  : Double;
  vlMulta: Double;
  vlJuros: Double;
  percMulta: Double;
  percJuros: Double;
begin
  if not (cdsData.State in dsEditModes) then
    Exit;

  vlLiquido    := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_TOTAL);
  vlTotal      := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_QUITACAO);
  vlDesconto   := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_DESCONTO);
  vlAcrescimo  := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_ACRESCIMO);
  vlMulta := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_MULTA);
  vlJuros := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoVL_JUROS);
  percMulta := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoPERC_MULTA);
  percJuros := TDatasetUtils.SomarColuna(cdsContaReceberQuitacaoPERC_JUROS);

  cdsDataVL_ACRESCIMO.AsFloat  := vlAcrescimo;
  cdsDataVL_DECRESCIMO.AsFloat := vlDesconto;

  cdsDataVL_JUROS.AsFloat  := vlJuros;
  cdsDataVL_MULTA.AsFloat  := vlMulta;

  cdsDataPERC_JUROS.AsFloat  := percJuros;
  cdsDataPERC_MULTA.AsFloat  := percMulta;

  cdsDataVL_QUITADO.AsFloat         := vlTotal;
  cdsDataVL_QUITADO_LIQUIDO.AsFloat := vlLiquido;

  cdsDataVL_ABERTO.AsFloat  := cdsDataVL_TITULO.AsFloat - vlTotal;
end;

procedure TMovContaReceber.AtualizarTotalItem;
begin
  if not FAtualizarTotalItem then
    exit;

  cdsContaReceberQuitacaoVL_TOTAL.AsFloat := cdsContaReceberQuitacaoVL_QUITACAO.AsFloat
                                          + cdsContaReceberQuitacaoVL_ACRESCIMO.AsFloat
                                          - cdsContaReceberQuitacaoVL_DESCONTO.AsFloat;
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsContaReceberQuitacaoDH_CADASTRO.AsDateTime     := Now;

  if FParametroSugerirDataVencimentoNaDataQuitacao.ValorSim then
  begin
    cdsContaReceberQuitacaoDT_QUITACAO.AsDateTime := cdsDataDT_VENCIMENTO.AsDateTime;
  end
  else
  begin
    cdsContaReceberQuitacaoDT_QUITACAO.AsDateTime := Now;
  end;

  cdsContaReceberQuitacaoID_USUARIO_BAIXA.AsInteger := TSistema.Sistema.Usuario.Id;
  cdsContaReceberQuitacaoJOIN_NOME_USUARIO_BAIXA.AsString := TSistema.Sistema.Usuario.Nome;
  cdsContaReceberQuitacaoNR_ITEM.AsFloat            := TDatasetUtils.MaiorValor(cdsContaReceberQuitacaoNR_ITEM)+1;
  cdsContaReceberQuitacaoID_FILIAL.AsInteger        := TFilial.GetIdFilial;
  cdsContaReceberQuitacaoVL_QUITACAO.AsFloat        := cdsDataVL_ABERTO.AsFloat;
  cdsContaReceberQuitacaoVL_TOTAL.AsFloat           := cdsDataVL_ABERTO.AsFloat;
  cdsContaReceberQuitacaoID_CENTRO_RESULTADO.AsInteger := cdsDataID_CENTRO_RESULTADO.AsInteger;
  cdsContaReceberQuitacaoJOIN_DESCRICAO_CENTRO_RESULTADO.AsString := cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO.AsString;

  if cdsDataID_CONTA_CORRENTE.AsInteger > 0 then
  begin
    cdsContaReceberQuitacaoID_CONTA_CORRENTE.AsInteger := cdsDataID_CONTA_CORRENTE.AsInteger;
    cdsContaReceberQuitacaoJOIN_DESCRICAO_CONTA_CORRENTE.AsString := cdsDataJOIN_DESCRICAO_CONTA_CORRENTE.AsString;
  end;

  cdsContaReceberQuitacaoID_CONTA_ANALISE.AsInteger := cdsDataID_CONTA_ANALISE.AsInteger;
  cdsContaReceberQuitacaoJOIN_DESCRICAO_CONTA_ANALISE.AsString := cdsDataJOIN_DESCRICAO_CONTA_ANALISE.AsString;
  cdsContaReceberQuitacaoID_DOCUMENTO_ORIGEM.AsInteger := 0;
  cdsContaReceberQuitacaoTP_DOCUMENTO_ORIGEM.AsString  := TContaReceberQuitacaoMovimento.ORIGEM_MANUAL;
  cdsContaReceberQuitacaoSTATUS.AsString               := TContaReceberQuitacaoMovimento.STATUS_CONFIRMADA;

  //Atribui��es do juros e multa
  TContaReceber.SetEncargosEmAberto(cdsData, cdsContaReceberQuitacao);
end;

procedure TMovContaReceber.cdsContaReceberQuitacaoVL_QUITACAOChange(
  Sender: TField);
begin
  inherited;
  AtualizarTotalItem;
end;

procedure TMovContaReceber.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  HabilitarPainelPrincipal;
end;

procedure TMovContaReceber.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime      := Now;
  cdsDataDT_DOCUMENTO.AsDateTime     := Date;
  cdsDataNR_PARCELA.AsInteger        := 1;
  cdsDataID_CHAVE_PROCESSO.AsInteger :=
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_CONTA_RECEBER, 0);
  cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_CONTA_RECEBER;
    TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_CONTA_RECEBER, 0);
  cdsDataSEQUENCIA.AsString          := '1/1';
  cdsDataID_DOCUMENTO_ORIGEM.AsInteger := 0;
  cdsDataTP_DOCUMENTO_ORIGEM.AsString  := TContaReceberMovimento.ORIGEM_MANUAL;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.Filial.Fid;
end;

procedure TMovContaReceber.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Data de Vencimento Inicio
  FParametroFiltroDataVencimentoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataVencimentoInicio);

  //Data de Vencimento Fim
  FParametroFiltroDataVencimentoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataVencimentoFim);

  //Status
  FParametroFiltroStatus := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_STATUS,
    TConstParametroFormulario.DESCRICAO_FILTRO_STATUS,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroStatus);

  //Imprimir comprovante apos incluir quita��o
  FParametroImprimirComprovanteAposRealizarQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_IMPRIMIR_COMPROVANTE_APOS_REALIZAR_QUITACAO,
    TConstParametroFormulario.DESCRICAO_IMPRIMIR_COMPROVANTE_APOS_REALIZAR_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroImprimirComprovanteAposRealizarQuitacao);

  //Permite alterar baixa do t�tulo
  FParametroPermiteAlterarBaixaDoTitulo := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_PERMITE_ALTERAR_BAIXA_DO_TITULO,
    TConstParametroFormulario.DESCRICAO_PERMITE_ALTERAR_BAIXA_DO_TITULO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroPermiteAlterarBaixaDoTitulo);

  //Gerar contra partida no conta corrente movimento
  FParametroGerarContraPartidaContaCorrenteMovimento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TConstParametroFormulario.DESCRICAO_GERAR_CONTRA_PARTIDA_CONTA_CORRENTE_MOVIMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroGerarContraPartidaContaCorrenteMovimento);

  //Bloquear Edi��o do Juros
  FParametroBloquearEdicaoJuros := TParametroFormulario.Create(
  TConstParametroFormulario.PARAMETRO_BLOQUEAR_EDICAO_JUROS,
    TConstParametroFormulario.DESCRICAO_BLOQUEAR_EDICAO_JUROS,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroBloquearEdicaoJuros);

  //Bloquear Edi��o de Multa
  FParametroBloquearEdicaoMulta := TParametroFormulario.Create(
  TConstParametroFormulario.PARAMETRO_BLOQUEAR_EDICAO_MULTA,
    TConstParametroFormulario.DESCRICAO_BLOQUEAR_EDICAO_MULTA,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroBloquearEdicaoMulta);

  //Sugerir Data de Vencimento na Data de Quita��o
  FParametroSugerirDataVencimentoNaDataQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_SUGERIR_DATA_VENCIMENTO_NA_DATA_QUITACAO,
    TConstParametroFormulario.DESCRICAO_SUGERIR_DATA_VENCIMENTO_NA_DATA_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroSugerirDataVencimentoNaDataQuitacao);

  //Bloquear data de quita��o
  FParametroBloquearDataQuitacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_BLOQUEAR_DATA_QUITACAO,
    TConstParametroFormulario.DESCRICAO_BLOQUEAR_DATA_QUITACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroBloquearDataQuitacao);

  //Foco Filtro Rapido de Conta a Receber
  FParametroFocoFiltroRapidoContaReceber := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FOCO_FILTRO_RAPIDO_CONTA_RECEBER,
    TConstParametroFormulario.DESCRICAO_FOCO_FILTRO_RAPIDO_CONTA_RECEBER,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, FOCO_FILTRO_RAPIDO_PESSOA);
  AParametros.AdicionarParametro(FParametroFocoFiltroRapidoContaReceber);

  //Exibir Dados do Boleto / Cedente
  FParametroExibirDadosBoleto := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_BOLETO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_BOLETO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirDadosBoleto);
end;

procedure TMovContaReceber.HabilitarPainelPrincipal;
begin
  gbPanel1.Enabled := (cdsContaReceberQuitacao.IsEmpty);
end;

procedure TMovContaReceber.InstanciarFrames;
begin
  inherited;
  FFrameQuitacaoContaReceber := TFrameQuitacaoContaReceber.Create(PnFrameQuitacao);
  FFrameQuitacaoContaReceber.Parent := PnFrameQuitacao;
  FFrameQuitacaoContaReceber.Align := alClient;
  FFrameQuitacaoContaReceber.permiteAlterar := FParametroPermiteAlterarBaixaDoTitulo.ValorSim;
end;

procedure TMovContaReceber.Level1BandedTableView1DataControllerDataChanged(Sender: TObject);
var
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not Level1BandedTableView1.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodape;
  end;

  FFiltroRegistroGradeAnterior := Level1BandedTableView1.DataController.Filter.FilterText;
end;

procedure TMovContaReceber.MostrarInformacoesImplantacao;
begin
  inherited;
  TControlFunction.CreateMDIChild(TFrmHelpImplantacaoContaReceber,
    FrmHelpImplantacaoContaReceber);
end;

procedure TMovContaReceber.PrepararFiltros;
begin
  inherited;
  filtroPessoa.EditValue := null;
  filtroNomePessoa.Caption := '';
  EdtConsultaDocumento.Text := '';
  EdtConsultaDescricao.Text := '';
  EdtConsultaVencimentoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataVencimentoInicio.AsString);
  EdtConsultaVencimentoFim.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataVencimentoFim.AsString);
  EdtConsultaSituacao.EditValue :=
    TFunction.Iif(FParametroFiltroStatus.EstaPreenchido, FParametroFiltroStatus.AsString, TODOS);
  EdtConsultaValor.EditValue := null;

  DefinirIdentificadorDaPesquisaPadrao;
  ExibirFiltroPeriodoQuitacao;
end;

procedure TMovContaReceber.FocarFiltroRapido;
begin
  if FParametroFocoFiltroRapidoContaReceber.AsString.Equals(FOCO_FILTRO_RAPIDO_PESSOA) then
  begin
    filtroPessoa.SetFocus;
  end
  else if FParametroFocoFiltroRapidoContaReceber.AsString.Equals(FOCO_FILTRO_RAPIDO_SITUACAO) then
  begin
    EdtConsultaSituacao.SetFocus;
  end
  else if FParametroFocoFiltroRapidoContaReceber.AsString.Equals(FOCO_FILTRO_RAPIDO_DATA_VENCIMENTO) then
  begin
    EdtConsultaVencimentoInicio.SetFocus;
  end;
end;

function TMovContaReceber.QuitacaoEmCartao: Boolean;
begin
  result := cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovContaReceber.SetWhereBundle;
begin
  inherited;
  //Pessoa
  if not VartoStr(filtroPessoa.EditValue).IsEmpty then
    WhereSearch.AddIgual(TContaReceberMovimento.FIELD_PESSOA, StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0));

  //Descri��o
  if not VartoStr(EdtConsultaDocumento.Text).IsEmpty then
    WhereSearch.AddContem(FIELD_DOCUMENTO, UpperCase(EdtConsultaDocumento.Text));

  //Documento
  if not VartoStr(EdtConsultaDescricao.Text).IsEmpty then
    WhereSearch.AddContem(FIELD_DOCUMENTO, UpperCase(EdtConsultaDescricao.Text));

  //Vencimento
  if not(VartoStr(EdtConsultaVencimentoInicio.EditValue).IsEmpty) and
     not(VartoStr(EdtConsultaVencimentoFim.EditValue).IsEmpty) and
     (VarToDateTime(EdtConsultaVencimentoInicio.EditValue) <= VarToDateTime(EdtConsultaVencimentoFim.EditValue)) then
    WhereSearch.AddEntre(FIELD_DATA_VENCIMENTO, StrtoDate(EdtConsultaVencimentoInicio.EditValue), StrtoDate(EdtConsultaVencimentoFim.EditValue));

  //Situa��o
  if PodeUtilizarFiltroStatusNaPesquisaPadrao then
  begin
    if not VartoStr(EdtConsultaSituacao.EditValue).Equals(TODOS) then
      WhereSearch.AddIgual(FIELD_STATUS,VartoStr(EdtConsultaSituacao.EditValue));
  end;

  //Valor
  if StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0) > 0 then
    WhereSearch.AddIgual(FIELD_VALOR_TITULO, StrtoFloatDef(VartoStr(EdtConsultaValor.EditValue), 0));
end;

procedure TMovContaReceber.SugerirDadosPessoaNoCheque;
var
  tipoQuitacaoCheque: Boolean;
  informacoesPessoaNaoForamPreenchidas: Boolean;
begin
  if not cdsContaReceberQuitacao.EstadoDeAlteracao then
  begin
    Exit;
  end;

  tipoQuitacaoCheque :=
    cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO);

  informacoesPessoaNaoForamPreenchidas := (cdsContaReceberQuitacaoCHEQUE_SACADO.AsString.IsEmpty) and
    (cdsContaReceberQuitacaoCHEQUE_DOC_FEDERAL.AsString.IsEmpty);

  if tipoQuitacaoCheque then
  begin
    if informacoesPessoaNaoForamPreenchidas then
    begin
      FFrameQuitacaoContaReceber.FFrameQuitacaoContaReceberTipoCheque.PreencherDadosPessoa(
        cdsDataID_PESSOA.AsInteger);
    end;
  end
  else
  begin
    cdsContaReceberQuitacaoCHEQUE_SACADO.Clear;
    cdsContaReceberQuitacaoCHEQUE_DOC_FEDERAL.Clear;
  end;
end;

procedure TMovContaReceber.SugerirDataVencimentoCheque;
begin
  if not cdsContaReceberQuitacao.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    cdsContaReceberQuitacaoJOIN_TIPO_TIPO_QUITACAO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsContaReceberQuitacaoCHEQUE_DT_VENCIMENTO.AsDateTime := cdsContaReceberQuitacaoDT_QUITACAO.AsDateTime;
  end
  else
  begin
    cdsContaReceberQuitacaoCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

procedure TMovContaReceber.TotalizarAcrescimo(Sender: TField);
begin
  if (FTotalizandoAcrescimo) or (Sender.Dataset <> cdsContaReceberQuitacao) then
    exit;

  try
    FTotalizandoAcrescimo := True;

    cdsContaReceberQuitacaoVL_ACRESCIMO.AsFloat :=
      cdsContaReceberQuitacaoVL_MULTA.AsFloat +
      cdsContaReceberQuitacaoVL_JUROS.AsFloat;
  finally
    FTotalizandoAcrescimo := False;
  end;
end;

function TMovContaReceber.PodeUtilizarFiltroStatusNaPesquisaPadrao: Boolean;
begin
  result := ExistePesquisaConfiguradaParaSituacaoAberto and ExistePesquisaConfiguradaParaSituacaoQuitado;
end;

procedure TMovContaReceber.DefinirIdentificadorDaPesquisaPadrao;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_QUITADO;
    exit;
  end;

  if VartoStr(EdtConsultaSituacao.EditValue).Equals(ABERTO) then
  begin
    identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_ABERTO;
    exit;
  end;

  identificadorConfiguracaoGradePesquisa := FIDENTIFICADOR_SQL_GERAL;
end;

function TMovContaReceber.ExistePesquisaConfiguradaParaSituacaoAberto: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_ABERTO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

function TMovContaReceber.ExistePesquisaConfiguradaParaSituacaoQuitado: Boolean;
begin
  result := not DmConnection.ServerMethodsClient.GetSQLPesquisaPadrao(FIDENTIFICADOR_SQL_QUITADO,
    TSistema.Sistema.Usuario.idSeguranca).IsEmpty;
end;

procedure TMovContaReceber.ExibirBotaoCancelarBoleto;
begin
  ActCancelarEmissaoBoleto.Visible := (cdsDataBOLETO_NOSSO_NUMERO.AsInteger > 0) and (ActReport.Visible) and
    FParametroExibirDadosBoleto.ValorSim;
end;

procedure TMovContaReceber.ExibirBotaoEmitirBoleto;
begin
  ActEmitirBoleto.Visible := cdsData.Active and (cdsDataBOLETO_NOSSO_NUMERO.AsInteger <= 0) and
    (ActReport.Visible) and FParametroExibirDadosBoleto.ValorSim;
end;

procedure TMovContaReceber.ExibirDadosBoleto;
begin
  //Implementar
end;

procedure TMovContaReceber.ExibirFiltroPeriodoQuitacao;
begin
  if VartoStr(EdtConsultaSituacao.EditValue).Equals(QUITADO) then
  begin
    FFiltroVertical.ExibirFiltro('DT_QUITACAO');
  end
  else
  begin
    FFiltroVertical.OcultarFiltro('DT_QUITACAO');
  end;
end;


Initialization
  RegisterClass(TMovContaReceber);

Finalization
  UnRegisterClass(TMovContaReceber);

end.
