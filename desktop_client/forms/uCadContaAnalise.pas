unit uCadContaAnalise;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  uGBDBTextEditPK, cxMaskEdit, cxDropDownEdit, cxBlobEdit, cxDBEdit,
  uGBDBBlobEdit, cxCheckBox, uGBDBCheckBox, cxTextEdit, uGBDBTextEdit,
  Vcl.StdCtrls, cxSpinEdit, uGBDBSpinEdit;

type
  TCadContaAnalise = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataSEQUENCIA: TStringField;
    cdsDataNIVEL: TIntegerField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataBO_EXIBIR_RESUMO_CONTA: TStringField;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    Label3: TLabel;
    Label2: TLabel;
    gbDBSpinEdit1: TgbDBSpinEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBCheckBox2: TgbDBCheckBox;
    Label4: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    procedure gbDBSpinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cdsDataSEQUENCIAChange(Sender: TField);
  private
    procedure SetNivel;
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection, uStringUtils;

procedure TCadContaAnalise.cdsDataSEQUENCIAChange(Sender: TField);
begin
  inherited;
  SetNivel;
end;

procedure TCadContaAnalise.gbDBSpinEdit1KeyPress(Sender: TObject;
  var Key: Char);
const AvaiableChar: AnsiString = '0123456789.';
begin
  inherited;
  if Pos(Key, AvaiableChar) = 0 then
    Key := #0;
end;

procedure TCadContaAnalise.SetNivel;
const CARACTER_PONTO: Char = '.';
var i, nivel: Integer;
begin
  nivel := 1;
  for i := 0 to Length(cdsDataSEQUENCIA.AsString) do
  begin
    if cdsDataSEQUENCIA.AsString[i] = CARACTER_PONTO then
      inc(nivel);

    cdsDataNIVEL.AsInteger := nivel;
  end;
end;

initialization
  RegisterClass(TCadContaAnalise);
finalization
  UnRegisterClass(TCadContaAnalise);

end.
