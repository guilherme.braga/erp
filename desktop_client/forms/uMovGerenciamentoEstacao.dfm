inherited MovGerenciamentoEstacao: TMovGerenciamentoEstacao
  Caption = 'Gerenciamento de Esta'#231#227'o'
  ExplicitWidth = 952
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 936
      ExplicitHeight = 271
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object lCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object lEstacao: TLabel
            Left = 109
            Top = 8
            Width = 38
            Height = 13
            Caption = 'Esta'#231#227'o'
          end
          object dbCodigo: TgbDBTextEditPK
            Left = 45
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 60
          end
          object dbEstacao: TgbDBButtonEditFK
            Left = 151
            Top = 5
            DataBinding.DataField = 'ID_ESTACAO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Properties.ReadOnly = False
            Style.Color = 14606074
            TabOrder = 1
            gbTextEdit = descEstacao
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_ESTACAO;JOIN_DESCRICAO_ESTACAO'
            gbTableName = 'ESTACAO'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 60
          end
          object descEstacao: TgbDBTextEdit
            Left = 208
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_ESTACAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 721
          end
          inline FrameGerenciamentoEstacaoMidia1: TFrameGerenciamentoEstacaoMidia
            Left = 2
            Top = 34
            Width = 932
            Height = 235
            Align = alClient
            TabOrder = 3
            ExplicitLeft = 2
            ExplicitTop = 34
            ExplicitWidth = 932
            ExplicitHeight = 235
            inherited cxpcMain: TcxPageControl
              Width = 932
              Height = 194
              ExplicitWidth = 932
              ExplicitHeight = 194
              ClientRectBottom = 194
              ClientRectRight = 932
              inherited cxtsSearch: TcxTabSheet
                ExplicitTop = 24
                ExplicitWidth = 932
                ExplicitHeight = 170
                inherited cxGridPesquisaPadrao: TcxGrid
                  Width = 932
                  Height = 170
                  ExplicitWidth = 932
                  ExplicitHeight = 170
                end
              end
              inherited cxtsData: TcxTabSheet
                inherited cxGBDadosMain: TcxGroupBox
                  inherited dxBevel1: TdxBevel
                    ExplicitWidth = 928
                  end
                  inherited descMidia: TgbDBTextEdit
                    ExplicitWidth = 821
                    Width = 821
                  end
                end
              end
            end
            inherited PnTituloFrameDetailPadrao: TgbPanel
              Style.IsFontAssigned = True
              ExplicitWidth = 932
              Width = 932
            end
            inherited dxBarManagerPadrao: TdxBarManager
              DockControlHeights = (
                0
                0
                22
                0)
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedLeft = 292
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedLeft = 524
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 682
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 853
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 607
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      FloatClientWidth = 124
      FloatClientHeight = 216
      OneOnRow = False
      Visible = True
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 761
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspGerenciamentoEstacao'
    RemoteServer = DmConnection.dspIndoor
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataID_ESTACAO: TIntegerField
      FieldName = 'ID_ESTACAO'
      Origin = 'ID_ESTACAO'
      Required = True
    end
    object cdsDatafdqGerenciamentoEstacaoMidia: TDataSetField
      FieldName = 'fdqGerenciamentoEstacaoMidia'
    end
    object cdsDataJOIN_DESCRICAO_ESTACAO: TStringField
      FieldName = 'JOIN_DESCRICAO_ESTACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object cdsGerencimentoEstacaoMidia: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqGerenciamentoEstacaoMidia
    Params = <>
    AfterOpen = cdsGerencimentoEstacaoMidiaAfterOpen
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 552
    Top = 80
    object cdsGerencimentoEstacaoMidiaID: TAutoIncField
      FieldName = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsGerencimentoEstacaoMidiaID_MIDIA: TIntegerField
      FieldName = 'ID_MIDIA'
      Required = True
    end
    object cdsGerencimentoEstacaoMidiaID_GERENCIAMENTO_ESTACAO: TIntegerField
      FieldName = 'ID_GERENCIAMENTO_ESTACAO'
    end
    object cdsGerencimentoEstacaoMidiaJOIN_DESCRICAO_MIDIA: TStringField
      FieldName = 'JOIN_DESCRICAO_MIDIA'
      Size = 255
    end
  end
end
