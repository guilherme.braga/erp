inherited CadCidade: TCadCidade
  Caption = 'Cadastro de Cidade'
  ClientWidth = 908
  ExplicitWidth = 924
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 908
    ExplicitWidth = 908
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 908
    Properties.ActivePage = cxtsData
    ExplicitWidth = 908
    ClientRectRight = 908
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 908
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 908
        ExplicitWidth = 908
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 908
      inherited DesignPanel: TJvDesignPanel
        Width = 908
        ExplicitWidth = 908
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 908
          Width = 908
          inherited dxBevel1: TdxBevel
            Width = 904
            ExplicitWidth = 904
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label1: TLabel
            Left = 8
            Top = 65
            Width = 23
            Height = 13
            Caption = 'IBGE'
          end
          object Label2: TLabel
            Left = 129
            Top = 65
            Width = 33
            Height = 13
            Caption = 'Estado'
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 60
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 60
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 839
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 60
            Top = 62
            DataBinding.DataField = 'IBGE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 58
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 168
            Top = 62
            DataBinding.DataField = 'ID_ESTADO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 3
            gbTextEdit = gbDBTextEdit3
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_ESTADO;JOIN_UF_ESTADO'
            gbTableName = 'ESTADO'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 60
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 225
            Top = 62
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_UF_ESTADO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 674
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Cidade'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspCidade'
    RemoteServer = DmConnection.dspLocalidade
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataIBGE: TIntegerField
      FieldName = 'IBGE'
      Origin = 'IBGE'
    end
    object cdsDataID_ESTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Estado'
      FieldName = 'ID_ESTADO'
      Origin = 'ID_ESTADO'
      Required = True
    end
    object cdsDataJOIN_UF_ESTADO: TStringField
      DisplayLabel = 'UF'
      FieldName = 'JOIN_UF_ESTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
