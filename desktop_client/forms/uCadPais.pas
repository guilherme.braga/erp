unit uCadPais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridCustomPopupMenu,
  cxGridPopupMenu, Vcl.Menus, UCBase, Datasnap.DBClient, frxClass, frxDBSet,
  dxBar, System.Actions, Vcl.ActnList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, dxRibbon,
  dxBarBuiltInMenu, cxContainer, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxPC,
  uGBClientDataset, cxGridBandedTableView, cxGridDBBandedTableView,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.JSON, Vcl.Grids, Vcl.DBGrids, cxTextEdit, cxDBEdit, Vcl.StdCtrls,
  uGBDBTextEditPK, dxBevel, uGBDBTextEdit, cxMaskEdit, cxButtonEdit;

type
  TCadPais = class(TFrmCadastro_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDatadescricao: TStringField;
    cdsDataID: TAutoIncField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TCadPais);
finalization
  UnRegisterClass(TCadPais);

end.
