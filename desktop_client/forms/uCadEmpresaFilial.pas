unit uCadEmpresaFilial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxButtonEdit, cxDBEdit,
  uGBDBButtonEditFK, uGBDBTextEdit, cxDropDownEdit, cxBlobEdit, uGBDBBlobEdit,
  cxTextEdit, cxMaskEdit, uGBDBComboBox, Vcl.StdCtrls, uGBPanel,
  uGBDBTextEditPK, cxCalendar, uGBDBDateEdit, uFrameEmpresaFilial, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore,
  dxPScxCommon, cxSplitter, JvExControls, JvButton, JvTransparentButton;

type
  TCadEmpresaFilial = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDatafdqFilial: TDataSetField;
    cdsFilial: TGBClientDataSet;
    cdsFilialID: TAutoIncField;
    cdsFilialFANTASIA: TStringField;
    cdsFilialRAZAO_SOCIAL: TStringField;
    cdsFilialCNPJ: TStringField;
    cdsFilialIE: TStringField;
    cdsFilialDT_FUNDACAO: TDateField;
    cdsFilialID_EMPRESA: TIntegerField;
    cdsFilialNR_ITEM: TIntegerField;
    cdsFilialID_ZONEAMENTO: TIntegerField;
    cdsFilialREGIME_TRIBUTARIO: TStringField;
    cdsFilialJOIN_DESCRICAO_ZONEAMENTO: TStringField;
    cdsFilialBO_MATRIZ: TStringField;
    cdsFilialID_CIDADE: TIntegerField;
    cdsFilialLOGRADOURO: TStringField;
    cdsFilialNUMERO: TStringField;
    cdsFilialCOMPLEMENTO: TStringField;
    cdsFilialBAIRRO: TStringField;
    cdsFilialCEP: TStringField;
    cdsFilialJOIN_DESCRICAO_CIDADE: TStringField;
    PnDadosEmpresa: TcxGroupBox;
    lbRazaoSocial: TLabel;
    edRazaoSocial: TgbDBTextEdit;
    lbCodigo: TLabel;
    edtCodigo: TgbDBTextEditPK;
    PnFrameEmpresaFilial: TgbPanel;
    cdsDataDESCRICAO: TStringField;
    cdsFilialTELEFONE: TStringField;
    cdsFilialTIPO_EMPRESA: TStringField;
    cdsFilialCRT: TIntegerField;
    cdsFilialBO_CONTRIBUINTE_ICMS: TStringField;
    cdsFilialBO_CONTRIBUINTE_IPI: TStringField;
    cdsFilialEMAIL: TStringField;
    cdsFilialSITE: TStringField;
    cdsFilialID_CNAE: TIntegerField;
    cdsFilialIM: TStringField;
    cdsFilialJOIN_SEQUENCIA_CNAE: TStringField;
    cdsFilialJOIN_DESCRICAO_CNAE: TStringField;
    procedure cdsFilialNewRecord(DataSet: TDataSet);
  private
    FFrameEmpresaFilial: TFrameEmpresaFilial;
  public
    procedure InstanciarFrames; override;
  end;

var
  CadEmpresaFilial: TCadEmpresaFilial;

implementation

{$R *.dfm}

uses uDmConnection, uCadPessoa, uTPessoa, uDatasetUtils, uConstantes, uConstantesFiscaisProxy;

procedure TCadEmpresaFilial.cdsFilialNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('NR_ITEM').AsFloat :=
    (TDatasetUtils.MaiorValor(DataSet.FieldByName('NR_ITEM'))+1);

  {Regime Fiscal}
  DataSet.FieldByName('TIPO_EMPRESA').AsString := TConstantesFiscais.TIPO_EMPRESA_PRIVADA;
  DataSet.FieldByName('REGIME_TRIBUTARIO').AsInteger := TConstantesFiscais.REGIME_TRIBUTARIO_SIMPLES_NACIONAL;
  DataSet.FieldByName('CRT').AsInteger := TConstantesFiscais.CRT_SIMPLES_NACIONAL;
end;

procedure TCadEmpresaFilial.InstanciarFrames;
begin
  inherited;
  FFrameEmpresaFilial := TFrameEmpresaFilial.Create(PnFrameEmpresaFilial);
  FFrameEmpresaFilial.Parent := PnFrameEmpresaFilial;
  FFrameEmpresaFilial.Align := alClient;
  FFrameEmpresaFilial.SetConfiguracoesIniciais(cdsFilial);
end;

initialization
  RegisterClass(TCadEmpresaFilial);

finalization
  UnRegisterClass(TCadEmpresaFilial);

end.
