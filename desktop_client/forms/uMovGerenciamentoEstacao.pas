unit uMovGerenciamentoEstacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxMaskEdit,
  cxButtonEdit, cxDBEdit, uGBDBButtonEditFK, cxTextEdit, uGBDBTextEditPK,
  Vcl.StdCtrls, uFrameDetailPadrao, uFrameGerenciamentoEstacaoMidia;

type
  TMovGerenciamentoEstacao = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataID_ESTACAO: TIntegerField;
    cdsDatafdqGerenciamentoEstacaoMidia: TDataSetField;
    lCodigo: TLabel;
    dbCodigo: TgbDBTextEditPK;
    lEstacao: TLabel;
    dbEstacao: TgbDBButtonEditFK;
    descEstacao: TgbDBTextEdit;
    cdsGerencimentoEstacaoMidia: TGBClientDataSet;
    cdsGerencimentoEstacaoMidiaID: TAutoIncField;
    cdsGerencimentoEstacaoMidiaID_MIDIA: TIntegerField;
    cdsGerencimentoEstacaoMidiaID_GERENCIAMENTO_ESTACAO: TIntegerField;
    FrameGerenciamentoEstacaoMidia1: TFrameGerenciamentoEstacaoMidia;
    cdsDataJOIN_DESCRICAO_ESTACAO: TStringField;
    cdsGerencimentoEstacaoMidiaJOIN_DESCRICAO_MIDIA: TStringField;
    procedure cdsGerencimentoEstacaoMidiaAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection, System.Generics.Collections;

procedure TMovGerenciamentoEstacao.cdsGerencimentoEstacaoMidiaAfterOpen(
  DataSet: TDataSet);
begin
  inherited;
  FrameGerenciamentoEstacaoMidia1.SetConfiguracoesIniciais(cdsGerencimentoEstacaoMidia);
end;

initialization
  RegisterClass(TMovGerenciamentoEstacao);
finalization
  UnRegisterClass(TMovGerenciamentoEstacao);

end.
