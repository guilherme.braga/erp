inherited CadTipoQuitacao: TCadTipoQuitacao
  Caption = 'Cadastro de Tipo de Quita'#231#227'o'
  ClientWidth = 923
  ExplicitWidth = 939
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 923
    ExplicitWidth = 923
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 923
    Properties.ActivePage = cxtsData
    ExplicitWidth = 923
    ClientRectRight = 923
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 923
      ExplicitHeight = 271
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 923
        ExplicitWidth = 923
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 923
      inherited DesignPanel: TJvDesignPanel
        Width = 923
        ExplicitWidth = 923
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 923
          DesignSize = (
            923
            271)
          Width = 923
          inherited dxBevel1: TdxBevel
            Width = 919
            ExplicitWidth = 919
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelDescricao: TLabel
            Left = 8
            Top = 42
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object edtCodigo: TgbDBTextEditPK
            Left = 60
            Top = 5
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edtDescricao: TgbDBTextEdit
            Left = 60
            Top = 38
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 792
          end
          object cbAtivo: TgbDBCheckBox
            Left = 859
            Top = 40
            Anchors = [akTop, akRight]
            Caption = 'Ativo'
            DataBinding.DataField = 'BO_ATIVO'
            DataBinding.DataSource = dsData
            Properties.ImmediatePost = True
            Properties.ValueChecked = 'S'
            Properties.ValueUnchecked = 'N'
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            Width = 57
          end
          object rgTipo: TgbDBRadioGroup
            Left = 60
            Top = 65
            Anchors = [akLeft, akTop, akRight]
            Caption = 'Tipo'
            DataBinding.DataField = 'TIPO'
            DataBinding.DataSource = dsData
            Properties.Columns = 4
            Properties.Items = <
              item
                Caption = 'Dinheiro'
                Value = 'DINHEIRO'
              end
              item
                Caption = 'Cheque Pr'#243'prio'
                Value = 'CHEQUE_PROPRIO'
              end
              item
                Caption = 'Cheque Terceiro'
                Value = 'CHEQUE_TERCEIRO'
              end
              item
                Caption = 'Cart'#227'o de Cr'#233'dito'
                Value = 'CREDITO'
              end
              item
                Caption = 'Cart'#227'o de D'#233'bito'
                Value = 'DEBITO'
              end
              item
                Caption = 'Cr'#233'dito de Pessoa'
                Value = 'CREDITO_PESSOA'
              end
              item
                Caption = 'Sem Movimento'
                Value = 'SEM_MOVIMENTO'
              end>
            Style.BorderStyle = ebsOffice11
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Height = 72
            Width = 792
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Tipo de Quita'#231#227'o'
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '1'
      end>
    ProviderName = 'dspTipoQuitacao'
    RemoteServer = DmConnection.dspFinanceiro
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsDataTIPO: TStringField
      DefaultExpression = 'DINHEIRO'
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 50
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo?'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
end
