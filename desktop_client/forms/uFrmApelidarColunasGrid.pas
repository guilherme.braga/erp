unit uFrmApelidarColunasGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxDBNavigator,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons, cxGroupBox,
  Datasnap.DBClient, cxGridDBBandedTableView, cxDropDownEdit, dxCore,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter;

type
  TFrmApelidarColunasGrid = class(TFrmModalPadrao)
    cxgdMain: TcxGrid;
    cxgdMainDBTableView1: TcxGridDBTableView;
    cxgdMainDBTableView1Column1: TcxGridDBColumn;
    cxgdMainDBTableView1Column2: TcxGridDBColumn;
    cxgdMainLevel1: TcxGridLevel;
    cdsColunas: TClientDataSet;
    cdsColunasname: TStringField;
    cdsColunasnickname: TStringField;
    dsColunas: TDataSource;
    cdsColunasalinhamento: TStringField;
    cxgdMainDBTableView1Column3: TcxGridDBColumn;
    cdsColunasordenacao: TStringField;
    cxgdMainDBTableView1Column4: TcxGridDBColumn;
    procedure ActConfirmarExecute(Sender: TObject);
  private
    FView: TcxGridDBBandedTableView;
    function GetDescricaoAlinhamento(AAlinhamento: TAlignment): String;
    function GetAlinhamentoOriginal(ADescricaoAlinhamento: String): TAlignment;
    function GetDescricaoOrdenacao(AOrdenacao: TDxSortOrder): String;
    function GetOrdenacaoOriginal(ADescricaoOrdenacao: String): TDxSortOrder;
  public
    class procedure SetColumns(AView: TcxGridDBBandedTableView); static;
  end;

var
  FrmApelidarColunasGrid: TFrmApelidarColunasGrid;

implementation

{$R *.dfm}

procedure TFrmApelidarColunasGrid.ActConfirmarExecute(Sender: TObject);
var i: Integer;
begin
  for i := 0 to Pred(FView.ColumnCount) do
    if cdsColunas.Locate('name', FView.Columns[i].DataBinding.FieldName, []) then
    begin
      FView.Columns[i].Caption := cdsColunas.FieldByName('nickname').AsString;
      FView.Columns[i].HeaderAlignmentHorz := GetAlinhamentoOriginal(cdsColunas.FieldByName('alinhamento').AsString);
      FView.Columns[i].SortOrder := GetOrdenacaoOriginal(cdsColunas.FieldByName('ordenacao').AsString);
    end;
  inherited;
end;

function TFrmApelidarColunasGrid.GetAlinhamentoOriginal(
  ADescricaoAlinhamento: String): TAlignment;
begin
  if ADescricaoAlinhamento.Equals('Centro') then
    result := taCenter
  else if ADescricaoAlinhamento.Equals('Esquerda') then
    result := taLeftJustify
  else if ADescricaoAlinhamento.Equals('Direita') then
    result := taRightJustify;
end;

function TFrmApelidarColunasGrid.GetDescricaoAlinhamento(
  AAlinhamento: TAlignment): String;
begin
  if AAlinhamento = taCenter then
    result := 'Centro'
  else if AAlinhamento = taLeftJustify then
    result := 'Esquerda'
  else if AAlinhamento = taRightJustify then
    result := 'Direita';
end;

function TFrmApelidarColunasGrid.GetDescricaoOrdenacao(
  AOrdenacao: TDxSortOrder): String;
begin
  if AOrdenacao = soNone then
    result := 'Nenhuma'
  else if AOrdenacao = soAscending then
    result := 'Crescente'
  else if AOrdenacao = soDescending then
    result := 'Decrescente';
end;

function TFrmApelidarColunasGrid.GetOrdenacaoOriginal(
  ADescricaoOrdenacao: String): TDxSortOrder;
begin
  if ADescricaoOrdenacao.Equals('Nenhuma') then
    result := soNone
  else if ADescricaoOrdenacao.Equals('Crescente') then
    result := soAscending
  else if ADescricaoOrdenacao.Equals('Decrescente') then
    result := soDescending;
end;

class procedure TFrmApelidarColunasGrid.SetColumns(AView: TcxGridDBBandedTableView);
var i: Integer;
begin
  Application.CreateForm(TFrmApelidarColunasGrid, FrmApelidarColunasGrid);
  try
    if FrmApelidarColunasGrid <> nil then
      with (FrmApelidarColunasGrid) do
      begin
        cdsColunas.CreateDataSet;
        FView := AView;

        for i := 0 to Pred(FView.ColumnCount) do
          begin
            cdsColunas.Append;
            cdsColunas.FieldByName('name').AsString := FView.Columns[i].DataBinding.FieldName;
            cdsColunas.FieldByName('nickname').AsString := FView.Columns[i].Caption;
            cdsColunas.FieldByName('alinhamento').AsString := GetDescricaoAlinhamento(FView.Columns[i].HeaderAlignmentHorz);
            cdsColunas.FieldByName('ordenacao').AsString := GetDescricaoOrdenacao(FView.Columns[i].SortOrder);
            cdsColunas.Post;
          end;

        cdsColunas.First;
      end;
    FrmApelidarColunasGrid.ShowModal;
  finally
    FreeAndNil(FrmApelidarColunasGrid);
  end;
end;

end.
