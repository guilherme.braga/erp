unit uMovVendaVarejo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, dxBarBuiltInMenu, uGBPanel,
  cxPC, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxLabel, cxTextEdit, cxDBEdit, uGBDBTextEdit, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxCheckBox, cxGridDBTableView,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset,
  cxMaskEdit, cxButtonEdit, uGBDBButtonEditFK, Vcl.StdCtrls, DateUtils,
  dxBarExtItems, cxBarEditItem, System.Generics.Collections, uFiltroFormulario,
  cxCalendar, cxCurrencyEdit, StrUtils, Vcl.Menus, cxGridCustomPopupMenu,
  cxGridPopupMenu, JvComponentBase, JvEnterTab, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, uUsuarioDesignControl, uFramePadrao,
  uFrameVendaVarejoFechamentoCrediario, UCBase, uFrameVendaVarejoReceitaOtica, uVendaProxy,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, uGBLabel, Vcl.ExtCtrls;

type TEStadoProduto = (Incluindo, Alterando, Exluindo, Nenhum);

type TTipoOperacaoInclusao = (Venda, DevolucaoCondicional);

type
  TMovVendaVarejo = class(TFrmChildPadrao)
    cxpcMain: TcxPageControl;
    tsVenda: TcxTabSheet;
    tsFechamento: TcxTabSheet;
    PnTopo: TgbPanel;
    tsPesquisa: TcxTabSheet;
    cxGridPesquisaPadrao: TcxGrid;
    viewPesquisa: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    PnProduto: TgbPanel;
    EdtDescricaoProduto: TgbDBTextEdit;
    cxLabel1: TcxLabel;
    PnDesconto: TgbPanel;
    EdtPercentualDesconto: TgbDBTextEdit;
    PnCodigo: TgbPanel;
    cxLabel3: TcxLabel;
    PnValor: TgbPanel;
    EdtValorUnitarioProduto: TgbDBTextEdit;
    cxLabel4: TcxLabel;
    PnQuantidade: TgbPanel;
    EdtQuantidadeProduto: TgbDBTextEdit;
    PnTotal: TgbPanel;
    EdtValorTotalProduto: TgbDBTextEdit;
    cxLabel2: TcxLabel;
    PnCentro: TgbPanel;
    fdmProduto: TFDMemTable;
    fdmProdutoCODIGO: TStringField;
    fdmProdutoPRODUTO: TStringField;
    fdmProdutoVALOR: TFloatField;
    fdmProdutoQUANTIDADE: TFloatField;
    fdmProdutoPERC_DESCONTO: TFloatField;
    fdmProdutoTOTAL: TFloatField;
    PnProdutos: TgbPanel;
    cxGridProdutos: TcxGrid;
    cxGridLevel1: TcxGridLevel;
    cxStyleRepository: TcxStyleRepository;
    cxStyleYellowStrong: TcxStyle;
    cxStyleYellowTooStrong: TcxStyle;
    fdmProdutoVL_DESCONTO: TFloatField;
    dsProduto: TDataSource;
    cdsVenda: TGBClientDataSet;
    dsVenda: TDataSource;
    cdsVendaID: TAutoIncField;
    cdsVendaDH_CADASTRO: TDateTimeField;
    cdsVendaVL_DESCONTO: TFMTBCDField;
    cdsVendaVL_ACRESCIMO: TFMTBCDField;
    cdsVendaVL_TOTAL_PRODUTO: TFMTBCDField;
    cdsVendaVL_VENDA: TFMTBCDField;
    cdsVendaSTATUS: TStringField;
    cdsVendaID_PESSOA: TIntegerField;
    cdsVendaID_FILIAL: TIntegerField;
    cdsVendaID_OPERACAO: TIntegerField;
    cdsVendaID_CHAVE_PROCESSO: TIntegerField;
    cdsVendaID_CENTRO_RESULTADO: TIntegerField;
    cdsVendaID_CONTA_ANALISE: TIntegerField;
    cdsVendaID_PLANO_PAGAMENTO: TIntegerField;
    cdsVendaID_FORMA_PAGAMENTO: TIntegerField;
    cdsVendaPERC_DESCONTO: TFMTBCDField;
    cdsVendaPERC_ACRESCIMO: TFMTBCDField;
    cdsVendaJOIN_NOME_PESSOA: TStringField;
    cdsVendaJOIN_DESCRICAO_OPERACAO: TStringField;
    cdsVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField;
    cdsVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    cdsVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsVendafdqVendaParcela: TDataSetField;
    cdsVendafdqVendaItem: TDataSetField;
    cdsVendaItem: TGBClientDataSet;
    cdsVendaParcela: TGBClientDataSet;
    cdsVendaItemID: TAutoIncField;
    cdsVendaItemID_VENDA: TIntegerField;
    cdsVendaItemID_PRODUTO: TIntegerField;
    cdsVendaItemNR_ITEM: TIntegerField;
    cdsVendaItemQUANTIDADE: TFMTBCDField;
    cdsVendaItemVL_DESCONTO: TFMTBCDField;
    cdsVendaItemVL_ACRESCIMO: TFMTBCDField;
    cdsVendaItemVL_BRUTO: TFMTBCDField;
    cdsVendaItemVL_LIQUIDO: TFMTBCDField;
    cdsVendaItemPERC_DESCONTO: TFMTBCDField;
    cdsVendaItemPERC_ACRESCIMO: TFMTBCDField;
    cdsVendaItemJOIN_DESCRICAO_PRODUTO: TStringField;
    cdsVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    cdsVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    cdsVendaParcelaID: TAutoIncField;
    cdsVendaParcelaID_VENDA: TIntegerField;
    cdsVendaParcelaDOCUMENTO: TStringField;
    cdsVendaParcelaVL_TITULO: TFMTBCDField;
    cdsVendaParcelaQT_PARCELA: TIntegerField;
    cdsVendaParcelaNR_PARCELA: TIntegerField;
    cdsVendaParcelaDT_VENCIMENTO: TDateField;
    dsVendaItem: TDataSource;
    dsVendaParcela: TDataSource;
    cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    fdmProdutoNR_ITEM: TIntegerField;
    cdsVendaID_TABELA_PRECO: TIntegerField;
    cdsVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    ActVenda: TAction;
    ActEfetivarVenda: TAction;
    ActVoltar: TAction;
    lbVoltar: TdxBarLargeButton;
    lbFechamentoVenda: TdxBarLargeButton;
    lbEfetivar: TdxBarLargeButton;
    PnParcela: TgbPanel;
    cdsVendaParcelaIC_DIAS: TIntegerField;
    PnInformacoesLateraisDireita: TgbPanel;
    PnValorBruto: TgbPanel;
    gbDBTextEdit13: TgbDBTextEdit;
    cxLabel7: TcxLabel;
    PnTotalDesconto: TgbPanel;
    gbDBTextEdit12: TgbDBTextEdit;
    cxLabel6: TcxLabel;
    PnValorLiquido: TgbPanel;
    gbDBTextEdit11: TgbDBTextEdit;
    cxLabel5: TcxLabel;
    gbPanel2: TgbPanel;
    Label2: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbPanel3: TgbPanel;
    Label5: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    EdtVendedor: TgbDBButtonEditFK;
    ActPesquisar: TAction;
    ActDevolucao: TAction;
    ActOrcamento: TAction;
    ActPedido: TAction;
    ActCondicional: TAction;
    lbDevolucao: TdxBarLargeButton;
    lbOrcamento: TdxBarLargeButton;
    lbPedido: TdxBarLargeButton;
    lbCondicional: TdxBarLargeButton;
    lbPesquisar: TdxBarLargeButton;
    barConsultar: TdxBar;
    ActCancelarVenda: TAction;
    lbCancelarDocumento: TdxBarLargeButton;
    filtroDataCadastroInicial: TcxBarEditItem;
    filtroDataCadastroFIm: TcxBarEditItem;
    filtroDataDocumentoInicio: TcxBarEditItem;
    filtroDataDocumentoFim: TcxBarEditItem;
    filtroTipoDocumento: TdxBarCombo;
    filtroSituacao: TdxBarCombo;
    filtroLabelDataCadastro: TdxBarStatic;
    filtroLabelDataDocumento: TdxBarStatic;
    filtroCodigoVenda: TcxBarEditItem;
    filtroPessoa: TcxBarEditItem;
    filtroNomePessoa: TdxBarStatic;
    fdmSearch: TFDMemTable;
    dsSearch: TDataSource;
    ActImprimir: TAction;
    BarInformacoes: TdxBar;
    informacoesCodigo: TdxBarStatic;
    informacoesSituacao: TdxBarStatic;
    cdsVendaTIPO: TStringField;
    cdsVendaDH_FECHAMENTO: TDateTimeField;
    lbImpressao: TdxBarLargeButton;
    lbCancelar: TdxBarLargeButton;
    ActionListAssistent: TActionList;
    ActSalvarConfiguracoes: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActAlterarColunasGrid: TAction;
    ActExibirAgrupamento: TAction;
    ActAbrirConsultando: TAction;
    ActAlterarSQLPesquisaPadrao: TAction;
    ActFullExpand: TAction;
    ActConfigurarValoresDefault: TAction;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    ExibirAgrupamento2: TMenuItem;
    RestaurarColunasPadrao: TMenuItem;
    SalvarConfiguraes1: TMenuItem;
    pmGridConsultaPadrao: TPopupMenu;
    AlterarSQL1: TMenuItem;
    ExibirAgrupamento1: TMenuItem;
    PopUpControllerPesquisa: TcxGridPopupMenu;
    fdmProdutoID_PRODUTO: TIntegerField;
    ActExcluir: TAction;
    lbExcluir: TdxBarLargeButton;
    JvEnterAsTab1: TJvEnterAsTab;
    EdtCodigoProduto: TgbDBButtonEditFK;
    viewProduto: TcxGridDBBandedTableView;
    viewProdutoID_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoNR_ITEM: TcxGridDBBandedColumn;
    viewProdutoQUANTIDADE: TcxGridDBBandedColumn;
    viewProdutoVL_DESCONTO: TcxGridDBBandedColumn;
    viewProdutoVL_ACRESCIMO: TcxGridDBBandedColumn;
    viewProdutoVL_BRUTO: TcxGridDBBandedColumn;
    viewProdutoVL_LIQUIDO: TcxGridDBBandedColumn;
    viewProdutoPERC_DESCONTO: TcxGridDBBandedColumn;
    viewProdutoPERC_ACRESCIMO: TcxGridDBBandedColumn;
    viewProdutoJOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TcxGridDBBandedColumn;
    viewProdutoJOIN_ESTOQUE_PRODUTO: TcxGridDBBandedColumn;
    viewProdutoJOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn;
    PopUpControllerFechamento: TcxGridPopupMenu;
    PopUpControllerProduto: TcxGridPopupMenu;
    gbDBTextEdit4: TgbDBTextEdit;
    pmTela: TPopupMenu;
    ConfigurarValoresPadres1: TMenuItem;
    cdsVendaParcelaID_OPERADORA_CARTAO: TIntegerField;
    cdsVendaParcelaCHEQUE_NUMERO: TIntegerField;
    cdsVendaParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    cdsVendaParcelaCHEQUE_AGENCIA: TStringField;
    cdsVendaParcelaCHEQUE_DT_EMISSAO: TDateField;
    cdsVendaParcelaCHEQUE_BANCO: TStringField;
    cdsVendaParcelaCHEQUE_DOC_FEDERAL: TStringField;
    cdsVendaParcelaCHEQUE_SACADO: TStringField;
    cdsVendaParcelaOBSERVACAO: TStringField;
    PnPlanoPagamento: TgbPanel;
    Label1: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    EdtPlanoPagamento: TgbDBButtonEditFK;
    cdsVendaVL_PAGAMENTO: TFMTBCDField;
    cdsVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField;
    cdsVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField;
    cdsVendaParcelaID_FORMA_PAGAMENTO: TIntegerField;
    cdsVendaParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField;
    cdsVendaCC_SALDO_PESSOA_CREDITO: TFloatField;
    cdsVendaCC_VL_DIFERENCA: TFloatField;
    cdsVendaCC_VL_TROCO: TFloatField;
    cdsVendaParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    bbInserirValoresPadrao: TdxBarButton;
    cdsVendaID_VENDEDOR: TIntegerField;
    cdsVendaJOIN_NOME_VENDEDOR: TStringField;
    cdsVendaParcelaID_CARTEIRA: TIntegerField;
    cdsVendaParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    UCCadastro_Padrao: TUCControls;
    tsReceitaOtica: TcxTabSheet;
    PnFrameVendaVarejoReceitaOtica: TgbPanel;
    cdsVendafdqVendaReceitaOtica: TDataSetField;
    cdsVendaReceitaOtica: TGBClientDataSet;
    cdsVendaReceitaOticaID: TAutoIncField;
    cdsVendaReceitaOticaID_RECEITA_OTICA: TIntegerField;
    cdsVendaReceitaOticaID_VENDA: TIntegerField;
    cdsVendaReceitaOticaJOIN_ID_PESSOA: TIntegerField;
    cdsVendaReceitaOticaJOIN_NOME_PESSOA: TStringField;
    cdsVendaReceitaOticaJOIN_ID_MEDICO: TIntegerField;
    cdsVendaReceitaOticaJOIN_NOME_MEDICO: TStringField;
    cdsVendaReceitaOticaJOIN_ESPECIFICACAO_RECEITA_OTICA: TBlobField;
    lbReceitaOtica: TdxBarLargeButton;
    ActReceitaOtica: TAction;
    ActNovaVenda: TAction;
    lbNovaVenda: TdxBarLargeButton;
    gbPanel4: TgbPanel;
    Label3: TLabel;
    gbDBTextEdit5: TgbDBTextEdit;
    EdtCliente: TgbDBButtonEditFK;
    tsDevolucaoCondicional: TcxTabSheet;
    gbPanel12: TgbPanel;
    gridDevolucaoCondicional: TcxGrid;
    viewDevolucaoCondicional: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    cxGridDBBandedColumn6: TcxGridDBBandedColumn;
    cxGridDBBandedColumn7: TcxGridDBBandedColumn;
    cxGridDBBandedColumn8: TcxGridDBBandedColumn;
    cxGridDBBandedColumn9: TcxGridDBBandedColumn;
    cxGridDBBandedColumn10: TcxGridDBBandedColumn;
    cxGridDBBandedColumn11: TcxGridDBBandedColumn;
    cxGridDBBandedColumn12: TcxGridDBBandedColumn;
    cxGridDBBandedColumn13: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    PnTituloDevolucaoCondicional: TgbPanel;
    ActDevolucaoCondicional: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    gbPanel5: TgbPanel;
    gbPanel6: TgbPanel;
    gbDBTextEdit6: TgbDBTextEdit;
    cxLabel8: TcxLabel;
    PnCodigoProdutoDevolucaoCondicional: TgbPanel;
    cxLabel9: TcxLabel;
    cdsVendaItemDevolucao: TGBClientDataSet;
    dsVendaItemDevolucao: TDataSource;
    cdsVendafdqVendaItemDevolucao: TDataSetField;
    cdsVendaItemDevolucaoID: TAutoIncField;
    cdsVendaItemDevolucaoID_VENDA: TIntegerField;
    cdsVendaItemDevolucaoID_PRODUTO: TIntegerField;
    cdsVendaItemDevolucaoNR_ITEM: TIntegerField;
    cdsVendaItemDevolucaoQUANTIDADE: TFMTBCDField;
    cdsVendaItemDevolucaoVL_DESCONTO: TFMTBCDField;
    cdsVendaItemDevolucaoVL_ACRESCIMO: TFMTBCDField;
    cdsVendaItemDevolucaoVL_BRUTO: TFMTBCDField;
    cdsVendaItemDevolucaoVL_LIQUIDO: TFMTBCDField;
    cdsVendaItemDevolucaoPERC_DESCONTO: TFMTBCDField;
    cdsVendaItemDevolucaoPERC_ACRESCIMO: TFMTBCDField;
    cdsVendaItemDevolucaoJOIN_DESCRICAO_PRODUTO: TStringField;
    cdsVendaItemDevolucaoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    cdsVendaItemDevolucaoJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    cdsVendaItemDevolucaoJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    EdtCodigoProdutoDevolucaoCondicional: TcxDBButtonEdit;
    pnProdutosEsquerda: TgbPanel;
    cdsVendaParcelaCHEQUE_DT_VENCIMENTO: TDateField;
    lbRSDesconto: TcxLabel;
    cxLabel10: TcxLabel;
    cdsVendaItemDESCRICAO_PRODUTO: TStringField;
    cdsVendaID_NOTA_FISCAL_NFE: TIntegerField;
    cdsVendaID_OPERACAO_FISCAL: TIntegerField;
    cdsVendaID_NOTA_FISCAL_NFSE: TIntegerField;
    cdsVendaID_NOTA_FISCAL_NFCE: TIntegerField;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    PnOperacao: TgbPanel;
    lbOperacao: TLabel;
    EdtDescricaoOperacao: TgbDBTextEdit;
    EdtCodigoOperacao: TgbDBButtonEditFK;
    PnTotalizadorConsulta: TFlowPanel;
    PnTotalVenda: TgbLabel;
    PnTotalPedido: TgbLabel;
    fdmProdutoVL_VENDA: TFloatField;
    PnTotalOrcamento: TgbLabel;
    PnTotalCondicional: TgbLabel;
    PnTotalDevolucao: TgbLabel;
    ActLiberarBloqueios: TAction;
    cdsVendaBloqueioPersonalizado: TGBClientDataSet;
    cdsVendafdqVendaBloqueioPersonalizado: TDataSetField;
    cdsVendaBloqueioPersonalizadoID: TAutoIncField;
    cdsVendaBloqueioPersonalizadoID_BLOQUEIO_PERSONALIZADO: TIntegerField;
    cdsVendaBloqueioPersonalizadoID_VENDA: TIntegerField;
    cdsVendaBloqueioPersonalizadoSTATUS: TStringField;
    cdsVendaBloqueioPersonalizadoDH_BLOQUEIO: TDateTimeField;
    cdsVendaBloqueioPersonalizadoDH_LIBERACAO: TDateTimeField;
    cdsVendaBloqueioPersonalizadoID_PESSOA_USUARIO: TIntegerField;
    cdsVendaBloqueioPersonalizadoJOIN_NOME_PESSOA_USUARIO: TStringField;
    cdsVendaBloqueioPersonalizadoJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TStringField;
    lbLiberarBloqueioPersonalizado: TdxBarLargeButton;
    procedure CalcularDescontoPeloPercentual(Sender: TField);
    procedure BuscarDadosProdutoPorCodigoBarra(Sender: TField);
    procedure BuscarProdutosDaTabelaPreco(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure cxGridProdutosViewDblClick(Sender: TObject);
    procedure cxGridProdutosViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsVendaItemAfterDelete(DataSet: TDataSet);
    procedure ActVendaExecute(Sender: TObject);
    procedure ActEfetivarVendaExecute(Sender: TObject);
    procedure ActVoltarExecute(Sender: TObject);
    procedure AlterandoDiasParcelamento(Sender: TField);
    procedure AlterandoDataParcelamento(Sender: TField);
    procedure cdsVendaNewRecord(DataSet: TDataSet);
    procedure GerarParcela(Sender: TField);
    procedure EdtPercentualDescontoExit(Sender: TObject);
    procedure AoAlterarQuantidade(Sender: TField);
    procedure cdsVendaItemAfterPost(DataSet: TDataSet);
    procedure ActDevolucaoExecute(Sender: TObject);
    procedure ActOrcamentoExecute(Sender: TObject);
    procedure ActPedidoExecute(Sender: TObject);
    procedure ActCondicionalExecute(Sender: TObject);
    procedure ActCancelarVendaExecute(Sender: TObject);
    procedure fdmSearchAfterClose(DataSet: TDataSet);
    procedure fdmSearchAfterDelete(DataSet: TDataSet);
    procedure fdmSearchAfterOpen(DataSet: TDataSet);
    procedure cdsVendaAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure viewPesquisaDblClick(Sender: TObject);
    procedure viewPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ActCancelExecute(Sender: TObject);
    procedure ActPesquisarExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure ActAbrirConsultandoExecute(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure pmGridConsultaPadraoPopup(Sender: TObject);
    procedure pmTituloGridPesquisaPadraoPopup(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActExcluirExecute(Sender: TObject);
    procedure gbDBTextEdit12Enter(Sender: TObject);
    procedure gbDBTextEdit11Enter(Sender: TObject);
    procedure ActConfigurarValoresDefaultExecute(Sender: TObject);
    procedure InserirValoresPadrao(DataSet: TDataSet);
    procedure cdsVendaCalcFields(DataSet: TDataSet);
    procedure AtualizarContadorReceitaOtica(Dataset: TDataset);
    procedure cdsVendaParcelaAfterPost(DataSet: TDataSet);
    procedure cdsVendaPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure ActReceitaOticaExecute(Sender: TObject);
    procedure ActNovaVendaExecute(Sender: TObject);
    procedure GerenciarSomenteLeitura(Dataset: TDataset);
    procedure EdtQuantidadeProdutoEnter(Sender: TObject);
    procedure filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure filtroPessoaPropertiesEditValueChanged(Sender: TObject);
    procedure ActImprimirExecute(Sender: TObject);
    procedure ActDevolucaoCondicionalExecute(Sender: TObject);
    procedure cdsVendaItemDevolucaoBeforeDelete(DataSet: TDataSet);
    procedure cdsVendaItemDevolucaoBeforeEdit(DataSet: TDataSet);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure viewDevolucaoCondicionalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cdsVendaItemDevolucaoAfterPost(DataSet: TDataSet);
    procedure EdtCodigoProdutoDevolucaoCondicionalExit(Sender: TObject);
    procedure cdsVendaID_PESSOAChange(Sender: TField);
    procedure cdsVendaItemBeforeDelete(DataSet: TDataSet);
    procedure cdsVendaParcelaBeforePost(DataSet: TDataSet);
    procedure cdsVendaPERC_DESCONTOChange(Sender: TField);
    procedure cdsVendaVL_DESCONTOChange(Sender: TField);
    procedure cdsVendaVL_VENDAChange(Sender: TField);
    procedure gbDBTextEdit4Enter(Sender: TObject);
    procedure EdtCodigoProdutogbDepoisDeConsultar(Sender: TObject);
    procedure filtroCodigoVendaExit(Sender: TObject);
    procedure EdtCodigoProdutoExit(Sender: TObject);
    procedure EdtValorUnitarioProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure viewPesquisaDataControllerDataChanged(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ActLiberarBloqueiosExecute(Sender: TObject);
  private
    const CID_NULL: Integer = -999;
    procedure ConfigurarPainelTituloDevolucaoCondicional;
    const FILTRO_TODOS: String = 'TODOS';

    var
    FParametroFormaPagamentoAVista: TParametroFormulario;
    FParametroPlanoPagamentoAVista: TParametroFormulario;
    FParametroFormaPagamentoCrediario: TParametroFormulario;
    FparametroFormaPagamentoPrazoConsumidorFinal: TParametroFormulario;
    FParametroContaCorrente: TParametroFormulario;
    FParametroCentroResultadoAPrazo: TParametroFormulario;
    FParametroContaAnaliseAPrazo: TParametroFormulario;
    FParametroCentroResultadoAVista: TParametroFormulario;
    FParametroContaAnaliseAVista: TParametroFormulario;
    FParametroExibeEmissaoDocumentoAoEfetivar: TParametroFormulario;
    FParametroCarteira: TParametroFormulario;
    FParametroExibirAbaReceitaOtica: TParametroFormulario;
    FParametroPermitirVendaParaConsumidorFinalNaModalidadeCondicional: TParametroFormulario;
    FParametroInclusaoDiretaAoPreencherCodigoBarras: TParametroFormulario;
    FParametroHabilitarEdicaoValorVenda: TParametroFormulario;
    FParametroHabilitarEdicaoDescricaoProduto: TParametroFormulario;
    FParametroExibirOperacaoComercial: TParametroFormulario;
    FUsarLikeConsultaProduto: TParametroFormulario;
    FParametroFiltroPadraoTipoVenda: TParametroFormulario;

    FDeletandoItemCondicionalManualmente: Boolean;
    FFechamentoCrediario: TFrameVendaVarejoFechamentoCrediario;
    FFrameVendaVarejoReceitaOtica: TFrameVendaVarejoReceitaOtica;
    FWhereSearch: TFiltroPadrao;
    FPodeExecutarGeracaoParcela: Boolean;
    FCalculandoPeriodoParcelamento: Boolean;
    //FFdmTabelaPrecoItem: TFDMemTable;
    FEstadoProduto: TEStadoProduto;
    FCalculandoValor: Boolean;
    FConsultaAutomatica: Boolean;
    FPodeExecutarConsultaPorCodigoBarra: Boolean;
    FRateandoDescontoTotal: Boolean;
    FDescontoEnter: Double;
    FVlVendaEnter: Double;
    FCalculandoDescontoDaVenda: Boolean;
    FFiltroRegistroGradeAnterior: String;
    FCalculandoTotalRodape: Boolean;
    FInserindoProduto: boolean;

    procedure CalcularTotaisDoRodape;
    procedure AlinharBarraTotalizadoraConsulta;
    procedure PreencherPlanoContaAVista;
    procedure PreencherPlanoContaAPrazo;
    procedure ConfigurarEventoInserirValoresPadrao(ADataset: TgbClientDataset);
    function ViewAtivaNaTela: TcxGridDBBandedTableView;
    procedure RatearValorDesconto;

    procedure NovoProduto;
    procedure SalvarProduto;
    procedure AlterarProduto;
    procedure CarregarItensTabelaPreco;
    procedure AtualizarPrecoProdutos;
    procedure TotalizarValoresVenda;
    procedure NovaVenda;
    procedure CalcularValorTotalItem;
    procedure AtualizarNumeroItemDosItensDaVenda;
    procedure GerenciarControles;
    procedure AoInicializarFormulario;
    procedure HabilitarBotaoEfetivar;
    procedure SalvarConfiguracaoGrid;
    procedure PesquisarVenda;

    procedure PrepararFiltros;
    procedure Consultar;
    procedure LimparVenda;
    procedure SetWhereBundle;
    procedure ClearWhereBundle;
    procedure ExibirFiltroDeMemoriaDaGrid(const AExibir: Boolean);
    procedure CarregarDocumento(AIdVenda: Integer);
    procedure AcaoDaTelaAposCarregarDocumento;
    procedure PreencherNomePessoaNoFiltro;
    procedure FechamentoVenda;
    procedure ValidarCamposObrigatorios;
    procedure EmitirDocumento;

    procedure AdicionarProdutoNaDevolucaoCondicional;
    procedure RemoverProdutoDaDevolucaoCondicional;
    procedure CarregarInformacoesDoItemParaDevolucao;
    function TipoOperacaoInclusaoAtual: TTipoOperacaoInclusao;
    procedure InformarSeClienteConsumidorFinal;
    function GetVendaProxy: TVendaProxy;
    procedure SugerirDataVencimentoCheque;

    procedure ValidarInformacoesCartao;
    function ParcelaNaFormaPagamentoCartao: Boolean;
    procedure ExecutarDelecaoItem;
    function BuscarFatorMultiplicacao(const ACodigoProduto: String): Double;
    function RemoverFatorMultiplicaoDoCodigoProduto(const ACodigoProduto: String): String;
    procedure ValidarPreenchimentoDaOperadoraCartao;
    procedure ExibirOperacaoComercial;

    procedure ConfigurarComponenteConsultaProduto;
    procedure AbrirDatasetVenda(const AIdVenda: Integer);

    procedure LiberarBloqueios;
    procedure ModificarRotuloBotaoLiberarBloqueios;
    function BuscarQuantidadeBloqueios: Integer;
    procedure ExibirBotaoBloqueios;
    procedure InserirBloqueios;
    function PlanoPagamentoAVista: Boolean;
  public
    FClienteConsumidorFinal: Boolean;
    FParametroHabilitarEmissaoNFCE: TParametroFormulario;
    FParametroHabilitarEmissaoNFE: TParametroFormulario;

    procedure GerarParcelamento;
  protected
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure MostrarInformacoesImplantacao; override;
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uControlsUtils, uMathUtils, uTabelaPreco, uProduto,
  uFrmMessage, uFilial, uTPessoa, uFrmMessage_Process, uVenda, uChaveProcesso,
  uDatasetUtils, uDevExpressUtils, uTUsuario, Data.FireDACJSONReflect,
  uStringUtils, uFrmAlterarSQLPesquisaPadrao, uFrmApelidarColunasGrid,
  uTFunction, uTMessage, uPlanoConta, uFormaPagamento,
  uMovVendaVarejoRecebimento, uChaveProcessoProxy, uSistema,
  uFrmConfiguracaoValoresPadroes, uDmAcesso, uConstParametroFormulario,
  uTControl_Function, uFrmHelpImplantacaoVendaVarejo,
  uPlanoPagamento, uPesqContaAnalise, uFrmEmissaoDocumentoFechamento, uFrmConsultaPadrao,
  uFrmVisualizacaoRelatoriosAssociados, uMovVendaVarejoEmitirDocumento, uFrmDevolucaoValorParaCliente,
  uTipoQuitacaoProxy, uProdutoProxy, uTabelaPrecoProxy, uFrmSolicitarValor,
  ugbFDMemTable, uBloqueioPersonalizadoProxy, uFrmLiberacaoBloqueioPersonalizado;

{ TMovVendaVarejo }

procedure TMovVendaVarejo.AbrirDatasetVenda(const AIdVenda: Integer);
begin
  cdsVenda.Fechar;
  cdsVenda.Params.Clear;
  cdsVenda.FetchParams;
  cdsVenda.Params.ParamByName('id').AsInteger := AIdVenda;
  cdsVenda.Abrir;
end;

procedure TMovVendaVarejo.AcaoDaTelaAposCarregarDocumento;
begin
  if not cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO) then
    exit;

  if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_PEDIDO) then
  begin
    NovoProduto;
  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_VENDA) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_ORCAMENTO) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
  begin

  end
  else if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_DEVOLUCAO) then
  begin

  end;
end;

procedure TMovVendaVarejo.ActCancelarVendaExecute(Sender: TObject);
begin
  inherited;
  if cdsVendaID.AsInteger > 0 then
  begin
    if not TVenda.PodeEstornar(cdsVendaID_CHAVE_PROCESSO.AsInteger) then
    begin
      TFrmMessage.Information('N�o � possivel realizar o estorno da venda pois existem t�tulos quitados.');
      exit;
    end;

    TVenda.Cancelar(cdsVendaID_CHAVE_PROCESSO.AsInteger);
    NovaVenda;
  end
  else
  begin
    LimparVenda;
    NovaVenda;
  end;
end;

procedure TMovVendaVarejo.ActCancelExecute(Sender: TObject);
var
  estaDevolvendoCondicional: Boolean;
begin
  //inherited;
  if TipoOperacaoInclusaoAtual = devolucaoCondicional then
  begin
    cdsVendaItemDevolucao.DeletarTodosRegistros;
  end
  else
  begin
    LimparVenda;
    NovaVenda;
  end;
end;

procedure TMovVendaVarejo.ActCondicionalExecute(Sender: TObject);
var
  vendaJaSalvaNoBanco: Boolean;
  idChaveProcesso: Integer;
begin
  inherited;
  if (not FParametroPermitirVendaParaConsumidorFinalNaModalidadeCondicional.ValorSim) and
    (FClienteConsumidorFinal) then
  begin
    TFrmMessage.Information('N�o � permitido realizar venda condicional para consumidor final');
    exit;
  end;

  try
    TFrmMessage_Process.SendMessage('Processando movimenta��es de estoque');
    vendaJaSalvaNoBanco := cdsVendaID.AsInteger > 0;
    idChaveProcesso := cdsVendaID_CHAVE_PROCESSO.AsInteger;

    if not cdsVenda.EstadoDeAlteracao then
    begin
      cdsVenda.Edit;
    end;

    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_CONDICIONAL;

    cdsVenda.Commit;

    if vendaJaSalvaNoBanco then
    begin
      TVenda.CancelarMovimentacaoVendaCondicional(idChaveProcesso);
    end;

    TVenda.EfetivarMovimentacaoVendaCondicional(idChaveProcesso);
    EmitirDocumento;
    NovaVenda;
  finally
    TFrmMessage_Process.CloseMessage();
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejo.ActConfigurarValoresDefaultExecute(Sender: TObject);
begin
  inherited;
  TFrmConfigurarValoresPadroes.ConfigurarValoresPadroes(Self.Name,
    TSistema.Sistema.usuario.idSeguranca, cdsVenda);
end;

procedure TMovVendaVarejo.ConfigurarComponenteConsultaProduto;
begin
  EdtCodigoProduto.gbUsarLikeConsulta := FUsarLikeConsultaProduto.ValorSim;
end;

procedure TMovVendaVarejo.ConfigurarEventoInserirValoresPadrao(ADataset: TgbClientDataset);
var i: Integer;
begin
  for i := 0 to Pred(ADataset.FListaDetail.Count) do
  begin
    ADataset.FListaDetail[i].AfterInsert := InserirValoresPadrao;
    ConfigurarEventoInserirValoresPadrao(ADataset.FListaDetail[i]);
  end;
end;

procedure TMovVendaVarejo.InformarSeClienteConsumidorFinal;
begin
  FClienteConsumidorFinal := TPessoa.VerificarConsumidorFinal(cdsVendaID_PESSOA.AsInteger);
end;

procedure TMovVendaVarejo.InserirBloqueios;
var
  existeBloqueios: boolean;
begin
  if PlanoPagamentoAVista then
  begin
    exit;
  end;

  AbrirDatasetVenda(TVenda.GetId(cdsVendaID_CHAVE_PROCESSO.AsInteger));

  try
    TFrmMessage_Process.SendMessage('Verificando bloqueios da venda');
    TVenda.IncluirBloqueiosVenda(cdsVendaBloqueioPersonalizado, cdsVendaID_CHAVE_PROCESSO.AsInteger);
  finally
    TFrmMessage_Process.CloseMessage;
  end;

  if BuscarQuantidadeBloqueios > 0 then
  try
    cdsVenda.Commit;

    AbrirDatasetVenda(TVenda.GetId(cdsVendaID_CHAVE_PROCESSO.AsInteger));

    cdsVenda.Alterar;

    TFrmMessage.Information('Foi encontrado bloqueios que impedem a efetiva��o dessa venda. '+
      'Desbloquei-os para prosseguir com a efetiva��o.');

    GerenciarControles;

    ActLiberarBloqueios.Execute;
  finally
    if BuscarQuantidadeBloqueios > 0 then
    begin
      abort;
    end;
  end;
end;

procedure TMovVendaVarejo.InserirValoresPadrao(DataSet: TDataSet);
begin
  inherited;
  TUsuarioDesignControl.CarregarValoresPadrao(DataSet,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TMovVendaVarejo.InstanciarFrames;
begin
  inherited;
  FFrameVendaVarejoReceitaOtica :=
    TFrameVendaVarejoReceitaOtica.Create(PnFrameVendaVarejoReceitaOtica);
  FFrameVendaVarejoReceitaOtica.Parent := PnFrameVendaVarejoReceitaOtica;
  FFrameVendaVarejoReceitaOtica.Align := alClient;
  FFrameVendaVarejoReceitaOtica.SetConfiguracoesIniciais(cdsVendaReceitaOtica);

  FFechamentoCrediario := TFrameVendaVarejoFechamentoCrediario.Create(PnParcela);
  FFechamentoCrediario.Parent := PnParcela;
  FFechamentoCrediario.Align := alClient;
  FFechamentoCrediario.SetConfiguracoesIniciais(
    StrtoIntDef(FParametroFormaPagamentoAVista.Valor, 0),
    StrtoIntDef(FParametroPlanoPagamentoAVista.Valor, 0),
    cdsVenda, cdsVendaParcela);
end;

procedure TMovVendaVarejo.ActDevolucaoCondicionalExecute(Sender: TObject);
begin
  inherited;
  cxpcMain.ActivePage := tsDevolucaoCondicional;
  TControlsUtils.SetFocus(EdtCodigoProdutoDevolucaoCondicional);

  GerenciarControles;
end;

procedure TMovVendaVarejo.ActDevolucaoExecute(Sender: TObject);
var
  vendaProxy: TVendaProxy;
begin
  inherited;
  vendaProxy := TVendaProxy.Create;
  try
    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_DEVOLUCAO;
    cdsVenda.Commit;

    vendaProxy := GetVendaProxy;
    vendaProxy.FIdPessoa := cdsVendaID_PESSOA.AsInteger;
    vendaProxy.FIdChaveProcesso := cdsVendaID_CHAVE_PROCESSO.AsInteger;
    if not TFrmDevolucaoValorParaCliente.EstornarFinanceiroVenda(vendaProxy, cdsVendaVL_VENDA.AsFloat) then
    begin
      Exit;
    end;

    AbrirDatasetVenda(StrtoIntDef(DmConnection.ServerMethodsClient.GetUltimaChaveDaTabela(
      TVendaProxy.NOME_TABELA_VENDA, TVendaProxy.FIELD_ID), 0));

    cdsVenda.Alterar;

    ActEfetivarVenda.Execute;
    GerenciarControles;
  finally
    //FreeAndNil(vendaProxy); tava dando acess violation, acredito que o server tava matando a instancia
  end;
end;

procedure TMovVendaVarejo.ActEfetivarVendaExecute(Sender: TObject);
var
  vendaProxy: TVendaProxy;
begin
  inherited;
  ActiveControl := nil;

  TValidacaoCampo.CampoPreenchido(cdsVendaID_PLANO_PAGAMENTO);

  ValidarPreenchimentoDaOperadoraCartao;

  //Em caso de troco, deve gerar apenas o caixa referente ao valor da venda
  if cdsVendaVL_PAGAMENTO.AsFloat > cdsVendaVL_VENDA.AsFloat then
    cdsVendaVL_PAGAMENTO.AsFloat := cdsVendaVL_VENDA.AsFloat;

  cdsVendaDH_FECHAMENTO.AsDateTime := Now;
  cdsVendaSTATUS.AsString := STATUS_BLOQUEADO;

  if cdsVendaParcela.IsEmpty then
  begin
    cdsVendaID_FORMA_PAGAMENTO.AsInteger := FParametroFormaPagamentoAVista.AsInteger;
  end;

  cdsVenda.Commit;

  InserirBloqueios;

  TFrmMessage_Process.SendMessage('Efetivando a venda');
  try
    vendaProxy := TVendaProxy.Create;
    try
      vendaProxy := GetVendaProxy;

      TVenda.Efetivar(cdsVendaID_CHAVE_PROCESSO.AsInteger, vendaProxy);

      if FParametroExibeEmissaoDocumentoAoEfetivar.ValorSim then
      begin
        TFrmMessage_Process.CloseMessage;
        EmitirDocumento;
      end;
    finally
      FreeAndNil(vendaProxy);
    end;
  finally
    TFrmMessage_Process.CloseMessage;
    NovaVenda;
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejo.ActExcluirExecute(Sender: TObject);
const MSG_NAO_PODE_EXCLUIR: String =
  'N�o � poss�vel excluir o documento pois existe quita��o realizada.';
begin
  if not TVenda.PodeExcluir(cdsVendaID_CHAVE_PROCESSO.AsInteger) then
  begin
    TFrmMessage.Information(MSG_NAO_PODE_EXCLUIR);
    abort;
  end;

  if TMessage.MessageDeleteAsk('Deseja realmente excluir todo este documento?') then
  try
    TFrmMessage_Process.SendMessage('Excluindo venda');
    cdsVenda.Cancelar;

    TVenda.ExcluirVenda(cdsVendaID_CHAVE_PROCESSO.AsInteger);
  finally
    NovaVenda;
    TFrmMessage_Process.CloseMessage;
    GerenciarControles;
  end
end;

procedure TMovVendaVarejo.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  inherited;

  ViewAtivaNaTela.OptionsView.GroupByBox := not ViewAtivaNaTela.OptionsView.GroupByBox;
end;

procedure TMovVendaVarejo.ActImprimirExecute(Sender: TObject);
begin
  inherited;
  EmitirDocumento;
end;

procedure TMovVendaVarejo.ActLiberarBloqueiosExecute(Sender: TObject);
begin
  inherited;
  LiberarBloqueios;
end;

procedure TMovVendaVarejo.ActNovaVendaExecute(Sender: TObject);
begin
  inherited;
  LimparVenda;
  NovaVenda;
end;

procedure TMovVendaVarejo.ActOrcamentoExecute(Sender: TObject);
begin
  inherited;
  try
    if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
    begin
      TVenda.CancelarMovimentacaoVendaCondicional(cdsVendaID_CHAVE_PROCESSO.AsInteger);
    end;

    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_ORCAMENTO;
    cdsVenda.Commit;
    EmitirDocumento;
    NovaVenda;
  finally
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejo.ActPedidoExecute(Sender: TObject);
begin
  inherited;
  try
    if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
    begin
      TVenda.CancelarMovimentacaoVendaCondicional(cdsVendaID_CHAVE_PROCESSO.AsInteger);
    end;

    cdsVendaTIPO.AsString := TIPO_DOCUMENTO_PEDIDO;
    cdsVenda.Commit;
    EmitirDocumento;
    NovaVenda;
  finally
    GerenciarControles;
  end;
end;

procedure TMovVendaVarejo.ActPesquisarExecute(Sender: TObject);
begin
  inherited;
  if cxpcMain.ActivePage = tsVenda then
  begin
    if cdsVenda.ChangeCount > 0 then
    begin
      if TFrmMessage.Question('Existe altera��es que n�o est�o salvas, '+
        'deseja cancela-las e continuar a pesquisa?') = MCANCELED then
        exit;
    end;

    cxpcMain.ActivePage := tsPesquisa;

    GerenciarControles;

    if FConsultaAutomatica then
      Consultar;
  end
  else if cxpcMain.ActivePage = tsPesquisa then
  begin
    Consultar;
  end;
end;

procedure TMovVendaVarejo.ActVendaExecute(Sender: TObject);
begin
  inherited;
  if cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
  begin
    TVenda.CancelarMovimentacaoVendaCondicional(cdsVendaID_CHAVE_PROCESSO.AsInteger);
  end;

  cdsVendaTIPO.AsString := TIPO_DOCUMENTO_VENDA;
  FechamentoVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejo.ActVoltarExecute(Sender: TObject);
begin
  inherited;
  cxpcMain.ActivePage := tsVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejo.AdicionarProdutoNaDevolucaoCondicional;
var
  idProdutoRemoverCondicional: Integer;
  quantidadeRemoverCondicional: Double;
  estaDevolvendoCondicional: Boolean;
begin
  idProdutoRemoverCondicional := cdsVendaItemDevolucaoID_PRODUTO.AsInteger;
  quantidadeRemoverCondicional := cdsVendaItemDevolucaoQUANTIDADE.AsFloat;

  if cdsVendaItem.Locate('ID_PRODUTO', idProdutoRemoverCondicional, []) then
  begin
    if quantidadeRemoverCondicional >= cdsVendaItemQUANTIDADE.AsFloat then
    begin
      cdsVendaItem.Excluir;
    end
    else
    begin
      cdsVendaItem.Alterar;
      cdsVendaItemQUANTIDADE.AsFloat := cdsVendaItemQUANTIDADE.AsFloat - quantidadeRemoverCondicional;
      cdsVendaItem.Salvar;
    end;
  end;

  estaDevolvendoCondicional := (cxpcMain.ActivePage = tsDevolucaoCondicional);

  if estaDevolvendoCondicional then
  begin
    TControlsUtils.SetFocus(EdtCodigoProdutoDevolucaoCondicional);
  end;
end;

procedure TMovVendaVarejo.AlterandoDataParcelamento(Sender: TField);
begin
  SugerirDataVencimentoCheque;

  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsVendaParcelaIC_DIAS.AsInteger := DaysBetween(Date,
      cdsVendaParcelaDT_VENCIMENTO.AsDateTime);

    TParcelamentoUtils.ReplicarDiaDeVencimento(cdsVendaParcelaDT_VENCIMENTO);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovVendaVarejo.AlterandoDiasParcelamento(Sender: TField);
begin
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsVendaParcelaDT_VENCIMENTO.AsDateTime := Date +
      cdsVendaParcelaIC_DIAS.AsInteger;

    TParcelamentoUtils.ReplicarDiaDeVencimento(cdsVendaParcelaDT_VENCIMENTO);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovVendaVarejo.AlterarProduto;
begin
  try
    FPodeExecutarConsultaPorCodigoBarra := false;
    NovoProduto;

    FEstadoProduto := Alterando;

    fdmProdutoCODIGO.AsString := cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO.AsString;
    fdmProdutoID_PRODUTO.AsInteger := cdsVendaItemID_PRODUTO.AsInteger;
    fdmProdutoNR_ITEM.AsInteger := cdsVendaItemNR_ITEM.AsInteger;
    fdmProdutoQUANTIDADE.AsFloat := cdsVendaItemQUANTIDADE.AsFloat;
    fdmProdutoVL_DESCONTO.AsFloat := cdsVendaItemVL_DESCONTO.AsFloat;
    fdmProdutoVALOR.AsFloat := cdsVendaItemVL_BRUTO.AsFloat;
    fdmProdutoTOTAL.AsFloat := cdsVendaItemVL_LIQUIDO.AsFloat;
    fdmProdutoVL_VENDA.AsFloat := cdsVendaItemVL_BRUTO.AsFloat;
    fdmProdutoPERC_DESCONTO.AsFloat := cdsVendaItemPERC_DESCONTO.AsFloat;
    TControlsUtils.SetFocus(EdtQuantidadeProduto);
    fdmProdutoPRODUTO.AsString := cdsVendaItemDESCRICAO_PRODUTO.AsString;
  finally
    FPodeExecutarConsultaPorCodigoBarra := true;
  end;
end;

procedure TMovVendaVarejo.AoAlterarQuantidade(Sender: TField);
begin
  CalcularValorTotalItem;
end;

procedure TMovVendaVarejo.AoInicializarFormulario;
begin
  PrepararFiltros;

  fdmProduto.Open;
  tsPesquisa.Visible := False;
  cxpcMain.ActivePage := tsVenda;
  cxpcMain.Properties.HideTabs := true;

  //FFdmTabelaPrecoItem := TFDMemTable.Create(nil);

  FPodeExecutarGeracaoParcela := true;
  FCalculandoPeriodoParcelamento := false;
  FEstadoProduto := nenhum;
  FCalculandoValor := false;
  FPodeExecutarConsultaPorCodigoBarra := true;
  FDeletandoItemCondicionalManualmente := false;
  FCalculandoDescontoDaVenda := false;
  FInserindoProduto := false;

  ConfigurarPainelTituloDevolucaoCondicional;

  FConsultaAutomatica := TUsuarioPesquisaPadrao.GetConsultaAutomatica(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name);

  NovaVenda;

  TUsuarioGridView.LoadGridView(viewProduto,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name+viewProduto.Name);
  viewProduto.OptionsView.Footer := true;

  TUsuarioGridView.LoadGridView(viewDevolucaoCondicional,
    TSistema.Sistema.Usuario.idSeguranca, Self.Name+viewProduto.Name);
  viewDevolucaoCondicional.OptionsView.Footer := true;

  EdtValorUnitarioProduto.gbReadyOnly := not FParametroHabilitarEdicaoValorVenda.ValorSim;
  EdtDescricaoProduto.gbReadyOnly := not FParametroHabilitarEdicaoDescricaoProduto.ValorSim;

  ExibirOperacaoComercial;

  ConfigurarComponenteConsultaProduto;

  FFiltroRegistroGradeAnterior := '';
  FCalculandoTotalRodape := false;
end;

procedure TMovVendaVarejo.AtualizarContadorReceitaOtica(Dataset: TDataset);
const CAPTION_RECEITA_OTICA = 'Receita �tica';
var complemento: String;
begin
  if not cdsVendaReceitaOtica.IsEmpty then
  begin
    complemento := ' ('+InttoStr(cdsVendaReceitaOtica.RecordCount)+')';
  end
  else
  begin
    complemento := '';
  end;

  ActReceitaOtica.Caption := CAPTION_RECEITA_OTICA+complemento;
end;

procedure TMovVendaVarejo.AtualizarNumeroItemDosItensDaVenda;
var registroAtual: TBookMark;
begin
  registroAtual := cdsVendaItem.GetBookMark;
  try
    cdsVendaItem.DisableControls;
    cdsVendaItem.First;
    while not cdsVendaItem.Eof do
    begin
      cdsVendaItem.Edit;
      cdsVendaItemNR_ITEM.AsInteger := cdsVendaItem.Recno;
      cdsVendaItem.Post;
      cdsVendaItem.Next;
    end;
  finally
    if (registroAtual <> nil) and cdsVendaItem.BookmarkValid(registroAtual) then
      cdsVendaItem.GotoBookmark(registroAtual);

    cdsVendaItem.EnableControls;
  end;
end;

procedure TMovVendaVarejo.AtualizarPrecoProdutos;
begin
  //Depois, quando alterar a tabela de pre�o recalcular os produtos
end;

procedure TMovVendaVarejo.BuscarDadosProdutoPorCodigoBarra(Sender: TField);
var fdmProdutoConsulta: TFDMemTable;
  incluirProdutoDireto: Boolean;
  nQuantidade: Double;
  produto: TTabelaPrecoProdutoProxy;
const
  NUMERO_CONVERSAO_INVALIDA = -155998;
begin
  if not(FPodeExecutarConsultaPorCodigoBarra) then
    exit;

  if not(TipoOperacaoInclusaoAtual = venda) then
    exit;

  try
    FPodeExecutarConsultaPorCodigoBarra := false;
    if (Sender = fdmProdutoCODIGO) then
    begin
      if fdmProdutoCODIGO.AsString = '' then exit;

      if StrtoFloatDef(fdmProdutoCODIGO.AsString, NUMERO_CONVERSAO_INVALIDA) =
        NUMERO_CONVERSAO_INVALIDA then
      begin
        EdtCodigoProduto.ExecutarAcaoBotaoConsulta;
        exit;
      end;

      nQuantidade := BuscarFatorMultiplicacao(fdmProdutoCODIGO.AsString);
      fdmProdutoCODIGO.AsString := RemoverFatorMultiplicaoDoCodigoProduto(fdmProdutoCODIGO.AsString);

      incluirProdutoDireto := FParametroInclusaoDiretaAoPreencherCodigoBarras.ValorSim;

      fdmProdutoConsulta := TFDMemTable.Create(nil);
      try
        fdmProdutoConsulta := TProduto.GetProdutoPeloCodigoBarra(fdmProdutoCODIGO.AsString);

        if fdmProdutoConsulta.IsEmpty then
          fdmProdutoConsulta := TProduto.GetProduto(StrtointDef(fdmProdutoCODIGO.AsString, 0));

        try
          produto := TTabelaPreco.GetProdutoTabelaPreco(cdsVendaID_TABELA_PRECO.AsInteger,
            fdmProdutoConsulta.FieldByName('ID').AsInteger, TSistema.Sistema.Filial.Fid);

          if produto.FId > 0 then
          begin
            fdmProdutoCODIGO.AsString := fdmProdutoConsulta.FieldByName('CODIGO_BARRA').AsString;
            fdmProdutoPRODUTO.AsString := fdmProdutoConsulta.FieldByName('DESCRICAO').AsString;
            fdmProdutoID_PRODUTO.AsInteger := fdmProdutoConsulta.FieldByName('ID').AsInteger;
            fdmProdutoVL_VENDA.AsFloat := produto.FVlVenda;
            fdmProdutoVALOR.AsFloat := produto.FVlVenda;
            fdmProdutoQUANTIDADE.AsFloat := nQuantidade;

            if incluirProdutoDireto then
            begin
              SalvarProduto
            end;
          end
          else
          begin
            TFrmMessage.Information('Produto n�o encontrado na tabela de pre�o.');
            NovoProduto;
          end;
        finally
          //FreeAndNil(produto);
        end;
      finally
        FreeAndNil(fdmProdutoConsulta);
      end;
    end;
  finally
    FPodeExecutarConsultaPorCodigoBarra := true;
  end;
end;

function TMovVendaVarejo.BuscarFatorMultiplicacao(const ACodigoProduto: String): Double;
var
  posicaoAsterisco: Integer;
begin
  posicaoAsterisco := Pos('*', ACodigoProduto);

  if posicaoAsterisco > 0 then
  begin
    result := StrtoFloatDef(Copy(ACodigoProduto, 1, posicaoAsterisco - 1), 1);
  end
  else
  begin
    result := 1;
  end;
end;

procedure TMovVendaVarejo.BuscarProdutosDaTabelaPreco(Sender: TField);
begin
  {if (Sender = cdsVendaID_TABELA_PRECO) then
    CarregarItensTabelaPreco;}
end;

function TMovVendaVarejo.BuscarQuantidadeBloqueios: Integer;
begin
  if not cdsVendaBloqueioPersonalizado.Active then
  begin
    result := 0;
    exit;
  end;

  try
    cdsVendaBloqueioPersonalizado.DisableControls;
    cdsVendaBloqueioPersonalizado.Filter := 'STATUS = '+QuotedStr(TBloqueioPersonalizadoProxy.STATUS_BLOQUEADO);
    cdsVendaBloqueioPersonalizado.Filtered := true;

    result := cdsVendaBloqueioPersonalizado.RecordCount;
  finally
    cdsVendaBloqueioPersonalizado.Filter := '';
    cdsVendaBloqueioPersonalizado.Filtered := false;
    cdsVendaBloqueioPersonalizado.EnableControls;
  end;
end;

procedure TMovVendaVarejo.CalcularDescontoPeloPercentual(Sender: TField);
begin
  if FCalculandoValor then
    Exit;

  try
    FCalculandoValor := true;

    if Sender = fdmProdutoPERC_DESCONTO then
    begin
      fdmProdutoVL_DESCONTO.AsFloat := TMathUtils.ValorSobrePercentual(
        fdmProdutoPERC_DESCONTO.AsFloat, fdmProdutoVALOR.AsFloat);

      CalcularValorTotalItem;
    end
  finally
    FCalculandoValor := false;
  end;
end;

procedure TMovVendaVarejo.CalcularTotaisDoRodape;
var
  fdmTotais: TgbFDMemTable;
  idsVenda: String;
begin
  if not fdmSearch.Active then
  begin
    Exit;
  end;

  fdmTotais := TgbFDMemTable.Create(nil);
  try
    FCalculandoTotalRodape := true;

    idsVenda := TDatasetUtils.ConcatenarRegistrosFiltradosNaView(
      fdmSearch.FieldByName('ID'), viewPesquisa);

    if idsVenda.IsEmpty then
    begin
      idsVenda := '-9999999';
    end;

    fdmTotais := TVenda.GetTotaisPorTipoVenda(idsVenda);

    PnTotalVenda.Caption := 'Vendas R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('vendas'));
    PnTotalPedido.Caption := 'Pedidos R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('pedidos'));
    PnTotalOrcamento.Caption := 'Or�amentos R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('orcamentos'));
    PnTotalCondicional.Caption := 'Condicionais R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('condicionais'));
    PnTotalDevolucao.Caption := 'Devolu��es R$ '+
      FormatFloat('###,###,###,###,##0.00', fdmTotais.GetAsFloat('devolucoes'));
  finally
    FreeAndNil(fdmTotais);
    FCalculandoTotalRodape := false;
    AlinharBarraTotalizadoraConsulta;
  end;
end;

procedure TMovVendaVarejo.AlinharBarraTotalizadoraConsulta;
begin
  PnTotalizadorConsulta.AutoWrap := false;
  PnTotalizadorConsulta.AutoWrap := true;
end;

procedure TMovVendaVarejo.CalcularValorTotalItem;
begin
  fdmProdutoTOTAL.AsFloat := fdmProdutoQUANTIDADE.AsFloat *
    (fdmProdutoVALOR.AsFloat - fdmProdutoVL_DESCONTO.AsFloat);
end;

procedure TMovVendaVarejo.CarregarDocumento(AIdVenda: Integer);
begin
  AbrirDatasetVenda(AIdVenda);

  InformarSeClienteConsumidorFinal;

  if not Assigned(cdsVenda.AfterInsert) then
    ConfigurarEventoInserirValoresPadrao(cdsVenda);

  if not cdsVenda.IsEmpty then
  begin
    try
      cxpcMain.ActivePage := tsVenda;
      cdsVenda.Alterar;
    finally
      GerenciarControles;
      AcaoDaTelaAposCarregarDocumento;
    end;
  end
  else
  begin
    TFrmMessage.Information('Venda n�o encontrada. Possivelmente foi exclu�da por outro usu�rio');
  end;
end;

procedure TMovVendaVarejo.CarregarInformacoesDoItemParaDevolucao;
begin
  if fdmProduto.State in dsEditModes then
  begin
    fdmProdutoID_PRODUTO.AsString := cdsVendaItem.FieldByName('ID_PRODUTO').AsString;
    fdmProdutoNR_ITEM.AsInteger := cdsVendaItem.FieldByName('NR_ITEM').AsInteger;
    fdmProdutoQUANTIDADE.AsFloat := cdsVendaItem.FieldByName('QUANTIDADE').AsFloat;
    fdmProdutoVALOR.AsFloat := cdsVendaItem.FieldByName('VL_BRUTO').AsFloat;
    fdmProdutoTOTAL.AsFloat := cdsVendaItem.FieldByName('VL_LIQUIDO').AsFloat;
    fdmProdutoPERC_DESCONTO.AsFloat := cdsVendaItem.FieldByName('PERC_DESCONTO').AsFloat;
    fdmProdutoPRODUTO.AsString := cdsVendaItem.FieldByName('DESCRICAO_PRODUTO').AsString;
    fdmProdutoCODIGO.AsString := cdsVendaItem.FieldByName('JOIN_CODIGO_BARRA_PRODUTO').AsString;

    fdmProduto.Post;
  end;
end;

procedure TMovVendaVarejo.CarregarItensTabelaPreco;
begin
  //FFdmTabelaPrecoItem := TTabelaPreco.GetProdutosTabelaPreco(cdsVendaID_TABELA_PRECO.AsInteger);
end;

procedure TMovVendaVarejo.cdsVendaAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if cdsVendaID.AsInteger > 0 then
  begin
    informacoesCodigo.Caption :=
      TStringUtils.PrimeiraLetraMaiuscula(cdsVendaTIPO.AsString) + ':' +
      cdsVendaID.AsString;

    informacoesSituacao.Caption := TStringUtils.PrimeiraLetraMaiuscula(
      cdsVendaSTATUS.AsString);
  end;
end;

procedure TMovVendaVarejo.cdsVendaCalcFields(DataSet: TDataSet);
var diferenca: Double;
begin
  cdsVendaCC_SALDO_PESSOA_CREDITO.AsFloat :=
    cdsVendaJOIN_VALOR_PESSOA_CREDITO.AsFloat - cdsVendaVL_PESSOA_CREDITO_UTILIZADO.AsFloat;

  diferenca := cdsVendaVL_VENDA.AsFloat -
    cdsVendaVL_PESSOA_CREDITO_UTILIZADO.AsFloat - cdsVendaVL_PAGAMENTO.AsFloat;

  if diferenca > 0 then
  begin
    cdsVendaCC_VL_DIFERENCA.AsFloat := diferenca;
  end
  else if diferenca < 0 then
  begin
    cdsVendaCC_VL_TROCO.AsFloat := diferenca;
  end;
end;

procedure TMovVendaVarejo.cdsVendaID_PESSOAChange(Sender: TField);
begin
  inherited;
  InformarSeClienteConsumidorFinal;
end;

procedure TMovVendaVarejo.cdsVendaItemAfterDelete(DataSet: TDataSet);
begin
  inherited;
  AtualizarNumeroItemDosItensDaVenda;
  TotalizarValoresVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejo.cdsVendaItemAfterPost(DataSet: TDataSet);
begin
  inherited;
  TotalizarValoresVenda;
  GerenciarControles;
end;

procedure TMovVendaVarejo.cdsVendaItemBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  GerenciarSomenteLeitura(Dataset);
end;

procedure TMovVendaVarejo.cdsVendaItemDevolucaoAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdicionarProdutoNaDevolucaoCondicional;
end;

procedure TMovVendaVarejo.cdsVendaItemDevolucaoBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  GerenciarSomenteLeitura(Dataset);
end;

procedure TMovVendaVarejo.cdsVendaItemDevolucaoBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  GerenciarSomenteLeitura(Dataset);
end;

procedure TMovVendaVarejo.cdsVendaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsVendaDH_CADASTRO.AsDateTime := Now;
  cdsVendaID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_VENDA_VAREJO, 0);
  cdsVendaID_FILIAL.AsInteger := TSistema.Sistema.filial.Fid;

  cdsVendaID_VENDEDOR.AsInteger := TSistema.Sistema.usuario.id;
  cdsVendaJOIN_NOME_VENDEDOR.AsString := TSistema.Sistema.usuario.nome;

  cdsVendaVL_TOTAL_PRODUTO.AsFloat := 0;
  cdsVendaVL_VENDA.AsFloat := 0;
  cdsVendaVL_DESCONTO.AsFloat := 0;
  cdsVendaPERC_DESCONTO.AsFloat := 0;
  cdsVendaVL_ACRESCIMO.AsFloat := 0;
  cdsVendaPERC_ACRESCIMO.AsFloat := 0;

  PreencherPlanoContaAVista;

  //Plano de Pagamento
  cdsVendaID_PLANO_PAGAMENTO.AsInteger := StrtoIntDef(FParametroPlanoPagamentoAVista.valor,0);
  cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO.AsString := TPlanoPagamento.GetDescricaoPlanoPagamento(cdsVendaID_PLANO_PAGAMENTO.AsInteger);

  cdsVendaSTATUS.AsString := STATUS_ABERTO;
  cdsVendaTIPO.AsString := TIPO_DOCUMENTO_PEDIDO;
end;

procedure TMovVendaVarejo.cdsVendaParcelaAfterPost(DataSet: TDataSet);
begin
  inherited;
  FFechamentoCrediario.ReplicarDadosEntreAsParcelas;
end;

procedure TMovVendaVarejo.cdsVendaParcelaBeforePost(DataSet: TDataSet);
begin
  inherited;
  //ValidarInformacoesCartao; Refatorar pois esta validando logo ap�s a consulta
end;

procedure TMovVendaVarejo.cdsVendaPERC_DESCONTOChange(Sender: TField);
begin
  inherited;
  if not FCalculandoDescontoDaVenda then
  try
    FCalculandoDescontoDaVenda := true;

    cdsVendaVL_DESCONTO.AsFloat := TMathUtils.ValorSobrePercentual(cdsVendaPERC_DESCONTO.AsFloat
                                                              ,cdsVendaVL_TOTAL_PRODUTO.AsFloat);

    RatearValorDesconto;
  finally
    FCalculandoDescontoDaVenda := false;
    FVlVendaEnter  := 0;
    FDescontoEnter := 0;
  end;
end;

procedure TMovVendaVarejo.cdsVendaPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  TMessagePersistenceError.OnPostError(E.Message);
  Action := daAbort;
end;

procedure TMovVendaVarejo.cdsVendaVL_DESCONTOChange(Sender: TField);
begin
  inherited;
  if not FCalculandoDescontoDaVenda then
  try
    FCalculandoDescontoDaVenda := true;
    RatearValorDesconto;
  finally
    FCalculandoDescontoDaVenda := false;
    FVlVendaEnter  := 0;
    FDescontoEnter := 0;
  end;
end;

procedure TMovVendaVarejo.cdsVendaVL_VENDAChange(Sender: TField);
var
  ValorDesconto: Double;
begin
  inherited;
  if (FVlVendaEnter <= 0) then
  begin
    exit;
  end;

  if not FCalculandoDescontoDaVenda then
  try
    FCalculandoDescontoDaVenda := true;

    ValorDesconto := cdsVendaVL_VENDA.AsFloat - FVlVendaEnter;
    cdsVendaVL_DESCONTO.AsFloat :=  cdsVendaVL_DESCONTO.AsFloat - TMathUtils.Arredondar(ValorDesconto);

    try
      FRateandoDescontoTotal := True;
      if cdsVendaVL_DESCONTO.AsFloat >  TDataSetUtils.SomarColunaExpressaoMultiplicacao(
        cdsVendaItemQUANTIDADE, cdsVendaItemVL_DESCONTO) then
      begin
        TDatasetUtils.RatearValores(FVlVendaEnter //cdsVendaVL_VENDA.AsFloat
                                   ,cdsVendaVL_DESCONTO.AsFloat
                                   ,cdsVendaItemQUANTIDADE
                                   ,cdsVendaItemVL_LIQUIDO
                                   ,cdsVendaItemVL_DESCONTO
                                   ,cdsVendaItemPERC_DESCONTO
                                   ,cdsVendaItemVL_BRUTO);
      end
      else if cdsVendaVL_DESCONTO.AsFloat <  TDataSetUtils.SomarColunaExpressaoMultiplicacao(
        cdsVendaItemQUANTIDADE, cdsVendaItemVL_DESCONTO) then
      begin
        TDatasetUtils.RatearValores(FVlVendaEnter //cdsVendaVL_VENDA.AsFloat
                                   ,cdsVendaVL_DESCONTO.AsFloat
                                   ,cdsVendaItemQUANTIDADE
                                   ,cdsVendaItemVL_LIQUIDO
                                   ,cdsVendaItemVL_DESCONTO
                                   ,cdsVendaItemPERC_DESCONTO
                                   ,cdsVendaItemVL_BRUTO
                                   ,false
                                   ,FDescontoEnter);
      end;
    finally
      FRateandoDescontoTotal := False;
    end;

    TotalizarValoresVenda;
  finally
    FCalculandoDescontoDaVenda := false;
    FVlVendaEnter  := 0;
    FDescontoEnter := 0;
  end;
end;

procedure TMovVendaVarejo.gbDBTextEdit11Enter(Sender: TObject);
begin
  inherited;
  FVlVendaEnter  := cdsVendaVL_VENDA.AsFloat;
  FDescontoEnter := cdsVendaVL_DESCONTO.AsFloat;
end;

procedure TMovVendaVarejo.gbDBTextEdit12Enter(Sender: TObject);
begin
  inherited;
  FDescontoEnter := cdsVendaVL_DESCONTO.AsFloat;
end;

procedure TMovVendaVarejo.gbDBTextEdit4Enter(Sender: TObject);
begin
  inherited;
  FDescontoEnter := cdsVendaVL_DESCONTO.AsFloat;
end;

procedure TMovVendaVarejo.RatearValorDesconto;
begin
  try
    FRateandoDescontoTotal := True;
    if TMathUtils.Arredondar(cdsVendaVL_DESCONTO.AsFloat) >  TMathUtils.Arredondar(TDataSetUtils.SomarColuna(cdsVendaItemVL_DESCONTO)) then
    begin
      TDatasetUtils.RatearValores(cdsVendaVL_VENDA.AsFloat
                                 ,cdsVendaVL_DESCONTO.AsFloat
                                 ,cdsVendaItemQUANTIDADE
                                 ,cdsVendaItemVL_LIQUIDO
                                 ,cdsVendaItemVL_DESCONTO
                                 ,cdsVendaItemPERC_DESCONTO
                                 ,cdsVendaItemVL_BRUTO);
    end
    else if TMathUtils.Arredondar(cdsVendaVL_DESCONTO.AsFloat) <  TMathUtils.Arredondar(TDataSetUtils.SomarColuna(cdsVendaItemVL_DESCONTO)) then
    begin
      TDatasetUtils.RatearValores(cdsVendaVL_VENDA.AsFloat
                                 ,cdsVendaVL_DESCONTO.AsFloat
                                 ,cdsVendaItemQUANTIDADE
                                 ,cdsVendaItemVL_LIQUIDO
                                 ,cdsVendaItemVL_DESCONTO
                                 ,cdsVendaItemPERC_DESCONTO
                                 ,cdsVendaItemVL_BRUTO
                                 ,false
                                 ,FDescontoEnter);
    end;
  finally
    FRateandoDescontoTotal := False;
  end;

  TotalizarValoresVenda;

  if not cdsVendaParcela.IsEmpty then
  begin
    GerarParcelamento;
  end;
end;

function TMovVendaVarejo.RemoverFatorMultiplicaoDoCodigoProduto(const ACodigoProduto: String): String;
var
  posicaoAsterisco: Integer;
begin
  result := ACodigoProduto;

  posicaoAsterisco := Pos('*', ACodigoProduto);

  if posicaoAsterisco > 0 then
  begin
    result := Copy(ACodigoProduto, posicaoAsterisco + 1, Length(ACodigoProduto));
  end;
end;

procedure TMovVendaVarejo.RemoverProdutoDaDevolucaoCondicional;
begin
  cdsVendaItem.Append;
  TDatasetUtils.CopiarRegistro(cdsVendaItemDevolucao, cdsVendaItem);
  cdsVendaItem.Post;
end;

procedure TMovVendaVarejo.cxDBButtonEdit1PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  RecnoRegistro: Integer;
begin
  inherited;
  RecnoRegistro := TFrmConsultaPadrao.ConsultarRegistroDeUmDatasetEspecifico(
    Self.Name+viewDevolucaoCondicional.Name, cdsVendaItem);

  if RecnoRegistro > 0 then
  begin
    cdsVendaItem.RecNo := RecnoRegistro;
    fdmProdutoCODIGO.AsString := cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO.AsString;
  end;
end;

procedure TMovVendaVarejo.cxGridProdutosViewDblClick(Sender: TObject);
begin
  inherited;
  if not cdsVendaItem.IsEmpty then
    AlterarProduto;
end;

procedure TMovVendaVarejo.cxGridProdutosViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (not cdsVendaItem.IsEmpty) AND (Key = VK_DELETE) then
  begin
    ExecutarDelecaoItem;
  end;
end;

procedure TMovVendaVarejo.EdtCodigoProdutoDevolucaoCondicionalExit(Sender: TObject);
begin
  inherited;
  if not(TipoOperacaoInclusaoAtual = DevolucaoCondicional) then
  begin
    Exit;
  end;

  if fdmProdutoCODIGO.AsString.IsEmpty then
  begin
    Exit;
  end;

  if cdsVendaItem.Locate('JOIN_CODIGO_BARRA_PRODUTO', fdmProdutoCODIGO.AsString, []) OR
     cdsVendaItem.Locate('id_produto', fdmProdutoCODIGO.AsString, []) then
  begin
    CarregarInformacoesDoItemParaDevolucao;
    SalvarProduto;
  end
  else
  begin
    if not fdmProdutoCODIGO.AsString.IsEmpty then
    begin
      fdmProdutoCODIGO.Clear;
      TControlsUtils.SetFocus(EdtCodigoProdutoDevolucaoCondicional);
    end;
  end;
end;

procedure TMovVendaVarejo.EdtCodigoProdutoExit(Sender: TObject);
begin
  inherited;
  if not FInserindoProduto then
  begin
    if fdmProduto.FieldByName('CODIGO').AsString.IsEmpty then
    begin
      TControlsUtils.SetFocus(PnInformacoesLateraisDireita);
      Self.SelectNext(PnInformacoesLateraisDireita, true, true);
    end;
  end;

  FInserindoProduto := false;
end;

procedure TMovVendaVarejo.EdtCodigoProdutogbDepoisDeConsultar(Sender: TObject);
begin
  inherited;
  try
    if not FInserindoProduto then
    begin
      if fdmProduto.FieldByName('CODIGO').AsString.IsEmpty then
      begin
        TControlsUtils.SetFocus(PnInformacoesLateraisDireita);
        Self.SelectNext(PnInformacoesLateraisDireita, true, true);
        exit;
      end;
    end;
  finally
    FInserindoProduto := false;
  end;

  if FParametroHabilitarEdicaoDescricaoProduto.ValorSim then
  begin
    TControlsUtils.SetFocus(EdtDescricaoProduto);
    exit;
  end;

  {if FParametroHabilitarEdicaoValorVenda.ValorSim then
  begin
    TControlsUtils.SetFocus(EdtValorUnitarioProduto);
    exit;
  end;}

  if not FParametroInclusaoDiretaAoPreencherCodigoBarras.ValorSim then
  begin
    TControlsUtils.SetFocus(EdtQuantidadeProduto);
  end;
end;

procedure TMovVendaVarejo.EdtPercentualDescontoExit(Sender: TObject);
begin
  inherited;
  if TValidacaoCampo.CampoPreenchido(fdmProdutoCODIGO) and
    TValidacaoCampo.CampoPreenchido(fdmProdutoPRODUTO) and
    TValidacaoCampo.CampoMaiorQueZero(fdmProdutoVALOR) and
    TValidacaoCampo.CampoMaiorQueZero(fdmProdutoQUANTIDADE) and
    TValidacaoCampo.CampoMaiorQueZero(fdmProdutoTOTAL) then
    SalvarProduto;
end;

procedure TMovVendaVarejo.EdtQuantidadeProdutoEnter(Sender: TObject);
begin
  inherited;
  if fdmProduto.FieldByName('CODIGO').AsString.IsEmpty then
  begin
    TControlsUtils.SetFocus(EdtCliente);
  end;
end;

procedure TMovVendaVarejo.EdtValorUnitarioProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  valorRetorno: Double;
begin
  inherited;

  if Key = VK_F1 then
  begin
    if TFrmSolicitarValor.SolicitarValor('Informe o valor unit�rio', valorRetorno) then
    begin
      fdmProdutoQUANTIDADE.AsFloat := valorRetorno / fdmProdutoVL_VENDA.AsFloat;
    end;
  end;
end;

procedure TMovVendaVarejo.EmitirDocumento;
var
  movVendaVarejoEmitirDocumento: TMovVendaVarejoEmitirDocumento;
  id: Integer;
begin
  if cxpcMain.ActivePage = tsPesquisa then
  begin
    id := fdmSearch.FieldByName('ID').AsInteger;
  end
  else
  begin
    if cdsVendaID.AsInteger = 0 then
    begin
      id := TVenda.GetId(cdsVenda.FieldByName('ID_CHAVE_PROCESSO').AsInteger);
    end
    else
    begin
      id := cdsVendaID.AsInteger;
    end;
  end;

  TMovVendaVarejoEmitirDocumento.EmissaoDocumentoVenda(id, GetVendaProxy);
end;

procedure TMovVendaVarejo.FechamentoVenda;
var AValorRecebido, AValorTroco: Double;
begin
  ValidarCamposObrigatorios;

  if cdsVendaID_PLANO_PAGAMENTO.AsInteger =
    StrtoIntDef(FParametros.Valor(TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA),0) then
  begin
    if TMovVendaVarejoRecebimento.RecebimentoAVista(cdsVendaVL_VENDA.AsFloat, AValorRecebido, AValorTroco) = MCONFIRMED then
    begin
      cdsVendaVL_PAGAMENTO.AsFloat := cdsVendaVL_VENDA.AsFloat;

      PreencherPlanoContaAVista;

      ActEfetivarVenda.Execute;
    end
  end
  else
  begin
    PreencherPlanoContaAPrazo;
    cxpcMain.ActivePage := tsFechamento;
    TControlsUtils.SetFocus(FFechamentoCrediario.EdtAcrescimo);

    if not cdsVendaParcela.IsEmpty then
    begin
      GerarParcelamento;
    end;
  end;
end;

procedure TMovVendaVarejo.filtroCodigoVendaExit(Sender: TObject);
begin
  inherited;
    //Deve existir devido ao bug que existe no onexit do filtro de pessoa.
  {
  //Corre��o de um bug que existe no filtro de pessoa, ap�s sair dele o bot�o pesquisar
  //n�o ganha foco ao passar com o mouse por cima dele, e nem recebe a��o de click qdo clicado.
  }
end;

procedure TMovVendaVarejo.filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := TFrmConsultaPadrao.ConsultarID('PESSOA');
  if idPessoa > 0 then
  begin
    filtroPessoa.EditValue := idPessoa;
    filtroPessoaPropertiesEditValueChanged(filtroPessoa);
  end;
end;

procedure TMovVendaVarejo.filtroPessoaPropertiesEditValueChanged(Sender: TObject);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0);
  if idPessoa > 0 then
  begin
    filtroNomePessoa.Caption := Copy(TPessoa.GetNomePessoa(idPessoa),1,40);
  end
  else
  begin
    filtroNomePessoa.Caption := '';
  end;

  //Corre��o de um bug que existe no filtro de pessoa, ap�s sair dele o bot�o pesquisar
  //n�o ganha foco ao passar com o mouse por cima dele, e nem recebe a��o de click qdo clicado.
  filtroCodigoVendaExit(Sender);

  abort;
end;

procedure TMovVendaVarejo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SalvarConfiguracaoGrid;
  dxRibbon1.ShowTabGroups := false;
  Application.ProcessMessages;
  FWhereSearch.Free;
  inherited;
end;

procedure TMovVendaVarejo.FormCreate(Sender: TObject);
begin
  inherited;
  AoInicializarFormulario;
end;

procedure TMovVendaVarejo.FormResize(Sender: TObject);
begin
  inherited;
  AlinharBarraTotalizadoraConsulta;
end;

procedure TMovVendaVarejo.GerarParcela(Sender: TField);
var
  planoPagamentoValido: Boolean;
  valorTituloValido: Boolean;
begin
  if not(FPodeExecutarGeracaoParcela) then
    exit;

  planoPagamentoValido := (not(cdsVendaID_PLANO_PAGAMENTO.AsString.IsEmpty) or
    (TDatasetUtils.ValorFoiAlterado(cdsVendaID_PLANO_PAGAMENTO))) and
    (not cdsVendaID_PLANO_PAGAMENTO.AsString.Equals(FParametros.Valor(TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA)));

  valorTituloValido := ((cdsVendaVL_VENDA.AsFloat > 0) or
    TDatasetUtils.ValorFoiAlterado(cdsVendaVL_VENDA)) and
    ((cdsVendaCC_VL_DIFERENCA.AsFloat > 0) or ((cdsVendaCC_VL_TROCO.AsFloat > 0)));

  if (planoPagamentoValido) and
     (valorTituloValido) then
  begin
    GerarParcelamento;
  end;
end;

procedure TMovVendaVarejo.GerarParcelamento;
var
  codigoFormaPagamento: Integer;
begin
  FPodeExecutarGeracaoParcela := false;
  try
    if FClienteConsumidorFinal then
    begin
      codigoFormaPagamento := FparametroFormaPagamentoPrazoConsumidorFinal.AsInteger;
    end
    else
    begin
      codigoFormaPagamento := FParametroFormaPagamentoCrediario.AsInteger;
    end;

    TVenda.LimparParcelas(cdsVendaParcela);
    TVenda.GerarParcelas(cdsVenda, cdsVendaParcela,
      codigoFormaPagamento, FParametroCarteira.AsInteger);
  finally
    FPodeExecutarGeracaoParcela := true;
    HabilitarBotaoEfetivar;
  end;
end;

procedure TMovVendaVarejo.GerenciarControles;
var
  possuiProdutos: boolean;
  estaVendendo: boolean;
  estaFaturando: boolean;
  estaPesquisando: boolean;
  estaDevolvendoCondicional: Boolean;
  estaLancandoReceitaOtica: Boolean;
  documentoAberto: Boolean;
  documentoCancelado: Boolean;
  documentoFechado: Boolean;
  possuiAlteracoes: Boolean;
  possuiDevolucaoCondicional: Boolean;
  vendaCondicional: Boolean;
  documentoEstaSalvoNoBancoDados: Boolean;
begin
  TControlsUtils.CongelarFormulario(Self);
  try
    estaVendendo := (cxpcMain.ActivePage = tsVenda);
    estaFaturando := (cxpcMain.ActivePage = tsFechamento);
    estaPesquisando := (cxpcMain.ActivePage = tsPesquisa);
    estaLancandoReceitaOtica := (cxpcMain.ActivePage = tsReceitaOtica);
    estaDevolvendoCondicional := (cxpcMain.ActivePage = tsDevolucaoCondicional);

    possuiProdutos := not(cdsVendaItem.IsEmpty);
    possuiDevolucaoCondicional := not(cdsVendaItemDevolucao.IsEmpty);
    documentoAberto := cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO);
    documentoCancelado := cdsVendaSTATUS.AsString.Equals(STATUS_CANCELADO);
    documentoFechado := cdsVendaSTATUS.AsString.Equals(STATUS_FECHADO);
    possuiAlteracoes := cdsVenda.ChangeCount > 0;
    documentoEstaSalvoNoBancoDados := cdsVendaID.AsInteger > 0;

    vendaCondicional := cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL);

    ActReceitaOtica.Visible := FParametroExibirAbaReceitaOtica.ValorSim and (estaVendendo or estaFaturando);

    ActVoltar.Visible := estaFaturando or estaPesquisando or estaLancandoReceitaOtica or estaDevolvendoCondicional;
    ActVenda.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActDevolucao.Visible := estaVendendo and possuiProdutos and documentoAberto and not(vendaCondicional);
    ActOrcamento.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActPedido.Visible := estaVendendo and possuiProdutos and documentoAberto;
    ActCondicional.Visible := estaVendendo and (possuiProdutos or possuiDevolucaoCondicional) and documentoAberto;
    ActDevolucaoCondicional.Visible := estaVendendo and documentoAberto and vendaCondicional;
    ActCancelarVenda.Visible := estaVendendo and documentoFechado;
    ActCancel.Visible := not(estaPesquisando) and possuiAlteracoes;
    ActNovaVenda.Visible := not(estaPesquisando) and not(ActCancel.Visible);
    ActPesquisar.Visible := estaPesquisando or estaVendendo and (not estaDevolvendoCondicional);

    ActExcluir.Visible := not(estaPesquisando) and documentoAberto
      and documentoEstaSalvoNoBancoDados and (not estaDevolvendoCondicional);

    ActImprimir.Visible := not(documentoAberto) or estaPesquisando;

    barConsultar.Visible := estaPesquisando;

    HabilitarBotaoEfetivar;

    ExibirBotaoBloqueios;

    BarAcao.Visible := ActVoltar.Visible or ActPedido.Visible or ActOrcamento.Visible or
      ActCondicional.Visible or ActVenda.Visible or ActDevolucao.Visible or
      ActEfetivarVenda.Visible or ActCancelarVenda.Visible or ActPesquisar.Visible or
      ActCancel.Visible or ActExcluir.Visible;

    BarInformacoes.Visible := cdsVendaID.AsInteger > 0;

    PnTopo.Enabled := cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO);
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TMovVendaVarejo.GerenciarSomenteLeitura(Dataset: TDataset);
begin
  if not(cdsVenda.Active) or
    not(cdsVendaSTATUS.AsString.Equals(STATUS_ABERTO) or  cdsVendaSTATUS.AsString.Equals(STATUS_BLOQUEADO)) then
  begin
    Abort;
  end;
end;

function TMovVendaVarejo.GetVendaProxy: TVendaProxy;
begin
  result := TVendaProxy.Create;
  result.FIdContaCorrente := FParametroContaCorrente.AsInteger;

  result.FIdFormaPagamentoDinheiro :=
    FParametroFormaPagamentoAVista.AsInteger;

  result.FIdCarteira := FParametroCarteira.AsInteger;
end;

procedure TMovVendaVarejo.HabilitarBotaoEfetivar;
var estaFaturando, possuiParcelamento: Boolean;
  possuiBloqueios: boolean;
begin
  estaFaturando := (cxpcMain.ActivePage = tsFechamento);
  possuiParcelamento := not cdsVendaParcela.IsEmpty;
  possuiBloqueios := BuscarQuantidadeBloqueios > 0;

  ActEfetivarVenda.Visible := estaFaturando and possuiParcelamento and (not possuiBloqueios);
end;

procedure TMovVendaVarejo.viewDevolucaoCondicionalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (not cdsVendaItemDevolucao.IsEmpty) AND (Key = VK_DELETE) then
  begin
    RemoverProdutoDaDevolucaoCondicional;
    cdsVendaItemDevolucao.Delete;
  end;
end;

procedure TMovVendaVarejo.viewPesquisaDataControllerDataChanged(
  Sender: TObject);
var
  existeFiltro: boolean;
  existiaFiltro: boolean;
begin
  inherited;
  existiaFiltro := (not FFiltroRegistroGradeAnterior.IsEmpty);
  existeFiltro := (not viewPesquisa.DataController.Filter.FilterText.IsEmpty);

  if (existiaFiltro or existeFiltro) and (not FCalculandoTotalRodape) then
  begin
    CalcularTotaisDoRodape;
  end;

  FFiltroRegistroGradeAnterior := viewPesquisa.DataController.Filter.FilterText;
end;

procedure TMovVendaVarejo.viewPesquisaDblClick(Sender: TObject);
begin
  inherited;
  CarregarDocumento(fdmSearch.FieldByName('id').AsInteger);
end;

procedure TMovVendaVarejo.viewPesquisaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    CarregarDocumento(fdmSearch.FieldByName('id').AsInteger);
end;

procedure TMovVendaVarejo.LiberarBloqueios;
begin
  TFrmLiberacaoBloqueioPersonalizado.AprovarBloqueios(cdsVendaBloqueioPersonalizado);

{  if BuscarQuantidadeBloqueios = 0 then
  begin
    //cdsVenda.Commit;
    //AbrirDataSetVenda(TVenda.GetId(cdsVendaID_CHAVE_PROCESSO.AsInteger));
  end; }

  GerenciarControles;
end;

procedure TMovVendaVarejo.LimparVenda;
begin
  cdsVenda.CancelUpdates;
end;

procedure TMovVendaVarejo.ModificarRotuloBotaoLiberarBloqueios;
begin
  ActLiberarBloqueios.Caption := 'Liberar Bloqueios ('+InttoStr(BuscarQuantidadeBloqueios)+')';
end;

procedure TMovVendaVarejo.MostrarInformacoesImplantacao;
begin
  inherited;
  TControlFunction.CreateMDIChild(TFrmHelpImplantacaoVendaVarejo,
    FrmHelpImplantacaoVendaVarejo);
end;

procedure TMovVendaVarejo.NovaVenda;
begin
  cxpcMain.ActivePage := tsVenda;

  AbrirDatasetVenda(CID_NULL);

  if not Assigned(cdsVenda.AfterInsert) then
    ConfigurarEventoInserirValoresPadrao(cdsVenda);

  try
    cdsVenda.Incluir;
    GerenciarControles;
    NovoProduto;
  finally
    if not(cdsVenda.State in dsEditModes) then
    begin
      ActNovaVenda.Execute;
    end;
  end;
end;

procedure TMovVendaVarejo.NovoProduto;
begin
  FInserindoProduto := true;

  if fdmProduto.State in dsEditModes then
    fdmProduto.Cancel;

  if not fdmProduto.IsEmpty then
    fdmProduto.EmptyDataSet;

  fdmProduto.Append;
  fdmProdutoNR_ITEM.AsInteger := cdsVendaItem.RecordCount + 1;
  fdmProdutoPERC_DESCONTO.AsFloat := 0;

  FEstadoProduto := Incluindo;

  if Self.Visible then
  begin
    Case TipoOperacaoInclusaoAtual of
      Venda:
      begin
        if PnCodigo.Visible then
          TControlsUtils.SetFocus(EdtCodigoProduto);
      end;
      DevolucaoCondicional:
      begin
        if PnCodigoProdutoDevolucaoCondicional.Visible then
          TControlsUtils.SetFocus(EdtCodigoProduto);
      end;
    End;
  end;
end;

function TMovVendaVarejo.ParcelaNaFormaPagamentoCartao: Boolean;
begin
  result := cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovVendaVarejo.PesquisarVenda;
begin
  Consultar;
end;

function TMovVendaVarejo.PlanoPagamentoAVista: Boolean;
begin
  result := FParametroPlanoPagamentoAVista.AsInteger = cdsVendaID_PLANO_PAGAMENTO.AsInteger;
end;

procedure TMovVendaVarejo.SalvarConfiguracaoGrid;
begin
  TUsuarioGridView.SaveGridView(viewProduto,
    TSistema.Sistema.usuario.idSeguranca, Self.Name+viewProduto.Name);
end;

procedure TMovVendaVarejo.SalvarProduto;
var
  dataset: TDataset;
begin
  case TipoOperacaoInclusaoAtual of
    Venda:
    begin
      dataset := cdsVendaItem;
    end;
    DevolucaoCondicional:
    begin
      dataset := cdsVendaItemDevolucao;
    end;
  end;

  case FEstadoProduto of
    Incluindo:
    begin
      dataset.Append;
    end;
    Alterando:
    begin
      dataset.Edit;
    end;
  end;

  dataset.FieldByName('ID_PRODUTO').AsString := fdmProdutoID_PRODUTO.AsString;
  dataset.FieldByName('NR_ITEM').AsFloat := TDatasetUtils.MaiorValor(dataset.FieldByName('NR_ITEM'))+1;
  dataset.FieldByName('QUANTIDADE').AsFloat := fdmProdutoQUANTIDADE.AsFloat;

  dataset.FieldByName('VL_DESCONTO').AsFloat := TMathUtils.ValorSobrePercentual(
    fdmProdutoPERC_DESCONTO.AsFloat, fdmProdutoVALOR.AsFloat);

  dataset.FieldByName('VL_ACRESCIMO').AsFloat := 0;
  dataset.FieldByName('VL_BRUTO').AsFloat := fdmProdutoVALOR.AsFloat;

  dataset.FieldByName('VL_LIQUIDO').AsFloat := fdmProdutoTOTAL.AsFloat;

  dataset.FieldByName('PERC_DESCONTO').AsFloat := fdmProdutoPERC_DESCONTO.AsFloat;
  dataset.FieldByName('PERC_ACRESCIMO').AsFloat := 0;
  dataset.FieldByName('JOIN_DESCRICAO_PRODUTO').AsString := fdmProdutoPRODUTO.AsString;

  if Assigned(dataset.FindField('DESCRICAO_PRODUTO')) then
  begin
    dataset.FieldByName('DESCRICAO_PRODUTO').AsString := fdmProdutoPRODUTO.AsString;
  end;

  dataset.FieldByName('JOIN_SIGLA_UNIDADE_ESTOQUE').AsString := 'UN';
  dataset.FieldByName('JOIN_ESTOQUE_PRODUTO').AsFloat := 0;
  dataset.FieldByName('JOIN_CODIGO_BARRA_PRODUTO').AsString := fdmProdutoCODIGO.AsString;

  dataset.Post;

  NovoProduto;
end;

function TMovVendaVarejo.TipoOperacaoInclusaoAtual: TTipoOperacaoInclusao;
begin
  if cxpcMain.ActivePage = tsVenda then
  begin
    result := Venda;
  end
  else if cxpcMain.ActivePage = tsDevolucaoCondicional then
  begin
    result := DevolucaoCondicional;
  end;
end;

procedure TMovVendaVarejo.TotalizarValoresVenda;
var
  cdsClone: TClientDataset;
  valorDesconto: Double;
  valorAcrescimo: Double;
  valorBruto: Double;
  valorLiquido: Double;
begin

  if FRateandoDescontoTotal then
    Exit;

  valorDesconto := 0;
  valorAcrescimo := 0;
  valorBruto := 0;
  valorLiquido := 0;

  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(cdsVendaItem), true);
  try
    FCalculandoDescontoDaVenda := true;

    cdsClone.First;
    while not cdsClone.Eof do
    begin
      valorDesconto := valorDesconto + (cdsClone.FieldByName('QUANTIDADE').AsFloat *
        cdsClone.FieldByName('VL_DESCONTO').AsFloat);

      valorAcrescimo := valorAcrescimo + (cdsClone.FieldByName('QUANTIDADE').AsFloat * cdsClone.FieldByName('VL_ACRESCIMO').AsFloat);

      valorBruto := valorBruto + (cdsClone.FieldByName('QUANTIDADE').AsFloat *
        cdsClone.FieldByName('VL_BRUTO').AsFloat);

      valorLiquido := valorLiquido + cdsClone.FieldByName('VL_LIQUIDO').AsFloat;

      cdsClone.Next;
    end;

    if not cdsVenda.EstadoDeAlteracao then
    begin
      cdsVenda.Alterar;
    end;

    cdsVendaVL_ACRESCIMO.AsFloat := valorAcrescimo;
    cdsVendaVL_TOTAL_PRODUTO.AsFloat := valorBruto;
    cdsVendaVL_VENDA.AsFloat := valorLiquido;

    cdsVendaVL_DESCONTO.AsFloat := valorDesconto;
    cdsVendaPERC_DESCONTO.AsFloat := TMathUtils.PercentualSobreValor(cdsVendaVL_DESCONTO.AsFloat
                                                                    ,cdsVendaVL_TOTAL_PRODUTO.AsFloat);
  finally
    FCalculandoDescontoDaVenda := false;
    FreeAndNil(cdsClone);
  end;
end;

procedure TMovVendaVarejo.ValidarCamposObrigatorios;
begin
  TValidacaoCampo.CampoPreenchido(cdsVendaID_PESSOA);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_CENTRO_RESULTADO);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_CONTA_ANALISE);
  TValidacaoCampo.CampoPreenchido(cdsVendaID_TABELA_PRECO);
end;

procedure TMovVendaVarejo.ValidarInformacoesCartao;
begin
  if ParcelaNaFormaPagamentoCartao and (cdsVendaParcelaID_OPERADORA_CARTAO.AsInteger <= 0) then
  begin
    TFrmMessage.Information('Em parcelas com cart�o � necess�rio informar a operadora do cart�o.');
    Abort;
  end;
end;

procedure TMovVendaVarejo.ValidarPreenchimentoDaOperadoraCartao;
var
  cdsClone: TClientDataset;
begin
  cdsClone := TClientDataset.Create(nil);
  cdsClone.CloneCursor(TClientDataset(cdsVendaParcela), true);
  try
    cdsClone.First;
    while not cdsClone.Eof do
    begin
      ValidarInformacoesCartao;
      cdsClone.Next;
    end;
  finally
    FreeAndNil(cdsClone);
  end;
end;

function TMovVendaVarejo.ViewAtivaNaTela: TcxGridDBBandedTableView;
begin
  if cxpcMain.ActivePage = tsVenda then
    result := viewProduto
  else if cxpcMain.ActivePage = tsPesquisa then
    result := viewPesquisa
  else if cxpcMain.ActivePage = tsDevolucaoCondicional then
    result := viewDevolucaoCondicional;
end;

procedure TMovVendaVarejo.Consultar;
var DSList: TFDJSONDataSets;
begin
  inherited;
  try
    try
      SetWhereBundle;
      if fdmSearch.Active then
        fdmSearch.EmptyDataSet;

      DSList := DmConnection.ServerMethodsClient.GetDados(
        Self.Name, FWhereSearch.where, TSistema.Sistema.Usuario.idSeguranca);
      try
        TDatasetUtils.JSONDatasetToMemTable(DSList, fdmSearch);

         TcxGridUtils.AdicionarTodosCamposNaView(viewPesquisa);
         TcxGridUtils.OcultarTodosCamposView(viewPesquisa);

        TUsuarioGridView.LoadGridView(viewPesquisa,
          TSistema.Sistema.Usuario.idSeguranca, Self.Name+viewPesquisa.Name);
      finally
        FreeAndNil(DSList);
      end;
      ClearWhereBundle;

      ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
    finally
      CalcularTotaisDoRodape;
    end;
  Except
    TFrmMessage.Failure('Falha ao consultar os dados, verifique o SQL associado a esta tela.');
    abort;
  end;
end;

procedure TMovVendaVarejo.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Forma de Pagamento a Vista
  FparametroFormaPagamentoAVista := TParametroFormulario.Create;
  FparametroFormaPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoAVista);

  //Parametro Plano de Pagamento a Vista
  FparametroPlanoPagamentoAVista := TParametroFormulario.Create;
  FparametroPlanoPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroPlanoPagamentoAVista);

  //Parametro Forma de Pagamento Crediario
  FparametroFormaPagamentoCrediario := TParametroFormulario.Create;
  FparametroFormaPagamentoCrediario.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_CREDIARIO;
  FparametroFormaPagamentoCrediario.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_CREDIARIO;
  FparametroFormaPagamentoCrediario.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoCrediario);

  //Parametro Forma de Pagamento Prazo quando cliente � consumidor final
  FparametroFormaPagamentoPrazoConsumidorFinal := TParametroFormulario.Create;
  FparametroFormaPagamentoPrazoConsumidorFinal.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_PRAZO_CONSUMIDOR_FINAL;
  FparametroFormaPagamentoPrazoConsumidorFinal.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_PRAZO_CONSUMIDOR_FINAL;
  FparametroFormaPagamentoPrazoConsumidorFinal.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoPrazoConsumidorFinal);

  //Parametro Conta Corrente
  FparametroContaCorrente := TParametroFormulario.Create;
  FparametroContaCorrente.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FparametroContaCorrente.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_CORRENTE;
  FparametroContaCorrente.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroContaCorrente);

  //Parametro Centro de Resultado A Vista
  FParametroCentroResultadoAVista := TParametroFormulario.Create;
  FParametroCentroResultadoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroCentroResultadoAVista);

  //Parametro Conta de Analise A Vista
  FParametroContaAnaliseAVista := TParametroFormulario.Create;
  FParametroContaAnaliseAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaAnaliseAVista);

  //Parametro Centro de Resultado A Prazo
  FParametroCentroResultadoAPrazo := TParametroFormulario.Create;
  FParametroCentroResultadoAPrazo.parametro :=
    TConstParametroFormulario.PARAMETRO_CENTRO_RESULTADO_APRAZO;
  FParametroCentroResultadoAPrazo.descricao :=
    TConstParametroFormulario.DESCRICAO_CENTRO_RESULTADO_APRAZO;
  FParametroCentroResultadoAPrazo.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroCentroResultadoAPrazo);

  //Parametro Conta de Analise A Prazo
  FParametroContaAnaliseAPrazo := TParametroFormulario.Create;
  FParametroContaAnaliseAPrazo.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_ANALISE_APRAZO;
  FParametroContaAnaliseAPrazo.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_ANALISE_APRAZO;
  FParametroContaAnaliseAPrazo.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaAnaliseAPrazo);

  //Exibir tela de emiss�o de documento
  FParametroExibeEmissaoDocumentoAoEfetivar := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_EMISSAO_DOCUMENTO_AO_EFETIVAR,
    TConstParametroFormulario.DESCRICAO_EXIBIR_EMISSAO_DOCUMENTO_AO_EFETIVAR);
  AParametros.AdicionarParametro(FParametroExibeEmissaoDocumentoAoEfetivar);

  //Carteira
  FParametroCarteira := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCarteira);

  //Exibir aba de Receita �tica
  FParametroExibirAbaReceitaOtica := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_ABA_RECEITA_OTICA,
    TConstParametroFormulario.DESCRICAO_EXIBIR_ABA_RECEITA_OTICA,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroExibirAbaReceitaOtica);

  //Permitir Venda Condicional para consumidor final
  FParametroPermitirVendaParaConsumidorFinalNaModalidadeCondicional := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_PERMITIR_VENDA_PARA_CONSUMIDOR_FINAL_NA_MODALIDADE_CONDICIONAL,
    TConstParametroFormulario.DESCRICAO_PERMITIR_VENDA_PARA_CONSUMIDOR_FINAL_NA_MODALIDADE_CONDICIONAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroPermitirVendaParaConsumidorFinalNaModalidadeCondicional);

  //Inclus�o autom�tica do produto ao digitar o c�digo de barras
  FParametroInclusaoDiretaAoPreencherCodigoBarras := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_INCLUSAO_DIRETA_AO_PREENCHER_CODIGO_BARRAS,
    TConstParametroFormulario.DESCRICAO_INCLUSAO_DIRETA_AO_PREENCHER_CODIGO_BARRAS,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroInclusaoDiretaAoPreencherCodigoBarras);

  //Habilitar Edi��o do Valor de Venda
  FParametroHabilitarEdicaoValorVenda := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EDICAO_VALOR_VENDA,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EDICAO_VALOR_VENDA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEdicaoValorVenda);

  //Habilitar Edi��o da Descri��o do Produto
  FParametroHabilitarEdicaoDescricaoProduto := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EDICAO_DESCRICAO_PRODUTO,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EDICAO_DESCRICAO_PRODUTO,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEdicaoDescricaoProduto);

    //Habilitar Emiss�o de NFCE
  FParametroHabilitarEmissaoNFCE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFCE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFCE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFCE);

  //Habilitar Emiss�o de NFE
  FParametroHabilitarEmissaoNFE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFE);

  //Exibir campo opera��o comercial
  FParametroExibirOperacaoComercial := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_OPERACAO_COMERCIAL,
    TConstParametroFormulario.DESCRICAO_EXIBIR_OPERACAO_COMERCIAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirOperacaoComercial);

  //Usar like na consulta de produto
  FUsarLikeConsultaProduto := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_USAR_LIKE_PESQUISA_PRODUTO,
    TConstParametroFormulario.DESCRICAO_USAR_LIKE_PESQUISA_PRODUTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FUsarLikeConsultaProduto);

  FParametroFiltroPadraoTipoVenda := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_TIPO_DOCUMENTO_VENDA,
    TConstParametroFormulario.DESCRICAO_FILTRO_TIPO_DOCUMENTO_VENDA,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, FILTRO_TODOS);
  AParametros.AdicionarParametro(FParametroFiltroPadraoTipoVenda);
end;

procedure TMovVendaVarejo.SetWhereBundle;
var AItemList: TcxFilterCriteriaItemList;
    BetweenCondicional: array of string;
    textoBetween: String;
begin
  inherited;
  ClearWhereBundle;

  //Pessoa
  if not VartoStr(filtroPessoa.EditValue).IsEmpty then
    FWhereSearch.AddIgual(FIELD_PESSOA, StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0));

  //Numero do Documento
  if not VartoStr(filtroCodigoVenda.EditValue).IsEmpty then
    FWhereSearch.AddIgual(FIELD_CODIGO, StrtoIntDef(VartoStr(filtroCodigoVenda.EditValue), 0));

  //Data de Emissao
  if not(VartoStr(filtroDataCadastroInicial.EditValue).IsEmpty) and
     not(VartoStr(filtroDataCadastroFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataCadastroInicial.EditValue) <= VarToDateTime(filtroDataCadastroFIm.EditValue)) then
    FWhereSearch.AddEntre(FIELD_DATA_CADASTRO, StrtoDate(filtroDataCadastroInicial.EditValue),
      StrtoDate(filtroDataCadastroFIm.EditValue));

  //Data de Fechamento
  if not(VartoStr(filtroDataDocumentoInicio.EditValue).IsEmpty) and
     not(VartoStr(filtroDataDocumentoFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataDocumentoInicio.EditValue) <= VarToDateTime(filtroDataDocumentoFIm.EditValue)) then
   FWhereSearch.AddEntre(FIELD_DATA_FECHAMENTO, StrtoDate(filtroDataDocumentoInicio.EditValue),
     StrtoDate(filtroDataDocumentoFIm.EditValue));

  //Tipo do Documento
  if not VartoStr(filtroTipoDocumento.Text).Equals(FILTRO_TODOS) then
    FWhereSearch.AddIgual(FIELD_TIPO_DOCUMENTO,VartoStr(filtroTipoDocumento.Text));

  //Status
  if not VartoStr(filtroSituacao.Text).Equals(FILTRO_TODOS) then
    FWhereSearch.AddIgual(FIELD_SITUACAO_DOCUMENTO,VartoStr(filtroSituacao.Text));
end;

procedure TMovVendaVarejo.SugerirDataVencimentoCheque;
begin
  if not cdsVendaParcela.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    cdsVendaParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsVendaParcelaCHEQUE_DT_VENCIMENTO.AsDateTime := cdsVendaParcelaDT_VENCIMENTO.AsDateTime;
  end
  else
  begin
    cdsVendaParcelaCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

procedure TMovVendaVarejo.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TMovVendaVarejo.PreencherPlanoContaAPrazo;
begin
  //Centro de Resultado
  cdsVendaID_CENTRO_RESULTADO.AsInteger := StrtoIntDef(FParametroCentroResultadoAPrazo.valor,0);
  cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO.AsString :=
    TPlanoConta.GetDescricaoCentroResultado(cdsVendaID_CENTRO_RESULTADO.AsInteger);

  //Dados Conta de Analise
  cdsVendaID_CONTA_ANALISE.AsInteger := StrtoIntDef(FParametroContaAnaliseAPrazo.valor,0);
  TPesqContaAnalise.ExecutarConsultaOculta(cdsVendaID_CENTRO_RESULTADO.AsInteger,
  cdsVendaID_CONTA_ANALISE, cdsVendaJOIN_DESCRICAO_CONTA_ANALISE);
end;

procedure TMovVendaVarejo.PreencherPlanoContaAVista;
begin
  //Centro de Resultado
  cdsVendaID_CENTRO_RESULTADO.AsInteger := StrtoIntDef(FParametroCentroResultadoAVista.valor,0);
  cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO.AsString := TPlanoConta.GetDescricaoCentroResultado(cdsVendaID_CENTRO_RESULTADO.AsInteger);

  //Dados Conta de Analise
  cdsVendaID_CONTA_ANALISE.AsInteger := StrtoIntDef(FParametroContaAnaliseAVista.valor,0);
  TPesqContaAnalise.ExecutarConsultaOculta(cdsVendaID_CENTRO_RESULTADO.AsInteger,
  cdsVendaID_CONTA_ANALISE, cdsVendaJOIN_DESCRICAO_CONTA_ANALISE);
end;

procedure TMovVendaVarejo.PreencherNomePessoaNoFiltro;
begin

end;

procedure TMovVendaVarejo.PrepararFiltros;
begin
  FWhereSearch := TFiltroPadrao.Create;

  filtroCodigoVenda.EditValue := null;
  filtroPessoa.EditValue := null;
  filtroNomePessoa.Caption := '';
  filtroDataCadastroInicial.EditValue := Date;
  filtroDataCadastroFIm.EditValue := Date;
  filtroDataDocumentoInicio.EditValue := null;
  filtroDataDocumentoFIm.EditValue := null;

  if FParametroFiltroPadraoTipoVenda.AsString.IsEmpty then
  begin
    filtroTipoDocumento.Text := FILTRO_TODOS;
  end
  else
  begin
    filtroTipoDocumento.Text := FParametroFiltroPadraoTipoVenda.AsString;
  end;

  filtroSituacao.Text := FILTRO_TODOS;
end;

procedure TMovVendaVarejo.ExecutarDelecaoItem;
begin
  if (not FDeletandoItemCondicionalManualmente) and
      cdsVendaTIPO.AsString.Equals(TIPO_DOCUMENTO_CONDICIONAL) then
  begin
    try
      TControlsUtils.CongelarFormulario(Self);
      FDeletandoItemCondicionalManualmente := true;
      cxpcMain.ActivePage := tsDevolucaoCondicional;
      CarregarInformacoesDoItemParaDevolucao;
      SalvarProduto;
    finally
      cxpcMain.ActivePage := tsVenda;
      FDeletandoItemCondicionalManualmente := false;
      GerenciarControles;
      TControlsUtils.DescongelarFormulario;
    end;
  end
  else
  begin
    cdsVendaItem.Delete;
  end;
end;

procedure TMovVendaVarejo.ExibirBotaoBloqueios;
var
  existeBloqueios: Boolean;
begin
  existeBloqueios := BuscarQuantidadeBloqueios > 0;

  if existeBloqueios then
  begin
    ModificarRotuloBotaoLiberarBloqueios;
  end;

  ActLiberarBloqueios.Visible := existeBloqueios;
end;

procedure TMovVendaVarejo.ExibirFiltroDeMemoriaDaGrid(
  const AExibir: Boolean);
begin
  viewPesquisa.FilterRow.Visible := AExibir;
end;

procedure TMovVendaVarejo.ExibirOperacaoComercial;
begin
  PnOperacao.Visible := FParametroExibirOperacaoComercial.ValorSim;
end;

procedure TMovVendaVarejo.fdmSearchAfterClose(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(false);
end;

procedure TMovVendaVarejo.fdmSearchAfterDelete(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
end;

procedure TMovVendaVarejo.fdmSearchAfterOpen(DataSet: TDataSet);
begin
  inherited;
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
  viewPesquisa.DataController.Groups.FullExpand;
  fdmSearch.First;
end;

procedure TMovVendaVarejo.ActReceitaOticaExecute(Sender: TObject);
begin
  inherited;
  cxpcMain.ActivePage := tsReceitaOtica;

  GerenciarControles;
end;

procedure TMovVendaVarejo.ActRestaurarColunasPadraoExecute(Sender: TObject);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, Self.Name);
  ViewAtivaNaTela.RestoreDefaults;
end;

procedure TMovVendaVarejo.ActAbrirConsultandoExecute(Sender: TObject);
var consultandoAutomaticamente: boolean;
begin
  consultandoAutomaticamente := FConsultaAutomatica;

  TUsuarioPesquisaPadrao.SetConsultaAutomatica(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name,
    not consultandoAutomaticamente);

  FConsultaAutomatica := not consultandoAutomaticamente;
end;

procedure TMovVendaVarejo.ActAlterarColunasGridExecute(Sender: TObject);
begin
  TFrmApelidarColunasGrid.SetColumns(ViewAtivaNaTela);
end;

procedure TMovVendaVarejo.ActAlterarSQLPesquisaPadraoExecute(
  Sender: TObject);
begin
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, Self.Name) then
    Consultar;
end;

procedure TMovVendaVarejo.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  TUsuarioGridView.SaveGridView(ViewAtivaNaTela,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+ViewAtivaNaTela.Name);
end;

procedure TMovVendaVarejo.pmGridConsultaPadraoPopup(Sender: TObject);
begin
  ActAbrirConsultando.Caption := TFunction.Iif(FConsultaAutomatica,
    'Desabilitar Consulta Autom�tica', 'Habilitar Consulta Autom�tica');
end;

procedure TMovVendaVarejo.pmTituloGridPesquisaPadraoPopup(Sender: TObject);
begin
  ActExibirAgrupamento.Caption := TFunction.Iif(ViewAtivaNaTela.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');
end;

procedure TMovVendaVarejo.ConfigurarPainelTituloDevolucaoCondicional;
begin
  PnTituloDevolucaoCondicional.Transparent := false;
  PnTituloDevolucaoCondicional.PanelStyle.OfficeBackgroundKind := pobkGradient;
end;

Initialization
  RegisterClass(TMovVendaVarejo);

Finalization
  UnRegisterClass(TMovVendaVarejo);

end.
