unit uMovInventario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBPanel, uGBDBTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, uGBDBDateEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxRadioGroup, uGBDBRadioGroup, cxCalc,
  cxSpinEdit, uGBDBSpinEdit, JvBaseDlg, JvDesktopAlert, cxButtonEdit,
  uGBDBButtonEditFK, dxRibbonRadialMenu, JvComponentBase, JvEnterTab, cxSplitter, JvExControls, JvButton,
  JvTransparentButton;

type
  TMovInventario = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataTIPO: TStringField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataDH_INICIO: TDateTimeField;
    cdsDataDH_FIM: TDateTimeField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataJOIN_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_USUARIO: TStringField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDatafdqInventarioProduto: TDataSetField;
    cdsInventarioProduto: TGBClientDataSet;
    cdsInventarioProdutoID: TAutoIncField;
    cdsInventarioProdutoID_INVENTARIO: TIntegerField;
    cdsInventarioProdutoID_PRODUTO: TIntegerField;
    cdsInventarioProdutoJOIN_DESCRICAO_PRODUTO: TStringField;
    labelCodigo: TLabel;
    labelDtCadastro: TLabel;
    labelProcesso: TLabel;
    labelSituacao: TLabel;
    lbDtFechamento: TLabel;
    pkCodigo: TgbDBTextEditPK;
    edDtCadastro: TgbDBDateEdit;
    descProcesso: TgbDBTextEdit;
    edtChaveProcesso: TgbDBTextEdit;
    edSituacao: TgbDBTextEdit;
    EdtDtFechamento: TgbDBDateEdit;
    gbPanel1: TgbPanel;
    gbDBDateEdit1: TgbDBDateEdit;
    Label1: TLabel;
    gbDBDateEdit2: TgbDBDateEdit;
    Label2: TLabel;
    rgTipoConferencia: TgbDBRadioGroup;
    PnProdutos: TgbPanel;
    gridInventario: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Label3: TLabel;
    EdtCodigoBarra: TcxTextEdit;
    dsInventarioProduto: TDataSource;
    cdsDataCONTAGEM_ESCOLHIDA: TIntegerField;
    cdsDataSTATUS: TStringField;
    cdsInventarioProdutoQUANTIDADE_ESTOQUE: TFMTBCDField;
    cdsInventarioProdutoPRIMEIRA_CONTAGEM: TFMTBCDField;
    cdsInventarioProdutoSEGUNDA_CONTAGEM: TFMTBCDField;
    cdsInventarioProdutoTERCEIRA_CONTAGEM: TFMTBCDField;
    cdsInventarioProdutoJOIN_CODIGO_BARRA_PRODUTO: TStringField;
    cxGridDBBandedTableView1ID_PRODUTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1QUANTIDADE_ESTOQUE: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1PRIMEIRA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1CC_DIFERENCA_PRIMEIRA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1SEGUNDA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1CC_DIFERENCA_SEGUNDA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1TERCEIRA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1CC_DIFERENCA_TERCEIRA_CONTAGEM: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn;
    cdsInventarioProdutoCC_DIFERENCA_PRIMEIRA_CONTAGEM: TFloatField;
    cdsInventarioProdutoCC_DIFERENCA_SEGUNDA_CONTAGEM: TFloatField;
    cdsInventarioProdutoCC_DIFERENCA_TERCEIRA_CONTAGEM: TFloatField;
    Label4: TLabel;
    jvdaAlertaConferencia: TJvDesktopAlert;
    cbConferencia: TcxComboBox;
    ActReabrir: TAction;
    ActEfetivar: TAction;
    lbMotivo: TLabel;
    edtMotivo: TgbDBButtonEditFK;
    edtDescMotivo: TgbDBTextEdit;
    cdsDataDH_FECHAMENTO: TDateTimeField;
    cdsDataID_MOTIVO: TIntegerField;
    cdsDataJOIN_DESCRICAO_MOTIVO: TStringField;
    ActBuscarProdutos: TAction;
    bbEfetivar: TdxBarLargeButton;
    bbReabrir: TdxBarLargeButton;
    bbIncluirProdutos: TdxBarLargeButton;
    ActReiniciarPrimeiraContagem: TAction;
    ActReiniciarSegundaContagem: TAction;
    ActReiniciarTerceiraContagem: TAction;
    ActExcluirTodaConferencia: TAction;
    ActBuscarProdutoSelecionado: TAction;
    ActBuscarTodosProdutos: TAction;
    dxRibbonRadialMenu: TdxRibbonRadialMenu;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    ActEstornarContagem: TAction;
    dxBarButton8: TdxBarButton;
    ActExcluirProdutoConferencia: TAction;
    dxBarButton9: TdxBarButton;
    lbContagemCorreta: TLabel;
    EdtContagemCorreta: TgbDBTextEdit;
    JvEnterAsTab: TJvEnterAsTab;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ConferirProduto(Sender: TObject);
    procedure ActReabrirExecute(Sender: TObject);
    procedure ActEfetivarExecute(Sender: TObject);
    procedure ActBuscarProdutosExecute(Sender: TObject);
    procedure cxGridDBBandedTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cdsInventarioProdutoAfterPost(DataSet: TDataSet);
    procedure cdsInventarioProdutoAfterDelete(DataSet: TDataSet);
    procedure ActReiniciarPrimeiraContagemExecute(Sender: TObject);
    procedure ActReiniciarSegundaContagemExecute(Sender: TObject);
    procedure ActReiniciarTerceiraContagemExecute(Sender: TObject);
    procedure ActExcluirTodaConferenciaExecute(Sender: TObject);
    procedure ActBuscarProdutoSelecionadoExecute(Sender: TObject);
    procedure ActBuscarTodosProdutosExecute(Sender: TObject);
    procedure cdsInventarioProdutoCalcFields(DataSet: TDataSet);
    procedure ActEstornarContagemExecute(Sender: TObject);
    procedure ActExcluirProdutoConferenciaExecute(Sender: TObject);
  private
    var FIconeAlertaConferenciaEncontrada: TBitmap;
    var FIconeAlertaConferenciaNaoEncontrada: TBitmap;
    const ICONE_PRODUTO_ENCONTRADO = 13;
    const ICONE_PRODUTO_NAO_ENCONTRADO = 14;

    procedure CarregarImagensAlertaConferencia;
    procedure ExibirAlertaConferencia(AProdutoEncontrado: Boolean;
      AIdProduto: Integer = 0; ADescricaoProduto: String = '');
    procedure ExibirConferencias;
    procedure BloquearEdicaoDoTipoDeConferencia;
    procedure IncluirProdutosNaConferencia;
    procedure AdicionarProduto(AIdProduto: Integer; AConferir: Boolean = true);
    procedure HabilitarCamposFechamento;
    procedure HabilitarBotaoReabrir;
    procedure HabilitarBotaoEfetivar;
    procedure HabilitarBotaoAdicionarProdutos;
    procedure HabilitarBotaoRemover;
    procedure ReiniciarContagem(const AContagem: Integer);
    procedure IncrementarConferencia;
    procedure DecrementarConferencia;
  public
    procedure AoCriarFormulario; override;
    procedure BloquearEdicao; override;
    procedure AntesDeRemover; override;
    procedure GerenciarControles; override;
  end;

var
  MovInventario: TMovInventario;

implementation

{$R *.dfm}

uses uDmConnection, uInventarioProxy, uSistema, uChaveProcesso,
  uChaveProcessoProxy, uDmAcesso, uProduto, uProdutoProxy, uPesqProduto,
  uFrmMessage_Process, uTMessage, uFrmMessage, uFrmAutenticacaoUsuario,
  uInventario, uControlsUtils, uDevExpressUtils, uCadProduto, uDatasetUtils,
  uMovInventarioDefinirContagemValida;

procedure TMovInventario.ConferirProduto(Sender: TObject);
const
  PRODUTO_ENCONTRADO = true;
  PRODUTO_NAO_ENCONTRADO = false;
var
  codigoBarras: String;
  produto: TFDMemTable;
begin
  codigoBarras := (Sender as TcxTextEdit).Text;

  if Trim(codigoBarras).IsEmpty then
    Exit;

  if cdsInventarioProduto.Locate('JOIN_CODIGO_BARRA_PRODUTO', codigoBarras, []) or
    cdsInventarioProduto.Locate('ID_PRODUTO', codigoBarras, []) then
  begin
    ExibirAlertaConferencia(PRODUTO_ENCONTRADO,
      cdsInventarioProdutoID_PRODUTO.AsInteger,
      cdsInventarioProdutoJOIN_DESCRICAO_PRODUTO.AsString);
    IncrementarConferencia;
  end
  else
  begin
    produto := TFDMemTable.Create(nil);
    try
      produto := TProduto.GetProdutoPeloCodigoBarra(codigoBarras);

      if produto.IsEmpty then
      begin
        produto := TProduto.GetProduto(StrtointDef(codigoBarras, 0));
      end;

      if produto.IsEmpty then
      begin
        ExibirAlertaConferencia(PRODUTO_NAO_ENCONTRADO);
      end
      else
      begin
        AdicionarProduto(produto.FieldByName('ID').AsInteger);
        ExibirAlertaConferencia(PRODUTO_ENCONTRADO,
          produto.FieldByName('ID').AsInteger,
          produto.FieldByName('DESCRICAO').AsString);
      end;
    finally
      FreeAndNil(produto);
    end;
  end;
  (Sender as TcxTextEdit).Text := EmptyStr;
  TControlsUtils.SetFocus(Sender as TcxTextEdit);
end;

procedure TMovInventario.cxGridDBBandedTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
const BOTAO_IMPRESSORA: Integer = 16;
const BOTAO_EXPORTAR_EXCEL: Integer = 17;
begin
  inherited;
  if AButtonIndex = BOTAO_IMPRESSORA then
  begin
    dxComponentPrinterLink1.Component := gridInventario;
    dxComponentPrinterLink1.Preview();
  end
  else if AButtonIndex = BOTAO_EXPORTAR_EXCEL then
  begin
    TcxGridUtils.ExportarParaExcel(
      gridInventario, 'Invent�rio')
  end;
end;

procedure TMovInventario.DecrementarConferencia;
var
  fieldContagem: TField;
begin
  case cbConferencia.ItemIndex of
    0: fieldContagem := cdsInventarioProdutoPRIMEIRA_CONTAGEM;
    1: fieldContagem := cdsInventarioProdutoSEGUNDA_CONTAGEM;
    2: fieldContagem := cdsInventarioProdutoTERCEIRA_CONTAGEM;
  end;

  cdsInventarioProduto.Alterar;
  fieldContagem.AsFloat := fieldContagem.AsFloat - 1;
  cdsInventarioProduto.Salvar;
end;

procedure TMovInventario.ActBuscarProdutoSelecionadoExecute(Sender: TObject);
begin
  inherited;
  TCadProduto.AbrirTelaFiltrando(
    cdsInventarioProdutoID_PRODUTO.AsString);
end;

procedure TMovInventario.ActBuscarProdutosExecute(Sender: TObject);
begin
  inherited;
  IncluirProdutosNaConferencia;
end;

procedure TMovInventario.ActBuscarTodosProdutosExecute(Sender: TObject);
begin
  inherited;
  TCadProduto.AbrirTelaFiltrando(
    TDatasetUtils.Concatenar(cdsInventarioProdutoID_PRODUTO));
end;

procedure TMovInventario.ActEfetivarExecute(Sender: TObject);
var
  idChaveProcesso: Integer;
  contagemSelecionada: Integer;
begin
  inherited;
  if TFrmMessage.Question('Deseja Efetivar o Invent�rio?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    contagemSelecionada :=
      TMovInventarioDefinirContagemValida.BuscarContagem;

    if contagemSelecionada = 0 then
    begin
      TMessage.MessageInformation(
        'Defina uma contagem v�lida para poder efetivar o invent�rio');
      exit;
    end;

    if not (cdsData.State in dsEditModes) then
      cdsData.Edit;

    if cdsDataDH_INICIO.AsDateTime = 0 then
    begin
      cdsDataDH_INICIO.AsDateTime := Now;
    end;

    if cdsDataDH_FIM.AsDateTime = 0 then
    begin
      cdsDataDH_FIM.AsDateTime := Now;
    end;

    cdsDataDH_FECHAMENTO.AsDateTime := Now;
    cdsDataSTATUS.AsString := TInventarioProxy.STATUS_CONFIRMADO;
    cdsDataCONTAGEM_ESCOLHIDA.AsInteger := contagemSelecionada;

    ActConfirm.Execute;

    TFrmMessage_Process.SendMessage('Efetivando o Invent�rio');
    try
      TInventario.Efetivar(idChaveProcesso);
      AtualizarRegistro;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovInventario.ActEstornarContagemExecute(Sender: TObject);
begin
  inherited;
  DecrementarConferencia;
end;

procedure TMovInventario.ActExcluirProdutoConferenciaExecute(Sender: TObject);
begin
  inherited;
  if TMessage.MessageDeleteAsk() then
  begin
    cdsInventarioProduto.Delete;
  end;
end;

procedure TMovInventario.ActExcluirTodaConferenciaExecute(Sender: TObject);
begin
  if TMessage.MessageDeleteAsk(
    'Deseja realmente excluir todos os registros de confer�ncia?') then
  try
    cdsInventarioProduto.DisableControls;
    while not cdsInventarioProduto.IsEmpty do
    begin
      cdsInventarioProduto.Delete;
    end;
  finally
    cdsInventarioProduto.EnableControls;
  end;
end;

procedure TMovInventario.ActReabrirExecute(Sender: TObject);
var idChaveProcesso: Integer;
begin
  inherited;
  if TFrmMessage.Question('Deseja Reabrir o Invent�rio?') = MCONFIRMED then
  begin
    if TFrmAutenticacaoUsuario.Autorizado then
    begin
      idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

      if (cdsData.Alterando) then
      begin
        ActConfirm.Execute;
      end;

      TFrmMessage_Process.SendMessage('Reabrindo o Invent�rio');
      try
        TInventario.Reabrir(idChaveProcesso);
        AtualizarRegistro;
      finally
        TFrmMessage_Process.CloseMessage;
      end;
    end
    else
    begin
      TFrmMessage.Information('Usu�rio n�o possui Autoriza��o para realizar esta Opera��o.');
    end;
  end;
end;

procedure TMovInventario.ActReiniciarPrimeiraContagemExecute(Sender: TObject);
begin
  inherited;
  ReiniciarContagem(1);
end;

procedure TMovInventario.ActReiniciarSegundaContagemExecute(Sender: TObject);
begin
  inherited;
  ReiniciarContagem(2);
end;

procedure TMovInventario.ActReiniciarTerceiraContagemExecute(Sender: TObject);
begin
  inherited;
  ReiniciarContagem(3);
end;

procedure TMovInventario.AdicionarProduto(AIdProduto: Integer; AConferir: Boolean = true);
var produtoFilial: TProdutoProxy;
begin
  produtoFilial := TProduto.GetProdutoFilial(AIdProduto,
    cdsDataID_FILIAL.AsInteger);
  try
    cdsInventarioProduto.Incluir;
    cdsInventarioProdutoID_PRODUTO.AsInteger := produtoFilial.FId;

    cdsInventarioProdutoJOIN_DESCRICAO_PRODUTO.AsString :=
      produtoFilial.FDescricao;

    cdsInventarioProdutoJOIN_CODIGO_BARRA_PRODUTO.AsString :=
      produtoFilial.FCodigoBarra;

    cdsInventarioProdutoQUANTIDADE_ESTOQUE.AsFloat :=
      produtoFilial.FQtdeEstoque;

    if AConferir then
    begin
      case cbConferencia.ItemIndex of
        0: cdsInventarioProdutoPRIMEIRA_CONTAGEM.AsFloat := 1;
        1: cdsInventarioProdutoSEGUNDA_CONTAGEM.AsFloat := 1;
        2: cdsInventarioProdutoTERCEIRA_CONTAGEM.AsFloat := 1;
      else
        cdsInventarioProdutoPRIMEIRA_CONTAGEM.AsFloat := 1;
      end;
    end;
  finally
    FreeAndNil(produtoFilial);
    BloquearEdicaoDoTipoDeConferencia;
    HabilitarBotaoEfetivar;
  end;
end;

procedure TMovInventario.AntesDeRemover;
begin
  if not cdsDataSTATUS.AsString.Equals(TInventarioProxy.STATUS_ABERTO) then
  begin
    TMessage.MessageSucess(
      'S� � permitido excluir um invent�rio que esteja aberto.');
    exit;
  end;
  inherited;
end;

procedure TMovInventario.AoCriarFormulario;
begin
  inherited;
  CarregarImagensAlertaConferencia;
end;

procedure TMovInventario.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados :=
    not cdsDataSTATUS.AsString.Equals(TInventarioProxy.STATUS_ABERTO);
end;

procedure TMovInventario.BloquearEdicaoDoTipoDeConferencia;
begin
  rgTipoConferencia.Properties.ReadOnly := not cdsInventarioProduto.IsEmpty;
  EdtCodigoBarra.Properties.ReadOnly := Self.bloquearEdicaoDados;

  cxGridDBBandedTableView1PRIMEIRA_CONTAGEM.Options.Editing :=
    cdsDataTIPO.AsString.Equals(TInventarioProxy.TIPO_MANUAL);

  cxGridDBBandedTableView1SEGUNDA_CONTAGEM.Editing :=
    cdsDataTIPO.AsString.Equals(TInventarioProxy.TIPO_MANUAL);

  cxGridDBBandedTableView1TERCEIRA_CONTAGEM.Editing :=
    cdsDataTIPO.AsString.Equals(TInventarioProxy.TIPO_MANUAL);
end;

procedure TMovInventario.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataDH_INICIO.AsDateTime := Now;
  cdsDataTIPO.AsString := TInventarioProxy.TIPO_MANUAL;

  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.filial.Fid;
  cdsDataJOIN_FANTASIA_FILIAL.AsString := TSistema.Sistema.filial.FFantasia;

  cdsDataID_PESSOA_USUARIO.AsInteger := TSistema.Sistema.usuario.id;
  cdsDataJOIN_USUARIO.AsString := TSistema.Sistema.Usuario.usuario;

  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(
    TChaveProcessoProxy.ORIGEM_INVENTARIO, cdsDataID.AsInteger);
  cdsDataJOIN_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_INVENTARIO;

  cdsDataSTATUS.AsString := TInventarioProxy.STATUS_ABERTO;

  cdsDataCONTAGEM_ESCOLHIDA.AsInteger := 1;
end;

procedure TMovInventario.cdsInventarioProdutoAfterDelete(DataSet: TDataSet);
begin
  inherited;
  BloquearEdicaoDoTipoDeConferencia;
  HabilitarBotaoEfetivar;
end;

procedure TMovInventario.cdsInventarioProdutoAfterPost(DataSet: TDataSet);
begin
  inherited;
  BloquearEdicaoDoTipoDeConferencia;
  HabilitarBotaoEfetivar;
end;

procedure TMovInventario.cdsInventarioProdutoCalcFields(DataSet: TDataSet);
var
  qtdeAjuste: Double;
begin
  inherited;
  qtdeAjuste := 0;
  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') >
    cdsInventarioProduto.GetAsFloat('PRIMEIRA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('PRIMEIRA_CONTAGEM');
  end
  else  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') <
    cdsInventarioProduto.GetAsFloat('PRIMEIRA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('PRIMEIRA_CONTAGEM');

    qtdeAjuste := -1 * qtdeAjuste;
  end;
  cdsInventarioProduto.SetAsFloat('CC_DIFERENCA_PRIMEIRA_CONTAGEM', qtdeAjuste);

  qtdeAjuste := 0;
  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') >
    cdsInventarioProduto.GetAsFloat('SEGUNDA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('SEGUNDA_CONTAGEM');
  end
  else  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') <
    cdsInventarioProduto.GetAsFloat('SEGUNDA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('SEGUNDA_CONTAGEM');

    qtdeAjuste := -1 * qtdeAjuste;
  end;
  cdsInventarioProduto.SetAsFloat('CC_DIFERENCA_SEGUNDA_CONTAGEM', qtdeAjuste);

  qtdeAjuste := 0;
  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') >
    cdsInventarioProduto.GetAsFloat('TERCEIRA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('TERCEIRA_CONTAGEM');
  end
  else  if cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') <
    cdsInventarioProduto.GetAsFloat('TERCEIRA_CONTAGEM') then
  begin
    qtdeAjuste := cdsInventarioProduto.GetAsFloat('QUANTIDADE_ESTOQUE') -
      cdsInventarioProduto.GetAsFloat('TERCEIRA_CONTAGEM');

    qtdeAjuste := -1 * qtdeAjuste;
  end;
  cdsInventarioProduto.SetAsFloat('CC_DIFERENCA_TERCEIRA_CONTAGEM', qtdeAjuste);
end;

procedure TMovInventario.CarregarImagensAlertaConferencia;
begin
  FIconeAlertaConferenciaEncontrada := TBitMap.Create;
  FIconeAlertaConferenciaNaoEncontrada := TBitMap.Create;

  DmAcesso.cxImage32x32.GetBitmap(
    ICONE_PRODUTO_ENCONTRADO, FIconeAlertaConferenciaEncontrada);

  DmAcesso.cxImage32x32.GetBitmap(
    ICONE_PRODUTO_NAO_ENCONTRADO, FIconeAlertaConferenciaNaoEncontrada);
end;

procedure TMovInventario.ExibirAlertaConferencia(AProdutoEncontrado: Boolean;
  AIdProduto: Integer; ADescricaoProduto: String);
const
  MENSAGEM_PRODUTO_ENCONTRADO = 'Produto encontrado: ';
  MENSAGEM_PRODUTO_NAO_ENCONTRADO = 'Produto n�o encontrado';
begin
  if AProdutoEncontrado then
  begin
    jvdaAlertaConferencia.Image.Bitmap := FIconeAlertaConferenciaEncontrada;
    jvdaAlertaConferencia.MessageText := MENSAGEM_PRODUTO_ENCONTRADO+
      InttoStr(AIdProduto)+' '+ADescricaoProduto;
    MessageBeep(0);
  end
  else
  begin
    jvdaAlertaConferencia.Image.Bitmap := FIconeAlertaConferenciaNaoEncontrada;
    jvdaAlertaConferencia.MessageText := MENSAGEM_PRODUTO_NAO_ENCONTRADO;
  end;
  jvdaAlertaConferencia.Execute();
end;

procedure TMovInventario.ExibirConferencias;
begin
{  cxGridDBBandedTableView1PRIMEIRA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 1;

  cxGridDBBandedTableView1CC_DIFERENCA_PRIMEIRA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 1;

  cxGridDBBandedTableView1SEGUNDA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 2;

  cxGridDBBandedTableView1CC_DIFERENCA_SEGUNDA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 2;

  cxGridDBBandedTableView1TERCEIRA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 3;

  cxGridDBBandedTableView1CC_DIFERENCA_TERCEIRA_CONTAGEM.Visible :=
    cdsDataQUANTIDADE_CONTAGENS.AsInteger >= 3;   }
end;

procedure TMovInventario.GerenciarControles;
begin
  inherited;
  HabilitarBotaoRemover;
  HabilitarBotaoReabrir;
  HabilitarBotaoEfetivar;
  HabilitarCamposFechamento;
  HabilitarBotaoAdicionarProdutos;
  BloquearEdicaoDoTipoDeConferencia;
end;

procedure TMovInventario.HabilitarBotaoReabrir;
begin
  ActReabrir.Visible := cdsDataSTATUS.AsString =
    TInventarioProxy.STATUS_CONFIRMADO;
end;

procedure TMovInventario.HabilitarBotaoAdicionarProdutos;
begin
  ActBuscarProdutos.Visible :=
    (cdsDataSTATUS.AsString = TInventarioProxy.STATUS_ABERTO);
end;

procedure TMovInventario.HabilitarBotaoEfetivar;
begin
  ActEfetivar.Visible :=
    (cdsDataSTATUS.AsString = TInventarioProxy.STATUS_ABERTO)
    and not(cdsInventarioProduto.IsEmpty);
end;

procedure TMovInventario.HabilitarBotaoRemover;
begin
  ActRemove.Visible :=
    cdsDataSTATUS.AsString.Equals(TInventarioProxy.STATUS_ABERTO);
end;

procedure TMovInventario.HabilitarCamposFechamento;
var inventarioFechado: Boolean;
begin
  inventarioFechado :=
    cdsDataSTATUS.AsString.Equals(TInventarioProxy.STATUS_CONFIRMADO);

  lbDtFechamento.Visible := inventarioFechado;
  EdtDtFechamento.Visible := inventarioFechado;
  lbContagemCorreta.Visible := inventarioFechado;
  EdtContagemCorreta.Visible := inventarioFechado;
end;

procedure TMovInventario.IncluirProdutosNaConferencia;
var
  IDs: string;
  fdmProdutosSelecionados: TfdMemTable;
begin
  inherited;
  IDs := TPesqProduto.BuscarProdutos;

  if IDs <> EmptyStr then
  try
    TFrmMessage_Process.SendMessage('Inserindo produtos');
    cdsInventarioProduto.DisableControls;
    fdmProdutosSelecionados := TFDMemTable.Create(nil);
    try
      TProduto.SetProdutosCompleto(fdmProdutosSelecionados, IDs);

      fdmProdutosSelecionados.First;
      while not fdmProdutosSelecionados.Eof do
      begin
        if cdsInventarioProduto.Locate('ID_PRODUTO',
          fdmProdutosSelecionados.FieldByName('ID_PRODUTO').AsInteger, []) then
        begin
          fdmProdutosSelecionados.Next;
          continue;
        end;

        AdicionarProduto(
          fdmProdutosSelecionados.FieldByName('ID_PRODUTO').AsInteger, false);

        fdmProdutosSelecionados.Next;
      end;
    finally
      fdmProdutosSelecionados.Free;
      cdsInventarioProduto.First;
    end;
  finally
    cdsInventarioProduto.EnableControls;
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TMovInventario.IncrementarConferencia;
var
  fieldContagem: TField;
begin
  case cbConferencia.ItemIndex of
    0: fieldContagem := cdsInventarioProdutoPRIMEIRA_CONTAGEM;
    1: fieldContagem := cdsInventarioProdutoSEGUNDA_CONTAGEM;
    2: fieldContagem := cdsInventarioProdutoTERCEIRA_CONTAGEM;
  end;

  cdsInventarioProduto.Alterar;
  fieldContagem.AsFloat := fieldContagem.AsFloat + 1;
  cdsInventarioProduto.Salvar;
end;

procedure TMovInventario.ReiniciarContagem(const AContagem: Integer);
var
  fieldContagem: TField;
begin
  case AContagem of
    1: fieldContagem := cdsInventarioProdutoPRIMEIRA_CONTAGEM;
    2: fieldContagem := cdsInventarioProdutoSEGUNDA_CONTAGEM;
    3: fieldContagem := cdsInventarioProdutoTERCEIRA_CONTAGEM;
  end;

  try
    cdsInventarioProduto.DisableControls;
    cdsInventarioProduto.GuardarBookMark;
    cdsInventarioProduto.First;
    while not cdsInventarioProduto.Eof do
    begin
      cdsInventarioProduto.Alterar;
      fieldContagem.AsFloat := 0;
      cdsInventarioProduto.Salvar;
      cdsInventarioProduto.Next;
    end;
  finally
    cdsInventarioProduto.PosicionarBookmark;
    cdsInventarioProduto.EnableControls;
  end;
end;

initialization
  RegisterClass(TMovInventario);

finalization
  UnRegisterClass(TMovInventario);

end.
