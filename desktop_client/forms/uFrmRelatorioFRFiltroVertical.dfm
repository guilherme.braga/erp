inherited FrmRelatorioFRFiltroVertical: TFrmRelatorioFRFiltroVertical
  Caption = 'FrmRelatorioFRFiltroVertical'
  ClientHeight = 441
  ClientWidth = 932
  ExplicitWidth = 948
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 932
    ExplicitWidth = 932
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarFiltro'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    ExplicitWidth = 932
    ExplicitHeight = 339
    Height = 339
    Width = 932
    object cxGridRelatorios: TcxGrid
      AlignWithMargins = True
      Left = 328
      Top = 5
      Width = 599
      Height = 329
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object cxGridRelatoriosView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.CustomizeDialog = False
        DataController.DataSource = dsConsultaRelatorioFR
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        DateTimeHandling.UseLongDateFormat = False
        OptionsBehavior.PostponedSynchronization = False
        OptionsBehavior.CopyCaptionsToClipboard = False
        OptionsBehavior.DragHighlighting = False
        OptionsBehavior.DragOpening = False
        OptionsBehavior.DragScrolling = False
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.ColumnHeaderHints = False
        OptionsBehavior.CopyPreviewToClipboard = False
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnHidingOnGrouping = False
        OptionsCustomize.ColumnMoving = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrDisabled
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.FocusRect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GridLineColor = clInfoBk
        OptionsView.GroupByBox = False
        object cxGridRelatoriosViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 61
        end
        object cxGridRelatoriosViewNOME: TcxGridDBColumn
          DataBinding.FieldName = 'NOME'
          Width = 275
        end
        object cxGridRelatoriosViewDESCRICAO: TcxGridDBColumn
          DataBinding.FieldName = 'DESCRICAO'
          Width = 325
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridRelatoriosView
      end
    end
    object pnFrameFiltroVerticalPadrao: TgbPanel
      Left = 2
      Top = 2
      Align = alLeft
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      Height = 335
      Width = 315
    end
    object spFiltroVertical: TcxSplitter
      Left = 317
      Top = 2
      Width = 8
      Height = 335
      HotZoneClassName = 'TcxMediaPlayer9Style'
      AllowHotZoneDrag = False
      MinSize = 0
      Control = pnFrameFiltroVerticalPadrao
    end
  end
  inherited ActionListMain: TActionList
    Left = 100
    Top = 155
    object ActImprimir: TAction
      Category = 'Action'
      Caption = 'Imprimir F9'
      Hint = 'Imprimir relat'#243'rio selecionado'
      ImageIndex = 27
      ShortCut = 120
      OnExecute = ActImprimirExecute
    end
    object ActVisualizar: TAction
      Category = 'Action'
      Caption = 'Visualizar F10'
      Hint = 'Visualizar relat'#243'rio associado'
      ImageIndex = 28
      ShortCut = 121
      OnExecute = ActVisualizarExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Filtros')
    Left = 72
    Top = 155
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 224
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      FloatClientWidth = 86
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbImprimir'
        end
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 136
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object BarFiltro: TdxBar [3]
      Caption = 'Filtro'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 187
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object lbImprimir: TdxBarLargeButton [9]
      Action = ActImprimir
      Category = 1
    end
    object lbVisualizar: TdxBarLargeButton [10]
      Action = ActVisualizar
      Category = 1
    end
    object filtroLabelDataMovimento: TdxBarStatic
      Caption = 'Per'#237'odo de Movimenta'#231#227'o'
      Category = 2
      Hint = 'Per'#237'odo de Movimenta'#231#227'o'
      Visible = ivAlways
    end
    object filtroDataInicio: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataTermino: TcxBarEditItem
      Caption = 'Fim'
      Category = 2
      Hint = 'Fim'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroGrupoCentroResultado: TcxBarEditItem
      Caption = 'Grupo de Centro de Resultado'
      Category = 2
      Hint = 'Grupo de Centro de Resultado'
      Visible = ivAlways
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
    end
    object filtroCentroResultado: TcxBarEditItem
      Caption = 'Centro de Resultado'
      Category = 2
      Hint = 'Centro de Resultado'
      Visible = ivAlways
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
    end
    object filtroContaAnalise: TcxBarEditItem
      Caption = 'Conta de An'#225'lise'
      Category = 2
      Hint = 'Conta de An'#225'lise'
      Visible = ivAlways
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
    end
    object filtroContaCorrente: TcxBarEditItem
      Caption = 'Conta Corrente'
      Category = 2
      Hint = 'Conta Corrente'
      Visible = ivAlways
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
    end
    object filtroConciliado: TcxBarEditItem
      Caption = 'Concilia'#231#227'o'
      Category = 2
      Hint = 'Concilia'#231#227'o'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        'Todos'
        'Conciliado'
        'N'#227'o Conciliado')
      Properties.PostPopupValueOnTab = True
    end
    object filtroTipoMovimento: TcxBarEditItem
      Caption = 'Tipo de Movimento'
      Category = 2
      Hint = 'Tipo de Movimento'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        'Todos'
        'Cr'#233'dito'
        'D'#233'bito')
      Properties.PostPopupValueOnTab = True
    end
    object filtroDescricaoCentroResultado: TdxBarStatic
      Caption = 'Descri'#231#227'o de Centro de Resultado'
      Category = 2
      Hint = 'Descri'#231#227'o de Centro de Resultado'
      Visible = ivAlways
    end
    object filtroDescricaoGrupoCentroResultado: TdxBarStatic
      Caption = 'Descri'#231#227'o Grupo de Centro de Resultado'
      Category = 2
      Hint = 'Descri'#231#227'o Grupo de Centro de Resultado'
      Visible = ivAlways
    end
    object filtroDescricaoContaAnalise: TdxBarStatic
      Caption = 'Descri'#231#227'o de Conta de An'#225'lise'
      Category = 2
      Hint = 'Descri'#231#227'o de Conta de An'#225'lise'
      Visible = ivAlways
    end
    object filtroDescricaoContaCorrente: TdxBarStatic
      Caption = 'Descri'#231#227'o de Conta Corrente'
      Category = 2
      Hint = 'Descri'#231#227'o de Conta Corrente'
      Visible = ivAlways
    end
  end
  object dsConsultaRelatorioFR: TDataSource
    DataSet = cdsConsultaRelatorioFR
    Left = 284
    Top = 152
  end
  object cdsConsultaRelatorioFR: TGBClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'NOME'
        ParamType = ptInput
        Value = #39'TESTE'#39
      end
      item
        DataType = ftString
        Name = 'ID_PESSOA_USUARIO'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspConsultaRelatorioFR'
    RemoteServer = DmConnection.dspRelatorioFR
    AfterScroll = cdsConsultaRelatorioFRAfterScroll
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 264
    Top = 160
    object cdsConsultaRelatorioFRID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsConsultaRelatorioFRDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsConsultaRelatorioFRNOME: TStringField
      DisplayLabel = 'Relat'#243'rio'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 80
    end
    object cdsConsultaRelatorioFRIMPRESSORA: TStringField
      FieldName = 'IMPRESSORA'
      Size = 255
    end
    object cdsConsultaRelatorioFRACAO: TStringField
      FieldName = 'ACAO'
      Size = 30
    end
    object cdsConsultaRelatorioFRNUMERO_COPIAS: TIntegerField
      FieldName = 'NUMERO_COPIAS'
    end
    object cdsConsultaRelatorioFRFILTROS_PERSONALIZADOS: TBlobField
      FieldName = 'FILTROS_PERSONALIZADOS'
    end
  end
  object FdmFiltros: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvEDelete, uvCountUpdatedRecords, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckUpdatable = False
    Left = 256
    Top = 232
  end
end
