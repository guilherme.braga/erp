inherited CadCep: TCadCep
  Caption = 'CEP'
  ClientWidth = 929
  ExplicitWidth = 945
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 929
    ExplicitWidth = 929
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 929
    Properties.ActivePage = cxtsData
    ExplicitWidth = 929
    ClientRectRight = 929
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 936
      ExplicitHeight = 271
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 929
        ExplicitWidth = 929
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 929
      inherited DesignPanel: TJvDesignPanel
        Width = 929
        ExplicitWidth = 929
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 929
          Width = 929
          inherited dxBevel1: TdxBevel
            Width = 925
            ExplicitWidth = 925
          end
          object Label1: TLabel
            Left = 8
            Top = 65
            Width = 28
            Height = 13
            Caption = 'Bairro'
          end
          object Label2: TLabel
            Left = 8
            Top = 90
            Width = 33
            Height = 13
            Caption = 'Cidade'
          end
          object Label3: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label4: TLabel
            Left = 8
            Top = 38
            Width = 19
            Height = 13
            Caption = 'CEP'
          end
          object Label5: TLabel
            Left = 143
            Top = 38
            Width = 19
            Height = 13
            Caption = 'Rua'
          end
          object eFkModelo: TgbDBButtonEditFK
            Left = 57
            Top = 59
            DataBinding.DataField = 'ID_BAIRRO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = clWhite
            TabOrder = 3
            gbTextEdit = descModelo
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_BAIRRO; JOIN_DESCRICAO_BAIRRO'
            gbTableName = 'BAIRRO'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 59
          end
          object descModelo: TgbDBTextEdit
            Left = 113
            Top = 59
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_BAIRRO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 807
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 113
            Top = 84
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_CIDADE_CIDADE'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 807
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 57
            Top = 84
            DataBinding.DataField = 'JOIN_CIDADE_CIDADE'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = clWhite
            TabOrder = 4
            gbTextEdit = gbDBTextEdit1
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_CIDADE;JOIN_DESCRICAO_CIDADE'
            gbTableName = 'CIDADE'
            gbCamposConsulta = 'ID;DESCRICAO'
            Width = 59
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 58
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edDescricao: TgbDBTextEdit
            Left = 57
            Top = 34
            DataBinding.DataField = 'CEP'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 80
          end
          object gbDBTextEdit2: TgbDBTextEdit
            Left = 166
            Top = 34
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'RUA'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            Width = 754
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspCEP'
    RemoteServer = DmConnection.dspLocalidade
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataCEP: TStringField
      FieldName = 'CEP'
      Origin = 'CEP'
      Required = True
      Size = 15
    end
    object cdsDataID_CIDADE: TIntegerField
      DisplayLabel = 'C'#243'digo da Cidade'
      FieldName = 'ID_CIDADE'
      Origin = 'ID_CIDADE'
      Required = True
    end
    object cdsDataRUA: TStringField
      DisplayLabel = 'Rua'
      FieldName = 'RUA'
      Origin = 'RUA'
      Size = 255
    end
    object cdsDataID_BAIRRO: TIntegerField
      DisplayLabel = 'C'#243'digo do Bairro'
      FieldName = 'ID_BAIRRO'
      Origin = 'ID_BAIRRO'
      Required = True
    end
    object cdsDataJOIN_CIDADE_CIDADE: TStringField
      DisplayLabel = 'Cidade'
      FieldName = 'JOIN_CIDADE_CIDADE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_BAIRRO: TStringField
      DisplayLabel = 'Bairro'
      FieldName = 'JOIN_DESCRICAO_BAIRRO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
end
