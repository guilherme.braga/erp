inherited FrmLiberacaoBloqueioPersonalizado: TFrmLiberacaoBloqueioPersonalizado
  Caption = 'Libera'#231#227'o de Bloqueios'
  ClientWidth = 799
  ExplicitWidth = 805
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    Width = 799
    object cxGridPesquisaPadrao: TcxGrid
      Left = 2
      Top = 2
      Width = 795
      Height = 265
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      ExplicitLeft = 0
      ExplicitTop = -3
      ExplicitWidth = 1186
      ExplicitHeight = 300
      object viewBloqueiosPersonalizados: TcxGridDBBandedTableView
        OnDblClick = viewBloqueiosPersonalizadosDblClick
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        DataController.DataSource = dsBloqueiosPersonalizados
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Images = DmAcesso.cxImage16x16
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.ShowColumnFilterButtons = sfbAlways
        OptionsView.BandHeaders = False
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
          end>
        object viewBloqueiosPersonalizadosSTATUS: TcxGridDBBandedColumn
          DataBinding.FieldName = 'STATUS'
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringAddValueItems = False
          Options.FilteringFilteredItemsList = False
          Options.FilteringMRUItemsList = False
          Options.FilteringPopup = False
          Options.FilteringPopupMultiSelect = False
          Options.GroupFooters = False
          Options.Grouping = False
          Options.Moving = False
          Options.Sorting = False
          Width = 92
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object viewBloqueiosPersonalizadosDH_LIBERACAO: TcxGridDBBandedColumn
          Caption = 'Data/Hora da Libera'#231#227'o'
          DataBinding.FieldName = 'DH_LIBERACAO'
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringAddValueItems = False
          Options.FilteringFilteredItemsList = False
          Options.FilteringMRUItemsList = False
          Options.FilteringPopup = False
          Options.FilteringPopupMultiSelect = False
          Options.GroupFooters = False
          Options.Grouping = False
          Options.Moving = False
          Options.Sorting = False
          Width = 139
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object viewBloqueiosPersonalizadosJOIN_NOME_PESSOA_USUARIO: TcxGridDBBandedColumn
          Caption = 'Usu'#225'rio da Libera'#231#227'o'
          DataBinding.FieldName = 'JOIN_NOME_PESSOA_USUARIO'
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringAddValueItems = False
          Options.FilteringFilteredItemsList = False
          Options.FilteringMRUItemsList = False
          Options.FilteringPopup = False
          Options.FilteringPopupMultiSelect = False
          Options.GroupFooters = False
          Options.Grouping = False
          Options.Moving = False
          Options.Sorting = False
          Width = 161
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object viewBloqueiosPersonalizadosJOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JOIN_DESCRICAO_BLOQUEIO_PERSONALIZADO'
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringAddValueItems = False
          Options.FilteringFilteredItemsList = False
          Options.FilteringMRUItemsList = False
          Options.FilteringPopup = False
          Options.FilteringPopupMultiSelect = False
          Options.GroupFooters = False
          Options.Grouping = False
          Options.Moving = False
          Options.Sorting = False
          Width = 401
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
      end
      object cxGridPesquisaPadraoLevel1: TcxGridLevel
        GridView = viewBloqueiosPersonalizados
      end
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Width = 799
    inherited BtnConfirmar: TcxButton
      Left = 572
    end
    inherited BtnCancelar: TcxButton
      Left = 647
    end
    object btnSair: TcxButton
      Left = 722
      Top = 2
      Width = 75
      Height = 52
      Align = alRight
      Action = ActSair
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
      ExplicitLeft = 751
      ExplicitTop = 1
    end
  end
  inherited ActionList: TActionList
    inherited ActConfirmar: TAction
      Enabled = False
      Visible = False
    end
    inherited ActCancelar: TAction
      Enabled = False
      Visible = False
    end
    object ActSair: TAction
      Caption = 'Sair Esc'
      ImageIndex = 12
      ShortCut = 27
      OnExecute = ActSairExecute
    end
  end
  object dsBloqueiosPersonalizados: TDataSource
    Left = 320
    Top = 160
  end
end
