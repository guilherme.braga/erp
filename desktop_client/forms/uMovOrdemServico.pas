unit uMovOrdemServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxTextEdit, cxDBEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxSpinEdit, cxTimeEdit, cxButtonEdit,
  uGBDBButtonEditFK, uGBDBTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  uGBDBDateEdit, uGBPanel, cxMemo, uGBDBMemo, cxLabel, uFrameDetailPadrao,
  uFrameOrdemServicoProduto, DateUtils,
  uGBGroupBox, uGBDBValorComPercentual, cxCalc, uGBDBCalcEdit,
  uFrameDetailTransientePadrao, cxListBox, cxGridCardView, cxGridDBCardView,
  cxGridCustomLayoutView, uFrameOrdemServicoServico,
  uFrameOrdemServicoFechamentoCrediario, uUsuarioDesignControl, cxSplitter,
  cxRadioGroup, uGBDBRadioGroup, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, JvExControls, JvButton, JvTransparentButton, uFrameFiltroVerticalPadrao,
  cxCurrencyEdit, cxBarEditItem, dxBarExtItems;
type
  TMovOrdemServico = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsOrdemServicoChecklist: TGBClientDataSet;
    cdsOrdemServicoServico: TGBClientDataSet;
    cdsOrdemServicoChecklistID: TAutoIncField;
    cdsOrdemServicoChecklistBO_MARCADO: TStringField;
    cdsOrdemServicoChecklistID_CHECKLIST_ITEM: TIntegerField;
    cdsOrdemServicoChecklistID_ORDEM_SERVICO: TIntegerField;
    cdsOrdemServicoChecklistJOIN_DESCRICAO_CHECKLIST: TStringField;
    cdsOrdemServicoServicoID: TAutoIncField;
    cdsOrdemServicoServicoNR_ITEM: TIntegerField;
    cdsOrdemServicoServicoQUANTIDADE: TFMTBCDField;
    cdsOrdemServicoServicoVL_DESCONTO: TFMTBCDField;
    cdsOrdemServicoServicoPERC_DESCONTO: TFMTBCDField;
    cdsOrdemServicoServicoVL_ACRESCIMO: TFMTBCDField;
    cdsOrdemServicoServicoPERC_ACRESCIMO: TFMTBCDField;
    cdsOrdemServicoServicoVL_BRUTO: TFMTBCDField;
    cdsOrdemServicoServicoVL_LIQUIDO: TFMTBCDField;
    cdsOrdemServicoServicoVL_CUSTO: TFMTBCDField;
    cdsOrdemServicoServicoID_ORDEM_SERVICO: TIntegerField;
    cdsOrdemServicoServicoID_SERVICO: TIntegerField;
    cdsOrdemServicoServicoJOIN_DESCRICAO_SERVICO: TStringField;
    cdsOrdemServicoServicoJOIN_VALOR_VENDA_SERVICO: TFMTBCDField;
    cdsOrdemServicoServicoJOIN_DURACAO_SERVICO: TTimeField;
    cdsOrdemServicoProduto: TGBClientDataSet;
    cdsOrdemServicoProdutoID: TAutoIncField;
    cdsOrdemServicoProdutoNR_ITEM: TIntegerField;
    cdsOrdemServicoProdutoQUANTIDADE: TFMTBCDField;
    cdsOrdemServicoProdutoVL_DESCONTO: TFMTBCDField;
    cdsOrdemServicoProdutoPERC_DESCONTO: TFMTBCDField;
    cdsOrdemServicoProdutoVL_ACRESCIMO: TFMTBCDField;
    cdsOrdemServicoProdutoPERC_ACRESCIMO: TFMTBCDField;
    cdsOrdemServicoProdutoVL_BRUTO: TFMTBCDField;
    cdsOrdemServicoProdutoVL_LIQUIDO: TFMTBCDField;
    cdsOrdemServicoProdutoVL_CUSTO: TFMTBCDField;
    cdsOrdemServicoProdutoID_ORDEM_SERVICO: TIntegerField;
    cdsOrdemServicoProdutoID_PRODUTO: TIntegerField;
    cdsOrdemServicoProdutoJOIN_DESCRICAO_PRODUTO: TStringField;
    cdsOrdemServicoProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    cdsOrdemServicoProdutoJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    ActEstornar: TAction;
    ActEfetivar: TAction;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDH_FECHAMENTO: TDateTimeField;
    cdsDataDH_ENTRADA: TDateTimeField;
    cdsDataSTATUS: TStringField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataID_TABELA_PRECO: TIntegerField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataID_CHECKLIST: TIntegerField;
    cdsDataID_SITUACAO: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_PLANO_PAGAMENTO: TIntegerField;
    cdsDataID_OPERACAO: TIntegerField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDataPROBLEMA_RELATADO: TBlobField;
    cdsDataPROBLEMA_ENCONTRADO: TBlobField;
    cdsDataSOLUCAO_TECNICA: TBlobField;
    cdsDataJOIN_USUARIO: TStringField;
    cdsDataJOIN_FANTASIA_FILIAL: TStringField;
    cdsDataJOIN_ORIGEM_CHAVE_PROCESSO: TStringField;
    cdsDataJOIN_DESCRICAO_SITUACAO: TStringField;
    cdsDataJOIN_DESCRICAO_CHECKLIST: TStringField;
    cdsDataJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    cdsDataJOIN_DESCRICAO_OPERACAO: TStringField;
    cdsDatafdqOrdemServicoProduto: TDataSetField;
    cdsDatafdqOrdemServicoChecklist: TDataSetField;
    cdsDatafdqOrdemServicoServico: TDataSetField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cxPageControlDetails: TcxPageControl;
    tsDadosGerais: TcxTabSheet;
    PnDados: TgbPanel;
    Label9: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit3: TgbDBTextEdit;
    gbDBDateEdit3: TgbDBDateEdit;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    tsFechamento: TcxTabSheet;
    PnTotalizadores: TgbPanel;
    Label1: TLabel;
    Label13: TLabel;
    gbDBDateEdit1: TgbDBDateEdit;
    gbDBTextEdit8: TgbDBTextEdit;
    Label31: TLabel;
    gbDBDateEdit5: TgbDBDateEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK6: TgbDBButtonEditFK;
    Label2: TLabel;
    Label3: TLabel;
    descProcesso: TgbDBTextEdit;
    gbDBTextEdit4: TgbDBTextEdit;
    cdsDataDH_PREVISAO_TERMINO: TDateTimeField;
    cdsDataID_EQUIPAMENTO: TIntegerField;
    cdsDataID_USUARIO_RECEPCAO: TIntegerField;
    cdsDataJOIN_DESCRICAO_EQUIPAMENTO: TStringField;
    Label32: TLabel;
    gbDBDateEdit2: TgbDBDateEdit;
    cdsDataVL_TOTAL_BRUTO_PRODUTO: TFMTBCDField;
    cdsDataVL_TOTAL_DESCONTO_PRODUTO: TFMTBCDField;
    cdsDataVL_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField;
    cdsDataVL_TOTAL_LIQUIDO_PRODUTO: TFMTBCDField;
    cdsDataVL_TOTAL_BRUTO_SERVICO: TFMTBCDField;
    cdsDataVL_TOTAL_DESCONTO_SERVICO: TFMTBCDField;
    cdsDataVL_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField;
    cdsDataVL_TOTAL_LIQUIDO_SERVICO: TFMTBCDField;
    cdsDataVL_TOTAL_BRUTO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataVL_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataVL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataVL_TOTAL_LIQUIDO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataVL_TOTAL_DESCONTO: TFMTBCDField;
    cdsDataVL_TOTAL_ACRESCIMO: TFMTBCDField;
    cdsDataVL_TOTAL_LIQUIDO: TFMTBCDField;
    cdsDataVL_TOTAL_BRUTO: TFMTBCDField;
    gbPanel3: TgbPanel;
    pnTotalProduto: TgbPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label27: TLabel;
    gbDBTextEdit11: TgbDBTextEdit;
    gbDBTextEdit12: TgbDBTextEdit;
    gbDBTextEdit13: TgbDBTextEdit;
    gbDBTextEdit14: TgbDBTextEdit;
    gbDBTextEdit15: TgbDBTextEdit;
    gbDBTextEdit22: TgbDBTextEdit;
    gbPanel1: TgbPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    gbDBTextEdit16: TgbDBTextEdit;
    gbDBTextEdit17: TgbDBTextEdit;
    gbDBTextEdit18: TgbDBTextEdit;
    gbDBTextEdit19: TgbDBTextEdit;
    gbDBTextEdit20: TgbDBTextEdit;
    gbDBTextEdit21: TgbDBTextEdit;
    gbPanel2: TgbPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    gbDBTextEdit23: TgbDBTextEdit;
    gbDBTextEdit24: TgbDBTextEdit;
    gbDBTextEdit27: TgbDBTextEdit;
    gbDBTextEdit28: TgbDBTextEdit;
    gbDBTextEdit29: TgbDBTextEdit;
    gbDBTextEdit30: TgbDBTextEdit;
    gbPanel4: TgbPanel;
    Label37: TLabel;
    gbDBTextEdit31: TgbDBTextEdit;
    Label38: TLabel;
    Label39: TLabel;
    gbDBTextEdit32: TgbDBTextEdit;
    gbDBTextEdit33: TgbDBTextEdit;
    Label40: TLabel;
    Label41: TLabel;
    gbDBTextEdit34: TgbDBTextEdit;
    gbDBTextEdit35: TgbDBTextEdit;
    Label42: TLabel;
    gbDBTextEdit36: TgbDBTextEdit;
    pnRelatos: TgbPanel;
    tsProduto: TcxTabSheet;
    pnSolucaoTecnica: TgbPanel;
    gbDBMemo1: TgbDBMemo;
    cxLabel1: TcxLabel;
    pnProblemaEncontrado: TgbPanel;
    gbDBMemo2: TgbDBMemo;
    cxLabel2: TcxLabel;
    PnProblemaRelatado: TgbPanel;
    gbDBMemo3: TgbDBMemo;
    cxLabel3: TcxLabel;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    tsServico: TcxTabSheet;
    tsServicoTerceiro: TcxTabSheet;
    gbPanel7: TgbPanel;
    Label5: TLabel;
    gbDBButtonEditFK8: TgbDBButtonEditFK;
    gbDBTextEdit25: TgbDBTextEdit;
    cdsDataPERC_TOTAL_DESCONTO_PRODUTO: TFMTBCDField;
    cdsDataPERC_TOTAL_ACRESCIMO_PRODUTO: TFMTBCDField;
    cdsDataPERC_TOTAL_DESCONTO_SERVICO: TFMTBCDField;
    cdsDataPERC_TOTAL_ACRESCIMO_SERVICO: TFMTBCDField;
    cdsDataPERC_TOTAL_DESCONTO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataPERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO: TFMTBCDField;
    cdsDataPERC_TOTAL_DESCONTO: TFMTBCDField;
    cdsDataPERC_TOTAL_ACRESCIMO: TFMTBCDField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsOrdemServicoParcela: TGBClientDataSet;
    cdsDatafdqOrdemServicoParcela: TDataSetField;
    cdsOrdemServicoParcelaID: TAutoIncField;
    cdsOrdemServicoParcelaDOCUMENTO: TStringField;
    cdsOrdemServicoParcelaVL_TITULO: TFMTBCDField;
    cdsOrdemServicoParcelaQT_PARCELA: TIntegerField;
    cdsOrdemServicoParcelaNR_PARCELA: TIntegerField;
    cdsOrdemServicoParcelaDT_VENCIMENTO: TDateField;
    cdsOrdemServicoParcelaID_ORDEM_SERVICO: TIntegerField;
    cdsOrdemServicoParcelaIC_DIAS: TIntegerField;
    cdsOrdemServicoServicoVL_UNITARIO: TFMTBCDField;
    cdsOrdemServicoProdutoVL_UNITARIO: TFMTBCDField;
    cdsDataJOIN_SERIE_EQUIPAMENTO: TStringField;
    cdsDataJOIN_IMEI_EQUIPAMENTO: TStringField;
    dsChecklist: TDataSource;
    cdsOrdemServicoChecklistJOIN_NR_ITEM_CHECKLIST: TIntegerField;
    PnCheckList: TgbGroupBox;
    gridChecklist: TcxGrid;
    viewChecklist: TcxGridDBBandedTableView;
    viewChecklistID: TcxGridDBBandedColumn;
    viewChecklistBO_MARCADO: TcxGridDBBandedColumn;
    viewChecklistID_CHECKLIST_ITEM: TcxGridDBBandedColumn;
    viewChecklistID_ORDEM_SERVICO: TcxGridDBBandedColumn;
    viewChecklistJOIN_DESCRICAO_CHECKLIST: TcxGridDBBandedColumn;
    viewChecklistJOIN_NR_ITEM_CHECKLIST: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    gbPanel5: TgbPanel;
    gbDBButtonEditFK10: TgbDBButtonEditFK;
    gbDBTextEdit37: TgbDBTextEdit;
    cdsOrdemServicoParcelaOBSERVACAO: TStringField;
    cdsOrdemServicoParcelaCHEQUE_SACADO: TStringField;
    cdsOrdemServicoParcelaCHEQUE_DOC_FEDERAL: TStringField;
    cdsOrdemServicoParcelaCHEQUE_BANCO: TStringField;
    cdsOrdemServicoParcelaCHEQUE_DT_EMISSAO: TDateField;
    cdsOrdemServicoParcelaCHEQUE_AGENCIA: TStringField;
    cdsOrdemServicoParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    cdsOrdemServicoParcelaCHEQUE_NUMERO: TIntegerField;
    cdsOrdemServicoParcelaID_OPERADORA_CARTAO: TIntegerField;
    PnFrameProduto: TgbPanel;
    PnFrameServico: TgbPanel;
    PnFrameFechamento: TgbPanel;
    PnFrameServicoTerceiro: TgbPanel;
    cdsOrdemServicoParcelaID_FORMA_PAGAMENTO: TIntegerField;
    cdsOrdemServicoParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO: TStringField;
    cdsOrdemServicoParcelaJOIN_DESCRICAO_OPERADORA_CARTAO: TStringField;
    cdsOrdemServicoServicoBO_SERVICO_TERCEIRO: TStringField;
    cdsDataCC_SALDO_PESSOA_CREDITO: TFloatField;
    cdsDataCC_VL_DIFERENCA: TFloatField;
    cdsDataCC_VL_TROCO: TFloatField;
    cdsDataVL_PAGAMENTO: TFMTBCDField;
    cdsDataVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField;
    cdsDataJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField;
    cdsDataKM: TIntegerField;
    cdsDataTANQUE: TStringField;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    cxLabel4: TcxLabel;
    LBEfetivar: TdxBarLargeButton;
    LBEstornar: TdxBarLargeButton;
    PnInformacoesVeiculo: TgbPanel;
    Label15: TLabel;
    Label30: TLabel;
    Label46: TLabel;
    Label50: TLabel;
    descModelo: TgbDBTextEdit;
    descMarca: TgbDBTextEdit;
    EdtPlaca: TgbDBButtonEditFK;
    Label43: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBTextEdit9: TgbDBTextEdit;
    gbDBTextEdit10: TgbDBTextEdit;
    gbDBCalcEdit1: TgbDBCalcEdit;
    gbDBRadioGroup1: TgbDBRadioGroup;
    Label49: TLabel;
    cdsDataID_VEICULO: TIntegerField;
    cdsDataJOIN_PLACA_VEICULO: TStringField;
    cdsDataJOIN_ANO_VEICULO: TIntegerField;
    cdsDataJOIN_COMBUSTIVEL_VEICULO: TStringField;
    cdsDataJOIN_COR_VEICULO: TStringField;
    cdsDataJOIN_MODELO_VEICULO: TStringField;
    cdsDataJOIN_MARCA_VEICULO: TStringField;
    tsObservacao: TcxTabSheet;
    gbDBMemo4: TgbDBMemo;
    cdsOrdemServicoParcelaID_CARTEIRA: TIntegerField;
    cdsOrdemServicoParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    cdsDataEQUIPAMENTO: TStringField;
    cdsDataEQUIPAMENTO_SERIE: TStringField;
    cdsDataEQUIPAMENTO_IMEI: TStringField;
    cdsOrdemServicoParcelaCHEQUE_DT_VENCIMENTO: TDateField;
    lbInformacoesCliente: TLabel;
    dxBevel2: TdxBevel;
    dxBevel3: TdxBevel;
    Label51: TLabel;
    pnInformacoesCliente: TgbPanel;
    Label52: TLabel;
    dxBevel4: TdxBevel;
    pnInformacoesEquipamento: TgbPanel;
    dxBevel5: TdxBevel;
    Label53: TLabel;
    Label10: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit5: TgbDBTextEdit;
    Label7: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdtEquipamento: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    gbDBTextEdit26: TgbDBTextEdit;
    barFiltroRapido: TdxBar;
    bsLabelStatus: TdxBarStatic;
    lbVazio: TdxBarStatic;
    filtroDataCadastroInicial: TcxBarEditItem;
    filtroDataCadastroFIm: TcxBarEditItem;
    filtroDataDocumentoInicio: TcxBarEditItem;
    filtroDataDocumentoFim: TcxBarEditItem;
    filtroSituacao: TdxBarCombo;
    filtroLabelDataCadastro: TdxBarStatic;
    filtroLabelDataDocumento: TdxBarStatic;
    filtroCodigoOrdemServico: TcxBarEditItem;
    filtroPessoa: TcxBarEditItem;
    filtroNomePessoa: TdxBarStatic;
    procedure cdsOrdemServicoProdutoAfterDelete(DataSet: TDataSet);
    procedure cdsOrdemServicoServicoAfterDelete(DataSet: TDataSet);
    procedure cdsOrdemServicoServicoAfterPost(DataSet: TDataSet);
    procedure cdsOrdemServicoProdutoAfterPost(DataSet: TDataSet);
    procedure AtualizarTotalDoProduto(Sender: TField);
    procedure AtualizarTotalDoServico(Sender: TField);
    procedure ActEstornarExecute(Sender: TObject);
    procedure ActEfetivarExecute(Sender: TObject);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure cdsOrdemServicoParcelaDT_VENCIMENTOChange(Sender: TField);
    procedure cdsOrdemServicoParcelaIC_DIASChange(Sender: TField);
    procedure ModificarParcelaAposParcelamento(Sender: TField);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure EdtContaAnaliseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure GerarParcela(Sender: TField);
    procedure cdsDataID_CHECKLISTChange(Sender: TField);
    procedure IncrementarNrItem(Dataset: TDataset);
    procedure cdsOrdemServicoProdutoNewRecord(DataSet: TDataSet);
    procedure cdsOrdemServicoServicoNewRecord(DataSet: TDataSet);
    procedure EdtPlacagbDepoisDeConsultar(Sender: TObject);
    procedure cxPageControlDetailsPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
    procedure cdsDataVL_TOTAL_LIQUIDOChange(Sender: TField);
    procedure cdsOrdemServicoParcelaAfterPost(DataSet: TDataSet);
    procedure cdsDataVL_PAGAMENTOChange(Sender: TField);
    procedure dsDataDataChange(Sender: TObject; Field: TField);
    procedure cdsOrdemServicoParcelaBeforePost(DataSet: TDataSet);
    procedure EdtEquipamentogbDepoisDeConsultar(Sender: TObject);
    procedure filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure filtroPessoaPropertiesEditValueChanged(Sender: TObject);
    procedure filtroCodigoOrdemServicoExit(Sender: TObject);
  private
    FFrameProduto: TFrameOrdemServicoProduto;
    FFrameServico: TFrameOrdemServicoServico;
    FFrameServicoTerceiro: TFrameOrdemServicoServico;
    FFrameFechamentoCrediario: TFrameOrdemServicoFechamentoCrediario;
    FPodeExecutarGeracaoParcela: Boolean;
    FCalculandoPeriodoParcelamento: Boolean;
    FConstruindoVisaoChecklist: Boolean;

    {Parametros}
    FParametroFormaPagamentoAVista: TParametroFormulario;
    FParametroPlanoPagamentoAVista: TParametroFormulario;
    FParametroFormaPagamentoCrediario: TParametroFormulario;
    FParametroContaCorrente: TParametroFormulario;
    FParametroCentroResultadoAVista: TParametroFormulario;
    FParametroCentroResultadoAPrazo: TParametroFormulario;
    FParametroContaAnaliseAVista: TParametroFormulario;
    FParametroContaAnaliseAPrazo: TParametroFormulario;
    FParametroExibeEmissaoDocumentoAoEfetivar: TParametroFormulario;
    FParametroExibirDadosVeiculo: TParametroFormulario;
    FParametroExibirDadosEquipamento: TParametroFormulario;
    FParametroPermiteRealizarOrdemServicoParaConsumidorFinal: TParametroFormulario;
    FParametroCarteira: TParametroFormulario;
    FParametroCheckList: TParametroFormulario;
    FParametroAplicarTributosNosProdutos: TParametroFormulario;
    FParametroFiltroDataCadastroInicio: TParametroFormulario;
    FParametroFiltroDataCadastroFim: TParametroFormulario;
    FParametroFiltroDataEfetivacaoInicio: TParametroFormulario;
    FParametroFiltroDataEfetivacaoFim: TParametroFormulario;

    const FILTRO_TODOS: String = 'TODOS';

    procedure HabilitarBotaoCancelar;
    procedure HabilitarBotaoEfetivar;
    procedure AtualizarValorOS;
    procedure AtualizarValorProduto;
    procedure AtualizarValorServico;
    procedure PesquisarContaAnalise;
    procedure MostrarAbaDeDados;
    procedure CalcularTamanhoDosCamposDeRelatos;

    procedure ValidarCentroResultado;
    procedure ValidarContaAnalise;
    procedure ValidarPlanoPagamento;
    procedure ValidarSePodeRealizarOrdemServicoParaConsumidorFinal;
    procedure ZerarTotalizadoresOS;
    procedure PreencherPlanoContaAVista;
    procedure EmitirDocumento;
    procedure PreencherCheckListParametrizadoNaOrdemDeServico;
    procedure SugerirDataVencimentoCheque;
    procedure ValidarInformacoesCartao;
    function ParcelaNaFormaPagamentoCartao: Boolean;

    procedure ExibirDadosVeiculo;
    procedure ExibirDadosEquipamento;
  public
    procedure GerarParcelamento;
  protected
    procedure GerenciarControles; override;
    procedure AntesDeRemover; override;
    procedure AntesDeConfirmar; override;
    procedure AoCriarFormulario; override;
    procedure DepoisDeAlterar;override;
    procedure DepoisDeInserir;override;
    procedure BloquearEdicao; override;
    procedure InstanciarFrames; override;
    procedure AoImprimir; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure MostrarInformacoesImplantacao; override;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;
    procedure PrepararFiltros; override;
    procedure SetWhereBundle;override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uDatasetUtils, uFrmMessage, uFrmMessage_Process,
  uOrdemServico, uPesqContaAnalise, uMathUtils, uOrdemServicoProxy,
  uChaveProcesso, uChaveProcessoProxy, uFilial, uPlanoPagamento, uSistema,
  uChecklist, uDevExpressUtils, uTabelaPrecoProxy, uProdutoProxy,
  uFiltroFormulario, uConstParametroFormulario, uPlanoConta, uVeiculo,
  uVeiculoProxy, uTControl_Function, uFrmHelpImplantacaoOrdemServico,
  uFrmEmissaoDocumentoFechamento, uMovOrdemServicoEmitirDocumento, uTPessoa, uTipoQuitacaoProxy,
  uFormaPagamento, uEquipamentoProxy, uEquipamento, uFrmConsultaPadrao;

procedure TMovOrdemServico.cdsDataID_CHECKLISTChange(Sender: TField);
begin
  inherited;
  if FConstruindoVisaoChecklist then
    Exit;

  try
    FConstruindoVisaoChecklist := true;

    cdsOrdemServicoChecklist.DisableControls;

    while not cdsOrdemServicoChecklist.IsEmpty do
    begin
      cdsOrdemServicoChecklist.Delete;
    end;

    TChecklist.ConstruirVisao(cdsOrdemServicoChecklist,
      cdsDataID_CHECKLIST.AsInteger);
  finally
    FConstruindoVisaoChecklist := false;
    cdsOrdemServicoChecklist.EnableControls;
  end;
end;

procedure TMovOrdemServico.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataDH_ENTRADA.AsDateTime := Now;
  cdsDataDH_PREVISAO_TERMINO.AsDateTime := Now;
  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(
    TChaveProcessoProxy.ORIGEM_ORDEM_SERVICO, cdsDataID.AsInteger);
  cdsDataJOIN_ORIGEM_CHAVE_PROCESSO.AsString := TChaveProcessoProxy.ORIGEM_ORDEM_SERVICO;
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.filial.Fid;
  cdsDataID_PESSOA_USUARIO.AsInteger := TSistema.Sistema.usuario.id;
  cdsDataID_USUARIO_RECEPCAO.AsInteger := TSistema.Sistema.usuario.id;
  cdsDataJOIN_USUARIO.AsString := TSistema.Sistema.usuario.usuario;
  cdsDataSTATUS.AsString := TOrdemServicoProxy.STATUS_ABERTO;

  PreencherPlanoContaAVista;

  //Plano de Pagamento
  cdsDataID_PLANO_PAGAMENTO.AsInteger :=
    StrtoIntDef(FParametroPlanoPagamentoAVista.valor,0);
  cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO.AsString :=
    TPlanoPagamento.GetDescricaoPlanoPagamento(cdsDataID_PLANO_PAGAMENTO.AsInteger);

  ZerarTotalizadoresOS;
end;

procedure TMovOrdemServico.cdsDataVL_PAGAMENTOChange(Sender: TField);
begin
  inherited;
  HabilitarBotaoEfetivar;
end;

procedure TMovOrdemServico.cdsDataVL_TOTAL_LIQUIDOChange(Sender: TField);
begin
  inherited;
  FFrameFechamentoCrediario.ProcessarCalculoTrocoDiferenca;
end;

procedure TMovOrdemServico.PreencherPlanoContaAVista;
begin
  //Centro de Resultado
  cdsDataID_CENTRO_RESULTADO.AsInteger := StrtoIntDef(FParametroCentroResultadoAVista.valor,0);
  cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO.AsString := TPlanoConta.GetDescricaoCentroResultado(cdsDataID_CENTRO_RESULTADO.AsInteger);

  //Dados Conta de Analise
  cdsDataID_CONTA_ANALISE.AsInteger := StrtoIntDef(FParametroContaAnaliseAVista.valor,0);
  TPesqContaAnalise.ExecutarConsultaOculta(cdsDataID_CENTRO_RESULTADO.AsInteger,
  cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
end;

procedure TMovOrdemServico.PrepararFiltros;
begin
  inherited;
  filtroCodigoOrdemServico.EditValue := null;
  filtroPessoa.EditValue := null;
  filtroNomePessoa.Caption := '';

  filtroDataCadastroInicial.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataCadastroInicio.AsString);

  filtroDataCadastroFIm.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataCadastroFim.AsString);

  filtroDataDocumentoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataEfetivacaoInicio.AsString);

  filtroDataDocumentoFIm.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataEfetivacaoFim.AsString);

  filtroSituacao.Text := FILTRO_TODOS;
end;

procedure TMovOrdemServico.SetWhereBundle;
begin
  inherited;
  //Pessoa
  if not VartoStr(filtroPessoa.EditValue).IsEmpty then
    WhereSearch.AddIgual(TOrdemServicoProxy.FIELD_ID_PESSOA, StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0));

  //Numero do Documento
  if not VartoStr(filtroCodigoOrdemServico.EditValue).IsEmpty then
    WhereSearch.AddIgual(TOrdemServicoProxy.FIELD_ID, StrtoIntDef(VartoStr(filtroCodigoOrdemServico.EditValue), 0));

  //Data de Emissao
  if not(VartoStr(filtroDataCadastroInicial.EditValue).IsEmpty) and
     not(VartoStr(filtroDataCadastroFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataCadastroInicial.EditValue) <= VarToDateTime(filtroDataCadastroFIm.EditValue)) then
    WhereSearch.AddEntre(TOrdemServicoProxy.FIELD_DATA_CADASTRO, StrtoDate(filtroDataCadastroInicial.EditValue),
      StrtoDate(filtroDataCadastroFIm.EditValue));

  //Data de Fechamento
  if not(VartoStr(filtroDataDocumentoInicio.EditValue).IsEmpty) and
     not(VartoStr(filtroDataDocumentoFIm.EditValue).IsEmpty) and
     (VarToDateTime(filtroDataDocumentoInicio.EditValue) <= VarToDateTime(filtroDataDocumentoFIm.EditValue)) then
   WhereSearch.AddEntre(TOrdemServicoProxy.FIELD_DATA_FECHAMENTO, StrtoDate(filtroDataDocumentoInicio.EditValue),
     StrtoDate(filtroDataDocumentoFIm.EditValue));

  //Status
  if not VartoStr(filtroSituacao.Text).Equals(FILTRO_TODOS) then
    WhereSearch.AddIgual(TOrdemServicoProxy.FIELD_STATUS,VartoStr(filtroSituacao.Text));
end;

procedure TMovOrdemServico.SugerirDataVencimentoCheque;
begin
  if not cdsOrdemServicoParcela.EstadoDeAlteracao then
  begin
    Exit;
  end;

  if cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_TERCEIRO) or
    cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CHEQUE_PROPRIO) then
  begin
    cdsOrdemServicoParcelaCHEQUE_DT_VENCIMENTO.AsDateTime := cdsOrdemServicoParcelaDT_VENCIMENTO.AsDateTime;
  end
  else
  begin
    cdsOrdemServicoParcelaCHEQUE_DT_VENCIMENTO.Clear;
  end;
end;

procedure TMovOrdemServico.cdsOrdemServicoParcelaAfterPost(DataSet: TDataSet);
begin
  inherited;
  FFrameFechamentoCrediario.ReplicarDadosEntreAsParcelas;
end;

procedure TMovOrdemServico.cdsOrdemServicoParcelaBeforePost(DataSet: TDataSet);
begin
  inherited;
  //ValidarInformacoesCartao; Refatorar pois esta validando logo ap�s a consulta
end;

procedure TMovOrdemServico.cdsOrdemServicoParcelaDT_VENCIMENTOChange(
  Sender: TField);
begin
  inherited;
  SugerirDataVencimentoCheque;

  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsOrdemServicoParcelaIC_DIAS.AsInteger :=
      DaysBetween(Date, cdsOrdemServicoParcelaDT_VENCIMENTO.AsDateTime);

    TParcelamentoUtils.ReplicarDiaDeVencimento(cdsOrdemServicoParcelaDT_VENCIMENTO);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovOrdemServico.cdsOrdemServicoParcelaIC_DIASChange(Sender: TField);
begin
  inherited;
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsOrdemServicoParcelaDT_VENCIMENTO.AsDateTime := Date +
      cdsOrdemServicoParcelaIC_DIAS.AsInteger;

    TParcelamentoUtils.ReplicarDiaDeVencimento(cdsOrdemServicoParcelaDT_VENCIMENTO);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovOrdemServico.cdsOrdemServicoProdutoAfterDelete(DataSet: TDataSet);
begin
  inherited;
  AtualizarValorOS;
end;

procedure TMovOrdemServico.cdsOrdemServicoProdutoAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarValorOS;
end;

procedure TMovOrdemServico.cdsOrdemServicoProdutoNewRecord(DataSet: TDataSet);
begin
  inherited;
  IncrementarNrItem(DataSet);
end;

procedure TMovOrdemServico.cdsOrdemServicoServicoAfterDelete(DataSet: TDataSet);
begin
  inherited;
  AtualizarValorOS;
end;

procedure TMovOrdemServico.cdsOrdemServicoServicoAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarValorOS;
end;

procedure TMovOrdemServico.cdsOrdemServicoServicoNewRecord(DataSet: TDataSet);
begin
  inherited;
  IncrementarNrItem(DataSet);
end;

procedure TMovOrdemServico.AtualizarTotalDoProduto(Sender: TField);
begin
  AtualizarValorProduto;
end;

procedure TMovOrdemServico.AtualizarTotalDoServico(Sender: TField);
begin
  AtualizarValorServico;
end;

procedure TMovOrdemServico.CalcularTamanhoDosCamposDeRelatos;
var
  tamanhoDoPainel: Integer;
  quantidadePaineisVisiveis: Integer;
begin
  quantidadePaineisVisiveis := 0;

  if PnCheckList.Visible then
    Inc(quantidadePaineisVisiveis);

  if PnProblemaRelatado.Visible then
    Inc(quantidadePaineisVisiveis);

  if PnProblemaEncontrado.Visible then
    Inc(quantidadePaineisVisiveis);

  if pnSolucaoTecnica.Visible then
    Inc(quantidadePaineisVisiveis);

  if quantidadePaineisVisiveis <= 0 then
    quantidadePaineisVisiveis := 1;

  tamanhoDoPainel := pnRelatos.Width div quantidadePaineisVisiveis;

  PnProblemaRelatado.Width := tamanhoDoPainel;
  PnProblemaEncontrado.Width := tamanhoDoPainel;
  pnSolucaoTecnica.Width := tamanhoDoPainel;
  PnCheckList.Width := tamanhoDoPainel;
end;

procedure TMovOrdemServico.AtualizarValorProduto;
begin
  cdsOrdemServicoProdutoVL_BRUTO.AsFloat :=
    cdsOrdemServicoProdutoQUANTIDADE.AsFloat *
    cdsOrdemServicoProdutoVL_UNITARIO.AsFloat;

  cdsOrdemServicoProdutoVL_LIQUIDO.AsFloat :=
    (cdsOrdemServicoProdutoQUANTIDADE.AsFloat *
    (cdsOrdemServicoProdutoVL_UNITARIO.AsFloat -
     cdsOrdemServicoProdutoVL_DESCONTO.AsFloat +
     cdsOrdemServicoProdutoVL_ACRESCIMO.AsFloat));
end;

procedure TMovOrdemServico.AtualizarValorServico;
begin
  cdsOrdemServicoServicoVL_BRUTO.AsFloat :=
    cdsOrdemServicoServicoQUANTIDADE.AsFloat *
    cdsOrdemServicoServicoVL_UNITARIO.AsFloat;

  cdsOrdemServicoServicoVL_LIQUIDO.AsFloat :=
    (cdsOrdemServicoServicoQUANTIDADE.AsFloat *
    (cdsOrdemServicoServicoVL_UNITARIO.AsFloat -
     cdsOrdemServicoServicoVL_DESCONTO.AsFloat +
     cdsOrdemServicoServicoVL_ACRESCIMO.AsFloat));
end;

procedure TMovOrdemServico.BloquearEdicao;
begin
  inherited;
  Self.BloquearEdicaoDados :=
    not cdsDataSTATUS.AsString.Equals(TOrdemServicoProxy.STATUS_ABERTO);
end;

procedure TMovOrdemServico.AtualizarValorOS;
begin
  TOrdemServico.CalcularValorTotalProdutos(cdsData, cdsOrdemServicoProduto);
  TOrdemServico.CalcularValorTotalServicos(cdsData, cdsOrdemServicoServico);
  TOrdemServico.CalcularValorTotalOS(cdsData);
end;

procedure TMovOrdemServico.HabilitarBotaoCancelar;
begin
  ActEstornar.Visible := cdsDataSTATUS.AsString =
    TOrdemServicoProxy.STATUS_FECHADO;
end;

procedure TMovOrdemServico.ActEstornarExecute(Sender: TObject);
const MSG_NAO_PODE_ESTORNAR: String =
  'N�o � poss�vel estornar o documento pois existe quita��o realizada.';
var idChaveProcesso: Integer;
    tipoDocumento: String;
begin
  inherited;
  if TFrmMessage.Question('Deseja estornar a ordem de servi�o?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    ActConfirm.Execute;

    if TOrdemServico.PodeEstornar(cdsDataID_CHAVE_PROCESSO.AsInteger) then
    begin
      TFrmMessage_Process.SendMessage('Estornando a ordem de servi�o');
      try
        TOrdemServico.Cancelar(idChaveProcesso);
        AtualizarRegistro;
      finally
        TFrmMessage_Process.CloseMessage;
      end;
    end
    else
      TFrmMessage.Information(MSG_NAO_PODE_ESTORNAR);
  end;
end;

procedure TMovOrdemServico.ActEfetivarExecute(Sender: TObject);
var
  idChaveProcesso: Integer;
  ordemServicoProxy: TOrdemServicoProxy;
begin
  inherited;
  if TFrmMessage.Question('Deseja efetivar a ordem de servi�o?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    //Em caso de troco, deve gerar apenas o caixa referente ao valor da venda
    if cdsDataVL_PAGAMENTO.AsFloat > cdsDataVL_TOTAL_LIQUIDO.AsFloat then
      cdsDataVL_PAGAMENTO.AsFloat := cdsDataVL_TOTAL_LIQUIDO.AsFloat;

    ActConfirm.Execute;

    TFrmMessage_Process.SendMessage('Efetivando a ordem de servi�o');
    try
      ordemServicoProxy := TOrdemServicoProxy.Create;
      try
        ordemServicoProxy.FIdContaCorrente := FParametroContaCorrente.AsInteger;
        ordemServicoProxy.FIdFormaPagamentoDinheiro := FParametroFormaPagamentoAVista.AsInteger;
        ordemServicoProxy.FIdCarteira := FParametroCarteira.AsInteger;

        TOrdemServico.Efetivar(idChaveProcesso, ordemServicoProxy);

        if FParametroExibeEmissaoDocumentoAoEfetivar.ValorSim then
        begin
          TFrmMessage_Process.CloseMessage;
          EmitirDocumento;
          TFrmMessage_Process.SendMessage('Efetivando a ordem de servi�o');
        end;

        AtualizarRegistro;
      finally
        FreeAndNil(ordemServicoProxy);
      end;
    finally
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovOrdemServico.EmitirDocumento;
var
  movOrdemServicoEmitirDocumento: TMovOrdemServicoEmitirDocumento;
  id: Integer;
begin
  if not cdsData.Active then
  begin
    id := fdmSearch.FieldByName('ID').AsInteger;
  end
  else
  begin
    if cdsDataID.AsInteger = 0 then
    begin
      id := TOrdemServico.GetId(cdsData.FieldByName('ID_CHAVE_PROCESSO').AsInteger);
    end
    else
    begin
      id := cdsDataID.AsInteger;
    end;
  end;

  TFrmEmissaoDocumentoFechamento.EmissaoDocumento(id);
end;

procedure TMovOrdemServico.ExibirDadosEquipamento;
begin
  pnInformacoesEquipamento.Visible := FParametroExibirDadosEquipamento.ValorSim;
end;

procedure TMovOrdemServico.ExibirDadosVeiculo;
begin
  pnInformacoesVeiculo.Visible := FParametroExibirDadosVeiculo.ValorSim;
end;

procedure TMovOrdemServico.filtroCodigoOrdemServicoExit(Sender: TObject);
begin
  inherited;
  //Deve existir devido ao bug que existe no onexit do filtro de pessoa.
  {
  //Corre��o de um bug que existe no filtro de pessoa, ap�s sair dele o bot�o pesquisar
  //n�o ganha foco ao passar com o mouse por cima dele, e nem recebe a��o de click qdo clicado.
  }
end;

procedure TMovOrdemServico.filtroPessoaPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := TFrmConsultaPadrao.ConsultarID('PESSOA');
  if idPessoa > 0 then
  begin
    filtroPessoa.EditValue := idPessoa;
    filtroPessoaPropertiesEditValueChanged(filtroPessoa);
  end;
end;

procedure TMovOrdemServico.filtroPessoaPropertiesEditValueChanged(Sender: TObject);
var
  idPessoa: Integer;
begin
  inherited;
  idPessoa := StrtoIntDef(VartoStr(filtroPessoa.EditValue), 0);
  if idPessoa > 0 then
  begin
    filtroNomePessoa.Caption := Copy(TPessoa.GetNomePessoa(idPessoa),1,40);
  end
  else
  begin
    filtroNomePessoa.Caption := '';
  end;

  //Corre��o de um bug que existe no filtro de pessoa, ap�s sair dele o bot�o pesquisar
  //n�o ganha foco ao passar com o mouse por cima dele, e nem recebe a��o de click qdo clicado.
  filtroCodigoOrdemServicoExit(Sender);

  abort;
end;

procedure TMovOrdemServico.AntesDeConfirmar;
begin
  inherited;
  {Campos n�o requireds no server por causa da tela}
  ValidarSePodeRealizarOrdemServicoParaConsumidorFinal;
  ValidarCentroResultado;
  ValidarContaAnalise;
  ValidarPlanoPagamento;
end;

procedure TMovOrdemServico.AntesDeRemover;
const MSG_NAO_PODE_EXCLUIR: String =
  'N�o � poss�vel excluir o documento pois existe quita��o realizada.';
begin
  inherited;
  if not TOrdemServico.PodeExcluir(cdsDataID_CHAVE_PROCESSO.AsInteger) then
  begin
    TFrmMessage.Information(MSG_NAO_PODE_EXCLUIR);
    abort;
  end;
end;

procedure TMovOrdemServico.AoCriarFormulario;
begin
  inherited;
  FPodeExecutarGeracaoParcela := true;
  MostrarAbaDeDados;

  EdtEquipamento.NaoLimparCampoAposConsultaInvalida;
  TcxGridUtils.PersonalizarColunaPeloTipoCampo(viewChecklist);

  ExibirDadosVeiculo;
  ExibirDadosEquipamento;
end;

procedure TMovOrdemServico.AoImprimir;
begin
  //inherited;
  EmitirDocumento;
end;

procedure TMovOrdemServico.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Parametro Forma de Pagamento a Vista
  FparametroFormaPagamentoAVista := TParametroFormulario.Create;
  FparametroFormaPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoAVista);

  //Parametro Plano de Pagamento a Vista
  FparametroPlanoPagamentoAVista := TParametroFormulario.Create;
  FparametroPlanoPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroPlanoPagamentoAVista);

  //Parametro Forma de Pagamento Crediario
  FparametroFormaPagamentoCrediario := TParametroFormulario.Create;
  FparametroFormaPagamentoCrediario.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_CREDIARIO;
  FparametroFormaPagamentoCrediario.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_CREDIARIO;
  FparametroFormaPagamentoCrediario.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoCrediario);

  //Parametro Conta Corrente
  FparametroContaCorrente := TParametroFormulario.Create;
  FparametroContaCorrente.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FparametroContaCorrente.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_CORRENTE;
  FparametroContaCorrente.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroContaCorrente);

  //Parametro Centro de Resultado A Vista
  FParametroCentroResultadoAVista := TParametroFormulario.Create;
  FParametroCentroResultadoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CENTRO_RESULTADO_AVISTA;
  FParametroCentroResultadoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroCentroResultadoAVista);

  //Parametro Centro de Resultado A Prazo
  FParametroCentroResultadoAPrazo := TParametroFormulario.Create;
  FParametroCentroResultadoAPrazo.parametro :=
    TConstParametroFormulario.PARAMETRO_CENTRO_RESULTADO_APRAZO;
  FParametroCentroResultadoAPrazo.descricao :=
    TConstParametroFormulario.DESCRICAO_CENTRO_RESULTADO_APRAZO;
  FParametroCentroResultadoAPrazo.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroCentroResultadoAPrazo);

  //Parametro Conta de Analise A Vista
  FParametroContaAnaliseAVista := TParametroFormulario.Create;
  FParametroContaAnaliseAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_ANALISE_AVISTA;
  FParametroContaAnaliseAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaAnaliseAVista);

  //Parametro Conta de Analise A Prazo
  FParametroContaAnaliseAPrazo := TParametroFormulario.Create;
  FParametroContaAnaliseAPrazo.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_ANALISE_APRAZO;
  FParametroContaAnaliseAPrazo.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_ANALISE_APRAZO;
  FParametroContaAnaliseAPrazo.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroContaAnaliseAPrazo);

  //Exibir tela de emiss�o de documento
  FParametroExibeEmissaoDocumentoAoEfetivar := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_EMISSAO_DOCUMENTO_AO_EFETIVAR,
    TConstParametroFormulario.DESCRICAO_EXIBIR_EMISSAO_DOCUMENTO_AO_EFETIVAR);
  AParametros.AdicionarParametro(FParametroExibeEmissaoDocumentoAoEfetivar);

  //Exibir dados do Ve�culo
  FParametroExibirDadosVeiculo := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_VEICULO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_VEICULO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirDadosVeiculo);

  //Exibir dados do Equipamento
  FParametroExibirDadosEquipamento := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_EQUIPAMENTO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_EQUIPAMENTO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirDadosEquipamento);

  //Carteira
  FParametroCarteira := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCarteira);

  //CheckList Padr�o
  FParametroCheckList := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CHECKLIST,
    TConstParametroFormulario.DESCRICAO_CHECKLIST,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCheckList);

  //Permite realizar ordem de servi�o para cliente consumidor final
  FParametroPermiteRealizarOrdemServicoParaConsumidorFinal := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_PERMITE_REALIZAR_ORDEM_SERVICO_PARA_CONSUMIDOR_FINAL,
    TConstParametroFormulario.DESCRICAO_PERMITE_REALIZAR_ORDEM_SERVICO_PARA_CONSUMIDOR_FINAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroPermiteRealizarOrdemServicoParaConsumidorFinal);
  
  //Aplicar Tributacao Nos Produtos
  FParametroAplicarTributosNosProdutos := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroAplicarTributosNosProdutos);

  //Data de Cadastro Inicio
  FParametroFiltroDataCadastroInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_CADASTRO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_CADASTRO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataCadastroInicio);

  //Data de Cadastro Fim
  FParametroFiltroDataCadastroFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_CADASTRO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_CADASTRO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataCadastroFim);

  //Data de Efetivacao Inicio
  FParametroFiltroDataEfetivacaoInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_EFETIVACAO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_EFETIVACAO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataEfetivacaoInicio);

  //Data de Efetivacao Fim
  FParametroFiltroDataEfetivacaoFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_EFETIVACAO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_EFETIVACAO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataEfetivacaoFim);
end;

procedure TMovOrdemServico.cxPageControlDetailsPageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
  inherited;
  if NewPage = tsDadosGerais then
  begin
    CalcularTamanhoDosCamposDeRelatos;
  end;
end;

procedure TMovOrdemServico.HabilitarBotaoEfetivar;
begin
  ActEfetivar.Visible :=
    (cdsDataSTATUS.AsString = TOrdemServicoProxy.STATUS_ABERTO) and
    (cdsDataVL_TOTAL_BRUTO.AsFloat > 0) and
    (TOrdemServico.OrdemServicoQuitada(cdsData, cdsOrdemServicoParcela));
end;

procedure TMovOrdemServico.IncrementarNrItem(Dataset: TDataset);
begin
  DataSet.FieldByName('NR_ITEM').AsFloat :=
    (TDatasetUtils.MaiorValor(DataSet.FieldByName('NR_ITEM'))+1);
end;

procedure TMovOrdemServico.InstanciarFrames;
begin
  inherited;
  FFrameProduto := TFrameOrdemServicoProduto.Create(PnFrameProduto);
  FFrameProduto.Parent := PnFrameProduto;
  FFrameProduto.Align := alClient;
  FFrameProduto.SetConfiguracoesIniciais(cdsData, cdsOrdemServicoProduto);

  FFrameServico := TFrameOrdemServicoServico.Create(PnFrameServico);
  FFrameServico.Parent := PnFrameServico;
  FFrameServico.Align := alClient;
  FFrameServico.SetConfiguracoesIniciaisServico(cdsOrdemServicoServico, false);

  FFrameServicoTerceiro := TFrameOrdemServicoServico.Create(PnFrameServicoTerceiro);
  FFrameServicoTerceiro.Parent := PnFrameServicoTerceiro;
  FFrameServicoTerceiro.Align := alClient;
  FFrameServicoTerceiro.SetConfiguracoesIniciaisServico(cdsOrdemServicoServico, true);

  FFrameFechamentoCrediario := TFrameOrdemServicoFechamentoCrediario.Create(PnFrameFechamento);
  FFrameFechamentoCrediario.Parent := PnFrameFechamento;
  FFrameFechamentoCrediario.Align := alClient;
  FFrameFechamentoCrediario.SetConfiguracoesIniciais(
  StrtoIntDef(FParametroFormaPagamentoAVista.Valor, 0),
  StrtoIntDef(FParametroPlanoPagamentoAVista.Valor, 0),
  cdsData, cdsOrdemServicoParcela);
end;

procedure TMovOrdemServico.ModificarParcelaAposParcelamento(Sender: TField);
begin
  inherited;
  if FPodeExecutarGeracaoParcela then
  try
    FPodeExecutarGeracaoParcela := false;

    TParcelamentoUtils.Reparcelar(Sender, cdsDataVL_TOTAL_LIQUIDO.AsFloat);

    if TParcelamentoUtils.ParcelamentoInvalido(Sender) then
      GerarParcelamento;
  finally
    TParcelamentoUtils.ConferirValorParcelado(Sender, cdsDataVL_TOTAL_LIQUIDO.AsFloat);
    FPodeExecutarGeracaoParcela := true;
  end;
end;

procedure TMovOrdemServico.MostrarAbaDeDados;
begin
  cxPageControlDetails.ActivePageIndex := 0;
end;

procedure TMovOrdemServico.MostrarInformacoesImplantacao;
begin
  inherited;
  TControlFunction.CreateMDIChild(TFrmHelpImplantacaoOrdemServico,
    FrmHelpImplantacaoOrdemServico);
end;

procedure TMovOrdemServico.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  //C�digo
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Data de Cadastro
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM ORDEM_SERVICO.DH_CADASTRO)';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Valor de Ordem Servico
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_TOTAL_LIQUIDO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Pessoa
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Pessoa';
  campoFiltro.campo := 'ID_PESSOA';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'PESSOA';
  campoFiltro.campoFK.camposConsulta := 'ID;NOME';
  AFiltroVertical.FCampos.Add(campoFiltro);}

{  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TOrdemServicoProxy.LISTA_STATUS;
  campoFiltro.campoCaixaSelecao.valores := campoFiltro.campoCaixaSelecao.itens;
  AFiltroVertical.FCampos.Add(campoFiltro);}

  //Data de Fechamento
{  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Fechamento';
  campoFiltro.campo := 'DH_FECHAMENTO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM ORDEM_SERVICO.DH_FECHAMENTO)';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro); }
end;

procedure TMovOrdemServico.DepoisDeAlterar;
begin
  inherited;
  MostrarAbaDeDados;
end;

procedure TMovOrdemServico.DepoisDeInserir;
begin
  inherited;
  MostrarAbaDeDados;
end;

procedure TMovOrdemServico.dsDataDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  PreencherCheckListParametrizadoNaOrdemDeServico;
end;

procedure TMovOrdemServico.PreencherCheckListParametrizadoNaOrdemDeServico;
begin
  if (cdsData.EstadoDeAlteracao) and (FParametroCheckList.AsInteger > 0)
    and (cdsDataID_CHECKLIST.AsInteger = 0) and (cdsData.ValidarCamposObrigatorios(false, false)) then
  begin
    cdsDataID_CHECKLIST.AsInteger := FParametroCheckList.AsInteger;
  end;
end;

procedure TMovOrdemServico.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovOrdemServico.EdtContaAnaliseKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    PesquisarContaAnalise;
end;

procedure TMovOrdemServico.EdtContaAnalisePropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovOrdemServico.EdtEquipamentogbDepoisDeConsultar(Sender: TObject);
var
  equipamento: TEquipamentoProxy;
begin
  inherited;
  if (not cdsData.GetAsString('EQUIPAMENTO').IsEmpty) then
  begin
    equipamento := TEquipamento.GetEquipamento(TEquipamento.BuscarIdEquipamentoPelaDescricao(
      cdsDataEQUIPAMENTO.AsString));

    cdsData.SetAsString('EQUIPAMENTO',equipamento.FDescricao);
    cdsData.SetAsString('EQUIPAMENTO_SERIE',equipamento.FSerie);
    cdsData.SetAsString('EQUIPAMENTO_IMEI',equipamento.FIMEI);

    if cdsDataID_PESSOA.AsString.IsEmpty or (equipamento.FIdPessoa > 0) then
    begin
      cdsData.SetAsInteger('ID_PESSOA', equipamento.FIdPessoa);
      cdsData.SetAsString('JOIN_NOME_PESSOA', TPessoa.GetNomePessoa(equipamento.FIdPessoa));
    end;
  end;
end;

procedure TMovOrdemServico.EdtPlacagbDepoisDeConsultar(Sender: TObject);
var
  veiculo: TVeiculoProxy;
begin
  inherited;
  if (cdsData.GetAsInteger('ID_VEICULO') > 0) then
  begin
    veiculo := TVeiculo.GetVeiculo(cdsData.GetAsInteger('ID_VEICULO'));

    cdsData.SetAsString('JOIN_PLACA_VEICULO',veiculo.FPlaca);
    cdsData.SetAsInteger('JOIN_ANO_VEICULO',veiculo.FAno);
    cdsData.SetAsString('JOIN_COMBUSTIVEL_VEICULO',veiculo.FCombustivel);
    cdsData.SetAsString('JOIN_MODELO_VEICULO',veiculo.FDescricaoModelo);
    cdsData.SetAsString('JOIN_MARCA_VEICULO',veiculo.FDescricaoMarca);
    cdsData.SetAsString('JOIN_COR_VEICULO',veiculo.FDescricaoCor);

    if cdsDataID_PESSOA.AsString.IsEmpty or (veiculo.FIdPessoa > 0) then
    begin
      cdsData.SetAsInteger('ID_PESSOA', veiculo.FIdPessoa);
      cdsData.SetAsString('JOIN_NOME_PESSOA', TPessoa.GetNomePessoa(veiculo.FIdPessoa));
    end;
  end;
end;

procedure TMovOrdemServico.GerarParcela(Sender: TField);
var
  planoPagamentoValido: Boolean;
  valorTituloValido: Boolean;
begin
  if not(FPodeExecutarGeracaoParcela) then
    exit;

  planoPagamentoValido := (not(cdsDataID_PLANO_PAGAMENTO.AsString.IsEmpty) or
    (TDatasetUtils.ValorFoiAlterado(cdsDataID_PLANO_PAGAMENTO))) and
    (not cdsDataID_PLANO_PAGAMENTO.AsString.Equals(
    FParametros.Valor(TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA)));

  valorTituloValido := (cdsDataVL_TOTAL_LIQUIDO.AsFloat > 0) or
    TDatasetUtils.ValorFoiAlterado(cdsDataVL_TOTAL_LIQUIDO);

  if (planoPagamentoValido) and
     (valorTituloValido) then
  begin
    GerarParcelamento;
  end;
end;

procedure TMovOrdemServico.GerarParcelamento;
begin
  FPodeExecutarGeracaoParcela := false;
  try
    TOrdemServico.LimparParcelas(cdsOrdemServicoParcela);
    TOrdemServico.GerarParcelas(cdsData, cdsOrdemServicoParcela,
      FParametroFormaPagamentoCrediario.AsInteger, FParametroCarteira.AsInteger);
  finally
    FPodeExecutarGeracaoParcela := true;
    HabilitarBotaoEfetivar;
  end;
end;

procedure TMovOrdemServico.GerenciarControles;
begin
  inherited;
  HabilitarBotaoCancelar;
  HabilitarBotaoEfetivar;
  dxBarReport.Visible := cdsData.State in dsEditModes;
  barFiltroRapido.Visible := AcaoCrud = uFrmCadastro_Padrao.TAcaoCrud(pesquisar);
  CalcularTamanhoDosCamposDeRelatos;
end;

function TMovOrdemServico.ParcelaNaFormaPagamentoCartao: Boolean;
begin
  result := cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_CREDITO) or
    cdsOrdemServicoParcelaJOIN_TIPO_FORMA_PAGAMENTO.AsString.Equals(TTipoQuitacaoProxy.TIPO_DEBITO);
end;

procedure TMovOrdemServico.PesquisarContaAnalise;
begin
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovOrdemServico.ValidarCentroResultado;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataID_CENTRO_RESULTADO);
end;

procedure TMovOrdemServico.ValidarContaAnalise;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataID_CONTA_ANALISE);
end;

procedure TMovOrdemServico.ValidarInformacoesCartao;
begin
  if ParcelaNaFormaPagamentoCartao and (cdsOrdemServicoParcelaID_OPERADORA_CARTAO.AsInteger <= 0) then
  begin
    TFrmMessage.Information('Em parcelas com cart�o � necess�rio informar a operadora do cart�o.');
    Abort;
  end;
end;

procedure TMovOrdemServico.ValidarPlanoPagamento;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataID_PLANO_PAGAMENTO);
end;

procedure TMovOrdemServico.ValidarSePodeRealizarOrdemServicoParaConsumidorFinal;
begin
  if (not FParametroPermiteRealizarOrdemServicoParaConsumidorFinal.ValorSim) and
    (TPessoa.VerificarConsumidorFinal(cdsDataID_PESSOA.AsInteger)) then
  begin
    TFrmMessage.Information(
      'N�o � permitido realizar ordem de servi�o para cliente de modalidade Consumidor Final');
    Abort;
  end;
end;

procedure TMovOrdemServico.ZerarTotalizadoresOS;
begin
  cdsDataVL_TOTAL_BRUTO_PRODUTO.AsFloat := 0;
  cdsDataVL_TOTAL_DESCONTO_PRODUTO.AsFloat := 0;
  cdsDataPERC_TOTAL_DESCONTO_PRODUTO.AsFloat := 0;
  cdsDataVL_TOTAL_ACRESCIMO_PRODUTO.AsFloat := 0;
  cdsDataPERC_TOTAL_ACRESCIMO_PRODUTO.AsFloat := 0;
  cdsDataVL_TOTAL_LIQUIDO_PRODUTO.AsFloat := 0;
  cdsDataVL_TOTAL_BRUTO_SERVICO.AsFloat := 0;
  cdsDataVL_TOTAL_DESCONTO_SERVICO.AsFloat := 0;
  cdsDataPERC_TOTAL_DESCONTO_SERVICO.AsFloat := 0;
  cdsDataVL_TOTAL_ACRESCIMO_SERVICO.AsFloat := 0;
  cdsDataPERC_TOTAL_ACRESCIMO_SERVICO.AsFloat := 0;
  cdsDataVL_TOTAL_LIQUIDO_SERVICO.AsFloat := 0;
  cdsDataVL_TOTAL_BRUTO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataVL_TOTAL_DESCONTO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataPERC_TOTAL_DESCONTO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataVL_TOTAL_ACRESCIMO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataPERC_TOTAL_ACRESCIMO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataVL_TOTAL_LIQUIDO_SERVICO_TERCEIRO.AsFloat := 0;
  cdsDataVL_TOTAL_BRUTO.AsFloat := 0;
  cdsDataVL_TOTAL_DESCONTO.AsFloat := 0;
  cdsDataPERC_TOTAL_DESCONTO.AsFloat := 0;
  cdsDataVL_TOTAL_ACRESCIMO.AsFloat := 0;
  cdsDataPERC_TOTAL_ACRESCIMO.AsFloat := 0;
  cdsDataVL_TOTAL_LIQUIDO.AsFloat := 0;
end;

initialization
  RegisterClass(TMovOrdemServico);

finalization
  UnRegisterClass(TMovOrdemServico);

end.
