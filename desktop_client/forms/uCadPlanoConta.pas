unit uCadPlanoConta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxTLData, cxDBTL, Vcl.StdCtrls, cxTextEdit, cxDBEdit,
  uGBDBTextEditPK, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, uGBDBTextEdit, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK, System.Generics.Collections, cxImage, Vcl.Grids,
  Vcl.DBGrids;

type
  TCadPlanoConta = class(TFrmCadastro_Padrao)
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label1: TLabel;
    TreeListPlanoConta: TcxDBTreeList;
    Label2: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    cdsDataID: TAutoIncField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataJOIN_CENTRO_RESULTADO: TStringField;
    fdmVisaoPlanoConta: TFDMemTable;
    fdmVisaoPlanoContaID: TIntegerField;
    fdmVisaoPlanoContaSEQUENCIA: TStringField;
    fdmVisaoPlanoContaDESCRICAO: TStringField;
    fdmVisaoPlanoContaNIVEL: TIntegerField;
    fdmVisaoPlanoContaID_ASSOCIACAO: TIntegerField;
    dsPlanoContaItem: TDataSource;
    cxDBTreeList1SEQUENCIA: TcxDBTreeListColumn;
    cxDBTreeList1DESCRICAO: TcxDBTreeListColumn;
    cdsDatafdqPlanoContaItem: TDataSetField;
    cdsPlanoContaItem: TGBClientDataSet;
    cdsPlanoContaItemID: TAutoIncField;
    cdsPlanoContaItemID_CONTA_ANALISE: TIntegerField;
    cdsPlanoContaItemID_PLANO_CONTA: TIntegerField;
    ColumnIcon: TcxDBTreeListColumn;
    ViewPopUpMenu: TPopupMenu;
    ActHabilitarConta: TAction;
    ActDesabilitarConta: TAction;
    Habilitar1: TMenuItem;
    Desabilitar1: TMenuItem;
    fdmVisaoPlanoContaBO_HABILITADO: TIntegerField;
    procedure gbDBButtonEditFK1Enter(Sender: TObject);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ExecutarAcaoNaVisao(Sender: TObject);
  private
    const VALOR_HABILITADO: Integer = 18;
    const VALOR_DESABILITADO: Integer = 17;
    procedure SetDefaultConfiguration;
    procedure SetVisaoPlanoContas;
    procedure ConfigurarVisao;
    procedure AdicionarRegistro;
    procedure RemoverRegistro;
  public

  end;

implementation

{$R *.dfm}

uses uDmConnection, uPlanoConta,
  uFrmMessage_Process, uTFunction, uDmAcesso;

{ TCadPlanoConta }

procedure TCadPlanoConta.AdicionarRegistro;
var i: Integer;
    idRegistro, idRegistroAtual: Integer;
    listaVisaoParaAtualizar: TStringList;
begin
  listaVisaoParaAtualizar := TStringList.Create;
  try
    fdmVisaoPlanoConta.DisableControls;
    idRegistroAtual := fdmVisaoPlanoContaID.AsInteger;
    for i := 0 to Pred(TreeListPlanoConta.SelectionCount) do
    begin
      idRegistro := (TreeListPlanoConta.Selections[i] as TcxDBTreeListNode).KeyValue;

      if not cdsPlanoContaItem.Locate('ID_CONTA_ANALISE', idRegistro, []) then
      begin
        cdsPlanoContaItem.Incluir;
        cdsPlanoContaItemID_CONTA_ANALISE.AsInteger := idRegistro;
        cdsPlanoContaItem.Salvar;

        listaVisaoParaAtualizar.Add(InttoStr(idRegistro));
      end;
    end;

    for i := 0 to Pred(listaVisaoParaAtualizar.Count) do
    begin
      idRegistro := StrtoInt(listaVisaoParaAtualizar[i]);
      fdmVisaoPlanoConta.Locate('id',idRegistro, []);

      while fdmVisaoPlanoContaBO_HABILITADO.AsInteger = VALOR_DESABILITADO do
      begin
        fdmVisaoPlanoConta.Edit;
        fdmVisaoPlanoContaBO_HABILITADO.AsInteger := VALOR_HABILITADO;
        fdmVisaoPlanoConta.Post;

        if not cdsPlanoContaItem.Locate('ID_CONTA_ANALISE', idRegistro, []) then
        begin
          cdsPlanoContaItem.Incluir;
          cdsPlanoContaItemID_CONTA_ANALISE.AsInteger := idRegistro;
          cdsPlanoContaItem.Salvar;
        end;

        idRegistro := fdmVisaoPlanoContaID_ASSOCIACAO.AsInteger;
        fdmVisaoPlanoConta.Locate('id',idRegistro, []);
      end;
    end;
  finally
    FreeAndNil(listaVisaoParaAtualizar);
    fdmVisaoPlanoConta.Locate('id',idRegistroAtual, []);
    fdmVisaoPlanoConta.EnableControls;
  end;
end;

procedure TCadPlanoConta.RemoverRegistro;
var i: Integer;
    idRegistro, nivel, idRegistroAtual: Integer;
    sequenciaOrigem: String;
    listaVisaoParaAtualizar: TStringList;
begin
  listaVisaoParaAtualizar := TStringList.Create;
  try
    fdmVisaoPlanoConta.DisableControls;
    idRegistroAtual := fdmVisaoPlanoContaID.AsInteger;
    for i := 0 to Pred(TreeListPlanoConta.SelectionCount) do
    begin
      idRegistro := (TreeListPlanoConta.Selections[i] as TcxDBTreeListNode).KeyValue;

      listaVisaoParaAtualizar.Add(InttoStr(idRegistro));
    end;

    for i := 0 to Pred(listaVisaoParaAtualizar.Count) do
    begin
      idRegistro := StrtoInt(listaVisaoParaAtualizar[i]);
      if fdmVisaoPlanoConta.Locate('id',idRegistro, []) then
      begin
        nivel := fdmVisaoPlanoContaNIVEL.AsInteger;
        sequenciaOrigem := fdmVisaoPlanoContaSEQUENCIA.AsString;
        try
          fdmVisaoPlanoConta.Filter :=
            '(id = '+InttoStr(idRegistro)+')'+//Registro Selecionado
            ' or (nivel > '+InttoStr(nivel)+')';
          fdmVisaoPlanoConta.Filtered := true;

          while not fdmVisaoPlanoConta.Eof do
          begin
            if Copy(fdmVisaoPlanoContaSEQUENCIA.AsString, 1, Length(sequenciaOrigem)) = sequenciaOrigem then
            begin
              fdmVisaoPlanoConta.Edit;
              fdmVisaoPlanoContaBO_HABILITADO.AsInteger := VALOR_DESABILITADO;
              fdmVisaoPlanoConta.Post;

              if cdsPlanoContaItem.Locate('ID_CONTA_ANALISE', fdmVisaoPlanoContaID.AsInteger, []) then
                cdsPlanoContaItem.Delete;
            end;

            fdmVisaoPlanoConta.Next;
          end;
        finally
          fdmVisaoPlanoConta.Filter := '';
          fdmVisaoPlanoConta.Filtered := false;
        end;
      end;
    end;
  finally
    FreeAndNil(listaVisaoParaAtualizar);
    fdmVisaoPlanoConta.Locate('id',idRegistroAtual, []);
    fdmVisaoPlanoConta.EnableControls;
  end;
end;

procedure TCadPlanoConta.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetVisaoPlanoContas;
end;

procedure TCadPlanoConta.FormCreate(Sender: TObject);
begin
  inherited;
  SetDefaultConfiguration;
end;

procedure TCadPlanoConta.gbDBButtonEditFK1Enter(Sender: TObject);
begin
  inherited;
  consultaExecutada := false;
end;

procedure TCadPlanoConta.SetDefaultConfiguration;
begin
  TreeListPlanoConta.OptionsView.Headers := false;
end;

procedure TCadPlanoConta.ConfigurarVisao;
  procedure SetIDAssociado;
  begin
    fdmVisaoPlanoContaID_ASSOCIACAO.AsInteger :=
      TPlanoConta.GetIDNivel(TFunction.Iif(fdmVisaoPlanoContaNIVEL.AsInteger = 1, 1, fdmVisaoPlanoContaNIVEL.AsInteger - 1),
        fdmVisaoPlanoContaSEQUENCIA.AsString);
  end;

  procedure SetBOHabilitado;
  var registroExiste: boolean;
  begin
    registroExiste := cdsPlanoContaItem.Locate('ID_CONTA_ANALISE', fdmVisaoPlanoContaID.AsInteger, []);
    fdmVisaoPlanoContaBO_HABILITADO.AsInteger := TFunction.Iif(registroExiste, VALOR_HABILITADO, VALOR_DESABILITADO);
  end;

begin
  fdmVisaoPlanoConta.DisableControls;
  try
    while not fdmVisaoPlanoConta.Eof do
    begin
      fdmVisaoPlanoConta.Edit;
      SetIDAssociado;
      SetBOHabilitado;
      fdmVisaoPlanoConta.Post;

      fdmVisaoPlanoConta.Next;
    end;
    fdmVisaoPlanoConta.First;
  finally
    fdmVisaoPlanoConta.EnableControls;
  end;
end;

procedure TCadPlanoConta.ExecutarAcaoNaVisao(Sender: TObject);
begin
  if Sender = ActHabilitarConta then
    AdicionarRegistro
  else if Sender = ActDesabilitarConta then
    RemoverRegistro;
end;

procedure TCadPlanoConta.SetVisaoPlanoContas;
begin
  try
    TFrmMessage_Process.SendMessage('Construindo plano de contas');
    TPlanoConta.SetPlanoContaCompleto(fdmVisaoPlanoConta);
    ConfigurarVisao;
    TreeListPlanoConta.FullExpand;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

initialization
  RegisterClass(TCadPlanoConta);
finalization
  UnRegisterClass(TCadPlanoConta);

end.
