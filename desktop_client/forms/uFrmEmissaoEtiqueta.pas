unit uFrmEmissaoEtiqueta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxCurrencyEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uGBFDMemTable, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid, Data.FireDACJSONReflect,
  Data.DBXCommon, dxRibbonRadialMenu, uGBPanel, cxDBEdit, uGBDBTextEdit, cxTextEdit, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK, Vcl.StdCtrls, FireDAC.Stan.Async, FireDAC.DApt, uGBFDQuery, uUsuarioDesignControl,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans,
  dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TFrmEmissaoEtiqueta = class(TFrmChildPadrao)
    lbBuscarProduto: TdxBarLargeButton;
    ActBuscarProduto: TAction;
    cxGridPesquisaPadrao: TcxGrid;
    viewProdutos: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    dsProdutos: TDataSource;
    fdmProdutos: TgbFDMemTable;
    ActExcluirTodosRegistros: TAction;
    ActBuscarProdutoSelecionado: TAction;
    ActBuscarTodosProdutos: TAction;
    ActExcluirProdutoSelecionado: TAction;
    dxRibbonRadialMenu: TdxRibbonRadialMenu;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    gbPanel1: TgbPanel;
    Label1: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBTextEdit1: TgbDBTextEdit;
    fdmTabelaPreco: TgbFDMemTable;
    fdmTabelaPrecoID_TABELA_PRECO: TIntegerField;
    fdmTabelaPrecoJOIN_DESCRICAO_TABELA_PRECO: TStringField;
    dsTabelaPreco: TDataSource;
    gbFDMemTable1: TgbFDMemTable;
    gbFDMemTable1ID_PRODUTO: TIntegerField;
    gbFDMemTable1QUANTIDADE: TIntegerField;
    ActAlterarSQL: TAction;
    dxBarButton5: TdxBarButton;
    procedure ActBuscarProdutoExecute(Sender: TObject);
    procedure ActExcluirTodosRegistrosExecute(Sender: TObject);
    procedure ActBuscarProdutoSelecionadoExecute(Sender: TObject);
    procedure ActBuscarTodosProdutosExecute(Sender: TObject);
    procedure ActExcluirProdutoSelecionadoExecute(Sender: TObject);
    procedure ActAlterarSQLExecute(Sender: TObject);
  private
    FParametroEmissaoEtiquetaArgox: TParametroFormulario;
    FParametroTabelaPrecoPadrao: TParametroFormulario;
    procedure InserirProdutos(const ADatasetPesquisaProduto: TFDMemTable);
    procedure ConfigurarColunas;
    procedure InicializarDatasetTabelaPreco;
  public
    class procedure EmitirEtiquetaProduto(AIDProduto, AQuantidade: TField);
  protected
    procedure Imprimir; override;
    procedure AoCriarFormulario; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

implementation

{$R *.dfm}

uses uPesqProduto, uProdutoProxy, uFrmMessage, uFrmMessage_Process, Generics.Collections, uDmConnection,
  uSistema, uFiltroFormulario, uDatasetUtils, uDevExpressUtils, uTUsuario,
  uFrmVisualizacaoRelatoriosAssociados, uTMessage, uCadProduto, uTControl_Function, uTFunction,
  uControlsUtils, uConstParametroFormulario, uProduto, uTabelaPreco, uEmissaoEtiqueta,
  uFrmAlterarSQLPesquisaPadrao;

procedure TFrmEmissaoEtiqueta.ActAlterarSQLExecute(Sender: TObject);
begin
  inherited;
  TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, TPesqProduto.NOME_CONSULTA_PRODUTO)
end;

procedure TFrmEmissaoEtiqueta.ActBuscarProdutoExecute(Sender: TObject);
var
  datasetConsultaProdutos: TFDMemTable;
  codigosProdutos: String;
begin
  inherited;
  TPesqProduto.BuscarProdutosComRecursoDeSelecao(fdmProdutos, viewProdutos);
  ConfigurarColunas;

  {datasetConsultaProdutos := TPesqProduto.BuscarDatasetProdutos(codigosProdutos);
  if (datasetConsultaProdutos <> nil) and not(codigosProdutos.IsEmpty) then
  begin
    InserirProdutos(datasetConsultaProdutos);
  end;}
end;

procedure TFrmEmissaoEtiqueta.ActBuscarProdutoSelecionadoExecute(Sender: TObject);
begin
  inherited;
  if fdmProdutos.IsEmpty then
    Exit;

  TCadProduto.AbrirTelaFiltrando(fdmProdutos.FieldByName('ID').AsString);
end;

procedure TFrmEmissaoEtiqueta.ActBuscarTodosProdutosExecute(Sender: TObject);
begin
  inherited;
  if fdmProdutos.IsEmpty then
    Exit;

  TCadProduto.AbrirTelaFiltrando(TDatasetUtils.Concatenar(fdmProdutos.FieldByName('ID')));
end;

procedure TFrmEmissaoEtiqueta.ActExcluirProdutoSelecionadoExecute(Sender: TObject);
begin
  inherited;
  if fdmProdutos.IsEmpty then
    Exit;

  if TMessage.MessageDeleteAsk() then
  begin
    fdmProdutos.Delete;
  end;
end;

procedure TFrmEmissaoEtiqueta.ActExcluirTodosRegistrosExecute(Sender: TObject);
begin
  inherited;
  if fdmProdutos.IsEmpty then
    Exit;

  if TMessage.MessageDeleteAsk(
    'Deseja realmente excluir todos os produtos da emiss�o de etiquetas?') then
  try
    fdmProdutos.DisableControls;
    while not fdmProdutos.IsEmpty do
    begin
      fdmProdutos.Delete;
    end;
  finally
    fdmProdutos.EnableControls;
  end;
end;

procedure TFrmEmissaoEtiqueta.AoCriarFormulario;
begin
  inherited;
  InicializarDatasetTabelaPreco;
end;

procedure TFrmEmissaoEtiqueta.ConfigurarColunas;
var i: Integer;
begin
  for i := 0 to Pred(viewProdutos.ColumnCount) do
  begin
    viewProdutos.Columns[i].Options.Editing :=
      viewProdutos.Columns[i].DataBinding.FieldName = 'QUANTIDADE_ETIQUETA';
  end;
end;

procedure TFrmEmissaoEtiqueta.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Etiqueta Argox
  FParametroEmissaoEtiquetaArgox := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EMISSAO_ETIQUETA_ARGOX,
    TConstParametroFormulario.DESCRICAO_EMISSAO_ETIQUETA_ARGOX,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroEmissaoEtiquetaArgox);

  FParametroTabelaPrecoPadrao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_TABELA_PRECO_PADRAO,
    TConstParametroFormulario.DESCRICAO_TABELA_PRECO_PADRAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroTabelaPrecoPadrao);
end;

class procedure TFrmEmissaoEtiqueta.EmitirEtiquetaProduto(AIDProduto, AQuantidade: TField);
var
  instanciaFormulario: TFrmEmissaoEtiqueta;
begin
  try
    try
      TFrmMessage_Process.SendMessage('Buscando informa��es');
      TControlsUtils.CongelarFormulario(Application.MainForm);
      instanciaFormulario := TFrmEmissaoEtiqueta(TControlFunction.GetInstance('TFrmEmissaoEtiqueta'));
      if not Assigned(instanciaFormulario) then
      begin
        instanciaFormulario := TFrmEmissaoEtiqueta(
          TControlFunction.CreateMDIForm('TFrmEmissaoEtiqueta'));
      end;
      instanciaFormulario.InserirProdutos(TPesqProduto.BuscarDatasetProdutosOculto(
        TDatasetUtils.Concatenar(AIDProduto)));

      try
        instanciaFormulario.fdmProdutos.DisableControls;
        AIDProduto.Dataset.First;
        while not AIDProduto.Dataset.Eof do
        begin
          if instanciaFormulario.fdmProdutos.Locate('ID',
            AIDProduto.AsInteger, []) then
          begin
            instanciaFormulario.fdmProdutos.Edit;
            instanciaFormulario.fdmProdutos.FieldByName('QUANTIDADE_ETIQUETA').AsString := AQuantidade.AsString;
            instanciaFormulario.fdmProdutos.Post;
          end;
          AIDProduto.Dataset.Next;
        end;
      finally
        instanciaFormulario.fdmProdutos.First;
        instanciaFormulario.fdmProdutos.EnableControls;
      end;

      instanciaFormulario.Show;
      Application.ProcessMessages;
    finally
      TControlsUtils.DescongelarFormulario;
      TFrmMessage_Process.CloseMessage;
    end;
  except
  end;
end;

procedure TFrmEmissaoEtiqueta.Imprimir;
var
  filtro: TFiltroPadrao;
  i: Integer;
const
  ID_CHAVE_PK_FILTRO = 0;
begin
  inherited;
  TValidacaoCampo.CampoPreenchido(fdmTabelaPrecoID_TABELA_PRECO);

{  filtro := TFiltroPadrao.Create;
  try
    filtro.AddFaixa(TProdutoProxy.FIELD_ID, TDatasetUtils.Concatenar(fdmProdutos.FieldByName('ID')));
    filtro.AddIgual(TProdutoProxy.FIELD_ID_TABELA_PRECO, fdmTabelaPrecoID_TABELA_PRECO.AsInteger);
    TFrmVisualizacaoRelatoriosAssociados.ImprimirRelatoriosAssociados(
      ID_CHAVE_PK_FILTRO, Self.Name, TSistema.Sistema.usuario.idSeguranca, filtro.Where);
  finally
    FreeAndNil(filtro);
  end; }

  if FParametroEmissaoEtiquetaArgox.ValorSim then
  begin
    TProdutoEtiqueta.ImprimirEtiquetaArgox(fdmProdutos.FieldByName('ID'), 0,
        fdmProdutos.FieldByName('QUANTIDADE_ETIQUETA'));
  end
  else
  begin
    try
      TFrmMessage_Process.SendMessage('Realizando Impress�o de Etiquetas');

      TEmissaoEtiqueta.LimparRegistrosDaEmissaoEtiqueta;

      fdmProdutos.DisableControls;
      fdmProdutos.GuardarBookmark;

      fdmProdutos.First;
      while not fdmProdutos.Eof do
      begin
        for i := 1 to StrtointDef(fdmProdutos.FieldByName('QUANTIDADE_ETIQUETA').AsString, 1) do
        begin
          TEmissaoEtiqueta.InserirProdutoParaEmissaoEtiqueta(fdmProdutos.FieldByName('ID').AsInteger);
        end;

        fdmProdutos.Next;
      end;

      TFrmVisualizacaoRelatoriosAssociados.ImprimirRelatoriosAssociados(
        0, Self.Name, TSistema.Sistema.usuario.idSeguranca, '', fdmProdutos);

  {    else //Impress�o unica.
      begin
        fdmProdutos.First;
        while not fdmProdutos.Eof do
        begin
          for i := 1 to StrtointDef(fdmProdutos.FieldByName('QUANTIDADE_ETIQUETA').AsString, 1) do
          begin
            TFrmVisualizacaoRelatoriosAssociados.ImprimirRelatoriosAssociados(
              fdmProdutos.FieldByName('ID').AsInteger, Self.Name, TSistema.Sistema.usuario.idSeguranca, '');
          end;

          fdmProdutos.Next;
        end;
      end;     }
    finally
      fdmProdutos.PosicionarBookmark;
      fdmProdutos.EnableControls;
      TEmissaoEtiqueta.LimparRegistrosDaEmissaoEtiqueta;
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TFrmEmissaoEtiqueta.InicializarDatasetTabelaPreco;
begin
  fdmTabelaPreco.Open;
  fdmTabelaPreco.Append;

  if FParametroTabelaPrecoPadrao.ValorNumericoValido then
  begin
    fdmTabelaPrecoID_TABELA_PRECO.AsInteger := FParametroTabelaPrecoPadrao.AsInteger;
    fdmTabelaPrecoJOIN_DESCRICAO_TABELA_PRECO.AsString :=
      TTabelaPreco.GetDescricaoTabelaPreco(FParametroTabelaPrecoPadrao.AsInteger);
  end;
end;

procedure TFrmEmissaoEtiqueta.InserirProdutos(const ADatasetPesquisaProduto: TFDMemTable);
var
  produtos: TList<TprodutoProxy>;
  produto: TProdutoProxy;
  i: Integer;

var newField: TFloatField;
begin
  try
    TFrmMessage_Process.SendMessage('Inserindo produtos');
    if fdmProdutos.Active then
    begin
      fdmProdutos.EmptyDataSet;
    end;

    fdmProdutos.CopyDataset(ADatasetPesquisaProduto, [coStructure, coRestart, coAppend]);

    TcxGridUtils.AdicionarTodosCamposNaView(viewProdutos);

    TUsuarioGridView.LoadGridView(viewProdutos,
      TSistema.Sistema.Usuario.idSeguranca, TPesqProduto.NOME_CONSULTA_PRODUTO);

    ConfigurarColunas;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

initialization
  RegisterClass(TFrmEmissaoEtiqueta);
finalization
  UnRegisterClass(TFrmEmissaoEtiqueta);


end.
