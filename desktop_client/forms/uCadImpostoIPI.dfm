inherited CadImpostoIPI: TCadImpostoIPI
  Caption = 'Cadastro de Imposto IPI'
  ClientWidth = 818
  ExplicitWidth = 834
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 818
    ExplicitWidth = 818
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 818
    Properties.ActivePage = cxtsData
    ExplicitWidth = 818
    ClientRectRight = 818
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 818
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 786
        ExplicitWidth = 786
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 818
      inherited DesignPanel: TJvDesignPanel
        Width = 818
        ExplicitWidth = 818
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 818
          Width = 818
          inherited dxBevel1: TdxBevel
            Width = 814
            ExplicitWidth = 814
          end
          object Label2: TLabel
            Left = 10
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 10
            Top = 40
            Width = 36
            Height = 13
            Caption = 'CST IPI'
          end
          object Label3: TLabel
            Left = 10
            Top = 66
            Width = 39
            Height = 13
            Caption = 'Al'#237'quota'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 66
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 123
            Top = 36
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_CST_IPI'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbPassword = False
            Width = 683
          end
          object gbDBButtonEditFK1: TgbDBButtonEditFK
            Left = 66
            Top = 36
            DataBinding.DataField = 'JOIN_CODIGO_CST_IPI'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClearKey = 16430
            Properties.ClickKey = 112
            Style.Color = 14606074
            TabOrder = 1
            gbTextEdit = gbDBTextEdit1
            gbRequired = True
            gbCampoPK = 'CODIGO_CST'
            gbCamposRetorno = 'ID_CST_IPI;JOIN_CODIGO_CST_IPI;JOIN_DESCRICAO_CST_IPI'
            gbTableName = 'CST_IPI'
            gbCamposConsulta = 'ID;CODIGO_CST;DESCRICAO'
            gbIdentificadorConsulta = 'CST_IPI'
            Width = 60
          end
          object gbDBCalcEdit1: TgbDBCalcEdit
            Left = 66
            Top = 62
            DataBinding.DataField = 'PERC_ALIQUOTA'
            DataBinding.DataSource = dsData
            Properties.DisplayFormat = '###,###,###,###,###,##0.00'
            Properties.ImmediatePost = True
            Properties.UseThousandSeparator = True
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbRequired = True
            Width = 120
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspImpostoIPI'
    RemoteServer = DmConnection.dspImpostoIPI
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataPERC_ALIQUOTA: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Aliquota'
      FieldName = 'PERC_ALIQUOTA'
      Origin = 'PERC_ALIQUOTA'
      Precision = 24
      Size = 9
    end
    object cdsDataID_CST_IPI: TIntegerField
      DisplayLabel = 'C'#243'digo CST IPI'
      FieldName = 'ID_CST_IPI'
      Origin = 'ID_CST_IPI'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_CST_IPI: TStringField
      DisplayLabel = 'CST IPI'
      FieldName = 'JOIN_DESCRICAO_CST_IPI'
      Origin = 'JOIN_DESCRICAO_CST_IPI'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_CODIGO_CST_IPI: TStringField
      DisplayLabel = 'C'#243'digo CST IPI'
      FieldName = 'JOIN_CODIGO_CST_IPI'
      Origin = 'JOIN_CODIGO_CST_IPI'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.885074236110000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
