unit uMovNotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset,
  cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase, frxClass, frxDBSet,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxSpinEdit, cxTimeEdit,
  cxCheckBox, uGBDBCheckBox, cxButtonEdit, uGBDBButtonEditFK, uGBDBTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, uGBDBDateEdit, uGBPanel,
  uFrameDetailPadrao, DateUtils,
  uFrameNotaFiscalItemRapido, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, uUsuarioDesignControl, cxSplitter, JvExControls, JvButton, JvTransparentButton,
  JvExStdCtrls, JvCombobox, JvDBCombobox, uGBDBValorComPercentual, cxRadioGroup, uGBDBRadioGroup,
  uFrameNotaFiscalItem, cxCalc, uGBDBCalcEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter,
  uNotaFiscalProxy;

type
  TMovNotaFiscal = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataB_NNF: TIntegerField;
    cdsDataB_SERIE: TIntegerField;
    cdsDataID_MODELO_NOTA: TIntegerField;
    cdsDataB_DEMI: TDateField;
    cdsDataB_DSAIENT: TDateField;
    cdsDataB_HSAIENT: TTimeField;
    cdsDataW_VPROD: TBCDField;
    cdsDataW_VST: TBCDField;
    cdsDataW_VSEG: TBCDField;
    cdsDataW_VDESC: TBCDField;
    cdsDataW_VOUTRO: TBCDField;
    cdsDataW_VIPI: TBCDField;
    cdsDataW_VFRETE: TBCDField;
    cdsDataW_VNF: TBCDField;
    cdsDataX_XNOME: TStringField;
    cdsDataID_CENTRO_RESULTADO: TIntegerField;
    cdsDataID_CONTA_ANALISE: TIntegerField;
    cdsDataID_CHAVE_PROCESSO: TIntegerField;
    cdsDataID_OPERACAO: TIntegerField;
    cdsDataID_FILIAL: TIntegerField;
    cdsDataVL_FRETE_TRANSPORTADORA: TBCDField;
    cdsDataDT_VENCIMENTO: TDateField;
    cdsDataID_PESSOA: TIntegerField;
    cdsDataJOIN_NOME_PESSOA: TStringField;
    cdsDataJOIN_DESCRICAO_OPERACAO: TStringField;
    cdsDataJOIN_DESCRICAO_MODELO: TStringField;
    cdsDataJOIN_SEQUENCIA_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField;
    cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField;
    cdsDatafdqNotaFiscalParcela: TDataSetField;
    cdsDatafdqNotaFiscalItem: TDataSetField;
    cxPageControlDetails: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    PnDados: TgbPanel;
    Label1: TLabel;
    gbDBDateEdit1: TgbDBDateEdit;
    Label3: TLabel;
    gbDBDateEdit2: TgbDBDateEdit;
    Label2: TLabel;
    EdtSerie: TgbDBTextEdit;
    Label9: TLabel;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    lbDtEntradaSaida: TLabel;
    gbDBDateEdit3: TgbDBDateEdit;
    gbDBTextEdit1: TgbDBTextEdit;
    Label5: TLabel;
    lbHrEntradaSaida: TLabel;
    cxDBTimeEdit1: TcxDBTimeEdit;
    lbPessoa: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit5: TgbDBTextEdit;
    cdsNotaFiscalItem: TGBClientDataSet;
    cdsNotaFiscalItemID: TAutoIncField;
    cdsNotaFiscalItemH_NITEM: TIntegerField;
    cdsNotaFiscalItemI_QCOM: TBCDField;
    cdsNotaFiscalItemI_VUNCOM: TFMTBCDField;
    cdsNotaFiscalItemI_VDESC: TBCDField;
    cdsNotaFiscalItemI_VFRETE: TBCDField;
    cdsNotaFiscalItemI_VSEG: TBCDField;
    cdsNotaFiscalItemI_VOUTRO: TBCDField;
    cdsNotaFiscalItemO_VIPI: TBCDField;
    cdsNotaFiscalItemID_PRODUTO: TIntegerField;
    cdsNotaFiscalItemJOIN_DESCRICAO_PRODUTO: TStringField;
    cdsNotaFiscalItemVL_LIQUIDO: TBCDField;
    cdsNotaFiscalItemVL_CUSTO_IMPOSTO: TBCDField;
    cdsNotaFiscalItemID_NOTA_FISCAL: TIntegerField;
    cdsNotaFiscalItemPERC_DESCONTO: TBCDField;
    cdsNotaFiscalItemPERC_ACRESCIMO: TBCDField;
    cdsNotaFiscalItemPERC_FRETE: TBCDField;
    cdsNotaFiscalItemPERC_SEGURO: TBCDField;
    cdsNotaFiscalItemPERC_IPI: TBCDField;
    PnParcela: TgbPanel;
    gridParcela: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cdsNotaFiscalParcela: TGBClientDataSet;
    dsNotaFiscalParcela: TDataSource;
    cdsNotaFiscalParcelaIC_DIAS: TIntegerField;
    cdsDataPERC_VST: TBCDField;
    cdsDataPERC_VSEG: TBCDField;
    cdsDataPERC_VDESC: TBCDField;
    cdsDataPERC_VOUTRO: TBCDField;
    cdsDataPERC_VIPI: TBCDField;
    cdsDataPERC_VFRETE: TBCDField;
    ActEstornar: TAction;
    ActEfetivar: TAction;
    cdsDataSTATUS: TStringField;
    lbEfetivar: TdxBarLargeButton;
    lbCancelar: TdxBarLargeButton;
    cdsDataID_PLANO_PAGAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField;
    cdsNotaFiscalItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField;
    cdsNotaFiscalItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField;
    cdsNotaFiscalParcelaID: TAutoIncField;
    cdsNotaFiscalParcelaDOCUMENTO: TStringField;
    cdsNotaFiscalParcelaVL_TITULO: TFMTBCDField;
    cdsNotaFiscalParcelaQT_PARCELA: TIntegerField;
    cdsNotaFiscalParcelaNR_PARCELA: TIntegerField;
    cdsNotaFiscalParcelaDT_VENCIMENTO: TDateField;
    cdsNotaFiscalParcelaID_NOTA_FISCAL: TIntegerField;
    cxGridDBBandedTableView1DOCUMENTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1NR_PARCELA: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1IC_DIAS: TcxGridDBBandedColumn;
    gbPanel1: TgbPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    gbDBTextEdit15: TgbDBTextEdit;
    gbDBTextEdit22: TgbDBTextEdit;
    Label13: TLabel;
    gbDBTextEdit8: TgbDBTextEdit;
    cdsDataID_FORMA_PAGAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    dxBarLargeButton4: TdxBarLargeButton;
    ActImprimirEtiqueta: TAction;
    ActManutencaoTabelaPreco: TAction;
    lbManutencaoTabelaPreco: TdxBarLargeButton;
    cdsDataDT_COMPETENCIA: TDateField;
    cdsNotaFiscalParcelaOBSERVACAO: TStringField;
    cdsNotaFiscalParcelaCHEQUE_SACADO: TStringField;
    cdsNotaFiscalParcelaCHEQUE_DOC_FEDERAL: TStringField;
    cdsNotaFiscalParcelaCHEQUE_BANCO: TStringField;
    cdsNotaFiscalParcelaCHEQUE_DT_EMISSAO: TDateField;
    cdsNotaFiscalParcelaCHEQUE_AGENCIA: TStringField;
    cdsNotaFiscalParcelaCHEQUE_CONTA_CORRENTE: TStringField;
    cdsNotaFiscalParcelaCHEQUE_NUMERO: TIntegerField;
    cdsNotaFiscalParcelaID_OPERADORA_CARTAO: TIntegerField;
    cdsNotaFiscalParcelaID_FORMA_PAGAMENTO: TIntegerField;
    cdsNotaFiscalParcelaID_CARTEIRA: TIntegerField;
    cdsNotaFiscalParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField;
    cdsNotaFiscalParcelaJOIN_DESCRICAO_CARTEIRA: TStringField;
    cxGridDBBandedTableView1ID_CARTEIRA: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JOIN_DESCRICAO_CARTEIRA: TcxGridDBBandedColumn;
    cdsNotaFiscalItemVL_CUSTO: TFMTBCDField;
    cdsNotaFiscalItemVL_CUSTO_OPERACIONAL: TFMTBCDField;
    cdsNotaFiscalItemPERC_CUSTO_OPERACIONAL: TFMTBCDField;
    cdsNotaFiscalItemI_XPROD: TStringField;
    cdsNotaFiscalItemN_VICMS: TBCDField;
    cdsNotaFiscalItemN_VBCST: TBCDField;
    cdsNotaFiscalItemN_PREDBCST: TBCDField;
    cdsNotaFiscalItemN_MOTDESICMS: TIntegerField;
    cdsNotaFiscalItemN_PMVAST: TBCDField;
    cdsNotaFiscalItemN_MODBCST: TIntegerField;
    cdsNotaFiscalItemI_NCM: TIntegerField;
    cdsNotaFiscalItemI_EXTIPI: TStringField;
    cdsNotaFiscalItemI_CFOP: TIntegerField;
    cdsNotaFiscalItemI_CEANTRIB: TStringField;
    cdsNotaFiscalItemI_UTRIB: TStringField;
    cdsNotaFiscalItemI_QTRIB: TBCDField;
    cdsNotaFiscalItemI_VUNTRIB: TFMTBCDField;
    cdsNotaFiscalItemI_INDTOT: TIntegerField;
    cdsNotaFiscalItemI_NVE: TStringField;
    cdsNotaFiscalItemI_CEAN: TStringField;
    cdsNotaFiscalItemN_ORIG: TIntegerField;
    cdsNotaFiscalItemN_CST: TIntegerField;
    cdsNotaFiscalItemN_MODBC: TIntegerField;
    cdsNotaFiscalItemN_PREDBC: TBCDField;
    cdsNotaFiscalItemN_VBC: TBCDField;
    cdsNotaFiscalItemN_PICMS: TBCDField;
    cdsNotaFiscalItemN_VICMSOP: TBCDField;
    cdsNotaFiscalItemN_UFST: TStringField;
    cdsNotaFiscalItemN_PDIF: TBCDField;
    cdsNotaFiscalItemN_VLICMSDIF: TBCDField;
    cdsNotaFiscalItemN_VICMSDESON: TBCDField;
    cdsNotaFiscalItemN_PBCOP: TBCDField;
    cdsNotaFiscalItemN_BCSTRET: TBCDField;
    cdsNotaFiscalItemN_PCREDSN: TBCDField;
    cdsNotaFiscalItemN_VBCSTDEST: TBCDField;
    cdsNotaFiscalItemN_VICMSSTDEST: TBCDField;
    cdsNotaFiscalItemN_VCREDICMSSN: TBCDField;
    cdsNotaFiscalItemO_CLENQ: TStringField;
    cdsNotaFiscalItemO_CNPJPROD: TStringField;
    cdsNotaFiscalItemO_CSELO: TStringField;
    cdsNotaFiscalItemO_QSELO: TFMTBCDField;
    cdsNotaFiscalItemO_CENQ: TStringField;
    cdsNotaFiscalItemO_CST: TIntegerField;
    cdsNotaFiscalItemO_VBC: TBCDField;
    cdsNotaFiscalItemO_QUNID: TBCDField;
    cdsNotaFiscalItemO_VUNID: TBCDField;
    cdsNotaFiscalItemO_PIPI: TBCDField;
    cdsNotaFiscalItemQ_CST: TIntegerField;
    cdsNotaFiscalItemQ_VBC: TBCDField;
    cdsNotaFiscalItemQ_PPIS: TBCDField;
    cdsNotaFiscalItemQ_VPIS: TBCDField;
    cdsNotaFiscalItemQ_QBCPROD: TBCDField;
    cdsNotaFiscalItemQ_VALIQPROD: TBCDField;
    cdsNotaFiscalItemS_CST: TIntegerField;
    cdsNotaFiscalItemS_VBC: TBCDField;
    cdsNotaFiscalItemS_PCOFINS: TBCDField;
    cdsNotaFiscalItemS_QBCPROD: TBCDField;
    cdsNotaFiscalItemS_VALIQPROD: TBCDField;
    cdsNotaFiscalItemS_VCOFINS: TBCDField;
    cdsNotaFiscalItemJOIN_BO_PRODUTO_AGRUPADOR_PRODUTO: TStringField;
    cdsNotaFiscalItemJOIN_ID_PRODUTO_AGRUPADOR_PRODUTO: TIntegerField;
    cdsNotaFiscalItemJOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO: TStringField;
    cdsDataJOIN_NOME_PESSOA_USUARIO: TStringField;
    cdsDataID_PESSOA_USUARIO: TIntegerField;
    cdsDataTIPO_NF: TStringField;
    cdsNotaFiscalItemI_UCOM: TStringField;
    cdsNotaFiscalItemI_VPROD: TBCDField;
    cdsNotaFiscalItemI_XPED: TStringField;
    cdsNotaFiscalItemI_NITEMPED: TIntegerField;
    ActEmitirNFE: TAction;
    lbEmitirNotaFiscal: TdxBarLargeButton;
    PnFrameNotaFiscalItem: TgbPanel;
    Label31: TLabel;
    EdtOperacaoFiscal: TgbDBButtonEditFK;
    EdtDescricaoOperacaoFiscal: TgbDBTextEdit;
    Label8: TLabel;
    EdtOperacao: TgbDBButtonEditFK;
    gbDBTextEdit2: TgbDBTextEdit;
    gbDBTextEdit25: TgbDBTextEdit;
    Label32: TLabel;
    gbDBTextEdit26: TgbDBTextEdit;
    lbSituacaoSefaz: TLabel;
    EdtSituacaoSefaz: TgbDBTextEdit;
    cdsDataNFE_CHAVE: TStringField;
    cdsDataAR_VERSAO: TStringField;
    cdsDataAR_TP_AMB: TStringField;
    cdsDataAR_VERAPLIC: TStringField;
    cdsDataAR_XMOTIVO: TStringField;
    cdsDataAR_CUF: TStringField;
    cdsDataAR_DHRECBTO: TDateTimeField;
    cdsDataAR_INFREC: TStringField;
    cdsDataAR_NREC: TStringField;
    cdsDataAR_TMED: TIntegerField;
    cdsDataAR_PROTNFE: TStringField;
    cdsDataID_OPERACAO_FISCAL: TIntegerField;
    cdsDataFORMA_PAGAMENTO: TStringField;
    PnDadosFechamento: TgbPanel;
    pnDadosFrete: TgbPanel;
    PnFinanceiro: TgbPanel;
    dxBevel2: TdxBevel;
    Label14: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label30: TLabel;
    Label34: TLabel;
    EdtPlanoPagamento: TgbDBButtonEditFK;
    gbDBTextEdit9: TgbDBTextEdit;
    gbDBTextEdit6: TgbDBTextEdit;
    gbDBButtonEditFK4: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    EdtContaAnalise: TcxDBButtonEdit;
    gbDBTextEdit10: TgbDBTextEdit;
    EdtFormaPagamento: TgbDBButtonEditFK;
    gbDBDateEdit4: TgbDBDateEdit;
    dxBevel3: TdxBevel;
    lbInformacoesCliente: TLabel;
    Label35: TLabel;
    EdtValorFrete: TGBDBValorComPercentual;
    EdtModalidadeFrete: TJvDBComboBox;
    Label36: TLabel;
    gbDBRadioGroup1: TgbDBRadioGroup;
    Label37: TLabel;
    cdsDataJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField;
    cdsDataJOIN_MODELO_MODELO: TStringField;
    cdsDataX_MODFRETE: TIntegerField;
    EdtQuitacaoDinheiro: TgbDBCalcEdit;
    Label38: TLabel;
    cdsDataVL_QUITACAO_DINHEIRO: TFMTBCDField;
    GBDBValorComPercentual1: TGBDBValorComPercentual;
    GBDBValorComPercentual2: TGBDBValorComPercentual;
    GBDBValorComPercentual3: TGBDBValorComPercentual;
    GBDBValorComPercentual4: TGBDBValorComPercentual;
    GBDBValorComPercentual5: TGBDBValorComPercentual;
    GBDBValorComPercentual6: TGBDBValorComPercentual;
    ActEmitirNFCE: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    cdsNotaFiscalItemN_VICMSSTRET: TBCDField;
    cdsNotaFiscalItemID_IMPOSTO_ICMS: TIntegerField;
    Label18: TLabel;
    EdtDocumento: TgbDBTextEdit;
    cdsDataDOC_FEDERAL_PESSOA: TStringField;
    cdsDataBO_ENVIO_DPEC: TStringField;
    cdsDataNR_REG_DPEC: TStringField;
    cdsDataDH_REG_DPEC: TDateTimeField;
    cdsDataW_VBC: TBCDField;
    cdsDataW_VICMS: TBCDField;
    cdsDataW_VBCST: TBCDField;
    cdsDataW_VII: TBCDField;
    cdsDataW_VPIS: TBCDField;
    cdsDataW_VCOFINS: TBCDField;
    cdsDataW_VRETPIS: TBCDField;
    cdsDataW_VRETCOFINS: TBCDField;
    cdsDataW_VRETCSLL: TBCDField;
    cdsDataW_VBCIRRF: TBCDField;
    cdsDataW_VIRRF: TBCDField;
    cdsDataW_VBCRETPREV: TBCDField;
    cdsDataW_VRETPREV: TBCDField;
    cdsDataPERC_ICMS: TBCDField;
    cdsDataPERC_PIS: TBCDField;
    cdsDataPERC_COFINS: TBCDField;
    cdsDataAR_CSTAT: TStringField;
    cdsDataDOCUMENTO_FISCAL_XML: TBlobField;
    cdsNotaFiscalItemID_IMPOSTO_PIS_COFINS: TIntegerField;
    cdsNotaFiscalItemID_OPERACAO_FISCAL: TIntegerField;
    cdsNotaFiscalItemN_NVICMSST: TBCDField;
    cdsNotaFiscalItemID_IMPOSTO_IPI: TIntegerField;
    cdsNotaFiscalItemPERC_ICMS_ST: TBCDField;
    cdsNotaFiscalItemN_PICMSST: TBCDField;
    cdsNotaFiscalItemN_VICMSST: TBCDField;
    ActCancelarEmissaoNFE: TAction;
    ActCancelarEmissaoNFCE: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    bbCancelarNFCE: TdxBarLargeButton;
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure ActEstornarExecute(Sender: TObject);
    procedure ActEfetivarExecute(Sender: TObject);
    procedure GerarParcela(Sender: TField);
    procedure EdtContaAnalisePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure EdtContaAnaliseDblClick(Sender: TObject);
    procedure EdtContaAnaliseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsNotaFiscalItemAfterPost(DataSet: TDataSet);
    procedure cdsNotaFiscalItemAfterDelete(DataSet: TDataSet);
    procedure cdsNotaFiscalParcelaIC_DIASChange(Sender: TField);
    procedure cdsNotaFiscalParcelaDT_VENCIMENTOChange(Sender: TField);
    procedure ModificarParcelaAposParcelamento(Sender: TField);
    procedure ActImprimirEtiquetaExecute(Sender: TObject);
    procedure ActManutencaoTabelaPrecoExecute(Sender: TObject);
    procedure AoAlterarCarteira(Sender: TField);
    procedure cxGridDBBandedTableView1ID_CARTEIRAPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGridDBBandedTableView1ID_CARTEIRAPropertiesEditValueChanged(Sender: TObject);
    procedure ActEmitirNFEExecute(Sender: TObject);
    procedure CondicionarHabilitacaoCamposFinanceiro(Sender: TField);
    procedure EdtOperacaoFiscalgbAntesDeConsultar(Sender: TObject);
    procedure cdsNotaFiscalItemNewRecord(DataSet: TDataSet);
    procedure AoAltearFretePeloCampo(Sender: TField);
    procedure CalcularTotaisDoItem(Sender: TField);
    procedure ActEmitirNFCEExecute(Sender: TObject);
    procedure cdsDataTIPO_NFChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure cdsDataID_OPERACAOChange(Sender: TField);
    procedure cdsDataJOIN_MODELO_MODELOChange(Sender: TField);
    procedure ActCancelarEmissaoNFEExecute(Sender: TObject);
    procedure ActCancelarEmissaoNFCEExecute(Sender: TObject);
  private
    FPodeExecutarGeracaoParcela: Boolean;
    FCalculandoPeriodoParcelamento: Boolean;
    FPodeExecutarAlteracaoCarteira: Boolean;
    FCalculandoFrete: Boolean;
    FCalculandoTotalItens: Boolean;
    FCalculandoTotalNota: Boolean;
    FSugerindoDadosFinanceiros: Boolean;

    FParametroCarteira: TParametroFormulario;
    FParametroExibirDadosFrete: TParametroFormulario;
    FparametroFormaPagamentoAVista: TParametroFormulario;
    FparametroPlanoPagamentoAVista: TParametroFormulario;

    FFrameNotaFiscalItem: TFrameNotaFiscalItem;

    procedure SugerirDadosFinanceirosQuandoFormaPagamentoAVista;
    procedure SugerirValorNotaQuitacaoDinheiro;
    procedure SugerirDadosPorModeloNotaFiscal;
    procedure HabilitarBotaoCancelar;
    procedure HabilitarBotaoCancelarNFE;
    procedure HabilitarBotaoCancelarNFCE;
    procedure HabilitarBotaoEfetivar;
    procedure HabilitarBotaoManutencaoTabelaPreco;
    procedure HabilitarBotaoEmitirNFE;
    procedure HabilitarBotaoEmitirNFCE;
    procedure HabilitarFrameItensParaEdicao;
    procedure HabilitarOperacaoFiscal;
    procedure AtualizarValorNota;
    procedure PesquisarContaAnalise;
    procedure GerarParcelamento;
    procedure MostrarAbaDeDados;
    procedure ValidarDataDeCompetencia;
    procedure EmitirNFE;
    procedure EmitirNFCE;
    procedure CriarFrameNotaFiscalItem;
    procedure ExibirDadosFrete;
    procedure HabilitarCamposFinanceiro;
    procedure InstanciarFiltrosComponentesConsulta;
    procedure DefinirTipoNotaFiscal;
    procedure ModificarLayoutTipoNota;
    procedure IncrementarNrItem(Dataset: TDataset);
    procedure SugerirSerieNotaFiscal;
    procedure SugerirNumeroNotaFiscal;
    procedure ExibirSituacaoSefaz;
    function GetNotaFiscalTransienteProxy: TNotaFiscalTransienteProxy;
  public
    FParametroHabilitarEmissaoNFCE: TParametroFormulario;
    FParametroHabilitarEmissaoNFE: TParametroFormulario;
    FParametroTabelaPrecoPadrao: TParametroFormulario;
    FparametroContaCorrente: TParametroFormulario;
  protected
    procedure GerenciarControles; override;
    procedure AntesDeRemover; override;
    procedure AntesDeConfirmar; override;
    procedure DepoisDeConfirmar; override;
    procedure AoCriarFormulario; override;
    procedure DepoisDeAlterar;override;
    procedure DepoisDeInserir;override;
    procedure DepoisDeVisualizar;override;
    procedure InstanciarFrames; override;

    procedure BloquearEdicao; override;

    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uNotaFiscal, uPesqContaAnalise,
  uFrmMessage, uMathUtils, uFrmMessage_Process, uDateUtils, uDatasetUtils,
  uChaveProcesso, uFilial, uPlanoPagamento, uProduto,
  uMovNotaFiscalManutencaoTabelaPreco, uChaveProcessoProxy, uCarteiraProxy, uCarteira, uConsultaGrid,
  uConstParametroFormulario, uFrmEmissaoEtiqueta, uSistema, uRetornoProxy,
  uFiltroFormulario, uOperacaoProxy, uFrmExibirListaMensagemSimples, uOperacao,
  uFormaPagamento, uControlsUtils;

procedure TMovNotaFiscal.HabilitarBotaoCancelar;
begin
  ActEstornar.Visible := cdsDataSTATUS.AsString = STATUS_NOTA_CONFIRMADA;
end;

procedure TMovNotaFiscal.HabilitarBotaoCancelarNFCE;
begin
  ActCancelarEmissaoNFCE.Visible := cdsDataSTATUS.AsString.Equals(STATUS_NOTA_CONFIRMADA) and
    (FParametroHabilitarEmissaoNFCE.ValorSim) and
    (cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFCE)) and
    (cdsDataAR_CSTAT.AsString.Equals(TNotaFiscalProxy.STATUS_NFE_ENVIADA));
end;

procedure TMovNotaFiscal.HabilitarBotaoCancelarNFE;
begin
  ActCancelarEmissaoNFE.Visible := cdsDataSTATUS.AsString.Equals(STATUS_NOTA_CONFIRMADA) and
    (FParametroHabilitarEmissaoNFE.ValorSim) and
    (cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFE)) and
    (cdsDataAR_CSTAT.AsString.Equals(TNotaFiscalProxy.STATUS_NFE_ENVIADA));
end;

procedure TMovNotaFiscal.ActEmitirNFCEExecute(Sender: TObject);
begin
  inherited;
  EmitirNFCE;
end;

procedure TMovNotaFiscal.ActEmitirNFEExecute(Sender: TObject);
begin
  inherited;
  EmitirNFE;
end;

procedure TMovNotaFiscal.ActEstornarExecute(Sender: TObject);
const MSG_NAO_PODE_ESTORNAR: String =
  'N�o � poss�vel estornar o documento pois existe quita��o realizada.';
var idChaveProcesso: Integer;
begin
  inherited;
  if TFrmMessage.Question('Deseja estornar a nota fiscal?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    if (cdsData.Alterando) then
    begin
      ActConfirm.Execute;
    end;

    if TNotaFiscal.PodeEstornar(cdsDataID_CHAVE_PROCESSO.AsInteger) then
    begin
      TFrmMessage_Process.SendMessage('Estornando a nota fiscal');
      try
        TNotaFiscal.Cancelar(idChaveProcesso);
        AtualizarRegistro;
      finally
        TFrmMessage_Process.CloseMessage;
      end;
    end
    else
      TFrmMessage.Information(MSG_NAO_PODE_ESTORNAR);
  end;
end;

procedure TMovNotaFiscal.ActCancelarEmissaoNFCEExecute(Sender: TObject);
begin
  inherited;
  TNotaFiscal.CancelarDocumentoFiscal(cdsDataID.AsInteger, TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
  AtualizarRegistro;
end;

procedure TMovNotaFiscal.ActCancelarEmissaoNFEExecute(Sender: TObject);
begin
  inherited;
  TNotaFiscal.CancelarDocumentoFiscal(cdsDataID.AsInteger, TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
  AtualizarRegistro;
end;

procedure TMovNotaFiscal.ActEfetivarExecute(Sender: TObject);
var idChaveProcesso: Integer;
begin
  inherited;
  if TFrmMessage.Question('Deseja efetivar a nota fiscal?') = MCONFIRMED then
  begin
    idChaveProcesso := cdsDataID_CHAVE_PROCESSO.AsInteger;

    ActConfirm.Execute;

    TFrmMessage_Process.SendMessage('Efetivando a nota fiscal');
    try
      TNotaFiscal.Efetivar(idChaveProcesso, GetNotaFiscalTransienteProxy);
      AtualizarRegistro;
    finally
      MostrarAbaDeDados;
      TFrmMessage_Process.CloseMessage;
    end;
  end;
end;

procedure TMovNotaFiscal.ActImprimirEtiquetaExecute(Sender: TObject);
begin
  inherited;
  TFrmEmissaoEtiqueta.EmitirEtiquetaProduto(cdsNotaFiscalItemID_PRODUTO, cdsNotaFiscalItemI_QCOM);
end;

procedure TMovNotaFiscal.ActManutencaoTabelaPrecoExecute(Sender: TObject);
begin
  inherited;
  TMovNotaFiscalManutencaoTabelaPreco.RealizarManutencaoNaTabelaPreco(cdsNotaFiscalItem);
end;

procedure TMovNotaFiscal.SugerirDadosFinanceirosQuandoFormaPagamentoAVista;
begin
  if FSugerindoDadosFinanceiros then
  begin
    Exit;
  end;

  try
    FSugerindoDadosFinanceiros := true;

    if cdsDataFORMA_PAGAMENTO.AsString.Equals(TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA) then
    begin
      SugerirValorNotaQuitacaoDinheiro;

      cdsDataID_FORMA_PAGAMENTO.AsInteger := FparametroFormaPagamentoAVista.AsInteger;
      cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO.AsString :=
        TFormaPagamento.GetDescricaoFormaPagamento(FparametroFormaPagamentoAVista.AsInteger);
      cdsDataID_PLANO_PAGAMENTO.AsInteger := FparametroPlanoPagamentoAVista.AsInteger;
      cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO.AsString :=
        TPlanoPagamento.GetDescricaoPlanoPagamento(FparametroFormaPagamentoAVista.AsInteger);

      TNotaFiscal.LimparParcelas(cdsNotaFiscalParcela);
    end
    else
    begin
      cdsDataVL_QUITACAO_DINHEIRO.AsFloat := 0;
      cdsDataID_FORMA_PAGAMENTO.Clear;
      cdsDataID_PLANO_PAGAMENTO.Clear;
      cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO.Clear;
      cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO.Clear;
    end;
  finally
    FSugerindoDadosFinanceiros := false;
  end;
end;

procedure TMovNotaFiscal.SugerirDadosPorModeloNotaFiscal;
begin
  if (not cdsDataJOIN_MODELO_MODELO.AsString.IsEmpty) and
    (not cdsDataTIPO_NF.AsString.IsEmpty) then
  begin
    SugerirNumeroNotaFiscal;
    SugerirSerieNotaFiscal;
  end;
end;

procedure TMovNotaFiscal.SugerirNumeroNotaFiscal;
var
  numeroNF: Integer;
begin
  if cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    if cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFE) then
    begin
      numeroNF := TNotaFiscal.GetNumeroDocumentoNotaFiscalSaida(
        TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
    end
    else
    begin
      numeroNF := TNotaFiscal.GetNumeroDocumentoNotaFiscalSaida(
        TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
    end;

    cdsDataB_NNF.AsInteger := numeroNF;
  end
  else
  begin
    cdsDataB_NNF.Clear;
  end;
end;

procedure TMovNotaFiscal.SugerirSerieNotaFiscal;
var
  serieNF: Integer;
begin
  if cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    if cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFE) then
    begin
      serieNF := TNotaFiscal.GetSerieNotaFiscalSaida(
        TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
    end
    else
    begin
      serieNF := TNotaFiscal.GetSerieNotaFiscalSaida(
        TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
    end;

    cdsDataB_SERIE.AsInteger := serieNF;
  end
  else
  begin
    cdsDataB_SERIE.Clear;
  end;
end;

procedure TMovNotaFiscal.SugerirValorNotaQuitacaoDinheiro;
begin
  cdsDataVL_QUITACAO_DINHEIRO.AsFloat := cdsDataW_VNF.AsFloat;
end;

procedure TMovNotaFiscal.AntesDeConfirmar;
begin
  {Campos n�o requireds no server por causa da tela}
  TValidacaoCampo.CampoPreenchido(cdsDataID_CENTRO_RESULTADO);
  TValidacaoCampo.CampoPreenchido(cdsDataID_PLANO_PAGAMENTO);
  TValidacaoCampo.CampoPreenchido(cdsDataID_CONTA_ANALISE);
  TValidacaoCampo.CampoPreenchido(cdsDataID_FORMA_PAGAMENTO);

  {Requires no server mais nao no client por causa da inclusao dos itens}
  ValidarDataDeCompetencia;

  inherited;
end;

procedure TMovNotaFiscal.AntesDeRemover;
const MSG_NAO_PODE_EXCLUIR: String =
  'N�o � poss�vel excluir o documento pois existe quita��o realizada.';
begin
  inherited;
  if not TNotaFiscal.PodeExcluir(cdsDataID_CHAVE_PROCESSO.AsInteger) then
  begin
    TFrmMessage.Information(MSG_NAO_PODE_EXCLUIR);
    abort;
  end;
end;

procedure TMovNotaFiscal.AoAltearFretePeloCampo(Sender: TField);
begin
  if FCalculandoFrete then
  begin
    exit;
  end;

  try
    FCalculandoFrete := true;
    TNotaFiscal.RatearFreteNotaParaItens(cdsData, cdsNotaFiscalitem);
  finally
    FCalculandoFrete := false;
  end;
end;

procedure TMovNotaFiscal.AoAlterarCarteira(Sender: TField);
var
  idCarteira: Integer;
  carteira: TCarteiraProxy;
  Bookmark: TBookmark;
begin
  if FPodeExecutarAlteracaoCarteira then
  try
    FPodeExecutarAlteracaoCarteira := false;

    idCarteira := Sender.Dataset.FieldByName('ID_CARTEIRA').AsInteger;

    carteira := TCarteira.GetCarteira(idCarteira);

    try
      Sender.Dataset.DisableControls;
      Bookmark := Sender.Dataset.GetBookMark;
      while not Sender.Dataset.Eof do
      begin
        Sender.Dataset.Edit;
        Sender.AsInteger := carteira.Fid;
        Sender.Dataset.FieldByName('JOIN_DESCRICAO_CARTEIRA').AsString :=
          carteira.FDescricao;
        Sender.Dataset.Post;

        Sender.Dataset.Next;
      end;
    finally
      if (Bookmark <> nil) and Sender.Dataset.BookmarkValid(Bookmark) then
        Sender.Dataset.GoToBookmark(Bookmark);

      Sender.Dataset.EnableControls;
    end;
  finally
    FPodeExecutarAlteracaoCarteira := true;
  end;
end;

procedure TMovNotaFiscal.AoCriarFormulario;
begin
  inherited;
  FPodeExecutarGeracaoParcela := true;
  FCalculandoFrete := false;
  FSugerindoDadosFinanceiros := false;
  MostrarAbaDeDados;
  ExibirDadosFrete;
  InstanciarFiltrosComponentesConsulta;
end;

procedure TMovNotaFiscal.AtualizarValorNota;
begin
  TNotaFiscal.CalcularValorNotaFiscal(cdsData, cdsNotaFiscalItem);

  if cdsNotaFiscalParcela.IsEmpty and (cdsDataFORMA_PAGAMENTO.AsString.Equals(
    TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA)) then
  begin
    SugerirValorNotaQuitacaoDinheiro;
  end
  else
  begin
    GerarParcela(nil);
  end;
end;

procedure TMovNotaFiscal.BloquearEdicao;
begin
  inherited;
  Self.bloquearEdicaoDados :=
    not cdsDataSTATUS.AsString.Equals(STATUS_NOTA_ABERTA);
end;

procedure TMovNotaFiscal.CalcularTotaisDoItem(Sender: TField);
begin
  TNotaFiscal.CalcularValorTotalItem(cdsData, Sender.Dataset);
end;

procedure TMovNotaFiscal.HabilitarBotaoEfetivar;
begin
  ActEfetivar.Visible := (cdsDataSTATUS.AsString = STATUS_NOTA_ABERTA)
    and (not(cdsNotaFiscalParcela.IsEmpty) or
    (cdsDataW_VNF.AsFloat = cdsDataVL_QUITACAO_DINHEIRO.AsFloat));
end;

procedure TMovNotaFiscal.HabilitarBotaoEmitirNFCE;
begin
  ActEmitirNFCE.Visible :=
    (FParametroHabilitarEmissaoNFCE.ValorSim) and
    (cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFCE)) and
    (cdsDataAR_CSTAT.AsString.Equals(TNotaFiscalProxy.STATUS_NFE_AENVIAR));
end;

procedure TMovNotaFiscal.HabilitarBotaoEmitirNFE;
begin
  ActEmitirNFE.Visible :=
    (FParametroHabilitarEmissaoNFE.ValorSim) and
    (cdsDataJOIN_MODELO_MODELO.AsString.Equals(TNotaFiscalProxy.MODELO_NFE)) and
    (cdsDataAR_CSTAT.AsString.Equals(TNotaFiscalProxy.STATUS_NFE_AENVIAR));
  //Implementar status emitada
end;

procedure TMovNotaFiscal.HabilitarBotaoManutencaoTabelaPreco;
begin
  ActManutencaoTabelaPreco.Visible := (cdsDataSTATUS.AsString.Equals(STATUS_NOTA_CONFIRMADA));
end;

procedure TMovNotaFiscal.HabilitarCamposFinanceiro;
var
  pagamentoIntegralVista: Boolean;
begin
  EdtQuitacaoDinheiro.gbReadyOnly :=
    cdsDataFORMA_PAGAMENTO.AsString.Equals(TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA);

  pagamentoIntegralVista := cdsDataW_VNF.AsFloat = cdsDataVL_QUITACAO_DINHEIRO.AsFloat;

  EdtFormaPagamento.gbReadyOnly := (pagamentoIntegralVista) and
    (cdsDataFORMA_PAGAMENTO.AsString.Equals(TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA));
  EdtPlanoPagamento.gbReadyOnly := (pagamentoIntegralVista) and
    (cdsDataFORMA_PAGAMENTO.AsString.Equals(TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA));
end;

procedure TMovNotaFiscal.HabilitarFrameItensParaEdicao;
begin
  if cdsData.State in dsEditModes then
  begin
    FFrameNotaFiscalItem.DesativarSomenteLeitura;
  end
  else
  begin
    FFrameNotaFiscalItem.AtivarSomenteLeitura;
  end;
end;

procedure TMovNotaFiscal.HabilitarOperacaoFiscal;
var
  notaFiscalEntrada: Boolean;
begin
  notaFiscalEntrada :=
    cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_ENTRADA);

  EdtOperacaoFiscal.gbReadyOnly := notaFiscalEntrada;
  EdtOperacaoFiscal.TabStop := not notaFiscalEntrada;

  if notaFiscalEntrada then
  begin
   cdsDataID_OPERACAO_FISCAL.Clear;
   cdsDataJOIN_DESCRICAO_OPERACAO_FISCAL.Clear;

   exit;
  end;

  if cdsDataID_OPERACAO_FISCAL.AsString.IsEmpty then
  begin
    TControlsUtils.SetFocus(EdtOperacaoFiscal);
  end;
end;

procedure TMovNotaFiscal.InstanciarFiltrosComponentesConsulta;
begin
  EdtOperacaoFiscal.FWhereInjetado := TFiltroPadrao.Create;
end;

procedure TMovNotaFiscal.InstanciarFrames;
begin
  inherited;
  CriarFrameNotaFiscalItem;
end;

procedure TMovNotaFiscal.CondicionarHabilitacaoCamposFinanceiro(Sender: TField);
begin
  SugerirDadosFinanceirosQuandoFormaPagamentoAVista;
  HabilitarCamposFinanceiro;
end;

procedure TMovNotaFiscal.CriarFrameNotaFiscalItem;
begin
  //Nota Fiscal Item
  FFrameNotaFiscalItem := TFrameNotaFiscalItem.Create(PnFrameNotaFiscalItem);
  FFrameNotaFiscalItem.Parent := PnFrameNotaFiscalItem;
  FFrameNotaFiscalItem.Align := alClient;
  FFrameNotaFiscalItem.SetConfiguracoesIniciais(cdsData, cdsNotaFiscalItem);
end;

procedure TMovNotaFiscal.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FFrameNotaFiscalItem.GerenciarCamposDeAcordoComTipoNotaFiscal;
end;

procedure TMovNotaFiscal.cdsDataID_OPERACAOChange(Sender: TField);
begin
  inherited;
  DefinirTipoNotaFiscal;
  SugerirDadosPorModeloNotaFiscal;
end;

procedure TMovNotaFiscal.cdsDataJOIN_MODELO_MODELOChange(Sender: TField);
begin
  inherited;
  SugerirDadosPorModeloNotaFiscal;
end;

procedure TMovNotaFiscal.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataB_DEMI.AsDateTime := Date;
  cdsDataB_DSAIENT.AsDateTime := Date;
  cdsDataB_HSAIENT.AsDateTime := Time;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataID_CHAVE_PROCESSO.AsInteger := TChaveProcesso.NovaChaveProcesso(TChaveProcessoProxy.ORIGEM_NOTA_FISCAL, 0);
  cdsDataID_FILIAL.AsInteger := TSistema.Sistema.filial.FId;
  cdsDataID_PESSOA_USUARIO.AsInteger := TSistema.Sistema.usuario.id;
  cdsDataJOIN_NOME_PESSOA_USUARIO.AsString := TSistema.Sistema.usuario.nome;
  cdsDataTIPO_NF.AsString := TNotaFiscalProxy.TIPO_NF_ENTRADA;
  cdsDataX_MODFRETE.AsInteger := TNotaFiscalProxy.MODALIDADE_FRETE_SEM_FRETE;
  cdsDataFORMA_PAGAMENTO.AsString := TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA;
  cdsDataAR_CSTAT.AsString := TNotaFiscalProxy.STATUS_NFE_AENVIAR;
end;

procedure TMovNotaFiscal.cdsDataTIPO_NFChange(Sender: TField);
begin
  inherited;
  ModificarLayoutTipoNota;
  FFrameNotaFiscalItem.GerenciarCamposDeAcordoComTipoNotaFiscal;
end;

procedure TMovNotaFiscal.cdsNotaFiscalItemAfterDelete(DataSet: TDataSet);
begin
  inherited;
  if cdsData.State in dsEditModes then
    AtualizarValorNota;
end;

procedure TMovNotaFiscal.cdsNotaFiscalItemAfterPost(DataSet: TDataSet);
begin
  inherited;
  AtualizarValorNota;
end;

procedure TMovNotaFiscal.cdsNotaFiscalItemNewRecord(DataSet: TDataSet);
begin
  inherited;
  IncrementarNrItem(DataSet);
  DataSet.FieldByName('I_QCOM').AsFloat := 1;
  DataSet.FieldByName('I_VUNCOM').AsFloat := 0;
  DataSet.FieldByName('I_VDESC').AsFloat := 0;
  DataSet.FieldByName('I_VFRETE').AsFloat := 0;
  DataSet.FieldByName('I_VSEG').AsFloat := 0;
  DataSet.FieldByName('I_VOUTRO').AsFloat := 0;
  DataSet.FieldByName('N_VICMS').AsFloat := 0;
  DataSet.FieldByName('O_VIPI').AsFloat := 0;
  DataSet.FieldByName('VL_LIQUIDO').AsFloat := 0;
  DataSet.FieldByName('VL_CUSTO_IMPOSTO').AsFloat := 0;
  DataSet.FieldByName('PERC_DESCONTO').AsFloat := 0;
  DataSet.FieldByName('PERC_ACRESCIMO').AsFloat := 0;
  DataSet.FieldByName('PERC_FRETE').AsFloat := 0;
  DataSet.FieldByName('PERC_SEGURO').AsFloat := 0;
  DataSet.FieldByName('N_PICMS').AsFloat := 0;
  DataSet.FieldByName('PERC_IPI').AsFloat := 0;
end;

procedure TMovNotaFiscal.cdsNotaFiscalParcelaDT_VENCIMENTOChange(
  Sender: TField);
begin
  inherited;
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsNotaFiscalParcelaIC_DIAS.AsInteger := DaysBetween(cdsDataDT_COMPETENCIA.AsDateTime,
      cdsNotaFiscalParcelaDT_VENCIMENTO.AsDateTime);
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovNotaFiscal.cdsNotaFiscalParcelaIC_DIASChange(Sender: TField);
begin
  inherited;
  if FCalculandoPeriodoParcelamento then
    exit;

  try
    FCalculandoPeriodoParcelamento := true;
    cdsNotaFiscalParcelaDT_VENCIMENTO.AsDateTime := cdsDataDT_COMPETENCIA.AsDateTime +
      cdsNotaFiscalParcelaIC_DIAS.AsInteger;
  finally
    FCalculandoPeriodoParcelamento := false;
  end;
end;

procedure TMovNotaFiscal.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Carteira
  FParametroCarteira := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CARTEIRA,
    TConstParametroFormulario.DESCRICAO_CARTEIRA,
    TParametroFormulario.PARAMETRO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroCarteira);

  //Habilitar Emiss�o de NFCE
  FParametroHabilitarEmissaoNFCE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFCE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFCE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFCE);

  //Habilitar Emiss�o de NFE
  FParametroHabilitarEmissaoNFE := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_HABILITAR_EMISSAO_NFE,
    TConstParametroFormulario.DESCRICAO_HABILITAR_EMISSAO_NFE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroHabilitarEmissaoNFE);

  //Exibir Dados Frete
  FParametroExibirDadosFrete := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_FRETE,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_FRETE,
    TParametroFormulario.PARAMETRO_OBRIGATORIO, TParametroFormulario.VALOR_NAO);
  AParametros.AdicionarParametro(FParametroExibirDadosFrete);

  //Parametro Forma de Pagamento a Vista
  FparametroFormaPagamentoAVista := TParametroFormulario.Create;
  FparametroFormaPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_FORMA_PAGAMENTO_AVISTA;
  FparametroFormaPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroFormaPagamentoAVista);

  //Parametro Plano de Pagamento a Vista
  FparametroPlanoPagamentoAVista := TParametroFormulario.Create;
  FparametroPlanoPagamentoAVista.parametro :=
    TConstParametroFormulario.PARAMETRO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.descricao :=
    TConstParametroFormulario.DESCRICAO_PLANO_PAGAMENTO_AVISTA;
  FparametroPlanoPagamentoAVista.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroPlanoPagamentoAVista);

  //Tabela Pre�o Padr�o
  FParametroTabelaPrecoPadrao := TParametroFormulario.Create;
  FParametroTabelaPrecoPadrao.parametro :=
    TConstParametroFormulario.PARAMETRO_TABELA_PRECO_PADRAO;
  FParametroTabelaPrecoPadrao.descricao :=
    TConstParametroFormulario.DESCRICAO_TABELA_PRECO_PADRAO;
  FParametroTabelaPrecoPadrao.obrigatorio := true;
  AParametros.AdicionarParametro(FParametroTabelaPrecoPadrao);

  //Parametro Conta Corrente - Entrada
  FparametroContaCorrente := TParametroFormulario.Create;
  FparametroContaCorrente.parametro :=
    TConstParametroFormulario.PARAMETRO_CONTA_CORRENTE;
  FparametroContaCorrente.descricao :=
    TConstParametroFormulario.DESCRICAO_CONTA_CORRENTE+' - PARA A ENTRADA EM DINHEIRO';
  FparametroContaCorrente.obrigatorio := true;
  AParametros.AdicionarParametro(FparametroContaCorrente);
end;

procedure TMovNotaFiscal.cxGridDBBandedTableView1ID_CARTEIRAPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  TConsultaGrid.ConsultarPorCarteira(cdsNotaFiscalParcela);
end;

procedure TMovNotaFiscal.cxGridDBBandedTableView1ID_CARTEIRAPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  gridParcela.SetFocus;
  TConsultaGrid.ConsultarPorCarteira(cdsNotaFiscalParcela, false,
    cxGridDBBandedTableView1ID_CARTEIRA.EditValue);
end;

procedure TMovNotaFiscal.ModificarLayoutTipoNota;
begin
  if cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_ENTRADA) then
  begin
    lbDtEntradaSaida.Caption := 'Dt. Entrada';
    lbHrEntradaSaida.Caption := 'Hr. Entrada';
    lbPessoa.Caption := 'Fornecedor';
    EdtDocumento.gbReadyOnly := false;
    EdtSerie.gbReadyOnly := false;
  end
  else if cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_SAIDA) then
  begin
    lbDtEntradaSaida.Caption := 'Dt. Sa�da';
    lbHrEntradaSaida.Caption := 'Hr. Sa�da';
    lbPessoa.Caption := 'Cliente';
    EdtDocumento.gbReadyOnly := true;
    EdtSerie.gbReadyOnly := true;
  end;

  ExibirSituacaoSefaz;
  HabilitarOperacaoFiscal;
end;

procedure TMovNotaFiscal.ModificarParcelaAposParcelamento(Sender: TField);
begin
  inherited;
  if FPodeExecutarGeracaoParcela then
  try
    FPodeExecutarGeracaoParcela := false;

    TParcelamentoUtils.Reparcelar(Sender, cdsDataW_VNF.AsFloat);

    if TParcelamentoUtils.ParcelamentoInvalido(Sender) then
      GerarParcelamento;
  finally
    TParcelamentoUtils.ConferirValorParcelado(Sender, cdsDataW_VNF.AsFloat);
    FPodeExecutarGeracaoParcela := true;
  end;
end;

procedure TMovNotaFiscal.MostrarAbaDeDados;
begin
  cxPageControlDetails.ActivePageIndex := 0;
end;

procedure TMovNotaFiscal.DefinirTipoNotaFiscal;
begin
  if TOperacao.ExisteAcaoNotaFiscalEntrada(cdsDataID_OPERACAO.AsInteger) then
  begin
    cdsDataTIPO_NF.AsString := TNotaFiscalProxy.TIPO_NF_ENTRADA;
    exit;
  end;

  if TOperacao.ExisteAcaoNotaFiscalSaida(cdsDataID_OPERACAO.AsInteger) then
  begin
    cdsDataTIPO_NF.AsString := TNotaFiscalProxy.TIPO_NF_SAIDA;
  end;
end;

procedure TMovNotaFiscal.DepoisDeAlterar;
begin
  inherited;
end;

procedure TMovNotaFiscal.DepoisDeConfirmar;
var
  idGerado: Integer;
begin
  inherited;
  if cdsData.FieldByName('ID').AsInteger <= 0 then
  begin
    AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('NOTA_FISCAL', 'ID'), 0));
  end;
end;

procedure TMovNotaFiscal.DepoisDeInserir;
begin
  inherited;
  MostrarAbaDeDados;
end;

procedure TMovNotaFiscal.DepoisDeVisualizar;
begin
  inherited;
  MostrarAbaDeDados;
end;

procedure TMovNotaFiscal.EdtContaAnaliseDblClick(Sender: TObject);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovNotaFiscal.EdtContaAnaliseKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    PesquisarContaAnalise;
end;

procedure TMovNotaFiscal.EdtContaAnalisePropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  PesquisarContaAnalise;
end;

procedure TMovNotaFiscal.EdtOperacaoFiscalgbAntesDeConsultar(Sender: TObject);
begin
  inherited;
  EdtOperacaoFiscal.FWhereInjetado.Limpar;
  EdtOperacaoFiscal.FWhereInjetado.AddIgual(TOperacaoFiscalProxy.FIELD_ID_OPERACAO,
    cdsDataID_OPERACAO.AsInteger);
end;

procedure TMovNotaFiscal.EmitirNFCE;
begin
  TNotaFiscal.EmitirNFCE(cdsDataID.AsInteger);
  TNotaFiscal.ImprimirNotaFiscal(cdsDataID.AsInteger, TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFCE);
  AtualizarRegistro;
end;

procedure TMovNotaFiscal.EmitirNFE;
begin
  TNotaFiscal.EmitirNFE(cdsDataID.AsInteger);
  TNotaFiscal.ImprimirNotaFiscal(cdsDataID.AsInteger, TNotaFiscalProxy.TIPO_DOCUMENTO_FISCAL_NFE);
  AtualizarRegistro;
end;

procedure TMovNotaFiscal.ExibirDadosFrete;
begin
  pnDadosFrete.Visible := FParametroExibirDadosFrete.ValorSim;
end;

procedure TMovNotaFiscal.ExibirSituacaoSefaz;
var
  exibirSituacaoSefaz: Boolean;
begin
  exibirSituacaoSefaz := cdsDataTIPO_NF.AsString.Equals(TNotaFiscalProxy.TIPO_NF_SAIDA)
     and (FParametroHabilitarEmissaoNFCE.ValorSim or FParametroHabilitarEmissaoNFE.ValorSim);

  lbSituacaoSefaz.Visible := exibirSituacaoSefaz;
  edtSituacaoSefaz.Visible := exibirSituacaoSefaz;
end;

procedure TMovNotaFiscal.GerarParcela(Sender: TField);
var
  planoPagamentoValido: Boolean;
  valorTituloValido: Boolean;
  dataBaseValida: Boolean;
begin
  if not(FPodeExecutarGeracaoParcela) then
    exit;

  if cdsDataFORMA_PAGAMENTO.AsString.Equals(
    TNotaFiscalProxy.FORMA_PAGAMENTO_VISTA) then
  begin
    exit;
  end;

  planoPagamentoValido := ((not(cdsDataID_PLANO_PAGAMENTO.AsString.IsEmpty) or
    (TDatasetUtils.ValorFoiAlterado(cdsDataID_PLANO_PAGAMENTO))) and
    (not cdsDataID_PLANO_PAGAMENTO.AsString.Equals(FParametroPlanoPagamentoAVista.AsString)));

  valorTituloValido := (cdsDataW_VNF.AsFloat > 0) or
    TDatasetUtils.ValorFoiAlterado(cdsDataW_VNF);

  dataBaseValida := not(cdsDataDT_COMPETENCIA.AsString.IsEmpty) or
    TDatasetUtils.ValorFoiAlterado(cdsDataDT_COMPETENCIA);

  if (planoPagamentoValido) and
     (valorTituloValido) and
     (dataBaseValida) then
  begin
    GerarParcelamento;
  end;
end;

procedure TMovNotaFiscal.GerarParcelamento;
begin
  FPodeExecutarGeracaoParcela := false;
  try
    TNotaFiscal.LimparParcelas(cdsNotaFiscalParcela);
    TNotaFiscal.GerarParcelas(cdsData, cdsNotaFiscalParcela, FParametroCarteira.AsInteger);
  finally
    FPodeExecutarGeracaoParcela := true;
    HabilitarBotaoEfetivar;
    HabilitarBotaoManutencaoTabelaPreco;
  end;
end;

procedure TMovNotaFiscal.GerenciarControles;
begin
  inherited;
  HabilitarBotaoCancelar;
  HabilitarBotaoEfetivar;
  HabilitarBotaoManutencaoTabelaPreco;
  HabilitarBotaoEmitirNFE;
  HabilitarBotaoEmitirNFCE;
  HabilitarBotaoCancelarNFCE;
  HabilitarBotaoCancelarNFE;
  HabilitarFrameItensParaEdicao;
  ActImprimirEtiqueta.Visible := cdsData.Active and cdsNotaFiscalItem.Active and not(cdsNotaFiscalItem.IsEmpty);
end;

function TMovNotaFiscal.GetNotaFiscalTransienteProxy: TNotaFiscalTransienteProxy;
begin
  result := TNotaFiscalTransienteProxy.Create;
  result.FIdContaCorrente := FParametroContaCorrente.AsInteger;

  result.FIdFormaPagamentoDinheiro :=
    FParametroFormaPagamentoAVista.AsInteger;

  result.FIdCarteira := FParametroCarteira.AsInteger;
end;

procedure TMovNotaFiscal.PesquisarContaAnalise;
begin
  if not cdsDataID_CENTRO_RESULTADO.AsString.IsEmpty then
  begin
    TPesqContaAnalise.ExecutarConsulta(cdsDataID_CENTRO_RESULTADO.AsInteger,
      cdsDataID_CONTA_ANALISE, cdsDataJOIN_DESCRICAO_CONTA_ANALISE);
  end;
end;

procedure TMovNotaFiscal.ValidarDataDeCompetencia;
begin
  TValidacaoCampo.CampoPreenchido(cdsDataDT_COMPETENCIA);
end;

procedure TMovNotaFiscal.IncrementarNrItem(Dataset: TDataset);
begin
  DataSet.FieldByName('H_NITEM').AsFloat :=
    (TDatasetUtils.MaiorValor(DataSet.FieldByName('H_NITEM'))+1);
end;

Initialization
  RegisterClass(TMovNotaFiscal);

Finalization
  UnRegisterClass(TMovNotaFiscal);

end.
