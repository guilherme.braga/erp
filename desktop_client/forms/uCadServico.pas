unit uCadServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxTextEdit, cxDBEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxMaskEdit, cxSpinEdit, cxTimeEdit, cxCheckBox,
  uGBDBCheckBox, uGBDBTextEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon;

type
  TCadServico = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataVL_CUSTO: TFMTBCDField;
    cdsDataPERC_LUCRO: TFMTBCDField;
    cdsDataVL_VENDA: TFMTBCDField;
    cdsDataDURACAO: TTimeField;
    cdsDataBO_SERVICO_TERCEIRO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    Label2: TLabel;
    edDescricao: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBTextEdit3: TgbDBTextEdit;
    Label5: TLabel;
    Label1: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    Label4: TLabel;
    gbDBCheckBox2: TgbDBCheckBox;
    cxDBTimeEdit1: TcxDBTimeEdit;
    procedure CalcularValor(Sender: TField);
  private
    FCalculandoValores : Boolean;
  public

  protected
    procedure AoCriarFormulario; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection, uMathUtils;

procedure TCadServico.AoCriarFormulario;
begin
  inherited;
  FCalculandoValores := False;
end;

procedure TCadServico.CalcularValor(Sender: TField);
var
  vlCusto : Double;
begin
  if FCalculandoValores then
    Exit;

  FCalculandoValores := True;

  if Sender = cdsDataVL_CUSTO then
  begin
    vlCusto := cdsDataVL_CUSTO.AsFloat;
    cdsDataVL_VENDA.AsFloat := vlCusto +
      TMathUtils.ValorSobrePercentual(cdsDataPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = cdsDataPERC_LUCRO then
  begin
    vlCusto := cdsDataVL_CUSTO.AsFloat;
    cdsDataVL_VENDA.AsFloat := vlCusto +
      TMathUtils.ValorSobrePercentual(cdsDataPERC_LUCRO.AsFloat, vlCusto);
  end
  else
  if Sender = cdsDataVL_VENDA then
  begin
    vlCusto := cdsDataVL_CUSTO.AsFloat;
    if vlCusto > 0 then
      cdsDataPERC_LUCRO.AsFloat :=
        TMathUtils.PercentualSobreValor(cdsDataVL_VENDA.AsFloat, vlCusto);
  end;

  FCalculandoValores := False;
end;

initialization
  RegisterClass(TCadServico);

finalization
  UnRegisterClass(TCadServico);

end.
