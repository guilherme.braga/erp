unit uRelFluxoCaixaResumido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxCalendar, dxBarExtItems,
  cxBarEditItem, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, dxLayoutContainer, cxGridViewLayoutContainer,
  cxGridLayoutView, cxGridDBLayoutView, cxGridCustomLayoutView,
  cxGridDBTableView, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid, cxPC, DateUtils,
  Vcl.Menus, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, cxDBLookupComboBox, cxDropDownEdit, dxRibbonRadialMenu, cxRadioGroup,
  uUsuarioDesignControl, cxVGrid, cxInplaceContainer, cxDBVGrid, ugbFDMemTable;

type
  TOrdenacaoContaCorrente = (ordenarContaCorrenteParaCima, ordenarContaCorrenteParaBaixo);

  TRelFluxoCaixaResumido = class(TFrmChildPadrao)
    ActProximo: TAction;
    ActAnterior: TAction;
    ActAtualizar: TAction;
    dxBarOperacao: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxPeriodo: TdxBar;
    dxBarAtualizar: TdxBarLargeButton;
    FiltroPeriodoInicio: TcxBarEditItem;
    FiltroPeriodoFim: TcxBarEditItem;
    filtroPeriodoRotulo: TdxBarStatic;
    pgFluxoCaixaResumido: TcxPageControl;
    tsContaCorrente: TcxTabSheet;
    gridContaCorrente: TcxGrid;
    ViewContaCorrente: TcxGridDBBandedTableView;
    cxGridLevel12: TcxGridLevel;
    tsEstruturaFluxoCaixaResumido: TcxTabSheet;
    fdmContaCorrente: TgbFDMemTable;
    fdmEstruturaFluxoCaixaResumido: TgbFDMemTable;
    dsContaCorrente: TDataSource;
    dsEstruturaFluxoCaixaResumido: TDataSource;
    fdmConsultaDados: TgbFDMemTable;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    AlterarRtulodasColunas1: TMenuItem;
    ACExtratoConta16x16: TActionList;
    ActAlterarColunasGrid: TAction;
    ActRestaurarColunasPadrao: TAction;
    ActRestaurarColunasPadrao1: TMenuItem;
    ActExportarExcel: TAction;
    ActExibirAgrupamento: TAction;
    ExportarparaExcel1: TMenuItem;
    ActExpandirTudo: TAction;
    ActRecolherAgrupamento: TAction;
    ActExportarPDF: TAction;
    ExpandirAgrupamentos1: TMenuItem;
    RecolherAgrupamento1: TMenuItem;
    ExportarparaPDF1: TMenuItem;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    N1: TMenuItem;
    N2: TMenuItem;
    ActAbrirMovimentoSelecionado: TAction;
    ActAbrirTodosRegistros: TAction;
    ExplorarTodososRegistros1: TMenuItem;
    ExplorarTodososRegistros2: TMenuItem;
    dxRibbonRadialMenu: TdxRibbonRadialMenu;
    bbExplorarRegistroSelecionado: TdxBarButton;
    bbExplorarTodosRegistros: TdxBarButton;
    bbExportarExcel: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    bbExpandirAgrupamentos: TdxBarButton;
    bbRecolherAgrupamentos: TdxBarButton;
    bbAlterarRotulo: TdxBarButton;
    bbRestaurarConfiguracoes: TdxBarButton;
    dxBarSeparator1: TdxBarSeparator;
    cxBarEditItem1: TcxBarEditItem;
    FiltroTipoPeriodo: TcxBarEditItem;
    ViewEstruturaFluxoCaixaResumido: TcxDBVerticalGrid;
    ActImprimirGrade: TAction;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    procedure ActProximoExecute(Sender: TObject);
    procedure ActAnteriorExecute(Sender: TObject);
    procedure ActAtualizarExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActExpandirTudoExecute(Sender: TObject);
    procedure ActRecolherAgrupamentoExecute(Sender: TObject);
    procedure pmTituloGridPesquisaPadraoPopup(Sender: TObject);
    procedure FiltroTipoPeriodoPropertiesEditValueChanged(Sender: TObject);
    procedure ActExportarPDFExecute(Sender: TObject);
    procedure ActExportarExcelExecute(Sender: TObject);
    procedure ActImprimirGradeExecute(Sender: TObject);
    procedure ViewContaCorrenteNavigatorButtonsButtonClick(Sender: TObject; AButtonIndex: Integer;
      var ADone: Boolean);
    procedure ViewContaCorrenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FDtInicial, FDtFinal: String;
    FListaPeriodo: TStringList;
    FTipoPeriodoUtilizado: String;

    FParametroFiltroDataCompetenciaInicio: TParametroFormulario;
    FParametroFiltroDataCompetenciaFim: TParametroFormulario;

    procedure ValidarPreenchimentoDoPeriodo;
    function GetDisplayLabelDoCampoPeriodo(const AMes: String): String;
    function GetFieldNameDoCampoPeriodo(const AMes: String): String;
    procedure DeletarCamposPersonalizados(const AQueryNivel: array of TFDMemTable;
      const AView: array of TcxGridDBBandedTableView);
    procedure MarcarTodos(Sender: TObject);
    procedure SetListaPeriodo;
    procedure PrepararFiltros;
    procedure SmartOpen(Sender: TObject);
    function String_AGG(AQuery: TFDMemTable; const AFieldName: String; const AStringAgg: String = ''): String;
    procedure SetValorPeriodo(const AQuery: TFDMemTable; const AStringAGGContaCorrente: String);
    procedure ConfigurarColunas(AView: TcxGridDBBandedTableView);
    procedure CarregarPersonalizacaoGrid(AView: TcxGridDBBandedTableView);
    procedure SalvarPersonalizacaoGrid;
    function ViewAtivaNaTela: TcxGridDBBandedTableView;
    function CriarCampoMesAno(const AQuery: TFDMemTable; AFieldName, ADisplayLabel: AnsiString): TFloatField;

    procedure SetListaPeriodoMensal;
    procedure SetListaPeriodoSemanal;
    procedure SetListaPeriodoDiario;

    procedure OrdenacaoInicial;
    procedure RealizarOrdenacaoSalva(const AOrdenacao: String);
    function BuscarOrdenacaoConfiguradaPeloUsuario: String;
    procedure SalvarOrdenadacaoContaCorrente;
    procedure AjustarCamposDatasetContaCorrente;
    procedure MudarOrdemContaCorrente(const AOrdem: TOrdenacaoContaCorrente);

    const FILTRO_TIPO_PERIODO_DIARIO = 'DIARIO';
    const FILTRO_TIPO_PERIODO_SEMANAL = 'SEMANAL';
    const FILTRO_TIPO_PERIODO_MENSAL = 'MENSAL';

  public
    { Public declarations }
  protected
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
  end;

implementation

{$R *.dfm}

uses uDateUtils, uFrmMessage, uTFunction, uFrmMessage_Process, uPlanoConta,
  uDevExpressUtils, uTUsuario, uDatasetUtils, uFrmApelidarColunasGrid,
  uDmAcesso, uSistema, uFluxoCaixaResumido, uConstParametroFormulario, uVerticalGridUtils,
  uFluxoCaixaResumidoProxy;

procedure TRelFluxoCaixaResumido.ActAlterarColunasGridExecute(Sender: TObject);
var view: TcxGridDBBandedTableView;
begin
  inherited;
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    view := ViewContaCorrente;
  {else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    view := ViewEstruturaFluxoCaixaResumido;}

  TFrmApelidarColunasGrid.SetColumns(view);
end;

procedure TRelFluxoCaixaResumido.ActAnteriorExecute(Sender: TObject);
begin
  inherited;
  pgFluxoCaixaResumido.SelectNextPage(false);
  SmartOpen(Sender);
end;

function TRelFluxoCaixaResumido.String_AGG(AQuery: TFDMemTable; const AFieldName: String; const AStringAgg: String = ''): String;
begin
  result := AStringAgg;
  if AQuery.Active then
  try
    AQuery.DisableControls;
    AQuery.First;
    while not AQuery.Eof do
    begin
      if AQuery.FieldByName('BO_CHECKED').AsString = 'S' then
      begin
        if Pos(','+AQuery.FieldByName(AFieldName).AsString, result) = 0 then
        begin
          if result <> '' then
            result := result + ',';
          result := result + AQuery.FieldByName(AFieldName).AsString;
        end;
      end;
      AQuery.Next;
    end;
  finally
    AQuery.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.ActAtualizarExecute(Sender: TObject);
var
  AnsiStringAGGContaCorrente: String;
  fluxoCaixaResumido: TFluxoCaixaResumidoProxy;
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Carregando dados');

    ValidarPreenchimentoDoPeriodo;

    FDtInicial := VartoStr(FiltroPeriodoInicio.EditValue);
    FDtFinal := VartoStr(FiltroPeriodoFim.EditValue);

    if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    begin
      SalvarOrdenadacaoContaCorrente;
      TFluxoCaixaResumido.SetContasCorrentes(fdmContaCorrente);
      AjustarCamposDatasetContaCorrente;

      try
        fluxoCaixaResumido := TFluxoCaixaResumido.GetFluxoCaixaResumidoProxy;
        if (fluxoCaixaResumido.FOrdemContaCorrente.IsEmpty) then
        begin
          OrdenacaoInicial;
        end
        else
        begin
          RealizarOrdenacaoSalva(fluxoCaixaResumido.FOrdemContaCorrente);
        end;
      finally
        //FreeAndNil(fluxoCaixaResumido);
      end;

      TcxGridUtils.AdicionarTodosCamposNaView(ViewContaCorrente);
      CarregarPersonalizacaoGrid(ViewContaCorrente);
      ViewContaCorrente.DataController.Groups.FullExpand;
      ConfigurarColunas(ViewContaCorrente);
      fdmContaCorrente.First;
    end
    else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    begin
      AnsiStringAGGContaCorrente := String_Agg(fdmContaCorrente, 'ID_CONTA_CORRENTE');
      if not AnsiStringAGGContaCorrente.IsEmpty then
      begin
        TFluxoCaixaResumido.SetFluxoCaixaResumido(fdmEstruturaFluxoCaixaResumido,
          AnsiStringAGGContaCorrente, FListaPeriodo.CommaText, FTipoPeriodoUtilizado);
        TVerticalGridUtils.AdicionarTodosCamposNaView(ViewEstruturaFluxoCaixaResumido);
        //ConfigurarColunas(ViewEstruturaFluxoCaixaResumido);
        //CarregarPersonalizacaoGrid(ViewEstruturaFluxoCaixaResumido);

        if not fdmEstruturaFluxoCaixaResumido.IsEmpty then
        begin
          SetValorPeriodo(fdmEstruturaFluxoCaixaResumido, AnsiStringAGGContaCorrente);
          ViewEstruturaFluxoCaixaResumido.DataController.Groups.FullExpand;
          fdmEstruturaFluxoCaixaResumido.First;
        end;
      end;
    end
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TRelFluxoCaixaResumido.AjustarCamposDatasetContaCorrente;
var
  fieldContaCorrente: TField;
begin
  fieldContaCorrente := fdmContaCorrente.FindField('ORDENACAO');

  if Assigned(fieldContaCorrente) then
  begin
    fieldContaCorrente.ReadOnly := false;
  end;
end;

procedure TRelFluxoCaixaResumido.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  inherited;
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    ViewContaCorrente.OptionsView.GroupByBox :=
    not ViewContaCorrente.OptionsView.GroupByBox;
{  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    ViewEstruturaFluxoCaixaResumido.OptionsView.GroupByBox :=
    not ViewEstruturaFluxoCaixaResumido.OptionsView.GroupByBox;}
end;

procedure TRelFluxoCaixaResumido.ActExpandirTudoExecute(Sender: TObject);
begin
  inherited;
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    ViewContaCorrente.DataController.Groups.FullExpand
  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    ViewEstruturaFluxoCaixaResumido.DataController.Groups.FullExpand;
end;

procedure TRelFluxoCaixaResumido.ActExportarExcelExecute(Sender: TObject);
begin
  inherited;
  TVerticalGridUtils.ExportarParaExcel(ViewEstruturaFluxoCaixaResumido, Self.Caption)
end;

procedure TRelFluxoCaixaResumido.ActExportarPDFExecute(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1Link1.PDFExportOptions.DefaultFileName := Self.Caption;
  dxComponentPrinter1Link1.PDFExportOptions.OpenDocumentAfterExport := true;
  dxComponentPrinter1Link1.Component := ViewEstruturaFluxoCaixaResumido;
  dxComponentPrinter1Link1.ExportToPDF;
end;

procedure TRelFluxoCaixaResumido.ActImprimirGradeExecute(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1Link1.PDFExportOptions.DefaultFileName := Self.Caption;
  dxComponentPrinter1Link1.PDFExportOptions.OpenDocumentAfterExport := true;
  dxComponentPrinter1Link1.Component := ViewEstruturaFluxoCaixaResumido;
  dxComponentPrinter1Link1.Preview();
end;

procedure TRelFluxoCaixaResumido.ActProximoExecute(Sender: TObject);
begin
  inherited;
  pgFluxoCaixaResumido.SelectNextPage(true);
  SmartOpen(Sender);
end;

procedure TRelFluxoCaixaResumido.ActRecolherAgrupamentoExecute(Sender: TObject);
begin
  inherited;
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    ViewContaCorrente.DataController.Groups.FullCollapse
  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    ViewEstruturaFluxoCaixaResumido.DataController.Groups.FullCollapse;
end;

procedure TRelFluxoCaixaResumido.ActRestaurarColunasPadraoExecute(
  Sender: TObject);
begin
  inherited;
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+'.'+ViewAtivaNaTela.Name);

  ViewAtivaNaTela.RestoreDefaults;
end;

function TRelFluxoCaixaResumido.BuscarOrdenacaoConfiguradaPeloUsuario: String;
var
  ordenacao: String;
begin
  try
    fdmContaCorrente.DisableControls;
    fdmContaCorrente.First;
    while not fdmContaCorrente.Eof do
    begin
      if not ordenacao.IsEmpty then
      begin
        ordenacao := ordenacao + ',';
      end;

      ordenacao := ordenacao + fdmContaCorrente.FieldByName('id_conta_corrente').AsString + '=' +
        fdmContaCorrente.FieldByName('ORDENACAO').AsString;
      fdmContaCorrente.next;
    end;
    result := ordenacao;
  finally
    fdmContaCorrente.EnableControls;
  end;
end;

function TRelFluxoCaixaResumido.ViewAtivaNaTela: TcxGridDBBandedTableView;
begin
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
    result := ViewContaCorrente
{  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    result := ViewEstruturaFluxoCaixaResumido;}
end;

procedure TRelFluxoCaixaResumido.ViewContaCorrenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = VK_UP) then
  begin
    MudarOrdemContaCorrente(ordenarContaCorrenteParaCima);
    Abort;
  end
  else if (ssCtrl in Shift) and (Key = VK_DOWN) then
  begin
    MudarOrdemContaCorrente(ordenarContaCorrenteParaBaixo);
    Abort;
  end;
  inherited;
end;

procedure TRelFluxoCaixaResumido.ViewContaCorrenteNavigatorButtonsButtonClick(Sender: TObject;
  AButtonIndex: Integer; var ADone: Boolean);
begin
  inherited;
  if (AButtonIndex = 16) then
  begin
    MudarOrdemContaCorrente(ordenarContaCorrenteParaCima);
  end
  else if (AButtonIndex = 17) then
  begin
    MudarOrdemContaCorrente(ordenarContaCorrenteParaBaixo);
  end;
end;

procedure TRelFluxoCaixaResumido.CarregarPersonalizacaoGrid(AView: TcxGridDBBandedTableView);
begin
  TUsuarioGridView.LoadGridView(AView, TSistema.Sistema.Usuario.idSeguranca,
    Self.Name+'.'+ViewAtivaNaTela.Name);
end;

procedure TRelFluxoCaixaResumido.ConfigurarColunas(
  AView: TcxGridDBBandedTableView);
var i: Integer;
    field: TField;
begin
  if AView = ViewContaCorrente then
  begin
    for i := Pred(AView.ColumnCount) downto 0 do
    begin
      if AView.Columns[i].DataBinding.FieldName = 'BO_CHECKED' then
      begin
        AView.Columns[i].DataBinding.Field.ReadOnly := false;
        AView.Columns[i].Options.Editing := true;
        AView.Columns[i].Options.Sorting := false;
        AView.Columns[i].OnHeaderClick := MarcarTodos;
      end
      else if AView.Columns[i].DataBinding.FieldName = 'ORDENACAO' then
      begin
        AView.Columns[i].DataBinding.Field.ReadOnly := false;
        AView.Columns[i].Options.Editing := false;
        AView.Columns[i].Options.Sorting := true;
        //AView.Columns[i].Visible := false;
        //AView.Columns[i].VisibleForCustomization := false;
      end
      else
      begin
        AView.Columns[i].Options.Editing := false;
        AView.Columns[i].Options.Sorting := false;
      end;
    end;
  end
  else
  begin
    {for i := Pred(AView.ColumnCount) downto 0 do
    begin
      if AView.Columns[i].DataBinding.FieldName = 'BO_CHECKED' then
      begin
        AView.Columns[i].DataBinding.Field.ReadOnly := false;
        AView.Columns[i].Options.Editing := true;
        AView.Columns[i].Options.Sorting := false;
        AView.Columns[i].OnHeaderClick := MarcarTodos;
      end
      else
      begin
        AView.Columns[i].Options.Editing := true;
        AView.Columns[i].Options.Sorting := true;
      end;
    end;

    for i := 0 to Pred(FListaPeriodo.Count) do
    begin
      field := AView.DataController.DataSource.DataSet.FindField(GetFieldNameDoCampoPeriodo(FListaPeriodo[i]));
      if (field <> nil) then
      begin
        field.displayLabel := GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]);
        TFMTBCDField(field).DisplayFormat := '###,###,###,###,##0.00';
        field.ReadOnly := false;

        with AView.GetColumnByFieldName(field.FieldName) do
        begin
          Width := 245;
          Options.Editing := false;
          Options.Sorting := false;
          Summary.GroupFooterKind := skSum;
          Summary.GroupFooterFormat := '###,###,###,###,##0.00';
        end;
      end;
    end;}
  end;
end;

function TRelFluxoCaixaResumido.CriarCampoMesAno(const AQuery: TFDMemTable;
  AFieldName, ADisplayLabel: AnsiString): TFloatField;
var newField: TFloatField;
begin
  newField := TFloatField.Create(AQuery);
  newField.FieldName := AFieldName;
  newField.DisplayLabel := ADisplayLabel;
  newField.DisplayFormat := '###,###,###,##0.00';
  newField.Name := AQuery.Name + newField.FieldName;
  newField.Index := AQuery.FieldCount;
  newField.DataSet := AQuery;
  newField.FieldKind := fkInternalCalc;
  //AQuery.FieldDefs.UpDate;
  result := newField;
end;

procedure TRelFluxoCaixaResumido.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Data de Competencia Inicio
  FParametroFiltroDataCompetenciaInicio := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_INICIO,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_INICIO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataCompetenciaInicio);

  //Data de Competencia Fim
  FParametroFiltroDataCompetenciaFim := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_FILTRO_DATA_VENCIMENTO_FIM,
    TConstParametroFormulario.DESCRICAO_FILTRO_DATA_VENCIMENTO_FIM,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FParametroFiltroDataCompetenciaFim);
end;

function TRelFluxoCaixaResumido.GetFieldNameDoCampoPeriodo(const AMes: String): String;
begin
{  if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_MENSAL) then
  begin
    result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes)+1, 4);
  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_DIARIO) then
  begin}
    result := StringReplace(AMes, '/', '', [rfReplaceAll]);
{  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_SEMANAL) then
  begin
    result := StringReplace(AMes, '/', '', [rfReplaceAll]);
  end;}
end;

function TRelFluxoCaixaResumido.GetDisplayLabelDoCampoPeriodo(const AMes: String): String;
begin
{  if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_MENSAL) then
  begin
    result := Copy(AMes, 1, 3)+Copy(AMes, POS('/',AMes), 5);
  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_DIARIO) then
  begin}
  result := Copy(AMes, 1, 2)+'/'+Copy(AMes, 3, 2)+'/'+Copy(AMes, 5, 4);
{  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_SEMANAL) then
  begin
    result := Copy(AMes, 1, 2)+'/'+Copy(AMes, 3, 2)+'/'+Copy(AMes, 5, 4);
  end;}
end;

procedure TRelFluxoCaixaResumido.DeletarCamposPersonalizados(const AQueryNivel: array of TFDMemTable; const AView: array of TcxGridDBBandedTableView);
var i,c,q: integer;
    fieldName: String;
    field : TField;
begin
  if Assigned(FListaPeriodo) then
  try
    for i := 0 to Pred(FListaPeriodo.Count) do
    begin
      for q := 0 to Pred(Length(AQueryNivel)) do
      begin
        fieldName := GetFieldNameDoCampoPeriodo(FListaPeriodo[i]);

        for c := Pred(AView[q].ColumnCount) downto 0 do
          if AView[q].Columns[c].DataBinding.FieldName = fieldName then
          begin
            AView[q].BeginUpdate;
            AView[q].Columns[c].Destroy;
            AView[q].EndUpdate;
          end;
      end;
    end;
  except
  end;
end;

procedure TRelFluxoCaixaResumido.FiltroTipoPeriodoPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  FTipoPeriodoUtilizado :=
    TcxRadioGroup(Sender).Properties.Items[TcxRadioGroup(Sender).ItemIndex].Value;
end;

procedure TRelFluxoCaixaResumido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
{  DeletarCamposPersonalizados(
    [fdmEstruturaFluxoCaixaResumido]
  ,
    [ViewEstruturaFluxoCaixaResumido]
  );                                 }

  SalvarPersonalizacaoGrid;
  inherited;
end;

procedure TRelFluxoCaixaResumido.FormCreate(Sender: TObject);
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Construindo plano de contas');
    TPlanoConta.SetIDPlanoContaAssociado;
    pgFluxoCaixaResumido.Properties.HideTabs := true;
    pgFluxoCaixaResumido.ActivePage := tsContaCorrente;
    PrepararFiltros;
  finally
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TRelFluxoCaixaResumido.SalvarOrdenadacaoContaCorrente;
var
  fluxoCaixaResumido: TFluxoCaixaResumidoProxy;
begin
  if not fdmContaCorrente.Active then
  begin
    exit;
  end;

  fluxoCaixaResumido := TFluxoCaixaResumidoProxy.Create;
  fluxoCaixaResumido.FOrdemContaCorrente := BuscarOrdenacaoConfiguradaPeloUsuario;
  TFluxoCaixaResumido.GerarFluxoCaixaResumido(fluxoCaixaResumido);
end;

procedure TRelFluxoCaixaResumido.SalvarPersonalizacaoGrid;
begin
  if fdmContaCorrente.Active then
    TUsuarioGridView.SaveGridView(ViewContaCorrente,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewContaCorrente.Name);

  SalvarOrdenadacaoContaCorrente;

{  if fdmEstruturaFluxoCaixaResumido.Active then
    TUsuarioGridView.SaveGridView(ViewEstruturaFluxoCaixaResumido,
    TSistema.Sistema.usuario.idSeguranca,
    Self.Name+'.'+ViewEstruturaFluxoCaixaResumido.Name);}
end;

procedure TRelFluxoCaixaResumido.SetListaPeriodo;
begin
{  if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_MENSAL) then
  begin
    SetListaPeriodoMensal;
  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_DIARIO) then
  begin}
  SetListaPeriodoDiario;
{  end
  else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_SEMANAL) then
  begin
    SetListaPeriodoSemanal;
  end;}
end;

procedure TRelFluxoCaixaResumido.SetListaPeriodoMensal;
var listaPeriodo: TStringList;
    mesInicial, anoInicial, mesFinal, anoFinal: Integer;
begin
  if not Assigned(FListaPeriodo) then
    FListaPeriodo := TStringList.Create
  else
    FListaPeriodo.Clear;

  listaPeriodo := TStringList.Create;
  try
    mesInicial := MonthOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    anoInicial := YearOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    mesFinal := MonthOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));
    anoFinal := YearOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));

    repeat
      listaPeriodo.Add(TDateUtils.NumeroMesParaDescricaoMes(mesInicial)+'/'+IntToStr(anoInicial));

      if (mesInicial = 12) then
      begin
        mesInicial := 01;
        inc(anoInicial);
      end
      else
        inc(mesInicial);

    until (((mesInicial > mesFinal) or ((mesInicial = 1) and (anoInicial > anoFinal))) and (anoInicial >= anoFinal));

    FListaPeriodo.Text := listaPeriodo.Text;
  finally
    listaPeriodo.Free;
  end;
end;

procedure TRelFluxoCaixaResumido.SetListaPeriodoSemanal;
var listaPeriodo: TStringList;
    mesInicial, anoInicial, mesFinal, anoFinal: Integer;
begin
{  if not Assigned(FListaPeriodo) then
    FListaPeriodo := TStringList.Create
  else
    FListaPeriodo.Clear;

  listaPeriodo := TStringList.Create;
  try
    mesInicial := MonthOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    anoInicial := YearOf(StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue)));
    mesFinal := MonthOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));
    anoFinal := YearOf(StrtoDate(VartoStr(FiltroPeriodoFim.EditValue)));

    repeat
      listaPeriodo.Add(TDateUtils.NumeroMesParaDescricaoMes(mesInicial)+'/'+IntToStr(anoInicial));

      if (mesInicial = 12) then
      begin
        mesInicial := 01;
        inc(anoInicial);
      end
      else
        inc(mesInicial);

    until (((mesInicial > mesFinal) or ((mesInicial = 1) and (anoInicial > anoFinal))) and (anoInicial >= anoFinal));

    FListaPeriodo.Text := listaPeriodo.Text;
  finally
    listaPeriodo.Free;
  end;        }
end;

procedure TRelFluxoCaixaResumido.SetListaPeriodoDiario;
var listaPeriodo: TStringList;
    diaInicial, diaFinal, diaRelatorio: TDate;
begin
  if not Assigned(FListaPeriodo) then
    FListaPeriodo := TStringList.Create
  else
    FListaPeriodo.Clear;

  listaPeriodo := TStringList.Create;
  try
    diaInicial := StrtoDate(VartoStr(FiltroPeriodoInicio.EditValue));
    diaFinal := StrtoDate(VartoStr(FiltroPeriodoFim.EditValue));
    diaRelatorio := diaInicial;
    while diaRelatorio <= diaFinal do
    begin
      listaPeriodo.Add(StringReplace(DatetoStr(diaRelatorio), '/', '', [rfReplaceAll]));
      diaRelatorio := IncDay(diaRelatorio);
    end;
    FListaPeriodo.Text := listaPeriodo.Text;
  finally
    listaPeriodo.Free;
  end;
end;

procedure TRelFluxoCaixaResumido.PrepararFiltros;
begin
  FiltroPeriodoInicio.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataCompetenciaInicio.AsString);
  FiltroPeriodoFim.EditValue :=
    TConstParametroFormulario.GetParametroData(FParametroFiltroDataCompetenciaFim.AsString);

  FiltroTipoPeriodo.EditValue := FILTRO_TIPO_PERIODO_DIARIO;
  FTipoPeriodoUtilizado := FILTRO_TIPO_PERIODO_DIARIO;
end;

procedure TRelFluxoCaixaResumido.RealizarOrdenacaoSalva(const AOrdenacao: String);
var
  ordenacao: TStringList;
  ordem: Integer;
  idCOntaCorrente: Integer;
  i: Integer;
  posicaoSeparador: Integer;
begin
  ordenacao := TStringList.Create;
  try
    ordenacao.CommaText := AOrdenacao;
    fdmContaCorrente.DisableControls;

    for i := 0 to ordenacao.Count -1 do
    begin
      posicaoSeparador := Pos('=', ordenacao[i]);
      idCOntaCorrente := StrtoIntDef(Copy(ordenacao[i], 1, posicaoSeparador-1), 0);
      ordem := StrtoIntDef(Copy(ordenacao[i], posicaoSeparador+1, Length(ordenacao[i])), 0);

      if fdmContaCorrente.Locate('id_conta_corrente', idCOntaCorrente, []) then
      begin
        fdmContaCorrente.Edit;
        fdmContaCorrente.FieldByName('ORDENACAO').AsInteger := ordem;
        fdmContaCorrente.Post;
      end;
    end;
  finally
    FreeAndNil(ordenacao);
    ViewContaCorrente.DataController.GotoFirst;
    fdmContaCorrente.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.SmartOpen(Sender: TObject);
var query: TFDMemTable;
begin
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
  begin
    ActAtualizar.Execute;

    query := fdmContaCorrente;
  end
  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
  begin
    ActAtualizar.Execute;

    query := fdmEstruturaFluxoCaixaResumido;
  end;

  if Assigned(query) and query.IsEmpty then
  begin
    if Sender = ActProximo then
      ActProximo.Execute
    else
      ActAnterior.Execute;
  end;
end;

procedure TRelFluxoCaixaResumido.SetValorPeriodo(const AQuery: TFDMemTable; const AStringAGGContaCorrente: String);
var i: integer;
    diaInicial, diaFinal, mes, ano: Integer;
    field: TFloatField;
    fieldColumn: String;
    stringAGGCentroResultado: String;
    whereIDPlanoConta, whereFieldIDPlanoConta: String;
    sumPeriodo, sumCR, sumCP: Double;
    fdmConsulta: TFDMemTable;
begin
  AQuery.DisableControls;
  try
    AQuery.First;
    for i := 0 to Pred(FListaPeriodo.Count) do
    begin
      fieldColumn := GetFieldNameDoCampoPeriodo(FListaPeriodo[i]);
      sumPeriodo := 0;
      sumCR := 0;
      sumCP := 0;
      AQuery.First;
      while not AQuery.Eof do
      begin
        AQuery.Edit;

        if not AQuery.FieldByName('ID_CONTA_CORRENTE').AsString.IsEmpty then
        try
          fdmConsulta := TFDMemTable.Create(nil);
          fdmConsulta.FetchOptions.RecordCountMode := cmTotal;

          TFluxoCaixaResumido.SetValores(fdmConsulta, AQuery.FieldByName('ID_CONTA_CORRENTE').AsString,
            GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]), GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]),
            FTipoPeriodoUtilizado);

  {          if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_MENSAL) then
          begin
            mes := TDateUtils.NumeroMes(Copy(FListaPeriodo[i],1,3));
            ano := StrtoInt(Copy(FListaPeriodo[i], Pos('/',FListaPeriodo[i])+1, 4));
            fieldColumn := GetFieldNameDoCampoPeriodo(FListaPeriodo[i]);

            if fdmConsulta.locate('id_conta_corrente;periodo',
              VarArrayOf([AQuery.FieldByName('ID_CONTA_CORRENTE').AsInteger, InttoStr(mes)+'/'+InttoStr(ano)]),[]) then
            begin
              AQuery.FieldByName(fieldColumn).AsFloat :=
                fdmConsulta.FieldByName('vl_total').AsFloat
            end
            else
              AQuery.FieldByName(fieldColumn).AsFloat := 0;
          end
          else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_DIARIO) then
          begin}

          AQuery.FieldByName(fieldColumn).AsFloat :=
            fdmConsulta.FieldByName('vl_total').AsFloat;
  {          end
          else if FTipoPeriodoUtilizado.Equals(FILTRO_TIPO_PERIODO_SEMANAL) then
          begin
            SetListaPeriodoSemanal;
          end;}
          sumPeriodo := sumPeriodo + AQuery.FieldByName(fieldColumn).AsFloat;
        finally
          FreeAndNil(fdmConsulta);
        end;

        if AQuery.FieldByName('descricao').AsString = 'TOTAL CONTAS CORRENTES' then
        begin
          AQuery.FieldByName(fieldColumn).AsFloat := sumPeriodo;
        end
        else
        if AQuery.FieldByName('descricao').AsString = 'CONTAS A RECEBER' then
        begin
          sumCR := TFluxoCaixaResumido.GetTotalContaReceber(GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]),
            GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]));

          AQuery.FieldByName(fieldColumn).AsFloat := sumCR;
        end
        else
        if AQuery.FieldByName('descricao').AsString = 'CONTAS A PAGAR' then
        begin
          sumCP := TFluxoCaixaResumido.GetTotalContaPagar(GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]),
            GetDisplayLabelDoCampoPeriodo(FListaPeriodo[i]));

          AQuery.FieldByName(fieldColumn).AsFloat := sumCP;
        end
        else
        if AQuery.FieldByName('descricao').AsString = 'TOTAL GERAL' then
        begin
          AQuery.FieldByName(fieldColumn).AsFloat := sumPeriodo + sumCR - sumCP;
        end;

        AQuery.Post;
        AQuery.Next;
      end;
    end;
  finally
    AQuery.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.MarcarTodos(Sender: TObject);
var query: TFDMemTable;
    value: String;
begin
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
  begin
    query := fdmContaCorrente;
  end;

  if (query <> nil) and (query.Active) then
  try
    query.DisableControls;
    query.First;
    value := TFunction.IIF(query.FieldByName('BO_CHECKED').AsString = 'S', 'N', 'S');
    while not query.Eof do
    begin
      query.Edit;
      query.FieldByName('BO_CHECKED').AsString := value;
      query.Post;
      query.Next;
    end;
  finally
    query.First;
    query.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.MudarOrdemContaCorrente(const AOrdem: TOrdenacaoContaCorrente);
var
  posicao: Integer;
  recno: Integer;
begin
  try
    fdmContaCorrente.DisableControls;
    fdmContaCorrente.GuardarBookmark;

    case AOrdem of
      ordenarContaCorrenteParaCima:
      begin
        recno := ViewContaCorrente.DataController.RecNo;

        if fdmContaCorrente.FieldByName('ORDENACAO').AsInteger = 1 then
        begin
          exit;
        end;

        ViewContaCorrente.DataController.GotoPrev;
        fdmContaCorrente.Recno := ViewContaCorrente.DataController.RecNo;
        fdmContaCorrente.Edit;
        fdmContaCorrente.FieldByName('ORDENACAO').AsInteger :=
          fdmContaCorrente.FieldByName('ORDENACAO').AsInteger + 1;
        fdmContaCorrente.Post;

        fdmContaCorrente.Recno := recno;
        fdmContaCorrente.Edit;
        fdmContaCorrente.FieldByName('ORDENACAO').AsInteger :=
          fdmContaCorrente.FieldByName('ORDENACAO').AsInteger - 1;
        fdmContaCorrente.Post;
      end;

      ordenarContaCorrenteParaBaixo:
      begin
        recno := ViewContaCorrente.DataController.RecNo;

        if fdmContaCorrente.FieldByName('ORDENACAO').AsInteger = fdmContaCorrente.RecordCount then
        begin
          exit;
        end;

        ViewContaCorrente.DataController.GotoNext;
        fdmContaCorrente.Recno := ViewContaCorrente.DataController.RecNo;
        fdmContaCorrente.Edit;
        fdmContaCorrente.FieldByName('ORDENACAO').AsInteger :=
          fdmContaCorrente.FieldByName('ORDENACAO').AsInteger - 1;
        fdmContaCorrente.Post;

        fdmContaCorrente.Recno := recno;
        fdmContaCorrente.Edit;
        fdmContaCorrente.FieldByName('ORDENACAO').AsInteger :=
          fdmContaCorrente.FieldByName('ORDENACAO').AsInteger + 1;
        fdmContaCorrente.Post;
      end;
    end;
  finally
    fdmContaCorrente.PosicionarBookmark;
    fdmContaCorrente.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.OrdenacaoInicial;
begin
  try
    fdmContaCorrente.DisableControls;
    fdmContaCorrente.First;
    while not fdmContaCorrente.Eof do
    begin
      fdmContaCorrente.Edit;
      fdmContaCorrente.FieldByName('ORDENACAO').AsInteger := fdmContaCorrente.Recno;
      fdmContaCorrente.Post;
      fdmContaCorrente.next;
    end;
  finally
    fdmContaCorrente.First;
    fdmContaCorrente.EnableControls;
  end;
end;

procedure TRelFluxoCaixaResumido.pmTituloGridPesquisaPadraoPopup(
  Sender: TObject);
var
  view: TcxGridDBBandedTableView;
begin
  inherited;
  if pgFluxoCaixaResumido.ActivePage = tsContaCorrente then
  begin
    view := ViewContaCorrente;
    ActExibirAgrupamento.Caption := TFunction.Iif(view.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');

    ActExibirAgrupamento.Visible := true;
    ActAlterarColunasGrid.Visible := true;
    ActRestaurarColunasPadrao.Visible := true;
    ActExpandirTudo.Visible := true;
    ActRecolherAgrupamento.Visible := true;
    ActExportarPDF.Visible := false;
    ActExportarExcel.Visible := false;
    ActImprimirGrade.Visible := false;
  end
  else
  begin
    ActExibirAgrupamento.Visible := false;
    ActAlterarColunasGrid.Visible := false;
    ActRestaurarColunasPadrao.Visible := false;
    ActExpandirTudo.Visible := false;
    ActRecolherAgrupamento.Visible := false;
    ActExportarExcel.Visible := true;
    ActExportarPDF.Visible := false;
    ActImprimirGrade.Visible := false;
  end;
{  else if pgFluxoCaixaResumido.ActivePage = tsEstruturaFluxoCaixaResumido then
    view := ViewEstruturaFluxoCaixaResumido;}
end;

procedure TRelFluxoCaixaResumido.ValidarPreenchimentoDoPeriodo;
begin
  if (VartoStr(FiltroPeriodoInicio.EditValue).IsEmpty) or
     (VartoStr(FiltroPeriodoFim.EditValue).IsEmpty) then
  begin
    TFrmMessage.Information('Informe o per�odo desejado!');
    Abort;
  end;

  if (FDtInicial <> VartoStr(FiltroPeriodoInicio.EditValue)) or
     (FDtFinal <> VartoStr(FiltroPeriodoFim.EditValue)) or
     (FTipoPeriodoUtilizado <> VartoStr(FiltroTipoPeriodo.EditValue)) then
  begin
    fdmContaCorrente.Close;
    fdmEstruturaFluxoCaixaResumido.Close;
    pgFluxoCaixaResumido.ActivePage := tsContaCorrente;

    SetListaPeriodo;
  end;
end;

Initialization
  RegisterClass(TRelFluxoCaixaResumido);

Finalization
  UnRegisterClass(TRelFluxoCaixaResumido);

end.


