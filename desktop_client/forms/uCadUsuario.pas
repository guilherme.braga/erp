unit uCadUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  cxCheckBox, cxDBEdit, uGBDBCheckBox, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls;

type
  TCadUsuario = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label2: TLabel;
    edNome: TgbDBTextEdit;
    gbDBCheckBox7: TgbDBCheckBox;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataID: TAutoIncField;
    cdsDataUSUARIO: TStringField;
    cdsDataSENHA: TStringField;
    cdsDataDT_EXPIRACAO: TDateField;
    cdsDataUSUARIO_EXPIRADO: TIntegerField;
    cdsDataUSUARIO_DIAS_EXPIRAR_SENHA: TIntegerField;
    cdsDataEMAIL: TStringField;
    cdsDataPRIVILEGIADO: TIntegerField;
    cdsDataSENHA_KEY: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataID_USUARIO_PERFIL: TIntegerField;
    cdsDataUSUARIO_TIPO: TStringField;
    cdsDataESTACAO_ID: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadUsuario);

finalization
  UnRegisterClass(TCadUsuario);


end.
