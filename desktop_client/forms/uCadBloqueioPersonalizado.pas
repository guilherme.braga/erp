unit uCadBloqueioPersonalizado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus,
  UCBase, dxBar, System.Actions, Vcl.ActnList, dxBevel, Vcl.ExtCtrls, JvDesignSurface, cxSplitter,
  JvExControls, JvButton, JvTransparentButton, cxGroupBox, uGBPanel, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC,
  dxRibbon, uGBDBTextEditPK, cxMaskEdit, cxDropDownEdit, cxBlobEdit, cxDBEdit, uGBDBBlobEdit, cxCheckBox,
  uGBDBCheckBox, cxTextEdit, uGBDBTextEdit, Vcl.StdCtrls, cxMemo, uGBDBMemo,
  uFrameBloqueioPersonalizadoUsuarioAutorizado, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TCadBloqueioPersonalizado = class(TFrmCadastro_Padrao)
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataSQL: TBlobField;
    cdsDataBO_VENDA: TStringField;
    cdsDataBO_NOTA_FISCAL: TStringField;
    cdsDataBO_ORDEM_SERVICO: TStringField;
    cdsDataBO_ATIVO: TStringField;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label4: TLabel;
    PnFrameBloqueioPersonalizadoUsuarioLiberacao: TgbPanel;
    gbPanel2: TgbPanel;
    gbPanel3: TgbPanel;
    Label2: TLabel;
    gbDBMemo1: TgbDBMemo;
    Label1: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBCheckBox2: TgbDBCheckBox;
    gbDBCheckBox3: TgbDBCheckBox;
    gbDBCheckBox4: TgbDBCheckBox;
    Label3: TLabel;
    cdsBloqueioPersonalizadoUsuarioLiberacao: TGBClientDataSet;
    cdsBloqueioPersonalizadoUsuarioLiberacaoID: TAutoIncField;
    cdsBloqueioPersonalizadoUsuarioLiberacaoID_PESSOA_USUARIO: TIntegerField;
    cdsBloqueioPersonalizadoUsuarioLiberacaoID_BLOQUEIO_PERSONALIZADO: TIntegerField;
    cdsBloqueioPersonalizadoUsuarioLiberacaoJOIN_NOME_PESSOA_USUARIO: TStringField;
    cdsDatafdqBloqueioPersonalizadoLib: TDataSetField;
  private
    FFrameBloqueioPersonalizadoUsuarioAutorizado: TFrameBloqueioPersonalizadoUsuarioAutorizado;
    procedure CriarFrameUsuariosAutorizados;
  public
    { Public declarations }
  protected
    procedure InstanciarFrames; override;
  end;

implementation

{$R *.dfm}

uses uDmConnection;

{ TCadBloqueioPersonalizado }

procedure TCadBloqueioPersonalizado.CriarFrameUsuariosAutorizados;
begin
  FFrameBloqueioPersonalizadoUsuarioAutorizado := TFrameBloqueioPersonalizadoUsuarioAutorizado.Create(
    PnFrameBloqueioPersonalizadoUsuarioLiberacao);
  FFrameBloqueioPersonalizadoUsuarioAutorizado.Parent := PnFrameBloqueioPersonalizadoUsuarioLiberacao;
  FFrameBloqueioPersonalizadoUsuarioAutorizado.Align := alClient;

  FFrameBloqueioPersonalizadoUsuarioAutorizado.SetConfiguracoesIniciais(
    cdsBloqueioPersonalizadoUsuarioLiberacao);
end;

procedure TCadBloqueioPersonalizado.InstanciarFrames;
begin
  inherited;
  CriarFrameUsuariosAutorizados;
end;

Initialization
  RegisterClass(TCadBloqueioPersonalizado);

Finalization
  UnRegisterClass(TCadBloqueioPersonalizado);

end.
