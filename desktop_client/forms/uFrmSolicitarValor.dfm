inherited FrmSolicitarValor: TFrmSolicitarValor
  Caption = 'Informe o valor'
  ClientHeight = 140
  ClientWidth = 279
  OnShow = FormShow
  ExplicitWidth = 285
  ExplicitHeight = 169
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 279
    ExplicitHeight = 84
    Height = 84
    Width = 279
    object gbPanel1: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Height = 72
      Width = 275
      object lbDigiteValor: TcxLabel
        Left = 2
        Top = 2
        Align = alTop
        Caption = 'Digite o valor'
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object EdtProblemaFoco: TEdit
        Left = 384
        Top = 24
        Width = 1
        Height = 21
        TabOrder = 1
        Text = 'EdtProblemaFoco'
      end
      object EdtValor: TcxCalcEdit
        Left = 2
        Top = 22
        Align = alClient
        EditValue = 0.000000000000000000
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -21
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 271
      end
    end
    object EdtFoco: TcxCalcEdit
      Left = 2
      Top = 74
      EditValue = 0.000000000000000000
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 1
      Width = 0
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 84
    ExplicitTop = 84
    ExplicitWidth = 279
    Width = 279
    inherited BtnConfirmar: TcxButton
      Left = 107
      Width = 85
      ExplicitLeft = 105
      ExplicitWidth = 85
    end
    inherited BtnCancelar: TcxButton
      Left = 192
      Width = 85
      ExplicitLeft = 191
      ExplicitWidth = 85
    end
  end
  inherited ActionList: TActionList
    Left = 142
    Top = 6
    inherited ActConfirmar: TAction
      Caption = 'Confirmar Enter'
      ShortCut = 13
    end
  end
end
