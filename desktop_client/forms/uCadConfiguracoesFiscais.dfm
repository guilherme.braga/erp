inherited CadConfiguracoesFiscais: TCadConfiguracoesFiscais
  Caption = 'Configura'#231#245'es Fiscais'
  ExplicitWidth = 952
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Properties.ActivePage = cxtsData
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          inherited dxBevel1: TdxBevel
            Visible = False
          end
          object cxDBVerticalGrid1: TcxDBVerticalGrid
            Left = 2
            Top = 34
            Width = 932
            Height = 297
            Align = alClient
            OptionsView.RowHeaderWidth = 238
            Navigator.Buttons.CustomButtons = <>
            TabOrder = 0
            DataController.DataSource = dsData
            Version = 1
            object categoriaCertificadoDigital: TcxCategoryRow
              Properties.Caption = 'Certificado Digital'
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1CERTIFICADO_NUMERO_SERIE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'CERTIFICADO_NUMERO_SERIE'
              ID = 1
              ParentID = 0
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1CERTIFICADO_SENHA: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'CERTIFICADO_SENHA'
              ID = 2
              ParentID = 0
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_ID_CSC: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_ID_CSC'
              ID = 3
              ParentID = 0
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_CSC: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_CSC'
              ID = 4
              ParentID = 0
              Index = 3
              Version = 1
            end
            object categoriaDiretorioArquivo: TcxCategoryRow
              Properties.Caption = 'Diret'#243'rios/Arquivos'
              ID = 5
              ParentID = -1
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_RELATORIO_NFCE_FR: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_RELATORIO_NFCE_FR'
              ID = 6
              ParentID = 5
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_EVENTO_NFCE_FR: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_EVENTO_NFCE_FR'
              ID = 7
              ParentID = 5
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_RELATORIO_NFE_FR: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_RELATORIO_NFE_FR'
              ID = 8
              ParentID = 5
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_EVENTO_NFE_FR: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_EVENTO_NFE_FR'
              ID = 9
              ParentID = 5
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_NFE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_NFE'
              ID = 10
              ParentID = 5
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_LOGS: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_LOGS'
              ID = 11
              ParentID = 5
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_SCHEMAS: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_SCHEMAS'
              ID = 12
              ParentID = 5
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_CANCELAMENTO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_CANCELAMENTO'
              ID = 13
              ParentID = 5
              Index = 7
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_INUTILIZACAO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_INUTILIZACAO'
              ID = 14
              ParentID = 5
              Index = 8
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_DPEC: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_DPEC'
              ID = 15
              ParentID = 5
              Index = 9
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_EVENTO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_EVENTO'
              ID = 16
              ParentID = 5
              Index = 10
              Version = 1
            end
            object cxDBVerticalGrid1ARQUIVO_PATH_PDF: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'ARQUIVO_PATH_PDF'
              ID = 17
              ParentID = 5
              Index = 11
              Version = 1
            end
            object categoriaEmail: TcxCategoryRow
              Properties.Caption = 'Email'
              ID = 18
              ParentID = -1
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_ENVIAR_NFE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_ENVIAR_NFE'
              ID = 19
              ParentID = 18
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_ENVIAR_NFCE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_ENVIAR_NFCE'
              ID = 20
              ParentID = 18
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_EMAIL_HOST: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_EMAIL_HOST'
              ID = 21
              ParentID = 18
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_PORTA: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_PORTA'
              ID = 22
              ParentID = 18
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_USUARIO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_USUARIO'
              ID = 23
              ParentID = 18
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_SENHA: TcxDBEditorRow
              Properties.EditPropertiesClassName = 'TcxMaskEditProperties'
              Properties.EditProperties.EchoMode = eemPassword
              Properties.EditProperties.PasswordChar = '*'
              Properties.DataBinding.FieldName = 'EMAIL_SENHA'
              ID = 24
              ParentID = 18
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_SMTP: TcxDBEditorRow
              Height = 17
              Properties.DataBinding.FieldName = 'EMAIL_SMTP'
              ID = 25
              ParentID = 18
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_NOME_ORIGEM: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_NOME_ORIGEM'
              ID = 26
              ParentID = 18
              Index = 7
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_ASSUNTO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_ASSUNTO'
              ID = 27
              ParentID = 18
              Index = 8
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_MENSAGEM: TcxDBEditorRow
              Properties.EditPropertiesClassName = 'TcxBlobEditProperties'
              Properties.EditProperties.BlobEditKind = bekMemo
              Properties.DataBinding.FieldName = 'EMAIL_MENSAGEM'
              ID = 28
              ParentID = 18
              Index = 9
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_EMAIL_CC: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_EMAIL_CC'
              ID = 29
              ParentID = 18
              Index = 10
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_EMAIL_CCO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_EMAIL_CCO'
              ID = 30
              ParentID = 18
              Index = 11
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_SOLICITA_CONFIRMACAO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_SOLICITA_CONFIRMACAO'
              ID = 31
              ParentID = 18
              Index = 12
              Version = 1
            end
            object cxDBVerticalGrid1EMAIL_SSL: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'EMAIL_SSL'
              ID = 32
              ParentID = 18
              Index = 13
              Version = 1
            end
            object categoriaGeral: TcxCategoryRow
              Properties.Caption = 'Geral'
              ID = 33
              ParentID = -1
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid1LOGOMARCA: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'LOGOMARCA'
              ID = 34
              ParentID = 33
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1SALVAR_PDF_JUNTO_XML: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'SALVAR_PDF_JUNTO_XML'
              ID = 35
              ParentID = 33
              Index = 1
              Version = 1
            end
            object categoriaImpressora: TcxCategoryRow
              Properties.Caption = 'Impressoras'
              ID = 36
              ParentID = -1
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid1IMPRESSORA_NFE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IMPRESSORA_NFE'
              ID = 37
              ParentID = 36
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1IMPRESSORA_NFCE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IMPRESSORA_NFCE'
              ID = 38
              ParentID = 36
              Index = 1
              Version = 1
            end
            object categoriaNFCE: TcxCategoryRow
              Properties.Caption = 'NFCE'
              ID = 39
              ParentID = -1
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_WEBSERVICE_AMBIENTE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_WEBSERVICE_AMBIENTE'
              ID = 40
              ParentID = 39
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_PREVIEW: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_PREVIEW'
              ID = 41
              ParentID = 39
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_WEBSERVICE_UF: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_WEBSERVICE_UF'
              ID = 42
              ParentID = 39
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1NFCE_MODO_IMPRESSAO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFCE_MODO_IMPRESSAO'
              ID = 43
              ParentID = 39
              Index = 3
              Version = 1
            end
            object categoriaNFE: TcxCategoryRow
              Properties.Caption = 'NFE'
              ID = 44
              ParentID = -1
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid1NFE_WEBSERVICE_AMBIENTE: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFE_WEBSERVICE_AMBIENTE'
              ID = 45
              ParentID = 44
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1NFE_PREVIEW: TcxDBEditorRow
              Height = 17
              Properties.DataBinding.FieldName = 'NFE_PREVIEW'
              ID = 46
              ParentID = 44
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1NFE_WEBSERVICE_UF: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFE_WEBSERVICE_UF'
              ID = 47
              ParentID = 44
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1NFE_MODO_IMPRESSAO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFE_MODO_IMPRESSAO'
              ID = 48
              ParentID = 44
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid1NFE_FORMA_EMISSAO: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NFE_FORMA_EMISSAO'
              ID = 49
              ParentID = 44
              Index = 4
              Version = 1
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 165
      FloatClientHeight = 280
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end
        item
          Visible = True
          ItemName = 'lbDuplicarRegistro'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 420
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 613
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 784
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 538
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 692
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object dxBarLargeButton4: TdxBarLargeButton [16]
      Action = ActValidarStatusReceita
      Category = 1
    end
  end
  inherited ActionListMain: TActionList
    object ActValidarStatusReceita: TAction
      Category = 'Manager'
      Caption = 'Verificar Comunica'#231#227'o Sefaz'
      ImageIndex = 157
      OnExecute = ActValidarStatusReceitaExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspConfiguracoesFiscais'
    RemoteServer = DmConnection.dspConfiguracaoFiscal
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataBO_ATIVO: TStringField
      DefaultExpression = 'S'
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsDataLOGOMARCA: TBlobField
      DisplayLabel = 'Logomarca'
      FieldName = 'LOGOMARCA'
      Origin = 'LOGOMARCA'
    end
    object cdsDataCERTIFICADO_CAMINHO: TStringField
      DisplayLabel = 'Caminho do Certificado'
      FieldName = 'CERTIFICADO_CAMINHO'
      Origin = 'CERTIFICADO_CAMINHO'
      Size = 255
    end
    object cdsDataCERTIFICADO_SENHA: TStringField
      DisplayLabel = 'Senha do Certificado'
      FieldName = 'CERTIFICADO_SENHA'
      Origin = 'CERTIFICADO_SENHA'
      Size = 255
    end
    object cdsDataCERTIFICADO_NUMERO_SERIE: TStringField
      DisplayLabel = 'N'#250'mero de S'#233'rie do Certificado'
      FieldName = 'CERTIFICADO_NUMERO_SERIE'
      Origin = 'CERTIFICADO_NUMERO_SERIE'
      Size = 255
    end
    object cdsDataNFE_MODO_IMPRESSAO: TStringField
      DefaultExpression = 'RETRATO'
      DisplayLabel = 'Modo de Impress'#227'o NFE'
      FieldName = 'NFE_MODO_IMPRESSAO'
      Origin = 'NFE_MODO_IMPRESSAO'
      Required = True
      Size = 15
    end
    object cdsDataNFE_FORMA_EMISSAO: TStringField
      DefaultExpression = 'NORMAL'
      DisplayLabel = 'Forma de Emiss'#227'o NFE'
      FieldName = 'NFE_FORMA_EMISSAO'
      Origin = 'NFE_FORMA_EMISSAO'
      Required = True
      Size = 15
    end
    object cdsDataNFE_WEBSERVICE_UF: TStringField
      DefaultExpression = 'PR'
      DisplayLabel = 'UF NFE'
      FieldName = 'NFE_WEBSERVICE_UF'
      Origin = 'NFE_WEBSERVICE_UF'
      Required = True
      Size = 2
    end
    object cdsDataNFE_WEBSERVICE_AMBIENTE: TStringField
      DefaultExpression = 'HOMOLOGACAO'
      DisplayLabel = 'Ambiente NFE'
      FieldName = 'NFE_WEBSERVICE_AMBIENTE'
      Origin = 'NFE_WEBSERVICE_AMBIENTE'
      Required = True
      Size = 15
    end
    object cdsDataNFE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField
      DisplayLabel = 'Visualizar Mensagem NFE'
      FieldName = 'NFE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      Origin = 'NFE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      FixedChar = True
      Size = 1
    end
    object cdsDataNFE_PROXY_HOST: TStringField
      DisplayLabel = 'Host Proxy NFE'
      FieldName = 'NFE_PROXY_HOST'
      Origin = 'NFE_PROXY_HOST'
      Size = 255
    end
    object cdsDataNFE_PROXY_PORTA: TIntegerField
      DisplayLabel = 'Porta Proxy NFE'
      FieldName = 'NFE_PROXY_PORTA'
      Origin = 'NFE_PROXY_PORTA'
    end
    object cdsDataNFE_PROXY_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio Proxy NFE'
      FieldName = 'NFE_PROXY_USUARIO'
      Origin = 'NFE_PROXY_USUARIO'
      Size = 255
    end
    object cdsDataNFE_PROXY_SENHA: TStringField
      DisplayLabel = 'Senha Proxy NFE'
      FieldName = 'NFE_PROXY_SENHA'
      Origin = 'NFE_PROXY_SENHA'
      Size = 255
    end
    object cdsDataEMAIL_SMTP: TStringField
      DisplayLabel = 'Email - Email do Remetente'
      FieldName = 'EMAIL_SMTP'
      Origin = 'EMAIL_SMTP'
      Size = 255
    end
    object cdsDataEMAIL_PORTA: TStringField
      DisplayLabel = 'Email - Porta SMTP'
      FieldName = 'EMAIL_PORTA'
      Origin = 'EMAIL_PORTA'
      Size = 255
    end
    object cdsDataEMAIL_USUARIO: TStringField
      DisplayLabel = 'Email - Email de Autentica'#231#227'o'
      FieldName = 'EMAIL_USUARIO'
      Origin = 'EMAIL_USUARIO'
      Size = 255
    end
    object cdsDataEMAIL_SENHA: TStringField
      DisplayLabel = 'Email - Senha de Autentica'#231#227'o'
      FieldName = 'EMAIL_SENHA'
      Origin = 'EMAIL_SENHA'
      Size = 255
    end
    object cdsDataEMAIL_ASSUNTO: TStringField
      DisplayLabel = 'Email - Assunto'
      FieldName = 'EMAIL_ASSUNTO'
      Origin = 'EMAIL_ASSUNTO'
      Size = 255
    end
    object cdsDataEMAIL_MENSAGEM: TBlobField
      DisplayLabel = 'Email - Mensagem'
      FieldName = 'EMAIL_MENSAGEM'
      Origin = 'EMAIL_MENSAGEM'
    end
    object cdsDataEMAIL_ENVIAR_NFE: TStringField
      DisplayLabel = 'Email - Enviar Email Ap'#243's Emiss'#227'o da NFE'
      FieldName = 'EMAIL_ENVIAR_NFE'
      Origin = 'EMAIL_ENVIAR_NFE'
      FixedChar = True
      Size = 1
    end
    object cdsDataEMAIL_ENVIAR_NFCE: TStringField
      DisplayLabel = 'Email - Enviar Email Ap'#243's Emiss'#227'o da NFCE'
      FieldName = 'EMAIL_ENVIAR_NFCE'
      Origin = 'EMAIL_ENVIAR_NFCE'
      FixedChar = True
      Size = 1
    end
    object cdsDataEMAIL_EMAIL_HOST: TStringField
      DisplayLabel = 'Email - Host'
      FieldName = 'EMAIL_EMAIL_HOST'
      Origin = 'EMAIL_EMAIL_HOST'
      Size = 255
    end
    object cdsDataEMAIL_EMAIL_DESTINO: TStringField
      DisplayLabel = 'Email - Email Principal do Destinat'#225'rio'
      FieldName = 'EMAIL_EMAIL_DESTINO'
      Origin = 'EMAIL_EMAIL_DESTINO'
      Size = 255
    end
    object cdsDataEMAIL_EMAIL_CC: TStringField
      DisplayLabel = 'Email - Emails para Envio em C'#243'pia'
      FieldName = 'EMAIL_EMAIL_CC'
      Origin = 'EMAIL_EMAIL_CC'
      Size = 255
    end
    object cdsDataEMAIL_EMAIL_CCO: TStringField
      DisplayLabel = 'Email - Emails para Envio em C'#243'pia Oculta'
      FieldName = 'EMAIL_EMAIL_CCO'
      Origin = 'EMAIL_EMAIL_CCO'
      Size = 255
    end
    object cdsDataEMAIL_SSL: TStringField
      DisplayLabel = 'Email - Utiliza SSL'
      FieldName = 'EMAIL_SSL'
      Origin = 'EMAIL_SSL'
      FixedChar = True
      Size = 1
    end
    object cdsDataEMAIL_SOLICITA_CONFIRMACAO: TStringField
      DisplayLabel = 'Email - Solicita Confirma'#231#227'o de Email'
      FieldName = 'EMAIL_SOLICITA_CONFIRMACAO'
      Origin = 'EMAIL_SOLICITA_CONFIRMACAO'
      FixedChar = True
      Size = 1
    end
    object cdsDataEMAIL_USAR_THREAD: TStringField
      DisplayLabel = 'Email - Usar Thread'
      FieldName = 'EMAIL_USAR_THREAD'
      Origin = 'EMAIL_USAR_THREAD'
      FixedChar = True
      Size = 1
    end
    object cdsDataEMAIL_NOME_ORIGEM: TStringField
      DisplayLabel = 'Email - Nome do Remetente'
      FieldName = 'EMAIL_NOME_ORIGEM'
      Origin = 'EMAIL_NOME_ORIGEM'
      Size = 255
    end
    object cdsDataNFCE_MODO_IMPRESSAO: TStringField
      DefaultExpression = 'RETRATO'
      DisplayLabel = 'Modo de Impress'#227'o NFCE'
      FieldName = 'NFCE_MODO_IMPRESSAO'
      Origin = 'NFCE_MODO_IMPRESSAO'
      Required = True
      Size = 15
    end
    object cdsDataNFCE_WEBSERVICE_UF: TStringField
      DefaultExpression = 'PR'
      DisplayLabel = 'UF Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_UF'
      Origin = 'NFCE_WEBSERVICE_UF'
      Required = True
      Size = 2
    end
    object cdsDataNFCE_WEBSERVICE_AMBIENTE: TStringField
      DefaultExpression = 'HOMOLOGACAO'
      DisplayLabel = 'Ambiente Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_AMBIENTE'
      Origin = 'NFCE_WEBSERVICE_AMBIENTE'
      Required = True
      Size = 15
    end
    object cdsDataNFCE_WEBSERVICE_VISUALIZAR_MENSAGEM: TStringField
      DisplayLabel = 'Visualizar Mensagem Webservice NFCE'
      FieldName = 'NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      Origin = 'NFCE_WEBSERVICE_VISUALIZAR_MENSAGEM'
      FixedChar = True
      Size = 1
    end
    object cdsDataNFCE_PROXY_HOST: TStringField
      DisplayLabel = 'Host Proxy NFCE'
      FieldName = 'NFCE_PROXY_HOST'
      Origin = 'NFCE_PROXY_HOST'
      Size = 255
    end
    object cdsDataNFCE_PROXY_PORTA: TIntegerField
      DisplayLabel = 'Porta Proxy NFCE'
      FieldName = 'NFCE_PROXY_PORTA'
      Origin = 'NFCE_PROXY_PORTA'
    end
    object cdsDataNFCE_PROXY_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio Proxy NFCE'
      FieldName = 'NFCE_PROXY_USUARIO'
      Origin = 'NFCE_PROXY_USUARIO'
      Size = 255
    end
    object cdsDataNFCE_PROXY_SENHA: TStringField
      DisplayLabel = 'Senha Proxy NFCE'
      FieldName = 'NFCE_PROXY_SENHA'
      Origin = 'NFCE_PROXY_SENHA'
      Size = 255
    end
    object cdsDataID_FILIAL: TIntegerField
      DefaultExpression = '1'
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataNFE_ID_RELATORIO_FR: TIntegerField
      DisplayLabel = 'C'#243'digo do Relat'#243'rio FR'
      FieldName = 'NFE_ID_RELATORIO_FR'
      Origin = 'NFE_ID_RELATORIO_FR'
    end
    object cdsDataEXPANDIR_LOGOMARCAR: TStringField
      DisplayLabel = 'Expandir Logomarca'
      FieldName = 'EXPANDIR_LOGOMARCAR'
      Origin = 'EXPANDIR_LOGOMARCAR'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsDataNFCE_ID_CSC: TStringField
      DisplayLabel = 'ID CSC - NFCe'
      FieldName = 'NFCE_ID_CSC'
      Origin = 'NFCE_ID_CSC'
      Size = 255
    end
    object cdsDataNFCE_CSC: TStringField
      DisplayLabel = 'CSC - NFCe'
      FieldName = 'NFCE_CSC'
      Origin = 'NFCE_CSC'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_LOGS: TStringField
      DisplayLabel = 'Diret'#243'rio de Logs'
      FieldName = 'ARQUIVO_PATH_LOGS'
      Origin = 'ARQUIVO_PATH_LOGS'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_SCHEMAS: TStringField
      DisplayLabel = 'Diret'#243'rio Schemas'
      FieldName = 'ARQUIVO_PATH_SCHEMAS'
      Origin = 'ARQUIVO_PATH_SCHEMAS'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_NFE: TStringField
      DisplayLabel = 'Diret'#243'rio NFE'
      FieldName = 'ARQUIVO_PATH_NFE'
      Origin = 'ARQUIVO_PATH_NFE'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_CANCELAMENTO: TStringField
      DisplayLabel = 'Diret'#243'rio de Cancelamento'
      FieldName = 'ARQUIVO_PATH_CANCELAMENTO'
      Origin = 'ARQUIVO_PATH_CANCELAMENTO'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_INUTILIZACAO: TStringField
      DisplayLabel = 'Diret'#243'rio de Inutiliza'#231#227'o'
      FieldName = 'ARQUIVO_PATH_INUTILIZACAO'
      Origin = 'ARQUIVO_PATH_INUTILIZACAO'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_DPEC: TStringField
      DisplayLabel = 'Diret'#243'rio DPEC'
      FieldName = 'ARQUIVO_PATH_DPEC'
      Origin = 'ARQUIVO_PATH_DPEC'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_EVENTO: TStringField
      DisplayLabel = 'Diret'#243'rio Eventos'
      FieldName = 'ARQUIVO_PATH_EVENTO'
      Origin = 'ARQUIVO_PATH_EVENTO'
      Size = 255
    end
    object cdsDataARQUIVO_RELATORIO_NFCE_FR: TStringField
      DisplayLabel = 'Arquivo DANFE NFCE Fast Report'
      FieldName = 'ARQUIVO_RELATORIO_NFCE_FR'
      Origin = 'ARQUIVO_RELATORIO_NFCE_FR'
      Size = 255
    end
    object cdsDataARQUIVO_EVENTO_NFCE_FR: TStringField
      DisplayLabel = 'Arquivo Eventos DANFE NFCE Fast Report'
      FieldName = 'ARQUIVO_EVENTO_NFCE_FR'
      Origin = 'ARQUIVO_EVENTO_NFCE_FR'
      Size = 255
    end
    object cdsDataARQUIVO_RELATORIO_NFE_FR: TStringField
      DisplayLabel = 'Arquivo DANFE NFE Fast Report'
      FieldName = 'ARQUIVO_RELATORIO_NFE_FR'
      Origin = 'ARQUIVO_RELATORIO_NFE_FR'
      Size = 255
    end
    object cdsDataARQUIVO_EVENTO_NFE_FR: TStringField
      DisplayLabel = 'Arquivo Eventos DANFE NFE Fast Report'
      FieldName = 'ARQUIVO_EVENTO_NFE_FR'
      Origin = 'ARQUIVO_EVENTO_NFE_FR'
      Size = 255
    end
    object cdsDataIMPRESSORA_NFE: TStringField
      DisplayLabel = 'Impressora NF-e'
      FieldName = 'IMPRESSORA_NFE'
      Origin = 'IMPRESSORA_NFE'
      Size = 255
    end
    object cdsDataIMPRESSORA_NFCE: TStringField
      DisplayLabel = 'Impressora NFC-e'
      FieldName = 'IMPRESSORA_NFCE'
      Origin = 'IMPRESSORA_NFCE'
      Size = 255
    end
    object cdsDataARQUIVO_PATH_PDF: TStringField
      DisplayLabel = 'Diret'#243'rio para salvar arquivos PDF'
      FieldName = 'ARQUIVO_PATH_PDF'
      Origin = 'ARQUIVO_PATH_PDF'
      Size = 255
    end
    object cdsDataNFCE_PREVIEW: TStringField
      DisplayLabel = 'Visualizar NFCE'
      FieldName = 'NFCE_PREVIEW'
      Origin = 'NFCE_PREVIEW'
      FixedChar = True
      Size = 1
    end
    object cdsDataNFE_PREVIEW: TStringField
      DisplayLabel = 'Visualizar NFE'
      FieldName = 'NFE_PREVIEW'
      Origin = 'NFE_PREVIEW'
      FixedChar = True
      Size = 1
    end
    object cdsDataSALVAR_PDF_JUNTO_XML: TStringField
      DisplayLabel = 'Salvar PDF Junto com XML'
      FieldName = 'SALVAR_PDF_JUNTO_XML'
      Origin = 'SALVAR_PDF_JUNTO_XML'
      FixedChar = True
      Size = 1
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42324.462158912040000000
      BuiltInReportLink = True
    end
  end
end
