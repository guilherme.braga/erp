inherited FrmAutenticacaoUsuario: TFrmAutenticacaoUsuario
  Caption = 'Autentica'#231#227'o'
  ClientHeight = 176
  ClientWidth = 332
  ExplicitWidth = 338
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ExplicitWidth = 332
    ExplicitHeight = 120
    Height = 120
    Width = 332
    object cxLabel1: TcxLabel
      Left = 2
      Top = 2
      Align = alTop
      Caption = 'Para executar essa a'#231#227'o ser'#225' necess'#225'rio autoriza'#231#227'o:'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object EdtUsuario: TcxTextEdit
      Left = 69
      Top = 33
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      Width = 257
    end
    object cxLabel2: TcxLabel
      Left = 2
      Top = 35
      Caption = 'Usu'#225'rio'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 4
      Top = 71
      Caption = 'Senha'
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object EdtSenha: TcxTextEdit
      Left = 69
      Top = 69
      ParentFont = False
      Properties.EchoMode = eemPassword
      Properties.PasswordChar = '*'
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 4
      OnExit = EdtSenhaExit
      Width = 257
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    Top = 120
    ExplicitTop = 120
    ExplicitWidth = 332
    Width = 332
    inherited BtnConfirmar: TcxButton
      Left = 180
      ExplicitLeft = 180
    end
    inherited BtnCancelar: TcxButton
      Left = 255
      ExplicitLeft = 255
    end
  end
  inherited ActionList: TActionList
    Left = 22
    Top = 110
  end
end
