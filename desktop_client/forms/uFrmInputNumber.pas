unit uFrmInputNumber;

interface

uses
  //System
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ExtCtrls,
  Keyboard,

  //Project
  uFrmTecladoNumerico,

  //DevExpress
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  cxTextEdit,
  cxGroupBox;

type
  TFrmInputNumber = class(TFrmTecladoNumerico)
    PnCaption: TPanel;
    procedure EdtVisorKeyPress(Sender: TObject; var Key: Char);
  private

  public
    class function buildKeyboard(const Atext: string): Real; overload;
    class function buildKeyboard(const Atext: string; AValue: String): Real; overload;
  end;

var
  FrmInputNumber: TFrmInputNumber;

implementation

{$R *.dfm}

class function TFrmInputNumber.buildKeyboard(const Atext: string): Real;
begin
  try
    Application.CreateForm(TFrmInputNumber, FrmInputNumber);
    FrmInputNumber.PnCaption.Caption := Atext;
    FrmInputNumber.ShowModal;

    try
      result := StrtoFloat(FrmInputNumber.EdtVisor.Text);
    except
      result := 0;
    end;
  finally
    FreeAndNil(FrmInputNumber);
  end;
end;

class function TFrmInputNumber.buildKeyboard(const Atext: string;
  AValue: String): Real;
begin
  try
    Application.CreateForm(TFrmInputNumber, FrmInputNumber);
    FrmInputNumber.PnCaption.Caption := Atext;
    FrmInputNumber.EdtVisor.Text := AValue;
    FrmInputNumber.ShowModal;

    try
      result := StrtoFloat(FrmInputNumber.EdtVisor.Text);
    except
      result := 0;
    end;
  finally
    FreeAndNil(FrmInputNumber);
  end;
end;

procedure TFrmInputNumber.EdtVisorKeyPress(Sender: TObject; var Key: Char);

  function keyNumber(CharKey: Char): Char;
  begin
    if CharKey in ['0'..'9'] then
      result := CharKey
    else
      if CharKey in [',', '.'] then
        result := FormatSettings.DecimalSeparator
      else
        result := #0;
  end;

begin
  inherited;
  if Key <> #8 then
    Key := keyNumber(Key);
end;

end.
