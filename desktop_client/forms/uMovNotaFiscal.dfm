inherited MovNotaFiscal: TMovNotaFiscal
  Caption = 'Nota Fiscal'
  ClientHeight = 470
  ClientWidth = 1269
  ExplicitWidth = 1285
  ExplicitHeight = 509
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1269
    ExplicitWidth = 1269
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1269
    Height = 343
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1269
    ExplicitHeight = 343
    ClientRectBottom = 343
    ClientRectRight = 1269
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1269
      ExplicitHeight = 319
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1237
        Height = 319
        ExplicitWidth = 1237
        ExplicitHeight = 319
      end
      inherited pnFiltroVertical: TgbPanel
        ExplicitHeight = 319
        Height = 319
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 319
        ExplicitHeight = 319
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1269
      ExplicitHeight = 319
      inherited DesignPanel: TJvDesignPanel
        Width = 1269
        Height = 319
        ExplicitWidth = 1269
        ExplicitHeight = 319
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1269
          ExplicitHeight = 319
          Height = 319
          Width = 1269
          inherited dxBevel1: TdxBevel
            Width = 1265
            ExplicitWidth = 987
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label1: TLabel
            Left = 107
            Top = 9
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object Label13: TLabel
            Left = 318
            Top = 9
            Width = 41
            Height = 13
            Caption = 'Situa'#231#227'o'
          end
          object Label32: TLabel
            Left = 468
            Top = 9
            Width = 53
            Height = 13
            Caption = 'Chave NFE'
          end
          object lbSituacaoSefaz: TLabel
            Left = 1066
            Top = 9
            Width = 86
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o na Sefaz'
            ExplicitLeft = 790
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 45
            Top = 5
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object cxPageControlDetails: TcxPageControl
            Left = 2
            Top = 34
            Width = 1265
            Height = 235
            Align = alClient
            Focusable = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            Properties.ActivePage = cxTabSheet2
            Properties.CustomButtons.Buttons = <>
            Properties.NavigatorPosition = npLeftTop
            Properties.Options = [pcoAlwaysShowGoDialogButton, pcoGradient, pcoRedrawOnResize]
            Properties.Style = 8
            ClientRectBottom = 235
            ClientRectRight = 1265
            ClientRectTop = 24
            object cxTabSheet2: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 0
              object PnDados: TgbPanel
                Left = 0
                Top = 0
                Align = alTop
                Alignment = alTopCenter
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                DesignSize = (
                  1265
                  76)
                Height = 76
                Width = 1265
                object Label3: TLabel
                  Left = 6
                  Top = 31
                  Width = 79
                  Height = 13
                  Caption = 'Data de Emiss'#227'o'
                end
                object Label2: TLabel
                  Left = 177
                  Top = 55
                  Width = 54
                  Height = 13
                  Caption = 'Documento'
                end
                object Label9: TLabel
                  Left = 477
                  Top = 31
                  Width = 34
                  Height = 13
                  Caption = 'Modelo'
                end
                object lbDtEntradaSaida: TLabel
                  Left = 177
                  Top = 31
                  Width = 56
                  Height = 13
                  Caption = 'Dt. Entrada'
                end
                object Label5: TLabel
                  Left = 330
                  Top = 55
                  Width = 24
                  Height = 13
                  Caption = 'S'#233'rie'
                end
                object lbHrEntradaSaida: TLabel
                  Left = 330
                  Top = 31
                  Width = 56
                  Height = 13
                  Caption = 'Hr. Entrada'
                end
                object lbPessoa: TLabel
                  Left = 477
                  Top = 55
                  Width = 55
                  Height = 13
                  Caption = 'Fornecedor'
                end
                object Label31: TLabel
                  Left = 477
                  Top = 7
                  Width = 76
                  Height = 13
                  Caption = 'Opera'#231#227'o Fiscal'
                  Visible = False
                end
                object Label8: TLabel
                  Left = 6
                  Top = 7
                  Width = 47
                  Height = 13
                  Caption = 'Opera'#231#227'o'
                end
                object Label18: TLabel
                  Left = 6
                  Top = 55
                  Width = 51
                  Height = 13
                  Caption = 'Tipo da NF'
                end
                object gbDBDateEdit2: TgbDBDateEdit
                  Left = 87
                  Top = 27
                  DataBinding.DataField = 'B_DEMI'
                  DataBinding.DataSource = dsData
                  Properties.DateButtons = [btnClear, btnToday]
                  Properties.ImmediatePost = True
                  Properties.ReadOnly = False
                  Properties.SaveTime = False
                  Properties.ShowTime = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 4
                  gbRequired = True
                  gbDateTime = False
                  Width = 83
                end
                object EdtSerie: TgbDBTextEdit
                  Left = 390
                  Top = 51
                  DataBinding.DataField = 'B_SERIE'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 9
                  gbRequired = True
                  gbPassword = False
                  Width = 83
                end
                object gbDBButtonEditFK1: TgbDBButtonEditFK
                  Left = 556
                  Top = 27
                  DataBinding.DataField = 'JOIN_MODELO_MODELO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 7
                  gbRequired = True
                  gbCampoPK = 'MODELO'
                  gbCamposRetorno = 'ID_MODELO_NOTA;JOIN_DESCRICAO_MODELO;JOIN_MODELO_MODELO'
                  gbTableName = 'MODELO_NOTA'
                  gbCamposConsulta = 'ID;DESCRICAO;MODELO'
                  gbClasseDoCadastro = 'TCadModeloNotaFiscal'
                  gbIdentificadorConsulta = 'MODELO_NOTA'
                  Width = 60
                end
                object gbDBDateEdit3: TgbDBDateEdit
                  Left = 237
                  Top = 27
                  DataBinding.DataField = 'B_DSAIENT'
                  DataBinding.DataSource = dsData
                  Properties.DateButtons = [btnClear, btnToday]
                  Properties.ImmediatePost = True
                  Properties.ReadOnly = False
                  Properties.SaveTime = False
                  Properties.ShowTime = False
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 5
                  gbRequired = True
                  gbDateTime = False
                  Width = 89
                end
                object gbDBTextEdit1: TgbDBTextEdit
                  Left = 87
                  Top = 51
                  TabStop = False
                  DataBinding.DataField = 'TIPO_NF'
                  DataBinding.DataSource = dsData
                  ParentFont = False
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 10
                  gbReadyOnly = True
                  gbRequired = True
                  gbPassword = False
                  Width = 83
                end
                object cxDBTimeEdit1: TcxDBTimeEdit
                  Left = 390
                  Top = 27
                  DataBinding.DataField = 'B_HSAIENT'
                  DataBinding.DataSource = dsData
                  Properties.ImmediatePost = True
                  TabOrder = 6
                  Width = 83
                end
                object gbDBButtonEditFK3: TgbDBButtonEditFK
                  Left = 556
                  Top = 51
                  DataBinding.DataField = 'ID_PESSOA'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 11
                  gbTextEdit = gbDBTextEdit5
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_PESSOA;JOIN_NOME_PESSOA'
                  gbTableName = 'PESSOA'
                  gbCamposConsulta = 'ID;NOME'
                  gbClasseDoCadastro = 'TCadPessoa'
                  gbIdentificadorConsulta = 'PESSOA'
                  Width = 60
                end
                object gbDBTextEdit5: TgbDBTextEdit
                  Left = 613
                  Top = 51
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_NOME_PESSOA'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 12
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 647
                end
                object EdtOperacaoFiscal: TgbDBButtonEditFK
                  Left = 556
                  Top = 3
                  DataBinding.DataField = 'ID_OPERACAO_FISCAL'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 2
                  Visible = False
                  gbTextEdit = EdtDescricaoOperacaoFiscal
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_OPERACAO_FISCAL;JOIN_DESCRICAO_OPERACAO_FISCAL'
                  gbTableName = 'OPERACAO_FISCAL'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbAntesDeConsultar = EdtOperacaoFiscalgbAntesDeConsultar
                  gbClasseDoCadastro = 'TCadOperacao'
                  gbIdentificadorConsulta = 'OPERACAO_FISCAL'
                  Width = 60
                end
                object EdtDescricaoOperacaoFiscal: TgbDBTextEdit
                  Left = 613
                  Top = 3
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 3
                  Visible = False
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 647
                end
                object EdtOperacao: TgbDBButtonEditFK
                  Left = 87
                  Top = 3
                  DataBinding.DataField = 'ID_OPERACAO'
                  DataBinding.DataSource = dsData
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.CharCase = ecUpperCase
                  Properties.ClickKey = 13
                  Properties.ReadOnly = False
                  Style.Color = 14606074
                  TabOrder = 0
                  gbTextEdit = gbDBTextEdit2
                  gbRequired = True
                  gbCampoPK = 'ID'
                  gbCamposRetorno = 'ID_OPERACAO;JOIN_DESCRICAO_OPERACAO'
                  gbTableName = 'OPERACAO'
                  gbCamposConsulta = 'ID;DESCRICAO'
                  gbClasseDoCadastro = 'TCadOperacao'
                  gbIdentificadorConsulta = 'OPERACAO'
                  Width = 60
                end
                object gbDBTextEdit2: TgbDBTextEdit
                  Left = 144
                  Top = 3
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_OPERACAO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 1116
                end
                object gbDBTextEdit25: TgbDBTextEdit
                  Left = 613
                  Top = 27
                  TabStop = False
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'JOIN_DESCRICAO_MODELO'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Properties.ReadOnly = True
                  Style.BorderStyle = ebsOffice11
                  Style.Color = clGradientActiveCaption
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 13
                  gbReadyOnly = True
                  gbPassword = False
                  Width = 647
                end
                object EdtDocumento: TgbDBTextEdit
                  Left = 237
                  Top = 51
                  DataBinding.DataField = 'B_NNF'
                  DataBinding.DataSource = dsData
                  Properties.CharCase = ecUpperCase
                  Style.BorderStyle = ebsOffice11
                  Style.Color = 14606074
                  Style.LookAndFeel.Kind = lfOffice11
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 8
                  gbRequired = True
                  gbPassword = False
                  Width = 89
                end
              end
              object PnFrameNotaFiscalItem: TgbPanel
                Left = 0
                Top = 76
                Align = alClient
                Alignment = alCenterCenter
                Caption = 'TFrameNotaFiscal'
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 135
                Width = 1265
              end
            end
            object cxTabSheet3: TcxTabSheet
              Caption = 'Fechamento'
              ImageIndex = 1
              object PnParcela: TgbPanel
                Left = 464
                Top = 0
                Align = alClient
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 211
                Width = 801
                object gridParcela: TcxGrid
                  Left = 2
                  Top = 2
                  Width = 797
                  Height = 207
                  Align = alClient
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  Images = DmAcesso.cxImage16x16
                  ParentFont = False
                  TabOrder = 0
                  LookAndFeel.Kind = lfOffice11
                  LookAndFeel.NativeStyle = False
                  object cxGridDBBandedTableView1: TcxGridDBBandedTableView
                    Navigator.Buttons.CustomButtons = <>
                    Navigator.Buttons.Images = DmAcesso.cxImage16x16
                    Navigator.Buttons.PriorPage.Visible = False
                    Navigator.Buttons.NextPage.Visible = False
                    Navigator.Buttons.Insert.Visible = False
                    Navigator.Buttons.Delete.Visible = False
                    Navigator.Buttons.Edit.Visible = False
                    Navigator.Buttons.Post.Visible = False
                    Navigator.Buttons.Cancel.Visible = False
                    Navigator.Buttons.Refresh.Visible = False
                    Navigator.Buttons.SaveBookmark.Visible = False
                    Navigator.Buttons.GotoBookmark.Visible = False
                    Navigator.Buttons.Filter.Visible = False
                    Navigator.InfoPanel.Visible = True
                    Navigator.Visible = True
                    DataController.DataSource = dsNotaFiscalParcela
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    Images = DmAcesso.cxImage16x16
                    OptionsBehavior.NavigatorHints = True
                    OptionsBehavior.ExpandMasterRowOnDblClick = False
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.BandMoving = False
                    OptionsCustomize.NestedBands = False
                    OptionsData.CancelOnExit = False
                    OptionsData.Deleting = False
                    OptionsData.DeletingConfirmation = False
                    OptionsData.Inserting = False
                    OptionsView.ColumnAutoWidth = True
                    OptionsView.GroupByBox = False
                    OptionsView.GroupRowStyle = grsOffice11
                    OptionsView.ShowColumnFilterButtons = sfbAlways
                    OptionsView.BandHeaders = False
                    Styles.ContentOdd = DmAcesso.OddColor
                    Bands = <
                      item
                      end>
                    object cxGridDBBandedTableView1NR_PARCELA: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'NR_PARCELA'
                      HeaderAlignmentHorz = taCenter
                      HeaderGlyphAlignmentHorz = taCenter
                      Options.Editing = False
                      Options.Sorting = False
                      Width = 72
                      Position.BandIndex = 0
                      Position.ColIndex = 0
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1DOCUMENTO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'DOCUMENTO'
                      Visible = False
                      Options.Editing = False
                      Options.Sorting = False
                      Width = 128
                      Position.BandIndex = 0
                      Position.ColIndex = 1
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1DT_VENCIMENTO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'DT_VENCIMENTO'
                      HeaderAlignmentHorz = taCenter
                      Options.Sorting = False
                      Width = 124
                      Position.BandIndex = 0
                      Position.ColIndex = 3
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1IC_DIAS: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'IC_DIAS'
                      HeaderAlignmentHorz = taCenter
                      HeaderGlyphAlignmentHorz = taCenter
                      Options.Sorting = False
                      Width = 91
                      Position.BandIndex = 0
                      Position.ColIndex = 4
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1VL_TITULO: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'VL_TITULO'
                      HeaderAlignmentHorz = taCenter
                      Options.Sorting = False
                      Width = 182
                      Position.BandIndex = 0
                      Position.ColIndex = 5
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1ID_CARTEIRA: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'ID_CARTEIRA'
                      PropertiesClassName = 'TcxButtonEditProperties'
                      Properties.Buttons = <
                        item
                          Default = True
                          Kind = bkEllipsis
                        end>
                      Properties.OnButtonClick = cxGridDBBandedTableView1ID_CARTEIRAPropertiesButtonClick
                      Properties.OnEditValueChanged = cxGridDBBandedTableView1ID_CARTEIRAPropertiesEditValueChanged
                      Visible = False
                      Position.BandIndex = 0
                      Position.ColIndex = 6
                      Position.RowIndex = 0
                    end
                    object cxGridDBBandedTableView1JOIN_DESCRICAO_CARTEIRA: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'JOIN_DESCRICAO_CARTEIRA'
                      PropertiesClassName = 'TcxButtonEditProperties'
                      Properties.Buttons = <
                        item
                          Default = True
                          Kind = bkEllipsis
                        end>
                      Properties.OnButtonClick = cxGridDBBandedTableView1ID_CARTEIRAPropertiesButtonClick
                      Properties.OnEditValueChanged = cxGridDBBandedTableView1ID_CARTEIRAPropertiesEditValueChanged
                      Visible = False
                      Width = 125
                      Position.BandIndex = 0
                      Position.ColIndex = 2
                      Position.RowIndex = 0
                    end
                  end
                  object cxGridLevel1: TcxGridLevel
                    GridView = cxGridDBBandedTableView1
                  end
                end
              end
              object PnDadosFechamento: TgbPanel
                AlignWithMargins = True
                Left = 3
                Top = 3
                Align = alLeft
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 205
                Width = 458
                object pnDadosFrete: TgbPanel
                  AlignWithMargins = True
                  Left = 5
                  Top = 5
                  Align = alTop
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 0
                  Transparent = True
                  Height = 42
                  Width = 448
                  object dxBevel3: TdxBevel
                    Left = 2
                    Top = 2
                    Width = 444
                    Height = 19
                    Align = alTop
                    Shape = dxbsLineBottom
                    ExplicitWidth = 480
                  end
                  object lbInformacoesCliente: TLabel
                    Left = 1
                    Top = 0
                    Width = 106
                    Height = 13
                    Caption = 'Transporte e Frete'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label35: TLabel
                    Left = 218
                    Top = 25
                    Width = 24
                    Height = 13
                    Caption = 'Valor'
                  end
                  object Label36: TLabel
                    Left = 1
                    Top = 25
                    Width = 98
                    Height = 13
                    Caption = 'Modalidade do Frete'
                  end
                  object EdtValorFrete: TGBDBValorComPercentual
                    Left = 247
                    Top = 21
                    PanelStyle.Active = True
                    PanelStyle.OfficeBackgroundKind = pobkGradient
                    Style.BorderStyle = ebsNone
                    Style.LookAndFeel.Kind = lfOffice11
                    Style.Shadow = False
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    Transparent = True
                    gbFieldValorBase = 'W_VNF'
                    gbDataSourceValorBase = dsData
                    gbFieldValor = 'W_VFRETE'
                    gbDataSourceValor = dsData
                    gbFieldPercentual = 'PERC_VFRETE'
                    gbDataSourcePercentual = dsData
                    Height = 21
                    Width = 194
                  end
                  object EdtModalidadeFrete: TJvDBComboBox
                    Left = 106
                    Top = 21
                    Width = 106
                    Height = 21
                    DataField = 'X_MODFRETE'
                    DataSource = dsData
                    Items.Strings = (
                      'Emitente'
                      'Destinat'#225'rio/Remetente'
                      'Terceiros'
                      'Sem Frete')
                    TabOrder = 0
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '9')
                    ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                    ListSettings.OutfilteredValueFont.Color = clRed
                    ListSettings.OutfilteredValueFont.Height = -11
                    ListSettings.OutfilteredValueFont.Name = 'Tahoma'
                    ListSettings.OutfilteredValueFont.Style = []
                  end
                end
                object PnFinanceiro: TgbPanel
                  Left = 2
                  Top = 50
                  Align = alClient
                  PanelStyle.Active = True
                  PanelStyle.OfficeBackgroundKind = pobkGradient
                  Style.BorderStyle = ebsNone
                  Style.LookAndFeel.Kind = lfOffice11
                  Style.Shadow = False
                  StyleDisabled.LookAndFeel.Kind = lfOffice11
                  StyleFocused.LookAndFeel.Kind = lfOffice11
                  StyleHot.LookAndFeel.Kind = lfOffice11
                  TabOrder = 1
                  Transparent = True
                  DesignSize = (
                    454
                    153)
                  Height = 153
                  Width = 454
                  object dxBevel2: TdxBevel
                    Left = 2
                    Top = 2
                    Width = 450
                    Height = 19
                    Align = alTop
                    Shape = dxbsLineBottom
                  end
                  object Label14: TLabel
                    Left = 4
                    Top = 144
                    Width = 98
                    Height = 13
                    Caption = 'Plano de Pagamento'
                  end
                  object Label11: TLabel
                    Left = 4
                    Top = 75
                    Width = 99
                    Height = 13
                    Caption = 'Centro de Resultado'
                  end
                  object Label12: TLabel
                    Left = 4
                    Top = 98
                    Width = 81
                    Height = 13
                    Caption = 'Conta de An'#225'lise'
                  end
                  object Label15: TLabel
                    Left = 4
                    Top = 121
                    Width = 102
                    Height = 13
                    Caption = 'Forma de Pagamento'
                  end
                  object Label30: TLabel
                    Left = 271
                    Top = 27
                    Width = 80
                    Height = 13
                    Caption = 'Dt. Compet'#234'ncia'
                  end
                  object Label34: TLabel
                    Left = 4
                    Top = 2
                    Width = 58
                    Height = 13
                    Caption = 'Financeiro'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label37: TLabel
                    Left = 4
                    Top = 26
                    Width = 91
                    Height = 13
                    Caption = 'Forma de Quita'#231#227'o'
                  end
                  object Label38: TLabel
                    Left = 3
                    Top = 52
                    Width = 102
                    Height = 13
                    Caption = 'Quita'#231#227'o em Dinheiro'
                  end
                  object EdtPlanoPagamento: TgbDBButtonEditFK
                    Left = 109
                    Top = 140
                    DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Properties.ReadOnly = False
                    Style.Color = 14606074
                    TabOrder = 9
                    gbTextEdit = gbDBTextEdit9
                    gbRequired = True
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO'
                    gbTableName = 'PLANO_PAGAMENTO'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'PLANO_PAGAMENTO'
                    Width = 60
                  end
                  object gbDBTextEdit9: TgbDBTextEdit
                    Left = 166
                    Top = 140
                    TabStop = False
                    DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 10
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 278
                  end
                  object gbDBTextEdit6: TgbDBTextEdit
                    Left = 166
                    Top = 71
                    TabStop = False
                    DataBinding.DataField = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 4
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 278
                  end
                  object gbDBButtonEditFK4: TgbDBButtonEditFK
                    Left = 109
                    Top = 71
                    DataBinding.DataField = 'ID_CENTRO_RESULTADO'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Properties.ReadOnly = False
                    Style.Color = 14606074
                    TabOrder = 3
                    gbTextEdit = gbDBTextEdit6
                    gbRequired = True
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_CENTRO_RESULTADO;JOIN_DESCRICAO_CENTRO_RESULTADO'
                    gbTableName = 'CENTRO_RESULTADO'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'CENTRO_RESULTADO'
                    Width = 60
                  end
                  object gbDBTextEdit7: TgbDBTextEdit
                    Left = 166
                    Top = 94
                    TabStop = False
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_CONTA_ANALISE'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 6
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 278
                  end
                  object EdtContaAnalise: TcxDBButtonEdit
                    Left = 109
                    Top = 94
                    DataBinding.DataField = 'ID_CONTA_ANALISE'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.ClickKey = 13
                    Properties.OnButtonClick = EdtContaAnalisePropertiesButtonClick
                    Style.Color = 14606074
                    TabOrder = 5
                    OnDblClick = EdtContaAnaliseDblClick
                    OnKeyDown = EdtContaAnaliseKeyDown
                    Width = 60
                  end
                  object gbDBTextEdit10: TgbDBTextEdit
                    Left = 166
                    Top = 117
                    TabStop = False
                    Anchors = [akLeft, akTop, akRight]
                    DataBinding.DataField = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
                    DataBinding.DataSource = dsData
                    Properties.CharCase = ecUpperCase
                    Properties.ReadOnly = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clGradientActiveCaption
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 8
                    gbReadyOnly = True
                    gbPassword = False
                    Width = 278
                  end
                  object EdtFormaPagamento: TgbDBButtonEditFK
                    Left = 109
                    Top = 117
                    DataBinding.DataField = 'ID_FORMA_PAGAMENTO'
                    DataBinding.DataSource = dsData
                    Properties.Buttons = <
                      item
                        Default = True
                        Kind = bkEllipsis
                      end>
                    Properties.CharCase = ecUpperCase
                    Properties.ClickKey = 13
                    Properties.ReadOnly = False
                    Style.Color = 14606074
                    TabOrder = 7
                    gbTextEdit = gbDBTextEdit9
                    gbRequired = True
                    gbCampoPK = 'ID'
                    gbCamposRetorno = 'ID_FORMA_PAGAMENTO;JOIN_DESCRICAO_FORMA_PAGAMENTO'
                    gbTableName = 'FORMA_PAGAMENTO'
                    gbCamposConsulta = 'ID;DESCRICAO'
                    gbIdentificadorConsulta = 'FORMA_PAGAMENTO'
                    Width = 60
                  end
                  object gbDBDateEdit4: TgbDBDateEdit
                    Left = 355
                    Top = 23
                    DataBinding.DataField = 'DT_COMPETENCIA'
                    DataBinding.DataSource = dsData
                    Properties.DateButtons = [btnClear, btnToday]
                    Properties.ImmediatePost = True
                    Properties.ReadOnly = False
                    Properties.SaveTime = False
                    Properties.ShowTime = False
                    Style.BorderStyle = ebsOffice11
                    Style.Color = 14606074
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 1
                    gbRequired = True
                    gbDateTime = False
                    Width = 89
                  end
                  object gbDBRadioGroup1: TgbDBRadioGroup
                    Left = 110
                    Top = 17
                    DataBinding.DataField = 'FORMA_PAGAMENTO'
                    DataBinding.DataSource = dsData
                    Properties.Columns = 3
                    Properties.ImmediatePost = True
                    Properties.Items = <
                      item
                        Caption = #192' Vista'
                        Value = 'VISTA'
                      end
                      item
                        Caption = #192' Prazo'
                        Value = 'PRAZO'
                      end>
                    Style.BorderStyle = ebsOffice11
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 0
                    Height = 27
                    Width = 156
                  end
                  object EdtQuitacaoDinheiro: TgbDBCalcEdit
                    Left = 109
                    Top = 48
                    DataBinding.DataField = 'VL_QUITACAO_DINHEIRO'
                    DataBinding.DataSource = dsData
                    Properties.DisplayFormat = '###,###,###,###,###,##0.00'
                    Properties.ImmediatePost = True
                    Properties.UseThousandSeparator = True
                    Style.BorderStyle = ebsOffice11
                    Style.Color = clWhite
                    Style.LookAndFeel.Kind = lfOffice11
                    StyleDisabled.LookAndFeel.Kind = lfOffice11
                    StyleFocused.LookAndFeel.Kind = lfOffice11
                    StyleHot.LookAndFeel.Kind = lfOffice11
                    TabOrder = 2
                    Width = 120
                  end
                end
              end
            end
          end
          object gbDBDateEdit1: TgbDBDateEdit
            Left = 184
            Top = 5
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 269
            Align = alBottom
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            Transparent = True
            Height = 48
            Width = 1265
            object Label16: TLabel
              Left = 312
              Top = 8
              Width = 45
              Height = 13
              Caption = 'Desconto'
            end
            object Label17: TLabel
              Left = 559
              Top = 8
              Width = 48
              Height = 13
              Caption = 'Acr'#233'scimo'
            end
            object Label20: TLabel
              Left = 5
              Top = 8
              Width = 106
              Height = 13
              Caption = 'Total dos Produtos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label21: TLabel
              Left = 809
              Top = 8
              Width = 26
              Height = 13
              Caption = 'Frete'
            end
            object Label22: TLabel
              Left = 5
              Top = 32
              Width = 34
              Height = 13
              Caption = 'Seguro'
            end
            object Label23: TLabel
              Left = 312
              Top = 32
              Width = 40
              Height = 13
              Caption = 'ICMS ST'
            end
            object Label24: TLabel
              Left = 559
              Top = 32
              Width = 14
              Height = 13
              Caption = 'IPI'
            end
            object Label27: TLabel
              Left = 809
              Top = 32
              Width = 61
              Height = 13
              Caption = 'Valor Total'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object gbDBTextEdit15: TgbDBTextEdit
              Left = 115
              Top = 4
              TabStop = False
              DataBinding.DataField = 'W_VPROD'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 121
            end
            object gbDBTextEdit22: TgbDBTextEdit
              Left = 874
              Top = 28
              TabStop = False
              DataBinding.DataField = 'W_VNF'
              DataBinding.DataSource = dsData
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.Kind = lfOffice11
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbReadyOnly = True
              gbRequired = True
              gbPassword = False
              Width = 181
            end
            object GBDBValorComPercentual1: TGBDBValorComPercentual
              Left = 361
              Top = 4
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              Transparent = True
              gbFieldValorBase = 'W_VNF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VDESC'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VDESC'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
            object GBDBValorComPercentual2: TGBDBValorComPercentual
              Left = 611
              Top = 4
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              Transparent = True
              gbFieldValorBase = 'W_VNF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VOUTRO'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VOUTRO'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
            object GBDBValorComPercentual3: TGBDBValorComPercentual
              Left = 874
              Top = 4
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              Transparent = True
              gbFieldValorBase = 'W_VNF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VFRETE'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VFRETE'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
            object GBDBValorComPercentual4: TGBDBValorComPercentual
              Left = 115
              Top = 28
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 5
              Transparent = True
              gbFieldValorBase = 'W_VNF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VSEG'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VSEG'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
            object GBDBValorComPercentual5: TGBDBValorComPercentual
              Left = 361
              Top = 28
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 6
              Transparent = True
              gbFieldValorBase = 'W_VNF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VST'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VST'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
            object GBDBValorComPercentual6: TGBDBValorComPercentual
              Left = 611
              Top = 28
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 7
              Transparent = True
              gbFieldValorBase = 'W_NF'
              gbDataSourceValorBase = dsData
              gbFieldValor = 'W_VIPI'
              gbDataSourceValor = dsData
              gbFieldPercentual = 'PERC_VIPI'
              gbDataSourcePercentual = dsData
              gbReadOnlyValor = True
              gbReadOnlyPercentual = True
              Height = 21
              Width = 194
            end
          end
          object gbDBTextEdit8: TgbDBTextEdit
            Left = 363
            Top = 5
            TabStop = False
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 101
          end
          object gbDBTextEdit26: TgbDBTextEdit
            Left = 525
            Top = 5
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'NFE_CHAVE'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbPassword = False
            Width = 537
          end
          object EdtSituacaoSefaz: TgbDBTextEdit
            Left = 1155
            Top = 5
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'AR_CSTAT'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            gbReadyOnly = True
            gbPassword = False
            Width = 105
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 200
      FloatClientHeight = 231
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'bbCancelarNFCE'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbCancelar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbManutencaoTabelaPreco'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedLeft = 801
      FloatClientWidth = 101
      FloatClientHeight = 226
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbReport'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'lbEmitirNotaFiscal'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
    end
    inherited dxBarEnd: TdxBar
      DockedLeft = 1141
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedLeft = 726
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
      FloatClientWidth = 61
      FloatClientHeight = 180
      OneOnRow = False
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 1049
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object lbEfetivar: TdxBarLargeButton [9]
      Action = ActEfetivar
      Category = 0
    end
    object lbCancelar: TdxBarLargeButton [10]
      Action = ActEstornar
      Category = 0
    end
    object lbManutencaoTabelaPreco: TdxBarLargeButton [11]
      Action = ActManutencaoTabelaPreco
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton [19]
      Action = ActCancelarEmissaoNFE
      Category = 1
    end
    object bbCancelarNFCE: TdxBarLargeButton [20]
      Action = ActCancelarEmissaoNFCE
      Category = 1
    end
    object dxBarLargeButton4: TdxBarLargeButton [21]
      Action = ActImprimirEtiqueta
      Category = 2
    end
    inherited dxlbReport: TdxBarLargeButton
      Visible = ivNever
    end
    object lbEmitirNotaFiscal: TdxBarLargeButton [23]
      Action = ActEmitirNFE
      Category = 2
    end
    object dxBarLargeButton5: TdxBarLargeButton [24]
      Action = ActEmitirNFCE
      Category = 2
    end
  end
  inherited ActionListMain: TActionList
    object ActEstornar: TAction [12]
      Caption = 'Estornar F10'
      Hint = 'Estornar a nota fiscal'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = ActEstornarExecute
    end
    object ActEfetivar: TAction [13]
      Caption = 'Efetivar F9'
      Hint = 'Efetivar a nota fiscal'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarExecute
    end
    object ActImprimirEtiqueta: TAction [14]
      Category = 'Report'
      Caption = 'Etiqueta Ctrl+F8'
      ImageIndex = 156
      ShortCut = 16503
      OnExecute = ActImprimirEtiquetaExecute
    end
    object ActManutencaoTabelaPreco: TAction [15]
      Category = 'Action'
      Caption = 'Manuten'#231#227'o da Tabela de Pre'#231'o F6'
      Hint = 'Realizar manuten'#231#227'o na tabela de pre'#231'o para os itens desta nota'
      ImageIndex = 7
      ShortCut = 117
      OnExecute = ActManutencaoTabelaPrecoExecute
    end
    object ActEmitirNFE: TAction
      Category = 'Report'
      Caption = 'Emitir NFe'
      ImageIndex = 157
      OnExecute = ActEmitirNFEExecute
    end
    object ActEmitirNFCE: TAction
      Category = 'Report'
      Caption = 'Emitir NFCe'
      ImageIndex = 158
      OnExecute = ActEmitirNFCEExecute
    end
    object ActCancelarEmissaoNFE: TAction
      Category = 'Report'
      Caption = 'Cancelar NFe'
      Hint = 'Realiza o cancelamento do documento fiscal na SEFAZ'
      ImageIndex = 110
      OnExecute = ActCancelarEmissaoNFEExecute
    end
    object ActCancelarEmissaoNFCE: TAction
      Category = 'Report'
      Caption = 'Cancelar NFCe'
      Hint = 'Realiza o cancelamento do documento fiscal na SEFAZ'
      ImageIndex = 110
      OnExecute = ActCancelarEmissaoNFCEExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'ID'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspNotaFiscal'
    RemoteServer = DmConnection.dspNotaFiscal
    AfterOpen = cdsDataAfterOpen
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataB_NNF: TIntegerField
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'B_NNF'
      Origin = 'B_NNF'
    end
    object cdsDataB_SERIE: TIntegerField
      DisplayLabel = 'S'#233'rie'
      FieldName = 'B_SERIE'
      Origin = 'B_SERIE'
    end
    object cdsDataID_MODELO_NOTA: TIntegerField
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO_NOTA'
      Origin = 'ID_MODELO_NOTA'
    end
    object cdsDataB_DEMI: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'B_DEMI'
      Origin = 'B_DEMI'
    end
    object cdsDataB_DSAIENT: TDateField
      DisplayLabel = 'Dt. Entrada'
      FieldName = 'B_DSAIENT'
      Origin = 'B_DSAIENT'
    end
    object cdsDataB_HSAIENT: TTimeField
      DisplayLabel = 'Hr. Entrada'
      FieldName = 'B_HSAIENT'
      Origin = 'B_HSAIENT'
    end
    object cdsDataX_XNOME: TStringField
      DisplayLabel = 'Transportadora'
      FieldName = 'X_XNOME'
      Origin = 'X_XNOME'
      Size = 45
    end
    object cdsDataW_VPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Produto'
      FieldName = 'W_VPROD'
      Origin = 'W_VPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'W_VST'
      Origin = 'W_VST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VSEG: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Seguro'
      FieldName = 'W_VSEG'
      Origin = 'W_VSEG'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VDESC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'W_VDESC'
      Origin = 'W_VDESC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VOUTRO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'W_VOUTRO'
      Origin = 'W_VOUTRO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VIPI: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. IPI'
      FieldName = 'W_VIPI'
      Origin = 'W_VIPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VFRETE: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Frete'
      FieldName = 'W_VFRETE'
      Origin = 'W_VFRETE'
      OnChange = AoAltearFretePeloCampo
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VNF: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Liqu'#237'do'
      FieldName = 'W_VNF'
      Origin = 'W_VNF'
      OnChange = GerarParcela
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataPERC_VST: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VST'
      Origin = 'PERC_VST'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataPERC_VSEG: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VSEG'
      Origin = 'PERC_VSEG'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataPERC_VDESC: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VDESC'
      Origin = 'PERC_VDESC'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataPERC_VOUTRO: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VOUTRO'
      Origin = 'PERC_VOUTRO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataPERC_VIPI: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VIPI'
      Origin = 'PERC_VIPI'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataPERC_VFRETE: TBCDField
      DefaultExpression = '0'
      FieldName = 'PERC_VFRETE'
      Origin = 'PERC_VFRETE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsDataID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object cdsDataID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataID_OPERACAO: TIntegerField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
      Required = True
      OnChange = cdsDataID_OPERACAOChange
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      OnChange = GerarParcela
    end
    object cdsDataID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object cdsDataVL_FRETE_TRANSPORTADORA: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Frete Transportadora'
      FieldName = 'VL_FRETE_TRANSPORTADORA'
      Origin = 'VL_FRETE_TRANSPORTADORA'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsDataDT_VENCIMENTO: TDateField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object cdsDataDT_COMPETENCIA: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia'
      FieldName = 'DT_COMPETENCIA'
      Origin = 'DT_COMPETENCIA'
      OnChange = GerarParcela
    end
    object cdsDataID_PESSOA: TIntegerField
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsDataSTATUS: TStringField
      DefaultExpression = 'ABERTA'
      DisplayLabel = 'Status'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
    end
    object cdsDataTIPO_NF: TStringField
      DisplayLabel = 'Tipo da Nota'
      FieldName = 'TIPO_NF'
      Origin = 'TIPO_NF'
      OnChange = cdsDataTIPO_NFChange
      Size = 15
    end
    object cdsDataJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_OPERACAO: TStringField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_MODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_DESCRICAO_MODELO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_SEQUENCIA_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_SEQUENCIA_CONTA_ANALISE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object cdsDataJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_NOME_PESSOA_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_NOME_PESSOA_USUARIO'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataJOIN_DESCRICAO_OPERACAO_FISCAL: TStringField
      DisplayLabel = 'Opera'#231#227'o Fiscal'
      FieldName = 'JOIN_DESCRICAO_OPERACAO_FISCAL'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDataJOIN_MODELO_MODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'JOIN_MODELO_MODELO'
      Origin = 'MODELO'
      ProviderFlags = []
      OnChange = cdsDataJOIN_MODELO_MODELOChange
      FixedChar = True
      Size = 2
    end
    object cdsDataX_MODFRETE: TIntegerField
      DisplayLabel = 'Modalidade do Frete'
      FieldName = 'X_MODFRETE'
      Origin = 'X_MODFRETE'
    end
    object cdsDataNFE_CHAVE: TStringField
      DisplayLabel = 'Chave NFE'
      FieldName = 'NFE_CHAVE'
      Origin = 'NFE_CHAVE'
      Size = 50
    end
    object cdsDataAR_VERSAO: TStringField
      DisplayLabel = 'Vers'#227'o do Layout'
      FieldName = 'AR_VERSAO'
      Origin = 'AR_VERSAO'
      Size = 12
    end
    object cdsDataAR_TP_AMB: TStringField
      DisplayLabel = 'Ambiente Receita Estadual'
      FieldName = 'AR_TP_AMB'
      Origin = 'AR_TP_AMB'
      Size = 15
    end
    object cdsDataAR_VERAPLIC: TStringField
      DisplayLabel = 'Vers'#227'o do Aplicativo Fiscal'
      FieldName = 'AR_VERAPLIC'
      Origin = 'AR_VERAPLIC'
    end
    object cdsDataAR_XMOTIVO: TStringField
      DisplayLabel = 'Resposta do Servidor da Receita Estadual'
      FieldName = 'AR_XMOTIVO'
      Origin = 'AR_XMOTIVO'
      Size = 255
    end
    object cdsDataAR_CUF: TStringField
      DisplayLabel = 'UF do Servidor da Receita Estadual'
      FieldName = 'AR_CUF'
      Origin = 'AR_CUF'
      Size = 2
    end
    object cdsDataAR_DHRECBTO: TDateTimeField
      DisplayLabel = 'Data e Hora do Recebimento da Resposta'
      FieldName = 'AR_DHRECBTO'
      Origin = 'AR_DHRECBTO'
    end
    object cdsDataAR_INFREC: TStringField
      DisplayLabel = 'Informa'#231#245'es do Recebimento'
      FieldName = 'AR_INFREC'
      Origin = 'AR_INFREC'
      Size = 255
    end
    object cdsDataAR_NREC: TStringField
      DisplayLabel = 'N'#250'mero do Recebimento'
      FieldName = 'AR_NREC'
      Origin = 'AR_NREC'
      Size = 15
    end
    object cdsDataAR_TMED: TIntegerField
      DisplayLabel = 'Tempo M'#233'dio de Resposta do Servidor'
      FieldName = 'AR_TMED'
      Origin = 'AR_TMED'
    end
    object cdsDataAR_PROTNFE: TStringField
      DisplayLabel = 'Dados do Protocolo da NFE'
      FieldName = 'AR_PROTNFE'
      Origin = 'AR_PROTNFE'
      Size = 255
    end
    object cdsDataID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'Opera'#231#227'o Fiscal da NFE'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object cdsDataFORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      OnChange = CondicionarHabilitacaoCamposFinanceiro
      Size = 15
    end
    object cdsDataVL_QUITACAO_DINHEIRO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Entrada/Quita'#231#227'o em Dinheiro'
      FieldName = 'VL_QUITACAO_DINHEIRO'
      Origin = 'VL_QUITACAO_DINHEIRO'
      OnChange = GerarParcela
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataDOC_FEDERAL_PESSOA: TStringField
      DisplayLabel = 'Documento Federal'
      FieldName = 'DOC_FEDERAL_PESSOA'
      Origin = 'DOC_FEDERAL_PESSOA'
      Size = 14
    end
    object cdsDataBO_ENVIO_DPEC: TStringField
      DisplayLabel = 'DPEC Enviado'
      FieldName = 'BO_ENVIO_DPEC'
      Origin = 'BO_ENVIO_DPEC'
      FixedChar = True
      Size = 1
    end
    object cdsDataNR_REG_DPEC: TStringField
      DisplayLabel = 'N'#250'mero do Registro DPEC'
      FieldName = 'NR_REG_DPEC'
      Origin = 'NR_REG_DPEC'
      Size = 255
    end
    object cdsDataDH_REG_DPEC: TDateTimeField
      DisplayLabel = 'Data e Hora do Registro do DPEC'
      FieldName = 'DH_REG_DPEC'
      Origin = 'DH_REG_DPEC'
    end
    object cdsDataW_VBC: TBCDField
      DisplayLabel = 'Total da Base de C'#225'lculo'
      FieldName = 'W_VBC'
      Origin = 'W_VBC'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VICMS: TBCDField
      DisplayLabel = 'Total do Valor ICMS'
      FieldName = 'W_VICMS'
      Origin = 'W_VICMS'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VBCST: TBCDField
      DisplayLabel = 'Total da Base de C'#225'lculo'
      FieldName = 'W_VBCST'
      Origin = 'W_VBCST'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VII: TBCDField
      DisplayLabel = 'Total do Valor II'
      FieldName = 'W_VII'
      Origin = 'W_VII'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VPIS: TBCDField
      DisplayLabel = 'Total do Valor PIS'
      FieldName = 'W_VPIS'
      Origin = 'W_VPIS'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VCOFINS: TBCDField
      DisplayLabel = 'Total do Valor COFINS'
      FieldName = 'W_VCOFINS'
      Origin = 'W_VCOFINS'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VRETPIS: TBCDField
      DisplayLabel = 'Total do Valor PIS Retido'
      FieldName = 'W_VRETPIS'
      Origin = 'W_VRETPIS'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VRETCOFINS: TBCDField
      DisplayLabel = 'Total do Valor COFINS Retido'
      FieldName = 'W_VRETCOFINS'
      Origin = 'W_VRETCOFINS'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VRETCSLL: TBCDField
      DisplayLabel = 'Total do Valor CSLL Retido'
      FieldName = 'W_VRETCSLL'
      Origin = 'W_VRETCSLL'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VBCIRRF: TBCDField
      DisplayLabel = 'Total do Valor Base IRRF'
      FieldName = 'W_VBCIRRF'
      Origin = 'W_VBCIRRF'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VIRRF: TBCDField
      DisplayLabel = 'Total do Valor IRRF'
      FieldName = 'W_VIRRF'
      Origin = 'W_VIRRF'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VBCRETPREV: TBCDField
      DisplayLabel = 'Total da Base de Calculo Retida Prev'
      FieldName = 'W_VBCRETPREV'
      Origin = 'W_VBCRETPREV'
      Precision = 15
      Size = 2
    end
    object cdsDataW_VRETPREV: TBCDField
      DisplayLabel = 'Total do Valor Retido Prev'
      FieldName = 'W_VRETPREV'
      Origin = 'W_VRETPREV'
      Precision = 15
      Size = 2
    end
    object cdsDataPERC_ICMS: TBCDField
      DisplayLabel = '% ICMS'
      FieldName = 'PERC_ICMS'
      Origin = 'PERC_ICMS'
      Precision = 7
    end
    object cdsDataPERC_PIS: TBCDField
      DisplayLabel = '% PIS'
      FieldName = 'PERC_PIS'
      Origin = 'PERC_PIS'
      Precision = 7
    end
    object cdsDataPERC_COFINS: TBCDField
      DisplayLabel = '% COFINS'
      FieldName = 'PERC_COFINS'
      Origin = 'PERC_COFINS'
      Precision = 7
    end
    object cdsDatafdqNotaFiscalItem: TDataSetField
      FieldName = 'fdqNotaFiscalItem'
    end
    object cdsDatafdqNotaFiscalParcela: TDataSetField
      FieldName = 'fdqNotaFiscalParcela'
    end
    object cdsDataAR_CSTAT: TStringField
      DisplayLabel = 'Status da NFE'
      FieldName = 'AR_CSTAT'
      Origin = 'AR_CSTAT'
      Size = 25
    end
    object cdsDataDOCUMENTO_FISCAL_XML: TBlobField
      DisplayLabel = 'XML'
      FieldName = 'DOCUMENTO_FISCAL_XML'
      Origin = 'DOCUMENTO_FISCAL_XML'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42214.009667812500000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      BuiltInReportLink = True
    end
  end
  object cdsNotaFiscalItem: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqNotaFiscalItem
    Params = <>
    AfterPost = cdsNotaFiscalItemAfterPost
    AfterDelete = cdsNotaFiscalItemAfterDelete
    OnNewRecord = cdsNotaFiscalItemNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 724
    Top = 80
    object cdsNotaFiscalItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsNotaFiscalItemH_NITEM: TIntegerField
      DisplayLabel = 'Item'
      FieldName = 'H_NITEM'
      Origin = 'H_NITEM'
      Required = True
    end
    object cdsNotaFiscalItemI_QCOM: TBCDField
      DefaultExpression = '1'
      DisplayLabel = 'Quantidade'
      FieldName = 'I_QCOM'
      Origin = 'I_QCOM'
      Required = True
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemI_VUNCOM: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'UN'
      FieldName = 'I_VUNCOM'
      Origin = 'I_VUNCOM'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 21
      Size = 10
    end
    object cdsNotaFiscalItemI_VDESC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Desconto'
      FieldName = 'I_VDESC'
      Origin = 'I_VDESC'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemI_VFRETE: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Frete'
      FieldName = 'I_VFRETE'
      Origin = 'I_VFRETE'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemI_VSEG: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Seguro'
      FieldName = 'I_VSEG'
      Origin = 'I_VSEG'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemI_VOUTRO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Acr'#233'scimo'
      FieldName = 'I_VOUTRO'
      Origin = 'I_VOUTRO'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemO_VIPI: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. IPI'
      FieldName = 'O_VIPI'
      Origin = 'O_VIPI'
      OnChange = CalcularTotaisDoItem
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemID_PRODUTO: TIntegerField
      DisplayLabel = 'Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsNotaFiscalItemJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsNotaFiscalItemVL_LIQUIDO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemVL_CUSTO_IMPOSTO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Custo + Imposto'
      FieldName = 'VL_CUSTO_IMPOSTO'
      Origin = 'VL_CUSTO_IMPOSTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemID_NOTA_FISCAL: TIntegerField
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'ID_NOTA_FISCAL'
      Origin = 'ID_NOTA_FISCAL'
    end
    object cdsNotaFiscalItemPERC_DESCONTO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemPERC_ACRESCIMO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Acrescimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemPERC_FRETE: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Frete'
      FieldName = 'PERC_FRETE'
      Origin = 'PERC_FRETE'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemPERC_SEGURO: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Seguro'
      FieldName = 'PERC_SEGURO'
      Origin = 'PERC_SEGURO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemPERC_IPI: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% IPI'
      FieldName = 'PERC_IPI'
      Origin = 'PERC_IPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object cdsNotaFiscalItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      DisplayFormat = '###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalItemVL_CUSTO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      Origin = 'VL_CUSTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalItemVL_CUSTO_OPERACIONAL: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Custo Operacional'
      FieldName = 'VL_CUSTO_OPERACIONAL'
      Origin = 'VL_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalItemPERC_CUSTO_OPERACIONAL: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Custo Operacional'
      FieldName = 'PERC_CUSTO_OPERACIONAL'
      Origin = 'PERC_CUSTO_OPERACIONAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalItemI_XPROD: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'I_XPROD'
      Origin = 'I_XPROD'
      Size = 120
    end
    object cdsNotaFiscalItemN_VICMS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl ICMS'
      FieldName = 'N_VICMS'
      Origin = 'N_VICMS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_VBCST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Base de C'#225'lculo CST'
      FieldName = 'N_VBCST'
      Origin = 'N_VBCST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_PREDBCST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% Redu'#231#227'o da Base de C'#225'lculo da CST'
      FieldName = 'N_PREDBCST'
      Origin = 'N_PREDBCST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 5
      Size = 2
    end
    object cdsNotaFiscalItemN_MOTDESICMS: TIntegerField
      DisplayLabel = 'Motivo da Desonera'#231#227'o do ICMS'
      FieldName = 'N_MOTDESICMS'
      Origin = 'N_MOTDESICMS'
    end
    object cdsNotaFiscalItemN_PMVAST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% da MV de Antecipa'#231#227'o da ST'
      FieldName = 'N_PMVAST'
      Origin = 'N_PMVAST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_MODBCST: TIntegerField
      DisplayLabel = 'Motivo da Desonera'#231#227'o da Base de C'#225'lculo da CST'
      FieldName = 'N_MODBCST'
      Origin = 'N_MODBCST'
    end
    object cdsNotaFiscalItemI_NCM: TIntegerField
      DisplayLabel = 'NCM'
      FieldName = 'I_NCM'
      Origin = 'I_NCM'
    end
    object cdsNotaFiscalItemI_EXTIPI: TStringField
      DisplayLabel = 'Extens'#227'o do IPI'
      FieldName = 'I_EXTIPI'
      Origin = 'I_EXTIPI'
      Size = 3
    end
    object cdsNotaFiscalItemI_CFOP: TIntegerField
      DisplayLabel = 'CFOP'
      FieldName = 'I_CFOP'
      Origin = 'I_CFOP'
    end
    object cdsNotaFiscalItemI_CEANTRIB: TStringField
      DisplayLabel = 'C'#243'digo de Barra do Produto Tributado'
      FieldName = 'I_CEANTRIB'
      Origin = 'I_CEANTRIB'
      Size = 13
    end
    object cdsNotaFiscalItemI_UTRIB: TStringField
      DisplayLabel = 'Unidade Tribut'#225'vel'
      FieldName = 'I_UTRIB'
      Origin = 'I_UTRIB'
      Size = 6
    end
    object cdsNotaFiscalItemI_QTRIB: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Quantidade Tribut'#225'vel'
      FieldName = 'I_QTRIB'
      Origin = 'I_QTRIB'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemI_VUNTRIB: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Unit'#225'rio Tribut'#225'vel'
      FieldName = 'I_VUNTRIB'
      Origin = 'I_VUNTRIB'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 21
      Size = 10
    end
    object cdsNotaFiscalItemI_INDTOT: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Tipo de Composi'#231#227'o do Total da NFe'
      FieldName = 'I_INDTOT'
      Origin = 'I_INDTOT'
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object cdsNotaFiscalItemI_NVE: TStringField
      DisplayLabel = 'Nomenclatura de Valor Aduaneiro e Estat'#237'stica'
      FieldName = 'I_NVE'
      Origin = 'I_NVE'
      Size = 6
    end
    object cdsNotaFiscalItemI_CEAN: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'I_CEAN'
      Origin = 'I_CEAN'
      Size = 14
    end
    object cdsNotaFiscalItemN_ORIG: TIntegerField
      DisplayLabel = 'Origem da Mercadoria'
      FieldName = 'N_ORIG'
      Origin = 'N_ORIG'
    end
    object cdsNotaFiscalItemN_CST: TIntegerField
      DisplayLabel = 'Tributa'#231#227'o pelo ICMS'
      FieldName = 'N_CST'
      Origin = 'N_CST'
    end
    object cdsNotaFiscalItemN_MODBC: TIntegerField
      DisplayLabel = 'Modalidade da Base de C'#225'lculo'
      FieldName = 'N_MODBC'
      Origin = 'N_MODBC'
    end
    object cdsNotaFiscalItemN_PREDBC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Percentual da Redu'#231#227'o de BC'
      FieldName = 'N_PREDBC'
      Origin = 'N_PREDBC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 5
      Size = 2
    end
    object cdsNotaFiscalItemN_VBC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da BC do ICMS'
      FieldName = 'N_VBC'
      Origin = 'N_VBC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_PICMS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% ICMS'
      FieldName = 'N_PICMS'
      Origin = 'N_PICMS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 5
      Size = 2
    end
    object cdsNotaFiscalItemN_VICMSOP: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor do ICMS da Opera'#231#227'o'
      FieldName = 'N_VICMSOP'
      Origin = 'N_VICMSOP'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 5
      Size = 2
    end
    object cdsNotaFiscalItemN_UFST: TStringField
      DisplayLabel = 'UF ST'
      FieldName = 'N_UFST'
      Origin = 'N_UFST'
      Size = 2
    end
    object cdsNotaFiscalItemN_PDIF: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Percentual do Diferimento'
      FieldName = 'N_PDIF'
      Origin = 'N_PDIF'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 5
      Size = 2
    end
    object cdsNotaFiscalItemN_VLICMSDIF: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor do Diferimento ICMS'
      FieldName = 'N_VLICMSDIF'
      Origin = 'N_VLICMSDIF'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemN_VICMSDESON: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor do ICMS de Disonera'#231#227'o'
      FieldName = 'N_VICMSDESON'
      Origin = 'N_VICMSDESON'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemN_PBCOP: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% da Base de C'#225'lculo da Opera'#231#227'o Pr'#243'pria'
      FieldName = 'N_PBCOP'
      Origin = 'N_PBCOP'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_BCSTRET: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da BC do ICMS'
      FieldName = 'N_BCSTRET'
      Origin = 'N_BCSTRET'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_PCREDSN: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Al'#237'quota aplic'#225'vel de c'#225'lculo do cr'#233'dito (Simples Nacional).'
      FieldName = 'N_PCREDSN'
      Origin = 'N_PCREDSN'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemN_VBCSTDEST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da BC do ICMS ST da UF destino'
      FieldName = 'N_VBCSTDEST'
      Origin = 'N_VBCSTDEST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_VICMSSTDEST: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da do ICMS ST da UF destino'
      FieldName = 'N_VICMSSTDEST'
      Origin = 'N_VICMSSTDEST'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemN_VCREDICMSSN: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor do Cr'#233'dito ICMS SSN'
      FieldName = 'N_VCREDICMSSN'
      Origin = 'N_VCREDICMSSN'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemO_CLENQ: TStringField
      DisplayLabel = 'Classe de enquadramento do IPI para Cigarros e Bebidas'
      FieldName = 'O_CLENQ'
      Origin = 'O_CLENQ'
      Size = 5
    end
    object cdsNotaFiscalItemO_CNPJPROD: TStringField
      DisplayLabel = 'CNPJ do Produtor da Mercadoria'
      FieldName = 'O_CNPJPROD'
      Origin = 'O_CNPJPROD'
      Size = 14
    end
    object cdsNotaFiscalItemO_CSELO: TStringField
      DisplayLabel = 'C'#243'digo do selo de controle IPI'
      FieldName = 'O_CSELO'
      Origin = 'O_CSELO'
      Size = 60
    end
    object cdsNotaFiscalItemO_QSELO: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Quantidade de selo de controle'
      FieldName = 'O_QSELO'
      Origin = 'O_QSELO'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalItemO_CENQ: TStringField
      DisplayLabel = 'C'#243'digo de Enquadramento Legal do IPI'
      FieldName = 'O_CENQ'
      Origin = 'O_CENQ'
      Size = 3
    end
    object cdsNotaFiscalItemO_CST: TIntegerField
      DisplayLabel = 'CST'
      FieldName = 'O_CST'
      Origin = 'O_CST'
    end
    object cdsNotaFiscalItemO_VBC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da BC do IPI'
      FieldName = 'O_VBC'
      Origin = 'O_VBC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemO_QUNID: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Quantidade Total na Unidade Padr'#227'o'
      FieldName = 'O_QUNID'
      Origin = 'O_QUNID'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 12
    end
    object cdsNotaFiscalItemO_VUNID: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor por Unidade Tribut'#225'vel'
      FieldName = 'O_VUNID'
      Origin = 'O_VUNID'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 11
    end
    object cdsNotaFiscalItemO_PIPI: TBCDField
      DefaultExpression = '0'
      DisplayLabel = '% IPI'
      FieldName = 'O_PIPI'
      Origin = 'O_PIPI'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemQ_CST: TIntegerField
      DisplayLabel = 'CST PIS'
      FieldName = 'Q_CST'
      Origin = 'Q_CST'
    end
    object cdsNotaFiscalItemQ_VBC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da Base de C'#225'lculo do PIS'
      FieldName = 'Q_VBC'
      Origin = 'Q_VBC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemQ_PPIS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Al'#237'quota do PIS (em percentual)'
      FieldName = 'Q_PPIS'
      Origin = 'Q_PPIS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemQ_VPIS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor do PIS'
      FieldName = 'Q_VPIS'
      Origin = 'Q_VPIS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemQ_QBCPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Quantidade Vendida'
      FieldName = 'Q_QBCPROD'
      Origin = 'Q_QBCPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 12
    end
    object cdsNotaFiscalItemQ_VALIQPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Al'#237'quota do PIS (em reais)'
      FieldName = 'Q_VALIQPROD'
      Origin = 'Q_VALIQPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 11
    end
    object cdsNotaFiscalItemS_CST: TIntegerField
      DisplayLabel = 'C'#243'digo de Situa'#231#227'o Tribut'#225'ria da COFINS'
      FieldName = 'S_CST'
      Origin = 'S_CST'
    end
    object cdsNotaFiscalItemS_VBC: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor da Base de C'#225'lculo da COFINS'
      FieldName = 'S_VBC'
      Origin = 'S_VBC'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemS_PCOFINS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Al'#237'quota da COFINS (em percentual)'
      FieldName = 'S_PCOFINS'
      Origin = 'S_PCOFINS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemS_QBCPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Quantidade Vendida'
      FieldName = 'S_QBCPROD'
      Origin = 'S_QBCPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 12
    end
    object cdsNotaFiscalItemS_VALIQPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Al'#237'quota da COFINS (em reais)'
      FieldName = 'S_VALIQPROD'
      Origin = 'S_VALIQPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 11
    end
    object cdsNotaFiscalItemS_VCOFINS: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Vl. Cofins'
      FieldName = 'S_VCOFINS'
      Origin = 'S_VCOFINS'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 13
      Size = 2
    end
    object cdsNotaFiscalItemJOIN_BO_PRODUTO_AGRUPADOR_PRODUTO: TStringField
      DisplayLabel = 'Produto Agrupador?'
      FieldName = 'JOIN_BO_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'BO_PRODUTO_AGRUPADOR'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object cdsNotaFiscalItemJOIN_ID_PRODUTO_AGRUPADOR_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto Agrupador'
      FieldName = 'JOIN_ID_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'ID_PRODUTO_AGRUPADOR'
      ProviderFlags = []
    end
    object cdsNotaFiscalItemJOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO: TStringField
      FieldName = 'JOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO'
      Origin = 'JOIN_DESCRICAO_PRODUTO_AGRUPADOR_PRODUTO'
      ProviderFlags = []
      ReadOnly = True
      Size = 94
    end
    object cdsNotaFiscalItemI_UCOM: TStringField
      DisplayLabel = 'Unidade'
      FieldName = 'I_UCOM'
      Origin = 'I_UCOM'
      Size = 6
    end
    object cdsNotaFiscalItemI_VPROD: TBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Valor Total Bruto'
      FieldName = 'I_VPROD'
      Origin = 'I_VPROD'
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemI_XPED: TStringField
      DisplayLabel = 'Pedido de Compra'
      FieldName = 'I_XPED'
      Origin = 'I_XPED'
      Size = 15
    end
    object cdsNotaFiscalItemI_NITEMPED: TIntegerField
      DisplayLabel = 'Item do Pedido de Compra'
      FieldName = 'I_NITEMPED'
      Origin = 'I_NITEMPED'
    end
    object cdsNotaFiscalItemN_VICMSSTRET: TBCDField
      DisplayLabel = 'Valor do ICMS ST Retido'
      FieldName = 'N_VICMSSTRET'
      Origin = 'N_VICMSSTRET'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemID_IMPOSTO_ICMS: TIntegerField
      FieldName = 'ID_IMPOSTO_ICMS'
      Origin = 'ID_IMPOSTO_ICMS'
      DisplayFormat = 'C'#243'digo do Imposto ICMS'
    end
    object cdsNotaFiscalItemID_IMPOSTO_PIS_COFINS: TIntegerField
      DisplayLabel = 'C'#243'digo do Imposto PIS Cofins'
      FieldName = 'ID_IMPOSTO_PIS_COFINS'
      Origin = 'ID_IMPOSTO_PIS_COFINS'
    end
    object cdsNotaFiscalItemID_OPERACAO_FISCAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o Fiscal'
      FieldName = 'ID_OPERACAO_FISCAL'
      Origin = 'ID_OPERACAO_FISCAL'
    end
    object cdsNotaFiscalItemN_NVICMSST: TBCDField
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'N_NVICMSST'
      Origin = 'N_NVICMSST'
      Precision = 15
      Size = 2
    end
    object cdsNotaFiscalItemID_IMPOSTO_IPI: TIntegerField
      DisplayLabel = 'C'#243'digo do Imposto IPI'
      FieldName = 'ID_IMPOSTO_IPI'
      Origin = 'ID_IMPOSTO_IPI'
    end
    object cdsNotaFiscalItemPERC_ICMS_ST: TBCDField
      DisplayLabel = '% ICMS ST'
      FieldName = 'PERC_ICMS_ST'
      Origin = 'PERC_ICMS_ST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 7
    end
    object cdsNotaFiscalItemN_PICMSST: TBCDField
      DisplayLabel = '% ICMS ST'
      FieldName = 'N_PICMSST'
      Origin = 'N_PICMSST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
    end
    object cdsNotaFiscalItemN_VICMSST: TBCDField
      DisplayLabel = 'Vl. ICMS ST'
      FieldName = 'N_VICMSST'
      Origin = 'N_VICMSST'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 15
    end
  end
  object cdsNotaFiscalParcela: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqNotaFiscalParcela
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 816
    Top = 80
    object cdsNotaFiscalParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsNotaFiscalParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsNotaFiscalParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      OnChange = ModificarParcelaAposParcelamento
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsNotaFiscalParcelaQT_PARCELA: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Parcelas'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
      DisplayFormat = '00'
    end
    object cdsNotaFiscalParcelaNR_PARCELA: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Parcela'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
      DisplayFormat = '00'
    end
    object cdsNotaFiscalParcelaDT_VENCIMENTO: TDateField
      Alignment = taCenter
      DisplayLabel = 'Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
      OnChange = cdsNotaFiscalParcelaDT_VENCIMENTOChange
    end
    object cdsNotaFiscalParcelaID_NOTA_FISCAL: TIntegerField
      DisplayLabel = 'Nota Fiscal'
      FieldName = 'ID_NOTA_FISCAL'
      Origin = 'ID_NOTA_FISCAL'
    end
    object cdsNotaFiscalParcelaIC_DIAS: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Dias'
      FieldKind = fkInternalCalc
      FieldName = 'IC_DIAS'
      OnChange = cdsNotaFiscalParcelaIC_DIASChange
    end
    object cdsNotaFiscalParcelaOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 255
    end
    object cdsNotaFiscalParcelaCHEQUE_SACADO: TStringField
      DisplayLabel = 'Sacado do Cheque'
      FieldName = 'CHEQUE_SACADO'
      Origin = 'CHEQUE_SACADO'
      Size = 255
    end
    object cdsNotaFiscalParcelaCHEQUE_DOC_FEDERAL: TStringField
      DisplayLabel = 'Doc. Federal Cheque'
      FieldName = 'CHEQUE_DOC_FEDERAL'
      Origin = 'CHEQUE_DOC_FEDERAL'
      Size = 14
    end
    object cdsNotaFiscalParcelaCHEQUE_BANCO: TStringField
      DisplayLabel = 'Banco Cheque'
      FieldName = 'CHEQUE_BANCO'
      Origin = 'CHEQUE_BANCO'
      Size = 100
    end
    object cdsNotaFiscalParcelaCHEQUE_DT_EMISSAO: TDateField
      DisplayLabel = 'Dt. Emiss'#227'o Cheque'
      FieldName = 'CHEQUE_DT_EMISSAO'
      Origin = 'CHEQUE_DT_EMISSAO'
    end
    object cdsNotaFiscalParcelaCHEQUE_AGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia Cheque'
      FieldName = 'CHEQUE_AGENCIA'
      Origin = 'CHEQUE_AGENCIA'
      Size = 15
    end
    object cdsNotaFiscalParcelaCHEQUE_CONTA_CORRENTE: TStringField
      DisplayLabel = 'Conta Corrente Cheque'
      FieldName = 'CHEQUE_CONTA_CORRENTE'
      Origin = 'CHEQUE_CONTA_CORRENTE'
      Size = 15
    end
    object cdsNotaFiscalParcelaCHEQUE_NUMERO: TIntegerField
      DisplayLabel = 'N'#250'mero Cheque'
      FieldName = 'CHEQUE_NUMERO'
      Origin = 'CHEQUE_NUMERO'
    end
    object cdsNotaFiscalParcelaID_OPERADORA_CARTAO: TIntegerField
      DisplayLabel = 'Operadora Cart'#227'o'
      FieldName = 'ID_OPERADORA_CARTAO'
      Origin = 'ID_OPERADORA_CARTAO'
    end
    object cdsNotaFiscalParcelaID_FORMA_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo da Forma de Pagamento'
      FieldName = 'ID_FORMA_PAGAMENTO'
      Origin = 'ID_FORMA_PAGAMENTO'
    end
    object cdsNotaFiscalParcelaID_CARTEIRA: TIntegerField
      DisplayLabel = 'C'#243'digo da Carteira'
      FieldName = 'ID_CARTEIRA'
      Origin = 'ID_CARTEIRA'
      Required = True
      OnChange = AoAlterarCarteira
    end
    object cdsNotaFiscalParcelaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsNotaFiscalParcelaJOIN_DESCRICAO_CARTEIRA: TStringField
      DisplayLabel = 'Carteira'
      FieldName = 'JOIN_DESCRICAO_CARTEIRA'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
  end
  object dsNotaFiscalParcela: TDataSource
    DataSet = cdsNotaFiscalParcela
    Left = 846
    Top = 80
  end
end
