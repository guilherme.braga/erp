unit uCadImpostoPISCofins;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox, Vcl.ExtCtrls,
  JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, cxMaskEdit, cxDropDownEdit,
  cxBlobEdit, cxDBEdit, uGBDBBlobEdit, uGBDBTextEdit, cxTextEdit,
  uGBDBTextEditPK, Vcl.StdCtrls, cxCalc, uGBDBCalcEdit, cxButtonEdit,
  uGBDBButtonEditFK, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, cxSplitter, JvExControls, JvButton,
  JvTransparentButton, uGBPanel;

type
  TCadImpostoPISCofins = class(TFrmCadastro_Padrao)
    Label2: TLabel;
    ePkCodig: TgbDBTextEditPK;
    cdsDataID: TAutoIncField;
    cdsDataPERC_ALIQUOTA_PIS: TFMTBCDField;
    cdsDataPERC_ALIQUOTA_COFINS: TFMTBCDField;
    cdsDataPERC_ALIQUOTA_PIS_ST: TFMTBCDField;
    cdsDataPERC_ALIQUOTA_COFINS_ST: TFMTBCDField;
    cdsDataID_CST_PIS_COFINS: TIntegerField;
    cdsDataJOIN_DESCRICAO_CST_PIS_COFINS: TStringField;
    Label1: TLabel;
    Label3: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    gbDBCalcEdit1: TgbDBCalcEdit;
    Label4: TLabel;
    gbDBCalcEdit2: TgbDBCalcEdit;
    Label5: TLabel;
    gbDBCalcEdit3: TgbDBCalcEdit;
    gbDBCalcEdit4: TgbDBCalcEdit;
    Label6: TLabel;
    cdsDataJOIN_CODIGO_CST_PIS_COFINS: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadImpostoPISCofins);
finalization
  UnRegisterClass(TCadImpostoPISCofins);

end.
