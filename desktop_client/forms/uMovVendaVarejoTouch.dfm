inherited MovVendaVarejoTouch: TMovVendaVarejoTouch
  Caption = 'Venda Touch'
  ClientHeight = 464
  ClientWidth = 892
  PopupMenu = pmTela
  ExplicitWidth = 908
  ExplicitHeight = 503
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 892
    ExplicitWidth = 892
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'BarInformacoes'
        end
        item
          ToolbarName = 'barConsultar'
        end
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    PanelStyle.OfficeBackgroundKind = pobkOffice11Color
    ExplicitWidth = 892
    ExplicitHeight = 362
    Height = 362
    Width = 892
    object cxpcMain: TcxPageControl
      Left = 2
      Top = 2
      Width = 888
      Height = 358
      Align = alClient
      Focusable = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = tsVenda
      Properties.CustomButtons.Buttons = <>
      Properties.NavigatorPosition = npLeftTop
      Properties.Options = [pcoAlwaysShowGoDialogButton, pcoGradientClientArea, pcoRedrawOnResize]
      Properties.Style = 8
      LookAndFeel.Kind = lfOffice11
      ClientRectBottom = 358
      ClientRectRight = 888
      ClientRectTop = 24
      object tsPesquisa: TcxTabSheet
        Caption = 'Pesquisa'
        ImageIndex = 2
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGridPesquisaPadrao: TcxGrid
          Left = 0
          Top = 0
          Width = 888
          Height = 334
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Images = DmAcesso.cxImage16x16
          ParentFont = False
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object viewPesquisa: TcxGridDBBandedTableView
            OnDblClick = viewPesquisaDblClick
            OnKeyDown = viewPesquisaKeyDown
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Images = DmAcesso.cxImage16x16
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsSearch
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.ApplyChanges = fracImmediately
            Images = DmAcesso.cxImage16x16
            OptionsBehavior.NavigatorHints = True
            OptionsBehavior.ExpandMasterRowOnDblClick = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.BandMoving = False
            OptionsCustomize.NestedBands = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.ShowColumnFilterButtons = sfbAlways
            OptionsView.BandHeaders = False
            Styles.ContentOdd = DmAcesso.OddColor
            Bands = <
              item
              end>
          end
          object cxGridPesquisaPadraoLevel1: TcxGridLevel
            GridView = viewPesquisa
          end
        end
      end
      object tsVenda: TcxTabSheet
        Caption = 'Venda'
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnTopo: TgbPanel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -16
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 328
          Width = 882
        end
        object PnCentro: TgbPanel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 328
          Width = 882
          object PnProdutos: TgbPanel
            Left = 560
            Top = 2
            Align = alRight
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Transparent = True
            Height = 324
            Width = 320
            object cxGridProdutos: TcxGrid
              Left = 2
              Top = 49
              Width = 316
              Height = 239
              Align = alClient
              BevelInner = bvNone
              BevelOuter = bvNone
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object viewProduto: TcxGridDBBandedTableView
                OnDblClick = cxGridProdutosViewDblClick
                OnKeyDown = cxGridProdutosViewKeyDown
                Navigator.Buttons.CustomButtons = <>
                FilterBox.CustomizeDialog = False
                DataController.DataSource = dsVendaItem
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsSelection.HideSelection = True
                OptionsSelection.InvertSelect = False
                OptionsView.FocusRect = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.Footer = True
                OptionsView.FooterMultiSummaries = True
                OptionsView.GridLineColor = clInfoBk
                OptionsView.GroupByBox = False
                OptionsView.RowSeparatorColor = clBlack
                OptionsView.BandHeaders = False
                Styles.Background = cxStyleYellowStrong
                Styles.Content = cxStyleYellowStrong
                Styles.ContentEven = cxStyleYellowStrong
                Styles.ContentOdd = cxStyleYellowStrong
                Styles.Selection = cxStyleYellowTooStrong
                Bands = <
                  item
                  end>
                object viewProdutoID_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ID_PRODUTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 62
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object viewProdutoJOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_DESCRICAO_PRODUTO'
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 207
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object viewProdutoQUANTIDADE: TcxGridDBBandedColumn
                  Caption = 'Qtde'
                  DataBinding.FieldName = 'QUANTIDADE'
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 47
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object viewProdutoVL_LIQUIDO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_LIQUIDO'
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 124
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
                object viewProdutoNR_ITEM: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'NR_ITEM'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object viewProdutoVL_DESCONTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_DESCONTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 89
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object viewProdutoVL_ACRESCIMO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_ACRESCIMO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object viewProdutoVL_BRUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'VL_BRUTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 88
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object viewProdutoPERC_DESCONTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'PERC_DESCONTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 8
                  Position.RowIndex = 0
                end
                object viewProdutoPERC_ACRESCIMO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'PERC_ACRESCIMO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 9
                  Position.RowIndex = 0
                end
                object viewProdutoJOIN_SIGLA_UNIDADE_ESTOQUE: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 10
                  Position.RowIndex = 0
                end
                object viewProdutoJOIN_ESTOQUE_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_ESTOQUE_PRODUTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Position.BandIndex = 0
                  Position.ColIndex = 11
                  Position.RowIndex = 0
                end
                object viewProdutoJOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
                  Visible = False
                  Options.Editing = False
                  Options.Sorting = False
                  Width = 144
                  Position.BandIndex = 0
                  Position.ColIndex = 12
                  Position.RowIndex = 0
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = viewProduto
              end
            end
            object cxGroupBox18: TcxGroupBox
              Left = 2
              Top = 2
              Align = alTop
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TransparentBorder = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Height = 47
              Width = 316
              object cxGroupBox27: TcxGroupBox
                Left = 210
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton21: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActAdicionar2
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 52
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 52
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
                object JvTransparentButton22: TJvTransparentButton
                  Left = 2
                  Top = 21
                  Width = 20
                  Height = 17
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList22x22
                  Images.ActiveIndex = 1
                  Images.DownImage = cxImageList22x22
                  Images.DownIndex = 1
                end
              end
              object cxGroupBox28: TcxGroupBox
                Left = 262
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton23: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActSubtrair2
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 52
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 52
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
                object JvTransparentButton24: TJvTransparentButton
                  Left = 2
                  Top = 21
                  Width = 20
                  Height = 17
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList22x22
                  Images.ActiveIndex = 6
                  Images.DownImage = cxImageList22x22
                  Images.DownIndex = 6
                end
              end
              object cxGroupBox3: TcxGroupBox
                Left = 2
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton1: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActCancelarItem
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 18
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 53
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
              end
              object cxGroupBox4: TcxGroupBox
                Left = 54
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 3
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton2: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActAlterarQuantidade
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 17
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 53
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
              end
              object cxGroupBox6: TcxGroupBox
                Left = 158
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 4
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton3: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActSubtrair1
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 55
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 55
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
                object JvTransparentButton4: TJvTransparentButton
                  Left = 2
                  Top = 21
                  Width = 20
                  Height = 17
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList22x22
                  Images.ActiveIndex = 6
                  Images.DownImage = cxImageList22x22
                  Images.DownIndex = 6
                end
              end
              object cxGroupBox7: TcxGroupBox
                Left = 106
                Top = 2
                Align = alLeft
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 5
                Transparent = True
                Height = 43
                Width = 52
                object JvTransparentButton5: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 50
                  Height = 39
                  Action = ActAdicionar1
                  Align = alLeft
                  AutoGray = False
                  FrameStyle = fsNone
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 55
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 55
                  ExplicitLeft = 3
                  ExplicitHeight = 36
                end
                object JvTransparentButton6: TJvTransparentButton
                  Left = 2
                  Top = 21
                  Width = 20
                  Height = 17
                  AutoGray = False
                  FrameStyle = fsNone
                  OnClick = ActAdicionar1Execute
                  Images.ActiveImage = cxImageList22x22
                  Images.ActiveIndex = 1
                  Images.DownImage = cxImageList22x22
                  Images.DownIndex = 1
                end
              end
            end
            object gbPanel2: TgbPanel
              Left = 2
              Top = 288
              Align = alBottom
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 2
              Transparent = True
              Height = 34
              Width = 316
              object BtnGridUpComandaItem: TJvTransparentButton
                Left = 34
                Top = 2
                Width = 32
                Height = 30
                Hint = 'Produto Anterior'
                Align = alLeft
                AutoGray = False
                FrameStyle = fsNone
                OnClick = MoveGridUpDown
                Images.ActiveImage = cxImageList32x32
                Images.ActiveIndex = 47
                Images.DownImage = cxImageList32x32
                Images.DownIndex = 48
                ExplicitLeft = 26
                ExplicitTop = 6
              end
              object BtnGridDownComandaItem: TJvTransparentButton
                Left = 2
                Top = 2
                Width = 32
                Height = 30
                Hint = 'Pr'#243'ximo Produto'
                Align = alLeft
                AutoGray = False
                FrameStyle = fsNone
                OnClick = MoveGridUpDown
                Images.ActiveImage = cxImageList32x32
                Images.ActiveIndex = 41
                Images.DownImage = cxImageList32x32
                Images.DownIndex = 42
                ExplicitLeft = 26
                ExplicitTop = 1
              end
              object cxDBLabel1: TcxDBLabel
                Left = 305
                Top = 2
                Align = alRight
                AutoSize = True
                DataBinding.DataField = 'VL_TOTAL_PRODUTO'
                DataBinding.DataSource = dsVenda
                ParentFont = False
                Properties.Alignment.Horz = taCenter
                Properties.Alignment.Vert = taVCenter
                Style.BorderStyle = ebsNone
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -16
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                Transparent = True
                ExplicitLeft = 260
                AnchorX = 310
                AnchorY = 17
              end
              object cxLabel1: TcxLabel
                Left = 66
                Top = 2
                Align = alClient
                AutoSize = False
                Caption = 'Total R$'
                ParentFont = False
                Style.BorderStyle = ebsNone
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -16
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                Properties.Alignment.Horz = taRightJustify
                Properties.Alignment.Vert = taVCenter
                Transparent = True
                ExplicitWidth = 149
                Height = 30
                Width = 239
                AnchorX = 305
                AnchorY = 17
              end
            end
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 2
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            Height = 324
            Width = 558
            object cxGBGrupoProduto: TcxGroupBox
              Left = 2
              Top = 229
              Align = alBottom
              PanelStyle.Active = True
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TransparentBorder = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              Height = 93
              Width = 554
              object PnPaginacaoGrupo: TcxGroupBox
                Left = 520
                Top = 22
                Align = alRight
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 69
                Width = 32
                object BtnGridDownGrupoProdutos: TJvTransparentButton
                  Left = 2
                  Top = 34
                  Width = 28
                  Height = 33
                  Hint = 'Pr'#243'xima p'#225'gina'
                  Align = alBottom
                  AutoGray = False
                  FrameStyle = fsNone
                  OnClick = MoveGridUpDown
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 41
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 42
                  ExplicitTop = 88
                end
                object BtnGridUpGrupoProdutos: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 28
                  Height = 33
                  Hint = 'P'#225'gina anterior'
                  Align = alTop
                  AutoGray = False
                  FrameStyle = fsNone
                  OnClick = MoveGridUpDown
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 47
                  Images.GrayIndex = 43
                  Images.DisabledIndex = 43
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 48
                  Images.HotIndex = 43
                end
              end
              object cxGroupBox1: TcxGroupBox
                Left = 2
                Top = 2
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Transparent = True
                Height = 20
                Width = 550
                object cxLabel9: TcxLabel
                  Left = 2
                  Top = 2
                  Align = alLeft
                  Caption = 'Grupos de Produto'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clNavy
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taLeftJustify
                  Properties.Alignment.Vert = taBottomJustify
                  Transparent = True
                  AnchorY = 18
                end
              end
              object cxGrid1: TcxGrid
                Left = 2
                Top = 22
                Width = 518
                Height = 69
                Align = alClient
                BorderStyle = cxcbsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
                LookAndFeel.Kind = lfOffice11
                LookAndFeel.NativeStyle = False
                object cxGridDBTableView1: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.ColumnAutoWidth = True
                  OptionsView.GroupByBox = False
                  OptionsView.GroupRowStyle = grsOffice11
                end
                object cxGridDBCardView1: TcxGridDBCardView
                  Navigator.Buttons.CustomButtons = <>
                  OnCellClick = cxGridDBCardView1CellClick
                  DataController.DataSource = dsTodosGruposProduto
                  DataController.KeyFieldNames = 'ID'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  LayoutDirection = ldVertical
                  OptionsBehavior.CopyCaptionsToClipboard = False
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsBehavior.ImmediateEditor = False
                  OptionsBehavior.RowCaptionHints = False
                  OptionsCustomize.RowFiltering = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.HideSelection = True
                  OptionsSelection.CardBorderSelection = False
                  OptionsView.CaptionSeparator = #0
                  OptionsView.CardAutoWidth = True
                  OptionsView.CardBorderWidth = 1
                  OptionsView.CardIndent = 2
                  OptionsView.CardWidth = 130
                  OptionsView.CellAutoHeight = True
                  Styles.Background = cxStyleYellowStrongNegrito
                  Styles.Content = cxStyleYellowStrongNegrito
                  Styles.ContentEven = cxStyleYellowStrongNegrito
                  Styles.ContentOdd = cxStyleYellowStrongNegrito
                  Styles.Selection = cxStyleYellowTooStrong
                  Styles.CardBorder = cxStyleYellowStrongNegrito
                  Styles.CategoryRow = cxStyleYellowStrongNegrito
                  object cxGridDBCardViewRow1: TcxGridDBCardViewRow
                    Caption = ' '
                    DataBinding.FieldName = 'DESCRICAO'
                    CaptionAlignmentHorz = taCenter
                    Position.BeginsLayer = True
                    Position.LineCount = 2
                  end
                end
                object cxGridGrupoProduto: TcxGridLevel
                  GridView = cxGridDBCardView1
                end
              end
            end
            object cxGBProduto: TcxGroupBox
              Left = 2
              Top = 2
              Align = alClient
              PanelStyle.Active = True
              Style.BorderStyle = ebsNone
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TransparentBorder = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              Transparent = True
              Height = 227
              Width = 554
              object PnPaginacaoProduto: TcxGroupBox
                Left = 520
                Top = 22
                Align = alRight
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 0
                Height = 203
                Width = 32
                object BtnGridDownProdutos: TJvTransparentButton
                  Left = 2
                  Top = 168
                  Width = 28
                  Height = 33
                  Hint = 'Pr'#243'xima p'#225'gina'
                  Align = alBottom
                  AutoGray = False
                  FrameStyle = fsNone
                  OnClick = MoveGridUpDown
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 41
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 42
                  ExplicitTop = 262
                end
                object BtnGridUpProdutos: TJvTransparentButton
                  Left = 2
                  Top = 2
                  Width = 28
                  Height = 33
                  Hint = 'P'#225'gina anterior'
                  Align = alTop
                  AutoGray = False
                  FrameStyle = fsNone
                  OnClick = MoveGridUpDown
                  Images.ActiveImage = cxImageList32x32
                  Images.ActiveIndex = 47
                  Images.GrayIndex = 43
                  Images.DisabledIndex = 43
                  Images.DownImage = cxImageList32x32
                  Images.DownIndex = 48
                  Images.HotIndex = 43
                  ExplicitLeft = 3
                  ExplicitTop = 6
                end
              end
              object cxGroupBox13: TcxGroupBox
                Left = 2
                Top = 22
                Align = alClient
                PanelStyle.Active = True
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 1
                Height = 203
                Width = 518
                object cxGrid2: TcxGrid
                  Left = 2
                  Top = 2
                  Width = 514
                  Height = 199
                  Align = alClient
                  BorderStyle = cxcbsNone
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                  LookAndFeel.Kind = lfOffice11
                  LookAndFeel.NativeStyle = False
                  object cxGridDBTableView2: TcxGridDBTableView
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsData.CancelOnExit = False
                    OptionsData.Deleting = False
                    OptionsData.DeletingConfirmation = False
                    OptionsData.Editing = False
                    OptionsData.Inserting = False
                    OptionsView.ColumnAutoWidth = True
                    OptionsView.GroupByBox = False
                    OptionsView.GroupRowStyle = grsOffice11
                  end
                  object cxGrid2DBCardView1: TcxGridDBCardView
                    Navigator.Buttons.CustomButtons = <>
                    OnCellClick = cxGrid2DBCardView1CellClick
                    DataController.DataSource = dsProdutoGrupoTabelaPreco
                    DataController.KeyFieldNames = 'ID'
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    LayoutDirection = ldVertical
                    OptionsBehavior.CopyCaptionsToClipboard = False
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsBehavior.ImmediateEditor = False
                    OptionsBehavior.RowCaptionHints = False
                    OptionsCustomize.RowFiltering = False
                    OptionsData.CancelOnExit = False
                    OptionsData.Deleting = False
                    OptionsData.DeletingConfirmation = False
                    OptionsData.Editing = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideSelection = True
                    OptionsSelection.CardBorderSelection = False
                    OptionsView.CaptionSeparator = #0
                    OptionsView.CardAutoWidth = True
                    OptionsView.CardBorderWidth = 1
                    OptionsView.CardIndent = 2
                    OptionsView.CardWidth = 130
                    OptionsView.CellAutoHeight = True
                    Styles.Background = cxStyleYellowStrongNegrito
                    Styles.Content = cxStyleYellowStrongNegrito
                    Styles.ContentEven = cxStyleYellowStrongNegrito
                    Styles.ContentOdd = cxStyleYellowStrongNegrito
                    Styles.Selection = cxStyleYellowTooStrong
                    Styles.CardBorder = cxStyleYellowStrongNegrito
                    Styles.CategoryRow = cxStyleYellowStrongNegrito
                    object cxGrid2DBCardView1Row1: TcxGridDBCardViewRow
                      Caption = ' '
                      DataBinding.FieldName = 'DESCRICAO'
                      CaptionAlignmentHorz = taCenter
                      Position.BeginsLayer = True
                      Position.LineCount = 2
                    end
                    object cxGrid2DBCardView1Row2: TcxGridDBCardViewRow
                      DataBinding.FieldName = 'VL_VENDA'
                      Position.BeginsLayer = True
                      IsCaptionAssigned = True
                    end
                  end
                  object cxGridProduto: TcxGridLevel
                    GridView = cxGrid2DBCardView1
                  end
                end
              end
              object cxGroupBox14: TcxGroupBox
                Left = 2
                Top = 2
                Align = alTop
                PanelStyle.Active = True
                PanelStyle.OfficeBackgroundKind = pobkGradient
                Style.BorderStyle = ebsNone
                Style.LookAndFeel.Kind = lfOffice11
                Style.Shadow = False
                Style.TransparentBorder = True
                StyleDisabled.LookAndFeel.Kind = lfOffice11
                StyleFocused.LookAndFeel.Kind = lfOffice11
                StyleHot.LookAndFeel.Kind = lfOffice11
                TabOrder = 2
                Transparent = True
                Height = 20
                Width = 550
                object cxLabel10: TcxLabel
                  Left = 2
                  Top = 2
                  Align = alLeft
                  Caption = 'Produtos'
                  ParentFont = False
                  Style.BorderStyle = ebsNone
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clNavy
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = [fsBold]
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taLeftJustify
                  Properties.Alignment.Vert = taBottomJustify
                  Transparent = True
                  AnchorY = 18
                end
              end
            end
          end
        end
      end
      object tsFechamento: TcxTabSheet
        Caption = 'Fechamento'
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnParcela: TgbPanel
          Left = 0
          Top = 59
          Align = alClient
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 1
          Transparent = True
          Height = 275
          Width = 888
          object cxGridFechamento: TcxGrid
            Left = 2
            Top = 2
            Width = 884
            Height = 271
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = []
            Images = DmAcesso.cxImage16x16
            ParentFont = False
            TabOrder = 0
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            object viewFechamento: TcxGridDBBandedTableView
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.Images = DmAcesso.cxImage16x16
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Insert.Visible = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Post.Visible = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Buttons.Filter.Visible = False
              Navigator.InfoPanel.Visible = True
              Navigator.Visible = True
              DataController.DataSource = dsVendaParcela
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Images = DmAcesso.cxImage16x16
              OptionsBehavior.NavigatorHints = True
              OptionsBehavior.ExpandMasterRowOnDblClick = False
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.BandMoving = False
              OptionsCustomize.NestedBands = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Inserting = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GroupByBox = False
              OptionsView.GroupRowStyle = grsOffice11
              OptionsView.ShowColumnFilterButtons = sfbAlways
              OptionsView.BandHeaders = False
              Styles.ContentOdd = DmAcesso.OddColor
              Bands = <
                item
                end>
              object viewFechamentoNR_PARCELA: TcxGridDBBandedColumn
                DataBinding.FieldName = 'NR_PARCELA'
                HeaderAlignmentHorz = taCenter
                HeaderGlyphAlignmentHorz = taCenter
                Options.Editing = False
                Options.Sorting = False
                Width = 90
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object viewFechamentoDOCUMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DOCUMENTO'
                Options.Editing = False
                Options.Sorting = False
                Width = 341
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object viewFechamentoDT_VENCIMENTO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'DT_VENCIMENTO'
                HeaderAlignmentHorz = taCenter
                Options.Sorting = False
                Width = 145
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object viewFechamentoIC_DIAS: TcxGridDBBandedColumn
                DataBinding.FieldName = 'IC_DIAS'
                HeaderAlignmentHorz = taCenter
                HeaderGlyphAlignmentHorz = taCenter
                Options.Sorting = False
                Width = 87
                Position.BandIndex = 0
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object viewFechamentoVL_TITULO: TcxGridDBBandedColumn
                DataBinding.FieldName = 'VL_TITULO'
                Options.Sorting = False
                Width = 248
                Position.BandIndex = 0
                Position.ColIndex = 4
                Position.RowIndex = 0
              end
            end
            object cxGridLevel2: TcxGridLevel
              GridView = viewFechamento
            end
          end
        end
        object PnPlanoPagamento: TgbPanel
          Left = 0
          Top = 0
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 59
          Width = 888
          object Label4: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 5
            Width = 878
            Height = 16
            Align = alTop
            Caption = 'Plano de Pagamento'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitWidth = 132
          end
          object gbDBTextEdit3: TgbDBTextEdit
            Left = 60
            Top = 24
            TabStop = False
            Align = alClient
            DataBinding.DataField = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
            DataBinding.DataSource = dsVenda
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            gbReadyOnly = True
            gbPassword = False
            ExplicitHeight = 21
            Width = 826
          end
          object EdtPlanoPagamento: TgbDBButtonEditFK
            Left = 2
            Top = 24
            Align = alLeft
            DataBinding.DataField = 'ID_PLANO_PAGAMENTO'
            DataBinding.DataSource = dsVenda
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Properties.ReadOnly = False
            Style.Color = 14606074
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            TabOrder = 1
            gbTextEdit = gbDBTextEdit3
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_PLANO_PAGAMENTO;JOIN_DESCRICAO_PLANO_PAGAMENTO'
            gbTableName = 'PLANO_PAGAMENTO'
            gbCamposConsulta = 'ID;DESCRICAO'
            ExplicitHeight = 21
            Width = 58
          end
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 212
    Top = 251
    inherited ActCancel: TAction
      Caption = 'Cancelar F11'
      Hint = 'Cancelar as altera'#231#245'es'
      ShortCut = 122
    end
    object ActEfetivarVenda: TAction
      Category = 'Action'
      Caption = 'Efetivar F9'
      Hint = 'Encerrar a venda lan'#231'ada'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarVendaExecute
    end
    object ActVoltar: TAction
      Category = 'Action'
      Caption = 'Voltar'
      Hint = 'Voltar para o lan'#231'amento dos produtos'
      ImageIndex = 18
      ShortCut = 39
      OnExecute = ActVoltarExecute
    end
    object ActPesquisar: TAction
      Category = 'Action'
      Caption = 'Pesquisar F7'
      ImageIndex = 16
      ShortCut = 118
      OnExecute = ActPesquisarExecute
    end
    object ActVenda: TAction
      Category = 'Action'
      Caption = 'Venda F6'
      Hint = 'Realizar uma venda'
      ImageIndex = 133
      ShortCut = 117
      OnExecute = ActVendaExecute
    end
    object ActDevolucao: TAction
      Category = 'Action'
      Caption = 'Devolu'#231#227'o F8'
      Hint = 'Realizar uma devolu'#231#227'o'
      ImageIndex = 136
      ShortCut = 119
      OnExecute = ActDevolucaoExecute
    end
    object ActOrcamento: TAction
      Category = 'Action'
      Caption = 'Or'#231'amento F3'
      Hint = 'Realizar um or'#231'amento'
      ImageIndex = 134
      OnExecute = ActOrcamentoExecute
    end
    object ActPedido: TAction
      Category = 'Action'
      Caption = 'Pedido F2'
      Hint = 'Realizar um pedido'
      ImageIndex = 135
      ShortCut = 113
      OnExecute = ActPedidoExecute
    end
    object ActCondicional: TAction
      Category = 'Action'
      Caption = 'Condicional F4'
      Hint = 'Realizar uma condicional'
      ImageIndex = 137
      ShortCut = 115
      OnExecute = ActCondicionalExecute
    end
    object ActCancelarVenda: TAction
      Category = 'Action'
      Caption = 'Estornar F10'
      Hint = 'Estornar o documento'
      ImageIndex = 95
      ShortCut = 121
      OnExecute = ActCancelarVendaExecute
    end
    object ActImprimir: TAction
      Category = 'Action'
      Caption = 'Imprimir F12'
      Hint = 'Imprimir documento'
      ImageIndex = 27
      ShortCut = 123
      OnExecute = ActImprimirExecute
    end
    object ActExcluir: TAction
      Category = 'Action'
      Caption = 'Excluir Del'
      Hint = 'Excluir documento'
      ImageIndex = 8
      ShortCut = 46
      OnExecute = ActExcluirExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Consultar'
      'Informacoes')
    Categories.ItemsVisibles = (
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True)
    Left = 184
    Top = 251
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedLeft = 622
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedLeft = 75
      FloatClientWidth = 95
      FloatClientHeight = 378
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbPesquisar'
        end
        item
          Visible = True
          ItemName = 'lbVoltar'
        end
        item
          Visible = True
          ItemName = 'lbFechamentoVenda'
        end
        item
          Visible = True
          ItemName = 'lbEfetivar'
        end
        item
          Visible = True
          ItemName = 'lbCancelarDocumento'
        end
        item
          Visible = True
          ItemName = 'lbCancelar'
        end
        item
          Visible = True
          ItemName = 'lbExcluir'
        end
        item
          Visible = True
          ItemName = 'lbImpressao'
        end>
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedLeft = 534
      FloatClientWidth = 100
      FloatClientHeight = 22
    end
    object barConsultar: TdxBar [3]
      Caption = 'Consultar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 75
      DockedTop = 0
      FloatLeft = 1259
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 102
          Visible = True
          ItemName = 'filtroCodigoVenda'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 129
          Visible = True
          ItemName = 'filtroPessoa'
        end
        item
          Visible = True
          ItemName = 'filtroNomePessoa'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataCadastro'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroInicial'
        end
        item
          Visible = True
          ItemName = 'filtroDataCadastroFIm'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroLabelDataDocumento'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoInicio'
        end
        item
          Visible = True
          ItemName = 'filtroDataDocumentoFim'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'filtroTipoDocumento'
        end
        item
          Visible = True
          ItemName = 'filtroSituacao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object BarInformacoes: TdxBar [4]
      Caption = 'Informa'#231#245'es'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 974
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'informacoesCodigo'
        end
        item
          Visible = True
          ItemName = 'informacoesSituacao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object lbVoltar: TdxBarLargeButton [10]
      Action = ActVoltar
      Category = 1
    end
    object lbFechamentoVenda: TdxBarLargeButton [11]
      Action = ActVenda
      Category = 1
    end
    object lbEfetivar: TdxBarLargeButton [12]
      Action = ActEfetivarVenda
      Category = 1
    end
    object lbDevolucao: TdxBarLargeButton [13]
      Action = ActDevolucao
      Category = 1
    end
    object lbOrcamento: TdxBarLargeButton [14]
      Action = ActOrcamento
      Category = 1
    end
    object lbPedido: TdxBarLargeButton [15]
      Action = ActPedido
      Category = 1
    end
    object lbCondicional: TdxBarLargeButton [16]
      Action = ActCondicional
      Category = 1
    end
    object lbPesquisar: TdxBarLargeButton [17]
      Action = ActPesquisar
      Category = 1
    end
    object lbCancelarDocumento: TdxBarLargeButton [18]
      Action = ActCancelarVenda
      Category = 1
    end
    object lbImpressao: TdxBarLargeButton [19]
      Action = ActImprimir
      Category = 1
    end
    object lbCancelar: TdxBarLargeButton [20]
      Action = ActCancel
      Category = 1
    end
    object lbExcluir: TdxBarLargeButton [21]
      Action = ActExcluir
      Category = 1
    end
    object filtroLabelDataCadastro: TdxBarStatic
      Align = iaCenter
      Caption = 'Data de Cadastro'
      Category = 2
      Hint = 'Data de Cadastro'
      Visible = ivAlways
    end
    object filtroDataCadastroInicial: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataCadastroFIm: TcxBarEditItem
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroLabelDataDocumento: TdxBarStatic
      Align = iaCenter
      Caption = 'Data de Efetiva'#231#227'o'
      Category = 2
      Hint = 'Data de Efetiva'#231#227'o'
      Visible = ivAlways
    end
    object filtroDataDocumentoInicio: TcxBarEditItem
      Caption = 'In'#237'cio'
      Category = 2
      Hint = 'In'#237'cio'
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroDataDocumentoFim: TcxBarEditItem
      Caption = 'Fim   '
      Category = 2
      Hint = 'Fim   '
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.ImmediatePost = True
      Properties.PostPopupValueOnTab = True
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object filtroTipoDocumento: TdxBarCombo
      Caption = 'Documento'
      Category = 2
      Hint = 'Documento'
      Visible = ivAlways
      Items.Strings = (
        'CONDICIONAL'
        'DEVOLU'#199#195'O'
        'PEDIDO'
        'OR'#199'AMENTO'
        'VENDA'
        'TODOS')
      ItemIndex = -1
    end
    object filtroSituacao: TdxBarCombo
      Caption = 'Situa'#231#227'o      '
      Category = 2
      Hint = 'Situa'#231#227'o      '
      Visible = ivAlways
      Items.Strings = (
        'ABERTO'
        'FECHADO'
        'CANCELADO')
      ItemIndex = -1
    end
    object filtroPessoa: TcxBarEditItem
      Caption = 'Pessoa'
      Category = 2
      Hint = 'Pessoa'
      Visible = ivAlways
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
    end
    object filtroCodigoVenda: TcxBarEditItem
      Caption = 'Documento'
      Category = 2
      Hint = 'Documento'
      Visible = ivAlways
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.AssignedValues.DisplayFormat = True
      Properties.DecimalPlaces = 0
      Properties.MaxLength = 8
      Properties.MaxValue = 99999999.000000000000000000
    end
    object filtroNomePessoa: TdxBarStatic
      Caption = 'Nome da Pessoa 40 Caracteres'
      Category = 2
      Hint = '123456789 123456789 123456789 123456789'
      Visible = ivAlways
      Alignment = taLeftJustify
    end
    object informacoesCodigo: TdxBarStatic
      Align = iaCenter
      Caption = 'Venda: 000'
      Category = 3
      Hint = 'Venda: 000'
      Visible = ivAlways
    end
    object informacoesSituacao: TdxBarStatic
      Align = iaCenter
      Caption = 'ABERTA'
      Category = 3
      Hint = 'ABERTA'
      Visible = ivAlways
      BorderStyle = sbsEtched
    end
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 324
    Top = 373
    PixelsPerInch = 96
    object cxStyleYellowStrong: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyleYellowTooStrong: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12713983
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBtnText
    end
    object cxStyleYellowStrongOdd: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12713983
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clDefault
    end
    object cxStyleYellowStrongNegrito: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
  end
  object cdsVenda: TGBClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspVenda'
    RemoteServer = DmConnection.dspVenda
    AfterOpen = cdsVendaAfterOpen
    OnNewRecord = cdsVendaNewRecord
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 104
    Top = 264
    object cdsVendaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsVendaDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsVendaDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsVendaVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_TOTAL_PRODUTO: TFMTBCDField
      DisplayLabel = 'Total dos Produtos'
      FieldName = 'VL_TOTAL_PRODUTO'
      Origin = 'VL_TOTAL_PRODUTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_VENDA: TFMTBCDField
      DisplayLabel = 'Valor L'#237'quido'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      OnChange = GerarParcela
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Required = True
    end
    object cdsVendaTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
    end
    object cdsVendaID_PESSOA: TIntegerField
      DisplayLabel = 'C'#243'digo da Pessoa'
      FieldName = 'ID_PESSOA'
      Origin = 'ID_PESSOA'
      Required = True
    end
    object cdsVendaID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsVendaID_OPERACAO: TIntegerField
      DisplayLabel = 'C'#243'digo da Opera'#231#227'o'
      FieldName = 'ID_OPERACAO'
      Origin = 'ID_OPERACAO'
    end
    object cdsVendaID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsVendaID_CENTRO_RESULTADO: TIntegerField
      DisplayLabel = 'C'#243'digo do Centro de Resultado'
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
    object cdsVendaID_CONTA_ANALISE: TIntegerField
      DisplayLabel = 'C'#243'digo da Conta de An'#225'lise'
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object cdsVendaID_PLANO_PAGAMENTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Plano de Pagamento'
      FieldName = 'ID_PLANO_PAGAMENTO'
      Origin = 'ID_PLANO_PAGAMENTO'
      OnChange = GerarParcela
    end
    object cdsVendaID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo da Tabela de Pre'#231'o'
      FieldName = 'ID_TABELA_PRECO'
      Origin = 'ID_TABELA_PRECO'
      Required = True
    end
    object cdsVendaID_VENDEDOR: TIntegerField
      DisplayLabel = 'C'#243'digo do Vendedor'
      FieldName = 'ID_VENDEDOR'
      Origin = 'ID_VENDEDOR'
    end
    object cdsVendaPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaJOIN_NOME_PESSOA: TStringField
      DisplayLabel = 'Pessoa'
      FieldName = 'JOIN_NOME_PESSOA'
      Origin = 'NOME'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaJOIN_NOME_VENDEDOR: TStringField
      DisplayLabel = 'Vendedor'
      FieldName = 'JOIN_NOME_VENDEDOR'
      Origin = 'JOIN_NOME_VENDEDOR'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaJOIN_DESCRICAO_OPERACAO: TStringField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'JOIN_DESCRICAO_OPERACAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_SEQUENCIA_CONTA_ANALISE: TStringField
      DisplayLabel = 'Sequ'#234'ncia da Conta de An'#225'lise'
      FieldName = 'JOIN_SEQUENCIA_CONTA_ANALISE'
      Origin = 'SEQUENCIA'
      ProviderFlags = []
    end
    object cdsVendaJOIN_DESCRICAO_CONTA_ANALISE: TStringField
      DisplayLabel = 'Conta de An'#225'lise'
      FieldName = 'JOIN_DESCRICAO_CONTA_ANALISE'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_CENTRO_RESULTADO: TStringField
      DisplayLabel = 'Centro de Resultado'
      FieldName = 'JOIN_DESCRICAO_CENTRO_RESULTADO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_PLANO_PAGAMENTO: TStringField
      DisplayLabel = 'Plano de Pagamento'
      FieldName = 'JOIN_DESCRICAO_PLANO_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsVendaJOIN_DESCRICAO_FORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'JOIN_DESCRICAO_FORMA_PAGAMENTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaJOIN_DESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Tabela de Pre'#231'o'
      FieldName = 'JOIN_DESCRICAO_TABELA_PRECO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 45
    end
    object cdsVendaVL_PAGAMENTO: TFMTBCDField
      DisplayLabel = 'Vl. Pagamento'
      FieldName = 'VL_PAGAMENTO'
      Origin = 'VL_PAGAMENTO'
      Precision = 24
      Size = 9
    end
    object cdsVendaVL_PESSOA_CREDITO_UTILIZADO: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Utilizado'
      FieldName = 'VL_PESSOA_CREDITO_UTILIZADO'
      Origin = 'VL_PESSOA_CREDITO_UTILIZADO'
      Precision = 24
      Size = 9
    end
    object cdsVendaJOIN_VALOR_PESSOA_CREDITO: TFMTBCDField
      DisplayLabel = 'Vl. Cr'#233'dito Dispon'#237'vel'
      FieldName = 'JOIN_VALOR_PESSOA_CREDITO'
      Origin = 'VL_CREDITO'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsVendafdqVendaItem: TDataSetField
      FieldName = 'fdqVendaItem'
    end
    object cdsVendafdqVendaParcela: TDataSetField
      FieldName = 'fdqVendaParcela'
    end
  end
  object dsVenda: TDataSource
    DataSet = cdsVenda
    Left = 76
    Top = 264
  end
  object cdsVendaItem: TGBClientDataSet
    Active = True
    Aggregates = <>
    AggregatesActive = True
    DataSetField = cdsVendafdqVendaItem
    Params = <>
    BeforePost = cdsVendaItemBeforePost
    AfterPost = cdsVendaItemAfterPost
    AfterDelete = cdsVendaItemAfterDelete
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 80
    Top = 320
    object cdsVendaItemID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaItemID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object cdsVendaItemID_PRODUTO: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsVendaItemNR_ITEM: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Item'
      FieldName = 'NR_ITEM'
      Origin = 'NR_ITEM'
      Required = True
    end
    object cdsVendaItemQUANTIDADE: TFMTBCDField
      Alignment = taCenter
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_DESCONTO: TFMTBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'VL_DESCONTO'
      Origin = 'VL_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_ACRESCIMO: TFMTBCDField
      DisplayLabel = 'Acr'#233'scimo'
      FieldName = 'VL_ACRESCIMO'
      Origin = 'VL_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_BRUTO: TFMTBCDField
      DisplayLabel = 'Vl. Bruto'
      FieldName = 'VL_BRUTO'
      Origin = 'VL_BRUTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemVL_LIQUIDO: TFMTBCDField
      DisplayLabel = 'Vl. L'#237'quido'
      FieldName = 'VL_LIQUIDO'
      Origin = 'VL_LIQUIDO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemPERC_DESCONTO: TFMTBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'PERC_DESCONTO'
      Origin = 'PERC_DESCONTO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemPERC_ACRESCIMO: TFMTBCDField
      DisplayLabel = '% Acr'#233'scimo'
      FieldName = 'PERC_ACRESCIMO'
      Origin = 'PERC_ACRESCIMO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaItemJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsVendaItemJOIN_SIGLA_UNIDADE_ESTOQUE: TStringField
      DisplayLabel = 'UN'
      FieldName = 'JOIN_SIGLA_UNIDADE_ESTOQUE'
      Origin = 'SIGLA'
      ProviderFlags = []
      Size = 2
    end
    object cdsVendaItemJOIN_ESTOQUE_PRODUTO: TFMTBCDField
      DisplayLabel = 'Estoque'
      FieldName = 'JOIN_ESTOQUE_PRODUTO'
      Origin = 'QT_ESTOQUE'
      ProviderFlags = []
      Precision = 24
      Size = 9
    end
    object cdsVendaItemJOIN_CODIGO_BARRA_PRODUTO: TStringField
      Alignment = taCenter
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
  end
  object cdsVendaParcela: TGBClientDataSet
    Active = True
    Aggregates = <>
    DataSetField = cdsVendafdqVendaParcela
    Params = <>
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 144
    Top = 320
    object cdsVendaParcelaID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsVendaParcelaID_VENDA: TIntegerField
      DisplayLabel = 'Venda'
      FieldName = 'ID_VENDA'
      Origin = 'ID_VENDA'
    end
    object cdsVendaParcelaDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Required = True
      Size = 50
    end
    object cdsVendaParcelaVL_TITULO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_TITULO'
      Origin = 'VL_TITULO'
      Required = True
      DisplayFormat = '###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsVendaParcelaQT_PARCELA: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'QT_PARCELA'
      Origin = 'QT_PARCELA'
      Required = True
    end
    object cdsVendaParcelaNR_PARCELA: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Parcelas'
      FieldName = 'NR_PARCELA'
      Origin = 'NR_PARCELA'
      Required = True
    end
    object cdsVendaParcelaDT_VENCIMENTO: TDateField
      Alignment = taCenter
      DisplayLabel = 'Vencimento'
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
      Required = True
      OnChange = AlterandoDataParcelamento
    end
    object cdsVendaParcelaIC_DIAS: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Dias'
      FieldKind = fkInternalCalc
      FieldName = 'IC_DIAS'
      OnChange = AlterandoDiasParcelamento
    end
  end
  object dsVendaItem: TDataSource
    DataSet = cdsVendaItem
    Left = 52
    Top = 320
  end
  object dsVendaParcela: TDataSource
    DataSet = cdsVendaParcela
    Left = 116
    Top = 320
  end
  object fdmSearch: TFDMemTable
    AfterOpen = fdmSearchAfterOpen
    AfterClose = fdmSearchAfterClose
    AfterDelete = fdmSearchAfterDelete
    FieldOptions.PositionMode = poFirst
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 258
    Top = 184
  end
  object dsSearch: TDataSource
    DataSet = fdmSearch
    Left = 278
    Top = 320
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 334
    Top = 320
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      Hint = 'Voltar as configura'#231#245'es originais da grade'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActAbrirConsultando: TAction
      Category = 'filter'
      Caption = 'Consultar Autom'#225'ticamente'
      Hint = 'Realiza a consulta de dados ao abrir o formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActAbrirConsultandoExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
    object ActFullExpand: TAction
      Category = 'filter'
      Caption = 'Expandir Grupos'
      Hint = 'Expandir/Contrair agrupamentos'
      ImageIndex = 13
    end
    object ActConfigurarValoresDefault: TAction
      Category = 'filter'
      Caption = 'Configurar Valores Padr'#245'es'
      Hint = 'Configurar valores que ser'#227'o preenchidos em um novo registro'
      ImageIndex = 13
      OnExecute = ActConfigurarValoresDefaultExecute
    end
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 156
    Top = 192
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ExibirAgrupamento2: TMenuItem
      Action = ActExibirAgrupamento
    end
    object RestaurarColunasPadrao: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
  object pmGridConsultaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmGridConsultaPadraoPopup
    Left = 128
    Top = 192
    object AlterarSQL1: TMenuItem
      Action = ActAlterarSQLPesquisaPadrao
    end
    object ExibirAgrupamento1: TMenuItem
      Action = ActAbrirConsultando
    end
  end
  object PopUpControllerPesquisa: TcxGridPopupMenu
    Grid = cxGridPesquisaPadrao
    PopupMenus = <
      item
        GridView = viewPesquisa
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = viewPesquisa
        HitTypes = [gvhtNone]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
    Left = 204
    Top = 368
  end
  object JvEnterAsTab1: TJvEnterAsTab
    Left = 328
    Top = 208
  end
  object PopUpControllerFechamento: TcxGridPopupMenu
    Grid = cxGridFechamento
    PopupMenus = <
      item
        GridView = viewFechamento
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end>
    Left = 44
    Top = 240
  end
  object PopUpControllerProduto: TcxGridPopupMenu
    Grid = cxGridProdutos
    PopupMenus = <
      item
        GridView = viewProduto
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end>
    Left = 204
    Top = 312
  end
  object cxImageList32x32: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 9371701
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAFABA3D8C9C4BBF8C3BEB5FDC1BCB1FFC1BCB1FFC2BD
          B2FFC7C1B7FBCBC7BCF5A19F99D7626261971B1B1B2B01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002423
          222EA8A49CCDC2BEB3FD74716BFF42413DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF3B3936FF514F4BFFA29D95FFCCC7BCF9989792D140404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC4C0
          B5EA827F79FF373633FF2E2D2BFF2F2E2CFF3D3B39FF474542FF474643FF4342
          3FFF353431FF2E2D2BFF2F2E2BFF4E4C48FFBDB8AEFEB5B0A9E5515150800404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002C2B2832C6C1B6EC6260
          5BFF2F2E2BFF302F2DFF51504CFFB1AEA7FFD7D3CBFFE1DDD5FFE2DED6FFDDD9
          D1FFCCC8C0FF7F7C78FF3A3937FF2E2D2BFF3A3835FFA8A49BFFB7B3ABE74040
          4066010101020000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000011100F13B9B4AAD56C6A64FF2F2E
          2BFF363532FF97958EFFE3DFD7FFEBE8E0FFECE8E1FFD6D3CCFFCDCAC4FFE9E6
          DFFFECE8E1FFEAE6DEFFCBC8C1FF585753FF2E2D2BFF3A3835FFBDB8ADFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          000000000000000000000000000001010101706D667F9F9C93FE312F2CFF3332
          30FFA9A59FFFEAE6DDFFECE9E1FFEEEBE4FFDFDCD6FF484746FF333231FFBBB9
          B3FFEFECE5FFEDEAE3FFECE8E0FFDCD9D0FF585753FF2E2D2BFF4E4B48FFCDC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000014141217C1BDB2E54A4844FF2E2D2BFF7976
          71FFE8E4DCFFEDE9E2FFEFECE5FFF0EEE8FFE5E3DEFF535351FF3E3D3CFFC6C4
          BFFFF1EFE9FFF0EDE6FFEEEBE4FFECE8E0FFCCC8C1FF3C3B38FF2F2E2CFFA19D
          95FFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB0ACA2FE302F2CFF3D3C39FFD4D0
          C8FFECE8E0FFEEEBE4FFF1EEE8FFF2F0EBFFF4F2EEFFCDCCC9FFC2C1BEFFF2F0
          ECFFF4F2EDFFF2EFEAFFF0EDE7FFEDEAE3FFEAE6DEFF85837DFF2E2D2BFF504E
          49FFCAC6BBF53939385A00000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF676561FFE9E5
          DDFFEDEAE2FFF0EDE6FFF2F0EBFFF5F3EEFFEFEEEAFF545352FF434242FFE2E0
          DEFFF6F4F0FFF4F2EDFFF1EFE9FFEFECE5FFECE8E1FFCCC8C1FF353431FF3A39
          36FFC6C2B7FB6161609300000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF9E9B94FFEBE7
          DFFFEEEBE4FFF1EEE8FFF3F1ECFFF6F4F1FFF0EFEDFF50504FFF333333FFA5A5
          A4FFF7F5F2FFF5F3EFFFF2F0EBFFF0EDE6FFEDE9E2FFDEDBD2FF444340FF2F2E
          2BFFC2BDB2FF72716FA901010101000000000000000000000000000000000000
          0000000000000000000007070608BAB4AAD8494743FF2E2D2BFFA9A69FFFEBE8
          E0FFEEEBE4FFF1EEE9FFF4F2EDFFF7F5F2FFF9F8F6FF9A9A99FF353535FF3A3A
          3AFFA3A2A0FFF3F1EEFFF3F1ECFFF0EDE7FFEDEAE3FFE2DED6FF474643FF2E2D
          2AFFAFABA1FF7E7C79B303030305000000000000000000000000000000000000
          0000000000000000000002020202B9B4AAD34D4B47FF2E2D2BFFA8A59EFFEBE8
          DFFFEEEBE4FFF1EEE8FFF4F1EDFFF6F5F1FFF9F8F5FFF7F7F5FF8B8B8AFF3535
          34FF363535FF9F9E9CFFF1EFEAFFF0EDE7FFEDEAE2FFE1DED5FF474643FF2E2D
          2AFFBDB8AEFF73726FA700000000000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC62605BFF2E2D2BFF888680FFEBE7
          DEFFEDEAE3FFF0EDE7FFB8B7B3FF636261FFDBDAD7FFF9F8F6FFF5F4F2FF9897
          95FF323231FF3E3D3CFFDBD8D4FFEFECE6FFECE9E1FFD8D4CCFF3E3D3AFF3432
          30FFC4BFB4FE5655548000000000000000000000000000000000000000000000
          00000000000000000000000000006E6B647D949189FF2E2D2AFF4E4C49FFE4E0
          D7FFECE9E1FFEFECE5FF686764FF31302FFF7B7A78FFF5F4F0FFF7F5F2FFE0DE
          DBFF3E3D3DFF313130FFCECCC7FFEEEBE4FFEBE8E0FFB3B0A9FF2F2E2CFF4240
          3DFFC8C3BAF82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC1BCB1F7363532FF32312FFFB8B4
          ADFFEBE7DFFFEDEAE3FFA19F9BFF323130FF3A3A38FFB4B2AFFFD7D5D1FF8280
          7EFF313130FF3E3D3CFFDAD7D1FFECE9E1FFE5E1D9FF585753FF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A8A39AC1696761FF2E2D2AFF4A49
          46FFD8D5CDFFEBE8E0FFE7E4DDFF72706DFF333231FF31312FFF373735FF3030
          2FFF373635FF9C9A95FFECE8E1FFEAE6DDFF9C9993FF302F2DFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BEB3F7403E3BFF2E2D
          2BFF5D5B57FFD9D5CDFFEBE7DFFFE7E4DCFFBAB7B2FF7C7A76FF706F6BFF8886
          82FFCBC8C2FFEAE7DFFFE8E4DCFFA9A69FFF363532FF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000030302037D79728EAFABA1FD3533
          31FF2E2D2BFF4C4A47FFB9B6AEFFE4E0D8FFEBE7DEFFEBE8DFFFEBE8E0FFEBE7
          DFFFE9E5DDFFD7D3CBFF7F7D78FF333230FF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8ABA7
          9EFD403E3BFF2E2D2AFF32312FFF4E4C49FF898681FFACA9A2FFBCB8B1FF9F9C
          96FF686662FF3F3D3BFF2E2D2BFF302F2DFF696761FFC5C0B4EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097C79
          718DC4BEB3F7696761FF363533FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF474542FF9F9B92FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CA9A49BC2C1BCB1F78F8C84FF5D5B56FF4B4945FF474542FF504E
          4AFF73706AFFAFAAA1FEBFBBB0E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB7B3A8D3BAB4ABDAB4AF
          A5CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202080807090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000D00000027000000260000000B0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000601001A320B008C3E0E01AD0C02006A000000370000
          000D000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000A0200227C3926E2E5CEBCFFEEDBC8FF975946F61604007A0000
          00370000000D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000090200217A3724E1EAD2B9FFE3AF61FFE7B462FFF1D9B4FF985A47F61604
          007A000000380000000D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000902
          002075301EDFDCBEA3FFD9A257FFDEA859FFE2AE5EFFE7B462FFE9CAA3FF9352
          3FF51604007A000000380000000D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000902001F722A
          17DDCFA68BFFCF954DFFD49A4FFFD8A154FFDDA758FFE2AD5DFFE7B362FFE1BC
          93FF8E4A37F51604007A000000390000000D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000902001F6E2714DCC191
          75FFC58743FFC98D45FFCE944AFFD39A4FFFD8A053FFDDA658FFE2AD5DFFE6B3
          61FFD9AE82FF8A432FF41604007A000000390000000D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000090200206C2410DBB57C5FFFBB7A
          38FFC0803CFFC48740FFC98D45FFCE934AFFD3994EFFD7A053FFDCA658FFE1AC
          5CFFE6B261FFD1A073FF863B26F51604007A000000390000000D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000090200206B230FDBAD6E51FFB06D2FFFB573
          32FFBA7937FFBF803BFFC48640FFC98C44FFCD9249FFD2994EFFD79F53FFDCA5
          57FFE1AC5CFFE5B260FFCC986AFF823721F41605007A000000390000000D0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000090200206A210DDBA56246FFA66024FFAB6628FFB06C
          2DFFB57331FFBA7936FFBF7F3BFFC3853FFFC88C44FFCD9249FFD2984DFFD79E
          52FFDBA557FFE0AB5BFFE5B160FFC89062FF82341EF51605007A000000390000
          000D000000000000000000000000000000000000000000000000000000000000
          000000000000090200206A210CDB9E583BFF9D531BFFA1591EFFA65F23FFAB66
          28FFB06C2CFFB57231FFB97836FFBE7F3AFFC3853FFFC88B44FFCD9148FFD198
          4DFFD69E52FFDBA456FFE0AA5BFFE5B160FFC48C5DFF80301AF51705007B0000
          00390000000D0000000000000000000000000000000000000000000000000000
          000009020020691F0ADB9A5135FF934612FF974C15FF9C5219FFA1581EFFAB68
          2FFFB47842FFBD8652FFC59262FFCA9866FFCB9862FFCD9A61FFCD9859FFCC92
          4AFFD1974DFFD69D51FFDAA356FFDFAA5AFFE4B05FFFC3895AFF7F2E17F51705
          007B000000390000000D00000000000000000000000000000000000000000902
          00166B210EDA9C573DFF883A08FF8D3F0BFF924510FF9F5926FFB47D52FFC190
          6AFFC3946DFFC79971FFCA9E74FFCDA277FFD0A67BFFD4AA7EFFD7AF82FFD9B1
          82FFD7A86FFFD39D57FFD59D51FFDAA355FFDFA95AFFE4AF5FFFC58C5EFF8031
          1BF51705007B000000390000000D000000000000000000000000000000006723
          10CAA16147FF823203FF833201FF8D4111FFAD7550FFBC8D6DFFBF9271FFC295
          73FFC69A77FFC99E7AFFCCA27DFFCEA680FFD1AA84FFD4AE86FFD8B28AFFDBB6
          8DFFDDBA90FFE0BD92FFDAAE75FFD59D52FFDAA255FFDEA95AFFE3AF5EFFC994
          67FF83341FF51705007B000000390000000D000000000000000019060034A05D
          49FF833204FF823000FF8E4419FFB58568FFBD9175FFBF9578FFC3997CFFC59C
          7EFFC8A081FFCBA484FFCEA788FFD2AF92FFD4B297FFD6B391FFD9B693FFDBB9
          96FFDEBD99FFE1C09BFFE3C49FFFE3C094FFD8A663FFD9A255FFDEA859FFE3AE
          5EFFCE9C71FF853924F61705007B000000390000000D00000000090200139853
          3FF9924A23FF96522AFFBE947CFFC19881FFC19A82FFC49C84FFC7A087FFC9A3
          89FFCCA78DFFD3B39EFFD8BDB2FFB9877BFEAF7869FAD4B5ACFFDEC3AFFFDDBE
          9FFFDFC1A2FFE2C4A4FFE4C7A7FFE6CAA9FFE8CDAAFFDBAD6FFFD9A154FFDEA7
          59FFE2AE5DFFD3A57BFF893F29F61705007C000000390000000B00000000350D
          016EB88577FFCFB09EFFC8A490FFC8A490FFC9A591FFC9A591FFCBA994FFCDAC
          95FFD7BBAAFFC3998EFE3E0E02800C0300190601000C2D0A005EAA7363F6E4CE
          BEFFE1C6ABFFE3C9ADFFE5CCB0FFE7CEB2FFE9D1B5FFEBD3B5FFDDB27AFFD8A1
          54FFDDA758FFE2AD5DFFD8AD84FF8B432EF61705007A00000027000000000000
          0000380D0272CDAA9FFFD7BDAFFFCFB19FFFD0B1A0FFD0B1A0FFD0B2A1FFDAC2
          B4FFC9A499FE2F0A0064000000000000000000000000000000001B060037AF7B
          6CF7E9D7CAFFE5CEB7FFE7D1B9FFE9D3BBFFEBD6BEFFECD8BFFFEEDAC1FFE0B9
          86FFD8A053FFDDA658FFE1AD5DFFDDB78EFF732F1EE000000027000000000000
          000000000000380D0272D3B4ABFFDEC9BCFFD7BEAFFFD7BEAFFFE0CCC1FFCFAE
          A4FE2F0A00640000000000000000000000000000000000000000000000001B06
          0038B48476F8EEE0D5FFEAD7C4FFEBD8C5FFECDBC7FFEEDCC9FFEFDFCBFFF1E0
          CCFFDFB57EFFD79F53FFDCA657FFE1B06AFFA46554FC04010014000000000000
          00000000000000000000380D0272D8BCB4FFE6D6CDFFE8D9D0FFD4B7AFFE2F0A
          0064000000000000000000000000000000000000000000000000000000000000
          00001B060039B88A7DF8F2E8DFFFEEDFD0FFEFE1D2FFF0E2D3FFF2E4D5FFF2E5
          D5FFF3E5D4FFDBAD71FFD7A259FFDABBA4FF591F0EAE00000000000000000000
          0000000000000000000000000000320C0265AE8174EEAA7A6DEC2A0900570000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001B060039BD9287F9F6EFE8FFF2E7DDFFF3E8DDFFF4EADFFFF5EB
          E0FFF6ECE1FFF5E9DAFFE3CBB9FF6C2916CB0601000C00000000000000000000
          0000000000000000000000000000000000000100000201000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C06003AC29A8FFAF9F5F1FFF7F0E9FFF7F1EAFFF8F1
          EAFFF9F3EDFFF8F3EFFF7C4130D10601000C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001C06003BC49D92FAFCFAF8FFFBF8F6FFFCF9
          F6FFFAF6F3FF804536D20601000C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C06003BC49E92FAFCF9F7FFF7F1
          EEFF81493AD00601000C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000010030021421103862F0A
          0061040100080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010101060303020E050404160606061F090808290A0A09310B0A09330909
          082C070706230605051A04040312020202090000000200000000000000000000
          0000010101040303020D0606051B080807280B0B0A360F0E0D4411100F511312
          105A14141262161513671716136B1716136B17151369151412641413115C1211
          0F540F0F0D470D0C0A3B0B0A09340A0A082E08080726070606200606051E0606
          051E0606051B05050417040303110202020A0000000100000000000000000000
          00000303020D626664B6969B99EB9A9E9DED9A9E9DEE9A9F9DF09B9F9EF19B9F
          9EF29B9F9EF39BA09EF49C9F9FF59C9F9FF59C9F9FF59B9F9EF49B9F9DF39B9E
          9EF29C9F9EF29A9E9DF09B9E9DEF979B99ED636765BC07070621040403120000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E2E7E4FFD7DEDBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DE
          DBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DEDBFFD7DE
          DBFFD7DEDBFFD7DEDBFFD7DEDBFFE2E7E4FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E0E5E2FFCFD7D3FFB7BCB8FFCDD5D1FFD0D8D4FFD4DB
          D7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD0D7
          D4FFCFD7D3FFCFD7D3FFCFD7D3FFE0E5E2FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E0E5E2FF8E928DFF36342EFF64655FFFD4DBD7FFBDC3
          C0FFACB2AFFFACB2AFFFACB2AFFFACB2AFFFACB2AFFFACB2AFFFB1BAB9FFB4C2
          C3FFCFD7D3FFCFD7D3FFCFD7D3FFE0E5E2FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E0E5E2FF898C87FF36342EFF5E5F59FFD4DBD7FFBCC2
          BFFFABB1AEFFABB1AEFFABB1AEFFABB1AEFFABB1AEFFABB1AEFFBCC2BFFF2253
          6CFF839FA8FFCFD7D3FFCFD7D3FFE0E5E2FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E0E5E2FFCED6D2FFADB3AEFFCAD1CEFFD0D8D4FFD4DB
          D7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FFD4DBD7FF4574
          89FF1C3947FF507B8EFFC9D2CFFFE0E5E2FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E0E5E2FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7
          D3FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7D3FFCFD7D3FF829D
          A7FF71A9C1FF6AAFCAFF376B83FFC3CFD0FF6E7270C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E1E5E3FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8
          D5FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8D5FFD1D8D5FFBDC9
          C8FF5490A9FFA3D6E3FF8DC9DCFF316782FF606A6DCC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7270C3E2E7E4FFAEB3AFFF50504AFF8D908BFFCBD2CEFFBDC4
          C0FFBDC4C0FFBDC4C0FFBDC4C0FFBDC3C0FFBDC3C0FFBDC3C0FFBDC3C0FFBDC3
          C0FF3C718AFFA5D8E4FF7CC1DCFF1B73A3FF1F5069F500010104000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7271C3E3E7E5FF7E807AFF36342EFF51504AFFBEC5C1FF959A
          97FF959A97FF959A97FF959A97FF959A97FF959A97FF959A97FF959A97FF959A
          97FF466B7DFF599EC3FF59ABD8FF228FCAFF185F85FD08293A8F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7271C3E3E8E6FFC4CAC6FF7E807BFFB0B4B1FFD5DCD9FFD5DC
          D9FFD5DCD9FFD5DCD9FFD5DCD9FFD5DCD9FFD5DCD9FFD5DCD9FFD5DCD9FFD5DC
          D9FFCDD6D4FF366A83FF6FB4D9FF409FD2FF2088C0FF0F4765EB030F15380000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006E7271C3E5E9E7FFD7DDDAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DD
          DAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DDDAFFD7DD
          DAFFD7DDDAFFA9BBBEFF467E9CFF6EB6DDFF2C95CDFF1D79ABFF0C3F58DD0003
          040A000000000000000000000000000000000000000000000000000000000000
          0000000000006F7371C3E6E9E8FFD9DEDCFFBFC2C0FFD7DCDAFFD9DEDCFFD9DE
          DCFFD8DEDCFFD8DEDCFFD8DEDCFFD8DEDCFFD8DEDCFFD8DEDCFFD8DEDCFFD8DE
          DCFFD8DEDCFFD8DEDCFF5F8697FF5C9FC3FF5EAED9FF2290CBFF17658DFC0A31
          46A9000000000000000000000000000000000000000000000000000000000000
          0000000000006F7371C3E7EBE9FF959792FF36342EFF676862FFCBD1CEFFAFB4
          B2FFAFB4B2FFAFB4B2FFAFB4B2FFAFB4B2FFAFB4B2FFAFB4B2FFAFB4B2FFAFB4
          B2FFAFB4B2FFAFB4B2FFB9C0BEFF396C85FF6DB3D8FF46A2D3FF218BC4FF104D
          6CEF04161F500000000000000000000000000000000000000000000000000000
          0000000000006F7371C3E8EBEAFF90928EFF36342EFF61615CFFCDD2D0FFB1B5
          B3FFB1B5B3FFB1B5B3FFB1B5B3FFB1B5B3FFB1B5B3FFB1B5B3FFB1B5B3FFB1B5
          B3FFB1B5B3FFB1B5B3FFC0C4C2FFBCC9CEFF3A7390FA70B7DDFF2F97CEFF1E7F
          B3FF0D405BE30105071400000000000000000000000000000000000000000000
          0000000000006F7371C3E9ECEBFFDCE1DFFFB8BBB8FFD7DCDAFFDDE2E0FFDDE2
          E0FFDDE2E0FFDDE2E0FFDDE2E0FFDDE2E0FFDDE2E0FFDDE2E0FFDDE2E0FFDDE2
          E0FFDDE2E0FFDDE2E0FFDDE2E0FFE9ECEBFF395C6DE55396B9F963B1DAFF2491
          CBFF196C99FE0B374EC000000000000000000000000000000000000000000000
          000000000000707372C3DED4C9FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6
          A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6A5FFCBB6
          A5FFCBB6A5FFCBB6A4FFCBB6A4FFDED4C9FF6E7271C414455FD56BB1D6FF4BA5
          D5FF218EC8FF165479F2061F2C6C000000000000000000000000000000000000
          000000000000707372C3CBA789FFC28D5FFFC28D5FFFC28D5FFFC28D5FFFC28D
          5FFFC28D5FFFC28D5FFFC28D5FFFC28D5FFFC28D5FFFC28D5FFFC28D5FFFC28D
          5FFFC28D5FFFC28D5FFFC28D5FFFCBA789FF707372C3020B102B2D6381E972B8
          DEFF3399CFFF667EB4FF37557EF102080C210000000000000000000000000000
          000000000000707372C3CFAC90FFA6825FFF463F35FF846A51FFC8996EFFBA8F
          66FFBA8F66FFBA8F66FFBA8F66FFBA8F66FFBA8F66FFBA8F66FFBA8F66FFBA8F
          66FFBA8F66FFBA8F66FFC1946AFFCFAC90FF707372C30000000008273786709B
          C1FB98A5D2FFB3B3B4FF5C61ABFF0B3958D10001010200000000000000000000
          000000000000707372C3CFAC90FF7D664EFF36342EFF51483BFFBA8F66FF9170
          50FF917050FF917050FF917050FF917050FF917050FF917050FF917050FF9170
          50FF917050FF917050FFA6805CFFCFAC90FF707372C300000000000101022652
          6AD7B2B4D0FF9191D0FF1D1DD7FF0E2D99FC061C286600000000000000000000
          000000000000707472C3CFAC90FFC2956CFF846B51FFB18A65FFCF9F72FFCF9F
          72FFCF9F72FFCF9F72FFCF9F72FFCF9F72FFCF9F72FFCF9F72FFCF9F72FFCF9F
          72FFCF9F72FFCF9F72FFCF9F72FFCFAC90FF707472C300000000000000000209
          0D254D769CED7C7CFFFF2B2BFFFF131EC8FF0A3146A400000000000000000000
          000000000000707473C3C9A587FFB58156FFB58156FFB58156FFB58156FFB581
          56FFB58156FFB58156FFB58156FFB58156FFB58156FFB58156FFB58156FFB581
          56FFB58156FFB58156FFB58156FFC9A587FF707473C300000000000000000000
          0000072332797291C9F76569FCFF1641A9F404161E4D00000000000000000000
          000000000000717473C3EFF0EFFFE6E7E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7
          E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7E5FFE6E7
          E5FFE6E7E5FFE6E7E5FFE6E7E5FFEFF0EFFF717473C300000000000000000000
          00000000000004121A430A354AB004161F4F0000000000000000000000000000
          000000000000717473C3F1F3F3FFD6D8D7FF888783FFC0C0BFFFEAECECFFEAEC
          ECFFEAECECFFEAECECFFE9ECECFFE9ECECFFE9ECECFFE9ECECFFE9ECECFFE9EC
          ECFFE9ECECFFE9ECECFFE9ECECFFF1F3F3FF717473C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000717473C3F2F3F3FF888884FF36342EFF55534EFFD3D5D5FFA5A6
          A6FFA5A6A6FFA5A6A6FFA5A6A6FFA5A6A6FFA5A6A6FFA5A6A6FFA5A6A6FFA5A6
          A6FFA5A6A6FFA5A6A6FFBDBFBFFFF2F3F3FF717473C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000717573C3F3F4F4FFC1C2C1FF55534EFF9B9C99FFE4E6E6FFD4D6
          D6FFD4D6D6FFD4D6D6FFD4D6D6FFD4D6D6FFD4D6D6FFD4D6D6FFD4D6D6FFD4D6
          D6FFD4D6D6FFD4D6D6FFDCDEDEFFF3F4F4FF717573C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000717573C3F4F5F5FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1
          F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1F1FFEFF1
          F1FFEFF1F1FFEFF1F1FFEFF1F1FFF4F5F5FF717573C300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000636665AD9B9E9EE79EA1A0E89EA1A0E89EA1A0E89EA1A0E89EA1
          A0E89EA1A0E89EA1A0E89EA1A0E89EA1A0E89EA1A0E89EA1A0E89EA1A0E89EA1
          A0E89EA1A0E89EA1A0E89EA1A0E89B9E9EE7636665AD00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000070000002B0000004E0000004E0000
          002B000000070000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000D0000
          00190000000A0000000100000000000000147B7B7BBBF0F0F0FFF0F0F0FF7B7B
          7BBB0000001400000000000000010000000A000000190000000D000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000040000001D101010667B79
          79C90E0E0E4E0000000A0000000100000019838383C9F0F0F0FFF0F0F0FF8383
          83C900000019000000010000000A0E0E0E4E7C7B7BC9101010660000001D0000
          0004000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000115B5757A5D0CACAFBE4DF
          DFFF858484D6000000310000001D0000003A8A8A8AD8F0F0F0FFF0F0F0FF8A8A
          8AD80000003A0000001D00000031878585D6E7E4E4FFD3CECEFB5B5858A50000
          0011000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000C726F6FABDDD6D6FFE1DC
          DCFFE3DFDFFF444343B8585757C0999999E8E1E1E1FDF3F3F3FFF3F3F3FFE1E1
          E1FD9A9A9AE8595858C0444444B8E6E3E3FFE4E0E0FFE1DADAFF747171AB0000
          000C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000209090931C5BDBDF5DED7
          D7FFE2DCDCFFE9E6E6FFECEAEAFFEDECECFFEDECECFFEDECECFFEDECECFFEEED
          EDFFEFEDEDFFEEEBEBFFEBE8E8FFE4E0E0FFE1DBDBFFC7C1C1F5090909310000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000040000001600000018000000090000000700000030797575D9DED7
          D7FFE4DEDEFFE1DBDBFFE4DFDFFFE6E2E2FFE7E4E4FFE8E5E5FFE9E6E6FFE8E5
          E5FFE7E4E4FFE5E1E1FFE3DEDEFFE6E0E0FFE1DADAFF7B7777D9000000300000
          0007000000090000001800000016000000040000000000000000000000000000
          00010000001C494242AA524A4ABA040404500000003B4B4646B6D9D0D0FFDCD2
          D2FFDBD2D2FFDDD6D6FFE0D9D9FFE2DDDDFFE9E5E5FFEFECECFFEFECECFFE9E5
          E5FFE4DEDEFFE1DBDBFFDFD8D8FFDDD5D5FFDDD5D5FFDBD3D3FF4C4747B60000
          003B04040450534C4CBA4A4343AA0000001C0000000100000000000000000000
          00080D0C0C54B29E9EFBC3B0B0FFAB9C9CF96D6464E2D1C5C5FFD2C6C6FFD4C9
          C9FFD7CCCCFFDDD5D5FFF4F0F0FFF7F4F4FFF4F0F0FFF1EBEBFFF1EBEBFFF4F0
          F0FFF7F4F4FFF4F1F1FFDFD6D6FFD9CFCFFFD6CBCBFFD4CACAFFD3C7C7FF6E66
          66E2ADA0A0F9C6B4B4FFB4A2A2FB0D0C0C540000000800000000000000000000
          0010766C6CBFBCA7A7FFC0ACACFFC4B1B1FFC9B9B9FFCEC1C1FFCDBFBFFFD0C3
          C3FFE2DADAFFF6F3F3FFF1EAEAFFEEE8E8FFE5DFDFFFD8D5D5FCD8D5D5FCE5DF
          DFFFEEE8E8FFF1EAEAFFF6F3F3FFE3DBDBFFD2C6C6FFCFC2C2FFD1C3C3FFCBBC
          BCFFC6B5B5FFC3B0B0FFBFABABFF766D6DBF0000001000000000000000000000
          000419181838948B8BD0C3B0B0FFC0ADADFFCAB9B9FFC7B6B6FFC9BABAFFDDD4
          D4FFF4EFEFFFF0E9E9FFDCD8D8FE9F9E9ECA4A4A4A6212121219121212194A4A
          4A629F9E9ECADCD8D8FEF0E9E9FFF4EFEFFFDFD5D5FFCCBDBDFFC9B9B9FFCCBD
          BDFFC3B0B0FFC5B4B4FF948B8BD0191818380000000400000000000000000000
          00000000000300000019504B4BA3C0ACACFFC1ADADFFC3B0B0FFCCBBBBFFF4F0
          F0FFEAE3E3FFC4C2C2F54544445B000000000000000000000000000000000000
          0000000000004544445BC4C2C2F5EAE3E3FFF4F0F0FFCCBEBEFFC5B3B3FFC3B0
          B0FFC2AFAFFF514B4BA300000019000000030000000000000000000000000000
          00000000000100000014463D3DAABDA8A8FFBCA7A7FFBFABABFFEAE4E4FFDED6
          D6FFCAC5C5FE4241415B00000000000000000000000000000000000000000000
          000000000000000000004241415BCAC5C5FEDED6D6FFEAE5E5FFC1AEAEFFBEAA
          AAFFBDA9A9FF463D3DAA00000014000000010000000000000000000000060000
          001C000000220000003B776969DFBDA7A7FFBBA5A5FFBDA7A7FFE1DBDBFFCFC7
          C7FF8B8A8ACA0000000000000000000000000000000000000000000000000000
          00000000000000000000000000008B8A8ACACFC7C7FFE0DBDBFFBFAAAAFFBBA5
          A5FFBDA7A7FF776969DF0000003B000000220000001C000000060000001D6056
          56CA665B5BDC6C5F5FE4AE9999FDBDA7A7FFBBA5A5FFC3B1B1FFCEC7C7FFBBB4
          B4FF3E3E3E620000000000000000000000000000000000000000000000000000
          00000000000000000000000000003E3E3E62BBB4B4FFCEC7C7FFC3B1B1FFBBA5
          A5FFBDA7A7FFAE9999FD6C5F5FE4665B5BDC605656CA0000001D00000029BBA5
          A5FFBBA5A5FFBBA5A5FFBCA6A6FFC2ADADFFC6B4B4FFCFC4C4FFBCB3B3FFA7A2
          A2FC0E0E0E190000000000000000000000000000000000000000000000000000
          00000000000000000000000000000E0E0E19A7A2A2FCBCB3B3FFCFC4C4FFC6B4
          B4FFC2ADADFFBCA6A6FFBBA5A5FFBBA5A5FFBBA5A5FF0000002900000026CCBC
          BCFFD7CACAFFDDD2D2FFE2D8D8FFE2D9D9FFE2D9D9FFD7D0D0FFB2A8A8FF9E98
          98FC0D0D0D190000000000000000000000000000000000000000000000000000
          00000000000000000000000000000D0D0D199E9898FCB2A8A8FFD7D0D0FFE2D9
          D9FFE2D9D9FFE2D8D8FFDDD2D2FFD7CACAFFCCBCBCFF000000260000000F9790
          90C5A19999D1A79F9FDCD7CCCCFDE5DCDCFFE5DCDCFFDCD4D4FFACA2A2FF9D94
          94FF343333620000000000000000000000000000000000000000000000000000
          0000000000000000000000000000343333629D9494FFACA2A2FFDCD4D4FFE5DC
          DCFFE5DCDCFFD7CCCCFDA79F9FDCA19999D1979090C50000000F000000020000
          000A0000000D00000025A39999E3E7E0E0FFE7E0E0FFE6DFDFFFA49B9BFF9C90
          90FF676565CB0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000676565CB9C9090FFA49B9BFFE6DFDFFFE7E0
          E0FFE7E0E0FFA39999E3000000250000000D0000000A00000002000000000000
          00000000000100000012665F5FB3EAE3E3FFEAE3E3FFEAE3E3FFAFA8A8FF978A
          8AFF877F7FFE2B2A2A5C00000000000000000000000000000000000000000000
          000000000000000000002B2A2A5C877F7FFE978A8AFFAFA8A8FFEAE3E3FFEAE3
          E3FFEAE3E3FF665F5FB300000012000000010000000000000000000000000000
          00020000000D00000032413B3BB2E9E2E2FFECE6E6FFECE6E6FFE3DCDCFF9189
          89FF908383FF756F6FF62826265C000000000000000000000000000000000000
          0000000000002826265C756F6FF6908383FF918989FFE3DCDCFFECE6E6FFECE6
          E6FFE9E2E2FF413B3BB2000000320000000D0000000200000000000000000000
          000A0B0A0A4E8C8181E3E7E0E0FFEFEAEAFFEFEAEAFFEFEAEAFFEFEAEAFFC8C2
          C2FF887C7CFF8B7E7EFF7A7373FE545050CD272626650909091A0909091A2726
          2665545050CD7A7373FE8B7E7EFF887C7CFFC8C2C2FFEFEAEAFFEFEAEAFFEFEA
          EAFFEFEAEAFFE7E0E0FF8C8181E30B0A0A4E0000000A00000000000000000000
          000E8C8383C7F1EDEDFFF1EDEDFFF1EDEDFFF1EDEDFFF1EDEDFFF1EDEDFFF1ED
          EDFFC6BEBEFF807575FF877A7AFF867979FF7C7272FF706868FD706868FD7C72
          72FF867979FF877A7AFF807575FFC6BEBEFFF1EDEDFFF1EDEDFFF1EDEDFFF1ED
          EDFFF1EDEDFFF1EDEDFFF1EDEDFF8C8383C70000000E00000000000000000000
          00041B1A1A49E1DADAFDF3EFEFFFDBD3D3F9A19A9AD9EDE8E8FFF4F1F1FFF4F1
          F1FFF4F1F1FFE6E2E2FF8D8383FF716464FF746868FF796C6CFF796C6CFF7468
          68FF716464FF8D8383FFE6E2E2FFF4F1F1FFF4F1F1FFF4F1F1FFEEE9E9FFA29C
          9CDADBD3D3F9F3EFEFFFE1DADAFD1B1A1A490000000400000000000000000000
          00000000000D756F6FA4888282B40908082900000019756E6EAFEFEBEBFFF6F4
          F4FFF6F4F4FFF6F4F4FFF6F4F4FFF2F0F0FFD6D2D2FFB6B0B0FFB6B0B0FFD6D2
          D2FFF2F0F0FFF6F4F4FFF6F4F4FFF6F4F4FFF6F4F4FFF0EBEBFF757070AF0000
          001909080829888282B4756F6FA40000000D0000000000000000000000000000
          00000000000200000008000000070000000200000002000000248F8787DFF9F7
          F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7
          F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FFF9F7F7FF8F8787DF000000240000
          0002000000020000000700000008000000020000000000000000000000000000
          0000000000000000000000000000000000000000000704030341D5CDCDFBFCFA
          FAFFFCFAFAFFF8F6F6FFFCFAFAFFFCFAFAFFFCFAFAFFFCFAFAFFFCFAFAFFFCFA
          FAFFFCFAFAFFFCFAFAFFF8F6F6FFFCFAFAFFFCFAFAFFD5CDCDFB040303410000
          0007000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000012665E5EB5FDFCFCFFFEFE
          FEFFF5F2F2FF635E5EA2878080B4C3BCBCE9EEE9E9FEFEFEFEFFFEFEFEFFEEE9
          E9FEC3BCBCE9878080B4635E5EA2F5F2F2FFFEFEFEFFFDFCFCFF665E5EB50000
          0012000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000097D7777ACEBE6E6FDFFFF
          FFFFA59C9CDA0000001800000008000000279B9393E0FFFFFFFFFFFFFFFF9B93
          93E0000000270000000800000018A59C9CDAFFFFFFFFEBE6E6FD7D7777AC0000
          0009000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000000B1E1D1D43A29A
          9ACB16151533000000040000000000000016938A8AD7FFFFFFFFFFFFFFFF938A
          8AD700000016000000000000000416151533A29A9ACB1E1D1D430000000B0000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          00090000000200000000000000000000000F8F8787C5F2EFEFFFF2EFEFFF8F87
          87C50000000F0000000000000000000000020000000900000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000003000000100000001E0000001E0000
          0010000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000040000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000E0000002700000015000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000020002032E00283D8700070B3E00000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000B3E3E3E8C909090E6575757B80202022C0000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000030003053700648CD300B1E4FF019AC9E9000A0F36000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000006060624939393D50808081B6161619C757575C90101
          0126000000010000000000000000000000000000000000000000000000000000
          00040005083E006D96DA00B4E5FF0DD1F2FF07CBF1FF00436281000000010202
          0105020201050202010502020105020201050201000502010005020100050201
          000502010005020100050A0909228A8A8ACC0101003A4A4A4AA0989898FF6868
          68BD020100220201000502010005020100050201000502010005020100080108
          0C4800759FE000B5E5FF1BD5F4FF11D2F3FF0085B0DA000E1523010000020906
          04140A0704140A07041409060314080402140804021408040214090502140905
          02140905021409050214090502165D5B598AA0A0A0EC878887FC1B1C1CFF7B7B
          7BFF535151AE0804022809050214080502140804021408040214070503340172
          A3E400B9E7FF2BD9F5FF18D2F2FF007DA8D2000A0F1E0000000001010008130D
          09560B0907620706056206050462070504620705046207050462070504620705
          046207050462070504620705046207050465595858C29C9D9CFF3E413FFF1D1E
          1EFF848484FE3C3C3BAD0705046807050462070504640705047004384DB400C2
          EBFF3CE3F9FF1DC8EDFF01769FE500070B1F000000000000000001010008754F
          33F5A66C47FFCA8C68FF000000FF000000FF000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FF000000FF000000FF4A4A4AFFA1A1A1FF4043
          42FF232424FF8A8A8AFF262626FF000000FF001019FF006390FF00B6E5FF00DC
          F8FF15DDF8FF07719CFF485551F80100000C0000000000000000000000028055
          37FDA37354FFB68A6FFF1A130EFF1B130EFF1B130EFF1C140FFF1C140FFF1C14
          0FFF1D140FFF1D150FFF1D150FFF1D150FFF1F1712FF1F1813FF58514DFFA7A6
          A6FF434645FF2A2B2AFF898989FF231F1DFF11516CFF00C2EFFF01BFFBFF06AE
          D8FF284D57FF2B2E2BFF4E645FFF010100060000000000000000000000027449
          2AFD835B3AFF845B3AFF7B4F31FF6D3D21FF6E3E21FF704022FF734223FF7544
          23FF774624FF794725FF7B4825FF7C4926FF7E4C2AFF875C3DFF785034FF765B
          47FFA9A7A6FF474948FF313332FF706E6CFFADA7A3FF7BCFDEFF48C5EEFF297B
          93FF637A6FFF5D6F66FF5C4938FF02010006000000000000000000000001764B
          2BFD7E532FFF7F532FFF80542FFF7C4E2AFF6A3716FF663210FF693411FF6C36
          12FF6E3813FF713A14FF723B15FF743D15FF743D16FF76411BFF7E5030FF6D44
          27FF6A4E39FFA7A5A3FF404141FFAFAFAEFFBCBCBBFF525350FF4C8390FF3A6D
          66FF48625BFF473B30FF562911FF010100060000000000000000000000018357
          33FD895E37FF895E37FF885E37FF885D37FF865C36FF744725FF623313FF6835
          12FF6E3813FF713B14FF743D15FF763E16FF773F16FF773F16FF77411BFF7F53
          33FF6B452AFF533F30FFC8C7C7FFBEBEBDFF5F605FFF777E7FFF345758FF4155
          4DFF523721FF57280DFF592910FF010100060000000000000000000000018A5A
          33FD926338FF936438FF956539FF99683AFF9C6A3BFFA16D3CFFA16B3AFF8F55
          28FF884F22FF854D22FF824C22FF804B21FF7C4820FF78451EFF72411CFF6B3B
          18FF644833FFC4C0BEFFBEBCBAFF999491FF585A59FF5D5E5DFF707474FF3F2A
          1BFF50240BFF4F230BFF562C15FF010100060000000000000000000000017C59
          35FC86623CFF8A643CFF8B653DFF8B653DFF8A653CFF88633CFF85623BFF8360
          3AFF6D4827FF5F3719FF673A16FF7A461BFF8A4F20FF945622FF955723FF7951
          30FFC8C4C1FFC9C3BFFF7D5C43FF87664CFFB9B2ACFF717372FF9E9E9EFF7170
          6FFF655D58FF5A4436FF5C3923FF030201320000000400000000000000018F61
          36FC97693CFF96693CFF97693CFF996A3DFF9A6B3DFF9D6D3EFFA06F3EFFA471
          3FFFA97541FFA76F3BFF955B2BFF875326FF7A4B23FF663E1DFF553D2AFFC7C4
          C2FFBFBAB7FF6A452AFF876044FF8A6449FF694831FFBAB4B0FFD8D7D7FF6D6F
          6EFF7F817FFF969796FF7A7B7AFF606160DD050505380000000100000001885F
          36FC92683DFF93693DFF94693DFF956A3DFF966A3DFF976B3DFF996B3DFF9A6C
          3DFF9B6D3DFF9C6D3EFF9B6B3CFF895327FF80481BFF735033FFCCC8C6FFC8BF
          B8FF7C512FFF7D5C42FF765A44FF5C402DFF604330FF634D3FFFA4A4A3FFA4A7
          A5FF717371FFC1C3C2FFA4A6A5FFB9B9B9FF484948B60000000D000000018C62
          38FC966C3FFF976C3FFF986D3FFF996D40FF9A6E40FF9B6E40FF9C6F40FF9D70
          40FF9E7040FF9F7140FF9F7140FF8D6438FF6F5743FFC7C6C4FFBFB5AEFF7245
          24FF875B3BFF8E603FFF844B21FF894C1EFF845532FF72523BFFA8A6A4FFD6D6
          D6FFCBCCCBFFD2D4D3FF868582FF7C7D7CA98D8E8DEF0303031D000000018F66
          3AFC9A7041FF9A7041FF9A7041FF997041FF966E41FF936D41FF967044FF9972
          45FF9B7445FF9C7446FF8C683EFFABA297FFC2C2C1FFB7B2ABFF6F4B2DFF7B53
          36FF7F5637FF713E1AFF6F3914FF6C3713FF6A3512FF754628FF948981FFD4D5
          D5FFE9E9E9FFC0B7B0FF774D33FF020100070D0D0D1D02020208000000018E67
          3BFC96764AFF82A187FF92916CFFA87D49FFB0814BFFB5844CFFB48149FFB281
          49FFB28048FFB28048FFAA8C69FFF4F4F3FFD9D8D5FF9D774CFFBC915FFFAD81
          56FF895429FF7B471FFF6E3E1BFF633717FF592E11FF5E3114FF786457FFE9E9
          E9FFF5F5F5FFA1958EFF5C3724FF0101000B000000000000000000000000A376
          43FCB1986CFFA0DACCFFA4BBA4FFA57946FFA37845FFA47845FFA57946FFA679
          46FFA67946FFA77A46FFA27745FFBDA991FFAA8963FFAC8455FFAC8354FFA376
          44FF9A6B3EFF804921FF7C4118FF814419FF86481CFF7B441DFF735038FFD3D3
          D3FFEFF0EFFFDBDADAFF857D78FF1C1B1B6E0000000400000000000000009970
          3FFCA17947FFAAA07AFF9E7D4EFF9A7646FF997545FF997545FF9A7645FF9C76
          45FF9F7745FFA27946FFA47A46FFA87E4BFFB28A58FFAE8654FFA67A47FFA477
          44FFA27543FF9F7243FF84532DFF6A3512FF673211FF653110FF6F3E1FFF886C
          5CFFCBC9C7FFE2E3E2FFDAD5D2FF535353670000000100000000000000009B74
          43FBAD844CFFB2864DFFB8894FFFBB8B4FFFBC8C50FFBD8C50FFBD8D51FFBC8D
          51FFBA8B51FFB78A50FFB2874FFFAE8550FFAE8956FFA6814FFF9F7A49FF9A76
          47FF967143FF997142FF9A7042FF8E633BFF6B3918FF632F0FFF633215FF6B3D
          23FF673B22FF623922FF5D3018FF02010006000000000000000000000000A77A
          43FBAF9572FFB5B4B2FFB9A07FFFAC824AFFAC824AFFAD824AFFAD824AFFAD82
          4AFFAE824AFFAF824AFFB1834BFFB3854BFFB5864CFFB7874CFFB9884DFFBA88
          4DFFBA894FFFB3854EFFAA7F4BFFA27948FF987246FF6B4628FF542C12FF532F
          18FF52301BFF4C2B17FF4D2E1BFF01010005000000000000000000000000A178
          43FBBFAA8CFFD4D3D2FFC7B194FFAF854BFFAF844BFFB0854BFFB0844BFFB084
          4BFFB0844BFFB0844BFFB0834BFFAF834AFFAE824AFFAE814AFFAC8049FFAA7E
          48FFA97C48FFA77B47FFA77B46FFA87A46FFA87A46FFA97A46FF996538FF7A3D
          16FF773812FF763611FF783C19FF02010005000000000000000000000000A47A
          44FBB19267FF796D5DFF95784FFFB2884DFFB2884DFFB2874DFFB2874DFFB287
          4CFFB2874CFFB2874CFFB2864CFFB1854CFFB1844BFFAF834BFFAE824AFFAD81
          4AFFAB7F49FFA97D48FFA77B47FFA47946FFA27745FF9F7544FF9D7243FF9064
          39FF633315FF55240AFF592A13FF01010005000000000000000000000000A77D
          45FBB58B4DFFB58B4DFFB58B4DFFB58B4DFFB58A4DFFB58A4DFFB58A4EFFB58A
          4EFFB5894EFFB4894DFFB4884DFFB3884DFFB3874CFFB2864CFFB0854BFFAF83
          4BFFAD824AFFAB8049FFA97E48FFA77C48FFA47A46FFA27845FF9F7544FF9D73
          43FF986E41FF734523FF582A13FF01010005000000000000000000000000A685
          58E5B28950FEB1874EFEB28850FEB38951FEB48A52FEB48B54FEB58C55FEB68D
          56FEB68E57FEB68E57FEB78E58FEB68E58FEB68D58FEB58D58FEB48B58FEB28A
          57FEB08855FEAE8654FEAC8352FEA98151FEA67F4FFEA47C4DFEA0794BFE9D76
          49FE9A7246FE977045FE8F7053EF010100020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000E0000005E0000006A0000006A0000
          006A0000006A0000006A0000006A0000006A0000006A0000006A0000006A0000
          006A0000006A0000006A0000006A0000006A0000006A0000006A0000006A0000
          005E0000000E0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000101014B929292DAFEFEFEFFFEFEFEFFFEFE
          FEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
          FEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFF9292
          92DA0101014C0000000000000000000000000000000000000000000000000000
          000000000000000000000000000208080882949494FFFDFDFDFFF3F3F3FFF2F2
          F2FFF3F3F3FFF2F2F2FFF3F3F3FFF3F3F3FFF4F4F4FFF4F4F4FFF2F2F2FFF4F4
          F4FFF4F4F4FFF1F1F1FFF5F5F5FFF9F9F9FFF9F9F9FFF9F9F9FFFDFDFDFF9494
          94FF080808820000000200000000000000000000000000000000000000000000
          00000000000000000000000000020B0B0B82858585FFFDFDFDFFF8F8F8FFF7F7
          F7FFF5F5F5FFF7F7F7FFF3F3F3FFF7F7F7FFF7F7F7FFF3F3F3FFF5F5F5FFF7F7
          F7FFF7F7F7FFF4F4F4FFF5F5F5FFF7F7F7FFF5F5F5FFF7F7F7FFFDFDFDFF8585
          85FF0B0B0B820000000200000000000000000000000000000000000000000000
          000000000000000000000000000211111182808080FFF9F8F8FFF0EFEFFFEEED
          EDFFF0EFEFFFEFEEEEFFEEEDEDFFF1F0F0FFEFEEEEFFF0EFEFFFEDECECFFF1F0
          F0FFF0EFEFFFF0EFEFFFEFEEEEFFEFEEEEFFEFEEEEFFEFEEEEFFF9F8F8FF8080
          80FF111111820000000200000000000000000000000000000000000000020000
          001100000014000000140000001610101083706F6FFFEDECECFFEAE9E9FFEAE9
          E9FFE9E8E8FFEAE9E9FFEAE9E9FFE8E7E7FFEAE9E9FFE9E8E8FFE9E8E8FFE9E8
          E8FFE9E8E8FFEAE9E9FFE8E7E7FFEBEAEAFFEAE9E9FFEAE9E9FFEDECECFF706F
          6FFF1010108300000016000000140000001400000011000000020000001F2A2A
          2AAC333333B7333333B7333333B7414141CA616161EFD2CECEFFCBC7C7FFC9C5
          C5FFC8C4C4FFC8C4C4FFC7C4C4FFC9C5C5FFC8C4C4FFCCC8C8FFC7C3C3FFCBC7
          C7FFCBC8C8FFCAC6C6FFD2CECEFFD0CCCCFFD0CCCCFFD1CDCDFFD2CECEFF6160
          60EF414141CA333333B7333333B7333333B72A2A2AAA0000001F0000001D8E8E
          8EE1BBBBBBFFBBBBBBFFBBBBBBFFA7A7A7FF535353FF978E8EFF958C8CFF938B
          8BFF908888FF938B8BFF928989FF928989FF908888FF938B8BFF918989FF9189
          89FF938A8AFF8F8787FF948B8BFF908888FF928989FF968D8DFF978E8EFF5353
          53FFA7A7A7FFBBBBBBFFBBBBBBFFBBBBBBFF8D8D8DE00000001D000000079C9C
          9CE1C4C4C4FFC4C4C4FFC4C4C4FFAFAFAFFF222222FF2C2A2AFF2C2A2AFF2C2A
          2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A
          2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2222
          22FFAFAFAFFFC4C4C4FFB9B9B9FFC5C5C5FF9C9C9CE10000000700000000AAAA
          AAE3CCCCCCFFCCCCCCFFCCCCCCFFC4C4C4FF171717FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1717
          17FFC4C4C4FFCCCCCCFF9A9A9AFFC0C0C0FFAAAAAAE30000000000000000BABA
          BAE8D4D4D4FFD4D4D4FFD4D4D4FFD6D6D6FFABABABFF7F7F7FFF7F7F7FFF7E7E
          7EFF7D7D7DFF7C7C7CFF7B7B7BFF7A7A7AFF797979FF787878FF787878FF7979
          79FF7A7A7AFF7B7B7BFF7B7B7BFF7C7C7CFF7D7D7DFF7E7E7EFF7F7F7FFFABAB
          ABFFD6D6D6FFD4D4D4FFB4B4B4FFD0D0D0FFBABABAE80000000000000000CBCB
          CBF7DDDDDDFFDDDDDDFFDDDDDDFFDDDDDDFFD9D9D9FFD2D2D2FFD2D2D2FFD2D2
          D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2
          D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD9D9
          D9FFDDDDDDFFDDDDDDFFABABABFFD2D2D2FFCBCBCBF70000000000000000BFBF
          BFFBC9C9C9FFCACACAFFCBCBCBFFCCCCCCFFBEBEBEFF9C9C9CFF9D9D9DFF9E9E
          9EFF9F9F9FFFA0A0A0FF8B8B8BFF575757FF616161FF636363FF696969FF6161
          61FF5F5F5FFF8D8D8DFFA2A2A2FFA1A1A1FFA0A0A0FF9F9F9FFF9E9E9EFFC3C3
          C3FFCFCFCFFFCFCFCFFFCECECEFFCDCDCDFFC3C3C3FB0000000000000000C5C5
          C5EAE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFBCBCBCFF414141FF3F3F3FFF3F3F
          3FFF3F3F3FFF3F3F3FFF404040FF1C1C1CFF2A2A2AFF373737FF393939FF3737
          37FF313131FF404040FF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF414141FFC0C0
          C0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFC5C5C5EA0000000000000000C4C4
          C4E0E8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFC5C5C5FF515151FF4F4F4FFF4F4F
          4FFF4F4F4FFF4F4F4FFF4F4F4FFF565656FF616161FF616161FF616161FF6161
          61FF565656FF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF515151FFC8C8
          C8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFC4C4C4E00000000000000000C0C0
          C0D7EEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFCDCDCDFF575757FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF575757FFD1D1
          D1FFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFC0C0C0D70000000000000000B6B6
          B6CDEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFD3D3D3FF585858FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF585858FFD7D7
          D7FFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFB6B6B6CD00000000000000007575
          759C949494C19E9E9EC1A0A0A0C1A3A3A3C1A5A5A5D4C3C3C3F0C5C5C5F0C5C5
          C5F0C6C6C6F0C6C6C6F0C6C6C6F0C6C6C6F0C7C7C7F0C7C7C7F0C7C7C7F0C7C7
          C7F0C7C7C7F0C6C6C6F0C6C6C6F0C6C6C6F0C6C6C6F0C5C5C5F0C3C3C3F0A7A7
          A7D3A3A3A3C1A3A3A3C1A0A0A0C19E9E9EC17A7A7A9C00000000000000003E3E
          3E759D9D9DFFB2B2B2FFBDBDBDFFC1C1C1FFAEAEAEFF4D4D4DFF494949FF4848
          48FF474747FF464646FF464646FF464646FF454545FF444444FF444444FF4545
          45FF464646FF464646FF464646FF474747FF484848FF494949FF4D4D4DFFB4B4
          B4FFC5C5C5FFC1C1C1FFBDBDBDFFB3B3B3FF4848487500000000000000001D1D
          1D36959595FFA9A9A9FFBBBBBBFFBFBFBFFFACACACFF6E6E6EFFB8B3B3FFBFBA
          BAFFC5C0C0FFCAC5C5FFCECACAFFD2CECEFFD4D0D0FFD6D2D2FFD6D3D3FFD6D2
          D2FFD4D0D0FFD2CECEFFCECACAFFCAC5C5FFC5C0C0FFBFBABAFF6E6E6EFFB1B1
          B1FFC3C3C3FFBFBFBFFFBBBBBBFFA9A9A9FF2020203600000000000000000202
          0204848484F39F9F9FFFB2B2B2FFBDBDBDFFB2B2B2FF787878FFC4BFBFFFCBC7
          C7FFD1CDCDFFD7D3D3FFDCD8D8FFDFDCDCFFE1DEDEFFE2DFDFFFE3E0E0FFE2DF
          DFFFE1DEDEFFDFDCDCFFDCD8D8FFD7D3D3FFD1CDCDFFCBC7C7FF787878FFB5B5
          B5FFC0C0C0FFBDBDBDFFB2B2B2FF989898F30202020400000000000000000000
          0000000000000000000000000000000000000E0E0E187F7F7FFFD0CCCCFFD7D4
          D4FFDEDBDBFFE2E0E0FFE6E4E4FFE9E7E7FFEBEAEAFFEDEBEBFFEDECECFFEDEB
          EBFFEBEAEAFFE9E7E7FFE6E4E4FFE2E0E0FFDEDBDBFFD7D4D4FF7F7F7FFF0E0E
          0E18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E0E0E18868686FFDBD8D8FFE2DF
          DFFFE7E5E5FFECEAEAFFF0EFEFFFF4F2F2FFF6F5F5FFF7F6F6FFF7F6F6FFF7F6
          F6FFF6F5F5FFF4F2F2FFF0EFEFFFECEAEAFFE7E5E5FFE2DFDFFF868686FF0E0E
          0E18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000D0D0D18828282FFE4E1E1FFEAE8
          E8FFF0EFEFFFF6F5F5FFF8F8F8FFFBFAFAFFFCFCFCFFFDFDFDFFFEFEFEFFFDFD
          FDFFFCFCFCFFFBFAFAFFF8F8F8FFF6F5F5FFF0EFEFFFEAE8E8FF828282FF0D0D
          0D18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000808080E858585FDECEAEAFFF3F2
          F2FFF8F7F7FFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF8F7F7FFF3F2F2FF858585FD0808
          080E000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028F4F2F2FFF9F8
          F8FFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFF9F8F8FF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028F9F8F8FFFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028FDFDFDFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000009000000280000
          0028000000280000002800000028000000280000002800000028000000280000
          0028000000280000002800000028000000280000002800000028000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000E0000005E0000006A0000006A0000
          006A0000006A0000006A0000006A0000006A0000006A0000006A0000006A0000
          006A0000006A0000006A0000006A0000006A0000006A0000006A0000006A0000
          005E0000000E0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000101014B929292DAFEFEFEFFFEFEFEFFFEFE
          FEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
          FEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFF9292
          92DA0101014C0000000000000000000000000000000000000000000000000000
          000000000000000000000000000208080882949494FFFDFDFDFFF3F3F3FFF2F2
          F2FFF3F3F3FFF2F2F2FFF3F3F3FFF3F3F3FFF4F4F4FFF4F4F4FFF2F2F2FFF4F4
          F4FFF4F4F4FFF1F1F1FFF5F5F5FFF9F9F9FFF9F9F9FFF9F9F9FFFDFDFDFF9494
          94FF080808820000000200000000000000000000000000000000000000000000
          00000000000000000000000000020B0B0B82858585FFFDFDFDFFF8F8F8FFF7F7
          F7FFF5F5F5FFF7F7F7FFF3F3F3FFF7F7F7FFF7F7F7FFF3F3F3FFF5F5F5FFF7F7
          F7FFF7F7F7FFF4F4F4FFF5F5F5FFF7F7F7FFF5F5F5FFF7F7F7FFFDFDFDFF8585
          85FF0B0B0B820000000200000000000000000000000000000000000000000000
          000000000000000000000000000211111182808080FFF9F8F8FFF0EFEFFFEEED
          EDFFF0EFEFFFEFEEEEFFEEEDEDFFF1F0F0FFEFEEEEFFF0EFEFFFEDECECFFF1F0
          F0FFF0EFEFFFF0EFEFFFEFEEEEFFEFEEEEFFEFEEEEFFEFEEEEFFF9F8F8FF8080
          80FF111111820000000200000000000000000000000000000000000000020000
          001100000014000000140000001610101083706F6FFFEDECECFFEAE9E9FFEAE9
          E9FFE9E8E8FFEAE9E9FFEAE9E9FFE8E7E7FFEAE9E9FFE9E8E8FFE9E8E8FFE9E8
          E8FFE9E8E8FFEAE9E9FFE8E7E7FFEBEAEAFFEAE9E9FFEAE9E9FFEDECECFF706F
          6FFF1010108300000016000000140000001400000011000000020000001F2A2A
          2AAC333333B7333333B7333333B7414141CA616161EFD2CECEFFCBC7C7FFC9C5
          C5FFC8C4C4FFC8C4C4FFC7C4C4FFC9C5C5FFC8C4C4FFCCC8C8FFC7C3C3FFCBC7
          C7FFCBC8C8FFCAC6C6FFD2CECEFFD0CCCCFFD0CCCCFFD1CDCDFFD2CECEFF6160
          60EF414141CA333333B7333333B7333333B72A2A2AAA0000001F0000001D8E8E
          8EE1BBBBBBFFBBBBBBFFBBBBBBFFA7A7A7FF535353FF978E8EFF958C8CFF938B
          8BFF908888FF938B8BFF928989FF928989FF908888FF938B8BFF918989FF9189
          89FF938A8AFF8F8787FF948B8BFF908888FF928989FF968D8DFF978E8EFF5353
          53FFA7A7A7FFBBBBBBFFBBBBBBFFBBBBBBFF8D8D8DE00000001D000000079C9C
          9CE1C4C4C4FFC4C4C4FFC4C4C4FFAFAFAFFF222222FF2C2A2AFF2C2A2AFF2C2A
          2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A
          2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2C2A2AFF2222
          22FFAFAFAFFFC4C4C4FFB9B9B9FFC5C5C5FF9C9C9CE10000000700000000AAAA
          AAE3CCCCCCFFCCCCCCFFCCCCCCFFC4C4C4FF171717FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1717
          17FFC4C4C4FFCCCCCCFF9A9A9AFFC0C0C0FFAAAAAAE30000000000000000BABA
          BAE8D4D4D4FFD4D4D4FFD4D4D4FFD6D6D6FFABABABFF7F7F7FFF7F7F7FFF7E7E
          7EFF7D7D7DFF7C7C7CFF7B7B7BFF7A7A7AFF797979FF787878FF787878FF7979
          79FF7A7A7AFF7B7B7BFF7B7B7BFF7C7C7CFF7D7D7DFF7E7E7EFF7F7F7FFFABAB
          ABFFD6D6D6FFD4D4D4FFB4B4B4FFD0D0D0FFBABABAE80000000000000000CBCB
          CBF7DDDDDDFFDDDDDDFFDDDDDDFFDDDDDDFFD9D9D9FFD2D2D2FFD2D2D2FFD2D2
          D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2
          D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD9D9
          D9FFDDDDDDFFDDDDDDFFABABABFFD2D2D2FFCBCBCBF70000000000000000BFBF
          BFFBC9C9C9FFCACACAFFCBCBCBFFCCCCCCFFBEBEBEFF9C9C9CFF9D9D9DFF9E9E
          9EFF9F9F9FFFA0A0A0FF8B8B8BFF575757FF616161FF636363FF696969FF6161
          61FF5F5F5FFF8D8D8DFFA2A2A2FFA1A1A1FFA0A0A0FF9F9F9FFF9E9E9EFFC3C3
          C3FFCFCFCFFFCFCFCFFFCECECEFFCDCDCDFFC3C3C3FB0000000000000000C5C5
          C5EAE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFBCBCBCFF414141FF3F3F3FFF3F3F
          3FFF3F3F3FFF3F3F3FFF404040FF1C1C1CFF2A2A2AFF373737FF393939FF3737
          37FF313131FF404040FF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF414141FFC0C0
          C0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFC5C5C5EA0000000000000000C4C4
          C4E0E8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFC5C5C5FF515151FF4F4F4FFF4F4F
          4FFF4F4F4FFF4F4F4FFF4F4F4FFF565656FF616161FF616161FF616161FF6161
          61FF565656FF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF515151FFC8C8
          C8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFC4C4C4E00000000000000000C0C0
          C0D7EEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFCDCDCDFF575757FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF575757FFD1D1
          D1FFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFC0C0C0D70000000000000000B6B6
          B6CDEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFD3D3D3FF585858FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF555555FF5555
          55FF555555FF555555FF555555FF555555FF555555FF555555FF585858FFD7D7
          D7FFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFB6B6B6CD00000000000000007575
          759C949494C19E9E9EC1A0A0A0C1A3A3A3C1A5A5A5D4C3C3C3F0C5C5C5F0C5C5
          C5F0C6C6C6F0C6C6C6F0C6C6C6F0C6C6C6F0C7C7C7F0C7C7C7F0C7C7C7F0C7C7
          C7F0C7C7C7F0C6C6C6F0C6C6C6F0C6C6C6F0C6C6C6F0C5C5C5F0C3C3C3F0A7A7
          A7D3A3A3A3C1A3A3A3C1A0A0A0C19E9E9EC17A7A7A9C00000000000000003E3E
          3E759D9D9DFFB2B2B2FFBDBDBDFFC1C1C1FFAEAEAEFF4D4D4DFF494949FF4848
          48FF474747FF464646FF464646FF464646FF454545FF444444FF444444FF4545
          45FF464646FF464646FF464646FF474747FF484848FF494949FF4D4D4DFFB4B4
          B4FFC5C5C5FFC1C1C1FFBDBDBDFFB3B3B3FF4848487500000000000000001D1D
          1D36959595FFA9A9A9FFBBBBBBFFBFBFBFFFACACACFF6E6E6EFFB8B3B3FFBFBA
          BAFFC5C0C0FFCAC5C5FFCECACAFFD2CECEFFD4D0D0FFD6D2D2FFD6D3D3FFD6D2
          D2FFD4D0D0FFD2CECEFFCECACAFFCAC5C5FFC5C0C0FFBFBABAFF6E6E6EFFB1B1
          B1FFC3C3C3FFBFBFBFFFBBBBBBFFA9A9A9FF2020203600000000000000000202
          0204848484F39F9F9FFFB2B2B2FFBDBDBDFFB2B2B2FF787878FFC4BFBFFFCBC7
          C7FFD1CDCDFFD7D3D3FFDCD8D8FFDFDCDCFFE1DEDEFFE2DFDFFFE3E0E0FFE2DF
          DFFFE1DEDEFFDFDCDCFFDCD8D8FFD7D3D3FFD1CDCDFFCBC7C7FF787878FFB5B5
          B5FFC0C0C0FFBDBDBDFFB2B2B2FF989898F30202020400000000000000000000
          0000000000000000000000000000000000000E0E0E187F7F7FFFD0CCCCFFD7D4
          D4FFDEDBDBFFE2E0E0FFE6E4E4FFE9E7E7FFEBEAEAFFEDEBEBFFEDECECFFEDEB
          EBFFEBEAEAFFE9E7E7FFE6E4E4FFE2E0E0FFDEDBDBFFD7D4D4FF7F7F7FFF0E0E
          0E18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E0E0E18868686FFDBD8D8FFE2DF
          DFFFE7E5E5FFECEAEAFFF0EFEFFFF4F2F2FFF6F5F5FFF7F6F6FFF7F6F6FFF7F6
          F6FFF6F5F5FFF4F2F2FFF0EFEFFFECEAEAFFE7E5E5FFE2DFDFFF868686FF0E0E
          0E18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000D0D0D18828282FFE4E1E1FFEAE8
          E8FFF0EFEFFFF6F5F5FFF8F8F8FFFBFAFAFFFCFCFCFFFDFDFDFFFEFEFEFFFDFD
          FDFFFCFCFCFFFBFAFAFFF8F8F8FFF6F5F5FFF0EFEFFFEAE8E8FF828282FF0D0D
          0D18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000808080E858585FDECEAEAFFF3F2
          F2FFF8F7F7FFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF8F7F7FFF3F2F2FF858585FD0808
          080E000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028F4F2F2FFF9F8
          F8FFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFF9F8F8FF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028F9F8F8FFFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028FDFDFDFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000028FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000009000000280000
          0028000000280000002800000028000000280000002800000028000000280000
          0028000000280000002800000028000000280000002800000028000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000010000000200000002000000020000
          0001000000010000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010E0E
          0E2937373787555555B16A6A6ACC777777E1868686F0909090FB909090FB8383
          83F0767676E1656565CC535353B1373737860E0E0E2900000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010D0D0D2A39393991717171E5A1A1
          A1FED0D0D0FFEDEDEDFFBDBDBDFFB2B2B2FFAFAFAFFFACACACFFABABABFFACAC
          ACFFAFAFAFFFB2B2B2FFBDBDBDFFE9E9E9FFB5B5B5FE7D7D7DE5393939900D0D
          0D29000000010000000000000000000000000000000000000000000000000000
          00000000000000000000020202094A4A4AB0878787FE888888FFA5A5A5FFBFBF
          BFFFD8D8D8FFEEEEEEFFC9C9C9FFB4B4B4FFB2B2B2FFB0B0B0FFAFAFAFFFB0B0
          B0FFB2B2B2FFB4B4B4FFC9C9C9FFEEEEEEFFD8D8D8FFBFBFBFFFA4A4A4FF8282
          82FE4B4B4BAF0202020900000000000000000000000000000000000000000000
          0000000000000E0E0E30717171E69B9B9BFF8A8A8AFF919191FFAAAAAAFFC3C3
          C3FFD5D5D5FFE0E0E0FFDFDFDFFFDBDBDBFFE2E2E2FFB2B2B2FFB1B1B1FFE4E4
          E4FFDCDCDCFFD7D7D7FFDCDCDCFFE1E1E1FFD8D8D8FFC1C1C1FFAAAAAAFF9191
          91FF898989FF737373E50E0E0E2F000000000000000000000000000000000000
          00000E0E0E2F8F8F8FFB7A7A7AFF969696FF8C8C8CFFA8A8A8FFC8C8C8FFCFCF
          CFFFC0C0C0FFB1B1B1FFA4A4A4FF9C9C9CFF979797FF949494FF939393FF9696
          96FF9A9A9AFFA0A0A0FFB2B2B2FFC1C1C1FFD1D1D1FFD0D0D0FFB8B8B8FF9898
          98FF888888FF969696FF757575FA0E0E0E2D0000000000000000000000000000
          00016F6F6FD89E9E9EFF929292FFA8A8A8FFBDBDBDFFB5B5B5FF929292FF9C9C
          9CFFC9C9C9FFEDEDEDFFBDBDBDFFB2B2B2FFAEAEAEFFACACACFFABABABFFACAC
          ACFFAEAEAEFFB2B2B2FFBDBDBDFFE1E1E1FFAEAEAEFF999999FFB6B6B6FFC1C1
          C1FFA4A4A4FF949494FF929292FF6B6B6BD70000000100000000000000000000
          00029D9D9DFC9E9E9EFFB1B1B1FF999999FF878787FF858585FFA4A4A4FFBFBF
          BFFFD8D8D8FFEEEEEEFFC7C7C7FFB4B4B4FFB1B1B1FFB0B0B0FFAFAFAFFFB0B0
          B0FFB1B1B1FFB4B4B4FFC7C7C7FFEEEEEEFFD8D8D8FFBFBFBFFF9F9F9FFF8282
          82FF9A9A9AFFB2B2B2FF9B9B9BFF909090FC0000000200000000000000000000
          0002A6A6A6FDAFAFAFFF8E8E8EFF9A9A9AFF8B8B8BFF919191FFAAAAAAFFC1C1
          C1FFD7D7D7FFE1E1E1FFDCDCDCFFD6D6D6FFDDDDDDFFE4E4E4FFE6E6E6FFDFDF
          DFFFD6D6D6FFCFCFCFFFDBDBDBFFE5E5E5FFD6D6D6FFC1C1C1FFAAAAAAFF9191
          91FF898989FF909090FFB0B0B0FF989898FD0000000200000000000000000000
          00039E9E9EF4939393FF767676FF979797FF888888FFA1A1A1FFC3C3C3FFD4D4
          D4FFDFDFDFFFD1D1D1FFC3C3C3FFAFAFAFFFA8A8A8FF9F9F9FFF9F9F9FFFA8A8
          A8FFAFAFAFFFC4C4C4FFD2D2D2FFE0E0E0FFD7D7D7FFCDCDCDFFB3B3B3FF9494
          94FF888888FF979797FF7B7B7BFF9C9C9CF40000000300000000000000000000
          0003949494FC9F9F9FFF919191FFA2A2A2FFBEBEBEFFB7B7B7FFA6A6A6FF9898
          98FFABABABFFCBCBCBFFB2B2B2FFAEAEAEFFAEAEAEFFABABABFFAAAAAAFFABAB
          ABFFAAAAAAFFA7A7A7FFABABABFFBCBCBCFFA1A1A1FFA7A7A7FFD2D2D2FFC3C3
          C3FF9C9C9CFF949494FF919191FF919191FC0000000300000000000000000000
          0003A0A0A0FF9D9D9DFFADADADFFADADADFF8F8F8FFF818181FF9D9D9DFFBCBC
          BCFFD7D7D7FFEFEFEFFFC2C2C2FFB3B3B3FFB0B0B0FFAEAEAEFFAEAEAEFFAEAE
          AEFFB0B0B0FFB3B3B3FFC2C2C2FFEFEFEFFFD7D7D7FFB7B7B7FF919191FF8B8B
          8BFFAEAEAEFFAFAFAFFF9A9A9AFF959595FF0000000300000000000000000000
          0003A8A8A8FFB3B3B3FF9D9D9DFF959595FF8C8C8CFF8E8E8EFFA9A9A9FFC1C1
          C1FFD7D7D7FFECECECFFD9D9D9FFC3C3C3FFC7C7C7FFCFCFCFFFCFCFCFFFC7C7
          C7FFC2C2C2FFBFBFBFFFD7D7D7FFEBEBEBFFD7D7D7FFC1C1C1FFA9A9A9FF8E8E
          8EFF878787FF9D9D9DFFB3B3B3FF9B9B9BFF0000000300000000000000000000
          0003A3A3A3F7949494FF7E7E7EFF989898FF898989FF949494FFB5B5B5FFCFCF
          CFFFB9B9B9FFD3D3D3FFE7E7E7FFE4E4E4FFD6D6D6FFC9C9C9FFC9C9C9FFD7D7
          D7FFE5E5E5FFE9E9E9FFD5D5D5FFBDBDBDFFD7D7D7FFC5C5C5FFABABABFF9393
          93FF898989FF949494FF8C8C8CFF9E9E9EF60000000300000000000000000000
          0003959595FA9F9F9FFF8A8A8AFF999999FFAFAFAFFFC5C5C5FFD4D4D4FFA7A7
          A7FFA0A0A0FFAFAFAFFFA1A1A1FF9C9C9CFF9E9E9EFFA0A0A0FFA0A0A0FF9C9C
          9CFF999999FF989898FFA1A1A1FFA9A9A9FFA8A8A8FFD5D5D5FFC9C9C9FFB6B6
          B6FF8F8F8FFF949494FF8A8A8AFF949494FA0000000300000000000000000000
          00039E9E9EFF9C9C9CFFA0A0A0FFBABABAFF9F9F9FFF8B8B8BFF8F8F8FFFB5B5
          B5FFD5D5D5FFEEEEEEFFBFBFBFFFB3B3B3FFAFAFAFFFADADADFFADADADFFADAD
          ADFFAFAFAFFFB3B3B3FFBFBFBFFFEEEEEEFFCFCFCFFFA1A1A1FF919191FF9F9F
          9FFFBBBBBBFF9E9E9EFF979797FF959595FF0000000300000000000000000000
          0004A5A5A5FFA9A9A9FFAEAEAEFF8E8E8EFF8C8C8CFF8C8C8CFFA7A7A7FFC0C0
          C0FFD8D8D8FFEDEDEDFFCECECEFFB5B5B5FFB6B6B6FFB9B9B9FFB9B9B9FFB4B4
          B4FFB3B3B3FFB5B5B5FFCECECEFFEDEDEDFFD8D8D8FFC0C0C0FFA7A7A7FF8B8B
          8BFF898989FFAEAEAEFFA9A9A9FF979797FF0000000300000000000000000000
          0004A5A5A5FAA3A3A3FF868686FF9A9A9AFF898989FF929292FFABABABFFC6C6
          C6FFD8D8D8FFDFDFDFFFBFBFBFFFB8B8B8FFEDEDEDFFEEEEEEFFEFEFEFFFEFEF
          EFFFB6B6B6FFB6B6B6FFE2E2E2FFDFDFDFFFD5D5D5FFC2C2C2FFAAAAAAFF9292
          92FF898989FF8F8F8FFFA1A1A1FF9E9E9EFA0000000400000000000000000000
          0004979797F79B9B9BFF828282FF959595FF989898FFBABABAFFCCCCCCFFDCDC
          DCFFC5C5C5FFC6C6C6FFC7C7C7FFCACACAFFCCCCCCFFCCCCCCFFCCCCCCFFCCCC
          CCFFCACACAFFC7C7C7FFC6C6C6FFC6C6C6FFDEDEDEFFD0D0D0FFC4C4C4FFA3A3
          A3FF888888FF959595FF818181FF989898F70000000400000000000000000000
          00049B9B9BFF9D9D9DFF979797FFB5B5B5FFBFBFBFFFBABABAFFC1C1C1FFC6C6
          C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6
          C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC1C1C1FFBABABAFFC1C1
          C1FFB5B5B5FF969696FF959595FF949494FF0000000400000000000000000000
          0004A3A3A3FFA0A0A0FFB5B5B5FFB2B2B2FFBFBFBFFFC0C0C0FFC0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFBFBFBFFFBFBFBFFFBFBFBFFFBFBF
          BFFFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFBFBF
          BFFFB2B2B2FFB8B8B8FF9E9E9EFF969696FF0000000400000000000000000000
          0004A6A6A6FEB8B8B8FFBABABAFFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6
          C6FFC7C7C7FFC6C6C6FFC8C8C8FFC5C5C5FFC0C0C0FFC1C1C1FFC1C1C1FFC2C2
          C2FFC7C7C7FFC7C7C7FFC6C6C6FFC6C6C6FFC7C7C7FFC6C6C6FFC6C6C6FFC6C6
          C6FFC6C6C6FFBBBBBBFFB9B9B9FF9C9C9CFD0000000400000000000000000000
          0004A2A2A2F7BFBFBFFFCDCDCDFFCDCDCDFFCDCDCDFFCDCDCDFFCBCBCBFFC6C6
          C6FFCACACAFFD0D0D0FFD0D0D0FFD3D3D3FFD3D3D3FFD3D3D3FFD4D4D4FFD4D4
          D4FFD3D3D3FFD3D3D3FFD0D0D0FFCBCBCBFFC9C9C9FFCCCCCCFFCDCDCDFFCDCD
          CDFFCDCDCDFFCDCDCDFFBFBFBFFFA0A0A0F60000000400000000000000000000
          0004B1B1B1FAD3D3D3FFD3D3D3FFD3D3D3FFD3D3D3FFD5D5D5FFD7D7D7FFD3D3
          D3FFD4D4D4FFD6D6D6FFD7D7D7FFD9D9D9FFDADADAFFDADADAFFDADADAFFDADA
          DAFFDADADAFFD9D9D9FFD8D8D8FFD6D6D6FFD7D7D7FFDBDBDBFFD6D6D6FFD3D3
          D3FFD3D3D3FFD3D3D3FFD3D3D3FFB1B1B1FA0000000300000000000000000000
          0003BBBBBBF3D9D9D9FFD9D9D9FFD9D9D9FFDBDBDBFFDFDFDFFFE2E2E2FFDADA
          DAFFD9D9D9FFDBDBDBFFDDDDDDFFDEDEDEFFDDDDDDFFDCDCDCFFDEDEDEFFDADA
          DAFFDFDFDFFFDFDFDFFFDEDEDEFFDCDCDCFFDCDCDCFFE3E3E3FFE2E2E2FFDCDC
          DCFFD9D9D9FFD9D9D9FFD9D9D9FFBCBCBCF30000000300000000000000000000
          0002828282B4E0E0E0FFE0E0E0FFE0E0E0FFE3E3E3FFE0E0E0FFDADADAFFDCDC
          DCFFDEDEDEFFE0E0E0FFE1E1E1FFE2E2E2FFEEEEEEFFF3F3F3FFF3F3F3FFF2F2
          F2FFE3E3E3FFE3E3E3FFE2E2E2FFE0E0E0FFDEDEDEFFDCDCDCFFE3E3E3FFE5E5
          E5FFE0E0E0FFE0E0E0FFE0E0E0FF828282B30000000200000000000000000000
          00000E0E0E13C2C2C2E8E6E6E6FFE6E6E6FFE7E7E7FFEAEAEAFFDDDDDDFFDDDD
          DDFFDEDEDEFFE3E3E3FFE5E5E5FFE6E6E6FFEAEAEAFFEFEFEFFFF1F1F1FFEBEB
          EBFFE7E7E7FFE6E6E6FFE4E4E4FFE2E2E2FFDEDEDEFFDFDFDFFFE9E9E9FFE9E9
          E9FFE6E6E6FFE6E6E6FFC2C2C2E70E0E0E130000000000000000000000000000
          0000000000000B0B0B0F969696B8ECECECFFECECECFFEEEEEEFFF2F2F2FFF4F4
          F4FFE7E7E7FFE5E5E5FFE6E6E6FFE7E7E7FFE7E7E7FFE8E8E8FFE8E8E8FFE8E8
          E8FFE7E7E7FFE6E6E6FFE6E6E6FFE3E3E3FFF2F2F2FFF3F3F3FFEFEFEFFFECEC
          ECFFECECECFF969696B70B0B0B0F000000000000000000000000000000000000
          00000000000000000000000000015B5B5B6EE1E1E1F5F3F3F3FFF4F4F4FFF7F7
          F7FFF1F1F1FFE2E2E2FFE2E2E2FFE5E5E5FFE4E4E4FFE8E8E8FFEAEAEAFFE5E5
          E5FFE5E5E5FFE4E4E4FFE2E2E2FFEAEAEAFFF7F7F7FFF4F4F4FFF3F3F3FFE2E2
          E2F55B5B5B6E0000000100000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404064444444A969696AEE5E5
          E5F4FAFAFAFFFBFBFBFFFCFCFCFFFDFDFDFFFEFEFEFFEEEEEEFFEBEBEBFFF6F6
          F6FFFDFDFDFFFCFCFCFFFAFAFAFFF9F9F9FFE4E4E4F3969696AE4444444A0404
          0406000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010303
          03053B3B3B405D5D5D6478787887919191A7ABABABC4C7C7C7DDC7C7C7DDABAB
          ABC4919191A7787878875D5D5D643B3B3B400303030500000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000100000005000000090000000D0000001000000012000000140000
          0016000000190000001A0000001C0000001E0000001F00000020000000200000
          001F0000001D0000001C0000001A000000190000001700000016000000140000
          00100000000F0000000C00000006000000030000000100000000000000000000
          0000000000060000000B0000001000000014000000180000001A0000001C0000
          001E0000001F0000002000000020000000200000002100000021000000210000
          0021000000210000002000000020000000200000001E0000001E0000001C0000
          001B0000001800000015000000110000000B0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000011111129454545D81D1D1DF712151CFD484849FC59595AE71C1C1C540000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001D1C1D59454446E51819
          1AFB151A23FD333235F3575758DF131314300000000000000000000000003837
          389A292929FF182031FF1D4185FF204EA0FF1E478FFF18315EFF3D3E41FF3D3D
          3DC4000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000393839C53A3A3BFF1F3050FF1C41
          81FF204A99FF1F4A96FF1A325BFF131214FF3A393A9E00000000303031721F1F
          1FFF1B2A47FF1F478FFF3666BEFF4B79CFFF3465BEFF214EA0FF1A325EFF3030
          32FF2E2D2F8A0000000000000000000000000000000000000000000000000000
          00000000000000000000000000003030318C2E2D2FFF162745FF1B3E7FFF315D
          ACFF2456B4FF386BC8FF2657B3FF1C3462FF232323FF32323378313131EE2A2E
          39FF173367FF274F96FF2854A5FF2554AAFF2856A8FF29529EFF193A75FF1E28
          3BFF272627F80000000000000000000000000000000000000000000000000000
          0000000000000000000000000000242325F8252A34FF132B57FF234684FF254C
          95FF224FA2FF2957AEFF2C59ACFF1D4589FF1F283CFF302F31EF1C1C1DFF272F
          40FF132C59FF16346AFF244787FF1B3F82FF335593FF1C3D75FF32558FFF1422
          3CFF181719FF0101010200000000000000000000000000000000000000000000
          00000000000000000000020202031A191AFF1E2531FF10254BFF193261FF3252
          8BFF345694FF17348FFF1B3D7DFF2B4F90FF162746FF1B1A1BFF252526FC2D32
          3EFF0D1F3EFF193468FF18366EFF1D3D76FF254178FF2A487DFF162D59FF1720
          32FF1E1E1FFF363637FD3F3E40F9373738EA1615166300000000000000001717
          186D383739EA403E41F6474748F9202021FF252930FF122344FF203A68FF1732
          63FF254277FF2C497FFF304D82FF122A54FF1C2940FF252526FC403F41D1393A
          3DFF0D1E3CFF0E2245FF1F355CFF33486FFF465979FF576680FF37455FFF2021
          26FF3D3D3EFF2D2D2DFF6F6E6FFFA09FA2FF3A393BFF070708220D0D0D3E3D3C
          3EFF919092FFA5A4A7FF505051FF4F4E51FF333335FF0D172CFF0C1B37FF2234
          56FF354A6DFF495B7EFF5B6B87FF586783FF282C33FF434244D11A1A1A272D2C
          2EFF3B3D42FF232E41FF313E56FF455269FF5A6578FF686F7CFF424448FF1717
          17FF39393AFF272727FF787778FF6D6D6FFF373638FF1F1D1F93252325AE3C3B
          3DFF8C8B8CFF767677FF4B4B4CFF777679FF242425FF3B3B3EFF252C39FF2C37
          4BFF3F4C62FF535F74FF666E7FFF4F5258FF2D2C2DFF1B1B1B27000000008B8A
          8CB35A595BFF565657FF45474BFF4B4E53FF47484CFF302F31FF201F21FF1A19
          1AFF2F2F31FF252526FF3D3C3EFF393839FF363537FF201F21A0262527BC3837
          39FF3A3A3BFF3D3B3EFF3C3B3DFF59585AFF3D3C3EFF2E2D2FFF2D2D2EFF3E3F
          41FF414447FF434448FF353636FF5D5C5EFF888889AE00000000000000001B1B
          1B26717071FFC4C3C5FFA9A8A9FF9A9A9CFF7E7D7FFF575759FF191819FF1312
          13FF262627FF343435FF3C3B3DFF3B3A3CFF343334FF151516751B1A1B913433
          34FF3C3B3DFF3C3B3DFF3C3B3DFF3B3A3CFF323133FF3D3C3EFF858487FF9897
          99FF8E8D90FFB1B0B3FFCECDCFFF707072FE1212121600000000000000000000
          0000807F80B4858486FF777678FF5A595AFF4B4B4CFF303030FF1C1C1DFF2524
          26FF343435FF3C3B3DFF3A393BFF343335FF565454FF818082F18B8A8BF44D4B
          4CFF333233FF3B3A3CFF3C3B3DFF3C3B3DFF343335FF39383AFF8A898CFF9C9B
          9EFFAEADB0FFBFBEC0FFABAAACFF7372749B0000000000000000000000000000
          00002423242BADACAEFF838383FF5C5C5EFF414142FF373738FF323233FF3938
          3AFF3C3B3DFF383738FF373638FF474545FF6C696AFFAEADAFFFB0AFB2FF6968
          68FF3D3B3BFF363638FF383739FF3C3B3DFF3C3B3DFF3C3B3DFF434244FF5554
          56FF737273FF8D8D8FFFC3C2C4FA141414180000000000000000000000000000
          000000000000707071A5BEBDC0FFA09FA1FF717073FF353435FF3C3B3DFF3E3D
          3FFF373638FF474749FF39383AFF343435FF4E4D4EFC767577FF717173FE4C49
          4AFE312F31FF3C3B3DFF454446FF373638FF3E3D3FFF3C3B3DFF353436FF9090
          91FFB5B4B6FFBEBDBFFF65646589000000000000000000000000000000000000
          00000000000002020202777678A98C8C8EFF5E5E5FFF6F6F71FF4B4B4CFF3939
          3AFF68676AFF464547FF373638FF2E2D2EED3F3C3CF4434242FF4B4747FF6662
          62EE363536F3383739FF484749FF585759FF434243FF535353FF747375FF807F
          82FFB3B2B5FF6A6A6B9A00000000000000000000000000000000000000000000
          0000000000000000000003030304434243744D4C4ECA939294F48D8C8FFF2E2D
          2EFF464547FF444345FF2E2D2FD50808092A1D1C1C6D2E2C2CDF353333CA2524
          245B0909092F323133E6454446FF434244FF313031FF979699FF818083F35858
          5ABD424142690101010100000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000003838385E939295FF3C3C
          3DFF2B2B2CFF333334FF2B2B2B62000000000000000000000000000000000000
          0000000000003534358F343334FF2B2B2CFF403F41FF929194FF272727490000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000006B6B6CED4645
          47FF1F1E1FFF272627FE14141420000000000000000000000000000000000000
          00000000000021212238242325FF2C2C2CFF555456FF6B6A6CDB000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001B1B1C249C9B
          9DCD868588E11616177000000000000000000000000000000000000000000000
          000000000000000000001E1D1E818A898BDF919192B70D0D0D11000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B0C0C75232526A81D1D
          1DA01B1B1CA05254559F42434468000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000708087B161818FB464A4BFF7276
          79FF49494AFF414242FFAEB0B1FF656666870000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000809097C171919F8484C4DFF6A6F71FF888D
          8FFF7D8183FF3B3B3BFF383838FFD3D3D3FF3232323900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000208090983242727F7515556FF6E7274FF888C8EFF7C7F
          82FF535558FF606162FF353535FF4A4A4AFF3131314A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000009252E2F2382A4BF315265FF767B7FFF7B8082FF85898CFF85888BFF5254
          57FF5D5F61FFCECECFFFBBBBBCFF363737FF0C0C0C5200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020A0E0E1A66
          7F813ACBF7F942D3FDFF47C2EBFF6398B7FF8C959CFF818487FF626467FF6163
          65FFA0A1A2FFEFEFEFFFDADCDCFF767777FF1010106100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000106090A072F4649186D979B31B9EDF143D3
          FDFF50DDFEFF59E2FFFF50DCFBFF45C5ECFF5A97BDFF29333DFF46484BFFBDBE
          BFFFE8E9E9FFE4E5E5FFB7B8B8FF333536FF1819198800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000006
          0A0B02284448084F8086127ABCC31FA7F0F82EBCFAFF3ECCFCFF4FD9FEFF5DE3
          FFFF60E7FFFF59E8FFFF58E7FFFF53DEF9FF49C7ECFF62A1C8FF2D353FFF999A
          9CFFDBDDDDFFB3B5B5FF4D4F50FF111313BF0203030B00000000000000000000
          00000000000000000000000000000000000000010101002A494C0868ADB41499
          EEF722ADF8FF30BAFAFF3EC7FCFF4CD2FDFF59DBFEFF62E2FFFF60E6FFFF5CE9
          FFFF5EEBFFFF62EDFFFF62EDFFFF5EEBFFFF56DFF9FF50C9EEFF82B9DFFFECF2
          F7FFE6E7E7FF4C4E4EFF141515C2020202070000000000000000000000000000
          0000000000000000000000000000001725260676BCC316A6F8FF28B8FBFF39C5
          FDFF4ACFFEFF58D7FFFF66DEFFFF68E1FFFF63E3FFFF5FE6FFFF63EAFFFF67ED
          FFFF6AF1FFFF6CF3FFFF6BF2FFFF65EFFFFF5DEAFFFF51DDFAFF65CEF1FFA8D1
          F0FF868889FF1F2020C301010102000000000000000000000000000000000000
          00000000000000000000002B44460C9AEAF122B4FBFF33C3FEFF47CEFFFF5AD6
          FFFF6BDDFFFF69DFFFFF61DFFFFF62E2FFFF66E5FFFF6AE9FFFF6FEDFFFF73F2
          FFFF77F6FFFF78F8FFFF72F6FFFF69F1FFFF5FECFFFF56E6FFFF4CD8FBFF8FD6
          F1FF1F3646D70303031800000000000000000000000000000000000000000000
          00000000000000253A3C0D9EF0F627B9FCFF39C7FEFF4ED0FFFF65D9FFFF70DD
          FFFF63DCFFFF64DFFFFF69E2FFFF6DE5FFFF70E8FFFF74ECFFFF78F0FFFF7CF4
          FFFF81F9FFFF81FBFFFF75F7FFFF69F1FFFF5EEBFFFF58E6FFFF50DDFEFF67D9
          FDFF164050520000000000000000000000000000000000000000000000000000
          0000000C1313058EDFE524B5FBFF3AC7FEFF50D0FFFF6BD9FFFF6FDCFFFF64DB
          FFFF6ADFFFFF6FE2FFFF73E5FFFF76E7FFFF79EAFFFF7BEDFFFF7EF1FFFF81F4
          FFFF82F7FFFF7EF7FFFF73F4FFFF66EEFFFF5CE9FFFF58E3FFFF4BDAFEFF2A8F
          B0B1000000000000000000000000000000000000000000000000000000000000
          000000588F9418AAF9FF34C3FEFF4DCFFFFF69D8FFFF6EDBFFFF66DBFFFF6FDF
          FFFF75E2FFFF7AE5FFFF7DE7FFFF7EE9FFFF7FEBFFFF80EDFFFF81F0FFFF81F2
          FFFF7FF3FFFF79F2FFFF70F0FFFF63EBFFFF5AE6FFFF55DFFFFF41CDF5F60616
          1B1B00000000000000000000000000000000000000000000000000000000000D
          15150795EEF727B8FCFF44CBFFFF63D6FFFF6EDAFFFF64D9FFFF70DEFFFF7AE2
          FFFF80E5FFFF84E7FFFF85E9FFFF86EAFFFF85EBFFFF84EDFFFF83EEFFFF81EF
          FFFF7CEFFFFF75EEFFFF6CEBFFFF61E7FFFF5BE3FFFF4CDBFEFF257D97980000
          000000000000000000000000000000000000000000000000000000000000003E
          656811A3F8FF32C2FDFF54D1FFFF70DAFFFF5DD6FFFF6CDCFFFF7AE1FFFF84E5
          FFFF8AE7FFFF8DE9FFFF8DEAFFFF8CEBFFFF8AEBFFFF87ECFFFF84EDFFFF80ED
          FFFF7AECFFFF72EAFFFF68E8FFFF60E4FFFF5ADFFFFF42D3FEFF0D2F3A3A0000
          0000000000000000000000000000000000000000000000000000000000000066
          A3A81AAEFAFF3EC8FEFF66D6FFFF5DD5FFFF61D7FFFF72DEFFFF81E3FFFF8CE7
          FFFF94EAFFFF96EBFFFF96ECFFFF92ECFFFF8EEBFFFF8AEBFFFF85EBFFFF7FEA
          FFFF77E9FFFF6FE7FFFF65E4FFFF61E1FFFF54D9FEFF32B8E6E9010102020000
          0000000000000000000000000000000000000000000000000000000000000282
          CACF21B5FBFF48CCFFFF65D6FFFF52D2FFFF63D8FFFF75DEFFFF86E4FFFF94E9
          FFFF9DECFFFFA0EDFFFF9EEDFFFF98ECFFFF92EBFFFF8CEBFFFF85EAFFFF7EE8
          FFFF75E6FFFF6BE4FFFF63E1FFFF61DEFFFF4AD0FDFF207EA6A9000000000000
          000000000000000000000000000000000000000000000000000000000000038D
          D9DE24B7FCFF52CFFFFF53D1FFFF51D2FFFF62D8FFFF75DEFFFF89E5FFFF9BEB
          FFFFAAEFFFFFB0F1FFFFABF0FFFFA2EEFFFF98ECFFFF8EEAFFFF85E8FFFF7CE6
          FFFF72E4FFFF68E1FFFF63DFFFFF5ED9FEFF3EC6FCFF15516E71000000000000
          0000000000000000000000000000000000000000000000000000000000000385
          D0D624B5FBFF58D0FFFF3FCCFFFF4DD1FFFF60D7FFFF79DEFFFF92E7FFFFA3EC
          FFFFB1F0FFFFB9F3FFFFB3F2FFFFACEFFFFFA4EEFFFF98EBFFFF87E7FFFF79E4
          FFFF6FE1FFFF66DEFFFF65DDFFFF55D3FEFF31BAFAFF0F2B3A3B000000000000
          000000000000000000000000000000000000000000000000000000000000016F
          B1B721AEF9FF4FCCFEFF36C9FFFF47CFFFFF62D7FFFF75DDFFFF80E1FFFF8FE7
          FFFF9EECFFFFA8EFFFFFA6EFFFFF9FEDFFFF96EAFFFF90E8FFFF8CE7FFFF79E2
          FFFF6BDEFFFF67DDFFFF63DAFFFF48CBFDFF25A5E8EE0D121414000000000000
          000000000000000000000000000000000000000000000000000000000000004A
          7B7F18A0F7FF42C4FDFF2EC6FFFF40CCFFFF5ED5FFFF63D8FFFF79DFFFFF91E6
          FFFFA1EBFFFFABEEFFFFADEFFFFFAAEEFFFFA2ECFFFF8FE7FFFF83E4FFFF76E0
          FFFF69DCFFFF69DBFFFF59D4FFFF38C0FCFF1E7CB0B4080A0B0B000000000000
          000000000000000000000000000000000000000000000000000000000000001C
          2C2D0C90F4FF36B7FBFF24C0FEFF38C9FFFF6CD8FFFF97E4FFFFA0E7FFFFA1E9
          FFFFA8EBFFFFADEDFFFFAFEEFFFFAFEDFFFFAFEDFFFFB7EFFFFFB5EEFFFF99E7
          FFFF71DCFFFF62D7FFFF47CBFEFF27B3FAFF1A4D696C01010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000167B5BE2AA4F7FF1BB6FCFF6FD7FFFF8AE0FFFF97E3FFFFA9E9FFFFB5EC
          FFFFBAEEFFFFBDEFFFFFBFF0FFFFC0F0FFFFBCEFFFFFB1ECFFFFA8EAFFFFA8E9
          FFFFB0EBFFFF55CFFEFF31BBFCFF1C96DDE311191C1C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000001E3133148CEEFB34AEF8FF86D8FEFF94E1FFFF9CE5FFFFA2E7FFFFA9E9
          FFFFB0EBFFFFB4ECFFFFB6EDFFFFB4ECFFFFB2ECFFFFB2EBFFFFB4ECFFFFB2EB
          FFFFA7E6FFFF61CAFCFF21ABF9FF1C4F6C6F0203030300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000004272772294F3FF7DC8FAFF9ADEFEFFA7E6FFFFADE9FFFFB1EA
          FFFFB5ECFFFFB8ECFFFFBAEDFFFFBCEEFFFFBFEFFFFFC2EFFFFFBEEDFFFFB3E8
          FEFFA1DDFDFF38B3F9FF1D75A9AD090C0D0D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000014D868D3B9EF4FF89CAF9FFA7DEFDFFB6E8FEFFBEEC
          FFFFC2EEFFFFC5EFFFFFC9F0FFFFCBF1FFFFCBF1FFFFC5EEFFFFB9E7FEFFA5DB
          FCFF61BDF9FF1C7BB4B90D131616000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000003963673194E9F57EBFF7FFA4D6FAFFBAE3
          FDFFC6EAFEFFCCEDFEFFCDEDFEFFC9EBFEFFBFE5FDFFAFDCFBFF9AD0FAFF54B1
          F4FB1A618C91090D0E0E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000121D1E0D589299429DE9F37CBD
          F7FFA1D0F9FFABD6FAFFACD7FAFFA3D1F9FF84C2F8FF55ABF0F92371A9AF0E27
          3638010202020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000070B0B002B
          46490B4B767A1B608D921D618E93104F7B7F07325053050F1515000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000200010107165266962785A7F11A6E8CC00002021C0000001E0000
          0023000000280000002D00000033000000380000003D00000042000000430000
          00400000003B00000036000000310000002C00000027000000210000001D0000
          0018000000144642418E837D7AF8AFA9A6F9847E7ACB00000000000000000000
          000003151B27207590D132ADD4FF31BCEAFF2CABD8FF134A5E9A0000000E0000
          00110000001400000017000000180000001A0000001B0000001C0000001C0000
          001C0000001B0000001A000000180000001700000014000000110000000E0000
          000B4D494699B7B0AEFEBCBAB8CE7372717DC6C3C0E2847F7CB4000000000010
          1520097394F223C2F2FF39CDFCFF34C4F2FF2EB8E6FF27ABDAFB071E27460000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004441
          3E87B3AEAAFEAAA8A7BB00000000000000003737364DA09A97CB00000000001B
          233904B0E5FF14B4E5FF28C7F8FF36C4F2FF32C1EFFF2DB4E2FF2282A5E20104
          0508000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003E3A387BB1AC
          AAFEEAE8E5FF7573728600000000030303054F4A479D817C79AE000000000000
          0000006584AE08B1E6FF17BCF0FF2DCAFBFF34C6F4FF2FBFEDFF2AB2E0FF1650
          64AF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000037343370AEA9A4FDE3DF
          DCFFD0C8C5FFE2E0DDF95754509C54504BAF95908CCB17161522000000000000
          0000000A0D150094C1E40DB3E6FF1BC1F4FF33CBFAFF31C5F5FF2CBBEBFF27AE
          DBFD0A27315A0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000312E2D63A8A39FFCE4DEDDFFE9E6
          E4FFF0EDECFFD8D1CEFFECE9E8FFB3B0AEE92E2D2C4100000000000000000000
          00000000000000232E4C02ADE3FE12B6E8FF1FC4F7FF33C9F8FF2EC3F2FF2BB7
          E5FF2180A0EC02080A1200000000000000000000000000000000000000000000
          00000000000000000000000000002B282759A59F9BFCE4E0DDFFE8E4E4FFEBE8
          E6FFF4F3F3FFD9D3D0FFB1ACA9F8242322330000000000000000000000000000
          0000000000000000000000465C9707B2E7FF15BBEFFF26C7FAFF31C7F7FF2CC0
          F0FF28B5E2FF18586EBE00000000000000000000000000000000000000000000
          000000000000000000002523224FA19B96FCE3DEDEFFE8E4E2FFEAE7E5FFF4F2
          F2FFD8D2CFFFB0ABA8F51B1A1925000000000000000000000000000000000000
          0000000000000000000000030407007397D30CB2E6FF19BFF3FF2BC9FAFF2DC6
          F6FF29BDECFF25A9D5FF0B2F3A6C000000000000000000000000000000000000
          000000000000211E1D469D9693FBE2E0DCFFE7E3E1FFE9E6E4FFF3F1F0FFD7D3
          D0FFACA7A3EF1312121A00000000000000000000000000000000000000000000
          000000000000000000000000000000141B2F009ECFF610B5E8FF1EC3F6FF2FC9
          F9FF2BC4F5FF27B9E6FF1F7D9DF202080B130000000000000000000000000000
          00001B19183C98918EF9E1DDDCFFE6E2DFFFE8E5E3FFF2F1EFFFD9D3D1FFA8A3
          A1E90B0B0B0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000032427605AFE5FF13BBEFFF22C6
          F9FF2CC8F8FF28C1F2FF25B6E5FF13556AB10000000000000000000000001715
          1433948C8AF8E0DCDBFFE4E1DEFFE7E4E2FFF1EFEFFFD7D4D2FFA29D9BE00404
          0406000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000005874BD0AB0E5FF18BE
          F2FF27C8F9FF29C8F8FF25BFEFFF219EC6FD061C2341000000001311112C908A
          86F7DED9D9FFE5E0DEFFE6E2E0FFF1EFEEFFD9D6D3FF999692D4000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000A0E19007EA5E60EB4
          E6FF1CC1F5FF2BC8F9FF27C4F6FF24BAE9FF187290D90F0E0D278C8481F6DDD8
          D7FFE3E0DCFFE5E1DFFFF1F0EEFFD8D5D3FF908C8BC700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000001A233F039E
          CEF812B9EDFF20C4F7FF28C8F9FF23C1F3FF22B7E6FF406F7FFBD0CCC9FFE2DF
          DCFFE4E0DEFFF1EFEEFFD7D3D2FF85827FB70000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000032
          416F08B0E4FF16BDF1FF25C6F8FF24C7F9FF21BFF0FF2192B6FF9EA2A3FFE3DF
          DDFFF0EEEDFFD4D1CFFE787674A6000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000005169A30DB2E5FF1AC0F4FF29C7F8FF21C4F7FF20BBEBFF3A859EFFE2E1
          E0FFCFCCCAFE6C6A679400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000030407027497D110B8EBFF1FC3F6FF2AC9FAFF1EC0F3FF22ADD8FF928F
          8EFE615E5D840000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A090916787D7DED1BA5D1FF14BCF0FF23C5F8FF27A5CCFF8DB5C2FF4745
          4490000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000100F
          0E228B817EE9D1CCC8FEA3C0C8FF21B7E5FF51A5C0FFD7E1E9FFDEDEE0FF9898
          97C4000000020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020101033A3533739A92
          8DF0D2CDCBFEDBD7D3FFDFDAD7FFEEEBEAFF817F7DFDFAFAFBFFDBDBE0FFDADA
          DBFF1818187D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000101010123211F382825234846403D81837B76E1B7B0ACF3D6D0
          CDFDD8D4D1FFDED8D5FFEAE9E8FFC2BDBBF934333248938F93C8EFEFF1FFD6D6
          DBFFD0D0D0FA0A0A0A3500000000000000000000000000000000000000000000
          00000000000000000000000000000E0E0E2C0000000000000000000000000000
          0000554E4B8A958982F8998F89FF918580FFBAB3AFFBD2CBC8FFA49F9EEBAFA9
          A7EDDDD7D4FFEBE7E6FFC2BEBDFD32302F430000000008080825E0DCE0F4E1E1
          E5FFD2D2D5FFB3B3B2D90202020A000000000000000000000000000000000000
          00000000000011101022545454D2565656F81515153F00000000000000000000
          00004B464375B9B3AFF2C2BDBAFDC9C2BFFFAAA4A3F1696969FB8E8E8EFB5E5C
          5CC4E5E2DFFFEEEAEAFF8B8785C700000000000000000000000017171767F5F5
          F5FFD3D3D8FFCECED0FF37373799010101020000000000000000000000000000
          0000010101034E4E4EDE616060FF797878FF575757D900000001000000002925
          234383756DFCCEC6C3FFEDE9E7FFB9B3B1FD8A8989E2BBBBBBFFABABABFF6666
          66EBDDDCDCFBBDBAB9F41B1B1A28000000000000000000000000000000005F5C
          5FB4E7E7E8FFB4B4B5D5C0BFC0DC7D7D7DE22F2F2F6700000001000000000000
          00003E3E3EB7585757FF6F6E6EFFAAAAAAFFD7D7D7FF737373AD262322389F96
          8EFBB5ACA5FFE2DDD9FFE6E1DEFFCDC5C2FF8E8D8CCFADADADFFC8C8C8FEA4A4
          A3ECF2F1F0FF7B7978B000000000000000000000000000000000000000000505
          0517848384CAF8F8F8F9F7F7F8FFD7D7D9FA858484FF696969E2656565DF5656
          56ED3D3D3DFE767676FFD1D1D1FFE7E7E7FFC0BFBFFF616060AB918A84D0D8D3
          CFFFF3F1EFFFD8D2CEFFE6E0DDFFDAD1CCFFB8B2AFF79D9B9BE0C6C4C4F1F4F3
          F2FFC7C5C5EB1010101800000000000000000000000000000000000000000000
          00005D5C5CB0EFEFEFFFFBFBFCFFF1F1F3FFDCDCDEFFABAAABFF989797FF908F
          8FFFA1A1A1FFD4D4D4FFC6C5C5FD7C7C7CD43232326500000000C1BBB6F6FBFA
          FAFFF2F0EFFFC7BBB7FBD5CECBFED7CEC9FFD0C6BFFFECE9E7FFEEEBEAFFF5F4
          F4FF7B7978970000000000000000000000000000000000000000000000000000
          000043434370AFAEAFFFFDFDFDFFF7F7F8FFECECEEFFE5E5E7FFDBDBDDFFD5D5
          D5FFCBCBCBFF767676E50A0A0A17000000000000000000000000DBD8D5F7E8E6
          E4F6B0AAA8C9504C4A632C2A2938CAC1BEF4D9D0CBFFE6E1DFFFF4F3F2FFE3E2
          E1F61C1B1B250000000000000000000000000000000000000000000000000000
          0000737171BF9C9B9BFFFFFFFFFFFAFAFAFFF4F4F5FFECEBEDFFE5E5E8FFD7D6
          D9FF908F8FFF2F2F2F68000000000000000000000000000000001A19191E0000
          00000000000000000000423E3C64B8B0ABFBE2DCD8FFE6E1DDFFF6F6F6FF9290
          8EA933333389373737A826262664070707190F0F0F241F1F1F3A515151978480
          81EE7F7D7DFF878787FFFDFDFDFFFBFBFCFFF6F6F8FFEEEEF1FFD0CFD2FFA2A2
          A4FC656567AC0000000000000000000000000000000000000000000000000000
          00004D4B4968A8A39DEC958982FFD1CBC8FFEBE7E5FFECE9E6FFD8D6D4FE3A38
          37483736378E454545FF4A494AFF4C4B4CFF585657FF686667FF797778FF8B89
          89FF787577FFD5D3D5FFFCFBFCFFFCFCFCFFE5E5E6FFB5B5B7FE6B6B6CC01C1C
          1D37000000000000000000000000000000000000000000000000000000005C59
          567CB4ADA7FFCEC9C5FFF1EFEDFFF6F4F3FFF5F3F2FFD7D5D3FD5757576B0000
          000003030306373737775B595BDA626062FE656365FF6F6E6FFF868284FFB0AE
          B0FFDCDCDCFFE8E7E8FFDFDEDFFFC4C4C4FA818181C826262751000000000000
          0000000000000000000000000000000000000000000000000000000000000D0C
          0C0F9E9C9CAADAD8D7ECE8E5E4FEDCD8D7F8B4B0AEDB44424154000000000000
          00000000000000000000000000001D1D1D31605E60866E6D6E9E777477AC8482
          84B9848284B95D5C5D8D3131315A0707071A0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002050300120B0601290E080139120A0148150C0257190E0265190E0266150C
          0259120A014A0E08013B0B06012B050300130000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010000050A060125140B
          02521B0F03722E1802984A2602C45B2E01E0633201ED6C3600F96C3600F96432
          00EE5C2E01E14B2602C83019029E1D0F0379150B02580A060129010000060000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010000040A060127170D025E1D10037A4121
          01B9693400F48B4700FFB96100FFAC5A00FFB96200FFC56900FFC76A00FFBC63
          00FFAE5C00FFBA6100FF8B4800FF6A3400F5432202C11E110385180E02670A06
          012B010000040000000000000000000000000000000000000000000000000000
          00000000000000000000010000050D0701301A0E0268371C02A5673300F19F52
          00FFAC5A00FFD36F00FFD27000FFD87400FFDD7700FFE17A00FFE37B00FFE17A
          00FFDD7700FFD87400FFD77300FFB15D00FFA05300FF683300F3391E02B01A0F
          02740D0701360100000600000000000000000000000000000000000000000000
          000000000000010000060D070132190E0264462301BB824200FF9F5200FFC769
          00FFC86A00FFCE6E00FFD47100FFD97500FFDF7900FFE57C00FFE97F00FFE57C
          00FFDF7900FFDA7500FFD47100FFCE6E00FFCC6C00FFA25500FF834200FF4925
          02C51A0F02710D07013A01010007000000000000000000000000000000000000
          0000000000020704001C170C0256522901C8904900FFC16400FFBC6200FFC266
          00FFC86A00FFCE6E00FFD47100FFD57608FFC28A48FFC6B49EFFC6B5A1FFC58E
          4DFFD7790BFFDA7500FFD47100FFCE6E00FFC86A00FFC26600FFC46600FF914A
          00FF542A01D1190D026208050021000000030000000000000000000000000000
          0000010000050E080133442201A98D4700FFBA6000FFB55E00FFBB6200FFC166
          00FFC76900FFCC6D00FFD27000FFC79255FFEDEDEDFFEEEEEEFFEEEEEEFFEEEE
          EEFFCC9B63FFD87400FFD27100FFCD6D00FFC76900FFC16600FFBB6200FFBF63
          00FF8E4800FF472401B21009013C010000060000000000000000000000000000
          0000010000052E180174804100FEB35C00FFAE5A00FFB45D00FFBA6100FFBF65
          00FFC56800FFCA6C00FFD06F00FFD0BAA0FFF7F7F7FFF8F8F8FFF8F8F8FFF8F8
          F8FFD6C5B1FFD47200FFD06F00FFCA6C00FFC56800FFBF6500FFBA6100FFB45E
          00FFB75F00FF824000FF3019007B010000070000000000000000000000000000
          00000402000C653200E5AA5700FFA75500FFAD5900FFB25C00FFB86000FFBD63
          00FFC26600FFC76A00FFCC6D00FFD2BDA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD7C5B2FFCF6F00FFCC6D00FFC76A00FFC26600FFBD6300FFB86000FFB25C
          00FFAD5900FFAE5900FF663300E80502000E0000000000000000000000000000
          0000331900758E4700FFA25300FFA55400FFAA5700FFB05B00FFB55E00FFBA61
          00FFBF6400FFC46700FFC86A00FFC18C50FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC5955EFFCB6C00FFC86A00FFC46700FFBF6400FFBA6100FFB55E00FFB05B
          00FFAA5700FFA95600FF914A00FF361B007B0000000000000000000000000100
          0002673300E89F5000FF9D4F00FFA25200FFA85600FFAD5900FFB25C00FFB75F
          00FFBB6200FFBF6500FFC36700FFC06805FFC19058FFD7C9B9FFD9CCBEFFC394
          5EFFC26B08FFC66900FFC36700FFBF6500FFBB6200FFB75F00FFB25C00FFAD59
          00FFA85600FFA35200FFA65400FF693400ED0201000400000000000000001A0D
          003B7E3F00FF9C4D00FF9A4D00FFA05100FFA55400FFA95700FFAE5A00FFB35D
          00FFB75F00FFBB6200FFBE6400FFC16600FFC36700FFC26700FFC36700FFC468
          00FFC36700FFC16600FFBE6400FFBB6200FFB75F00FFB35D00FFAE5A00FFAA57
          00FFA55400FFA05100FF9F5000FF834000FF1E0F004300000000000000003D1E
          008A8C4600FF984C00FF984C00FF9C4F00FFA15200FFA65500FFAA5700FFAE5A
          00FFB25C00FFB65F00FFB96100FFBC6200FFBD6300FFBF6400FFBF6400FFBF64
          00FFBD6300FFBC6200FFB96100FFB65F00FFB35D00FFAF5A00FFAA5700FFA655
          00FFA15200FF9C4F00FF984C00FF934900FF412000920000000000000000542A
          00BE904901FF994F03FF9A5004FF9C5006FFA15407FFA65709FFAA5D0AFFAF5F
          0CFFB4630EFFBA6A14FFBC6D15FFBE6E14FFC06D12FFC26E11FFC26E11FFC16D
          0FFFBE6A0CFFBA6507FFB55E01FFB15C00FFAE5A00FFAA5700FFA65500FFA252
          00FF9E4F00FF994C00FF984C00FF984B00FF572B00C40000000000000000602F
          00D89B5611FFA25D16FFA25C15FFA15B15FFA35B14FFA65D13FFAA5F13FFAC60
          12FFB06212FFB26311FFB46510FFB26410FFB18A61FFCBC1B7FFCBC0B5FFBA92
          69FFB6650FFFB96912FFB96C16FFBC711EFFB96E1DFFB56C1CFFA85B0BFF9E4F
          00FF9A4D00FF984C00FF984C00FF974C00FF623100DC00000000000000006B35
          00F2A05E18FFA6611DFFA5611CFFA5611CFFA5601BFFA5621BFFA9631AFFAC63
          19FFAE6519FFB06618FFB26617FFB08250FFF4F4F4FFF4F4F4FFF5F5F5FFF4F4
          F4FFAF7D48FFB26613FFB06313FFAD6112FFAB5F11FFA85D11FFAD661CFFAE6A
          23FFAB6720FFA9641CFF9C5208FF9A4C00FF703700F400000000000000006B35
          00F2A3631FFFAA6723FFA96622FFA96522FFA96621FFA86520FFA86521FFAB67
          20FFAC681FFFAF691EFFB0691EFFC7B39EFFFDFDFDFFFEFEFEFFFEFEFEFFFEFE
          FEFFCBB49CFFB0681BFFAE661AFFAC6419FFA96219FFA75F18FFA35D17FFA35D
          16FFA35D16FFA25D16FFAB6824FFAE6E2BFF723900F40000000000000000602F
          00D8A26320FFAE6C28FFAD6B27FFAD6B27FFAD6B26FFAC6B26FFAC6A26FFAB6A
          26FFAC6A25FFAE6B25FFAB6824FFE2DFDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDCD7D2FFAE6820FFAD6820FFAB6720FFA8661FFFA7641FFFA7631EFFA762
          1EFFA6621DFFA6611DFFA5611CFFAA6C29FF683500DC0000000000000000552A
          00BEA26120FFB1702CFFB1712CFFB1702BFFB1702CFFB06F2BFFB06E2BFFAF6F
          2AFFAF6E2AFFAF6D29FFA96A28FFE5E4E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDBD8D4FFAD6C26FFAC6A26FFAB6A26FFAB6925FFAB6824FFAB6825FFAB68
          24FFAA6823FFAA6723FFA96622FFAC6D2CFF5E2F00C400000000000000003E1F
          008A985819FFB5762FFFB5752FFFB5742FFFB4742EFFB4732EFFB3732EFFB373
          2EFFB3732EFFB3722EFFAC6E2CFFE5E4E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDBD8D4FFB06F2BFFB06E2BFFAF6F2AFFAF6E2AFFAF6D29FFAE6D29FFAE6D
          28FFAE6C28FFAE6C28FFAD6B27FFA86B2AFF4623009200000000000000001A0D
          003B854709FFBA7B35FFB97932FFB87832FFB87731FFB87731FFB87731FFB777
          31FFB77630FFB77631FFB0722EFFE5E4E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDBD8D4FFB4732EFFB3732EFFB3722EFFB3732EFFB3722EFFB2722DFFB271
          2DFFB1702DFFB1702CFFB67933FF965817FF2010004300000000000000000100
          00026F3700E8B4742EFFBC7C34FFBC7B34FFBC7B34FFBC7A34FFBC7B34FFBB7A
          33FFBB7A33FFBA7B33FFB47631FFE5E4E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDBD8D5FFB87731FFB87731FFB77731FFB77630FFB67630FFB67630FFB675
          30FFB57530FFB5762FFFBC803DFF793D00ED0201000400000000000000000000
          0000371B0074985917FFC18038FFC07E35FFC07F35FFBF7F35FFBF7E35FFBF7F
          35FFBF7E35FFBE7E35FFB87932FFE5E4E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDCD8D5FFBC7A34FFBC7B34FFBB7A33FFBB7A33FFBA7B33FFBA7A33FFBA79
          33FFB97A32FFC0843FFFA86A27FF3E1F007A0000000000000000000000000000
          000004020008723A03E1BD7F39FFC48336FFC48236FFC38235FFC38135FFC380
          35FFC28135FFC28035FFBD7D35FFE2DFDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDBD6D3FFBF7F35FFBF7E35FFBF7E35FFBF7E35FFBE7E35FFBE7D35FFBE7D
          34FFBE7F37FFCB9250FF7C4408E4050200090000000000000000000000000000
          00000000000029150252975A1EFECA8F48FFC8873AFFC78639FFC78538FFC685
          37FFC68437FFC68336FFC68335FFBFAF9CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC3B19CFFC38236FFC38135FFC38035FFC28135FFC28035FFC27F35FFC280
          36FFD09752FFA66929FF2D180453000000000000000000000000000000000000
          0000000000000000000047280984AE7438FFD19851FFCB8D43FFCB8C42FFCA8C
          41FFCA8B40FFCA8A3FFFC98A3FFFB89367FFFBFBFBFFFFFFFFFFFFFFFFFFF9F9
          F9FFBA9262FFC78639FFC78538FFC68537FFC68437FFC68336FFC68335FFD195
          4BFFB77E40FF502F0D8A00000000000000000000000000000000000000000000
          0000000000000000000001000001613C15A9B78248FFD69F5DFFCF944DFFCE93
          4CFFCE934CFFCE924BFFCD9149FFC98E47FFAD977CFFBCB6AFFFBAB5ADFFB299
          7BFFC98B43FFCB8C43FFCB8C42FFCA8C41FFCA8B40FFCB8D43FFDAA260FFC08C
          50FF6E451BB20100000100000000000000000000000000000000000000000000
          0000000000000000000000000000010000015234158AB4824CFED8A565FFD7A1
          5FFFD29A57FFD19956FFD19955FFD19954FFD19753FFD09752FFD09651FFD095
          50FFCF9550FFCF944EFFCF944DFFCE934CFFD79F5AFFE0AD6BFFBE9056FF5E3D
          1B94010000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000037230E5B9E7242E8CF9F
          68FFDDAB6CFFDCAA6BFFD6A061FFD5A061FFD4A060FFD49F5FFFD49E5EFFD49D
          5DFFD39C5BFFD49C5BFFDAA663FFDEAA69FFD4A56DFFA87C4EEC3E2812620000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050301085439
          1D81AD8458EFD0A778FFDCAF7BFFDFB179FFE1B379FFE3B479FFE3B477FFE1B2
          77FFE0B077FFDDB17BFFD5AD7EFFB58F64F15B3F228706030109000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010000012B1D0E436F50319F9D7A54D3B79570E9CBAC8BF9CCAE8DF9BA99
          75EAA3805CD5755636A330211148010100020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000D0000
          00300000002E0000000500000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000008000000310000002D0000
          0008000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000C083045221889C0120D
          4B9B000000660000004900000005000000000000000000000000000000000000
          000000000000000000000000000007051B28201782BE181163B0010003630000
          003C000000090000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E0A3A513123C9FA3527D0FF3325
          CFFF191261AE000000660000004A000000050000000000000000000000000000
          0000000000000000000007051D2B2D20B8EC3528D1FF3325CFFF22188BCD0100
          03630000003C0000000900000000000000000000000000000000000000000000
          00000000000000000000000000000E0A3A513123C9FA3629D2FF423ADFFF443B
          DAFF4639D3FF191362AF00000066000000490000000500000000000000000000
          00000000000007051E2C382CBCED4438D5FF473FE3FF3E34DAFF3325CFFF2218
          8BCD010003630000003C00000009000000000000000000000000000000000000
          000000000000000000000E0A39513122C6FA3426CFFF3F34DBFF433AE0FF473F
          E4FF5349DDFF594DD5FF1A1361AF000000660000004900000005000000000000
          000008051F2D4235BDEF5A50D8FF4D45E7FF4942E6FF453DE2FF3C30D7FF3324
          CCFF221889CD010003630000003C000000090000000000000000000000000000
          0000000000000E0A39513424C3FA3324CBFF3B2FD6FF3F34DBFF4339DFFF463E
          E3FF4A43E7FF625AE1FF6C61D8FF1C1561B00000006600000049000000050805
          1F2E4B3FBFF07166DDFF514CE9FF4C45E9FF4841E5FF453CE1FF4137DEFF392D
          D3FF3323C9FF221786CD010003630000003C0000000900000000000000000000
          00000E0A38514436C6FA4A3DCFFF4032D3FF3A2ED6FF3E33DAFF4138DEFF453D
          E2FF4941E6FF4C46E9FF716AE5FF7F74DBFF1D1561B00000006608051E675449
          C1F1867EE2FF5550ECFF4D47EAFF4A43E7FF473FE4FF433BE0FF4036DDFF3D32
          D9FF3729D0FF3322C6FF221785CD010003630000003C00000008000000000E09
          38494B3EC6FA584CD1FF5348D5FF4F44D7FF493ED8FF3E33D9FF4036DDFF433B
          E0FF473FE4FF4A43E7FF4D47EAFF7F79E8FF8F86DEFF271E77C25F53C3F49B93
          E6FF5853EBFF4E48EBFF4B45E8FF4841E5FF453DE2FF4239DFFF3F34DBFF3B30
          D8FF382CD4FF3325CBFF3321C3FF221783CD010003630000002D040310155042
          C5F7665BD3FF6156D9FF5B50D8FF574BD8FF5449D9FF5146DBFF443ADCFF4138
          DEFF443CE1FF4740E4FF4A43E7FF4C46E9FF837DEBFFB4B0ECFF9E98EAFF5953
          EAFF4D47EAFF4B44E8FF4841E5FF463EE3FF433AE0FF4036DDFF3D32D9FF3A2E
          D6FF362AD3FF3325CFFF3324CAFF3321C1FF18105DB0000000310A0625316355
          CEFF6F63D7FF695FDBFF6359DAFF5D52D8FF594ED9FF574DDBFF544ADCFF4B41
          DDFF4238DEFF443CE1FF473FE4FF4941E5FF4A43E7FF4B44E8FF4B44E8FF4A44
          E7FF4942E6FF4840E5FF463DE2FF433AE0FF4137DDFF3E33DAFF3B2FD7FF382B
          D4FF3527D1FF3325CFFF3224CCFF3221C0FF201478BE0000000800000000261C
          74996B5ECFFF7065D7FF6B60DCFF655ADAFF5F54D9FF5C52DBFF594FDCFF564D
          DEFF5148DEFF433ADEFF433AE0FF453DE2FF463EE3FF473FE4FF473FE4FF473F
          E3FF463DE2FF443CE1FF4239DFFF4037DDFF3E33DAFF3B30D8FF392CD5FF3629
          D2FF3325CFFF3224CCFF3220BFFF2D1CA8EC0704182800000000000000000000
          0101271C749C6D60CEFF7368D7FF6D63DCFF675DDBFF6258DAFF5F55DBFF5B52
          DDFF594FDDFF534ADEFF4239DCFF4138DEFF4239DFFF433AE0FF433AE0FF433A
          E0FF4239DFFF4137DDFF3F35DCFF3D33DAFF3B30D8FF392DD5FF3629D2FF3326
          CFFF3325CCFF3220BCFF2D1CA6EC07041A2B0000000000000000000000000000
          000000000101271C749D7163CDFF7A70D9FF6F65DDFF6A5FDBFF655ADBFF6157
          DCFF5E54DCFF5A50DDFF534ADDFF4137DAFF3F34DBFF3F35DCFF3F35DCFF3F35
          DCFF3E34DBFF3D33DAFF3C31D8FF3A2ED6FF382CD4FF3629D2FF3426D0FF3527
          CCFF3623BBFF2E1BA5ED07041B2C000000000000000000000000000000000000
          00000000000000000101271C739E7C6FCFFF8177DBFF7268DDFF6C62DCFF675D
          DBFF6258DCFF5F55DBFF5B51DCFF554ADBFF3F34D7FF3B30D8FF3C30D8FF3B30
          D8FF3B2FD7FF3A2ED6FF382CD5FF372AD3FF3528D1FF3325CFFF3629CDFF4332
          BEFF2E1BA3EE08051B2D00000000000000000000000000000000000000000000
          0000000000000000000000000101291C74A18579D1FF877DDDFF746BDEFF6F65
          DDFF695FDBFF6459DBFF5F55DAFF5B50DAFF544AD9FF3D30D5FF382BD4FF382B
          D4FF372AD3FF3629D2FF3528D1FF3426D0FF3325CFFF382ACDFF503EC1FF3422
          A4EF08051B2E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000001012A1E73A28F83D3FF8077DEFF776D
          DFFF7167DDFF6B61DCFF665BDBFF5F54D9FF5A4FD8FF5348D7FF3A2CD1FF3426
          D0FF3326CFFF3325CFFF3325CFFF3325CFFF3325CFFF5243C4FF3827A4F20804
          1B67000000050000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000008041A2D796AC9FF8D84DEFF7E75
          E0FF786FDFFF7369DEFF6D63DCFF675DDBFF6257DAFF5C51D8FF5448D6FF3729
          CFFF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF5241BEFF1F1266C00000
          0066000000490000000500000000000000000000000000000000000000000000
          00000000000000000000000000000704182B5847ACF09C92D8FF8C84E3FF867D
          E2FF8077E1FF7B71DFFF756BDEFF6F65DDFF6A5FDBFF645ADAFF5F54D9FF5246
          D6FF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF3F2EC2FF3B25AEFF180D
          50AE000000660000004900000005000000000000000000000000000000000000
          000000000000000000000704182B5B4AACF09D93D8FF9992E5FF938CE5FF8E86
          E4FF8880E3FF837AE1FF7D74E0FF776EDFFF7268DDFF6C62DCFF675CDBFF6156
          D9FF4B3FD4FF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF3320BAFF331C
          A9FF180D4FAE0000006600000049000000050000000000000000000000000000
          0000000000000704172B6050AEF1ABA2DCFFA7A0E8FFA19AE8FF9B94E7FF968F
          E6FF9089E5FF8B83E3FF857DE2FF7F77E1FF7A71DFFF746BDEFF6F65DDFF695F
          DBFF6359DAFF4336D2FF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF331F
          B8FF331BA6FF180C4EAE000000660000004A0000000500000000000000000000
          00000704172B6A5AB1F2BDB5E3FFB4AFEBFFAFA9ECFFA9A3EAFFA39DE9FF9E97
          E8FF9891E6FF938BE5FF8D85E4FF877FE2FF8279E1FF7C73E0FF776DDFFF7167
          DDFF6B61DCFF645ADAFF3B2DD0FF3325CFFF3325CFFF3325CFFF3325CFFF3325
          CFFF3422B8FF361EA5FF180C4DAE000000660000004900000005000000000704
          17247667B5F0CEC9EAFFC3BEEFFFBCB8EFFFB7B2EEFFB1ACECFFABA6EBFFA6A0
          EAFFA09AE8FF9B94E7FF958EE6FF8F88E4FF8981E3FF837BE2FF7E75E0FF7970
          DFFF736ADEFF6E64DCFF6055D9FF3527CFFF3325CFFF3325CFFF3325CFFF3325
          CFFF3325CFFF3F2DBCFF442DA9FF190C4CAE000000660000002E0201060A7B6D
          B6EDDDDAF0FFD0CCF3FFC9C5F2FFC3BFF1FFBEB9EFFFB9B4EEFFB3AEEDFFAEA8
          EBFFA7A1EAFFA29BE9FF9C95E7FF978FE6FF9189E5FF8B83E3FF867DE2FF8077
          E1FF7B71DFFF756BDEFF6F65DDFF5448D6FF3325CFFF3325CFFF3325CFFF3325
          CFFF3325CFFF3325CFFF4938BFFF533EAEFF130A3A9B000000300C06253CCAC4
          E6FFE0DEF5FFD7D4F5FFD1CEF4FFCBC8F2FFC6C2F1FFC0BCF0FFBBB6EFFFB5B0
          EDFFAFAAECFFAAA4EBFFA49EE9FF9F98E8FF9A93E6FF9F97E2FF938BE2FF8880
          E3FF837AE1FF7D74E0FF776EDFFF7167DDFF3D30D1FF3325CFFF3325CFFF3325
          CFFF3325CFFF3325CFFF3527CEFF6857BDFF2F1F72C40000000D010002036557
          A1D7EAE8F5FFE2E0F7FFD9D6F6FFD3D0F4FFCECAF3FFC8C4F2FFC3BEF0FFBDB8
          EFFFB7B2EEFFB2ACECFFACA6EBFFABA5E9FFC3BCE5FF9A8DCFFFB3AADBFFA39B
          E2FF8B83E3FF857DE2FF7F77E1FF7A71DFFF6257DAFF3325CFFF3325CFFF3325
          CFFF3325CFFF3628CEFF6E60C8FF604EB1FB0C06254500000000000000000301
          090E6A5CA4D9EEECF7FFE5E3F8FFDBD9F6FFD6D3F5FFD0CDF4FFCBC7F2FFC5C1
          F1FFBFBBF0FFBAB5EEFFB8B4EDFFD2CDECFF7C6EB7F30804182D352771A7BCB4
          DFFFA9A2E5FF8D85E4FF877FE2FF8279E1FF7C73E0FF4639D3FF3325CFFF3325
          CFFF3729CFFF7D71CEFF6D5CB7FB0E072C510000000000000000000000000000
          00000301090E6D5FA6D9F2F0F9FFE8E6F8FFDEDBF7FFD8D5F5FFD3CFF4FFCDC9
          F3FFC7C4F2FFC6C2F0FFE0DCF2FF877BBEF50704162B00000000000001013629
          71A7C7C0E5FFAFA9E8FF8F88E4FF8A82E3FF847CE2FF7167DDFF3325CFFF372A
          CFFF8C82D6FF7969BDFC0F082D52000000000000000000000000000000000000
          0000000000000301080D7265A8DAF5F4FAFFEAE9F9FFE0DEF7FFDBD8F6FFD5D2
          F5FFD3D0F4FFEAE8F6FF9186C3F50704162B0000000000000000000000000000
          0101392C73A6D0CBE9FFB5AFEAFF928AE5FF8B83E3FF867DE2FF554AD6FF9B92
          DCFF8476C3FC0F082D5200000000000000000000000000000000000000000000
          000000000000000000000301080D7568AADAF8F7FBFFEDECFAFFE3E1F8FFE0DE
          F7FFF3F1FAFF9B91C9F60704162B000000000000000000000000000000000000
          0000000001013C2E74A6DAD6EDFFBAB5ECFF938CE5FF918AE5FFCAC5EEFF9083
          C8FC0F082E520000000000000000000000000000000000000000000000000000
          00000000000000000000000000000301080D796DADDBFAFAFDFFF1F0FBFFF9F8
          FDFFA59BCDF60704162B00000000000000000000000000000000000000000000
          000000000000000001013F3276A7E4E2F2FFC2BDEFFFDAD7F4FFB8B0DCFD1009
          2F53000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000201070C7A6EADDAF1F0F8FFA69D
          CEF3070416240000000000000000000000000000000000000000000000000000
          0000000000000000000000000101423576A6DDD8EEFFB7AEDBFC10092F4B0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000001010C06243A0201
          0609000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000A051F3104020D15000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000D00000027000000260000000B0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000204001A1427008C1A3101AD050A006A000000370000
          000D000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000004070022496926E2C0DBC4FFC8E6D4FF678646F60911007A0000
          00370000000D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000004070021476724E1B7E0C7FF4FC791FF4ECC96FFAEE8CFFF678647F60911
          007A000000380000000D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000407
          002041631EDFA6D1B0FF4AC086FF4AC58BFF4DC992FF50CE99FFA0DDBFFF6181
          3FF50911007A000000380000000D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000307001F3B5E
          17DD95C198FF45B87AFF45BD80FF48C287FF4BC68EFF4ECB95FF51D09CFF93D5
          B1FF597B37F50911007A000000390000000D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000307001F385B14DC85B4
          81FF40B16EFF41B674FF44BA7BFF46BF82FF49C48AFF4CC891FF4FCD98FF52D2
          9FFF86CCA1FF53762FF40911007A000000390000000D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000003070020345810DB74A56BFF3BA8
          62FF3CAE69FF3FB370FF42B777FF45BC7EFF48C185FF4AC68DFF4DCA94FF50CF
          9BFF53D4A2FF7AC394FF4C7026F50911007A000000390000000D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000307002034570FDB699B5BFF37A257FF38A6
          5DFF3AAB65FF3DB06CFF40B573FF43B97AFF46BE81FF49C388FF4CC890FF4FCC
          97FF51D19EFF54D6A5FF73BE8CFF496D21F40911007A000000390000000D0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000307002032550DDB609450FF329A4BFF339F52FF36A4
          59FF39A860FF3CAD68FF3EB26FFF41B776FF44BB7DFF47C084FF4AC58BFF4DCA
          93FF50CE9AFF53D3A1FF55D8A8FF6EBA86FF466C1EF50911007A000000390000
          000D000000000000000000000000000000000000000000000000000000000000
          0000000000000307002031550CDB5A8D45FF2E9340FF2E9747FF319C4EFF34A1
          55FF37A55CFF3AAA63FF3DAF6AFF40B472FF43B979FF45BD80FF48C287FF4BC7
          8EFF4ECC95FF51D09DFF54D5A4FF57DAABFF6AB782FF426A1AF50911007B0000
          00390000000D0000000000000000000000000000000000000000000000000000
          00000307002030540ADB558A40FF298A35FF2A903BFF2D9543FF30994AFF3FA3
          5BFF4EAE6CFF5CB77CFF69BF8AFF6CC390FF68C591FF65C693FF5CC691FF4BC4
          8BFF4CC991FF4FCE98FF52D2A0FF55D7A7FF58DCAEFF68B67FFF406817F50911
          007B000000390000000D00000000000000000000000000000000000000000307
          001632560EDA5E914AFF288932FF268931FF288D37FF3B9A4DFF62B074FF77BD
          89FF79C08EFF7CC493FF7DC698FF7FC99CFF82CDA1FF83D0A7FF86D3ACFF85D5
          AEFF72D2A6FF58CE9BFF50CF9BFF53D4A2FF56D9AAFF59DEB1FF6CB985FF426A
          1BF50912007B000000390000000D000000000000000000000000000000003354
          10CA679855FF288933FF268931FF308E3AFF64AB6DFF7CBA87FF7FBE8CFF81C0
          90FF84C496FF85C79AFF88CA9FFF8ACCA3FF8CD0A7FF8DD3ACFF90D6B1FF92D8
          B5FF94DBBAFF95DEBDFF77D8AFFF54D19FFF54D6A5FF57DBADFF5AE0B4FF73BF
          8EFF466D1FF50912007B000000390000000D00000000000000000A1400346B90
          4CFF298A33FF268931FF3B9445FF7EB985FF87BE8DFF88BF90FF8BC395FF8CC5
          99FF8EC89DFF90CBA1FF94CEA6FF9DD1ACFFA1D3B1FF9AD5B2FF9AD8B6FF9CDB
          BAFF9EDDBEFF9FE0C2FFA2E3C6FF97E2C3FF65D7ABFF56D8A8FF59DDB0FF5BE2
          B7FF7BC498FF4A7024F60912007B000000390000000D00000000040700136385
          3FF9449448FF499C52FF8FC295FF93C499FF94C59AFF94C59AFF96C89EFF98CA
          A2FF9ACDA6FFA9D2B1FFBFD2B7FF93AB7BFE85A069FABBCCAEFFB7DCC2FFA6DE
          BFFFA8E0C3FFA9E2C7FFABE5CBFFADE7CEFFADE9D0FF73DCB4FF57DAABFF5ADF
          B3FF5CE3B9FF83CBA2FF4F7429F60912007C000000390000000B00000000162A
          016E8FAA78FFADD0AFFFA0CBA5FFA0CBA5FFA1CCA6FFA1CCA6FFA2CDA8FFA3CF
          ABFFB5D8BAFFA3B88FFE19300280050900190205000C1223005E7F9A63F6C5E2
          CDFFB2E3C9FFB3E5CCFFB5E7CFFFB7E9D2FFB8EBD6FFB9EDD8FF7FE0BCFF58DC
          AEFF5BE1B5FF5CE3B9FF8CD1ABFF53772EF60912007A00000027000000000000
          0000172C0272B1C4A0FFBBD8BDFFADD2B1FFAED3B2FFAED3B2FFAFD3B3FFBFDB
          C2FFABC09BFE13250064000000000000000000000000000000000B15003787A0
          6CF7D0E9D8FFBEE7D2FFBFE9D5FFC0EBD7FFC2EDDAFFC3EEDDFFC4F0DFFF8BE4
          C4FF59DEB1FF5CE3B8FF5CE3B9FF94D6B4FF3F601EE000000027000000000000
          000000000000172C0272BACAACFFC6DFC8FFBAD9BEFFBAD9BEFFCAE1CBFFB4C6
          A6FE132500640000000000000000000000000000000000000000000000000B15
          00388FA676F8DAEEE2FFCAECDBFFCAEDDDFFCCEFDFFFCDF0E1FFCEF2E4FFCFF3
          E6FF85E4C3FF5AE0B4FF5CE3B9FF6BE2BBFF749254FC02030014000000000000
          00000000000000000000172C0272C2D1B5FFD5E7D6FFD7E8D9FFBDCCB0FE1325
          0064000000000000000000000000000000000000000000000000000000000000
          00000B15003995AB7DF8E3F3EAFFD5F0E3FFD6F2E5FFD7F3E6FFD8F4E8FFD9F5
          EAFFD7F5EBFF7AE3BFFF63E2B9FFB0D6B9FF2C480EAE00000000000000000000
          0000000000000000000000000000152702658BA174EE859C6DEC112100570000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000B1500399DB287F9EBF7F1FFE1F5EBFFE1F5ECFFE2F6EEFFE3F7
          EFFFE4F8F0FFDDF7EEFFC2E2CCFF395916CB0205000C00000000000000000000
          0000000000000000000000000000000000000001000200010002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000B16003AA4B68FFAF2FAF6FFEBF8F2FFECF9F3FFEDFA
          F4FFEEFAF6FFF1F9F4FF4F6B30D10205000C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000B16003BA6B992FAF8FCFAFFF7FCFAFFF7FC
          FAFFF4FAF6FF526E36D20205000C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B16003BA7B992FAF8FCFAFFF0F7
          F1FF56713AD00205000C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000060C00211D3503861325
          0061020300080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000040000000D0000001300000014000000140000
          0014000000140000001400000014000000140000001400000014000000140000
          0014000000140000001400000014000000140000001400000014000000140000
          0011000000080000000200000000000000000000000000000000000000000000
          000000000000000000010000000E00000026000000390000003C0000003C0000
          003D0000003D0000003D0000003D0000003D0000003D0000003D0000003D0000
          003D0000003D0000003D0000003D0000003D0000003D0000003D0000003D0000
          00320000001A0000000500000000000000000000000000000000000000000000
          0000000000000000000100000013A3A3A3B5CDCDCDDEC9C9C9DBC9C9C9DBCACB
          C9DBBDB4C9DCC4C0C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9
          C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCC9C9C9DCD1D1
          D1E04242425B0000000100000000000000000000000000000000000000000000
          0000000000000000000100000014CCCCCCDBFFFFFFFFFDFDFDFFFDFDFDFFFAFB
          F8FFE9DEF8FFF2EDF8FFF9F9F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFFFFF
          FFFF5151516D0000000000000000000000000000000000000000000000000000
          0000000000000000000100000013CCCCCCDBAFAFAFC43C3C3C548080809AF3F5
          F2FEE7DBF5FFEEE9F4FFF5F5F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4
          F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFFFFF
          FFFF5050506B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014D0D0D0DB4343436D000000180C0C0C3FE3E4
          E1EEEFE3FEFFF4EFFAFFFBFBFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFFFF
          FFFF5252526B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014C8C8C8DB828282B00B0B0B554D4D4D89E6E8
          E5FAE1D6EFFFE7E2EDFFEEEEEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDED
          EDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFF7F7
          F7FF4E4E4E6B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014CECECED8FFFFFFFFE1E1E1EEF3F3F3FAFFFF
          FDFFEEE2FCFFF6F1FCFFFDFDFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFBFBFBFFF8F8F8FFF9F9F9FFFCFCFCFFFCFCFCFFFCFCFCFFFFFF
          FFFF5353536B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014C5C5C5D8F6F6F6FFF3F3F3FFF1F1F1FFF0F2
          EFFFE1D6EFFFE9E4EFFFF0F0EFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEF
          EFFFEEEEEEFFE8E8E8FFDFDFDFFFE0E0E0FFEAEAEAFFEFEFEFFFEFEFEFFFFAFA
          FAFF4F4F4F6B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014C8C8C8D8FAFAFAFFF4F4F4FFF4F4F4FFF5F7
          F4FFE6DAF4FFEEE9F4FFF5F5F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4F4FFF4F4
          F4FFF4F4F3FFE7E7E7FFB2B5B8FFC6C6C7FFDFDFDFFFEFEFEFFFF4F4F4FFFFFF
          FFFF5151516B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014C9C9C9D8FCFCFCFFF6F6F6FFF6F6F6FFF7F9
          F6FFE8DDF7FFF0EBF6FFF7F7F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6
          F6FFF6F6F6FFEEEEEEFF7F8386FF7D8084FFC8C9C9FFE1E1E1FFF2F2F2FFFFFF
          FFFF5252526B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014C2C2C2D8EFEFEFFFEBEBEBFFEAEAEAFFEAEC
          E9FFDDD2EBFFE4DFEAFFEAEAE9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
          E9FFE9E9E9FFE4E3E2FFAEB9C1FF7FA8D2FFA4B9CEFFBCBBBBFFD9D8D8FFF3F3
          F3FF4E4E4E6B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014CCCCCCD7FFFFFFFFF2F2F2F9F8F8F8FFFAFC
          F9FFEBE0FAFFF2EDF9FFF9F9F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF8F8F8FFF3F2F1FFD0DCE7FF89B9EBFF8EBAE8FFB1C3D6FFD0D1D1FFFBFB
          FAFF5353536B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000013C6C6C6DB9B9B9BB42424243A6A6A6A85E8E9
          E6FCDFD3ECFFE4DFEAFFE9EAE9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
          E9FFE9E9E9FFE7E6E5FFCED4DBFF96C7F0FF78BAF8FF4794F0FF8499C3FFE7E5
          E1FF4D4D4D6B0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014CCCCCCDA3F3F3F6E0000001E0B0B0B41DBDC
          D9EDE8DCF5FFEBE6F1FFF0F1F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
          F0FFF0F0F0FFF0EFEEFFDCE2E4FF70DAF7FF33A7FFFF258EFBFF4477CCFFDCDB
          DBFF4E4E4D6C0000000000000000000000000000000000000000000000000000
          0000000000000000000100000014CBCBCBDA999999C21F1F1F696767679DECEE
          EBFCE5D9F3FFE9E4EFFFEEEFEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEE
          EEFFEEEEEEFFEFEEEEFFEAE7E5FF92DBEAFF43BFFFFF319BFFFF2574E1FFB2BB
          D1FF4D4C496F0000000200000000000000000000000000000000000000000000
          0000000000000000000100000014C0C0C0D8EDEDEDFFE0E0E0FAE6E6E6FFE7E8
          E6FFDBD0E8FFE1DCE6FFE5E6E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
          E5FFE5E5E5FFE5E5E5FFE5E2E1FFBBD7DCFF56D3FBFF35A1FFFF2386F3FF7090
          CCFF474541750000000400000000000000000000000000000000000000000000
          0000000000000000000100000014C8C8C8D8F7F7F7FFF2F2F2FFF2F2F2FFF2F3
          F1FFE7DBF6FFEDE8F3FFF1F2F1FFF1F1F1FFF1F1F1FFF1F1F1FFF1F1F1FFF1F1
          F1FFF1F1F1FFF1F1F1FFF2F1F1FFE3E5E5FF78DEF4FF39A9F3FF2784E0FF4281
          E1FF393B3F7C0000000900000000000000000000000000000000000000000000
          0000000000000000000100000014BEBEBED8E6E6E6FFE1E1E1FFE1E1E1FFE2E3
          E1FFD8CDE6FFDDD9E3FFE1E2E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1
          E1FFE1E1E1FFE1E1E1FFE1E1E1FFE0DCDBFF9CD5E0FF4AC9FDFF2B82D4FF2B7E
          E5FF132546890000001300000001000000000000000000000000000000000000
          0000000000000000000100000014C5C5C5D8EFEFEFFFEAEAEAFFEAEAEAFFEBEC
          EAFFE1D6F0FFE6E2EDFFEAEBEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEA
          EAFFEBEBEBFFEBEBEBFFEBEBEBFFEBEAE9FFCCDCDFFF61DAFBFF3599E4FF2D8A
          E5FF0A3981B90000002100000003000000000000000000000000000000000000
          0000000000000000000100000014C2C2C2D7ECECECFFE9E9E9FFE8E8E8FFE7E8
          E5FFDED2ECFFE2DDE8FFE6E7E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6
          E6FFE4E4E4FFDDDDDDFFDCDCDCFFE2E2E2FFDFDCDBFF83D9ECFF42B9F5FF2E85
          D0FF1B6AC8ED0006144500000007000000000000000000000000000000000000
          0000000000000000000100000014BEBEBED7E5E5E5FFCDCDCDE8DBDBDBF7E2E3
          E0FFD7CCE5FFDCD7E2FFDFE0DFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDF
          DFFFD5D5D5FFDBDBDBFFDCDCDCFFCECECEFFCDCAC8FF9EC7D0FF51D2FAFF3593
          D6FF2D8FE8FF05224E810000000F000000020000000000000000000000000000
          0000000000000000000100000013CCCCCCDB8787879F0C0C0C265151516EE8E9
          E6F9E5D9F3FFE8E3EEFFEBECEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFECEC
          ECFFDCDCDCFFE6E6E6FFFFFFFFFFFEFEFEFFFCFAFAFFDDE4E6FF6EE0FBFF39A0
          D9FF318CD3FF12539EC800000023000000050000000000000000000000000000
          0000000000000000000100000014CECECEDA42424272000000240C0C0C46DBDC
          D9EEE6DAF5FFE7E2EDFFEAEBEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEBEB
          EBFFDBDBDBFFE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCFF9CE3F2FF4AC8
          F6FF3692CEFF2683D5F7010F2554000000090000000000000000000000000000
          0000000000000000000100000014CBCBCBDAADADADD03D3D3D7E818181AFEAEC
          E9FEE3D7F2FFE5E0ECFFE8E9E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFE9E9
          E9FFD8D8D8FFEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFDAD7D7E2425C617B4FCD
          EEF345C0F6FF36B2FAFF0B35689A000000130000000200000000000000000000
          0000000000000000000100000014C5C5C5D8F1F1F1FFF0F0F0FFEEEEEEFFE8EA
          E7FFE1D5F0FFE5DFEBFFE7E8E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7E7FFE8E8
          E8FFD4D4D4FFEEEEEEFFFFFFFFFFFFFFFFFFC3C3C3CF2120202B0000000741A0
          B7C16CDBFFFFA3DFF6FFA9B4BCDD0C0B092F0000000600000000000000000000
          0000000000000000000100000013C4C4C4D7EAEAEAFFE5E5E5FFE5E5E5FFE6E7
          E4FFE0D4EFFFE3DEE9FFE5E6E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE2E2
          E2FFD1D1D1FFFBFBFBFFFFFFFFFF959595A20C0C0C1400000000000000004B57
          5A71EFF3F0FFEBE5EDFFBBB9DDFA2424315F0000000A00000001000000000000
          000000000000000000000000000CC9C9C9DBEFEFEFFFEAEAEAFFEAEAEAFFEAEC
          E9FFE5D9F5FFE8E3EEFFEAEBEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFDEDE
          DEFFECECECFFF6F6F6F86A6A6A74000000000000000000000000000000001715
          14269B9AD0E28D8EF2FF7A7AE4FF4B4B7CA70000001000000001000000000000
          0000000000000000000000000003999999A2BCBCBCC9B8B8B8C5B8B8B8C5B8B8
          B8C5B6B3BBC5B7B6B9C5B8B8B8C5B8B8B8C5B8B8B8C5B8B8B8C5B8B8B8C5B4B4
          B4C6A5A5A5B83939394100000000000000000000000000000000000000000000
          000154548E9E8D8DFFFF8585EDFF6969AACB0C0C0F1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000252538407070B1BD3B3B5C68111119200101010200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010101020505050700000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010101190000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000604010E0502000E000000000000
          00000000000000000000000000000000000000000000000000000707071D4848
          4875686868AD0101011D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000012120F25575443BB2812005D0301
          0009000000000000000000000000000000001717173D61616198C9C9C9E7FBFB
          FBFFFFFFFFFF595959A300000003000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000070553788BB6917FF6C38
          08C3160A003D030303112F2F3060888888BDE9E9E9FDF8F8F8FFF1F1F1FFD9D9
          D9FFF8F8F8FFFEFEFEFF2222226C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002010002B17B42D2CA87
          40FF8C511CEC9B8C81DCF5F8FBFFF4F4F4FFF1F1F1FFF8F8F8FFFFFFFFFFEFEF
          EFFFD5D5D5FFFFFFFFFFD8D8D8EF080808390000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000404040C201F1D4EAFA193D3F0C9
          9FFFC68239FFA86A32FFCDBDB2FFFDFFFFFFFEFEFEFFFDFDFDFFFDFDFDFFFFFF
          FFFFE1E1E1FFE1E1E1FFFFFFFFFF959595CC0000001300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A263E3E3E7BABABABD2ECEEF0FFECEFF2FFECDB
          CDFFECC59BFFC17D34FFA5672FFFD5C6BAFFFFFFFFFFFBFCFCFFF7F7F7FFF8F8
          F8FFFCFCFCFFD6D6D6FFEAEAEAFFFFFFFFFF4F4F4F9B00000001000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          02061919194A696969A5D6D6D6EFF5F5F5FFEEEEEEFFF0F0F0FFF7F7F8FFFAFC
          FFFFF4E3D5FFEDC49AFFC17D33FFA4662EFFD2C2B6FFFCFFFFFFFBFBFBFFFCFC
          FCFFFBFBFBFFF4F4F4FFD2D2D2FFF4F4F4FFFAFAFAFE1D1D1D64000000000000
          0000000000000000000000000000000000000000000000000000000000000404
          0407BABABAD2F6F6F6FFECECECFFF1F1F1FFF7F7F7FFFAFAFAFFFAFAFAFFFAFB
          FBFFF9FBFCFFF1E1D2FFECC49AFFC17D33FFA4672DFFD4C4B8FFFDFFFFFFF7F7
          F7FFF6F6F6FFFCFCFCFFEBEBEBFFD2D2D2FFFFFFFFFFCECECEEB060606330000
          00000000000000000000000000000000000000000000000000000A0A0A243131
          3172B7B7B7D9F7F7F7FFF8F8F8FFF9F9F9FFF9F9F9FFF9F9F9FFF6F6F6FFF4F4
          F4FFF5F6F6FFF8FBFDFFF4E2D4FFECC49AFFC27D34FFA3662DFFD0C1B5FFFDFF
          FFFFFAFAFAFFF7F7F7FFFAFAFAFFDCDCDCFFDBDBDBFFFFFFFFFF898989C60000
          00100000000000000000000000000000000000000000000000006363636AFFFF
          FFFFFAFAFAFFF7F7F7FFF7F7F7FFF5F5F5FFF3F3F3FFF3F3F3FFF5F5F5FFF7F7
          F7FFF7F7F7FFF5F6F6FFF3F6F9FFF0DFD1FFEDC49BFFC17D34FFA4672EFFD3C3
          B6FFF8FBFEFFF4F4F4FFF7F7F7FFF9F9F9FFD0D0D0FFE8E8E8FFFFFFFFFF4545
          4593000000000000000000000000000000000000000000000000000000008484
          8489F6F6F6FFF3F3F3FFF1F1F1FFF2F2F2FFF5F5F5FFF6F6F6FFF5F5F5FFF2F2
          F2FFF1F1F1FFF3F3F3FFF5F6F6FFF6F9FCFFF0DFD1FFECC49AFFC17D34FF965C
          27FFD2C3B5FFFAFFFFFFF5F5F5FFF4F4F3FFEEEEEEFFCCCCCCFFEFEFEFFFF2F2
          F2FC1717175B0000000000000000000000000000000000000000000000000000
          00008E8E8E96F2F2F2FFF4F4F4FFF4F4F4FFF2F2F2FFF0F0F0FFF0F0F0FFF2F2
          F2FFF5F5F5FFF4F4F4FFF2F1F1FFF0F1F1FFF1F4F6FFF0E0D1FFECC59BFF8C4A
          24FF81451BFFD2C2B2FFF5F9FCFFF4F4F4FFF8F8F8FFE3E3E3FFCFCFCFFFFDFD
          FDFFC2C2C2E70404042C00000000000000000000000000000000000000000000
          0000050505059C9C9CA5F2F2F2FFEEEEEEFFEFEFEFFFF2F2F2FFF3F3F3FFF1F1
          F1FFEFEFEFFFEEEEEEFFF0F0F0FFF2F2F2FFF3F3F4FFF1F3F6FFEDDCCDFFEAC2
          9FFF935638FF834A1FFFD4C4B5FFF6FAFCFFF0F0F0FFF4F4F4FFD6D6D6FFD5D5
          D5FFFFFFFFFF7B7B7BBE0000000D000000000000000000000000000000000000
          0000000000000909090AA9A9A9B4F1F1F1FFF1F1F1FFEFEFEFFFEDEDEDFFEDED
          EDFFF0F0F0FFF1F1F1FFF1F1F1FFEEEEEEFFEDEDEDFFEEEFEFFFF0F3F6FFF0DF
          CDFFEECBABFF965C3EFF82471CFFCEBEAFFFF4F7F9FFF2F3F3FFF1F1F1FFCBCB
          CBFFE4E4E4FFFFFFFFFF3A3A3A8A000000000000000000000000000000000000
          0000000000000000000011111112B7B7B7C2ECECECFFECECECFFEFEFEFFFF0F0
          F0FFEDEDEDFFECECECFFECECECFFEEEEEEFFEFEFEFFFEFEFEFFFEEEEEEFFEBEE
          F1FFEDDBCAFFEBC5A3FF8D4A2AFF8C5130FFD2C0B6FFEFF1F2FFEFEFEFFFE9E9
          E9FFC6C6C6FFEEEEEEFFE9E9E9FA111111520000000000000000000000000000
          000000000000000000000000000017171719C0C0C0CFEEEEEEFFEBEBEBFFEAEA
          EAFFEBEBEBFFEEEEEEFFEEEEEEFFEDEDEDFFEAEAEAFFEAEAEAFFECECECFFEEEF
          EFFFEEF1F3FFEBDAC9FFD1A284FF91482CFFAA8877FFF0F3F5FFEFEFEFFFF0F0
          F0FFDCDCDCFFCCCCCCFFFCFEFFFFB5B8B8E10202022400000000000000000000
          0000000000000000000000000000000000001E1E1E21C8C8C8D9EBEBEBFFECEC
          ECFFECECECFFEAEAEAFFE8E8E8FFEAEAEAFFECECECFFEDEDEDFFEBEBEBFFEAEA
          EAFFE8E9EAFFEBEEEFFFD8C3B9FFB89383FFABA5A3FFE0E1E2FFEAEAEAFFE9E9
          E9FFF1F1F1FFCBCACAFFA99183FFDDCBC0FF686868B30000000A000000000000
          0000000000000000000000000000000000000000000026262629D0D0D0E1E8E8
          E8FFE8E8E8FFE9E9E9FFEBEBEBFFEAEAEAFFE8E8E8FFE7E7E7FFE8E8E8FFEAEA
          EAFFEBEBEBFFEAEAEBFFEAEDEEFFEAEEEFFFE0E1E2FFE8E7E7FFECECECFFEDEE
          EFFFEFF2F5FFF2F6F9FF957968FF926347FFFAF6F4FF32323281000000000000
          000000000000000000000000000000000000000000000000000030303035D4D4
          D4E9E9E9E9FFE7E7E7FFE6E6E6FFE6E6E6FFE8E8E8FFE9E9E9FFE9E9E9FFE7E7
          E7FFE6E6E6FFE6E6E6FFE8E8E8FFEAEAEAFFEEEFF0FFEEF0F2FFE9ECEEFFDDD9
          D6FFC4B1A4FFBDA394FFD7D0CCFF9E9B99E76A6A6B962B2B2B45000000000000
          0000000000000000000000000000000000000000000000000000000000003C3C
          3C43DADADAF1E6E6E6FFE8E8E8FFE8E8E8FFE6E6E6FFE4E4E4FFE4E4E4FFE6E6
          E6FFE8E8E9FFEAECEDFFECEFF1FFE8EAECFFDCD7D3FFC5B2A6FFA9856FFF915E
          3FFF844B28FF854C29FFD1BAACFFB0B4B6E40303031400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00004A4A4A52E0E0E0F7E4E4E4FFE3E3E3FFE5E5E5FFE7E7E7FFE9EBECFFEAEE
          F0FFE7E9EBFFDAD5D1FFC3B0A3FFA7836DFF8F5D3EFF844B28FF8C5434FFA379
          5FFFBFA595FFC6BEB7F28F8E8EB9555555730404041600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000057575761E3E3E3FDE5E5E5FFE7E9EAFFE7E9EAFFD8D3D0FFC1AD
          A1FFA6816AFF8E5C3DFF844B28FF8D5636FFA77D63FFC1A899FFC2BAB4EF8E8E
          8EB24E5051681717182500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000064646470E7E8E9FFCDC0B8FFA17961FF8E5A3CFF854B
          28FF8C5636FFA87E65FFC3A99AFFBCB4AEEA7D7D7DAB3F4041611112121F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000007172737DCBBDB3FF966749FFA57C62FFBCA4
          95FFBAB2ADE5777878A5363738590C0C0C1A0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008182828FC1C0BFE77778789E3E40
          40530C0C0C150000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001717171A2E2F2F4F050505090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000040200062513062F45230A5768361182763E1394763E1395713B
          128E542B0E6A3119073E11090216000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001008
          02145E300F75A3561ECBC8702FF6D97E3BFFDB8443FFDC8647FFDC8647FFDB85
          46FFDA813FFFD27734FFB56225E07E41159D2B16073700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000005030007542C0D6AB15F
          22DDDA7F3CFFDD8C4FFFDB8E54FFDA8D53FFD98A4EFFD8884CFFD8884CFFD889
          4DFFD98B52FFDA8D55FFDC8D53FFDD8545FFC86E2CF77C40159C1D0F05250000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001B0E0423964F1BBCD87C38FFDB8A
          4EFFD88A4FFFD68446FFD58041FFD57E3DFFD47D3CFFD57E3EFFD57E3EFFD57E
          3EFFD57E3EFFD57F3FFFD68243FFD8874CFFDA8B51FFDC8443FFBD6626EA4825
          0B5B000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000027140631B25E21DDDD8544FFD98A4FFFD683
          44FFD57E3EFFD57F3FFFD47D3CFFD58041FFD8894EFFD57F40FFD47B3AFFD57F
          3FFFD57F3FFFD57F3FFFD57F3FFFD57E3EFFD58040FFD8884CFFDB8A4DFFD074
          30FF5D300F740000000000000000000000000000000000000000000000000000
          000000000000000000001D0F0525B25F22DEDC8443FFD78548FFD57F3EFFD57F
          3EFFD57F3FFFD47D3CFFDB9059FFF4DFCFFFFBF2ECFFF5E1D2FFE1A478FFD47C
          3BFFD57F3EFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57E3EFFD68243FFDA88
          4CFFD37632FF562D0E6C00000000000000000000000000000000000000000000
          0000000000000804010A9C531DC3DB8240FFD68345FFD57F3EFFD57F3FFFD57F
          3FFFD57F3FFFD47C3BFFEDC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFDF8F5FFE7B5
          8FFFD47C3BFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3EFFD580
          41FFD98547FFCA6F2BF9361B0943000000000000000000000000000000000000
          0000000000006534117ED87A34FFD68243FFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD47C3AFFECC5A7FFFFFFFFFFFFFFFFFFFFFFFFFFE5B18AFFEABF
          A0FFE2A679FFD47C3AFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD58040FFDA813EFFA8591FD00B05010D0000000000000000000000000000
          00001C0E0424BF6725EDD7803FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD47B39FFE1A376FFFFFFFFFFFFFFFFFFFFFFFFFFE8B996FFD57D
          3CFFE9BE9DFFD98A50FFD57E3DFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD58040FFD77730FF5A2F10720000000000000000000000000000
          00006D3B1687DA7F3AFFD58040FFD57E3EFFD57F3EFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57E3DFFD8884CFFFAF1EAFFFFFFFFFFFFFFFFFFF6E2D3FFD57F
          3FFFD78346FFD68345FFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD58040FFD77C38FFAD5B1FD80C06020F00000000000000000B05
          000EAE6027D6E0935CFFDC925DFFD9894FFFD68244FFD57F3EFFD57E3EFFD57F
          3FFFD57F3FFFD57F3FFFD47C3AFFEECCB3FFFFFFFFFFFFFFFFFFFEFAF7FFDB92
          5CFFD47C3AFFD57F3EFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD57F3EFFD16F27FF3F200B4F00000000000000003018
          073CD07633FFDE9662FFDD9763FFDD9763FFDD9560FFDA8F57FFD8864AFFD580
          40FFD57E3EFFD57F3EFFD37B38FFE2A67AFFFFFFFFFFFFFFFFFFFFFFFFFFE6B4
          8FFFD37A37FFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD58040FFD6742CFF793D11980000000000000000582C
          0B70DC8444FFDE9864FFDD9662FFDD9662FFDD9662FFDD9763FFDD9663FFDC93
          5DFFD98B51FFD78345FFD57E3DFFD7874BFFFAF1EAFFFFFFFFFFFFFFFFFFF3D7
          C4FFD57E3EFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD58041FFD77934FFA04F13CA0201010300000000783B
          1098DD894CFFDE9864FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          63FFDD9763FFDD9561FFDB8F57FFD78446FFEECDB3FFFFFFFFFFFFFFFFFFFCF7
          F2FFD98C53FFD57D3DFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD58040FFD77E3BFFB35816E2160B041B000000008845
          14ABE09258FFDD9764FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9763FFDC925CFFE8B895FFFFFFFFFFFFFFFFFFFFFF
          FFFFE3AC82FFD37A37FFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD57F3FFFD7803FFFBA5B15EC1D0E0425000000008A43
          10AFE1965EFFDD9763FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9560FFE09F70FFFCF6F2FFFFFFFFFFFFFF
          FFFFF1D2BCFFD57F3EFFD57E3EFFD57E3EFFD57F3FFFD57F3FFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD57F3FFFD88344FFBE5F19F0200E0129000000007F3C
          0BA3E29862FFDD9763FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9561FFDE9A68FFFAEEE7FFFFFFFFFFFFFF
          FFFFFCF4EFFFDE9B69FFDA8C52FFD78345FFD57E3EFFD57E3EFFD57F3FFFD57F
          3FFFD57F3FFFD57F3FFFD57F3FFFD98445FFB95D18E91A0B002200000000662B
          0086E09157FFDF9A68FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD945EFFE4AD85FFF4DDCDFFFEFDFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFE8B794FFDC945EFFDD9561FFDA8F56FFD8884DFFD58041FFD57E
          3DFFD57E3EFFD57F3FFFD57F3FFFDC8B4EFFAA500ED80B05001000000000421A
          0058DC884AFFE1A274FFDD9560FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9661FFDE9967FFE3A97FFFE7B48EFFEBC1A3FFF1D2
          BCFFF7E6DAFFEBC2A5FFDD935EFFDD9662FFDD9763FFDD9663FFDC935DFFD98C
          52FFD78446FFD57E3EFFD58041FFDF9056FF8E3C01B900000000000000001D0A
          0027C26B2AF0E6AD83FFDD9560FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9662FFDC945FFFDC935DFFDC915BFFDC93
          5EFFDD9661FFDD9764FFDD9662FFDD9662FFDD9662FFDD9662FFDD9663FFDD97
          63FFDD9662FFDA8E55FFDD9762FFDD8647FF5A24007700000000000000000000
          000090430BB9E9AC80FFE09F6FFFDD9560FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD9561FFDC93
          5EFFDE9966FFDC945FFFDD9560FFDD9662FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9663FFE8B48DFFCA7231F8240D003100000000000000000000
          0000431A0058DA864AFFE8B793FFDD9561FFDD9662FFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD9661FFDE9765FFEECC
          B2FFF9EBE0FFF1D4BFFFDF9D6DFFDD9560FFDD9662FFDD9662FFDD9662FFDD96
          62FFDC945FFFE2A87DFFE9AB7DFF8A3F08B20000000000000000000000000000
          0000050100089C4A0FC7EAB085FFE5AE86FFDC945FFFDD9662FFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDC935DFFEABE9DFFFFFF
          FFFFFFFFFFFFFFFFFFFFEECAB0FFDC935EFFDD9662FFDD9662FFDD9662FFDD94
          60FFDF9C6CFFEBBE9CFFCE7534FC3011003F0000000000000000000000000000
          00000000000032130042CC7331FBECC1A1FFE2A77BFFDC945FFFDD9662FFDD96
          62FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDB915BFFEECCB3FFFFFF
          FFFFFFFFFFFFFFFFFFFFF2D8C6FFDD945FFFDD9662FFDD9662FFDD9460FFDE9B
          69FFEBC2A3FFE49B65FF75320298000000000000000000000000000000000000
          000000000000000000005F27007CDB8646FFEDC6A9FFE4AC83FFDC955FFFDD95
          60FFDD9662FFDD9662FFDD9662FFDD9662FFDD9662FFDD945FFFE2A77BFFFBF2
          ECFFFFFFFFFFFDF9F5FFE6B28CFFDC935EFFDD9661FFDC945FFFDF9D6DFFEBC2
          A5FFE8AA7AFFA04C0ECD0C040010000000000000000000000000000000000000
          000000000000000000000000000076330299DF8E53FFEDC6A9FFE8BA96FFE09D
          6DFFDC955FFFDD9560FFDD9662FFDD9662FFDD9662FFDD9662FFDC945FFFE1A3
          75FFE8B896FFE3A77BFFDC945EFFDC945FFFDE9864FFE4AD84FFEEC9ADFFE8A9
          7AFFAD5311DC1E0A002800000000000000000000000000000000000000000000
          0000000000000000000000000000030100036E300290D67E3DFFEDBE9BFFEECA
          AEFFE6B38CFFDF9E6DFFDD9662FFDC945FFFDC9460FFDC9460FFDD9560FFDC93
          5DFFDB9059FFDC925CFFDE9A68FFE3A97DFFEBC3A4FFEEC8ABFFE59D68FFA34D
          0ED01F0B00290000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000004A1E0061B55D1DE3E39A
          65FFEEC3A4FFEECCB1FFEBC0A1FFE7B48FFFE3AA80FFE2A77BFFE2A77BFFE3A8
          7DFFE5B089FFE9BC99FFEDC8ACFFEFCBB0FFEAAE82FFCD7433FC7934039D0F05
          0014000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000012060018682D
          0287BD6526EBE09259FFEBB48BFFEFC3A2FFEFC9ADFFEFCCB1FFEFCCB1FFEFCB
          B0FFEFC6A7FFEDBD98FFE6A06DFFD07938FD8E4209B62E12003D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000018080020501F006A8B3F07B3AD5516DAC16624F0CD793BFACE7B3FF9C870
          2FF6B55C1BE39B490DC76A2C008A2C10003C0100000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000C0300101F0A002A260D0033260D0033240C
          00301407001C0000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000908080F02020203000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000004030306645E59A59F9488F5988E83EF433F3B700303
          0305000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000B0B0A137F776FCEAC9E91F7DAD3CCFFC5B8ADFFA8988BF39187
          7DE6312E2B520000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001615142590867DE5B6A99BFAF1EFEDFFF2F1EFFFF4F2F1FFDFDAD4FFC1C3
          BBFFA4978AF2827A72D222201E39000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002523
          213F9B9086F2C0B4A9FEF1EFEDFFECE9E7FFEEEBE9FFF1EFEDFFE6F2F2FFF5F5
          F4FFB7E9F0FFB1B4AAFFA29689F3706962B71514132400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000039363360A196
          89F6CCC1B8FFEFEDEBFFE5E2DEFFE8E5E2FFEBE8E5FFEDEBE9FFE9E9EAFFECE8
          E9FFDAE4E7FFCFE1E7FF8ECBE6FFB3A395FCA09488F55D58529A0B0B0A130000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001010101524D4989A4978AF5D6CE
          C6FFEAE8E5FFDFDBD7FFE2DEDAFFE5E1DEFFE8E4E1FFE8E5E3FFE7DFE0FFD5DB
          DFFFC3D8DEFFC1D1DEFFBDCEDCFFF1EDEAFFCDC3B9FFAE9E90F99C9186F34945
          417B050404080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000050404086D6761B1ADA095F5DFD9D3FFE4E0
          DCFFD9D4CFFFDCD7D3FFDFDBD6FFE2DEDAFFE5E1DDFFD6D4D4FFE0D7D8FFB9D0
          D7FFABCDD8FF9EC2D9FF9CC3DAFFF8F7F6FFFBFAFAFFEFEBE8FFC6BAAEFFA99B
          8DF5958A81EB3835325E01010101000000000000000000000000000000000000
          000000000000000000000C0C0B1589827CD2C6BFB8F7E6E2DEFFD9D3CEFFD4CE
          C8FFD6D1CBFFD9D4CFFFDCD7D2FFDFDAD6FFE1DEDAFFC5C1BEFFC9D6D9FFB1D3
          D8FF71BBDDFF67AED3FFDBDAD8FFF5F4F2FFF8F7F6FFFBFAFAFFFDFDFDFFE8E3
          DFFFBFB2A5FFA6988BF3898077DB272523420000000000000000000000000000
          000000000000181716299F9992E8D4CEC9FAE7E3DEFFD9D2CAFFD8D1CBFFD6CF
          C9FFD4CEC8FFD6D0CBFFD9D4CFFFDCD7D2FFDEDAD6FFB7B1AEFF868284FF6395
          A6FF5289A4FFD6D3D0FFEFEDEBFFF2F0EFFFF5F3F2FFF8F7F6FFFAFAF9FFFDFD
          FDFFFEFDFDFFE0DAD4FFBAAB9EFFA19588F32C2A284B00000000000000000000
          000028262343B4AEA9F3DED9D5FEE7E2DDFFD6CDC5FFD4CCC4FFD7D0C8FFDAD3
          CEFFDDD8D3FFDBD6D1FFD7D1CDFFD9D3CEFFDBD6D2FFADAAA6FF111111FF6D6C
          6AFFE2DFDCFFE8E5E2FFE6E4E1FFEFEDEBFFF2F0EEFFF4F3F2FFF7F6F5FFFAF9
          F9FFFDFDFCFFFFFFFFFFFBFBFAFFD3CAC2FB4844407900000000000000003E3A
          3866C5C1BDF5E4E1DCFFE6E1DCFFDBD4CDFFD9D1CAFFD7CFC7FFD7CFC8FFD9D2
          CBFFDCD5CFFFDFDAD5FFE2DEDAFFDEDAD6FFDBD6D2FFB9B4B0FF8F8B89FFBBB7
          B3FFB1AEABFFA7A5A3FFA7A5A2FFB7B4B3FFD1CECCFFEAE9E7FFF4F3F1FFF7F6
          F5FFFAF9F8FFFDFCFCFFFFFFFFFFE9E6E3FC2E2C294E000000004B474478D6D3
          D1F4E8E3DFFFE6E1DCFFE2DCD7FFE0DAD4FFDED8D1FFDCD5CEFFDAD3CCFFDAD2
          CBFFDCD5CEFFDED8D2FFE0DCD7FFE3E0DCFFE4E1DDFFB6B3B0FF262625FF8C89
          86FFA09D9BFFA2A09DFFA5A2A0FFA7A5A2FFA8A7A5FFABAAA8FFBEBDBBFFD9D7
          D6FFF2F1F0FFF9F9F8FFDDDAD6FB5A55518F01010101000000008A857ED2EBE7
          E3FFE8E4DFFFE9E5E1FFE7E2DEFFE5E0DBFFE3DED8FFE1DBD5FFDFD9D2FFDDD6
          D0FFDCD5CEFFDED7D1FFE0DAD5FFE2DDD9FFE4E1DEFFADAAA7FF313131FF232F
          2FFF5E686AFFA09D9BFFA2A09DFFA5A2A0FFA6A4A2FFA8A7A5FFAAA9A7FFACAB
          AAFFB0AEAEFFA09B95FA272523770000002B00000011000000009F9994D6F1EF
          EDFFF0EDEBFFEEEBE8FFECE8E5FFEAE6E2FFE8E4DFFFE6E1DDFFE4DFDAFFE2DC
          D7FFE0DAD4FFDFD8D2FFE0DAD5FFE2DCD8FFE4DFDBFFA0A09DFF98C1C5FF84E8
          EFFF52C2DBFF245B71FF9B9996FFA2A09DFFA4A29FFFA6A4A2FFA8A6A5FFA7A5
          A3FF7A7670F213121168000000480000004800000048000000351B1A182EACA7
          A1E1E7E3DFFDECE8E5FFF1EFECFFEFECE9FFEDEAE6FFEBE7E4FFE9E5E1FFE7E2
          DEFFE5E0DBFFE3DED8FFE1DCD6FFE3DDD8FFE4DFDAFF929694FFCFF7FAFF5EE6
          F6FF40D7F8FF02B3FEFF4A788AFFAAA7A4FFA29F9DFFA4A29FFF9E9B97FE625D
          59DC070707550000004800000048000000480000004800000036000000000000
          00002D2B284CBBB5B0EDE9E5E2FEEEEAE7FFF2F0EDFFF0EDEAFFEEEBE8FFECE8
          E5FFEBE6E2FFE8E4DFFFE6E1DDFFE5DFDAFFE5E0DBFF8A9594FFDFFAFBFF42E3
          FBFF08D9FDFF00B4FFFF0795D2FFDAD6D1FFD4D0CCFFB2ADA8FC504C47A80101
          012F000000430000004800000048000000480000004800000017000000000000
          000000000000010101023835325EC9C4BEF6EAE7E3FEF0ECEAFFF3F1EFFFF1EF
          ECFFEFECE9FFEEEAE7FFEBE7E4FFE9E5E1FFE8E3DEFFC8C7C5FFD6F5F7FFA1EE
          FBFF00D0FEFF0EC2FDFF04B3FDFF829EA9FFC6C1BBFA3734315C000000000000
          000000000001000000130000002A0000003E0000002700000000000000000000
          00000000000000000000000000000403030648444175D3CDC8FAECE8E5FFF2EF
          EDFFF4F2F0FFF3F0EEFFF0EEEBFFEEEBE8FFEDE9E5FFEBE6E2FF98C2C6FFE1FA
          FCFF1CCCFDFF10C8FCFF00B3FFFF108BBFFE1D1B1A3100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000807070D5B56538DDAD5
          D1FCECE8E5FFF4F2F0FFF6F4F2FFF3F1EFFFF2EFECFFF0ECEAFFB5C0C3FFE3F9
          FBFF89E3FBFF00BDFEFF08B8FDFF0BB2F6FF00131B3100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000D0C
          0C16706B67A6DFDBD7FDEDE9E6FFF6F5F3FFF6F5F3FFF5F3F1FFF3F0EEFFACD7
          DCFFDCF7FCFF0AB5FDFF16BBFBFF02A6FAFF086A9AD900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001413122286817CBEE3E0DBFDEDEAE7FFF8F7F6FFE7E2DDFF839C
          A2FCECFBFDFF70D3FBFF01A9FDFF079DF6FF1A97E1FD000C1117000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001C1A192F9A958FD1E0DBD6FDBDB6B0F4201F
          1E38B7E0E7FBD2F2FDFF02A0FEFF1AA7F7FF0A87EEFF085882B3000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000100F0E1B050404080000
          0000366978A4F5FCFEFF54BCFCFF089AFDFF0B7EEBFF2582CCF9000507070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000050709C5EBF3FEC1E9FDFF008CFEFF1C8FF1FF156DE4FF05496A800000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000005294A8CCFFFFFFFF359FFDFF128DFAFF135FE0FF2A71BAF20000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000F1516D3F1FAFFABDCFDFF0078FEFF1D6EE7FF2159D8FF012E
          4249000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000006DB2C6E4FAFDFEFF278BFBFF4FAADEFF6ABFE4FF2473
          9BC5000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000003202D2EC8F2FAFF6EC0C8FF3B686CFF60B8C4F20018
          2222000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000236274884398AAD40F485C68000203030000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000000000000000000010000000D000000230000002E0000002E0000
          002E0000002E0000002E0000002E0000002E0000002E0000002E0000002E0000
          002E0000002E0000002D00000016000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C19175247413ABA504841D04F4841D54F4841D55049
          42D5504942D5504942D5504942D5514943D6514A43D6514A43D6524B44D6524B
          44D64D4640D0272421A502020265000000440000002200000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000453E3998908983FFB6B3B1FFCCCCCCFFCBCBCBFFCDCDCDFFCFCF
          CFFFD1D1D1FFD2D2D2FFD4D4D4FFD5D5D5FFD7D7D7FFD9D9D9FFDADADAFFDADA
          DAFFDBDBDBFF9B9794FE96918CF9504C47C90404036F0000003A000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000090807137D746EF7CCCCCCFFCCCCCCFFCCCCCCFFCDCDCDFFD2D2D2FFD7D7
          D7FFDCDCDCFFE0E0E0FFE5E5E5FFEAEAEAFFEFEFEFFFF3F3F3FFF6F6F6FFF7F7
          F7FFF8F8F8FFC3C3C3FF88827EFFD5D4D4FF86817CF00C0B0A82000000450000
          0010000000000000000000000000000000000000000000000000000000000000
          000027232055A4A09CFFCCCCCCFFCCCCCCFFCCCCCCFFCECECEFFD3D3D3FFD7D7
          D7FFDCDCDCFFE1E1E1FFE6E6E6FFEBEBEBFFEFEFEFFFF4F4F4FFF6F6F6FFF7F7
          F7FFF8F8F8FFC1C1C1FF85817EFFE5E4E4FFD8D8D8FFA29E9AFA1A1816960000
          0050000000170000000000000000000000000000000000000000000000000000
          000028242158B0ADAAFFCCCCCCFFCCCCCCFFCCCCCCFFCECECEFFD3D3D3FFD8D8
          D8FFDCDCDCFFE1E1E1FFE6E6E6FFEBEBEBFFF0F0F0FFF5F5F5FFF6F6F6FFF7F7
          F7FFF5F5F5FFB8B8B8FF84807DFFEFEFEEFFEAEAEAFFD6D6D6FFB5B2B0FE2E2A
          28AE000000520000001100000000000000000000000000000000000000000000
          000028242158B2AFACFFCCCCCCFFCCCCCCFFCCCCCCFFCECECEFFD3D3D3FFD8D8
          D8FFDCDCDCFFE1E1E1FFE6E6E6FFEBEBEBFFF0F0F0FFF5F5F5FFF6F6F6FFF7F7
          F7FFF5F5F5FFBBBBBBFF86827FFFF5F5F4FFF7F7F7FFE9E9E9FFD5D5D5FFBBB8
          B7FF201D1A9E000000490000000A000000000000000000000000000000000000
          000028242158B5B2AFFFCCCCCCFFCCCCCCFFCCCCCCFFCECECEFFD2D2D2FFD7D7
          D7FFDCDCDCFFE1E1E1FFE6E6E6FFEAEAEAFFEFEFEFFFF4F4F4FFF6F6F6FFF7F7
          F7FFF8F8F8FFCFCFCFFF928E8BFFF9F9F9FFFBFBFBFFF7F7F7FFE7E7E7FFD4D4
          D4FFA8A5A2FC100E0D880000003D000000050000000000000000000000000000
          000028242158B7B4B1FFCCCCCCFFCCCCCCFFCCCCCCFFCDCDCDFFD2D2D2FFD6D6
          D6FFD6D6D6FFDBDBDBFFE5E5E5FFE9E9E9FFEEEEEEFFF3F3F3FFF6F6F6FFF7F7
          F7FFF8F8F8FFF2F2F2FFBFBEBDFFD8D5D3FFFFFFFFFFFAFAFAFFF6F6F6FFE5E5
          E5FFD1D1D1FF908B87F4060605760000002D0000000000000000000000000000
          000028242158B9B6B3FFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFD1D1D1FFABA8
          CCFF6F6AAEFF919092FFD1D1D1FFE8E8E8FFEDEDEDFFF1F1F1FFF6F6F6FFF6F6
          F6FFD6D4E9FF7670B9FFA09FA8FFAEAAA7FFEAE8E7FFFDFDFDFFF9F9F9FFF4F4
          F4FFE2E2E2FFCECECEFF6A6561DE000000520000000800000000000000000000
          000028242158BCB9B6FFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFF9D99C8FF3629
          CEFF3A2ED5FF544BB5FF8D8D8FFFD3D3D3FFEBEBEBFFEFEFEFFFF3F3F3FFD1CF
          E7FF473DCDFF3B2FD7FF483DC2FF9C9BA5FFCBC9C8FFBAB4B0FFB6B1ADFFB2AD
          A9FFAEA9A5FFA7A29DFFABA7A3FF0F0E0D810000002100000000000000000000
          000028242158BEBBB8FFCCCCCCFFCCCCCCFFCCCCCCFF9B97C5FF3629CEFF3D32
          D9FF443CE1FF554BDDFF5D54B9FF8F8F91FFD5D5D5FFEDEDEDFFCDCBE3FF5D53
          D3FF514AE5FF4840E5FF3B30D7FF483DC2FF9B9AA4FFE3E3E3FFF9F9F9FFF8F8
          F8FFF8F8F8FFF8F8F8FFB9B5B1FE49433EC70000004200000001000000000000
          000028242158C0BEBBFFCCCCCCFFCCCCCCFF9D99C5FF4235CDFF372BD2FF3D32
          DAFF433AE0FF4941E6FF7067E3FF665EBAFF919193FFB8B6CEFF7369D7FF6059
          E9FF4C45E9FF473FE3FF4138DEFF382BD2FF473BBEFF9B9AA4FFE2E2E2FFF7F7
          F7FFF7F7F7FFF7F7F7FFE7E7E7FF7F7873F00000004800000002000000000000
          000028242158C2BFBDFFCCCCCCFFA4A0CAFF594CD0FF584CD7FF4E42D7FF4035
          D8FF4037DDFF463DE2FF4B45E7FF8882E8FF7169C1FF867DD9FF6C67EBFF4D47
          EAFF4941E6FF443BE1FF3F35DCFF3A2ED6FF3425CCFF473ABBFF9E9DA6FFEEEE
          EEFFF6F6F6FFF6F6F6FFE6E6E6FF928B85FF0000004800000002000000000000
          000028242158C5C2BFFFE4E4E4FF6B5FD0FF6D62DAFF6459DAFF5B50D8FF564C
          DBFF483EDCFF4239DFFF463EE3FF4B44E6FF7A74EBFF6760EAFF4B44E8FF4841
          E5FF453CE1FF4137DDFF3C31D8FF372BD4FF3325CFFF3323C7FF756EB3FFF2F2
          F2FFF4F4F4FFF3F3F3FFE4E4E4FF918A84FF0000004800000002000000000000
          000028242158C9C6C3FFF8F8F8FFADA6E2FF7165D4FF7066DDFF675DDBFF6156
          DBFF5B51DDFF5047DEFF4238DEFF443BE0FF453DE2FF463EE3FF453CE2FF433A
          E0FF4036DDFF3D32D9FF382CD5FF3427D0FF3324CAFF3E2EBCFFD1CFE2FFF0F0
          F0FFEFEFEFFFEEEEEEFFE0E0E0FF918A84FF0000004800000002000000000000
          000028242158CBC8C5FFF9F9F9FFF8F8F8FFA9A1DFFF7468D3FF756CDEFF6B61
          DCFF645ADCFF5E55DDFF5248DCFF4036DBFF4035DCFF4036DCFF3F35DCFF3E33
          DAFF3B30D8FF382CD4FF3427D0FF3829CBFF3E2DB8FFC9C7DCFFECECECFFEBEB
          EBFFEBEBEBFFEAEAEAFFDEDEDEFF8F8882FE0000004800000002000000000000
          000028242158CDCAC7FFFAFAFAFFF9F9F9FFF8F8F8FFA89FDDFF8176D5FF7A70
          DEFF6E64DDFF665CDCFF6056DCFF5348DBFF3C30D6FF3A2ED6FF392ED6FF382C
          D4FF3629D2FF3325CFFF4132CCFF4535B8FFC6C3D9FFE8E8E8FFE7E7E7FFE7E7
          E7FFE6E6E6FFE5E5E5FFDBDBDBFF8F8882FE0000004800000002000000000000
          000028242158CDCAC7FFFAFAFAFFFAFAFAFFF9F9F9FFF8F8F8FFA79EDBFF8B81
          D8FF7A71E0FF7269DEFF695FDCFF6156DAFF5246D7FF362AD1FF3426D0FF3325
          CFFF3325CFFF3F31CEFF4F3EB6FFB6B3C8FFE6E6E6FFE5E5E5FFE4E4E4FFE2E2
          E2FFE1E1E1FFE0E0E0FFD8D8D8FF8F8781FE0000004800000002000000000000
          000028242158CDCAC7FFFBFBFBFFFAFAFAFFFAFAFAFFF9F9F9FFD3D0E5FF8376
          CDFF887FE3FF7E75E1FF756CDFFF6D63DDFF6459DAFF5246D6FF3426CFFF3325
          CFFF3325CFFF4233CAFF5446A5FF919192FFD2D2D2FFE3E3E3FFE1E1E1FFE0E0
          E0FFDCDCDCFFDBDBDBFFD5D5D5FF8F8781FE0000004800000002000000000000
          000028242158CECBC8FFFCFCFCFFFBFBFBFFFBFBFBFFD4D1E5FF7F72C7FF9D95
          E3FF938CE6FF8A82E4FF8179E1FF796FDFFF7167DDFF685DDBFF4C40D5FF3325
          CFFF3325CFFF3325CFFF3622B8FF53459EFF8F8F90FFCDCDCDFFDFDFDFFFDDDD
          DDFFDBDBDBFFD7D7D7FFD2D2D2FF8E8680FE0000004800000002000000000000
          000028242158CFCCC9FFFDFDFDFFFCFCFCFFD5D2E6FF9389CFFFB4AEE9FFA8A2
          EAFF9F98E8FF968FE6FF8D86E4FF857DE2FF7D74E0FF746ADEFF6B61DCFF4235
          D3FF3325CFFF3325CFFF3325CFFF3F2CBBFF55489EFF8C8C8DFFCCCCCCFFDBDB
          DBFFD9D9D9FFD5D5D5FFCFCFCFFF8E8680FE0000004800000001000000000000
          000028242158D0CDCAFFFEFEFEFFE0DDF0FFA9A0D7FFCAC6F0FFBDB8EFFFB4AF
          EDFFABA5EBFFA39DE9FF9A93E7FF918AE5FF8980E3FF8077E1FF776EDFFF6C62
          DCFF3A2DD1FF3325CFFF3325CFFF3426CFFF5241C1FF5C4D9FFF908F91FFD5D5
          D5FFD7D7D7FFD3D3D3FFCDCDCDFF8F8780FE0000004800000001000000000000
          000028242158D0CDCAFFFEFEFEFFB5ACDDFFDFDDF6FFD1CEF4FFC9C5F2FFC0BB
          F0FFB8B3EEFFAFAAECFFA6A0EAFF9D97E8FF968EE6FF8C84E4FF837AE2FF7B72
          E0FF6358DAFF3325CFFF3325CFFF3325CFFF3426CFFF6658C7FF746A9FFFD7D7
          D7FFD8D8D8FFD6D6D6FFCFCFCFFF8E8680FE0000004800000001000000000000
          000028242158D0CDCAFFFFFFFFFFB3AADBFFEBEAF8FFDEDCF7FFD5D2F5FFCDC9
          F3FFC4C0F1FFBBB6EFFFB2ADEDFFB8B3ECFFB1A8DBFFB9B2E4FF958EE5FF877F
          E3FF7E75E1FF493CD4FF3325CFFF3325CFFF4639D0FF8072C7FFB6B1CDFFE0E0
          E0FFDCDCDCFFD9D9D9FFD1D1D1FF8E8781FD0000004800000001000000000000
          000028242158D1CECBFFFFFFFFFFFDFDFEFFB0A6DAFFF1EFFAFFE2E0F8FFD9D6
          F6FFD0CDF4FFC7C3F2FFCCC8F2FFB8B0DCFFD4D1E3FFB0A7D8FFC7C1E9FF9992
          E7FF8A82E4FF776EDFFF3527CFFF4C3FD2FF9589D0FFB0A9CDFFE6E6E6FFE3E3
          E3FFDFDFDFFFDDDDDDFFD4D4D4FF8D8680FD0000004800000001000000000000
          000028242158D1CECDFFFEFEFEFFFFFFFFFFFDFDFEFFB3AADCFFF5F4FCFFE5E3
          F9FFDCD9F7FFDFDCF7FFC9C3E4FFD4D1E3FFF9F9F9FFF9F9F9FFB3A9D9FFD4CF
          EFFF9E98E8FF8E86E4FF7067DDFFA99FD9FFB6AFD1FFEBEBEBFFE8E8E8FFE6E6
          E6FFE3E3E3FFDFDFDFFFD6D6D6FF8C857EFC0000003C00000001000000000000
          000026232054C0BCB8FFF3F3F3FFFFFFFFFFFFFFFFFFFDFDFEFFB6ADDDFFF8F7
          FDFFF0EFFBFFD7D2EBFFD6D3E5FFFBFBFBFFFAFAFAFFF9F9F9FFF9F9F9FFB5AD
          DAFFE0DDF5FFB4AFEDFFD2CDEBFFB9B4D5FFF1F1F1FFEEEEEEFFEBEBEBFFE9E9
          E9FFE6E6E6FFE3E3E3FFD1D1D1FF7F7670F90000002100000000000000000000
          000008070611877D76F7F6F6F6FFF8F8F8FFFFFFFFFFFFFFFFFFFDFDFEFFB9B1
          DEFFC6BFE4FFDFDCEEFFFCFCFCFFFCFCFCFFFBFBFBFFFAFAFAFFF9F9F9FFF9F9
          F9FFB9B1DCFFB6AEDCFFC5C1E0FFF6F6F6FFF4F4F4FFF1F1F1FFEEEEEEFFECEC
          ECFFE8E8E8FFDEDEDEFFB9B6B4FF4D4640BB0000000800000000000000000000
          000000000000423B3690A19A94FFDCD9D7FFF4F4F4FFF1F1F1FFF1F1F1FFF1F1
          F1FFF1F1F1FFF0F0F0FFEFEFEFFFEFEFEFFFEEEEEEFFEEEEEEFFEDEDEDFFEBEB
          EBFFE9E9E9FFE7E7E7FFE5E5E5FFE3E3E3FFE1E1E1FFDFDFDFFFDDDDDDFFDBDB
          DBFFD0CFCFFFA9A5A1FF7F756DFB1C19173E0000000000000000000000000000
          0000000000000000000018161435443D38954C453FA84C453FA84C453FA84C45
          3FA84C453FA84C453FA84C453FA84C453FA84C453FA84C453FA84C453FA84C45
          3FA84C453FA84C453FA84C453FA84C453FA84C453FA84C453FA84C453FA84C45
          3FA84C453FA837322E790A090917000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00030000000C00000012000000230000003E000000490000004D000000500000
          00500000004E0000004A0000004000000028000000140000000D000000050000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000B0000
          001F0000004226160C7B8D6953D7A58068EAB38C75F3BC9880F9C19D85FBB891
          79F5A9846BEC927159DB4F4A4795000000580000005200000045000000240000
          000D000000020000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000002000000111F1B17549F7A
          63E7B08368FDC9ADA1FFE3D8D7FFE8E0E0FFEBE5E5FFEEE8E8FFF0EBEBFFF3EE
          EEFFF2EEEEFFF0E9E9FFD2B9ABFFBE967FFEAA846AEE332C2786000000540000
          003A000000140000000200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000A795E4CB6B58B76FED8C9
          C9FFE2D8D8FFE4DBDBFFE7DEDEFFE9E1E1FFEBE4E4FFEDE7E7FFF0EAEAFFF2ED
          EDFFF4F0F0FFF6F3F3FFF9F6F6FFFBF9F9FFF2EDECFFBB947EFF8C7463D00000
          005B000000400000000D00000000000000000000000000000000000000000000
          000000000000000000000000000000000000917869BCC5A89CFFDED2D2FFE0D5
          D5FFE2D8D8FFE4DBDBFFE7DEDEFFE9E1E1FFEBE4E4FFEDE7E7FFF0EAEAFFF2ED
          EDFFF4F0F0FFF6F3F3FFF9F6F6FFFBF9F9FFFFFFFFFFFFFFFFFFD5BCB0FFA98B
          75E6000000530000001A00000002000000000000000000000000000000000000
          000000000000000000000000000000000005BE9781FEDCCFCFFFDED2D2FFE0D5
          D5FFE3D9D9FFECE6E6FFF2EBEAFFF0E6E0FFEDDED3FFE9D4C1FFE6CBB3FFE7D5
          C6FFE9DBD1FFEBE1DDFFF0EAEAFFF7F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFCBA7
          91FF131312690000003400000009000000000000000000000000000000000000
          0000000000000000000000000000362B234DCAAEA1FFDCCFCFFFE4DADAFFF2EB
          E7FFF0DECAFFE5C4A2FFDCAF80FFDAA976FFDAB692FFD8A570FFD39758FFD397
          58FFD39758FFD39758FFD59E66FFD8B18DFFDEC8B8FFEEE7E7FFFEFEFEFFF5EE
          E9FF6A5545B8000000490000000E000000000000000000000000000000000000
          0000000000000000000000000000977760C6DCD0CEFFF3ECEAFFEBD4BEFFDEBA
          95FFDCB58CFFDAB084FFD9AA7BFFD7A572FFDBCBBEFFF4F1F1FFE1D3C5FFD7AF
          86FFD19456FFD09354FFD09354FFD09354FFD09354FFD29C68FFD7BCA9FFF3EF
          EFFFAA856DF10000005100000010000000000000000000000000000000000000
          0000000000000000000000000000BC997FF0F4ECE5FFEBD3B7FFDFBE9DFFDBB5
          90FFD9B188FFD7AB7FFFD7AE86FFE1D5D0FFEEE8E8FFF3EFEFFFF8F5F5FFFCFB
          FAFFBB956EFFCC8E50FFCC8E50FFCC8E50FFCC8E50FFCD9053FFCB8F53FFC098
          6FFFB99378FD0000005300000013000000010000000000000000000000000000
          0000000000000000000000000002CAA587F9F3E1CAFFEFDABFFFE7CBAAFFDAB4
          8FFFD6AD84FFD5AB83FFDDD2D0FFE7DFDFFFE2DBD9FFF1EDEDFFD8CFC8FFA67E
          56FFC7894BFFC98A4CFFC98A4CFFC98A4CFFC98A4CFFC1874AFFB37D42FFB07C
          3FFFA6754AFE0000005600000020000000040000000000000000000000000000
          00000000000000000000110A0520D3B191FDF4E2CBFFF0DCC3FFE8CDAAFFE3C3
          9DFFDCB78DFFCEB6A5FFE1D7D7FFBFAFA5FFB98E66FFBAA798FFB27842FFC686
          48FFD09F72FFD9C6B3FFD0A377FFBB7E3EFFB07534FFAD7637FFB5834AFFB482
          49FFB7824FFF2B211A7F00000034000000090000000000000000000000000000
          000000000000000000005D432D84ECD7C0FFF4E3CEFFF1DDC4FFE8CCA9FFE3C3
          9DFFDDBC95FFD8CCCBFFD9CDCCFFB48F65FFCA9861FFC69056FFC18646FFBE83
          44FFCAA67EFFFFFFFFFFFCFCFBFFA46F33FFAA7130FFAB7332FFB98954FFCB9C
          68FFDDAC77FF6D513ABA000000440000000D0000000000000000000000000000
          00000000000000000000926D4DC3F7E9D7FFF5E5D1FFF2DDC5FFEACDABFFE3C3
          9DFFD8BB9DFFD9CBCBFFC0B1A9FFCFA26EFFCA9962FFCDA67AFFC5AB91FFBD85
          47FFE7DCD0FFFFFFFFFFFCFBFAFFA26D32FFAA7130FFBB8242FFDDAE7CFFE4B8
          88FFE4B888FF986F4CDE0000004C0000000F0000000000000000000000000000
          00000000000000000001B08967E1F8EBDAFFF6E7D4FFF2DDC3FFEED1AFFFEACA
          A5FFD1B69CFFB5A59EFFA9927AFFCFA26EFFD0B090FFE9E2E1FFDFD7D2FFEBE5
          E0FFFAF8F8FFFFFFFFFFCAC0B4FFC48C4FFFD69E5FFFDDA466FFE3B380FFE6BC
          8FFFE6BC8FFFB4845BF100000052000000140000000100000000000000000000
          00000000000008050211C6A07EF4F8ECDCFFF6E8D7FFF1DBC0FFEED1AFFFEBCB
          A6FFE8C69DFFE8C195FFE5BB8CFFD9BEA7FFE4DBDBFFE9E1E1FFEEE8E8FFF3EF
          EFFFF8F5F5FFDFD8D1FFC39563FFDDA466FFDDA466FFDDA466FFE1B07AFFE8C1
          97FFE8C197FFC69365FD0402015B000000260000000500000000000000000000
          0000000000003D28165CE0C9B0FCF9EDDFFFF7EADAFFF0D8BBFFEED1AFFFEBCB
          A6FFE9C79EFFE8C195FFE5BD91FFC6B5ACFFE1D8D8FFE7E0E0FFECE6E6FFC5B6
          A9FFB79774FFD59E62FFDDA466FFDDA466FFDDA466FFDDA466FFDFAB71FFE9C5
          9EFFE9C59EFFD5A676FF3E3023900000003A0000000A00000000000000000000
          000000000000805B3BAEF7EBDCFFF9EFE1FFF7EBDBFFEFD6B7FFEED1AFFFEBCB
          A6FFE9C79EFFE8C195FFE6BC8CFFE3B683FFC49F76FFBCA693FFDBD1CDFFC499
          6BFFDDA466FFDDA466FFDDA466FFDDA466FFDDA466FFDDA466FFDDA567FFEBC8
          A3FFEBC9A5FFE5C098FF805E3FC8000000470000000E00000000000000000000
          000000000000A77E5AD6FAF0E4FFFAF0E4FFF7E9D9FFEFD6B7FFEED1AFFFEBCB
          A6FFE9C79EFFE8C195FFE6BC8CFFE4B785FFE3B787FFE3B787FFDEB07FFFD6A4
          6EFFDDA467FFDDA466FFDDA466FFDDA466FFDDA466FFDDA466FFDDA466FFE9C5
          9DFFEDCEADFFEDCEACFFA77A4EE60000004E0000001000000000000000000000
          000003020108BF9771EEFAF2E7FFFAF2E7FFF6E7D4FFEED6B9FFEBD4BCFFE8D4
          C3FFE7D9D0FFE8DDD8FFEAE0DEFFECE5E5FFEDE7E7FFEFE9E9FFF1EBEBFFF2EE
          EEFFF4F0F0FFF5EEEBFFF4EAE3FFF2E1D2FFEBCCACFFE4B787FFDDA568FFE7BE
          92FFEED2B4FFEED2B3FFBE8D5EF6000000530000001800000002000000000000
          00002B1D0E43D7BA9EF7FBF3E9FFF2E9E2FFE5D8D4FFE2D7D7FFE3DADAFFE5DC
          DCFFE7DEDEFFE8E0E0FFEAE2E2FFECE5E5FFEDE7E7FFEFE9E9FFF1EBEBFFF2EE
          EEFFF4F0F0FFF6F2F2FFF7F4F4FFF9F6F6FFFBF9F9FFFCFBFBFFFFFEFEFFF5E3
          D0FFF1DABFFFF0D6BBFFCE9D6EFE0D0905650000002B00000007000000000000
          00006B4A2A93EADBCFFDE2D7D5FFDED3D3FFE0D5D5FFE2D7D7FFE3DADAFFE5DC
          DCFFE7DEDEFFE8E0E0FFEAE2E2FFECE5E5FFEDE7E7FFEFE9E9FFF1EBEBFFF2EE
          EEFFF4F0F0FFF6F2F2FFF7F4F4FFF9F6F6FFFBF9F9FFFCFBFBFFFFFFFFFFFFFF
          FFFFFEFDFCFFF7E9D9FFDDB68CFF533F2CA00000003F0000000C000000000000
          00009E754ECEDCD1D0FFDDD1D1FFDED3D3FFE0D5D5FFE2D7D7FFE3DADAFFE5DC
          DCFFEEE5E1FFF0E5DEFFF1E5DBFFF3E6D9FFF3E5D8FFF4E6D8FFF4E6D8FFF3E5
          D7FFF3E6D8FFF4E6DAFFF6EADFFFF8F0EBFFFBF9F9FFFCFBFBFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF1DFCCFF916A43D40000004A0000000E000000000000
          0003BE936CF1DBCFCFFFDDD1D1FFDED3D3FFEDE5E1FFF6EEE8FFFBF5F0FFFBF3
          ECFFF5DFCAFFF3D6BCFFF0D0B3FFECC9AAFFE9C3A1FFE4B893FFDDA87EFFDCAA
          82FFDDAF8CFFDFB597FFE2BEA3FFE7CAB4FFF6ECE3FFF9F1EAFFF8ECE1FFFBF5
          EEFFFFFFFFFFFFFFFFFFFEFEFDFFB38555EE0000005000000011000000001D13
          092DCDAA8CFBDED2D2FFF2ECEBFFFDFBF8FFF6E2CEFFEFC9A6FFEAB88AFFE8B4
          84FFE7B382FFE7B17FFFE5AE7AFFE1A876FFDDA471FFDA9F6DFFD69967FFD292
          60FFC77E44FFC3763EFFC0733CFFBC6F3AFFB86A37FFB86D3DFFC99371FFE0BF
          A9FFFAF3EDFFFBF3ECFFFFFFFFFFCA9D6EFB000000550000001D000000005A3F
          227BDAC5B6FDFEFEFDFFFAEEE3FFEDC49EFFEBBA8DFFEAB88AFFE9B687FFE8B4
          84FFE7B382FFE7B17FFFE5AE7AFFE1A876FFDDA471FFDA9F6DFFD69967FFD495
          65FFCF905FFFC47942FFC0733CFFBC6F3AFFB86A37FFB56735FFB16332FFAE5F
          30FFB67149FFE6CFC2FFFAF5F2FFD9B797FF1E150D7300000033000000009067
          3CBBFAF7F5FEF7E4D5FFEEC5A1FFEEC3A0FFEDC29DFFEBBD93FFEAB88BFFE8B4
          84FFE7B382FFE7B17FFFE5AE7AFFE1A876FFDDA471FFDA9F6DFFD69967FFD495
          65FFD09362FFCB8857FFC0733CFFBC6F3AFFB86A37FFB56735FFB16332FFAD5F
          32FFA85E3BFFA25E45FFE2CFCAFFE1C8B4FF765D46BA0000003500000000A277
          47CCFFFEFEFFF1CDAFFFEEC5A1FFEEC3A0FFEDC29EFFECC29DFFEBC19DFFEBBF
          9BFFE9BB93FFE8B78BFFE6B285FFE1AC7EFFDDA678FFDBA171FFD69A69FFD498
          68FFD09468FFCE9267FFC37E4FFFBC7346FFB7714AFFB36F4DFFAE6D50FFA96A
          53FFA56551FFA0604EFFBC9186FFE8D5C6FF856649C40000001A000000004431
          1B58D2AE86F5FBF3EBFFEEC6A1FFEEC3A0FFEDC29EFFECC29DFFEBC19DFFEBC0
          9CFFEABF9BFFE9BE9AFFE6BB98FFE3B794FFDFB392FFDDAF90FFD8AB8CFFD5A6
          8AFFD1A287FFCE9D84FFC79379FFBA7F62FFB6795FFFB2745BFFAD6F58FFA96A
          54FFA56551FFA36554FFE7D7D2FFC59463FB18110B3900000006000000000000
          00005A422771C39764EDE6D2BBF9F2DCC7FDEDC3A0FFECC29EFFEBC19DFFEBC0
          9CFFEABF9BFFE9BE9AFFE6BB98FFE3B794FFDFB392FFDDAF90FFD8AB8CFFD4A6
          8AFFD1A287FFCE9D84FFC9987FFFBA7F62FFB6795FFFB2745BFFAD6F58FFB27A
          66FFD3B3A8FFDABA9EFEAD845CDE21170C370000000300000000000000000000
          00000000000007050209644B2E7CC69760F0E7CEB1FEEAD4BAFBF0D1B7FEEEC7
          A5FFE9BD98FFE9BE99FFE6BB98FFE3B794FFDFB392FFDDAF90FFD8AB8CFFD4A6
          8AFFD1A287FFCE9D84FFCB9A82FFBE866BFFCB9F8CFFDABBAEFFE7D2C1FFDCBD
          9CFDBF9669E92F21104700000005000000000000000000000000000000000000
          0000000000000000000000000000010100013B2A154A866640A4B68B58DDCDA1
          6CF4DCBA93FBE3C6A4FCEBD0B4FFEDD2B8FFECD2BAFFEDD2BBFFEACEB8FFE9CF
          BAFFE9CFB8FFE8CFB6FFE6C9ACFFDFBA92FFD0A674F7B48E62D96B5234881E15
          0A2D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000021170C29433018556146257B8F6E45ACAB8556CEC19661E6C49761E9B08B
          5CD4997952B77961439255442E691B1309260000000200000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000A000000260000002700000028000000290000002900000029000000290000
          00290000002900000029000000290000002A0000002300000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000101030E1C14
          6DAA261C83C8261C84C9261C84C9261C84C9261D85CA261D85CA261D86CA261D
          86CA261D86CA261D87CB271D87CB231984C90604197700000030000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000101030E2C228DC6B1AB
          EAFFCFCCF2FFCFCCF2FFCFCCF2FFCFCCF2FFCFCCF2FFCFCBF2FFCFCBF2FFCFCB
          F2FFCFCBF2FFCFCBF2FFCFCBF2FFC4C0EFFF4B40BCEF06041977000000310000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000101030E2A208AC5A39CE6FFBAB5
          EDFF8481F5FF7E7CF5FF7E7CF5FF7E7CF5FF7E7CF5FF7E7CF5FF7E7CF5FF7E7C
          F5FF7E7CF5FF7E7CF5FF7E7CF5FFA49FF0FFB5AFEBFF4A3EBAEF07041A770000
          0032000000030000000000000000000000000000000000000000000000000000
          00000000000000000000000000000101030E2A208AC4978EE2FFABA5EAFF6D69
          F3FF5957F7FF5957F7FF5957F7FF5957F7FF5957F7FF5957F7FF5957F7FF5957
          F7FF5957F7FF5957F7FF5957F7FF5B59F6FF9793ECFFA7A0E6FF463AB8EF0704
          1977000000320000000300000000000000000000000000000000000000000000
          000000000000000000000100030E281D87C4897FDDFF9B94E4FF6662F0FF5653
          F3FF5653F3FF5653F3FF5653F3FF5653F3FF5653F3FF5653F3FF5653F3FF5653
          F3FF5653F3FF5653F3FF5653F3FF5653F3FF5855F2FF8B84E7FF9990E2FF4437
          B7EF07041A780000003200000003000000000000000000000000000000000000
          0000000000000100030E261B85C37C71D9FF8B83E0FF5F59ECFF524EF0FF524E
          F0FF514DEFFF4A43E4FF524DEFFF524EF0FF524EF0FF524EF0FF524EF0FF524E
          F0FF524EF0FF514DF0FF4A44E8FF514DEFFF524EF0FF5450EFFF8078E2FF8A80
          DCFF4134B5F007041A7800000032000000030000000000000000000000000000
          00000100030E251A84C36F63D4FF7C73DAFF5952E8FF4F49ECFF4F49ECFF4E47
          EAFF4739CBFF766ACFFF4E41D1FF4E48EBFF4F49ECFF4F49ECFF4F49ECFF4F49
          ECFF4F48EBFF655BDFFFCECAF2FF7B73E3FF4D47EAFF4F49ECFF514BEBFF736A
          DCFF7B70D7FF3E30B2EF07041A78000000330000000300000000000000000100
          030E251983C36356CFFF6E61D4FF524AE4FF4B45E8FF4B45E8FF4A43E6FF3C2C
          BFFF8E84CCFFEAEAEBFF9C93DBFF4D40D0FF4A44E7FF4B45E8FF4B45E8FF4B44
          E7FF5E54DBFFD6D4F3FFF5F5F5FFE8E6F4FF7A72E3FF4A43E6FF4B45E8FF4C46
          E6FF665BD7FF6C5FD1FF3A2BAFEF07041A780000003300000003010103042418
          81C15748CAFF5E51CEFF4C43E0FF4840E5FF4840E5FF473EE3FF3726B8FF7D72
          BFFFE8E8E8FFF0F0F0FFF5F5F6FFA199E0FF4C3FCFFF473FE4FF483FE4FF564A
          D6FFC8C4F0FFFEFEFFFFFBFBFBFFF4F4F4FFE7E6F3FF7A72E2FF473EE4FF4840
          E5FF4841E3FF5A4DD1FF5E50CBFF3728ADEF07041A7800000021110B3E544736
          C3FF5041C9FF463CDBFF443CE1FF443CE1FF433BDEFF3423B2FF69619EFFC7C7
          C7FFEBEBEBFFF3F3F3FFFBFBFBFFFEFEFFFFA39BE1FF4C3FCFFF4E42D1FFB8B2
          EAFFFEFEFFFFFFFFFFFFFFFFFFFFF8F8F8FFF1F1F1FFE4E3F1FF7870E1FF433A
          E0FF443CE1FF443CDFFF4D3FCCFF4F3EC6FF201475C500000027110B40573E2C
          BFFF4131C8FF4137DDFF4137DDFF4137DDFF3929C0FF605696FF787878FF9C9C
          9CFFD5D5D5FFF5F5F5FFFDFDFDFFFFFFFFFFFEFEFFFFBFB9EAFFC3BEECFFFEFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF3F3F3FFEBEBEBFFE1DFEFFF4F45
          DAFF4137DDFF4137DDFF4033D3FF402EBFFF211475C600000028110B3F563320
          BAFF3525C3FF3D32DAFF3D32DAFF3D32DAFF3A2CCCFF3B2993FF8B8896FF8B8B
          8BFFB4B4B4FFEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFFF4F4F4FFE5E5ECFFB5B0EAFF4236
          D8FF3D32DAFF3D32DAFF392BCFFF3320BAFF201475C700000028110B3E55331F
          B9FF3424C2FF3A2ED6FF3A2ED6FF3A2ED6FF3A2ED6FF3728C5FF3A2893FF9997
          A5FFB1B1B1FFD7D7D7FFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFE8E7F1FFA19AE5FF3F34D5FF3A2E
          D6FF3A2ED6FF3A2ED6FF3728CBFF331FB8FF201474C700000029110A3D55331F
          B7FF3322C0FF3629D2FF3629D2FF3629D2FF3629D2FF3629D2FF3425C3FF3B29
          95FFB0ADBFFFD7D7D7FFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E7F5FF8D84DEFF3B2DCFFF3629D2FF3629
          D2FF3629D2FF3629D2FF3525C8FF331FB6FF201473C700000029110A3C54331F
          B5FF3320BDFF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF3325CFFF3222
          C2FF3B2896FFD9D7E2FFF7F7F7FFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF2F1F9FF7A6DD7FF3829CDFF3325CFFF3325CFFF3325
          CFFF3325CFFF3325CFFF3322C5FF331FB5FF201473C800000029110A3B53331E
          B4FF3320BCFF3527CFFF3628CFFF3628CFFF3628CFFF3628CFFF3628CFFF3627
          CEFF2E1BA0FFBBB6D1FFF2F2F2FFF8F8F8FFFCFCFCFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFBFBFBFFD6D3EDFF4C3DCAFF3325CEFF3325CFFF3325CFFF3325
          CFFF3325CFFF3325CFFF3322C5FF331EB3FF201373C800000029110A3A53331E
          B2FF3320BBFF3B2ED1FF4033D2FF4033D2FF4033D2FF4033D2FF4032D1FF3220
          A5FF685E9FFFE4E4E5FFECECECFFF1F1F1FFF5F5F5FFF8F8F8FFF9F9F9FFF9F9
          F9FFF7F7F7FFF4F4F4FFEFEFF0FF968CD7FF4435C9FF3325CEFF3325CFFF3325
          CFFF3325CFFF3325CFFF3322C4FF331EB2FF201372C800000029100A3952331E
          B1FF3320BBFF4235D2FF4B3ED4FF4B3ED4FF4B3ED4FF4B3DD3FF3622A0FF665B
          96FFDAD9DAFFE0E0E0FFE5E5E5FFEAEAEAFFEEEEEEFFF0F0F0FFF1F1F1FFF1F1
          F1FFEFEFEFFFEDEDEDFFE9E9E9FFE3E3E4FF9289D3FF4435C9FF3325CEFF3325
          CFFF3325CFFF3325CFFF3322C4FF331EB0FF201372C900000029100938513E29
          B3FF3C29BDFF493CD4FF5549D7FF5549D7FF5548D6FF37259FFF5E5585FFCDCC
          CDFFDBDBDBFFDBDBDBFFDFDFDFFFE3E3E3FFE6E6E6FFE8E8E8FFE9E9E9FFE9E9
          E9FFE7E7E7FFE5E5E5FFE2E2E2FFDDDDDDFFDADADBFF9188D1FF4435C9FF3325
          CEFF3325CFFF3325CFFF3727C5FF402CB4FF211371CA00000029100937514935
          B6FF4736C1FF5044D5FF6055D9FF6055D9FF4C3EBDFF504873FF8B8B8BFFABAB
          ABFFCFCFCFFFDBDBDBFFDBDBDBFFDBDBDBFFDEDEDEFFDFDFE0FFDCDCDDFFCECE
          CEFFCBCBCBFFD5D5D5FFDBDBDBFFDBDBDBFFDBDBDBFFDADADBFF8D84D0FF3929
          C9FF3325CFFF3325CFFF3D2CC6FF503DB8FF211370CA0000002A100936505442
          B9FF5343C2FF4E42D5FF6B60DCFF6B60DCFF5B4DC9FF362871FF6D6C72FF8C8C
          8CFFACACACFFCFCFCFFFDBDBDBFFDBDBDBFFC1BFCDFF433492FF433296FFA7A4
          B4FFAFAFAFFFB3B3B3FFCACACAFFDBDBDBFFDBDBDBFFCBC9D7FF5E51C3FF3626
          CAFF3325CFFF3325CFFF4434C8FF604EBDFF231470CA000000290B0626394C39
          B3FD7161C4FF5142C9FF584DD7FF756BDEFF756BDEFF5D4EC3FF372972FF6E6D
          72FF8D8D8DFFADADADFFD0D0D0FFC1BECBFF3B2B87FF6053CAFF6255CDFF3B2A
          92FF94919FFF979797FFA7A7A7FFC6C6C6FFC9C8D5FF493AB2FF3424C6FF3325
          CFFF3325CFFF3E2FCDFF6A5AC3FF6352BEFF1C0F5AA80000000900000000130B
          40615744B7FD8173CAFF584ACBFF5F54D9FF8077E1FF8077E1FF6356C4FF372A
          72FF706F74FF8E8E8EFF9E9CA6FF3A2980FF675ACAFF8077E1FF8077E1FF6A5E
          D0FF3B2B91FF7F7D89FF898989FF9E9DA6FF4537A6FF3222C4FF3325CFFF3325
          CFFF4132CDFF796BC8FF7060C2FF261770C00100020C00000000000000000000
          0000130A3F605F4CB9FD9083CFFF5F51CFFF655ADAFF8B83E3FF8B83E3FF6A5E
          C5FF382B72FF6F6D7AFF362875FF6F62CBFF8B83E3FF8B83E3FF8B83E3FF8B83
          E3FF7267D1FF3A2B90FF737083FF433498FF5648C8FF3426CFFF3325CFFF4336
          CEFF887BCEFF7C6DC6FF271871C20100020C0000000000000000000000000000
          000000000000130A3E5F6857BCFEA095D5FF665AD2FF6B61DCFF958EE6FF958E
          E6FF7166C9FF3A288EFF776BCEFF958EE6FF958EE6FF958EE6FF958EE6FF958E
          E6FF958EE6FF7A6FD3FF3F2DA4FF7165CEFF958EE6FF685EDBFF483BCFFF978C
          D4FF887ACAFF291972C30100020D000000000000000000000000000000000000
          00000000000000000000130A3C5E7161BEFEB0A7DCFF6C61D4FF7268DDFF9F99
          E8FF9F99E8FF9F99E8FF9F99E8FF9F99E8FF9F99E8FF9F99E8FF9F99E8FF9F99
          E8FF9F99E8FF9F99E8FF9F99E8FF9F99E8FF8C84E4FF5347D2FFA69CDAFF9488
          CFFF291972C40100030E00000000000000000000000000000000000000000000
          0000000000000000000000000000130A3C5E7969C0FEBFB8E2FF7267D7FF7970
          DFFFABA5EBFFABA5EBFFABA5EBFFABA5EBFFABA5EBFFABA5EBFFABA5EBFFABA5
          EBFFABA5EBFFABA5EBFFABA5EBFF938CE5FF564AD3FFB6ADDFFFA095D3FF2A19
          72C50100030F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000013093B5D8274C4FECFCAE9FF786E
          DAFF7268DDFF847CE2FF857DE2FF867DE2FF867DE2FF867DE2FF877EE2FF877E
          E2FF877FE2FF8880E3FF837AE1FF5B4FD6FFC5BFE6FFABA1D8FF2C1B73C70100
          030F000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001209395C8C7FC8FEDFDB
          F0FFAEA7E6FFA9A2E5FFA8A1E5FFA7A0E5FFA79FE4FFA79FE4FFA69EE4FFA59E
          E4FFA49DE4FFA49DE4FFA39DE4FFD4D0ECFFB7AEDDFF2C1B73C8010103100000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001209395B8A7C
          C6FDB4ABDBFFB5ACDCFFB5ACDCFFB6ADDCFFB7AEDDFFB7AEDDFFB7AFDDFFB8AF
          DDFFB8AFDDFFB9B0DEFFBAB1DEFFAA9FD7FF2C1B73C701010310000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000C06
          253B110936571109365611093656110935551109345411093454110834531008
          33521008335210083351100832501008314F0101040600000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000400000829030319640D0D2D8B1A1A3D9F222248A51A1A
          3C970B0B27780202124800000210000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          031200001C78020538D60B2567FE13328FFF1A39AEFF1C3BBEFF1F3EC5FF203F
          BEFF1F3EA9FF193785FF071451F400002BAB00000E3E00000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000400001A6A050E
          4AEB0F2F8BFF0D29BDFF0309CBFF0000CFFF0000D3FF0000D6FF0000D6FF0000
          D5FF0000D2FF0002CDFF081AC9FF0E2EA8FF0B236DFE000132B5000008250000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000415000133B60D287AFF0B23
          B7FF0001C0FF0000C7FF0000CEFF0000D5FF0000DAFF0001DDFF0001DEFF0001
          DCFF0000D8FF0000D3FF0000CCFF0000C4FF040CBFFF0F2FA0FF051057F00000
          1758000000010000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000061B020542D30F3094FF030AB4FF0000
          BAFF0000C3FF0102CCFF0A20CEFF1E3ABAFF3C58B3FF536EB3FF5B76B4FF526D
          B7FF3753BCFF1935C8FF050FD1FF0000C8FF0000C0FF0000B7FF0B22AFFF091C
          6DFC000020700000000100000000000000000000000000000000000000000000
          0000000000000000000000000310010344CF0F2F98FF0103ACFF0000B4FF0000
          BDFF091CC4FF0D298EFF030A51DF141439871717284C0C0C15290A0A12231414
          1F3725253A652D2D59AF182B79F90D2AAAFF030BC3FF0000B9FF0000B0FF0818
          ABFF091C71FD00001D6000000000000000000000000000000000000000000000
          00000000000000000002000035A40D2B8DFF0103A4FF0000ACFF0000B5FF0000
          BEFF030AC8FF0D2994FF00013BB0000001080000000000000000000000000000
          0000000000000000000000000A1F00013BA50B2283FF0718B9FF0000B2FF0000
          A8FF091CA4FF061167F500000E30000000000000000000000000000000000000
          0000000000000000184D091E79FE040C9EFF0000A3FF0000ACFF0000B6FF0000
          BFFF0000C8FF030AD2FF0C2798FF00013EAF0000010800000000000000000000
          00000000000000000000000000000000000100002360071775F90818B1FF0000
          A9FF0000A0FF0C2699FF01024CC7000001040000000000000000000000000000
          000000000004010451D30B2297FF00009AFF0000A3FF081BB1FF0000B6FF0000
          BFFF0000C8FF0000D1FF0309DAFF0C259CFF000141AE00000208000000000000
          0000000000000000000000000000000000000000000000002158091B80FD040D
          ABFF00009FFF020598FF0A2083FF00001F570000000000000000000000000000
          000000001B4B0A1F81FF010391FF000099FF0716A5FF08197BFF0A22ACFF0000
          BEFF0000C6FF0000CFFF0000D8FF0309E0FF0B239FFF000144AE000002080000
          000000000000000000000000000000000000000000000000000000003B910B23
          96FF00009FFF000095FF0A1F95FF010253C80000000200000000000000000000
          0000000044A80A1F8EFF00008EFF000097FF0A2091FF0000419D030A68E40A20
          B0FF0000C4FF0000CDFF0000D5FF0000DCFF0208E1FF0A21A1FF000147AD0000
          02080000000000000000000000000000000000000000000000000000050F040C
          6FEB0614A1FF000094FF02078EFF071880FE00000E2800000000000000000000
          0105030A6BE906138CFF00008DFF040D9AFF051179F800000714000010270309
          6BE40A1EB4FF0000C9FF0000D0FF0000D7FF0000DCFF0208DFFF0A1FA1FF0001
          4AAC000002080000000000000000000000000000000000000000000000000000
          3D89091D96FF000092FF000089FF091C8AFF00002F6E00000000000000000000
          0D23081884FF010283FF00008AFF081A97FF000153B900000000000000000000
          1127030970E4091CB6FF0000CBFF0000D1FF0000D6FF0000D8FF0207D8FF091D
          A1FF00014DAC0000020700000000000000000000000000000000000000000000
          183708198CFF000190FF000087FF08198BFF00004BA200000000000000000000
          1E46081988FF000080FF000088FF081991FF00003D8300000000000000000000
          000000001126030873E31022BAFF1313CFFF1212D2FF1010D4FF0909D4FF0308
          D0FF081BA0FF000050AB00000207000000000000000000000000000000000000
          0209051084F9020890FF000084FF07168AFF000261C500000000000000000000
          28560F1F8FFF1D1D93FF27279DFF182899FF0000316800000000000000000000
          00000000000000001226232886E44354CDFF3434D6FF3535D8FF3A3AD9FF3E3E
          D9FF4347D7FF2435A7FF000052AA000002070000000000000000000000000000
          0002030878E6040E8FFF000082FF061388FF01036CD400000001000000000000
          29551E2D9BFF4444ADFF4343ADFF21309FFF0000326700000000000000000000
          000000000000000000000A0A18275F63A6E44F5ED1FF3737D3FF3636D3FF3535
          D2FF3434D0FF4347D1FF2434A7FF000055AA0000020700000000000000000000
          010303087CE6060F8CFF000080FF051188FF010370D300000001000000000000
          21431C2A9CFF4B4BB1FF4A4AB1FF2A38A5FF0000428200000000000000000000
          00000000000000000000000000000E0E1A27777BB4E45967D3FF3D3DD0FF3C3C
          CFFF3B3BCEFF3A3ACBFF4448CBFF2331A8FF000058A900000206000000000000
          030B060F8FF9474BADFF4444A4FF1E2B97FF000169C200000000000000000000
          0E1F131F9AFF5253B6FF5151B5FF3C49AFFF000161B600000000000000000000
          00000000000000000000000000000000000012121C27898EBDE55F6CD2FF4343
          CDFF4242CBFF4141C9FF4040C6FF4649C5FF212FA7FF00005AA8000002060000
          1E3B111D9BFF3839A5FF3737A4FF4E5AB5FF0000579E00000000000000000000
          0103040983E4555DBBFF5858BAFF5459B9FF070D8FF500000813000000000000
          0000000000000000000000000000000000000000000013131D278A8EBFE5606C
          D0FF4949CAFF4848C7FF4747C5FF4646C1FF484BC0FF202CA7FF00005CA70000
          51942530A5FF4040AAFF3F3FA9FF3C47B0FF0000396700000000000000000000
          000000005A9F4550B6FF5F5FBFFF5E5EBEFF2934AAFF0000447B000000000000
          000000000000000000000000000000000000000000000000000012121D27787B
          B8E45E68CBFF4F4FC6FF4E4EC4FF4D4DC1FF4C4CBEFF4C4EBBFF1E29A7FF0408
          99FD4148AFFF4646AEFF585AB7FF141CA1FD0000112100000000000000000000
          000000002440242DAAFF6667C3FF6565C3FF5960BFFF070A93F1000011220000
          0000000000000000000000000000000000000000000000000000000000000E0E
          1B265E61AFE3565FC5FF5555C4FF5454C1FF5353BEFF5252BAFF5052B7FF3945
          B0FF4F4FB4FF4D4DB3FF525BBAFF000173BF0000000100000000000000000000
          00000000000102027AC95E66C3FF6C6CC7FF6B6BC7FF424BB8FF00017ACA0000
          070F000000000000000000000000000000000000000000000000000000000000
          000008081925151895E2434BBBFF5B5BC1FF5A5ABEFF5959BBFF5858BAFF5757
          B9FF5656B8FF5B5CBBFF252CAEFF00002C4A0000000000000000000000000000
          0000000000000000253E2228ADFE7476CDFF7272CBFF7171CBFF363EB6FF0001
          79C500000D190000000000000000000000000000000000000000000000000000
          00000000000000001624040591E1454CBBFF6161C0FF6060BFFF5F5FBFFF5E5E
          BEFF5D5DBDFF5057BEFF000176BA000000010000000000000000000000000000
          0000000000000000000000005C92484FBFFF797AD0FF7878CFFF7777CFFF434A
          BDFF040594E60000395F00000206000000000000000000000000000000000000
          0000000000000000000100002C4C0305A7FD4E54BEFF6767C4FF6666C3FF6565
          C3FF6569C5FF0B0DA0EF00001523000000000000000000000000000000000000
          000000000000000000000000040802027EC05E64C9FF7F80D4FF7E7ED3FF7D7D
          D3FF676BCBFF2529B5FF010290DD0000588A0000304F00001A2E000017290000
          243D0000446D000077B80B0EAAFA3F43BCFF6D6EC9FF6E6EC9FF6D6DC8FF6E71
          CAFF191BAFFA0000314C00000000000000000000000000000000000000000000
          000000000000000000000000000000000A10020283C45B5FCAFF8889D9FF8484
          D7FF8383D7FF8282D6FF6F72CFFF474AC3FF2B2EBAFF1D1FB6FF191CB5FF2224
          B7FF3639BDFF5659C6FF7577CFFF7777CFFF7676CEFF7575CDFF7274CEFF171A
          B2F900003B590000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000060A00006DA03A3CC2FF8A8C
          DBFF8A8ADBFF8989DBFF8888DAFF8787D9FF8686D8FF8485D8FF8383D7FF8282
          D6FF8181D6FF8080D5FF7F7FD4FF7E7ED4FF8586D5FF5C5ECBFF090AA2E60000
          2C42000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000037510A0A
          9FDF5354CCFF8F8FDEFF9494E0FF8E8EDEFF8D8DDDFF8C8CDCFF8B8BDCFF8989
          DBFF8888DAFF8888DAFF9596DEFF7878D5FF2324BDFC00006F9E00000E150000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          04060000436014148FC25E5ECFFC9292E0FFA8A8E6FFB5B5EAFFBCBCECFFBEBE
          ECFFB5B5E9FF9F9FE3FF5C5CC1E92020729303031C2700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020D1317173C4C37376173575779876868828E5E5E
          76813A3A55611515283100000203000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000100000004000000080000000C0000001000000013000000130000
          00110000000D0000000900000005000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0006000000100000001D0000002B000000380000074800001A5D00001D600000
          12530000013B0000002F00000021000000130000000800000003000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000005000000100000
          0024000008430000498A000085C50000AFED0000C2FF0000C0FF0000C0FF0000
          C1FF0000BDFA00009CDA000069A8000023600000002A00000015000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000002000000080000001B000009410000
          66A40000BFF40000C9FF0000C3FF0000BCFF0000B7FF0000B4FF0000B4FF0000
          B6FF0000BAFF0000BFFF0000C7FF0000CCFF00009BD200002F68000000220000
          000C000000020000000000000000000000000000000000000000000000000000
          00000000000000000000000000020000000A00000023000033710000B5EB0000
          C8FF0000C0FF0000B6FF0000AFFF0000A9FF0000A6FF0000A4FF0000A4FF0000
          A4FF0000A7FF0000ABFF0000B3FF0000BBFF0000C5FF0000CCFF00007AB30000
          08340000000F0000000300000000000000000000000000000000000000000000
          000000000000000000010000000900000024000052910000C5FD0000C0FF0000
          B6FF0000ADFF0000A6FF0002A3FF0007A2FF000FA4FF0014A6FF0016A7FF0016
          A7FF0010A5FF0007A3FF0001A4FF0000A9FF0000B2FF0000BCFF0000C5FF0000
          A2DB000013400000000E00000002000000000000000000000000000000000000
          000000000001000000060000001F000054920000C3FF0000BBFF0000B2FF0000
          A9FF0002A5FF000CA6FF001CABFF0027AFFF0031B4FF0039B7FF003FBAFF0043
          BCFF0043BCFF003DBAFF0029B2FF000FA9FF0001A7FF0000AEFF0000B6FF0000
          C0FF0000A5E1000010390000000A000000010000000000000000000000000000
          00000000000300000015000036710000BDFE0000B9FF0000B0FF0000A9FF0006
          A7FF0018ACFF0026B1FF0033B7FF003CBBFF0044BFFF004BC2FF0051C4FF0055
          C6FF0056C7FF0057C7FF0055C6FF004BC2FF0026B4FF0006AAFF0000ACFF0000
          B4FF0000BCFF000090CF00000321000000050000000000000000000000000000
          00010000000A00000E3B0000AAEF0000B7FF0000B1FF0000ACFF0009ABFF001D
          AEFF062EB1FF1242B9FF0040BBFF004BC3FF0055C9FF005CCCFF0062CFFF0066
          D0FF0067D1FF0064CEFF005EC7FF1868CAFF0052C3FF0034BAFF0009ADFF0000
          AEFF0000B4FF0000B9FF00005A93000000100000000200000000000000000000
          000300000018000069AC0000B5FF0000B2FF0000AEFF0008ADFF001CAEFF062B
          ABFFAFBDE4FFD1DCF1FF1454BAFF0054C2FF0064D0FF006DD6FF0073D8FF0076
          DAFF0072D4FF0066C7FF679FDAFFF3F7FCFF407DCCFF004EBEFF0030BBFF0005
          AEFF0000B0FF0000B4FF0000B0F9000014380000000600000000000000000000
          00070000113C0000ADFB0000B1FF0000B0FF0003B0FF0019B0FF0527A4FFAFBB
          E1FFFFFFFFFFFFFFFFFFD1DEF0FF145EB9FF0066C8FF007BDBFF0083E1FF007E
          D9FF006AC4FF67A0D5FFFEFEFFFFFFFFFFFFF4F7FCFF3F74C3FF0043B7FF001D
          B7FF0001B0FF0000B1FF0000B1FF000065A30000000D00000001000000010000
          000E000052920000AEFF0000B1FF0000B3FF0011B8FF0026B3FF3552AFFFF7F8
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFD1DFEFFF1464B5FF0073CBFF0083D9FF006C
          BFFF679ED0FFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFACBFE0FF0041B0FF003B
          C4FF0009B6FF0000B2FF0000B0FF0000A2F30000061E00000002000000020000
          0016000086D50000ACFF0000B2FF0003B7FF0023C1FF0034C3FF0034ACFF4867
          B1FFF7F8FBFFFFFFFFFFFFFFFFFFFFFFFFFFD1DFEEFF1463ADFF005DABFF6799
          C8FFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7C6E1FF0845A8FF004CC3FF0046
          CCFF001ABFFF0000B5FF0000AFFF0000AAFF0000325E00000004000000040000
          07260000A3FC0000ACFF0000B5FF000CBDFF0030C9FF003FCEFF0049CAFF003F
          ACFF4868ABFFF7F8FBFFFFFFFFFFFFFFFFFFFFFFFFFFD1DDEBFF769AC3FFFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFB7C4DDFF0843A0FF0052C4FF0052D2FF0045
          D0FF0027C7FF0001B9FF0000B1FF0000A8FF0000589400000007000000040000
          224B0000A3FF0000ACFF0000B7FF0015C3FF0037CDFF0047D2FF0055D6FF005B
          D1FF0047AAFF4865A3FFF7F8FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFB7C1D8FF083D96FF0056C2FF005AD6FF004ED4FF003E
          D0FF0029C9FF0005BCFF0000B2FF0000A7FF000073BC00000009000000050000
          2F5E0000A0FF0000ABFF0001B7FF001EC5FF003CCFFF004BD3FF0059D8FF0067
          DCFF006AD4FF0049A4FF485D97FFF7F8FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BCD3FF083387FF0056BFFF005FD7FF0054D6FF0046D2FF0036
          CDFF0026C8FF0006BCFF0000B2FF0000A4FF00007ED00000000A000000040000
          336400009CFF0000A8FF0001B7FF0020C6FF003ECFFF004DD4FF005AD8FF0067
          DCFF0072DFFF0061C2FF002274FF959ABBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF7F8FAFF1D2A71FF003895FF005CD0FF0057D7FF004AD3FF003CCFFF002D
          CAFF001DC5FF0004BCFF0000B0FF0000A1FF000080D700000009000000040000
          2B58000098FF0000A5FF0000B5FF001EC5FF003ED0FF004CD3FF0059D8FF0064
          DBFF0061CDFF003C96FF6879AAFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD2D5E3FF153184FF003DACFF0048CDFF003ED0FF0030CBFF0022
          C7FF0012C2FF0003BAFF0000AEFF00009DFF000076CC00000007000000020000
          1D3F000095FF0000A2FF0000B3FF0017C2FF003BCFFF0048D3FF0456D6FF0A5C
          CDFF0942A2FF6B80B3FFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD2D6E6FF1B358FFF0B36B0FF0836C8FF0124C8FF0016
          C3FF0007BFFF0000B8FF0000ABFF000099FF000066B500000005000000010000
          041400008EFA00009EFF0000B0FF000CBEFF0438CDFF1955D5FF215DCDFF1746
          A8FF7186BCFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFBCC6DEFF5A73AEFFF8F9
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFD4D7E8FF253899FF1C34B3FF2032C7FF0E16
          C3FF0001BDFF0000B6FF0000A7FF000095FF0000498600000003000000000000
          000800006BC5000098FF0000AAFF070ABBFF3251D2FF325ED0FF264CB1FF788C
          C3FFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFC0CBE4FF2851A8FF2450ADFF5E76
          B8FFF8F9FCFFFFFFFFFFFFFFFFFFFFFFFFFFD5D8EAFF323AA3FF2C31B7FF3536
          C9FF1D1DC3FF0000B2FF0000A0FF000090FF0000254800000001000000000000
          000300003B74000091FF0000A3FF0203B4FF4452D1FF405EC9FF5E74BFFFFEFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CFE8FF395DB6FF3F6CCDFF4370D4FF385B
          BEFF6A7CC2FFF9F9FCFFFFFFFFFFFFFFFFFFFFFFFFFFC4C4E3FF3535AEFF4545
          CBFF2323C2FF0000ACFF000098FF00007FE90000020900000000000000000000
          000100000919000084F300009AFF0000ACFF393CC8FF5466D2FF4860C1FFADB7
          E1FFFFFFFFFFFFFFFFFFCBD3ECFF4B69C2FF5173D2FF577BDDFF587ADDFF5571
          D7FF4A5FC4FF7881CAFFF9FAFDFFFFFFFFFFEDEDF8FF6060BEFF4D4DC3FF5757
          D1FF0E0EB7FF0000A3FF000090FF000047860000000200000000000000000000
          00000000000300004686000090FF0000A1FF1212B6FF696DD7FF6675D6FF5E70
          CDFFB8C0E8FFD2D7F1FF6176CDFF647BD6FF6980DEFF6A7FDFFF6A7CDEFF6A79
          DDFF6771D8FF5F64CBFF898AD4FFEBEBF8FF7474CDFF6161CCFF6868D6FF4646
          CAFF0000AAFF000098FF000081EE00000A160000000000000000000000000000
          00000000000100000510000077DC000095FF0000A6FF4242C6FF7C7EDCFF7982
          DCFF7682DAFF7984DAFF7885DDFF7B87E0FF7C87E1FF7C85E0FF7C82DFFF7C7F
          DFFF7C7DDEFF7A7ADBFF7676D6FF7777D5FF7676D7FF7B7BDBFF7272D7FF0D0D
          B1FF00009DFF00008EFF00003768000000010000000000000000000000000000
          0000000000000000000100001B37000087F6000097FF0303A8FF6666D0FF8C8E
          E0FF8C90E2FF8C90E2FF8C90E3FF8C90E3FF8C8FE2FF8C8EE2FF8C8DE2FF8C8D
          E2FF8C8CE2FF8C8CE2FF8C8CE1FF8B8BE0FF8C8CE0FF8888DDFF2626B9FF0000
          9FFF000091FF00005DAB00000002000000000000000000000000000000000000
          000000000000000000000000000200002B5400008AFB000097FF0505A6FF6C6C
          D1FF9E9EE4FF9E9EE5FF9E9EE6FF9E9EE6FF9E9EE6FF9E9EE6FF9E9EE6FF9E9E
          E6FF9E9EE6FF9E9EE6FF9E9EE6FF9E9EE5FF9595E0FF2C2CB8FF00009EFF0000
          90FF000069C20000050C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000020000284E000084F2000093FF0101
          9FFF4747C0FFA2A2E2FFB0B0E8FFB0B0E9FFB0B0EAFFB0B0EAFFB0B0EAFFB0B0
          EAFFB0B0EAFFB0B0E9FFB0B0E8FF7D7DD5FF1515ABFF000098FF00008EFF0000
          60B20000060D0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000152900006CC70000
          8EFF000096FF0808A2FF4D4DC0FF8888D6FFB6B6E8FFC1C1ECFFC1C1ECFFBFBF
          EBFFA2A2E1FF6D6DCCFF2525AFFF00009AFF000092FF000086F600003E750000
          0103000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000001030000
          315C000072D300008DFF000093FF000099FF00009DFF0909A2FF1111A5FF0202
          9EFF00009BFF000096FF000090FF000087F60000539B0000101F000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000200001E3800004A8600006BBE000085E7000093FB000094FD0000
          8DF300007AD500005DA60000356000000A130000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000002000000020000
          0002000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000030000000A000000140000001F0000
          002B00000033000000370000003A0000003C0000003D0000003C000000390000
          00330000002D00000027000000210000001C000000160000000F0000000A0000
          0005000000020000000100000000000000000000000000000000000000000000
          00000000000B000000230000003F00000055000000690808088B545454E00505
          05880000007900000073000000680000005F0000005700000055000000550000
          00510000004B000000450000003F00000039000000340000002E000000280000
          00220000001C000000170000000E000000050000000100000000000000000000
          001300000026000000380000004900000057070707749F9F9FEFDDDDDDFF6969
          69E6050505170000000000000000000000000000000000000000000000000000
          000100000005000000080000000C00000013000000190000001D000000200000
          0021000000210000001B00000016000000100000000A00000002000000000000
          000500000014000000200000002C0808084DA0A0A0EBDFDFDFFFDEDEDEFFDDDD
          DDFF656565E70505051400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000300000005000000050000000400000001000000000000
          0000000000000000000008080827A2A2A2E9E1E1E1FFE0E0E0FFE0E0E0FFDFDF
          DFFFDEDEDEFF5F5F60EA07041D560000001700000017000000130000000F0000
          000A000000060000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000008080822A4A4A4E8E4E4E4FFE3E3E3FFE2E2E2FFE1E1E1FFE1E1
          E1FFD3D2D6FF272374F70705A3F1060324510000000800000009000000080000
          0008000000070000000500000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000008080823A5A5A5E9E7E7E7FFE6E6E6FFE5E5E5FFE4E4E4FFE3E3E3FFD7D7
          DBFF262478F70303BFFF0303BFFF0705A4F10603244B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000808
          0824A7A7A7E9E9E9E9FFE8E8E8FFE8E8E8FFE7E7E7FFE6E6E6FFE0DFDFFF7471
          84F30505B3EF0303BFFF0303BFFF0303BFFF0705A5F10503254A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007171
          71C0EDEDEDFFEBEBEBFFEAEAEAFFE9E9E9FFE9E9E9FFE4E3E3FF949291F5DFDF
          DFFF9292B6D80404B3EF0303BFFF0303BFFF0303BFFF0604A7F1050325480000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000505
          0509868686EDEEEEEEFFECECECFFEBEBEBFFE5E5E8FF7F7E91F3DFDFDFFFDFDF
          DFFFDFDFDFFF9192B6D80304B3EF0203BFFF0203BFFF0203BFFF0504A8F10403
          2647000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000050505087F7F7FEEEEEEEEFFE8E8EBFF33338CF40D0DB6F09393B7D8DFDF
          DFFFDFDFDFFFCDCDDAFF9192B8D80304B6EF0203C2FF0203C2FF0203C2FF0504
          ACF1030227450000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000005050508757578EF393993F61616C4FF1313C3FF1212B8F09696
          B9D9C9C9D9FF6B6BBAFFB0B0D1FF9395BAD80304B9EF0203C5FF0203C5FF0203
          C5FF0404B0F10302284400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000909252F2626BEF11D1DC6FF1B1BC5FF1919C5FF1717
          B8F09595BBDAB1B1D2FFCDCDDCFFA9A9D0FF9597BCD90305BBEF0204C8FF0204
          C8FF0204C8FF0405B5F102022942000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000A0A2C392B2BBFF12222C7FF2020C6FF1E1E
          C6FF1C1CBBF19B9BBDDA8F8FC7FF8888C5FFB6B6D5FF9797BED90709BFEF0205
          CBFF0104CBFF0104CBFF0305B8F102022A400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B0B2D3A3030C0F12828C8FF2626
          C8FF2424C7FF2121BCF19B9BBEDBA8A8D1FFD2D2DFFFB3B3D5FF9B9CC1DA0C0F
          C5F0070BD0FF0206CFFF0105CFFF0206BCF002022B3F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000C0C2E3A3434C3F22D2D
          CAFF2B2BC9FF2929C9FF2626BEF2A0A0C1DBB4B4D6FF7D7DC3FFA5A5D0FF9D9F
          C4DB1014C8F00B0FD4FF070BD3FF0307D2FF0205C1F001022C3D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000E0E303C3939
          C3F23131CBFF2F2FCAFF2D2DCAFF2C2CC0F29F9FC2DDCDCDE0FFE2E2E7FFBBBB
          DAFFA0A1C6DC151ACBF01015D7FF0B10D7FF070CD6FF0307C5F001012D3C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000E0F
          323D3E3FC6F23636CEFF3434CDFF3232CDFF3031C2F3A5A6C5DD9595CCFF9999
          CDFFCECEE1FFA2A3C8DC191FD1F11319DBFF0F15DAFF0A10DAFF1A1DCCF10101
          2E39000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001011343F4343C9F33C3CD1FF3A3AD1FF3737D0FF3536C6F3A2A3C7DED3D3
          E3FF6C6CBEFFB6B6D9FFA6A6CCDD1D24D3F1181EDEFF1319DEFF1D24DEFF1C21
          D2F2010330390000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001112353F4748CEF34142D6FF3F40D5FF3D3ED5FF3A3CCBF3A6A7
          CADFB9B9DAFFD9D9E7FFAEAED7FFA8A9CEDE2229D7F11C22E2FF171DE1FF2328
          E1FF2024D6F21011394300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000121337404C4ED1F34547D9FF4345D8FF4143D8FF3E40
          CFF4AAAACCE09E9ED1FF8E8ECCFFB9B9DCFFAAABCEDE262CD9F12127E5FF1D23
          E4FF2E34D0EC5B5B68CA00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000131438415053D4F44B4EDCFF494CDCFF4649
          DBFF4346D2F5AAABCEE0A1A1D3FFF2F2F2FFF1F1F1FFADAED1DF2C32DAF22E33
          C7E3A8A8B3FD5E5E5EC200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000014153A425558D7F45052DFFF4E50
          DFFF4B4EDEFF484BD5F5B0B1D0DFF2F2F2FFF2F2F2FFF2F2F2FFA2A3C2D89494
          9EFBABABABFF3737377A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000016173D445B5EDBF45558
          E3FF5356E3FF5154E2FF4D51D9F5B2B3D2E0F3F3F3FFD7D7D7E79E9E9EFBAAAA
          AAFE383838920000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000017173D445F63
          DFF5595DE6FF575BE5FF5458E5FF5356DCF5B3B4CEDF9F9F9FFBAAAAAAFE3636
          368A000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001819
          3F456368E0F55F63E8FF5E62E8FF8689DBEE91919EFAA9A9A9FD333333820000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000191A41466A6DE3F5A4A6E8F78C8C99FBA9A9A9FC3030307A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000037375667454553D0464646C32828286800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000100000003000000070000000D00000011000000160000001B000000210000
          00270000002E000000340000003B00000041000000480000004D0000004E0000
          004B000000460000004000000039000000320000002C00000026000000200000
          001A00000015000000100000000A000000050000000200000000000000000000
          00000000000000000003000000080000000D0000001100000016000000190000
          001D000000210000002400000026000000280000002A0000002B0000002B0000
          002B0000002A000000280000002600000024000000210000001D000000190000
          0016000000110000000D00000008000000030000000000000000000000000403
          0312070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          05200706052007060520070605200706052004040314000000001A161377372E
          29FC382F29FF382F29FF39302AFF3C332DFF403731FF443B36FF473E39FF4C44
          3EFF514943FF554E48FF58514CFF5C544FFF5F5853FF615B56FF635C57FF655E
          59FF665F5AFF665F5AFF665F5AFF655F5AFF645E59FF645E58FF635D57FF625A
          56FF615A56FF605853FF5E5752FF605954FF5A524DFD26221E90362F29E1615A
          55FF9F9B98FFA09B99FFA19C99FFA29E9BFFA29E9BFFA39F9CFFA4A09EFFA6A2
          9FFFA8A3A1FFA9A5A2FFABA7A5FFACA8A5FFADA9A6FFAFABA8FFB0ACA9FFB0AD
          AAFFB1AEABFFB2AFACFFB2AFADFFB3B0ADFFB4B0AEFFB5B1AFFFB5B1AFFFB5B2
          AFFFB6B2B0FFB6B3B0FFB6B3B1FFB7B4B1FF8D8884FF524A45F9453D39EAB3AF
          ADFFDEDDDCFFE0DEDDFFE1E0DFFFE3E2E1FFE4E3E2FFE5E4E3FFE7E6E5FFE9E8
          E7FFEAE9E8FFEBEBEAFFECECEBFFEDEDECFFEEEEEDFFEEEEEDFFEFEFEEFFF0EF
          EEFFF0EFEFFFEFEFEEFFEFEFEEFFEFEEEEFFEEEDEDFFEDEDECFFEDECECFFECEB
          EBFFEBEAE9FFEBEAE9FFEAE9E8FFE9E8E7FFD0CDCCFF57504BFF453D39EAB5B2
          AFFFE0DEDDFFDDD0C4FFD5AB88FFD5AC8AFFD6AD8BFFD8B08FFFD9B190FFDAB2
          93FFDBB494FFDAB697FFDCB799FFDEB99CFFDDBB9EFFDEBC9FFFE0BEA0FFDFBE
          A3FFDEBFA4FFE1C1A7FFE1C3A8FFE0C3AAFFE0C4ABFFE0C5ACFFE0C3ADFFE0C4
          AEFFE2C7B1FFE1C8B2FFE2CEBFFFEAE9E8FFD2D0CEFF58504BFF453D39EAB6B2
          B0FFE1DFDEFFDDCCBCFFD19F74FFD3A175FFD4A177FFD3A379FFD4A47CFFD6A5
          7CFFD5A67FFFD7A881FFD6A983FFD8AB85FFD9AB87FFD9AF89FFDAAE8AFFDBAF
          8DFFDBB290FFDCB392FFDCB593FFDCB596FFDDB797FFDBB799FFDCB99BFFDDB9
          9CFFDDBA9EFFDDBB9FFFE0C6B1FFEBEAEAFFD2D0CFFF58514CFF453D39EAB6B3
          B1FFE1E0DFFFDDCCC0FFD3A47BFFD3A47DFFD4A57DFFD5A880FFD5A881FFD7AA
          84FFD8AA86FFD8AD87FFDBAF8BFFD9AE8BFFDBB18FFFDDB491FFDDB492FFDEB7
          94FFDEB796FFDFB898FFDEB89AFFDFBB9CFFE0BB9EFFDEBB9EFFE0BEA0FFDEBD
          A0FFDFBDA2FFDFC0A5FFDFC8B5FFEDECEBFFD3D1D0FF59514CFF453D39EAB6B3
          B1FFE1E0DFFFE4E2E2FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF1F1F0FFF4F3F3FFF5F5F5FFF8F7F7FFF9F9F9FFFBFBFAFFFCFCFCFFFDFD
          FDFFFCFCFCFFFBFBFBFFFAF9F9FFF8F7F7FFF7F6F6FFF5F4F4FFF4F3F3FFF2F1
          F1FFF1F0F0FFEFEEEEFFEEEDECFFEDECEBFFD3D1D0FF59514CFF453D39EAB6B2
          B0FFE1E0DFFFE3E2E1FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF2F1F0FFF4F3F3FFF5F5F5FFF7F7F6FFF9F9F9FFFBFBFAFFFCFCFCFFFDFD
          FDFFFCFCFCFFFBFBFBFFF9F9F9FFF8F7F7FFF6F6F5FFF5F4F4FFF3F2F2FFF2F1
          F1FFF0F0EFFFEFEEEEFFEEEDECFFECEBEBFFD3D1D0FF59524DFF453D39EAB5B2
          AFFFE1DFDEFFDDCEC1FFD4A57EFFD3A580FFD5A781FFD6A884FFD6A984FFD8AD
          88FFD9AE8AFFD8AD8AFFDAB08EFFDCB290FFDCB492FFDEB594FFDEB695FFDEB7
          97FFDEB898FFDFBA9BFFDFBB9CFFDFBB9DFFDDB99DFFDDBB9FFFDEBB9FFFDEBD
          A0FFDEBDA2FFDFBEA4FFE0C8B4FFECEBEAFFD3D1D0FF59524DFF453D39EAB4B1
          AEFFDFDEDDFFDCC6B4FFD0996CFFD19A6CFFD19B6DFFD19B6EFFD39D70FFD29E
          72FFD49E74FFD49F74FFD3A075FFD4A277FFD6A279FFD5A47AFFD6A57DFFD6A7
          7EFFD6A67FFFD8A881FFD6A983FFD8AA84FFD9AC85FFD7AC87FFD9AE8AFFD8AD
          8AFFD8AE8CFFD9B08EFFDBBCA0FFEAEAE9FFD2D0CFFF5A524DFF453D39EAB3AF
          ADFFDDDBDAFFDBC7B8FFD09C70FFD29C71FFD29F73FFD19F74FFD4A076FFD2A0
          76FFD3A278FFD5A37AFFD5A47BFFD6A67EFFD6A57EFFD7A881FFD7AA83FFD7AA
          84FFD9AB85FFD7AB86FFD9AD87FFD9AE8AFFD9AD8BFFDAAF8DFFD9B08EFFD8B1
          90FFD9B08FFFD9B291FFDDBEA4FFE9E8E7FFD1CFCDFF5A534EFF453D39EAB1AD
          ABFFDAD9D8FFDCDAD9FFDDDCDBFFDFDEDDFFE1DFDEFFE3E1E0FFE4E3E2FFE6E5
          E4FFE7E6E5FFE9E8E7FFEAE9E8FFEBEAE9FFECEBEAFFECEBEBFFEDECECFFEDEC
          ECFFEDEDECFFEDECECFFEDECEBFFEDECEBFFECEBEAFFEBEAEAFFEBEAEAFFEAE9
          E8FFE9E8E7FFE8E7E6FFE7E6E6FFE6E5E5FFCFCDCCFF5A534EFF453D39EAAFAC
          A9FFD8D6D5FFDAD8D7FFDAD9D8FFDCDAD9FFDEDCDBFFE0DEDDFFE1DFDEFFE3E1
          E0FFE4E2E2FFE5E3E3FFE6E5E4FFE7E6E5FFE8E7E6FFE9E8E7FFE9E8E7FFE9E8
          E8FFE9E8E8FFE9E8E7FFE9E8E8FFE9E8E7FFE9E8E7FFE8E7E7FFE8E7E6FFE7E6
          E5FFE7E6E5FFE6E5E4FFE5E4E3FFE4E3E2FFCECCCAFF5A534EFF453D39EAADA9
          A7FFD5D3D2FFD4C2B3FFD19E73FFD09D74FFD1A075FFD2A077FFD2A178FFD3A2
          7BFFD2A27BFFD2A37BFFD3A57EFFD3A57FFFD3A680FFD5A883FFD5A783FFD4AA
          84FFD7AA85FFD6AB86FFD6AB89FFD7AE8BFFD8B18FFFD8B190FFD7B290FFD8B1
          91FFD9B494FFD9B596FFDBC0A7FFE4E3E2FFCECCCBFF5B534EFF453D39EAABA7
          A4FFD1CFCDFFD2BAA7FFD09564FFCF9665FFCF9667FFCF9566FFCF9667FFCF97
          68FFD09869FFD0976AFFD0996BFFD0996BFFD09B6CFFD19B6DFFD29B6FFFD19D
          70FFD19C70FFD29D72FFD19E73FFD29F74FFD3A279FFD4A47EFFD5A77FFFD5A7
          81FFD7AA86FFD8AA87FFD9B699FFE5E4E3FFD0CECDFF5B544FFF453D39EAA8A4
          A1FFCECCCAFFD0B8A6FFD09769FFCE9669FFCF976BFFD09A6CFFD09A6DFFD19C
          6FFFD09A6DFFD19A6FFFD09B6FFFD19C70FFD29C71FFD09D72FFD19F73FFD29E
          74FFD29F77FFD2A278FFD2A277FFD3A279FFD3A47AFFD5A983FFD6AB87FFD7AF
          8CFFDBB191FFD8B190FFDCBCA2FFE6E5E4FFD3D1D0FF5C544FFF453D39EABAB6
          B4FFD8D7D6FFD8D6D5FFD6D5D3FFD8D5D4FFDAD8D7FFDBDAD8FFDDDBDAFFDEDC
          DBFFDEDDDBFFD8D6D5FFD6D4D3FFD7D5D4FFD8D6D5FFD8D7D5FFD9D7D6FFD9D7
          D6FFDAD8D7FFDAD9D7FFDBD9D8FFDBD9D8FFDBD9D8FFDDDBDAFFE3E2E1FFE7E6
          E6FFE8E8E8FFE9E7E7FFE8E7E6FFE7E6E5FFD5D4D3FF5C544FFF453D39EABEBB
          B9FFDDDBDBFFDFDEDDFFE0DFDEFFE0DEDEFFDEDDDBFFDDDBDAFFDCDAD9FFDDDC
          DAFFDEDDDCFFE0DEDDFFDAD8D7FFD3D1D0FFD4D2D1FFD4D2D1FFD6D4D3FFD6D4
          D3FFD6D4D3FFD6D4D3FFD7D5D4FFD7D6D4FFD7D5D4FFD7D5D4FFDDDCDBFFE6E4
          E4FFEBE9E9FFEAE9E8FFE9E8E8FFE8E7E6FFD8D6D6FF5C5550FF453D39EAC2BF
          BDFFDFDEDDFFE0D0C4FFD7AA83FFD9AB85FFD9AE88FFD8AD89FFDAAE8BFFDAAC
          89FFD7AB86FFD6A983FFD8AA85FFD5A881FFD09E77FFD09F75FFD09E75FFCFA0
          77FFD09F77FFD09F77FFCFA079FFD1A27BFFD0A27BFFD0A37DFFD0A37DFFD5AC
          8AFFDDBB9CFFDEBC9FFFE1C6B0FFEAE8E8FFDBD9D8FF5C5550FF453D39EAC6C3
          C1FFE1E0DFFFE1CDBEFFD49C6FFFD39D70FFD59F72FFD4A073FFD5A176FFD6A0
          76FFD5A378FFD6A379FFD6A277FFD39F73FFD5A075FFD29A6DFFCF9666FFD097
          67FFCE9667FFD09768FFCF9769FFCF986AFFCF986BFFD0986CFFCF996CFFD09A
          6EFFD2A279FFDBB08DFFE0BDA2FFECEBEAFFDDDCDBFF5D5550FF453D39EAC9C7
          C5FFE4E3E2FFE3D3C5FFD7A47AFFD7A57CFFD7A77DFFD8A780FFD8A981FFD8A9
          82FFDAAC84FFD9AB84FFDAAD86FFDBAE89FFD9AB87FFD8AA84FFD5A57DFFD19B
          70FFCF986BFFCF9A6CFFCF9A6EFFCE996EFFCF9B6FFFCE9B6EFFCE9B70FFD09D
          71FFCE9C72FFD3A67FFFE1C1A7FFEDEDECFFE0DEDDFF5D5651FF453D39EACCCA
          C9FFE6E5E4FFE7E7E6FFE9E8E7FFEBEAEAFFECEBEBFFEEEDECFFEFEEEEFFEFEF
          EEFFF1F0EFFFF1F0F0FFF1F1F1FFF2F2F1FFF3F2F2FFF3F2F2FFF1F1F1FFECEB
          EAFFD9D8D7FFC8C6C4FFC7C5C3FFC7C5C4FFC7C5C4FFC8C6C5FFC8C6C4FFC8C6
          C4FFC9C7C5FFC9C7C5FFCCCAC8FFE4E2E2FFE2E0E0FF5D5651FF3C3430E9908E
          8BFFD0D0CFFFD3D3D3FFD5D5D5FFD7D7D6FFD9D9D9FFDBDBDAFFDCDCDCFFDEDD
          DDFFDFDFDEFFE0E0DFFFE1E1E1FFE2E1E1FFE2E2E2FFE3E3E2FFE3E3E3FFE3E3
          E3FFE2E2E1FFD3D3D3FFA4A3A3FF939291FF919090FF919090FF919090FF9291
          90FF91908FFF91908FFF919090FF91908FFF888686FF5E5752FF332B25E8362D
          27FF312924FF312924FF332B26FF352D28FF372F2AFF38312CFF3A332EFF3C35
          30FF3E3731FF403834FF423B36FF443D37FF453E3AFF47413CFF49433EFF4B44
          40FF4D4642FF4F4844FF514A45FF514B47FF433C37FF312A24FF312924FF3129
          24FF312924FF312924FF312924FF312924FF342B26FF3E352FFF332B25E8382F
          29FF382F29FF39302AFF3B322CFF3D352FFF3F3731FF423933FF433B35FF463D
          38FF48403AFF4A423CFF4C443FFF4F4741FF504943FF534B46FF554D48FF574F
          4AFF59524DFF5B544FFF5D5651FF5F5853FF625B56FF5B544FFF403731FF382F
          29FF382F29FF2F2823FF372E28FF2F2823FF362E28FF382F29FF332B25E8382F
          29FF382F29FF3A312BFF3B332DFF3E352FFF403731FF423934FF443C36FF463E
          38FF48403BFF4B433DFF4D453FFF4F4742FF514944FF534B46FF554E49FF5750
          4BFF5A524DFF5C544FFF5E5752FF605954FF625B56FF645D59FF655E59FF4B43
          3EFF2E2823FF4D4D4DFF292320FF323232FF252220FF382F29FF413A37C44840
          3AFF423933FF443B35FF463D38FF48403BFF4D4540FF524B45FF57504BFF5B54
          4FFF5F5853FF635C57FF66605BFF69625EFF6D6662FF6E6764FF716A65FF726C
          67FF746E69FF746E6AFF746E6AFF756F6BFF746F6BFF756F6AFF746E6BFF746F
          6BFF605955FF27221FFF3D342FFF2B2521FF3B3530FF433C38D90B0A09275954
          52AB696563BE6A6663BE6A6764BF6B6764BF6B6765BF6C6966C06D6966C06D69
          67C06D6967C06E6A67C06F6B69C06F6B69C0706B69C1706C6AC1716D6BC2716E
          6BC2726E6BC2726E6CC2726F6CC2736F6DC2746F6EC274706EC375716EC37571
          70C376736FC4716E6BC2696563BE696563BE5C5855AF0B09082E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000100000003000000070000000D00000011000000160000001B000000210000
          00270000002E000000340000003B00000041000000480000004D000000520000
          0056000000570000005800000059000000590000005E00000060000000550000
          00470000003C0000003000000023000000160000000A00000001000000000000
          00000000000000000003000000080000000D0000001100000016000000190000
          001D000000210000002400000026000000280000002A0000002B0000002B0000
          002B0000002A0000002F0202025922222293474747B5545454BD353535A00E0E
          0E80000000510000001A00000008000000030000000000000000000000000403
          0312070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          0520070605414E4D4DBEA0A0A0F9C4C4C4FFC8C8C8FFCACACAFFCCCCCCFFCACA
          CAFF909090DA1E1D1D880605044A0706052004040314000000001A161377372E
          29FC382F29FF382F29FF39302AFF3C332DFF403731FF443B36FF473E39FF4C44
          3EFF514943FF554E48FF58514CFF5C544FFF5F5853FF615B56FF635C57FF615B
          58FF858483FFC0C0C0FFC9C9C9FFCFCFCFFFD3D3D3FFD5D5D5FFD7D7D7FFD5D5
          D5FFD2D2D2FFC6C6C6FF6E6C6AFF48433FFF5A524DFD26221E90362F29E1615A
          55FF9F9B98FFA09B99FFA19C99FFA29E9BFFA29E9BFFA39F9CFFA4A09EFFA6A2
          9FFFA8A3A1FFA9A5A2FFABA7A5FFACA8A5FFADA9A6FFB3AFACFFADA9A7FF8B8B
          8AFFC0C0C0FFCACACAFFD2D2D2FFD7D7D7FFD8D8D8FFD9D9D9FFDBDBDBFFDCDC
          DCFFDCDCDCFFD6D6D6FFCECECEFF8B8987FF7B7673FF524A45F9453D39EAB3AF
          ADFFDEDDDCFFE0DEDDFFE1E0DFFFE3E2E1FFE4E3E2FFE5E4E3FFE7E6E5FFE9E8
          E7FFEAE9E8FFEBEBEAFFECECEBFFEDEDECFFF0F0EFFFF1F1F0FFAEAEAEFFA7A7
          A7FFC6C6C6FFC6C6D1FF3333C7FF7575CDFFD8D8D8FFD9D9D9FFD8D8DAFF5353
          CBFF5353CBFFDBDBDDFFD6D6D6FFCBCBCBFF858382FF564F4AFF453D39EAB5B2
          AFFFE0DEDDFFDFD8D1FFDBC1ACFFDBC2AEFFDDC4B0FFDFC7B4FFDFC8B5FFE1CA
          B8FFE2CCB9FFE3CEBCFFE6D2C0FFE9D6C7FFEBDBCDFFEADDD0FF8F8F8FFFC1C1
          C1FFC3C3CBFF2D2DC3FF0202BFFF0202BFFF6D6DCBFFD5D5D9FF4A4AC8FF0202
          BEFF0202BEFF4E4EC9FFDCDCDCFFD4D4D4FFA5A4A3FF4E4743FF453D39EAB6B2
          B0FFE1DFDEFFE0D7CEFFD7B698FFDAB89BFFDBB99EFFDBBCA0FFDDBDA3FFDEBF
          A5FFDFC0A7FFE2C5ACFFE3CAB5FFE8D1BDFFEBD5C4FFCEC2B7FF9C9C9CFFC4C4
          C4FFCDCDD0FF4B4BC4FF0202BCFF0202BCFF0202BCFF2121C0FF0202BCFF0202
          BCFF0202BBFF7474CDFFDFDFDFFFD8D8D8FFC0C0BFFF433E3AFF453D39EAB6B3
          B1FFE1E0DFFFE1D7D1FFD9BBA0FFDBBCA3FFDCBDA4FFDEC1A8FFDEC1A9FFE0C4
          ADFFE1C5AFFFE4CCB7FFE9D3C0FFEBD6C6FFEDDCCEFFC2BBB5FFA2A2A2FFC6C6
          C6FFD2D2D2FFD3D3D4FF5252C4FF0202B9FF0202B9FF0202B9FF0101B9FF0101
          B9FF7B7BCDFFDEDEDEFFDFDFDFFFDADADAFFCBCBCBFF3A3431FF453D39EAB6B3
          B1FFE1E0DFFFE4E2E2FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF1F1F0FFF5F4F4FFF7F7F7FFFAF9F9FFFBFBFBFFC4C4C4FFC0C0C0FFD8D8
          D8FFDFDFDFFFE0E0E0FFD8D8DFFF4646C8FF2E2EC4FF2A2AC2FF3434C5FF6767
          CFFFE6E6E7FFE9E9E9FFECECECFFEAEAEAFFD9D9D9FF3E3936FF453D39EAB6B2
          B0FFE1E0DFFFE3E2E1FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF3F2F1FFF5F5F5FFF7F7F7FFF9F9F8FFFBFBFBFFDBDBDAFFC3C3C3FFDCDC
          DCFFE4E4E4FFE1E1E6FF8F8FDAFF7777D7FF7B7BD8FF7E7ED9FF8282DAFF8585
          DBFFB1B1E3FFEFEFF0FFF2F2F2FFEFEFEFFFD8D8D8FF4A4441FF453D39EAB5B2
          AFFFE1DFDEFFE0D8D1FFDABDA4FFDABDA6FFDDBFA8FFDEC1ABFFDFC3ABFFE0C6
          B0FFE4CCB7FFE7CFBDFFEAD6C6FFEDDBCCFFEFDFD2FFE9DED4FFBCBCBCFFDEDE
          DEFFE1E1E4FF9595DCFF8181D8FF8484D9FF8989DBFFA8A8E1FF9090DDFF9393
          DEFF9797DFFFBBBBE7FFF2F2F2FFF0F0F0FFC0BFBFFF5A544FFF453D39EAB4B1
          AEFFDFDEDDFFDED2C9FFD6AD8DFFD7AF8EFFD7B190FFD7B292FFDAB596FFDAB6
          98FFDEBDA2FFE2C2A9FFE4C8B1FFE7CEB9FFEAD3C1FFECD8C8FFC7C6C5FFD8D8
          D8FFE4E4E5FFABABE0FF8F8FDCFF9494DDFFD0D0E9FFF0F0F0FFC6C6E9FFA2A2
          E1FFA5A5E2FFCFCFECFFF2F2F2FFF1F1F1FF9F9E9EFF625A55FF453D39EAB3AF
          ADFFDDDBDAFFDDD2CAFFD5B193FFD7B395FFD8B598FFD8B699FFDAB89CFFDAB9
          9DFFDEBFA5FFE1C5ACFFE3CAB4FFE7D0BCFFE9D3C2FFEBD8C8FFE1D7CEFFCFCF
          CFFFE5E5E5FFE9E9EAFFBDBDE5FFD7D7EBFFF1F1F1FFF2F2F2FFF3F3F3FFD4D4
          EDFFD7D7EFFFF4F4F4FFF3F3F3FFD5D5D5FFCCCBC9FF605954FF453D39EAB1AD
          ABFFDAD9D8FFDCDAD9FFDDDCDBFFDFDEDDFFE1DFDEFFE3E1E0FFE4E3E2FFE6E5
          E4FFE8E7E6FFEBEAE9FFEDECECFFEFEFEEFFF1F0F0FFF2F1F1FFF4F3F3FFD8D8
          D8FFDBDBDBFFEBEBEBFFEEEEEEFFF1F1F1FFF2F2F2FFF4F4F4FFF5F5F5FFF5F5
          F5FFF4F4F4FFF4F4F4FFE6E6E6FFC6C6C6FFD5D3D2FF5D5651FF453D39EAAFAC
          A9FFD8D6D5FFDAD8D7FFDAD9D8FFDCDAD9FFDEDCDBFFE0DEDDFFE1DFDEFFE3E1
          E0FFE4E2E2FFE7E5E5FFE9E8E7FFEBEBEAFFEDECECFFEFEEEEFFF0EFEFFFF0EF
          EFFFDCDCDCFFDDDDDDFFEAEAEAFFF0F0F0FFF2F2F2FFF3F3F3FFF5F5F5FFF5F5
          F5FFF5F5F5FFDBDBDBFFD7D6D6FFE8E8E7FFD2D0CEFF5A534EFF453D39EAADA9
          A7FFD5D3D2FFD5CBC3FFD3B194FFD3B196FFD4B398FFD6B59AFFD6B69BFFD7B7
          9EFFD7B89FFFDABCA4FFDCC2AAFFDEC5B0FFE0C9B6FFE3CEBCFFE5CFBFFFE6D3
          C3FFE8D5C7FFE1D6CCFFCCCAC9FFDDDCDCFFEDEDEDFFF0F0F0FFEBEBEBFFDBDA
          DAFFCBC6C3FFE4D4C7FFE5D8CCFFE7E6E5FFCFCDCCFF5B534EFF453D39EAABA7
          A4FFD1CFCDFFD2C5BAFFD1A27CFFD0A47EFFD1A581FFD1A580FFD2A783FFD2A8
          84FFD3AA87FFD3AA88FFD6AF90FFD8B395FFDBBA9CFFDDBDA2FFDFC0A8FFE0C4
          ACFFE1C6AFFFE2C9B4FFE3CAB5FFE3CBB8FFDECCBCFFDDCDC0FFE2CCBAFFE3CB
          B7FFE2CAB6FFE1C7B2FFE1CCBCFFE6E5E4FFD0CECDFF5B544FFF453D39EAA8A4
          A1FFCECCCAFFCFC2B8FFD0A684FFCFA585FFD0A787FFD1AA88FFD2AA8AFFD3AD
          8CFFD3AC8DFFD3AD8FFFD3AF90FFD6B396FFD8B69BFFD9BBA0FFDBBEA5FFDDC0
          A9FFDEC3ADFFDFC6B1FFDFC7B1FFE0C7B3FFE0C8B4FFE1C9B5FFE1CAB6FFE0C9
          B5FFE2C8B4FFDEC5B1FFDFCCBBFFE6E5E4FFD3D1D0FF5C544FFF453D39EABAB6
          B4FFD8D7D6FFD8D6D5FFD6D5D3FFD8D5D4FFDAD8D7FFDBDAD8FFDDDBDAFFDEDC
          DBFFDEDDDBFFD8D6D5FFD6D4D3FFD7D5D4FFD9D7D6FFDAD9D7FFDCDAD9FFDDDB
          DAFFDFDDDCFFDFDEDDFFE0DFDEFFE0DFDEFFE0DFDEFFE2E0DFFFE6E5E5FFE9E8
          E8FFE9E9E9FFE9E7E7FFE8E7E6FFE7E6E5FFD5D4D3FF5C544FFF453D39EABEBB
          B9FFDDDBDBFFDFDEDDFFE0DFDEFFE0DEDEFFDEDDDBFFDDDBDAFFDCDAD9FFDDDC
          DAFFDEDDDCFFE0DEDDFFDAD8D7FFD3D1D0FFD4D2D1FFD4D2D1FFD7D5D4FFD8D6
          D5FFD8D6D5FFD9D7D6FFDAD8D7FFDAD9D7FFDAD8D7FFD9D7D6FFDEDDDCFFE6E4
          E4FFEBE9E9FFEAE9E8FFE9E8E8FFE8E7E6FFD8D6D6FF5C5550FF453D39EAC2BF
          BDFFDFDEDDFFDED3CBFFD5B08FFFD8B192FFD8B494FFD7B396FFD9B597FFD9B4
          98FFD6B496FFD5B496FFD7B598FFD5B397FFD0AE94FFD0B095FFD1AF95FFD0B1
          97FFD1B197FFD1B197FFD1B399FFD2B49BFFD1B49BFFD2B59DFFD2B59EFFD6BB
          A4FFDEC4ACFFDFC4AEFFE2CEBEFFEAE8E8FFDBD9D8FF5C5550FF453D39EAC6C3
          C1FFE1E0DFFFDFD1C5FFD39F75FFD2A076FFD4A278FFD3A379FFD5A47DFFD5A4
          7DFFD5A77EFFD5A680FFD5A781FFD3A680FFD4A883FFD2A581FFCFA37FFFCFA5
          81FFCEA581FFD0A684FFCFA684FFCFA786FFCFA887FFD0A889FFD0A989FFD0AA
          8CFFD3AF91FFDBB79AFFE0C5AEFFECEBEAFFDDDCDBFF5D5550FF453D39EAC9C7
          C5FFE4E3E2FFE1D6CAFFD5A781FFD6A882FFD6A983FFD7AB86FFD7AC88FFD7AC
          89FFD9AF8BFFD8AE8BFFD9B08DFFDAB190FFD9AF8FFFD7B08FFFD5AD8DFFCFA8
          88FFCDA787FFCEA889FFCEA98AFFCDA98BFFCEAA8DFFCEAB8CFFCEAC8EFFCFAD
          8FFFCEAD91FFD3B397FFE1C7B3FFEDEDECFFE0DEDDFF5D5651FF453D39EACCCA
          C9FFE6E5E4FFE7E7E6FFE9E8E7FFEBEAEAFFECEBEBFFEEEDECFFEFEEEEFFEFEF
          EEFFF1F0EFFFF1F0F0FFF1F1F1FFF2F2F1FFF3F2F2FFF3F2F2FFF1F1F1FFECEB
          EAFFD9D8D7FFC8C6C4FFC7C5C3FFC7C5C4FFC7C5C4FFC8C6C5FFC8C6C4FFC8C6
          C4FFC9C7C5FFC9C7C5FFCCCAC8FFE4E2E2FFE2E0E0FF5D5651FF3C3430E9908E
          8BFFD0D0CFFFD3D3D3FFD5D5D5FFD7D7D6FFD9D9D9FFDBDBDAFFDCDCDCFFDEDD
          DDFFDFDFDEFFE0E0DFFFE1E1E1FFE2E1E1FFE2E2E2FFE3E3E2FFE3E3E3FFE3E3
          E3FFE2E2E1FFD3D3D3FFA4A3A3FF939291FF919090FF919090FF919090FF9291
          90FF91908FFF91908FFF919090FF91908FFF888686FF5E5752FF332B25E8362D
          27FF312924FF312924FF332B26FF352D28FF372F2AFF38312CFF3A332EFF3C35
          30FF3E3731FF403834FF423B36FF443D37FF453E3AFF47413CFF49433EFF4B44
          40FF4D4642FF4F4844FF514A45FF514B47FF433C37FF312A24FF312924FF3129
          24FF312924FF312924FF312924FF312924FF342B26FF3E352FFF332B25E8382F
          29FF382F29FF39302AFF3B322CFF3D352FFF3F3731FF423933FF433B35FF463D
          38FF48403AFF4A423CFF4C443FFF4F4741FF504943FF534B46FF554D48FF574F
          4AFF59524DFF5B544FFF5D5651FF5F5853FF625B56FF5B544FFF403731FF382F
          29FF382F29FF2F2823FF372E28FF2F2823FF362E28FF382F29FF332B25E8382F
          29FF382F29FF3A312BFF3B332DFF3E352FFF403731FF423934FF443C36FF463E
          38FF48403BFF4B433DFF4D453FFF4F4742FF514944FF534B46FF554E49FF5750
          4BFF5A524DFF5C544FFF5E5752FF605954FF625B56FF645D59FF655E59FF4B43
          3EFF2E2823FF4D4D4DFF292320FF323232FF252220FF382F29FF413A37C44840
          3AFF423933FF443B35FF463D38FF48403BFF4D4540FF524B45FF57504BFF5B54
          4FFF5F5853FF635C57FF66605BFF69625EFF6D6662FF6E6764FF716A65FF726C
          67FF746E69FF746E6AFF746E6AFF756F6BFF746F6BFF756F6AFF746E6BFF746F
          6BFF605955FF27221FFF3D342FFF2B2521FF3B3530FF433C38D90B0A09275954
          52AB696563BE6A6663BE6A6764BF6B6764BF6B6765BF6C6966C06D6966C06D69
          67C06D6967C06E6A67C06F6B69C06F6B69C0706B69C1706C6AC1716D6BC2716E
          6BC2726E6BC2726E6CC2726F6CC2736F6DC2746F6EC274706EC375716EC37571
          70C376736FC4716E6BC2696563BE696563BE5C5855AF0B09082E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000100000003000000070000000D00000011000000160000001B000000210000
          00270000002E000000340000003B000000410000004C000000570000005E0000
          0061000000630000006400000064000000650000006600000060000000550000
          004A0000003F0000003200000025000000170000000B00000002000000000000
          00000000000000000003000000080000000D0000001100000016000000190000
          001D000000210000002400000026000000280000002A0000002B0000002B0000
          002B00000032000000521010107C363636A94D4D4DBA303030A00B0B0B790000
          00570000002F0000000D00000008000000030000000000000000000000000403
          0312070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          05464C4C4BBA909090F1B7B7B7FFC5C5C5FFC8C8C8FFC8C8C8FFC6C6C6FE9797
          97E93C3C3CA2050404640706052C0706052004040314000000001A161377372E
          29FC382F29FF382F29FF39302AFF3C332DFF403731FF443B36FF473E39FF4C44
          3EFF514943FF554E48FF58514CFF5C544FFF5F5853FF615B56FF5B5653FF8382
          82FFB5B5B5FFC6C6C6FFCBCBCBFFCFCFCFFFD2D2D2FFD3D3D3FFD2D2D2FFD0D0
          D0FFCDCDCDFF8A8786FF3F3A38FF58514DFF5A524DFD26221E90312924E0615A
          55FF9F9B98FFA09B99FFA19C99FFA29E9BFFA29E9BFFA39F9CFFA4A09EFFA6A2
          9FFFA8A3A1FFA9A5A2FFABA7A5FFACA8A5FFADA9A6FF959290FF939292FFC0C0
          C0FFC8C8C8FFBDC8BFFF64A56CFF489B51FF3A9846FF429C4DFF6CAE74FFA9C5
          ACFFD5D5D5FFD0D0D0FFAFAFAFFF716F6DFF898480FF524A45F9332B25E8B3AF
          ADFFDEDDDCFFE0DEDDFFE1E0DFFFE3E2E1FFE4E3E2FFE5E4E3FFE7E6E5FFE9E8
          E7FFEAE9E8FFEBEBEAFFECECEBFFEDEDECFFE5E5E4FF8E8E8EFFB4B4B4FFC7C7
          C7FF7CAB81FF2F903AFF2A8F36FF2A9137FF2E953AFF2E953AFF2C9639FF2E99
          3BFF70B479FFD5D6D5FFD1D1D1FFADACABFF979594FF57504BFF332B25E8B5B2
          AFFFE0DEDDFFDFD8D1FFDBC1ACFFDBC2AEFFDDC4B0FFDFC7B4FFDFC8B5FFE1CA
          B8FFE2CCB9FFE3CEBCFFE5D0BEFFE6D1C0FFB4ABA2FF9D9D9DFFC4C4C4FF8CB2
          92FF2A8E36FF2A8F36FF52A15CFFB3CAB6FFD8D9D8FFD2D6D2FF96C19CFF3CA0
          49FF2E9C3BFF5AAE65FFD7D7D7FFCECECEFF8E8E8DFF534B47FF332B25E8B6B2
          B0FFE1DFDEFFE0D7CEFFD7B698FFDAB89BFFDBB99EFFDBBCA0FFDDBDA3FFDEBF
          A5FFDFC0A7FFE1C3AAFFE0C4ADFFE3C7AFFF8D8986FFB2B2B2FFAFBCB1FF2E8E
          3AFF2A8F36FF5AA364FFD7D7D7FFD8D8D8FFD9D9D9FFDADADAFFDBDBDBFFCFD8
          D0FF51AB5DFF2FA03DFFA1C8A5FFD3D3D3FFA7A7A7FF49433FFF332B25E8B6B3
          B1FFE1E0DFFFE1D7D1FFD9BBA0FFDBBCA3FFDCBDA4FFDEC1A8FFDEC1A9FFE0C4
          ADFFE1C5AFFFE2C8B1FFE5CBB5FFE4CAB5FF8F8E8DFFBFBFBFFF72A878FF2A8E
          36FF30933BFFCFD4D1FFD7D7D7FFD8D8D8FFD9D9D9FFDADADAFFDBDBDBFFDCDC
          DCFFCCD8CEFF36A544FF4EAE59FFD6D6D6FFBEBDBDFF3E3835FF332B25E8B6B3
          B1FFE1E0DFFFE4E2E2FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF1F1F0FFF4F3F3FFF5F5F5FFE8E8E8FF949494FFC1C1C1FF499954FF2A90
          37FF4B9E54FFD6D6D6FFD7D7D7FFD8D8D8FFD9D9D9FFDADADAFFDBDBDBFFDCDC
          DCFFDEDEDEFF7ABD82FF36A645FFD8D8D8FFCBCBCBFF3F3A37FF332B25E8B6B2
          B0FFE1E0DFFFE3E2E1FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF2F1F0FFF4F3F3FFF5F5F5FFE6E6E5FFA7A7A7FFD6D6D6FF5EA968FF4CA3
          56FF63AE6CFFE0E0E0FFE0E0E0FFDFDFDFFFDFDFDFFFE1E1E1FFE3E3E3FFE6E6
          E6FFE9E9E9FFBADBBFFF5DBA68FFDEE7DFFFD8D8D8FF474341FF332B25E8B5B2
          AFFFE1DFDEFFE0D8D1FFDABDA4FFDABDA6FFDDBFA8FFDEC1ABFFDFC3ABFFE0C6
          B0FFE2C8B2FFE3C8B3FFE5CCB7FFE5CCB9FFACACACFFD9D9D9FF72B37AFF58AB
          62FF5FAF68FFD7E1D8FFE9E9E9FFEAEAEAFFEBEBEBFFECECECFFEDEDEDFFEEEE
          EEFFF0F0F0FFD0E6D4FF76C67FFFEEEEEEFFD7D7D7FF4C4744FF332B25E8B4B1
          AEFFDFDEDDFFDED2C9FFD6AD8DFFD7AF8EFFD7B190FFD7B292FFDAB596FFDAB6
          98FFDCB89BFFDEB99CFFDDBB9EFFDEBDA0FFA5A4A2FFD7D7D7FF87BC8DFF5EAF
          68FF60B26AFF80C088FFE6E9E7FFECECECFFEDEDEDFFEEEEEEFFEFEFEFFFF0F0
          F0FFF1F1F1FFB1DCB7FF9AD3A1FFEFEFEFFFBFBFBEFF5D5652FF332B25E8B3AF
          ADFFDDDBDAFFDDD2CAFFD5B193FFD7B395FFD8B598FFD8B699FFDAB89CFFDAB9
          9DFFDCBBA0FFDDBDA1FFDDBEA4FFDFC1A6FFC2BBB4FFCFCFCFFFC7D7C9FF65B2
          6EFF66B56FFF68B772FF7DC185FFDEE9E0FFEFEFEFFFF0F0F0FFF1F1F1FFF2F2
          F2FFF1F3F1FF8FCF97FFD9E9DBFFF0F0F0FFA0A0A0FF6A635FFF332B25E8B1AD
          ABFFDAD9D8FFDCDAD9FFDDDCDBFFDFDEDDFFE1DFDEFFE3E1E0FFE4E3E2FFE6E5
          E4FFE7E6E5FFE9E8E7FFEAE9E8FFEBEAE9FFE6E5E4FFC5C5C5FFE1E1E1FF90C5
          97FF6BB875FF6EBB77FF70BD78FFACD7B0FFF1F1F1FFF2F2F2FFF3F3F3FFF4F4
          F4FFAFDDB5FFC3E3C8FFF2F2F2FFDEDEDEFFACAAAAFF6A635FFF332B25E8AFAC
          A9FFD8D6D5FFDAD8D7FFDAD9D8FFDCDAD9FFDEDCDBFFE0DEDDFFE1DFDEFFE3E1
          E0FFE4E2E2FFE5E3E3FFE6E5E4FFE7E6E5FFE8E7E6FFD8D7D6FFD6D6D6FFE2E5
          E2FF93C89AFF73BE7BFF75C17FFF89CA90FFEFF0EFFFD8E9DBFFC9E4CCFFBEE2
          C3FFDFEDE0FFF4F4F4FFEFEFEFFFABAAAAFFCECCCAFF6A635FFF332B25E8ADA9
          A7FFD5D3D2FFD5CBC3FFD3B194FFD3B196FFD4B398FFD6B59AFFD6B69BFFD7B7
          9EFFD7B89FFFD8B9A0FFD9BCA2FFD9BCA3FFD9BDA5FFDBBFA8FFD2CDC9FFDDDD
          DDFFEBEBEBFFB9DBBEFFBEDDC3FFD9E9DBFFE6EFE7FFE0EDE2FFF0F3F2FFF5F5
          F5FFF5F5F5FFF1F1F1FFBBB8B4FFE4E3E2FFCECCCBFF6B635FFF332B25E8ABA7
          A4FFD1CFCDFFD2C5BAFFD1A27CFFD0A47EFFD1A581FFD1A580FFD2A783FFD2A8
          84FFD3AA87FFD3AA88FFD4AB8AFFD4AC8BFFD5AF8DFFD6AF8FFFD7B091FFD2C3
          B5FFD8D8D8FFE9E9E9FFF1F1F1FFF2F2F2FFF4F4F4FFF5F5F5FFF5F5F5FFF5F5
          F5FFDFDFDEFFC4B4AAFFDFCBBAFFE5E4E3FFD0CECDFF6B6460FF332B25E8A8A4
          A1FFCECCCAFFCFC2B8FFD0A684FFCFA585FFD0A787FFD2AB8AFFD3AE8EFFD5B1
          92FFD3AE8EFFD3AD8FFFD3AF90FFD4B091FFD5B093FFD4B294FFD5B396FFD6B3
          97FFD5BAA3FFD5CDC6FFCFCCCAFFDFDFDFFFEAEAEAFFDDDDDDFFCFCDCBFFCAC4
          BDFFDFC8B7FFDEC7B5FFE1CFC0FFE6E5E4FFD3D1D0FF6C6460FF332B25E8BAB6
          B4FFD8D7D6FFD8D6D5FFD6D5D3FFD8D5D4FFDAD8D7FFDBDAD8FFDDDBDAFFDEDC
          DBFFDEDDDBFFD8D6D5FFD6D4D3FFD7D5D4FFD8D6D5FFD8D7D5FFD9D7D6FFD9D7
          D6FFDAD8D7FFDAD9D7FFDBD9D8FFDBD9D8FFD9D7D6FFDDDBDAFFE3E2E1FFE7E6
          E6FFE8E8E8FFE9E7E7FFE8E7E6FFE7E6E5FFD5D4D3FF6C6460FF332B25E8BEBB
          B9FFDDDBDBFFDFDEDDFFE0DFDEFFE0DEDEFFDEDDDBFFDDDBDAFFDCDAD9FFDDDC
          DAFFDEDDDCFFE0DEDDFFDAD8D7FFD3D1D0FFD4D2D1FFD4D2D1FFD6D4D3FFD6D4
          D3FFD6D4D3FFD6D4D3FFD7D5D4FFD7D6D4FFD7D5D4FFD7D5D4FFDDDCDBFFE6E4
          E4FFEBE9E9FFEAE9E8FFE9E8E8FFE8E7E6FFD8D6D6FF6C6561FF332B25E8C2BF
          BDFFDFDEDDFFE0D8D2FFDFC4AEFFE1C6B1FFE1C9B3FFE2C9B6FFE3CBB8FFE2CA
          B8FFE1CAB7FFDFCAB7FFE1CBB9FFDDC5B1FFD1B299FFD0B095FFD1AF95FFD0B1
          97FFD1B197FFD1B197FFD1B399FFD2B49BFFD1B49BFFD2B59DFFD2B59EFFDAC4
          B0FFE5D3C3FFE6D3C4FFE7D9CDFFEAE8E8FFDBD9D8FF6C6561FF332B25E8C6C3
          C1FFE1E0DFFFE3D9D2FFE1C0A4FFE1C1A6FFE3C3A9FFE3C5ABFFE4C6AFFFE4C7
          B0FFE5C9B1FFE5CAB4FFE4CAB3FFE3C9B3FFE3C8B2FFD7B395FFCFA380FFCFA5
          81FFCEA581FFD0A684FFCFA684FFCFA786FFCFA887FFD0A889FFD0A989FFD0AA
          8CFFD8BAA1FFE6CEBCFFE7D5C7FFECEBEAFFDDDCDBFF615955FF332B25E8C9C7
          C5FFE4E3E2FFE5DDD6FFE3C8B1FFE5CAB4FFE5CBB6FFE6CDB9FFE7CEBAFFE7CF
          BCFFE9D2BEFFE8D2BFFFE9D3C1FFEAD4C3FFE8D3C2FFE7D2C2FFE0C8B4FFD3AF
          92FFCDA787FFCEA889FFCEA98AFFCDA98BFFCEAA8DFFCEAB8CFFCEAC8EFFCFAD
          8FFFCEAD91FFD6BBA3FFE7D7CAFFEDEDECFFE0DEDDFF5D5651FF332B25E8CCCA
          C9FFE4E3E2FFE6E5E4FFE7E6E6FFE9E8E8FFEBEAE9FFECEBEBFFEDECECFFEEED
          ECFFEFEEEEFFEFEFEEFFF0EFEFFFF1F0F0FFF1F0F0FFF1F0F0FFF0EFEFFFEAE9
          E9FFD8D7D6FFC8C5C4FFC7C4C3FFC7C5C3FFC7C5C3FFC8C6C4FFC8C6C4FFC9C6
          C4FFC9C7C5FFC9C7C6FFCCCAC9FFE3E2E1FFE1E0DFFF5D5651FF332B25E89490
          8DFFC5C4C3FFC8C6C5FFC9C8C7FFCBCAC9FFCECCCBFFCFCECDFFCFCFCEFFD0CF
          CFFFD2D1D0FFD3D2D1FFD4D3D2FFD5D4D3FFD6D5D4FFD6D5D4FFD6D5D5FFD7D6
          D5FFD6D5D5FFCCCBCAFFAEABAAFFA39F9DFFA19D9AFFA29E9CFFA29E9CFFA39F
          9DFFA3A09DFFA4A19EFFA4A29FFFA4A2A0FF9F9B99FF5E5752FF332B25E8382F
          29FF372E28FF372E28FF3A312BFF3C332DFF3E352FFF403832FF423A34FF443C
          36FF463E38FF49403BFF4B433DFF4D453FFF4F4742FF514A44FF534C46FF564E
          49FF58504BFF5A524DFF5C544FFF5D5651FF4C443FFF382F29FF372E28FF372E
          28FF372E28FF372E28FF372E28FF372E28FF382F29FF3E352FFF332B25E8382F
          29FF382F29FF39302AFF3B322CFF3D352FFF3F3731FF423933FF433B35FF463D
          38FF48403AFF4A423CFF4C443FFF4F4741FF504943FF534B46FF554D48FF574F
          4AFF59524DFF5B544FFF5D5651FF5F5853FF625B56FF5B544FFF403731FF382F
          29FF382F29FF2F2823FF372E28FF2F2823FF362E28FF382F29FF332B25E8382F
          29FF382F29FF3A312BFF3B332DFF3E352FFF403731FF423934FF443C36FF463E
          38FF48403BFF4B433DFF4D453FFF4F4742FF514944FF534B46FF554E49FF5750
          4BFF5A524DFF5C544FFF5E5752FF605954FF625B56FF645D59FF655E59FF4B43
          3EFF2E2823FF4D4D4DFF292320FF323232FF252220FF382F29FF413A37C44840
          3AFF423933FF443B35FF463D38FF48403BFF4D4540FF524B45FF57504BFF5B54
          4FFF5F5853FF635C57FF66605BFF69625EFF6D6662FF6E6764FF716A65FF726C
          67FF746E69FF746E6AFF746E6AFF756F6BFF746F6BFF756F6AFF746E6BFF746F
          6BFF605955FF27221FFF3D342FFF2B2521FF3B3530FF433C38D90B0A09275954
          52AB696563BE6A6663BE6A6764BF6B6764BF6B6765BF6C6966C06D6966C06D69
          67C06D6967C06E6A67C06F6B69C06F6B69C0706B69C1706C6AC1716D6BC2716E
          6BC2726E6BC2726E6CC2726F6CC2736F6DC2746F6EC274706EC375716EC37571
          70C376736FC4716E6BC2696563BE696563BE5C5855AF0B09082E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010011003400290081003900B5004200CF05520BEA024D03ED0044
          00D5003C00BD002A00850014003F000200040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000200050027
          006F05590AE61B8636FF0A8A13FF0BAC16FF0AC313FF18E130FF18E82FFF01CD
          01FF03AC05FF038D07FF198B31FF014E02E5001D005500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000001C0050055909DE0677
          0CFF25AD48FF1AB233FF15BE29FF11CB21FF0DD719FF09E211FF06EC0BFF05EE
          0AFF07E80EFF0BDD15FF11D21FFF22BB43FF147E28FF004300BF000A001E0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000002F008C117623FE1CA138FF14A3
          27FF18A82EFF1BAE33FF17BA2BFF13C524FF0FD01DFF0CD917FF0AE013FF09E1
          12FF0BDD15FF0DD51AFF11CB20FF15C027FF1FB83AFF10871EF80A6215E40028
          0070000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000003A00B7167A2AFF119922FF0F9D1DFF13A1
          24FF17A62BFF1AAA32FF19B330FF15BE29FF12C723FF10CE1EFF0ED31BFF0ED4
          1BFF0FD11CFF11CB20FF14C326FF17B92CFF1BAE33FF179C2CED1A8D32D40A58
          15BD001F00520000000000000000000000000000000000000000000000000000
          0000000000000000000000310098137125FF099312FF099712FF0D9B19FF119F
          20FF15A427FF18A82EFF1BAB34FF18B42FFF16BC2AFF14C326FF12C623FF12C7
          23FF13C524FF15C027FF17B92CFF1AB131FF18A02FF0138D24D80E791BBF116B
          20A9053F0B95000C001F00000000000000000000000000000000000000000000
          000000000000002100640C5C18FF088D10FF059109FF08950FFF0B9916FF0F9D
          1CFF12A123FF15A529FF18A82FFF1BAB34FF1AB131FF18B62DFF17B92CFF17BA
          2BFF17B82CFF19B42FFF1BAE33FF18A02FF0148E26DA0F7C1DC20B6B15AB085B
          0F960B4F1582022C056D00030008000000000000000000000000000000000000
          00000003000806460AEE0A7C15FF008C01FF039006FF06930BFF099611FF0C9A
          18FF0F9E1EFF12A123FF15A429FF18A72DFF1BA932FF21AF3FFF20AE3FFF1CAE
          35FF1CAC35FF1AA731FB179C2CED149025DD0F7E1DC50C6D17AE085D1098064F
          0B840342067107360E5F00190045000000010000000000000000000000000000
          0000012F02910E5B1AFF007B00FF008800FF018E03FF049108FF07930DFF0997
          12FF0D9A18FF0F9D1DFF19A230FF128823FF1B8537FF156C27F5156428F31A70
          33FF116E20F8189030E9128C22DA0F7E1CC70C6D16AF085E109A06500B850443
          077202370460022D0350031C06410005000C0000000000000000000000000001
          000105430CF306640DFF007400FF008000FF008C00FF028E04FF049109FF0794
          0DFF0B9916FF0D8A18FF137621FC06300C610108021100000000000000000105
          010A041C07470A3C15BC115A21B80A6413A307550E8C05480A78033C06660231
          045501280246001F00380216042C00080116000000000000000000000000011B
          03490A4714FF006000FF006B00FF007700FF008200FF008C00FF028F05FF0491
          09FF0E881EFF0B6317E6010B0217000000000000000000000000000000000000
          00000000000000010002041B075E07340F700438075F022E045001260342011E
          01360018012B00120021000C0118000601110000000000000000000000000437
          07A407480FFF005700FF006300FF006E00FF007900FF008300FF008C00FF078C
          0FFF0B5F13FB0210042300000000000000000000000000000000000000000000
          0000000000000000000000010002020F043C021E043A011B0230001601270011
          001F000C0017000800100005000B000301070000000100000000000000000542
          0AD2044A0AFF004E00FF005900FF006400FF006E00FF007800FF008100FF0769
          0FFF06380A930000000000000000000000000000000000000000000000000000
          000000000000000000000000000000030109010B021E000C0017000800120005
          000D0004000A0002000600010004000000010000000000000000000000000644
          0DE5024904FF004D00FF005000FF005A00FF006400FF006D00FF007600FF0754
          0EFF021204300000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000002000800010006000100030000
          0001000000010000000000000000000000000000000000000000000000000845
          0FF40D570EFF0F5B0FFF125D12FF136113FF0F650FFF006200FF006900FF0848
          0FFB000100030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000948
          11F5236C24FF267126FF267126FF267126FF226C22FF005600FF005C01FF0847
          11FA000100020000000000000000000000000000000000000000000000000B23
          124520572DF707390EFF083C0EFF083F0FFE08400FFD084310FD094711FE094A
          12FF0A5013FF0B5615FF0B5E17FF0C5F16E4031C063B00000000000000000B49
          13E5296E2AFF2E782EFF2E782EFF2E782EFF2F792FFF035003FF005100FF084A
          10FF0311042A0000000000000000000000000000000000000000000000000612
          09280F5B1AFF01D101FF13E213FF25DD25FF26D226FF28C628FF28B828FF1DA5
          1DFF0F900FFF007900FF006E00FF016401FF10711EEA00000000000000000C4C
          15D12D702EFF378137FF378137FF378137FF378137FF105A10FF004D00FF074A
          0DFF08320E820000000000000000000000000000000000000000000000000000
          000007320E9A07550CFF00B901FF09C209FF24C424FF36C336FF37BA37FF37B2
          37FF37A937FF369E36FF278E27FF167816FF148426FF00000000000000000B42
          14A4296A2DFF3F883FFF3F883FFF3F883FFF3F883FFF2F762FFF004D00FF024C
          03FF0C5117FA0209031600000000000000000000000000000000000000000000
          000000000000093912A908560FFF00A000FF00A000FF0BA00BFF2CAB2CFF3FAF
          3FFF3FAA3FFF3FA43FFF3F9E3FFF409840FF1B912EFF00000000000000000522
          094D1B5A24FF489048FF489048FF489048FF489048FF4B924BFF1F671FFF004D
          00FF054E09FF0B4B13E0020B031B000000000000000000000000000000000000
          0000000000000311052A0A4613F1027B03FF008B00FF008900FF2DA02DFF48AE
          48FF48AA48FF48A548FF48A148FF499E49FF1B982DFF00000000000000000001
          00030A4F13F34E904EFF509850FF509850FF509850FF509850FF519551FF0B56
          0BFF004D00FF044E08FF094710F907340D94031205320000000000000000020E
          042607320C91084210F8036006FF007700FF007900FF1C8B1CFF50AF50FF50AD
          50FF50AA50FF50A750FF50A450FF53A353FF189728FF00000000000000000000
          0000063B0D95307034FF5AA05AFF59A059FF59A059FF59A059FF59A059FF5193
          51FF0E580EFF004D00FF014D02FF05470AFF06410CFF073E0DF7073D0DF6063C
          0CFF054708FF015902FF006300FF006700FF208220FF54AB54FF59AE59FF59AD
          59FF59AB59FF59AA59FF59A859FF5EA75EFF159423FF00000000000000000000
          00000005010B07510FED5B9C5CFF61A761FF61A761FF61A761FF61A761FF61A7
          61FF63A663FF2F752FFF004D00FF004D00FF004D00FF024D02FF034E03FF004D
          00FF005000FF005300FF095E09FF419241FF61AE61FF61AE61FF61AE61FF61AE
          61FF61AD61FF61AC61FF61AB61FF67AD67FF129420FF00000000000000000000
          000000000000042A0666216A26FF6EAE6EFF6AAF6AFF6AAF6AFF6AAF6AFF6AAF
          6AFF6AAF6AFF6CB06CFF5B9C5BFF387D38FF226922FF045104FF014E01FF165E
          16FF287128FF428742FF60A760FF6AB26AFF6AB26AFF6AB26AFF6AB26AFF6AB2
          6AFF6AB26AFF6AB16AFF6AB16AFF72B472FF0F941AFF00000000000000000000
          00000000000000000000043F089C347E39FF79BA79FF72B772FF72B772FF72B7
          72FF72B772FF72B772FF72B772FF72B772FF72B772FF7ABA7AFF75B675FF73B8
          73FF72B772FF72B772FF72B772FF72B772FF72B872FF72B872FF72B872FF7ABB
          7AFF74B475FF7BBB7BFF72B772FF7BBC7BFF0C9214FF00000000000000000000
          0000000000000000000000010001044D07BB3E8A40FF81C081FF7BBF7BFF7BBF
          7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF
          7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF7BBF7BFF80C080FF4896
          4BFF05690ADE2E9032FF80C080FF85C485FF099110FF00000000000000000000
          0000000000000000000000000000000100020243049828832AFF84C084FF86C8
          86FF83C783FF83C783FF83C783FF83C783FF83C783FF83C783FF83C783FF83C7
          83FF83C783FF83C783FF83C783FF83C783FF84C784FF6CB36CFF1C7D1DFF0248
          049B00020004024504882F9630FF94CC94FF049108FF00000000000000000000
          00000000000000000000000000000000000000000000002E0161036B04EB59A1
          59FF97CE97FF90D190FF8BCF8BFF8BCF8BFF8BCF8BFF8BCF8BFF8BCF8BFF8BCF
          8BFF8BCF8BFF8BCF8BFF90D190FF8FCA8FFF429142FF026703EE0030016A0000
          00000000000000000000014502835AB05BFF49A44AEE00000000000000000000
          00000000000000000000000000000000000000000000000000000005000C003C
          0085036903F04B974BFF7EB87EFF98CD98FFA2D6A2FFABDEABFFA7DCA7FF95D1
          95FF80C280FF6DB06DFF4F984FFF046804F3003F008F0007000E000000000000
          00000000000000000000000000000712071C1928193400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000200040021004A0C4E0CA32C752CD8529252EE7FB17FFD93BE93FD71A6
          71F24A884ADE225E22AE05270553000300060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000100010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000300000005000000060000
          0005000000050000000500000005000000050000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000100000002B010D0772051C12A81841
          2DCB163928C6041A10A80109056F0000004F0000003A0000001F0000001B0000
          001B0000001B0000001B0000001B0000001B0000001B0000001B0000001B0000
          001B0000001B0000001B00000011000000020000001100000000000000000000
          0000000000000000000000000000051E126C1C7147EE11B564FF1CE6A1FF22F0
          BFFF21F2C5FF19E4A3FF11A55AFF1A603FFB2B3C34E43E3E3ED53F3F3FD53F3F
          3FD53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F
          3FD53F3F3FD53F3F3FD50B0B0B3A000000040000001600000000000000000000
          0000000000000004020A104E30BD10B563FF0DD075FF13E085FF16EA92FF25E5
          96FF28EA9FFF13E68DFF13DE82FF11CB71FF0C954FFF517A68FFC5C8C4FFC9CA
          C8FFCACBCAFFCCCDCAFFCECECDFFCFD0CEFFD2D2D0FFD3D4D2FFD6D6D4FFD6D8
          D5FFD8D9D6FF6D6E6DFF0E0E0E2D000000000000000000000000000000000000
          0000000101030E5632C210A75CFF0EBF68FF0DC96FFF0FD476FF6A977FFF9DA3
          9DFFB0B7B2FF8EBCA6FF0BD074FF0EC56DFF0DBA62FF0B8947FF305D51FFBABD
          B7FFBFC1BBFFC4C8C1FFCCCFC8FFD2D5CEFFD8DBD3FFE0E3DAFFE6E9E0FFEBEF
          E6FFF9FBF7FF6E6E6EFF0E0E0E2D000000000000000000000000000000000000
          0000073D23860D8E4CFF0CA255FF0DAD5DFF0FB762FF0CC168FF616A63FF614B
          26FF5B4B31FFA0A9A4FF10BD67FF0BB25DFF0BA758FF089B4CFF0B6B39FF3F61
          5DFFA4A7A1FFACAEA7FFB5B9B0FFBEC1B8FFC9CDC2FFD3D7CCFFDBDFD4FFE2E7
          DBFFF9FBF8FF6F706FFF0E0E0E2D00000000000000000000000000000000010D
          071B0C7B47F30A914AFF0B984EFF0CA056FF0AA758FF0DAF5EFF4F5752FF5344
          2AFF524533FF959D99FF0DAB5CFF0CA057FF0A974FFF078B47FF0A7F43FF0B46
          2FFF98A099FFAEB2AAFFB6BAB2FFBEC3B9FFCBCFC4FFD3D8CDFFDBDFD4FFE2E7
          DCFFF9FAF8FF707170FF0F0F0F2D000000000000000000000000000000000941
          25770D9451FF0A9950FF0A9950FF0D9F56FF0CA55BFF0FA95FFF646C67FF483E
          2FFF474036FFA1A9A5FF0DA35BFF0A9A53FF0B924FFF0B8E4CFF098B4BFF0965
          38FF41635FFFA9ADA5FFB1B5ACFFBABDB5FFC1C7BCFFCCD0C5FFD9DDD3FFE3E8
          DDFFF9FBF8FF727272FF0F0F0F2D000000000000000000000000000000000D6F
          42C10B9E56FF0DA359FF6C907FFFA0A9A5FFACB5B0FF9EA7A2FF7F8782FF3732
          2CFF35322FFFA9B0ADFFAFB8B3FFACB5B1FFAFB7B3FF83AC98FF099451FF0978
          44FF22584EFFB5B8B0FFBBC0B8FFC4C9C0FFCED3C9FFD7DBD1FFDEE2D8FFE3E8
          DEFFF9FAF8FF737373FF0F0F0F2D000000000000000000000000000000001283
          50DE0DAA5FFF14A662FF6A706DFF343436FF363536FF373636FF2D2C2CFF2E2D
          2FFF302F30FF313132FF3E3D3FFF3E3D3EFF3D3C3EFFA2A9A6FF099C58FF0C8C
          51FF1B5A48FFB9BEB5FFC2C5BDFFCACEC4FFD1D6CCFFD9DDD3FFDEE3D9FFE4E9
          DFFFF9FBF8FF747574FF1010102D000000000000000000000000000000001890
          5AEE13B66DFF1AAF6CFF444A47FF2F2F31FF302F31FF2F2F30FF2C2B2DFF2E2D
          2FFF2E2E30FF302F31FF353537FF343434FF363537FF848B88FF24AF70FF1A9E
          63FF226C53FFB7BBB2FFBDC1B8FFC4CABFFFCCD0C5FFD2D6CDFFDCE1D8FFE5EB
          E0FFF9FBF8FF767676FF1010102D000000000000000000000000000000001F87
          59D815BA72FF1CC47CFF2E5544FF343D39FF343D39FF343D39FF333B37FF2D2C
          2EFF2E2D2FFF616865FF474F4CFF363E3AFF474F4BFF557E6CFF30BA7EFF27A5
          6DFF337B68FFC2C7BDFFC9CDC3FFD1D5CBFFD6DAD0FFDADED5FFE1E7DCFFE6EB
          E0FFF9FBF8FF787877FF1010102D000000000000000000000000000000001F72
          4DB224BC7BFF23CE89FF24CF8AFF2AD18DFF2FD18FFF36D293FF454D4AFF3231
          34FF363637FF868E8BFF3ECE93FF3FCC91FF3BC98FFF3CC88CFF35C289FF32A7
          75FF3D8276FFCCD1C7FFD1D6CBFFD7DBD1FFDBE0D6FFE0E5DBFFE4E9DEFFE6EB
          E0FFF9FBF8FF797A79FF1111112D000000000000000000000000000000000A3B
          24633DB982FF29D896FF2ED997FF34DA99FF3BDA9CFF3FDA9FFF515956FF3333
          34FF363637FF989F9CFF4CD6A2FF4AD49EFF46D29AFF43D097FF40CD95FF41A1
          77FF789E9AFFD0D4CAFFD4D8CEFFD7DCD2FFDCE0D6FFDEE3D9FFE4E9DEFFE8ED
          E2FFF9FAF7FF7B7B7AFF1111112D000000000000000000000000000000000003
          01042D9563E05CD6A8FF38E0A6FF3BE1A5FF3DE1A6FF49E2ACFF48504DFF3131
          33FF343335FF8A918FFF5BDFB0FF4CDBA4FF4DD9A3FF53D9A7FF69CDA4FF2D83
          68FFCFD3C9FFD1D6CCFFD4D9CFFFD7DCD2FFDBE0D5FFDDE2D7FFE2E7DCFFE4E9
          DEFFF8F9F6FF7C7C7CFF1111112D000000000000000000000000000000000000
          000004321C5163C194FE73E4BEFF41E7B1FF4AE8B4FF51E8B6FF3C5F53FF393E
          3DFF4F5452FF66867BFF5EE4B7FF57E2B0FF5CE1B1FF7DDEB9FF55AB85FF8BB1
          ACFFD7DCD2FFDBE0D6FFDCE2D7FFDFE4D9FFE1E6DBFFE2E8DCFFE2E7DCFFE0E5
          DAFFF6F8F5FF7D7E7DFF1212122D000000000000000000000000000000000000
          0000000000000C5935857BD6AEFFA4EDD5FF6EEFCAFF57ECBEFF56ECBBFF5DD8
          B1FF62D9B5FF69EAC1FF6AE9C2FF80EBC8FFAAE9D3FF6CBA97FF69A5A0FFD9DE
          D4FFDADFD5FFDDE1D7FFDDE2D7FFDEE3D8FFE0E5DAFFDFE4D9FFDDE2D7FFD9DE
          D4FFF4F5F3FF7F7F7FFF1212122D000000000000000000000000000000000000
          00000000000000000000145A398D66CA9AFFC5EFE0FFC8F7E9FFA5F7E0FFA3F6
          E0FFA7F5DFFFAAF5E0FFCEF6E9FFBCEAD9FF5AAE8DFF7DACA7FFD1D5CBFFD2D5
          CCFFD2D6CCFFD3D7CDFFD3D8CEFFD3D8CEFFD1D5CCFFCFD3CAFFD2D6CDFFD2D6
          CCFFF2F4F1FF808080FF1212122D000000000000000000000000000000000000
          000000000000000000003333337863756DFF41A576FF6FCCA0FF9DDCBFFFA6E1
          C7FFA5E0C5FF99D5B8FF66B69CFF56A297FFCEDAD0FFDBE0D5FFDBE0D5FFDCE1
          D6FFDBE0D5FFDAE0D5FFD9DFD4FFD8DCD2FFD5D9CFFFD1D6CCFFCDD2C7FFC7CD
          C1FFF0F1EFFF818181FF1313132D000000000000000000000000000000000000
          000000000000000000004E4E4EB46F6F6FFF7B7B7BFF727272FFC9DBD2FF99CB
          BDFF8CBEB6FFD5E1D5FFDFE4D9FFDFE4D9FFDDE2D7FFDCE1D6FFDBE0D5FFDBE0
          D5FFD8DDD3FFD8DDD3FFD5D9CEFFD1D5CBFFCCD1C8FFC8CCC1FFC2C8BDFFBFC2
          B9FFEEEFEDFF838382FF1313132D000000000000000000000000000000000000
          000000000000030303066D6D6DF6717171FF767676FF717171FFDFE1DEFFE9ED
          E4FFDCE1D7FFD5DAD0FFD5DAD0FFD4D9CFFFD3D8CEFFD2D6CCFFD0D4CBFFCED3
          C9FFCDD2C8FFC9CEC3FFC3C9BDFFBEC2BBFFBABDB5FFB5BAB1FFB6BAB0FFB4B7
          AFFFEBECEAFF848484FF1313132D000000000000000000000000000000000000
          0000000000001B1B1B3C737373FF8B8B8BFF757575FF707070FFDFE0DEFFE7EB
          E3FFDEE3D8FFDBE0D5FFDBE0D5FFDADED4FFD8DCD2FFD5DBCFFFD4D9CFFFD1D6
          CCFFCDD1C7FFC7CCC1FFC0C4BBFFB9BEB5FFB4B9AFFFAFB3A9FFADB1A9FFA9AC
          A5FFE9E9E7FF858585FF1414142D000000000000000000000000000000000000
          0000000000003A3A3A7E757575FF9F9F9EFF838382FF717171FFE0E1DEFFE7EA
          E2FFDEE2D8FFDDE2D7FFDCE1D6FFDAE0D4FFD8DDD2FFD5DAD0FFD3D8CEFFCED3
          C8FFC8CEC2FFC1C5BBFFB9BDB4FFB3B7AEFFADB1A8FFA8ACA4FFA3A69EFF9FA2
          9BFFE6E7E5FF868686FF1414142D000000000000000000000000000000000000
          0000000000005B5B5BC3777777FFAFAFAEFF90908FFF737373FFDFE1DEFFE5EA
          E1FFDBE1D5FFD9DDD3FFD8DCD2FFD6DBD0FFD3D9CDFFD0D5CBFFCBCFC4FFC5CB
          C0FFBFC3BAFFB7BCB2FFB0B4ABFFAAADA4FFA4A69FFF9EA19AFF9A9D95FFB0B2
          ADFFE4E5E3FF878887FF1414142D000000000000000000000000000000000000
          000004040409767676F9797979FFB8B8B6FF9D9D9CFF757575FFE0E0DEFFE4E8
          E0FFD8DDD2FFD4D9CEFFD2D8CCFFD0D5CAFFCDD2C8FFC9CDC2FFC1C5BBFFBEC2
          B8FFB7BBB2FFB5B9B1FFFAFBF4FFFAFBF4FFFAFBF4FFFAFBF4FFF2F3ECFFE1E1
          DCFFC3C4BFFF7C7C7AD803030306000000000000000000000000000000000000
          0000222222487A7A7AFF969695FFA1A1A0FFA8A8A7FF777776FFE0E1DEFFE4E7
          DFFFDADFD4FFD9DED3FFD7DCD1FFD5DACFFFCFD5CAFFC9CDC2FFBFC4BBFFB7BC
          B3FFB0B4AAFFAFB2A9FFFAFBF4FFFAFBF4FFFAFBF4FFF2F3EDFFE1E2DCFFC3C4
          C0FF787876D10101010300000000000000000000000000000000000000000000
          0000424242877C7C7CFF7C7C7CFF7B7B7BFFB0B0AFFF787877FFE0E1DEFFE3E7
          DFFFD9DED3FFD8DDD2FFD4DACEFFD0D5CAFFCBCFC4FFC1C7BDFFBABEB4FFB1B5
          ADFFA8ACA2FFA7ABA4FFFAFBF4FFFAFBF4FFF3F3EDFFE5E5E1FFC5C7C2FF7373
          71CA000000000000000000000000000000000000000000000000000000000000
          0000030303061F1F1F3F3D3D3D7B717171E7B0B0AEFF797979FFE0E0DFFFE2E6
          DEFFD5DACFFFCED3C8FFC9CEC1FFC1C8BCFFBCC1B8FFB4B8B0FFABB0A7FFABAE
          A5FFA2A69FFFA3A59FFFFAFBF4FFF4F5EEFFE9E9E5FFCCCDC9FF6F6F6CC00000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000606060C3ADAEAAFF7B7B7AFFE0E1DEFFE2E5
          DDFFD7DCD1FFD4D9CEFFCDD2C7FFC7CBC1FFBEC3BAFFB7BCB2FFAFB1A9FFA5A9
          A1FF9EA099FF9FA099FFF3F4EDFFEBEBE7FFCFD0CCFF686866B5000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000585858B17B7B7BF67E7E7EFFE0E1DFFFE2E6
          DEFFE0E4DCFFDDE1D9FFD9DDD6FFD4D7D1FFCED1CAFFCACCC4FFC2C4BFFFBBBD
          B8FFB6B8B3FFD8D9D6FFEDEEEBFFD1D2CEFF636362AC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000696969D2DDDEDCFFDDDE
          DCFFDDDEDCFFDCDDDBFFDADBDAFFDADAD8FFD7D8D6FFD6D6D5FFD4D5D2FFD2D2
          D2FFD1D1D0FFCFD0CFFFD4D5D2FF5A5A599C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000545454A8666666CC6666
          66CC666666CC666666CC666666CC666666CC666666CC666666CC666666CC6666
          66CC666666CC6B6C6BCE717170A4000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0008000000100000001300000012000000120000001200000010000000090000
          000200000000000000030000000B000000110000001300000012000000130000
          00120000000E0000000700000003000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000A0000001300000017000000160000001B0000001D0000002A000000270000
          001E0000001B0000001E000000270000002E000000300000002F0000002F0000
          002F0000002A000000220000001E0000001B0000001B0000001B0000001B0000
          001B0000001B0000001B00000011000000020000001100000000000000000000
          0004000000080100173A040256E801011D5E00000017323232B7424242E24A4A
          4AF5454545E8404040DA3D3D3DD63C3C3CD6363541DB0B0955FA252448E73C3C
          3CD63D3D3DD63E3E3ED53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F3FD53F3F
          3FD53F3F3FD53F3F3FD50B0B0B3A000000040000001600000000000000000000
          000001011E33060277ED3531B3FF040257F901011D55434343D8CDCEC7FFCFD0
          CAFFD1D2CEFFD2D2CEFFD0D1CDFFB6B7BEFF1B186FFF2B28A2FF242192FF7776
          96FFCECFC7FFD2D2CCFFD4D5D2FFD7D7D6FFD9D9D7FFDADAD8FFDADAD8FFDADA
          D8FFDADAD9FF6E6E6DFF0E0E0E2D000000000000000000000000000000000201
          23340804A5F03431C3FF5250DBFF3C38BBFF040257F92F2E4CE8CDCEC8FFC2C5
          B3FFC8CBBDFFCACDC0FFB0B2B2FF181579FF2D29A8FF4E4BD9FF423FC3FF231F
          93FF6F7090FFD2D5C4FFE0E2D8FFE9EBE6FFF0F0EDFFF2F4F0FFF5F5F1FFF5F6
          F1FFFCFCFAFF6E6E6EFF0E0E0E2D000000000000000000000000000000001412
          75AE231FBCFF4742CBFF3E39CBFF524FD1FF3D39B4FF060459FE8484A1FFB1B4
          A6FF9BA08FFF8A8C8EFF131175FF2E2AA9FF524ED1FF2A25C4FF504BD0FF2623
          9FFF191684FFBCBFB3FFCFD2CCFFDDDFDDFFE6E8E5FFEAEBEAFFECEDEAFFF0F0
          EBFFFBFCFAFF70706FFF0E0E0E2D000000000000000000000000000000001312
          324B2825B5FF2521BAFF4944C7FF3833C0FF514DCAFF3F3CB1FF08065CFF6464
          85FF777A7FFF120F6CFF2E2AA5FF524ECAFF2B25BEFF514DCBFF2823ABFF120F
          8CFF414098FFBEC1C0FFD2D2D3FFDCDEDFFFE4E6E6FFE9E9E9FFEAEBEAFFEDEE
          EAFFFBFBFAFF707070FF0F0F0F2D000000000000000000000000000000000202
          06094544B4EA2D2ABFFF2721B9FF4844C1FF3A35BCFF514DC4FF3E39AFFF0805
          66FF0D0A68FF2C299EFF534FC5FF2C25B7FF534EC5FF2723B0FF131098FF2E2C
          88FF636496FFBBBCC3FFC9C9D1FFD2D3D8FFD8D8DDFFDBDBDEFFE4E5E5FFECED
          EBFFFBFBFAFF727272FF0F0F0F2D000000000000000000000000000000000000
          00000E0E252D4E4CC8EA2F2CC3FF2521B8FF4A44C1FF3C37BBFF534FC3FF4F4B
          C1FF4D48BDFF514CC3FF2C25B5FF5451C4FF2824ADFF130F96FF2F2D8BFF4948
          90FFA8A8C0FFC8C9D6FFD4D4DEFFDADBE5FFE0E0E7FFE3E3E8FFE6E6E9FFE9EA
          EAFFFAFAFAFF737373FF0F0F0F2D000000000000000000000000000000000000
          0000000000000F0F272D4C4AC2EA2522A9FF2723BAFF4F4AC7FF3831BFFF231D
          B9FF2925BBFF1E17B7FF544EC9FF2925A7FF0E0B7FFF2C2A80FF474690FF9B9B
          B4FFBCBCCDFFCBCBDAFFD3D3E0FFD9D9E3FFDDDEE5FFE2E2E7FFE3E4E8FFE7E8
          E9FFF9F9F9FF747474FF1010102D000000000000000000000000000000000000
          000000000000000000000D0D222D302F79EA141288FF342EBFFF5A55CEFF251F
          BCFF2721BEFF413CC5FF4E48C0FF08056DFF1A1A50FF3D3D7BFF83839CFFA1A1
          B2FFB2B2C1FFBEBECFFFC9C9D5FFD0D0D9FFD3D3DAFFD5D5DBFFDEDFE2FFE5E6
          E7FFF9F9F9FF767676FF1010102D000000000000000000000000000000000000
          0000000000000000000000000000080729480B07A7FE5D58D0FF514ACEFF3D37
          C8FF3B36C7FF3E38C8FF6E69D5FF06027AFF222163FF757589FF8C8C9CFF9C9C
          ADFFADADBFFFBCBCCCFFC9C9D6FFD2D3DCFFD8D8DEFFDBDBE0FFE0E1E4FFE4E5
          E6FFF9F9F9FF787878FF1010102D000000000000000000000000000000000000
          000000000000000000000100152107039BE43F3AC5FF716DD8FF4A42CEFF635D
          D5FF7671DAFF6561D5FF7874DAFF5956C3FF060366FF434373FF838493FF9393
          A2FFA4A4B3FFB6B6C4FFC5C5D2FFD1D1DBFFD8D9E0FFDDDDE3FFDFE0E4FFE3E4
          E4FFF8F8F8FF797979FF1111112D000000000000000000000000000000000000
          0000000000000100142106038EE43F3AC4FF746EDAFF5D56D4FF7D76DDFF5D58
          D1FF423CC7FF7470D9FF746FDAFF847FDEFF625EC2FF05035AFF414170FF8586
          93FF9797A3FFAAAAB6FFBCBCC9FFCACAD4FFD4D4DCFFD8D8DDFFDDDEE1FFE2E3
          E4FFF8F8F8FF7A7A7AFF1111112D000000000000000000000000000000000000
          000001001621070396E4423EC7FF7B77DFFF615BD8FF847EE1FF403CC5FF0C08
          91FF2522ACFF3E39C4FF7975DBFF7F79DEFF8D89E1FF6761C4FF05035DFF4443
          71FF8D8D99FFA1A1ACFFB5B6C0FFC3C4CFFFCFCFD6FFD5D5DAFFDADBDEFFDDDE
          DFFFF6F6F6FF7C7C7CFF1111112D000000000000000000000000000000000100
          192407039DE4443FCBFF7E78E1FF6862DCFF8882E3FF423DC7FF0E0B98FF1817
          4CFF4140A9FF302DC1FF433EC5FF837EDEFF8F87E3FF9792E5FF706CCAFF0603
          6DFF4C4B81FFA5A5B0FFBABAC4FFCACAD2FFD3D3DAFFD8D8DDFFD8D9DCFFD8D9
          D9FFF5F5F5FF7D7D7DFF1212122D000000000000000000000000000000000806
          5C893631C5FF807AE4FF6864DEFF8A84E4FF4641C9FF110DA0FF222167FF5050
          76FF7878AAFF5857DAFF3632CCFF4944C8FF938CE2FF9791E5FFA09AE7FF4E4A
          B7FF161389FFACACB6FFBCBCC7FFC9C9D1FFD1D1D7FFD4D4D7FFD3D4D5FFD2D2
          D2FFF3F3F3FF7F7F7FFF1212122D000000000000000000000000000000000B0B
          1F33211EB4FB3B36C5FF8D88E7FF4640C9FF130FA5FF2A2A7EFF8585A8FFACAC
          BFFFA9A9BFFF8E8DC2FF615FE9FF3834D4FF4D48C9FF9994E3FF5651CCFF110C
          A5FF434295FFABABB6FFB8B8C1FFBFBFC8FFC2C2C9FFC4C5C9FFC9C9CBFFCBCB
          CBFFF1F1F1FF808080FF1212122D000000000000000000000000000000000000
          00002827677E2724C2FF3733C3FF130FA6FF2F2D8AFF585879FFCFCFD4FFC4C4
          D4FFB4B4CAFFB7B7CEFF9898CCFF6563F7FF413EDBFF3E39C2FF110CA9FF3431
          92FF9393AEFFB9BAC4FFC0C1CAFFC5C7CDFFC8C8CCFFC5C7C9FFC3C4C4FFC0C0
          C0FFEFEFEEFF818181FF1313132D000000000000000000000000000000000000
          0000000000002D2C747E3633BDFF2F2D89FF5F5E80FF727272FFD6D6DAFFCBCB
          DAFFBABACEFFBBBBD0FFBABACEFF9B9ACCFF706EFCFF4F4CD4FF343392FF9797
          B2FFB8B8C2FFBEBEC7FFC1C1C8FFC1C1C7FFBFC0C3FFBCBDBFFFBBBBBBFFB9BA
          B8FFEDEDECFF828282FF1313132D000000000000000000000000000000000000
          00000000000003030306636398FA595979FF767676FF717171FFD8D8DBFFCFCF
          DCFFBBBBCEFFB6B6C8FFB5B5C7FFB5B5C4FF9696C3FF6E6CC0FF9292ADFFB3B3
          BCFFB4B5BCFFB6B6BCFFB5B5BAFFB4B4B6FFAFB0B2FFAFAFAFFFB0B0AFFFAEAF
          ADFFEAEAEAFF848484FF1313132D000000000000000000000000000000000000
          0000000000001B1B1B3C737373FF8B8B8BFF757575FF707070FFDADADCFFD3D3
          DDFFC1C1D0FFBEBECDFFBEBECDFFBCBDCBFFBCBDC9FFBCBCC9FFBBBBC5FFBCBC
          C4FFB8B9C0FFB7B7BCFFB4B4B7FFAFB0B1FFACADAEFFA9A9A9FFA7A8A6FFA6A6
          A4FFE8E8E7FF858585FF1414142D000000000000000000000000000000000000
          0000000000003A3A3A7E757575FF9F9F9EFF838382FF717171FFDADADCFFD4D4
          DDFFC4C4D1FFC3C3D0FFC2C2CFFFC2C2CEFFC1C1CCFFC0C0CAFFBFC0C8FFBBBC
          C3FFB9B9BEFFB5B5B8FFAEAEB1FFABACADFFA7A7A7FFA2A3A2FF9E9F9EFF9C9C
          9AFFE6E6E5FF868686FF1414142D000000000000000000000000000000000000
          0000000000005B5B5BC3777777FFAFAFAEFF90908FFF737373FFDBDBDDFFD6D6
          DCFFC5C7CFFFC2C3CDFFC1C2CCFFC1C1CAFFC0C0C8FFBDBEC5FFBABABFFFB8B8
          BDFFB3B3B7FFADAEB0FFA8A9AAFFA3A3A3FF9E9E9DFF9A9B99FF979895FFAEAF
          ADFFE4E4E3FF878787FF1414142D000000000000000000000000000000000000
          000004040409767676F9797979FFB8B8B6FF9D9D9CFF757575FFDCDCDDFFD8D8
          DCFFC7C8CEFFC2C2C9FFC1C1C7FFC0C0C4FFBEBEC3FFBABABFFFB5B5B8FFB2B3
          B6FFADAEB0FFB0B1B0FFFAFBF4FFFAFBF4FFFAFBF4FFFAFBF4FFF2F3ECFFE1E1
          DCFFC3C4BFFF7C7C7AD803030306000000000000000000000000000000000000
          0000222222487A7A7AFF969695FFA1A1A0FFA8A8A7FF777776FFDDDDDDFFD9D9
          DCFFCCCCCFFFCCCCCFFFC9C9CDFFC8C8CAFFC2C2C7FFBCBDBEFFB5B6B7FFAFB0
          B0FFA9A9A9FFA9A9A9FFFAFBF4FFFAFBF4FFFAFBF4FFF2F3EDFFE1E2DCFFC3C4
          C0FF787876D10101010300000000000000000000000000000000000000000000
          0000424242877C7C7CFF7C7C7CFF7B7B7BFFB0B0AFFF787877FFDEDEDEFFDADA
          DCFFCDCDCFFFCDCDCFFFC9CACCFFC5C7C8FFC0C1C2FFBABABAFFB3B3B3FFABAB
          ABFFA2A2A2FFA3A4A1FFFAFBF4FFFAFBF4FFF3F3EDFFE5E5E1FFC5C7C2FF7373
          71CA000000000000000000000000000000000000000000000000000000000000
          0000030303061F1F1F3F3D3D3D7B717171E7B0B0AEFF797979FFDEDEDEFFDCDC
          DCFFCCCCCCFFC4C4C4FFBFBFBFFFBCBCBCFFB6B6B6FFAFAFAEFFA6A7A5FFA5A5
          A4FF9FA09DFFA0A09EFFFAFBF4FFF4F5EEFFE9E9E5FFCCCDC9FF6F6F6CC00000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000606060C3ADAEAAFF7B7B7AFFDEDEDEFFDCDD
          DCFFD0D1CFFFCDCECCFFC5C7C4FFC0C1BFFFBABBB8FFB3B4B1FFA9AAA8FFA2A2
          9FFF9A9B98FF9C9D99FFF3F4EDFFEBEBE7FFCFD0CCFF686866B5000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000585858B17B7B7BF67E7E7EFFDFDFDFFFDFDF
          DDFFDDDEDBFFDADAD8FFD6D7D5FFD1D2D0FFCCCCCAFFC7C8C4FFC0C0BEFFBABA
          B7FFB5B5B3FFD7D7D5FFEDEEEBFFD1D2CEFF636362AC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000696969D2DDDDDCFFDDDD
          DCFFDCDDDCFFDBDCDBFFDADADAFFD9D9D8FFD7D7D6FFD5D6D5FFD3D4D2FFD2D2
          D2FFD0D1CFFFCFCFCFFFD4D5D2FF5A5A599C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000545454A8666666CC6666
          66CC666666CC666666CC666666CC666666CC666666CC666666CC666666CC6666
          66CC666666CC6B6C6BCE717170A4000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000F0000
          002C000000450000004C0000004B0000004B0000004B0000004B0000004B0000
          004B0000004B0000004B0000004B0000004B0000004B0000004B0000004B0000
          004B0000004B0000004B0000004B0000004B0000004B0000004B0000004B0000
          004B0000004B0000004B0000004B0000004D000000460000001F0001021B0128
          378F013D54DE014057F201435CFF014058FF014058FF014058FF014058FF0140
          58FF014058FF014058FF014058FF014058FF014058FF014058FF014058FF0140
          58FF014058FF014058FF014058FF014058FF014058FF014058FF014058FF0140
          58FF014058FF013E56FF01364BF8012837E401151EB60003043E0257788C0288
          D3FF0280E4FF0288F1FF0290FFFF0293FFFF0299FFFF03A3FFFF03AAFFFF03B3
          FFFF03BAFFFF03C8FFFF04D1FFFF04D9FFFF05DFFFFF05E4FFFF0DE6FFFF05E3
          FFFF05DDFFFF04D6FFFF03CCFFFF02BCFFFF03B7FFFF03AEFFFF02A3FFFF029C
          FFFF0297FFFF028EFFFF0288F7FF0279DBFF01609EFF012837BC1ABBF9F9048E
          FFFF048CFFFF0591FFFF0593FFFF0595FFFF059CFFFF05A5FFFF05AAFFFF05B1
          FFFF05BBFFFF06C4FFFF08D3FFFF09D9FFFF27DFFFFF37E3FFFF3EE3FFFF39E1
          FFFF22DEFFFF09D9FFFF08CEFFFF06C1FFFF05B6FFFF06B1FFFF05A3FFFF059C
          FFFF059BFFFF0593FFFF058CFFFF0591FFFF048BFFFF02608FFF52CEFCFC0790
          FFFF0792FFFF0894FFFF0894FFFF0898FFFF0799FFFF08A4FFFF08AAFFFF08B1
          FFFF0ABBFFFF0CC8FFFF0ECDFFFF32DAFFFF5DE0FFFF73E2FFFF75E3FFFF6BE0
          FFFF4BDCFFFF28D7FFFF0FCDFFFF0CC2FFFF0AB8FFFF09AEFFFF08A5FFFF08A0
          FFFF089BFFFF0896FFFF0791FFFF0791FFFF0791FFFF0380BBFF55A3C0C029A9
          FFFF0A93FFFF0B97FFFF0C9DFFFF0CA7FFFF0BABFFFF0CB0FFFF0DB9FFFF0DC1
          FFFF0FCCFFFF14DBFFFF20E6FFFF5AEFFFFF90F6FFFFB6F8FFFFC1FAFFFFADF8
          FFFF89F4FFFF5FEDFFFF24E2FFFF13D6FFFF0FCBFFFF0DBDFFFF0CB4FFFF0BAD
          FFFF0BA6FFFF0A9CFFFF0B95FFFF0B95FFFF0996FAFF0289BCEA26414B4B5EC8
          FFFF0E98FFFF0D98FFFF0F98FFFF0E9FFFFF0EAAFFFF11B3FFFF10B7FFFF12C1
          FFFF13CBFFFF1BD9FFFF3EE5FFFF76EFFFFFB8F0F7FFE6E8E9FFEFEFEFFFE2F5
          F8FFA5F5FFFF78EEFFFF39E3FFFF1AD5FFFF13C5FFFF12BCFFFF0FB3FFFF0DAA
          FFFF0EA2FFFF0E96FFFF0E98FFFF0F9AFFFF08A0F1FF014C6978000000005E9D
          B4B43DB3FFFF0F98FFFF0F98FFFF139EFFFF14AFFFFF14B3FFFF0EB1FFFF15BB
          FFFF17C5FFFF22D4FFFF37E1FFFF82EDFFFFB2BABBFF6C6964FF524B42FFE0DF
          DEFFB0F2FEFF70E9FFFF40DEFFFF22D2FFFF1AC5FFFF13B8FFFF12B1FFFF10AA
          FFFF10A0FFFF119BFFFF119BFFFF0E9FFAFF0396CFE40006080900000000182A
          303080DAFCFC139CFFFF139BFFFF159EFFFF15A8FFFF17B2FFFF16B5FFFF1ABD
          FFFF1FC5FFFF2CD2FFFF3DDCFFFF77E9FFFF738184FF4E4A41FF4D4637FFD4D3
          D2FFA7F0FDFF70E7FFFF33D9FFFF29CFFFFF1EC1FFFF1ABAFFFF18B5FFFF15A8
          FFFF129BFFFF16A0FFFF139DFFFF09A5F2FF013C535A00000000000000000000
          00004C8296964DBFFFFF1AA2FFFF1AA4FFFF15A0FFFF1CB2FFFF1FB8FFFF20BD
          FFFF24C3FFFF2ECDFFFF3AD8FFFF61E1FFFF76BACCFF4F5455FF727272FFA2CB
          D4FF83E7FFFF5FDFFFFF38D6FFFF2CCAFFFF22C0FFFF1DB8FFFF1AB2FFFF17A3
          FFFF19A2FFFF169FFFFF0EA6FBFF0286B8C30000000000000000000000000000
          0000091012127ACEEDED38B3FFFF27A9FFFF25A8FFFF2CB2FFFF34BEFFFF31BE
          FFFF33C1FFFF42CCFFFF52D6FFFF61DDFFFF73E3FFFF81E7FFFF89E7FFFF7CE4
          FFFF64DFFFFF47D8FFFF3BCEFFFF2BC3FFFF20B9FFFF1FB8FFFF1EACFFFF18A0
          FFFF1BA3FFFF1AA5FFFF03A8E8FC01212D300000000000000000000000000000
          0000000000003A5F6D6D63CBFFFF32B0FFFF33B1FFFF37B3FFFF3EBFFFFF3FC4
          FFFF3FC4FFFF48CAFFFF53D0FFFF63D9FFFF6DDCFFFF97D9EDFFB7E4F0FF7CE2
          FFFF75DEFFFF69D8FFFF54CEFFFF36C3FFFF27BCFFFF22B7FFFF22ACFFFF1EA7
          FFFF1FA7FFFF12A8F2FF02628796000000000000000000000000000000000000
          0000000000000203030374B8D2D254C2FFFF3DB6FFFF40B7FFFF45BEFFFF4DCB
          FFFF4BCAFFFF4DCBFFFF54CFFFFF59D1FFFF65D6FFFF95C8DAFFCCE2E9FF70DA
          FFFF6FD8FFFF63D2FFFF67D2FFFF6DD3FFFF6FD4FFFF5CC8FFFF31B1FFFF24AB
          FFFF22ADFCFF039FDBF3000E1315000000000000000000000000000000000000
          000000000000000000002C454E4E7BD6FFFF47BCFFFF4ABCFFFF4BBDFFFF4AC4
          FFFF51CEFFFF58CFFFFF5CD1FFFF5BD0FFFF5DD1FFFFA1C7D5FFDDE4E7FF70D8
          FFFF67D4FFFF68D3FFFF6FD6FFFF67D4FFFF75D5FFFF75CDFFFF7DD1FFFF7ACF
          FFFF3FBCF5FF014E6C7800000000000000000000000000000000000000000000
          00000000000000000000000000006FA7BCBC71D1FFFF54C1FFFF4FC0FFFF55C5
          FFFF5ED3FFFF67D6FFFF64D6FFFF67D7FFFF69D5FFFFB9C0C3FFC2BFB9FF86DB
          FBFF6ED7FFFF75D9FFFF73D9FFFF76D9FFFF7BD4FFFF7FD2FFFF81D2FFFF7AD1
          FCFF6DC2E1EA060A0C0C00000000000000000000000000000000000000000000
          00000000000000000000000000001824292995DFFAFA5ECAFFFF5EC9FFFF63CA
          FFFF60D0FFFF69D9FFFF68D8FFFF66D8FFFF77D7F8FFB3B3B1FF9C988EFFA8E2
          F6FF7EDEFFFF7EDEFFFF7EDEFFFF7DDAFFFF80D4FFFF80D5FFFF89D7FFFF77D2
          F7FF214D5E620000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000598494947FD9FFFF6ECFFFFF6ACF
          FFFF64CDFFFF69DAFFFF79DFFFFF79E0FFFF86CADFFF8C8985FF7F7A70FFBBE6
          F4FF86E3FFFF88E2FFFF8AE4FFFF8ADBFFFF8CDAFFFF92DBFFFF85D7FCFF53A6
          C4CC000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000090D0F0F92D2EAEA76D5FFFF75D3
          FFFF72D4FFFF71D9FFFF7AE2FFFF85E4FFFF8EBCCAFF686661FF474238FFD8EA
          EFFF90E6FFFF89E5FFFF8DE1FFFF94DEFFFF92DDFFFF96DFFFFF78CFF0FD132B
          3437000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000476874748FE0FFFF7AD7
          FFFF7ED8FFFF73D7FFFF7CE1FFFF8BE7FFFF97AFB5FF423F3BFF17130DFFDCDC
          DBFF95E9FEFF96E8FFFF96E0FFFF99E0FFFF92E0FFFF8ADAF9FF4B8FA9B40000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000304040490C6DBDB92E0
          FFFF80DBFFFF89DDFFFF85E0FFFF8EEBFFFF868C8DFF1A1815FF13100BFFBBBA
          B9FFA9ECFAFF9FE8FFFF9DE3FFFF9DE3FFFF9CE1FDFF79CCEAF50914191A0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000032454C4CA5E7
          FFFF8EE1FFFF97E1FFFF88E1FFFF95E9FFFF67787BFF2B2B2AFF201F1EFFADAD
          ACFFAFEBF9FFA5E6FFFFA1E6FFFFA7E7FFFF93DEF9FF376D8189000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000080AC
          BCBCA3E7FFFF9BE5FFFF9BE5FFFF9DE7FFFF8BC1CCFF4F5455FF6D6D6DFF9DC9
          D0FFA9ECFFFFA8E9FFFFA5E8FFFF9EE4FCFF71BEDBE402040505000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001822
          2525B0E5F8F8A1E8FFFFA2E8FFFF9BE8FFFFA4EFFFFFACF5FFFFA8F4FFFFABF1
          FFFFA9EBFFFFAEECFFFFB2ECFFFF95DDF7FF254B595D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00006C919E9EB2ECFFFFA3EBFFFFA4EBFFFFAEECFFFFABF4FFFFB0F7FFFFB3EF
          FFFFB1EEFFFFB2EEFFFFA5E6FCFF68AFCBD20000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000C101212B1DFF0F0B1EEFFFFA7EDFFFFB4EEFFFFB7F3FFFFB9F5FFFFB5EF
          FFFFB7F0FFFFB3EFFFFF90D8F3FC152B33360000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000556F7878BEEFFFFFACF0FFFFABF0FFFFAEF1FFFFABF1FFFFBAF1
          FFFFB7F2FFFFA8E6FAFF518DA4AD000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A6CDDCDCC0F2FFFFACF2FFFFB7F3FFFFB4F2FFFFBEF3
          FFFFB7EEFDFF8AD1EAF3070F1213000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000031414646CAF0FEFEBBF4FFFFBCF4FFFFC2F4FFFFBBF4
          FFFFAAE8FDFF37687A7E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000809DA7A7CEF4FFFFC8F5FFFFC0F5FFFFB5EE
          FFFF6FB6D0D00000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000072878F8FBCDBE7E7C4EBFAFA75A8
          BABA05090A0A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101020A0A0A100606
          0609000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001717172374726FA2989690CA7575
          73AF363636570202020400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000002827252DC0BBB1DFABA69DFF8D8A82FFC6C0
          B7FDADAAA4E04443436C02020204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002827252DC5C0B8E17E7B75FF302F2CFF2E2D2BFF3E3C
          39FFB5B0A7FEAFACA6E04443436C020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000002828262DC7C2B9E17F7C76FF302F2DFF2E2D2BFF2E2D2BFF2E2D
          2BFF3B3A38FFB5B2AAFEB0ADA7E04443436C0202020400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002928262DC7C3BBE1807D78FF31302EFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF3C3B39FFB7B3ABFEB0ADA8E04443436C02020204000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002928
          262DC7C4BBE1807E78FF31302EFF2F2E2DFF2F2F2DFF2F2F2DFF302F2DFF2F2F
          2DFF2F2F2DFF2F2E2DFF3C3B39FFB7B4ACFEB0ADA8E04443436C020202040000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002828262DC7C3
          BAE1807E78FF31302EFF2F2F2DFF302F2EFF302F2EFF30302EFF30302EFF302F
          2EFF302F2EFF2F2F2DFF2F2E2DFF3C3B39FFB7B4ACFEB0ADA8E04443436C0202
          0204000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002827252DC7C2B8E1807D
          78FF31302EFF2F2F2DFF302F2EFF30302EFF30302FFF31302FFF31302FFF3130
          2FFF30302FFF302F2EFF302F2DFF2F2E2DFF3C3B39FFB6B2ABFEAFACA7E04443
          436C020202040000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000023232128C5C0B7E17F7C76FF3130
          2EFF2F2E2DFF302F2DFF30302EFF323130FF323231FF313130FF313130FF3131
          30FF353433FF30302FFF302F2EFF2F2F2DFF2F2E2CFF3B3A38FFB5B1A8FEADAA
          A5E0373737580000000000000000000000000000000000000000000000000000
          000000000000000000000000000002020202A4A097BB807D77FF302F2DFF2F2E
          2CFF2F2F2DFF302F2EFF313130FF737271FF4F4E4DFF323231FF323231FF3231
          31FF91918EFF484746FF30302EFF302F2EFF2F2E2DFF2E2E2CFF3E3D39FFC6C1
          B8FD7A7977B30606060A00000000000000000000000000000000000000000000
          000000000000000000000000000012121115BEBAAFE6403F3BFF2E2D2BFF2F2E
          2CFF2F2F2DFF31302FFF72716FFFEEECE9FF565554FF323232FF323232FF3232
          31FFB4B3B1FFD2D0CDFF474746FF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF8F8B
          84FF9A9792CB0A0A0A1000000000000000000000000000000000000000000000
          00000000000000000000000000000A09090BBAB6ACDA504E4AFF2E2D2BFF2F2E
          2CFF30302EFF71706DFFCFCEC9E1E4E2DFEC565654FF323232FF333332FF3232
          32FFB4B3B1FFDCDBD7F2CDCBC6FD474644FF2F2E2DFF2F2E2CFF302F2DFFABA7
          9EFF7A7875A60202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000066645F74C1BDB3FC504F4BFF4241
          3EFF7E7D78FFCDCAC4E12B2A292DC9C7C4D1555554FF323231FF323231FF3232
          31FFB3B2AFFF919190C083828096CBC8C2FC52514EFF42403EFF7B7872FFC0BB
          B2DF1D1C1B240000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000404040568656074C1BDB4DDC5C1
          BAE6AFACA6BE2525242800000000C7C6C2D1555453FF313130FF313130FF3131
          30FFB2B0ADFF8B8B8ABB0909090E6C6A6774C6C2BBDDC4C0B8E6AAA69FBE2423
          2128000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101010D0C0C0E1313
          1215040403040000000000000000C6C4BFD1545351FF31302FFF313030FF3130
          2FFFB0AEABFF8B8A88BB06060609010101010D0D0C0E13131215040403040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C4C1BCD1535250FF30302EFF30302FFF3030
          2EFFAEACA7FF8B8988BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C1BFB8D152514EFF302F2EFF302F2EFF302F
          2DFFADAAA4FF8A8886BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C0BDB5D1514F4CFF2F2E2DFF2F2E2DFF2F2E
          2DFFAAA7A1FF888885BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000BDB9B1D1504E4BFF2E2E2CFF2E2E2CFF2E2E
          2CFFABA8A1FF838280B502020204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000ABA79FBF66655FFF2E2D2BFF2E2D2BFF3533
          31FFC6C1B8FE5251507700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000003D3C3845C6C0B7F465635DFF504F4BFF9B97
          8FFEA7A39AC10C0C0B1000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000020202023D3B3745A9A49AC0BBB6ABD57E7A
          738F11100F130000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001010101040404050000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000060606090A0A0A100101
          0102000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000040404054443426292908BC28B8A86C25E5D
          5D921313131E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000040404057C79738EC8C3B8FB8E8B83FFABA79EFFCDC9
          BFF6747372AE0B0B0B1100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000004040405807D778FC2BDB4FC43423FFF2E2D2BFF302F2DFF7B78
          73FFC6C3BBEF2D2D2D4800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000004040405807E778FC3BFB7FC444240FF2E2E2CFF2F2E2CFF2F2E2CFF4241
          3EFFCDCAC1F93F3E3E6000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000504
          0405817E788FC4C0B8FC454341FF2F2E2CFF2F2E2DFF2F2E2DFF2F2E2DFF514F
          4DFFD0CDC6F21D1D1C2C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000004040405807E
          788FC4C0B8FC454341FF2F2E2DFF2F2F2DFF302F2EFF302F2EFF3E3D3BFFC0BD
          B8FC777673870202020300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004040405807E778FC4BF
          B7FC454341FF2F2E2DFF2F2F2DFF302F2EFF30302EFF3F3E3DFFC0BEBAFC8786
          838F050505050000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404057F7C768FC3BEB5FC4543
          41FF2F2E2CFF2F2F2DFF302F2EFF30302FFF3F3F3EFFC4C3BFFFCECDCAEB8080
          7FAD757574A8757473A8747473A8747372A8737371A8737271A86F6F6DA54544
          446C0808080D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000040303047C79728DC1BDB3FC44423FFF2F2E
          2CFF2F2E2DFF302F2EFF30302FFF353534FFA1A09EFFD3D2CFFFD4D3D0FFD3D1
          CFFFD1D0CCFFD0CDC9FFCDCBC5FFCBC8C2FFC9C5BEFFC6C2BAFFC8C4BAFEC1BD
          B4ED5C5C5B900404040700000000000000000000000000000000000000000000
          00000000000000000000000000002928262FC4C0B5F843423EFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF313130FF323131FF323231FF323232FF3232
          31FF313130FF31302FFF30302EFF302F2EFF2F2E2DFF2E2E2CFF353431FF9C98
          90FFACA9A3DF1515152200000000000000000000000000000000000000000000
          0000000000000000000000000000696760788D8A82FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF313130FF323231FF323232FF333332FF3232
          32FF323131FF313130FF30302FFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF4D4B
          47FFC9C4BAF41E1E1E3000000000000000000000000000000000000000000000
          00000000000000000000000000004C4A4556A9A59CFE302F2CFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF313130FF323131FF323232FF323232FF3232
          31FF313130FF313030FF30302FFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF6562
          5DFFB9B5ACE10D0D0D1500000000000000000000000000000000000000000000
          000000000000000000000000000012121115BDB9AEDA7E7B74FF302F2DFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF323131FF4E4E4DFF565554FF565654FF5655
          54FF555453FF545351FF535250FF52514EFF51504DFF504E4CFF676460FFC7C2
          B8F5454441560101010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000002827252DC5C0B7E17F7C76FF3130
          2EFF2F2E2DFF302F2DFF302F2EFF313130FF737371FFEEECE8FFF0EEEBFCDCDB
          D8EDD3D1CCDCD1CECADCCECCC6DCCCC9C3DCCAC6BFDCC6C3BADBB3AFA6C73D3C
          3945020202020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002827252DC7C2B8E17F7D
          78FF31302EFF2F2E2DFF302F2EFF302F2EFF313130FF73726FFFE1DFDAFA8685
          85BC1F1F1E2D0A0A0A0B0A0A0A0B0A0A0A0B0A0A0A0B0909090A010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002828262DC7C3
          BAE1807E78FF31302EFF2F2E2DFF302F2DFF302F2EFF31302FFF716F6DFFD9D7
          D1F8757574AE0B0B0B1100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002828
          262DC7C4BBE1807D78FF31302EFF2F2E2DFF2F2F2DFF2F2F2DFF30302EFF7E7D
          78FFCAC9C2EF2D2D2D4800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002828262DC7C3BAE1807D77FF31302EFF2F2E2CFF2F2E2CFF2F2E2CFF4241
          3EFFCECBC3F93F3E3E6000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000002827252DC7C2B8E17F7C76FF302F2DFF2E2D2BFF2E2D2BFF4F4E
          4AFFCCC8C0F21D1D1C2C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002827252DC5C0B7E1807D77FF403F3BFF4E4D49FFBAB6
          ADFC73706B870202020300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000023222028A7A299BEBDB9AFE6BDB9AEDD6664
          5E74040404050000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304121211150C0C0B0E0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000060606090A0A
          0A10010101020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404054443426293908BC28B8A
          86C25E5D5D921313131E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000040303047C79738DC8C3B9FB8E8B83FFABA7
          9EFFCDC8BFF6797776B315151522000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000002A29272FC8C3BBF8434240FF2E2D2BFF302F
          2DFF6C6A65FFCFCBC3F8797877B3151515220000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000006D6B6778928F89FF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2DFF6C6A65FFD1CCC4F8797877B31515152200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000004F4E4B56B1AFA8FE31312FFF2F2E2DFF2F2E
          2DFF2F2E2CFF302F2DFF6D6B66FFD2CDC5F8797877B315151522000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000014131315C8C5BFDA84827FFF323230FF302F
          2EFF2F2F2DFF2F2F2DFF302F2DFF6D6C67FFD2CDC5F8797877B3151515220000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002B2A292DD2CFCBE1868582FF3332
          31FF302F2EFF302F2EFF2F2F2DFF302F2DFF6D6B67FFD1CDC4F8797877B31515
          1522000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010101012726263A6968679A7373
          71A8737371A8747373A8747473A8757473A8757574A8999997C7EBE9E5FC8786
          84FF333231FF30302EFF302F2EFF2F2F2DFF302F2DFF6C6B65FFCFCBC3F87977
          76B3151515220000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101014A484455C9C5BBF2C5C1B8FFC7C4
          BCFFC9C6BFFFCBC9C4FFCECCC7FFD0CECAFFD2D0CDFFD3D2CFFFD4D3D0FFCFCE
          CBFF686766FF31302FFF30302EFF302F2DFF2F2E2DFF2F2F2DFF6C6964FFCEC9
          C0F8737371AE0B0B0B1100000000000000000000000000000000000000000000
          00000000000000000000000000001817161BC3BFB4EA524F4CFF2F2E2CFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF313130FF313131FF323231FF323232FF3232
          31FF313130FF313030FF30302FFF302F2EFF2F2E2DFF2F2E2CFF2F2E2CFF7976
          6FFFC3BEB5EF2D2D2D4800000000000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB1FE2F2D2BFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF313130FF323131FF323232FF333333FF3232
          32FF323131FF313130FF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2D2BFF403E
          3BFFC7C2B8F93E3E3E6000000000000000000000000000000000000000000000
          000000000000000000000000000024232129C3BEB3FA353330FF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF313130FF323131FF323232FF323232FF3232
          31FF323131FF313130FF30302FFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF4E4C
          48FFC7C3B8F21C1C1C2C00000000000000000000000000000000000000000000
          000000000000000000000000000008080709A39E96BA9C9890FE52504DFF514F
          4CFF51504EFF53514FFF535351FF545352FF555453FF565554FF565654FF5655
          54FF40403FFF31302FFF30302EFF302F2EFF2F2E2DFF2E2E2CFF3B3A38FFB6B2
          A8FC726F69870202020300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000F0F0E117E7A748DBEBBB2D4C8C5
          BDDCCBC7C0DCCDCAC4DCCFCCC7DCD1CFCBDCD3D1CDDCD9D7D4E1F4F3EFFDD3D1
          CEFF484746FF30302FFF302F2EFF2F2F2DFF2F2E2CFF3B3B38FFB5B1A8FC7F7C
          758F040404050000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050505060A0A
          090B0A0A0A0B0A0A0A0B0A0A0A0B0A0A0A0B1010101193918E9ACFCDC9FD4747
          46FF30302FFF302F2EFF2F2F2DFF2F2E2DFF3C3B39FFB6B2AAFC807D778F0404
          0405000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000404040484827F8DCCC9C4FC474645FF302F
          2EFF302F2EFF2F2F2DFF2F2E2DFF3C3B39FFB7B3ABFC807E788F040404050000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000002B2B292FCECBC4F8464543FF2F2F2DFF2F2F
          2DFF2F2E2DFF2F2E2CFF3C3B39FFB7B4ABFC817E788F05040405000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000006E6B677893908BFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF3C3A39FFB6B3AAFC807E788F0504040500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000004E4C4956ADAAA2FE302F2DFF2E2D2BFF2E2D
          2BFF3B3A38FFB5B1A9FC807E778F040404050000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000013121115BFBBB2DA807D78FF403F3BFF4E4D
          49FFB6B2A9FC7F7C768F04040405000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000023232028A7A299BEBDB9AFE6BDB8
          AEDD66645E740404040500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004030304121211150C0C
          0B0E010101010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000A0A0A0F4E4D4C74706F6DA66262
          6196202020330101010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000F0F0E11A49F97BEC4BFB5FDC1BCB2FFCCC8
          BCFA92908CCC2020203300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000007E7C758E9C9991FE353330FF2F2E2BFF5250
          4CFFCFCAC0FA6363629701010101000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000BBB7AFCF52514DFF2E2D2BFF2E2D2BFF2F2E
          2CFFB7B3ABFF7E7E7CB505050508000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000BFBBB3D1514F4CFF2F2E2CFF2F2E2CFF2F2E
          2CFFA9A6A0FF888785BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C1BEB8D151514EFF2F2F2DFF2F2F2DFF2F2F
          2DFFACA9A3FF898886BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C3C1BBD153514FFF302F2EFF302F2EFF302F
          2EFFAEABA6FF8A8987BB06060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010101020A0A
          0A10060606090000000000000000C5C3BED1535351FF31302FFF31302FFF3030
          2FFFAFAEAAFF8B8A88BB0606060900000000010101020A0A0A10060606090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000017171723767572A29B9A
          97CA777675AF3737365702020204C7C5C1D1545352FF313130FF313130FF3131
          30FFB1B0ADFF8B8B89BB0606060918181723777674A29B9A96CA767574AF3636
          3657020202040000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000023232128C1BDB4DFAEABA3FF9390
          8AFFCFCCC5FDB4B2AFE04444446CCAC8C5D5555453FF323131FF323231FF3231
          31FFB3B1AEFF8B8B8ABB2A292936CAC7C2DFB2AFA9FF928F89FFC9C5BCFDADAB
          A4DF373737580000000000000000000000000000000000000000000000000000
          000000000000000000000000000002020202A4A098BB817D77FF302F2DFF2F2E
          2CFF403F3DFFBEBCB7FEB6B5B1E0DCDBD8F2565554FF323232FF323232FF3232
          31FFB4B3B0FFAFAEACD7D2D0CCE684837FFF31302FFF2F2E2CFF413F3CFFC7C2
          B9FD7A7977B30606060A00000000000000000000000000000000000000000000
          000000000000000000000000000012121115BEBAAFE6403F3BFF2E2D2BFF2F2E
          2CFF2F2F2DFF3F3E3CFFC1BFBBFEF4F2EFFE565654FF323232FF333332FF3232
          32FFB4B3B1FFEFEDE9FE868582FF323130FF2F2E2DFF2F2E2CFF2E2D2BFF8F8B
          84FF9A9792CB0A0A0A1000000000000000000000000000000000000000000000
          00000000000000000000000000000A09090BBAB5ACDA504D4AFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF3F3F3DFFC5C3C0FF555453FF323231FF323232FF3232
          31FFB0AEACFF878684FF323231FF302F2EFF2F2E2DFF2F2E2CFF302F2DFFABA7
          9EFF7A7875A60202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000066645E74C1BDB2FC44423FFF2F2E
          2CFF2F2E2DFF302F2EFF30302EFF3F3F3EFF3C3B3AFF313130FF323131FF3131
          30FF535352FF333231FF302F2EFF2F2F2DFF2F2E2CFF2F2E2DFF6C6A64FFC0BB
          B2DF1D1C1B240000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404057F7C768FC2BEB5FC4443
          40FF2F2E2CFF2F2F2DFF302F2EFF30302FFF31302FFF313030FF313130FF3130
          2FFF31302FFF30302EFF302F2EFF2F2E2DFF302F2DFF6C6A65FFC3BFB5E12827
          252D000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004040405807D778FC4BF
          B7FC454341FF2F2E2DFF2F2F2DFF302F2EFF30302EFF30302FFF30302FFF3030
          2FFF302F2EFF302F2EFF2F2F2DFF302F2DFF6D6B66FFC4BFB7E12828262D0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000004040405807E
          788FC4C0B8FC454341FF2F2E2DFF2F2F2DFF302F2DFF302F2EFF302F2EFF302F
          2EFF2F2F2DFF2F2E2DFF302F2DFF6D6C66FFC5C0B8E12928262D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000404
          0405807E788FC4C0B8FC454341FF2F2E2CFF2F2E2DFF2F2E2DFF2F2E2DFF2F2E
          2DFF2F2E2CFF302F2DFF6D6B66FFC5C1B8E12928262D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000004040405807E778FC3BFB6FC444240FF2E2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2DFF6C6A65FFC4C0B7E12828262D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000004040405807C768FC2BDB4FC43423FFF2E2D2BFF2E2D2BFF2F2E
          2CFF6C6964FFC3BFB5E12827252D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000040404057F7B758FC0BBB1FC4F4D49FF3F3E3BFF7976
          6FFFC1BDB2E12827252D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000404040566635D74BCB8ACDDBDB8ADE6A6A2
          98BE232220280000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010101010C0C0B0E121211150403
          0304000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2DFF2F2E2DFF2F2F2DFF2F2F
          2DFF2F2E2DFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2DFF2F2F2DFF302F2DFF41403EFF53514FFF4E4D
          4BFF333231FF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF302F2EFF302F2EFFAAA8A4FFF2EFEAFFEDEB
          E5FF50504DFF302F2EFF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF302F2EFF30302EFF31302FFFB1B0ADFFF5F3EFFFF4F2
          EEFF545352FF30302FFF302F2EFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF302F2EFF30302EFF31302FFF313130FFB3B2B0FFF7F6F3FFF7F6
          F2FF555553FF31302FFF30302FFF302F2EFF2F2F2DFF2F2E2DFF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF2F2E
          2CFF2F2F2DFF41403EFF535351FF555353FF555553FFC1C0BEFFFAF9F7FFFAF9
          F7FF737372FF555553FF545352FF4F4E4CFF333231FF2F2E2DFF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF2F2E
          2CFF2F2F2DFFA9A7A2FFF2F0EBFFF5F4F0FFF8F7F4FFFBFAF8FFFDFCFBFFFCFB
          FAFFFAF9F7FFF7F6F2FFF4F2EEFFEDEAE5FF504F4DFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF2F2E
          2CFF2F2F2DFFAFADA9FFF3F1ECFFF6F4F0FFF8F7F4FFFBFAF9FFFDFDFDFFFDFC
          FBFFFAF9F7FFF7F6F3FFF5F3EFFFF2EFEAFF535250FF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF2F2E
          2CFF2F2F2DFF7E7D7AFFB0AFABFFB2B1AEFFB4B3B1FFE2E2E0FFFBFAF9FFFBFA
          F8FFC1C0BEFFB3B2B0FFB1B0ADFFAAA8A4FF41403FFF2F2E2DFF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF31302FFF313130FFB4B3B1FFF8F7F4FFF8F7
          F4FF555553FF313130FF31302FFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF2F2E2DFF2F2F2DFF302F2EFF30302FFF31302FFFB2B1AEFFF6F4F0FFF5F4
          F0FF555353FF31302FFF30302EFF302F2EFF2F2F2DFF2F2E2CFF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF2F2E2CFF2F2E2DFF302F2DFF302F2EFF30302FFFB0AFABFFF3F1ECFFF3F1
          ECFF535351FF30302EFF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2E2DFF2F2F2DFF302F2EFF7E7D7AFFAFADA9FFA9A7
          A3FF41403FFF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF2F2F2DFF2F2F2DFF2F2F2DFF2F2F
          2DFF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2DFF2F2E2DFF2F2F2DFF2F2F
          2DFF2F2E2DFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2DFF2F2F2DFF323130FF5B5957FF3D3C3AFF302F
          2EFF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF323130FF858481FFEDEAE5FFC2BFBBFF3F3E
          3DFF30302EFF302F2EFF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF323130FF858481FFEFEDE8FFF4F2EEFFF4F2EEFFC4C2
          BEFF3F3F3EFF30302FFF302F2EFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF323130FF858481FFEFEDE8FFF5F3EFFFF6F5F2FFF7F6F3FFF7F5
          F2FFC5C4C1FF403F3EFF30302FFF302F2EFF2F2F2DFF2F2E2DFF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF2F2E
          2CFF31312FFF858380FFEEECE6FFF5F3EFFFF4F3F0FFBBBBB8FFEBEAE8FFFAF9
          F7FFF8F7F4FFC5C4C1FF3F3F3EFF30302EFF302F2EFF2F2E2DFF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF2F2E
          2CFF7C7A76FFECE9E3FFF3F0ECFFF3F1EDFFA0A09DFF373736FF656564FFEDEC
          EBFFFAF9F7FFF7F5F2FFC4C2BEFF3F3E3DFF302F2EFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF2F2E
          2CFF6B6966FFE8E5DFFFF1EEE9FF9F9E9BFF363535FF323232FF333332FF6565
          64FFEBEAE8FFF7F6F3FFF4F2EEFFC2BFBBFF3F3D3BFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF2F2E
          2CFF30302EFF706F6BFF979692FF353534FF313130FF323231FF323232FF3232
          32FF646362FFE7E6E3FFF4F2EEFFF1EEE9FFBEBCB7FF393836FF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF31302FFF313130FF313130FF323131FF3231
          31FF323131FF626160FFE4E2DEFFF0EEE8FFCBC9C2FF3C3B39FF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF2F2E2DFF2F2F2DFF302F2EFF30302FFF31302FFF313130FF313130FF3131
          30FF313030FF31302FFF605F5DFFBFBCB8FF464543FF2F2E2CFF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF2F2E2CFF2F2E2DFF302F2DFF302F2EFF30302FFF30302FFF31302FFF3130
          2FFF30302FFF30302EFF302F2EFF343331FF2F2E2DFF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2E2DFF2F2F2DFF302F2EFF302F2EFF302F2EFF302F
          2EFF302F2EFF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF2F2F2DFF2F2F2DFF2F2F2DFF2F2F
          2DFF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2DFF2F2E2DFF2F2F2DFF2F2F
          2DFF2F2E2DFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2DFF2F2F2DFF302F2DFF302F2EFF302F2EFF302F
          2EFF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF302F2EFF52504EFF434240FF30302FFF30302FFF3030
          2FFF323231FF5E5D5AFF383735FF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF2F2E2CFF302F2DFF605E5CFFE3E0DCFFD0CFCAFF484746FF313030FF3333
          32FF878583FFEDEBE6FFB0AEA9FF383735FF2F2E2DFF2F2E2CFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF343332FFC1BEBAFFF3F1ECFFF5F3EFFFD3D2D0FF4B4B4AFF8888
          86FFF2F0EDFFF4F2EEFFEDEBE6FF5E5D5AFF2F2F2DFF2F2E2DFF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF474746FFD2D0CCFFF7F5F2FFF9F8F5FFE7E7E5FFF6F5
          F3FFF8F7F4FFF2F0EDFF878583FF323231FF302F2EFF2F2E2DFF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF484847FFD4D3D1FFFBFAF8FFFDFCFBFFFCFB
          FAFFF6F5F3FF888886FF333332FF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF333332FF969594FFFBFAF9FFFDFDFDFFFDFC
          FBFFE7E7E5FF4C4B4AFF313030FF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF2F2E
          2CFF2F2F2DFF302F2EFF333231FF878784FFF3F2EFFFFAF9F7FFFBFAF9FFFBFA
          F8FFF9F8F5FFD3D2CFFF484746FF30302FFF302F2EFF2F2E2DFF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2F2DFF31302FFF848380FFF0EEE9FFF6F4F0FFF3F2EFFF969594FFD4D3
          D2FFF7F5F2FFF5F3EFFFD0CECAFF434240FF302F2DFF2F2E2DFF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF2F2E2DFF323130FFACA9A4FFF1EEE9FFF0EEE9FF878784FF333332FF4848
          47FFD2D0CDFFF3F1ECFFE3E1DBFF52504EFF2F2F2DFF2F2E2CFF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF2F2E2CFF2F2E2DFF383735FFACA9A4FF848380FF333231FF31302FFF3130
          2FFF474745FFC0BEBAFF605E5CFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2E2DFF323130FF31302FFF302F2EFF302F2EFF302F
          2EFF302F2EFF343332FF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF2F2F2DFF2F2F2DFF2F2F2DFF2F2F
          2DFF2F2F2DFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF363532FF5C5A58FF9A9893FFACAAA5FFAAA8
          A2FF7D7B77FF454442FF302F2DFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF4B4A48FFBFBCB6FFECE8E2FFEFECE5FFEFECE6FFEFEC
          E6FFEFEBE5FFDFDBD5FF82807CFF333230FF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF4B4A47FFD6D4CEFFEFECE6FFEEEBE6FFD7D5D0FFCFCCC8FFD0CE
          C9FFE4E1DCFFF0EDE7FFECE9E3FF989691FF31302EFF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF353432FFBFBCB6FFEFECE6FFE6E3DEFF747270FF3A3A38FF313030FF3332
          31FF4B4A48FFBAB8B4FFEFEDE7FFE9E6E0FF63625FFF2F2E2CFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF5A5A57FFEBE8E1FFEEECE6FF747370FF313130FF313130FF313130FF3131
          30FF313130FF3C3C3AFFD0CECAFFF0EDE7FFC6C4BEFF333230FF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF2F2E
          2CFF9A9894FFF0EDE6FFD8D6D1FF3B3A39FF313130FF323131FF323231FF3232
          31FF323131FF313130FF716F6DFFF1EEE9FFE1DDD8FF444241FF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF2F2E
          2CFF82807CFFAFADA8FF999894FF313130FF323131FF323231FF323232FF3232
          32FF323231FF313130FF525250FFF0EDE8FFE8E5DFFF4B4A48FF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF2F2E
          2CFF2F2F2DFF302F2EFF31302FFF313130FF323131FF323232FF333332FF3232
          32FF323231FF313130FF585756FFF2EFEAFFE6E3DDFF494846FF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF313130FF313130FF343433FF6A6A69FF3232
          32FF323131FF323231FFA1A09DFFF1EFE9FFD9D6D0FF3C3B39FF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF31302FFF363635FF9E9D9BFFD0CFCDFF3231
          31FF323231FF676663FFEAE8E4FFF0EDE8FF9B9995FF2F2E2DFF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF2F2E2DFF2F2F2DFF302F2EFF3B3A39FFB0AEABFFF3F1EDFFD8D7D3FF5A5A
          58FFA3A29FFFEBE9E4FFF1EFE9FFD4D1CCFF41413FFF2F2E2CFF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF2F2E2CFF2F2E2DFF353432FFB9B6B2FFF0EEE8FFF2F0EBFFF3F1ECFFF3F1
          ECFFF2F0EAFFF1EEE8FFD4D1CBFF51504EFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF2F2E2CFF302F2EFF6C6B68FFE1DED8FFF0EDE7FFEFECE6FFE7E4
          DEFFDAD7D1FF9C9A95FF41413FFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF2F2E2CFF2F2F2DFF5A5956FFD6D3CDFFCFCDC7FF4848
          45FF3C3B39FF2F2F2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF4B4A47FFA7A59FFF2F2E
          2CFF2F2E2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF363633FF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF363532FF5C5A58FF9A9893FFACAAA5FFAAA8
          A2FF7D7B77FF454442FF302F2DFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF4B4A48FFBFBCB6FFECE8E2FFEFECE5FFEFECE6FFEFEC
          E6FFEFEBE5FFDFDBD5FF82807CFF333230FF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF4B4A47FFD6D4CEFFEFECE6FFEEEBE6FFD7D5D0FFCFCCC8FFD0CE
          C9FFE4E1DCFFF0EDE7FFECE9E3FF989691FF31302EFF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF353432FFBFBCB6FFEFECE6FFE6E3DEFF747270FF3A3A38FF313030FF3332
          31FF4B4A48FFBAB8B4FFEFEDE7FFE9E6E0FF63625FFF2F2E2CFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF5A5A57FFEBE8E1FFEEECE6FF747370FF313130FF313130FF313130FF3131
          30FF313130FF3C3C3AFFD0CECAFFF0EDE7FFC6C4BEFF333230FF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF2F2E
          2CFF9A9894FFF0EDE6FFD8D6D1FF3B3A39FF313130FF323131FF323231FF3232
          31FF323131FF313130FF716F6DFFF1EEE9FFE1DDD8FF444241FF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF2F2E
          2CFFADAAA6FFF0EDE7FFCFCDC9FF313130FF323131FF323231FF323232FF3232
          32FF323231FF313130FF484847FFAFAEAAFFA8A6A2FF40403EFF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF2F2E
          2CFFAAA7A3FFF0EDE7FFD1CFCAFF333332FF323131FF323232FF333332FF3232
          32FF323231FF313130FF313030FF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF2F2E
          2CFF7E7D79FFF0EDE7FFE5E3DEFF4B4B49FF313130FF323231FF3B3B3BFF4645
          45FF323131FF313130FF31302FFF30302FFF302F2EFF2F2E2DFF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF2F2E
          2CFF454442FFE0DDD7FFF1EFE9FFBAB9B5FF3C3C3BFF313130FF4D4C4CFFD3D1
          CFFF565554FF313130FF31302FFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF302F2EFF82817DFFEEEBE5FFF1EEE9FFD2D0CBFF767673FF6B6A68FFEEEC
          E9FFE3E1DDFF636260FF31312FFF302F2EFF2F2F2DFF2F2E2CFF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF2F2E2CFF333231FF999793FFEBE8E2FFF1EFE9FFF2F0EBFFF3F1ECFFF3F1
          ECFFF2F0EAFFE7E5DFFF5E5D5AFF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF2F2E2CFF31312FFF646360FFC8C5C0FFE2DFDAFFE9E6E0FFF0ED
          E7FFEFECE5FFB6B4AFFF3E3C3BFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF333231FF444441FF605E5CFFE3E0
          DAFFA19E9AFF373634FF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF454441FF807E
          79FF333230FF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF2F2F2DFF2F2F
          2DFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000404040616161623444342666766649872716FA87271
          6FA86E6D6CA4545353812525253C0C0C0C130101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202032A2A293F908E88BBC6C1B7F0C6C1B7FCC1BCB2FFAFABA2FFBDB8
          AEFFC3BEB4FEC8C3BAF9C2BDB5EE878784C24646466F0C0C0C14000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000A0A
          0A0F7B78749DC9C5BAF7959189FF4D4B47FF383734FF2F2E2BFF2E2D2AFF2E2D
          2AFF32312EFF403F3BFF686560FFBDB9AFFEC7C2B9F27A7977B52525253C0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717151DA9A5
          9BC8ACA89EFE464541FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF302F2DFF302F
          2DFF2E2D2BFF2E2D2BFF2E2D2BFF343230FF706E67FFCCC6BCFB95928FCF3333
          3352010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0F0E11B1ACA2CB938F
          87FF33312FFF2E2D2BFF2E2D2BFF2E2D2CFF2F2E2CFF333230FF83817CFF4D4B
          49FF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF2E2D2BFF4F4D49FFC9C4B9FE9593
          8FCF2626263D0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004030304928E86A798948CFE3230
          2DFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF373634FF9B9A94FFE8E5DEFF6564
          61FF444341FF333230FF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF4F4D49FFCDC8
          BDFC7A7978B60C0C0C1400000000000000000000000000000000000000000000
          000000000000000000000000000000000000403E3B49C0BBB0FB393834FF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2DFF3E3C3BFFB1AFAAFFEEEBE4FFEFECE6FFE8E5
          DFFFE1DED8FFC7C4BEFF63625FFF31302EFF2F2E2CFF2E2D2BFF2E2D2BFF706E
          68FFC6C3B9F24646466F01010101000000000000000000000000000000000000
          000000000000000000000000000008080709B3AEA4CE625F5AFF2E2D2AFF2E2D
          2BFF2F2E2CFF3F3E3CFF32312FFF5E5D5AFFE5E3DDFFF1EFE9FFF2EFEAFFF2EF
          EAFFF1EEE9FFF0EDE7FFE9E6E0FF989691FF333230FF2E2E2CFF2E2D2BFF3432
          30FFBEB9AEFE878582C10C0C0C13000000000000000000000000000000000000
          00000000000000000000000000002826242DC0BBB0F8353331FF2E2D2BFF2E2E
          2CFF403F3CFFCDCAC4FF84827FFF343332FF62615FFFE1DFDBFFF5F3EFFF7270
          6FFF767472FFD0CECAFFEFEDE7FFECE9E3FF81807BFF302F2DFF2E2D2BFF2E2D
          2BFF686560FFC3BFB6EF2525253C000000000000000000000000000000000000
          00000000000000000000000000006E6B647D8F8C84FF2E2D2AFF2E2D2BFF2F2E
          2CFF999791FFEEEBE5FFE5E2DDFF484746FF313130FF555453FFDAD9D6FF5555
          53FF313130FF3C3B3AFFBAB8B3FFF0EDE7FFDFDBD5FF454442FF2E2E2CFF2E2D
          2BFF403E3BFFC8C3BAF954545381000000000000000000000000000000000000
          0000000000000000000000000000A5A197BC5D5B56FF2E2D2BFF2E2D2BFF3B3A
          38FFD6D3CCFFF0EDE6FFA19F9CFF323131FF313130FF323131FF4A4948FF4141
          40FF323131FF313130FF4B4A48FFE3E1DBFFEFEBE5FF7E7C78FF2F2E2CFF2E2D
          2BFF32312EFFC3BEB3FE6D6D6BA3000000000000000000000000000000000000
          0000000000000000000000000000B7B2A8D14D4B47FF2E2D2BFF2E2D2CFF4846
          44FFE4E1DAFFF0EDE7FF595856FF313130FF323131FF323231FF323232FF3232
          32FF323231FF313130FF323131FFCECCC7FFEFECE6FFABA9A3FF2F2E2CFF2E2D
          2BFF2E2D2AFFBDB8AEFF747371AC020202030000000000000000000000000000
          0000000000000000000000000000B3AFA5D1494743FF2E2D2BFF2E2E2CFF4947
          45FFE5E2DBFFF0EDE7FF545351FF313130FF323131FF323232FF333332FF3232
          32FF323231FF313130FF313030FFC8C5C1FFEFECE6FFB3B0ABFF2F2E2CFF2E2D
          2BFF2E2D2AFFAAA69DFF797875AD010101010000000000000000000000000000
          0000000000000000000000000000B4AFA5CD514F4AFF2E2D2BFF2E2D2CFF4342
          40FFDFDCD6FFF0EDE7FF757471FF313130FF313130FF323231FF363636FF3232
          32FF323131FF313130FF393837FFD6D4CFFFEFECE5FF9A9893FF2F2E2CFF2E2D
          2BFF2F2E2BFFC1BDB2FF68676599000000000000000000000000000000000000
          0000000000000000000000000000908C84A4727069FF2E2D2AFF2E2D2BFF3332
          2FFFC6C3BDFFEFECE5FFCFCDC8FF3C3B3AFF313130FF313130FF949492FF5050
          4FFF313130FF313130FF71706DFFEEEBE6FFEBE8E2FF5C5B58FF2F2E2CFF2E2D
          2BFF383734FFC6C1B6FC44444266000000000000000000000000000000000000
          00000000000000000000000000004544404FAEA9A0FE2F2D2BFF2E2D2BFF2F2E
          2CFF63615FFFE9E5DEFFEFECE6FFBAB7B4FF4B4A49FF333231FFB2B1AFFFDFDE
          DAFF5E5E5CFF31302FFF6C6A68FFE7E4DEFFC0BEB7FF353432FF2E2D2CFF2E2D
          2BFF4D4B47FFC6C2B8F119191825000000000000000000000000000000000000
          000000000000000000000000000016151419C3BDB2EA44423FFF2E2D2BFF2E2D
          2BFF31302EFF989590FFECE9E2FFF0EDE7FFE4E2DCFFD0CECAFFE7E5E0FFF3F1
          ECFFE5E2DEFF6F6D6BFF323130FF666562FF4B4A47FF2F2E2CFF2E2D2BFF2F2E
          2BFF959189FF918E89BC04040406000000000000000000000000000000000000
          0000000000000000000000000000010101017F7C7591949188FF2F2E2BFF2E2D
          2BFF2E2E2CFF333230FF82807BFFDFDBD5FFEFECE5FFF0EDE7FFF0EDE7FFF0ED
          E7FFEFECE5FFB8B6B1FF353432FF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4644
          41FFC9C5BAF72B2A2A3F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001817161BC0BBB0E25A5853FF2E2D
          2AFF2E2D2BFF2E2E2CFF302F2DFF454442FF7E7C78FFAAA8A2FFD7D4CEFFEBE8
          E1FFADABA5FF3A3937FF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF33312FFFACA8
          9EFE7D7B769E0202020300000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000413F3B4AC6C0B5F64D4B
          48FF2E2D2AFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFFA8A59FFF9795
          90FF353431FF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF32302DFF938F87FFAAA5
          9DC80E0E0D110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020202605D586DC6C0
          B5F65A5753FF2F2E2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2CFF4F4E4AFF3131
          2EFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2AFF393835FF98948CFEB1ACA2CB1918
          171D000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020202413F
          3B4AC0BBB1E2949188FF44423FFF2F2D2BFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2AFF353431FF625F5AFFC0BBB0FB949087A911100F130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001817161B7F7C7591C3BEB2EAAEA9A0FE73706AFF504E4BFF494743FF4D4B
          47FF5D5B56FF8F8C84FFC0BBB1F9B3AEA4CE403E3B4904030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101161514194544404F908C84A4B4AFA5CDB3AEA5D1B7B2
          A7D1A5A197BC6E6B647D2827252E070706080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF30302EFF605F5CFF868480FF4040
          3EFF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2F2DFF31302EFF706F6CFFE8E5DFFFF0EDE7FFCECB
          C5FF474644FF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF2E2D
          2CFF2F2E2CFF2F2F2DFF31302FFF71706DFFEAE7E2FFF2F0EBFFF3F1ECFFF2F0
          EBFFCFCDC8FF474645FF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF2E2D2BFF2F2E
          2CFF2F2E2DFF31302FFF71706DFFEAE8E4FFF4F2EEFFF5F3F0FFF5F4F0FFF5F3
          EFFFF4F2EDFFD0CEC9FF474645FF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF2E2D2BFF2F2E
          2CFF30302EFF716F6CFFEAE8E3FFF4F3EEFFF6F5F1FFF8F7F4FFF8F7F5FFF8F6
          F3FFF6F4F0FFF4F2EDFFCFCDC8FF474644FF2F2E2DFF2F2E2CFF2E2D2BFF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF2E2E2CFF302F
          2EFF706E6CFFE9E6E0FFF3F1ECFFF2F0EDFFA3A2A1FFF3F2F1FFFBFAF9FFE1E0
          DEFFC6C5C2FFF4F3EFFFF2F0EBFFCECBC6FF464643FF2F2E2CFF2E2D2BFF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF615F
          5CFFE6E3DDFFF1EEE9FFF0EEE9FF888785FF50504EFFF3F3F2FFFDFDFCFFD7D6
          D5FF41403FFFC4C3C0FFF3F0EBFFF0EDE7FFC5C2BDFF373634FF2E2D2BFF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF8C89
          85FFEEEAE3FFEDEAE4FF878583FF333332FF4D4C4CFFF2F2F0FFFCFCFBFFD6D6
          D4FF323131FF403F3EFFC2C0BCFFEFEDE6FFDDDAD3FF41403EFF2E2D2BFF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF2E2D2CFF3D3B
          39FF92908BFF6A6866FF333231FF313130FF4C4C4BFFF0EFEDFFF9F8F6FFD4D3
          D2FF313130FF31302FFF3E3D3BFF8E8C88FF686764FF302F2DFF2E2D2BFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF302F2EFF30302EFF31302FFF4C4B4AFFEDECE8FFF7F5F2FFD2D1
          CEFF313030FF30302FFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF302F2EFF30302EFF4B4A49FFEBE8E4FFF4F2EDFFD0CE
          CAFF30302FFF302F2EFF302F2EFF2F2F2DFF2F2E2CFF2E2D2BFF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF302F2EFF4A4947FFE8E5E0FFF1EEE9FFCAC7
          C2FF302F2EFF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2F2DFF3B3B38FFC6C4BEFFE3E0D9FF7876
          73FF2F2F2DFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2CFF3A3938FF464543FF3130
          2FFF2F2E2CFF2F2E2CFF2E2D2CFF2E2D2BFF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2F2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097673
          6C86C4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2F2E2CFF3D3C39FF474643FF474643FF4443
          40FF353431FF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF302F2DFF575652FFB3B0A8FFD7D3CBFFE1DDD6FFE2DED6FFDEDB
          D2FFCCC8C0FF85837EFF3C3B38FF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF363532FF97958EFFE3DFD7FFEBE8E0FFECE9E1FFEAE7DFFFE4E1DAFFECE9
          E2FFECE8E1FFEBE7DFFFCECAC2FF5A5954FF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF3332
          30FFA9A59FFFEAE6DDFFECE9E1FFEEEBE4FFE0DDD8FF676562FF4B4A48FF9E9C
          98FFECE9E3FFEDEAE3FFECE8E0FFDCD9D0FF5A5854FF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF7876
          71FFE8E4DCFFEDE9E2FFEFECE5FFE1DFDAFF605F5DFF31302FFF31302FFF3434
          33FF9B9A96FFEEEAE4FFEEEBE4FFECE8E0FFCDC9C2FF3C3B38FF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF3D3C39FFD4D0
          C8FFECE8E0FFEEEBE4FFE2DFDAFF60605DFF313130FF313130FF313130FF3131
          30FF353433FF9C9A96FFEEEBE4FFEDEAE3FFEBE7DFFF85837EFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF676561FFE9E5
          DDFFEDEAE2FFE1DED8FF605F5DFF313130FF313130FF323131FF323131FF3131
          30FF313130FF353433FF9C9A96FFECE9E3FFECE9E1FFCCC8C1FF353432FF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF9E9B94FFEBE7
          DFFFDFDCD6FF605F5CFF31302FFF313130FF323131FF323231FF323232FF3232
          31FF313130FF313130FF343433FF9A9894FFEAE7E0FFDEDBD2FF444340FF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA9A69FFFE3DF
          D8FF615F5DFF30302EFF31302FFF353534FF7F7F7EFF323232FF333333FF4444
          44FF5C5B5AFF313130FF31302FFF343332FFAAA7A1FFE5E1D9FF4B4946FF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA8A59EFFCCC9
          C1FF333230FF30302EFF353433FF9F9E9CFFD2D1CFFF323232FF323232FF4D4D
          4CFFE0DFDCFF626160FF31302FFF302F2EFF5A5956FFE2DED6FF484643FF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF84817CFFE2DE
          D6FF656260FF484745FFA09F9CFFF3F1EDFFD3D2CFFF323131FF323231FF4D4C
          4BFFEDECE9FFE5E4E0FF676664FF474644FFACA9A3FFD8D4CCFF3E3D3AFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF4E4C49FFE3DF
          D7FFE7E3DCFFE3E0DAFFF0EEE8FFF3F1EDFFD1D0CCFF313130FF313130FF4C4C
          4AFFECEAE5FFF3F0EBFFECEAE4FFE1DED8FFEAE7DFFFB3B0A9FF2F2E2CFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF32312EFFB2AF
          A8FFEBE7DFFFEDEAE3FFEFECE6FFF1EFE9FFD0CDC9FF31302FFF31302FFF4B4A
          49FFE9E7E2FFF1EEE8FFEFECE5FFECE9E1FFE5E1D9FF585753FF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF4947
          44FFD7D4CBFFEBE8E0FFEDEAE3FFEFECE5FFCDCBC6FF30302FFF30302EFF4D4D
          4AFFE8E5E0FFEEEBE4FFEDE9E2FFEAE6DDFF9C9993FF302F2DFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF5D5B57FFD7D4CBFFEBE7DFFFECE9E1FFDFDCD5FF575653FF434140FF9E9B
          97FFEDEAE2FFECE8E1FFE8E4DCFFA9A69FFF363532FF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF494744FFB2B0A8FFE4E0D7FFEBE7DEFFE2DFD7FFDCD8D1FFE9E6
          DEFFE9E5DDFFD5D2C9FF7A7873FF333230FF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF32312FFF4D4C49FF84817CFFA8A59EFFA9A69FFF9E9B
          95FF676561FF3D3C39FF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097673
          6C86C4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF302F2DFF403F3CFF3736
          34FF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF30302EFF6F6E6BFFDAD7D1FFBEBC
          B6FF373634FF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2F2DFF31302EFF706F6CFFE8E5DFFFF0EDE7FFE0DD
          D7FF434140FF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF2E2D
          2CFF2F2E2CFF2F2F2DFF31302FFF71706DFFEAE7E2FFF2F0EBFFEFEDE8FF8583
          80FF31312FFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF2E2D2BFF2F2E
          2CFF2F2E2DFF31302FFF71706DFFEAE8E4FFF4F2EEFFF1EFECFF878784FF3333
          32FF31302FFF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF2E2D2BFF2F2E
          2CFF30302EFF716F6CFFEAE8E3FFF4F3EEFFF2F1EDFF898887FF343433FF3131
          30FF313130FF31302FFF30302EFF302F2EFF2F2E2DFF2F2E2CFF2E2D2BFF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF2E2E2CFF302F
          2EFF706E6BFFE9E6E0FFF3F1ECFFF6F4F1FFF4F3F1FFD8D8D7FFD7D6D5FFD6D5
          D3FFD4D2D0FFD1D0CCFFCFCDC9FFAFADA9FF434341FF2F2E2CFF2E2D2BFF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF3E3D
          3BFFD9D6D0FFF1EEE9FFF4F2EDFFF6F5F2FFF9F8F6FFFCFCFBFFFDFDFCFFFBFA
          F9FFF8F7F5FFF6F4F0FFF3F1ECFFF0EDE7FFA9A7A2FF2F2E2CFF2E2D2BFF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF3534
          32FFBAB8B3FFF0EEE8FFF4F1EDFFF6F5F1FFF7F6F3FFF2F2F0FFF3F3F2FFF1F1
          EFFFEFEEEBFFECEBE7FFE9E7E3FFE5E2DCFF777673FF2F2E2CFF2E2D2BFF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF2E2D2CFF2F2E
          2CFF3E3D3AFFC0BEB9FFF2F0EBFFF5F3EFFFE7E6E3FF656463FF4D4D4CFF4D4C
          4CFF4C4C4BFF4C4B49FF4B4A48FF474645FF31312FFF2F2E2CFF2E2D2BFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF3E3D3BFFC1BFBAFFF3F1ECFFF5F3EFFFD3D1CEFF484847FF3131
          30FF313030FF30302FFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF3F3E3CFFC1BFBAFFF2F0EBFFF4F1EDFFD1CFCBFF4847
          46FF30302FFF302F2EFF302F2EFF2F2F2DFF2F2E2CFF2E2D2BFF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF3E3D3BFFC0BDB9FFF0EEE8FFF1EEE9FFC8C5
          C0FF383736FF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2F2DFF3E3C3BFFBEBCB7FFEEEBE4FFDEDB
          D4FF42413FFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2CFF3C3B39FF8C8A85FF6866
          63FF302F2DFF2F2E2CFF2E2D2CFF2E2D2BFF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2F2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097C79
          718DC4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2F2E2CFF3D3C39FF474643FF474643FF4443
          40FF353431FF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF302F2DFF575652FFB3B0A8FFD7D3CBFFE1DDD6FFE2DED6FFDEDB
          D2FFCCC8C0FF85837EFF3C3B38FF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF363532FF97958EFFE3DFD7FFEBE8E0FFECE9E1FFCECBC5FF686663FF8B89
          85FFE7E3DCFFEBE7DFFFCECAC2FF5A5954FF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF3332
          30FFA9A59FFFEAE6DDFFECE9E1FFEEEBE4FFCDCAC5FF474644FF302F2EFF3434
          32FFD0CDC8FFEDEAE3FFECE8E0FFDCD9D0FF5A5854FF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF7876
          71FFE8E4DCFFEDE9E2FFEFECE5FFCECCC7FF474745FF30302FFF31302FFF4444
          43FFDEDCD7FFF0EDE7FFEEEBE4FFECE8E0FFCDC9C2FF3C3B38FF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF3D3C39FFD4D0
          C8FFECE8E0FFEEEBE4FFCECCC7FF474745FF31302FFF313130FF40403EFFC4C2
          BFFFF3F1EDFFF2EFEAFFF0EDE7FFEDEAE3FFEBE7DFFF85837EFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF676561FFE9E5
          DDFFEDEAE2FFCECBC5FF474745FF313030FF313130FF3C3B3AFFBDBDBAFFEEED
          EAFFEDEBE7FFEBE9E4FFE9E7E1FFEEEBE4FFECE9E1FFCCC8C1FF353432FF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF9E9B94FFEBE7
          DFFFCFCCC6FF474744FF31302FFF313130FF323131FF383837FF4D4D4CFF4D4D
          4CFF4C4C4BFF4C4B4AFF4B4B49FF7B7A76FFE7E3DCFFDEDBD2FF444340FF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA9A69FFFEBE8
          E0FF767470FF30302EFF31302FFF313130FF323231FF323232FF333333FF3232
          32FF323131FF313130FF31302FFF32312FFFBFBCB7FFE5E1D9FF4B4946FF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA8A59EFFEBE8
          DFFF97958FFF323230FF31302FFF313130FF323131FF323232FF323232FF3232
          31FF323131FF313130FF30302FFF41403EFFD3D0C9FFE2DED6FF484643FF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF84817CFFEBE7
          DEFFE9E6DFFF85837FFF333231FF313130FF313130FF585756FFCDCCCAFFD4D3
          D1FFD2D1CEFFD0CFCBFFCFCCC8FFD8D5CFFFECE8E1FFD8D4CCFF3E3D3AFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF4E4C49FFE3DF
          D7FFECE9E1FFEBE8E1FF858480FF333231FF313130FF323231FF747372FFEEEC
          E9FFF5F3EEFFF3F0EBFFF0EEE8FFEEEBE4FFEBE8E0FFB3B0A9FF2F2E2CFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF32312EFFB2AF
          A8FFEBE7DFFFEDEAE3FFEBE8E2FF858481FF333231FF31302FFF323130FF7473
          70FFEDEBE6FFF1EEE8FFEFECE5FFECE9E1FFE5E1D9FF585753FF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF4947
          44FFD7D4CBFFEBE8E0FFEDEAE3FFEBE8E1FF85837FFF323230FF30302EFF3434
          33FFD1CEC9FFEEEBE4FFEDE9E2FFEAE6DDFF9C9993FF302F2DFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF5D5B57FFD7D4CBFFEBE7DFFFECE9E1FFE9E6DFFF85827EFF363534FF4A48
          46FFDAD8D1FFECE8E1FFE8E4DCFFA9A69FFF363532FF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF494744FFB2B0A8FFE4E0D7FFEBE7DEFFE8E5DCFFCFCCC5FFD8D5
          CDFFE9E5DDFFD5D2C9FF7A7873FF333230FF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF32312FFF4D4C49FF84817CFFA8A59EFFA9A69FFF9E9B
          95FF676561FF3D3C39FF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097C79
          718DC4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF2E2D2BFF2E2D2BFF2F2E2CFF302F2DFF41403EFF353432FF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF666561FFDCD9D2FFC3C1BBFF4645
          43FF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2F2DFF302F2DFF8D8B87FFEFECE7FFF0EDE7FFCECB
          C5FF474644FF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF2E2D
          2CFF2F2E2CFF2F2F2DFF302F2EFF302F2EFF3D3D3BFFC2C0BCFFF2F0EBFFF2F0
          EBFFCFCDC8FF474645FF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF2E2D2BFF2F2E
          2CFF2F2E2DFF302F2EFF302F2EFF30302FFF31302FFF403F3EFFC4C2C0FFF5F3
          EFFFF4F2EDFFD0CEC9FF474645FF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF2E2D2BFF2F2E
          2CFF2F2F2DFF302F2EFF30302FFF313030FF313130FF323131FF41403FFFC5C4
          C2FFF6F4F0FFF4F2EDFFCFCDC8FF474644FF2F2E2DFF2F2E2CFF2E2D2BFF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF2E2E2CFF3130
          2FFF767572FFC3C1BCFFD0CECAFFD2D1CEFFD4D3D1FFD6D6D4FFD7D6D5FFE0DF
          DDFFF7F6F3FFF5F3EFFFF2F0EBFFCECBC6FF42413FFF2F2E2CFF2E2D2BFF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF4645
          43FFE2E0D9FFF1EEE9FFF4F2EDFFF6F5F2FFF9F8F6FFFCFCFBFFFDFDFCFFFBFA
          F9FFF8F7F5FFF6F4F0FFF3F1ECFFF0EDE7FF8E8C87FF2F2E2CFF2E2D2BFF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF3B3A
          38FFC6C4BEFFE8E5E0FFEBE8E4FFEDECE8FFF0EFECFFF2F2F0FFF3F3F2FFF2F2
          F0FFF7F6F4FFF5F4F0FFF2F0EBFFECE9E3FF64635FFF2F2E2CFF2E2D2BFF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF2E2D2CFF2F2E
          2CFF3B3A38FF4A4947FF4B4A49FF4C4B4AFF4C4C4BFF4D4C4CFF4F4E4EFF908F
          8DFFF5F3F0FFF4F2EEFFEEEBE6FF85837FFF31312FFF2F2E2CFF2E2D2BFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF302F2EFF30302EFF31302FFF313130FF323231FF747371FFEEEC
          E9FFF5F3EEFFEFECE7FF858481FF32312FFF2F2E2DFF2F2E2CFF2E2D2BFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF302F2EFF30302EFF313130FF72716FFFECEAE5FFF3F1
          ECFFEEECE7FF858481FF323130FF2F2F2DFF2F2E2CFF2E2D2BFF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF302F2EFF63615FFFE9E6E1FFF1EEE9FFEDEA
          E4FF858380FF323130FF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2F2DFF8D8B87FFEEEAE4FFEAE7E0FF8482
          7EFF32312FFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF2E2D2BFF2E2E2CFF2F2E2CFF3D3C39FF918F8AFF686663FF3131
          2FFF2F2E2CFF2F2E2CFF2E2D2CFF2E2D2BFF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2F2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097C79
          718DC4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2F2E2CFF3D3C39FF474643FF474643FF4443
          40FF353431FF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF302F2DFF575652FFB3B0A8FFD7D3CBFFE1DDD6FFE2DED6FFDEDB
          D2FFCCC8C0FF85837EFF3C3B38FF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF363532FF97958EFFE3DFD7FFEBE8E0FFCDCAC3FF63615EFF8C8A85FFE5E2
          DBFFECE8E1FFEBE7DFFFCECAC2FF5A5954FF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF3332
          30FFA9A59FFFEAE6DDFFECE9E1FFEEEBE4FF686764FF302F2EFF31302FFF706E
          6CFFE7E4DDFFEDEAE3FFECE8E0FFDCD9D0FF5A5854FF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF7876
          71FFE8E4DCFFEDE9E2FFEFECE5FFF0EEE8FF969491FF333231FF31302FFF3131
          30FF71706DFFE8E5DFFFEEEBE4FFECE8E0FFCDC9C2FF3C3B38FF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF3D3C39FFD4D0
          C8FFECE8E0FFEEEBE4FFF1EEE8FFF2F0EBFFF0EEEAFF878785FF333332FF3131
          30FF323130FF71706EFFE8E5DFFFEDEAE3FFEBE7DFFF85837EFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF676561FFE9E5
          DDFFEDEAE2FFEBE8E2FFE9E7E2FFEBEAE5FFEDECE8FFEBEAE7FF80807FFF3232
          31FF313130FF323130FF716F6DFFE7E4DDFFECE9E1FFCCC8C1FF353432FF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF9E9B94FFEBE7
          DFFFC1BFB9FF545351FF4B4A49FF4C4C4AFF4D4C4BFF4D4D4CFF494949FF3333
          32FF313130FF313130FF313130FF72706EFFE8E5DDFFDEDBD2FF444340FF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA9A69FFFEBE8
          E0FF575654FF30302EFF31302FFF313130FF323231FF323232FF333333FF3232
          32FF323131FF313130FF31302FFF363534FFD1CEC8FFE5E1D9FF4B4946FF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA8A59EFFEBE8
          DFFF7B7A76FF32312FFF31302FFF313130FF323131FF323232FF323232FF3232
          31FF323131FF313130FF30302FFF444341FFDBD8D0FFE2DED6FF484643FF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF84817CFFEBE7
          DEFFE7E4DDFFCFCCC7FFD0CDC9FFD1D0CCFFD3D2CFFFD5D4D2FFB1B0AFFF3A3A
          39FF313130FF31302FFF3F3E3CFFBFBCB7FFECE8E1FFD8D4CCFF3E3D3AFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF4E4C49FFE3DF
          D7FFECE9E1FFEFECE5FFF1EFE9FFF3F1EDFFF5F3EFFFD3D2CFFF494847FF3131
          30FF313030FF3F3E3CFFC0BEB9FFEEEAE4FFEBE8E0FFB3B0A9FF2F2E2CFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF32312EFFB2AF
          A8FFEBE7DFFFEDEAE3FFEFECE6FFF1EFE9FFD3D1CDFF484746FF31302FFF3130
          2FFF3F3E3CFFC0BEB9FFEFEBE4FFECE9E1FFE5E1D9FF585753FF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF4947
          44FFD7D4CBFFEBE8E0FFEDEAE3FFEFECE5FF6A6966FF30302EFF30302EFF3E3D
          3CFFC0BDB8FFEEEBE4FFEDE9E2FFEAE6DDFF9C9993FF302F2DFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF5D5B57FFD7D4CBFFEBE7DFFFECE9E1FF8F8D88FF353432FF444341FFBEBB
          B5FFEDE9E2FFECE8E1FFE8E4DCFFA9A69FFF363532FF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF494744FFB2B0A8FFE4E0D7FFE7E4DBFFCECBC3FFD9D6CEFFEBE7
          DFFFE9E5DDFFD5D2C9FF7A7873FF333230FF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF32312FFF4D4C49FF84817CFFA8A59EFFA9A69FFF9E9B
          95FF676561FF3D3C39FF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097C79
          718DC4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF2E2D2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E2CFF2F2E
          2CFF2F2E2CFF2E2E2CFF2E2D2BFF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2DFF31312FFF767471FFA3A19CFF4343
          40FF2F2E2DFF2F2E2CFF2F2E2CFF2E2D2BFF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF2E2D
          2BFF2E2E2CFF2F2E2CFF2F2F2DFF302F2DFF474644FFE4E1DBFFF0EDE7FFAFAD
          A8FF302F2EFF2F2F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF2E2D
          2CFF2F2E2CFF2F2F2DFF302F2EFF302F2EFF4B4A48FFE9E7E3FFF3F1ECFFCFCD
          C9FF30302EFF302F2EFF302F2DFF2F2E2DFF2F2E2CFF2E2D2BFF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF2E2D2BFF2F2E
          2CFF2F2E2DFF302F2EFF302F2EFF30302FFF4C4B49FFECEAE7FFF5F4F0FFD1D0
          CCFF31302FFF30302FFF302F2EFF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF2E2D2BFF302F
          2DFF42413FFF363534FF30302FFF313030FF4C4C4BFFEFEEEBFFF8F7F5FFD4D2
          D0FF313130FF31302FFF31312FFF41403EFF373635FF2F2E2CFF2E2D2BFF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF2E2E2CFF6563
          60FFDDDAD3FFC6C4BFFF484746FF313130FF4D4C4CFFF1F1EFFFFBFAF9FFD6D5
          D3FF313130FF323231FF72706EFFDDDAD4FFBEBCB5FF373634FF2E2D2BFF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF8B88
          84FFEEEBE4FFF1EEE9FFD1CFCBFF484847FF4D4D4CFFF3F3F2FFFDFDFCFFD7D6
          D5FF333232FF737270FFEBE9E4FFF0EDE7FFDDDAD4FF41403EFF2E2D2BFF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFF2E2E2CFF3B3B
          39FFBEBCB6FFF0EEE8FFF4F1EDFFD3D1CEFF656463FFF2F2F0FFFCFCFBFFD7D7
          D5FF757472FFEDECE8FFF2F0EBFFECE9E3FF82817DFF302F2DFF2E2D2BFF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF2E2D2CFF2F2E
          2CFF3E3D3AFFC0BEB9FFF2F0EBFFF5F3EFFFE7E6E3FFF7F6F4FFF9F8F6FFF3F2
          F0FFEFEEEBFFF4F2EEFFEEEBE6FF85837FFF31312FFF2F2E2CFF2E2D2BFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF2E2D2BFF2F2E
          2CFF2F2E2DFF3E3D3BFFC1BFBAFFF3F1ECFFF5F3EFFFF6F5F1FFF7F5F2FFF6F4
          F1FFF5F3EEFFEFECE7FF858481FF32312FFF2F2E2DFF2F2E2CFF2E2D2BFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF2E2D2BFF2E2E
          2CFF2F2E2CFF2F2F2DFF3F3E3CFFC1BFBAFFF2F0EBFFF4F1EDFFF4F2EDFFF3F1
          ECFFEEECE7FF858481FF323130FF2F2F2DFF2F2E2CFF2E2D2BFF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF2E2D
          2BFF2F2E2CFF2F2E2DFF2F2F2DFF3E3D3BFFC0BDB9FFF0EEE8FFF1EEE9FFEDEA
          E4FF858380FF323130FF2F2F2DFF2F2E2CFF2E2E2CFF2E2D2BFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF2E2D2BFF2F2E2CFF2F2E2CFF2F2F2DFF3E3C3BFFBAB8B3FFDBD8D1FF8281
          7DFF32312FFF2F2E2DFF2F2E2CFF2E2E2CFF2E2D2BFF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF2E2D2BFF2E2E2CFF2F2E2CFF2F2E2CFF353432FF3E3D3BFF302F
          2EFF2F2E2CFF2F2E2CFF2E2D2CFF2E2D2BFF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2CFF2E2E2CFF2F2E2CFF2E2E
          2CFF2E2D2BFF2E2D2BFF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097673
          6C86C4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A102929283F5555537F706F6DA572716FA87171
          6FA8626260943939385A14141420020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A0A090F4C4B4A6EAEAAA3D8C9C4BAF8C3BEB5FDBDB8AEFFAFABA2FFC2BD
          B2FFC5C0B6FBCAC5BAF5A19F99D7626261971B1B1B2C01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002322
          202CA8A49CCDC2BEB3FD73716BFF42403DFF343330FF2E2D2AFF2E2D2AFF2F2E
          2BFF393735FF4E4D48FFA09C94FFCCC7BCF99B9893D340404066040404060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013433303FC3BE
          B4E8827F79FF373633FF2E2D2BFF2F2E2CFF3D3C39FF474643FF474643FF4443
          40FF353431FF2E2D2BFF2F2E2BFF4E4C48FFB8B3A9FEB7B3ACE7515151810404
          0406000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A292730C4C0B5EA6260
          5BFF2F2E2BFF302F2DFF575652FFB3B0A8FFD7D3CBFFE1DDD6FFE2DED6FFDEDB
          D2FFCCC8C0FF85837EFF3C3B38FF2E2D2BFF393734FFA5A199FFB7B3ABE74343
          436B010101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000100F0E12B8B3A9D46C6A64FF2F2E
          2BFF363532FF97958EFFE3DFD7FFEBE8E0FFEAE6DFFFADABA6FF8C8A85FFD9D6
          CFFFECE8E1FFEBE7DFFFCECAC2FF5A5954FF2E2D2BFF393734FFB8B4AAFE9A98
          94D31C1C1C2D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101016F6C657E9F9C93FE312F2CFF3332
          30FFA9A59FFFEAE6DDFFECE9E1FFEEEBE4FFD1CFC9FF353433FF302F2EFF6362
          5FFFEDEAE3FFEDEAE3FFECE8E0FFDCD9D0FF5A5854FF2E2D2BFF4D4B48FFCCC7
          BDF9626161970202020400000000000000000000000000000000000000000000
          000000000000000000000000000012121115C0BCB1E44A4844FF2E2D2BFF7876
          71FFE8E4DCFFEDE9E2FFEFECE5FFF0EEE8FFCFCCC8FF30302FFF31302FFF4A4A
          48FFE8E6E0FFF0EDE7FFEEEBE4FFECE8E0FFCDC9C2FF3C3B38FF2F2E2BFF9B97
          8FFFA4A29CD91414142100000000000000000000000000000000000000000000
          000000000000000000000000000044423E4DB5B1A6FE302F2CFF3D3C39FFD4D0
          C8FFECE8E0FFEEEBE4FFF1EEE8FFF2F0EBFFD0CFCBFF313130FF313130FF4C4B
          4AFFEBE9E4FFF2EFEAFFF0EDE7FFEDEAE3FFEBE7DFFF85837EFF2E2D2BFF4E4C
          48FFC9C5BBF53D3D3C6000000000000000000000000000000000000000000000
          000000000000000000000000000089857D9C7A7770FF2E2D2BFF676561FFE8E4
          DCFFBEBBB5FFA4A19DFFE1DEDAFFF4F3EEFFD2D1CEFF323131FF323131FF4C4C
          4BFFEDEBE7FFF3F1ECFFC7C5C1FF9F9D99FFDEDBD3FFCCC8C1FF353432FF3837
          34FFC6C1B6FC6363629600000000000000000000000000000000000000000000
          0000000000000000000000000000B4AFA5CD52504CFF2E2D2BFF9E9B94FFD3D0
          C8FF3B3A38FF30302EFF545351FFDEDDDAFFD4D3D1FF323231FF323232FF4D4D
          4CFFEEECE9FFB3B1AFFF383837FF302F2EFF716F6BFFDEDBD2FF444340FF2F2E
          2BFFC1BDB2FF737271AB02020203000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA9A69FFFD4D2
          CAFF3D3C3AFF30302EFF31302FFF545452FFBDBCBBFF323232FF333333FF4C4C
          4CFFACABA9FF393938FF31302FFF302F2EFF716F6BFFE5E1D9FF4B4946FF2E2D
          2AFFA8A59CFF83817EB805050508000000000000000000000000000000000000
          0000000000000000000000000000B8B3A9D14E4C48FF2E2D2BFFA8A59EFFEAE7
          DEFFADABA6FF383835FF31302FFF313130FF40403FFF323232FF323232FF3636
          35FF373636FF313130FF30302FFF52504FFFD9D6CFFFE2DED6FF484643FF2E2D
          2AFFBCB7ACFF757472A900000000000000000000000000000000000000000000
          0000000000000000000000000000A39F96BA64615CFF2E2D2BFF84817CFFEBE7
          DEFFECE9E2FFAFADA8FF383836FF313130FF313130FF323131FF323231FF3231
          31FF313130FF31302FFF525150FFD8D5CFFFECE9E1FFD8D4CCFF3E3D3AFF3231
          2EFFC3BEB3FE5959588500000000000000000000000000000000000000000000
          000000000000000000000000000067645E7596938AFF2E2D2AFF4E4C49FFE3DF
          D7FFECE9E1FFEEEBE4FFAFAEAAFF393836FF313130FF313130FF313130FF3131
          30FF313030FF535250FFD9D7D1FFEEEBE4FFEBE8E0FFB3B0A9FF2F2E2CFF403E
          3BFFC7C3B9F82B2B2A4100000000000000000000000000000000000000000000
          00000000000000000000000000002726232CC2BDB2F6383734FF32312EFFB2AF
          A8FFEBE7DFFFEDEAE3FFEEEBE5FFAFADAAFF383836FF31302FFF31302FFF3130
          2FFF525250FFDAD7D1FFEFECE5FFECE9E1FFE5E1D9FF585753FF2E2D2BFF6E6B
          65FFB0ADA5DA0B0B0B1100000000000000000000000000000000000000000000
          000000000000000000000000000005050506A9A49AC16E6C66FF2E2D2AFF4947
          44FFD7D4CBFFEBE8E0FFEDEAE3FFEEEBE4FFAFADA8FF383736FF30302FFF5252
          4FFFD9D6D0FFEEEBE4FFEDE9E2FFEAE6DDFF9C9993FF302F2DFF363432FFBFBB
          B1FD4E4D4C6F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000034322F3BC3BFB4F7413F3BFF2E2D
          2BFF5D5B57FFD7D4CBFFEBE7DFFFECE9E1FFECE9E2FFC6C3BEFFABA9A4FFDEDC
          D5FFEDEAE2FFECE8E1FFE8E4DCFFA9A69FFF363532FF2F2E2BFF817E77FFACA8
          A0D00A0A0A0F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000303020377736D87B1ADA3FD3736
          33FF2E2D2BFF494744FFB2B0A8FFE4E0D7FFEBE7DEFFEBE8DFFFEBE8E0FFEBE7
          DFFFE9E5DDFFD5D2C9FF7A7873FF333230FF2E2D2BFF5E5B57FFC3BFB5EA2524
          232F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008080709938F86A8B1AD
          A3FD403F3CFF2E2D2AFF32312FFF4D4C49FF84817CFFA8A59EFFA9A69FFF9E9B
          95FF676561FF3D3C39FF2E2D2BFF302F2DFF6A6863FFC6C1B5EC3735323F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000080807097673
          6C86C4BFB3F76E6B66FF383734FF2E2D2AFF2E2D2BFF2E2D2BFF2E2D2BFF2E2D
          2BFF2E2D2BFF2F2E2BFF494844FFA09C93FEB9B4AAD72D2C2933010101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000303
          02033533303CAAA59BC2C2BDB2F795928AFF64615CFF4D4B47FF494743FF5250
          4CFF75736DFFB0ACA2FEC1BDB1E5706D667F11100F1300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000050505062726232C6E6B647DA5A197BCB9B4A9D3BBB6ACDAB4AF
          A6CD908C84A44544404F14141217010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202020909080A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000100000003000000070000000D00000011000000160000001B000000210000
          00270000002E000000340000003B00000041000000480000004D0000004E0000
          004B000000460000004000000039000000320000002C00000026000000200000
          001A00000015000000100000000A000000050000000200000000000000000000
          00000000000000000003000000080000000D0000001100000016000000190000
          001D000000210000002400000026000000280000002A0000002B0000002B0000
          002B0000002A000000280000002600000024000000210000001D000000190000
          0016000000110000000D00000008000000030000000000000000000000000403
          0312070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          0520070605200706052007060520070605200706052007060520070605200706
          05200706052007060520070605200706052004040314000000001A161377372E
          29FC382F29FF382F29FF39302AFF3C332DFF403731FF443B36FF473E39FF4C44
          3EFF514943FF554E48FF58514CFF5C544FFF5F5853FF615B56FF635C57FF655E
          59FF665F5AFF665F5AFF665F5AFF655F5AFF645E59FF645E58FF635D57FF625A
          56FF615A56FF605853FF5E5752FF605954FF5A524DFD26221E90362F29E1615A
          55FF9F9B98FFA09B99FFA19C99FFA29E9BFFA29E9BFFA39F9CFFA4A09EFFA6A2
          9FFFA8A3A1FFA9A5A2FFABA7A5FFACA8A5FFADA9A6FFAFABA8FFB0ACA9FFB0AD
          AAFFB1AEABFFB2AFACFFB2AFADFFB3B0ADFFB4B0AEFFB5B1AFFFB5B1AFFFB5B2
          AFFFB6B2B0FFB6B3B0FFB6B3B1FFB7B4B1FF8D8884FF524A45F9453D39EAB3AF
          ADFFDEDDDCFFE0DEDDFFE1E0DFFFE3E2E1FFE4E3E2FFE5E4E3FFE7E6E5FFE9E8
          E7FFEAE9E8FFEBEBEAFFECECEBFFEDEDECFFEEEEEDFFEEEEEDFFEFEFEEFFF0EF
          EEFFF0EFEFFFEFEFEEFFEFEFEEFFEFEEEEFFEEEDEDFFEDEDECFFEDECECFFECEB
          EBFFEBEAE9FFEBEAE9FFEAE9E8FFE9E8E7FFD0CDCCFF57504BFF453D39EAB5B2
          AFFFE0DEDDFFDDD0C4FFD5AB88FFD5AC8AFFD6AD8BFFD8B08FFFD9B190FFDAB2
          93FFDBB494FFDAB697FFDCB799FFDEB99CFFDDBB9EFFDEBC9FFFE0BEA0FFDFBE
          A3FFDEBFA4FFE1C1A7FFE1C3A8FFE0C3AAFFE0C4ABFFE0C5ACFFE0C3ADFFE0C4
          AEFFE2C7B1FFE1C8B2FFE2CEBFFFEAE9E8FFD2D0CEFF58504BFF453D39EAB6B2
          B0FFE1DFDEFFDDCCBCFFD19F74FFD3A175FFD4A177FFD3A379FFD4A47CFFD6A5
          7CFFD5A67FFFD7A881FFD6A983FFD8AB85FFD9AB87FFD9AF89FFDAAE8AFFDBAF
          8DFFDBB290FFDCB392FFDCB593FFDCB596FFDDB797FFDBB799FFDCB99BFFDDB9
          9CFFDDBA9EFFDDBB9FFFE0C6B1FFEBEAEAFFD2D0CFFF58514CFF453D39EAB6B3
          B1FFE1E0DFFFDDCCC0FFD3A47BFFD3A47DFFD4A57DFFD5A880FFD5A881FFD7AA
          84FFD8AA86FFD8AD87FFDBAF8BFFD9AE8BFFDBB18FFFDDB491FFDDB492FFDEB7
          94FFDEB796FFDFB898FFDEB89AFFDFBB9CFFE0BB9EFFDEBB9EFFE0BEA0FFDEBD
          A0FFDFBDA2FFDFC0A5FFDFC8B5FFEDECEBFFD3D1D0FF59514CFF453D39EAB6B3
          B1FFE1E0DFFFE4E2E2FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF1F1F0FFF4F3F3FFF5F5F5FFF8F7F7FFF9F9F9FFFBFBFAFFFCFCFCFFFDFD
          FDFFFCFCFCFFFBFBFBFFFAF9F9FFF8F7F7FFF7F6F6FFF5F4F4FFF4F3F3FFF2F1
          F1FFF1F0F0FFEFEEEEFFEEEDECFFEDECEBFFD3D1D0FF59514CFF453D39EAB6B2
          B0FFE1E0DFFFE3E2E1FFE5E4E3FFE8E7E6FFE9E8E8FFECEBEAFFEDEDECFFF0EF
          EFFFF2F1F0FFF4F3F3FFF5F5F5FFF7F7F6FFF9F9F9FFFBFBFAFFFCFCFCFFFDFD
          FDFFFCFCFCFFFBFBFBFFF9F9F9FFF8F7F7FFF6F6F5FFF5F4F4FFF3F2F2FFF2F1
          F1FFF0F0EFFFEFEEEEFFEEEDECFFECEBEBFFD3D1D0FF59524DFF453D39EAB5B2
          AFFFE1DFDEFFDDCEC1FFD4A57EFFD3A580FFD5A781FFD6A884FFD6A984FFD8AD
          88FFD9AE8AFFD8AD8AFFDAB08EFFDCB290FFDCB492FFDEB594FFDEB695FFDEB7
          97FFDEB898FFDFBA9BFFDFBB9CFFDFBB9DFFDDB99DFFDDBB9FFFDEBB9FFFDEBD
          A0FFDEBDA2FFDFBEA4FFE0C8B4FFECEBEAFFD3D1D0FF59524DFF453D39EAB4B1
          AEFFDFDEDDFFDCC6B4FFD0996CFFD19A6CFFD19B6DFFD19B6EFFD39D70FFD29E
          72FFD49E74FFD49F74FFD3A075FFD4A277FFD6A279FFD5A47AFFD6A57DFFD6A7
          7EFFD6A67FFFD8A881FFD6A983FFD8AA84FFD9AC85FFD7AC87FFD9AE8AFFD8AD
          8AFFD8AE8CFFD9B08EFFDBBCA0FFEAEAE9FFD2D0CFFF5A524DFF453D39EAB3AF
          ADFFDDDBDAFFDBC7B8FFD09C70FFD29C71FFD29F73FFD19F74FFD4A076FFD2A0
          76FFD3A278FFD5A37AFFD5A47BFFD6A67EFFD6A57EFFD7A881FFD7AA83FFD7AA
          84FFD9AB85FFD7AB86FFD9AD87FFD9AE8AFFD9AD8BFFDAAF8DFFD9B08EFFD8B1
          90FFD9B08FFFD9B291FFDDBEA4FFE9E8E7FFD1CFCDFF5A534EFF453D39EAB1AD
          ABFFDAD9D8FFDCDAD9FFDDDCDBFFDFDEDDFFE1DFDEFFE3E1E0FFE4E3E2FFE6E5
          E4FFE7E6E5FFE9E8E7FFEAE9E8FFEBEAE9FFECEBEAFFECEBEBFFEDECECFFEDEC
          ECFFEDEDECFFEDECECFFEDECEBFFEDECEBFFECEBEAFFEBEAEAFFEBEAEAFFEAE9
          E8FFE9E8E7FFE8E7E6FFE7E6E6FFE6E5E5FFCFCDCCFF5A534EFF453D39EAAFAC
          A9FFD8D6D5FFDAD8D7FFDAD9D8FFDCDAD9FFDEDCDBFFE0DEDDFFE1DFDEFFE3E1
          E0FFE4E2E2FFE5E3E3FFE6E5E4FFE7E6E5FFE8E7E6FFE9E8E7FFE9E8E7FFE9E8
          E8FFE9E8E8FFE9E8E7FFE9E8E8FFE9E8E7FFE9E8E7FFE8E7E7FFE8E7E6FFE7E6
          E5FFE7E6E5FFE6E5E4FFE5E4E3FFE4E3E2FFCECCCAFF5A534EFF453D39EAADA9
          A7FFD5D3D2FFD4C2B3FFD19E73FFD09D74FFD1A075FFD2A077FFD2A178FFD3A2
          7BFFD2A27BFFD2A37BFFD3A57EFFD3A57FFFD3A680FFD5A883FFD5A783FFD4AA
          84FFD7AA85FFD6AB86FFD6AB89FFD7AE8BFFD8B18FFFD8B190FFD7B290FFD8B1
          91FFD9B494FFD9B596FFDBC0A7FFE4E3E2FFCECCCBFF5B534EFF453D39EAABA7
          A4FFD1CFCDFFD2BAA7FFD09564FFCF9665FFCF9667FFCF9566FFCF9667FFCF97
          68FFD09869FFD0976AFFD0996BFFD0996BFFD09B6CFFD19B6DFFD29B6FFFD19D
          70FFD19C70FFD29D72FFD19E73FFD29F74FFD3A279FFD4A47EFFD5A77FFFD5A7
          81FFD7AA86FFD8AA87FFD9B699FFE5E4E3FFD0CECDFF5B544FFF453D39EAA8A4
          A1FFCECCCAFFD0B8A6FFD09769FFCE9669FFCF976BFFD09A6CFFD09A6DFFD19C
          6FFFD09A6DFFD19A6FFFD09B6FFFD19C70FFD29C71FFD09D72FFD19F73FFD29E
          74FFD29F77FFD2A278FFD2A277FFD3A279FFD3A47AFFD5A983FFD6AB87FFD7AF
          8CFFDBB191FFD8B190FFDCBCA2FFE6E5E4FFD3D1D0FF5C544FFF453D39EABAB6
          B4FFD8D7D6FFD8D6D5FFD6D5D3FFD8D5D4FFDAD8D7FFDBDAD8FFDDDBDAFFDEDC
          DBFFDEDDDBFFD8D6D5FFD6D4D3FFD7D5D4FFD8D6D5FFD8D7D5FFD9D7D6FFD9D7
          D6FFDAD8D7FFDAD9D7FFDBD9D8FFDBD9D8FFDBD9D8FFDDDBDAFFE3E2E1FFE7E6
          E6FFE8E8E8FFE9E7E7FFE8E7E6FFE7E6E5FFD5D4D3FF5C544FFF453D39EABEBB
          B9FFDDDBDBFFDFDEDDFFE0DFDEFFE0DEDEFFDEDDDBFFDDDBDAFFDCDAD9FFDDDC
          DAFFDEDDDCFFE0DEDDFFDAD8D7FFD3D1D0FFD4D2D1FFD4D2D1FFD6D4D3FFD6D4
          D3FFD6D4D3FFD6D4D3FFD7D5D4FFD7D6D4FFD7D5D4FFD7D5D4FFDDDCDBFFE6E4
          E4FFEBE9E9FFEAE9E8FFE9E8E8FFE8E7E6FFD8D6D6FF5C5550FF453D39EAC2BF
          BDFFDFDEDDFFE0D0C4FFD7AA83FFD9AB85FFD9AE88FFD8AD89FFDAAE8BFFDAAC
          89FFD7AB86FFD6A983FFD8AA85FFD5A881FFD09E77FFD09F75FFD09E75FFCFA0
          77FFD09F77FFD09F77FFCFA079FFD1A27BFFD0A27BFFD0A37DFFD0A37DFFD5AC
          8AFFDDBB9CFFDEBC9FFFE1C6B0FFEAE8E8FFDBD9D8FF5C5550FF453D39EAC6C3
          C1FFE1E0DFFFE1CDBEFFD49C6FFFD39D70FFD59F72FFD4A073FFD5A176FFD6A0
          76FFD5A378FFD6A379FFD6A277FFD39F73FFD5A075FFD29A6DFFCF9666FFD097
          67FFCE9667FFD09768FFCF9769FFCF986AFFCF986BFFD0986CFFCF996CFFD09A
          6EFFD2A279FFDBB08DFFE0BDA2FFECEBEAFFDDDCDBFF5D5550FF453D39EAC9C7
          C5FFE4E3E2FFE3D3C5FFD7A47AFFD7A57CFFD7A77DFFD8A780FFD8A981FFD8A9
          82FFDAAC84FFD9AB84FFDAAD86FFDBAE89FFD9AB87FFD8AA84FFD5A57DFFD19B
          70FFCF986BFFCF9A6CFFCF9A6EFFCE996EFFCF9B6FFFCE9B6EFFCE9B70FFD09D
          71FFCE9C72FFD3A67FFFE1C1A7FFEDEDECFFE0DEDDFF5D5651FF453D39EACCCA
          C9FFE6E5E4FFE7E7E6FFE9E8E7FFEBEAEAFFECEBEBFFEEEDECFFEFEEEEFFEFEF
          EEFFF1F0EFFFF1F0F0FFF1F1F1FFF2F2F1FFF3F2F2FFF3F2F2FFF1F1F1FFECEB
          EAFFD9D8D7FFC8C6C4FFC7C5C3FFC7C5C4FFC7C5C4FFC8C6C5FFC8C6C4FFC8C6
          C4FFC9C7C5FFC9C7C5FFCCCAC8FFE4E2E2FFE2E0E0FF5D5651FF3C3430E9908E
          8BFFD0D0CFFFD3D3D3FFD5D5D5FFD7D7D6FFD9D9D9FFDBDBDAFFDCDCDCFFDEDD
          DDFFDFDFDEFFE0E0DFFFE1E1E1FFE2E1E1FFE2E2E2FFE3E3E2FFE3E3E3FFE3E3
          E3FFE2E2E1FFD3D3D3FFA4A3A3FF939291FF919090FF919090FF919090FF9291
          90FF91908FFF91908FFF919090FF91908FFF888686FF5E5752FF332B25E8362D
          27FF312924FF312924FF332B26FF352D28FF372F2AFF38312CFF3A332EFF3C35
          30FF3E3731FF403834FF423B36FF443D37FF453E3AFF47413CFF49433EFF4B44
          40FF4D4642FF4F4844FF514A45FF514B47FF433C37FF312A24FF312924FF3129
          24FF312924FF312924FF312924FF312924FF342B26FF3E352FFF332B25E8382F
          29FF382F29FF39302AFF3B322CFF3D352FFF3F3731FF423933FF433B35FF463D
          38FF48403AFF4A423CFF4C443FFF4F4741FF504943FF534B46FF554D48FF574F
          4AFF59524DFF5B544FFF5D5651FF5F5853FF625B56FF5B544FFF403731FF382F
          29FF382F29FF2F2823FF372E28FF2F2823FF362E28FF382F29FF332B25E8382F
          29FF382F29FF3A312BFF3B332DFF3E352FFF403731FF423934FF443C36FF463E
          38FF48403BFF4B433DFF4D453FFF4F4742FF514944FF534B46FF554E49FF5750
          4BFF5A524DFF5C544FFF5E5752FF605954FF625B56FF645D59FF655E59FF4B43
          3EFF2E2823FF4D4D4DFF292320FF323232FF252220FF382F29FF413A37C44840
          3AFF423933FF443B35FF463D38FF48403BFF4D4540FF524B45FF57504BFF5B54
          4FFF5F5853FF635C57FF66605BFF69625EFF6D6662FF6E6764FF716A65FF726C
          67FF746E69FF746E6AFF746E6AFF756F6BFF746F6BFF756F6AFF746E6BFF746F
          6BFF605955FF27221FFF3D342FFF2B2521FF3B3530FF433C38D90B0A09275954
          52AB696563BE6A6663BE6A6764BF6B6764BF6B6765BF6C6966C06D6966C06D69
          67C06D6967C06E6A67C06F6B69C06F6B69C0706B69C1706C6AC1716D6BC2716E
          6BC2726E6BC2726E6CC2726F6CC2736F6DC2746F6EC274706EC375716EC37571
          70C376736FC4716E6BC2696563BE696563BE5C5855AF0B09082E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000151A0000617900008DB0000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008DB0000061790000151A000000000000000000000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C4F5000054690000000000000000000000000000
          54690000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF00005469000000000000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A0000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB1000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0E0ECEFFA7A7EDFFC2C2F3FF8282
          E6FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF4E4EDBFFFFFFFFFFFFFFFFFFFEFE
          FEFF0A0ACEFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0A0ACEFF7D7DE5FF5454DCFF0404CCFF5454DCFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF5252DCFFFFFFFFFFFFFFFFFFDBDBF7FFA7A7EDFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF4646DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0202CCFF7E7EE5FFF0F0FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF1616D0FFA8A8EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF7A7AE4FFFEFEFEFFFFFFFFFFFEFE
          FEFF0C0CCEFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF8080E5FFE9E9FAFFAEAE
          EEFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB10000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A0000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A000000000000
          54690000CBFE0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CBFE0000546900000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C5F6000054690000000000000000000000000000
          0000000000000000151A0000617900008CAF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008CAF000061790000151A000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000151A0000617900008DB0000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008DB0000061790000151A000000000000000000000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C4F5000054690000000000000000000000000000
          54690000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF00005469000000000000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A0000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB1000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF2323D3FF4444D9FF4545D9FF4545D9FF4545D9FF4545D9FF4545D9FF4545
          D9FF4545D9FF4545D9FF4343D9FF1616D0FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF1515
          D0FFF8F8FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1F9FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0F0F
          CFFFF9F9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFEFF0202CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF9C9CEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADAF7FFCCCCF5FFCCCC
          F5FFCCCCF5FFCCCCF5FFCACAF4FF8181E5FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF1010CFFFD8D8F7FFFFFFFFFFFFFFFFFFFFFFFFFFBFBFF2FF1919D1FF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF1C1CD1FFD5D5F6FFFFFFFFFFFFFFFFFFFFFFFFFFE8E8FAFF4545
          D9FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0F0FCFFFAFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFF7B7BE4FF0101CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0101CCFF6E6EE2FFF8F8FDFFFFFFFFFFFFFF
          FFFFFFFFFFFFA3A3ECFF0404CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF3535D6FFE2E2F9FFFFFF
          FFFFFFFFFFFFFFFFFFFFA2A2ECFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF1A1AD1FFD3D3
          F6FFFFFFFFFFFFFFFFFFFFFFFFFF5959DDFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF1F1FD2FF2C2CD4FF0202CCFF0000CCFF0000CCFF0000CCFF1B1B
          D1FFECECFBFFFFFFFFFFFFFFFFFFC8C8F4FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF9191E9FFFFFFFFFFFFFFFFFF8585E6FF0000CCFF0000CCFF0000CCFF0000
          CCFF9292E9FFFFFFFFFFFFFFFFFFF6F6FDFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFFACACEEFFFFFFFFFFFFFFFFFFE2E2F9FF0B0BCEFF0000CCFF0000CCFF0000
          CCFFB1B1EFFFFFFFFFFFFFFFFFFFECECFBFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF5858DDFFFFFFFFFFFFFFFFFFFFFFFFFFD3D3F6FF7878E4FF6F6FE2FFB6B6
          F0FFFFFFFFFFFFFFFFFFFFFFFFFFA1A1ECFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0202CCFFB5B5F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE6E6FAFF1C1CD1FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0808CDFF8E8EE8FFF6F6FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFEFEFFB8B8F0FF2020D2FF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF1010CFFF4D4DDBFF6E6EE2FF7272E2FF5C5C
          DEFF2323D3FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB10000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A0000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A000000000000
          54690000CBFE0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CBFE0000546900000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C5F6000054690000000000000000000000000000
          0000000000000000151A0000617900008CAF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008CAF000061790000151A000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000280F0439712D0EA3923E16D3994217DD984219DC984219DC9842
          19DC984219DC984219DC984219DC984219DC984219DC984219DC984219DC9842
          19DC984219DC984219DC984219DC984219DC984219DC984219DC984219DC9942
          17DD923E16D3712D0EA3270F0439000000000000000000000000000000000000
          00007C3210B0B25729FFB45A2DFFB4592CFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592CFFB45A2DFFB25729FF7C320FB00000000000000000000000007D34
          11B0B45C2FFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB45C2FFF7D3411B00000000029110639B65C
          2DFFB85D2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85D2FFFB65C2DFF29110639783716A4BA60
          32FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6032FF783716A49D4C21D4BB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FF9D4C21D4A65326DDBE66
          36FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBE6636FFA65328DDA85629DCC169
          39FFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC168
          39FFC06635FFBF6432FFBF6432FFBF6432FFBF6432FFBF6432FFBF6432FFBF64
          32FFBF6432FFBF6432FFBF6432FFC16636FFC16839FFC16839FFC16839FFC168
          39FFC16839FFC16839FFC16839FFC16839FFC16939FFA85629DCAC5A2EDCC66D
          3BFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC468
          35FFC87545FFD28E68FFD28C65FFD28C65FFD28C65FFD28C65FFD28C65FFD28C
          65FFD28C65FFD28C65FFD18E68FFC66B3AFFC56B38FFC66D3AFFC66D3AFFC66D
          3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3BFFAC5B2EDCAF5E31DCC870
          3EFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC86E3BFFC970
          3DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC4602BFFC8703DFFC9703EFFC970
          3EFFC9703EFFC9703EFFC9703EFFC9703EFFC8703EFFAF5E31DCB26135DCCB73
          41FFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCB713FFFC96E
          39FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7652EFFCB7240FFCC7341FFCC73
          41FFCC7341FFCC7341FFCC7341FFCC7341FFCB7341FFB26135DCB46637DCCE76
          43FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCA6A
          33FFEDCFBEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7E8E1FFF7EBE3FFF7ED
          E6FFF7EDE6FFF7EDE6FFF9F0EAFFE7BFA7FFCC6D37FFCF7642FFCF7642FFCF76
          42FFCF7642FFCF7642FFCF7642FFCF7642FFCE7643FFB46637DCB7693BDCD078
          46FFD07946FFD07946FFD07946FFD07946FFD07946FFD07946FFD07946FFD077
          44FFCD6F37FFFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFF3DDD0FFCA652BFFCA66
          2CFFCC6B34FFCC6B34FFCC6B34FFCD703AFFD07946FFD07946FFD07946FFD079
          46FFD07946FFD07946FFD07946FFD07946FFD07846FFB7693BDCB96D3FDCD37B
          47FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C
          48FFD27943FFD27842FFFDF9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDA92
          68FFD07037FFD47C47FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C
          48FFD47C48FFD47C48FFD47C48FFD47C48FFD37B47FFB96D3FDCBC6F42DCD57E
          49FFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F
          4AFFD67F4AFFD47C45FFD27339FFF4E0D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEABCA0FFD06E33FFD57E49FFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F
          4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD57E49FFBC6F42DCBE7244DCD780
          4BFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD881
          4CFFD8814CFFD8814CFFD77F4AFFD27135FFE7B394FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF3D9CAFFD37235FFD7804BFFD8814CFFD8814CFFD8814CFFD881
          4CFFD8814CFFD8814CFFD8814CFFD8814CFFD7804BFFBE7244DCC07447DCD982
          4DFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA83
          4EFFDA834EFFDA834EFFDA834EFFDA824DFFD67940FFDD8D5DFFFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF3D9C8FFD57639FFDA824EFFDA834EFFDA834EFFDA83
          4EFFDA834EFFDA834EFFDA834EFFDA834EFFD9824DFFC07447DCC17749DCDB84
          4FFFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC85
          50FFDC8450FFDB824DFFDB824CFFDC844FFFDC8550FFDA8049FFD87F46FFFDF9
          F7FFFFFFFFFFFFFFFFFFFFFFFFFFE6AA84FFDA7F49FFDC8550FFDC8550FFDC85
          50FFDC8550FFDC8550FFDC8550FFDC8550FFDB844FFFC17748DCC37A4BDCDD86
          4FFFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE86
          50FFDB7E44FFDF8C58FFE09565FFDB7E45FFDE8650FFDE8750FFDD834CFFDB81
          48FFFFFFFFFFFFFFFFFFFFFFFFFFF9F0EAFFDB7B40FFDE8750FFDE8750FFDE87
          50FFDE8750FFDE8750FFDE8750FFDE8750FFDD864FFFC37A4BDCC47B4EDCDE88
          51FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDC80
          47FFF3D3C0FFFFFFFFFFFFFFFFFFF0C9B0FFDC8047FFDE8953FFDE8953FFDC7F
          45FFEFC7AFFFFFFFFFFFFFFFFFFFFFFFFFFFDA7B41FFDE8952FFDE8953FFDE89
          53FFDE8953FFDE8953FFDE8953FFDE8953FFDE8851FFC47B4EDCC67E50DCE089
          53FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFDE81
          47FFF7E3D7FFFFFFFFFFFFFFFFFFFDFFFFFFDC7B3DFFDE844AFFDF844BFFDC78
          39FFF5DDCEFFFFFFFFFFFFFFFFFFFFFFFFFFDD7E42FFE18B55FFE18B55FFE18B
          55FFE18B55FFE18B55FFE18B55FFE18B55FFE08953FFC67E50DCC68053DCE18C
          54FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE088
          4FFFEAAF8AFFFFFFFFFFFFFFFFFFFFFFFFFFFCF5F1FFEDBC9EFFEBB797FFF6E3
          D5FFFFFFFFFFFFFFFFFFFFFFFFFFF5DBCAFFDF8449FFE18D55FFE18D55FFE18D
          55FFE18D55FFE18D55FFE18D55FFE18D55FFE18C54FFC68052DCC88155DCE38C
          56FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D
          58FFE08144FFF9E7DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE38C56FFE38B55FFE48D58FFE48D58FFE48D
          58FFE48D58FFE48D58FFE48D58FFE48D58FFE38C56FFC88155DCC98356DCE58E
          57FFE69058FFE69058FFE69058FFE69058FFE69058FFE69058FFE69058FFE690
          58FFE68F57FFE18548FFF4D0BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFAE9DFFFE58F59FFE58C54FFE69058FFE69058FFE69058FFE690
          58FFE69058FFE69058FFE69058FFE69058FFE58E57FFC98356DCCA855ADCE68F
          59FFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE790
          5BFFE7905BFFE7905AFFE4884EFFE48A52FFECAE87FFF0C0A1FFF2C1A3FFEFB6
          94FFE79662FFE4854AFFE68E58FFE7905BFFE7905BFFE7905BFFE7905BFFE790
          5BFFE7905BFFE7905BFFE7905BFFE7905BFFE68F59FFCA855ADCCC875BDDE791
          59FFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE893
          5CFFE8935CFFE8935CFFE8935CFFE7925AFFE68E55FFE68D53FFE68D52FFE68E
          55FFE79059FFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE893
          5CFFE8935CFFE8935CFFE8935CFFE8935CFFE79159FFCC875CDDC4845BD4E892
          5AFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8925AFFC4835AD498694CA4EA92
          5BFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEA925CFF98694BA435261C39EB97
          60FFEC955FFFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC955FFFEB9760FF35261C3900000000A475
          57B0EC935CFFED975FFFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED975FFFEC945CFFA47557B000000000000000000000
          0000A57658B0ED9963FFEE965EFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE965EFFED9964FFA57658B00000000000000000000000000000
          00000000000034261D38986B4FA2C68962D3CF8E65DDCE8D66DCCE8D66DCCE8D
          66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D
          66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCF8E
          65DDC68962D3986B4FA234261D38000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000020101026A442271A16834ACAE7038BAAD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AE70
          38BAA16834AC6A44227102010102000000000000000000000000000000000000
          00005D3C1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFF5D3C1E620000000000000000000000005D3C
          1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E620000000002010102EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF020101026B452272EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF6B452272A16834ACEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFA26834ADAE7038BAEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAE7038BAAD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF994BFFED8F39FFED8D36FFED91
          3EFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF994BFFEC9241FFFBE9DAFFFDF6EFFFF7D1
          AEFFED903CFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9544FFF2B47EFFFFFFFFFFFFFFFFFFFFFF
          FFFFEC8F39FFEF994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57DFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF994BFFED913EFFEE9443FFEE9443FFF3B57CFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF99
          4BFFED9340FFF7D4B5FFF3B782FFEB8223FFF2AF72FFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE94
          43FFF4B985FFFFFFFFFFFFFFFFFFFDFCFDFFF7D2AFFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE95
          44FFF2B277FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4CFFED8D37FFF5CCA6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4CFFED903BFFEC8C36FFFAE5D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFED913EFFEE994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEE984AFFEC872BFFF5C89FFFFFFFFFFFFFFFFFFFFFFF
          FFFFEC8F3BFFEF994BFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEC8930FFF7CDA8FFFFFFFFFFFAED
          DFFFED8D37FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFED913EFFEC8A31FFED8E
          38FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AE7038BAEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAE7038BAA16834ACEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFA26834AD6B452272EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF6B45227202010102EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF02010102000000005D3C
          1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E6200000000000000000000
          00005C3B1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E620000000000000000000000000000
          000000000000020101026A442271A06734ABAF7138BBAD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AF71
          38BBA06734AB6A44227102010102000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000020101026A442271A16834ACAE7038BAAD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AE70
          38BAA16834AC6A44227102010102000000000000000000000000000000000000
          00005D3C1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFF5D3C1E620000000000000000000000005D3C
          1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E620000000002010102EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF020101026B452272EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF6B452272A16834ACEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFA26834ADAE7038BAEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAE7038BAAD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEE9749FFEE9545FFEE9545FFEE9545FFEE9545FFEE9545FFEE9545FFEE95
          45FFEE9545FFEE9545FFEE9545FFEE984AFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE96
          46FFEF9A50FFF2AF73FFF1AD6FFFF1AD6FFFF1AD6FFFF1AD6FFFF1AD6FFFF1AD
          6FFFF1AD6FFFF1AD6FFFF2AE73FFED9444FFEE984AFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE984AFFEE96
          47FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEC8B32FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF994BFFEC92
          3EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEC8B33FFEF994CFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFED8E
          39FFF8DDC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEDE1FFFCF2E9FFFCF5
          EEFFFCF5EEFFFCF5EEFFFDF8F3FFF7D1AEFFED913DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE99
          4BFFED8B34FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E5D3FFEA7F20FFEC86
          2AFFED8C35FFED8C35FFED8C35FFED913EFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEE9748FFEC903BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0A7
          64FFED8D36FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEE9748FFEB8A30FFFCEADBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF5CAA1FFEC872BFFEF994CFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF994BFFEC872BFFF3C092FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFAE2CFFFEB862CFFEF994CFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFED8F3AFFEF9D53FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFAE2CDFFEC892FFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEE9849FFEE9748FFEF9A4CFFEF9A4DFFEE9443FFEC8E38FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2B67FFFEE9442FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFED8E38FFED9A4DFFEFA35DFFED8E3AFFEF9A4CFFEF9A4DFFEE9647FFEC8D
          37FFFFFFFFFFFFFFFFFFFFFFFFFFFDF6F1FFED8C35FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFED90
          3CFFF9DCC3FFFFFFFFFFFFFFFFFFF7D2AFFFED903CFFEF9A4DFFEF9A4DFFED8E
          39FFF7CEA9FFFFFFFFFFFFFFFFFFFFFFFFFFEC8930FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFED8E
          39FFFAEBDCFFFFFFFFFFFFFFFFFFFFFFFFFFEA8425FFED913DFFEE9240FFEB82
          23FFFAE3CDFFFFFFFFFFFFFFFFFFFFFFFFFFEC8A31FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE94
          42FFF3B880FFFFFFFFFFFFFFFFFFFFFFFFFFFEFAF7FFF4C296FFF3BE8CFFFBE9
          D8FFFFFFFFFFFFFFFFFFFFFFFFFFFAE2CCFFED8F3AFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4CFFEC882FFFFCEFE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEC9240FFEE9849FFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF994CFFEB892FFFF7D5B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFBF0E5FFEE9241FFEE9646FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AD7038B9EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF994CFFED8F3AFFED8E39FFF1B37AFFF4C497FFF6C79BFFF3BB
          88FFEF9A4CFFEC8B33FFEE9849FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAD7038B9AE7038BAEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEE994BFFEE9544FFEE9240FFED923FFFEE94
          42FFEE9749FFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFAE7038BAA16834ACEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFA26834AD6B452272EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF6B45227202010102EF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF02010102000000005D3C
          1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E6200000000000000000000
          00005C3B1E62EF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A4DFFEF9A
          4DFFEF9A4DFFEF9A4DFFEF9A4DFF5C3B1E620000000000000000000000000000
          000000000000020101026A442271A06734ABAF7138BBAD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD70
          38B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AD7038B9AF71
          38BBA06734AB6A44227102010102000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000280F0439712D0EA3923E16D3994217DD984219DC984219DC9842
          19DC984219DC984219DC984219DC984219DC984219DC984219DC984219DC9842
          19DC984219DC984219DC984219DC984219DC984219DC984219DC984219DC9942
          17DD923E16D3712D0EA3270F0439000000000000000000000000000000000000
          00007C3210B0B25729FFB45A2DFFB4592CFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB4592BFFB459
          2BFFB4592CFFB45A2DFFB25729FF7C320FB00000000000000000000000007D34
          11B0B45C2FFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B2DFFB65B
          2DFFB65B2DFFB65B2DFFB65B2DFFB45C2FFF7D3411B00000000029110639B65C
          2DFFB85D2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C2FFFB85C
          2FFFB85C2FFFB85C2FFFB85C2FFFB85D2FFFB65C2DFF29110639783716A4BA60
          32FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA6030FFBA60
          30FFBA6030FFBA6030FFBA6030FFBA6030FFBA6032FF783716A49D4C21D4BB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FFBB63
          34FFBB6334FFBB6334FFBB6334FFBB6334FFBB6334FF9D4C21D4A65326DDBE66
          36FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF6635FFBF66
          35FFBF6635FFBF6635FFBF6635FFBF6635FFBE6636FFA65328DDA85629DCC169
          39FFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC168
          39FFC16839FFC16839FFC16839FFC16839FFC16737FFBD5D29FFBC5B27FFBE5F
          2DFFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC16839FFC168
          39FFC16839FFC16839FFC16839FFC16839FFC16939FFA85629DCAC5A2EDCC66D
          3BFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D
          3AFFC66D3AFFC66D3AFFC66D3AFFC56B39FFC46835FFF0D9CDFFF7EBE4FFE3BB
          A5FFC2632DFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D
          3AFFC66D3AFFC66D3AFFC66D3AFFC66D3AFFC66D3BFFAC5B2EDCAF5E31DCC870
          3EFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC970
          3EFFC9703EFFC9703EFFC9703EFFC66A37FFD99975FFFFFFFFFFFFFFFFFFFFFF
          FFFFC56631FFC86F3CFFC9703EFFC9703EFFC9703EFFC9703EFFC9703EFFC970
          3EFFC9703EFFC9703EFFC9703EFFC9703EFFC8703EFFAF5E31DCB26135DCCB73
          41FFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCC73
          41FFCC7341FFCC7341FFCC7341FFC96D39FFDA9B77FFFFFFFFFFFFFFFFFFFFFF
          FFFFC96E38FFCB713FFFCC7341FFCC7341FFCC7341FFCC7341FFCC7341FFCC73
          41FFCC7341FFCC7341FFCC7341FFCC7341FFCB7341FFB26135DCB46637DCCE76
          43FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF76
          42FFCF7642FFCF7642FFCF7642FFCC703BFFDC9D79FFFFFFFFFFFFFFFFFFFFFF
          FFFFCD713BFFCE7440FFCF7642FFCF7642FFCF7642FFCF7642FFCF7642FFCF76
          42FFCF7642FFCF7642FFCF7642FFCF7642FFCE7643FFB46637DCB7693BDCD078
          46FFD07946FFD07946FFD07946FFD07946FFD07946FFD07946FFD07946FFD079
          46FFD07946FFD07946FFD07946FFCE733FFFDD9F7BFFFFFFFFFFFFFFFFFFFFFF
          FFFFCF743DFFD07744FFD07946FFD07946FFD07946FFD07946FFD07946FFD079
          46FFD07946FFD07946FFD07946FFD07946FFD07846FFB7693BDCB96D3FDCD37B
          47FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C
          48FFD47C48FFD47C48FFD47C48FFD17641FFDFA17CFFFFFFFFFFFFFFFFFFFFFF
          FFFFD27640FFD37A46FFD47C48FFD47C48FFD47C48FFD47C48FFD47C48FFD47C
          48FFD47C48FFD47C48FFD47C48FFD47C48FFD37B47FFB96D3FDCBC6F42DCD57E
          49FFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F
          4AFFD67F4AFFD67F4AFFD67F4AFFD37A43FFE0A37FFFFFFFFFFFFFFFFFFFFFFF
          FFFFD47942FFD57D48FFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD67F
          4AFFD67F4AFFD67F4AFFD67F4AFFD67F4AFFD57E49FFBC6F42DCBE7244DCD780
          4BFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD881
          4CFFD8814CFFD8814CFFD8814CFFD57C45FFE3A67FFFFFFFFFFFFFFFFFFFFFFF
          FFFFD67B44FFD77F4AFFD8814CFFD8814CFFD8814CFFD8814CFFD8814CFFD881
          4CFFD8814CFFD8814CFFD8814CFFD8814CFFD7804BFFBE7244DCC07447DCD982
          4DFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA83
          4EFFD9824CFFD77C43FFD87E47FFD87D46FFE3A780FFFFFFFFFFFFFFFFFFFFFF
          FFFFD77E46FFD9814CFFDA834EFFDA834EFFDA834EFFDA834EFFDA834EFFDA83
          4EFFDA834EFFDA834EFFDA834EFFDA834EFFD9824DFFC07447DCC17749DCDB84
          4FFFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDB84
          4FFFDA8049FFEFC9B0FFE6AA86FFD57235FFE4A37AFFFFFFFFFFFFFFFFFFFFFF
          FFFFD98048FFDB834EFFDC8550FFDC8550FFDC8550FFDC8550FFDC8550FFDC85
          50FFDC8550FFDC8550FFDC8550FFDC8550FFDB844FFFC17748DCC37A4BDCDD86
          4FFFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDC82
          4AFFE7AD88FFFFFFFFFFFFFFFFFFFCF8F5FFF0CCB7FFFFFFFFFFFFFFFFFFFFFF
          FFFFDC8249FFDD854FFFDE8750FFDE8750FFDE8750FFDE8750FFDE8750FFDE87
          50FFDE8750FFDE8750FFDE8750FFDE8750FFDD864FFFC37A4BDCC47B4EDCDE88
          51FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDD84
          4DFFE6A67EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDD834CFFDE8751FFDE8953FFDE8953FFDE8953FFDE8953FFDE8953FFDE89
          53FFDE8953FFDE8953FFDE8953FFDE8953FFDE8851FFC47B4EDCC67E50DCE089
          53FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18A
          54FFDE8148FFF0C4A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDF864CFFE08953FFE18B55FFE18B55FFE18B55FFE18B55FFE18B55FFE18B
          55FFE18B55FFE18B55FFE18B55FFE18B55FFE08953FFC67E50DCC68053DCE18C
          54FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D
          55FFE18D55FFDF854AFFE0864BFFF6DFD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE08850FFE18C54FFE18D55FFE18D55FFE18D55FFE18D55FFE18D55FFE18D
          55FFE18D55FFE18D55FFE18D55FFE18D55FFE18C54FFC68052DCC88155DCE38C
          56FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D
          58FFE48D58FFE48D58FFE38C55FFDF7E41FFF0C2A5FFFFFFFFFFFFFFFFFFFFFF
          FFFFE1884EFFE38C56FFE48D58FFE48D58FFE48D58FFE48D58FFE48D58FFE48D
          58FFE48D58FFE48D58FFE48D58FFE48D58FFE38C56FFC88155DCC98356DCE58E
          57FFE69058FFE69058FFE69058FFE69058FFE69058FFE69058FFE69058FFE690
          58FFE69058FFE69058FFE69058FFE69058FFE38245FFF2C9ADFFFFFFFFFFF9E6
          DAFFE3864CFFE69058FFE69058FFE69058FFE69058FFE69058FFE69058FFE690
          58FFE69058FFE69058FFE69058FFE69058FFE58E57FFC98356DCCA855ADCE68F
          59FFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE790
          5BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE58A51FFE48548FFE487
          4DFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE7905BFFE790
          5BFFE7905BFFE7905BFFE7905BFFE7905BFFE68F59FFCA855ADCCC875BDDE791
          59FFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE893
          5CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE893
          5CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE8935CFFE893
          5CFFE8935CFFE8935CFFE8935CFFE8935CFFE79159FFCC875CDDC4845BD4E892
          5AFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE8945CFFE894
          5CFFE8945CFFE8945CFFE8945CFFE8945CFFE8925AFFC4835AD498694CA4EA92
          5BFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB955FFFEB95
          5FFFEB955FFFEB955FFFEB955FFFEB955FFFEA925CFF98694BA435261C39EB97
          60FFEC955FFFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC9560FFEC95
          60FFEC9560FFEC9560FFEC9560FFEC955FFFEB9760FF35261C3900000000A475
          57B0EC935CFFED975FFFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED9861FFED98
          61FFED9861FFED9861FFED975FFFEC945CFFA47557B000000000000000000000
          0000A57658B0ED9963FFEE965EFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE975FFFEE97
          5FFFEE975FFFEE965EFFED9964FFA57658B00000000000000000000000000000
          00000000000034261D38986B4FA2C68962D3CF8E65DDCE8D66DCCE8D66DCCE8D
          66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D
          66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCE8D66DCCF8E
          65DDC68962D3986B4FA234261D38000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000050000000C000000190000002A0000003F2727279E151515950000
          003D00000029000000180000000B000000040000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000050000
          000C000000190000002A0000003D16161687828282E7F0F0F0F9EEEEEEF97676
          76E31010107E0000003B00000028000000170000000B00000004000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000040000000C000000190000
          002A0000003E1111117F7A7A7AE3E5E5E5F6FFFFFFFFF8F8F8FCFCFCFCFFF7F7
          F5FFD4D3D2F4696968DE0D0D0D760000003900000026000000160000000A0000
          0004000000010000000000000000000000000000000000000000000000000000
          00000000000000000001000000030000000A00000016000000280000003D1011
          127C86A0B9F3D1D6DAF5FFFFFFFFFFFFFFFFFFFFFFFFF8F8F8FCFAFAF9FFF4F3
          F2FFF1F0EFFFEEECEBFFC5C3C3F25F5E5DDA0A0A0A6E00000038000000250000
          00150000000A0000000300000001000000000000000000000000000000000000
          000000000002000000090000001500000025000000390C0C0C71758DA5EE639A
          D2FE0E6DCAFFE7EEF3FFFAF9F9FFFBFBFBFFFFFFFFFFF8F8F8FCF8F8F7FFF0EF
          EDFFEDEBEAFFEAE8E7FFE7E5E3FFE4E2E0FFB9B6B5F1545453D5080808670000
          0037000000230000001400000008000000020000000000000000000000000000
          0001000000090000001E0000003608080867595959D58699ABFB1F558AFF005D
          B4FF0060BCFFE5EAEFFFF7F6F5FFF8F8F7FFFAFAF9FFF5F5F5FBF7F5F4FFEDEA
          E9FFEAE7E6FFE7E4E2FFE4E1DFFFE1DEDCFFDFDBD8FFDCD8D5FFAAA8A6F04A49
          48CF06060661000000340000001C000000090000000100000000000000000000
          000200000013060606514F4E4ECFB8B6B5F0E9E7E5FF8B9CACFF174D80FF0058
          ABFF005CB2FFE0E5EBFFF3F2F1FFF5F4F3FFF7F6F5FFF4F4F3FBF4F3F2FFE9E7
          E4FFE6E4E1FFE3E1DEFFE0DEDAFFDEDBD7FFDBD7D4FFD8D4D0FFD5D1CDFFD2CE
          CAFF9D9B97EF403F3EC70505054A000000120000000200000000000000000000
          0003000000176F6E6DE3E2DFDDFFE4E1DFFFE5E3E1FF8899A8FF184D7DFF005A
          ADFF005AABFFDDE2E5FFF0EEECFFF1F0EEFFF3F2F0FFF3F2F1FBF2F1EFFFE5E3
          E1FFE2E0DDFFDFDDDAFFDCDAD7FFDAD6D3FFD7D3D0FFD4D0CDFFD1CDC9FFCECA
          C6FFCBC7C2FFC8C4BFFF595755DC000000170000000300000000000000000000
          000300000018848382E4DEDBD8FFDFDDDAFFE1DFDCFF8696A3FF154976FF0051
          98FF00539BFFDADEE1FFEBEAE8FFEDECEAFFEFEEECFFEDECECFBBAB9B8FFD2D0
          CEFFDEDCD9FFDBD9D6FFD9D5D2FFD6D2CFFFD3CFCCFFD0CCC8FFCDC9C5FFCAC6
          C2FFC7C3BEFFC4C0BBFF686665DF000000170000000300000000000000000000
          000300000018838282E4DBD7D4FFDCD9D6FFE0DDDAFF85939EFF16466DFF004E
          8EFF115A94FFDDDEDFFFE8E6E4FFEAE8E6FFECEAE8FFEBEAE9FBA6A5A4FF9B99
          97FFADAAA8FFCDC8C5FFD6D1CEFFD3CECBFFD0CBC7FFCDC8C4FFCAC5C1FFC7C2
          BDFFC4BFBAFFC2BBB6FF666565DF000000170000000300000000000000000000
          000300000018817F81E4E1DEDBFFE7E4E2FFE6E4E2FF85929DFF264F6FFF88A5
          BCFFDEDCDBFFE3E0DEFFE4E2E0FFE6E4E2FFE8E5E4FFE7E6E5FBA4A3A2FF9996
          94FF928E96FF959390FFA8A5A1FFC5C1BDFFCCC7C3FFC9C4BFFFC6C1BCFFC4BE
          B9FFC1BBB5FFBEB7B2FF656265DF000000170000000300000000000000000000
          0003000000187E7C80E4E4E2DFFFE7E4E2FFE6E4E2FFB9C0C4FFE3E1DFFFE5E2
          E0FFE0DDDAFFDFDCD9FFE1DEDBFFE2E0DDFFE4E2DFFFC9C8C6FBA3A1A0FF8B88
          99FF3221E2FF4B3DCBFF807B98FF8F8B88FFA29E9AFFBDB9B4FFC3BDB8FFC0B9
          B4FFBDB6B1FFBAB3ADFF625F65DF000000170000000300000000000000000000
          0003000000187B7A7FE4E4E1E0FFE7E4E3FFE6E4E2FFE6E3E2FFE5E3E1FFE5E3
          E1FFE4E2E0FFDCD9D6FFDDD9D7FFDEDBD9FFD5D2D0FFA3A1A1FCA1A09EFF5347
          CDFFB8B3F7FFA29AF6FF3C2AEDFF4D3FC5FF7D7890FF898581FF9D9794FFB6AF
          ACFFB9B2ADFFB5AEA8FF615F66DF000000170000000300000000000000000000
          00030000001879777FE4E5E3E0FFE8E6E4FFE7E5E3FFE7E4E2FFE6E3E1FFE4E2
          DFFFD8D4D2FFE0DCD9FFD9D6D2FFDAD6D3FFA7A4A2FF9E9D9CFC6F67B5FF9189
          EFFF2B1AE3FF5244EAFFBBB5F8FF9A91F7FF3826ECFF4E41BDFF7B7486FF837E
          7AFF97918CFFB1AAA4FF615E68DF0000001A0000000400000000000000000000
          00030000001877757EE4E6E4E2FFEAE7E6FFE8E6E4FFE6E2E0FFCCC7C5FFB1AA
          A9FF837474FFDBD7D5FFDAD6D3FFB5B2AFFF989593FF8B8999F7736BE1FE483C
          E0FF2313DEFF2413E1FF2412E4FF5B4CEEFFBEB8FAFF7F73F4FF311EE9FF4F42
          B6FF78727FFF807B77FF585662E2000000270000000F00000004000000000000
          00030000001874727EE4E8E6E4FFECEAE8FFEAE8E7FFACA9A7FF8B8787FF9288
          88FFA9A1A0FFDAD7D5FFCDCAC7FF94928FFF949191FF403AAAEA726AE4FF2215
          D7FF2314DAFF2313DEFF2413E1FF2412E4FF2512E8FF5747F1FF8A7FF5FF5545
          F1FF2C19E6FF5245B2FF504E67EB000000430000002800000010000000000000
          00040000001872707CE4EAE8E6FFEEECEBFFECEBE9FFABA9A6FF736D6DFFA59D
          9DFF392C2DFFDAD7D4FFA6A4A2FF918E8BFF474284E38883E4FF2216D0FF2215
          D3FF2215D7FF2314DAFF2314DEFF2413E1FF2412E4FF2512E8FF2612EDFF4534
          F0FF4F3EF0FF3D2BEFFF2917E1FB201894EB0E0E288700000120000000060000
          000C0000001E666471E7E6E4E3FFF1EFEEFFEFEDECFFABA9A6FF544E4EFF8E88
          87FFE5E2E1FFBDBBB9FFA09F9DFF6F6D80F0746FD5FD3229CCFF2116CCFF2116
          CFFF2215D2FF5050BBFF3D3DB0FF2217CCFF2413E0FF2412E3FF2512E7FF2511
          EDFF2612EDFF3927EFFF4F3FF0FF3B29EFFF2412D6F70A0B1C60000000130000
          0022000000305C5968EAA8A7A7FFBBBAB9FFD7D6D5FFD2CFCFFFE5E3E2FFECEA
          E9FFD5D4D2FFA3A2A1FF918E8DFE4945A8ED5651D1FF2018C5FF2118C8FF2117
          CCFF2F2ABEFFBDBEE6FFECE9E8FF5B5CB1FF2215D7FF2414E0FF2413E3FF2513
          E7FF2511EDFF2511EDFF2612EDFF3B2AEFFF4536DCF5090A1B53060713470F11
          31930001014A5A5868EBAAA9A9FFAEADACFFABABABFFAEADADFFC7C6C5FFDEDC
          DBFFABAAA9FF9B9997FF3D3A7BE07572D5FF1F1ABEFF2E27C6FF2D27C8FF2B25
          C5FF6E6FB9FFE8E6F6FFF9F9F8FF8A89BFFF2216D1FF2314DDFF2414E0FF2413
          E3FF2513E7FF2511EDFF2511EDFF2511EDFF1B177ACD0000000A0D0F2E741B22
          8EFA1B1F7DF2535270F29C9A98FFB0B0B0FFAEAEAEFFADACABFFABAAA9FFA9A9
          A8FFA4A4A2FF646276ED7472CDFE5856C9FF726FD4FF6D6AD5FF6763D5FF5453
          ACFFD6D2D0FFE5E3F0FFFFFFFFFF8F8EBDFF2217CBFF2215D9FF2314DCFF2313
          DFFF2413E3FF2412E6FF2512EAFF2014B0E803040C270000000303040C222629
          6BC9323691ED2C31A1FD282979E368657BF1979493FFA9A8A7FFAEAEADFFACAB
          ABFF888484FC5050A8F16261C8FF7F7DD4FF7876D3FF726FD4FF6B68D4FF5554
          9EFFDFDCDAFFE2E0EAFFFFFFFFFF9A9BC1FF2218C6FF2215D5FF2215D8FF2314
          DBFF2313DFFF2413E2FF2111CEF20C0D326E0000000500000000000000010000
          0003050614321F2265BE44469CEB4D4EB5FE282891EA42406EE1868283FC9492
          8FFF3A3A76DF6B6BC6FF8686D2FF8483D4FF7E7CD3FF7775D3FF716ED3FF5958
          95FFF0EEEDFFCECCD5FFFFFFFFFFA5A4BDFF2D25C4FF2216D1FF2215D5FF2215
          D8FF2314DBFF2212DCFE171573C2000000090000000100000000000000000000
          000000000001000000030405122B1A1D60B25758A9EB7373C9FD4D4CBAFB2B29
          74DC6E70C2FE7F80CCFF8F8FD4FF8989D4FF8282D3FF7C7BD2FF7674D3FF5A58
          8AFF555368FF05050AFF11101FFF5D5D7AFF4E47C9FF2C23D0FF2216D1FF2215
          D4FF2215D8FF1D16A2E502030A20000000020000000000000000000000000000
          00000000000000000000000000010000000303040F24121557A55A5AADEA8A8A
          D3FD6063BCFF9B9DD6FF9596D5FF8D8ED3FF8888D3FF8181D2FF7C7AD2FF6565
          AFFF383854FF161621FF2C2B45FF4B479BFF5650D4FF4B44D4FF2319CDFF2116
          D0FF1E15BBEF090A2C5D00000005000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000000302030C1D0F12
          53981A1B90E64345B5FD7F81CAFF9495D5FF8D8ED3FF8686D2FF807FD2FF7B79
          D2FF7472D1FF59579BFF6764CEFF625ED2FF5B56D3FF544ED3FF3229CDFF2016
          C9FC13156BB50000000800000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002020209170D104D8C181993E53D3DB2FC7576C8FF8B8CD2FF8686D1FF7F7F
          D1FF7877D0FF7371D0FF6D6AD1FF6764D1FF605CD2FF5954D2FF3C35CCFF1917
          99E2010207180000000200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000002010107130C0E467F181994E53636AFF96B6CC6FF8585
          D1FF7E7ED1FF7776D0FF7270D0FF6B68D0FF6562D0FF5C57D0FF231EADEE0608
          254E000000040000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000020101050E0B0E3F74191993E33030
          ACF76262C4FF7C7BCFFF7776D0FF706ECFFF6A68CFFF3834BDFA10125FA60000
          0007000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020101030A0A0C
          386A191992E22C2CA9F45959C3FF7473CFFF5654C7FF17188ADD010104130000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000200000208090B336118198EDF2020A3F122229DEC05061D3E000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000200000105080A2D540A0D3C6B00000003000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000000E000000270000
          002F0000002F0000002F0000002F0000002F0000002F0000002F0000002F0000
          002F0000002F0000002F0000002F0000002F0000002F0000002F0000002F0000
          002C000000150000000400000000000000000000000000000000000000000000
          00000000000000000000000000000000000003070929142A377F152D398E142D
          3991142D3991142D3991142D3991142D3991142D3B9110303D910D3341910A37
          459107394891043D4A9101404E9102404D91053D4C9109394991061B23770000
          005D000000500000003000000001000000000000000000000000000000000000
          000000000000000000000001010127546D964790B9F94F97BDFB4F97BDFB4F97
          BDFB4F97BDFB4F97BDFB4F96BDFB4F96BDFB4C98BFFB449FC8FB3CA7D0FB34B0
          D8FB2CB9E0FB24C1E8FB1CCAF1FB1EC5EEFB25B7DEFB2FB3DCFB37B4E2FD2982
          AADD0A1A237F0000004A0000000F000000000000000000000000000000000000
          00000000000000000000132935494E97BEFA3FBBE2FF4AB3D8FF44ACD4FF36AC
          D5FF2EAFDAFF29B4DEFF26B9E3FF1AA3C8FF0F8EB0FF0B93B3FF0698B9FF0E9A
          BFFF20C0EDFF26BAE8FF2DB7E2FF32B1DFFF3195BCFF276580FF336D85FF3F98
          BEFD257092CD000000580000001E000000000000000000000000000000000000
          00000000000000000000316989B945ADD3FD59A3C8FF549FC5FF3B97C3FF349E
          CAFF2DA5D2FF26ACD8FF1FB3DFFF17B0DAFF0EAED7FF06B5DEFF00BCE5FF07B8
          E4FF13C2F1FF1BBCEDFF24B6E6FF2BB0E1FF32A9DCFF318EBBFF406F88FF397C
          97FF37AFDDFD030B0F630000001E000000000000000000000000000000000000
          000000000000000000003B7EA2DA4CABD1FF5AA2C8FF4999C3FF3B98C2FF359F
          CAFF2DA5D1FF27ABD8FF20B3DFFF17AFDAFF0EADD6FF08B3DDFF01BBE4FF05B9
          E4FF12C4F2FF19BDEEFF20B8E9FF28B3E3FF2FACDEFF33A7DAFF427C98FF417D
          96FF31BBE9FF051C24720000001E000000000000000000000000000000000000
          000000000000000000003E80A4DB53A0C0FF65A4C5FF4A94BBFF3A90B9FF3496
          C1FF2C9DC8FF27A9D3FF21B1DDFF19AED9FF10ABD4FF09B3DBFF03B9E2FF04BB
          E6FF0FC5F4FF16C0EFFF1CBBEBFF24B6E5FF29B1E2FF2EADDFFF4088A6FF4C8D
          A7FF27C2F0FF041E25720000001E000000000000000000000000000000000000
          000000000000000000004384A7DC6194ABFF79A7C2FF4E8BB0FF3A81A9FF3388
          B0FF2C8FB7FF28A3CCFF23B0DCFF1AABD7FF12AAD2FF0CB0D8FF05B6E0FF00BE
          E8FF0CC9F7FF13C3F2FF18BEEEFF1EBAEAFF24B6E6FF27B2E4FF4296B5FF549D
          B6FF1DCBF6FF021F27720000001E000000000000000000000000000000000000
          000000000000000000004987AADD7AA7BDFF9BBFD3FF679DBCFF4284ACFF3789
          B1FF2F90B7FF2AA1CDFF25AEDAFF1DABD4FF15A7CFFF0EADD6FF08B3DCFF03BB
          E5FF08CDFAFF0DC7F6FF13C3F2FF18BEEEFF1FB8E6FF25B4E1FF45A4C2FF58A9
          C4FF13D3FEFF002028720000001E000000000000000000000000000000000000
          000000000000000000004D8AACDE9DCCE1FFB0D0E2FF85B4D0FF70A8CAFF59A0
          C4FF3A9BC7FF2EA4D1FF27ACD7FF1FA7D2FF17A3CCFF12AAD2FF0CAFD8FF06B7
          E2FF06CEFBFF09CCF9FF0DC7F6FF13C2F1FF1FBEE9FF28C2EDFF4BB1CFFF5FB4
          CFFF1BCBF5FF011F27720000001E000000000000000000000000000000000000
          00000000000000000000538EAEDFACD2E4FFBED8E7FF89B8D2FF72AACAFF6FA9
          C9FF60ABCFFF4BADD3FF36ACD6FF23A4CFFF1BA0C8FF16A6CFFF10ABD4FF0BB3
          DCFF0AC8F6FF07CDFAFF07CCFAFF0DC6F2FF1CC8F3FF22C9F3FF4FC2E0FF65BE
          D6FF23C1ECFF031E25720000001E000000000000000000000000000000000000
          000000000000000000005891B1E0BBD9E7FFCCE0ECFF86B2CCFF5F97B8FF5F97
          B8FF5B9BBBFF56A0C1FF51A4C5FF46A6C7FF33A4CAFF21A3CBFF15A7CFFF0FAB
          D5FF0BB1D9FF07B4DEFF03B9E1FF00BCE5FF02BAE4FF09BDE6FF4FCFECFF6BC9
          E0FF2CB8E2FF041C23720000001E000000000000000000000000000000000000
          000000000000000000005B93B1E0C7DEEAFFD9E8F1FF92BDD7FF689FC0FF5F97
          B8FF5E98B8FF599CBDFF54A2C1FF50A6C7FF4BAACBFF48AFD0FF41B1D4FF36B3
          D5FF28B3D8FF1CB4DAFF0EB4DBFF06B5DFFF07BBE4FF16CDF6FF54DAF7FF71D1
          E9FF36AFD9FF061B22720000001E000000000000000000000000000000000000
          000000000000000000005B93B1E0CDE2ECFFE6F0F6FF97C0D8FF73AACBFF6FA6
          C7FF6CA6C7FF68A9C9FF63ACCEFF5BAECFFF4FA6C8FF4BAACBFF48AFD0FF45B8
          DAFF45C2E6FF41C5E9FF3EC7EBFF3AC6EAFF3ACAEEFF33CDF3FF65D7F3FF7BD0
          E7FF3FA6CFFF08191F710000001D000000000000000000000000000000000000
          000000000000000000005B93B1E0D3E5EFFFF1F6FAFFB5D2E4FF74ABCCFF74AB
          CCFF72AACBFF71AACBFF6CAECEFF66B1D2FF5BAFCFFF52ABCCFF4FB1D2FF52BF
          E1FF4DC2E5FF49C3E8FF46C6EAFF45C5E8FF4CC9EBFF4DCCEEFF9BE1F4FF7AC3
          DCFF489CC5FF09171E650000000E000000000000000000000000000000000000
          00000000000000000000366782ADF0F7F9FFECF3F8FFF1F6FAFF7FB2D0FF74AB
          CCFF74ABCCFF74ABCCFF72AACBFF6CAFCEFF68B2D2FF62B5D6FF5DB8DAFF59BB
          DDFF54BDE0FF52C0E4FF4EC2E4FF4CC0E3FF52C4E5FF5AC9E9FFB8E5F2FF59A2
          BEFF478EB7F90306082C00000002000000000000000000000000000000000000
          000000000000000000000E1F2938ABCCDFFDFCFDFEFFDAE9F0FFD3E4EEFFBDD7
          E7FFB5D3E4FFB5D3E4FFB5D3E4FFB5D3E4FFAAD2E4FF9FD2E7FF95D2E8FF8DD1
          E9FF82CFE9FF79CDE8FF71CBE9FF6FBFD8FF90CADEFFB5DCE8FFAEC0C9FF61A0
          C0FD28556FAA0000000000000000000000000000000000000000000000000000
          00000000000000000000000000001B394B67669AB6ECA3C8DDFFA6CADEFFA6CA
          DEFFA6CADEFFA6CADEFFA6CADEFFA6CADEFFA6CADEFFA6CADEFFA0C6DCFF9AC2
          DAFF93BED7FF8CBAD5FF86B6D2FF7FB2D0FF78AECEFF72AACBFF64A0C3FF3460
          78D1060E123E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001E1E1E58B9C2C6FAD2DCE1FFAAB4
          B9FC242F33A80812174708121720081217200812172008121720081217200812
          172008121720081217200812172025303477BDC7CCFAD1DBE0FFAEB0B2FB1C1C
          1C910000002A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001E1E1E58D6D6D6FBE9E9E9FFB9B9
          B9FB2121219B0000002D00000000000000000000000000000000000000000000
          000000000000000000000000000022222263E4E4E4FCE6E6E6FFB2B2B2FB1C1C
          1C920000002A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001E1E1E58D6D6D6FBE9E9E9FFB9B9
          B9FB2020209B0000002F00000000000000000000000000000000000000000000
          000000000000000000000000000021212162E4E4E4FCE6E6E6FFB2B2B2FB1D1D
          1D92000000280000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001E1E1E58D6D6D6FBE9E9E9FFB9B9
          B9FB2020209E0000003A00000002000000000000000000000000000000000000
          000000000000000000000000000021212160D9D9D9FCE6E6E6FFB5B5B5FB1D1D
          1D900000001B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001515153FAFAFAFFBE6E6E6FFBCBC
          BCFA292929AE000000440000000B000000000000000000000000000000000000
          000000000000000000000000000029292979AAAAAAFAE4E4E4FFA7A7A7FD1515
          1578000000100000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000404040C8C8C8CFEE2E2E2FFB7B7
          B7FF3D3D3DD0000000590000002A000000010000000000000000000000000000
          0000000000000000000000000000404040B7ACACACFFD3D3D3FF7B7B7BFE0404
          0449000000070000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000686868DECECECEFFC2C2
          C2FF5D5D5DF7070707730000004C0000001B0000000100000000000000000000
          000000000000000000000909091C5D5D5DF7C4C4C4FFBDBDBDFF555555E90000
          0025000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000003838389C999999FBE3E3
          E3FF8C8C8CFB3C3C3CCD000000630000004C0000002B0000000D000000040000
          0000000000040000000E414141C88B8B8BFCE4E4E4FF8F8F8FFC363636B20000
          0006000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000080808186E6E6EF4DADA
          DAFFD6D6D6FF6D6D6DFC3B3B3BCC0606067100000059000000470000003B0000
          00340808084F404040CD6A6A6AFCD7D7D7FFCFCFCFFF7F7F7FF7080808280000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001F1F1F5AAAAA
          AAFDEEEEEEFFD5D5D5FF818181FB565656F5383838CA262626A9272727AB3A3A
          3ACB565656F8818181FCD5D5D5FFE7E7E7FFBCBCBCFE1D1D1D60000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003939
          39A3A8A8A8FDECECECFFE7E7E7FFB7B7B7FF979797FE777777FA757575FA9595
          95FFB7B7B7FFE6E6E6FFE4E4E4FFBBBBBBFD3535359D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001F1F1F5C7E7E7EF6D6D6D6FEE2E2E2FFD4D4D4FFE4E4E4FFE3E3E3FFCFCF
          CFFFDADADAFFD9D9D9FE888888F61E1E1E570000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000909091A383838A1656565E6858585FE5E5E5EFC5B5B5BFC7A7A
          7AFE6E6E6EE63535359E08080817000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000505050E1515153F1515153D0404
          040C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000070000
          001C0000002B0000002B0000002B0000002B0000002B0000002B0000002B0000
          002B0000002B0000002B0000002B0000002B0000002B0000002B0000002B0000
          00200000000B0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000050B0F1A1B3B4C8C1F43
          58A71F4357AA1F4357AA1F4357AA1F4357AA1F4357AA1B455BAA154B5FAA114F
          65AA0B556BAA075B6FAA015F75AA035F75AA095971AA0E5168A706171E740000
          005B000000430000000D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C3F52704B93BAF848A0C6F84AA6
          CCFB4AA6CCFB48A4CCFB49A4CBFB4AA3CBFB4AA3CAFB47A6CCFB40ACD3FB3CB2
          D9FB36B7DEFB31BDE3FB2BC3E9FB2DBAE0FB2D99BAFC349CBEFB39ABD7FA2466
          86C60000005D0000002400000000000000000000000000000000000000000000
          000000000000000000000000000001030405468DB4F042B7DEFF53AAD1FF479C
          C4FF359ECAFF2DA5D2FF24ADDAFF1EB4E2FF12ACD4FF0AB2DAFF03B9E2FF05B8
          E2FF13C2F0FF1BBCEDFF24B6E5FF2DAEE0FF33A0D1FF33677FFF35687DFF3BAA
          D9FB06141B750000003100000000000000000000000000000000000000000000
          00000000000000000000000000000F212B3B4C97BEF753A8CCFF549FC6FF3C97
          C1FF359ECAFF2DA5D1FF25ACD9FF1EB4E1FF12ABD4FF0BB1DAFF04B8E1FF04B9
          E2FF12C3F1FF1ABDEEFF23B7E7FF2AB1E2FF32A9DCFF2C7FA8FF527E94FF379F
          C3FA0F4A5FA40000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F404F98BDF769ABCDFF57A1C7FF3D96
          C2FF369DC9FF2EA5D0FF26ACD8FF1FB3E0FF14AAD2FF0DAFD8FF05B6E0FF02BB
          E4FF0FC5F4FF17C0EFFF1EBAEAFF26B4E4FF2DAEE0FF2A94BFFF5E93A9FF2F9E
          C2FB0C5267A70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F405693B3F97CA9C4FF5E96B7FF3B80
          A8FF3388B0FF2B8FB7FF27A7D2FF21B2DDFF16A8D1FF0EADD6FF07B4DDFF00BC
          E5FF0AC8F6FF13C3F2FF1ABDEEFF21B9E9FF26B4E4FF259DCAFF69A5BCFF2AA7
          CBFB07566BA70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F40689FBBFAA0C1D4FF78A6C2FF3E81
          A9FF3585AEFF2E8DB5FF2AA5D0FF24B0DDFF18A5CEFF11AAD3FF0BB1DAFF04B8
          E1FF06CBF9FF0EC7F6FF13C2F1FF1ABDEEFF1EBAE9FF1FA7D3FF72B6CCFF25B0
          D5FA015A6FA70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F4081B6D0FAB4D4E5FF95BDD5FF6AA2
          C3FF5399BDFF379DC8FF2CA5D2FF25ACD9FF1CA2CAFF15A6CFFF0EADD6FF08B3
          DDFF06CCF8FF08CDF9FF0DC7F6FF13C3F2FF17C0EEFF18B1DEFF7BC5DBFF2AAF
          D5FA04586BA70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F4094BFD7FBC5DCEAFFA1C5DBFF71A9
          C9FF6EA7C8FF5CA9CCFF46ABD1FF30A9D4FF209EC7FF19A2CBFF13A9D1FF0DAE
          D8FF0BC4F0FF06CAF6FF06CCF8FF0BC8F6FF0EC6F5FF11BDE9FF84D4E9FF30AF
          D4FA085367A70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F40A6CBDDFCD4E5EFFFA9CADEFF72AA
          CAFF70A7C8FF5D9DBDFF56A0C1FF50A5C5FF43A7C9FF30A5CAFF21A6CEFF12AA
          D2FF0DAED8FF08B3DCFF05B8E3FF05CBF7FF06CDFAFF08C8F3FF8DE0F4FF37AD
          D3FA0C4E62A70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F40B7D3E2FCE3EDF4FFB2D0E2FF73AA
          CBFF71AACAFF6BA7C9FF5BA0C1FF55A3C3FF50A6C7FF4AAACCFF46AFD0FF40B3
          D5FF36B7D9FF28B7DDFF1FC4EDFF13C6F3FF0AC8F6FF0BC9F6FF9AE8FBFF40AA
          D1FA12495DA70000003100000000000000000000000000000000000000000000
          000000000000000000000000000011242F40B8D4E4FCF1F6F9FFBDD7E6FF73AA
          CBFF72AACBFF70A9CAFF6BABCDFF64B0D2FF5AAECFFF4FA6C8FF4AAACCFF49B6
          D8FF49C3E7FF45C5EAFF41C9EDFF3FC9EFFF3ACAF0FF31CAEFFFAEEAF9FF49A4
          CAFA174357A50000002A00000000000000000000000000000000000000000000
          00000000000000000000000000000E1F2837BBD6E5FCE4EEF4FFF1F6FAFF76AD
          CDFF74ABCCFF72AACBFF71AACBFF6CACCDFF66B0D2FF5CB0D2FF58B3D5FF55BC
          DEFF51BEE2FF4DC1E4FF49C3E7FF46C5E8FF44C5E9FF51CAEBFFAAE3F4FF4F9A
          BFF9193A4C940000001000000000000000000000000000000000000000000000
          00000000000000000000000000000001010179ABC6EFE8F2F6FFD7E6F0FFC6DC
          E9FF93BDD7FF85B5D3FF85B5D3FF81B4D2FF7BB6D3FF73B9D7FF6DBBD9FF66BE
          DEFF5FBEE0FF5AC0E1FF54C1E4FF50C0E4FF51C2E4FF6DCBE8FF57BADBFF4B92
          BAFA050C0F260000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000001932415886B4CDF6D0E0EAFFDDEA
          F2FFDBE8F0FFD4E6EEFED3E7F0FDD2E6EFFCD2E6EFFCC9E1EDFCBCDBE8FBB0D4
          E4FAA6CEE1FA99C6DCF98EC0D7F883B9D3F878B1CDF76BA8C8F65598BDF82148
          5E80000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000406070D7A909DFFACC6
          D5FF90AAB9FB284251B717313F72173140581731405817314058173140581731
          40581731405817314058173140581731405817314058152E3C57030709230000
          0008000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003030308A7A7A7FFECEC
          ECFFBABABAF91B1B1B9100000028000000000000000000000000000000000000
          0000000000000000000000000000000000000909091A0A0A0A41000000520000
          002C000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003030308A9A9A9FFEDED
          EDFFBABABAF91B1B1B9100000028000000000000000000000000000000000000
          00000000000000000000000000000C0C0C249E9E9EFA949494FC1212127F0000
          0043000000090000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003030308A9A9A9FFEDED
          EDFFBABABAF91B1B1B9100000028000000000000000000000000000000000000
          000000000000000000000000000033333392EAEAEAFFDBDBDBFE4B4B4BC60000
          00400000000A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003030308A9A9A9FFEDED
          EDFFBABABAF91B1B1B9100000028000000000000000000000000000000000000
          00000000000000000000000000002E2E2E82BBBBBBF9DDDDDDFE5B5B5BDB0000
          00460000000B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003030308A9A9A9FFEDED
          EDFFA4A4A4F91B1B1B920000002B000000000000000000000000000000000000
          000000000000000000000000000008080817A9A9A9F6D6D6D6FF626262E40000
          0045000000090000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020206A6A6A6FFE9E9
          E9FF878787F81C1C1C9700000038000000010000000000000000000000000000
          000000000000000000000000000034343494D9D9D9FFC6C6C6FF5E5E5EE70000
          003A000000020000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000006A6A6AE6CBCB
          CBFF9D9D9DFC292929AF000000480000000D0000000000000000000000000000
          00000000000000000000000000004A4A4AC7BEBEBEFFB3B3B3FF3D3D3DCB0000
          002B000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000414141AEB6B6
          B6FFB6B6B6FF464646D70000005F000000320000000400000000000000000000
          00000000000000000000020202075F5F5FF6D4D4D4FF969696FB2525259D0000
          001A000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002626266F9494
          94F9DADADAFF6D6D6DFA1717178E000000560000002F0000000B000000000000
          0000000000000000000130303096878787FCE1E1E1FF737373FB1313135D0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000060606116161
          61EFD3D3D3FFBDBDBDFF585858F31515158C0000005D00000042000000310000
          0022000000272525258C646464FCD4D4D4FFBEBEBEFE4A4A4AD60000000D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001919
          194AB2B2B2FCEBEBEBFFBABABAFF666666FA3E3E3ED1272727AA1D1D1D952A2A
          2AAD484848E3727272F9CACACAFFE3E3E3FF9E9E9EFB0B0B0B2F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000035353595B1B1B1FBE8E8E8FFDBDBDBFFA8A8A8FF868686FA727272F98F8F
          8FFDB1B1B1FFE4E4E4FFE4E4E4FFA0A0A0FC1F1F1F6000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001C1C1C517D7D7DF3D4D4D4FDD8D8D8FFCBCBCBFFDADADAFFC4C4
          C4FFD6D6D6FFCACACAFC6E6E6EE01010102F0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000060606132A2A2A7C5A5A5AC6575757F0575757FF5656
          56E74F4F4FB52323236601010104000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000303030A0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000030000000900000011000000150000001600000016000000160000
          0016000000150000001100000009000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000060000
          00100000001B0000002900000038000000410000004300000043000000430000
          00430000004100000038000000290000001B0000001000000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000030000000D000000200000
          00350006034C003D209900713DDE008C4AFF008B4AFF008B4AFF008B4AFF008B
          4AFF008C4AFF00713DDE003D20990006034C00000035000000200000000D0000
          0003000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000005000000160000002E0011095A0073
          3EDF008D4DFF009A5EFF00A269FF00AA74FF00AC77FF00AC77FF00AC77FF00AC
          77FF00AA74FF00A269FF009A5EFF008D4DFF00733EDF0011095A0000002E0000
          0016000000050000000000000000000000000000000000000000000000000000
          0000000000000000000000000005000000190000003800522CB4008D4DFF00A0
          67FF01B07CFF20BD90FF3ECCA5FF57D8B6FF5DDBBAFF5DDBB9FF5DDBB9FF5DDB
          B9FF57D8B6FF3ECCA5FF20BD90FF01B07CFF00A067FF008D4DFF00522CB40000
          0038000000190000000500000000000000000000000000000000000000000000
          00000000000000000005000000190000003A007D43ED009558FF00AA74FF2BC4
          99FF57D8B6FF3EDAB0FF1ED8A4FF05D79CFF00D797FF00D797FF00D797FF00D7
          97FF05D79CFF1ED8A4FF3EDAAFFF57D8B6FF2BC498FF00AA74FF009558FF007D
          43ED0000003A0000001900000005000000000000000000000000000000000000
          0000000000030000001600000038007C41EB009E64FF07B27FFF4BD3AEFF44DA
          B1FF05D79CFF00D698FF00D598FF00D598FF00D599FF00D599FF00D599FF00D5
          99FF00D598FF00D598FF00D698FF05D79CFF44DAB0FF4BD3AEFF07B27FFF009E
          64FF007C41EB0000003800000016000000030000000000000000000000000000
          00010000000D0000002E007D43EC009E64FF0CB583FF57D8B7FF24D7A7FF00D5
          97FF00D498FF00D499FF00D499FF00D499FF00D498FF00D397FF00D397FF00D4
          98FF00D499FF00D499FF00D499FF00D498FF00D598FF24D7A6FF57D8B6FF0CB5
          83FF009E64FF007D43EC0000002E0000000D0000000100000000000000000000
          00060000001F00512BB0009558FF07B17FFF57D8B7FF1ED6A4FF00D397FF00D3
          99FF00D399FF00D399FF00D399FF00D297FF00D093FF00CF90FF00CF90FF00D0
          93FF00D297FF00D399FF00D399FF00D399FF00D399FF00D398FF1ED6A4FF57D8
          B6FF07B17FFF009558FF00512BB00000001F0000000600000000000000000000
          00100010094C008D4DFF00AA73FF4BD3AFFF24D6A7FF00D397FF00D399FF00D3
          99FF00D399FF00D399FF00D398FF00D093FFB0F1E0FFFFFFFFFFFFFFFFFFB0F1
          E0FF00D093FF00D398FF00D399FF00D399FF00D399FF00D399FF00D397FF24D6
          A6FF4BD3AEFF00AA74FF008D4DFF0010094C0000001000000000000000030000
          001B00723EDD00A067FF2BC599FF44D9B2FF00D297FF00D299FF00D299FF00D2
          99FF00D299FF00D299FF00D197FF00CE90FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00CE90FF00D197FF00D299FF00D299FF00D299FF00D299FF00D299FF00D2
          97FF44D9B0FF2BC598FF00A067FF00723EDD0000001B00000003000000090006
          0332008D4DFF01B07CFF57D8B8FF05D39CFF00D198FF00D199FF00D199FF00D1
          99FF00D199FF00D199FF00D097FF00CC8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00CC8EFF00D097FF00D199FF00D199FF00D199FF00D199FF00D199FF00D1
          98FF05D39CFF57D8B6FF01B07CFF008D4DFF000603320000000900000011003C
          218D009A5EFF20BD91FF3ED8B1FF00D097FF00D099FF00D099FF00D099FF00D0
          99FF00D099FF00D099FF00CF97FF00CB8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00CB8EFF00CF97FF00D099FF00D099FF00D099FF00D099FF00D099FF00D0
          99FF00D098FF3ED8AFFF20BD90FF009A5EFF003C218D00000011000000150071
          3DDA00A268FF3DCCA7FF1ED4A5FF00D098FF00D099FF00D099FF00D098FF00CF
          97FF00CF97FF00CF97FF00CE96FF00CB8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00CB8DFF00CE96FF00CF97FF00CF97FF00CF97FF00D098FF00D099FF00D0
          99FF00D098FF1ED4A4FF3ECDA5FF00A269FF00713DDA0000001500000016008C
          4AFF00AA74FF59DBBBFF05D09BFF00CF98FF00CF99FF00CE97FF00CC93FF00CA
          90FF00CA8EFF00CA8EFF00C98DFF00C686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C686FF00C98DFF00CA8EFF00CA8EFF00CA90FF00CC93FF00CE97FF00CF
          99FF00CF98FF05D19CFF56D9B6FF00AA74FF008C4AFF0000001600000016008B
          4AFF00AB76FF64E2C4FF00CD97FF00CE98FF00CE98FF00CB93FFB0F0E0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0F0E0FF00CB93FF00CE
          98FF00CE99FF00CE97FF5DDCBAFF00AC77FF008B4AFF0000001600000016008B
          49FF03B07DFF67E5C7FF00CC96FF00CD98FF00CC97FF00C890FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C890FF00CC
          97FF00CD99FF00CD97FF5DDCBAFF00AC77FF008B4AFF0000001600000016008A
          48FF10B686FF6BEACBFF00CB96FF00CD98FF00CD97FF00C890FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C890FF00CD
          97FF00CD99FF00CD97FF5DDCBAFF00AC77FF008B4AFF0000001600000015008A
          48FF1FBC8FFF6FEDCFFF00CA95FF00CC98FF00CC98FF00C993FFB0F0E0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0F0E0FF00C993FF00CC
          98FF00CC99FF00CC97FF5DDCBAFF00AC77FF008B4AFF0000001500000011008A
          48FF28BF93FF6FEED0FF05CD9CFF00CA97FF00CB99FF00CA97FF00C893FF00C6
          90FF00C68EFF00C68EFF00C58DFF00C286FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C286FF00C58DFF00C68EFF00C68EFF00C690FF00C893FF00CA97FF00CB
          99FF00CB98FF05CC9CFF56D9B6FF00AA74FF008C4AFF00000011000000090070
          3BD626B484FF64E6C6FF23D6ABFF00C896FF00CA99FF00CA99FF00CA98FF00C9
          97FF00C997FF00C997FF00C896FF00C48DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C48DFF00C896FF00C997FF00C997FF00C997FF00CA98FF00CA99FF00CA
          99FF00CA98FF1ED1A4FF3ECDA5FF00A269FF00713CD60000000900000003003C
          207C1BA772FF5DDEBEFF4DE6C3FF00C795FF00CA98FF00CA99FF00CA99FF00CA
          99FF00CA99FF00CA99FF00C997FF00C48EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C48EFF00C997FF00CA99FF00CA99FF00CA99FF00CA99FF00CA99FF00CA
          99FF00CA97FF3ED6B0FF20BE91FF009A5EFF003C217C00000003000000000006
          031A008C4BFF5DDABAFF77F6DAFF03CB9BFF00C797FF00C999FF00C999FF00C9
          99FF00C999FF00C999FF00C897FF00C38EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C38EFF00C897FF00C999FF00C999FF00C999FF00C999FF00C999FF00C9
          98FF05CB9CFF57D9B7FF01B07CFF008D4DFF0006031A00000000000000000000
          000600703AD63EBD91FF75EDD1FF59ECCAFF00C494FF00C797FF00C899FF00C8
          99FF00C899FF00C899FF00C797FF00C390FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF00C390FF00C797FF00C899FF00C899FF00C899FF00C899FF00C899FF00C7
          97FF44D7B2FF2BC599FF00A167FF00723ED60000000600000000000000000000
          000100100929008C4BFF75E0C5FF7EF9DCFF2DDBB3FF00C494FF00C697FF00C7
          99FF00C799FF00C799FF00C698FF00C493FFB0EEE0FFFFFFFFFFFFFFFFFFB0EE
          E0FF00C493FF00C698FF00C799FF00C799FF00C799FF00C799FF00C697FF24D0
          A7FF4BD3AFFF00AA73FF008D4DFF001009290000000100000000000000000000
          00000000000300502A9C21A56FFF90F0DBFF7CFCE0FF25D7AEFF00C394FF00C6
          97FF00C799FF00C799FF00C799FF00C697FF00C493FF00C290FF00C290FF00C4
          93FF00C697FF00C799FF00C799FF00C799FF00C799FF00C697FF1ECFA5FF57D9
          B7FF07B27FFF009558FF00512C9C000000030000000000000000000000000000
          0000000000000000000500793EE753C59DFF9EF9E7FF7DFCDFFF2CDBB3FF00C1
          93FF00C496FF00C598FF00C699FF00C699FF00C698FF00C597FF00C597FF00C6
          98FF00C699FF00C699FF00C699FF00C598FF00C597FF24CFA7FF57D9B8FF0CB5
          83FF009F64FF007D43E700000005000000000000000000000000000000000000
          000000000000000000000000000500773AE55EC9A4FFB5FFF4FF85FDE3FF58EC
          CBFF02C69BFF00C194FF00C396FF00C397FF00C498FF00C498FF00C498FF00C4
          98FF00C498FF00C497FF00C397FF04C79CFF45D6B3FF4BD4B0FF07B27EFF009E
          64FF007C42E50000000500000000000000000000000000000000000000000000
          00000000000000000000000000000000000500793CE632AD7AFFC0FFF7FF9CFF
          EAFF7EFDE0FF50E8C5FF25D6AEFF03C79BFF00BF93FF00C094FF00C094FF00C1
          94FF05C69BFF22D0AAFF45DCB8FF5FE0C0FF2DC69DFF00AA73FF009558FF007D
          43E6000000050000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000300502999008C4BFF72D5
          B4FFADFDEFFF91F8E1FF81F8DCFF7BF9DCFF79F8DBFF76F6D8FF73F3D5FF73F1
          D4FF6CECCEFF56DEBBFF34CAA2FF0CB684FF00A066FF008D4CFF00522C990000
          0003000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100100923006F
          37D3008C4BFF3DB688FF5CCDA9FF75E1C6FF6FDFC5FF60D9B9FF50D2B0FF41CB
          A7FF30C398FF17AE7BFF089F65FF008C4CFF00723DD300110923000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000006030E003B1F72006E39D1008744FF008744FF008744FF008845FF0089
          46FF008A47FF00703BD1003C21720006030E0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00040000000E000000150000001A0000001E0000002100000024000000260000
          0028000000290000012B000000260000001F0000002500000025000000250000
          002500000020000000250000012B000000290000002800000026000000240000
          00210000001E0000001A000000150000000E0000000400000000000000000000
          00080000001B000000280000012F00000134000001380000013B0000013D0000
          0140000001410000003C040A054D1D5022BA24652AD8236229D5236129D42465
          2AD81F5525BF040A054E0000003C00000141000001400000013D0000013B0000
          0138000001340000012F000000280000001B0000000800000000000000000000
          000000000001000000030000000500000007000000090000000A0000000C0000
          000D0000000D000000031F5828AC2D893CFF2D883CFF2E883CFF2E883CFF2D88
          3CFF2D893CFF1F5B29B1000000030000000D0000000D0000000C0000000A0000
          0009000000070000000500000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000277739D52D8D42FF2D8D43FF2D8D43FF2D8D43FF2D8D
          43FF2D8D43FF297D3CE101010101000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000277C3FD52D9349FF2D9349FF2D9349FF2D9349FF2D93
          49FF2D9349FF288241E002030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000278144D52D9950FF2D9950FF2D9950FF2D9950FF2D99
          50FF2D9950FF288747E002030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000027854AD52D9F57FF2D9F57FF2D9F57FF2D9F57FF2D9F
          57FF2D9F57FF288D4DE002030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000278A4FD52DA45DFF2DA45DFF2DA45DFF2DA45DFF2DA4
          5DFF2DA45DFF289153E002030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000278E54D42DAA64FF2DAA64FF2DAA64FF2DAA64FF2DAA
          64FF2DAA64FF289658DF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000050D0912040D0813040D0913040D0913040D
          0913040D0913050D0913289459D72DAE69FF2DAE69FF2DAE69FF2DAE69FF2DAE
          69FF2DAE69FF299A5EE206100A16040D0913040D0913040D0913040D0913040D
          0813050E09130000000000000000000000000000000000000000000000000000
          00000000000008140E1B258755C02AA667EF29A767F02AA767F02AA767F02AA7
          67F02AA767F02AA666EF2BAF6CFC2CB06CFF2CB06CFF2CB06CFF2CB06CFF2CB0
          6CFF2CB06CFF2CB06CFD2AA767F02AA666EF2AA767F02AA767F02AA767F02AA7
          67F029A666EF268A57C50A191121000000000000000000000000000000000000
          0000000000001F6D479729B470FF2AB471FF2AB471FF2AB471FF2AB471FF2AB4
          71FF2AB471FF2AB471FF2AB471FF2BB471FF2BB471FF2BB471FF2BB471FF2BB4
          71FF2BB471FF2AB471FF2AB471FF2AB471FF2AB471FF2AB471FF2AB471FF2AB4
          71FF2AB471FF2AB470FF1F7249A0000000000000000000000000000000000000
          0000000000001E7E51AE2AB876FF29B876FF29B876FF29B876FF29B876FF29B8
          76FF29B876FF29B876FF29B876FF29B876FF29B876FF29B876FF29B876FF29B8
          76FF29B876FF29B876FF29B876FF29B876FF29B876FF29B876FF29B876FF29B8
          76FF29B876FF2AB876FF1C7E50AF000000000000000000000000000000000000
          0000000000001D7D53AA29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB
          7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB
          7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB7AFF29BB
          7AFF29BB7AFF29BB7AFF1B7D52AB000000000000000000000000000000000000
          0000000000001D7F55AA28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE
          7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE
          7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE7EFF28BE
          7EFF28BE7EFF28BE7EFF1B8055AC000000000000000000000000000000000000
          0000000000001C845AAF25C081FF24BF80FF25BF80FF25BF80FF25BF80FF25BF
          80FF25BF80FF24BF80FF25BF81FF26C081FF26C081FF26C081FF26C081FF26C0
          81FF26C081FF25BF81FF25BF80FF25BF80FF25BF80FF25BF80FF25BF80FF25BF
          80FF24BF80FF25C081FF1A865AB2000000000000000000000000000000000000
          0000000000001962458026C184FF2CC386FF2CC386FF2CC286FF2CC286FF2CC2
          86FF2CC286FF2CC386FF29C285FF23C083FF24C183FF24C183FF24C183FF24C1
          83FF23C083FF28C185FF2CC287FF2CC286FF2CC286FF2CC286FF2CC286FF2CC2
          86FF2CC286FF27C185FF196A4B8B000000000000000000000000000000000000
          00000000000002060508206F508E389D74C5379D74C6389C73C5389C73C5379C
          73C5379C73C5369C72C548C390F525C287FF21C286FF22C286FF22C286FF22C2
          86FF23C286FF48C490F7389D74C6389C73C5389C73C5389C73C5389C73C5379C
          73C5399C74C527745593030A070C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000037A77BD327C48AFF1FC388FF20C388FF20C388FF20C3
          88FF24C489FF3AB082DE00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000037A97DD425C58CFF1EC48AFF1FC48AFF1FC48AFF1FC4
          8AFF22C48BFF3BB283DF00010102000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000036AA80D523C68FFF1CC58DFF1DC58DFF1DC58DFF1DC5
          8DFF20C58EFF39B286E000030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000035AA81D521C791FF1AC68FFF1BC68FFF1BC68FFF1BC6
          8FFF1EC690FF38B387E000030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000034AB82D51FC894FF18C792FF19C792FF19C792FF19C7
          92FF1CC793FF37B489E000030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000032AB83D51CC995FF16C894FF17C894FF17C894FF17C8
          94FF19C895FF35B48AE000030204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000039B18ADA1BCA98FF10C996FF12C996FF12C996FF11C9
          96FF17CA97FF3BBA90E500020203000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000277D629840CFA1FF21CC9CFF21CD9CFF21CD9CFF20CC
          9BFF3ECFA1FF2E876BA400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000403051D614C76267F639C237E629A227E629A2680
          649D1F644F7A0007060800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000405090C1A204051283365812F3C799B37468DB636468DB62D3B
          789B25316381171E3E510304090C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001C21
          40513D498FB45265C4F95366C8FF5165C7FF5064C8FF4E63C8FF4D62C7FF4B61
          C5FF4A61C3FF4960C3FF455DBDF9314288B4161E3D5100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000013172B3646529FC65969
          CCFF5768CBFF5567CAFF6E7CD6FF808CE0FF8691E4FF8B95E9FF8A94E9FF828E
          E3FF7A87DEFF6576D2FF4860C2FF475FC2FF465EC1FF354895C60E1429360000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002D3361785D6CCEFF5B6BCDFF6B79
          D4FF8994E4FFABB0F5FFACB1F7FFA6ACF7FF9FA5F5FF989EF4FF979DF3FF9CA1
          F4FFA1A7F5FFA5AAF6FFA1A7F4FF7C89DFFF596DCCFF455EC1FF445DC0FF202C
          5A78000000000000000000000000000000000000000000000000000000000000
          000000000000000000000304070940488AA8606DD0FF5E6CCFFF858FE1FFB3B8
          F8FFADB3F7FFA6ADF6FF9EA4F5FF949BF4FF8B92F2FF848BF0FF838AF0FF888F
          F1FF8F95F2FF979CF4FF9DA2F5FFA2A7F6FFA5AAF6FF7181D9FF455EC1FF445D
          C0FF2C3D7EA80203070900000000000000000000000000000000000000000000
          00000000000000000000414888A5636FD1FF6774D4FF9EA5EDFFB2B8F8FFAAB0
          F7FF9FA6F5FF9099F3FF848CF1FF7A83EFFF727BEEFF6D76EEFF6C74EDFF6F77
          EDFF757CEEFF7D84EFFF878DF1FF9398F3FF9CA1F4FFA2A6F5FF8993E6FF4C63
          C5FF445DC0FF2C3D7EA800000000000000000000000000000000000000000000
          0000000000003237667B6671D3FF6975D4FFADB4F4FFB3B8F8FFA6ADF7FF979F
          F5FF868FF2FF7883F0FF6E79EEFF6772EDFF636EECFF616BECFF606AECFF6069
          EBFF626BEBFF676FEBFF6F76EDFF7A81EFFF898EF1FF969BF3FFA0A5F5FF989E
          EFFF4C63C4FF435DC0FF202C5C7B000000000000000000000000000000000000
          0000181A30396872D5FF6771D4FFA3ABEFFFB4BBF8FFA6AEF6FF939CF4FF808A
          F1FF727EF0FF6A75EEFF6571EDFF616DECFF606BECFF5E69EBFF5D67EBFF5D67
          EBFF5C65EAFF5D66EBFF6069EAFF666DEBFF7278EDFF8288F0FF9499F2FFA0A4
          F5FF8B94E7FF445DC0FF435DC0FF0F152B390000000000000000000000000000
          00005259A4C36A73D5FF9098E5FFB8BFF9FFAAB2F8FF959EF4FF7F8BF2FF717D
          EFFF6975EEFF6572EEFF636FEDFF626EEDFF606CECFF5F6AECFF5E69EBFF5D67
          EBFF5C66EBFF5B64EAFF5B64EAFF5D65EAFF626AEBFF6F76EDFF8288F0FF969B
          F3FFA2A6F5FF7281D9FF445DC0FF334793C30000000000000000000000002224
          424E6C75D7FF7A83DCFFBDC3FAFFB2BAF9FF9DA7F6FF8490F3FF7380F0FF6B78
          EFFF6875EEFF6572EEFF6471EEFF636FEDFF626EEDFF606CECFF5F6AECFF5E69
          EBFF5D67EBFF5C66EBFF5B64EAFF5B64EAFF5C64E9FF6269EBFF7178EDFF888E
          F1FF9CA1F4FFA5A9F6FF566CCAFF445DC0FF141C3A4E00000000000000005257
          A0BD6E75D7FF9AA0EAFFBAC1F9FFA9B2F7FF909BF5FF7986F2FF6E7CF0FF6A78
          EFFF6876EFFF6774EEFF6572EEFF6471EEFF636FEDFF626EEDFF606CECFF5F6A
          ECFF5E69EBFF5D67EBFF5C66EBFF5B64EAFF5B64EAFF5D65EAFF666DEBFF7A81
          EFFF9298F3FFA1A6F5FF7A87DEFF455EC1FF32458EBD00000000090A12156F76
          D6FC6F76D8FFBEC6FAFFB5BDF9FF9EA9F7FF8491F3FF7482F2FF6C7BF0FF6A79
          F0FF6977EFFF6876EFFF6774EEFF6572EEFF6471EEFF636FEDFF626EEDFF606C
          ECFF5F6AECFF5E69EBFF5D67EBFF5C66EBFF5B64EAFF5B64EAFF6069EAFF6E76
          EDFF878DF1FF9CA1F5FFA3A8F5FF465EC1FF445DBEFC0608101527294A577177
          DAFF8B92E4FFBFC6FAFFAEB7F8FF93A0F5FF7C8BF3FF7181F1FF6D7CF1FF6B7A
          F0FF6A79F0FF6977EFFF6876EFFF6774EEFF6572EEFF6471EEFF636FEDFF626E
          EDFF606CECFF5F6AECFF5E69EBFF5D67EBFF5C66EBFF5B64EAFF5D66EAFF666F
          EBFF7C83EFFF959BF3FFA3A9F6FF6678D3FF465EC1FF1821445A3F42778B7278
          DAFF9EA5EBFFBBC3FAFFA7B1F8FF8B99F5FF7888F3FF7080F2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C65EAFF626A
          EBFF737BEDFF8E94F2FFA0A5F5FF7B89DFFF475FC2FF26336A8B4F5396AD747A
          DCFFA7AFF0FFB5BEFAFF9FABF7FF8695F5FF7686F3FF7182F2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D67EBFF6069
          EBFF6E76EDFF868DF1FF999FF4FF8691E6FF485FC3FF304084AD575AA3BB757A
          DDFFA9B0F2FFB0BAF9FF9AA7F7FF8392F5FF7687F3FF7182F2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D67EBFF606A
          ECFF6B73ECFF8088F0FF949AF3FF8A93E9FF4A60C6FF364793BE575AA3BA767B
          DEFFAAB2F2FFB1BBF9FF9BA8F7FF8493F5FF7788F3FF7284F3FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E69EBFF616B
          ECFF6C75EDFF8289F0FF959BF3FF8B94E9FF4B61C6FF364894BE515497AD777B
          DEFFAAB1F1FFB8C1FAFFA3AFF8FF8999F6FF798AF4FF7486F3FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF606BECFF636E
          ECFF717AEEFF8990F1FF9DA3F4FF8993E7FF4B61C5FF324185AD4244798B787B
          DDFFA3A8EDFFC0C8FAFFACB7F9FF909FF6FF7D8EF5FF7587F4FF7385F3FF7284
          F3FF7182F2FF7081F2FF6F7FF2FF6E7EF1FF6D7CF1FF6B7AF0FF6A79F0FF6977
          EFFF6876EFFF6774EEFF6572EEFF6471EEFF636FEDFF626EEDFF616DECFF6771
          EDFF7881EFFF9399F3FFA5AAF5FF808DE0FF4C62C5FF29356B8B2A2A4C57797C
          DEFF9196E6FFC6CCFCFFB5BFF9FF9AA8F8FF8394F5FF788AF4FF7486F4FF7385
          F3FF7284F3FF7182F2FF7081F2FF6F7FF2FF6E7EF1FF6D7CF1FF6B7AF0FF6A79
          F0FF6977EFFF6876EFFF6774EEFF6572EEFF6471EEFF636FEDFF6571EDFF6D78
          EEFF838BF1FF9CA3F5FFAAAFF7FF6D7CD6FF4D62C5FF1A21435709091012797B
          DBFC787BDEFFC7CEFBFFBEC6FBFFA7B3F8FF8D9CF7FF7C8EF5FF7689F4FF7486
          F4FF7385F3FF7284F3FF7182F2FF7081F2FF6F7FF2FF6E7EF1FF6D7CF1FF6B7A
          F0FF6A79F0FF6977EFFF6876EFFF6774EEFF6572EEFF6572EEFF6975EEFF7882
          F0FF8F97F3FFA5ABF6FFACB1F7FF5064C7FF4D62C4FC06081015000000005A5D
          A5BD797CDEFFA6ADEFFFC4CCFBFFB4BEFAFF9AA8F7FF8495F6FF798CF4FF7689
          F4FF7486F4FF7385F3FF7284F3FF7182F2FF7081F2FF6F7FF2FF6E7EF1FF6D7C
          F1FF6B7AF0FF6A79F0FF6977EFFF6876EFFF6875EEFF6875EEFF717DF0FF858E
          F2FF9DA4F5FFACB1F7FF8793E3FF5165C7FF3B4A93BD00000000000000002929
          49547A7DDFFF8C90E5FFC9D0FCFFBEC7FBFFA9B5F9FF91A1F7FF8092F6FF798B
          F4FF7689F4FF7486F4FF7385F3FF7284F3FF7182F2FF7081F2FF6F7FF2FF6E7E
          F1FF6D7CF1FF6B7AF0FF6A79F0FF6A78EFFF6B78EFFF707CEFFF7E89F1FF959D
          F4FFA8AEF6FFB1B6F8FF6978D4FF5265C8FF1B21425400000000000000000101
          03036465B5CF7A7CDEFFA1A5ECFFC6CEFCFFB9C3FBFFA3B0F9FF8F9FF7FF8192
          F6FF798CF4FF7689F4FF7486F4FF7385F3FF7284F3FF7182F2FF7081F2FF6F7F
          F2FF6E7EF1FF6D7CF1FF6C7BF0FF6E7CF0FF7380F0FF7F8AF2FF919BF4FFA4AB
          F6FFB0B6F7FF838FE0FF5567CAFF4353A3CF0101020300000000000000000000
          00001D1E353C7B7DDFFF7A7CDEFFB8BEF5FFC4CDFBFFB6C1FAFFA4B1F9FF92A2
          F7FF8596F6FF7D8FF5FF788BF4FF7587F4FF7486F3FF7284F3FF7182F2FF7182
          F2FF7080F2FF7181F1FF7482F2FF7986F2FF8490F3FF949DF4FFA4ACF6FFB0B6
          F8FFA0A8EEFF5869CBFF5668CAFF14182F3C0000000000000000000000000000
          000000000000424276877B7DDFFF8385E1FFBFC6F8FFC4CDFBFFB9C3FBFFAAB6
          F9FF9AA9F8FF8D9DF7FF8495F5FF7D8FF5FF798BF4FF7789F3FF7688F3FF7687
          F3FF7888F3FF7D8BF3FF8491F3FF8F9AF5FF9CA5F5FFA9B1F7FFB2B9F8FFAAB1
          F3FF6573D2FF5969CCFF2E376B87000000000000000000000000000000000000
          0000000000000101030356579BB17A7DDFFF7F82E0FFB3B8F3FFC7CEFCFFBFC8
          FBFFB4BEFAFFA7B3F8FF9BA9F8FF91A0F6FF8A99F6FF8595F5FF8494F5FF8796
          F5FF8C99F5FF94A0F6FF9EA8F7FFA9B1F8FFB1B8F9FFB7BDF8FF9FA7EDFF6472
          D1FF5C6BCDFF3E4A8EB101010203000000000000000000000000000000000000
          0000000000000000000004040809515293A87A7DDFFF797CDEFF9EA2EBFFC9D0
          FCFFC4CBFCFFBEC6FAFFB6BFFAFFADB7F9FFA4B0F8FF9DAAF7FF9CA8F7FFA1AC
          F8FFA8B2F8FFAFB7F8FFB5BCF9FFB9BFF9FFBCC2FAFF8B94E3FF606ED0FF5F6D
          CFFF3D4788A80304070900000000000000000000000000000000000000000000
          0000000000000000000000000000000000003A3B69787A7CDEFF797CDEFF878B
          E2FFA4AAEDFFC3C9FAFFC5CCFBFFC0C8FAFFB9C1FAFFB3BCF9FFB2BBF9FFB6BE
          FAFFBBC2FAFFBEC5FAFFBAC0F7FF96A0E8FF757FDAFF636FD2FF626ED1FF2D33
          6278000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001A1A2F365D5FAAC3797C
          DEFF777BDDFF767ADCFF8C91E5FF9BA2EAFFA3AAEEFFA7AEF0FFA6ADF0FFA0A7
          EDFF959CE8FF838BE0FF6973D5FF6872D4FF6671D3FF4C56A1C315182C360000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002526
          444E53559AB17377D5F6767ADCFF7479DBFF7378DCFF7279DBFF7078DAFF6F76
          D9FF6D75D7FF6C74D7FF666FCEF6494F94B12023414E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000040408092426444E383A68794649859A4F5497B04E5397B04448
          849A353866792124424E04040809000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000B3423451A7E56A71D8E60BB1C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91D8E60BB1A7E56A70B3423450000000000000000000000001A7F
          56A827C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF1A7F56A8000000000B34234527C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF0B3423451A7E56A727C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1A7E56A71D8E60BB27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8D60BA1C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF23C081FF20BF7FFF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF24C081FF24C081FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF1ABD
          7BFF22BF7FFF40C891FF12BB77FF27C183FF27C183FF27C183FF27C183FF26C1
          83FF17BC7AFF05B76FFF19BC7AFF16BB78FF06B770FF19BD7BFF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF1DBE7DFF52CD
          9BFFFFFFFFFFFFFFFFFF70D5ACFF14BC78FF27C183FF27C183FF26C183FF03B6
          6DFF80DAB6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6DD4ABFF07B870FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF17BC7AFF8FDE
          BEFFFFFFFFFFFFFFFFFFFFFFFFFF13BA77FF23C080FF27C183FF0AB872FFB4EA
          D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF93E0C1FF10BA
          76FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF25C082FF08B8
          72FFFFFFFFFFFFFFFFFFFFFFFFFFCEF1E1FF04B76FFF20BF7FFF39C68DFFFFFF
          FFFFFFFFFFFFFFFFFFFF8FDFBFFF9CE2C6FFFFFFFFFFFFFFFFFFFFFFFFFF22BF
          80FF22BF80FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF19BD
          7BFF56CE9DFFFFFFFFFFFFFFFFFFFFFFFFFF4ECB98FF0AB973FF8DDDBDFFFFFF
          FFFFFFFFFFFF92DEC0FF00B56CFF00B469FFB0E7D1FFFFFFFFFFFFFFFFFF71D5
          ACFF1ABD7BFF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF02B66EFFD6F2E8FFFFFFFFFFFFFFFFFFFFFFFFFF00B165FF83DCB6FFFFFF
          FFFFFFFFFFFFB2E9D3FF00B266FF00B163FFD0F0E3FFFFFFFFFFFFFFFFFF68D3
          A8FF1BBD7CFF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF22BF80FF18BB79FFFFFFFFFFFFFFFFFFFFFFFFFF9BE2C4FF06B770FFFFFF
          FFFFFFFFFFFFFFFFFFFFCFF1E3FFDCF4EAFFFFFFFFFFFFFFFFFFFFFFFFFF10BA
          76FF24C081FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF11BB77FF86DBB9FFFFFFFFFFFFFFFFFFFFFFFFFF14BC78FF6CD4
          AAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF58CE9EFF18BD
          7BFF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF26C082FF00B56BFFFEFEFEFFFFFFFFFFFFFFFFFFEEFAF6FF00AE
          5DFF41C890FFCEF1E3FFFFFFFFFFFFFFFFFFC6EEDDFF32C489FF14BB78FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF1EBE7EFF39C58CFFFFFFFFFFFFFFFFFFFFFFFFFF72D6
          AEFF0CB974FF0EBA75FF07B871FF07B871FF0FBA75FF20BF7FFF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF1BBD7CFF0DB9
          74FF07B770FF08B871FF10BA76FF03B66EFFB8E9D5FFFFFFFFFFFFFFFFFFFFFF
          FFFF0EB974FF23C081FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF08B871FF5ACFA0FFDAF3
          EAFFFFFFFFFFFFFFFFFFBEEBD9FF24C080FF00B36AFFFFFFFFFFFFFFFFFFFFFF
          FFFFC3EEDDFF06B770FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF0AB973FFA3E5C9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF34C48AFF5CD0A2FFFFFFFFFFFFFF
          FFFFFFFFFFFF43C893FF1CBE7DFF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF1EBE7EFF44C893FFFFFFFFFFFFFFFFFFFFFF
          FFFFC1EDDCFFE7F8F1FFFFFFFFFFFFFFFFFFFFFFFFFF00AB59FFE6F7F1FFFFFF
          FFFFFFFFFFFFFFFFFFFF00B46BFF26C082FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF13BB77FFAAE7CFFFFFFFFFFFFFFFFFFF88DB
          B9FF00B469FF00B063FFE7F8F0FFFFFFFFFFFFFFFFFF48C995FF19BC7AFFFFFF
          FFFFFFFFFFFFFFFFFFFF96E0C2FF0FBA75FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF12BB77FFAFE8D1FFFFFFFFFFFFFFFFFF6ED4
          ACFF04B76FFF00B266FFD1F0E4FFFFFFFFFFFFFFFFFF54CC9CFF05B770FF97E1
          C4FFFFFFFFFFFFFFFFFFFFFFFFFF20BE7FFF21BF7FFF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF1BBD7CFF5ED0A2FFFFFFFFFFFFFFFFFFFFFF
          FFFF86DBBAFFAEE8D0FFFFFFFFFFFFFFFFFFFFFFFFFF0EB974FF24C081FF01B6
          6DFFFFFFFFFFFFFFFFFFFFFFFFFFE5F7EFFF02B66EFF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF26C183FF04B66EFFD8F3E8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6ED5ACFF17BC79FF27C183FF1BBD
          7CFF48CA95FFFFFFFFFFFFFFFFFFFFFFFFFF5ED0A2FF1DBE7DFF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF26C082FF04B66DFF94DFC1FFFFFF
          FFFFFFFFFFFFFFFFFFFFF6FCFAFF54CE9DFF0DB974FF27C183FF27C183FF27C1
          83FF05B770FFC7EFDEFFFFFFFFFFFFFFFFFF2DC286FF22BF80FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF26C082FF15BC78FF03B7
          6EFF1CBC7CFF11BB75FF08B871FF1CBD7CFF27C183FF27C183FF27C183FF27C1
          83FF26C082FF0EB974FF4FCB99FF16BB78FF1FBF7EFF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91C8C5FB927C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF24C081FF25C082FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF26C082FF1FBE7EFF24C081FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8C5FB91D8E60BB27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1C8D60BA1A7E56A727C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF1A7E56A70B34234527C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF0B34234500000000197D
          55A527C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C183FF27C1
          83FF27C183FF27C183FF27C183FF27C183FF197D55A500000000000000000000
          00000A3323441A7E56A71D8E60BB1C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C5FB91C8C
          5FB91C8C5FB91D8E60BB1A7E56A70A3323440000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000151A0000617900008DB0000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008DB0000061790000151A000000000000000000000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C4F5000054690000000000000000000000000000
          54690000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF00005469000000000000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A0000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB1000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF1A1AD1FF2E2ED5FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF1111CFFF0F0FCFFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF4242
          D9FFF8F8FDFFFFFFFFFF6565E0FF0000CCFF0000CCFF0000CCFF0000CCFF0101
          CCFF6F6FE2FFE0E0F8FFFFFFFFFFFFFFFFFFD8D8F7FF6161DFFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF6C6C
          E1FFFFFFFFFFFFFFFFFFF1F1FCFF1B1BD1FF0000CCFF0000CCFF0000CCFF9898
          EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8080E5FF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0C0C
          CEFFE3E3F9FFFFFFFFFFFFFFFFFFACACEEFF0000CCFF0000CCFF3232D6FFFEFE
          FEFFFFFFFFFFF7F7FDFF8B8BE7FF9494E9FFFCFCFEFFFFFFFFFFFBFBFEFF1E1E
          D2FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF5252DCFFFFFFFFFFFFFFFFFFFFFFFFFF4B4BDBFF0000CCFF7171E2FFFFFF
          FFFFFFFFFFFF8F8FE8FF0000CCFF0000CCFFA6A6EDFFFFFFFFFFFFFFFFFF5A5A
          DEFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFFB3B3EFFFFFFFFFFFFFFFFFFFDFDFF8FF0A0ACEFF6A6AE1FFFFFF
          FFFFFFFFFFFFA4A4ECFF0000CCFF0000CCFFBBBBF1FFFFFFFFFFFFFFFFFF5353
          DCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF1F1FD2FFF4F4FCFFFFFFFFFFFFFFFFFF8989E7FF1F1FD2FFF9F9
          FDFFFFFFFFFFFFFFFFFFBEBEF2FFC6C6F3FFFFFFFFFFFFFFFFFFF1F1FCFF1010
          CFFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF7676E3FFFFFFFFFFFFFFFFFFFAFAFEFF2D2DD5FF6868
          E0FFFDFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFEFF5454DCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0404CDFFD3D3F6FFFFFFFFFFFFFFFFFFC5C5F3FF0101
          CCFF3D3DD8FFADADEEFFDEDEF8FFDCDCF8FFA5A5EDFF3131D6FF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF3B3BD7FFFDFDFEFFFFFFFFFFFFFFFFFF6666
          E0FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF9B9BEBFFFFFFFFFFFFFFFFFFEEEE
          FBFF1616D0FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF5050DCFFB7B7
          F0FFE2E2F9FFDADAF7FF9E9EEBFF2828D4FF1212CFFFEAEAFAFFFFFFFFFFFFFF
          FFFFA4A4ECFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF8B8BE7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6FDFF4545D9FF5F5FDFFFFFFFFFFFFFFF
          FFFFFEFEFEFF4444D9FF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF3C3CD8FFFEFEFEFFFFFFFFFFFCFC
          FEFFB5B5F0FFCDCDF5FFFFFFFFFFFFFFFFFFE5E5F9FF0707CDFFBFBFF2FFFFFF
          FFFFFFFFFFFFDADAF7FF0707CDFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF8B8BE7FFFFFFFFFFFFFFFFFF8383
          E6FF0000CCFF0202CCFFCFCFF5FFFFFFFFFFFFFFFFFF3C3CD8FF2828D4FFF8F8
          FDFFFFFFFFFFFFFFFFFF8181E6FF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF9090E8FFFFFFFFFFFFFFFFFF7171
          E2FF0000CCFF0000CCFFC0C0F2FFFFFFFFFFFFFFFFFF4141D9FF0000CCFF8383
          E6FFFFFFFFFFFFFFFFFFF8F8FDFF2727D3FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF5050DCFFFFFFFFFFFFFFFFFFF0F0
          FCFF8585E6FFA1A1ECFFFFFFFFFFFFFFFFFFF2F2FCFF0D0DCEFF0000CCFF0808
          CDFFDCDCF8FFFFFFFFFFFFFFFFFFBEBEF2FF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0101CCFFB1B1EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF6464E0FF0000CCFF0000CCFF0000
          CCFF4747DAFFFEFEFEFFFFFFFFFFFFFFFFFF4444D9FF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0505CDFF7E7EE5FFE6E6
          FAFFFFFFFFFFFEFEFEFFCECEF5FF4E4EDBFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFFA5A5EDFFFFFFFFFFEFEFFBFF2424D3FF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF000099BF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF1313CFFF0C0CCEFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0505CDFF3838D7FF1212CFFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF000099BF00008DB00000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF00008EB10000627A0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000627A0000151A0000
          C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000C4F50000151A000000000000
          54690000CBFE0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CBFE0000546900000000000000000000
          0000000054690000C4F50000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000C5F6000054690000000000000000000000000000
          0000000000000000151A0000617900008CAF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF000099BF000099BF000099BF000099BF000099BF000099BF000099BF0000
          99BF00008CAF000061790000151A000000000000000000000000}
      end>
  end
  object ActMainMenu: TActionList
    Images = cxImageList32x32
    Left = 212
    Top = 132
    object ActAlterarQuantidade: TAction
      ImageIndex = 17
      OnExecute = ActAlterarQuantidadeExecute
    end
    object ActCancelarItem: TAction
      ImageIndex = 18
      OnExecute = ActCancelarItemExecute
    end
    object ActAdicionar1: TAction
      ImageIndex = 55
      OnExecute = ActAdicionar1Execute
    end
    object ActAdicionar2: TAction
      ImageIndex = 52
      OnExecute = ActAdicionar2Execute
    end
    object ActSubtrair1: TAction
      ImageIndex = 55
      OnExecute = ActSubtrair1Execute
    end
    object ActSubtrair2: TAction
      ImageIndex = 52
      OnExecute = ActSubtrair2Execute
    end
  end
  object cxImageList22x22: TcxImageList
    DrawingStyle = dsTransparent
    Height = 22
    Width = 22
    FormatVersion = 1
    DesignInfo = 8847467
    ImageInfo = <
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B0B1E2125276872272A6F7A2224
          60691C1E50581314353A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          0404242666704145B7C95257E8FF5257E8FF5257E8FF5257E8FF5257E8FF5257
          E8FF4044B6C81819434A00000000000000000000000000000000000000000000
          00000000000000000000000000000000000004040C0D3B3EA8B95257E8FF5257
          E8FF5257E8FF5257E8FF5257E8FF5257E8FF5257E8FF5257E8FF5257E8FF5257
          E8FF4145B8CA0809181A00000000000000000000000000000000000000000000
          0000000000000708181A3D41BCCF4C51E7FF4348E1FF4044E0FF5055E7FF5257
          E8FF5257E8FF5257E8FF5257E8FF5257E8FF4347E0FF474BE2FF5257E8FF4347
          BCCF04040C0D000000000000000000000000000000000000000000000000363B
          B7CA464BE7FF3A3EE0FF5051D7FF7C7DE0FF3336DAFF4C52E7FF4E53E7FF4F54
          E7FF5156E7FF383BDBFF7D7EE0FF5152D7FF4347E0FF5257E8FF3B3FA8B90101
          0404000000000000000000000000000000001214434A3F45E6FF383DE0FF4F50
          D7FFF6F6FDFFFFFFFFFF7E7EE1FF3034D9FF494EE7FF4A4FE7FF3236D9FF7E7F
          E1FFFFFFFFFFF6F6FDFF5152D7FF474BE2FF5257E8FF24266670000000000000
          000000000000000000002D31B4C83A40E6FF3035DFFF7879DFFFFFFFFFFFFFFF
          FFFFFFFFFFFF7B7CE0FF2D31D9FF2E31D9FF7C7CE0FFFFFFFFFFFFFFFFFFFFFF
          FFFF7D7DE0FF3F43E0FF4F54E7FF3F43B6C90000000000000000000000000B0D
          343A343AE5FF353BE5FF363CE6FF282CDAFF7374DFFFFEFEFFFFFFFFFFFFFFFF
          FFFF7979DFFF797ADFFFFFFFFFFFFFFFFFFFFFFFFFFF7E7EE1FF3034DAFF484D
          E7FF494FE7FF4B50E7FF0A0A1E21000000000000000010124F582E35E5FF3036
          E5FF3137E5FF3339E5FF262ADBFF696ADDFFFDFDFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF7B7CE0FF2B2FD9FF4247E6FF4349E6FF444AE7FF464B
          E7FF20226772000000000000000010135E692930E5FF2B31E5FF2C33E5FF2F35
          E5FF3137E5FF262ADAFF6162DBFFFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF7979
          DFFF292DD9FF3D42E6FF3D43E6FF3E44E6FF3F45E6FF4146E6FF20226E7A0000
          00000000000011146D7A242BE4FF272DE4FF3138E5FF3C43E7FF4045E8FF2B2F
          DAFF797ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7ADFFF3134DAFF4A50
          E8FF4A4FE8FF4248E7FF3B41E6FF3C41E6FF191C5F6900000000000000000D11
          6672292FE5FF4247E8FF464CE8FF474DE8FF3134DAFF7C7DE0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFDFDFFFFFFFFFFFFFFFFFFFF7D7EE1FF383BDBFF5459E9FF555A
          E9FF5358E9FF3F44E7FF13154F58000000000000000003041D21262CE4FF5156
          EAFF5258EAFF383CDCFF7F80E2FFFFFFFFFFFFFFFFFFFFFFFFFF7C7DE1FF6263
          DBFFFCFCFFFFFFFFFFFFFFFFFFFF8081E2FF4044DDFF6065EBFF6165EBFF3C41
          E6FF0C0D343A0000000000000000000000001116B4CA4F55EAFF4D51E4FF7F7F
          E1FFFFFFFFFFFFFFFFFFFFFFFFFF7F80E1FF4447DDFF4D51E2FF6465DBFFFCFC
          FFFFFFFFFFFFFFFFFFFF8081E1FF575AE4FF5F63EBFF2328B4C8000000000000
          00000000000000000000070A64702D34E6FF5C5FE6FF5657D8FFF7F7FDFFFFFF
          FFFF8283E1FF4B4DDEFF6F73EDFF7074EEFF575AE3FF6667DBFFFCFCFEFFF8F8
          FDFF5859D9FF6569E7FF4045E7FF0B0D424A0000000000000000000000000000
          000000000404090FA4B94047E9FF6165E6FF5859D8FF8282E1FF5356DFFF7B80
          EFFF7C80EFFF7D81EFFF7D82EFFF6164E4FF6869DCFF5D5ED9FF696CE7FF4E54
          EAFF1A1FB5CA0000000000000000000000000000000000000000000000000001
          0C0D060DB7CF4E53EAFF7478EAFF6E72E8FF898CF1FF898DF1FF8A8EF1FF8B8E
          F1FF8B8FF1FF8C90F1FF7A7DEAFF7C80ECFF595EEBFF151BB8CF0304171A0000
          00000000000000000000000000000000000000000000000000000001171A040A
          B3CA2128E5FF7277EFFF979AF3FF979BF3FF989BF3FF999BF3FF999CF3FF9A9D
          F3FF787CEFFF2C33E6FF0F14A5B901010C0D0000000000000000000000000000
          00000000000000000000000000000000000000000000000000000003424A0208
          B1C8171EE4FF343BE7FF5258EBFF5458EBFF383FE8FF1C24E4FF090FB2C90609
          6470000004040000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000002333A0003
          4E5800045D6901056C7A0205657201021D210000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000008361853168A
          31D8018D01FF0E8F10F63B833DCA081B09300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101011B9E42F122B25EFF078F08FF0D92
          10FF45A548FF1A7E1FDF00000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000020404040C9316FF1BA033FF089009FF09930DFF2DA031FF1593
          1CFF010101020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000028C02FF1E9C22FF139E20FF19A52BFF26A632FF0C9814FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000038E04FF2CA6
          35FF20AD38FF2AB84AFF39B44BFF0E9A18FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000069007FF39AD43FF29B647FF33C1
          59FF46BC5BFF119C1CFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000008910AFF45B34FFF2DB94CFF33C059FF4FBF62FF139E
          20FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A930EFF50B85AFF2CB74AFF32BD55FF57C169FF15A024FF000000000000
          000000000000000000000000000000000000000000000000000000110023006D
          00D8058505FF088A08FF088C08FF088E08FF09900AFF0B920DFF139818FF59BC
          63FF2AB446FF2FB94DFF60C270FF1EA42CFF1AA52CFF1CA830FF1EAA35FF21AD
          3AFF24B03FFF25B342FF1E993AD805190A23005B00B5309A30FF5EB15EFF60B4
          61FF63B864FF67BD69FF6EC272FF73C477FF73C679FF71C77AFF2DB144FF27B2
          42FF67C777FF69C677FF69C677FF68C676FF68C475FF66C372FF65C270FF68C5
          77FF4DC469FF1D8537B5007C00F669B769FF61B967FF4CB353FF4CB653FF4DB9
          55FF4FBB59FF51BE5DFF53C162FF52C162FF55C467FF5BC76DFF61C973FF68CA
          79FF62C772FF4ABA57FF21A42CFF169B1DFF119413FF2A992AFF76CF8AFF29B6
          4EF6068106F68EC98FFF73C37BFF54B85DFF54BB5DFF54BD5DFF55C061FF57C2
          63FF59C367FF56C465FF50C262FF50C364FF58C66AFF5CC76FFF5CC66DFF64C8
          74FF74CD80FF64C26CFF3DA83FFF309A30FF80D494FF2BB852F64D8F4DCCFDFE
          FDFFABD9ADFF93D097FF93D096FF92D297FF92D297FF91D397FF92D498FF95D8
          9EFF5CC96EFF5CC96EFF97DAA1FF95D89FFF98D9A2FF9BDAA4FF9EDBA7FFA1DC
          AAFFACE0B4FFAEE0B7FF9AE0ACFF32914DBA2030203FA5CEA5F13BA53BFF0D93
          0EFF0F9512FF119615FF139919FF159B1DFF21A22BFF9BD8A2FF66CC78FF67CD
          7AFF9EDCA6FF2DB041FF26AF3DFF29B243FF2CB647FF2FB94DFF32BC53FF5ACB
          77FF9BD4ABED2133263A00000000181818180D0D0D0D00000000000000000000
          0000000000000000000019A025FFA1DBA8FF6ED180FF6ED181FFA6DEAEFF26AD
          3CFF00000000000000000000000000000000000000000D0D0D0D181818180000
          0000000000000000000000000000000000000000000000000000000000000000
          00001AA128FFA3DCA9FF73D587FF74D587FFA9E0B0FF27AF3EFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001CA32AFFA4DD
          ACFF7ADA91FF7ADA91FFABE1B4FF29B141FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001DA52CFFA6DDAFFF82E09CFF82E0
          9CFFAEE2B8FF2AB344FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001FA72FFFA9DFB2FF89E5A6FF89E5A6FFB1E4BBFF2CB5
          48FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000020A832FFB8E4BFFFA8F0C2FFA4F0C0FFB9E6C2FF4BC164FF020202020000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003CAA4BEFF6FC
          F7FFA6DEB0FFA4DEB0FFA2DEAEFF7EC48EE90606060600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000F32144868B374DF1EAD37FF20AA
          3BF63F9451C11E30223900000000000000000000000000000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          00030000000A000000130000001B000000230000002C000000360000003F0000
          0049000000530000005500000050000000460000003D000000330000002B0000
          00220000001A0000000F00000007000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000A0C000099BA0000
          3A47000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000003B4700009ABA00000A0C00000000000000000000
          0000000000000000000000000A0C00009EC30000CEFF0000C8F7000039460000
          0000000000000000000000000000000000000000000000000000000000000000
          39460000C9F70000CFFF00009EC300000A0C0000000000000000000000000000
          0000000093BA0000CAFF0000CAFF0000CAFF0000C4F700003745000000000000
          000000000000000000000000000000000000000037450000C4F70000CAFF0000
          CAFF0000CBFF000094BA00000000000000000000000000000000000037470000
          BFF70000C5FF0000C5FF0000C5FF0000BFF60000354400000000000000000000
          000000000000000035440000BFF60000C6FF0000C6FF0000C6FF0000C0F70000
          37470000000000000000000000000000000000000000000035460000BBF70000
          C1FF0000C1FF0000C1FF0000BAF6000033430000000000000000000033430000
          BBF60000C2FF0000C2FF0000C2FF0000BCF70000354600000000000000000000
          000000000000000000000000000000000000000033450000B6F60000BDFF0000
          BDFF0000BDFF0000B6F600003142000031420000B6F60000BDFF0000BDFF0000
          BEFF0000B7F60000334500000000000000000000000000000000000000000000
          0000000000000000000000000000000031440000B2F60000B9FF0000B9FF0000
          B9FF0000B2F50000B2F50000B9FF0000B9FF0000B9FF0000B2F6000031440000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000002F430000AEF60000B5FF0000B5FF0000B5FF0000
          B5FF0000B5FF0000B5FF0000AFF6000030430000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000002E420000A9F50000B0FF0000B0FF0000B1FF0000B1FF0000
          AAF500002E420000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          2D420000A5F50000ACFF0000ACFF0000ACFF0000ACFF0000A5F500002D420000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000002C430000A2F60000A8FF0000
          A8FF0000A8FF0000A8FF0000A8FF0000A8FF0000A2F600002C43000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000002B4400009DF60000A3FF0000A3FF0000A4FF00009EF50000
          9EF50000A4FF0000A4FF0000A4FF00009EF600002C4400000000000000000000
          000000000000000000000000000000000000000000000000000000002B450000
          99F600009FFF00009FFF00009FFF000099F6000029420000294200009AF60000
          A0FF0000A0FF0000A0FF00009AF600002B450000000000000000000000000000
          000000000000000000000000000000002B46000096F700009BFF00009BFF0000
          9BFF000096F600002943000000000000000000002943000096F600009BFF0000
          9BFF00009BFF000097F700002B46000000000000000000000000000000000000
          000000002A47000091F7000096FF000097FF000097FF000092F6000028440000
          000000000000000000000000000000002844000092F6000097FF000097FF0000
          97FF000092F700002A470000000000000000000000000000000000006ABA0000
          92FF000092FF000092FF00008DF7000028450000000000000000000000000000
          000000000000000000000000284500008EF7000093FF000093FF000093FF0000
          6BBA000000000000000000000000000000000000070C00006DC300008EFF0000
          8AF7000027460000000000000000000000000000000000000000000000000000
          0000000000000000274600008BF700008FFF00006DC30000070C000000000000
          00000000000000000000000000000000060C000065BA00002647000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000002747000065BA0000070C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000502020246363937C44E5250E2141515320000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000050202024C3A3D3CD1686C
          6AFF696E6CFF666A69E000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000060202024D3A3D3CD1757877FFC8CAC9FF818583FF5457
          56A6000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000000000000000000000000000060202
          024E3C3E3CD36D716FFFDCDDDDFF8B908EFF5A5E5CB403030306000000000000
          0000000000000000000000000001000000070000000F00000015000000190000
          0019000000150000000F00000007000000040102014D3C3E3DD35E6160FF9EA1
          A0FF858988FF5B5E5DB503030306000000000000000000000000000000000000
          0004000000130000002C00000042000000500000005B0000005B000000500000
          00420000002D0000002B11121286595D5BFF666B69FF787D7BFF5C5F5EB70404
          040700000000000000000000000000000000000000060000001F0404044A595B
          5A9F8B8F8EC29FA5A2DEA7AEABF69AA09DDC828584BC3F444196030303600000
          005E0E0E0D9A565956FB747977FF5C605FB80404040700000000000000000000
          000000000000000000040000001F35373683AFB3B1E4D9DDDBFFD5DAD8FFC0C7
          C4FFAFB6B3FFBAC1BEFFC6CBC9FF99A49FFF858E88E22F312EC232302AF30F0F
          0D6A1C1C1C3B0202020400000000000000000000000000000000000000010000
          00133436357CC3C9C6FEC7CCCAF5B6B8B7CEBBBDBDCECBCDCDDDE3E5E5F5CBCD
          CDDDB9BBBBCE9CA29FCC99A39EF750504BFF303130A200000049000000130000
          0001000000000000000000000000000000000000000715161647ADB3B0F3BBBE
          BCE8CDCFCFDFAEA8A0C17D6D5D92725D49886F563E8777604B8E83705F9BB0A7
          9FC2CBCDCDDEAAAEACE59BA39FEF1415146E0000002F00000007000000000000
          000000000000000000000000000E606462A8A2A4A3D8D5D5D3E9726253865843
          306C55412F6956422F6A5944316E5F493475674F397F71563E8B836D599BD6D4
          D2E8979B99CC585D5BA8000000460000000E0000000000000000000000000000
          000000000014979C9ADCD7D8D7EE6556487B4938285A433325524031234F4132
          2350453526554C3A2A5E56422F6A614A35776E543D877E675296D7D9D8EC8D93
          91D500000056000000140000000000000000000000000000000000000016D0D4
          D3FD8B847BAC41322453372A1E432F241A3A2E2319392E23193932261C3D3B2D
          20494736275753402E67614B357870563E8A94877AABCACFCDFC0000005F0000
          00160000000000000000000000000000000000000014DFE2E2FF796D61A73428
          1D462E2319382C2219372B2118352C2218362D2219372E2319393A2C20474938
          285A5944316D68503A808F7964B3DBDFDEFF0000005600000014000000000000
          000000000000000000000000000EE9EBEBFF7F776BBE2E2319452C2219382B20
          1734291F1732292017332B2118352D23193833271C3F4434255354402E67644D
          377D917E69C4E8EAEAFF000000460000000E0000000000000000000000000000
          000000000007D5D7D7F3C6C2BBFC50473C7D2C22193C2B201734291F17322920
          17332B2118352D23193833271C3F4434255354402E6777614B9CBBB4A5FCD2D6
          D5F60000002F000000070000000000000000000000000000000000000001AAAE
          ADCBD4D7D5FFB8B4A8F25B5247822C22193D2B2118362C2218362D2219372E23
          19393A2C20474938285B735F4B9AAE9F8CF2C9CDCAFFA7AAA9D7000000130000
          000100000000000000000000000000000000000000002F30303CE5E8E8FDCED2
          CFFFCFC9C0FFA69D91D0675D5388483E3464372C214C5145386D736554969485
          71D6B0A492FFB9BEB9FFE4E7E7FE2D2E2D500000000400000000000000000000
          0000000000000000000000000000000000006769687DE8EBEAFFD8DBDAFFD9D7
          D1FFD5CFC3FFC4BCAFFFB9AE9FFFC0B6A6FFC8BFB0FFA1A399FFB4BCB8FFE5E8
          E8FF6365658A0000000600000000000000000000000000000000000000000000
          00000000000000000000000000006769687AE3E4E4F9E7E9E9FFDCE1DFFFC8CE
          CBFFB1B8B5FFC3C9C6FFD3D7D6FFD4D9D7FFDCE0DFFA61636382000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000909090B959896A9CBCDCDE2DFE2E1F8EAECECFFDEE1
          E0F8C7CAC9E3868A88AC08080811000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          0000000000000000000000000000020108140100033E0000002F000000010000
          0000000000000000000000000000000000000000000003020C28000000420000
          0017000000000000000000000000000000000000000000000000000000000000
          00000B082D3F2F22C0F22B1FB2EA060419790000003900000000000000000000
          00000000000000000000140F50753225CBFD251B97D60201085E000000190000
          000000000000000000000000000000000000000000000B082D3F3021C2F43D32
          D8FF453ADBFF3F33BAED06041878000000390000000000000000000000001813
          53774A3ED4FE4840E4FF382CD5FF251B99D70201085E00000019000000000000
          000000000000000000000B082C3F3021BEF4382CD3FF4036DDFF453DE2FF584F
          E1FF5247BEEF0604177700000039000000001C165479665CD9FE514AE8FF4942
          E6FF433ADFFF362AD0FF251A96D70201085E0000001900000000000000000C08
          2C3F4638C0F54536D1FF3A2ED5FF3E34DBFF443BE0FF4942E6FF6962E5FF655B
          C2F0060415771F1A55948078DEFE5751EAFF4C45E9FF473FE4FF4239DFFF3C31
          D9FF3426CBFF251993D70201085E000000170806202A5244C2F45E52D6FF564A
          D7FF5044D8FF4136DAFF4137DEFF463DE2FF4A43E7FF7670E9FF877DD4F89992
          E5FF5955EAFF4C45E9FF4841E5FF443CE1FF4036DCFF3B30D7FF3529D2FF3222
          C8FF25188FD6000000421710516B7165D6FF695FDBFF6156D9FF5A4FD8FF564C
          DBFF4A40DCFF4238DEFF453DE2FF4841E5FF534DE8FF4C45E6FF4942E6FF473F
          E4FF443CE1FF4137DDFF3D32D9FF382CD5FF3426D0FF3223CCFF3120BDFD0302
          0B28020106083F3396C17468D6FF6D63DCFF645ADAFF5E54DBFF5A50DCFF5047
          DDFF4138DDFF433AE0FF443CE1FF453CE1FF443BE1FF4239DFFF4036DCFF3D32
          D9FF392DD5FF3528D1FF3224CCFF321FBCFE140C487400000000000000000201
          0608403396C37B6FD7FF7066DCFF675DDBFF6157DBFF5C52DCFF5248DCFF4035
          DAFF3F34DBFF3F35DCFF3F34DBFF3D32DAFF3B30D7FF382CD4FF3528D1FF3627
          CDFF321EBAFE140C477400000000000000000000000000000000020106094236
          95C5867CDAFF736ADDFF6B61DCFF6359DBFF5D52DAFF5248D9FF3B2FD5FF3A2E
          D6FF392DD5FF382BD4FF3629D2FF3426D0FF392ACDFF4230BCFE140C46740000
          00000000000000000000000000000000000000000000020106094A3D98C98D83
          DCFF776DDFFF6F65DDFF675CDBFF5E53D9FF5246D7FF3729D0FF3426D0FF3325
          CFFF3325CFFF3325CFFF4F3EBEFE150D458F0000000000000000000000000000
          0000000000000000000000000000000000001F1651849389DAFF8279E1FF7A71
          DFFF7268DDFF6A5FDBFF6156D9FF5347D6FF3325CFFF3325CFFF3325CFFF3628
          CEFF402EA7F30603137700000039000000000000000000000000000000000000
          0000000000001E164A7B8A7FCFFE978FE4FF8D85E4FF857DE2FF7D74E0FF756B
          DEFF6D63DCFF655ADAFF4D41D5FF3325CFFF3325CFFF3325CFFF3727C2FF2B18
          92EA06031377000000390000000000000000000000000000000021194B7D9F95
          D7FFA9A3E8FFA19AE8FF9992E7FF9189E5FF8880E3FF8077E1FF786FDFFF7066
          DDFF685EDBFF4437D3FF3325CFFF3325CFFF3325CFFF3320BDFF2C1790EB0603
          1378000000390000000100000000261D4D78B9B1E1FFBFBAEEFFB5B0EDFFACA6
          EBFFA49EE9FF9C95E7FF948DE5FF8C84E4FF837BE2FF7B72E0FF736ADEFF6A5F
          DBFF3B2ED1FF3325CFFF3325CFFF3325CFFF3F2EC2FF3C2896ED060313790000
          002F110B2E48CFC9E9FFD2CFF3FFC8C4F2FFC0BCF0FFB8B3EEFFB0ABECFFA7A1
          EAFF9F99E8FF9790E6FF8F88E4FF877EE2FF7F76E0FF776DDFFF655ADAFF3325
          CFFF3325CFFF3325CFFF3325CFFF4B3BC5FF473598ED0100023E1C134060E4E1
          F4FFDDDBF6FFD3D0F4FFCBC8F2FFC3BFF1FFBBB7EFFFB3AEEDFFABA6EBFFA8A2
          E8FFB3ACE3FFA9A2E3FF8B83E3FF8279E1FF7A71DFFF4D41D5FF3325CFFF3325
          CFFF3325CFFF4A3CCCFF5D4CACF50201061400000000554C80A6E9E7F6FFE0DE
          F7FFD7D5F5FFCFCBF3FFC7C3F1FFBFBAEFFFBDB8EEFFC4BEE6FF2B21548A6D61
          A3D3B9B3E8FF8E86E3FF857DE2FF776EDFFF3729CFFF3325CFFF5245D0FF7C6D
          BDF80E082541000000000000000000000000595082A8EEECF8FFE4E2F8FFDBD8
          F6FFD2CFF4FFCFCCF3FFD7D3EEFF2F2753850000000002010609756AA7D4C4BE
          ECFF9189E4FF8981E3FF5C51D8FF5B4ED3FF8F81C6F90E082541000000000000
          00000000000000000000000000005F5587ABF3F1FBFFE7E5F9FFE2E0F8FFE7E4
          F5FF332B5787000000000000000000000000020106097F74ACD6CECAF1FF948D
          E5FFA59EE8FFA69BD2FA0E092642000000000000000000000000000000000000
          00000000000000000000635A8AADF6F6FCFFF2F0FAFF39315C86000000000000
          000000000000000000000000000002010508877DB1D8E1DFF6FFCDC7E5FC100B
          2742000000000000000000000000000000000000000000000000000000000000
          000000000000261F48671913344C000000000000000000000000000000000000
          00000000000000000000020105081E1449700A061C2C00000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060913005D020400500000001D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000102000A4A6530CCA4D6
          B6FF87A879F9081100720000001E000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000102000A415D26C97ECA9FFF4CC991FF63D2A3FF7DA1
          6FF8091100730000001F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000102
          00093A561DC56EB984FF46BE81FF4AC58BFF4ECC95FF5ED1A0FF6D965DF70911
          00730000001F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001020009314F14C15CA769FF3FB3
          70FF43BA7BFF47C185FF4CC78FFF50CE9AFF5BCF9EFF5E8B4AF6091100730000
          001F000000000000000000000000000000000000000000000000000000000000
          0000000000000102000A2E4D10C24E9952FF38A860FF3DAF6AFF41B674FF45BD
          7FFF49C389FF4DCA94FF51D19EFF5AD09EFF548240F6091100730000001E0000
          00000000000000000000000000000000000000000000000000000102000A2B4A
          0CC2458E42FF329D4FFF36A459FF3AAB64FF3EB26EFF42B879FF47BF83FF4BC6
          8DFF4FCD98FF53D4A2FF59D1A0FF4D7D38F5091100730000001E000000000000
          00000000000000000000000000000102000A2D4C0EC3438A3AFF2B923FFF2F99
          49FF3AA359FF4DB06FFF5CBB81FF60C08BFF5DC48EFF54C68FFF4DC992FF51D0
          9CFF55D7A7FF5AD3A3FF4D7D37F5091100730000001E00000000000000000000
          000001020005325012C34A8F42FF268931FF349442FF60AE70FF7FC190FF83C6
          98FF86CA9FFF8ACFA6FF8CD3ACFF90D8B3FF87D8B2FF69D4A6FF52D3A0FF57DA
          ABFF60D7AAFF568442F6091100730000001E0000000000000000152603606099
          55FF268931FF4D9E56FF89BF90FF8EC498FF90C89DFF93CCA3FF96D0A9FF9AD3
          AFFF9BD7B5FF9EDBBBFFA1DFC1FFA3E2C6FF8EE0BEFF5DD8A9FF58DDAFFF63DC
          B0FF608D4DF6091100730000001E000000000F1D014C70A162FF65AB6DFF9DC9
          A2FF9DCAA2FF9ECCA5FFA1CFAAFFABD4B4FFA6C5A1FE748C5ED995B389F7B7DD
          C2FFAFE2C8FFB1E6CDFFB4E9D2FFACE9D1FF64DCB0FF5ADFB3FF66DEB4FF6B94
          59F7091100730000001B00000000334A1D95BFD7BBFFAFD3B3FFB0D4B4FFB0D4
          B4FFB9D9BEFFA0B891F80911002F0000000001020005576F3EC0C8E6D2FFC0E9
          D5FFC3ECD9FFC5EEDDFFB9EDD9FF66DEB4FF5CE2B8FF69E0B8FF719962F70307
          004300000000000000003A4F2498CCDFCAFFC2DDC5FFC9E1CBFFACC09EF90911
          002E000000000000000000000000010200045F7747C4D7EEE0FFD1EFE1FFD3F1
          E4FFD4F3E7FFC6F1E1FF67E0B8FF5CE3B9FF99D8B6FF0E1C0059000000000000
          0000000000003D522899CFDECAFFABC09FF70911002D00000000000000000000
          0000000000000000000001020005687D51C8E4F4EBFFE1F5ECFFE3F7EEFFE4F8
          F0FFD1F4E8FF90E6C7FF5B7A43D8010200050000000000000000000000000000
          0000060B001E0203000800000000000000000000000000000000000000000000
          0000000000000102000570855ACCF0F9F4FFF2FBF7FFF2FBF7FFF3FCF8FF889E
          76E30205000D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001020005758A61CFF8FCFAFFFCFEFDFF93A681E60305000E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000102
          000642552F93546640A60305000E000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
      end
      item
        Image.Data = {
          C6070000424DC607000000000000360000002800000016000000160000000100
          2000000000009007000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000200000003000000050000000600000007000000080000
          0007000000060000000500000004000000030000000200000001000000000000
          0000000000000000000000000000000000000000000200000004000000050000
          0007000000090000000B0000000C0000000E00000010000000100000000E0000
          000D0000000B0000000900000007000000050000000300000001000000000000
          0000000000000000000000000000000000010000000300000004000000050000
          0007000000080000000800000009000000090000000800000008000000070000
          0005000000040000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000
          A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000
          A4FF0000A4FF00000000000000000000000000000000000000000000A4FF6969
          E1FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6C
          E2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF0000A4FF0000
          0000000000000000000000000000000000000000A4FF6969E1FF0303CDFF0000
          CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000CCFF0000
          CCFF0000CCFF0000CCFF0000CCFF6C6CE2FF0000A4FF00000000000000000000
          000000000000000000000000A4FF6969E1FF6C6CE2FF6C6CE2FF6C6CE2FF6C6C
          E2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6CE2FF6C6C
          E2FF6C6CE2FF6C6CE2FF0000A4FF000000000000000000000000000000000000
          00000000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000
          A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000A4FF0000
          A4FF0000A4FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
      end>
  end
  object cdsTodosGruposProduto: TGBClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspTodosGruposProduto'
    RemoteServer = DmConnection.dspProduto
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 440
    Top = 184
    object cdsTodosGruposProdutoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsTodosGruposProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
  end
  object dsTodosGruposProduto: TDataSource
    DataSet = cdsTodosGruposProduto
    Left = 416
    Top = 184
  end
  object cdsProdutoGrupoTabelaPreco: TGBClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_GRUPO_PRODUTO'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftString
        Name = 'ID_TABELA_PRECO'
        ParamType = ptInput
        Value = '0'
      end>
    ProviderName = 'dspProdutoGrupoTabelaPreco'
    RemoteServer = DmConnection.dspProduto
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 352
    Top = 128
    object cdsProdutoGrupoTabelaPrecoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsProdutoGrupoTabelaPrecoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsProdutoGrupoTabelaPrecoBO_ATIVO: TStringField
      DisplayLabel = 'Ativo'
      FieldName = 'BO_ATIVO'
      Origin = 'BO_ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsProdutoGrupoTabelaPrecoID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_SUB_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Sub Grupo de Produto'
      FieldName = 'ID_SUB_GRUPO_PRODUTO'
      Origin = 'ID_SUB_GRUPO_PRODUTO'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_MARCA: TIntegerField
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'ID_MARCA'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_CATEGORIA: TIntegerField
      DisplayLabel = 'Categoria'
      FieldName = 'ID_CATEGORIA'
      Origin = 'ID_CATEGORIA'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_SUB_CATEGORIA: TIntegerField
      DisplayLabel = 'Sub Categoria'
      FieldName = 'ID_SUB_CATEGORIA'
      Origin = 'ID_SUB_CATEGORIA'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_LINHA: TIntegerField
      DisplayLabel = 'Linha'
      FieldName = 'ID_LINHA'
      Origin = 'ID_LINHA'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_MODELO: TIntegerField
      DisplayLabel = 'Modelo'
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoID_UNIDADE_ESTOQUE: TIntegerField
      DisplayLabel = 'UN'
      FieldName = 'ID_UNIDADE_ESTOQUE'
      Origin = 'ID_UNIDADE_ESTOQUE'
      Required = True
    end
    object cdsProdutoGrupoTabelaPrecoTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Required = True
      Size = 30
    end
    object cdsProdutoGrupoTabelaPrecoCODIGO_BARRA: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      FieldName = 'CODIGO_BARRA'
      Origin = 'CODIGO_BARRA'
      Required = True
      Size = 30
    end
    object cdsProdutoGrupoTabelaPrecoVL_VENDA: TFMTBCDField
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      Origin = 'VL_VENDA'
      ProviderFlags = []
      ReadOnly = True
      currency = True
      Precision = 24
      Size = 9
    end
  end
  object dsProdutoGrupoTabelaPreco: TDataSource
    DataSet = cdsProdutoGrupoTabelaPreco
    Left = 328
    Top = 128
  end
  object pmTela: TPopupMenu
    Images = DmAcesso.cxImage16x16
    Left = 400
    Top = 232
    object ConfigurarValoresPadres1: TMenuItem
      Action = ActConfigurarValoresDefault
    end
  end
end
