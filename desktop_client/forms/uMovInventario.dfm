inherited MovInventario: TMovInventario
  Caption = 'Inventario'
  ClientWidth = 1001
  ExplicitWidth = 1017
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 1001
    ExplicitWidth = 1001
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 1001
    Properties.ActivePage = cxtsData
    ExplicitWidth = 1001
    ClientRectRight = 1001
    inherited cxtsSearch: TcxTabSheet
      ExplicitWidth = 1001
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 1001
        ExplicitWidth = 1001
      end
    end
    inherited cxtsData: TcxTabSheet
      ExplicitWidth = 1001
      inherited DesignPanel: TJvDesignPanel
        Width = 1001
        ExplicitWidth = 1001
        inherited cxGBDadosMain: TcxGroupBox
          ExplicitWidth = 1001
          Width = 1001
          inherited dxBevel1: TdxBevel
            Width = 997
            ExplicitWidth = 997
          end
          object labelCodigo: TLabel
            Left = 8
            Top = 8
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object labelDtCadastro: TLabel
            Left = 103
            Top = 8
            Width = 73
            Height = 13
            Caption = 'Cadastrado em'
          end
          object labelProcesso: TLabel
            Left = 315
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Processo'
          end
          object labelSituacao: TLabel
            Left = 524
            Top = 8
            Width = 41
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Situa'#231#227'o'
          end
          object lbDtFechamento: TLabel
            Left = 659
            Top = 8
            Width = 72
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Confirmado em'
          end
          object lbContagemCorreta: TLabel
            Left = 870
            Top = 7
            Width = 96
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Contagem Escolhida'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 45
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 52
          end
          object edDtCadastro: TgbDBDateEdit
            Left = 181
            Top = 4
            TabStop = False
            DataBinding.DataField = 'DH_CADASTRO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object descProcesso: TgbDBTextEdit
            Left = 422
            Top = 4
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'JOIN_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbReadyOnly = True
            gbRequired = True
            gbPassword = False
            Width = 98
          end
          object edtChaveProcesso: TgbDBTextEdit
            Left = 360
            Top = 4
            TabStop = False
            DataBinding.DataField = 'ID_CHAVE_PROCESSO'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            Width = 65
          end
          object edSituacao: TgbDBTextEdit
            Left = 569
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 4
            gbReadyOnly = True
            gbPassword = False
            Width = 86
          end
          object EdtDtFechamento: TgbDBDateEdit
            Left = 735
            Top = 4
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'DH_FECHAMENTO'
            DataBinding.DataSource = dsData
            Properties.DateButtons = [btnClear, btnNow]
            Properties.ImmediatePost = True
            Properties.Kind = ckDateTime
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 5
            gbReadyOnly = True
            gbDateTime = True
            Width = 130
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 6
            Transparent = True
            DesignSize = (
              997
              55)
            Height = 55
            Width = 997
            object Label1: TLabel
              Left = 6
              Top = 7
              Width = 25
              Height = 13
              Caption = 'In'#237'cio'
            end
            object Label2: TLabel
              Left = 182
              Top = 7
              Width = 16
              Height = 13
              Caption = 'Fim'
            end
            object Label3: TLabel
              Left = 518
              Top = 31
              Width = 82
              Height = 13
              Caption = 'C'#243'digo de Barras'
            end
            object Label4: TLabel
              Left = 338
              Top = 31
              Width = 43
              Height = 13
              Caption = 'Executar'
            end
            object lbMotivo: TLabel
              Left = 338
              Top = 7
              Width = 32
              Height = 13
              Caption = 'Motivo'
            end
            object gbDBDateEdit1: TgbDBDateEdit
              Left = 43
              Top = 3
              DataBinding.DataField = 'DH_INICIO'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnNow]
              Properties.ImmediatePost = True
              Properties.Kind = ckDateTime
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbDateTime = True
              Width = 130
            end
            object gbDBDateEdit2: TgbDBDateEdit
              Left = 203
              Top = 3
              DataBinding.DataField = 'DH_FIM'
              DataBinding.DataSource = dsData
              Properties.DateButtons = [btnClear, btnNow]
              Properties.ImmediatePost = True
              Properties.Kind = ckDateTime
              Style.BorderStyle = ebsOffice11
              Style.Color = clWhite
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbDateTime = True
              Width = 130
            end
            object rgTipoConferencia: TgbDBRadioGroup
              Left = 6
              Top = 25
              Hint = 
                'Na confer'#234'ncia MANUAL o produto poder'#225' ser conferido utilizando ' +
                'o leitor de c'#243'digo de barras ou digitando a quantidade conrrespo' +
                'ndente no campo da grade.'#13#10'Na confger'#234'ncia AUTOM'#193'TICA s'#243' ser'#225' pe' +
                'rmitido a utiliza'#231#227'o do leitor de c'#243'digo de barras para a confer' +
                #234'ncia dos produtos.'
              DataBinding.DataField = 'TIPO'
              DataBinding.DataSource = dsData
              Properties.Columns = 2
              Properties.Items = <
                item
                  Caption = 'Confer'#234'ncia Manual'
                  Value = 'MANUAL'
                end
                item
                  Caption = 'Confer'#234'ncia Autom'#225'tica'
                  Value = 'AUTOMATICA'
                end>
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 4
              Height = 24
              Width = 326
            end
            object EdtCodigoBarra: TcxTextEdit
              Left = 604
              Top = 27
              Anchors = [akLeft, akTop, akRight]
              ParentFont = False
              Properties.CharCase = ecUpperCase
              Style.Color = clInfoBk
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clBlack
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              TabOrder = 6
              OnExit = ConferirProduto
              Width = 388
            end
            object cbConferencia: TcxComboBox
              Left = 383
              Top = 27
              Properties.DropDownListStyle = lsEditFixedList
              Properties.ImmediatePost = True
              Properties.Items.Strings = (
                'Primeira Contagem'
                'Segunda Contagem'
                'Terceira Contagem')
              Properties.PostPopupValueOnTab = True
              TabOrder = 5
              Text = 'Primeira Contagem'
              Width = 130
            end
            object edtMotivo: TgbDBButtonEditFK
              Left = 383
              Top = 3
              DataBinding.DataField = 'ID_MOTIVO'
              DataBinding.DataSource = dsData
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.CharCase = ecUpperCase
              Properties.ClickKey = 13
              Properties.ReadOnly = False
              Style.Color = 14606074
              TabOrder = 2
              gbTextEdit = edtDescMotivo
              gbRequired = True
              gbCampoPK = 'ID'
              gbCamposRetorno = 'ID_MOTIVO;JOIN_DESCRICAO_MOTIVO'
              gbTableName = 'MOTIVO'
              gbCamposConsulta = 'ID;DESCRICAO'
              Width = 65
            end
            object edtDescMotivo: TgbDBTextEdit
              Left = 445
              Top = 3
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'JOIN_DESCRICAO_MOTIVO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Properties.ReadOnly = True
              Style.BorderStyle = ebsOffice11
              Style.Color = clGradientActiveCaption
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 3
              gbReadyOnly = True
              gbPassword = False
              Width = 547
            end
          end
          object PnProdutos: TgbPanel
            Left = 2
            Top = 89
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 7
            Transparent = True
            Height = 180
            Width = 997
            object gridInventario: TcxGrid
              Left = 2
              Top = 2
              Width = 993
              Height = 176
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              Images = DmAcesso.cxImage16x16
              ParentFont = False
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              LookAndFeel.NativeStyle = False
              object cxGridDBBandedTableView1: TcxGridDBBandedTableView
                PopupMenu = dxRibbonRadialMenu
                Navigator.Buttons.OnButtonClick = cxGridDBBandedTableView1NavigatorButtonsButtonClick
                Navigator.Buttons.CustomButtons = <
                  item
                    Hint = 'Realiza a impress'#227'o desta grade'
                    ImageIndex = 12
                  end
                  item
                    Hint = 'Exportar grade para excel'
                    ImageIndex = 274
                  end>
                Navigator.Buttons.Images = DmAcesso.cxImage16x16
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Insert.Visible = False
                Navigator.Buttons.Delete.Visible = False
                Navigator.Buttons.Edit.Visible = False
                Navigator.Buttons.Post.Visible = False
                Navigator.Buttons.Cancel.Visible = False
                Navigator.Buttons.Refresh.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.InfoPanel.Visible = True
                Navigator.Visible = True
                DataController.DataSource = dsInventarioProduto
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.Filter.Active = True
                DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                FilterRow.Visible = True
                FilterRow.ApplyChanges = fracImmediately
                Images = DmAcesso.cxImage16x16
                OptionsBehavior.NavigatorHints = True
                OptionsBehavior.ExpandMasterRowOnDblClick = False
                OptionsCustomize.ColumnMoving = False
                OptionsCustomize.BandMoving = False
                OptionsCustomize.NestedBands = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.ShowColumnFilterButtons = sfbAlways
                OptionsView.BandHeaders = False
                Styles.ContentOdd = DmAcesso.OddColor
                Bands = <
                  item
                    Caption = 'Invent'#225'rio'
                  end>
                object cxGridDBBandedTableView1ID_PRODUTO: TcxGridDBBandedColumn
                  Caption = 'C'#243'digo'
                  DataBinding.FieldName = 'ID_PRODUTO'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 64
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1QUANTIDADE_ESTOQUE: TcxGridDBBandedColumn
                  Caption = 'Estoque'
                  DataBinding.FieldName = 'QUANTIDADE_ESTOQUE'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 83
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1PRIMEIRA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '1'#170' Contagem'
                  DataBinding.FieldName = 'PRIMEIRA_CONTAGEM'
                  PropertiesClassName = 'TcxCalcEditProperties'
                  Properties.DisplayFormat = '###,###,###,##0.00'
                  Properties.UseThousandSeparator = True
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 95
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1CC_DIFERENCA_PRIMEIRA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '1'#170' Diferen'#231'a'
                  DataBinding.FieldName = 'CC_DIFERENCA_PRIMEIRA_CONTAGEM'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 87
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1SEGUNDA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '2'#170' Contagem'
                  DataBinding.FieldName = 'SEGUNDA_CONTAGEM'
                  PropertiesClassName = 'TcxCalcEditProperties'
                  Properties.DisplayFormat = '###,###,###,##0.00'
                  Properties.UseThousandSeparator = True
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 95
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1CC_DIFERENCA_SEGUNDA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '2'#170' Diferen'#231'a'
                  DataBinding.FieldName = 'CC_DIFERENCA_SEGUNDA_CONTAGEM'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 86
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1TERCEIRA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '3'#170' Contagem'
                  DataBinding.FieldName = 'TERCEIRA_CONTAGEM'
                  PropertiesClassName = 'TcxCalcEditProperties'
                  Properties.DisplayFormat = '###,###,###,##0.00'
                  Properties.UseThousandSeparator = True
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 96
                  Position.BandIndex = 0
                  Position.ColIndex = 8
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1CC_DIFERENCA_TERCEIRA_CONTAGEM: TcxGridDBBandedColumn
                  Caption = '3'#170' Diferen'#231'a'
                  DataBinding.FieldName = 'CC_DIFERENCA_TERCEIRA_CONTAGEM'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 88
                  Position.BandIndex = 0
                  Position.ColIndex = 9
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1JOIN_DESCRICAO_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_DESCRICAO_PRODUTO'
                  Options.Editing = False
                  Options.Moving = False
                  SortIndex = 0
                  SortOrder = soAscending
                  Width = 165
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGridDBBandedTableView1JOIN_CODIGO_BARRA_PRODUTO: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
                  Options.Editing = False
                  Options.AutoWidthSizable = False
                  Options.Moving = False
                  Width = 118
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = cxGridDBBandedTableView1
              end
            end
          end
          object EdtContagemCorreta: TgbDBTextEdit
            Left = 970
            Top = 3
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'CONTAGEM_ESCOLHIDA'
            DataBinding.DataSource = dsData
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.LookAndFeel.Kind = lfOffice11
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 8
            gbReadyOnly = True
            gbPassword = False
            Width = 24
          end
        end
      end
    end
  end
  inherited ActionListAssistent: TActionList
    object ActReiniciarPrimeiraContagem: TAction
      Category = 'Inventario'
      Caption = 'Reiniciar 1'#170' Contagem'
      Hint = 'Limpar toda a confer'#234'ncia da primeira contagem'
      ImageIndex = 55
      OnExecute = ActReiniciarPrimeiraContagemExecute
    end
    object ActReiniciarSegundaContagem: TAction
      Category = 'Inventario'
      Caption = 'Reiniciar 2'#170' Contagem'
      Hint = 'Limpar toda a confer'#234'ncia da segunda contagem'
      ImageIndex = 55
      OnExecute = ActReiniciarSegundaContagemExecute
    end
    object ActReiniciarTerceiraContagem: TAction
      Category = 'Inventario'
      Caption = 'Reiniciar 3'#170' Contagem'
      Hint = 'Limpar toda a confer'#234'ncia da terceira contagem'
      ImageIndex = 55
      OnExecute = ActReiniciarTerceiraContagemExecute
    end
    object ActExcluirTodaConferencia: TAction
      Category = 'Inventario'
      Caption = 'Remover Todos Produtos'
      Hint = 'Remover todos os produtos deste invent'#225'rio'
      ImageIndex = 21
      OnExecute = ActExcluirTodaConferenciaExecute
    end
    object ActBuscarProdutoSelecionado: TAction
      Category = 'Inventario'
      Caption = 'Explorar Registro Selecionado'
      ImageIndex = 11
      OnExecute = ActBuscarProdutoSelecionadoExecute
    end
    object ActBuscarTodosProdutos: TAction
      Category = 'Inventario'
      Caption = 'Explorar Todos os Registros'
      ImageIndex = 11
      OnExecute = ActBuscarTodosProdutosExecute
    end
    object ActEstornarContagem: TAction
      Category = 'Inventario'
      Caption = 'Estornar Contagem do Registro Selecionado'
      Hint = 'Remove uma contagem do registro selecionado'
      ImageIndex = 51
      OnExecute = ActEstornarContagemExecute
    end
    object ActExcluirProdutoConferencia: TAction
      Category = 'Inventario'
      Caption = 'Excluir o Registro Selecionado'
      ImageIndex = 52
      OnExecute = ActExcluirProdutoConferenciaExecute
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 191
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end
        item
          Visible = True
          ItemName = 'bbEfetivar'
        end
        item
          Visible = True
          ItemName = 'bbReabrir'
        end
        item
          Visible = True
          ItemName = 'bbIncluirProdutos'
        end>
    end
    inherited dxBarReport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 651
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 822
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 576
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 730
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
    object bbEfetivar: TdxBarLargeButton [10]
      Action = ActEfetivar
      Category = 0
    end
    object bbReabrir: TdxBarLargeButton [11]
      Action = ActReabrir
      Category = 0
    end
    object bbIncluirProdutos: TdxBarLargeButton [12]
      Action = ActBuscarProdutos
      Category = 0
    end
    object dxBarButton2: TdxBarButton [13]
      Action = ActReiniciarPrimeiraContagem
      Category = 0
    end
    object dxBarButton3: TdxBarButton [14]
      Action = ActBuscarTodosProdutos
      Category = 0
    end
    object dxBarButton4: TdxBarButton [15]
      Action = ActReiniciarSegundaContagem
      Category = 0
    end
    object dxBarButton5: TdxBarButton [16]
      Action = ActReiniciarTerceiraContagem
      Category = 0
    end
    object dxBarButton6: TdxBarButton [17]
      Action = ActExcluirTodaConferencia
      Category = 0
    end
    object dxBarButton7: TdxBarButton [18]
      Action = ActBuscarProdutoSelecionado
      Category = 0
    end
    object dxBarButton8: TdxBarButton [19]
      Action = ActEstornarContagem
      Category = 0
    end
    object dxBarButton9: TdxBarButton [20]
      Action = ActExcluirProdutoConferencia
      Category = 0
    end
  end
  inherited ActionListMain: TActionList
    object ActReabrir: TAction [1]
      Category = 'Inventario'
      Caption = 'Reabrir F10'
      Hint = 'Reabrir o ajuste de estoque'
      ImageIndex = 97
      ShortCut = 121
      OnExecute = ActReabrirExecute
    end
    object ActEfetivar: TAction
      Category = 'Inventario'
      Caption = 'Efetivar F9'
      Hint = 'Efetivar o ajuste de estoque'
      ImageIndex = 92
      ShortCut = 120
      OnExecute = ActEfetivarExecute
    end
    object ActBuscarProdutos: TAction
      Category = 'Inventario'
      Caption = 'Adicionar Produtos no Invent'#225'rio'
      Hint = 'Incluir produtos neste invent'#225'rio'
      ImageIndex = 131
      OnExecute = ActBuscarProdutosExecute
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspAjusteEstoque'
    RemoteServer = DmConnection.dspInventario
    OnNewRecord = cdsDataNewRecord
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDH_CADASTRO: TDateTimeField
      DisplayLabel = 'Data e Hora do Cadastro'
      FieldName = 'DH_CADASTRO'
      Origin = 'DH_CADASTRO'
      Required = True
    end
    object cdsDataTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 15
    end
    object cdsDataID_FILIAL: TIntegerField
      DisplayLabel = 'C'#243'digo da Filial'
      FieldName = 'ID_FILIAL'
      Origin = 'ID_FILIAL'
      Required = True
    end
    object cdsDataID_PESSOA_USUARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Usu'#225'rio'
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDataID_CHAVE_PROCESSO: TIntegerField
      DisplayLabel = 'C'#243'digo da Chave de Processo'
      FieldName = 'ID_CHAVE_PROCESSO'
      Origin = 'ID_CHAVE_PROCESSO'
      Required = True
    end
    object cdsDataDH_INICIO: TDateTimeField
      DisplayLabel = 'Data e Hora do In'#237'cio do Invent'#225'rio'
      FieldName = 'DH_INICIO'
      Origin = 'DH_INICIO'
    end
    object cdsDataDH_FIM: TDateTimeField
      DisplayLabel = 'Data e Hora do Fim do Invent'#225'rio'
      FieldName = 'DH_FIM'
      Origin = 'DH_FIM'
    end
    object cdsDataOBSERVACAO: TBlobField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
    end
    object cdsDataJOIN_CHAVE_PROCESSO: TStringField
      DisplayLabel = 'Processo'
      FieldName = 'JOIN_CHAVE_PROCESSO'
      Origin = 'ORIGEM'
      ProviderFlags = []
      Size = 100
    end
    object cdsDataJOIN_USUARIO: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'JOIN_USUARIO'
      Origin = 'USUARIO'
      ProviderFlags = []
      Size = 50
    end
    object cdsDataJOIN_FANTASIA_FILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'JOIN_FANTASIA_FILIAL'
      Origin = 'FANTASIA'
      ProviderFlags = []
      Size = 80
    end
    object cdsDataCONTAGEM_ESCOLHIDA: TIntegerField
      DisplayLabel = 'Contagem Escolhida'
      FieldName = 'CONTAGEM_ESCOLHIDA'
      Origin = 'CONTAGEM_ESCOLHIDA'
    end
    object cdsDataSTATUS: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
      Origin = '`STATUS`'
      Size = 15
    end
    object cdsDataDH_FECHAMENTO: TDateTimeField
      DisplayLabel = 'Data e Hora do Fechamento'
      FieldName = 'DH_FECHAMENTO'
      Origin = 'DH_FECHAMENTO'
    end
    object cdsDataID_MOTIVO: TIntegerField
      DisplayLabel = 'C'#243'digo do Motivo'
      FieldName = 'ID_MOTIVO'
      Origin = 'ID_MOTIVO'
      Required = True
    end
    object cdsDataJOIN_DESCRICAO_MOTIVO: TStringField
      DisplayLabel = 'Motivo'
      FieldName = 'JOIN_DESCRICAO_MOTIVO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 255
    end
    object cdsDatafdqInventarioProduto: TDataSetField
      FieldName = 'fdqInventarioProduto'
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42209.904121053240000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
  object cdsInventarioProduto: TGBClientDataSet
    Aggregates = <>
    DataSetField = cdsDatafdqInventarioProduto
    Params = <>
    AfterPost = cdsInventarioProdutoAfterPost
    AfterDelete = cdsInventarioProdutoAfterDelete
    OnCalcFields = cdsInventarioProdutoCalcFields
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 792
    Top = 80
    object cdsInventarioProdutoID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsInventarioProdutoID_INVENTARIO: TIntegerField
      DisplayLabel = 'C'#243'digo do Invent'#225'rio'
      FieldName = 'ID_INVENTARIO'
      Origin = 'ID_INVENTARIO'
    end
    object cdsInventarioProdutoID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo do Produto'
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object cdsInventarioProdutoQUANTIDADE_ESTOQUE: TFMTBCDField
      DisplayLabel = 'Quantidade em Estoque'
      FieldName = 'QUANTIDADE_ESTOQUE'
      Origin = 'QUANTIDADE_ESTOQUE'
      Precision = 24
      Size = 9
    end
    object cdsInventarioProdutoPRIMEIRA_CONTAGEM: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Primeira Contagem'
      FieldName = 'PRIMEIRA_CONTAGEM'
      Origin = 'PRIMEIRA_CONTAGEM'
      Precision = 24
      Size = 9
    end
    object cdsInventarioProdutoSEGUNDA_CONTAGEM: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Segunda Contagem'
      FieldName = 'SEGUNDA_CONTAGEM'
      Origin = 'SEGUNDA_CONTAGEM'
      Precision = 24
      Size = 9
    end
    object cdsInventarioProdutoTERCEIRA_CONTAGEM: TFMTBCDField
      DefaultExpression = '0'
      DisplayLabel = 'Terceira Contagem'
      FieldName = 'TERCEIRA_CONTAGEM'
      Origin = 'TERCEIRA_CONTAGEM'
      Precision = 24
      Size = 9
    end
    object cdsInventarioProdutoJOIN_DESCRICAO_PRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'JOIN_DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
    object cdsInventarioProdutoJOIN_CODIGO_BARRA_PRODUTO: TStringField
      DisplayLabel = 'C'#243'digo de Barras'
      FieldName = 'JOIN_CODIGO_BARRA_PRODUTO'
      Origin = 'CODIGO_BARRA'
      ProviderFlags = []
      Size = 30
    end
    object cdsInventarioProdutoCC_DIFERENCA_PRIMEIRA_CONTAGEM: TFloatField
      DisplayLabel = 'Diferen'#231'a 1'#170' Contagem'
      FieldKind = fkCalculated
      FieldName = 'CC_DIFERENCA_PRIMEIRA_CONTAGEM'
      Calculated = True
    end
    object cdsInventarioProdutoCC_DIFERENCA_SEGUNDA_CONTAGEM: TFloatField
      DisplayLabel = 'Diferen'#231'a 2'#170' Contagem'
      FieldKind = fkCalculated
      FieldName = 'CC_DIFERENCA_SEGUNDA_CONTAGEM'
      Calculated = True
    end
    object cdsInventarioProdutoCC_DIFERENCA_TERCEIRA_CONTAGEM: TFloatField
      DisplayLabel = 'Diferen'#231'a 3'#170' Contagem'
      FieldKind = fkCalculated
      FieldName = 'CC_DIFERENCA_TERCEIRA_CONTAGEM'
      Calculated = True
    end
  end
  object dsInventarioProduto: TDataSource
    DataSet = cdsInventarioProduto
    Left = 820
    Top = 80
  end
  object jvdaAlertaConferencia: TJvDesktopAlert
    Location.Position = dapTopRight
    Location.Top = 0
    Location.Left = 0
    Location.Width = 0
    Location.Height = 0
    HeaderText = 'Confer'#234'ncia de Produto'
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'Segoe UI'
    HeaderFont.Style = [fsBold]
    ShowHint = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Buttons = <>
    Images = DmAcesso.cxImage32x32
    Left = 736
    Top = 80
  end
  object dxRibbonRadialMenu: TdxRibbonRadialMenu
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
        ItemName = 'dxBarButton2'
      end
      item
        Visible = True
        ItemName = 'dxBarButton4'
      end
      item
        Visible = True
        ItemName = 'dxBarButton5'
      end
      item
        Visible = True
        ItemName = 'dxBarButton6'
      end
      item
        Visible = True
        ItemName = 'dxBarButton7'
      end
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end
      item
        Visible = True
        ItemName = 'dxBarButton8'
      end
      item
        Visible = True
        ItemName = 'dxBarButton9'
      end>
    Images = DmAcesso.cxImage16x16
    Ribbon = dxMainRibbon
    UseOwnFont = False
    Left = 608
    Top = 72
  end
  object JvEnterAsTab: TJvEnterAsTab
    Left = 496
    Top = 280
  end
end
