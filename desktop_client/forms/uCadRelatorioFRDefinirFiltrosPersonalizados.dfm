inherited CadRelatorioFrDefinirFiltrosPersonalizados: TCadRelatorioFrDefinirFiltrosPersonalizados
  Caption = 'Definir Personaliza'#231#245'es dos Filtros'
  ExplicitWidth = 664
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    object cxgdMain: TcxGrid
      Left = 2
      Top = 2
      Width = 654
      Height = 265
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object ViewDefinicaoCampos: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsColunas
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsCustomize.DataRowSizing = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.Indicator = True
        OptionsView.ShowColumnFilterButtons = sfbAlways
        object ViewDefinicaoCamposDESCRICAO: TcxGridDBColumn
          DataBinding.FieldName = 'DESCRICAO'
          Options.Editing = False
          Width = 298
        end
        object ViewDefinicaoCamposVISIVEL: TcxGridDBColumn
          DataBinding.FieldName = 'VISIVEL'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Width = 58
        end
        object ViewDefinicaoCamposOBRIGATORIO: TcxGridDBColumn
          DataBinding.FieldName = 'OBRIGATORIO'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Width = 82
        end
      end
      object cxgdMainLevel1: TcxGridLevel
        GridView = ViewDefinicaoCampos
      end
    end
  end
  object cdsColunas: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 300
    Top = 144
    Data = {
      B20000009619E0BD010000001800000006000000000003000000B20006544142
      454C41020049000000010005574944544802000200FF000543414D504F020049
      000000010005574944544802000200FF00075649534956454C02000300000000
      000C56414C4F525F50414452414F020049000000010005574944544802000200
      FF000B4F4252494741544F52494F02000300000000000944455343524943414F
      020049000000010005574944544802000200FF000000}
    object cdsColunasTABELA: TStringField
      DisplayLabel = 'Tabela'
      FieldName = 'TABELA'
      Size = 255
    end
    object cdsColunasCAMPO: TStringField
      DisplayLabel = 'Campo'
      FieldName = 'CAMPO'
      Size = 255
    end
    object cdsColunasVISIVEL: TBooleanField
      DisplayLabel = 'Vis'#237'vel'
      FieldName = 'VISIVEL'
    end
    object cdsColunasVALOR_PADRAO: TStringField
      DisplayLabel = 'Valor'
      FieldName = 'VALOR_PADRAO'
      Size = 255
    end
    object cdsColunasOBRIGATORIO: TBooleanField
      DisplayLabel = 'Obrigat'#243'rio'
      FieldName = 'OBRIGATORIO'
    end
    object cdsColunasDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 255
    end
  end
  object dsColunas: TDataSource
    DataSet = cdsColunas
    Left = 272
    Top = 144
  end
end
