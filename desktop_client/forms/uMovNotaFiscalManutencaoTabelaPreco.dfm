inherited MovNotaFiscalManutencaoTabelaPreco: TMovNotaFiscalManutencaoTabelaPreco
  Caption = 'Manuten'#231#227'o da Tabela de Pre'#231'o para os Itens da Nota Fiscal'
  ExplicitWidth = 664
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    ParentBackground = False
    object cxGridPesquisaPadrao: TcxGrid
      Left = 2
      Top = 33
      Width = 654
      Height = 234
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Images = DmAcesso.cxImage16x16
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      object Level1BandedTableView1: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = DmAcesso.cxImage16x16
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        DataController.DataSource = dsTabelaPrecoItem
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.ApplyChanges = fracImmediately
        Images = DmAcesso.cxImage16x16
        OptionsBehavior.NavigatorHints = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandMoving = False
        OptionsCustomize.NestedBands = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.ShowColumnFilterButtons = sfbAlways
        OptionsView.BandHeaders = False
        Styles.ContentOdd = DmAcesso.OddColor
        Bands = <
          item
          end>
        object Level1BandedTableView1ID_PRODUTO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID_PRODUTO'
          Options.Editing = False
          Width = 106
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object Level1BandedTableView1DESCRICAO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'DESCRICAO'
          Options.Editing = False
          Width = 189
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object Level1BandedTableView1VL_CUSTO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VL_CUSTO'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.AssignedValues.MinValue = True
          Properties.MaxValue = 1E21
          Options.Editing = False
          Width = 136
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object Level1BandedTableView1PERC_LUCRO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PERC_LUCRO'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.AssignedValues.MinValue = True
          Properties.MaxValue = 1E21
          Width = 77
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object Level1BandedTableView1VL_VENDA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VL_VENDA'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.AssignedValues.MinValue = True
          Properties.MaxValue = 1E21
          Width = 132
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object cxGridPesquisaPadraoLevel1: TcxGridLevel
        GridView = Level1BandedTableView1
      end
    end
    object gbPanel1: TgbPanel
      Left = 2
      Top = 2
      Align = alTop
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Transparent = True
      DesignSize = (
        654
        31)
      Height = 31
      Width = 654
      object Label8: TLabel
        Left = 6
        Top = 8
        Width = 77
        Height = 13
        Caption = 'Tabela de Pre'#231'o'
      end
      object gbDBButtonEditFK2: TgbDBButtonEditFK
        Left = 86
        Top = 4
        DataBinding.DataField = 'ID_TABELA_PRECO'
        DataBinding.DataSource = dsTabelaPreco
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.CharCase = ecUpperCase
        Properties.ClickKey = 13
        Properties.ReadOnly = False
        Style.Color = 14606074
        TabOrder = 0
        gbTextEdit = gbDBTextEdit2
        gbRequired = True
        gbCampoPK = 'ID'
        gbCamposRetorno = 'ID_TABELA_PRECO;DESCRICAO_TABELA_PRECO'
        gbTableName = 'TABELA_PRECO'
        gbCamposConsulta = 'ID;DESCRICAO'
        gbIdentificadorConsulta = 'TABELA_PRECO'
        Width = 60
      end
      object gbDBTextEdit2: TgbDBTextEdit
        Left = 143
        Top = 4
        TabStop = False
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'DESCRICAO_TABELA_PRECO'
        DataBinding.DataSource = dsTabelaPreco
        Properties.CharCase = ecUpperCase
        Properties.ReadOnly = True
        Style.BorderStyle = ebsOffice11
        Style.Color = clGradientActiveCaption
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        gbReadyOnly = True
        gbPassword = False
        Width = 507
      end
    end
  end
  object dsTabelaPrecoItem: TDataSource
    DataSet = fdmTabelaPrecoItem
    Left = 478
    Top = 72
  end
  object fdmTabelaPrecoItem: TFDMemTable
    FieldOptions.PositionMode = poFirst
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 454
    Top = 72
    object fdmTabelaPrecoItemID_PRODUTO: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID_PRODUTO'
    end
    object fdmTabelaPrecoItemDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 255
    end
    object fdmTabelaPrecoItemVL_CUSTO: TFloatField
      DisplayLabel = 'Vl. Custo'
      FieldName = 'VL_CUSTO'
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object fdmTabelaPrecoItemPERC_LUCRO: TFloatField
      DisplayLabel = '% Lucro'
      FieldName = 'PERC_LUCRO'
      OnChange = CalcularValor
      DisplayFormat = '###,###,###,###,##0.00'
    end
    object fdmTabelaPrecoItemVL_VENDA: TFloatField
      DisplayLabel = 'Vl. Venda'
      FieldName = 'VL_VENDA'
      OnChange = CalcularPercentual
      DisplayFormat = '###,###,###,###,##0.00'
    end
  end
  object fdmTabelaPreco: TFDMemTable
    FieldOptions.PositionMode = poFirst
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 302
    Top = 64
    object fdmTabelaPrecoID_TABELA_PRECO: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID_TABELA_PRECO'
    end
    object fdmTabelaPrecoDESCRICAO_TABELA_PRECO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO_TABELA_PRECO'
      Size = 100
    end
  end
  object dsTabelaPreco: TDataSource
    DataSet = fdmTabelaPreco
    Left = 331
    Top = 64
  end
end
