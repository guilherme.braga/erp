inherited FrmHelpImplantacaoOrdemServico: TFrmHelpImplantacaoOrdemServico
  Caption = 'Help Implanta'#231#227'o - Ordem de Servi'#231'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    inherited Memo1: TMemo
      Lines.Strings = (
        'CONFIGURA'#199#195'O OBRIGAT'#211'RIA:'
        ''
        'Atrav'#233's dos par'#226'metros de tela configure:'
        
          '- PARAMETRO_FORMA_PAGAMENTO_AVISTA: FORMA DE PAGAMENTO QUE SER'#193' ' +
          'UTILIZADA QUANDO '
        'HOUVER ENTRADA OU '
        'QUITA'#199#195'O EM DINHEIRO.'
        
          '- PARAMETRO_PLANO_PAGAMENTO_AVISTA: PLANO DE PAGAMENTO QUE SER'#193' ' +
          'UTILIZADA QUANDO HOUVER '
        'ENTRADA OU '
        'QUITA'#199#195'O EM DINHEIRO.'
        
          '- PARAMETRO_FORMA_PAGAMENTO_CREDIARIO: FORMA DE PAGAMENTO QUE SE' +
          'R'#193' UTILIZADA PARA '
        'GERA'#199#195'O DAS PARCELAS '
        'DE CREDI'#193'RIO.'
        
          '- PARAMETRO_CONTA_CORRENTE: CONTA CORRENTE EM QUE SER'#193' GERADO O ' +
          'REGISTRO REFERENTE A '
        'QUITA'#199#195'O DE CR OU '
        'CP.'
        
          '- PARAMETRO_CENTRO_RESULTADO_AVISTA: CENTRO DE RESULTADO DE FORM' +
          'A DE PAGAMENTO A VISTA.'
        
          '- PARAMETRO_CENTRO_RESULTADO_APRAZO: CENTRO DE RESULTADO DE FORM' +
          'A DE PAGAMENTO A PRAZO.'
        
          '- PARAMETRO_CONTA_ANALISE_AVISTA: CONTA DE ANALISE DE FORMA DE P' +
          'AGAMENTO A VISTA.'
        
          '- PARAMETRO_CONTA_ANALISE_APRAZO: CONTA DE ANALISE DE FORMA DE P' +
          'AGAMENTO A PRAZO.    '
        
          '- PARAMETRO_CARTEIRA: CARTEIRA QUE SER'#193' USADA PARA GERA'#199#195'O DO CO' +
          'NTA A RECEBER E '
        'ENTRADA/QUITACAO.'
        
          '- PARAMETRO_EXIBIR_EMISSAO_DOCUMENTO_AO_EFETIVAR: '#39'EXIBIR TELA D' +
          'E EMISS'#195'O DE DOCUMENTO AO '
        'EFETIVAR A ORDEM DE SERVI'#199'O.'
        ''
        
          '----------------------------------------------------------------' +
          '--'
        ''
        'CONFIGURA'#199#213'ES DOS RELAT'#211'RIOS E IMPRESS'#213'ES:'
        ''
        
          'Para configurar impress'#245'es para os bot'#245'es de carne, duplicata, n' +
          'fe, nfce e comanda nessa tela utilize o '
        'Gerenciador de Relat'#243'rios, '
        'sendo que o formul'#225'rio ao qual o relat'#243'rio ser'#225' associado ser'#225':'
        '- MovOrdemServicoEmitirDocumentoDuplicata (para as duplicatas)'
        '- MovOrdemServicoEmitirDocumentoComanda (para as comandas)'
        '- MovOrdemServicoEmitirDocumentoCarne (para os carnes)'
        '- MovOrdemServicoEmitirDocumentoNFE (para a NFE)'
        '- MovOrdemServicoEmitirDocumentoNFCE (para o NFCE)')
    end
  end
end
