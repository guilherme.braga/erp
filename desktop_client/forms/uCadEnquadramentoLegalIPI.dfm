inherited CadEnquadramentoLegalIPI: TCadEnquadramentoLegalIPI
  Caption = 'Cadastro de Enquadramento Legal do IPI'
  ClientHeight = 407
  ClientWidth = 909
  ExplicitWidth = 925
  ExplicitHeight = 446
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 909
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 909
    Height = 280
    Properties.ActivePage = cxtsData
    ClientRectBottom = 280
    ClientRectRight = 909
    inherited cxtsSearch: TcxTabSheet
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 877
        Height = 256
      end
      inherited pnFiltroVertical: TgbPanel
        Height = 256
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 256
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        Width = 909
        Height = 256
        inherited cxGBDadosMain: TcxGroupBox
          Height = 256
          Width = 909
          inherited dxBevel1: TdxBevel
            Width = 905
          end
          object Label2: TLabel
            Left = 12
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 90
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbPanel1: TgbPanel
            Left = 2
            Top = 34
            Align = alTop
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            Transparent = True
            Height = 28
            Width = 905
            object Label3: TLabel
              Left = 10
              Top = 7
              Width = 76
              Height = 13
              Caption = 'Enquadramento'
            end
            object Label4: TLabel
              Left = 199
              Top = 7
              Width = 29
              Height = 13
              Caption = 'Grupo'
            end
            object edDescricao: TgbDBTextEdit
              Left = 88
              Top = 3
              DataBinding.DataField = 'CODIGO'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              gbRequired = True
              gbPassword = False
              Width = 105
            end
            object gbDBTextEdit2: TgbDBTextEdit
              Left = 232
              Top = 3
              DataBinding.DataField = 'GRUPO_CST'
              DataBinding.DataSource = dsData
              Properties.CharCase = ecUpperCase
              Style.BorderStyle = ebsOffice11
              Style.Color = 14606074
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              gbRequired = True
              gbPassword = False
              Width = 668
            end
          end
          object gbPanel2: TgbPanel
            Left = 2
            Top = 62
            Align = alClient
            PanelStyle.Active = True
            PanelStyle.OfficeBackgroundKind = pobkGradient
            Style.BorderStyle = ebsNone
            Style.LookAndFeel.Kind = lfOffice11
            Style.Shadow = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            Transparent = True
            ExplicitTop = 64
            Height = 192
            Width = 905
            object PnTituloFrameDetailPadrao: TgbPanel
              Left = 2
              Top = 2
              Align = alTop
              Alignment = alCenterCenter
              Caption = 'Descri'#231#227'o'
              PanelStyle.Active = True
              PanelStyle.OfficeBackgroundKind = pobkGradient
              ParentBackground = False
              ParentFont = False
              Style.BorderStyle = ebsOffice11
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.Kind = lfOffice11
              Style.Shadow = False
              Style.TextStyle = [fsBold]
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 0
              Transparent = True
              ExplicitLeft = 4
              ExplicitTop = 10
              Height = 19
              Width = 901
            end
            object gbDBMemo1: TgbDBMemo
              AlignWithMargins = True
              Left = 5
              Top = 24
              Align = alClient
              DataBinding.DataField = 'DESCRICAO'
              DataBinding.DataSource = dsData
              Style.BorderStyle = ebsOffice11
              Style.LookAndFeel.Kind = lfOffice11
              StyleDisabled.LookAndFeel.Kind = lfOffice11
              StyleFocused.LookAndFeel.Kind = lfOffice11
              StyleHot.LookAndFeel.Kind = lfOffice11
              TabOrder = 1
              ExplicitLeft = 7
              ExplicitTop = 32
              ExplicitHeight = 157
              Height = 163
              Width = 895
            end
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspEnquadramentoLegaIPI'
    RemoteServer = DmConnection.dspCSTIPI
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataCODIGO: TStringField
      DisplayLabel = 'C'#243'digo Enquadramento'
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Required = True
      Size = 3
    end
    object cdsDataGRUPO_CST: TStringField
      DisplayLabel = 'Grupo CST'
      FieldName = 'GRUPO_CST'
      Origin = 'GRUPO_CST'
      Required = True
      Size = 255
    end
    object cdsDataDESCRICAO: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42357.569519745370000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
