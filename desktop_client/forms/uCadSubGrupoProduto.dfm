inherited CadSubGrupoProduto: TCadSubGrupoProduto
  Caption = 'Cadastro de Sub Grupo de Produto'
  ClientWidth = 987
  ExplicitWidth = 1003
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 987
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 987
    Properties.ActivePage = cxtsData
    ClientRectRight = 987
    inherited cxtsSearch: TcxTabSheet
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 987
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        Width = 987
        inherited cxGBDadosMain: TcxGroupBox
          Width = 987
          inherited dxBevel1: TdxBevel
            Width = 983
          end
          object Label6: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object Label1: TLabel
            Left = 542
            Top = 39
            Width = 29
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Grupo'
            ExplicitLeft = 491
          end
          object gbDBTextEditPK1: TgbDBTextEditPK
            Left = 68
            Top = 5
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 58
            Top = 36
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            ExplicitWidth = 429
            Width = 480
          end
          object descGrupo: TgbDBTextEdit
            Left = 632
            Top = 36
            TabStop = False
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbReadyOnly = True
            gbPassword = False
            ExplicitLeft = 581
            Width = 346
          end
          object eFkGrupo: TgbDBButtonEditFK
            Left = 575
            Top = 36
            Anchors = [akTop, akRight]
            DataBinding.DataField = 'ID_GRUPO_PRODUTO'
            DataBinding.DataSource = dsData
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.CharCase = ecUpperCase
            Properties.ClickKey = 13
            Style.Color = 14606074
            TabOrder = 2
            gbTextEdit = descGrupo
            gbRequired = True
            gbCampoPK = 'ID'
            gbCamposRetorno = 'ID_GRUPO_PRODUTO;JOIN_DESCRICAO_GRUPO_PRODUTO'
            gbTableName = 'GRUPO_PRODUTO'
            gbCamposConsulta = 'ID;DESCRICAO'
            ExplicitLeft = 524
            Width = 60
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Sub Grupo de Produto'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspSubGrupo'
    RemoteServer = DmConnection.dspProduto
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 80
    end
    object cdsDataID_GRUPO_PRODUTO: TIntegerField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'ID_GRUPO_PRODUTO'
      Origin = 'ID_GRUPO_PRODUTO'
    end
    object cdsDataJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField
      DisplayLabel = 'Grupo de Produto'
      FieldName = 'JOIN_DESCRICAO_GRUPO_PRODUTO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      Size = 80
    end
  end
end
