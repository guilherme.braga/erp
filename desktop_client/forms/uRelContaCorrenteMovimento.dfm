inherited RelContaCorrenteMovimento: TRelContaCorrenteMovimento
  Caption = 'Relat'#243'rio de Movimenta'#231#227'o de Conta Corrente'
  ClientHeight = 404
  ClientWidth = 947
  ExplicitWidth = 963
  ExplicitHeight = 443
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 947
    ExplicitWidth = 947
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    ExplicitWidth = 947
    ExplicitHeight = 302
    Height = 302
    Width = 947
    inherited cxGridRelatorios: TcxGrid
      Width = 562
      Height = 292
      ExplicitWidth = 562
      ExplicitHeight = 292
    end
    inherited gbPanel1: TgbPanel
      ExplicitHeight = 298
      Height = 298
      inherited vgFiltros: TcxVerticalGrid
        Height = 294
        OptionsView.RowHeaderWidth = 212
        ExplicitHeight = 294
        Version = 1
        inherited vcatFiltrosPadrao: TcxCategoryRow
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object rowCentroResultado: TcxEditorRow
          Properties.Caption = 'Centro de Resultado'
          Properties.Hint = 'ID_CENTRO_RESULTADO'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = vgFiltrosEditorRow1EditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object rowContaAnalise: TcxEditorRow
          Properties.Caption = 'Conta de An'#225'lise'
          Properties.Hint = 'ID_CONTA_ANALISE'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = vgFiltrosEditorRow3EditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object rowContaCorrente: TcxEditorRow
          Properties.Caption = 'Conta Corrente'
          Properties.Hint = 'ID_CONTA_CORRENTE'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = vgFiltrosEditorRow2EditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 84
    Top = 211
  end
  inherited dxBarManager: TdxBarManager
    Left = 56
    Top = 211
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited BarFiltro: TdxBar
      DockedDockingStyle = dsNone
      FloatClientWidth = 51
      FloatClientHeight = 22
    end
    inherited filtroDataInicio: TcxBarEditItem
      Properties.ImmediatePost = False
      Properties.PostPopupValueOnTab = False
      Properties.SaveTime = True
      Properties.ShowTime = True
    end
    inherited filtroDataTermino: TcxBarEditItem
      Properties.ImmediatePost = False
      Properties.PostPopupValueOnTab = False
      Properties.SaveTime = True
      Properties.ShowTime = True
    end
    inherited filtroConciliado: TcxBarEditItem
      Properties.DropDownListStyle = lsEditList
      Properties.ImmediatePost = False
      Properties.Items.Strings = ()
      Properties.PostPopupValueOnTab = False
    end
    inherited filtroTipoMovimento: TcxBarEditItem
      Properties.DropDownListStyle = lsEditList
      Properties.ImmediatePost = False
      Properties.Items.Strings = ()
      Properties.PostPopupValueOnTab = False
    end
  end
  inherited dsConsultaRelatorioFR: TDataSource
    Left = 276
    Top = 272
  end
  inherited cdsConsultaRelatorioFR: TGBClientDataSet
    Left = 248
    Top = 208
  end
  inherited FdmFiltros: TFDMemTable
    Left = 160
    Top = 248
    object FdmFiltrosID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object FdmFiltrosDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 50
    end
    object fdmFiltrosDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 255
    end
    object fdmFiltrosDT_MOVIMENTO_INICIO: TDateField
      DisplayLabel = 'Dt. Movimento - In'#237'cio'
      FieldName = 'DT_MOVIMENTO_INICIO'
      Origin = 'DT_MOVIMENTO'
    end
    object FdmFiltrosDT_MOVIMENTO_TERMINO: TDateField
      DisplayLabel = 'Dt. Movimento - T'#233'rmino'
      FieldName = 'DT_MOVIMENTO_TERMINO'
      Origin = 'DT_MOVIMENTO'
    end
    object FdmFiltrosDT_COMPETENCIA_INICIO: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia - In'#237'cio'
      FieldName = 'DT_COMPETENCIA_INICIO'
      Origin = 'DT_COMPETENCIA'
    end
    object FdmFiltrosDT_COMPETENCIA_TERMINO: TDateField
      DisplayLabel = 'Dt. Compet'#234'ncia - T'#233'rmino'
      FieldName = 'DT_COMPETENCIA_TERMINO'
      Origin = 'DT_COMPETENCIA'
    end
    object FdmFiltrosVL_MOVIMENTO: TFMTBCDField
      DisplayLabel = 'Valor'
      FieldName = 'VL_MOVIMENTO'
      Origin = 'VL_MOVIMENTO'
      DisplayFormat = '###,###,###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object FdmFiltrosID_CONTA_CORRENTE: TLargeintField
      Tag = 1
      FieldName = 'ID_CONTA_CORRENTE'
      Origin = 'ID_CONTA_CORRENTE'
    end
    object FdmFiltrosID_CONTA_ANALISE: TLargeintField
      Tag = 1
      FieldName = 'ID_CONTA_ANALISE'
      Origin = 'ID_CONTA_ANALISE'
    end
    object FdmFiltrosID_CENTRO_RESULTADO: TLargeintField
      Tag = 1
      FieldName = 'ID_CENTRO_RESULTADO'
      Origin = 'ID_CENTRO_RESULTADO'
    end
  end
end
