inherited CadClassificacaoEmpresa: TCadClassificacaoEmpresa
  Caption = 'Cadastro de Classifica'#231#227'o de Empresa'
  ExplicitWidth = 952
  ExplicitHeight = 460
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    inherited cxtsSearch: TcxTabSheet
      ExplicitTop = 24
      ExplicitWidth = 936
      ExplicitHeight = 271
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        inherited cxGBDadosMain: TcxGroupBox
          object lbCodigo: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object lbDescricao: TLabel
            Left = 8
            Top = 37
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object lbVlInicial: TLabel
            Left = 8
            Top = 64
            Width = 42
            Height = 13
            Caption = 'Vl. Inicial'
          end
          object lbVlFinal: TLabel
            Left = 183
            Top = 64
            Width = 37
            Height = 13
            Caption = 'Vl. Final'
          end
          object pkCodigo: TgbDBTextEditPK
            Left = 58
            Top = 4
            HelpType = htKeyword
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 58
          end
          object edtDescricao: TgbDBTextEdit
            Left = 58
            Top = 34
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbPassword = False
            Width = 866
          end
          object edtVlInicial: TgbDBTextEdit
            Left = 58
            Top = 61
            DataBinding.DataField = 'VALOR_INICIAL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbPassword = False
            Width = 121
          end
          object edtVlFinal: TgbDBTextEdit
            Left = 224
            Top = 61
            DataBinding.DataField = 'VALOR_FINAL'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = clWhite
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 3
            gbPassword = False
            Width = 121
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspClassificacaoEmpresa'
    RemoteServer = DmConnection.dspPessoa
    object cdsDataID: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 80
    end
    object cdsDataVALOR_INICIAL: TFMTBCDField
      FieldName = 'VALOR_INICIAL'
      Origin = 'VALOR_INICIAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
    object cdsDataVALOR_FINAL: TFMTBCDField
      FieldName = 'VALOR_FINAL'
      Origin = 'VALOR_FINAL'
      DisplayFormat = '###,###,###,##0.00'
      Precision = 24
      Size = 9
    end
  end
end
