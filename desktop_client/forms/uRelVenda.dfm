inherited RelVenda: TRelVenda
  Caption = 'Relat'#243'rio de Venda'
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited cxPanelMain: TcxGroupBox
    inherited gbPanel1: TgbPanel
      inherited vgFiltros: TcxVerticalGrid
        ExplicitLeft = 2
        ExplicitTop = 2
        Version = 1
        inherited vcatFiltrosPadrao: TcxCategoryRow
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object rowPessoa: TcxEditorRow
          Properties.Caption = 'Pessoa'
          Properties.Hint = 'ID_PESSOA'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.MaskKind = emkRegExpr
          Properties.EditProperties.EditMask = '[0-9]+'
          Properties.EditProperties.OnButtonClick = vgFiltrosEditorRow1EditPropertiesButtonClick
          Properties.DataBinding.ValueType = 'LargeInt'
          Properties.Value = Null
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object vgFiltrosEditorRow2: TcxEditorRow
          Properties.Caption = 'Situa'#231#227'o'
          Properties.Hint = 'STATUS'
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsEditFixedList
          Properties.EditProperties.Items.Strings = (
            'ABERTA'
            'FECHADA'
            'CANCELADA')
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object vgFiltrosEditorRow3: TcxEditorRow
          Properties.Caption = 'Tipo'
          Properties.Hint = 'TIPO'
          Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsEditFixedList
          Properties.EditProperties.Items.Strings = (
            'VENDA'
            'ORCAMENTO'
            'CONDICIONAL'
            'PEDIDO'
            'DEVOLUCAO')
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
      end
    end
  end
  inherited ActionListMain: TActionList
    Left = 292
    Top = 211
  end
  inherited dxBarManager: TdxBarManager
    Left = 264
    Top = 211
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarFiltro: TdxBar
      DockedDockingStyle = dsNone
    end
  end
  inherited cdsConsultaRelatorioFR: TGBClientDataSet
    Left = 256
    Top = 152
  end
  inherited FdmFiltros: TFDMemTable
    object FdmFiltrosID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
    end
    object FdmFiltrosDT_CADASTRO_INICIO: TDateField
      DisplayLabel = 'Dt. Cadastro - In'#237'cio'
      FieldName = 'DT_CADASTRO_INICIO'
    end
    object FdmFiltrosDT_CADASTRO_TERMINO: TDateField
      DisplayLabel = 'Dt. Cadastro - T'#233'rmino'
      FieldName = 'DT_CADASTRO_TERMINO'
    end
    object FdmFiltrosDT_FECHAMENTO_INICIO: TDateField
      DisplayLabel = 'Dt. Fechamento - In'#237'cio'
      FieldName = 'DT_FECHAMENTO_INICIO'
    end
    object FdmFiltrosDT_FECHAMENTO_TERMINO: TDateField
      DisplayLabel = 'Dt. Fechamento - T'#233'rmino'
      FieldName = 'DT_FECHAMENTO_TERMINO'
    end
    object FdmFiltrosVL_VENDA: TFloatField
      DisplayLabel = 'Valor'
      FieldName = 'VL_VENDA'
    end
    object FdmFiltrosSTATUS: TStringField
      Tag = 1
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'STATUS'
    end
    object FdmFiltrosID_PESSOA: TIntegerField
      Tag = 1
      DisplayLabel = 'Pessoa'
      FieldName = 'ID_PESSOA'
    end
    object FdmFiltrosTIPO: TStringField
      Tag = 1
      DisplayLabel = 'Tipo do Documento'
      FieldName = 'TIPO'
    end
  end
end
