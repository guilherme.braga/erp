unit uCadPessoa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, cxGridDBTableView,
  dxBevel, cxGroupBox, Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon,
  cxTextEdit, cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, uGBDBDateEdit, cxCheckBox, uGBDBCheckBox,
  uGBDBTextEdit, uFramePessoaContato, cxSpinEdit, uFrameDetailPadrao,
  uGBDBComboBox, cxButtonEdit, uGBDBButtonEditFK, cxBlobEdit, uGBDBBlobEdit,
  cxRadioGroup, uGBDBRadioGroup, uGBPanel, uFramePessoaSetor,
  uFramePessoaRepresentante, uFramePessoaAtividadeSecundaria, Vcl.Mask,
  uFramePessoaEndereco, dxBarApplicationMenu, uGBDBSpinEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon,
  JvExStdCtrls, JvCombobox, JvDBCombobox, cxMemo, uGBDBMemo, uFrameConsultaDadosDetalheGrade,
  uUsuarioDesignControl, uFrameFiltroVerticalPadrao, cxSplitter, JvExControls, JvButton, JvTransparentButton,
  uFramePessoaContaReceber, uFramePessoaContaPagar, cxCalc, uGBDBCalcEdit,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, uFramePessoaVenda, uFramePessoaRestricao;

type
  TCadPessoa = class(TFrmCadastro_Padrao)
    edtCodigo: TgbDBTextEditPK;
    edtDtCadastro: TgbDBDateEdit;
    cxPageControlDetails: TcxPageControl;
    tsDadosGeral: TcxTabSheet;
    cdsPessoaContato: TGBClientDataSet;
    cdsPessoaEndereco: TGBClientDataSet;
    cdsPessoaEnderecoTIPO: TStringField;
    cdsPessoaEnderecoLOGRADOURO: TStringField;
    cdsPessoaEnderecoCOMPLEMENTO: TStringField;
    cdsPessoaEnderecoOBSERVACAO: TBlobField;
    cdsPessoaEnderecoID_PESSOA: TIntegerField;
    lbDtCadastro: TLabel;
    lbCodigo: TLabel;
    PnPessoa: TcxGroupBox;
    cdsPessoaContatoTIPO: TStringField;
    cdsPessoaContatoCONTATO: TStringField;
    cdsPessoaContatoOBSERVACAO: TStringField;
    cdsPessoaContatoID_PESSOA: TIntegerField;
    cdsPessoaContatoID: TAutoIncField;
    cdsPessoaEnderecoID: TAutoIncField;
    cdsDataID: TAutoIncField;
    cdsDataDH_CADASTRO: TDateTimeField;
    cdsDataDT_NASCIMENTO: TDateField;
    cdsDataTP_PESSOA: TStringField;
    cdsDataBO_ATIVO: TStringField;
    cdsDataBO_CLIENTE: TStringField;
    cdsDataBO_FORNECEDOR: TStringField;
    cdsDataBO_TRANSPORTADORA: TStringField;
    cdsDataDOC_FEDERAL: TStringField;
    cdsDataDOC_ESTADUAL: TStringField;
    cdsDataID_GRUPO_PESSOA: TIntegerField;
    cdsDataJOIN_DESCRICAO_GRUPO_PESSOA: TStringField;
    cdsDataRAZAO_SOCIAL: TStringField;
    cdsDataNOME: TStringField;
    cdsDataOBSERVACAO: TBlobField;
    cdsDatafdqPessoaContato: TDataSetField;
    cdsDatafdqPessoaEndereco: TDataSetField;
    cbTipoPessoa: TgbDBComboBox;
    cbCliente: TgbDBCheckBox;
    cbFornecedor: TgbDBCheckBox;
    cbTransportadora: TgbDBCheckBox;
    lTipoPessoa: TLabel;
    dxBevel3: TdxBevel;
    gbDBCheckBox7: TgbDBCheckBox;
    pnInfoCliente: TgbPanel;
    dxBevel8: TdxBevel;
    dxBevel2: TdxBevel;
    dxBevel4: TdxBevel;
    lbNomeCliente: TLabel;
    lbCPFCliente: TLabel;
    lbRG: TLabel;
    lbGrupoPessoa: TLabel;
    ldDtNascimento: TLabel;
    lbOrgaoEmissor: TLabel;
    lbUFEmissor: TLabel;
    lbNaturalidade: TLabel;
    lbNomePai: TLabel;
    lbNomeMae: TLabel;
    lbEmpresa: TLabel;
    lbCNPJEmpresa: TLabel;
    lbDtAdmissao: TLabel;
    lbRendaMensal: TLabel;
    lbOcupacao: TLabel;
    lbEnderecoEmpresa: TLabel;
    lbBairroEmpresa: TLabel;
    lbCidadeEmpresa: TLabel;
    ldCEPEmpresa: TLabel;
    lbInformacoesCliente: TLabel;
    descGrupoPessoa: TgbDBTextEdit;
    edNome: TgbDBTextEdit;
    fkIdGrupoPessoa: TgbDBButtonEditFK;
    edDocEstadual: TgbDBTextEdit;
    deDtNascimento: TgbDBDateEdit;
    edDocFederal: TgbDBTextEdit;
    edtOrgaoEmissor: TgbDBTextEdit;
    descNaturalidade: TgbDBTextEdit;
    edtNaturalidade: TgbDBButtonEditFK;
    edtNomePai: TgbDBTextEdit;
    edtNomeMae: TgbDBTextEdit;
    edtEmpresa: TgbDBTextEdit;
    EdtCNPJEmpresa: TgbDBTextEdit;
    edtDtAdmissao: TgbDBDateEdit;
    edtRendaMensal: TgbDBTextEdit;
    edtEnderecoEmpresa: TgbDBTextEdit;
    edtBairro: TgbDBButtonEditFK;
    edtCidade: TgbDBButtonEditFK;
    descCidadeEmpresa: TgbDBTextEdit;
    edtUF: TgbDBTextEdit;
    rgSexo: TgbDBRadioGroup;
    pnInfoEmpresa: TgbPanel;
    dxBevel10: TdxBevel;
    dxBevel7: TdxBevel;
    lbInformacoesEmpresa: TLabel;
    lbRazaoSocial: TLabel;
    lbDtFundacao: TLabel;
    lbNomeFantasia: TLabel;
    lbCNPJ: TLabel;
    lbInscricaoEstadual: TLabel;
    lbGrupoPessoaPJ: TLabel;
    lbInscricaoMunicipal: TLabel;
    lbSuframa: TLabel;
    edRazaoSocial: TgbDBTextEdit;
    edtDtFundacao: TgbDBDateEdit;
    edtNomeFantasia: TgbDBTextEdit;
    edtCNPJ: TgbDBTextEdit;
    edtInscricaoEstadual: TgbDBTextEdit;
    edtGrupoPessoaPJ: TgbDBButtonEditFK;
    edtDescGrupoPessoaPJ: TgbDBTextEdit;
    edtInscricaoMunicipal: TgbDBTextEdit;
    edtSuframa: TgbDBTextEdit;
    edtRamal: TgbDBTextEdit;
    Label18: TLabel;
    edtTelefone: TgbDBTextEdit;
    Label17: TLabel;
    tsConjuge: TcxTabSheet;
    pnInfoConjuge: TgbPanel;
    dxBevel16: TdxBevel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    lbEmpresaConjuge: TLabel;
    lbCNPJEmpresaConjuge: TLabel;
    lbDtAdmissaoConjuge: TLabel;
    lbRendaMensalConjuge: TLabel;
    lbOcupacaoConjuge: TLabel;
    lbEnderecoEmpresaConjuge: TLabel;
    lbBairroEmpresaConjuge: TLabel;
    lbCidadeEmpresaConjuge: TLabel;
    lbCEPEmpresaConjuge: TLabel;
    lbTelefoneEmpresaConjuge: TLabel;
    lbRamalEmpresaConjuge: TLabel;
    gbDBTextEdit21: TgbDBTextEdit;
    gbDBTextEdit22: TgbDBTextEdit;
    gbDBRadioGroup2: TgbDBRadioGroup;
    gbDBTextEdit23: TgbDBTextEdit;
    gbDBTextEdit24: TgbDBTextEdit;
    edtIdNaturatildadeConjuge: TgbDBButtonEditFK;
    descNaturalidadeConjuge: TgbDBTextEdit;
    edtDtNascimentoConjuge: TgbDBDateEdit;
    edtEmpresaConjuge: TgbDBTextEdit;
    edtCNPJEmpresaConjuge: TgbDBTextEdit;
    edtDtAdmissaoConjuge: TgbDBDateEdit;
    edtRendaMensalConjuge: TgbDBTextEdit;
    edtEnderecoEmpresaConjuge: TgbDBTextEdit;
    edtBairroEmpresaConjuge: TgbDBButtonEditFK;
    edtCidadeEmpresaConjuge: TgbDBButtonEditFK;
    descCidadeEmpresaConjuge: TgbDBTextEdit;
    descUFEmpresaConjuge: TgbDBTextEdit;
    edtCEPEmpresaConjuge: TgbDBButtonEditFK;
    edtTelefoneEmpresaConjuge: TgbDBTextEdit;
    edtRamalEmpresaConjuge: TgbDBTextEdit;
    dxBevel5: TdxBevel;
    dxBevel13: TdxBevel;
    cdsDataDOC_MUNICIPAL: TStringField;
    cdsDataNOME_PAI: TStringField;
    cdsDataNOME_MAE: TStringField;
    cdsDataORGAO_EMISSOR: TStringField;
    cdsDataID_UF_EMISSOR: TIntegerField;
    cdsDataSEXO: TStringField;
    cdsDataEMPRESA: TStringField;
    cdsDataENDERECO_EMPRESA: TStringField;
    cdsDataID_CIDADE_EMPRESA: TIntegerField;
    cdsDataCEP_EMPRESA: TStringField;
    cdsDataCNPJ_EMPRESA: TStringField;
    cdsDataBAIRRO_EMPRESA: TStringField;
    cdsDataDT_ADMISSAO: TDateField;
    cdsDataTELEFONE_EMPRESA: TStringField;
    cdsDataRAMAL_EMPRESA: TStringField;
    cdsDataRENDA_MENSAL: TFMTBCDField;
    cdsDataSUFRAMA: TStringField;
    cdsDataID_NATURALIDADE: TIntegerField;
    cdsPessoaConjuge: TGBClientDataSet;
    gbDBButtonEditFK1: TgbDBButtonEditFK;
    EdtOcupacao: TgbDBButtonEditFK;
    edtDescUFNaturalidade: TgbDBTextEdit;
    cdsDataJOIN_UF_ESTADO: TStringField;
    cdsDataJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField;
    cdsDataJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField;
    cdsPessoaConjugeID: TAutoIncField;
    cdsPessoaConjugeID_PESSOA: TIntegerField;
    cdsPessoaConjugeNOME: TStringField;
    cdsPessoaConjugeCPF: TStringField;
    cdsPessoaConjugeSEXO: TStringField;
    cdsPessoaConjugeRG: TStringField;
    cdsPessoaConjugeORGAO_EMISSOR: TStringField;
    cdsPessoaConjugeID_UF_EMISSOR: TIntegerField;
    cdsPessoaConjugeDT_NASCIMENTO: TDateField;
    cdsPessoaConjugeEMPRESA: TStringField;
    cdsPessoaConjugeENDERECO_EMPRESA: TStringField;
    cdsPessoaConjugeCEP_EMPRESA: TStringField;
    cdsPessoaConjugeID_CIDADE_EMPRESA: TIntegerField;
    cdsPessoaConjugeCNPJ_EMPRESA: TStringField;
    cdsPessoaConjugeBAIRRO_EMPRESA: TStringField;
    cdsPessoaConjugeDT_ADMISSAO: TDateField;
    cdsPessoaConjugeRENDA_MENSAL: TFMTBCDField;
    cdsPessoaConjugeTELEFONE_EMPRESA: TStringField;
    cdsPessoaConjugeRAMAL_EMPRESA: TStringField;
    cdsPessoaConjugeID_NATURALIDADE: TIntegerField;
    dsConjuge: TDataSource;
    gbDBButtonEditFK2: TgbDBButtonEditFK;
    cdsPessoaConjugeJOIN_UF_ESTADO: TStringField;
    cdsPessoaConjugeJOIN_DESCRICAO_CIDADE_NATURALIDADE: TStringField;
    cdsPessoaConjugeJOIN_DESCRICAO_CIDADE_EMPRESA: TStringField;
    descUFNaturalidadeConjuge: TgbDBTextEdit;
    lbObservacao: TLabel;
    edtObservacao: TgbDBBlobEdit;
    Label2: TLabel;
    gbDBBlobEdit1: TgbDBBlobEdit;
    tsInfoEmpresa: TcxTabSheet;
    dxBevel6: TdxBevel;
    Label3: TLabel;
    dxBevel9: TdxBevel;
    cdsDataID_AREA_ATUACAO: TIntegerField;
    cdsDataID_CLASSIFICACAO_EMPRESA: TIntegerField;
    cdsDataID_ATIVIDADE_PRINCIPAL: TIntegerField;
    cdsDataJOIN_DESCRICAO_CLASSIFICAO_EMPRESA: TStringField;
    cdsDataJOIN_DESCRICAO_CNAE: TStringField;
    lbClassificacao: TLabel;
    edtIdClassificacaoEmpresa: TgbDBButtonEditFK;
    descClassificacao: TgbDBTextEdit;
    edtIdAreaAtuacao: TgbDBButtonEditFK;
    descAreaAtuacao: TgbDBTextEdit;
    lbAreaAtuacao: TLabel;
    lbAtividadePrincipal: TLabel;
    edtAtividadePrincipal: TgbDBButtonEditFK;
    descAtividadePrincipal: TgbDBTextEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    PnFrameSetor: TcxGroupBox;
    PnFrameRepresentantes: TcxGroupBox;
    cdsDataESTADO_CIVIL: TStringField;
    cdsDatafdqPessoaSetor: TDataSetField;
    cdsDatafdqPessoaRepresentante: TDataSetField;
    lbEstadoCivil: TLabel;
    cbEstadoCivil: TgbDBComboBox;
    cdsPessoaRepresentante: TGBClientDataSet;
    cdsPessoaSetor: TGBClientDataSet;
    cdsPessoaRepresentanteID: TAutoIncField;
    cdsPessoaRepresentanteNOME: TStringField;
    cdsPessoaRepresentanteCPF: TStringField;
    cdsPessoaRepresentanteRG: TStringField;
    cdsPessoaRepresentanteDT_NASCIMENTO: TDateField;
    cdsPessoaRepresentanteESTADO_CIVIL: TStringField;
    cdsPessoaRepresentanteID_PESSOA: TIntegerField;
    cdsPessoaSetorID: TAutoIncField;
    cdsPessoaSetorQUANTIDADE_FUNCIONARIOS: TIntegerField;
    cdsPessoaSetorID_PESSOA: TIntegerField;
    cdsDatafdqPessoaAtividadeSecundaria: TDataSetField;
    cdsPessoaAtividadeSecundaria: TGBClientDataSet;
    cdsPessoaAtividadeSecundariaID: TAutoIncField;
    cdsPessoaAtividadeSecundariaID_PESSOA: TIntegerField;
    cdsPessoaAtividadeSecundariaID_CNAE: TIntegerField;
    tsAtividadeSecundaria: TcxTabSheet;
    pcContatoEndereco: TcxPageControl;
    tsContato: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    PnFramePessoaEndereco: TcxGroupBox;
    cdsPessoaEnderecoNUMERO: TStringField;
    Label1: TLabel;
    cdsDataJOIN_SEQUENCIA_CNAE: TStringField;
    cdsDataNUMERO_EMPRESA: TIntegerField;
    cdsDatafdqPessoaConjuge: TDataSetField;
    cdsPessoaSetorID_SETOR_COMERCIAL: TIntegerField;
    cdsPessoaSetorJOIN_DESCRICAO_SETOR_COMERCIAL: TStringField;
    cdsPessoaEnderecoCEP: TStringField;
    cdsPessoaEnderecoBAIRRO: TStringField;
    cdsPessoaEnderecoID_CIDADE: TIntegerField;
    cdsPessoaEnderecoJOIN_DESCRICAO_CIDADE: TStringField;
    cdsPessoaAtividadeSecundariaJOIN_DESCRICAO_CNAE: TStringField;
    cdsPessoaAtividadeSecundariaJOIN_SEQUENCIA_CNAE: TStringField;
    edtIDCEP: TgbDBButtonEditFK;
    Label4: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    PnFramePessoaContato: TgbPanel;
    PnFrameAtividadeSecundaria: TgbPanel;
    cdsDataJOIN_UF_ESTADO_NATURALIDADE: TStringField;
    cdsDataTEMPO_TRABALHO_EMPRESA: TStringField;
    cdsDataQUANTIDADE_FILHOS: TIntegerField;
    cdsDataTIPO_RESIDENCIA: TStringField;
    cdsDataRENDA_EXTRA: TStringField;
    cdsDataVL_RENDA_EXTRA: TFMTBCDField;
    cdsDataVL_LIMITE: TFMTBCDField;
    cdsDataOCUPACAO: TStringField;
    cdsDataJOIN_DESCRICAO_AREA_ATUACAO: TStringField;
    EdtOcupacaoConjuge: TgbDBButtonEditFK;
    cdsPessoaConjugeNUMERO_EMPRESA: TIntegerField;
    cdsPessoaConjugeTEMPO_TRABALHO: TStringField;
    cdsPessoaConjugeOCUPACAO_EMPRESA: TStringField;
    Label5: TLabel;
    gbDBTextEdit2: TgbDBTextEdit;
    Label6: TLabel;
    gbDBTextEdit3: TgbDBTextEdit;
    Label7: TLabel;
    gbDBTextEdit4: TgbDBTextEdit;
    gbDBSpinEdit1: TgbDBSpinEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdtLimiteCreditoPF: TgbDBTextEdit;
    Label10: TLabel;
    gbDBDateEdit1: TgbDBDateEdit;
    cdsDataDT_ASSOCIACAO: TDateField;
    Label11: TLabel;
    gbDBTextEdit6: TgbDBTextEdit;
    tsFiscal: TcxTabSheet;
    PnInformacoesRegimeFiscal: TgbPanel;
    cxPageControl2: TcxPageControl;
    tsPessoaProdutoSituacaoEspecial: TcxTabSheet;
    PnFramePessoaProdutoSituacaoEspecial: TcxGroupBox;
    Label12: TLabel;
    gbDBButtonEditFK3: TgbDBButtonEditFK;
    gbDBTextEdit7: TgbDBTextEdit;
    cdsDataID_ZONEAMENTO: TIntegerField;
    cdsDataJOIN_DESCRICAO_ZONEAMENTO: TStringField;
    cdsDataCRT: TIntegerField;
    cdsDataID_REGIME_ESPECIAL: TIntegerField;
    cdsDataREGIME_TRIBUTARIO: TIntegerField;
    cdsDataBO_CONTRIBUINTE_ICMS: TStringField;
    cdsDataBO_CONTRIBUINTE_IPI: TStringField;
    cdsDataBO_CONTRIBUINTE: TStringField;
    cdsDataJOIN_DESCRICAO_REGIME_ESPECIAL: TStringField;
    cdsDataTIPO_EMPRESA: TStringField;
    Label15: TLabel;
    gbDBButtonEditFK4: TgbDBButtonEditFK;
    gbDBTextEdit8: TgbDBTextEdit;
    gbDBCheckBox1: TgbDBCheckBox;
    gbDBCheckBox2: TgbDBCheckBox;
    gbDBRadioGroup1: TgbDBRadioGroup;
    gbDBRadioGroup3: TgbDBRadioGroup;
    gbDBRadioGroup4: TgbDBRadioGroup;
    Label13: TLabel;
    JvDBComboBox1: TJvDBComboBox;
    tsObservacao: TcxTabSheet;
    gbDBMemo1: TgbDBMemo;
    tsContaReceber: TcxTabSheet;
    pnFrameContaReceber: TgbPanel;
    tsContaPagar: TcxTabSheet;
    tsVenda: TcxTabSheet;
    tsOrdemServico: TcxTabSheet;
    pnFrameContaPagar: TgbPanel;
    pnFrameVenda: TgbPanel;
    pnFrameOrdemServico: TgbPanel;
    tsPessoaCredito: TcxTabSheet;
    cdsDatafdqPessoaCredito: TDataSetField;
    cdsPessoaCredito: TGBClientDataSet;
    cdsPessoaCreditoID: TAutoIncField;
    cdsPessoaCreditoVL_CREDITO: TFMTBCDField;
    cdsPessoaCreditoID_PESSOA: TIntegerField;
    pnFramePessoaCreditoMovimento: TgbPanel;
    pnResumoPessoaCredito: TgbPanel;
    Label14: TLabel;
    gbDBCalcEdit1: TgbDBCalcEdit;
    dsPessoaCredito: TDataSource;
    tsRestricao: TcxTabSheet;
    PnFramePessoaRestricao: TgbPanel;
    cdsDatafdqPessoaRestricao: TDataSetField;
    cdsPessoaRestricao: TGBClientDataSet;
    cdsPessoaRestricaoID: TAutoIncField;
    cdsPessoaRestricaoDESCRICAO: TStringField;
    cdsPessoaRestricaoDH_INCLUSAO: TDateTimeField;
    cdsPessoaRestricaoBO_ATIVO: TStringField;
    cdsPessoaRestricaoID_PESSOA: TIntegerField;
    cdsPessoaRestricaoID_PESSOA_USUARIO: TIntegerField;
    cdsPessoaRestricaoID_TIPO_RESTRICAO: TIntegerField;
    cdsPessoaRestricaoJOIN_DESCRICAO_RESTRICAO: TStringField;
    cdsPessoaContatoBO_EMAIL_ENVIO_DOCUMENTO_FISCAL: TStringField;
    Label16: TLabel;
    EdtLimiteCreditoPJ: TgbDBTextEdit;
    procedure FieldChangeControleDados(Sender: TField);
    procedure cdsDataNewRecord(DataSet: TDataSet);
    procedure edDocFederalExit(Sender: TObject);
    procedure cdsDataTP_PESSOAChange(Sender: TField);
    procedure cdsDataESTADO_CIVILChange(Sender: TField);
    procedure cdsDataAfterOpen(DataSet: TDataSet);
    procedure ValidarDocumentoFederal(AField: TField);
    procedure edtIDCEPgbDepoisDeConsultar(Sender: TObject);
    procedure edtCEPEmpresaConjugegbDepoisDeConsultar(Sender: TObject);
    procedure cdsPessoaEnderecoAfterOpen(DataSet: TDataSet);
    procedure cdsPessoaContatoAfterOpen(DataSet: TDataSet);
    procedure cdsPessoaAtividadeSecundariaAfterOpen(DataSet: TDataSet);
    procedure cdsPessoaRepresentanteAfterOpen(DataSet: TDataSet);
    procedure cdsPessoaSetorAfterOpen(DataSet: TDataSet);
    procedure cdsPessoaRestricaoNewRecord(DataSet: TDataSet);
    procedure cdsPessoaContatoNewRecord(DataSet: TDataSet);
    procedure EdtLimiteCreditoPFDblClick(Sender: TObject);
    procedure EdtLimiteCreditoPFExit(Sender: TObject);
  private
    frameEndereco: TFramePessoaEndereco;
    frameContato: TFramePessoaContato;
    frameAtividadeSecundaria: TFramePessoaAtividadeSecundaria;
    frameRepresentante: TFramePessoaRepresentante;
    frameSetor: TFramePessoaSetor;
    FFrameConsultaDadosDetalheGradeContaReceber: TFramePessoaContaReceber;
    FFrameConsultaDadosDetalheGradeContaPagar: TFramePessoaContaPagar;
    FFrameConsultaDadosDetalheGradeVenda: TFramePessoaVenda;
    FFrameConsultaDadosDetalheGradeOrdemServico: TFrameConsultaDadosDetalheGrade;
    FFrameConsultaDadosDetalheGradePessoaCreditoMovimento: TFrameConsultaDadosDetalheGrade;
    FFramePessoaRestricao: TFramePessoaRestricao;

    FParametroCodigoClienteConsumidorFinal: TParametroFormulario;
    FparametroExibirDadosConjuge: TParametroFormulario;
    FparametroExibirDadosClassificacaoEmpresarial: TParametroFormulario;
    FparametroExibirDadosRegimeFiscal: TParametroFormulario;
    FparametroExibirObservacao: TParametroFormulario;
    FparametroExibirDadosContaReceber: TParametroFormulario;
    FparametroExibirDadosContaPagar: TParametroFormulario;
    FparametroExibirDadosVenda: TParametroFormulario;
    FparametroExibirDadosOrdemServico: TParametroFormulario;
    FParametroExibirDadosCreditoHaver: TParametroFormulario;
    FParametroExibirDadosRestricao: TParametroFormulario;
    FParametroUsuarioPermissaoLimiteCredito: TParametroFormulario;

    procedure GerenciarPaineis;

    procedure ExibirAbaConjuge;
    procedure ExibirAbaDadosClassificacaoEmpresarial;
    procedure ExibirAbaDadosRegimeFiscal;
    procedure ExibirAbaObservacao;
    procedure ExibirAbaDadosContaReceber;
    procedure ExibirAbaDadosContaPagar;
    procedure ExibirAbaDadosVenda;
    procedure ExibirAbaDadosOrdemServico;
    procedure ExibirAbaDadosCreditoHaver;
    procedure ExibirAbaDadosRestricao;

    procedure CriarFrameContasReceber;
    procedure CriarFrameContasPagar;
    procedure CriarFrameVenda;
    procedure CriarFrameOrdemServico;
    procedure CriarFramePessoaCreditoMovimento;
    procedure CriarFrameRestricao;

    procedure SetIDMasterFramesConsultaDadosDetalhe;
  public

  protected
    procedure AntesDeConfirmar;override;
    procedure GerenciarControles; override;
    procedure AoCriarFormulario; override;
    procedure InstanciarFrames; override;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); override;
    procedure DepoisDeParametrizar; override;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); override;
  end;

implementation

{$R *.dfm}

uses uTPessoa, uPessoaProxy, uControlsUtils, uConstantes, uCEPProxy, uCEP,
  uConstParametroFormulario, uOrdemServicoProxy, uVendaProxy, uContaReceber, uContaReceberProxy, uVenda,
  uConstantesFiscaisProxy, uPessoaCredito, uSistema, uFrmAutenticacaoUsuario, uFrmMessage;

{ TCadPessoa }

procedure TCadPessoa.AntesDeConfirmar;
begin
  inherited;
end;

procedure TCadPessoa.AoCriarFormulario;
begin
  inherited;
  cxPageControlDetails.ActivePageIndex := 0;
  edtIDCEP.NaoLimparCampoAposConsultaInvalida;
  edtBairro.NaoLimparCampoAposConsultaInvalida;
  edtCEPEmpresaConjuge.NaoLimparCampoAposConsultaInvalida;
  edtBairroEmpresaConjuge.NaoLimparCampoAposConsultaInvalida;
  EdtOcupacao.NaoLimparCampoAposConsultaInvalida;
  EdtOcupacaoConjuge.NaoLimparCampoAposConsultaInvalida;
  pcContatoEndereco.ActivePage := tsContato;

  ExibirAbaConjuge;
  ExibirAbaDadosClassificacaoEmpresarial;
  ExibirAbaDadosRegimeFiscal;
  ExibirAbaObservacao;
  ExibirAbaDadosContaReceber;
  ExibirAbaDadosContaPagar;
  ExibirAbaDadosVenda;
  ExibirAbaDadosOrdemServico;
  ExibirAbaDadosRestricao;
end;

procedure TCadPessoa.cdsDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetIDMasterFramesConsultaDadosDetalhe;
  ExibirAbaConjuge;
  ExibirAbaDadosCreditoHaver;
end;

procedure TCadPessoa.cdsDataESTADO_CIVILChange(Sender: TField);
begin
  inherited;
  ExibirAbaConjuge;
end;

procedure TCadPessoa.cdsDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsDataDH_CADASTRO.AsDateTime := Now;
  cdsDataTP_PESSOA.AsString     := TPessoaProxy.TIPO_PESSOA_FISICA;
  cdsDataESTADO_CIVIL.AsString  := TPessoaProxy.ESTADO_CIVIL_SOLTEIRO;
  cdsDataSEXO.AsString          := TPessoaProxy.SEXO_MASCULINO;
  cdsDataTIPO_RESIDENCIA.AsString := TPessoaProxy.TIPO_RESIDENCIA_PROPRIA;

  {Regime Fiscal}
  cdsDataTIPO_EMPRESA.AsString := TConstantesFiscais.TIPO_EMPRESA_PRIVADA;
  cdsDataREGIME_TRIBUTARIO.AsInteger := TConstantesFiscais.REGIME_TRIBUTARIO_SIMPLES_NACIONAL;
  cdsDataCRT.AsInteger := TConstantesFiscais.CRT_SIMPLES_NACIONAL;
end;

procedure TCadPessoa.cdsDataTP_PESSOAChange(Sender: TField);
begin
  inherited;
  GerenciarPaineis;
end;

procedure TCadPessoa.cdsPessoaAtividadeSecundariaAfterOpen(DataSet: TDataSet);
begin
  inherited;
  frameAtividadeSecundaria.SetConfiguracoesIniciais(cdsPessoaAtividadeSecundaria);
end;

procedure TCadPessoa.cdsPessoaContatoAfterOpen(DataSet: TDataSet);
begin
  inherited;
  frameContato.SetConfiguracoesIniciais(cdsPessoaContato);
end;

procedure TCadPessoa.cdsPessoaContatoNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('BO_EMAIL_ENVIO_DOCUMENTO_FISCAL').AsString := 'N';
end;

procedure TCadPessoa.cdsPessoaEnderecoAfterOpen(DataSet: TDataSet);
begin
  inherited;
  frameEndereco.SetConfiguracoesIniciais(cdsPessoaEndereco);
end;

procedure TCadPessoa.cdsPessoaRepresentanteAfterOpen(DataSet: TDataSet);
begin
  inherited;
  frameRepresentante.SetConfiguracoesIniciais(cdsPessoaRepresentante);
end;

procedure TCadPessoa.cdsPessoaRestricaoNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('DH_INCLUSAO').AsDateTime := Now;
  Dataset.FieldByName('ID_PESSOA_USUARIO').AsInteger :=
    TSistema.Sistema.Usuario.id;
  DataSet.FieldByName('BO_ATIVO').AsString := 'S';
end;

procedure TCadPessoa.cdsPessoaSetorAfterOpen(DataSet: TDataSet);
begin
  inherited;
  frameSetor.SetConfiguracoesIniciais(cdsPessoaSetor);
end;

procedure TCadPessoa.edDocFederalExit(Sender: TObject);
begin
  inherited;
  ValidarDocumentoFederal(cdsDataDOC_FEDERAL);
end;

procedure TCadPessoa.edtCEPEmpresaConjugegbDepoisDeConsultar(Sender: TObject);
var
  CEP: TCEPProxy;
begin
  inherited;
  if (not cdsPessoaConjuge.GetAsString('CEP_EMPRESA').IsEmpty) then
  begin
    CEP := TCEP.GetLogradouroPeloCEP(cdsPessoaConjuge.GetAsString('CEP_EMPRESA'));
    try
      cdsPessoaConjuge.SetAsString('ENDERECO_EMPRESA', CEP.FRua);
      cdsPessoaConjuge.SetAsString('BAIRRO_EMPRESA', CEP.FBairro);
      if CEP.FIdCidade > 0 then
      begin
        cdsPessoaConjuge.SetAsInteger('ID_CIDADE_EMPRESA', CEP.FIdCidade);
      end
      else
      begin
        cdsPessoaConjuge.FieldByName('ID_CIDADE_EMPRESA').Clear;
      end;
      cdsPessoaConjuge.SetAsString('JOIN_DESCRICAO_CIDADE_EMPRESA', CEP.FCidade);
      cdsPessoaConjuge.SetAsString('JOIN_UF_ESTADO', CEP.FUf);
    finally
      FreeAndNil(CEP);
    end;
  end;
end;

procedure TCadPessoa.edtIDCEPgbDepoisDeConsultar(Sender: TObject);
var
  CEP: TCEPProxy;
begin
  inherited;
  if (not cdsData.GetAsString('CEP_EMPRESA').IsEmpty) then
  begin
    CEP := TCEP.GetLogradouroPeloCEP(cdsData.GetAsString('CEP_EMPRESA'));
    try
      cdsData.SetAsString('ENDERECO_EMPRESA', CEP.FRua);
      cdsData.SetAsString('BAIRRO_EMPRESA', CEP.FBairro);
      if CEP.FIdCidade > 0 then
      begin
        cdsData.SetAsInteger('ID_CIDADE_EMPRESA', CEP.FIdCidade);
      end
      else
      begin
        cdsData.FieldByName('ID_CIDADE_EMPRESA').Clear;
      end;
      cdsData.SetAsString('JOIN_DESCRICAO_CIDADE_EMPRESA', CEP.FCidade);
      cdsData.SetAsString('JOIN_UF_ESTADO', CEP.FUf);
    finally
      FreeAndNil(CEP);
    end;
  end;
end;

procedure TCadPessoa.EdtLimiteCreditoPFDblClick(Sender: TObject);
var
  idUsuario: Integer;
begin
  inherited;
  TFrmAutenticacaoUsuario.Autenticar(idUsuario);
  TgbDBTextEdit(Sender).gbReadyOnly :=
    Pos(InttoStr(idUsuario), FParametroUsuarioPermissaoLimiteCredito.AsString) <= 0;

  if TgbDBTextEdit(Sender).gbReadyOnly then
  begin
    if idUsuario > 0 then
    begin
      TFrmMessage.Information('O usu�rio informado n�o possui permiss�o para alterar o limite de cr�dito.');
    end;

    ActiveControl := nil;
  end;
end;

procedure TCadPessoa.EdtLimiteCreditoPFExit(Sender: TObject);
begin
  inherited;
  TgbDBTextEdit(Sender).gbReadyOnly := true;
end;

procedure TCadPessoa.ExibirAbaConjuge;
begin
  tsConjuge.TabVisible :=
    (cdsDataTP_PESSOA.AsString.Equals(TPessoaProxy.TIPO_PESSOA_FISICA) and
    cdsDataESTADO_CIVIL.AsString.Equals(TPessoaProxy.ESTADO_CIVIL_CASADO)) and
    FparametroExibirDadosConjuge.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosClassificacaoEmpresarial;
begin
  tsAtividadeSecundaria.TabVisible := FparametroExibirDadosClassificacaoEmpresarial.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosContaPagar;
begin
  tsContaPagar.TabVisible := FparametroExibirDadosContaPagar.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosContaReceber;
begin
  tsContaReceber.TabVisible := FparametroExibirDadosContaReceber.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosCreditoHaver;
begin
  tsPessoaCredito.TabVisible := not cdsPessoaCredito.IsEmpty;
end;

procedure TCadPessoa.ExibirAbaDadosOrdemServico;
begin
  tsOrdemServico.TabVisible := FparametroExibirDadosOrdemServico.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosRegimeFiscal;
begin
  tsFiscal.TabVisible := FparametroExibirDadosRegimeFiscal.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosRestricao;
begin
  tsRestricao.TabVisible := FParametroExibirDadosRestricao.ValorSim;
end;

procedure TCadPessoa.ExibirAbaDadosVenda;
begin
  tsVenda.TabVisible := FparametroExibirDadosVenda.ValorSim;
end;

procedure TCadPessoa.ExibirAbaObservacao;
begin
  tsObservacao.TabVisible := FparametroExibirObservacao.ValorSim;
end;

procedure TCadPessoa.FieldChangeControleDados(Sender: TField);
var container: TWinControl;
begin
  FocarContainer(container);
end;

procedure TCadPessoa.ValidarDocumentoFederal(AField: TField);
begin
  if not(AField.AsString.IsEmpty) and
   not(TPessoa.ValidaCnpjCeiCpf(AField.AsString))
  then
  begin
    AField.Clear;
    AField.FocusControl;
  end;
end;

procedure TCadPessoa.GerenciarControles;
begin
  inherited;
  GerenciarPaineis;
end;

procedure TCadPessoa.GerenciarPaineis;
begin
  try
    TControlsUtils.CongelarFormulario(self);
    pnInfoCliente.Visible    :=
      (cdsDataTP_PESSOA.AsString = TPessoaProxy.TIPO_PESSOA_FISICA);
    pnInfoEmpresa.Visible    :=
      (cdsDataTP_PESSOA.AsString = TPessoaProxy.TIPO_PESSOA_JURIDICA);

    ExibirAbaConjuge;

    tsInfoEmpresa.TabVisible :=
      (cdsDataTP_PESSOA.AsString = TPessoaProxy.TIPO_PESSOA_JURIDICA);
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TCadPessoa.InstanciarFrames;
begin
  inherited;
  //Endereco
  frameEndereco := TFramePessoaEndereco.Create(PnFramePessoaEndereco);
  frameEndereco.Parent := PnFramePessoaEndereco;
  frameEndereco.Align := alClient;

  //Contato
  frameContato := TFramePessoaContato.Create(PnFramePessoaContato);
  frameContato.Parent := PnFramePessoaContato;
  frameContato.Align := alClient;

  //Atividade Secundaria
  frameAtividadeSecundaria := TFramePessoaAtividadeSecundaria.Create(PnFrameAtividadeSecundaria);
  frameAtividadeSecundaria.Parent := PnFrameAtividadeSecundaria;
  frameAtividadeSecundaria.Align := alClient;

  //Representante
  frameRepresentante := TFramePessoaRepresentante.Create(PnFrameRepresentantes);
  frameRepresentante.Parent := PnFrameRepresentantes;
  frameRepresentante.Align := alClient;

  //Setor
  frameSetor := TFramePessoaSetor.Create(PnFrameSetor);
  frameSetor.Parent := PnFrameSetor;
  frameSetor.Align := alClient;

  CriarFrameContasReceber;
  CriarFrameContasPagar;
  CriarFrameVenda;
  CriarFrameOrdemServico;
  CriarFramePessoaCreditoMovimento;
  CriarFrameRestricao;
end;

procedure TCadPessoa.SetIDMasterFramesConsultaDadosDetalhe;
begin
  if Assigned(FFrameConsultaDadosDetalheGradeContaReceber) then
  begin
    FFrameConsultaDadosDetalheGradeContaReceber.SetDadosMaster('CONTA_RECEBER.ID_PESSOA', cdsDataID.AsInteger);
  end;

  if Assigned(FFrameConsultaDadosDetalheGradeContaPagar) then
  begin
    FFrameConsultaDadosDetalheGradeContaPagar.SetDadosMaster('CONTA_PAGAR.ID_PESSOA', cdsDataID.AsInteger);
  end;

  if Assigned(FFrameConsultaDadosDetalheGradeVenda) then
  begin
    FFrameConsultaDadosDetalheGradeVenda.SetDadosMaster('VENDA.ID_PESSOA', cdsDataID.AsInteger);
  end;

  if Assigned(FFrameConsultaDadosDetalheGradeOrdemServico) then
  begin
    FFrameConsultaDadosDetalheGradeOrdemServico.SetDadosMaster('ORDEM_SERVICO.ID_PESSOA', cdsDataID.AsInteger);
  end;
end;

procedure TCadPessoa.CriarParametrosFormulario(var AParametros: TListaParametroFormulario);
begin
  inherited;
  //Cliente Consumidor Final
  FparametroCodigoClienteConsumidorFinal := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_CODIGO_CLIENTE_CONSUMIDOR_FINAL,
    TConstParametroFormulario.DESCRICAO_CODIGO_CLIENTE_CONSUMIDOR_FINAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO);
  AParametros.AdicionarParametro(FparametroCodigoClienteConsumidorFinal);

  FparametroExibirDadosConjuge := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_CONJUGE,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_CONJUGE,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosConjuge);

  FparametroExibirDadosClassificacaoEmpresarial := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_CLASSIFICACAO_EMPRESARIAL,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_CLASSIFICACAO_EMPRESARIAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosClassificacaoEmpresarial);

  FparametroExibirDadosRegimeFiscal := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_REGIME_FISCAL,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_REGIME_FISCAL,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosRegimeFiscal);

  FparametroExibirObservacao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_OBSERVACAO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_OBSERVACAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirObservacao);

  FparametroExibirDadosContaReceber := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_CONTA_RECEBER,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_CONTA_RECEBER,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosContaReceber);

  FparametroExibirDadosContaPagar := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_CONTA_PAGAR,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_CONTA_PAGAR,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosContaPagar);

  FparametroExibirDadosVenda := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_VENDA,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_VENDA,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosVenda);

  FparametroExibirDadosOrdemServico := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_ORDEM_SERVICO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_ORDEM_SERVICO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FparametroExibirDadosOrdemServico);

  FParametroExibirDadosRestricao := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_EXIBIR_DADOS_RESTRICAO,
    TConstParametroFormulario.DESCRICAO_EXIBIR_DADOS_RESTRICAO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroExibirDadosRestricao);

  FParametroUsuarioPermissaoLimiteCredito := TParametroFormulario.Create(
    TConstParametroFormulario.PARAMETRO_USUARIO_PERMISSAO_LIMITE_CREDITO,
    TConstParametroFormulario.DESCRICAO_USUARIO_PERMISSAO_LIMITE_CREDITO,
    TParametroFormulario.PARAMETRO_NAO_OBRIGATORIO, TParametroFormulario.VALOR_SIM);
  AParametros.AdicionarParametro(FParametroUsuarioPermissaoLimiteCredito);
end;

procedure TCadPessoa.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
var
  campoFiltro: TcampoFiltro;
begin
  inherited;
  //Nome/Fantasia
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Nome/Fantasia';
  campoFiltro.campo := 'NOME';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Raz�o Social
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Raz�o Social';
  campoFiltro.campo := 'RAZAO_SOCIAL';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Contem);
  campoFiltro.tipo := TDominioFiltro(texto);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //C�digo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento Federal
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Federal (CPF/CNPJ)';
  campoFiltro.campo := 'DOC_FEDERAL';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Documento Estadual
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Documento Estadual (RG/IE)';
  campoFiltro.campo := 'DOC_ESTADUAL';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data de Anivers�rio
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data de Anivers�rio/Funda��o';
  campoFiltro.campo := 'DT_ANIVERSARIO';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  AFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data/Hora de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'PESSOA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  AFiltroVertical.FCampos.Add(campoFiltro);
end;

procedure TCadPessoa.DepoisDeParametrizar;
begin
  if FparametroCodigoClienteConsumidorFinal.AsInteger > 0 then
  begin
    TPessoa.SetClienteConsumidorFinal(FparametroCodigoClienteConsumidorFinal.AsInteger)
  end;
end;

procedure TCadPessoa.CriarFrameContasReceber;
begin
  //Conta a Receber
  FFrameConsultaDadosDetalheGradeContaReceber :=
  TFramePessoaContaReceber.Create(pnFrameContaReceber,
   TFramePessoaContaReceber.FIDENTIFICADOR_SQL_GERAL, 'Contas a Receber');
  FFrameConsultaDadosDetalheGradeContaReceber.Parent := pnFrameContaReceber;
end;


procedure TCadPessoa.CriarFrameContasPagar;
begin
  //Conta a Pagar
  FFrameConsultaDadosDetalheGradeContaPagar :=
  TFramePessoaContaPagar.Create(pnFrameContaPagar,
   TFramePessoaContaPagar.FIDENTIFICADOR_SQL_GERAL, 'Contas a Pagar');
  FFrameConsultaDadosDetalheGradeContaPagar.Parent := pnFrameContaPagar;
end;

procedure TCadPessoa.CriarFrameVenda;
var
  campoFiltro: TcampoFiltro;
begin
  //Venda
  FFrameConsultaDadosDetalheGradeVenda :=
    TFramePessoaVenda.Create(pnFrameVenda,
    Self.Name+'FrameConsultaDadosDetalheGradeVendas', 'Vendas');
  FFrameConsultaDadosDetalheGradeVenda.OcultarTituloFrame;
  FFrameConsultaDadosDetalheGradeVenda.RedimencionarTamanhoGrade;

  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data e Hora de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Fechamento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data e Hora de Fechamento';
  campoFiltro.campo := 'DH_FECHAMENTO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  //Tipo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Tipo do Documento';
  campoFiltro.campo := 'TIPO';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := TIPO_DOCUMENTO_PEDIDO+';'+
  TIPO_DOCUMENTO_ORCAMENTO+';'+TIPO_DOCUMENTO_CONDICIONAL+';'+
  TIPO_DOCUMENTO_VENDA+';'+TIPO_DOCUMENTO_DEVOLUCAO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens := STATUS_ABERTO+';'+STATUS_FECHADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor L�quido da Venda';
  campoFiltro.campo := 'VL_VENDA';
  campoFiltro.tabela := 'VENDA';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.FCampos.Add(campoFiltro);

  FFrameConsultaDadosDetalheGradeVenda.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeVenda.Parent := pnFrameVenda;
end;

procedure TCadPessoa.CriarFrameOrdemServico;
var
  campoFiltro: TcampoFiltro;
begin
  //Ordem de Servi�o
  FFrameConsultaDadosDetalheGradeOrdemServico :=
  TFrameConsultaDadosDetalheGrade.Create(pnFrameOrdemServico,
  Self.Name+'FrameConsultaDadosDetalheGradeOrdemServico',
  'Ordem Servico');
  FFrameConsultaDadosDetalheGradeOrdemServico.OcultarTituloFrame;

  //ID
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'C�digo';
  campoFiltro.campo := 'ID';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(inteiro);
  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Cadastro
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data e Hora de Cadastro';
  campoFiltro.campo := 'DH_CADASTRO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.FCampos.Add(campoFiltro);

  //Data e Hora de Fechamento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data e Hora de Fechamento';
  campoFiltro.campo := 'DH_FECHAMENTO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(datahora);
  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.FCampos.Add(campoFiltro);

  //Status
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Situa��o';
  campoFiltro.campo := 'STATUS';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(caixaSelecao);
  campoFiltro.campoCaixaSelecao := TCampoCaixaSelecao.Create;
  campoFiltro.campoCaixaSelecao.itens :=
  TOrdemServicoProxy.STATUS_ABERTO+';'+
  TOrdemServicoProxy.STATUS_FECHADO;
  campoFiltro.campoCaixaSelecao.valores :=
  campoFiltro.campoCaixaSelecao.itens;
  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor L�quido da OS';
  campoFiltro.campo := 'VL_TOTAL_LIQUIDO';
  campoFiltro.tabela := 'ORDEM_SERVICO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.FCampos.Add(campoFiltro);

  FFrameConsultaDadosDetalheGradeOrdemServico.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradeOrdemServico.Parent := pnFrameOrdemServico;
end;


procedure TCadPessoa.CriarFramePessoaCreditoMovimento;
var
  campoFiltro: TcampoFiltro;
begin
  //PessoaCreditoMovimento
  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento :=
  TFrameConsultaDadosDetalheGrade.Create(pnFramePessoaCreditoMovimento,
  Self.Name+'FrameConsultaDadosDetalheGradePessoaCreditoMovimento', 'Movimenta��es');

  //Data de Movimento
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Data da Movimenta��o';
  campoFiltro.campo := 'DH_MOVIMENTO';
  campoFiltro.tabela := 'PESSOA_CREDITO_MOVIMENTO';
  campoFiltro.condicaoAberta := 'EXTRACT(DAY FROM PESSOA_CREDITO_MOVIMENTO.DH_MOVIMENTO)';
  campoFiltro.condicao := TCondicaoFiltro(Entre);
  campoFiltro.tipo := TDominioFiltro(uFrameFiltroVerticalPadrao.data);
  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento.FFiltroVertical.FCampos.Add(campoFiltro);

  //Valor
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Valor';
  campoFiltro.campo := 'VL_CREDITO_MOVIMENTO';
  campoFiltro.tabela := 'PESSOA_CREDITO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(real);
  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento.FFiltroVertical.FCampos.Add(campoFiltro);

  //Chave de Processo
  campoFiltro := TcampoFiltro.Create;
  campoFiltro.rotulo := 'Chave de Processo';
  campoFiltro.campo := 'ID_CHAVE_PROCESSO';
  campoFiltro.tabela := 'PESSOA_CREDITO_MOVIMENTO';
  campoFiltro.condicao := TCondicaoFiltro(igual);
  campoFiltro.tipo := TDominioFiltro(fk);
  campoFiltro.campoFK := TCampoFK.Create;
  campoFiltro.campoFK.campoPK := 'ID';
  campoFiltro.campoFK.tabelaFK := 'CHAVE_PROCESSO';
  campoFiltro.campoFK.camposConsulta := 'ID;ORIGEM'; //CONFIRMAR
  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento.FFiltroVertical.FCampos.Add(campoFiltro);

  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento.FFiltroVertical.CriarFiltros;

  FFrameConsultaDadosDetalheGradePessoaCreditoMovimento.Parent := pnFramePessoaCreditoMovimento;
end;

procedure TCadPessoa.CriarFrameRestricao;
begin
  FFramePessoaRestricao := TFramePessoaRestricao.Create(PnFramePessoaRestricao);
  FFramePessoaRestricao.Parent := PnFramePessoaRestricao;
  FFramePessoaRestricao.Align := alClient;
  FFramePessoaRestricao.SetConfiguracoesIniciais(cdsData, cdsPessoaRestricao);
end;

initialization
  RegisterClass(TCadPessoa);

finalization
  UnRegisterClass(TCadPessoa);

end.
