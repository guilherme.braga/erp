inherited CadModeloNotaFiscal: TCadModeloNotaFiscal
  Caption = 'Cadastro de Modelo de Nota Fiscal'
  ClientHeight = 440
  ClientWidth = 910
  ExplicitWidth = 926
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxMainRibbon: TdxRibbon
    Width = 910
    inherited dxGerencia: TdxRibbonTab
      Index = 0
    end
    inherited dxDesign: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxpcMain: TcxPageControl
    Width = 910
    Height = 313
    Properties.ActivePage = cxtsData
    ClientRectBottom = 313
    ClientRectRight = 910
    inherited cxtsSearch: TcxTabSheet
      inherited cxGridPesquisaPadrao: TcxGrid
        Width = 878
        Height = 289
      end
      inherited pnFiltroVertical: TgbPanel
        Height = 289
      end
      inherited spFiltroVertical: TcxSplitter
        Height = 289
      end
    end
    inherited cxtsData: TcxTabSheet
      inherited DesignPanel: TJvDesignPanel
        Width = 910
        Height = 289
        inherited cxGBDadosMain: TcxGroupBox
          Height = 289
          Width = 910
          inherited dxBevel1: TdxBevel
            Width = 906
          end
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
          end
          object Label2: TLabel
            Left = 8
            Top = 37
            Width = 34
            Height = 13
            Caption = 'Modelo'
          end
          object Label3: TLabel
            Left = 121
            Top = 37
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object ePkCodig: TgbDBTextEditPK
            Left = 45
            Top = 6
            TabStop = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dsData
            Properties.ReadOnly = True
            Style.BorderStyle = ebsOffice11
            Style.Color = clGradientActiveCaption
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 0
            Width = 72
          end
          object edDescricao: TgbDBTextEdit
            Left = 171
            Top = 33
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'DESCRICAO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 2
            gbRequired = True
            gbPassword = False
            ExplicitWidth = 757
            Width = 731
          end
          object gbDBTextEdit1: TgbDBTextEdit
            Left = 45
            Top = 33
            DataBinding.DataField = 'MODELO'
            DataBinding.DataSource = dsData
            Properties.CharCase = ecUpperCase
            Style.BorderStyle = ebsOffice11
            Style.Color = 14606074
            Style.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.Kind = lfOffice11
            TabOrder = 1
            gbRequired = True
            gbPassword = False
            Width = 72
          end
        end
      end
    end
  end
  inherited dxBarManagerPadrao: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager: TdxBar
      FloatClientWidth = 80
      FloatClientHeight = 221
    end
    inherited dxBarAction: TdxBar
      FloatClientWidth = 82
    end
    inherited dxBarReport: TdxBar
      FloatClientWidth = 85
    end
    inherited dxBarEnd: TdxBar
      FloatClientWidth = 55
    end
    inherited dxBarSearch: TdxBar
      FloatClientWidth = 81
      FloatClientHeight = 54
    end
    inherited dxDesignDesign: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
    end
    inherited dxBarNavegacaoData: TdxBar
      DockedDockingStyle = dsNone
    end
    inherited dxBarPersonalizacoes: TdxBar
      FloatClientWidth = 85
      FloatClientHeight = 21
    end
  end
  inherited UCCadastro_Padrao: TUCControls
    GroupName = 'Cadastro de Modelo de Nota Fiscal'
  end
  inherited cdsData: TGBClientDataSet
    ProviderName = 'dspModeloNota'
    RemoteServer = DmConnection.dspNotaFiscal
    object cdsDataID: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsDataDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object cdsDataMODELO: TStringField
      DisplayLabel = 'Modelo'
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Required = True
      FixedChar = True
      Size = 2
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    inherited dxComponentPrinterLink1: TdxGridReportLink
      ReportDocument.CreationDate = 42343.764796990740000000
      AssignedFormatValues = []
      BuiltInReportLink = True
    end
  end
end
