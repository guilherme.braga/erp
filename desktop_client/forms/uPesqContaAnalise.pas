unit uPesqContaAnalise;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  cxCustomData, cxStyles, cxTL, cxMaskEdit, cxImage, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxDBTL, cxTLData, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.Actions, Vcl.ActnList, Vcl.StdCtrls, cxButtons,
  cxGroupBox;

type
  TPesqContaAnalise = class(TFrmModalPadrao)
    fdmVisaoPlanoConta: TFDMemTable;
    fdmVisaoPlanoContaID: TIntegerField;
    fdmVisaoPlanoContaSEQUENCIA: TStringField;
    fdmVisaoPlanoContaDESCRICAO: TStringField;
    fdmVisaoPlanoContaNIVEL: TIntegerField;
    fdmVisaoPlanoContaID_ASSOCIACAO: TIntegerField;
    fdmVisaoPlanoContaBO_HABILITADO: TIntegerField;
    dsPlanoContaItem: TDataSource;
    TreeListPlanoConta: TcxDBTreeList;
    cxDBTreeList1SEQUENCIA: TcxDBTreeListColumn;
    cxDBTreeList1DESCRICAO: TcxDBTreeListColumn;
    ColumnIcon: TcxDBTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure TreeListPlanoContaDblClick(Sender: TObject);
  private
    const VALOR_HABILITADO: Integer = 18;
    var FIdCentroResultado: Integer;
    procedure SetVisaoPlanoContas;
    procedure ConfigurarVisao;
  public
    class function ExecutarConsulta(AIdCentroResulta: Integer; AFieldCodigo, AFieldDescricao: TField): Boolean;
    class procedure ExecutarConsultaOculta(AIdCentroResultado: Integer; AFieldCodigo, AFieldDescricao: TField);
    class function IDSelecionado(AIdCentroResultado: Integer): Integer;
  end;

implementation

{$R *.dfm}

uses uFrmMessage, uFrmMessage_Process, uPlanoConta, uTFunction;

class function TPesqContaAnalise.ExecutarConsulta(AIdCentroResulta: Integer;
  AFieldCodigo, AFieldDescricao: TField): Boolean;
var PesqContaAnalise: TPesqContaAnalise;
begin
  if (AIdCentroResulta <= 0) then
    Exit;

  if not(AFieldCodigo.Dataset.State in dsEditModes) then
    Exit;

  Application.CreateForm(TPesqContaAnalise, PesqContaAnalise);
  try
    PesqContaAnalise.FIdCentroResultado := AIdCentroResulta;
    PesqContaAnalise.SetVisaoPlanoContas;
    PesqContaAnalise.ShowModal;
    if (PesqContaAnalise.result = MCONFIRMED) then
    begin
      result := true;
      AFieldCodigo.AsInteger := PesqContaAnalise.fdmVisaoPlanoContaID.AsInteger;
      AFieldDescricao.AsString := PesqContaAnalise.fdmVisaoPlanoContaDescricao.AsString;
    end
    else
    begin
      result := false;
    end;
  finally
    FreeAndNil(PesqContaAnalise);
  end;
end;

class procedure TPesqContaAnalise.ExecutarConsultaOculta(
  AIdCentroResultado: Integer; AFieldCodigo, AFieldDescricao: TField);
var descricaoContaAnalise: String;
begin
  if AIdCentroResultado <= 0 then
    Exit;

  if not(AFieldCodigo.Dataset.State in dsEditModes) then
    Exit;

  descricaoContaAnalise := TPlanoConta.GetDescricaoContaAnalise(AFieldCodigo.AsInteger);

  if descricaoContaAnalise.IsEmpty then
  begin
    AFieldCodigo.Clear;
    AFieldDescricao.Clear;
  end
  else
  begin
    AFieldDescricao.AsString := descricaoContaAnalise;
  end;
end;

procedure TPesqContaAnalise.FormCreate(Sender: TObject);
begin
  inherited;
  TreeListPlanoConta.OptionsView.Headers := false;
end;

class function TPesqContaAnalise.IDSelecionado(
  AIdCentroResultado: Integer): Integer;
var PesqContaAnalise: TPesqContaAnalise;
begin
  if (AIdCentroResultado <= 0) then
    Exit;

  Application.CreateForm(TPesqContaAnalise, PesqContaAnalise);
  try
    PesqContaAnalise.FIdCentroResultado := AIdCentroResultado;
    PesqContaAnalise.SetVisaoPlanoContas;
    PesqContaAnalise.ShowModal;
    if (PesqContaAnalise.result = MCONFIRMED) then
    begin
      result := PesqContaAnalise.fdmVisaoPlanoContaID.AsInteger;
    end;
  finally
    FreeAndNil(PesqContaAnalise);
  end;
end;

procedure TPesqContaAnalise.SetVisaoPlanoContas;
begin
  try
    TFrmMessage_Process.SendMessage('Construindo plano de contas');
    TPlanoConta.SetPlanoContaPorCentroResultado(fdmVisaoPlanoConta, FIdCentroResultado);
    ConfigurarVisao;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TPesqContaAnalise.TreeListPlanoContaDblClick(Sender: TObject);
begin
  inherited;
  ActConfirmar.Execute;
end;

procedure TPesqContaAnalise.ConfigurarVisao;
  procedure SetIDAssociado;
  begin
    fdmVisaoPlanoContaID_ASSOCIACAO.AsInteger :=
      TPlanoConta.GetIDNivel(TFunction.Iif(fdmVisaoPlanoContaNIVEL.AsInteger = 1, 1, fdmVisaoPlanoContaNIVEL.AsInteger - 1),
        fdmVisaoPlanoContaSEQUENCIA.AsString);
  end;

  procedure SetBOHabilitado;
  begin
    fdmVisaoPlanoContaBO_HABILITADO.AsInteger := VALOR_HABILITADO;
  end;

begin
  fdmVisaoPlanoConta.DisableControls;
  try
    while not fdmVisaoPlanoConta.Eof do
    begin
      fdmVisaoPlanoConta.Edit;
      SetIDAssociado;
      SetBOHabilitado;
      fdmVisaoPlanoConta.Post;

      fdmVisaoPlanoConta.Next;
    end;
    fdmVisaoPlanoConta.First;
  finally
    fdmVisaoPlanoConta.EnableControls;
  end;
end;

end.
