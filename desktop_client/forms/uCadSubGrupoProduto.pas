unit uCadSubGrupoProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmCadastro_Padrao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  uGBClientDataset, cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, UCBase,
  frxClass, frxDBSet, dxBar, System.Actions, Vcl.ActnList, dxBevel, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC, dxRibbon, uGBDBTextEdit, cxTextEdit,
  cxDBEdit, uGBDBTextEditPK, Vcl.StdCtrls, cxMaskEdit, cxButtonEdit,
  uGBDBButtonEditFK;

type
  TCadSubGrupoProduto = class(TFrmCadastro_Padrao)
    Label6: TLabel;
    gbDBTextEditPK1: TgbDBTextEditPK;
    Label7: TLabel;
    gbDBTextEdit1: TgbDBTextEdit;
    cdsDataID: TAutoIncField;
    cdsDataDESCRICAO: TStringField;
    cdsDataID_GRUPO_PRODUTO: TIntegerField;
    Label1: TLabel;
    descGrupo: TgbDBTextEdit;
    eFkGrupo: TgbDBButtonEditFK;
    cdsDataJOIN_DESCRICAO_GRUPO_PRODUTO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses uDmConnection;

initialization
  RegisterClass(TCadSubGrupoProduto);

finalization
  UnRegisterClass(TCadSubGrupoProduto);

end.
