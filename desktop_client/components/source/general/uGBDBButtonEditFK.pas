unit uGBDBButtonEditFK;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxDBEdit, vcl.Graphics, cxLookAndFeels, uGBDBTextEdit,
  Vcl.StdCtrls, uComponentUtils, Winapi.Messages, Windows, db, System.Generics.Collections,
  cxButtonEdit, Vcl.Menus, uFiltroFormulario, Forms;

type TgbEventSender = procedure (Sender: TObject) of object;

type
  TgbDBButtonEditFK = class(TcxDBButtonEdit)
  private
    FLimparRegistroNaoEncontrado: Boolean;
    FgbTextEdit: TgbDBTextEdit;
    FgbReadyOnly: Boolean;
    FgbRequired: Boolean;
    FConsultaExecutadaPeloFormulario: Boolean;
    FgbTableName: String;
    FgbCampoPK: String;
    FgbCamposRetorno: String;
    FgbCamposConsulta: String;
    gbSQLInjetado: String;
    FgbAntesDeConsultar: TgbEventSender;
    FgbDepoisDeConsultar: TgbEventSender;

    FPopUpMenu: TPopUpMenu;
    FPopUpItemExplorarRegistro: TMenuItem;
    FPopUpItemNovoRegistro: TMenuItem;

    FgbClasseDoCadastro: String;
    FgbAtivarPopupMenu: Boolean;
    FgbIdentificadorConsulta: String;
    FgbDesligarConsultaPadrao: Boolean;
    FgbEnviaValorCampoParaConsulta: Boolean;
    FgbNaoUsarLikeConsulta: Boolean;

    procedure SetDefaultColor;
    procedure SetgbTextEdit(const Value: TgbDBTextEdit);
    procedure SetgbReadyOnly(const Value: Boolean);
    procedure SetgbRequired(const Value: Boolean);
    procedure SetgbTableName(const Value: String);
    function PodeExecutarConsulta: boolean;
    procedure ExecutarConsulta(AExibirFormulario: Boolean);
    procedure ButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure ConstruirPopUpMenu;
    procedure PopUpMenuNovoCadastro(Sender: TObject);
    procedure PopUpMenuExplorarRegistro(Sender: TObject);
    procedure PopUpMenuAoAbrirPopUp(Sender: TObject);
    function BuscarClassNameDoCadastro: String;
  protected
    procedure DoEnter; override;
    procedure DoExit; override;
  public
    FValorAnterior: String;
    FWhereInjetado: TFiltroPadrao;
    procedure NaoLimparCampoAposConsultaInvalida;
    procedure LimparCampoAposConsultaInvalida;
    procedure InjetarSQL(ASQL: String);
    procedure ExecutarOnExit;
    procedure ExecutarOnEnter;
    procedure ExecutarConsultaOculta;
    procedure ExecutarAcaoBotaoConsulta;
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

  published
    property gbTextEdit: TgbDBTextEdit read FgbTextEdit write SetgbTextEdit;
    property gbReadyOnly: Boolean read FgbReadyOnly write SetgbReadyOnly default false;
    property gbRequired: Boolean read FgbRequired write SetgbRequired default false;
    property gbAtivarPopupMenu: Boolean read FgbAtivarPopupMenu write FgbAtivarPopupMenu default true;
    property gbCampoPK: String read FgbCampoPK write FgbCampoPK;
    property gbCamposRetorno: String read FgbCamposRetorno write FgbCamposRetorno;
    property gbTableName: String read FgbTableName write SetgbTableName;
    property gbCamposConsulta: String read FgbCamposConsulta write FgbCamposConsulta;
    property gbAntesDeConsultar: TgbEventSender read FgbAntesDeConsultar write FgbAntesDeConsultar;
    property gbDepoisDeConsultar: TgbEventSender read FgbDepoisDeConsultar write FgbDepoisDeConsultar;
    property gbClasseDoCadastro: String read FgbClasseDoCadastro write FgbClasseDoCadastro;
    property gbIdentificadorConsulta: String read FgbIdentificadorConsulta write FgbIdentificadorConsulta;
    property gbDesligarConsultaPadrao: Boolean
      read FgbDesligarConsultaPadrao write FgbDesligarConsultaPadrao default false;
    property gbEnviaValorCampoParaConsulta: Boolean read FgbEnviaValorCampoParaConsulta
      write FgbEnviaValorCampoParaConsulta default false;
    property gbUsarLikeConsulta: Boolean read FgbNaoUsarLikeConsulta
      write FgbNaoUsarLikeConsulta default True;
  end;

procedure Register;

implementation

uses uFrmConsultaPadrao, uTControl_Function, uFrmCadastro_Padrao, uStringUtils, uDmAcesso, Variants;

procedure Register;
begin
  RegisterComponents('GBComponentsERP', [TgbDBButtonEditFK]);
end;

{ TgbDBTextEditPK }
function TgbDBButtonEditFK.BuscarClassNameDoCadastro: String;
var className: String;
begin
  if gbClasseDoCadastro.IsEmpty then
  begin
    className := StringReplace(gbTableName, '_', ' ', [rfReplaceAll]);
    className := TStringUtils.PrimeiraLetraMaiuscula(className);
    className := StringReplace(className, ' ', '', [rfReplaceAll]);
    className := 'TCad'+className;
  end
  else
  begin
    className := Self.gbClasseDoCadastro;
  end;

  Self.gbClasseDoCadastro := className;

  result := className;
end;

procedure TgbDBButtonEditFK.ButtonClick(Sender: TObject; AButtonIndex: Integer);
const EXIBIR_FORMULARIO: boolean = true;
begin
  if Self.gbReadyOnly then
  begin
    Exit;
  end;

  inherited;
  ExecutarConsulta(EXIBIR_FORMULARIO);
end;

procedure TgbDBButtonEditFK.ConstruirPopUpMenu;
const
  INDICE_ICONE_NOVO_CADASTRO = 9;
  INDICE_ICONE_EXPLORAR_REGISTRO = 11;
begin
  FPopUpMenu := TPopUpMenu.Create(Self);
  //FPopUpMenu.Images := DmAcesso.cxImage16x16;
  FPopUpMenu.OnPopup := PopUpMenuAoAbrirPopUp;

  //Novo Cadastro
  FPopUpItemNovoRegistro := TMenuItem.Create(FPopUpMenu);
  FPopUpItemNovoRegistro.Caption := 'Novo Cadastro';
  FPopUpItemNovoRegistro.OnClick := PopUpMenuNovoCadastro;
  //FPopUpItemNovoRegistro.ImageIndex := INDICE_ICONE_NOVO_CADASTRO;
  FPopUpMenu.Items.Add(FPopUpItemNovoRegistro);

  //Explorar Registo
  FPopUpItemExplorarRegistro := TMenuItem.Create(FPopUpMenu);
  FPopUpItemExplorarRegistro.Caption := 'Explorar Registro';
  FPopUpItemExplorarRegistro.OnClick := PopUpMenuExplorarRegistro;
  //FPopUpItemExplorarRegistro.ImageIndex := INDICE_ICONE_EXPLORAR_REGISTRO;
  FPopUpMenu.Items.Add(FPopUpItemExplorarRegistro);

  Self.PopUpMenu := FPopUpMenu;
end;

procedure TgbDBButtonEditFK.PopUpMenuAoAbrirPopUp(Sender: TObject);
begin
  FPopUpItemExplorarRegistro.Visible := (GetClass(BuscarClassNameDoCadastro) <> nil)
    and (Self.gbAtivarPopupMenu) and not(VartoStr(Self.EditValue).IsEmpty);

  FPopUpItemNovoRegistro.Visible := (GetClass(BuscarClassNameDoCadastro) <> nil) and (Self.gbAtivarPopupMenu) and
    (Self.DataBinding.DataSource.DataSet.State in dsEditModes);
end;

constructor TgbDBButtonEditFK.Create(AOwner: TComponent);
begin
  inherited;
  SetDefaultColor;

  Self.Properties.ClickKey := TextToShortCut('F1');
  Self.Properties.CharCase := ecUpperCase;
  Self.Width := 60;

  gbAtivarPopupMenu := true;
  gbUsarLikeConsulta := true;

  LimparCampoAposConsultaInvalida;

  if not(FgbDesligarConsultaPadrao) then
  begin
    TcxButtonEditProperties(Self.Properties).OnButtonClick := ButtonClick;
  end;

  ConstruirPopUpMenu;
end;

procedure TgbDBButtonEditFK.SetgbTableName(const Value: String);
begin
  FgbTableName := Value;
  if FgbIdentificadorConsulta.IsEmpty then
  begin
    FgbIdentificadorConsulta := FgbTableName;
  end;
end;

procedure TgbDBButtonEditFK.SetgbTextEdit(const Value: TgbDBTextEdit);
begin
  FgbTextEdit := Value;
end;

procedure TgbDBButtonEditFK.SetDefaultColor;
begin
  if gbReadyOnly then exit;

  if (Self.gbRequired) and
     Assigned(Self.DataBinding.Field) and
     Assigned(Self.DataBinding.Field.Dataset) and
     (Self.DataBinding.Field.Dataset.Active) then
  begin
    if not(Self.DataBinding.Field.AsString.IsEmpty) then
      Self.Color := COLORNORMAL
    else
      Self.Color := COLORREQUIRED;
  end
  else if (Self.gbRequired) then
    Self.Color := COLORREQUIRED
  else
    Self.Color := COLORNORMAL;
end;

procedure TgbDBButtonEditFK.SetgbReadyOnly(const Value: Boolean);
begin
  FgbReadyOnly := Value;

  if Value then
  begin
    Self.Properties.ReadOnly := true;
    Self.Style.Color := COLORREADONLY;
    Self.TabStop := false;
  end
  else
  begin
    Self.Properties.ReadOnly := false;
    SetDefaultColor;
    Self.TabStop := true;
  end;
end;

procedure TgbDBButtonEditFK.SetgbRequired(const Value: Boolean);
begin
  FgbRequired := Value;
  SetDefaultColor;
end;

destructor TgbDBButtonEditFK.Destroy;
begin
  if Assigned(FPopupMenu) then
  begin
    FreeAndNil(FPopupMenu);
  end;
  inherited;
end;

procedure TgbDBButtonEditFK.DoEnter;
begin
  ExecutarOnEnter;
  inherited;
end;

procedure TgbDBButtonEditFK.DoExit;
begin
  inherited;
  ExecutarOnExit;
end;

procedure TgbDBButtonEditFK.ExecutarAcaoBotaoConsulta;
begin
  ButtonClick(Self, 0);
end;

procedure TgbDBButtonEditFK.ExecutarConsulta(AExibirFormulario: Boolean);
var
  SOURCE_DATASET: TDataset;
  IDENTIFICADOR_CONSULTA: String;
  SOURCE_CODIGO: String;
  SQL: String;
  camposRetorno, camposConsulta: TStringList;
  i: Integer;
  registroValido: Boolean;
  valorInicialPesquisa: String;
const CONSULTAR_SEM_EXIBIR_FORMULARIO: Boolean = true;
const CONSULTAR_EXIBINDO_FORMULARIO: Boolean = false;
begin
  if Assigned( FgbAntesDeConsultar) then
    FgbAntesDeConsultar(Self);

  SOURCE_DATASET := Self.DataBinding.DataSource.DataSet;

  if not (SOURCE_DATASET.State in dsEditModes) then
    SOURCE_DATASET.Edit;

  IDENTIFICADOR_CONSULTA := Self.gbIdentificadorConsulta;
  SOURCE_CODIGO := Self.DataBinding.DataField;

  camposRetorno := TStringList.Create;
  camposConsulta := TStringList.Create;
  try
    camposRetorno.Delimiter := ';';
    camposRetorno.DelimitedText := Self.gbCamposRetorno;

    camposConsulta.Delimiter := ';';
    camposConsulta.DelimitedText := Self.gbCamposConsulta;

    if AExibirFormulario then
    begin
      if gbEnviaValorCampoParaConsulta and (Self.Text <> EmptyStr) then
      begin
        valorInicialPesquisa := Self.Text;
      end
      else
      begin
        valorInicialPesquisa := '';
      end;

      registroValido := TFrmConsultaPadrao.Consultar(SOURCE_DATASET, IDENTIFICADOR_CONSULTA,
        CamposRetorno, CamposConsulta, Self.gbSQLInjetado, FWhereInjetado, CONSULTAR_EXIBINDO_FORMULARIO,
        '', '', valorInicialPesquisa, Self.gbUsarLikeConsulta);
    end
    else
    begin
      registroValido := TFrmConsultaPadrao.Consultar(SOURCE_DATASET, IDENTIFICADOR_CONSULTA,
        CamposRetorno, CamposConsulta, Self.gbSQLInjetado, FWhereInjetado, CONSULTAR_SEM_EXIBIR_FORMULARIO,
        Self.gbCampoPK, SOURCE_DATASET.FieldByName(SOURCE_CODIGO).AsString, '', Self.gbUsarLikeConsulta);
    end;

    if (not registroValido) and (FLimparRegistroNaoEncontrado) then
    begin
      for i := 0 to Pred(camposRetorno.Count) do
        if Self.DataBinding.DataSource.DataSet.FindField(camposRetorno[i]) <> nil then
          Self.DataBinding.DataSource.DataSet.FieldByName(camposRetorno[i]).Clear;
    end
    else
      Self.SelectNext(Self.Parent, true, true);
  finally
    FreeAndNil(camposRetorno);
    FreeAndNil(camposConsulta);

    if Assigned( FgbDepoisDeConsultar) then
      FgbDepoisDeConsultar(Self);
  end;
end;

procedure TgbDBButtonEditFK.ExecutarConsultaOculta;
begin
  ExecutarOnExit;
  ExecutarOnEnter;
end;

procedure TgbDBButtonEditFK.ExecutarOnEnter;
begin
  Self.Color := COLORFOCUS;
  FConsultaExecutadaPeloFormulario := false;
  FValorAnterior := Self.Text;
end;

procedure TgbDBButtonEditFK.ExecutarOnExit;
const
  NAO_EXIBIR_FORMULARIO: boolean = false;
begin
  if PodeExecutarConsulta then
    ExecutarConsulta(NAO_EXIBIR_FORMULARIO);

  SetDefaultColor;
end;

procedure TgbDBButtonEditFK.InjetarSQL(ASQL: String);
begin
  gbSQLInjetado := ASQL;
end;

procedure TgbDBButtonEditFK.LimparCampoAposConsultaInvalida;
begin
  FLimparRegistroNaoEncontrado := true;
end;

procedure TgbDBButtonEditFK.NaoLimparCampoAposConsultaInvalida;
begin
  FLimparRegistroNaoEncontrado := false;
end;

function TgbDBButtonEditFK.PodeExecutarConsulta: boolean;
begin
  result :=
     Assigned(Self.DataBinding.DataSource.DataSet) and
    (Self.DataBinding.DataSource.DataSet.State in dsEditModes) and
    (Self.Text <> '') and
    not(FValorAnterior.Equals(Self.Text)) and
    not(FConsultaExecutadaPeloFormulario) and (not FgbDesligarConsultaPadrao);
end;

procedure TgbDBButtonEditFK.PopUpMenuExplorarRegistro(Sender: TObject);
begin
  DoExit;
  Application.ProcessMessages;
  BuscarClassNameDoCadastro;
  TFrmCadastro_Padrao.ExplorarRegistroFK(Self);
end;

procedure TgbDBButtonEditFK.PopUpMenuNovoCadastro(Sender: TObject);
begin
  TFrmCadastro_Padrao.ModoDeInclusaoFK(Self, BuscarClassNameDoCadastro, Self.gbTableName);
end;

end.
