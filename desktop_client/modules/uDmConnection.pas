unit uDmConnection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  Data.DB, FireDAC.Comp.Client, Data.DBXDataSnap, IPPeerClient, Data.DBXCommon,
  Datasnap.DBClient, Datasnap.DSConnect, Data.SqlExpr, uGBClientDataset,
  Datasnap.Win.TConnect, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON, uClientClasses, Winapi.Windows;

type
  TDmConnection = class(TDataModule)
    Connection: TSQLConnection;
    dspLocalidade: TDSProviderConnection;
    LocalConnection: TLocalConnection;
    FDStanStorageBinLink: TFDStanStorageBinLink;
    dspPesquisaPadrao: TDSProviderConnection;
    dspPessoa: TDSProviderConnection;
    cdsDesignGrid: TgbClientDataSet;
    cdsDesignGridID: TAutoIncField;
    cdsDesignGridFORMULARIO: TStringField;
    cdsDesignGridARQUIVO_GRID: TBlobField;
    cdsDesignGridARQUIVO_FIELD_CAPTION: TBlobField;
    cdsDesignGridID_PESSOA_USUARIO: TIntegerField;
    dspFinanceiro: TDSProviderConnection;
    dspProduto: TDSProviderConnection;
    dspRestaurante: TDSProviderConnection;
    dspIndoor: TDSProviderConnection;
    dspChaveProcesso: TDSProviderConnection;
    dspEmpresaFilial: TDSProviderConnection;
    dspNotaFiscal: TDSProviderConnection;
    dspOperacao: TDSProviderConnection;
    dspTabelaPreco: TDSProviderConnection;
    dspContaReceber: TDSProviderConnection;
    dspContaPagar: TDSProviderConnection;
    dspGeracaoDocumento: TDSProviderConnection;
    dspVenda: TDSProviderConnection;
    dspRelatorioFR: TDSProviderConnection;
    cdsDesignControl: TGBClientDataSet;
    cdsDesignControlID: TAutoIncField;
    cdsDesignControlFORMULARIO: TStringField;
    cdsDesignControlARQUIVO_CONTROL: TBlobField;
    cdsDesignControlARQUIVO_INVISIBLE: TBlobField;
    cdsDesignControlID_PESSOA_USUARIO: TIntegerField;
    cdsDesignControlARQUIVO_VALORES_PADRAO: TBlobField;
    cdsDesignControlARQUIVO_FILTRO_PADRAO: TBlobField;
    dspCheque: TDSProviderConnection;
    dspAjusteEstoque: TDSProviderConnection;
    dspMotivo: TDSProviderConnection;
    dspOrdemServico: TDSProviderConnection;
    dspEquipamento: TDSProviderConnection;
    dspServico: TDSProviderConnection;
    dspChecklist: TDSProviderConnection;
    dspSituacao: TDSProviderConnection;
    dspLotePagamento: TDSProviderConnection;
    dspLoteRecebimento: TDSProviderConnection;
    dspCNAE: TDSProviderConnection;
    dspCor: TDSProviderConnection;
    dspVeiculo: TDSProviderConnection;
    dspOperadoraCartao: TDSProviderConnection;
    dspPessoaCredito: TDSProviderConnection;
    cdsDesignControlARQUIVO_PARAMETROS: TBlobField;
    dspNCM: TDSProviderConnection;
    dspCarteira: TDSProviderConnection;
    dspCSOSN: TDSProviderConnection;
    dspRegimeEspecial: TDSProviderConnection;
    dspSituacaoEspecial: TDSProviderConnection;
    dspZoneamento: TDSProviderConnection;
    dspMedico: TDSProviderConnection;
    dspReceitaOtica: TDSProviderConnection;
    dspRegraImposto: TDSProviderConnection;
    dspCSTIPI: TDSProviderConnection;
    dspCSTCSOSN: TDSProviderConnection;
    dspSCTPISCofins: TDSProviderConnection;
    dspImpostoIPI: TDSProviderConnection;
    dspImpostoICMS: TDSProviderConnection;
    dspImpostoPISCofins: TDSProviderConnection;
    dspConfiguracaoFiscal: TDSProviderConnection;
    dspInventario: TDSProviderConnection;
    dspMovimentacaoCheque: TDSProviderConnection;
    dspMontadora: TDSProviderConnection;
    dspModeloMontadora: TDSProviderConnection;
    dspNegociacaoContaReceber: TDSProviderConnection;
    dspGradeProduto: TDSProviderConnection;
    dspTamanho: TDSProviderConnection;
    dspCFOP: TDSProviderConnection;
    cdsDesignGrid_ConsultaFormulario: TGBClientDataSet;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    BlobField1: TBlobField;
    BlobField2: TBlobField;
    IntegerField1: TIntegerField;
    dspBloqueioPersonalizado: TDSProviderConnection;
    dspTipoRestricao: TDSProviderConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FInstanceOwner: Boolean;
    FServerMethodsClient: TServerMethodsClient;
    function GetServerMethodsClient: TServerMethodsClient;
  public
    property ServerMethodsClient: TServerMethodsClient read GetServerMethodsClient write FServerMethodsClient;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;

    procedure Commit;
    procedure RollBack;
    procedure StartTransaction;
    function GetLastedAutoIncrementValue: Integer;
  end;

var
  DmConnection: TDmConnection;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFrmMessage, uIniFileClient;

{$R *.dfm}

procedure TDmConnection.DataModuleCreate(Sender: TObject);
begin
  try
    Connection.Connected := false;

    Connection.Params.Clear;
    Connection.Params.Add('DriverName=DataSnap');
    Connection.Params.Add('DriverUnit=Data.DBXDataSnap');
    Connection.Params.Add('HostName='+IniSystemFile.server);
    Connection.Params.Add('Port='+IniSystemFile.porta);
    Connection.Params.Add('CommunicationProtocol=tcp/ip');
    Connection.Params.Add('DatasnapContext=datasnap/');
    Connection.Params.Add(
      'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland'+
      '.Data.DbxClientDriver,Version=20.0.0.0,Culture=neutral,PublicKey'+
      'Token=91d62ebb5b0d1b1b');
    Connection.Connected := true;
  Except
    on e: Exception do
    begin
      TFrmMessage.Failure('N�o foi poss�vel estabelecer uma conex�o com o servidor. Verifique se o servi�o est� operante.');
      FatalExit(0);
    end;
  end;
end;

function TDmConnection.GetServerMethodsClient: TServerMethodsClient;
begin
  if FServerMethodsClient = nil then
    FServerMethodsClient := TServerMethodsClient.Create(Connection.DBXConnection, FInstanceOwner);

  Result := FServerMethodsClient;
end;

procedure TDmConnection.DataModuleDestroy(Sender: TObject);
begin
  FServerMethodsClient.Free;
end;

procedure TDmConnection.Commit;
begin
  FServerMethodsClient.Commit();
end;

procedure TDmConnection.RollBack;
begin
  FServerMethodsClient.Rollback();
end;

procedure TDmConnection.StartTransaction;
begin
  FServerMethodsClient.StartTransaction();
end;

function TDmConnection.GetLastedAutoIncrementValue: Integer;
begin
  result := FServerMethodsClient.GetLastedAutoIncrementValue;
end;

end.
