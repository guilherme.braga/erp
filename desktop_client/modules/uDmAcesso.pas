unit uDmAcesso;

interface

uses
  System.SysUtils, System.Classes, dxScreenTip, dxCustomHint, cxHint,
  cxContainer, cxEdit, cxClasses, Vcl.ImgList, Vcl.Controls, cxGraphics,
  System.Actions, Vcl.ActnList, Vcl.Menus, cxStyles;

type
  TDmAcesso = class(TDataModule)
    cxImage32x32: TcxImageList;
    cxDefaultEditStyleController1: TcxDefaultEditStyleController;
    cxEditStyleController1: TcxEditStyleController;
    cxHintStyleController1: TcxHintStyleController;
    cxImage24x24: TcxImageList;
    cxImage64x64: TcxImageList;
    cxImageList48x48_Messages: TcxImageList;
    cxImage256x256: TcxImageList;
    cxImage16x16: TcxImageList;
    cxStyleRepository1: TcxStyleRepository;
    OddColor: TcxStyle;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmAcesso: TDmAcesso;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
