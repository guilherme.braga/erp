object DmConnection: TDmConnection
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 414
  Width = 673
  object Connection: TSQLConnection
    ConnectionName = 'DSServer211'
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=DataSnap'
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=20.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Filters={}')
    Left = 42
    Top = 5
  end
  object dspLocalidade: TDSProviderConnection
    ServerClassName = 'TSMLocalidade'
    SQLConnection = Connection
    Left = 24
    Top = 64
  end
  object LocalConnection: TLocalConnection
    Left = 4
    Top = 5
  end
  object FDStanStorageBinLink: TFDStanStorageBinLink
    Left = 592
    Top = 8
  end
  object dspPesquisaPadrao: TDSProviderConnection
    ServerClassName = 'TServerMethods'
    SQLConnection = Connection
    Left = 52
    Top = 64
  end
  object dspPessoa: TDSProviderConnection
    ServerClassName = 'TSMPessoa'
    SQLConnection = Connection
    Left = 81
    Top = 64
  end
  object cdsDesignGrid: TGBClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_PESSOA_USUARIO'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftString
        Name = 'FORMULARIO'
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    ProviderName = 'dspPessoaUsuarioDesignGrid'
    RemoteServer = dspPessoa
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 64
    Top = 128
    object cdsDesignGridID: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsDesignGridFORMULARIO: TStringField
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 255
    end
    object cdsDesignGridARQUIVO_GRID: TBlobField
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object cdsDesignGridARQUIVO_FIELD_CAPTION: TBlobField
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object cdsDesignGridID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspFinanceiro: TDSProviderConnection
    ServerClassName = 'TSMFinanceiro'
    SQLConnection = Connection
    Left = 109
    Top = 64
  end
  object dspIndoor: TDSProviderConnection
    ServerClassName = 'TSMIndoor'
    SQLConnection = Connection
    Left = 192
    Top = 64
  end
  object dspProduto: TDSProviderConnection
    ServerClassName = 'TSMProduto'
    SQLConnection = Connection
    Left = 139
    Top = 64
  end
  object dspRestaurante: TDSProviderConnection
    ServerClassName = 'TSMRestaurante'
    SQLConnection = Connection
    Left = 167
    Top = 64
  end
  object dspChaveProcesso: TDSProviderConnection
    ServerClassName = 'TSMChaveProcesso'
    SQLConnection = Connection
    Left = 352
    Top = 64
  end
  object dspEmpresaFilial: TDSProviderConnection
    ServerClassName = 'TSMEmpresaFilial'
    SQLConnection = Connection
    Left = 224
    Top = 64
  end
  object dspNotaFiscal: TDSProviderConnection
    ServerClassName = 'TSMNotaFiscal'
    SQLConnection = Connection
    Left = 320
    Top = 64
  end
  object dspOperacao: TDSProviderConnection
    ServerClassName = 'TSMOperacao'
    SQLConnection = Connection
    Left = 256
    Top = 64
  end
  object dspTabelaPreco: TDSProviderConnection
    ServerClassName = 'TSMTabelaPreco'
    SQLConnection = Connection
    Left = 288
    Top = 64
  end
  object dspContaReceber: TDSProviderConnection
    ServerClassName = 'TSMContaReceber'
    SQLConnection = Connection
    Left = 380
    Top = 64
  end
  object dspContaPagar: TDSProviderConnection
    ServerClassName = 'TSMContaPagar'
    SQLConnection = Connection
    Left = 408
    Top = 64
  end
  object dspGeracaoDocumento: TDSProviderConnection
    ServerClassName = 'TSMGeracaoDocumento'
    SQLConnection = Connection
    Left = 437
    Top = 64
  end
  object dspVenda: TDSProviderConnection
    ServerClassName = 'TSMVenda'
    SQLConnection = Connection
    Left = 469
    Top = 64
  end
  object dspRelatorioFR: TDSProviderConnection
    ServerClassName = 'TSMRelatorioFR'
    SQLConnection = Connection
    Left = 497
    Top = 64
  end
  object cdsDesignControl: TGBClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_PESSOA_USUARIO'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftString
        Name = 'FORMULARIO'
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    ProviderName = 'dspPessoaUsuarioDesignControl'
    RemoteServer = dspPessoa
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 144
    Top = 128
    object cdsDesignControlID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object cdsDesignControlFORMULARIO: TStringField
      FieldName = 'FORMULARIO'
      Size = 255
    end
    object cdsDesignControlARQUIVO_CONTROL: TBlobField
      FieldName = 'ARQUIVO_CONTROL'
    end
    object cdsDesignControlARQUIVO_INVISIBLE: TBlobField
      FieldName = 'ARQUIVO_INVISIBLE'
    end
    object cdsDesignControlID_PESSOA_USUARIO: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Required = True
    end
    object cdsDesignControlARQUIVO_VALORES_PADRAO: TBlobField
      FieldName = 'ARQUIVO_VALORES_PADRAO'
    end
    object cdsDesignControlARQUIVO_FILTRO_PADRAO: TBlobField
      FieldName = 'ARQUIVO_FILTRO_PADRAO'
    end
    object cdsDesignControlARQUIVO_PARAMETROS: TBlobField
      FieldName = 'ARQUIVO_PARAMETROS'
    end
  end
  object dspCheque: TDSProviderConnection
    ServerClassName = 'TSMCheque'
    SQLConnection = Connection
    Left = 288
    Top = 120
  end
  object dspAjusteEstoque: TDSProviderConnection
    ServerClassName = 'TSMAjusteEstoque'
    SQLConnection = Connection
    Left = 260
    Top = 120
  end
  object dspMotivo: TDSProviderConnection
    ServerClassName = 'TSMMotivo'
    SQLConnection = Connection
    Left = 231
    Top = 120
  end
  object dspOrdemServico: TDSProviderConnection
    ServerClassName = 'TSMOrdemServico'
    SQLConnection = Connection
    Left = 316
    Top = 120
  end
  object dspEquipamento: TDSProviderConnection
    ServerClassName = 'TSMEquipamento'
    SQLConnection = Connection
    Left = 343
    Top = 120
  end
  object dspServico: TDSProviderConnection
    ServerClassName = 'TSMServico'
    SQLConnection = Connection
    Left = 398
    Top = 120
  end
  object dspChecklist: TDSProviderConnection
    ServerClassName = 'TSMChecklist'
    SQLConnection = Connection
    Left = 370
    Top = 120
  end
  object dspSituacao: TDSProviderConnection
    ServerClassName = 'TSMSituacao'
    SQLConnection = Connection
    Left = 426
    Top = 120
  end
  object dspLotePagamento: TDSProviderConnection
    ServerClassName = 'TSMLotePagamento'
    SQLConnection = Connection
    Left = 450
    Top = 120
  end
  object dspLoteRecebimento: TDSProviderConnection
    ServerClassName = 'TSMLoteRecebimento'
    SQLConnection = Connection
    Left = 478
    Top = 120
  end
  object dspCNAE: TDSProviderConnection
    ServerClassName = 'TSMCNAE'
    SQLConnection = Connection
    Left = 226
    Top = 176
  end
  object dspCor: TDSProviderConnection
    ServerClassName = 'TSMCor'
    SQLConnection = Connection
    Left = 290
    Top = 176
  end
  object dspVeiculo: TDSProviderConnection
    ServerClassName = 'TSMVeiculo'
    SQLConnection = Connection
    Left = 322
    Top = 176
  end
  object dspOperadoraCartao: TDSProviderConnection
    ServerClassName = 'TSMOperadoraCartao'
    SQLConnection = Connection
    Left = 354
    Top = 176
  end
  object dspPessoaCredito: TDSProviderConnection
    ServerClassName = 'TSMCreditoPessoa'
    SQLConnection = Connection
    Left = 386
    Top = 176
  end
  object dspNCM: TDSProviderConnection
    ServerClassName = 'TSMNCM'
    SQLConnection = Connection
    Left = 66
    Top = 232
  end
  object dspCarteira: TDSProviderConnection
    ServerClassName = 'TSMCarteira'
    SQLConnection = Connection
    Left = 98
    Top = 232
  end
  object dspCSOSN: TDSProviderConnection
    ServerClassName = 'TSMCSOSN'
    SQLConnection = Connection
    Left = 130
    Top = 232
  end
  object dspRegimeEspecial: TDSProviderConnection
    ServerClassName = 'TSMRegimeEspecial'
    SQLConnection = Connection
    Left = 162
    Top = 232
  end
  object dspSituacaoEspecial: TDSProviderConnection
    ServerClassName = 'TSMSituacaoEspecial'
    SQLConnection = Connection
    Left = 192
    Top = 232
  end
  object dspZoneamento: TDSProviderConnection
    ServerClassName = 'TSMZoneamento'
    SQLConnection = Connection
    Left = 224
    Top = 232
  end
  object dspMedico: TDSProviderConnection
    ServerClassName = 'TSMMedico'
    SQLConnection = Connection
    Left = 252
    Top = 232
  end
  object dspReceitaOtica: TDSProviderConnection
    ServerClassName = 'TSMReceitaOtica'
    SQLConnection = Connection
    Left = 280
    Top = 232
  end
  object dspRegraImposto: TDSProviderConnection
    ServerClassName = 'TSMRegraImposto'
    SQLConnection = Connection
    Left = 64
    Top = 288
  end
  object dspCSTIPI: TDSProviderConnection
    ServerClassName = 'TSMCSTIPI'
    SQLConnection = Connection
    Left = 104
    Top = 288
  end
  object dspCSTCSOSN: TDSProviderConnection
    ServerClassName = 'TSMCSTCSOSN'
    SQLConnection = Connection
    Left = 144
    Top = 288
  end
  object dspSCTPISCofins: TDSProviderConnection
    ServerClassName = 'TSMCSTPisCofins'
    SQLConnection = Connection
    Left = 184
    Top = 288
  end
  object dspImpostoIPI: TDSProviderConnection
    ServerClassName = 'TSMImpostoIPI'
    SQLConnection = Connection
    Left = 224
    Top = 288
  end
  object dspImpostoICMS: TDSProviderConnection
    ServerClassName = 'TSMImpostoICMS'
    SQLConnection = Connection
    Left = 264
    Top = 288
  end
  object dspImpostoPISCofins: TDSProviderConnection
    ServerClassName = 'TSMImpostoPISCofins'
    SQLConnection = Connection
    Left = 304
    Top = 288
  end
  object dspConfiguracaoFiscal: TDSProviderConnection
    ServerClassName = 'TSMConfiguracoesFiscais'
    SQLConnection = Connection
    Left = 344
    Top = 288
  end
  object dspInventario: TDSProviderConnection
    ServerClassName = 'TSMInventario'
    SQLConnection = Connection
    Left = 312
    Top = 232
  end
  object dspMovimentacaoCheque: TDSProviderConnection
    ServerClassName = 'TSMMovimentacaoCheque'
    SQLConnection = Connection
    Left = 418
    Top = 176
  end
  object dspMontadora: TDSProviderConnection
    ServerClassName = 'TSMMontadora'
    SQLConnection = Connection
    Left = 526
    Top = 64
  end
  object dspModeloMontadora: TDSProviderConnection
    ServerClassName = 'TSMModeloMontadora'
    SQLConnection = Connection
    Left = 554
    Top = 64
  end
  object dspNegociacaoContaReceber: TDSProviderConnection
    ServerClassName = 'TSMNegociacaoContaReceber'
    SQLConnection = Connection
    Left = 583
    Top = 64
  end
  object dspGradeProduto: TDSProviderConnection
    ServerClassName = 'TSMGradeProduto'
    SQLConnection = Connection
    Left = 447
    Top = 176
  end
  object dspTamanho: TDSProviderConnection
    ServerClassName = 'TSMTamanho'
    SQLConnection = Connection
    Left = 475
    Top = 176
  end
  object dspCFOP: TDSProviderConnection
    ServerClassName = 'TSMCFOP'
    SQLConnection = Connection
    Left = 384
    Top = 288
  end
  object cdsDesignGrid_ConsultaFormulario: TGBClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_PESSOA_USUARIO'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftString
        Name = 'FORMULARIO'
        ParamType = ptInput
        Value = 'FrmPais'
      end>
    ProviderName = 'dspPessoaUsuarioDesignGrid_ConsultaFormulario'
    RemoteServer = dspPessoa
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 88
    Top = 176
    object AutoIncField1: TAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object StringField1: TStringField
      FieldName = 'FORMULARIO'
      Origin = 'FORMULARIO'
      Size = 255
    end
    object BlobField1: TBlobField
      FieldName = 'ARQUIVO_GRID'
      Origin = 'ARQUIVO_GRID'
    end
    object BlobField2: TBlobField
      FieldName = 'ARQUIVO_FIELD_CAPTION'
      Origin = 'ARQUIVO_FIELD_CAPTION'
    end
    object IntegerField1: TIntegerField
      FieldName = 'ID_PESSOA_USUARIO'
      Origin = 'ID_PESSOA_USUARIO'
      Required = True
    end
  end
  object dspBloqueioPersonalizado: TDSProviderConnection
    ServerClassName = 'TSMBloqueioPersonalizado'
    SQLConnection = Connection
    Left = 412
    Top = 288
  end
  object dspTipoRestricao: TDSProviderConnection
    ServerClassName = 'TSMTipoRestricao'
    SQLConnection = Connection
    Left = 444
    Top = 288
  end
end
