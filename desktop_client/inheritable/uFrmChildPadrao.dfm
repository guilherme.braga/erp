object FrmChildPadrao: TFrmChildPadrao
  Left = 345
  Top = 224
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Form Child Padr'#227'o'
  ClientHeight = 440
  ClientWidth = 849
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 849
    Height = 102
    BarManager = dxBarManager
    Style = rs2010
    ColorSchemeName = 'Blue'
    ShowTabHeaders = False
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = 'dxRibbon1Tab1'
      Groups = <
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  object cxPanelMain: TcxGroupBox
    Left = 0
    Top = 102
    Align = alClient
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Height = 338
    Width = 849
  end
  object ActionListMain: TActionList
    Images = DmAcesso.cxImage32x32
    Left = 748
    Top = 11
    object ActClose: TAction
      Category = 'End'
      Caption = '&Sair'
      Hint = 'Fechar o Formul'#225'rio'
      ImageIndex = 12
      ShortCut = 27
      OnExecute = ActCloseExecute
    end
    object ActConfirm: TAction
      Category = 'Action'
      Caption = '&Confirmar'
      ImageIndex = 13
      ShortCut = 16397
      OnExecute = ActConfirmExecute
    end
    object ActCancel: TAction
      Category = 'Action'
      Caption = 'Ca&ncelar'
      ImageIndex = 14
      ShortCut = 27
      OnExecute = ActCancelExecute
    end
    object ActPersonalizar: TAction
      Category = 'End'
      Caption = 'Personalizar'
      ImageIndex = 33
      OnExecute = ActPersonalizarExecute
    end
    object ActImprimirChildPadrao: TAction
      Category = 'Action'
      Caption = 'Impress'#227'o F8'
      ImageIndex = 115
      ShortCut = 119
      OnExecute = ActImprimirChildPadraoExecute
    end
  end
  object dxBarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Finalizar'
      'Acao'
      'Personalizacao')
    Categories.ItemsVisibles = (
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True)
    ImageOptions.Images = DmAcesso.cxImage32x32
    ImageOptions.LargeImages = DmAcesso.cxImage32x32
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 720
    Top = 11
    DockControlHeights = (
      0
      0
      0
      0)
    object BarSair: TdxBar
      Caption = 'Sair'
      CaptionButtons = <>
      DockedLeft = 229
      DockedTop = 0
      FloatLeft = 467
      FloatTop = 200
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object BarAcao: TdxBar
      Caption = 'A'#231#227'o'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarPersonalizacoes: TdxBar
      Caption = 'Personaliza'#231#227'o'
      CaptionButtons = <>
      DockedLeft = 141
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'siPersonalizar'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = ActClose
      Category = 0
    end
    object bbParametrosFormulario: TdxBarButton
      Action = ActParametrizarFormulario
      Category = 0
    end
    object bbHelpImplantacao: TdxBarButton
      Action = ActInformacaoImplantacao
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActConfirm
      Category = 1
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = ActCancel
      Category = 1
    end
    object lbImprimirChildPadrao: TdxBarLargeButton
      Action = ActImprimirChildPadrao
      Category = 1
    end
    object siPersonalizar: TdxBarSubItem
      Action = ActPersonalizar
      Category = 2
      Images = DmAcesso.cxImage16x16
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bbParametrosFormulario'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbHelpImplantacao'
        end>
    end
  end
  object alAcoes16x16: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 777
    Top = 10
    object ActParametrizarFormulario: TAction
      Caption = 'Par'#226'metros do Formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActParametrizarFormularioExecute
    end
    object ActInformacaoImplantacao: TAction
      Caption = 'Help Implanta'#231#227'o'
      ImageIndex = 85
      OnExecute = ActInformacaoImplantacaoExecute
    end
  end
end
