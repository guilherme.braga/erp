unit uFrmModalPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, System.Actions, Vcl.ActnList, cxGroupBox, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrmModalPadrao = class(TForm)
    cxGroupBox2: TcxGroupBox;
    ActionList: TActionList;
    ActConfirmar: TAction;
    ActCancelar: TAction;
    cxGroupBox1: TcxGroupBox;
    BtnConfirmar: TcxButton;
    BtnCancelar: TcxButton;
    procedure ActConfirmarExecute(Sender: TObject);
    procedure ActCancelarExecute(Sender: TObject);
  private
    FResult: Integer;
    { Private declarations }
  public

  protected
    property result: Integer read FResult write FResult;

    procedure Confirmar; virtual;
    procedure Cancelar; virtual;
  end;

const
  //Results
  MCONFIRMED: Integer = 1;
  MCANCELED:  Integer = 0;

var
  FrmModalPadrao: TFrmModalPadrao;

implementation

{$R *.dfm}

uses uDmAcesso, uDmConnection;

procedure TFrmModalPadrao.ActCancelarExecute(Sender: TObject);
begin
  Cancelar;
  result := MCANCELED;
  Close;
end;

procedure TFrmModalPadrao.ActConfirmarExecute(Sender: TObject);
begin
  Confirmar;
  result := MCONFIRMED;
  Close;
end;

procedure TFrmModalPadrao.Cancelar;
begin
  //Implementar na herdada
end;

procedure TFrmModalPadrao.Confirmar;
begin
  //Implementar na herdada
end;

end.
