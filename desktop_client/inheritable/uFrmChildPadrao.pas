unit uFrmChildPadrao;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  cxGroupBox,
  dxSkinsdxBarPainter,
  dxBar,
  cxClasses,
  dxRibbon,
  ActnList,
  cxStyles,
  dxSkinscxPCPainter,
  cxCustomData,
  cxFilter,
  cxData,
  cxDataStorage,
  DB,
  cxDBData,
  cxVGrid,
  cxGridLevel,
  cxGridCustomView,
  cxGridCustomTableView,
  cxGridTableView,
  cxGridDBTableView,
  cxGrid,
  cxInplaceContainer,
  DBClient,
  cxRadioGroup,
  cxDBEdit,
  cxMaskEdit,
  cxTextEdit,
  cxCalendar,
  SqlExpr,
  FMTBcd,
  uTTranslate,
  uDmAcesso, dxRibbonSkins, dxSkinsdxRibbonPainter, System.Actions,
  dxRibbonCustomizationForm, uUsuarioDesignControl, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrmChildPadrao = class(TForm)
    ActionListMain: TActionList;
    ActClose: TAction;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager: TdxBarManager;
    dxBarLargeButton5: TdxBarLargeButton;
    BarSair: TdxBar;
    cxPanelMain: TcxGroupBox;
    BarAcao: TdxBar;
    ActConfirm: TAction;
    ActCancel: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarPersonalizacoes: TdxBar;
    siPersonalizar: TdxBarSubItem;
    ActPersonalizar: TAction;
    alAcoes16x16: TActionList;
    ActParametrizarFormulario: TAction;
    bbParametrosFormulario: TdxBarButton;
    ActInformacaoImplantacao: TAction;
    bbHelpImplantacao: TdxBarButton;
    lbImprimirChildPadrao: TdxBarLargeButton;
    ActImprimirChildPadrao: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActCloseExecute(Sender: TObject);
    procedure ActConfirmExecute(Sender: TObject);
    procedure ActCancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActPersonalizarExecute(Sender: TObject);
    procedure ActParametrizarFormularioExecute(Sender: TObject);
    procedure ActInformacaoImplantacaoExecute(Sender: TObject);
    procedure ActImprimirChildPadraoExecute(Sender: TObject);
  private
    procedure CarregarParametrosFormulario(AParametros: TListaParametroFormulario);
  public
    FResult: Integer;
  protected
    FParametros: TListaParametroFormulario;
    procedure CarregarPersonalizacaoGrid;virtual;
    procedure SalvarPersonalizacaoGrid;virtual;
    procedure AoCriarFormulario; virtual;
    procedure AoFecharFormulario; virtual;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); virtual;
    procedure AoDestruirFormulario; virtual;
    procedure MostrarInformacoesImplantacao; virtual;
    procedure ValidarParametros; virtual;
    procedure InstanciarFrames;virtual;
    procedure Imprimir; virtual;
end;

var
  FrmChildPadrao: TFrmChildPadrao;

implementation

uses uTVariables, uTControl_Function, uSistema,
  uFrmParametrosFormulario, uTMessage;
{$R *.dfm}

procedure TFrmChildPadrao.ActCancelExecute(Sender: TObject);
begin
  FResult := Variables.CCANCEL;
  Close;
end;

procedure TFrmChildPadrao.ActCloseExecute(Sender: TObject);
begin
  FResult := Variables.CCLOSE;
  Close;
end;

procedure TFrmChildPadrao.ActConfirmExecute(Sender: TObject);
begin
  FResult := Variables.CCONFIRM;
  Close;
end;

procedure TFrmChildPadrao.ActImprimirChildPadraoExecute(Sender: TObject);
begin
  Imprimir;
end;

procedure TFrmChildPadrao.ActInformacaoImplantacaoExecute(Sender: TObject);
begin
  MostrarInformacoesImplantacao;
end;

procedure TFrmChildPadrao.ActParametrizarFormularioExecute(Sender: TObject);
begin
  tFrmParametrosFormulario.Parametrizar(Self.Name,
    TSistema.Sistema.Usuario.IdSeguranca);
end;

procedure TFrmChildPadrao.ActPersonalizarExecute(Sender: TObject);
begin
//
end;

procedure TFrmChildPadrao.AoCriarFormulario;
begin
  ActPersonalizar.Visible := TSistema.Sistema.Usuario.Administrador;
  dxBarPersonalizacoes.Visible := dxBarPersonalizacoes.ItemLinks.VisibleItemCount > 0;

  FParametros := TListaParametroFormulario.Create;

  CriarParametrosFormulario(FParametros);
  TUsuarioParametrosFormulario.CriarParametro(FParametros,
    TSistema.Sistema.Usuario.IdSeguranca, Self.Name);
  //CarregarParametrosFormulario(FParametros);
  ValidarParametros;

  InstanciarFrames;
  //Implementar na heranca
end;

procedure TFrmChildPadrao.AoDestruirFormulario;
begin
  dxRibbon1.ShowTabGroups := false;
  Application.ProcessMessages;

  if Assigned(FParametros) then
    FreeAndNil(FParametros);
end;

procedure TFrmChildPadrao.AoFecharFormulario;
begin
  //Implementar na heranca
end;

procedure TFrmChildPadrao.CarregarParametrosFormulario(
  AParametros: TListaParametroFormulario);
begin
  TUsuarioParametrosFormulario.CarregarParametros(FParametros,
    TSistema.Sistema.Usuario.IdSeguranca, Self.Name);
end;

procedure TFrmChildPadrao.CarregarPersonalizacaoGrid;
begin
  //Implementar na heranca
end;

procedure TFrmChildPadrao.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin

end;

procedure TFrmChildPadrao.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  AoFecharFormulario;
  AoDestruirFormulario;
  Action := caFree;
  TControlFunction.RemoveInstance(Self.ClassName);
  Application.MainForm.SetFocus;
end;

procedure TFrmChildPadrao.FormCreate(Sender: TObject);
begin
  AoCriarFormulario;
end;

procedure TFrmChildPadrao.Imprimir;
begin
  //Implementar na heran�a
end;

procedure TFrmChildPadrao.InstanciarFrames;
begin

end;

procedure TFrmChildPadrao.MostrarInformacoesImplantacao;
begin

end;

procedure TFrmChildPadrao.SalvarPersonalizacaoGrid;
begin
  //Implementar na heranca
end;

procedure TFrmChildPadrao.ValidarParametros;
var mensagemParametrosDesconfigurados: String;
begin
  if not FParametros.ParametrosConfigurados(mensagemParametrosDesconfigurados) then
  begin
    TMessage.MessageFailure(mensagemParametrosDesconfigurados);
  end;
end;

Initialization
  RegisterClass( TFrmChildPadrao );
Finalization
  UnRegisterClass( TFrmChildPadrao );

end.
