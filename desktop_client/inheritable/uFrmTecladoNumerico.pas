unit uFrmTecladoNumerico;

interface

uses
  //System
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ExtCtrls,

  //DevExpress
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  cxGroupBox,
  cxTextEdit,

  //JVCL
  JvExControls,
  JvButton,
  JvTransparentButton, Keyboard;


type
  TFrmTecladoNumerico = class(TForm)
    cxGBDados: TcxGroupBox;
    PnText: TPanel;
    EdtVisor: TcxTextEdit;
    TouchKeyboard1: TTouchKeyboard;
    procedure EdtVisorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTecladoNumerico: TFrmTecladoNumerico;

implementation

{$R *.dfm}

procedure TFrmTecladoNumerico.EdtVisorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    begin
      if EdtVisor.Text = '' then EdtVisor.Text := '0';
      Close;
    end;
end;

end.
