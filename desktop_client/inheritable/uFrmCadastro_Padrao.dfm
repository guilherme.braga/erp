object FrmCadastro_Padrao: TFrmCadastro_Padrao
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'FrmCadastro_Padrao'
  ClientHeight = 484
  ClientWidth = 936
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxMainRibbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 936
    Height = 127
    BarManager = dxBarManagerPadrao
    Style = rs2010
    ColorSchemeName = 'Blue'
    PopupMenuItems = []
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxGerencia: TdxRibbonTab
      Active = True
      Caption = 'Ger'#234'ncia'
      Groups = <
        item
          ToolbarName = 'dxBarNavegacaoData'
        end
        item
          ToolbarName = 'dxBarManager'
        end
        item
          ToolbarName = 'dxBarAction'
        end
        item
          ToolbarName = 'dxBarSearch'
        end
        item
          ToolbarName = 'dxBarReport'
        end
        item
          ToolbarName = 'dxBarPersonalizacoes'
        end
        item
          ToolbarName = 'dxBarEnd'
        end>
      Index = 0
    end
    object dxDesign: TdxRibbonTab
      Caption = 'Personaliza'#231#227'o'
      Groups = <
        item
          ToolbarName = 'dxDesignDesign'
        end>
      Index = 1
    end
  end
  object cxpcMain: TcxPageControl
    Left = 0
    Top = 127
    Width = 936
    Height = 357
    Align = alClient
    Focusable = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    TabStop = False
    Properties.ActivePage = cxtsSearch
    Properties.CustomButtons.Buttons = <>
    Properties.NavigatorPosition = npLeftTop
    Properties.Style = 8
    OnChange = cxpcMainChange
    OnEnter = cxpcMainEnter
    ClientRectBottom = 357
    ClientRectRight = 936
    ClientRectTop = 24
    object cxtsSearch: TcxTabSheet
      Caption = 'Pesquisar'
      ImageIndex = 0
      object cxGridPesquisaPadrao: TcxGrid
        Left = 32
        Top = 0
        Width = 904
        Height = 333
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Images = DmAcesso.cxImage16x16
        ParentFont = False
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object Level1BandedTableView1: TcxGridDBBandedTableView
          OnDblClick = Level1BandedTableView1DblClick
          Navigator.Buttons.OnButtonClick = Level1BandedTableView1NavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <
            item
              Hint = 'Realiza a impress'#227'o desta grade'
              ImageIndex = 12
            end
            item
              Hint = 'Exportar grade para arquivo excel'
              ImageIndex = 274
            end
            item
              Hint = 'Exportar grade para arquivo PDF'
              ImageIndex = 273
            end>
          Navigator.Buttons.Images = DmAcesso.cxImage16x16
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          OnEditKeyPress = Level1BandedTableView1EditKeyPress
          DataController.DataSource = dsSearch
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.Active = True
          DataController.Options = [dcoCaseInsensitive, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.ApplyChanges = fracImmediately
          Images = DmAcesso.cxImage16x16
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.ExpandMasterRowOnDblClick = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandMoving = False
          OptionsCustomize.NestedBands = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.ShowColumnFilterButtons = sfbAlways
          OptionsView.BandHeaders = False
          Styles.ContentOdd = DmAcesso.OddColor
          Bands = <
            item
            end>
        end
        object cxGridPesquisaPadraoLevel1: TcxGridLevel
          GridView = Level1BandedTableView1
        end
      end
      object pnFiltroVertical: TgbPanel
        Left = 0
        Top = 0
        Align = alLeft
        PanelStyle.Active = True
        PanelStyle.OfficeBackgroundKind = pobkGradient
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 1
        Transparent = True
        Height = 333
        Width = 24
        object pnAcoesFiltroVertical: TgbPanel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Align = alTop
          PanelStyle.Active = True
          PanelStyle.OfficeBackgroundKind = pobkGradient
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Transparent = True
          Height = 26
          Width = 14
          object btnLimparFiltroVerticalPadrao: TJvTransparentButton
            Left = 2
            Top = 2
            Width = 95
            Height = 22
            Action = ActLimparFiltroVertical
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            TextAlign = ttaRight
            Images.ActiveImage = DmAcesso.cxImage16x16
            Images.ActiveIndex = 17
            Images.GrayIndex = 17
            Images.DisabledIndex = 17
            Images.DownIndex = 17
            Images.HotIndex = 17
          end
        end
      end
      object spFiltroVertical: TcxSplitter
        Left = 24
        Top = 0
        Width = 8
        Height = 333
        HotZoneClassName = 'TcxMediaPlayer9Style'
        AllowHotZoneDrag = False
        MinSize = 0
        Control = pnFiltroVertical
      end
    end
    object cxtsData: TcxTabSheet
      Caption = 'Dados'
      ImageIndex = 1
      object DesignPanel: TJvDesignPanel
        Left = 0
        Top = 0
        Width = 936
        Height = 333
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object cxGBDadosMain: TcxGroupBox
          Left = 0
          Top = 0
          Align = alClient
          PanelStyle.Active = True
          Style.BorderStyle = ebsNone
          Style.LookAndFeel.Kind = lfOffice11
          Style.Shadow = False
          Style.TransparentBorder = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.Kind = lfOffice11
          TabOrder = 0
          Height = 333
          Width = 936
          object dxBevel1: TdxBevel
            Left = 2
            Top = 2
            Width = 932
            Height = 32
            Align = alTop
            Shape = dxbsLineBottom
          end
        end
      end
    end
  end
  object ActionListAssistent: TActionList
    Images = DmAcesso.cxImage16x16
    Left = 870
    Top = 32
    object ActSalvarConfiguracoes: TAction
      Category = 'filter'
      Caption = 'Salvar Configura'#231#245'es'
      Hint = 'Salva as configura'#231#245'es aplicadas na grade'
      ImageIndex = 13
      OnExecute = ActSalvarConfiguracoesExecute
    end
    object ActRestaurarColunasPadrao: TAction
      Category = 'filter'
      Caption = 'Restaurar Colunas'
      ImageIndex = 13
      OnExecute = ActRestaurarColunasPadraoExecute
    end
    object ActAlterarColunasGrid: TAction
      Category = 'filter'
      Caption = 'Alterar R'#243'tulo das Colunas'
      ImageIndex = 13
      OnExecute = ActAlterarColunasGridExecute
    end
    object ActExibirAgrupamento: TAction
      Category = 'filter'
      Caption = 'Exibir Agrupamento'
      Hint = 'Exibe/Oculta o agrupamento de colunas'
      ImageIndex = 13
      OnExecute = ActExibirAgrupamentoExecute
    end
    object ActAbrirConsultando: TAction
      Category = 'filter'
      Caption = 'Consultar Autom'#225'ticamente'
      Hint = 'Realiza a consulta de dados ao abrir o formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActAbrirConsultandoExecute
    end
    object ActAlterarSQLPesquisaPadrao: TAction
      Category = 'filter'
      Caption = 'Alterar SQL'
      Hint = 'Alterar SQL da consulta de dados'
      ImageIndex = 13
      OnExecute = ActAlterarSQLPesquisaPadraoExecute
    end
    object ActFullExpand: TAction
      Category = 'filter'
      Caption = 'Expandir Grupos'
      Hint = 'Expandir/Contrair agrupamentos'
      ImageIndex = 13
    end
    object ActConfigurarValoresDefault: TAction
      Category = 'filter'
      Caption = 'Configurar Valores Padr'#245'es'
      Hint = 'Configurar valores que ser'#227'o preenchidos em um novo registro'
      ImageIndex = 13
      OnExecute = ActConfigurarValoresDefaultExecute
    end
    object ActParametroFormulario: TAction
      Category = 'filter'
      Caption = 'Par'#226'metros do Formul'#225'rio'
      Hint = 'Parametrizar o formul'#225'rio'
      ImageIndex = 13
      OnExecute = ActParametroFormularioExecute
    end
    object ActExibirTotalizadorGrade: TAction
      Category = 'filter'
      Caption = 'Exibir Totalizador'
      Hint = 'Exibe/Oculta o rodap'#233' totalizador da grade'
      ImageIndex = 13
      OnExecute = ActExibirTotalizadorGradeExecute
    end
    object ActInformacaoImplatacao: TAction
      Category = 'filter'
      Caption = 'Help Implanta'#231#227'o'
      ImageIndex = 85
      OnExecute = ActInformacaoImplatacaoExecute
    end
    object ActTotalizadorTotalizarPorSum: TAction
      Category = 'filter'
      Caption = 'Somar'
      Visible = False
      OnExecute = ActTotalizadorTotalizarPorSumExecute
    end
    object ActLimparFiltroVertical: TAction
      Category = 'filter'
      Caption = 'Limpar &Filtros'
      ImageIndex = 17
      OnExecute = ActLimparFiltroVerticalExecute
    end
  end
  object dxBarManagerPadrao: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Action'
      'Manager'
      'Report'
      'End'
      'Search'
      'Design'
      'ActionDesign'
      'Navegacao'
      'Personalizacoes')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    FlatCloseButton = True
    ImageOptions.LargeImages = DmAcesso.cxImage32x32
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    PopupMenuLinks = <>
    ShowHelpButton = True
    ShowShortCutInHint = True
    Style = bmsOffice11
    UseF10ForMenu = False
    UseSystemFont = True
    Left = 672
    Top = 32
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager: TdxBar
      Caption = 'Gest'#227'o '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 66
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbVisualizar'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxlbInsert'
        end
        item
          Visible = True
          ItemName = 'dxlbUpdate'
        end
        item
          Visible = True
          ItemName = 'dxlbRemove'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      UseRecentItems = False
      Visible = True
      WholeRow = False
    end
    object dxBarAction: TdxBar
      Caption = 'A'#231#227'o'
      CaptionButtons = <>
      DockedLeft = 232
      DockedTop = 0
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 85
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbAccept'
        end
        item
          Visible = True
          ItemName = 'dxlbCancel'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarReport: TdxBar
      Caption = 'Relat'#243'rio'
      CaptionButtons = <>
      DockedLeft = 425
      DockedTop = 0
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 83
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbReport'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarEnd: TdxBar
      Caption = 'Finalizar'
      CaptionButtons = <>
      DockedLeft = 596
      DockedTop = 0
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 56
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSearch: TdxBar
      Caption = 'Consultar'
      CaptionButtons = <>
      DockedLeft = 350
      DockedTop = 0
      FloatLeft = 944
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxlbSearch'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxDesignDesign: TdxBar
      Caption = 'Design'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 493
      FloatTop = 281
      FloatClientWidth = 121
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarNavegacaoData: TdxBar
      Caption = 'Navega'#231#227'o'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 970
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lbNavegacaoDataPrimeiro'
        end
        item
          Visible = True
          ItemName = 'lbNavegacaoDataAnterior'
        end
        item
          Visible = True
          ItemName = 'lbNavegacaoDataProximo'
        end
        item
          Visible = True
          ItemName = 'lbNavegacaoDataUltimo'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarPersonalizacoes: TdxBar
      Caption = 'Personaliza'#231#245'es'
      CaptionButtons = <>
      DockedLeft = 504
      DockedTop = 0
      FloatLeft = 970
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'siPersonalizarPadrao'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxlbAccept: TdxBarLargeButton
      Action = ActConfirm
      Category = 0
    end
    object dxlbCancel: TdxBarLargeButton
      Action = ActCancel
      Category = 0
    end
    object bbExibirRodape: TdxBarButton
      Action = ActExibirTotalizadorGrade
      Category = 0
    end
    object dxlbInsert: TdxBarLargeButton
      Action = ActInsert
      Category = 1
    end
    object dxlbUpdate: TdxBarLargeButton
      Action = ActUpdate
      Category = 1
    end
    object dxlbRemove: TdxBarLargeButton
      Action = ActRemove
      Caption = '&Excluir F4'
      Category = 1
    end
    object lbVisualizar: TdxBarLargeButton
      Action = ActVisualizarRegistro
      Category = 1
    end
    object lbDuplicarRegistro: TdxBarLargeButton
      Action = ActDuplicarRegistro
      Category = 1
    end
    object dxlbReport: TdxBarLargeButton
      Action = ActReport
      Category = 2
    end
    object dxlbClose: TdxBarLargeButton
      Action = ActClose
      Category = 3
    end
    object dxlbSearch: TdxBarLargeButton
      Action = ActSearch
      Category = 4
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActDesign
      Category = 5
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = ActConfirmDesign
      Category = 6
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = ActCancelDesign
      Category = 6
    end
    object lbNavegacaoDataPrimeiro: TdxBarLargeButton
      Action = ActNavegacaoDataPrimeiro
      Category = 7
    end
    object lbNavegacaoDataAnterior: TdxBarLargeButton
      Action = ActNavegacaoDataAnterior
      Category = 7
    end
    object lbNavegacaoDataProximo: TdxBarLargeButton
      Action = ActNavegacaoDataProximo
      Category = 7
    end
    object lbNavegacaoDataUltimo: TdxBarLargeButton
      Action = ActNavegacaoDataUltimo
      Category = 7
    end
    object dxBarListItem1: TdxBarListItem
      Caption = 'Personalizar'
      Category = 8
      Visible = ivAlways
      LargeImageIndex = 32
      Items.Strings = (
        'SQL da Pesquisa'
        'Valores ao Incluir'
        'Parametriza'#231#245'es'
        '')
      ShowNumbers = False
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 8
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 8
      Hint = 'New Button'
      Visible = ivAlways
    end
    object siPersonalizarPadrao: TdxBarSubItem
      Action = ActPersonalizacoes
      Category = 8
      Images = DmAcesso.cxImage16x16
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bbAlterarSQL'
        end
        item
          Visible = True
          ItemName = 'bbAlterarRotuloColunas'
        end
        item
          Visible = True
          ItemName = 'bbConfigurarValorPadrao'
        end
        item
          Visible = True
          ItemName = 'bbConsultarAutomaticamente'
        end
        item
          Visible = True
          ItemName = 'bbExibirAgrupamento'
        end
        item
          Visible = True
          ItemName = 'bbExibirRodape'
        end
        item
          Visible = True
          ItemName = 'bbParametroFormulario'
        end
        item
          Visible = True
          ItemName = 'bbRestaurarColunas'
        end
        item
          Visible = True
          ItemName = 'bbSalvarConfiguracoes'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbHelpImplantacao'
        end>
    end
    object bbConsultarAutomaticamente: TdxBarButton
      Action = ActAbrirConsultando
      Category = 8
    end
    object bbAlterarRotuloColunas: TdxBarButton
      Action = ActAlterarColunasGrid
      Category = 8
    end
    object bbAlterarSQL: TdxBarButton
      Action = ActAlterarSQLPesquisaPadrao
      Category = 8
    end
    object bbConfigurarValorPadrao: TdxBarButton
      Action = ActConfigurarValoresDefault
      Category = 8
    end
    object bbExibirAgrupamento: TdxBarButton
      Action = ActExibirAgrupamento
      Category = 8
    end
    object bbRestaurarColunas: TdxBarButton
      Action = ActRestaurarColunasPadrao
      Category = 8
    end
    object bbSalvarConfiguracoes: TdxBarButton
      Action = ActSalvarConfiguracoes
      Category = 8
    end
    object bbParametroFormulario: TdxBarButton
      Action = ActParametroFormulario
      Category = 8
    end
    object bbHelpImplantacao: TdxBarButton
      Action = ActInformacaoImplatacao
      Category = 8
    end
  end
  object dsSearch: TDataSource
    DataSet = fdmSearch
    Left = 814
    Top = 32
  end
  object dsData: TDataSource
    DataSet = cdsData
    Left = 758
    Top = 32
  end
  object ActionListMain: TActionList
    Images = DmAcesso.cxImage32x32
    Left = 842
    Top = 32
    object ActConfirm: TAction
      Category = 'Action'
      Caption = 'Sa&lvar F5'
      Hint = 'Salvar as altera'#231#245'es efetuadas'
      ImageIndex = 13
      ShortCut = 116
      OnExecute = ActConfirmExecute
    end
    object ActClose: TAction
      Category = 'End'
      Caption = '&Sair Esc'
      Hint = 'Fechar esse formul'#225'rio'
      ImageIndex = 12
      ShortCut = 27
      OnExecute = ActCloseExecute
    end
    object ActInsert: TAction
      Category = 'Manager'
      Caption = '&Incluir F2'
      Hint = 'Inserir um novo registro'
      ImageIndex = 2
      ShortCut = 113
      OnExecute = ActInsertExecute
    end
    object ActReport: TAction
      Category = 'Report'
      Caption = '&Impress'#227'o F8'
      Hint = 'Visualizar/Imprimir arquivos referente a esta tela'
      ImageIndex = 115
      ShortCut = 119
      OnExecute = ActReportExecute
    end
    object ActCancel: TAction
      Category = 'Action'
      Caption = 'Ca&ncelar Esc'
      Hint = 'Cancelar as altera'#231#245'es efetuadas'
      ImageIndex = 14
      ShortCut = 27
      OnExecute = ActCancelExecute
    end
    object ActUpdate: TAction
      Category = 'Manager'
      Caption = '&Alterar F3'
      Hint = 'Alterar um registro existente'
      ImageIndex = 4
      ShortCut = 114
      OnExecute = ActUpdateExecute
    end
    object ActRemove: TAction
      Category = 'Manager'
      Caption = '&Excluir F5'
      Hint = 'Remover um registro existente'
      ImageIndex = 8
      ShortCut = 115
      OnExecute = ActRemoveExecute
    end
    object ActConfirmDesign: TAction
      Category = 'DesignAction'
      Caption = '&Confirmar (Enter)'
      Enabled = False
      Hint = 'Confirmar Personaliza'#231#227'o'
      ImageIndex = 13
      ShortCut = 13
    end
    object ActCancelDesign: TAction
      Category = 'DesignAction'
      Caption = 'Ca&ncelar Ctrl+F6'
      Enabled = False
      Hint = 'Cancelar Personaliza'#231#227'o'
      ImageIndex = 14
      ShortCut = 16501
    end
    object ActDesign: TAction
      Category = 'Design'
      Caption = '&Personalizar Ctrl+F2'
      ImageIndex = 33
      ShortCut = 16497
    end
    object ActSearch: TAction
      Category = 'Search'
      Caption = '&Pesquisar F7'
      Hint = 'Consultar registros'
      ImageIndex = 16
      ShortCut = 118
      OnExecute = ActSearchExecute
    end
    object ActVisualizarRegistro: TAction
      Category = 'Manager'
      Caption = 'Visualizar F1'
      Hint = 'Visualizar as informa'#231#245'es do registro selecionado'
      ImageIndex = 28
      ShortCut = 112
      OnExecute = ActVisualizarRegistroExecute
    end
    object ActNavegacaoDataPrimeiro: TAction
      Category = 'Navegacao'
      Caption = 'Primeiro Ctrl+Home'
      Hint = 'Navegar para o primeiro registro filtrado'
      ImageIndex = 25
      ShortCut = 16420
      OnExecute = ActNavegacaoDataPrimeiroExecute
    end
    object ActNavegacaoDataAnterior: TAction
      Category = 'Navegacao'
      Caption = 'Anterior Ctrl+PgUp'
      Hint = 'Navegar para o registro anterior filtrado'
      ImageIndex = 18
      ShortCut = 16417
      OnExecute = ActNavegacaoDataAnteriorExecute
    end
    object ActNavegacaoDataProximo: TAction
      Category = 'Navegacao'
      Caption = 'Pr'#243'ximo Ctrl+PgDn'
      Hint = 'Navegar para o pr'#243'ximo registro filtrado'
      ImageIndex = 17
      ShortCut = 16418
      OnExecute = ActNavegacaoDataProximoExecute
    end
    object ActNavegacaoDataUltimo: TAction
      Category = 'Navegacao'
      Caption = #218'ltimo Ctrl+End'
      Hint = 'Navegar para o '#250'ltimo registro filtrado'
      ImageIndex = 26
      ShortCut = 16419
      OnExecute = ActNavegacaoDataUltimoExecute
    end
    object ActPersonalizacoes: TAction
      Category = 'Personalizacoes'
      Caption = 'Personalizar'
      Hint = 'Personalizar formul'#225'rio'
      ImageIndex = 33
      OnExecute = ActPersonalizacoesExecute
    end
    object ActDuplicarRegistro: TAction
      Category = 'Manager'
      Caption = '&Duplicar Ctrl+F2'
      Hint = 'Duplicar o registro selecionado'
      ImageIndex = 5
      ShortCut = 16497
      OnExecute = ActDuplicarRegistroExecute
    end
  end
  object UCCadastro_Padrao: TUCControls
    UserControl = FrmPrincipal.UserControl
    Components = ''
    Left = 616
    Top = 32
  end
  object pmTituloGridPesquisaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmTituloGridPesquisaPadraoPopup
    Left = 700
    Top = 32
    object AlterarRtulodasColunas1: TMenuItem
      Action = ActAlterarColunasGrid
      SubMenuImages = DmAcesso.cxImage16x16
    end
    object ExibirAgrupamento2: TMenuItem
      Action = ActExibirAgrupamento
    end
    object SeparadorMenuPadraoCPN1: TMenuItem
      Caption = '-'
    end
    object ExibirTotalizador1: TMenuItem
      Action = ActExibirTotalizadorGrade
    end
    object otalizarPor1: TMenuItem
      Caption = 'M'#233'todo de Totalizador'
      object Somar1: TMenuItem
        Action = ActTotalizadorTotalizarPorSum
      end
    end
    object SeparadorMenuPadraoCPN2: TMenuItem
      Caption = '-'
    end
    object RestaurarColunasPadrao: TMenuItem
      Action = ActRestaurarColunasPadrao
    end
    object SalvarConfiguraes1: TMenuItem
      Action = ActSalvarConfiguracoes
    end
  end
  object cxGridPopupMenuPadrao: TcxGridPopupMenu
    Grid = cxGridPesquisaPadrao
    PopupMenus = <
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtColumnHeader]
        Index = 0
        PopupMenu = pmTituloGridPesquisaPadrao
      end
      item
        GridView = Level1BandedTableView1
        HitTypes = [gvhtNone]
        Index = 1
        PopupMenu = pmGridConsultaPadrao
      end>
    Left = 644
    Top = 32
  end
  object cdsData: TGBClientDataSet
    Aggregates = <>
    Params = <>
    AfterEdit = cdsDataAfterEdit
    OnDeleteError = cdsDataDeleteError
    OnEditError = cdsDataEditError
    OnPostError = cdsDataPostError
    OnReconcileError = cdsDataReconcileError
    AfterApplyUpdates = cdsDataAfterApplyUpdates
    gbUsarDefaultExpression = True
    gbValidarCamposObrigatorios = True
    Left = 728
    Top = 32
  end
  object fdmSearch: TFDMemTable
    AfterOpen = fdmSearchAfterOpen
    AfterClose = fdmSearchAfterClose
    AfterDelete = fdmSearchAfterDelete
    FieldOptions.PositionMode = poFirst
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 786
    Top = 32
  end
  object pmGridConsultaPadrao: TPopupMenu
    Images = DmAcesso.cxImage16x16
    OnPopup = pmGridConsultaPadraoPopup
    Left = 696
    Top = 80
    object AlterarSQL1: TMenuItem
      Action = ActAlterarSQLPesquisaPadrao
    end
    object ExibirAgrupamento1: TMenuItem
      Action = ActAbrirConsultando
    end
    object ConfigurarValoresPadres1: TMenuItem
      Action = ActConfigurarValoresDefault
    end
  end
  object dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Version = 0
    Left = 632
    Top = 72
    object dxComponentPrinterLink1: TdxGridReportLink
      Active = True
      Component = cxGridPesquisaPadrao
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.GrayShading = True
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42400.430498703700000000
      ShrinkToPageWidth = True
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsOnEveryPage.BandHeaders = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.BandHeaders = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
end
