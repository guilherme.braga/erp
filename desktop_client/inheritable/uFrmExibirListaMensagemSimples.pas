unit uFrmExibirListaMensagemSimples;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModalPadrao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  cxButtons, cxGroupBox, Vcl.ComCtrls, cxTreeView;

type
  TFrmExibirListaMensagemSimples = class(TFrmModalPadrao)
    PnTopo: TcxGroupBox;
    PnMensagens: TcxGroupBox;
    ViewListaMensagens: TcxTreeView;
  private
  public
    class procedure ExibirListaMensagem(const ATituloFormulario: String;
      const AListaMensagem: TStringList; const ATituloPainelAdvertencia: String = '');

    procedure PopuparListaMensagem(const AListaMensagem: TStringList);
    procedure AlterarTituloFormulario(const ATitulo: String);
    procedure ConfigurarPainelAdvertencia(const ATituloPainelAdvertencia: String = '');
  end;

var
  FrmExibirListaMensagemSimples: TFrmExibirListaMensagemSimples;

implementation

{$R *.dfm}

uses uTVariables, uTControl_Function;

{ TFrmExibirMensagemTreeView }

procedure TFrmExibirListaMensagemSimples.AlterarTituloFormulario(const ATitulo: String);
begin
  Self.Caption := ATitulo;
end;

procedure TFrmExibirListaMensagemSimples.ConfigurarPainelAdvertencia(const ATituloPainelAdvertencia: String);
begin
  PnTopo.Caption := ATituloPainelAdvertencia;
  PnTopo.Visible := not ATituloPainelAdvertencia.IsEmpty;
end;

class procedure TFrmExibirListaMensagemSimples.ExibirListaMensagem(const ATituloFormulario: String;
  const AListaMensagem: TStringList; const ATituloPainelAdvertencia: String = '');
begin
  Application.CreateForm(TFrmExibirListaMensagemSimples, FrmExibirListaMensagemSimples);
  try
    FrmExibirListaMensagemSimples.AlterarTituloFormulario(ATituloFormulario);
    FrmExibirListaMensagemSimples.ConfigurarPainelAdvertencia(ATituloPainelAdvertencia);
    FrmExibirListaMensagemSimples.PopuparListaMensagem(AListaMensagem);
    FrmExibirListaMensagemSimples.ShowModal;
  finally
    FreeAndNil(FrmExibirListaMensagemSimples);
  end;
end;

procedure TFrmExibirListaMensagemSimples.PopuparListaMensagem(const AListaMensagem: TStringList);
var
  ArquivoStream: TStream;
begin
  ArquivoStream := TMemoryStream.Create;
  try
    ArquivoStream.Position := 0;

    AListaMensagem.SaveToStream(ArquivoStream);

    ArquivoStream.Position := 0;

    ViewListaMensagens.LoadFromStream(ArquivoStream);
  finally
    FreeAndNil(ArquivoStream);
  end;
end;

end.
