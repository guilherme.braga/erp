unit uFrmRelatorioFRPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, Datasnap.DBClient, uGBClientDataset, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uFiltroFormulario, cxBarEditItem, cxCalendar, cxButtonEdit,
  dxBarExtItems, cxDropDownEdit, uGBPanel, cxInplaceContainer, cxVGrid,
  cxDBVGrid, System.Generics.Collections, cxTextEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TFrmRelatorioFRPadrao = class(TFrmChildPadrao)
    dsConsultaRelatorioFR: TDataSource;
    cdsConsultaRelatorioFR: TGBClientDataSet;
    cdsConsultaRelatorioFRID: TAutoIncField;
    cdsConsultaRelatorioFRDESCRICAO: TStringField;
    cdsConsultaRelatorioFRNOME: TStringField;
    cxGridRelatorios: TcxGrid;
    cxGridRelatoriosView: TcxGridDBTableView;
    cxGridRelatoriosViewID: TcxGridDBColumn;
    cxGridRelatoriosViewNOME: TcxGridDBColumn;
    cxGridRelatoriosViewDESCRICAO: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    ActImprimir: TAction;
    ActVisualizar: TAction;
    lbImprimir: TdxBarLargeButton;
    lbVisualizar: TdxBarLargeButton;
    BarFiltro: TdxBar;
    filtroDataTermino: TcxBarEditItem;
    filtroDataInicio: TcxBarEditItem;
    filtroGrupoCentroResultado: TcxBarEditItem;
    filtroCentroResultado: TcxBarEditItem;
    filtroContaAnalise: TcxBarEditItem;
    filtroLabelDataMovimento: TdxBarStatic;
    filtroContaCorrente: TcxBarEditItem;
    filtroConciliado: TcxBarEditItem;
    filtroTipoMovimento: TcxBarEditItem;
    filtroDescricaoCentroResultado: TdxBarStatic;
    filtroDescricaoGrupoCentroResultado: TdxBarStatic;
    filtroDescricaoContaAnalise: TdxBarStatic;
    filtroDescricaoContaCorrente: TdxBarStatic;
    gbPanel1: TgbPanel;
    vgFiltros: TcxVerticalGrid;
    FdmFiltros: TFDMemTable;
    vcatFiltrosPadrao: TcxCategoryRow;
    procedure ActImprimirExecute(Sender: TObject);
    procedure ActVisualizarExecute(Sender: TObject);
  private
    FWhereSearch: TFiltroPadrao;
    procedure ClearWhereBundle;
    procedure BuscarRelatoriosAssociados;
    procedure AtribuirValoresDaGridParaDataset;

  public
    FListaIndentificadoresConsultaFK: TDictionary<String, String>;
    class procedure CriarFormularioRelatorio;

  protected
    procedure SetWhereBundle; virtual;
    procedure PrepararFiltros; virtual;
    procedure AntesDeImprimir; virtual;
    procedure Imprimir; virtual;
    procedure DepoisDeImprimir; virtual;
    procedure AntesDeVisualizar; virtual;
    procedure Visualizar; virtual;
    procedure DepoisDeVisualizar; virtual;
    procedure AoCriarFormulario; override;
    procedure AoFecharFormulario; override;
    procedure AddFiltroIgual(ACampo: String; AField: TFMTBCDField);overload;
    procedure AddFiltroIgual(ACampo: String; AField: TFloatField);overload;
    procedure AddFiltroIgual(ACampo: String; AField: TStringField);overload;
    procedure AddFiltroIgual(ACampo: String; AField: TIntegerField);overload;
    procedure AddFiltroIgual(ACampo: String; AField: TLargeIntField);overload;
    procedure AddFiltroEntre(ACampo: String; AFieldInicio, AFieldTermino: TDateField);overload;
    procedure ConfigurarCampoConsultaFK(AColuna: TcxEditorRow; AIdentificadorConsulta: String);
    function BuscarColunaPeloNomeCampo(AFieldName: String): TcxEditorRow;

    procedure EventoClicarConsultaFK(Sender: TObject;
      AButtonIndex: Integer); virtual;

        //Métodos da consulta FK
    function GetIdentificadorConsultaFK(ANomeDoFiltro: String): String;
  end;

implementation

{$R *.dfm}

uses uTControl_Function, uRelatorioFR, uSistema, uVerticalGridUtils,
  uDatasetUtils, uFrmMessage_Process, uFrmConsultaPadrao;

{ TFrmRelatorioFRPadrao }

procedure TFrmRelatorioFRPadrao.ActImprimirExecute(Sender: TObject);
begin
  inherited;
  Imprimir;
end;

procedure TFrmRelatorioFRPadrao.ActVisualizarExecute(Sender: TObject);
begin
  inherited;
  Visualizar;
end;

procedure TFrmRelatorioFRPadrao.AddFiltroEntre(ACampo: String; AFieldInicio,
  AFieldTermino: TDateField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AFieldInicio, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) and
     TValidacaoCampo.CampoPreenchido(AFieldInicio, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddEntre(ACampo, AFieldInicio.AsDateTime,
      AFieldTermino.AsDateTime);
end;

procedure TFrmRelatorioFRPadrao.AddFiltroIgual(ACampo: String;
  AField: TFMTBCDField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AField, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddIgual(ACampo, AField.AsFloat);
end;

procedure TFrmRelatorioFRPadrao.AddFiltroIgual(ACampo: String;
  AField: TStringField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AField, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddIgual(ACampo, AField.AsString);
end;

procedure TFrmRelatorioFRPadrao.AddFiltroIgual(ACampo: String;
  AField: TIntegerField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AField, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddIgual(ACampo, AField.AsString);
end;

procedure TFrmRelatorioFRPadrao.AddFiltroIgual(ACampo: String;
  AField: TFloatField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AField, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddIgual(ACampo, AField.AsFloat);
end;

procedure TFrmRelatorioFRPadrao.AntesDeImprimir;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRPadrao.AntesDeVisualizar;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRPadrao.AoCriarFormulario;
begin
  FWhereSearch := TFiltroPadrao.Create;
  FListaIndentificadoresConsultaFK := TDictionary<String, String>.Create;
end;

procedure TFrmRelatorioFRPadrao.AoFecharFormulario;
begin
  if Assigned(FWhereSearch) then
   FreeAndNil(FWhereSearch);

  if Assigned(FListaIndentificadoresConsultaFK) then
    FreeAndNil(FListaIndentificadoresConsultaFK);
end;

procedure TFrmRelatorioFRPadrao.AtribuirValoresDaGridParaDataset;
var
  i: Integer;
  vgRow: TcxEditorRow;
begin
  if FdmFiltros.Active then
  begin
    FdmFiltros.EmptyDataset;
    FdmFiltros.Edit;
  end
  else
  begin
    FdmFiltros.Open;
    FdmFiltros.Insert;
  end;

  if vcatFiltrosPadrao.HasChildren then
  begin
    for i := 0 to Pred(vcatFiltrosPadrao.Count) do
    begin
      vgRow := TcxEditorRow(vcatFiltrosPadrao.Rows[i]);
      if not (vgRow.Properties.Hint.IsEmpty) then
      begin
        if Assigned(FdmFiltros.FindField(vgRow.Properties.Hint)) then
        begin
          if VartoStr(vgRow.Properties.Value).IsEmpty then
            FdmFiltros.FieldByName(vgRow.Properties.Hint).Clear
          else
            FdmFiltros.FieldByName(vgRow.Properties.Hint).AsString :=
              VartoStr(vgRow.Properties.Value);
        end;
      end;
    end;
  end;

  if FdmFiltros.State in dsEditModes then
    FdmFiltros.Post;
end;

function TFrmRelatorioFRPadrao.BuscarColunaPeloNomeCampo(AFieldName: String): TcxEditorRow;
var i: Integer;
begin
  result := nil;

  for i := 0 to Pred(vcatFiltrosPadrao.Count) do
  begin
    if TcxEditorRow(vcatFiltrosPadrao.Rows[i]).Properties.Hint = AFieldName then
    begin
      Result := TcxEditorRow(vcatFiltrosPadrao.Rows[i]);
      break;
    end;
  end;
end;

procedure TFrmRelatorioFRPadrao.BuscarRelatoriosAssociados;
begin
  cdsConsultaRelatorioFR.Close;
  cdsConsultaRelatorioFR.Params.Clear;
  cdsConsultaRelatorioFR.FetchParams;
  cdsConsultaRelatorioFR.Params.ParamByName('NOME').AsString := Self.Name;
  cdsConsultaRelatorioFR.Params.ParamByName('ID_PESSOA_USUARIO').AsInteger :=
    TSistema.Sistema.usuario.idSeguranca;
  cdsConsultaRelatorioFR.Open;
end;

procedure TFrmRelatorioFRPadrao.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TFrmRelatorioFRPadrao.ConfigurarCampoConsultaFK(
  AColuna: TcxEditorRow; AIdentificadorConsulta: String);
begin
  //TVerticalGridUtils.PersonalizarColunaParaButtonEdit(AColuna);
  FListaIndentificadoresConsultaFK.Add(AColuna.Properties.Hint, AIdentificadorConsulta);
  TcxButtonEditProperties(AColuna.Properties).OnButtonClick := EventoClicarConsultaFK;
end;

procedure TFrmRelatorioFRPadrao.Imprimir;
begin
  try
    TFrmMessage_Process.SendMessage('Carregando relatório');
    ActiveControl := nil;

    if cdsConsultaRelatorioFR.isEmpty then
      exit;

    SetWhereBundle;

    TRelatorioFR.ImprimirRelatorio(0, cdsConsultaRelatorioFRID.AsInteger,
      FWhereSearch.where);
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

class procedure TFrmRelatorioFRPadrao.CriarFormularioRelatorio;
begin
  with TFrmRelatorioFRPadrao(TControlFunction.CreateMDIForm(Self.ClassName)) do
  begin
    BuscarRelatoriosAssociados;

    PrepararFiltros;
  end;
end;

procedure TFrmRelatorioFRPadrao.DepoisDeImprimir;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRPadrao.DepoisDeVisualizar;
begin
  //Implementar nos herdados
end;

procedure TFrmRelatorioFRPadrao.PrepararFiltros;
const
  USAR_CONFIGURACOES_DE_COLUNA_PADRAO: Boolean = true;
  USAR_CAPTION_DO_FIELD_COMO_NOME_DA_COLUNA: Boolean = true;
var
  i: Integer;
begin
  TVerticalGridUtils.setEditorRowsFromDataset(vgFiltros, vcatFiltrosPadrao,
    FdmFiltros, USAR_CONFIGURACOES_DE_COLUNA_PADRAO,
    USAR_CAPTION_DO_FIELD_COMO_NOME_DA_COLUNA);

  //Setar os filtros nos formulário herdados
end;

procedure TFrmRelatorioFRPadrao.SetWhereBundle;
begin
  ClearWhereBundle;
  AtribuirValoresDaGridParaDataset;
  //Setar os filtros nos formulário herdados
end;

procedure TFrmRelatorioFRPadrao.EventoClicarConsultaFK(
  Sender: TObject; AButtonIndex: Integer);
var IDConsultado: Integer;
begin
  inherited;
  IDConsultado := TFrmConsultaPadrao.ConsultarID(
    GetIdentificadorConsultaFK(TcxEditorRow(Sender).Name));

  if IDConsultado > 0 then
  begin
    TcxEditorRow(Sender).Properties.Value := IDConsultado;
  end;
end;

function TFrmRelatorioFRPadrao.GetIdentificadorConsultaFK(
  ANomeDoFiltro: String): String;
begin
  if FListaIndentificadoresConsultaFK.ContainsKey(ANomeDoFiltro) then
    result := TPair<String, String>(FListaIndentificadoresConsultaFK.ExtractPair(ANomeDoFiltro)).Value
  else
    result := '';
end;

procedure TFrmRelatorioFRPadrao.Visualizar;
begin
  try
    TFrmMessage_Process.SendMessage('Carregando relatório');

    ActiveControl := nil;

    if cdsConsultaRelatorioFR.isEmpty then
      exit;

    SetWhereBundle;

    TRelatorioFR.VisualizarRelatorio(0, cdsConsultaRelatorioFRID.AsInteger,
      FWhereSearch.where);

  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmRelatorioFRPadrao.AddFiltroIgual(ACampo: String;
  AField: TLargeIntField);
const
  NAO_MOSTRAR_MENSAGEM: Boolean = false;
  NAO_ABORTAR: Boolean = False;
begin
  if TValidacaoCampo.CampoPreenchido(AField, NAO_MOSTRAR_MENSAGEM, NAO_ABORTAR) then
    FWhereSearch.AddIgual(ACampo, AField.AsInteger);
end;

end.

