object FrmModalComMenuPadrao: TFrmModalComMenuPadrao
  Left = 345
  Top = 224
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Form Modal Com Menu Padr'#227'o'
  ClientHeight = 443
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 679
    Height = 102
    BarManager = dxBarManager
    Style = rs2010
    ColorSchemeName = 'Blue'
    ShowTabHeaders = False
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = 'dxRibbon1Tab1'
      Groups = <
        item
          ToolbarName = 'BarAcao'
        end
        item
          ToolbarName = 'BarSair'
        end>
      Index = 0
    end
  end
  object cxPanelMain: TcxGroupBox
    Left = 0
    Top = 102
    Align = alClient
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Height = 341
    Width = 679
  end
  object ActionListMain: TActionList
    Images = DmAcesso.cxImage32x32
    Left = 260
    Top = 27
    object ActClose: TAction
      Category = 'End'
      Caption = '&Sair Esc'
      Hint = 'Fechar o Formul'#225'rio'
      ImageIndex = 12
      ShortCut = 27
      OnExecute = ActCloseExecute
    end
    object ActConfirm: TAction
      Category = 'Action'
      Caption = '&Confirmar F5'
      ImageIndex = 13
      ShortCut = 116
      OnExecute = ActConfirmExecute
    end
    object ActCancel: TAction
      Category = 'Action'
      Caption = 'Ca&ncelar Esc'
      ImageIndex = 14
      ShortCut = 27
      OnExecute = ActCancelExecute
    end
  end
  object dxBarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Finalizar'
      'Acao')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    ImageOptions.Images = DmAcesso.cxImage32x32
    ImageOptions.LargeImages = DmAcesso.cxImage32x32
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 232
    Top = 27
    DockControlHeights = (
      0
      0
      0
      0)
    object BarSair: TdxBar
      Caption = 'Sair'
      CaptionButtons = <>
      DockedLeft = 141
      DockedTop = 0
      FloatLeft = 467
      FloatTop = 200
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object BarAcao: TdxBar
      Caption = 'A'#231#227'o'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = ActClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActConfirm
      Category = 1
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = ActCancel
      Category = 1
    end
  end
end
