unit uFrmConsultaDadosPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmChildPadrao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxContainer, cxEdit, dxBar, System.Actions,
  Vcl.ActnList, cxGroupBox, cxClasses, dxRibbon, uFiltroFormulario;

type
  TFrmConsultaDadosPadrao = class(TFrmChildPadrao)
    ActPesquisar: TAction;
    procedure ActPesquisarExecute(Sender: TObject);
  private
    procedure ClearWhereBundle;
  public

  protected
    FWhereSearch: TFiltroPadrao;
    procedure AoCriarFormulario; virtual;
    procedure PrepararFiltros; virtual;
    procedure SetWhereBundle; virtual;
    procedure AntesDeConsultar; virtual;
    procedure Consultar; virtual;
    procedure DepoisDeConsultar; virtual;
  end;

var
  FrmConsultaDadosPadrao: TFrmConsultaDadosPadrao;

implementation

{$R *.dfm}

procedure TFrmConsultaDadosPadrao.ActPesquisarExecute(Sender: TObject);
begin
  inherited;
  AntesDeConsultar;
  Consultar;
  DepoisDeConsultar;
end;

procedure TFrmConsultaDadosPadrao.AntesDeConsultar;
begin
  SetWhereBundle;
end;

procedure TFrmConsultaDadosPadrao.AoCriarFormulario;
begin
  PrepararFiltros;
end;

procedure TFrmConsultaDadosPadrao.ClearWhereBundle;
begin
  FWhereSearch.Limpar;
end;

procedure TFrmConsultaDadosPadrao.Consultar;
begin

end;

procedure TFrmConsultaDadosPadrao.DepoisDeConsultar;
begin
  //Implementar nos forms herdados
end;

procedure TFrmConsultaDadosPadrao.PrepararFiltros;
begin
  //Preparar os filtros nos details
end;

procedure TFrmConsultaDadosPadrao.SetWhereBundle;
begin
  ClearWhereBundle;
  //Setar os filtros nos formulário herdados
end;

end.

