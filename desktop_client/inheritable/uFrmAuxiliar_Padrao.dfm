object FrmAuxiliar_Padrao: TFrmAuxiliar_Padrao
  Left = 345
  Top = 224
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Auxiliar Padr'#227'o'
  ClientHeight = 458
  ClientWidth = 891
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 891
    Height = 101
    BarManager = dxBarManager
    ColorSchemeName = 'Blue'
    ShowTabHeaders = False
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = 'dxRibbon1Tab1'
      Groups = <
        item
          ToolbarName = 'dxBarManagerBar2'
        end
        item
          ToolbarName = 'dxBarManagerBar1'
        end>
      Index = 0
    end
  end
  object cxGroupBox2: TcxGroupBox
    Left = 0
    Top = 101
    Align = alClient
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Height = 357
    Width = 891
  end
  object ActionListMain: TActionList
    Images = DmAcesso.cxImage32x32
    Left = 748
    Top = 11
    object ActClose: TAction
      Category = 'End'
      Caption = '&Sair F12'
      Hint = 'Fechar o Formul'#225'rio'
      ImageIndex = 12
      ShortCut = 123
      OnExecute = ActCloseExecute
    end
    object ActConfirm: TAction
      Category = 'Action'
      Caption = 'Confirmar F5'
      ImageIndex = 13
      ShortCut = 116
      OnExecute = ActConfirmExecute
    end
    object ActCancel: TAction
      Category = 'Action'
      Caption = 'Cancelar F6'
      ImageIndex = 14
      ShortCut = 117
      OnExecute = ActCancelExecute
    end
  end
  object dxBarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Finalizar'
      'Acao')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    ImageOptions.Images = DmAcesso.cxImage32x32
    ImageOptions.LargeImages = DmAcesso.cxImage32x32
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 720
    Top = 11
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManagerBar1: TdxBar
      Caption = 'Sair'
      CaptionButtons = <>
      DockedLeft = 141
      DockedTop = 0
      FloatLeft = 467
      FloatTop = 200
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManagerBar2: TdxBar
      Caption = 'A'#231#227'o'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = ActClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActConfirm
      Category = 1
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = ActCancel
      Category = 1
    end
  end
end
