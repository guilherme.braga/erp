inherited FrmConsultaDadosPadrao: TFrmConsultaDadosPadrao
  Caption = 'Consulta de Dados'
  ExplicitWidth = 907
  ExplicitHeight = 497
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
  end
  inherited ActionListMain: TActionList
    object ActPesquisar: TAction
      Category = 'Action'
      Caption = 'Pesquisar F7'
      Hint = 'Executar a pesquisa aos dados'
      ImageIndex = 16
      ShortCut = 118
      OnExecute = ActPesquisarExecute
    end
  end
  inherited dxBarManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSair: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 51
      FloatClientHeight = 54
    end
    inherited BarAcao: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 70
      FloatClientHeight = 108
    end
  end
end
