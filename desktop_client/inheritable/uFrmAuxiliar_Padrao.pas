unit uFrmAuxiliar_Padrao;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  cxGroupBox,
  dxSkinsdxBarPainter,
  dxBar,
  cxClasses,
  dxRibbon,
  ActnList,
  cxStyles,
  dxSkinscxPCPainter,
  cxCustomData,
  cxFilter,
  cxData,
  cxDataStorage,
  DB,
  cxDBData,
  cxVGrid,
  cxGridLevel,
  cxGridCustomView,
  cxGridCustomTableView,
  cxGridTableView,
  cxGridDBTableView,
  cxGrid,
  cxInplaceContainer,
  DBClient,
  cxRadioGroup,
  cxDBEdit,
  cxMaskEdit,
  cxTextEdit,
  cxCalendar,
  SqlExpr,
  FMTBcd,
  uTTranslate,
  uDmAcesso, dxRibbonSkins, dxSkinsdxRibbonPainter, System.Actions,
  dxRibbonCustomizationForm;

type
  TFrmAuxiliar_Padrao = class(TForm)
    ActionListMain: TActionList;
    ActClose: TAction;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager: TdxBarManager;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarManagerBar1: TdxBar;
    cxGroupBox2: TcxGroupBox;
    dxBarManagerBar2: TdxBar;
    ActConfirm: TAction;
    ActCancel: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActCloseExecute(Sender: TObject);
    procedure ActConfirmExecute(Sender: TObject);
    procedure ActCancelExecute(Sender: TObject);
  private

  public
    FResult: Integer;
end;

var
  FrmAuxiliar_Padrao: TFrmAuxiliar_Padrao;

implementation

uses uTVariables;
{$R *.dfm}

procedure TFrmAuxiliar_Padrao.ActCancelExecute(Sender: TObject);
begin
  FResult := Variables.CCANCEL;
  Close;
end;

procedure TFrmAuxiliar_Padrao.ActCloseExecute(Sender: TObject);
begin
  FResult := Variables.CCLOSE;
  Close;
end;

procedure TFrmAuxiliar_Padrao.ActConfirmExecute(Sender: TObject);
begin
  FResult := Variables.CCONFIRM;
  Close;
end;

procedure TFrmAuxiliar_Padrao.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  Action := caFree;
end;

Initialization RegisterClass( TFrmAuxiliar_Padrao );
Finalization UnRegisterClass( TFrmAuxiliar_Padrao );

end.
