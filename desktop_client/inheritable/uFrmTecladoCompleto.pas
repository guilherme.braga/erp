unit uFrmTecladoCompleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsDefaultPainters, Keyboard, cxGroupBox,
  cxTextEdit, ExtCtrls;

type
  TFrmTecladoCompleto = class(TForm)
    TouchKeyboard1: TTouchKeyboard;
    PnText: TPanel;
    EdtVisor: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure EdtVisorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTecladoCompleto: TFrmTecladoCompleto;

implementation

{$R *.dfm}

procedure TFrmTecladoCompleto.EdtVisorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then Close;
end;

procedure TFrmTecladoCompleto.FormCreate(Sender: TObject);
begin
  Self.Height := 221;
  Self.Width := 564;
end;

end.
