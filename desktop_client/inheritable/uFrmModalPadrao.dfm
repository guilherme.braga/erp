object FrmModalPadrao: TFrmModalPadrao
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'FrmModalPadrao'
  ClientHeight = 325
  ClientWidth = 658
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox2: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    PanelStyle.OfficeBackgroundKind = pobkGradient
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Height = 269
    Width = 658
  end
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 269
    Align = alBottom
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = False
    Style.TransparentBorder = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Height = 56
    Width = 658
    object BtnConfirmar: TcxButton
      Left = 506
      Top = 2
      Width = 75
      Height = 52
      Align = alRight
      Action = ActConfirmar
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 0
    end
    object BtnCancelar: TcxButton
      Left = 581
      Top = 2
      Width = 75
      Height = 52
      Align = alRight
      Action = ActCancelar
      OptionsImage.Images = DmAcesso.cxImage32x32
      OptionsImage.Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
      TabOrder = 1
    end
  end
  object ActionList: TActionList
    Images = DmAcesso.cxImage32x32
    Left = 398
    Top = 174
    object ActConfirmar: TAction
      Caption = 'Confirmar F5'
      Hint = 'Clique sobre o bot'#227'o ou pressione ENTER para "Confirmar"'
      ImageIndex = 13
      ShortCut = 116
      OnExecute = ActConfirmarExecute
    end
    object ActCancelar: TAction
      Caption = ' Cancelar Esc'
      Hint = 'Clique sobre o bot'#227'o ou pressione ESC para "Cancelar"'
      ImageIndex = 14
      ShortCut = 27
      OnExecute = ActCancelarExecute
    end
  end
end
