unit uFrmModalComMenuPadrao;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer,
  cxEdit,
  dxSkinsCore,
  dxSkinsDefaultPainters,
  cxGroupBox,
  dxSkinsdxBarPainter,
  dxBar,
  cxClasses,
  dxRibbon,
  ActnList,
  cxStyles,
  dxSkinscxPCPainter,
  cxCustomData,
  cxFilter,
  cxData,
  cxDataStorage,
  DB,
  cxDBData,
  cxVGrid,
  cxGridLevel,
  cxGridCustomView,
  cxGridCustomTableView,
  cxGridTableView,
  cxGridDBTableView,
  cxGrid,
  cxInplaceContainer,
  DBClient,
  cxRadioGroup,
  cxDBEdit,
  cxMaskEdit,
  cxTextEdit,
  cxCalendar,
  SqlExpr,
  FMTBcd,
  uDmAcesso, dxRibbonSkins, dxSkinsdxRibbonPainter, System.Actions,
  dxRibbonCustomizationForm, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFrmModalComMenuPadrao = class(TForm)
    ActionListMain: TActionList;
    ActClose: TAction;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager: TdxBarManager;
    dxBarLargeButton5: TdxBarLargeButton;
    BarSair: TdxBar;
    cxPanelMain: TcxGroupBox;
    BarAcao: TdxBar;
    ActConfirm: TAction;
    ActCancel: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    procedure ActCloseExecute(Sender: TObject);
    procedure ActConfirmExecute(Sender: TObject);
    procedure ActCancelExecute(Sender: TObject);
  private

  public
    FResult: Integer;
  protected
    procedure CarregarPersonalizacaoGrid;virtual;
    procedure SalvarPersonalizacaoGrid;virtual;
end;

var
  FrmModalComMenuPadrao: TFrmModalComMenuPadrao;

implementation

uses uTVariables;
{$R *.dfm}

procedure TFrmModalComMenuPadrao.ActCancelExecute(Sender: TObject);
begin
  FResult := Variables.CCANCEL;
  Close;
end;

procedure TFrmModalComMenuPadrao.ActCloseExecute(Sender: TObject);
begin
  FResult := Variables.CCLOSE;
  Close;
end;

procedure TFrmModalComMenuPadrao.ActConfirmExecute(Sender: TObject);
begin
  FResult := Variables.CCONFIRM;
  Close;
end;

procedure TFrmModalComMenuPadrao.CarregarPersonalizacaoGrid;
begin
  //Implementar na heranca
end;

procedure TFrmModalComMenuPadrao.SalvarPersonalizacaoGrid;
begin
  //Implementar na heranca
end;

end.
