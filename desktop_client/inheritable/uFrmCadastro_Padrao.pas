unit uFrmCadastro_Padrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxBar, cxClasses, System.Actions,
  Vcl.ActnList, dxRibbon, dxRibbonMiniToolbar, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, Data.DB, Datasnap.DBClient, frxClass, frxDBSet,
  Vcl.Menus, cxGridCustomPopupMenu, cxGridPopupMenu, UCBase, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxBarBuiltInMenu, cxContainer, cxGroupBox,
  Vcl.ExtCtrls, JvDesignSurface, cxPC, uGBClientDataset, cxGridBandedTableView,
  cxGridDBBandedTableView, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.Generics.Collections, dxBevel, uFiltroFormulario,
  uFrmChildClean, dxBarApplicationMenu, uUsuarioDesignControl, Data.DBXCommon,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPSCore, dxPScxCommon, ugbDBButtonEditFK,
  cxGridDBDataDefinitions, cxSplitter, JvExControls, JvButton, JvTransparentButton, uGBPanel,
  uFrameFiltroVerticalPadrao, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type TFieldFilter = (All, Required);

type TAcaoCrud = (Incluir, Alterar, Excluir, Visualizar, Pesquisar, None);

type
  TFrmCadastro_Padrao = class(TFrmChildClean)
    ActionListAssistent: TActionList;
    ActRestaurarColunasPadrao: TAction;
    ActAlterarColunasGrid: TAction;
    dxBarManagerPadrao: TdxBarManager;
    dxBarManager: TdxBar;
    dxBarAction: TdxBar;
    dxBarReport: TdxBar;
    dxBarEnd: TdxBar;
    dxBarSearch: TdxBar;
    dxDesignDesign: TdxBar;
    dxlbAccept: TdxBarLargeButton;
    dxlbCancel: TdxBarLargeButton;
    dxlbInsert: TdxBarLargeButton;
    dxlbUpdate: TdxBarLargeButton;
    dxlbRemove: TdxBarLargeButton;
    dxlbReport: TdxBarLargeButton;
    dxlbClose: TdxBarLargeButton;
    dxlbSearch: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxMainRibbon: TdxRibbon;
    dxGerencia: TdxRibbonTab;
    dxDesign: TdxRibbonTab;
    dsSearch: TDataSource;
    dsData: TDataSource;
    ActionListMain: TActionList;
    ActConfirm: TAction;
    ActClose: TAction;
    ActInsert: TAction;
    ActReport: TAction;
    ActCancel: TAction;
    ActUpdate: TAction;
    ActRemove: TAction;
    ActConfirmDesign: TAction;
    ActCancelDesign: TAction;
    ActDesign: TAction;
    UCCadastro_Padrao: TUCControls;
    pmTituloGridPesquisaPadrao: TPopupMenu;
    cxGridPopupMenuPadrao: TcxGridPopupMenu;
    cxpcMain: TcxPageControl;
    cxtsSearch: TcxTabSheet;
    cxtsData: TcxTabSheet;
    DesignPanel: TJvDesignPanel;
    cxGBDadosMain: TcxGroupBox;
    cdsData: TGBClientDataSet;
    cxGridPesquisaPadrao: TcxGrid;
    Level1BandedTableView1: TcxGridDBBandedTableView;
    cxGridPesquisaPadraoLevel1: TcxGridLevel;
    ActSearch: TAction;
    dxBevel1: TdxBevel;
    fdmSearch: TFDMemTable;
    AlterarRtulodasColunas1: TMenuItem;
    RestaurarColunasPadrao: TMenuItem;
    pmGridConsultaPadrao: TPopupMenu;
    ActExibirAgrupamento: TAction;
    ActAbrirConsultando: TAction;
    ExibirAgrupamento1: TMenuItem;
    ExibirAgrupamento2: TMenuItem;
    ActSalvarConfiguracoes: TAction;
    SalvarConfiguraes1: TMenuItem;
    ActAlterarSQLPesquisaPadrao: TAction;
    AlterarSQL1: TMenuItem;
    ActFullExpand: TAction;
    ActConfigurarValoresDefault: TAction;
    ConfigurarValoresPadres1: TMenuItem;
    ActVisualizarRegistro: TAction;
    lbVisualizar: TdxBarLargeButton;
    dxBarNavegacaoData: TdxBar;
    lbNavegacaoDataPrimeiro: TdxBarLargeButton;
    lbNavegacaoDataProximo: TdxBarLargeButton;
    lbNavegacaoDataAnterior: TdxBarLargeButton;
    lbNavegacaoDataUltimo: TdxBarLargeButton;
    ActNavegacaoDataPrimeiro: TAction;
    ActNavegacaoDataAnterior: TAction;
    ActNavegacaoDataProximo: TAction;
    ActNavegacaoDataUltimo: TAction;
    dxBarPersonalizacoes: TdxBar;
    dxBarListItem1: TdxBarListItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    siPersonalizarPadrao: TdxBarSubItem;
    ActPersonalizacoes: TAction;
    bbConsultarAutomaticamente: TdxBarButton;
    bbAlterarRotuloColunas: TdxBarButton;
    bbAlterarSQL: TdxBarButton;
    bbConfigurarValorPadrao: TdxBarButton;
    bbExibirAgrupamento: TdxBarButton;
    bbRestaurarColunas: TdxBarButton;
    bbSalvarConfiguracoes: TdxBarButton;
    ActParametroFormulario: TAction;
    bbParametroFormulario: TdxBarButton;
    ActInformacaoImplatacao: TAction;
    bbHelpImplantacao: TdxBarButton;
    dxComponentPrinter: TdxComponentPrinter;
    dxComponentPrinterLink1: TdxGridReportLink;
    ActExibirTotalizadorGrade: TAction;
    bbExibirRodape: TdxBarButton;
    ExibirTotalizador1: TMenuItem;
    SeparadorMenuPadraoCPN1: TMenuItem;
    SeparadorMenuPadraoCPN2: TMenuItem;
    otalizarPor1: TMenuItem;
    ActTotalizadorTotalizarPorSum: TAction;
    Somar1: TMenuItem;
    ActDuplicarRegistro: TAction;
    lbDuplicarRegistro: TdxBarLargeButton;
    pnFiltroVertical: TgbPanel;
    pnAcoesFiltroVertical: TgbPanel;
    btnLimparFiltroVerticalPadrao: TJvTransparentButton;
    spFiltroVertical: TcxSplitter;
    ActLimparFiltroVertical: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActInsertExecute(Sender: TObject);
    procedure ActUpdateExecute(Sender: TObject);
    procedure ActRemoveExecute(Sender: TObject);
    procedure ActConfirmExecute(Sender: TObject);
    procedure ActCancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActCloseExecute(Sender: TObject);
    procedure ActSearchExecute(Sender: TObject);
    procedure cdsDataPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure cdsDataDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure cdsDataEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure Level1BandedTableView1DblClick(Sender: TObject);
    procedure ActAlterarColunasGridExecute(Sender: TObject);
    procedure ActRestaurarColunasPadraoExecute(Sender: TObject);
    procedure cdsDataAfterApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
    procedure cxpcMainChange(Sender: TObject);
    procedure fdmSearchAfterOpen(DataSet: TDataSet);
    procedure fdmSearchAfterDelete(DataSet: TDataSet);
    procedure fdmSearchAfterClose(DataSet: TDataSet);
    procedure ActExibirAgrupamentoExecute(Sender: TObject);
    procedure ActAbrirConsultandoExecute(Sender: TObject);
    procedure pmTituloGridPesquisaPadraoPopup(Sender: TObject);
    procedure pmGridConsultaPadraoPopup(Sender: TObject);
    procedure ActSalvarConfiguracoesExecute(Sender: TObject);
    procedure Level1BandedTableView1EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure ActAlterarSQLPesquisaPadraoExecute(Sender: TObject);
    procedure ActConfigurarValoresDefaultExecute(Sender: TObject);
    procedure ActReportExecute(Sender: TObject);
    procedure InserirValoresPadrao(DataSet: TDataSet);
    procedure cdsDataReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure ActVisualizarRegistroExecute(Sender: TObject);
    procedure ActNavegacaoDataPrimeiroExecute(Sender: TObject);
    procedure ActNavegacaoDataAnteriorExecute(Sender: TObject);
    procedure ActNavegacaoDataProximoExecute(Sender: TObject);
    procedure ActNavegacaoDataUltimoExecute(Sender: TObject);
    procedure ActPersonalizacoesExecute(Sender: TObject);
    procedure ActParametroFormularioExecute(Sender: TObject);
    procedure ActInformacaoImplatacaoExecute(Sender: TObject);
    procedure cdsDataAfterEdit(DataSet: TDataSet);
    procedure Level1BandedTableView1NavigatorButtonsButtonClick(Sender: TObject;
      AButtonIndex: Integer; var ADone: Boolean);
    procedure cxpcMainEnter(Sender: TObject);
    procedure ActExibirTotalizadorGradeExecute(Sender: TObject);
    procedure ActTotalizadorTotalizarPorSumExecute(Sender: TObject);
    procedure ActDuplicarRegistroExecute(Sender: TObject);
    procedure ActLimparFiltroVerticalExecute(Sender: TObject);
  private
    FListaAcoes: TDictionary<String, TShortCut>;
    FConsultaAutomatica: Boolean;
    FWhereSearch: TFiltroPadrao;
    FInsertKey: Integer;
    FConsultaExecutada: Boolean;
    FacaoCrud: TAcaoCrud;
    FbloquearEdicaoDados: Boolean;
    FIdentificadorConfiguracaoGradePesquisa: String;

    procedure Summarize(ASummary: TcxDataSummary; AColumn: TcxGridDBBandedColumn;
      AKind: TcxSummaryKind; AFormat: string);

    procedure SetbloquearEdicaoDados(const Value: Boolean);
    const CID_NULL: Integer = -999;
    procedure AtualizarDadosDaConsulta(const AAcaoCrud: TAcaoCrud);
    procedure FocarProximoControle(AControl: TWinControl);
    procedure ExibirFiltroDeMemoriaDaGrid(const AExibir: Boolean);
    procedure PreencherValoresPadrao;
    procedure DesbloquearEdicao;
    procedure AtualizarDescricaoNavegacaoRegistros;
    procedure ConfigurarEventoInserirValoresPadrao(ADataset: TgbClientDataset);
    //procedure ConfigurarEventoDepoisAlterar(ADataset: TgbClientDataset);
    procedure ArmazenarListaAcoes;
    procedure ConfigurarAcionamentoPorAtalho;
    procedure CarregarParametrosFormulario(AParametros: TListaParametroFormulario);
    procedure LimparListaParametros;
    procedure InicializarListaParametros;
  public
    class procedure ModoDeInclusaoFK(var AComponentFK: TgbDBButtonEditFK;
      const AClassName, ATableName: String);
    class procedure ExplorarRegistroFK(var AComponentFK: TgbDBButtonEditFK);
    function UltimoRegistroGerado(const ATabela, ACampoRetorno: String): String;
  protected
    FParametros: TListaParametroFormulario;
    FComponenteModoDeInclusaoFK: TgbDBButtonEditFK;
    FFiltroVertical: TFrameFiltroVerticalPadrao;

    property WhereSearch: TFiltroPadrao read FWhereSearch write FWhereSearch;
    property InsertKey: Integer read FInsertKey write FInsertKey default 0;
    property consultaExecutada: Boolean read FConsultaExecutada write FConsultaExecutada default false;
    property acaoCrud: TAcaoCrud read FacaoCrud write FacaoCrud;
    property bloquearEdicaoDados: Boolean read FbloquearEdicaoDados write SetbloquearEdicaoDados;
    property identificadorConfiguracaoGradePesquisa: String read
      FIdentificadorConfiguracaoGradePesquisa write FIdentificadorConfiguracaoGradePesquisa;

    function SelecionarRegistroParaAlteracao(const AUsarDatasetConsulta: Boolean = true): Boolean;
    procedure AbrirPesquisaPadrao;
    procedure ClearWhereBundle;
    procedure ConfirmarMestreAntesDeAbrirDetalhe;
    procedure FocarContainer(AControl: TWinControl = nil);

    procedure ExecutarConsulta;
    procedure ExecutarIncluir;

    procedure HabilitarAcaoSalvar;
    procedure HabilitarAcaoAlterar;
    procedure ExibirBarraAcoes;

    procedure AtualizarRegistro; overload;
    procedure AtualizarRegistro(AId: Integer);overload;

    procedure PosicionarGradeDePesquisaNoPrimeiroRegistro; virtual;

    procedure Consultar;

    procedure ExpandirFiltro;
    procedure RetrairFiltro;

    procedure AoCriarFormulario; virtual;
    procedure PrepararFiltros; virtual;
    procedure GerenciarControlesFormulario; virtual;
    procedure GerenciarControles; virtual;
    procedure SetWhereBundle; virtual;
    procedure AntesDeConfirmar; virtual;
    procedure DepoisDeConfirmar; virtual;
    procedure AntesDeCancelar; virtual;
    procedure DepoisDeCancelar; virtual;
    procedure AntesDeRemover; virtual;
    procedure DepoisDeRemover; virtual;
    procedure AntesDePesquisar; virtual;
    procedure DepoisDePesquisar; virtual;
    procedure DepoisDeInserir; virtual;
    procedure AntesDeAlterar; virtual;
    procedure DepoisDeAlterar; virtual;
    procedure AntesDeGerenciarControlesFormulario; virtual;
    procedure DepoisDeGerenciarControlesFormulario; virtual;
    procedure BloquearEdicao; virtual;
    procedure AntesDeVisualizar; virtual;
    procedure DepoisDeVisualizar; virtual;
    procedure AoDestruirFormulario; virtual;
    procedure CriarParametrosFormulario(
      var AParametros: TListaParametroFormulario); virtual;
    procedure MostrarInformacoesImplantacao; virtual;
    procedure ValidarParametros; virtual;
    procedure InstanciarFrames;virtual;
    procedure AoImprimir; virtual;
    procedure AoExcluir; virtual;
    procedure FecharFormulario; virtual;
    procedure ParametrizarFormulario; virtual;
    procedure AntesDeParametrizar; virtual;
    procedure DepoisDeParametrizar; virtual;
    procedure AntesDeDuplicarRegistro; virtual;
    function DuplicarRegistro: Integer; virtual;
    procedure DepoisDeDuplicarRegistro(const AIdNovoRegistro: Integer); virtual;
    procedure DepoisDeCarregarRegistroDuplicado; virtual;
    procedure AntesDeDefinirFiltros; virtual;
    procedure DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); virtual;
    procedure CriarFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao); virtual;
    procedure DepoisDeDefinirFiltros; virtual;
    procedure AntesDeSelecionarRegistro; virtual;
    procedure DepoisDeSelecionarRegistro; virtual;
  end;

var
  FrmCadastro_Padrao: TFrmCadastro_Padrao;

implementation

{$R *.dfm}

uses uDmAcesso, uDmConnection, uTMessage, Data.FireDACJSONReflect, uFrmMessage,
  uTVariables, uFrmApelidarColunasGrid, uTUsuario, uFrmMessage_Process,
  uTFunction, uFrmAlterarSQLPesquisaPadrao, uDatasetUtils, uTControl_Function,
  uDevExpressUtils, uControlsUtils, uRelatorioFR, uFrmVisualizacaoRelatoriosAssociados, uSistema,
  uFrmConfiguracaoValoresPadroes, uFrmParametrosFormulario, vcl.Recerror;

procedure TFrmCadastro_Padrao.AbrirPesquisaPadrao;
begin
  try
    ClearWhereBundle;
    if not FConsultaAutomatica then
    begin
      AcaoCrud := TAcaoCrud(Pesquisar);
      WhereSearch.AddIgual('1','2');
      Consultar;
    end
    else
      ActSearch.Execute;
  finally
    GerenciarControlesFormulario;
  end;
end;

procedure TFrmCadastro_Padrao.ActCloseExecute(Sender: TObject);
begin
  FecharFormulario;
end;

procedure TFrmCadastro_Padrao.ActConfigurarValoresDefaultExecute(
  Sender: TObject);
begin
  TFrmConfigurarValoresPadroes.ConfigurarValoresPadroes(Self.Name,
    TSistema.Sistema.usuario.idSeguranca, cdsData);
end;

procedure TFrmCadastro_Padrao.ActConfirmExecute(Sender: TObject);
var idRegistroNaConsulta: Integer;
begin
  idRegistroNaConsulta := fdmSearch.FieldByName('ID').AsInteger;

  FocarProximoControle(Screen.ActiveControl);
  ActiveControl := nil;
  AntesDeConfirmar;
  cdsData.Commit;
  Application.ProcessMessages;

{  if cdsData.FieldByName('ID').AsInteger <= 0 then
  begin
    AtualizarRegistro(StrtoIntDef(UltimoRegistroGerado('PRODUTO', 'ID'), 0));
  end;}

  DepoisDeConfirmar;
  Application.ProcessMessages;
  //DmConnection.Commit;
  //AtualizarDadosDaConsulta(acaoCrud);

  if not Assigned(cdsData) then
  begin
    exit;
  end;

  if cdsData.FieldByName('ID').AsInteger > 0 then
  begin
    acaoCrud := TAcaoCrud(Visualizar);
  end
  else
  begin
    acaoCrud := TAcaoCrud(Pesquisar);
  end;
  GerenciarControlesFormulario;

  if idRegistroNaConsulta > 0 then
    fdmSearch.Locate('ID', idRegistroNaConsulta, []);

  if Assigned(FComponenteModoDeInclusaoFK) and Assigned(FComponenteModoDeInclusaoFK.Owner) then
  begin
    try
      ActClose.Execute;

      if Variables.classeDoFormularioAntivoAnteriormente <> '' then
      begin
        TControlFunction.CreateMDIForm(Variables.classeDoFormularioAntivoAnteriormente)
      end;
    finally
      try
        TControlsUtils.CongelarFormulario(Self);

        FComponenteModoDeInclusaoFK.ExecutarOnEnter;
        FComponenteModoDeInclusaoFK.DataBinding.Field.AsString :=
          DmConnection.ServerMethodsClient.GetUltimaChaveDaTabela(
            FComponenteModoDeInclusaoFK.gbTableName, FComponenteModoDeInclusaoFK.gbCampoPK);
        FComponenteModoDeInclusaoFK.ExecutarOnExit;
      finally
        TControlsUtils.DescongelarFormulario;
      end;
    end;
  end;
end;

procedure TFrmCadastro_Padrao.ActDuplicarRegistroExecute(Sender: TObject);
var
  registroDuplicado: Boolean;
  idNovoRegistro: Integer;
begin
  inherited;
  try
    TFrmMessage_Process.SendMessage('Duplicando registro');
    try
      AntesDeDuplicarRegistro;
      idNovoRegistro := DuplicarRegistro;
      DepoisDeDuplicarRegistro(idNovoRegistro);
      AtualizarRegistro(idNovoRegistro);
      registroDuplicado := true;
      DepoisDeCarregarRegistroDuplicado;
    except
      on e: exception do
      begin
       registroDuplicado := false;
      end;
    end;
  finally
    TFrmMessage_Process.CloseMessage();
  end;

  if registroDuplicado then
  begin
    TFrmMessage.Information('O novo registro foi gerado com sucesso e est� em modo visualiza��o!');
  end;
end;

procedure TFrmCadastro_Padrao.ActExibirAgrupamentoExecute(Sender: TObject);
begin
  Level1BandedTableView1.OptionsView.GroupByBox := not Level1BandedTableView1.OptionsView.GroupByBox;
end;

procedure TFrmCadastro_Padrao.ActExibirTotalizadorGradeExecute(Sender: TObject);
begin
  inherited;
  Level1BandedTableView1.OptionsView.Footer := not Level1BandedTableView1.OptionsView.Footer;
end;

procedure TFrmCadastro_Padrao.AtualizarDadosDaConsulta(const AAcaoCrud: TAcaoCrud);
var registro: TFDJSONDataSets;
begin
  case AAcaoCrud of
    Incluir:
    try
      registro := TFDJSONDataSets.Create;
      //TFDJSONDataSetsWriter.ListAdd(registro, cdsData);  //cds nao funciona
      fdmSearch.AppendData(TFDJSONDataSetsReader.GetListValue(registro,0));
    finally
      FreeAndNil(registro);
    end;

    Alterar:
    try
      fdmSearch.Delete;
      registro := TFDJSONDataSets.Create;
      //TFDJSONDataSetsWriter.ListAdd(registro, cdsData); //cds nao funciona
      fdmSearch.AppendData(TFDJSONDataSetsReader.GetListValue(registro,0));
    finally
      FreeAndNil(registro);
    end;

    Excluir:
    begin
      fdmSearch.Delete;
    end;
  end;
end;

procedure TFrmCadastro_Padrao.BloquearEdicao;
begin
  //Implementar nos n�veis de heran�a
  //cdsData.SomenteLeitura := condicao;
end;

procedure TFrmCadastro_Padrao.CarregarParametrosFormulario(
  AParametros: TListaParametroFormulario);
begin
  TUsuarioParametrosFormulario.CarregarParametros(FParametros,
    TSistema.Sistema.Usuario.IdSeguranca, Self.Name);
end;

procedure TFrmCadastro_Padrao.ActCancelExecute(Sender: TObject);
begin
  try
    TFrmMessage_Process.SendMessage('Cancelando altera��es');
    ActiveControl := nil;
    AntesDeCancelar;
    cdsData.Cancelar;
    DepoisDeCancelar;
    //DmConnection.Rollback;

    if not Assigned(fdmSearch) then
      Exit;

    if not fdmSearch.IsEmpty then
    begin
      SelecionarRegistroParaAlteracao;
      acaoCrud := TAcaoCrud(Visualizar);
    end
    else
    begin
      acaoCrud := TAcaoCrud(Pesquisar);
    end;
    GerenciarControlesFormulario;

    if Assigned(FComponenteModoDeInclusaoFK) and Assigned(FComponenteModoDeInclusaoFK.Owner) then
    begin
      try
        TControlsUtils.CongelarFormulario(Self);

        FComponenteModoDeInclusaoFK.ExecutarOnEnter;

        ActClose.Execute;

        if Variables.classeDoFormularioAntivoAnteriormente <> '' then
        begin
          TControlFunction.CreateMDIForm(Variables.classeDoFormularioAntivoAnteriormente)
        end;
      finally
        TControlsUtils.DescongelarFormulario;
      end;
    end;
  finally
    TFrmMessage_Process.CloseMessage();
  end;
end;

procedure TFrmCadastro_Padrao.ActInformacaoImplatacaoExecute(Sender: TObject);
begin
  inherited;
  MostrarInformacoesImplantacao;
end;

procedure TFrmCadastro_Padrao.ActInsertExecute(Sender: TObject);
begin
  ExecutarIncluir;
end;

procedure TFrmCadastro_Padrao.ActLimparFiltroVerticalExecute(Sender: TObject);
begin
  inherited;
  FFiltroVertical.LimparTodosFiltros;
end;

procedure TFrmCadastro_Padrao.ActNavegacaoDataAnteriorExecute(Sender: TObject);
begin
  inherited;
  fdmSearch.Prior;
  SelecionarRegistroParaAlteracao;
  AtualizarDescricaoNavegacaoRegistros;
  GerenciarControlesFormulario;
end;

procedure TFrmCadastro_Padrao.ActNavegacaoDataPrimeiroExecute(Sender: TObject);
begin
  inherited;
  fdmSearch.First;
  SelecionarRegistroParaAlteracao;
  AtualizarDescricaoNavegacaoRegistros;
  GerenciarControlesFormulario;
end;

procedure TFrmCadastro_Padrao.ActNavegacaoDataProximoExecute(Sender: TObject);
begin
  inherited;
  fdmSearch.Next;
  SelecionarRegistroParaAlteracao;
  AtualizarDescricaoNavegacaoRegistros;
  GerenciarControlesFormulario;
end;

procedure TFrmCadastro_Padrao.ActNavegacaoDataUltimoExecute(Sender: TObject);
begin
  inherited;
  fdmSearch.Last;
  SelecionarRegistroParaAlteracao;
  AtualizarDescricaoNavegacaoRegistros;
  GerenciarControlesFormulario;
end;

procedure TFrmCadastro_Padrao.ActParametroFormularioExecute(Sender: TObject);
begin
  inherited;
  ParametrizarFormulario;
end;

procedure TFrmCadastro_Padrao.ActPersonalizacoesExecute(Sender: TObject);
begin
  inherited;
  //
end;

procedure TFrmCadastro_Padrao.ActUpdateExecute(Sender: TObject);
begin
  if SelecionarRegistroParaAlteracao(false) then
  begin
    AntesDeAlterar;
    acaoCrud := TAcaoCrud(Alterar);
    cdsData.Alterar;
    DepoisDeAlterar;
    FocarContainer;
    GerenciarControlesFormulario;
  end
  else
    TFrmMessage.Information('O registro selecionado n�o foi encontrado na base'+
      ' de dados. Realize a pesquisa novamente!');
end;

procedure TFrmCadastro_Padrao.ActVisualizarRegistroExecute(Sender: TObject);
begin
  inherited;
  if SelecionarRegistroParaAlteracao then
  begin
    if not Assigned(cdsData.AfterInsert) then
      ConfigurarEventoInserirValoresPadrao(cdsData);

    AntesDeVisualizar;
    acaoCrud := TAcaoCrud(Visualizar);
    GerenciarControlesFormulario;
    DepoisDeVisualizar;
  end
  else
    TFrmMessage.Information('O registro selecionado n�o foi encontrado na base'+
      ' de dados. Realize a pesquisa novamente!');
end;

procedure TFrmCadastro_Padrao.AntesDeConfirmar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeDefinirFiltros;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeDuplicarRegistro;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeGerenciarControlesFormulario;
begin
  TControlsUtils.CongelarFormulario(Self);
end;

procedure TFrmCadastro_Padrao.AntesDeParametrizar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDePesquisar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeAlterar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeCancelar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeRemover;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.AntesDeSelecionarRegistro;
begin
  //Implementar na classe herdadada
  //Envento disparado antes que um registro seja carregado ao cdsData
end;

procedure TFrmCadastro_Padrao.AntesDeVisualizar;
begin
  //Implementar na classe herdada
end;

procedure TFrmCadastro_Padrao.AoCriarFormulario;
begin
  acaoCrud := TAcaoCrud(none);

  ArmazenarListaAcoes;
  cxpcMain.ActivePage := cxtsSearch;
  cxpcMain.Properties.HideTabs := true;
  dxMainRibbon.ShowTabHeaders := false;

  FIdentificadorConfiguracaoGradePesquisa := Self.Name;

  WhereSearch := TFiltroPadrao.Create;

  InicializarListaParametros;

  FConsultaAutomatica := TUsuarioPesquisaPadrao.GetConsultaAutomatica(
    TSistema.Sistema.Usuario.idSeguranca, FIdentificadorConfiguracaoGradePesquisa);

  InstanciarFrames;

  PrepararFiltros;

  AbrirPesquisaPadrao;
  //Implementar na classe herdada
end;

procedure TFrmCadastro_Padrao.AoDestruirFormulario;
begin
  WhereSearch.Free;

  LimparListaParametros;

  if Assigned(FListaAcoes) then
  begin
    FreeAndNil(FListaAcoes);
  end;

  if Assigned(FFiltroVertical) then
  begin
    FreeAndNil(FFiltroVertical);
  end;

  ActSalvarConfiguracoes.Execute;
end;

procedure TFrmCadastro_Padrao.AoExcluir;
begin
  cdsData.Excluir;
end;

procedure TFrmCadastro_Padrao.AoImprimir;
var
  id: Integer;
begin
  if not cdsData.Active then
  begin
    id := fdmSearch.FieldByName('ID').AsInteger;
  end
  else
  begin
    id := cdsData.FieldByName('ID').AsInteger;
  end;

  TFrmVisualizacaoRelatoriosAssociados.VisualizarRelatoriosAssociados(
    id, Self.Name, TSistema.Sistema.usuario.idSeguranca);
end;

procedure TFrmCadastro_Padrao.ArmazenarListaAcoes;
var i: Integer;
begin
  if not Assigned(FListaAcoes) then
    FListaAcoes := TDictionary<String, TShortCut>.Create;

  for i := 0 to Pred(ActionListMain.ActionCount) do
  begin
    FListaAcoes.Add(ActionListMain[I].Name, ActionListMain[I].ShortCut);
  end;
end;

procedure TFrmCadastro_Padrao.cdsDataAfterApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  cdsData.FgbPKGeradaPeloDB := DmConnection.GetLastedAutoIncrementValue;
end;

procedure TFrmCadastro_Padrao.cdsDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  if (FACaoCrud = TAcaoCrud(visualizar)) then
  begin
    acaoCrud := TAcaoCrud(Alterar);
    GerenciarControlesFormulario;
  end;
end;

procedure TFrmCadastro_Padrao.InicializarListaParametros;
begin
  if Assigned(FParametros) then
  begin
    LimparListaParametros;
  end;

  FParametros := TListaParametroFormulario.Create;

  CriarParametrosFormulario(FParametros);

  TUsuarioParametrosFormulario.CriarParametro(FParametros,
    TSistema.Sistema.Usuario.IdSeguranca, Self.Name);

  ValidarParametros;
end;

procedure TFrmCadastro_Padrao.InserirValoresPadrao(DataSet: TDataSet);
begin
  inherited;
  TUsuarioDesignControl.CarregarValoresPadrao(DataSet,
    TSistema.Sistema.usuario.idSeguranca, Self.Name);
end;

procedure TFrmCadastro_Padrao.InstanciarFrames;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.cdsDataDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  acaoCrud := TAcaoCrud(none);
  //DmConnection.RollBack;
  TMessagePersistenceError.OnDeleteError(E.Message);
  Action := daAbort;
end;

procedure TFrmCadastro_Padrao.cdsDataEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  acaoCrud := TAcaoCrud(none);
  //DmConnection.RollBack;
  TMessagePersistenceError.OnEditError(E.Message);
  Action := daAbort;
end;

procedure TFrmCadastro_Padrao.cdsDataPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  acaoCrud := TAcaoCrud(none);
  //DmConnection.RollBack;
  TMessagePersistenceError.OnPostError(E.Message);
  Action := daAbort;
end;

procedure TFrmCadastro_Padrao.cdsDataReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
const
  ERRO_FOREIGN_KEY = 'foreign key constraint fails';
  ERRO_CHANGED_BY_ANOTHER_USER = 'changed by another user';
begin
  inherited;
  if POS(ERRO_FOREIGN_KEY, E.message) > 0 then
  begin
    TFrmMessage.Information('Existem depend�ncias que impedem salvar as altera��es realizadas nesse registro.');
    Action := TReconcileAction(raAbort);
  end
  {else if POS(ERRO_CHANGED_BY_ANOTHER_USER, E.message) > 0 then
  begin
    //Action := TReconcileAction(raCorrect);
    //Action := TReconcileAction(raMerge);
    //HandleReconcileError(DataSet, UpdateKind, E);
  end}
  else
  begin
    Action := TReconcileAction(raAbort);
    TFrmMessage.Failure('Falha ao tentar salvar as informa��es no banco de dados, infelizmente seus dados ser�o perdidos, informe ao suporte t�cnico.'+
      ' Mensagem Original: '+E.message);
  end;
end;

procedure TFrmCadastro_Padrao.ActRemoveExecute(Sender: TObject);
begin
  if TMessage.MessageDeleteAsk() then
  begin
    if SelecionarRegistroParaAlteracao(cxpcMain.ActivePage = cxtsSearch) then
    try
      TFrmMessage_Process.SendMessage('Realizando a exclus�o');
      AntesDeRemover;
      //DmConnection.StartTransaction;
      AoExcluir;

      if cdsData.IsEmpty then
      begin
        DepoisDeRemover;
        //DmConnection.Commit;
        AtualizarDadosDaConsulta(Excluir);
        if not fdmSearch.IsEmpty then
        begin
          SelecionarRegistroParaAlteracao;

          if cxpcMain.ActivePage = cxtsData then
            acaoCrud := TAcaoCrud(Visualizar);
        end
        else
        begin
          acaoCrud := TAcaoCrud(Pesquisar);
        end;
      end;
    finally
      GerenciarControlesFormulario;
      TFrmMessage_Process.CloseMessage;
    end
    else
      TFrmMessage.Information('O registro selecionado n�o foi encontrado na base de dados. Realize a pesquisa novamente!');
  end;
end;

procedure TFrmCadastro_Padrao.ActReportExecute(Sender: TObject);
begin
  AoImprimir;
end;

procedure TFrmCadastro_Padrao.ActRestaurarColunasPadraoExecute(Sender: TObject);
begin
  TUsuarioGridView.DeleteView(TSistema.Sistema.Usuario.idSeguranca, FIdentificadorConfiguracaoGradePesquisa);
  Level1BandedTableView1.RestoreDefaults;
end;

procedure TFrmCadastro_Padrao.ActAbrirConsultandoExecute(Sender: TObject);
var consultandoAutomaticamente: boolean;
begin
  consultandoAutomaticamente := FConsultaAutomatica;
  TUsuarioPesquisaPadrao.SetConsultaAutomatica(
    TSistema.Sistema.Usuario.idSeguranca,
    FIdentificadorConfiguracaoGradePesquisa, not consultandoAutomaticamente);
  FConsultaAutomatica := not consultandoAutomaticamente;
end;

procedure TFrmCadastro_Padrao.ActAlterarColunasGridExecute(Sender: TObject);
begin
  TFrmApelidarColunasGrid.SetColumns(Level1BandedTableView1);
end;

procedure TFrmCadastro_Padrao.ActAlterarSQLPesquisaPadraoExecute(
  Sender: TObject);
begin
  if TFrmAlterarSQLPesquisaPadrao.AlterarSQLPesquisaPadrao(
    TSistema.Sistema.Usuario.idSeguranca, FIdentificadorConfiguracaoGradePesquisa) then
    AbrirPesquisaPadrao;
end;

procedure TFrmCadastro_Padrao.ActSalvarConfiguracoesExecute(Sender: TObject);
begin
  TUsuarioGridView.SaveGridView(Level1BandedTableView1,
    TSistema.Sistema.usuario.idSeguranca, FIdentificadorConfiguracaoGradePesquisa);
end;

procedure TFrmCadastro_Padrao.ActSearchExecute(Sender: TObject);
begin
  ExecutarConsulta;
end;

procedure TFrmCadastro_Padrao.ActTotalizadorTotalizarPorSumExecute(Sender: TObject);
begin
  try
    Level1BandedTableView1.DataController.Summary.BeginUpdate;
    Level1BandedTableView1.DataController.Summary.SummaryGroups.Clear;
    Summarize(Level1BandedTableView1.DataController.Summary, Level1BandedTableView1.Columns[0], skSum, ',0');
  finally
    Level1BandedTableView1.DataController.Summary.EndUpdate;
  end;
end;

procedure TFrmCadastro_Padrao.Summarize(ASummary: TcxDataSummary; AColumn: TcxGridDBBandedColumn;
  AKind: TcxSummaryKind; AFormat: string);
var
  sumGroup: TcxDataSummaryGroup;
  link: TcxGridTableSummaryGroupItemLink;
  item: TcxGridDBTableSummaryItem;
begin
  AColumn.Summary.FooterKind := AKind;
  AColumn.Summary.FooterFormat := AFormat;
  AColumn.Summary.GroupKind := AKind;
  AColumn.Summary.GroupFormat := AFormat;
  AColumn.GroupIndex := -1;
  sumGroup := ASummary.SummaryGroups.Add;
  link := sumGroup.Links.Add as TcxGridTableSummaryGroupItemLink;
  link.Column :=  AColumn;
  item := sumGroup.SummaryItems.Add as TcxGridDBTableSummaryItem;
  item.Column := AColumn;
  item.Kind := skSum;
  item.Position := spGroup;
  item.Format := AColumn.Summary.FooterFormat;
end;

function TFrmCadastro_Padrao.SelecionarRegistroParaAlteracao(
  const AUsarDatasetConsulta: Boolean = true): boolean;
var id: Integer;
begin
  try
    AntesDeSelecionarRegistro;

    if cdsData.Active and not(AUsarDatasetConsulta) then
    begin
      id := cdsData.FieldByName('id').AsInteger;
    end
    else
    begin
      id := fdmSearch.FieldByName('id').AsInteger;
    end;

    TFrmMessage_Process.SendMessage('Buscando dados');

    if id > 0 then
    begin
      cdsData.Fechar;
      cdsData.Params.Clear;
      cdsData.FetchParams;
      cdsData.Params.ParamByName('id').AsInteger := id;
      cdsData.Open;
    end;
  finally
    DepoisDeSelecionarRegistro;
    result := cdsData.Active and not(cdsData.IsEmpty);
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmCadastro_Padrao.FecharFormulario;
begin
  if (cxpcMain.ActivePage <> cxtsSearch) then
  begin
    try
      TControlsUtils.CongelarFormulario(Self);
      if (cdsData.State in dsEditModes) and (TFrmMessage.Question('O formul�rio est� em edi��o, deseja sair e cancelar as altera��es?') = MCONFIRMED) then
      begin
        ActCancel.Execute;
      end;

      cxpcMain.ActivePage := cxtsSearch;
      Application.ProcessMessages;
      Close;
    finally
      TControlsUtils.DescongelarFormulario;
    end;
  end
  else
  begin
    Close;
  end;
  abort;
end;

procedure TFrmCadastro_Padrao.FocarContainer(AControl: TWinControl = nil);
var container: TWinControl;
begin
  if cxpcMain.ActivePage = cxtsData then
  begin
    if AControl <> nil then
      container := AControl
    else
      container := cxGBDadosMain;

    if (container <> nil) and (container.CanFocus) then
    begin
      container.SetFocus;
      Self.SelectNext(container, true, true);
    end;
  end;
end;

procedure TFrmCadastro_Padrao.FocarProximoControle(AControl: TWinControl);
begin
  Self.SelectNext(AControl, true, true);
end;

procedure TFrmCadastro_Padrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  { TODO : Mudar Usuario }
  Action := caFree;
  AoDestruirFormulario;
  dxMainRibbon.ShowTabGroups := false;
  Application.ProcessMessages;
  TControlFunction.RemoveInstance(Self.ClassName);
  Application.MainForm.SetFocus;
end;

procedure TFrmCadastro_Padrao.FormCreate(Sender: TObject);
begin
  AoCriarFormulario;
end;

procedure TFrmCadastro_Padrao.GerenciarControles;
begin
  //Implementar nos formularios herdados
end;

procedure TFrmCadastro_Padrao.GerenciarControlesFormulario;
var bEdicao: Boolean;
    bExisteRegistro: Boolean;
    bEstaVisualizando: Boolean;
    bEdicaoBloqueada: Boolean;
    bEstaPesquisando: Boolean;
begin
  AntesDeGerenciarControlesFormulario;
  try
    //Realiza o desbloqueio de edicao
    DesbloquearEdicao;

    //Realiza o bloqueio de edicao - VIRTUAL
    BloquearEdicao;

    if cdsData.Active then
      bEdicao := cdsData.State in dsEditModes
    else
      bEdicao := false;

    if fdmSearch.Active then
      bExisteRegistro := fdmSearch.RecordCount > 0
    else
      bExisteRegistro := false;

    bEstaVisualizando := acaoCrud = TAcaoCrud(Visualizar);
    bEstaPesquisando := acaoCrud = TAcaoCrud(Pesquisar);
    bEdicaoBloqueada := cdsData.SomenteLeitura;

    ActVisualizarRegistro.Visible := not(bEdicao) and bExisteRegistro and not(bEstaVisualizando);
    ActInsert.Visible := not bEdicao;
    HabilitarAcaoAlterar;
    ActRemove.Visible := bExisteRegistro and not(bEdicao);
    ActReport.Visible := bExisteRegistro; //Definir
    ActSearch.Visible := not(bEdicao) or (bEstaPesquisando);
    ActClose.Visible := not bEdicao;
    ActPersonalizacoes.Visible := not(bEdicao) and
      TSistema.Sistema.Usuario.Administrador;

    ActDuplicarRegistro.Visible := bEstaVisualizando;

    GerenciarControles;

    ExibirBarraAcoes;
    dxBarReport.Visible := dxBarReport.ItemLinks.VisibleItemCount > 0;
    dxBarEnd.Visible := dxBarEnd.ItemLinks.VisibleItemCount > 0;
    dxBarSearch.Visible := dxBarSearch.ItemLinks.VisibleItemCount > 0;
    dxBarPersonalizacoes.Visible :=
      dxBarPersonalizacoes.ItemLinks.VisibleItemCount > 0;
    dxDesignDesign.Visible := false; //Definir

    if (cxpcMain.ActivePage = cxtsData) and bEstaPesquisando then
      cxpcMain.ActivePage := cxtsSearch
    else if (cxpcMain.ActivePage = cxtsSearch) and (bEdicao or bEstaVisualizando) then
    begin
      cxpcMain.ActivePage := cxtsData;
    end;

    HabilitarAcaoSalvar;
  finally
    DepoisDeGerenciarControlesFormulario;
  end;
end;

procedure TFrmCadastro_Padrao.HabilitarAcaoAlterar;
var
  bEdicao: Boolean;
  bExisteRegistro: Boolean;
  bEstaVisualizando: Boolean;
  bEdicaoBloqueada: Boolean;
begin
  if cdsData.Active then
    bEdicao := cdsData.State in dsEditModes
  else
    bEdicao := false;

  if fdmSearch.Active then
    bExisteRegistro := fdmSearch.RecordCount > 0
  else
    bExisteRegistro := false;

  bEstaVisualizando := acaoCrud = TAcaoCrud(Visualizar);
  bEdicaoBloqueada := cdsData.SomenteLeitura;

  ActUpdate.Visible := not(bEdicao) and bExisteRegistro and bEstaVisualizando and not(bEdicaoBloqueada);
  ExibirBarraAcoes;
end;

procedure TFrmCadastro_Padrao.HabilitarAcaoSalvar;
var
  possuiAlteracoes: Boolean;
  estaVisualizando: Boolean;
begin
  possuiAlteracoes :=
    cdsData.Active and
    ((AcaoCrud in [Incluir]) or ((AcaoCrud in [Alterar]) and
    cdsData.Modified) or (cdsData.EstadoDeAlteracao));

  {if possuiAlteracoes and
    dxBarAction.Visible and
    (dxBarAction.ItemLinks.VisibleItemCount >= 2) then
    exit;}

  estaVisualizando := (AcaoCrud in [Visualizar]);

  try
    TControlsUtils.CongelarFormulario(Self);

    ActConfirm.Visible := possuiAlteracoes;
    ActCancel.Visible := possuiAlteracoes;
    dxBarAction.Visible := dxBarAction.ItemLinks.VisibleItemCount > 0;

    //Navega��o
    ActNavegacaoDataPrimeiro.Visible := estaVisualizando and not(possuiAlteracoes);
    ActNavegacaoDataAnterior.Visible := estaVisualizando and not(possuiAlteracoes);
    ActNavegacaoDataProximo.Visible := estaVisualizando and not(possuiAlteracoes);
    ActNavegacaoDataUltimo.Visible := estaVisualizando and not(possuiAlteracoes);

    dxBarNavegacaoData.Visible := dxBarNavegacaoData.ItemLinks.VisibleItemCount > 0;
  finally
    TControlsUtils.DescongelarFormulario;
  end;
end;

procedure TFrmCadastro_Padrao.Level1BandedTableView1DblClick(Sender: TObject);
begin
  if not fdmSearch.IsEmpty then
    ActVisualizarRegistro.Execute;
end;

procedure TFrmCadastro_Padrao.Level1BandedTableView1EditKeyPress(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Char);
begin
  Key := UpperCase(String(Key)).ToCharArray[0];
end;

procedure TFrmCadastro_Padrao.Level1BandedTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
const
  BOTAO_IMPRESSORA: Integer = 16;
  BOTAO_EXCEL: Integer = 17;
  BOTAO_PDF: Integer = 18;
begin
  inherited;
  if AButtonIndex = BOTAO_IMPRESSORA then
  begin
    dxComponentPrinterLink1.PDFExportOptions.DefaultFileName := Self.Caption;
    dxComponentPrinterLink1.PDFExportOptions.OpenDocumentAfterExport := true;
    dxComponentPrinterLink1.Component := cxGridPesquisaPadrao;
    dxComponentPrinterLink1.Preview();
  end
  else if AButtonIndex = BOTAO_EXCEL then
  begin
    TcxGridUtils.ExportarParaExcel(cxGridPesquisaPadrao, Self.Caption)
  end
  else if AButtonIndex = BOTAO_PDF then
  begin
    dxComponentPrinterLink1.PDFExportOptions.DefaultFileName := Self.Caption;
    dxComponentPrinterLink1.PDFExportOptions.OpenDocumentAfterExport := true;
    dxComponentPrinterLink1.Component := cxGridPesquisaPadrao;
    dxComponentPrinterLink1.ExportToPDF;
  end;
end;

procedure TFrmCadastro_Padrao.LimparListaParametros;
begin
  if Assigned(FParametros) then
    FreeAndNil(FParametros);
end;

class procedure TFrmCadastro_Padrao.ModoDeInclusaoFK(var AComponentFK: TgbDBButtonEditFK;
  const AClassName, ATableName: String);
var
  instanciaFormulario: TFrmCadastro_Padrao;
begin
  try
    try
      TFrmMessage_Process.SendMessage('Preparando modo de inclus�o');
      TControlsUtils.CongelarFormulario(Application.MainForm);
      instanciaFormulario := TFrmCadastro_Padrao(TControlFunction.GetInstance(AClassName));
      if Assigned(instanciaFormulario) then
      begin
        if instanciaFormulario.cdsData.EstadoDeAlteracao then
        begin
          TMessage.MessageInformation('O formul�rio est� em edi��o, salve ou cancele as altera��es pendentes'+
           ' para poder executar esta a��o.');

          exit;
        end;
      end
      else
      begin
        instanciaFormulario := TFrmCadastro_Padrao(
          TControlFunction.CreateMDIForm(AClassName));
      end;
      instanciaFormulario.FComponenteModoDeInclusaoFK := AComponentFK;
      instanciaFormulario.ActInsert.Execute;
      instanciaFormulario.Show;
      Application.ProcessMessages;
    finally
      TControlsUtils.DescongelarFormulario;
      TFrmMessage_Process.CloseMessage;
    end;
  except
    on e: exception do
    begin
      TFrmMessage.Information(e.message);
    end;
  end;
end;

procedure TFrmCadastro_Padrao.MostrarInformacoesImplantacao;
begin

end;

procedure TFrmCadastro_Padrao.ParametrizarFormulario;
begin
  AntesDeParametrizar;

  TFrmParametrosFormulario.Parametrizar(Self.Name,
    TSistema.Sistema.Usuario.IdSeguranca);

  InicializarListaParametros;

  DepoisDeParametrizar;
end;

procedure TFrmCadastro_Padrao.pmGridConsultaPadraoPopup(Sender: TObject);
begin
  ActAbrirConsultando.Caption := TFunction.Iif(FConsultaAutomatica,
    'Desabilitar Consulta Autom�tica', 'Habilitar Consulta Autom�tica');
end;

procedure TFrmCadastro_Padrao.pmTituloGridPesquisaPadraoPopup(Sender: TObject);
begin
  ActExibirAgrupamento.Caption := TFunction.Iif(Level1BandedTableView1.OptionsView.GroupByBox,
    'Ocultar Agrupamento', 'Exibir Agrupamento');
  ActExibirTotalizadorGrade.Caption := TFunction.Iif(Level1BandedTableView1.OptionsView.Footer,
    'Ocultar Totalizador', 'Exibir Totalizador');
end;

procedure TFrmCadastro_Padrao.PreencherValoresPadrao;
begin
  //insert de default

end;

procedure TFrmCadastro_Padrao.PrepararFiltros;
begin
  PnFiltroVertical.Width := 316;
  FFiltroVertical := TFrameFiltroVerticalPadrao.Create(pnFiltroVertical);
  FFiltroVertical.Parent := pnFiltroVertical;

  DefinirFiltros(FFiltroVertical);

  RetrairFiltro;

  if FFiltroVertical.ExisteFiltros then
  begin
    CriarFiltros(FFiltroVertical);
  end
  else
  begin
    pnFiltroVertical.Visible := false;
    spFiltroVertical.Visible := false;
  end;
end;

procedure TFrmCadastro_Padrao.RetrairFiltro;
begin
  spFiltroVertical.CloseSplitter;
end;

procedure TFrmCadastro_Padrao.SetbloquearEdicaoDados(const Value: Boolean);
begin
  FbloquearEdicaoDados := Value;
  cdsData.SomenteLeitura := Value;
end;

procedure TFrmCadastro_Padrao.SetWhereBundle;
begin
  ClearWhereBundle;
  FFiltroVertical.SetWhere(FWhereSearch);
  //Setar os filtros nos formul�rio herdados
end;

function TFrmCadastro_Padrao.UltimoRegistroGerado(const ATabela, ACampoRetorno: String): String;
begin
  result := DmConnection.ServerMethodsClient.GetUltimaChaveDaTabela(ATabela, ACampoRetorno);
end;

procedure TFrmCadastro_Padrao.ValidarParametros;
var mensagemParametrosDesconfigurados: String;
begin
  if not FParametros.ParametrosConfigurados(mensagemParametrosDesconfigurados) then
  begin
    TMessage.MessageFailure(mensagemParametrosDesconfigurados);
  end;
end;

procedure TFrmCadastro_Padrao.ClearWhereBundle;
begin
  WhereSearch.Limpar;
end;

procedure TFrmCadastro_Padrao.ConfigurarEventoInserirValoresPadrao(ADataset: TgbClientDataset);
var i: Integer;
begin
  ADataset.AfterInsert := InserirValoresPadrao;

  for i := 0 to Pred(ADataset.FListaDetail.Count) do
  begin
    ConfigurarEventoInserirValoresPadrao(ADataset.FListaDetail[i]);
  end;
end;

procedure TFrmCadastro_Padrao.ConfigurarAcionamentoPorAtalho;
var i: Integer;
  atalho: TShortCut;
begin
  if not Assigned(FListaAcoes) then
    Exit;

  for i := 0 to Pred(ActionListMain.ActionCount) do
  begin
    if ActionListMain[I].Visible then
    begin
      atalho := 0;
      if FListaAcoes.TryGetValue(ActionListMain[I].Name, atalho) then
      begin
        ActionListMain[I].ShortCut := atalho;
      end;
    end
    else
    begin
      ActionListMain[I].ShortCut := 0
    end;
  end;
end;

procedure TFrmCadastro_Padrao.ConfirmarMestreAntesDeAbrirDetalhe;
begin
  cdsData.Salvar;
end;

procedure TFrmCadastro_Padrao.Consultar;
var
  DSList: TFDJSONDataSets;
  FiltroDaGrade: TcxDBDataFilterCriteria;
begin
  inherited;
  try
    FiltroDaGrade := TcxDBDataFilterCriteria.Create(Level1BandedTableView1.DataController);
    try
      if Level1BandedTableView1.DataController.Filter.IsFiltering then
      begin
        FiltroDaGrade.Assign(Level1BandedTableView1.DataController.Filter);
        Level1BandedTableView1.DataController.Filter.Clear;
      end;

      DSList := DmConnection.ServerMethodsClient.GetDados(FIdentificadorConfiguracaoGradePesquisa,
        WhereSearch.where, TSistema.Sistema.Usuario.idSeguranca);
      try
        TDatasetUtils.JSONDatasetToMemTable(DSList, fdmSearch);

        if FiltroDaGrade.FilterText.IsEmpty then
        begin
          TcxGridUtils.AdicionarTodosCamposNaView(Level1BandedTableView1);
          TcxGridUtils.OcultarTodosCamposView(Level1BandedTableView1);

          TUsuarioGridView.LoadGridView(Level1BandedTableView1,
            TSistema.Sistema.Usuario.idSeguranca, FIdentificadorConfiguracaoGradePesquisa);
        end;

        PosicionarGradeDePesquisaNoPrimeiroRegistro;
      finally
        FreeAndNil(DSList);
      end;
      ClearWhereBundle;

      ExibirFiltroDeMemoriaDaGrid((not fdmSearch.IsEmpty) or (not FiltroDaGrade.FilterText.IsEmpty));

      if Assigned(FiltroDaGrade) then
      try
        Level1BandedTableView1.DataController.Filter := FiltroDaGrade;
      except
      end;
    finally
      FreeAndNil(FiltroDaGrade);
    end;
  Except
    on e : Exception do
    begin
      if e is TDBXError then
      begin
        TFrmMessage.Failure('Falha ao executar a consulta vinculada a tela.');
      end
      else
      begin
        TFrmMessage.Failure(e.message);
      end;

      abort;
    end;
  end;
end;

procedure TFrmCadastro_Padrao.PosicionarGradeDePesquisaNoPrimeiroRegistro;
begin
  //Rolar para a primeira linha
  Level1BandedTableView1.Controller.TopRowIndex := 1;
end;

procedure TFrmCadastro_Padrao.CriarFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
begin
  AFiltroVertical.CriarFiltros;
end;

procedure TFrmCadastro_Padrao.CriarParametrosFormulario(
  var AParametros: TListaParametroFormulario);
begin

end;

procedure TFrmCadastro_Padrao.cxpcMainChange(Sender: TObject);
begin
  if cxpcMain.ActivePage = cxtsData then
  begin
    FocarContainer;
    Self.PopupMenu := nil;
    AtualizarDescricaoNavegacaoRegistros;
  end
  else if cxpcMain.ActivePage = cxtsSearch then
  begin
    Self.PopupMenu := pmGridConsultaPadrao;
  end;
end;

procedure TFrmCadastro_Padrao.cxpcMainEnter(Sender: TObject);
begin
  inherited;
  if cxpcMain.ActivePage = cxtsSearch then
    TControlsUtils.SetFocus(cxGridPesquisaPadrao);
end;

procedure TFrmCadastro_Padrao.DefinirFiltros(var AFiltroVertical: TFrameFiltroVerticalPadrao);
begin

end;

procedure TFrmCadastro_Padrao.DepoisDeAlterar;
begin
  //
end;

procedure TFrmCadastro_Padrao.DepoisDeCancelar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.DepoisDeCarregarRegistroDuplicado;
begin

end;

procedure TFrmCadastro_Padrao.DepoisDeConfirmar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.DepoisDeDefinirFiltros;
begin

end;

procedure TFrmCadastro_Padrao.DepoisDeDuplicarRegistro(const AIdNovoRegistro: Integer);
begin

end;

procedure TFrmCadastro_Padrao.DepoisDeGerenciarControlesFormulario;
begin
  ConfigurarAcionamentoPorAtalho;
  TControlsUtils.DescongelarFormulario;
end;

procedure TFrmCadastro_Padrao.DepoisDeInserir;
begin
  PreencherValoresPadrao;
  //Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.DepoisDeParametrizar;
begin

end;

procedure TFrmCadastro_Padrao.DepoisDePesquisar;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.DepoisDeRemover;
begin
  // Implementar na classe herdada.
end;

procedure TFrmCadastro_Padrao.DepoisDeSelecionarRegistro;
begin
  //Implementar na classe herdadada
  //Envento disparado depois que um registro � carregado ao cdsData
end;

procedure TFrmCadastro_Padrao.DepoisDeVisualizar;
begin
  //Implementar na classe herdada
end;

procedure TFrmCadastro_Padrao.DesbloquearEdicao;
begin
  cdsData.SomenteLeitura := false;
end;

function TFrmCadastro_Padrao.DuplicarRegistro: Integer;
begin

end;

procedure TFrmCadastro_Padrao.ExecutarConsulta;
begin
  ActiveControl := nil;

  if cdsData.Active then
    cdsData.Fechar;

  try
    AcaoCrud := TAcaoCrud(Pesquisar);
    if (cxpcMain.ActivePage = cxtsSearch) or
      ((cxpcMain.ActivePage = cxtsData) and FConsultaAutomatica) then
    begin
      TFrmMessage_Process.SendMessage('Consultando dados');
      try
        SetWhereBundle;
        AntesDePesquisar;
        Consultar;
        DepoisDePesquisar;
      finally
        TFrmMessage_Process.CloseMessage;
        if (cxpcMain.ActivePage = cxtsSearch) and (Self.Visible) then
          TControlsUtils.SetFocus(cxGridPesquisaPadrao);
      end;
    end;
  finally
    GerenciarControlesFormulario;
  end;
end;

procedure TFrmCadastro_Padrao.ExecutarIncluir;
begin
  acaoCrud := TAcaoCrud(Incluir);
  //  DmConnection.StartTransaction;
  DesbloquearEdicao;
  cdsData.Fechar;
  cdsData.Params.Clear;
  cdsData.FetchParams;
  cdsData.Params.ParamByName('id').AsInteger := CID_NULL;
  cdsData.Open;

  if not Assigned(cdsData.AfterInsert) then
    ConfigurarEventoInserirValoresPadrao(cdsData);

  cdsData.Incluir;
  FocarContainer;
  GerenciarControlesFormulario;
end;

procedure TFrmCadastro_Padrao.ExibirBarraAcoes;
begin
  dxBarManager.Visible := dxBarManager.ItemLinks.VisibleItemCount > 0;
end;

procedure TFrmCadastro_Padrao.ExibirFiltroDeMemoriaDaGrid(
  const AExibir: Boolean);
begin
  if (Level1BandedTableView1.FilterRow.Visible) and AExibir then
  begin
    exit;
  end;

  Level1BandedTableView1.FilterRow.Visible := AExibir;
end;

procedure TFrmCadastro_Padrao.ExpandirFiltro;
begin
  spFiltroVertical.OpenSplitter;
end;

class procedure TFrmCadastro_Padrao.ExplorarRegistroFK(var AComponentFK: TgbDBButtonEditFK);
var
  instanciaFormulario: TFrmCadastro_Padrao;
begin
  try
    try
      TFrmMessage_Process.SendMessage('Buscando informa��es');
      TControlsUtils.CongelarFormulario(Application.MainForm);
      instanciaFormulario := TFrmCadastro_Padrao(TControlFunction.GetInstance(AComponentFK.gbClasseDoCadastro));
      if Assigned(instanciaFormulario) then
      begin
        if instanciaFormulario.cdsData.EstadoDeAlteracao then
        begin
          TMessage.MessageInformation('O formul�rio est� em edi��o, salve ou cancele as altera��es pendentes'+
           ' para poder executar esta a��o.');

          exit;
        end;
      end
      else
      begin
        instanciaFormulario := TFrmCadastro_Padrao(
          TControlFunction.CreateMDIForm(AComponentFK.gbClasseDoCadastro));
      end;
      instanciaFormulario.FComponenteModoDeInclusaoFK := AComponentFK;
      instanciaFormulario.cxpcMain.ActivePage := instanciaFormulario.cxtsSearch;
      instanciaFormulario.WhereSearch.AddIgual(AComponentFK.gbTableName+'.'+AComponentFK.gbCampoPK,
        VartoStr(AComponentFK.EditValue));
      instanciaFormulario.Consultar;
      instanciaFormulario.ActVisualizarRegistro.Execute;
      instanciaFormulario.Show;
      Application.ProcessMessages;
    finally
      TControlsUtils.DescongelarFormulario;
      TFrmMessage_Process.CloseMessage;
    end;
  except
  end;
end;

procedure TFrmCadastro_Padrao.fdmSearchAfterClose(DataSet: TDataSet);
begin
  ExibirFiltroDeMemoriaDaGrid(false);
end;

procedure TFrmCadastro_Padrao.fdmSearchAfterDelete(DataSet: TDataSet);
begin
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
end;

procedure TFrmCadastro_Padrao.fdmSearchAfterOpen(DataSet: TDataSet);
begin
  ExibirFiltroDeMemoriaDaGrid(not fdmSearch.IsEmpty);
  Level1BandedTableView1.DataController.Groups.FullExpand;
  fdmSearch.First;
end;

procedure TFrmCadastro_Padrao.AtualizarDescricaoNavegacaoRegistros;
begin
  dxBarNavegacaoData.Caption := 'Navega��o  '+FormatFloat('00',fdmSearch.Recno) +' de '+
    FormatFloat('00',fdmSearch.RecordCount);

{  if fdmSearch.RecordCount > 1 then
  begin
    ActNavegacaoDataPrimeiro.Caption := 'Primeiro 01 de '+
      FormatFloat('00',fdmSearch.RecordCount);

    if fdmSearch.Recno = 1 then
    begin
      ActNavegacaoDataAnterior.Caption := 'Anterior '+FormatFloat('00',fdmSearch.Recno) +' de '+
        FormatFloat('00',fdmSearch.RecordCount);
    end
    else
    begin
      ActNavegacaoDataAnterior.Caption := 'Anterior '+FormatFloat('00',fdmSearch.Recno-1) +' de '+
        FormatFloat('00',fdmSearch.RecordCount);
    end;

    if fdmSearch.Recno = fdmSearch.RecordCount then
    begin
      ActNavegacaoDataProximo.Caption := 'Pr�ximo '+FormatFloat('00',fdmSearch.RecordCount) +' de '+
        FormatFloat('00',fdmSearch.RecordCount);
    end
    else
    begin
      ActNavegacaoDataProximo.Caption := 'Pr�ximo '+FormatFloat('00',fdmSearch.Recno+1) +' de '+
        FormatFloat('00',fdmSearch.RecordCount);
    end;

    ActNavegacaoDataUltimo.Caption := '�ltimo '+FormatFloat('00',fdmSearch.RecordCount) +' de '+
      FormatFloat('00',fdmSearch.RecordCount);
  end
  else
  begin
    ActNavegacaoDataPrimeiro.Caption := 'Primeiro';
    ActNavegacaoDataAnterior.Caption := 'Anterior';
    ActNavegacaoDataProximo.Caption := 'Pr�ximo';
    ActNavegacaoDataUltimo.Caption := '�ltimo';
  end;   }
end;

procedure TFrmCadastro_Padrao.AtualizarRegistro(AId: Integer);
begin
  if not Assigned(cdsData) then
  begin
    Exit;
  end;

  try
    TFrmMessage_Process.SendMessage('Atualizando dados');

    cdsData.Fechar;
    Application.ProcessMessages;
    cdsData.Params.Clear;
    cdsData.FetchParams;
    cdsData.Params.ParamByName('id').AsInteger := AId;
    cdsData.Open;

    GerenciarControlesFormulario;
  finally
    TFrmMessage_Process.CloseMessage;
  end;
end;

procedure TFrmCadastro_Padrao.AtualizarRegistro;
begin
  if not Assigned(cdsData) then
  begin
    Exit;
  end;

  if cdsData.Active and (cdsData.FieldByName('ID').AsInteger > 0) then
  begin
    AtualizarRegistro(cdsData.FieldByName('ID').AsInteger);
  end;
end;

end.





