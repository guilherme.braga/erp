inherited FrmExibirListaMensagemSimples: TFrmExibirListaMensagemSimples
  Caption = ''
  ExplicitWidth = 664
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited cxGroupBox2: TcxGroupBox
    object PnTopo: TcxGroupBox
      AlignWithMargins = True
      Left = 5
      Top = 5
      Align = alTop
      PanelStyle.Active = True
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      Visible = False
      Height = 20
      Width = 648
    end
    object PnMensagens: TcxGroupBox
      Left = 2
      Top = 28
      Align = alClient
      PanelStyle.Active = True
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Height = 239
      Width = 654
    end
  end
  object ViewListaMensagens: TcxTreeView [2]
    Left = 0
    Top = 0
    Width = 658
    Height = 269
    Align = alClient
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 2
    ReadOnly = True
    RowSelect = True
    ShowButtons = False
    ShowLines = False
    ShowRoot = False
  end
  inherited ActionList: TActionList
    inherited ActConfirmar: TAction
      Visible = False
    end
  end
end
