unit uAjusteEstoqueProxy;

interface

Uses Generics.Collections, SysUtils;

type TAjusteEstoqueItemProxy = class
  FId: Integer;
  FQuantidade: Double;
  FIdProduto: Integer;
  FIdAjusteEstoque: Integer;
  FNrItem: Integer;
  FTipo: String;
end;

type TAjusteEstoqueProxy = class
  FId: Integer;
  FDhCadastro: String;
  FDhFechamento: String;
  FStatus: String;
  FIdFilial: Integer;
  FIdPessoaUsuario: Integer;
  FIdMotivo: Integer;
  FIdChaveProcesso: Integer;
  FAjusteEstoqueItem: TList<TAjusteEstoqueItemProxy>;

  const STATUS_ABERTO= 'ABERTO';
  const STATUS_CONFIRMADO ='CONFIRMADO';
  const STATUS_CANCELADO  = 'CANCELADO';

  const TIPO_ENTRADA = 'ENTRADA';
  const TIPO_SAIDA = 'SAIDA';

  constructor Create;
  destructor Destroy;
end;

implementation

{ TAjusteEstoqueProxy }

constructor TAjusteEstoqueProxy.Create;
begin
  FAjusteEstoqueItem := TList<TAjusteEstoqueItemProxy>.Create;
end;

destructor TAjusteEstoqueProxy.Destroy;
begin
  FreeAndNil(FAjusteEstoqueItem);
end;

end.
