unit uPessoaCreditoProxy;

interface

Type TPessoaCreditoProxy = class
  public
  FId: Integer;
  FVlCredito: Double;
  FIdPessoa: Integer;
end;

type TPessoaCreditoMovimentoProxy = class
  FId: Integer;
  FVlCreditoAnterior: Double;
  FVlCreditoMovimento: Double;
  FVlCreditoAtual: Double;
  FDhMovimento: String;
  FIdDocumentoOrigem: Integer;
  FDocumentoOrigem: String;
  FIdPessoa: Integer;
  FIdPessoaUsuario: Integer;
  FIdChaveProcesso: Integer;
  FTipo: String; //CREDITO/DEBITO'

  const TIPO_CREDITO: String = 'CREDITO';
  const TIPO_DEBITO: String = 'DEBITO';
  const TABELA_PESSOA_CREDITO: String = 'PESSOA_CREDITO';
  const TABELA_PESSOA_CREDITO_MOVIMENTO: String = 'PESSOA_CREDITO_MOVIMENTO';
  const DOCUMENTO_ORIGEM_VENDA = 'VENDA';
end;

implementation

end.
