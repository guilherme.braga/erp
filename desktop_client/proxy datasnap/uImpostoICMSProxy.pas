unit uImpostoIcmsProxy;

Interface

type TImpostoIcmsProxy = class
  FId: String;
  FBoIncluirFreteNaBaseIpi: String;
  FBoIncluirFreteNaBaseIcms: String;
  FBoIncluirIpiNaBaseIcms: String;
  FPercReducaoIcmsSt: Double;
  FModalidadeBaseIcms: Integer;
  FModalidadeBaseIcmsSt: Integer;
  FPercReducaoIcms: Double;
  FPercAliquotaIcms: Double;
  FPercAliquotaIcmsSt: Double;
  FPercMvaAjustadoSt: Double;
  FPercMvaProprio: Double;
  FMotivoDesoneracaoIcms: Integer;
  FPercBaseIcmsProprio: Double;
  FPercAliquotaIcmsProprioParaSt: Double;
  FBoDestacarValorIcms: String;
  FIdCstCsosn: Integer;
end;

type TEntradaCalculoImpostoIcmsProxy = class
  FImpostoIcms: TImpostoIcmsProxy;
  FCrtEmitente: Integer;
  FCST: String;
  FQuantidadeItem: Double;
  FValorUnitario: Double;
  FValorSeguro: Double;
  FValorOutrasDespesas: Double;
  FValorDesconto: Double;
end;

type TSaidaCalculoImpostoIcmsProxy = class
  FValorBaseOperacaoCalculado: Double;
  FValorBaseCalculoICMSCalculado: Double;
  FValorReducaoBaseCalculoICMSCalculado: Double;
  FPercentualBaseCalculoICMSCalculado: Double;
  FValorBaseCalculoICMSSTCalculado: Double;
  FValorReducaoBaseCalculoICMSSTCalculado: Double;
  FPercentualBaseCalculoICMSSTCalculado: Double;
  FValorICMSCalculado: Double;
  FPercentualICMSCalculado: Double;
  FValorICMSSTCalculado: Double;
  FPercentualICMSSTCalculado: Double;
  FValorCreditoICMSSimplesNacionalCalculado: Double;
end;

implementation

end.


