unit uMontadoraProxy;

Interface

type TMontadoraProxy = class
  FId: Integer;
  FDescricao: String;
  FBoAtivo: String;
  FObservacao: String;

  constructor Create;
end;

type TModeloMontadoraProxy = class
  FId: Integer;
  FDescricao: String;
  FBoAtivo: String;
  FObservacao: String;
  FIdMontadora: Integer;

  constructor Create;
end;

implementation

{ TMontadoraProxy }

constructor TMontadoraProxy.Create;
begin
  FBoAtivo := 'S';
end;

{ TModeloMontadoraProxy }

constructor TModeloMontadoraProxy.Create;
begin
  FBoAtivo := 'S';
end;

end.

