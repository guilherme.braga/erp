unit uContaAnaliseProxy;

interface

type TContaAnaliseProxy = class
  FIdContaAnalise: Integer;
  FDescricaoContaAnalise: String;

  const NOME_TABELA: String = 'CONTA_ANALISE';
end;

implementation

end.
