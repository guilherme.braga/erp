unit uProdutoReservaEstoqueMovimentoProxy;

Interface

type TProdutoReservaEstoqueMovimentoProxy = class
  FId: Integer;
  FTipo: String;
  FDtMovimento: String;
  FQtEstoqueAnterior: Double;
  FQtMovimento: Double;
  FQtEstoqueAtual: Double;
  FDocumentoOrigem: String;
  FIdProduto: Integer;
  FIdFilial: Integer;
  FIdChaveProcesso: Integer;
  FBoProcessoAmortizado: String;
  FTipoReserva: String;

  const TIPO_RESERVA_ESTOQUE_CONDICIONAL = 'CONDICIONAL';

  const TIPO_ENTRADA= 'ENTRADA';
  const TIPO_SAIDA = 'SAIDA';
end;

implementation

end.

