unit uContaPagarProxy;

interface

  type TContaPagarMovimento = class

    public

      FId : Integer;
      FIdPessoa : Integer;

      FDocumento  : string;
      FDescricao  : string;
      FStatus     : string;

      FDtVencimento : string;
      FDhCadastro   : string;
      FDtCompetencia : string;
      FDtDocumento   : string;

      FVlTitulo  : Double;
      FVlQuitado : Double;
      FVlAberto  : Double;

      FVlAcrescimo : Double;
      FVlDecrescimo : Double;
      FVlQuitadoLiquido : Double;

      FVlJuros       : Double;
      FVlMulta       : Double;
      FPercJuros     : Double;
      FPercMulta     : Double;

      FQtParcela : Integer;
      FNrParcela : Integer;

      FSequencia  : string;
      FBoVencido  : string;
      FObservacao : string;

      FIdContaAnalise    : Integer;
      FIdCentroResultado : Integer;
      FIdContaCorrente   : Integer;

      FIdChaveProcesso  : Integer;
      FIdFormaPagamento : Integer;
      FIdCarteira : Integer;

      FIdFilial: Integer;

      FIdDocumentoOrigem  : Integer;
      FTPDocumentoOrigem  : string;
      FVlTituloOriginalHistorico: Double;
      const

        ABERTO    = 'ABERTO';
        QUITADO   = 'QUITADO';
        CANCELADO = 'CANCELADO';

        ORIGEM_GERACAO_DOCUMENTO = 'GERACAO DE DOCUMENTO';
        ORIGEM_MANUAL            = 'MANUAL';
        ORIGEM_VENDA             = 'VENDA';

        SIM = 'SIM';
        NAO = 'NAO';

        FIELD_PESSOA = 'CONTA_PAGAR.ID_PESSOA';

   end;

   type TContaPagarParametrizacao = class
     public
       FGerarContraPartidaContaCorrenteMovimento: Boolean;

       constructor Create;
   end;

   type TContaPagarQuitacaoMovimento = class
     public

      FId           : Integer;
      FNrItem       : Integer;
      FIdContaPagar : Integer;

      FDhCadastro : string;
      FDtQuitacao : string;
      FObservacao : string;

      FIdFilial        : Integer;
      FIdTipoQuitacao  : Integer;
      FIdContaCorrente : Integer;
      FIdContaAnalise  : Integer;
      FIdCentroResultado: Integer;

      FVlDesconto  : Double;
      FVlAcrescimo : Double;
      FVlQuitacao  : Double;
      FVlTotal     : Double;

      FVlTroco       : Double;

      FPercAcrescimo : Double;
      FPercDesconto  : Double;

      FVlJuros       : Double;
      FVlMulta       : Double;
      FPercJuros     : Double;
      FPercMulta     : Double;

      FIdDocumentoOrigem  : Integer;
      FTPDocumentoOrigem  : string;
      FStatus             : string;

      FChequeSacado: string;
      FChequeDocFederal: string;
      FChequeBanco: string;
      FChequeDtEmissao: string;
      FChequeDtVencimento: string;
      FChequeAgencia: string;
      FChequeContaCorrente: string;
      FChequeNumero: Integer;
      FIdOperadoraCartao: Integer;
      FIdUsuarioBaixa: Integer;

      const
        ORIGEM_MANUAL         = 'MANUAL';
        ORIGEM_LOTE_PAGAMENTO = 'LOTE DE PAGAMENTO';
        STATUS_CONFIRMADA     = 'CONFIRMADA';
        ORIGEM_GERACAO_DOCUMENTO = 'GERA��O DE DOCUMENTO';
        ORIGEM_VENDA            = 'VENDA';

      constructor Create;
      destructor Destroy;
   end;

implementation

{ TContaPagarParametrizacao }

Uses SysUtils;

constructor TContaPagarParametrizacao.Create;
begin
  FGerarContraPartidaContaCorrenteMovimento := True;
end;

constructor TContaPagarQuitacaoMovimento.Create;
begin
  //FContaPagarParametrizacao := TContaPagarParametrizacao.Create;
end;

destructor TContaPagarQuitacaoMovimento.Destroy;
begin
  //if Assigned(FContaPagarParametrizacao) then
  //begin
  //  FreeAndNil(FContaPagarParametrizacao);
  //end;
end;

end.
