unit uContaReceberProxy;

interface

  type TContaReceberMovimento = class

    public

      FId : Integer;
      FIdPessoa : Integer;

      FDocumento  : string;
      FDescricao  : string;
      FStatus     : string;

      FDtVencimento  : string;
      FDhCadastro    : string;
      FDtCompetencia : string;
      FDtDocumento   : string;

      FVlTitulo  : Double;
      FVlQuitado : Double;
      FVlAberto  : Double;

      FVlAcrescimo : Double;
      FVlDecrescimo : Double;
      FVlQuitadoLiquido : Double;

      FVlJuros       : Double;
      FVlMulta       : Double;
      FPercJuros     : Double;
      FPercMulta     : Double;

      FQtParcela : Integer;
      FNrParcela : Integer;

      FSequencia  : string;
      FBoVencido  : string;
      FObservacao : string;

      FIdContaAnalise    : Integer;
      FIdCentroResultado : Integer;
      FIdContaCorrente   : Integer;

      FIdChaveProcesso  : Integer;
      FIdFormaPagamento : Integer;
      FIdCarteira : Integer;

      FIdFilial: Integer;

      FIdDocumentoOrigem  : Integer;
      FTPDocumentoOrigem  : string;

      FBoNegociado : String;
      FBoGeradoPorNegociacao: String;
      FVlNegociado: Double;
      FVlTituloAntesNegociacao: Double;
      FBoBoletoEmitido: String;
      FBoletoNossoNumero: Integer;
      FDhEmissaoBoleto: String;
      FBoletoLinhaDigitavel: String;
      FBoletoCodigoBarras: String;
      FVlTituloOriginalHistorico: Double;
      const

        RETORNO_SUCESSO  = 'RETORNO_SUCESSO';

        ABERTO    = 'ABERTO';
        QUITADO   = 'QUITADO';
        CANCELADO = 'CANCELADO';

        ORIGEM_GERACAO_DOCUMENTO = 'GERACAO DE DOCUMENTO';
        ORIGEM_MANUAL            = 'MANUAL';
        ORIGEM_VENDA            = 'VENDA';
        ORIGEM_ORDEM_SERVICO    = 'ORDEM DE SERVI�O';
        ORIGEM_NOTA_FISCAL      = 'NOTA FISCAL';

        SIM = 'SIM';
        NAO = 'NAO';

        FIELD_PESSOA = 'CONTA_RECEBER.ID_PESSOA';
     constructor Create;
   end;

   type TContaReceberParametrizacao = class
     public
       FGerarContraPartidaContaCorrenteMovimento: Boolean;

       constructor Create;
   end;

   type TContaReceberQuitacaoMovimento = class
     public

      FId             : Integer;
      FNrItem         : Integer;
      FIdContaReceber : Integer;

      FDhCadastro : string;
      FDtQuitacao : string;
      FObservacao : string;

      FIdFilial        : Integer;
      FIdTipoQuitacao  : Integer;
      FIdContaCorrente : Integer;
      FIdCentroResultado: Integer;
      FIdContaAnalise  : Integer;

      FVlDesconto  : Double;
      FVlAcrescimo : Double;
      FVlQuitacao  : Double;
      FVlTotal     : Double;

      FVlTroco       : Double;

      FVlJuros       : Double;
      FVlMulta       : Double;
      FPercJuros     : Double;
      FPercMulta     : Double;

      FPercAcrescimo : Double;
      FPercDesconto  : Double;

      FIdDocumentoOrigem  : Integer;
      FTPDocumentoOrigem  : string;
      FStatus             : string;

      FChequeSacado: string;
      FChequeDocFederal: string;
      FChequeBanco: string;
      FChequeDtEmissao: string;
      FChequeDtVencimento: string;
      FChequeAgencia: string;
      FChequeContaCorrente: string;
      FChequeNumero: Integer;
      FIdOperadoraCartao: Integer;
      FIdUsuarioBaixa: Integer; //Usuario que realizou a baixa

      //Parametrizacao
      //FContaReceberParametrizacao: TContaReceberParametrizacao;

      const

        ORIGEM_MANUAL           = 'MANUAL';
        ORIGEM_LOTE_RECEBIMENTO = 'LOTE DE RECEBIMENTO';
        ORIGEM_VENDA            = 'VENDA';
        ORIGEM_ORDEM_SERVICO    = 'ORDEM DE SERVI�O';
        ORIGEM_NOTA_FISCAL      = 'NOTA FISCAL';
        ORIGEM_GERACAO_DOCUMENTO = 'GERA��O DE DOCUMENTO';
        STATUS_CONFIRMADA       = 'CONFIRMADA';


      constructor Create;
      destructor Destroy;
   end;

implementation

{ TContaReceberMovimento }

Uses SysUtils;

constructor TContaReceberMovimento.Create;
begin
  Self.FBoNegociado := NAO;
  Self.FBoGeradoPorNegociacao := NAO;
  Self.FBoVencido := NAO;
  Self.FStatus := ABERTO;
  FVlNegociado := 0;
  FVlTituloAntesNegociacao := 0;
  FBoBoletoEmitido := 'N';
end;

{ TContaReceberParametrizacao }

constructor TContaReceberParametrizacao.Create;
begin
  FGerarContraPartidaContaCorrenteMovimento := True;
end;

{ TContaReceberQuitacaoMovimento }

constructor TContaReceberQuitacaoMovimento.Create;
begin
  //FContaReceberParametrizacao := TContaReceberParametrizacao.Create;
end;

destructor TContaReceberQuitacaoMovimento.Destroy;
begin
  //if Assigned(FContaReceberParametrizacao) then
  //begin
  //  FreeAndNil(FContaReceberParametrizacao);
  //end;
end;

end.
