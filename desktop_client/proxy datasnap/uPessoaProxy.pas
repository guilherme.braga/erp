unit uPessoaProxy;

interface

type TPessoaProxy = class
  FId: Integer;
  FDhCadastro: String;
  FTpPessoa: String;
  FBoCliente: String;
  FBoFornecedor: String;
  FBoAtivo: String;
  FBoTransportadora: String;
  FIdGrupoPessoa: Integer;
  FObservacao: String;
  FDtAssociacao: String;
  FRazaoSocial: String;
  FNome: String;
  FDocFederal: String;
  FDocEstadual: String;
  FDtNascimento: String;
  FNomePai: String;
  FNomeMae: String;
  FOrgaoEmissor: String;
  FSexo: String;
  FEmpresa: String;
  FEnderecoEmpresa: String;
  FNumeroEmpresa: Integer;
  FCepEmpresa: String;
  FCnpjEmpresa: String;
  FBairroEmpresa: String;
  FDtAdmissao: String;
  FTelefoneEmpresa: String;
  FRamalEmpresa: String;
  FRendaMensal: Double;
  FSuframa: String;
  FDocMunicipal: String;
  FIdNaturalidade: Integer;
  FIdCidadeEmpresa: Integer;
  FIdUfEmissor: Integer;
  FIdAreaAtuacao: Integer;
  FIdClassificacaoEmpresa: Integer;
  FIdAtividadePrincipal: Integer;
  FEstadoCivil: String;
  FTempoTrabalhoEmpresa: String;
  FQuantidadeFilhos: Integer;
  FTipoResidencia: String;
  FRendaExtra: String;
  FVlRendaExtra: Double;
  FVlLimite: Double;
  FOcupacao: String;
  FIdZoneamento: Integer;
  FCrt: Integer;
  FRegimeTributario: Integer;
  FBoContribuinteIcms: String;
  FBoContribuinteIpi: String;
  FBoContribuinte: String;
  FIdRegimeEspecial: Integer;
  FTipoEmpresa: String;
  FIdPessoaImportacao: Integer;
  FBoClienteConsumidorFinal: String;

  const
    NOME_TABELA    = 'PESSOA';

    SEXO_MASCULINO = 'MASCULINO';
    SEXO_FEMININO  = 'FEMININO';

    TIPO_PESSOA_FISICA   = 'FISICA';
    TIPO_PESSOA_JURIDICA  = 'JURIDICA';

    ESTADO_CIVIL_SOLTEIRO   = 'SOLTEIRO';
    ESTADO_CIVIL_CASADO     = 'CASADO';
    ESTADO_CIVIL_SEPARADO   = 'SEPARADO';
    ESTADO_CIVIL_DIVORCIADO = 'DIVORCIADO';
    ESTADO_CIVIL_VIUVO      = 'VIUVO';

    TIPO_CONTATO_EMAIL    = 'EMAIL';
    TIPO_CONTATO_FACEBOOK = 'FACEBOOK';
    TIPO_CONTATO_LINKEDIN = 'LINKEDIN';
    TIPO_CONTATO_SITE     = 'SITE';
    TIPO_CONTATO_SKYPE    = 'SKYPE';
    TIPO_CONTATO_TELEFONE = 'TELEFONE';
    TIPO_CONTATO_TWITTER  = 'TWITTER';
    TIPO_CONTATO_VIBER    = 'VIBER';
    TIPO_CONTATO_WHATSAPP = 'WHASAPP';
    TIPO_CONTATO_OUTROS   = 'OUTROS';

    TIPO_ENDERECO_RESIDENCIAL = 'RESIDENCIAL';
    TIPO_ENDERECO_CONJUGE     = 'CONJUGE';
    TIPO_ENDERECO_COMERCIAL   = 'COMERCIAL';
    TIPO_ENDERECO_ENTREGA     = 'ENTREGA';
    TIPO_ENDERECO_COBRANCA    = 'COBRANCA';
    TIPO_ENDERECO_REFERENCIA  = 'REFERENCIA';

    TIPO_RESIDENCIA_PROPRIA = 'PROPRIA';
    TIPO_RESIDENCIA_ALUGADA = 'ALUGADA';
    TIPO_RESIDENCIA_FAMILIAR = 'FAMILIAR';

    FIELD_ID = 'PESSOA.ID';
end;

type TPessoaEnderecoProxy = class
  FId: Integer;
  FTipo: String;
  FLogradouro: String;
  FNumero: String;
  FComplemento: String;
  FObservacao: String;
  FIdPessoa: Integer;
  FBairro: String;
  FIdCidade: Integer;
  FCep: String;

  //Dados Transientes
  FCodigoIGBECidade: Integer;
  FCodigoIGBEEstado: Integer;
  FCodigoIGBEPais: Integer;
  FNomeCidade: String;
  FUF: String;
  FNomeEstado: String;
  FNomePais: String;
end;

type TPessoaContatoProxy = class
  FId: Integer;
  FContato: String;
  FTipo: String;
  FObservacao: String;
  FIdPessoa: Integer;
end;

{Transiente}
type TPessoaContatoPrincipalProxy = class
  FIdPessoa: Integer;
  FTelefone: String;
  FEmail: String;
end;


implementation

end.
