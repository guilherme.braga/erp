unit uNegociacaoContaReceberProxy;

Interface

type TNegociacaoContaReceberProxy = class
  FId: Integer;
  FDhCadastro: String;
  FVlTotal: Double;
  FVlAcrescimo: Double;
  FVlDesconto: Double;
  FVlLiquido: Double;
  FVlEntrada: Double;
  FDtParcelaInicial: String;
  FDtCompetenciaInicial: String;
  FStatus: String;
  FIdPessoa: Integer;
  FIdFilial: Integer;
  FIdChaveProcesso: Integer;
  FIdFormaPagamento: Integer;
  FIdPlanoPagamento: Integer;
  FIdContaAnalise: Integer;
  FIdCentroResultado: Integer;
  FIdContaCorrente: Integer;
  FObservacao: String;

  const
    STATUS_EFETIVADO = 'EFETIVADO';
    STATUS_ABERTO = 'ABERTO';
end;

implementation

end.
