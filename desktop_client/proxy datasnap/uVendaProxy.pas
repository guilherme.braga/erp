unit uVendaProxy;

interface

Type TVendaProxy = class
  var
  {Persistente}
  FId: Integer;
  FDhCadastro: String;
  FVlDesconto: Double;
  FVlAcrescimo: Double;
  FVlTotalProduto: Double;
  FVlVenda: Double;
  FStatus: String;
  FIdPessoa: Integer;
  FIdFilial: Integer;
  FIdOperacao: Integer;
  FIdChaveProcesso: Integer;
  FIdCentroResultado: Integer;
  FIdContaAnalise: Integer;
  FIdPlanoPagamento: Integer;
  FIdFormaPagamento: Integer;
  FPercDesconto: Double;
  FPercAcrescimo: Double;
  FIdTabelaPreco: Integer;
  FTipo: String;
  FDhFechamento: String;
  FVlPagamento: Double;
  FVlPessoaCreditoUtilizado: Double;
  FIdVendedor: Integer;
  FIdNotaFiscalNFE: Integer;
  FIdOperacaoFiscal: Integer;
  FIdNotaFiscalNFSE: Integer;
  FIdNotaFiscalNFCE: Integer;

  {Transiente}
  FIdContaCorrente: Integer;
  FIdFormaPagamentoDinheiro: Integer;
  FIdCarteira: Integer;

  const FIELD_ID = 'VENDA.ID';
  const FIELD_DATA_CADASTRO = 'VENDA.DH_CADASTRO';
  const FIELD_DATA_FECHAMENTO = 'VENDA.DH_FECHAMENTO';
  const FIELD_VALOR_VENDA = 'VENDA.VL_VENDA';
  const FIELD_STATUS = 'VENDA.STATUS';
  const FIELD_ID_PESSOA = 'VENDA.ID_PESSOA';
  const FIELD_TIPO = 'VENDA.TIPO';

  //Lista de Valores do Campo STATUS
  const LISTA_STATUS = 'ABERTO;FECHADO;CANCELADO';

  //Lista de Valores dos Campos TIPO
  const LISTA_TIPO = 'PEDIDO;VENDA;OR�AMENTO;CONDICIONAL;DEVOLU��O';

  const NOME_TABELA_VENDA = 'VENDA';
end;

type TVendaItemProxy = class
  const NOME_TABELA_VENDA_ITEM = 'VENDA_ITEM';
  const FIELD_ID_VENDA = 'VENDA_ITEM.ID_VENDA';
end;

implementation

end.
