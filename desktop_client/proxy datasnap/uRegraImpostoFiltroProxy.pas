unit uRegraImpostoFiltroProxy;

Interface

type TRegraImpostoFiltroProxy = class
  FId: Integer;
  FTipoEmpresaPessoa: String;
  FFormaAquisicao: String;
  FOrigemDaMercadoria: Integer;
  FRegimeTributarioDestinatario: Integer;
  FRegimeTributarioEmitente: Integer;
  FCrtEmitente: Integer;
  FBoContribuinteIcmsEmitente: String;
  FBoContribuinteIpiEmitente: String;
  FBoContribuinteIcmsDestinatario: String;
  FBoContribuinteIpiDestinatario: String;
  FIdZoneamentoEmitente: Integer;
  FIdZoneamentoDestinatario: Integer;
  FIdRegimeEspecialEmitente: Integer;
  FIdRegimeEspecialDestinatario: Integer;
  FIdSituacaoEspecial: Integer;
  FIdOperacaoFiscal: Integer;
  FIdRegraImposto: Integer;
  FIdEstadoOrigem: Integer;
  FIdEstadoDestino: Integer;
  FDhInicioVigencia: String;
  FDhFimVigencia: String;
  FIdImpostoPisCofins: Integer;
  FIdImpostoIcms: Integer;
  FIdImpostoIpi: Integer;
end;

implementation

end.
