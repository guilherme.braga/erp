unit uReceitaOticaProxy;

Interface

type TReceitaOticaProxy = class
  FId: Integer;
  FDhCadastro: String;
  FIdPessoaUsuario: Integer;
  FIdPessoa: Integer;
  FDhPrevisaoEntrega: String;
  FIdMedico: Integer;
  FEspecificacao: String;
  FObservacao: String;
  FIdVenda: Integer;
  FLongeDireitoEsferico: String;
  FLongeDireitoCilindrico: String;
  FLongeDireitoEixo: String;
  FLongeEsquerdoEsferico: String;
  FLongeEsquerdoCilindrico: String;
  FLongeEsquerdoEixo: String;
  FPertoDireitoEsferico: String;
  FPertoDireitoCilindrico: String;
  FPertoDireitoEixo: String;
  FPertoEsquerdoEsferico: String;
  FPertoEsquerdoCilindrico: String;
  FPertoEsquerdoEixo: String;
  FPertoDnpDireito: Double;
  FPertoDnpEsquerdo: Double;
  FPertoDnpGeral: Double;
  FLongeDnpDireito: Double;
  FLongeDnpEsquerdo: Double;
  FLongeDnpGeral: Double;
  FAltura: String;
  FAdicao: String;
  FIdFilial: Integer;

  {JOIN}
  FNomePessoa: String;
  FNomeMedico: String;

  {CONST}
  const FIELD_VENDA = 'RECEITA_OTICA.ID_VENDA';
end;

implementation

end.
