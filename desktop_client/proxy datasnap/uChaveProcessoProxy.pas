unit uChaveProcessoProxy;

interface

type TChaveProcessoProxy = class

    const
      ORIGEM_CONTA_CORRENTE           = 'MOVIMENTO DE CONTA CORRENTE';
      ORIGEM_CONTA_PAGAR              = 'CONTA A PAGAR';
      ORIGEM_CONTA_RECEBER            = 'CONTA A RECEBER';
      ORIGEM_GERACAO_DOCUMENTO        = 'GERACAO DE DOCUMENTO';
      ORIGEM_AJUSTE_ESTOQUE           = 'AJUSTE DE ESTOQUE';
      ORIGEM_NOTA_FISCAL              = 'NOTA FISCAL';
      ORIGEM_VENDA_VAREJO             = 'VENDA VAREJO';
      ORIGEM_CHEQUE                   = 'CHEQUE';
      ORIGEM_ORDEM_SERVICO            = 'ORDEM DE SERVI�O';
      ORIGEM_LOTE_PAGAMENTO           = 'LOTE DE PAGAMENTO';
      ORIGEM_LOTE_RECEBIMENTO         = 'LOTE DE RECEBIMENTO';
      ORIGEM_PRODUTO_GRADE            = 'PRODUTO EM GRADE';
      ORIGEM_INVENTARIO               = 'INVENT�RIO';
      ORIGEM_FORMACAO_PRECO_INICIAL   = 'FORMA��O DE PRE�O INICIAL';  //Aba forma��o de pre�o do cadastro de produto
      ORIGEM_NEGOCIACAO_CONTA_RECEBER = 'NEGOCIA��O DE CONTAS A RECEBER';
      ORIGEM_MOVIMENTACAO_CHEQUE      = 'MOVIMENTA��O DE CHEQUE';
  end;

implementation

end.
