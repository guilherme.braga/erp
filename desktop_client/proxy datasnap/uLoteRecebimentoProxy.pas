unit uLoteRecebimentoProxy;

interface

type TLoteRecebimentoProxy = class

    const
      STATUS_ABERTO    = 'ABERTO';
      STATUS_EFETIVADO = 'EFETIVADO';
      RETORNO_SUCESSO  = 'RETORNO_SUCESSO';

      CONSTANTE_TODOS = 'TODOS';

      FIELD_DATA_EFETIVACAO         = 'LOTE_RECEBIMENTO.DH_EFETIVACAO';
      FIELD_STATUS                  = 'LOTE_RECEBIMENTO.STATUS';
      FIELD_VALOR_LOTE_RECEBIMENTO  = 'LOTE_RECEBIMENTO.VALOR';
      FIELD_DOCUMENTO               = 'LOTE_RECEBIMENTO.DOCUMENTO';

  end;

implementation

end.
