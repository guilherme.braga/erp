unit uOperacaoProxy;

Interface

type TOperacaoProxy = class
  FId: Integer;
  FDescricao: String;
  FNaturezaOperacao: String;
end;

type TOperacaoFiscalProxy = class
  FId: Integer;
  FDescricao: String;
  FIdOperacao: Integer;
  FCfopProduto: Integer;
  FCfopProdutoSt: Integer;
  FCfopMercadoria: Integer;
  FCfopMercadoriaSt: Integer;

  const FIELD_ID_OPERACAO = 'OPERACAO_FISCAL.ID_OPERACAO';
end;

implementation

end.


