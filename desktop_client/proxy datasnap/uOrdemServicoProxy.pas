unit uOrdemServicoProxy;

interface

type TOrdemServicoProxy = class

  Fid: Integer;
  FDhCadastro: String;
  FDhFechamento: String;
  FDhEntrada: String;
  FStatus: String;
  FVlTotalDescontoProduto: Double;
  FVlTotalAcrescimoProduto: Double;
  FVlTotalDescontoServico: Double;
  FVlTotalAcrescimoServico: Double;
  FVlTotalDescontoServicoTerceiro: Double;
  FVlTotalAcrescimoServicoTerceiro: Double;
  FVlTotalDesconto: Double;
  FVlTotalAcrescimo: Double;
  FVlTotalProduto: Double;
  FVlTotalServico: Double;
  FVlTotalServicoTerceiro: Double;
  FVlTotalBruto: Double;
  FVlTotalLiquido: Double;
  FProblemaRelatado: String;
  FProblemaEncontrado: String;
  FSolucaoTecnica: String;
  FObservacao: String;
  FIdChaveProcesso: Integer;
  FIdSituacao: Integer;
  FIdPessoaUsuario: Integer;
  FIdFilial: Integer;
  FIdChecklist: Integer;
  FIdTabelaPreco: Integer;
  FIdCentroResultado: Integer;
  FIdContaAnalise: Integer;
  FIdPlanoPagamento: Integer;
  FIdFormaPagamento: Integer;
  FIdOperacao: Integer;
  FDtCompetencia: String;

  FIdContaCorrente: Integer;
  FIdFormaPagamentoDinheiro: Integer;
  FIdCarteira: Integer;

  const STATUS_ABERTO: String = 'ABERTO';
  const STATUS_FECHADO: String = 'FECHADO';
  const STATUS_CANCELADO: String = 'CANCELADO';

  //Lista de Valores do Campo STATUS
  const LISTA_STATUS = 'ABERTO;FECHADO;CANCELADO';

  const FIELD_ID = 'ORDEM_SERVICO.ID';
  const FIELD_DATA_CADASTRO = 'ORDEM_SERVICO.DH_CADASTRO';
  const FIELD_DATA_FECHAMENTO = 'ORDEM_SERVICO.DH_FECHAMENTO';
  const FIELD_VALOR_LIQUIDO = 'ORDEM_SERVICO.VL_TOTAL_LIQUIDO';
  const FIELD_STATUS = 'ORDEM_SERVICO.STATUS';
  const FIELD_ID_PESSOA = 'ORDEM_SERVICO.ID_PESSOA';

  const TANQUE_COMPLETO: String = 'COMPLETO';
  const TANQUE_UM_QUARTO: String = '1/4';
  const TANQUE_DOIS_QUARTOS: String = '2/4';
  const TANQUE_TRES_QUARTOS: String = '3/4';
  const TANQUE_QUATRO_QUARTOS: String = 'VAZIO';
end;

implementation

end.
