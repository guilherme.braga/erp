unit uCarteiraProxy;

Interface

type TCarteiraProxy = class
  FId: Integer;
  FDescricao: String;
  FPercJuros: Double;
  FPercMulta: Double;
  FBoAtivo: String;
  FCarencia: Integer;
  FObservacao: String;
  FTipoBoleto: String;
  FCbxArquivoLicenca: String;
  FCedenteNome: String;
  FCedenteDocFederal: String;
  FCedenteBanco: String;
  FCedenteCarteira: String;
  FCedenteNomePersonalizado: String;
  FCedenteAgencia: String;
  FCedenteContaCorrente: String;
  FCedenteCodigo: String;
  FBoletoNossoNumeroInicial: Integer;
  FBoletoNossoNumeroFinal: Integer;
  FBoletoNossoNumeroProximo: Integer;
  FBoletoModalidade: Integer;
  FBoletoCodigoTransmissao: Integer;
  FBoletoLocalPagamento: String;
  FBoletoInstrucoes: String;
  FBoletoDemonstrativo: String;
  FBoletoLayoutImpressao: String;
  FBoletoLayoutRemessa: String;
  FBoletoLayoutRetorno: String;
  FBoletoPathRemessa: String;
  FBoletoPathRetorno: String;
  FBoletoEspecieDocumento: String;
  FBoletoSequenciaRemessa: Integer;
  FBoletoAmbiente: String;
  FBoletoDiasProtesto: Integer;
  FBoAceiteBoleto: String;
  FBoletoVlAcrescimo: Double;
  FBoletoVlTarifaBancaria: Double;
  FBoletoPercMulta: Double;
  FBoletoPercMoraDiaria: Double;
  FBoletoPercDesconto: Double;
  FBoletoMoeda: String;
  FBoEnviaBoletoEmail: String;
  FBoletoAssuntoEmail: String;
  FBoletoLayoutBoleto: String;
  FBoletoIdReciboEmailPersonalizado: Integer;
  FBoletoIdReciboBoletoPersonalizado: Integer;
  FBoletoPrefixoNossoNumero: Integer;
  FBoletoTipoDocumento: String;
  FBoletoLayoutEmail: String;
  FIdContaCorrente: Integer;
end;

implementation

end.
