unit uTabelaPrecoProxy;

interface

type TTabelaPrecoProxy = class
  FId: Integer;
  FDescricao: String;
  FIdFilial: Integer;

  const FIELD_ID_TABELA_PRECO = 'TABELA_PRECO.ID';
end;

type TTabelaPrecoItemProxy = class
  FId: Integer;
  FIdProduto: Integer;
  FIdTabelaPreco: Integer;
  FPercLucro: Double;
  FVlVenda: Double;
  FVlCustoImpostoOperacional: Double;
end;

type TTabelaPrecoProdutoProxy = class
  FId: Integer;
  FDescricao: String;
  FCodigoBarra: String;
  FQtdeEstoque: Double;
  FVlCusto: Double;
  FVlVenda: Double;
  FUN: String;
end;

implementation

end.
