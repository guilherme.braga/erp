unit uEquipamentoProxy;

Interface

type TEquipamentoProxy = class
  FId: Integer;
  FDescricao: String;
  FIdMarca: Integer;
  FIdModelo: Integer;
  FSerie: String;
  FIdPessoa: Integer;
  FBoAtivo: String;
  FImei: String;
end;

implementation

end.

