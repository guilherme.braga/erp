unit uConfiguracaoFiscalProxy;

interface

type TConfiguracaoFiscalProxy = class
  FId: Integer;
  FBoAtivo: String;
  FLogomarca: String;
  FCertificadoCaminho: String;
  FCertificadoSenha: String;
  FCertificadoNumeroSerie: String;
  FNfeModoImpressao: String;
  FNfeFormaEmissao: String;
  FNfeWebserviceUf: String;
  FNfeWebserviceAmbiente: String;
  FNfeWebserviceVisualizarMensagem: String;
  FNfeProxyHost: String;
  FNfeProxyPorta: Integer;
  FNfeProxyUsuario: String;
  FNfeProxySenha: String;
  FEmailSmtp: String;
  FEmailPorta: String;
  FEmailUsuario: String;
  FEmailSenha: String;
  FEmailAssunto: String;
  FEmailMensagem: String;
  FNfceModoImpressao: String;
  FNfceWebserviceUf: String;
  FNfceWebserviceAmbiente: String;
  FNfceWebserviceVisualizarMensagem: String;
  FNfceProxyHost: String;
  FNfceProxyPorta: Integer;
  FNfceProxyUsuario: String;
  FNfceProxySenha: String;
  FIdFilial: Integer;
  FNfeIdRelatorioFr: Integer;
  FExpandirLogomarcar: String;
  FNfceIdCsc: String;
  FNfceCsc: String;
  FArquivoPathLogs: String;
  FArquivoPathSchemas: String;
  FArquivoPathNfe: String;
  FArquivoPathCancelamento: String;
  FArquivoPathInutilizacao: String;
  FArquivoPathDpec: String;
  FArquivoPathEvento: String;
  FArquivoRelatorioNfceFr: String;
  FArquivoEventoNfceFr: String;
  FArquivoRelatorioNfeFr: String;
  FArquivoEventoNfeFr: String;
  FImpressoraNfe: String;
  FImpressoraNfce: String;
  FArquivoPathPdf: String;
  FNfcePreview: String;
  FNfePreview: String;
  FSalvarPdfJuntoXml: String;
  FEmailEnviarNfe: String;
  FEmailEnviarNfce: String;
  FEmailEmailHost: String;
  FEmailEmailDestino: String;
  FEmailEmailCc: String;
  FEmailEmailCco: String;
  FEmailSsl: String;
  FEmailSolicitaConfirmacao: String;
  FEmailUsarThread: String;
  FEmailNomeOrigem: String;

  const NFE_WEBSERVICE_AMBIENTE_PRODUCAO = 'PRODUCAO';
  const NFE_WEBSERVICE_AMBIENTE_HOMOLOGACAO = 'HOMOLOGACAO';

  const NFE_MODO_IMPRESSAO_RETRATO = 'RETRATO';
  const NFE_MODO_IMPRESSAO_PAISAGEM = 'PAISAGEM';

  const NFE_FORMA_EMISSAO_NORMAL = 'NORMAL';
  const NFE_FORMA_EMISSAO_DPEC = 'DPEC';
  const NFE_FORMA_EMISSAO_CONTINGENCIA = 'CONTINGENCIA';
  const NFE_FORMA_EMISSAO_FSDA = 'FSDA';
  const NFE_FORMA_EMISSAO_SCAN = 'SCAN';

  const NFCE_WEBSERVICE_AMBIENTE_PRODUCAO = 'PRODUCAO';
  const NFCE_WEBSERVICE_AMBIENTE_HOMOLOGACAO = 'HOMOLOGACAO';

  const NFCE_MODO_IMPRESSAO_RETRATO = 'RETRATO';
  const NFCE_MODO_IMPRESSAO_PAISAGEM = 'PAISAGEM';

  const NFCE_FORMA_EMISSAO_NORMAL = 'NORMAL';
end;

implementation

end.
