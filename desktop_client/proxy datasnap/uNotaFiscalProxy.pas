unit uNotaFiscalProxy;

Interface

Uses Generics.Collections, SysUtils;

type TNotaFiscalParcelaProxy = class
  FId: String;
  FDocumento: String;
  FVlTitulo: Double;
  FQtParcela: Integer;
  FNrParcela: Integer;
  FDtVencimento: String;
  FIdNotaFiscal: Integer;
  FObservacao: String;
  FChequeSacado: String;
  FChequeDocFederal: String;
  FChequeBanco: String;
  FChequeDtEmissao: String;
  FChequeDtVencimento: String;
  FChequeAgencia: String;
  FChequeContaCorrente: String;
  FChequeNumero: Integer;
  FIdOperadoraCartao: Integer;
  FIdFormaPagamento: Integer;
  FIdCarteira: Integer;
end;

type TNotaFiscalItemProxy = class
  FId: Integer;
  FHNitem: Integer;
  FIQcom: Double;
  FIVuncom: Double;
  FIVdesc: Double;
  FVlLiquido: Double;
  FIVfrete: Double;
  FIVseg: Double;
  FIVoutro: Double;
  FNNvicmsst: Double;
  FOVipi: Double;
  FVlCustoImposto: Double;
  FIdProduto: Integer;
  FIdNotaFiscal: Integer;
  FPercDesconto: Double;
  FPercAcrescimo: Double;
  FPercFrete: Double;
  FPercSeguro: Double;
  FPercIcmsSt: Double;
  FPercIpi: Double;
  FVlCusto: Double;
  FVlCustoOperacional: Double;
  FPercCustoOperacional: Double;
  FIXprod: String;
  FNVicms: Double;
  FNVbcst: Double;
  FNPredbcst: Double;
  FNMotdesicms: Integer;
  FNPmvast: Double;
  FNModbcst: Integer;
  FINcm: Integer;
  FIExtipi: String;
  FICfop: Integer;
  FICeantrib: String;
  FIUtrib: String;
  FIQtrib: Double;
  FIVuntrib: Double;
  FIIndtot: Integer;
  FINve: String;
  FICean: String;
  FNOrig: Integer;
  FNCst: Integer;
  FNModbc: Integer;
  FNPredbc: Double;
  FNVbc: Double;
  FNPicms: Double;
  FNVicmsop: Double;
  FNUfst: String;
  FNPdif: Double;
  FNVlicmsdif: Double;
  FNVicmsdeson: Double;
  FNPbcop: Double;
  FNBcstret: Double;
  FNPcredsn: Double;
  FNVbcstdest: Double;
  FNVicmsstdest: Double;
  FNVcredicmssn: Double;
  FOClenq: String;
  FOCnpjprod: String;
  FOCselo: String;
  FOQselo: Double;
  FOCenq: String;
  FOCst: Integer;
  FOVbc: Double;
  FOQunid: Double;
  FOVunid: Double;
  FOPipi: Double;
  FQCst: Integer;
  FQVbc: Double;
  FQPpis: Double;
  FQVpis: Double;
  FQQbcprod: Double;
  FQValiqprod: Double;
  FSCst: Integer;
  FSVbc: Double;
  FSPcofins: Double;
  FSQbcprod: Double;
  FSValiqprod: Double;
  FSVcofins: Double;
end;

type TNotaFiscalProxy = class
  FId: Integer;
  FDhCadastro: String;
  FBNnf: Integer;
  FBSerie: Integer;
  FIdModeloNota: Integer;
  FBDemi: String;
  FBDsaient: String;
  FBHsaient: String;
  FWVprod: Double;
  FWVst: Double;
  FWVseg: Double;
  FWVdesc: Double;
  FWVoutro: Double;
  FWVipi: Double;
  FWVfrete: Double;
  FWVnf: Double;
  FXXnome: String;
  FVlFreteTransportadora: Double;
  FDtVencimento: String;
  FIdCentroResultado: Integer;
  FIdContaAnalise: Integer;
  FIdChaveProcesso: Integer;
  FIdOperacao: Integer;
  FIdFilial: Integer;
  FIdPessoa: Integer;
  FPercVst: Double;
  FPercVseg: Double;
  FPercVdesc: Double;
  FPercVoutro: Double;
  FPercVipi: Double;
  FPercVfrete: Double;
  FStatus: String;
  FIdPlanoPagamento: Integer;
  FIdFormaPagamento: Integer;
  FDtCompetencia: String;
  FIdPessoaUsuario: Integer;
  FTipoNf: String;
  FNfeChave: String;
  FArVersao: String;
  FArTpAmb: String;
  FArVeraplic: String;
  FArCstat: Integer;
  FArXmotivo: String;
  FArCuf: String;
  FArDhrecbto: String;
  FArInfrec: String;
  FArNrec: String;
  FArTmed: Integer;
  FArProtnfe: String;
  FIdOperacaoFiscal: Integer;
  FFormaPagamento: String;

  FNotaFiscalItem: TList<TNotaFiscalItemProxy>;
  FNotaFiscalParcela: TList<TNotaFiscalParcelaProxy>;

  //TIPO_NF
  const TIPO_NF_ENTRADA = 'ENTRADA';
  const TIPO_NF_SAIDA = 'SAIDA';

  //X_MODFRETE
  const MODALIDADE_FRETE_EMITENTE = 0;
  const MODALIDADE_FRETE_DESTINATARIO_REMETENTE = 1;
  const MODALIDADE_FRETE_TERCEIROS = 2;
  const MODALIDADE_FRETE_SEM_FRETE = 9;

  //FORMA_PAGAMENTO
  const FORMA_PAGAMENTO_VISTA = 'VISTA';
  const FORMA_PAGAMENTO_PRAZO = 'PRAZO';
  const FORMA_PAGAMENTO_OUTROS = 'OUTROS';

  const TIPO_DOCUMENTO_FISCAL_NFE = 'NFE';
  const TIPO_DOCUMENTO_FISCAL_NFCE = 'NFCE';

  const MODELO_NFE = '55';
  const MODELO_NFCE = '65';

  const STATUS_NFE_ENVIADA = 'ENVIADA';
  const STATUS_NFE_DENEGADA = 'DENEGADA';
  const STATUS_NFE_AENVIAR = 'A ENVIAR';
  const STATUS_NFE_CANCELADA = 'CANCELADA';
  const STATUS_NFE_INUTILIZADA = 'INUTILIZADA';

  //Lista de Valores do Campo STATUS NFE
  const LISTA_STATUS_NFE = 'ENVIADA;DENEGADA;A ENVIAR;CANCELADA;INUTILIZADA';

  //Lista de Valores do Campo STATUS NFE
  const LISTA_STATUS = 'EFETIVADO;CANCELADO;ABERTA';

  //Lista de Valores do Campo STATUS NFE
  const LISTA_TIPO_NF = 'ENTRADA;SAIDA';

  constructor Create;
  destructor Destroy;
end;

type TNotaFiscalTransienteProxy = class
  FIdContaCorrente: Integer;
  FIdFormaPagamentoDinheiro: Integer;
  FIdCarteira: Integer;
end;

type TRetornoDocumentoFiscalProxy = class
  FRetorno: Boolean;
  FMensagensCommaText: String;
end;

implementation

{ TNotaFiscalProxy }

constructor TNotaFiscalProxy.Create;
begin
  FNotaFiscalItem := TList<TNotaFiscalItemProxy>.Create;
  FNotaFiscalParcela := TList<TNotaFiscalParcelaProxy>.Create;
end;

destructor TNotaFiscalProxy.Destroy;
begin
  FreeAndNil(FNotaFiscalItem);
  FreeAndNil(FNotaFiscalParcela);
end;

end.

