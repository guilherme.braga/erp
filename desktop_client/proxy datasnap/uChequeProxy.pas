unit uChequeProxy;

interface

type TChequeProxy = class
  FId: Integer;
  FSacado: String;
  FDocFederal: String;
  FVlCheque: Double;
  FNumero: Integer;
  FBanco: String;
  FAgencia: String;
  FContaCorrente: String;
  FDtEmissao: String;
  FDtVencimento: String;
  FIdChaveProcesso: Integer;
  FIdContaCorrente: Integer;
  FBoConciliado: String;
  FDhConciliado: String;
  FIdDocumentoOrigem: Integer;
  FIdContaCorrenteMovimento: Integer;
  FDocumentoOrigem: String;
  FTipo: String;
  FBoChequeUtilizado: String;
  FStatus: String;

  const TIPO_CHEQUE_TERCEIRO = 'TERCEIRO';
  const TIPO_CHEQUE_PROPRIO = 'PROPRIO';
  const LISTA_VALORES_TIPO = 'TERCEIRO;PROPRIO';

  const STATUS_DISPONIVEL = 'DISPONIVEL';
  const STATUS_CANCELADO = 'CANCELADO';

  const FIELD_ID = 'CHEQUE.ID';

  constructor Create;
end;

type TChequeHistoricoProxy = class
  private

  public
    FId: Integer;
    FDhOcorrencia: String;
    FTipo: String;
    FOcorrencia: String;
    FIdPessoaUsuario: Integer;
    FIdFilial: Integer;
    FIdChaveProcesso: Integer;
    FDocumentoOrigem: String;
    FIdDocumentoOrigem: Integer;
    FSequencia: Integer;
    FIdCheque: Integer;

    const TIPO_LANCAMENTO_CHEQUE_PROPRIO = 'INCLUS�O DE CHEQUE PR�PRIO';
    const TIPO_ESTORNO_CHEQUE_PROPRIO = 'ESTORNO DE CHEQUE PR�PRIO';
    const TIPO_TRANSFERENCIA_CHEQUE_TERCEIRO = 'TRANSFER�NCIA DE CHEQUE DE TERCEIRO';
    const TIPO_DEBITO_CHEQUE_TERCEIRO = 'D�BITADO DE CHEQUE DE TERCEIRO';
    const TIPO_CREDITO_CHEQUE_TERCEIRO = 'CR�DITADO DE CHEQUE DE TERCEIRO';
    const TIPO_UTILIZADO_CHEQUE_TERCEIRO_PARA_PAGAMENTO = 'UTILIZA��O DE CHEQUE DE TERCEIRO PARA PAGAMENTO';
    const TIPO_ESTORNO_UTILIZACAO_CHEQUE_TERCEIRO_EM_PAGAMENTO = 'ESTORNO DE PAGAMENTO QUE UTILIZOU DE CHEQUE DE TERCEIRO';

    const OCORRENCIA_TIPO_LANCAMENTO_CHEQUE_PROPRIO =
    'CHEQUE PR�PRIO (%n) LAN�ADO PARA PAGAMENTO DO T�TULO DE CONTA A PAGAR'+
    ' (%n) REGISTRADO NA CONTA CORRENTE %n - %s (Movimenta��o %n).';

    const OCORRENCIA_TIPO_ESTORNO_CHEQUE_PROPRIO =
      'CHEQUE PR�PRIO (%n) ESTORNADO DO PAGAMENTO DO T�TULO DE CONTA A PAGAR'+
      ' (%n) REGISTRADO NA CONTA CORRENTE %n - %s (Movimenta��o %n).';

    const OCORRENCIA_TIPO_TRANSFERENCIA_CHEQUE_TERCEIRO =
      'CHEQUE DE TERCEIRO (%n) TR�NSFERIDO DA CONTA CORRENTE %n - %s (Movimenta��o %n)'+
      ' PARA A CONTA CORRENTE %n - %s (Movimenta��o %n).';

    const OCORRENCIA_TIPO_DEBITO_CHEQUE_TERCEIRO =
      'CHEQUE DE TERCEIRO (%n) DEBITADO DA CONTA CORRENTE %n - %s (Movimenta��o %n)'+
      ' GERANDO A MOVIMENTA��O DE ESTORNO %n.';

    const OCORRENCIA_TIPO_CREDITO_CHEQUE_TERCEIRO =
      'CHEQUE DE TERCEIRO (%n) CREDITADO NA CONTA CORRENTE %n - %s (Movimenta��o %n)';

    const OCORRENCIA_TIPO_UTILIZADO_CHEQUE_TERCEIRO_PARA_PAGAMENTO =
      'CHEQUE DE TERCEIRO (%n) DA CONTA CORRENTE %n - %s (Movimenta��o %n)'+
      ' UTILIZADO PARA PAGAMENTO DO T�TULO DE CONTA A PAGAR (%n), GERANDO A MOVIMENTA��O DE ESTORNO %n.';

    const OCORRENCIA_TIPO_ESTORNO_UTILIZACAO_CHEQUE_TERCEIRO_EM_PAGAMENTO =
      'CHEQUE DE TERCEIRO (%n) CREDITADO NA CONTA CORRENTE %n - %s (Movimenta��o %n)'+
      ' DEVIDO AO ESTORNO DO PAGAMENTO DO T�TULO DE CONTA A PAGAR (%n).';

end;

implementation

{ TChequeProxy }

constructor TChequeProxy.Create;
begin
  FBoConciliado := 'N';
  FIdContaCorrenteMovimento := 0;
  FBoChequeUtilizado := 'N';
  FTipo := TIPO_CHEQUE_TERCEIRO;
  FStatus := STATUS_DISPONIVEL;
end;

end.
