unit uImpostoPisCofinsProxy;

Interface

type TImpostoPisCofinsProxy = class
  FId: String;
  FPercAliquotaPis: Double;
  FPercAliquotaCofins: Double;
  FPercAliquotaPisSt: Double;
  FPercAliquotaCofinsSt: Double;
  FIdCstPisCofins: Integer;
end;

type TEntradaCalculoImpostoPisProxy = class
  FImpostoPis: TImpostoPisCofinsProxy;
  FCrtEmitente: Integer;
  FCST: String;
  FQuantidadeItem: Double;
  FValorUnitario: Double;
  FValorSeguro: Double;
  FValorOutrasDespesas: Double;
  FValorDesconto: Double;
  FTipoTributacaoPIS: Integer;
end;

type TSaidaCalculoImpostoPisProxy = class
  FValorBaseOperacaoCalculado: Double;
  FValorBaseCalculoPisCalculado: Double;
  FValorReducaoBaseCalculoPisCalculado: Double;
  FPercentualBaseCalculoPisCalculado: Double;
  FValorPisCalculado: Double;
  FPercentualPisCalculado: Double;
  FValorCreditoPisSimplesNacionalCalculado: Double;
end;

type TEntradaCalculoImpostoCofinsProxy = class
  FImpostoCofins: TImpostoPisCofinsProxy;
  FCrtEmitente: Integer;
  FCST: String;
  FQuantidadeItem: Double;
  FValorUnitario: Double;
  FValorSeguro: Double;
  FValorOutrasDespesas: Double;
  FValorDesconto: Double;
  FTipoTributacaoCOFINS: Integer;
end;

type TSaidaCalculoImpostoCofinsProxy = class
  FValorBaseOperacaoCalculado: Double;
  FValorBaseCalculoCofinsCalculado: Double;
  FValorReducaoBaseCalculoCofinsCalculado: Double;
  FPercentualBaseCalculoCofinsCalculado: Double;
  FValorCofinsCalculado: Double;
  FPercentualCofinsCalculado: Double;
  FValorCreditoCofinsSimplesNacionalCalculado: Double;
end;

implementation

end.


