unit uIBPTProxy;

Interface

type TIbptProxy = class
  FId: Integer;
  FNcm: String;
  FExcesaoNcm: String;
  FTabela: Integer;
  FAliquotaNacional: Double;
  FAliquotaInternacional: Double;
end;

type TIbptVersaoProxy = class
  FId: Integer;
  FVersao: String;
  FDhInicioAtualizacao: String;
  FDhTerminoAtualizacao: String;
end;

implementation

end.
