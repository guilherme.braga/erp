unit uRetornoProxy;

interface

Uses Generics.Collections, SysUtils, Classes;

type TRetornoProxy = class
  FRetorno: Boolean;
  FListaMensagensCommaText: String;

  constructor Create;
  destructor Destroy;
end;

implementation

{ TRetornoProxy }

constructor TRetornoProxy.Create;
begin

end;

destructor TRetornoProxy.Destroy;
begin

end;

end.
