//
// Created by the DataSnap proxy generator.
// 21/02/2016 16:46:25
// 

unit uClientClasses;

interface

uses System.JSON, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, uPessoaProxy, uProdutoProxy, uFilialProxy, uNotaFiscalProxy, uVendaProxy, uNaturezaOperacaoProxy, uTabelaPrecoProxy, System.Generics.Collections, uServicoProxy, uEquipamentoProxy, uCorProxy, uFluxoCaixaResumidoProxy, uMontadoraProxy, uTamanhoProxy, uBackupProxy, Data.DBXJSONReflect;

type
  TSMLocalidadeClient = class(TDSAdminClient)
  private
    FGetLogradouroPeloCEPCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetLogradouroPeloCEP(AIdCEP: string): string;
  end;

  TServerMethodsClient = class(TDSAdminClient)
  private
    FReverseStringCommand: TDBXCommand;
    FGetDadosCommand: TDBXCommand;
    FGetDadosComInjecaoCamposCommand: TDBXCommand;
    FGetDadosFromSQLCommand: TDBXCommand;
    FCommitCommand: TDBXCommand;
    FRollBackCommand: TDBXCommand;
    FStartTransactionCommand: TDBXCommand;
    FGetLastedAutoIncrementValueCommand: TDBXCommand;
    FGetSQLPesquisaPadraoCommand: TDBXCommand;
    FSetSQLPesquisaPadraoCommand: TDBXCommand;
    FGetUltimaChaveDaTabelaCommand: TDBXCommand;
    FHabilitarConsultaAutomaticaCommand: TDBXCommand;
    FEstaConsultandoAutomaticoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function ReverseString(Value: string): string;
    function GetDados(AIdentifier: string; AWhereBundle: string; AIdUsuario: Integer): TFDJSONDataSets;
    function GetDadosComInjecaoCampos(AIdentifier: string; AWhereBundle: string; AIdUsuario: Integer; ACamposInjetados: string): TFDJSONDataSets;
    function GetDadosFromSQL(ASQL: string): TFDJSONDataSets;
    procedure Commit;
    procedure RollBack;
    procedure StartTransaction;
    function GetLastedAutoIncrementValue: Integer;
    function GetSQLPesquisaPadrao(AIdentifier: string; AIdUsuario: Integer): string;
    procedure SetSQLPesquisaPadrao(AIdentifier: string; AIdUsuario: Integer; ASQL: string);
    function GetUltimaChaveDaTabela(ATabela: string; ACampoRetorno: string): string;
    procedure HabilitarConsultaAutomatica(ACodigoUsuario: Integer; AFormName: string; AHabilitar: Boolean);
    function EstaConsultandoAutomatico(ACodigoUsuario: Integer; AFormName: string): Boolean;
  end;

  TSMPessoaClient = class(TDSAdminClient)
  private
    FGetNomePessoaCommand: TDBXCommand;
    FGetPessoaEnderecoCommand: TDBXCommand;
    FGetUsuarioCommand: TDBXCommand;
    FGetPessoaCommand: TDBXCommand;
    FGerarPessoaCommand: TDBXCommand;
    FGetIdUsuarioCommand: TDBXCommand;
    FSetClienteConsumidorFinalCommand: TDBXCommand;
    FVerificarConsumidorFinalCommand: TDBXCommand;
    FValidaCnpjCeiCpfCommand: TDBXCommand;
    FPossuiCPFValidoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetNomePessoa(AIDPessoa: Integer): string;
    function GetPessoaEndereco(AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
    function GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
    function GetPessoa(AIdPessoa: Integer): string;
    function GerarPessoa(APessoa: string): Integer;
    function GetIdUsuario(AUsuario: string): Integer;
    procedure SetClienteConsumidorFinal(AIdPessoa: Integer);
    function VerificarConsumidorFinal(AIdPessoa: Integer): Boolean;
    function ValidaCnpjCeiCpf(Numero: string): Boolean;
    function PossuiCPFValido(AIdPessoa: Integer): Boolean;
  end;

  TSMFinanceiroClient = class(TDSAdminClient)
  private
    FfdqContaCorrenteMovimentoAfterPostCommand: TDBXCommand;
    FGetContaCorrenteCommand: TDBXCommand;
    FAtualizarSaldoMovimentacaoCommand: TDBXCommand;
    FRealizarTransferenciaCommand: TDBXCommand;
    FConciliarRegistroCommand: TDBXCommand;
    FDesconciliarRegistroCommand: TDBXCommand;
    FRegistroEstaConciliadoCommand: TDBXCommand;
    FGerarMovimentoContaCorrenteCommand: TDBXCommand;
    FRemoverMovimentoContaCorrenteCommand: TDBXCommand;
    FGerarDocumentoCommand: TDBXCommand;
    FGetSaldoAnteriorContaCorrenteCommand: TDBXCommand;
    FGetIDNivelCommand: TDBXCommand;
    FGetPlanoContaCompletoCommand: TDBXCommand;
    FGetPlanoContaPorCentroResultadoCommand: TDBXCommand;
    FSetIDPlanoContaAssociadoCommand: TDBXCommand;
    FGetDescricaoCentroResultadoCommand: TDBXCommand;
    FGetDescricaoContaAnaliseCommand: TDBXCommand;
    FGetCentroResultadosCommand: TDBXCommand;
    FGetPrimeiroNivelCommand: TDBXCommand;
    FGetNivelAninhadoCommand: TDBXCommand;
    FGetMovimentacaoCommand: TDBXCommand;
    FGetContasCorrentesCommand: TDBXCommand;
    FGetTotalPorCentroResultadoCommand: TDBXCommand;
    FGetTotalPorNivelCommand: TDBXCommand;
    FGetTotalPorContaCorrenteCommand: TDBXCommand;
    FBuscarValorPrevisaoCommand: TDBXCommand;
    FSalvarPrevisaoCommand: TDBXCommand;
    FGetPlanoPagamentoCommand: TDBXCommand;
    FGetDescricaoPlanoPagamentoCommand: TDBXCommand;
    FGetDescricaoFormaPagamentoCommand: TDBXCommand;
    FGetFormaPagamentoCommand: TDBXCommand;
    FGetCodigosFormaPagamentoCartaoCommand: TDBXCommand;
    FFormaPagamentoCartaoCommand: TDBXCommand;
    FFormaPagamentoCartaoDebitoCommand: TDBXCommand;
    FFormaPagamentoCartaoCreditoCommand: TDBXCommand;
    FFormaPagamentoDinheiroCommand: TDBXCommand;
    FFormaPagamentoChequeCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure fdqContaCorrenteMovimentoAfterPost(DataSet: TDataSet);
    function GetContaCorrente(AAtivo: Boolean): TFDJSONDataSets;
    procedure AtualizarSaldoMovimentacao(AIdContaCorrente: Integer);
    function RealizarTransferencia(AIdMovimentoOrigem: Integer; AIdContaCorrenteDestino: Integer): Integer;
    procedure ConciliarRegistro(AIdMovimentacao: Integer);
    procedure DesconciliarRegistro(AIdMovimentacao: Integer);
    function RegistroEstaConciliado(AIdMovimentacao: Integer): Boolean;
    function GerarMovimentoContaCorrente(AValue: string): Boolean;
    function RemoverMovimentoContaCorrente(AValue: Integer): Boolean;
    function GerarDocumento(AValue: string): Boolean;
    function GetSaldoAnteriorContaCorrente(AIdContaCorrente: Integer; ADtMovimento: string): Double;
    function GetIDNivel(ANivel: Integer; ASequencia: string): Integer;
    function GetPlanoContaCompleto: TFDJSONDataSets;
    function GetPlanoContaPorCentroResultado(AIdCentroResultado: Integer): TFDJSONDataSets;
    procedure SetIDPlanoContaAssociado;
    function GetDescricaoCentroResultado(AID: Integer): string;
    function GetDescricaoContaAnalise(AID: Integer): string;
    function GetCentroResultados: TFDJSONDataSets;
    function GetPrimeiroNivel(ANivel: Integer; ACentrosResultados: string; AListaCamposPeriodo: string): TFDJSONDataSets;
    function GetNivelAninhado(ANivel: Integer; ACentrosResultados: string; APlanosContas: string; AListaCamposPeriodo: string): TFDJSONDataSets;
    function GetMovimentacao(ACentrosResultados: string; APlanosContas: string; AContasCorrentes: string; AListaCamposPeriodo: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
    function GetContasCorrentes(ACentrosResultados: string; APlanosContas: string; AListaCamposPeriodo: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
    function GetTotalPorCentroResultado(ACentrosResultados: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
    function GetTotalPorNivel(ANivel: Integer; ACentrosResultados: string; AContasAnalises: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
    function GetTotalPorContaCorrente(ACentrosResultados: string; AContasAnalises: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
    function BuscarValorPrevisao(ANivel: Integer; APeriodoPrevisaoInicial: string; APeriodoPrevisaoFinal: string; AIdCentroResultado: Integer; AIdContaAnalise: Integer): Double;
    procedure SalvarPrevisao(AIdCentroResultado: Integer; AIdContaAnalise: Integer; APeriodo: string; AValor: Double);
    function GetPlanoPagamento(AIdPlanoPagamento: Integer): TFDJSONDataSets;
    function GetDescricaoPlanoPagamento(AID: Integer): string;
    function GetDescricaoFormaPagamento(AID: Integer): string;
    function GetFormaPagamento(AID: Integer): string;
    function GetCodigosFormaPagamentoCartao: string;
    function FormaPagamentoCartao(AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCartaoDebito(AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCartaoCredito(AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoDinheiro(AIdFormaPagamento: Integer): Boolean;
    function FormaPagamentoCheque(AIdFormaPagamento: Integer): Boolean;
  end;

  TSMIndoorClient = class(TDSAdminClient)
  private
    FRestTestCommand: TDBXCommand;
    FObterListaArquivosCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function RestTest(AValue: string): string;
    function ObterListaArquivos(AValue: string): string;
  end;

  TSMRestauranteClient = class(TDSAdminClient)
  private
    FFecharCaixaCommand: TDBXCommand;
    FGetCaixaCommand: TDBXCommand;
    FcheckUsuarioCommand: TDBXCommand;
    FimprimirCaixaCommand: TDBXCommand;
    FSetTrocoComandaCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure FecharCaixa(AIdCaixaMovimento: Integer);
    function GetCaixa: TFDJSONDataSets;
    function checkUsuario(AUsername: string; APassword: string): Integer;
    procedure imprimirCaixa(id_caixa_movimento: Integer);
    procedure SetTrocoComanda(vr_troco: Double; id_comanda: Integer);
  end;

  TSMProdutoClient = class(TDSAdminClient)
  private
    FfdqProdutoAfterPostCommand: TDBXCommand;
    FGerarMovimentacaoProdutoCommand: TDBXCommand;
    FGetChaveProcessoMovimentacaoInicialCommand: TDBXCommand;
    FRemoverMovimentacaoProdutoCommand: TDBXCommand;
    FGetProximoIDCommand: TDBXCommand;
    FCodigoDeBarraJaCadastradoCommand: TDBXCommand;
    FGetProdutoCommand: TDBXCommand;
    FGetTodosGruposProdutoCommand: TDBXCommand;
    FGetProdutoPeloCodigoBarraCommand: TDBXCommand;
    FGetProdutosCommand: TDBXCommand;
    FGetProdutosCompletoCommand: TDBXCommand;
    FGetProdutoFilialCommand: TDBXCommand;
    FGetProdutoFilialProxyCommand: TDBXCommand;
    FGetListaProdutoCommand: TDBXCommand;
    FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand: TDBXCommand;
    FDuplicarProdutoCommand: TDBXCommand;
    FGerarCodigoBarraEAN13Command: TDBXCommand;
    FGerarProdutosCommand: TDBXCommand;
    FVisaoProdutoGradeCommand: TDBXCommand;
    FInserirProdutoParaEmissaoEtiquetaCommand: TDBXCommand;
    FLimparRegistrosDaEmissaoEtiquetaCommand: TDBXCommand;
    FGetProdutoFiscalCommand: TDBXCommand;
    FVerificarTipoMercadoriaCommand: TDBXCommand;
    FVerificarTipoProdutoCommand: TDBXCommand;
    FRefazerEAN13TodosProdutosCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure fdqProdutoAfterPost(DataSet: TDataSet);
    procedure GerarMovimentacaoProduto(AValue: string);
    function GetChaveProcessoMovimentacaoInicial(AIdProduto: Integer): Integer;
    function RemoverMovimentacaoProduto(AValue: Integer): Boolean;
    function GetProximoID: Integer;
    function CodigoDeBarraJaCadastrado(ACodigoBarra: string; AIdProduto: Integer): Integer;
    function GetProduto(AIdProduto: Integer): TFDJSONDataSets;
    function GetTodosGruposProduto: TFDJSONDataSets;
    function GetProdutoPeloCodigoBarra(ACodigoBarraProduto: string): TFDJSONDataSets;
    function GetProdutos(AIdsProdutos: string): TFDJSONDataSets;
    function GetProdutosCompleto(AIdsProdutos: string): TFDJSONDataSets;
    function GetProdutoFilial(AIdProduto: Integer; AIdFilial: Integer): string;
    function GetProdutoFilialProxy(AIdProduto: Integer; AIdFilial: Integer): TProdutoProxy;
    function GetListaProduto(AIdsProdutos: string; AIdFilial: Integer): string;
    function ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto: Integer; AIdFilial: Integer): Boolean;
    function DuplicarProduto(AIdProdutoOrigem: Integer): Integer;
    function GerarCodigoBarraEAN13(ACodigoProduto: Integer): string;
    function GerarProdutos(AProdutosEmGrade: string): Boolean;
    function VisaoProdutoGrade(AIdGradeProduto: Integer; AIdProdutoAgrupador: Integer): TFDJSONDataSets;
    procedure InserirProdutoParaEmissaoEtiqueta(AIdProduto: Integer);
    procedure LimparRegistrosDaEmissaoEtiqueta;
    function GetProdutoFiscal(AIdProduto: Integer; AIdPessoa: Integer; AFilial: Integer): TProdutoFiscalProxy;
    function VerificarTipoMercadoria(AIdProduto: Integer): Boolean;
    function VerificarTipoProduto(AIdProduto: Integer): Boolean;
    procedure RefazerEAN13TodosProdutos;
  end;

  TSMChaveProcessoClient = class(TDSAdminClient)
  private
    FNovaChaveProcessoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function NovaChaveProcesso(AOrigem: string; AIdOrigem: Integer): Integer;
  end;

  TSMEmpresaFilialClient = class(TDSAdminClient)
  private
    FGetFantasiaCommand: TDBXCommand;
    FOptanteSimplesNacionalCommand: TDBXCommand;
    FGetCRTCommand: TDBXCommand;
    FGetFilialCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetFantasia(AIdFilial: Integer): string;
    function OptanteSimplesNacional(AIdFilial: Integer): Boolean;
    function GetCRT(AIdFilial: Integer): Integer;
    function GetFilial(AIdFilial: Integer): TFilialProxy;
  end;

  TSMNotaFiscalClient = class(TDSAdminClient)
  private
    FGerarContaReceberCommand: TDBXCommand;
    FGerarContaPagarCommand: TDBXCommand;
    FEstornarContaReceberCommand: TDBXCommand;
    FEstornarContaPagarCommand: TDBXCommand;
    FAtualizarStatusCommand: TDBXCommand;
    FPodeExcluirCommand: TDBXCommand;
    FPodeEstornarCommand: TDBXCommand;
    FEfetivarCommand: TDBXCommand;
    FCancelarCommand: TDBXCommand;
    FGerarNotaFiscalCommand: TDBXCommand;
    FEmitirNFECommand: TDBXCommand;
    FNFEEmitidaCommand: TDBXCommand;
    FGerarNotaFiscalDaVendaCommand: TDBXCommand;
    FGerarNotaFiscalDeVendaAgrupadaCommand: TDBXCommand;
    FGerarNotaFiscalDaOrdemServicoCommand: TDBXCommand;
    FAtualizarCPFNaNotaCommand: TDBXCommand;
    FImprimirDocumentoFiscalCommand: TDBXCommand;
    FChecarServidorAtivoCommand: TDBXCommand;
    FGetModeloNotaFiscalCommand: TDBXCommand;
    FGetNumeroDocumentoNotaFiscalSaidaCommand: TDBXCommand;
    FGetSerieNotaFiscalSaidaCommand: TDBXCommand;
    FCancelarDocumentoFiscalCommand: TDBXCommand;
    FInutilizarDocumentoFiscalCommand: TDBXCommand;
    FBuscarXMLCommand: TDBXCommand;
    FBuscarNotasFiscaisParaInutilizacaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GerarContaReceber(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
    function GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
    function EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
    function EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
    procedure AtualizarStatus(AIdChaveProcesso: Integer; AStatus: string);
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function Efetivar(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GerarNotaFiscal(ANotaFiscal: string): Integer;
    function EmitirNFE(AIdNotaFiscal: Integer; ATipoDocumentoFiscal: string): string;
    function NFEEmitida(AIdNotaFiscal: Integer): Boolean;
    function GerarNotaFiscalDaVenda(AId: Integer; ACPFNaNota: string; ATipoDocumentoFiscal: string; AVendaProxy: TVendaProxy): Boolean;
    function GerarNotaFiscalDeVendaAgrupada(AIdsVendas: string; ACPFNaNota: string; ATipoDocumentoFiscal: string; AVendaProxy: TVendaProxy): Boolean;
    function GerarNotaFiscalDaOrdemServico(AIdChaveProcesso: Integer; ACPFNaNota: string; ATipoDocumentoFiscal: string): Boolean;
    procedure AtualizarCPFNaNota(AIdNotaFiscal: Integer; ACPF: string);
    function ImprimirDocumentoFiscal(AIdNotaFiscal: Integer; ATipoDocumentoFiscal: string): string;
    function ChecarServidorAtivo(AIdFilial: Integer; AIdUsuario: Integer): string;
    function GetModeloNotaFiscal(AIdModeloNota: Integer): string;
    function GetNumeroDocumentoNotaFiscalSaida(ATipoDocumentoFiscal: string): Integer;
    function GetSerieNotaFiscalSaida(ATipoDocumentoFiscal: string): Integer;
    function CancelarDocumentoFiscal(AIdNotaFiscal: Integer; AMotivoCancelamento: string; ATipoDocumentoFiscal: string): Boolean;
    procedure InutilizarDocumentoFiscal(AIdsNotaFiscal: string);
    function BuscarXML(AIdNotaFiscal: Integer): string;
    function BuscarNotasFiscaisParaInutilizacao(AIdFIlial: Integer; AMes: Integer; AAno: Integer): TFDJSONDataSets;
  end;

  TSMOperacaoClient = class(TDSAdminClient)
  private
    FGetNaturezaOperacaoCommand: TDBXCommand;
    FGerarNaturezaOperacaoCommand: TDBXCommand;
    FExisteNaturezaOperacaoCommand: TDBXCommand;
    FGetIdNaturezaOperacaoPorDescricaoCommand: TDBXCommand;
    FGetDescricaoNaturezaOperacaoCommand: TDBXCommand;
    FExisteAcaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetNaturezaOperacao(AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
    function GerarNaturezaOperacao(ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
    function ExisteNaturezaOperacao(ADescricaoNaturezaOperacao: string): Boolean;
    function GetIdNaturezaOperacaoPorDescricao(ADescricaoNaturezaOperacao: string): Integer;
    function GetDescricaoNaturezaOperacao(AIdNaturezaOperacao: Integer): string;
    function ExisteAcao(AAcao: string; AIdOperacao: Integer): Boolean;
  end;

  TSMTabelaPrecoClient = class(TDSAdminClient)
  private
    FAtualizarRegistroCommand: TDBXCommand;
    FGetTabelaPrecoComMaisProdutosCommand: TDBXCommand;
    FGetProdutosTabelaPrecoCommand: TDBXCommand;
    FBuscarProdutoEmSuasTabelasPrecoCommand: TDBXCommand;
    FGetDescricaoTabelaPrecoCommand: TDBXCommand;
    FGetProdutoTabelaPrecoCommand: TDBXCommand;
    FGetTabelaPrecoItemCommand: TDBXCommand;
    FExcluirTabelaDePrecoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure AtualizarRegistro(AIdTabelaPreco: Integer; AIdProduto: Integer; AVlCusto: Double; APercLucro: Double; AVlVenda: Double);
    function GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
    function GetProdutosTabelaPreco(AIdTabelaPreco: Integer): TFDJSONDataSets;
    function BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
    function GetDescricaoTabelaPreco(AID: Integer): string;
    function GetProdutoTabelaPreco(AIdTabelaPreco: Integer; AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
    function GetTabelaPrecoItem(AIdTabelaPreco: Integer; AIdProduto: Integer): TTabelaPrecoItemProxy;
    function ExcluirTabelaDePreco(AIdTabelaPreco: Integer): Boolean;
  end;

  TSMContaReceberClient = class(TDSAdminClient)
  private
    FGerarContaReceberCommand: TDBXCommand;
    FRemoverContaReceberCommand: TDBXCommand;
    FPodeExcluirCommand: TDBXCommand;
    FAtualizarMovimentosContaCorrenteCommand: TDBXCommand;
    FRemoverMovimentosContaCorrenteCommand: TDBXCommand;
    FGetIdContaReceberPeloIdDaQuitacaoCommand: TDBXCommand;
    FGetIdsNovasQuitacoesCommand: TDBXCommand;
    FGetTotaisContaReceberCommand: TDBXCommand;
    FPessoaPossuiContaReceberEmAbertoCommand: TDBXCommand;
    FProcessoPossuiContaReceberEmAbertoCommand: TDBXCommand;
    FGetValorJurosCommand: TDBXCommand;
    FGetValorMultaCommand: TDBXCommand;
    FGetValorAbertoCommand: TDBXCommand;
    FAtualizarDadosBoletoContasReceberCommand: TDBXCommand;
    FGetQueryVariosContasReceberJSONDatasetsCommand: TDBXCommand;
    FCancelarBoletoEmitidoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure GerarContaReceber(AValue: string);
    function RemoverContaReceber(AValue: Integer): Boolean;
    function PodeExcluir(AValue: Integer): Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaReceber: Integer; AIdChaveProcesso: Integer; AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean;
    function GetIdContaReceberPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
    function GetIdsNovasQuitacoes(AIdUltimaQuitacao: Integer; AIdContaReceber: Integer): TList<System.Integer>;
    function GetTotaisContaReceber(AIdsContaReceber: string): TFDJSONDataSets;
    function PessoaPossuiContaReceberEmAberto(AIdPessoa: Integer): Integer;
    function ProcessoPossuiContaReceberEmAberto(AIdChaveProcesso: Integer): Integer;
    function GetValorJuros(AIdContaReceber: Integer): Double;
    function GetValorMulta(AIdContaReceber: Integer): Double;
    function GetValorAberto(AIdContaReceber: Integer): Double;
    procedure AtualizarDadosBoletoContasReceber(AIdContaReceber: Integer; ABoletoNossoNumero: string; ABoletoLinhaDigitavel: string; ABoletoCodigoBarras: string);
    function GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: string): TFDJSONDataSets;
    procedure CancelarBoletoEmitido(AIdsContaReceber: string);
  end;

  TSMContaPagarClient = class(TDSAdminClient)
  private
    FGerarContaPagarCommand: TDBXCommand;
    FRemoverContaPagarCommand: TDBXCommand;
    FPodeRemoverCommand: TDBXCommand;
    FAtualizarMovimentosContaCorrenteCommand: TDBXCommand;
    FRemoverMovimentosContaCorrenteCommand: TDBXCommand;
    FGetIdContaPagarPeloIdDaQuitacaoCommand: TDBXCommand;
    FGetTotaisContaPagarCommand: TDBXCommand;
    FPessoaPossuiContaPagarEmAbertoCommand: TDBXCommand;
    FProcessoPossuiContaPagarEmAbertoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GerarContaPagar(AValue: string): Boolean;
    function RemoverContaPagar(AValue: Integer): Boolean;
    function PodeRemover(AValue: Integer): Boolean;
    function AtualizarMovimentosContaCorrente(AIdContaPagar: Integer; AIdChaveProcesso: Integer; AGerarContraPartida: Boolean): Boolean;
    function RemoverMovimentosContaCorrente(AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean;
    function GetIdContaPagarPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
    function GetTotaisContaPagar(AIdsContaPagar: string): TFDJSONDataSets;
    function PessoaPossuiContaPagarEmAberto(AIdPessoa: Integer): Integer;
    function ProcessoPossuiContaPagarEmAberto(AIdChaveProcesso: Integer): Integer;
  end;

  TSMGeracaoDocumentoClient = class(TDSAdminClient)
  private
    FPodeExcluirCommand: TDBXCommand;
    FPodeEstornarCommand: TDBXCommand;
    FGerarDocumentoCommand: TDBXCommand;
    FEstornarDocumentoCommand: TDBXCommand;
    FGetContasReceberAbertoCommand: TDBXCommand;
    FGetContasPagarAbertoCommand: TDBXCommand;
    FDuplicarCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function GerarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: string): Boolean;
    function EstornarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: string): Boolean;
    function GetContasReceberAberto(AIdChaveProcesso: Integer): TFDJSONDataSets;
    function GetContasPagarAberto(AIdChaveProcesso: Integer): TFDJSONDataSets;
    function Duplicar(AIdGeracaoDocumento: Integer): Integer;
  end;

  TSMVendaClient = class(TDSAdminClient)
  private
    FEfetivarCommand: TDBXCommand;
    FCancelarCommand: TDBXCommand;
    FGetIdCommand: TDBXCommand;
    FEfetivarMovimentacaoVendaCondicionalCommand: TDBXCommand;
    FCancelarMovimentacaoVendaCondicionalCommand: TDBXCommand;
    FGetVendaProxyCommand: TDBXCommand;
    FGetIdNotaFiscalNFCECommand: TDBXCommand;
    FGetIdNotaFiscalNFECommand: TDBXCommand;
    FGerarDevolucaoEmDinheiroParaClienteCommand: TDBXCommand;
    FGerarDevolucaoEmCreditoParaClienteCommand: TDBXCommand;
    FGerarDevolucaoCreditandoNoContasAReceberCommand: TDBXCommand;
    FPodeEstornarCommand: TDBXCommand;
    FExcluirVendaCommand: TDBXCommand;
    FGetTotaisPorTipoVendaCommand: TDBXCommand;
    FBuscarBloqueiosVendaCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function Efetivar(AIdChaveProcesso: Integer; AVendaProxy: string): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GetId(AIdChaveProcesso: Integer): Integer;
    function EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
    function CancelarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
    function GetVendaProxy(AIdVenda: Integer): string;
    function GetIdNotaFiscalNFCE(AId: Integer): Integer;
    function GetIdNotaFiscalNFE(AId: Integer): Integer;
    function GerarDevolucaoEmDinheiroParaCliente(AVendaProxy: TVendaProxy; AValorDevolucao: Double): Boolean;
    function GerarDevolucaoEmCreditoParaCliente(AIdChaveProcesso: Integer; AValorDevolucao: Double): Boolean;
    function GerarDevolucaoCreditandoNoContasAReceber(AIdChaveProcesso: Integer; AValorDevolucao: Double; AIdsContasReceber: string): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    procedure ExcluirVenda(AIdChaveProcesso: Integer);
    function GetTotaisPorTipoVenda(AIdsVenda: string): TFDJSONDataSets;
    function BuscarBloqueiosVenda(AIdVenda: Integer): TFDJSONDataSets;
  end;

  TSMRelatorioFRClient = class(TDSAdminClient)
  private
    FBuscarRelatorioCommand: TDBXCommand;
    FBuscarRelatoriosAssociadosCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function BuscarRelatorio(AIdMaster: Integer; AIdRelatorio: Integer; AWhere: string): TStream;
    function BuscarRelatoriosAssociados(AClasseFormulario: string; AIdUsuario: Integer): TFDJSONDataSets;
  end;

  TSMAjusteEstoqueClient = class(TDSAdminClient)
  private
    FEfetivarCommand: TDBXCommand;
    FCancelarCommand: TDBXCommand;
    FReabrirCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function Efetivar(AIdChaveProcesso: Integer): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function Reabrir(AIdChaveProcesso: Integer): Boolean;
  end;

  TSMMotivoClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMChequeClient = class(TDSAdminClient)
  private
    FGerarUtilizacaoChequeCommand: TDBXCommand;
    FEstornarUtilizacaoChequeCommand: TDBXCommand;
    FGetChequeCommand: TDBXCommand;
    FChequeDisponivelParaUtilizacaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure GerarUtilizacaoCheque(AIdCheque: Integer);
    procedure EstornarUtilizacaoCheque(AIdCheque: Integer);
    function GetCheque(AIdCheque: Integer): string;
    function ChequeDisponivelParaUtilizacao(AIdCheque: Integer): Boolean;
  end;

  TSMServicoClient = class(TDSAdminClient)
  private
    FGetServicoCommand: TDBXCommand;
    FGetDescricaoCommand: TDBXCommand;
    FGerarServicoCommand: TDBXCommand;
    FExisteServicoCommand: TDBXCommand;
    FExisteServicoTerceiroCommand: TDBXCommand;
    FGetIdServicoPorDescricaoCommand: TDBXCommand;
    FGetIdServicoTerceiroPorDescricaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetServico(AIdServico: Integer): string;
    function GetDescricao(AIdServico: Integer): string;
    function GerarServico(AServico: TServicoProxy): Integer;
    function ExisteServico(ADescricaoServico: string): Boolean;
    function ExisteServicoTerceiro(ADescricaoServico: string): Boolean;
    function GetIdServicoPorDescricao(ADescricaoServico: string): Integer;
    function GetIdServicoTerceiroPorDescricao(ADescricaoServico: string): Integer;
  end;

  TSMCheckListClient = class(TDSAdminClient)
  private
    FGetTodosItensAtivosDoChecklistCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetTodosItensAtivosDoChecklist(AIdChecklist: Integer): TFDJSONDataSets;
  end;

  TSMEquipamentoClient = class(TDSAdminClient)
  private
    FGetEquipamentoCommand: TDBXCommand;
    FGerarEquipamentoCommand: TDBXCommand;
    FBuscarIdEquipamentoPelaDescricaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetEquipamento(AIdEquipamento: Integer): TEquipamentoProxy;
    function GerarEquipamento(AEquipamento: TEquipamentoProxy): Integer;
    function BuscarIdEquipamentoPelaDescricao(ADescricaoEquipamento: string): Integer;
  end;

  TSMSituacaoClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMOrdemServicoClient = class(TDSAdminClient)
  private
    FPodeExcluirCommand: TDBXCommand;
    FPodeEstornarCommand: TDBXCommand;
    FEfetivarCommand: TDBXCommand;
    FCancelarCommand: TDBXCommand;
    FGetIdCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function PodeExcluir(AIdChaveProcesso: Integer): Boolean;
    function PodeEstornar(AIdChaveProcesso: Integer): Boolean;
    function Efetivar(AIdChaveProcesso: Integer; OrdemServicoProxy: string): Boolean;
    function Cancelar(AIdChaveProcesso: Integer): Boolean;
    function GetId(AIdChaveProcesso: Integer): Integer;
  end;

  TSMLotePagamentoClient = class(TDSAdminClient)
  private
    FRemoverQuitacoesIndividuaisCommand: TDBXCommand;
    FRemoverMovimentosIndividuaisCommand: TDBXCommand;
    FGerarLancamentosAgrupadosCommand: TDBXCommand;
    FRemoverLancamentosAgrupadosCommand: TDBXCommand;
    FGetContasAPagarCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
    function RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
    function GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
    function RemoverLancamentosAgrupados(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
    function GetContasAPagar(AIdsContaPagar: string): TFDJSONDataSets;
  end;

  TSMLoteRecebimentoClient = class(TDSAdminClient)
  private
    FRemoverQuitacoesIndividuaisCommand: TDBXCommand;
    FGerarLancamentosAgrupadosCommand: TDBXCommand;
    FRemoverLancamentosAgrupadosCommand: TDBXCommand;
    FGetContasAReceberCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
    function GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
    function RemoverLancamentosAgrupados(AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
    function GetContasAReceber(AIdsContaReceber: string): TFDJSONDataSets;
  end;

  TSMCNAEClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMCorClient = class(TDSAdminClient)
  private
    FGetCorCommand: TDBXCommand;
    FGerarCorCommand: TDBXCommand;
    FExisteCorCommand: TDBXCommand;
    FGetIdCorPorDescricaoCommand: TDBXCommand;
    FGetDescricaoCorCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetCor(AIdCor: Integer): TCorProxy;
    function GerarCor(ACor: TCorProxy): Integer;
    function ExisteCor(ADescricaoCor: string): Boolean;
    function GetIdCorPorDescricao(ADescricaoCor: string): Integer;
    function GetDescricaoCor(AIdCor: Integer): string;
  end;

  TSMOperadoraCartaoClient = class(TDSAdminClient)
  private
    FGetOperadoraCartaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetOperadoraCartao(AIdOperadoraCartao: Integer): string;
  end;

  TSMVeiculoClient = class(TDSAdminClient)
  private
    FGetVeiculoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetVeiculo(AIdVeiculo: Integer): string;
  end;

  TSMCreditoPessoaClient = class(TDSAdminClient)
  private
    FCreditoDisponivelCommand: TDBXCommand;
    FRealizarTransacaoCreditoCommand: TDBXCommand;
    FExisteMovimentacaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function CreditoDisponivel(AIdPessoa: Integer): Double;
    function RealizarTransacaoCredito(APessoaCredito: string): Boolean;
    function ExisteMovimentacao(AIdPessoa: Integer): Boolean;
  end;

  TSMNCMClient = class(TDSAdminClient)
  private
    FGetNCMCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetNCM(AIdNCM: Integer): string;
  end;

  TSMCarteiraClient = class(TDSAdminClient)
  private
    FGetCarteiraCommand: TDBXCommand;
    FBuscarProximaSequenciaRemessaCommand: TDBXCommand;
    FBuscarProximaSequenciaNossoNumeroCommand: TDBXCommand;
    FIncrementarBoletoNossoNumeroProximoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetCarteira(AIdCarteira: Integer): string;
    function BuscarProximaSequenciaRemessa(AIdCarteira: Integer): Integer;
    function BuscarProximaSequenciaNossoNumero(AIdCarteira: Integer): Integer;
    procedure IncrementarBoletoNossoNumeroProximo(AIdCarteira: Integer);
  end;

  TSMCSOSNClient = class(TDSAdminClient)
  private
    FGetCstCsosnCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetCstCsosn(AIdCSOSCN: Integer): string;
  end;

  TSMRegimeEspecialClient = class(TDSAdminClient)
  private
    FGetRegimeEspecialCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetRegimeEspecial(AIdRegimeEspecial: Integer): string;
  end;

  TSMSituacaoEspecialClient = class(TDSAdminClient)
  private
    FGetSituacaoEspecialCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetSituacaoEspecial(AIdSituacaoEspecial: Integer): string;
  end;

  TSMZoneamentoClient = class(TDSAdminClient)
  private
    FGetZoneamentoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetZoneamento(AIdZoneamento: Integer): string;
  end;

  TSMReceitaOticaClient = class(TDSAdminClient)
  private
    FGetReceitaOticaCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetReceitaOtica(AIdReceitaOtica: Integer): string;
  end;

  TSMMedicoClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMRegraImpostoClient = class(TDSAdminClient)
  private
    FGetRegraImpostoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetRegraImposto(AIdRegraImposto: Integer): string;
  end;

  TSMCSTIPIClient = class(TDSAdminClient)
  private
    FGetCSTIPICommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetCSTIPI(AIdCSTIPI: Integer): string;
  end;

  TSMCSTPISCofinsClient = class(TDSAdminClient)
  private
    FGetCSTPISCofinsCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetCSTPISCofins(AIdCSTPISCofins: Integer): string;
  end;

  TSMImpostoIPIClient = class(TDSAdminClient)
  private
    FGetImpostoIPICommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetImpostoIPI(AIdImpostoIPI: Integer): string;
  end;

  TSMImpostoICMSClient = class(TDSAdminClient)
  private
    FGetImpostoICMSCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetImpostoICMS(AIdImpostoICMS: Integer): string;
  end;

  TSMImpostoPisCofinsClient = class(TDSAdminClient)
  private
    FGetImpostoPisCofinsCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetImpostoPisCofins(AIdImpostoPisCofins: Integer): string;
  end;

  TSMConfiguracoesFiscaisClient = class(TDSAdminClient)
  private
    FGetConfiguracaoFiscalCommand: TDBXCommand;
    FDuplicarConfiguracaoFiscalCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetConfiguracaoFiscal(AIdConfiguracaoFiscal: Integer): string;
    function DuplicarConfiguracaoFiscal(AIdConfiguracaoFiscalOrigem: Integer): Integer;
  end;

  TSMInventarioClient = class(TDSAdminClient)
  private
    FGetInventarioCommand: TDBXCommand;
    FGerarInventarioCommand: TDBXCommand;
    FEfetivarCommand: TDBXCommand;
    FReabrirCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetInventario(AIdInventario: Integer): string;
    function GerarInventario(AInventario: string): Integer;
    function Efetivar(AIdChaveProcesso: Integer): Boolean;
    function Reabrir(AIdChaveProcesso: Integer): Boolean;
  end;

  TSMFluxoCaixaResumidoClient = class(TDSAdminClient)
  private
    FGetContasCorrentesCommand: TDBXCommand;
    FGetFluxoCaixaResumidoCommand: TDBXCommand;
    FGetTotalPorContaCorrenteCommand: TDBXCommand;
    FGetFluxoCaixaResumidoProxyCommand: TDBXCommand;
    FGerarFluxoCaixaResumidoCommand: TDBXCommand;
    FGetTotalContaReceberCommand: TDBXCommand;
    FGetTotalContaPagarCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetContasCorrentes: TFDJSONDataSets;
    function GetFluxoCaixaResumido(AContasCorrentes: string; AListaCamposPeriodo: string; ATipoPeriodo: string): TFDJSONDataSets;
    function GetTotalPorContaCorrente(AContasCorrentes: string; ADtInicial: string; ADtFinal: string; ATipoPeriodo: string): TFDJSONDataSets;
    function GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
    function GerarFluxoCaixaResumido(AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
    function GetTotalContaReceber(ADtInicial: string; ADtFinal: string): Double;
    function GetTotalContaPagar(ADtInicial: string; ADtFinal: string): Double;
  end;

  TSMMovimentacaoChequeClient = class(TDSAdminClient)
  private
    FEfetivarCommand: TDBXCommand;
    FEstornarCommand: TDBXCommand;
    FPodeReabrirCommand: TDBXCommand;
    FPodeExcluirCommand: TDBXCommand;
    FBuscarIDPelaChaveProcessoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function Efetivar(AIdChaveProcesso: Integer): Boolean;
    function Estornar(AIdChaveProcesso: Integer): Boolean;
    function PodeReabrir(AIdMovimentacaoCheque: Integer): Boolean;
    function PodeExcluir(AIdMovimentacaoCheque: Integer): Boolean;
    function BuscarIDPelaChaveProcesso(AIDChaveProcesso: Integer): Integer;
  end;

  TSMMontadoraClient = class(TDSAdminClient)
  private
    FGetMontadoraCommand: TDBXCommand;
    FGerarMontadoraCommand: TDBXCommand;
    FExisteMontadoraCommand: TDBXCommand;
    FGetIdMontadoraPorDescricaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetMontadora(AIdMontadora: Integer): TMontadoraProxy;
    function GerarMontadora(AMontadora: TMontadoraProxy): Integer;
    function ExisteMontadora(ADescricaoMontadora: string): Boolean;
    function GetIdMontadoraPorDescricao(ADescricaoMontadora: string): Integer;
  end;

  TSMModeloMontadoraClient = class(TDSAdminClient)
  private
    FGetModeloMontadoraCommand: TDBXCommand;
    FGerarModeloMontadoraCommand: TDBXCommand;
    FExisteModeloCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetModeloMontadora(AIdModeloMontadora: Integer): TModeloMontadoraProxy;
    function GerarModeloMontadora(AModeloMontadora: TModeloMontadoraProxy): Integer;
    function ExisteModelo(ADescricaoModelo: string; ADescricaoMontadora: string): Boolean;
  end;

  TSMNegociacaoContaReceberClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMTamanhoClient = class(TDSAdminClient)
  private
    FGetTamanhoCommand: TDBXCommand;
    FGerarTamanhoCommand: TDBXCommand;
    FExisteTamanhoCommand: TDBXCommand;
    FGetIdTamanhoPorDescricaoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetTamanho(AIdTamanho: Integer): TTamanhoProxy;
    function GerarTamanho(ATamanho: TTamanhoProxy): Integer;
    function ExisteTamanho(ADescricaoTamanho: string): Boolean;
    function GetIdTamanhoPorDescricao(ADescricaoTamanho: string): Integer;
  end;

  TSMGradeProdutoClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMBackupClient = class(TDSAdminClient)
  private
    FValidarConfiguracoesParaBackupCommand: TDBXCommand;
    FRealizarBackupCommand: TDBXCommand;
    FBackupDiarioRealizadoCommand: TDBXCommand;
    FGerarRegistroBackupCommand: TDBXCommand;
    FDownloadBackupCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function ValidarConfiguracoesParaBackup: Boolean;
    function RealizarBackup(AIdUsuario: Integer): Boolean;
    function BackupDiarioRealizado: Boolean;
    function GerarRegistroBackup(ABackup: TBackupProxy): Integer;
    function DownloadBackup(out Size: Int64): TStream;
  end;

  TSMCFOPClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

  TSMBloqueioPersonalizadoClient = class(TDSAdminClient)
  private
    FUsuarioAutorizadoCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function UsuarioAutorizado(AIdBloqueioPersonalizado: Integer; AIdUsuario: Integer): Boolean;
  end;

  TSMTipoRestricaoClient = class(TDSAdminClient)
  private
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
  end;

implementation

function TSMLocalidadeClient.GetLogradouroPeloCEP(AIdCEP: string): string;
begin
  if FGetLogradouroPeloCEPCommand = nil then
  begin
    FGetLogradouroPeloCEPCommand := FDBXConnection.CreateCommand;
    FGetLogradouroPeloCEPCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetLogradouroPeloCEPCommand.Text := 'TSMLocalidade.GetLogradouroPeloCEP';
    FGetLogradouroPeloCEPCommand.Prepare;
  end;
  FGetLogradouroPeloCEPCommand.Parameters[0].Value.SetWideString(AIdCEP);
  FGetLogradouroPeloCEPCommand.ExecuteUpdate;
  Result := FGetLogradouroPeloCEPCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMLocalidadeClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMLocalidadeClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMLocalidadeClient.Destroy;
begin
  FGetLogradouroPeloCEPCommand.DisposeOf;
  inherited;
end;

function TServerMethodsClient.ReverseString(Value: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FDBXConnection.CreateCommand;
    FReverseStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReverseStringCommand.Text := 'TServerMethods.ReverseString';
    FReverseStringCommand.Prepare;
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.ExecuteUpdate;
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodsClient.GetDados(AIdentifier: string; AWhereBundle: string; AIdUsuario: Integer): TFDJSONDataSets;
begin
  if FGetDadosCommand = nil then
  begin
    FGetDadosCommand := FDBXConnection.CreateCommand;
    FGetDadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDadosCommand.Text := 'TServerMethods.GetDados';
    FGetDadosCommand.Prepare;
  end;
  FGetDadosCommand.Parameters[0].Value.SetWideString(AIdentifier);
  FGetDadosCommand.Parameters[1].Value.SetWideString(AWhereBundle);
  FGetDadosCommand.Parameters[2].Value.SetInt32(AIdUsuario);
  FGetDadosCommand.ExecuteUpdate;
  if not FGetDadosCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetDadosCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodsClient.GetDadosComInjecaoCampos(AIdentifier: string; AWhereBundle: string; AIdUsuario: Integer; ACamposInjetados: string): TFDJSONDataSets;
begin
  if FGetDadosComInjecaoCamposCommand = nil then
  begin
    FGetDadosComInjecaoCamposCommand := FDBXConnection.CreateCommand;
    FGetDadosComInjecaoCamposCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDadosComInjecaoCamposCommand.Text := 'TServerMethods.GetDadosComInjecaoCampos';
    FGetDadosComInjecaoCamposCommand.Prepare;
  end;
  FGetDadosComInjecaoCamposCommand.Parameters[0].Value.SetWideString(AIdentifier);
  FGetDadosComInjecaoCamposCommand.Parameters[1].Value.SetWideString(AWhereBundle);
  FGetDadosComInjecaoCamposCommand.Parameters[2].Value.SetInt32(AIdUsuario);
  FGetDadosComInjecaoCamposCommand.Parameters[3].Value.SetWideString(ACamposInjetados);
  FGetDadosComInjecaoCamposCommand.ExecuteUpdate;
  if not FGetDadosComInjecaoCamposCommand.Parameters[4].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetDadosComInjecaoCamposCommand.Parameters[4].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosComInjecaoCamposCommand.Parameters[4].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosComInjecaoCamposCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodsClient.GetDadosFromSQL(ASQL: string): TFDJSONDataSets;
begin
  if FGetDadosFromSQLCommand = nil then
  begin
    FGetDadosFromSQLCommand := FDBXConnection.CreateCommand;
    FGetDadosFromSQLCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDadosFromSQLCommand.Text := 'TServerMethods.GetDadosFromSQL';
    FGetDadosFromSQLCommand.Prepare;
  end;
  FGetDadosFromSQLCommand.Parameters[0].Value.SetWideString(ASQL);
  FGetDadosFromSQLCommand.ExecuteUpdate;
  if not FGetDadosFromSQLCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetDadosFromSQLCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosFromSQLCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosFromSQLCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TServerMethodsClient.Commit;
begin
  if FCommitCommand = nil then
  begin
    FCommitCommand := FDBXConnection.CreateCommand;
    FCommitCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCommitCommand.Text := 'TServerMethods.Commit';
    FCommitCommand.Prepare;
  end;
  FCommitCommand.ExecuteUpdate;
end;

procedure TServerMethodsClient.RollBack;
begin
  if FRollBackCommand = nil then
  begin
    FRollBackCommand := FDBXConnection.CreateCommand;
    FRollBackCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRollBackCommand.Text := 'TServerMethods.RollBack';
    FRollBackCommand.Prepare;
  end;
  FRollBackCommand.ExecuteUpdate;
end;

procedure TServerMethodsClient.StartTransaction;
begin
  if FStartTransactionCommand = nil then
  begin
    FStartTransactionCommand := FDBXConnection.CreateCommand;
    FStartTransactionCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FStartTransactionCommand.Text := 'TServerMethods.StartTransaction';
    FStartTransactionCommand.Prepare;
  end;
  FStartTransactionCommand.ExecuteUpdate;
end;

function TServerMethodsClient.GetLastedAutoIncrementValue: Integer;
begin
  if FGetLastedAutoIncrementValueCommand = nil then
  begin
    FGetLastedAutoIncrementValueCommand := FDBXConnection.CreateCommand;
    FGetLastedAutoIncrementValueCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetLastedAutoIncrementValueCommand.Text := 'TServerMethods.GetLastedAutoIncrementValue';
    FGetLastedAutoIncrementValueCommand.Prepare;
  end;
  FGetLastedAutoIncrementValueCommand.ExecuteUpdate;
  Result := FGetLastedAutoIncrementValueCommand.Parameters[0].Value.GetInt32;
end;

function TServerMethodsClient.GetSQLPesquisaPadrao(AIdentifier: string; AIdUsuario: Integer): string;
begin
  if FGetSQLPesquisaPadraoCommand = nil then
  begin
    FGetSQLPesquisaPadraoCommand := FDBXConnection.CreateCommand;
    FGetSQLPesquisaPadraoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetSQLPesquisaPadraoCommand.Text := 'TServerMethods.GetSQLPesquisaPadrao';
    FGetSQLPesquisaPadraoCommand.Prepare;
  end;
  FGetSQLPesquisaPadraoCommand.Parameters[0].Value.SetWideString(AIdentifier);
  FGetSQLPesquisaPadraoCommand.Parameters[1].Value.SetInt32(AIdUsuario);
  FGetSQLPesquisaPadraoCommand.ExecuteUpdate;
  Result := FGetSQLPesquisaPadraoCommand.Parameters[2].Value.GetWideString;
end;

procedure TServerMethodsClient.SetSQLPesquisaPadrao(AIdentifier: string; AIdUsuario: Integer; ASQL: string);
begin
  if FSetSQLPesquisaPadraoCommand = nil then
  begin
    FSetSQLPesquisaPadraoCommand := FDBXConnection.CreateCommand;
    FSetSQLPesquisaPadraoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSetSQLPesquisaPadraoCommand.Text := 'TServerMethods.SetSQLPesquisaPadrao';
    FSetSQLPesquisaPadraoCommand.Prepare;
  end;
  FSetSQLPesquisaPadraoCommand.Parameters[0].Value.SetWideString(AIdentifier);
  FSetSQLPesquisaPadraoCommand.Parameters[1].Value.SetInt32(AIdUsuario);
  FSetSQLPesquisaPadraoCommand.Parameters[2].Value.SetWideString(ASQL);
  FSetSQLPesquisaPadraoCommand.ExecuteUpdate;
end;

function TServerMethodsClient.GetUltimaChaveDaTabela(ATabela: string; ACampoRetorno: string): string;
begin
  if FGetUltimaChaveDaTabelaCommand = nil then
  begin
    FGetUltimaChaveDaTabelaCommand := FDBXConnection.CreateCommand;
    FGetUltimaChaveDaTabelaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetUltimaChaveDaTabelaCommand.Text := 'TServerMethods.GetUltimaChaveDaTabela';
    FGetUltimaChaveDaTabelaCommand.Prepare;
  end;
  FGetUltimaChaveDaTabelaCommand.Parameters[0].Value.SetWideString(ATabela);
  FGetUltimaChaveDaTabelaCommand.Parameters[1].Value.SetWideString(ACampoRetorno);
  FGetUltimaChaveDaTabelaCommand.ExecuteUpdate;
  Result := FGetUltimaChaveDaTabelaCommand.Parameters[2].Value.GetWideString;
end;

procedure TServerMethodsClient.HabilitarConsultaAutomatica(ACodigoUsuario: Integer; AFormName: string; AHabilitar: Boolean);
begin
  if FHabilitarConsultaAutomaticaCommand = nil then
  begin
    FHabilitarConsultaAutomaticaCommand := FDBXConnection.CreateCommand;
    FHabilitarConsultaAutomaticaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FHabilitarConsultaAutomaticaCommand.Text := 'TServerMethods.HabilitarConsultaAutomatica';
    FHabilitarConsultaAutomaticaCommand.Prepare;
  end;
  FHabilitarConsultaAutomaticaCommand.Parameters[0].Value.SetInt32(ACodigoUsuario);
  FHabilitarConsultaAutomaticaCommand.Parameters[1].Value.SetWideString(AFormName);
  FHabilitarConsultaAutomaticaCommand.Parameters[2].Value.SetBoolean(AHabilitar);
  FHabilitarConsultaAutomaticaCommand.ExecuteUpdate;
end;

function TServerMethodsClient.EstaConsultandoAutomatico(ACodigoUsuario: Integer; AFormName: string): Boolean;
begin
  if FEstaConsultandoAutomaticoCommand = nil then
  begin
    FEstaConsultandoAutomaticoCommand := FDBXConnection.CreateCommand;
    FEstaConsultandoAutomaticoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstaConsultandoAutomaticoCommand.Text := 'TServerMethods.EstaConsultandoAutomatico';
    FEstaConsultandoAutomaticoCommand.Prepare;
  end;
  FEstaConsultandoAutomaticoCommand.Parameters[0].Value.SetInt32(ACodigoUsuario);
  FEstaConsultandoAutomaticoCommand.Parameters[1].Value.SetWideString(AFormName);
  FEstaConsultandoAutomaticoCommand.ExecuteUpdate;
  Result := FEstaConsultandoAutomaticoCommand.Parameters[2].Value.GetBoolean;
end;


constructor TServerMethodsClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TServerMethodsClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TServerMethodsClient.Destroy;
begin
  FReverseStringCommand.DisposeOf;
  FGetDadosCommand.DisposeOf;
  FGetDadosComInjecaoCamposCommand.DisposeOf;
  FGetDadosFromSQLCommand.DisposeOf;
  FCommitCommand.DisposeOf;
  FRollBackCommand.DisposeOf;
  FStartTransactionCommand.DisposeOf;
  FGetLastedAutoIncrementValueCommand.DisposeOf;
  FGetSQLPesquisaPadraoCommand.DisposeOf;
  FSetSQLPesquisaPadraoCommand.DisposeOf;
  FGetUltimaChaveDaTabelaCommand.DisposeOf;
  FHabilitarConsultaAutomaticaCommand.DisposeOf;
  FEstaConsultandoAutomaticoCommand.DisposeOf;
  inherited;
end;

function TSMPessoaClient.GetNomePessoa(AIDPessoa: Integer): string;
begin
  if FGetNomePessoaCommand = nil then
  begin
    FGetNomePessoaCommand := FDBXConnection.CreateCommand;
    FGetNomePessoaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetNomePessoaCommand.Text := 'TSMPessoa.GetNomePessoa';
    FGetNomePessoaCommand.Prepare;
  end;
  FGetNomePessoaCommand.Parameters[0].Value.SetInt32(AIDPessoa);
  FGetNomePessoaCommand.ExecuteUpdate;
  Result := FGetNomePessoaCommand.Parameters[1].Value.GetWideString;
end;

function TSMPessoaClient.GetPessoaEndereco(AIdPessoaEndereco: Integer): TPessoaEnderecoProxy;
begin
  if FGetPessoaEnderecoCommand = nil then
  begin
    FGetPessoaEnderecoCommand := FDBXConnection.CreateCommand;
    FGetPessoaEnderecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPessoaEnderecoCommand.Text := 'TSMPessoa.GetPessoaEndereco';
    FGetPessoaEnderecoCommand.Prepare;
  end;
  FGetPessoaEnderecoCommand.Parameters[0].Value.SetInt32(AIdPessoaEndereco);
  FGetPessoaEnderecoCommand.ExecuteUpdate;
  if not FGetPessoaEnderecoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetPessoaEnderecoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TPessoaEnderecoProxy(FUnMarshal.UnMarshal(FGetPessoaEnderecoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetPessoaEnderecoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMPessoaClient.GetUsuario(AIdUsuario: Integer): TFDJSONDataSets;
begin
  if FGetUsuarioCommand = nil then
  begin
    FGetUsuarioCommand := FDBXConnection.CreateCommand;
    FGetUsuarioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetUsuarioCommand.Text := 'TSMPessoa.GetUsuario';
    FGetUsuarioCommand.Prepare;
  end;
  FGetUsuarioCommand.Parameters[0].Value.SetInt32(AIdUsuario);
  FGetUsuarioCommand.ExecuteUpdate;
  if not FGetUsuarioCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetUsuarioCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetUsuarioCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetUsuarioCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMPessoaClient.GetPessoa(AIdPessoa: Integer): string;
begin
  if FGetPessoaCommand = nil then
  begin
    FGetPessoaCommand := FDBXConnection.CreateCommand;
    FGetPessoaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPessoaCommand.Text := 'TSMPessoa.GetPessoa';
    FGetPessoaCommand.Prepare;
  end;
  FGetPessoaCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FGetPessoaCommand.ExecuteUpdate;
  Result := FGetPessoaCommand.Parameters[1].Value.GetWideString;
end;

function TSMPessoaClient.GerarPessoa(APessoa: string): Integer;
begin
  if FGerarPessoaCommand = nil then
  begin
    FGerarPessoaCommand := FDBXConnection.CreateCommand;
    FGerarPessoaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarPessoaCommand.Text := 'TSMPessoa.GerarPessoa';
    FGerarPessoaCommand.Prepare;
  end;
  FGerarPessoaCommand.Parameters[0].Value.SetWideString(APessoa);
  FGerarPessoaCommand.ExecuteUpdate;
  Result := FGerarPessoaCommand.Parameters[1].Value.GetInt32;
end;

function TSMPessoaClient.GetIdUsuario(AUsuario: string): Integer;
begin
  if FGetIdUsuarioCommand = nil then
  begin
    FGetIdUsuarioCommand := FDBXConnection.CreateCommand;
    FGetIdUsuarioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdUsuarioCommand.Text := 'TSMPessoa.GetIdUsuario';
    FGetIdUsuarioCommand.Prepare;
  end;
  FGetIdUsuarioCommand.Parameters[0].Value.SetWideString(AUsuario);
  FGetIdUsuarioCommand.ExecuteUpdate;
  Result := FGetIdUsuarioCommand.Parameters[1].Value.GetInt32;
end;

procedure TSMPessoaClient.SetClienteConsumidorFinal(AIdPessoa: Integer);
begin
  if FSetClienteConsumidorFinalCommand = nil then
  begin
    FSetClienteConsumidorFinalCommand := FDBXConnection.CreateCommand;
    FSetClienteConsumidorFinalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSetClienteConsumidorFinalCommand.Text := 'TSMPessoa.SetClienteConsumidorFinal';
    FSetClienteConsumidorFinalCommand.Prepare;
  end;
  FSetClienteConsumidorFinalCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FSetClienteConsumidorFinalCommand.ExecuteUpdate;
end;

function TSMPessoaClient.VerificarConsumidorFinal(AIdPessoa: Integer): Boolean;
begin
  if FVerificarConsumidorFinalCommand = nil then
  begin
    FVerificarConsumidorFinalCommand := FDBXConnection.CreateCommand;
    FVerificarConsumidorFinalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FVerificarConsumidorFinalCommand.Text := 'TSMPessoa.VerificarConsumidorFinal';
    FVerificarConsumidorFinalCommand.Prepare;
  end;
  FVerificarConsumidorFinalCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FVerificarConsumidorFinalCommand.ExecuteUpdate;
  Result := FVerificarConsumidorFinalCommand.Parameters[1].Value.GetBoolean;
end;

function TSMPessoaClient.ValidaCnpjCeiCpf(Numero: string): Boolean;
begin
  if FValidaCnpjCeiCpfCommand = nil then
  begin
    FValidaCnpjCeiCpfCommand := FDBXConnection.CreateCommand;
    FValidaCnpjCeiCpfCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FValidaCnpjCeiCpfCommand.Text := 'TSMPessoa.ValidaCnpjCeiCpf';
    FValidaCnpjCeiCpfCommand.Prepare;
  end;
  FValidaCnpjCeiCpfCommand.Parameters[0].Value.SetWideString(Numero);
  FValidaCnpjCeiCpfCommand.ExecuteUpdate;
  Result := FValidaCnpjCeiCpfCommand.Parameters[1].Value.GetBoolean;
end;

function TSMPessoaClient.PossuiCPFValido(AIdPessoa: Integer): Boolean;
begin
  if FPossuiCPFValidoCommand = nil then
  begin
    FPossuiCPFValidoCommand := FDBXConnection.CreateCommand;
    FPossuiCPFValidoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPossuiCPFValidoCommand.Text := 'TSMPessoa.PossuiCPFValido';
    FPossuiCPFValidoCommand.Prepare;
  end;
  FPossuiCPFValidoCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FPossuiCPFValidoCommand.ExecuteUpdate;
  Result := FPossuiCPFValidoCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMPessoaClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMPessoaClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMPessoaClient.Destroy;
begin
  FGetNomePessoaCommand.DisposeOf;
  FGetPessoaEnderecoCommand.DisposeOf;
  FGetUsuarioCommand.DisposeOf;
  FGetPessoaCommand.DisposeOf;
  FGerarPessoaCommand.DisposeOf;
  FGetIdUsuarioCommand.DisposeOf;
  FSetClienteConsumidorFinalCommand.DisposeOf;
  FVerificarConsumidorFinalCommand.DisposeOf;
  FValidaCnpjCeiCpfCommand.DisposeOf;
  FPossuiCPFValidoCommand.DisposeOf;
  inherited;
end;

procedure TSMFinanceiroClient.fdqContaCorrenteMovimentoAfterPost(DataSet: TDataSet);
begin
  if FfdqContaCorrenteMovimentoAfterPostCommand = nil then
  begin
    FfdqContaCorrenteMovimentoAfterPostCommand := FDBXConnection.CreateCommand;
    FfdqContaCorrenteMovimentoAfterPostCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FfdqContaCorrenteMovimentoAfterPostCommand.Text := 'TSMFinanceiro.fdqContaCorrenteMovimentoAfterPost';
    FfdqContaCorrenteMovimentoAfterPostCommand.Prepare;
  end;
  FfdqContaCorrenteMovimentoAfterPostCommand.Parameters[0].Value.SetDBXReader(TDBXDataSetReader.Create(DataSet, FInstanceOwner), True);
  FfdqContaCorrenteMovimentoAfterPostCommand.ExecuteUpdate;
end;

function TSMFinanceiroClient.GetContaCorrente(AAtivo: Boolean): TFDJSONDataSets;
begin
  if FGetContaCorrenteCommand = nil then
  begin
    FGetContaCorrenteCommand := FDBXConnection.CreateCommand;
    FGetContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContaCorrenteCommand.Text := 'TSMFinanceiro.GetContaCorrente';
    FGetContaCorrenteCommand.Prepare;
  end;
  FGetContaCorrenteCommand.Parameters[0].Value.SetBoolean(AAtivo);
  FGetContaCorrenteCommand.ExecuteUpdate;
  if not FGetContaCorrenteCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContaCorrenteCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContaCorrenteCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContaCorrenteCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TSMFinanceiroClient.AtualizarSaldoMovimentacao(AIdContaCorrente: Integer);
begin
  if FAtualizarSaldoMovimentacaoCommand = nil then
  begin
    FAtualizarSaldoMovimentacaoCommand := FDBXConnection.CreateCommand;
    FAtualizarSaldoMovimentacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarSaldoMovimentacaoCommand.Text := 'TSMFinanceiro.AtualizarSaldoMovimentacao';
    FAtualizarSaldoMovimentacaoCommand.Prepare;
  end;
  FAtualizarSaldoMovimentacaoCommand.Parameters[0].Value.SetInt32(AIdContaCorrente);
  FAtualizarSaldoMovimentacaoCommand.ExecuteUpdate;
end;

function TSMFinanceiroClient.RealizarTransferencia(AIdMovimentoOrigem: Integer; AIdContaCorrenteDestino: Integer): Integer;
begin
  if FRealizarTransferenciaCommand = nil then
  begin
    FRealizarTransferenciaCommand := FDBXConnection.CreateCommand;
    FRealizarTransferenciaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRealizarTransferenciaCommand.Text := 'TSMFinanceiro.RealizarTransferencia';
    FRealizarTransferenciaCommand.Prepare;
  end;
  FRealizarTransferenciaCommand.Parameters[0].Value.SetInt32(AIdMovimentoOrigem);
  FRealizarTransferenciaCommand.Parameters[1].Value.SetInt32(AIdContaCorrenteDestino);
  FRealizarTransferenciaCommand.ExecuteUpdate;
  Result := FRealizarTransferenciaCommand.Parameters[2].Value.GetInt32;
end;

procedure TSMFinanceiroClient.ConciliarRegistro(AIdMovimentacao: Integer);
begin
  if FConciliarRegistroCommand = nil then
  begin
    FConciliarRegistroCommand := FDBXConnection.CreateCommand;
    FConciliarRegistroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FConciliarRegistroCommand.Text := 'TSMFinanceiro.ConciliarRegistro';
    FConciliarRegistroCommand.Prepare;
  end;
  FConciliarRegistroCommand.Parameters[0].Value.SetInt32(AIdMovimentacao);
  FConciliarRegistroCommand.ExecuteUpdate;
end;

procedure TSMFinanceiroClient.DesconciliarRegistro(AIdMovimentacao: Integer);
begin
  if FDesconciliarRegistroCommand = nil then
  begin
    FDesconciliarRegistroCommand := FDBXConnection.CreateCommand;
    FDesconciliarRegistroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FDesconciliarRegistroCommand.Text := 'TSMFinanceiro.DesconciliarRegistro';
    FDesconciliarRegistroCommand.Prepare;
  end;
  FDesconciliarRegistroCommand.Parameters[0].Value.SetInt32(AIdMovimentacao);
  FDesconciliarRegistroCommand.ExecuteUpdate;
end;

function TSMFinanceiroClient.RegistroEstaConciliado(AIdMovimentacao: Integer): Boolean;
begin
  if FRegistroEstaConciliadoCommand = nil then
  begin
    FRegistroEstaConciliadoCommand := FDBXConnection.CreateCommand;
    FRegistroEstaConciliadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRegistroEstaConciliadoCommand.Text := 'TSMFinanceiro.RegistroEstaConciliado';
    FRegistroEstaConciliadoCommand.Prepare;
  end;
  FRegistroEstaConciliadoCommand.Parameters[0].Value.SetInt32(AIdMovimentacao);
  FRegistroEstaConciliadoCommand.ExecuteUpdate;
  Result := FRegistroEstaConciliadoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.GerarMovimentoContaCorrente(AValue: string): Boolean;
begin
  if FGerarMovimentoContaCorrenteCommand = nil then
  begin
    FGerarMovimentoContaCorrenteCommand := FDBXConnection.CreateCommand;
    FGerarMovimentoContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarMovimentoContaCorrenteCommand.Text := 'TSMFinanceiro.GerarMovimentoContaCorrente';
    FGerarMovimentoContaCorrenteCommand.Prepare;
  end;
  FGerarMovimentoContaCorrenteCommand.Parameters[0].Value.SetWideString(AValue);
  FGerarMovimentoContaCorrenteCommand.ExecuteUpdate;
  Result := FGerarMovimentoContaCorrenteCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.RemoverMovimentoContaCorrente(AValue: Integer): Boolean;
begin
  if FRemoverMovimentoContaCorrenteCommand = nil then
  begin
    FRemoverMovimentoContaCorrenteCommand := FDBXConnection.CreateCommand;
    FRemoverMovimentoContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverMovimentoContaCorrenteCommand.Text := 'TSMFinanceiro.RemoverMovimentoContaCorrente';
    FRemoverMovimentoContaCorrenteCommand.Prepare;
  end;
  FRemoverMovimentoContaCorrenteCommand.Parameters[0].Value.SetInt32(AValue);
  FRemoverMovimentoContaCorrenteCommand.ExecuteUpdate;
  Result := FRemoverMovimentoContaCorrenteCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.GerarDocumento(AValue: string): Boolean;
begin
  if FGerarDocumentoCommand = nil then
  begin
    FGerarDocumentoCommand := FDBXConnection.CreateCommand;
    FGerarDocumentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarDocumentoCommand.Text := 'TSMFinanceiro.GerarDocumento';
    FGerarDocumentoCommand.Prepare;
  end;
  FGerarDocumentoCommand.Parameters[0].Value.SetWideString(AValue);
  FGerarDocumentoCommand.ExecuteUpdate;
  Result := FGerarDocumentoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.GetSaldoAnteriorContaCorrente(AIdContaCorrente: Integer; ADtMovimento: string): Double;
begin
  if FGetSaldoAnteriorContaCorrenteCommand = nil then
  begin
    FGetSaldoAnteriorContaCorrenteCommand := FDBXConnection.CreateCommand;
    FGetSaldoAnteriorContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetSaldoAnteriorContaCorrenteCommand.Text := 'TSMFinanceiro.GetSaldoAnteriorContaCorrente';
    FGetSaldoAnteriorContaCorrenteCommand.Prepare;
  end;
  FGetSaldoAnteriorContaCorrenteCommand.Parameters[0].Value.SetInt32(AIdContaCorrente);
  FGetSaldoAnteriorContaCorrenteCommand.Parameters[1].Value.SetWideString(ADtMovimento);
  FGetSaldoAnteriorContaCorrenteCommand.ExecuteUpdate;
  Result := FGetSaldoAnteriorContaCorrenteCommand.Parameters[2].Value.GetDouble;
end;

function TSMFinanceiroClient.GetIDNivel(ANivel: Integer; ASequencia: string): Integer;
begin
  if FGetIDNivelCommand = nil then
  begin
    FGetIDNivelCommand := FDBXConnection.CreateCommand;
    FGetIDNivelCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIDNivelCommand.Text := 'TSMFinanceiro.GetIDNivel';
    FGetIDNivelCommand.Prepare;
  end;
  FGetIDNivelCommand.Parameters[0].Value.SetInt32(ANivel);
  FGetIDNivelCommand.Parameters[1].Value.SetWideString(ASequencia);
  FGetIDNivelCommand.ExecuteUpdate;
  Result := FGetIDNivelCommand.Parameters[2].Value.GetInt32;
end;

function TSMFinanceiroClient.GetPlanoContaCompleto: TFDJSONDataSets;
begin
  if FGetPlanoContaCompletoCommand = nil then
  begin
    FGetPlanoContaCompletoCommand := FDBXConnection.CreateCommand;
    FGetPlanoContaCompletoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPlanoContaCompletoCommand.Text := 'TSMFinanceiro.GetPlanoContaCompleto';
    FGetPlanoContaCompletoCommand.Prepare;
  end;
  FGetPlanoContaCompletoCommand.ExecuteUpdate;
  if not FGetPlanoContaCompletoCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetPlanoContaCompletoCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetPlanoContaCompletoCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetPlanoContaCompletoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetPlanoContaPorCentroResultado(AIdCentroResultado: Integer): TFDJSONDataSets;
begin
  if FGetPlanoContaPorCentroResultadoCommand = nil then
  begin
    FGetPlanoContaPorCentroResultadoCommand := FDBXConnection.CreateCommand;
    FGetPlanoContaPorCentroResultadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPlanoContaPorCentroResultadoCommand.Text := 'TSMFinanceiro.GetPlanoContaPorCentroResultado';
    FGetPlanoContaPorCentroResultadoCommand.Prepare;
  end;
  FGetPlanoContaPorCentroResultadoCommand.Parameters[0].Value.SetInt32(AIdCentroResultado);
  FGetPlanoContaPorCentroResultadoCommand.ExecuteUpdate;
  if not FGetPlanoContaPorCentroResultadoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetPlanoContaPorCentroResultadoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetPlanoContaPorCentroResultadoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetPlanoContaPorCentroResultadoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TSMFinanceiroClient.SetIDPlanoContaAssociado;
begin
  if FSetIDPlanoContaAssociadoCommand = nil then
  begin
    FSetIDPlanoContaAssociadoCommand := FDBXConnection.CreateCommand;
    FSetIDPlanoContaAssociadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSetIDPlanoContaAssociadoCommand.Text := 'TSMFinanceiro.SetIDPlanoContaAssociado';
    FSetIDPlanoContaAssociadoCommand.Prepare;
  end;
  FSetIDPlanoContaAssociadoCommand.ExecuteUpdate;
end;

function TSMFinanceiroClient.GetDescricaoCentroResultado(AID: Integer): string;
begin
  if FGetDescricaoCentroResultadoCommand = nil then
  begin
    FGetDescricaoCentroResultadoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoCentroResultadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoCentroResultadoCommand.Text := 'TSMFinanceiro.GetDescricaoCentroResultado';
    FGetDescricaoCentroResultadoCommand.Prepare;
  end;
  FGetDescricaoCentroResultadoCommand.Parameters[0].Value.SetInt32(AID);
  FGetDescricaoCentroResultadoCommand.ExecuteUpdate;
  Result := FGetDescricaoCentroResultadoCommand.Parameters[1].Value.GetWideString;
end;

function TSMFinanceiroClient.GetDescricaoContaAnalise(AID: Integer): string;
begin
  if FGetDescricaoContaAnaliseCommand = nil then
  begin
    FGetDescricaoContaAnaliseCommand := FDBXConnection.CreateCommand;
    FGetDescricaoContaAnaliseCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoContaAnaliseCommand.Text := 'TSMFinanceiro.GetDescricaoContaAnalise';
    FGetDescricaoContaAnaliseCommand.Prepare;
  end;
  FGetDescricaoContaAnaliseCommand.Parameters[0].Value.SetInt32(AID);
  FGetDescricaoContaAnaliseCommand.ExecuteUpdate;
  Result := FGetDescricaoContaAnaliseCommand.Parameters[1].Value.GetWideString;
end;

function TSMFinanceiroClient.GetCentroResultados: TFDJSONDataSets;
begin
  if FGetCentroResultadosCommand = nil then
  begin
    FGetCentroResultadosCommand := FDBXConnection.CreateCommand;
    FGetCentroResultadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCentroResultadosCommand.Text := 'TSMFinanceiro.GetCentroResultados';
    FGetCentroResultadosCommand.Prepare;
  end;
  FGetCentroResultadosCommand.ExecuteUpdate;
  if not FGetCentroResultadosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetCentroResultadosCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetCentroResultadosCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetCentroResultadosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetPrimeiroNivel(ANivel: Integer; ACentrosResultados: string; AListaCamposPeriodo: string): TFDJSONDataSets;
begin
  if FGetPrimeiroNivelCommand = nil then
  begin
    FGetPrimeiroNivelCommand := FDBXConnection.CreateCommand;
    FGetPrimeiroNivelCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPrimeiroNivelCommand.Text := 'TSMFinanceiro.GetPrimeiroNivel';
    FGetPrimeiroNivelCommand.Prepare;
  end;
  FGetPrimeiroNivelCommand.Parameters[0].Value.SetInt32(ANivel);
  FGetPrimeiroNivelCommand.Parameters[1].Value.SetWideString(ACentrosResultados);
  FGetPrimeiroNivelCommand.Parameters[2].Value.SetWideString(AListaCamposPeriodo);
  FGetPrimeiroNivelCommand.ExecuteUpdate;
  if not FGetPrimeiroNivelCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetPrimeiroNivelCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetPrimeiroNivelCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetPrimeiroNivelCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetNivelAninhado(ANivel: Integer; ACentrosResultados: string; APlanosContas: string; AListaCamposPeriodo: string): TFDJSONDataSets;
begin
  if FGetNivelAninhadoCommand = nil then
  begin
    FGetNivelAninhadoCommand := FDBXConnection.CreateCommand;
    FGetNivelAninhadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetNivelAninhadoCommand.Text := 'TSMFinanceiro.GetNivelAninhado';
    FGetNivelAninhadoCommand.Prepare;
  end;
  FGetNivelAninhadoCommand.Parameters[0].Value.SetInt32(ANivel);
  FGetNivelAninhadoCommand.Parameters[1].Value.SetWideString(ACentrosResultados);
  FGetNivelAninhadoCommand.Parameters[2].Value.SetWideString(APlanosContas);
  FGetNivelAninhadoCommand.Parameters[3].Value.SetWideString(AListaCamposPeriodo);
  FGetNivelAninhadoCommand.ExecuteUpdate;
  if not FGetNivelAninhadoCommand.Parameters[4].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetNivelAninhadoCommand.Parameters[4].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetNivelAninhadoCommand.Parameters[4].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetNivelAninhadoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetMovimentacao(ACentrosResultados: string; APlanosContas: string; AContasCorrentes: string; AListaCamposPeriodo: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
begin
  if FGetMovimentacaoCommand = nil then
  begin
    FGetMovimentacaoCommand := FDBXConnection.CreateCommand;
    FGetMovimentacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetMovimentacaoCommand.Text := 'TSMFinanceiro.GetMovimentacao';
    FGetMovimentacaoCommand.Prepare;
  end;
  FGetMovimentacaoCommand.Parameters[0].Value.SetWideString(ACentrosResultados);
  FGetMovimentacaoCommand.Parameters[1].Value.SetWideString(APlanosContas);
  FGetMovimentacaoCommand.Parameters[2].Value.SetWideString(AContasCorrentes);
  FGetMovimentacaoCommand.Parameters[3].Value.SetWideString(AListaCamposPeriodo);
  FGetMovimentacaoCommand.Parameters[4].Value.SetWideString(ADtInicial);
  FGetMovimentacaoCommand.Parameters[5].Value.SetWideString(ADtFinal);
  FGetMovimentacaoCommand.ExecuteUpdate;
  if not FGetMovimentacaoCommand.Parameters[6].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetMovimentacaoCommand.Parameters[6].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetMovimentacaoCommand.Parameters[6].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetMovimentacaoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetContasCorrentes(ACentrosResultados: string; APlanosContas: string; AListaCamposPeriodo: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
begin
  if FGetContasCorrentesCommand = nil then
  begin
    FGetContasCorrentesCommand := FDBXConnection.CreateCommand;
    FGetContasCorrentesCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasCorrentesCommand.Text := 'TSMFinanceiro.GetContasCorrentes';
    FGetContasCorrentesCommand.Prepare;
  end;
  FGetContasCorrentesCommand.Parameters[0].Value.SetWideString(ACentrosResultados);
  FGetContasCorrentesCommand.Parameters[1].Value.SetWideString(APlanosContas);
  FGetContasCorrentesCommand.Parameters[2].Value.SetWideString(AListaCamposPeriodo);
  FGetContasCorrentesCommand.Parameters[3].Value.SetWideString(ADtInicial);
  FGetContasCorrentesCommand.Parameters[4].Value.SetWideString(ADtFinal);
  FGetContasCorrentesCommand.ExecuteUpdate;
  if not FGetContasCorrentesCommand.Parameters[5].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasCorrentesCommand.Parameters[5].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasCorrentesCommand.Parameters[5].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasCorrentesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetTotalPorCentroResultado(ACentrosResultados: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
begin
  if FGetTotalPorCentroResultadoCommand = nil then
  begin
    FGetTotalPorCentroResultadoCommand := FDBXConnection.CreateCommand;
    FGetTotalPorCentroResultadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalPorCentroResultadoCommand.Text := 'TSMFinanceiro.GetTotalPorCentroResultado';
    FGetTotalPorCentroResultadoCommand.Prepare;
  end;
  FGetTotalPorCentroResultadoCommand.Parameters[0].Value.SetWideString(ACentrosResultados);
  FGetTotalPorCentroResultadoCommand.Parameters[1].Value.SetWideString(ADtInicial);
  FGetTotalPorCentroResultadoCommand.Parameters[2].Value.SetWideString(ADtFinal);
  FGetTotalPorCentroResultadoCommand.ExecuteUpdate;
  if not FGetTotalPorCentroResultadoCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotalPorCentroResultadoCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotalPorCentroResultadoCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotalPorCentroResultadoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetTotalPorNivel(ANivel: Integer; ACentrosResultados: string; AContasAnalises: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
begin
  if FGetTotalPorNivelCommand = nil then
  begin
    FGetTotalPorNivelCommand := FDBXConnection.CreateCommand;
    FGetTotalPorNivelCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalPorNivelCommand.Text := 'TSMFinanceiro.GetTotalPorNivel';
    FGetTotalPorNivelCommand.Prepare;
  end;
  FGetTotalPorNivelCommand.Parameters[0].Value.SetInt32(ANivel);
  FGetTotalPorNivelCommand.Parameters[1].Value.SetWideString(ACentrosResultados);
  FGetTotalPorNivelCommand.Parameters[2].Value.SetWideString(AContasAnalises);
  FGetTotalPorNivelCommand.Parameters[3].Value.SetWideString(ADtInicial);
  FGetTotalPorNivelCommand.Parameters[4].Value.SetWideString(ADtFinal);
  FGetTotalPorNivelCommand.ExecuteUpdate;
  if not FGetTotalPorNivelCommand.Parameters[5].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotalPorNivelCommand.Parameters[5].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotalPorNivelCommand.Parameters[5].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotalPorNivelCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetTotalPorContaCorrente(ACentrosResultados: string; AContasAnalises: string; ADtInicial: string; ADtFinal: string): TFDJSONDataSets;
begin
  if FGetTotalPorContaCorrenteCommand = nil then
  begin
    FGetTotalPorContaCorrenteCommand := FDBXConnection.CreateCommand;
    FGetTotalPorContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalPorContaCorrenteCommand.Text := 'TSMFinanceiro.GetTotalPorContaCorrente';
    FGetTotalPorContaCorrenteCommand.Prepare;
  end;
  FGetTotalPorContaCorrenteCommand.Parameters[0].Value.SetWideString(ACentrosResultados);
  FGetTotalPorContaCorrenteCommand.Parameters[1].Value.SetWideString(AContasAnalises);
  FGetTotalPorContaCorrenteCommand.Parameters[2].Value.SetWideString(ADtInicial);
  FGetTotalPorContaCorrenteCommand.Parameters[3].Value.SetWideString(ADtFinal);
  FGetTotalPorContaCorrenteCommand.ExecuteUpdate;
  if not FGetTotalPorContaCorrenteCommand.Parameters[4].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotalPorContaCorrenteCommand.Parameters[4].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotalPorContaCorrenteCommand.Parameters[4].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotalPorContaCorrenteCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.BuscarValorPrevisao(ANivel: Integer; APeriodoPrevisaoInicial: string; APeriodoPrevisaoFinal: string; AIdCentroResultado: Integer; AIdContaAnalise: Integer): Double;
begin
  if FBuscarValorPrevisaoCommand = nil then
  begin
    FBuscarValorPrevisaoCommand := FDBXConnection.CreateCommand;
    FBuscarValorPrevisaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarValorPrevisaoCommand.Text := 'TSMFinanceiro.BuscarValorPrevisao';
    FBuscarValorPrevisaoCommand.Prepare;
  end;
  FBuscarValorPrevisaoCommand.Parameters[0].Value.SetInt32(ANivel);
  FBuscarValorPrevisaoCommand.Parameters[1].Value.SetWideString(APeriodoPrevisaoInicial);
  FBuscarValorPrevisaoCommand.Parameters[2].Value.SetWideString(APeriodoPrevisaoFinal);
  FBuscarValorPrevisaoCommand.Parameters[3].Value.SetInt32(AIdCentroResultado);
  FBuscarValorPrevisaoCommand.Parameters[4].Value.SetInt32(AIdContaAnalise);
  FBuscarValorPrevisaoCommand.ExecuteUpdate;
  Result := FBuscarValorPrevisaoCommand.Parameters[5].Value.GetDouble;
end;

procedure TSMFinanceiroClient.SalvarPrevisao(AIdCentroResultado: Integer; AIdContaAnalise: Integer; APeriodo: string; AValor: Double);
begin
  if FSalvarPrevisaoCommand = nil then
  begin
    FSalvarPrevisaoCommand := FDBXConnection.CreateCommand;
    FSalvarPrevisaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSalvarPrevisaoCommand.Text := 'TSMFinanceiro.SalvarPrevisao';
    FSalvarPrevisaoCommand.Prepare;
  end;
  FSalvarPrevisaoCommand.Parameters[0].Value.SetInt32(AIdCentroResultado);
  FSalvarPrevisaoCommand.Parameters[1].Value.SetInt32(AIdContaAnalise);
  FSalvarPrevisaoCommand.Parameters[2].Value.SetWideString(APeriodo);
  FSalvarPrevisaoCommand.Parameters[3].Value.SetDouble(AValor);
  FSalvarPrevisaoCommand.ExecuteUpdate;
end;

function TSMFinanceiroClient.GetPlanoPagamento(AIdPlanoPagamento: Integer): TFDJSONDataSets;
begin
  if FGetPlanoPagamentoCommand = nil then
  begin
    FGetPlanoPagamentoCommand := FDBXConnection.CreateCommand;
    FGetPlanoPagamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetPlanoPagamentoCommand.Text := 'TSMFinanceiro.GetPlanoPagamento';
    FGetPlanoPagamentoCommand.Prepare;
  end;
  FGetPlanoPagamentoCommand.Parameters[0].Value.SetInt32(AIdPlanoPagamento);
  FGetPlanoPagamentoCommand.ExecuteUpdate;
  if not FGetPlanoPagamentoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetPlanoPagamentoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetPlanoPagamentoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetPlanoPagamentoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFinanceiroClient.GetDescricaoPlanoPagamento(AID: Integer): string;
begin
  if FGetDescricaoPlanoPagamentoCommand = nil then
  begin
    FGetDescricaoPlanoPagamentoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoPlanoPagamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoPlanoPagamentoCommand.Text := 'TSMFinanceiro.GetDescricaoPlanoPagamento';
    FGetDescricaoPlanoPagamentoCommand.Prepare;
  end;
  FGetDescricaoPlanoPagamentoCommand.Parameters[0].Value.SetInt32(AID);
  FGetDescricaoPlanoPagamentoCommand.ExecuteUpdate;
  Result := FGetDescricaoPlanoPagamentoCommand.Parameters[1].Value.GetWideString;
end;

function TSMFinanceiroClient.GetDescricaoFormaPagamento(AID: Integer): string;
begin
  if FGetDescricaoFormaPagamentoCommand = nil then
  begin
    FGetDescricaoFormaPagamentoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoFormaPagamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoFormaPagamentoCommand.Text := 'TSMFinanceiro.GetDescricaoFormaPagamento';
    FGetDescricaoFormaPagamentoCommand.Prepare;
  end;
  FGetDescricaoFormaPagamentoCommand.Parameters[0].Value.SetInt32(AID);
  FGetDescricaoFormaPagamentoCommand.ExecuteUpdate;
  Result := FGetDescricaoFormaPagamentoCommand.Parameters[1].Value.GetWideString;
end;

function TSMFinanceiroClient.GetFormaPagamento(AID: Integer): string;
begin
  if FGetFormaPagamentoCommand = nil then
  begin
    FGetFormaPagamentoCommand := FDBXConnection.CreateCommand;
    FGetFormaPagamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetFormaPagamentoCommand.Text := 'TSMFinanceiro.GetFormaPagamento';
    FGetFormaPagamentoCommand.Prepare;
  end;
  FGetFormaPagamentoCommand.Parameters[0].Value.SetInt32(AID);
  FGetFormaPagamentoCommand.ExecuteUpdate;
  Result := FGetFormaPagamentoCommand.Parameters[1].Value.GetWideString;
end;

function TSMFinanceiroClient.GetCodigosFormaPagamentoCartao: string;
begin
  if FGetCodigosFormaPagamentoCartaoCommand = nil then
  begin
    FGetCodigosFormaPagamentoCartaoCommand := FDBXConnection.CreateCommand;
    FGetCodigosFormaPagamentoCartaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCodigosFormaPagamentoCartaoCommand.Text := 'TSMFinanceiro.GetCodigosFormaPagamentoCartao';
    FGetCodigosFormaPagamentoCartaoCommand.Prepare;
  end;
  FGetCodigosFormaPagamentoCartaoCommand.ExecuteUpdate;
  Result := FGetCodigosFormaPagamentoCartaoCommand.Parameters[0].Value.GetWideString;
end;

function TSMFinanceiroClient.FormaPagamentoCartao(AIdFormaPagamento: Integer): Boolean;
begin
  if FFormaPagamentoCartaoCommand = nil then
  begin
    FFormaPagamentoCartaoCommand := FDBXConnection.CreateCommand;
    FFormaPagamentoCartaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFormaPagamentoCartaoCommand.Text := 'TSMFinanceiro.FormaPagamentoCartao';
    FFormaPagamentoCartaoCommand.Prepare;
  end;
  FFormaPagamentoCartaoCommand.Parameters[0].Value.SetInt32(AIdFormaPagamento);
  FFormaPagamentoCartaoCommand.ExecuteUpdate;
  Result := FFormaPagamentoCartaoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.FormaPagamentoCartaoDebito(AIdFormaPagamento: Integer): Boolean;
begin
  if FFormaPagamentoCartaoDebitoCommand = nil then
  begin
    FFormaPagamentoCartaoDebitoCommand := FDBXConnection.CreateCommand;
    FFormaPagamentoCartaoDebitoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFormaPagamentoCartaoDebitoCommand.Text := 'TSMFinanceiro.FormaPagamentoCartaoDebito';
    FFormaPagamentoCartaoDebitoCommand.Prepare;
  end;
  FFormaPagamentoCartaoDebitoCommand.Parameters[0].Value.SetInt32(AIdFormaPagamento);
  FFormaPagamentoCartaoDebitoCommand.ExecuteUpdate;
  Result := FFormaPagamentoCartaoDebitoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.FormaPagamentoCartaoCredito(AIdFormaPagamento: Integer): Boolean;
begin
  if FFormaPagamentoCartaoCreditoCommand = nil then
  begin
    FFormaPagamentoCartaoCreditoCommand := FDBXConnection.CreateCommand;
    FFormaPagamentoCartaoCreditoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFormaPagamentoCartaoCreditoCommand.Text := 'TSMFinanceiro.FormaPagamentoCartaoCredito';
    FFormaPagamentoCartaoCreditoCommand.Prepare;
  end;
  FFormaPagamentoCartaoCreditoCommand.Parameters[0].Value.SetInt32(AIdFormaPagamento);
  FFormaPagamentoCartaoCreditoCommand.ExecuteUpdate;
  Result := FFormaPagamentoCartaoCreditoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.FormaPagamentoDinheiro(AIdFormaPagamento: Integer): Boolean;
begin
  if FFormaPagamentoDinheiroCommand = nil then
  begin
    FFormaPagamentoDinheiroCommand := FDBXConnection.CreateCommand;
    FFormaPagamentoDinheiroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFormaPagamentoDinheiroCommand.Text := 'TSMFinanceiro.FormaPagamentoDinheiro';
    FFormaPagamentoDinheiroCommand.Prepare;
  end;
  FFormaPagamentoDinheiroCommand.Parameters[0].Value.SetInt32(AIdFormaPagamento);
  FFormaPagamentoDinheiroCommand.ExecuteUpdate;
  Result := FFormaPagamentoDinheiroCommand.Parameters[1].Value.GetBoolean;
end;

function TSMFinanceiroClient.FormaPagamentoCheque(AIdFormaPagamento: Integer): Boolean;
begin
  if FFormaPagamentoChequeCommand = nil then
  begin
    FFormaPagamentoChequeCommand := FDBXConnection.CreateCommand;
    FFormaPagamentoChequeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFormaPagamentoChequeCommand.Text := 'TSMFinanceiro.FormaPagamentoCheque';
    FFormaPagamentoChequeCommand.Prepare;
  end;
  FFormaPagamentoChequeCommand.Parameters[0].Value.SetInt32(AIdFormaPagamento);
  FFormaPagamentoChequeCommand.ExecuteUpdate;
  Result := FFormaPagamentoChequeCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMFinanceiroClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMFinanceiroClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMFinanceiroClient.Destroy;
begin
  FfdqContaCorrenteMovimentoAfterPostCommand.DisposeOf;
  FGetContaCorrenteCommand.DisposeOf;
  FAtualizarSaldoMovimentacaoCommand.DisposeOf;
  FRealizarTransferenciaCommand.DisposeOf;
  FConciliarRegistroCommand.DisposeOf;
  FDesconciliarRegistroCommand.DisposeOf;
  FRegistroEstaConciliadoCommand.DisposeOf;
  FGerarMovimentoContaCorrenteCommand.DisposeOf;
  FRemoverMovimentoContaCorrenteCommand.DisposeOf;
  FGerarDocumentoCommand.DisposeOf;
  FGetSaldoAnteriorContaCorrenteCommand.DisposeOf;
  FGetIDNivelCommand.DisposeOf;
  FGetPlanoContaCompletoCommand.DisposeOf;
  FGetPlanoContaPorCentroResultadoCommand.DisposeOf;
  FSetIDPlanoContaAssociadoCommand.DisposeOf;
  FGetDescricaoCentroResultadoCommand.DisposeOf;
  FGetDescricaoContaAnaliseCommand.DisposeOf;
  FGetCentroResultadosCommand.DisposeOf;
  FGetPrimeiroNivelCommand.DisposeOf;
  FGetNivelAninhadoCommand.DisposeOf;
  FGetMovimentacaoCommand.DisposeOf;
  FGetContasCorrentesCommand.DisposeOf;
  FGetTotalPorCentroResultadoCommand.DisposeOf;
  FGetTotalPorNivelCommand.DisposeOf;
  FGetTotalPorContaCorrenteCommand.DisposeOf;
  FBuscarValorPrevisaoCommand.DisposeOf;
  FSalvarPrevisaoCommand.DisposeOf;
  FGetPlanoPagamentoCommand.DisposeOf;
  FGetDescricaoPlanoPagamentoCommand.DisposeOf;
  FGetDescricaoFormaPagamentoCommand.DisposeOf;
  FGetFormaPagamentoCommand.DisposeOf;
  FGetCodigosFormaPagamentoCartaoCommand.DisposeOf;
  FFormaPagamentoCartaoCommand.DisposeOf;
  FFormaPagamentoCartaoDebitoCommand.DisposeOf;
  FFormaPagamentoCartaoCreditoCommand.DisposeOf;
  FFormaPagamentoDinheiroCommand.DisposeOf;
  FFormaPagamentoChequeCommand.DisposeOf;
  inherited;
end;

function TSMIndoorClient.RestTest(AValue: string): string;
begin
  if FRestTestCommand = nil then
  begin
    FRestTestCommand := FDBXConnection.CreateCommand;
    FRestTestCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRestTestCommand.Text := 'TSMIndoor.RestTest';
    FRestTestCommand.Prepare;
  end;
  FRestTestCommand.Parameters[0].Value.SetWideString(AValue);
  FRestTestCommand.ExecuteUpdate;
  Result := FRestTestCommand.Parameters[1].Value.GetWideString;
end;

function TSMIndoorClient.ObterListaArquivos(AValue: string): string;
begin
  if FObterListaArquivosCommand = nil then
  begin
    FObterListaArquivosCommand := FDBXConnection.CreateCommand;
    FObterListaArquivosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FObterListaArquivosCommand.Text := 'TSMIndoor.ObterListaArquivos';
    FObterListaArquivosCommand.Prepare;
  end;
  FObterListaArquivosCommand.Parameters[0].Value.SetWideString(AValue);
  FObterListaArquivosCommand.ExecuteUpdate;
  Result := FObterListaArquivosCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMIndoorClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMIndoorClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMIndoorClient.Destroy;
begin
  FRestTestCommand.DisposeOf;
  FObterListaArquivosCommand.DisposeOf;
  inherited;
end;

procedure TSMRestauranteClient.FecharCaixa(AIdCaixaMovimento: Integer);
begin
  if FFecharCaixaCommand = nil then
  begin
    FFecharCaixaCommand := FDBXConnection.CreateCommand;
    FFecharCaixaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFecharCaixaCommand.Text := 'TSMRestaurante.FecharCaixa';
    FFecharCaixaCommand.Prepare;
  end;
  FFecharCaixaCommand.Parameters[0].Value.SetInt32(AIdCaixaMovimento);
  FFecharCaixaCommand.ExecuteUpdate;
end;

function TSMRestauranteClient.GetCaixa: TFDJSONDataSets;
begin
  if FGetCaixaCommand = nil then
  begin
    FGetCaixaCommand := FDBXConnection.CreateCommand;
    FGetCaixaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCaixaCommand.Text := 'TSMRestaurante.GetCaixa';
    FGetCaixaCommand.Prepare;
  end;
  FGetCaixaCommand.ExecuteUpdate;
  if not FGetCaixaCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetCaixaCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetCaixaCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetCaixaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMRestauranteClient.checkUsuario(AUsername: string; APassword: string): Integer;
begin
  if FcheckUsuarioCommand = nil then
  begin
    FcheckUsuarioCommand := FDBXConnection.CreateCommand;
    FcheckUsuarioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FcheckUsuarioCommand.Text := 'TSMRestaurante.checkUsuario';
    FcheckUsuarioCommand.Prepare;
  end;
  FcheckUsuarioCommand.Parameters[0].Value.SetWideString(AUsername);
  FcheckUsuarioCommand.Parameters[1].Value.SetWideString(APassword);
  FcheckUsuarioCommand.ExecuteUpdate;
  Result := FcheckUsuarioCommand.Parameters[2].Value.GetInt32;
end;

procedure TSMRestauranteClient.imprimirCaixa(id_caixa_movimento: Integer);
begin
  if FimprimirCaixaCommand = nil then
  begin
    FimprimirCaixaCommand := FDBXConnection.CreateCommand;
    FimprimirCaixaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FimprimirCaixaCommand.Text := 'TSMRestaurante.imprimirCaixa';
    FimprimirCaixaCommand.Prepare;
  end;
  FimprimirCaixaCommand.Parameters[0].Value.SetInt32(id_caixa_movimento);
  FimprimirCaixaCommand.ExecuteUpdate;
end;

procedure TSMRestauranteClient.SetTrocoComanda(vr_troco: Double; id_comanda: Integer);
begin
  if FSetTrocoComandaCommand = nil then
  begin
    FSetTrocoComandaCommand := FDBXConnection.CreateCommand;
    FSetTrocoComandaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSetTrocoComandaCommand.Text := 'TSMRestaurante.SetTrocoComanda';
    FSetTrocoComandaCommand.Prepare;
  end;
  FSetTrocoComandaCommand.Parameters[0].Value.SetDouble(vr_troco);
  FSetTrocoComandaCommand.Parameters[1].Value.SetInt32(id_comanda);
  FSetTrocoComandaCommand.ExecuteUpdate;
end;


constructor TSMRestauranteClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMRestauranteClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMRestauranteClient.Destroy;
begin
  FFecharCaixaCommand.DisposeOf;
  FGetCaixaCommand.DisposeOf;
  FcheckUsuarioCommand.DisposeOf;
  FimprimirCaixaCommand.DisposeOf;
  FSetTrocoComandaCommand.DisposeOf;
  inherited;
end;

procedure TSMProdutoClient.fdqProdutoAfterPost(DataSet: TDataSet);
begin
  if FfdqProdutoAfterPostCommand = nil then
  begin
    FfdqProdutoAfterPostCommand := FDBXConnection.CreateCommand;
    FfdqProdutoAfterPostCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FfdqProdutoAfterPostCommand.Text := 'TSMProduto.fdqProdutoAfterPost';
    FfdqProdutoAfterPostCommand.Prepare;
  end;
  FfdqProdutoAfterPostCommand.Parameters[0].Value.SetDBXReader(TDBXDataSetReader.Create(DataSet, FInstanceOwner), True);
  FfdqProdutoAfterPostCommand.ExecuteUpdate;
end;

procedure TSMProdutoClient.GerarMovimentacaoProduto(AValue: string);
begin
  if FGerarMovimentacaoProdutoCommand = nil then
  begin
    FGerarMovimentacaoProdutoCommand := FDBXConnection.CreateCommand;
    FGerarMovimentacaoProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarMovimentacaoProdutoCommand.Text := 'TSMProduto.GerarMovimentacaoProduto';
    FGerarMovimentacaoProdutoCommand.Prepare;
  end;
  FGerarMovimentacaoProdutoCommand.Parameters[0].Value.SetWideString(AValue);
  FGerarMovimentacaoProdutoCommand.ExecuteUpdate;
end;

function TSMProdutoClient.GetChaveProcessoMovimentacaoInicial(AIdProduto: Integer): Integer;
begin
  if FGetChaveProcessoMovimentacaoInicialCommand = nil then
  begin
    FGetChaveProcessoMovimentacaoInicialCommand := FDBXConnection.CreateCommand;
    FGetChaveProcessoMovimentacaoInicialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetChaveProcessoMovimentacaoInicialCommand.Text := 'TSMProduto.GetChaveProcessoMovimentacaoInicial';
    FGetChaveProcessoMovimentacaoInicialCommand.Prepare;
  end;
  FGetChaveProcessoMovimentacaoInicialCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FGetChaveProcessoMovimentacaoInicialCommand.ExecuteUpdate;
  Result := FGetChaveProcessoMovimentacaoInicialCommand.Parameters[1].Value.GetInt32;
end;

function TSMProdutoClient.RemoverMovimentacaoProduto(AValue: Integer): Boolean;
begin
  if FRemoverMovimentacaoProdutoCommand = nil then
  begin
    FRemoverMovimentacaoProdutoCommand := FDBXConnection.CreateCommand;
    FRemoverMovimentacaoProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverMovimentacaoProdutoCommand.Text := 'TSMProduto.RemoverMovimentacaoProduto';
    FRemoverMovimentacaoProdutoCommand.Prepare;
  end;
  FRemoverMovimentacaoProdutoCommand.Parameters[0].Value.SetInt32(AValue);
  FRemoverMovimentacaoProdutoCommand.ExecuteUpdate;
  Result := FRemoverMovimentacaoProdutoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMProdutoClient.GetProximoID: Integer;
begin
  if FGetProximoIDCommand = nil then
  begin
    FGetProximoIDCommand := FDBXConnection.CreateCommand;
    FGetProximoIDCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProximoIDCommand.Text := 'TSMProduto.GetProximoID';
    FGetProximoIDCommand.Prepare;
  end;
  FGetProximoIDCommand.ExecuteUpdate;
  Result := FGetProximoIDCommand.Parameters[0].Value.GetInt32;
end;

function TSMProdutoClient.CodigoDeBarraJaCadastrado(ACodigoBarra: string; AIdProduto: Integer): Integer;
begin
  if FCodigoDeBarraJaCadastradoCommand = nil then
  begin
    FCodigoDeBarraJaCadastradoCommand := FDBXConnection.CreateCommand;
    FCodigoDeBarraJaCadastradoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCodigoDeBarraJaCadastradoCommand.Text := 'TSMProduto.CodigoDeBarraJaCadastrado';
    FCodigoDeBarraJaCadastradoCommand.Prepare;
  end;
  FCodigoDeBarraJaCadastradoCommand.Parameters[0].Value.SetWideString(ACodigoBarra);
  FCodigoDeBarraJaCadastradoCommand.Parameters[1].Value.SetInt32(AIdProduto);
  FCodigoDeBarraJaCadastradoCommand.ExecuteUpdate;
  Result := FCodigoDeBarraJaCadastradoCommand.Parameters[2].Value.GetInt32;
end;

function TSMProdutoClient.GetProduto(AIdProduto: Integer): TFDJSONDataSets;
begin
  if FGetProdutoCommand = nil then
  begin
    FGetProdutoCommand := FDBXConnection.CreateCommand;
    FGetProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoCommand.Text := 'TSMProduto.GetProduto';
    FGetProdutoCommand.Prepare;
  end;
  FGetProdutoCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FGetProdutoCommand.ExecuteUpdate;
  if not FGetProdutoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetTodosGruposProduto: TFDJSONDataSets;
begin
  if FGetTodosGruposProdutoCommand = nil then
  begin
    FGetTodosGruposProdutoCommand := FDBXConnection.CreateCommand;
    FGetTodosGruposProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTodosGruposProdutoCommand.Text := 'TSMProduto.GetTodosGruposProduto';
    FGetTodosGruposProdutoCommand.Prepare;
  end;
  FGetTodosGruposProdutoCommand.ExecuteUpdate;
  if not FGetTodosGruposProdutoCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTodosGruposProdutoCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTodosGruposProdutoCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTodosGruposProdutoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetProdutoPeloCodigoBarra(ACodigoBarraProduto: string): TFDJSONDataSets;
begin
  if FGetProdutoPeloCodigoBarraCommand = nil then
  begin
    FGetProdutoPeloCodigoBarraCommand := FDBXConnection.CreateCommand;
    FGetProdutoPeloCodigoBarraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoPeloCodigoBarraCommand.Text := 'TSMProduto.GetProdutoPeloCodigoBarra';
    FGetProdutoPeloCodigoBarraCommand.Prepare;
  end;
  FGetProdutoPeloCodigoBarraCommand.Parameters[0].Value.SetWideString(ACodigoBarraProduto);
  FGetProdutoPeloCodigoBarraCommand.ExecuteUpdate;
  if not FGetProdutoPeloCodigoBarraCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutoPeloCodigoBarraCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutoPeloCodigoBarraCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutoPeloCodigoBarraCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetProdutos(AIdsProdutos: string): TFDJSONDataSets;
begin
  if FGetProdutosCommand = nil then
  begin
    FGetProdutosCommand := FDBXConnection.CreateCommand;
    FGetProdutosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutosCommand.Text := 'TSMProduto.GetProdutos';
    FGetProdutosCommand.Prepare;
  end;
  FGetProdutosCommand.Parameters[0].Value.SetWideString(AIdsProdutos);
  FGetProdutosCommand.ExecuteUpdate;
  if not FGetProdutosCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutosCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutosCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetProdutosCompleto(AIdsProdutos: string): TFDJSONDataSets;
begin
  if FGetProdutosCompletoCommand = nil then
  begin
    FGetProdutosCompletoCommand := FDBXConnection.CreateCommand;
    FGetProdutosCompletoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutosCompletoCommand.Text := 'TSMProduto.GetProdutosCompleto';
    FGetProdutosCompletoCommand.Prepare;
  end;
  FGetProdutosCompletoCommand.Parameters[0].Value.SetWideString(AIdsProdutos);
  FGetProdutosCompletoCommand.ExecuteUpdate;
  if not FGetProdutosCompletoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutosCompletoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutosCompletoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutosCompletoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetProdutoFilial(AIdProduto: Integer; AIdFilial: Integer): string;
begin
  if FGetProdutoFilialCommand = nil then
  begin
    FGetProdutoFilialCommand := FDBXConnection.CreateCommand;
    FGetProdutoFilialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoFilialCommand.Text := 'TSMProduto.GetProdutoFilial';
    FGetProdutoFilialCommand.Prepare;
  end;
  FGetProdutoFilialCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FGetProdutoFilialCommand.Parameters[1].Value.SetInt32(AIdFilial);
  FGetProdutoFilialCommand.ExecuteUpdate;
  Result := FGetProdutoFilialCommand.Parameters[2].Value.GetWideString;
end;

function TSMProdutoClient.GetProdutoFilialProxy(AIdProduto: Integer; AIdFilial: Integer): TProdutoProxy;
begin
  if FGetProdutoFilialProxyCommand = nil then
  begin
    FGetProdutoFilialProxyCommand := FDBXConnection.CreateCommand;
    FGetProdutoFilialProxyCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoFilialProxyCommand.Text := 'TSMProduto.GetProdutoFilialProxy';
    FGetProdutoFilialProxyCommand.Prepare;
  end;
  FGetProdutoFilialProxyCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FGetProdutoFilialProxyCommand.Parameters[1].Value.SetInt32(AIdFilial);
  FGetProdutoFilialProxyCommand.ExecuteUpdate;
  if not FGetProdutoFilialProxyCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutoFilialProxyCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TProdutoProxy(FUnMarshal.UnMarshal(FGetProdutoFilialProxyCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutoFilialProxyCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.GetListaProduto(AIdsProdutos: string; AIdFilial: Integer): string;
begin
  if FGetListaProdutoCommand = nil then
  begin
    FGetListaProdutoCommand := FDBXConnection.CreateCommand;
    FGetListaProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetListaProdutoCommand.Text := 'TSMProduto.GetListaProduto';
    FGetListaProdutoCommand.Prepare;
  end;
  FGetListaProdutoCommand.Parameters[0].Value.SetWideString(AIdsProdutos);
  FGetListaProdutoCommand.Parameters[1].Value.SetInt32(AIdFilial);
  FGetListaProdutoCommand.ExecuteUpdate;
  Result := FGetListaProdutoCommand.Parameters[2].Value.GetWideString;
end;

function TSMProdutoClient.ExisteMovimentacoesDiferentesDaMovimentacaoInicial(AIdProduto: Integer; AIdFilial: Integer): Boolean;
begin
  if FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand = nil then
  begin
    FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand := FDBXConnection.CreateCommand;
    FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.Text := 'TSMProduto.ExisteMovimentacoesDiferentesDaMovimentacaoInicial';
    FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.Prepare;
  end;
  FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.Parameters[1].Value.SetInt32(AIdFilial);
  FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.ExecuteUpdate;
  Result := FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.Parameters[2].Value.GetBoolean;
end;

function TSMProdutoClient.DuplicarProduto(AIdProdutoOrigem: Integer): Integer;
begin
  if FDuplicarProdutoCommand = nil then
  begin
    FDuplicarProdutoCommand := FDBXConnection.CreateCommand;
    FDuplicarProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FDuplicarProdutoCommand.Text := 'TSMProduto.DuplicarProduto';
    FDuplicarProdutoCommand.Prepare;
  end;
  FDuplicarProdutoCommand.Parameters[0].Value.SetInt32(AIdProdutoOrigem);
  FDuplicarProdutoCommand.ExecuteUpdate;
  Result := FDuplicarProdutoCommand.Parameters[1].Value.GetInt32;
end;

function TSMProdutoClient.GerarCodigoBarraEAN13(ACodigoProduto: Integer): string;
begin
  if FGerarCodigoBarraEAN13Command = nil then
  begin
    FGerarCodigoBarraEAN13Command := FDBXConnection.CreateCommand;
    FGerarCodigoBarraEAN13Command.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarCodigoBarraEAN13Command.Text := 'TSMProduto.GerarCodigoBarraEAN13';
    FGerarCodigoBarraEAN13Command.Prepare;
  end;
  FGerarCodigoBarraEAN13Command.Parameters[0].Value.SetInt32(ACodigoProduto);
  FGerarCodigoBarraEAN13Command.ExecuteUpdate;
  Result := FGerarCodigoBarraEAN13Command.Parameters[1].Value.GetWideString;
end;

function TSMProdutoClient.GerarProdutos(AProdutosEmGrade: string): Boolean;
begin
  if FGerarProdutosCommand = nil then
  begin
    FGerarProdutosCommand := FDBXConnection.CreateCommand;
    FGerarProdutosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarProdutosCommand.Text := 'TSMProduto.GerarProdutos';
    FGerarProdutosCommand.Prepare;
  end;
  FGerarProdutosCommand.Parameters[0].Value.SetWideString(AProdutosEmGrade);
  FGerarProdutosCommand.ExecuteUpdate;
  Result := FGerarProdutosCommand.Parameters[1].Value.GetBoolean;
end;

function TSMProdutoClient.VisaoProdutoGrade(AIdGradeProduto: Integer; AIdProdutoAgrupador: Integer): TFDJSONDataSets;
begin
  if FVisaoProdutoGradeCommand = nil then
  begin
    FVisaoProdutoGradeCommand := FDBXConnection.CreateCommand;
    FVisaoProdutoGradeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FVisaoProdutoGradeCommand.Text := 'TSMProduto.VisaoProdutoGrade';
    FVisaoProdutoGradeCommand.Prepare;
  end;
  FVisaoProdutoGradeCommand.Parameters[0].Value.SetInt32(AIdGradeProduto);
  FVisaoProdutoGradeCommand.Parameters[1].Value.SetInt32(AIdProdutoAgrupador);
  FVisaoProdutoGradeCommand.ExecuteUpdate;
  if not FVisaoProdutoGradeCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FVisaoProdutoGradeCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FVisaoProdutoGradeCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FVisaoProdutoGradeCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TSMProdutoClient.InserirProdutoParaEmissaoEtiqueta(AIdProduto: Integer);
begin
  if FInserirProdutoParaEmissaoEtiquetaCommand = nil then
  begin
    FInserirProdutoParaEmissaoEtiquetaCommand := FDBXConnection.CreateCommand;
    FInserirProdutoParaEmissaoEtiquetaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FInserirProdutoParaEmissaoEtiquetaCommand.Text := 'TSMProduto.InserirProdutoParaEmissaoEtiqueta';
    FInserirProdutoParaEmissaoEtiquetaCommand.Prepare;
  end;
  FInserirProdutoParaEmissaoEtiquetaCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FInserirProdutoParaEmissaoEtiquetaCommand.ExecuteUpdate;
end;

procedure TSMProdutoClient.LimparRegistrosDaEmissaoEtiqueta;
begin
  if FLimparRegistrosDaEmissaoEtiquetaCommand = nil then
  begin
    FLimparRegistrosDaEmissaoEtiquetaCommand := FDBXConnection.CreateCommand;
    FLimparRegistrosDaEmissaoEtiquetaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FLimparRegistrosDaEmissaoEtiquetaCommand.Text := 'TSMProduto.LimparRegistrosDaEmissaoEtiqueta';
    FLimparRegistrosDaEmissaoEtiquetaCommand.Prepare;
  end;
  FLimparRegistrosDaEmissaoEtiquetaCommand.ExecuteUpdate;
end;

function TSMProdutoClient.GetProdutoFiscal(AIdProduto: Integer; AIdPessoa: Integer; AFilial: Integer): TProdutoFiscalProxy;
begin
  if FGetProdutoFiscalCommand = nil then
  begin
    FGetProdutoFiscalCommand := FDBXConnection.CreateCommand;
    FGetProdutoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoFiscalCommand.Text := 'TSMProduto.GetProdutoFiscal';
    FGetProdutoFiscalCommand.Prepare;
  end;
  FGetProdutoFiscalCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FGetProdutoFiscalCommand.Parameters[1].Value.SetInt32(AIdPessoa);
  FGetProdutoFiscalCommand.Parameters[2].Value.SetInt32(AFilial);
  FGetProdutoFiscalCommand.ExecuteUpdate;
  if not FGetProdutoFiscalCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutoFiscalCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TProdutoFiscalProxy(FUnMarshal.UnMarshal(FGetProdutoFiscalCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutoFiscalCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMProdutoClient.VerificarTipoMercadoria(AIdProduto: Integer): Boolean;
begin
  if FVerificarTipoMercadoriaCommand = nil then
  begin
    FVerificarTipoMercadoriaCommand := FDBXConnection.CreateCommand;
    FVerificarTipoMercadoriaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FVerificarTipoMercadoriaCommand.Text := 'TSMProduto.VerificarTipoMercadoria';
    FVerificarTipoMercadoriaCommand.Prepare;
  end;
  FVerificarTipoMercadoriaCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FVerificarTipoMercadoriaCommand.ExecuteUpdate;
  Result := FVerificarTipoMercadoriaCommand.Parameters[1].Value.GetBoolean;
end;

function TSMProdutoClient.VerificarTipoProduto(AIdProduto: Integer): Boolean;
begin
  if FVerificarTipoProdutoCommand = nil then
  begin
    FVerificarTipoProdutoCommand := FDBXConnection.CreateCommand;
    FVerificarTipoProdutoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FVerificarTipoProdutoCommand.Text := 'TSMProduto.VerificarTipoProduto';
    FVerificarTipoProdutoCommand.Prepare;
  end;
  FVerificarTipoProdutoCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FVerificarTipoProdutoCommand.ExecuteUpdate;
  Result := FVerificarTipoProdutoCommand.Parameters[1].Value.GetBoolean;
end;

procedure TSMProdutoClient.RefazerEAN13TodosProdutos;
begin
  if FRefazerEAN13TodosProdutosCommand = nil then
  begin
    FRefazerEAN13TodosProdutosCommand := FDBXConnection.CreateCommand;
    FRefazerEAN13TodosProdutosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRefazerEAN13TodosProdutosCommand.Text := 'TSMProduto.RefazerEAN13TodosProdutos';
    FRefazerEAN13TodosProdutosCommand.Prepare;
  end;
  FRefazerEAN13TodosProdutosCommand.ExecuteUpdate;
end;


constructor TSMProdutoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMProdutoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMProdutoClient.Destroy;
begin
  FfdqProdutoAfterPostCommand.DisposeOf;
  FGerarMovimentacaoProdutoCommand.DisposeOf;
  FGetChaveProcessoMovimentacaoInicialCommand.DisposeOf;
  FRemoverMovimentacaoProdutoCommand.DisposeOf;
  FGetProximoIDCommand.DisposeOf;
  FCodigoDeBarraJaCadastradoCommand.DisposeOf;
  FGetProdutoCommand.DisposeOf;
  FGetTodosGruposProdutoCommand.DisposeOf;
  FGetProdutoPeloCodigoBarraCommand.DisposeOf;
  FGetProdutosCommand.DisposeOf;
  FGetProdutosCompletoCommand.DisposeOf;
  FGetProdutoFilialCommand.DisposeOf;
  FGetProdutoFilialProxyCommand.DisposeOf;
  FGetListaProdutoCommand.DisposeOf;
  FExisteMovimentacoesDiferentesDaMovimentacaoInicialCommand.DisposeOf;
  FDuplicarProdutoCommand.DisposeOf;
  FGerarCodigoBarraEAN13Command.DisposeOf;
  FGerarProdutosCommand.DisposeOf;
  FVisaoProdutoGradeCommand.DisposeOf;
  FInserirProdutoParaEmissaoEtiquetaCommand.DisposeOf;
  FLimparRegistrosDaEmissaoEtiquetaCommand.DisposeOf;
  FGetProdutoFiscalCommand.DisposeOf;
  FVerificarTipoMercadoriaCommand.DisposeOf;
  FVerificarTipoProdutoCommand.DisposeOf;
  FRefazerEAN13TodosProdutosCommand.DisposeOf;
  inherited;
end;

function TSMChaveProcessoClient.NovaChaveProcesso(AOrigem: string; AIdOrigem: Integer): Integer;
begin
  if FNovaChaveProcessoCommand = nil then
  begin
    FNovaChaveProcessoCommand := FDBXConnection.CreateCommand;
    FNovaChaveProcessoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FNovaChaveProcessoCommand.Text := 'TSMChaveProcesso.NovaChaveProcesso';
    FNovaChaveProcessoCommand.Prepare;
  end;
  FNovaChaveProcessoCommand.Parameters[0].Value.SetWideString(AOrigem);
  FNovaChaveProcessoCommand.Parameters[1].Value.SetInt32(AIdOrigem);
  FNovaChaveProcessoCommand.ExecuteUpdate;
  Result := FNovaChaveProcessoCommand.Parameters[2].Value.GetInt32;
end;


constructor TSMChaveProcessoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMChaveProcessoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMChaveProcessoClient.Destroy;
begin
  FNovaChaveProcessoCommand.DisposeOf;
  inherited;
end;

function TSMEmpresaFilialClient.GetFantasia(AIdFilial: Integer): string;
begin
  if FGetFantasiaCommand = nil then
  begin
    FGetFantasiaCommand := FDBXConnection.CreateCommand;
    FGetFantasiaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetFantasiaCommand.Text := 'TSMEmpresaFilial.GetFantasia';
    FGetFantasiaCommand.Prepare;
  end;
  FGetFantasiaCommand.Parameters[0].Value.SetInt32(AIdFilial);
  FGetFantasiaCommand.ExecuteUpdate;
  Result := FGetFantasiaCommand.Parameters[1].Value.GetWideString;
end;

function TSMEmpresaFilialClient.OptanteSimplesNacional(AIdFilial: Integer): Boolean;
begin
  if FOptanteSimplesNacionalCommand = nil then
  begin
    FOptanteSimplesNacionalCommand := FDBXConnection.CreateCommand;
    FOptanteSimplesNacionalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FOptanteSimplesNacionalCommand.Text := 'TSMEmpresaFilial.OptanteSimplesNacional';
    FOptanteSimplesNacionalCommand.Prepare;
  end;
  FOptanteSimplesNacionalCommand.Parameters[0].Value.SetInt32(AIdFilial);
  FOptanteSimplesNacionalCommand.ExecuteUpdate;
  Result := FOptanteSimplesNacionalCommand.Parameters[1].Value.GetBoolean;
end;

function TSMEmpresaFilialClient.GetCRT(AIdFilial: Integer): Integer;
begin
  if FGetCRTCommand = nil then
  begin
    FGetCRTCommand := FDBXConnection.CreateCommand;
    FGetCRTCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCRTCommand.Text := 'TSMEmpresaFilial.GetCRT';
    FGetCRTCommand.Prepare;
  end;
  FGetCRTCommand.Parameters[0].Value.SetInt32(AIdFilial);
  FGetCRTCommand.ExecuteUpdate;
  Result := FGetCRTCommand.Parameters[1].Value.GetInt32;
end;

function TSMEmpresaFilialClient.GetFilial(AIdFilial: Integer): TFilialProxy;
begin
  if FGetFilialCommand = nil then
  begin
    FGetFilialCommand := FDBXConnection.CreateCommand;
    FGetFilialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetFilialCommand.Text := 'TSMEmpresaFilial.GetFilial';
    FGetFilialCommand.Prepare;
  end;
  FGetFilialCommand.Parameters[0].Value.SetInt32(AIdFilial);
  FGetFilialCommand.ExecuteUpdate;
  if not FGetFilialCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetFilialCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFilialProxy(FUnMarshal.UnMarshal(FGetFilialCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetFilialCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMEmpresaFilialClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMEmpresaFilialClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMEmpresaFilialClient.Destroy;
begin
  FGetFantasiaCommand.DisposeOf;
  FOptanteSimplesNacionalCommand.DisposeOf;
  FGetCRTCommand.DisposeOf;
  FGetFilialCommand.DisposeOf;
  inherited;
end;

function TSMNotaFiscalClient.GerarContaReceber(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
begin
  if FGerarContaReceberCommand = nil then
  begin
    FGerarContaReceberCommand := FDBXConnection.CreateCommand;
    FGerarContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarContaReceberCommand.Text := 'TSMNotaFiscal.GerarContaReceber';
    FGerarContaReceberCommand.Prepare;
  end;
  FGerarContaReceberCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  if not Assigned(ANotaFiscalTransienteProxy) then
    FGerarContaReceberCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarContaReceberCommand.Parameters[1].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarContaReceberCommand.Parameters[1].Value.SetJSONValue(FMarshal.Marshal(ANotaFiscalTransienteProxy), True);
      if FInstanceOwner then
        ANotaFiscalTransienteProxy.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarContaReceberCommand.ExecuteUpdate;
  Result := FGerarContaReceberCommand.Parameters[2].Value.GetBoolean;
end;

function TSMNotaFiscalClient.GerarContaPagar(AIdChaveProcesso: Integer): Boolean;
begin
  if FGerarContaPagarCommand = nil then
  begin
    FGerarContaPagarCommand := FDBXConnection.CreateCommand;
    FGerarContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarContaPagarCommand.Text := 'TSMNotaFiscal.GerarContaPagar';
    FGerarContaPagarCommand.Prepare;
  end;
  FGerarContaPagarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGerarContaPagarCommand.ExecuteUpdate;
  Result := FGerarContaPagarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.EstornarContaReceber(AIdChaveProcesso: Integer): Boolean;
begin
  if FEstornarContaReceberCommand = nil then
  begin
    FEstornarContaReceberCommand := FDBXConnection.CreateCommand;
    FEstornarContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstornarContaReceberCommand.Text := 'TSMNotaFiscal.EstornarContaReceber';
    FEstornarContaReceberCommand.Prepare;
  end;
  FEstornarContaReceberCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEstornarContaReceberCommand.ExecuteUpdate;
  Result := FEstornarContaReceberCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.EstornarContaPagar(AIdChaveProcesso: Integer): Boolean;
begin
  if FEstornarContaPagarCommand = nil then
  begin
    FEstornarContaPagarCommand := FDBXConnection.CreateCommand;
    FEstornarContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstornarContaPagarCommand.Text := 'TSMNotaFiscal.EstornarContaPagar';
    FEstornarContaPagarCommand.Prepare;
  end;
  FEstornarContaPagarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEstornarContaPagarCommand.ExecuteUpdate;
  Result := FEstornarContaPagarCommand.Parameters[1].Value.GetBoolean;
end;

procedure TSMNotaFiscalClient.AtualizarStatus(AIdChaveProcesso: Integer; AStatus: string);
begin
  if FAtualizarStatusCommand = nil then
  begin
    FAtualizarStatusCommand := FDBXConnection.CreateCommand;
    FAtualizarStatusCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarStatusCommand.Text := 'TSMNotaFiscal.AtualizarStatus';
    FAtualizarStatusCommand.Prepare;
  end;
  FAtualizarStatusCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FAtualizarStatusCommand.Parameters[1].Value.SetWideString(AStatus);
  FAtualizarStatusCommand.ExecuteUpdate;
end;

function TSMNotaFiscalClient.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeExcluirCommand = nil then
  begin
    FPodeExcluirCommand := FDBXConnection.CreateCommand;
    FPodeExcluirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeExcluirCommand.Text := 'TSMNotaFiscal.PodeExcluir';
    FPodeExcluirCommand.Prepare;
  end;
  FPodeExcluirCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeExcluirCommand.ExecuteUpdate;
  Result := FPodeExcluirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeEstornarCommand = nil then
  begin
    FPodeEstornarCommand := FDBXConnection.CreateCommand;
    FPodeEstornarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeEstornarCommand.Text := 'TSMNotaFiscal.PodeEstornar';
    FPodeEstornarCommand.Prepare;
  end;
  FPodeEstornarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeEstornarCommand.ExecuteUpdate;
  Result := FPodeEstornarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.Efetivar(AIdChaveProcesso: Integer; ANotaFiscalTransienteProxy: TNotaFiscalTransienteProxy): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMNotaFiscal.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  if not Assigned(ANotaFiscalTransienteProxy) then
    FEfetivarCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FEfetivarCommand.Parameters[1].ConnectionHandler).GetJSONMarshaler;
    try
      FEfetivarCommand.Parameters[1].Value.SetJSONValue(FMarshal.Marshal(ANotaFiscalTransienteProxy), True);
      if FInstanceOwner then
        ANotaFiscalTransienteProxy.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[2].Value.GetBoolean;
end;

function TSMNotaFiscalClient.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  if FCancelarCommand = nil then
  begin
    FCancelarCommand := FDBXConnection.CreateCommand;
    FCancelarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarCommand.Text := 'TSMNotaFiscal.Cancelar';
    FCancelarCommand.Prepare;
  end;
  FCancelarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FCancelarCommand.ExecuteUpdate;
  Result := FCancelarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.GerarNotaFiscal(ANotaFiscal: string): Integer;
begin
  if FGerarNotaFiscalCommand = nil then
  begin
    FGerarNotaFiscalCommand := FDBXConnection.CreateCommand;
    FGerarNotaFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarNotaFiscalCommand.Text := 'TSMNotaFiscal.GerarNotaFiscal';
    FGerarNotaFiscalCommand.Prepare;
  end;
  FGerarNotaFiscalCommand.Parameters[0].Value.SetWideString(ANotaFiscal);
  FGerarNotaFiscalCommand.ExecuteUpdate;
  Result := FGerarNotaFiscalCommand.Parameters[1].Value.GetInt32;
end;

function TSMNotaFiscalClient.EmitirNFE(AIdNotaFiscal: Integer; ATipoDocumentoFiscal: string): string;
begin
  if FEmitirNFECommand = nil then
  begin
    FEmitirNFECommand := FDBXConnection.CreateCommand;
    FEmitirNFECommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEmitirNFECommand.Text := 'TSMNotaFiscal.EmitirNFE';
    FEmitirNFECommand.Prepare;
  end;
  FEmitirNFECommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FEmitirNFECommand.Parameters[1].Value.SetWideString(ATipoDocumentoFiscal);
  FEmitirNFECommand.ExecuteUpdate;
  Result := FEmitirNFECommand.Parameters[2].Value.GetWideString;
end;

function TSMNotaFiscalClient.NFEEmitida(AIdNotaFiscal: Integer): Boolean;
begin
  if FNFEEmitidaCommand = nil then
  begin
    FNFEEmitidaCommand := FDBXConnection.CreateCommand;
    FNFEEmitidaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FNFEEmitidaCommand.Text := 'TSMNotaFiscal.NFEEmitida';
    FNFEEmitidaCommand.Prepare;
  end;
  FNFEEmitidaCommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FNFEEmitidaCommand.ExecuteUpdate;
  Result := FNFEEmitidaCommand.Parameters[1].Value.GetBoolean;
end;

function TSMNotaFiscalClient.GerarNotaFiscalDaVenda(AId: Integer; ACPFNaNota: string; ATipoDocumentoFiscal: string; AVendaProxy: TVendaProxy): Boolean;
begin
  if FGerarNotaFiscalDaVendaCommand = nil then
  begin
    FGerarNotaFiscalDaVendaCommand := FDBXConnection.CreateCommand;
    FGerarNotaFiscalDaVendaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarNotaFiscalDaVendaCommand.Text := 'TSMNotaFiscal.GerarNotaFiscalDaVenda';
    FGerarNotaFiscalDaVendaCommand.Prepare;
  end;
  FGerarNotaFiscalDaVendaCommand.Parameters[0].Value.SetInt32(AId);
  FGerarNotaFiscalDaVendaCommand.Parameters[1].Value.SetWideString(ACPFNaNota);
  FGerarNotaFiscalDaVendaCommand.Parameters[2].Value.SetWideString(ATipoDocumentoFiscal);
  if not Assigned(AVendaProxy) then
    FGerarNotaFiscalDaVendaCommand.Parameters[3].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarNotaFiscalDaVendaCommand.Parameters[3].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarNotaFiscalDaVendaCommand.Parameters[3].Value.SetJSONValue(FMarshal.Marshal(AVendaProxy), True);
      if FInstanceOwner then
        AVendaProxy.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarNotaFiscalDaVendaCommand.ExecuteUpdate;
  Result := FGerarNotaFiscalDaVendaCommand.Parameters[4].Value.GetBoolean;
end;

function TSMNotaFiscalClient.GerarNotaFiscalDeVendaAgrupada(AIdsVendas: string; ACPFNaNota: string; ATipoDocumentoFiscal: string; AVendaProxy: TVendaProxy): Boolean;
begin
  if FGerarNotaFiscalDeVendaAgrupadaCommand = nil then
  begin
    FGerarNotaFiscalDeVendaAgrupadaCommand := FDBXConnection.CreateCommand;
    FGerarNotaFiscalDeVendaAgrupadaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarNotaFiscalDeVendaAgrupadaCommand.Text := 'TSMNotaFiscal.GerarNotaFiscalDeVendaAgrupada';
    FGerarNotaFiscalDeVendaAgrupadaCommand.Prepare;
  end;
  FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[0].Value.SetWideString(AIdsVendas);
  FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[1].Value.SetWideString(ACPFNaNota);
  FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[2].Value.SetWideString(ATipoDocumentoFiscal);
  if not Assigned(AVendaProxy) then
    FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[3].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[3].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[3].Value.SetJSONValue(FMarshal.Marshal(AVendaProxy), True);
      if FInstanceOwner then
        AVendaProxy.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarNotaFiscalDeVendaAgrupadaCommand.ExecuteUpdate;
  Result := FGerarNotaFiscalDeVendaAgrupadaCommand.Parameters[4].Value.GetBoolean;
end;

function TSMNotaFiscalClient.GerarNotaFiscalDaOrdemServico(AIdChaveProcesso: Integer; ACPFNaNota: string; ATipoDocumentoFiscal: string): Boolean;
begin
  if FGerarNotaFiscalDaOrdemServicoCommand = nil then
  begin
    FGerarNotaFiscalDaOrdemServicoCommand := FDBXConnection.CreateCommand;
    FGerarNotaFiscalDaOrdemServicoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarNotaFiscalDaOrdemServicoCommand.Text := 'TSMNotaFiscal.GerarNotaFiscalDaOrdemServico';
    FGerarNotaFiscalDaOrdemServicoCommand.Prepare;
  end;
  FGerarNotaFiscalDaOrdemServicoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGerarNotaFiscalDaOrdemServicoCommand.Parameters[1].Value.SetWideString(ACPFNaNota);
  FGerarNotaFiscalDaOrdemServicoCommand.Parameters[2].Value.SetWideString(ATipoDocumentoFiscal);
  FGerarNotaFiscalDaOrdemServicoCommand.ExecuteUpdate;
  Result := FGerarNotaFiscalDaOrdemServicoCommand.Parameters[3].Value.GetBoolean;
end;

procedure TSMNotaFiscalClient.AtualizarCPFNaNota(AIdNotaFiscal: Integer; ACPF: string);
begin
  if FAtualizarCPFNaNotaCommand = nil then
  begin
    FAtualizarCPFNaNotaCommand := FDBXConnection.CreateCommand;
    FAtualizarCPFNaNotaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarCPFNaNotaCommand.Text := 'TSMNotaFiscal.AtualizarCPFNaNota';
    FAtualizarCPFNaNotaCommand.Prepare;
  end;
  FAtualizarCPFNaNotaCommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FAtualizarCPFNaNotaCommand.Parameters[1].Value.SetWideString(ACPF);
  FAtualizarCPFNaNotaCommand.ExecuteUpdate;
end;

function TSMNotaFiscalClient.ImprimirDocumentoFiscal(AIdNotaFiscal: Integer; ATipoDocumentoFiscal: string): string;
begin
  if FImprimirDocumentoFiscalCommand = nil then
  begin
    FImprimirDocumentoFiscalCommand := FDBXConnection.CreateCommand;
    FImprimirDocumentoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FImprimirDocumentoFiscalCommand.Text := 'TSMNotaFiscal.ImprimirDocumentoFiscal';
    FImprimirDocumentoFiscalCommand.Prepare;
  end;
  FImprimirDocumentoFiscalCommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FImprimirDocumentoFiscalCommand.Parameters[1].Value.SetWideString(ATipoDocumentoFiscal);
  FImprimirDocumentoFiscalCommand.ExecuteUpdate;
  Result := FImprimirDocumentoFiscalCommand.Parameters[2].Value.GetWideString;
end;

function TSMNotaFiscalClient.ChecarServidorAtivo(AIdFilial: Integer; AIdUsuario: Integer): string;
begin
  if FChecarServidorAtivoCommand = nil then
  begin
    FChecarServidorAtivoCommand := FDBXConnection.CreateCommand;
    FChecarServidorAtivoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FChecarServidorAtivoCommand.Text := 'TSMNotaFiscal.ChecarServidorAtivo';
    FChecarServidorAtivoCommand.Prepare;
  end;
  FChecarServidorAtivoCommand.Parameters[0].Value.SetInt32(AIdFilial);
  FChecarServidorAtivoCommand.Parameters[1].Value.SetInt32(AIdUsuario);
  FChecarServidorAtivoCommand.ExecuteUpdate;
  Result := FChecarServidorAtivoCommand.Parameters[2].Value.GetWideString;
end;

function TSMNotaFiscalClient.GetModeloNotaFiscal(AIdModeloNota: Integer): string;
begin
  if FGetModeloNotaFiscalCommand = nil then
  begin
    FGetModeloNotaFiscalCommand := FDBXConnection.CreateCommand;
    FGetModeloNotaFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetModeloNotaFiscalCommand.Text := 'TSMNotaFiscal.GetModeloNotaFiscal';
    FGetModeloNotaFiscalCommand.Prepare;
  end;
  FGetModeloNotaFiscalCommand.Parameters[0].Value.SetInt32(AIdModeloNota);
  FGetModeloNotaFiscalCommand.ExecuteUpdate;
  Result := FGetModeloNotaFiscalCommand.Parameters[1].Value.GetWideString;
end;

function TSMNotaFiscalClient.GetNumeroDocumentoNotaFiscalSaida(ATipoDocumentoFiscal: string): Integer;
begin
  if FGetNumeroDocumentoNotaFiscalSaidaCommand = nil then
  begin
    FGetNumeroDocumentoNotaFiscalSaidaCommand := FDBXConnection.CreateCommand;
    FGetNumeroDocumentoNotaFiscalSaidaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetNumeroDocumentoNotaFiscalSaidaCommand.Text := 'TSMNotaFiscal.GetNumeroDocumentoNotaFiscalSaida';
    FGetNumeroDocumentoNotaFiscalSaidaCommand.Prepare;
  end;
  FGetNumeroDocumentoNotaFiscalSaidaCommand.Parameters[0].Value.SetWideString(ATipoDocumentoFiscal);
  FGetNumeroDocumentoNotaFiscalSaidaCommand.ExecuteUpdate;
  Result := FGetNumeroDocumentoNotaFiscalSaidaCommand.Parameters[1].Value.GetInt32;
end;

function TSMNotaFiscalClient.GetSerieNotaFiscalSaida(ATipoDocumentoFiscal: string): Integer;
begin
  if FGetSerieNotaFiscalSaidaCommand = nil then
  begin
    FGetSerieNotaFiscalSaidaCommand := FDBXConnection.CreateCommand;
    FGetSerieNotaFiscalSaidaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetSerieNotaFiscalSaidaCommand.Text := 'TSMNotaFiscal.GetSerieNotaFiscalSaida';
    FGetSerieNotaFiscalSaidaCommand.Prepare;
  end;
  FGetSerieNotaFiscalSaidaCommand.Parameters[0].Value.SetWideString(ATipoDocumentoFiscal);
  FGetSerieNotaFiscalSaidaCommand.ExecuteUpdate;
  Result := FGetSerieNotaFiscalSaidaCommand.Parameters[1].Value.GetInt32;
end;

function TSMNotaFiscalClient.CancelarDocumentoFiscal(AIdNotaFiscal: Integer; AMotivoCancelamento: string; ATipoDocumentoFiscal: string): Boolean;
begin
  if FCancelarDocumentoFiscalCommand = nil then
  begin
    FCancelarDocumentoFiscalCommand := FDBXConnection.CreateCommand;
    FCancelarDocumentoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarDocumentoFiscalCommand.Text := 'TSMNotaFiscal.CancelarDocumentoFiscal';
    FCancelarDocumentoFiscalCommand.Prepare;
  end;
  FCancelarDocumentoFiscalCommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FCancelarDocumentoFiscalCommand.Parameters[1].Value.SetWideString(AMotivoCancelamento);
  FCancelarDocumentoFiscalCommand.Parameters[2].Value.SetWideString(ATipoDocumentoFiscal);
  FCancelarDocumentoFiscalCommand.ExecuteUpdate;
  Result := FCancelarDocumentoFiscalCommand.Parameters[3].Value.GetBoolean;
end;

procedure TSMNotaFiscalClient.InutilizarDocumentoFiscal(AIdsNotaFiscal: string);
begin
  if FInutilizarDocumentoFiscalCommand = nil then
  begin
    FInutilizarDocumentoFiscalCommand := FDBXConnection.CreateCommand;
    FInutilizarDocumentoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FInutilizarDocumentoFiscalCommand.Text := 'TSMNotaFiscal.InutilizarDocumentoFiscal';
    FInutilizarDocumentoFiscalCommand.Prepare;
  end;
  FInutilizarDocumentoFiscalCommand.Parameters[0].Value.SetWideString(AIdsNotaFiscal);
  FInutilizarDocumentoFiscalCommand.ExecuteUpdate;
end;

function TSMNotaFiscalClient.BuscarXML(AIdNotaFiscal: Integer): string;
begin
  if FBuscarXMLCommand = nil then
  begin
    FBuscarXMLCommand := FDBXConnection.CreateCommand;
    FBuscarXMLCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarXMLCommand.Text := 'TSMNotaFiscal.BuscarXML';
    FBuscarXMLCommand.Prepare;
  end;
  FBuscarXMLCommand.Parameters[0].Value.SetInt32(AIdNotaFiscal);
  FBuscarXMLCommand.ExecuteUpdate;
  Result := FBuscarXMLCommand.Parameters[1].Value.GetWideString;
end;

function TSMNotaFiscalClient.BuscarNotasFiscaisParaInutilizacao(AIdFIlial: Integer; AMes: Integer; AAno: Integer): TFDJSONDataSets;
begin
  if FBuscarNotasFiscaisParaInutilizacaoCommand = nil then
  begin
    FBuscarNotasFiscaisParaInutilizacaoCommand := FDBXConnection.CreateCommand;
    FBuscarNotasFiscaisParaInutilizacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarNotasFiscaisParaInutilizacaoCommand.Text := 'TSMNotaFiscal.BuscarNotasFiscaisParaInutilizacao';
    FBuscarNotasFiscaisParaInutilizacaoCommand.Prepare;
  end;
  FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[0].Value.SetInt32(AIdFIlial);
  FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[1].Value.SetInt32(AMes);
  FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[2].Value.SetInt32(AAno);
  FBuscarNotasFiscaisParaInutilizacaoCommand.ExecuteUpdate;
  if not FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscarNotasFiscaisParaInutilizacaoCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscarNotasFiscaisParaInutilizacaoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMNotaFiscalClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMNotaFiscalClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMNotaFiscalClient.Destroy;
begin
  FGerarContaReceberCommand.DisposeOf;
  FGerarContaPagarCommand.DisposeOf;
  FEstornarContaReceberCommand.DisposeOf;
  FEstornarContaPagarCommand.DisposeOf;
  FAtualizarStatusCommand.DisposeOf;
  FPodeExcluirCommand.DisposeOf;
  FPodeEstornarCommand.DisposeOf;
  FEfetivarCommand.DisposeOf;
  FCancelarCommand.DisposeOf;
  FGerarNotaFiscalCommand.DisposeOf;
  FEmitirNFECommand.DisposeOf;
  FNFEEmitidaCommand.DisposeOf;
  FGerarNotaFiscalDaVendaCommand.DisposeOf;
  FGerarNotaFiscalDeVendaAgrupadaCommand.DisposeOf;
  FGerarNotaFiscalDaOrdemServicoCommand.DisposeOf;
  FAtualizarCPFNaNotaCommand.DisposeOf;
  FImprimirDocumentoFiscalCommand.DisposeOf;
  FChecarServidorAtivoCommand.DisposeOf;
  FGetModeloNotaFiscalCommand.DisposeOf;
  FGetNumeroDocumentoNotaFiscalSaidaCommand.DisposeOf;
  FGetSerieNotaFiscalSaidaCommand.DisposeOf;
  FCancelarDocumentoFiscalCommand.DisposeOf;
  FInutilizarDocumentoFiscalCommand.DisposeOf;
  FBuscarXMLCommand.DisposeOf;
  FBuscarNotasFiscaisParaInutilizacaoCommand.DisposeOf;
  inherited;
end;

function TSMOperacaoClient.GetNaturezaOperacao(AIdNaturezaOperacao: Integer): TNaturezaOperacaoProxy;
begin
  if FGetNaturezaOperacaoCommand = nil then
  begin
    FGetNaturezaOperacaoCommand := FDBXConnection.CreateCommand;
    FGetNaturezaOperacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetNaturezaOperacaoCommand.Text := 'TSMOperacao.GetNaturezaOperacao';
    FGetNaturezaOperacaoCommand.Prepare;
  end;
  FGetNaturezaOperacaoCommand.Parameters[0].Value.SetInt32(AIdNaturezaOperacao);
  FGetNaturezaOperacaoCommand.ExecuteUpdate;
  if not FGetNaturezaOperacaoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetNaturezaOperacaoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TNaturezaOperacaoProxy(FUnMarshal.UnMarshal(FGetNaturezaOperacaoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetNaturezaOperacaoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMOperacaoClient.GerarNaturezaOperacao(ANaturezaOperacao: TNaturezaOperacaoProxy): Integer;
begin
  if FGerarNaturezaOperacaoCommand = nil then
  begin
    FGerarNaturezaOperacaoCommand := FDBXConnection.CreateCommand;
    FGerarNaturezaOperacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarNaturezaOperacaoCommand.Text := 'TSMOperacao.GerarNaturezaOperacao';
    FGerarNaturezaOperacaoCommand.Prepare;
  end;
  if not Assigned(ANaturezaOperacao) then
    FGerarNaturezaOperacaoCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarNaturezaOperacaoCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarNaturezaOperacaoCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ANaturezaOperacao), True);
      if FInstanceOwner then
        ANaturezaOperacao.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarNaturezaOperacaoCommand.ExecuteUpdate;
  Result := FGerarNaturezaOperacaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMOperacaoClient.ExisteNaturezaOperacao(ADescricaoNaturezaOperacao: string): Boolean;
begin
  if FExisteNaturezaOperacaoCommand = nil then
  begin
    FExisteNaturezaOperacaoCommand := FDBXConnection.CreateCommand;
    FExisteNaturezaOperacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteNaturezaOperacaoCommand.Text := 'TSMOperacao.ExisteNaturezaOperacao';
    FExisteNaturezaOperacaoCommand.Prepare;
  end;
  FExisteNaturezaOperacaoCommand.Parameters[0].Value.SetWideString(ADescricaoNaturezaOperacao);
  FExisteNaturezaOperacaoCommand.ExecuteUpdate;
  Result := FExisteNaturezaOperacaoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMOperacaoClient.GetIdNaturezaOperacaoPorDescricao(ADescricaoNaturezaOperacao: string): Integer;
begin
  if FGetIdNaturezaOperacaoPorDescricaoCommand = nil then
  begin
    FGetIdNaturezaOperacaoPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdNaturezaOperacaoPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdNaturezaOperacaoPorDescricaoCommand.Text := 'TSMOperacao.GetIdNaturezaOperacaoPorDescricao';
    FGetIdNaturezaOperacaoPorDescricaoCommand.Prepare;
  end;
  FGetIdNaturezaOperacaoPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoNaturezaOperacao);
  FGetIdNaturezaOperacaoPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdNaturezaOperacaoPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMOperacaoClient.GetDescricaoNaturezaOperacao(AIdNaturezaOperacao: Integer): string;
begin
  if FGetDescricaoNaturezaOperacaoCommand = nil then
  begin
    FGetDescricaoNaturezaOperacaoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoNaturezaOperacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoNaturezaOperacaoCommand.Text := 'TSMOperacao.GetDescricaoNaturezaOperacao';
    FGetDescricaoNaturezaOperacaoCommand.Prepare;
  end;
  FGetDescricaoNaturezaOperacaoCommand.Parameters[0].Value.SetInt32(AIdNaturezaOperacao);
  FGetDescricaoNaturezaOperacaoCommand.ExecuteUpdate;
  Result := FGetDescricaoNaturezaOperacaoCommand.Parameters[1].Value.GetWideString;
end;

function TSMOperacaoClient.ExisteAcao(AAcao: string; AIdOperacao: Integer): Boolean;
begin
  if FExisteAcaoCommand = nil then
  begin
    FExisteAcaoCommand := FDBXConnection.CreateCommand;
    FExisteAcaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteAcaoCommand.Text := 'TSMOperacao.ExisteAcao';
    FExisteAcaoCommand.Prepare;
  end;
  FExisteAcaoCommand.Parameters[0].Value.SetWideString(AAcao);
  FExisteAcaoCommand.Parameters[1].Value.SetInt32(AIdOperacao);
  FExisteAcaoCommand.ExecuteUpdate;
  Result := FExisteAcaoCommand.Parameters[2].Value.GetBoolean;
end;


constructor TSMOperacaoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMOperacaoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMOperacaoClient.Destroy;
begin
  FGetNaturezaOperacaoCommand.DisposeOf;
  FGerarNaturezaOperacaoCommand.DisposeOf;
  FExisteNaturezaOperacaoCommand.DisposeOf;
  FGetIdNaturezaOperacaoPorDescricaoCommand.DisposeOf;
  FGetDescricaoNaturezaOperacaoCommand.DisposeOf;
  FExisteAcaoCommand.DisposeOf;
  inherited;
end;

procedure TSMTabelaPrecoClient.AtualizarRegistro(AIdTabelaPreco: Integer; AIdProduto: Integer; AVlCusto: Double; APercLucro: Double; AVlVenda: Double);
begin
  if FAtualizarRegistroCommand = nil then
  begin
    FAtualizarRegistroCommand := FDBXConnection.CreateCommand;
    FAtualizarRegistroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarRegistroCommand.Text := 'TSMTabelaPreco.AtualizarRegistro';
    FAtualizarRegistroCommand.Prepare;
  end;
  FAtualizarRegistroCommand.Parameters[0].Value.SetInt32(AIdTabelaPreco);
  FAtualizarRegistroCommand.Parameters[1].Value.SetInt32(AIdProduto);
  FAtualizarRegistroCommand.Parameters[2].Value.SetDouble(AVlCusto);
  FAtualizarRegistroCommand.Parameters[3].Value.SetDouble(APercLucro);
  FAtualizarRegistroCommand.Parameters[4].Value.SetDouble(AVlVenda);
  FAtualizarRegistroCommand.ExecuteUpdate;
end;

function TSMTabelaPrecoClient.GetTabelaPrecoComMaisProdutos: TFDJSONDataSets;
begin
  if FGetTabelaPrecoComMaisProdutosCommand = nil then
  begin
    FGetTabelaPrecoComMaisProdutosCommand := FDBXConnection.CreateCommand;
    FGetTabelaPrecoComMaisProdutosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTabelaPrecoComMaisProdutosCommand.Text := 'TSMTabelaPreco.GetTabelaPrecoComMaisProdutos';
    FGetTabelaPrecoComMaisProdutosCommand.Prepare;
  end;
  FGetTabelaPrecoComMaisProdutosCommand.ExecuteUpdate;
  if not FGetTabelaPrecoComMaisProdutosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTabelaPrecoComMaisProdutosCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTabelaPrecoComMaisProdutosCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTabelaPrecoComMaisProdutosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTabelaPrecoClient.GetProdutosTabelaPreco(AIdTabelaPreco: Integer): TFDJSONDataSets;
begin
  if FGetProdutosTabelaPrecoCommand = nil then
  begin
    FGetProdutosTabelaPrecoCommand := FDBXConnection.CreateCommand;
    FGetProdutosTabelaPrecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutosTabelaPrecoCommand.Text := 'TSMTabelaPreco.GetProdutosTabelaPreco';
    FGetProdutosTabelaPrecoCommand.Prepare;
  end;
  FGetProdutosTabelaPrecoCommand.Parameters[0].Value.SetInt32(AIdTabelaPreco);
  FGetProdutosTabelaPrecoCommand.ExecuteUpdate;
  if not FGetProdutosTabelaPrecoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutosTabelaPrecoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutosTabelaPrecoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutosTabelaPrecoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTabelaPrecoClient.BuscarProdutoEmSuasTabelasPreco(AIdProduto: Integer): TFDJSONDataSets;
begin
  if FBuscarProdutoEmSuasTabelasPrecoCommand = nil then
  begin
    FBuscarProdutoEmSuasTabelasPrecoCommand := FDBXConnection.CreateCommand;
    FBuscarProdutoEmSuasTabelasPrecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarProdutoEmSuasTabelasPrecoCommand.Text := 'TSMTabelaPreco.BuscarProdutoEmSuasTabelasPreco';
    FBuscarProdutoEmSuasTabelasPrecoCommand.Prepare;
  end;
  FBuscarProdutoEmSuasTabelasPrecoCommand.Parameters[0].Value.SetInt32(AIdProduto);
  FBuscarProdutoEmSuasTabelasPrecoCommand.ExecuteUpdate;
  if not FBuscarProdutoEmSuasTabelasPrecoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FBuscarProdutoEmSuasTabelasPrecoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscarProdutoEmSuasTabelasPrecoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscarProdutoEmSuasTabelasPrecoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTabelaPrecoClient.GetDescricaoTabelaPreco(AID: Integer): string;
begin
  if FGetDescricaoTabelaPrecoCommand = nil then
  begin
    FGetDescricaoTabelaPrecoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoTabelaPrecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoTabelaPrecoCommand.Text := 'TSMTabelaPreco.GetDescricaoTabelaPreco';
    FGetDescricaoTabelaPrecoCommand.Prepare;
  end;
  FGetDescricaoTabelaPrecoCommand.Parameters[0].Value.SetInt32(AID);
  FGetDescricaoTabelaPrecoCommand.ExecuteUpdate;
  Result := FGetDescricaoTabelaPrecoCommand.Parameters[1].Value.GetWideString;
end;

function TSMTabelaPrecoClient.GetProdutoTabelaPreco(AIdTabelaPreco: Integer; AIdProduto: Integer; AIdFilial: Integer): TTabelaPrecoProdutoProxy;
begin
  if FGetProdutoTabelaPrecoCommand = nil then
  begin
    FGetProdutoTabelaPrecoCommand := FDBXConnection.CreateCommand;
    FGetProdutoTabelaPrecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetProdutoTabelaPrecoCommand.Text := 'TSMTabelaPreco.GetProdutoTabelaPreco';
    FGetProdutoTabelaPrecoCommand.Prepare;
  end;
  FGetProdutoTabelaPrecoCommand.Parameters[0].Value.SetInt32(AIdTabelaPreco);
  FGetProdutoTabelaPrecoCommand.Parameters[1].Value.SetInt32(AIdProduto);
  FGetProdutoTabelaPrecoCommand.Parameters[2].Value.SetInt32(AIdFilial);
  FGetProdutoTabelaPrecoCommand.ExecuteUpdate;
  if not FGetProdutoTabelaPrecoCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetProdutoTabelaPrecoCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TTabelaPrecoProdutoProxy(FUnMarshal.UnMarshal(FGetProdutoTabelaPrecoCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutoTabelaPrecoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTabelaPrecoClient.GetTabelaPrecoItem(AIdTabelaPreco: Integer; AIdProduto: Integer): TTabelaPrecoItemProxy;
begin
  if FGetTabelaPrecoItemCommand = nil then
  begin
    FGetTabelaPrecoItemCommand := FDBXConnection.CreateCommand;
    FGetTabelaPrecoItemCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTabelaPrecoItemCommand.Text := 'TSMTabelaPreco.GetTabelaPrecoItem';
    FGetTabelaPrecoItemCommand.Prepare;
  end;
  FGetTabelaPrecoItemCommand.Parameters[0].Value.SetInt32(AIdTabelaPreco);
  FGetTabelaPrecoItemCommand.Parameters[1].Value.SetInt32(AIdProduto);
  FGetTabelaPrecoItemCommand.ExecuteUpdate;
  if not FGetTabelaPrecoItemCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTabelaPrecoItemCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TTabelaPrecoItemProxy(FUnMarshal.UnMarshal(FGetTabelaPrecoItemCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTabelaPrecoItemCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTabelaPrecoClient.ExcluirTabelaDePreco(AIdTabelaPreco: Integer): Boolean;
begin
  if FExcluirTabelaDePrecoCommand = nil then
  begin
    FExcluirTabelaDePrecoCommand := FDBXConnection.CreateCommand;
    FExcluirTabelaDePrecoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExcluirTabelaDePrecoCommand.Text := 'TSMTabelaPreco.ExcluirTabelaDePreco';
    FExcluirTabelaDePrecoCommand.Prepare;
  end;
  FExcluirTabelaDePrecoCommand.Parameters[0].Value.SetInt32(AIdTabelaPreco);
  FExcluirTabelaDePrecoCommand.ExecuteUpdate;
  Result := FExcluirTabelaDePrecoCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMTabelaPrecoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMTabelaPrecoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMTabelaPrecoClient.Destroy;
begin
  FAtualizarRegistroCommand.DisposeOf;
  FGetTabelaPrecoComMaisProdutosCommand.DisposeOf;
  FGetProdutosTabelaPrecoCommand.DisposeOf;
  FBuscarProdutoEmSuasTabelasPrecoCommand.DisposeOf;
  FGetDescricaoTabelaPrecoCommand.DisposeOf;
  FGetProdutoTabelaPrecoCommand.DisposeOf;
  FGetTabelaPrecoItemCommand.DisposeOf;
  FExcluirTabelaDePrecoCommand.DisposeOf;
  inherited;
end;

procedure TSMContaReceberClient.GerarContaReceber(AValue: string);
begin
  if FGerarContaReceberCommand = nil then
  begin
    FGerarContaReceberCommand := FDBXConnection.CreateCommand;
    FGerarContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarContaReceberCommand.Text := 'TSMContaReceber.GerarContaReceber';
    FGerarContaReceberCommand.Prepare;
  end;
  FGerarContaReceberCommand.Parameters[0].Value.SetWideString(AValue);
  FGerarContaReceberCommand.ExecuteUpdate;
end;

function TSMContaReceberClient.RemoverContaReceber(AValue: Integer): Boolean;
begin
  if FRemoverContaReceberCommand = nil then
  begin
    FRemoverContaReceberCommand := FDBXConnection.CreateCommand;
    FRemoverContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverContaReceberCommand.Text := 'TSMContaReceber.RemoverContaReceber';
    FRemoverContaReceberCommand.Prepare;
  end;
  FRemoverContaReceberCommand.Parameters[0].Value.SetInt32(AValue);
  FRemoverContaReceberCommand.ExecuteUpdate;
  Result := FRemoverContaReceberCommand.Parameters[1].Value.GetBoolean;
end;

function TSMContaReceberClient.PodeExcluir(AValue: Integer): Boolean;
begin
  if FPodeExcluirCommand = nil then
  begin
    FPodeExcluirCommand := FDBXConnection.CreateCommand;
    FPodeExcluirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeExcluirCommand.Text := 'TSMContaReceber.PodeExcluir';
    FPodeExcluirCommand.Prepare;
  end;
  FPodeExcluirCommand.Parameters[0].Value.SetInt32(AValue);
  FPodeExcluirCommand.ExecuteUpdate;
  Result := FPodeExcluirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMContaReceberClient.AtualizarMovimentosContaCorrente(AIdContaReceber: Integer; AIdChaveProcesso: Integer; AGerarContraPartida: Boolean): Boolean;
begin
  if FAtualizarMovimentosContaCorrenteCommand = nil then
  begin
    FAtualizarMovimentosContaCorrenteCommand := FDBXConnection.CreateCommand;
    FAtualizarMovimentosContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarMovimentosContaCorrenteCommand.Text := 'TSMContaReceber.AtualizarMovimentosContaCorrente';
    FAtualizarMovimentosContaCorrenteCommand.Prepare;
  end;
  FAtualizarMovimentosContaCorrenteCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FAtualizarMovimentosContaCorrenteCommand.Parameters[1].Value.SetInt32(AIdChaveProcesso);
  FAtualizarMovimentosContaCorrenteCommand.Parameters[2].Value.SetBoolean(AGerarContraPartida);
  FAtualizarMovimentosContaCorrenteCommand.ExecuteUpdate;
  Result := FAtualizarMovimentosContaCorrenteCommand.Parameters[3].Value.GetBoolean;
end;

function TSMContaReceberClient.RemoverMovimentosContaCorrente(AIdContaReceber: Integer; AGerarContraPartida: Boolean): Boolean;
begin
  if FRemoverMovimentosContaCorrenteCommand = nil then
  begin
    FRemoverMovimentosContaCorrenteCommand := FDBXConnection.CreateCommand;
    FRemoverMovimentosContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverMovimentosContaCorrenteCommand.Text := 'TSMContaReceber.RemoverMovimentosContaCorrente';
    FRemoverMovimentosContaCorrenteCommand.Prepare;
  end;
  FRemoverMovimentosContaCorrenteCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FRemoverMovimentosContaCorrenteCommand.Parameters[1].Value.SetBoolean(AGerarContraPartida);
  FRemoverMovimentosContaCorrenteCommand.ExecuteUpdate;
  Result := FRemoverMovimentosContaCorrenteCommand.Parameters[2].Value.GetBoolean;
end;

function TSMContaReceberClient.GetIdContaReceberPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
begin
  if FGetIdContaReceberPeloIdDaQuitacaoCommand = nil then
  begin
    FGetIdContaReceberPeloIdDaQuitacaoCommand := FDBXConnection.CreateCommand;
    FGetIdContaReceberPeloIdDaQuitacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdContaReceberPeloIdDaQuitacaoCommand.Text := 'TSMContaReceber.GetIdContaReceberPeloIdDaQuitacao';
    FGetIdContaReceberPeloIdDaQuitacaoCommand.Prepare;
  end;
  FGetIdContaReceberPeloIdDaQuitacaoCommand.Parameters[0].Value.SetInt32(AIdQuitacao);
  FGetIdContaReceberPeloIdDaQuitacaoCommand.ExecuteUpdate;
  Result := FGetIdContaReceberPeloIdDaQuitacaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMContaReceberClient.GetIdsNovasQuitacoes(AIdUltimaQuitacao: Integer; AIdContaReceber: Integer): TList<System.Integer>;
begin
  if FGetIdsNovasQuitacoesCommand = nil then
  begin
    FGetIdsNovasQuitacoesCommand := FDBXConnection.CreateCommand;
    FGetIdsNovasQuitacoesCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdsNovasQuitacoesCommand.Text := 'TSMContaReceber.GetIdsNovasQuitacoes';
    FGetIdsNovasQuitacoesCommand.Prepare;
  end;
  FGetIdsNovasQuitacoesCommand.Parameters[0].Value.SetInt32(AIdUltimaQuitacao);
  FGetIdsNovasQuitacoesCommand.Parameters[1].Value.SetInt32(AIdContaReceber);
  FGetIdsNovasQuitacoesCommand.ExecuteUpdate;
  if not FGetIdsNovasQuitacoesCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetIdsNovasQuitacoesCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TList<System.Integer>(FUnMarshal.UnMarshal(FGetIdsNovasQuitacoesCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetIdsNovasQuitacoesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMContaReceberClient.GetTotaisContaReceber(AIdsContaReceber: string): TFDJSONDataSets;
begin
  if FGetTotaisContaReceberCommand = nil then
  begin
    FGetTotaisContaReceberCommand := FDBXConnection.CreateCommand;
    FGetTotaisContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotaisContaReceberCommand.Text := 'TSMContaReceber.GetTotaisContaReceber';
    FGetTotaisContaReceberCommand.Prepare;
  end;
  FGetTotaisContaReceberCommand.Parameters[0].Value.SetWideString(AIdsContaReceber);
  FGetTotaisContaReceberCommand.ExecuteUpdate;
  if not FGetTotaisContaReceberCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotaisContaReceberCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotaisContaReceberCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotaisContaReceberCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMContaReceberClient.PessoaPossuiContaReceberEmAberto(AIdPessoa: Integer): Integer;
begin
  if FPessoaPossuiContaReceberEmAbertoCommand = nil then
  begin
    FPessoaPossuiContaReceberEmAbertoCommand := FDBXConnection.CreateCommand;
    FPessoaPossuiContaReceberEmAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPessoaPossuiContaReceberEmAbertoCommand.Text := 'TSMContaReceber.PessoaPossuiContaReceberEmAberto';
    FPessoaPossuiContaReceberEmAbertoCommand.Prepare;
  end;
  FPessoaPossuiContaReceberEmAbertoCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FPessoaPossuiContaReceberEmAbertoCommand.ExecuteUpdate;
  Result := FPessoaPossuiContaReceberEmAbertoCommand.Parameters[1].Value.GetInt32;
end;

function TSMContaReceberClient.ProcessoPossuiContaReceberEmAberto(AIdChaveProcesso: Integer): Integer;
begin
  if FProcessoPossuiContaReceberEmAbertoCommand = nil then
  begin
    FProcessoPossuiContaReceberEmAbertoCommand := FDBXConnection.CreateCommand;
    FProcessoPossuiContaReceberEmAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FProcessoPossuiContaReceberEmAbertoCommand.Text := 'TSMContaReceber.ProcessoPossuiContaReceberEmAberto';
    FProcessoPossuiContaReceberEmAbertoCommand.Prepare;
  end;
  FProcessoPossuiContaReceberEmAbertoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FProcessoPossuiContaReceberEmAbertoCommand.ExecuteUpdate;
  Result := FProcessoPossuiContaReceberEmAbertoCommand.Parameters[1].Value.GetInt32;
end;

function TSMContaReceberClient.GetValorJuros(AIdContaReceber: Integer): Double;
begin
  if FGetValorJurosCommand = nil then
  begin
    FGetValorJurosCommand := FDBXConnection.CreateCommand;
    FGetValorJurosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetValorJurosCommand.Text := 'TSMContaReceber.GetValorJuros';
    FGetValorJurosCommand.Prepare;
  end;
  FGetValorJurosCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FGetValorJurosCommand.ExecuteUpdate;
  Result := FGetValorJurosCommand.Parameters[1].Value.GetDouble;
end;

function TSMContaReceberClient.GetValorMulta(AIdContaReceber: Integer): Double;
begin
  if FGetValorMultaCommand = nil then
  begin
    FGetValorMultaCommand := FDBXConnection.CreateCommand;
    FGetValorMultaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetValorMultaCommand.Text := 'TSMContaReceber.GetValorMulta';
    FGetValorMultaCommand.Prepare;
  end;
  FGetValorMultaCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FGetValorMultaCommand.ExecuteUpdate;
  Result := FGetValorMultaCommand.Parameters[1].Value.GetDouble;
end;

function TSMContaReceberClient.GetValorAberto(AIdContaReceber: Integer): Double;
begin
  if FGetValorAbertoCommand = nil then
  begin
    FGetValorAbertoCommand := FDBXConnection.CreateCommand;
    FGetValorAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetValorAbertoCommand.Text := 'TSMContaReceber.GetValorAberto';
    FGetValorAbertoCommand.Prepare;
  end;
  FGetValorAbertoCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FGetValorAbertoCommand.ExecuteUpdate;
  Result := FGetValorAbertoCommand.Parameters[1].Value.GetDouble;
end;

procedure TSMContaReceberClient.AtualizarDadosBoletoContasReceber(AIdContaReceber: Integer; ABoletoNossoNumero: string; ABoletoLinhaDigitavel: string; ABoletoCodigoBarras: string);
begin
  if FAtualizarDadosBoletoContasReceberCommand = nil then
  begin
    FAtualizarDadosBoletoContasReceberCommand := FDBXConnection.CreateCommand;
    FAtualizarDadosBoletoContasReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarDadosBoletoContasReceberCommand.Text := 'TSMContaReceber.AtualizarDadosBoletoContasReceber';
    FAtualizarDadosBoletoContasReceberCommand.Prepare;
  end;
  FAtualizarDadosBoletoContasReceberCommand.Parameters[0].Value.SetInt32(AIdContaReceber);
  FAtualizarDadosBoletoContasReceberCommand.Parameters[1].Value.SetWideString(ABoletoNossoNumero);
  FAtualizarDadosBoletoContasReceberCommand.Parameters[2].Value.SetWideString(ABoletoLinhaDigitavel);
  FAtualizarDadosBoletoContasReceberCommand.Parameters[3].Value.SetWideString(ABoletoCodigoBarras);
  FAtualizarDadosBoletoContasReceberCommand.ExecuteUpdate;
end;

function TSMContaReceberClient.GetQueryVariosContasReceberJSONDatasets(AIdsContasReceber: string): TFDJSONDataSets;
begin
  if FGetQueryVariosContasReceberJSONDatasetsCommand = nil then
  begin
    FGetQueryVariosContasReceberJSONDatasetsCommand := FDBXConnection.CreateCommand;
    FGetQueryVariosContasReceberJSONDatasetsCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetQueryVariosContasReceberJSONDatasetsCommand.Text := 'TSMContaReceber.GetQueryVariosContasReceberJSONDatasets';
    FGetQueryVariosContasReceberJSONDatasetsCommand.Prepare;
  end;
  FGetQueryVariosContasReceberJSONDatasetsCommand.Parameters[0].Value.SetWideString(AIdsContasReceber);
  FGetQueryVariosContasReceberJSONDatasetsCommand.ExecuteUpdate;
  if not FGetQueryVariosContasReceberJSONDatasetsCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetQueryVariosContasReceberJSONDatasetsCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetQueryVariosContasReceberJSONDatasetsCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetQueryVariosContasReceberJSONDatasetsCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TSMContaReceberClient.CancelarBoletoEmitido(AIdsContaReceber: string);
begin
  if FCancelarBoletoEmitidoCommand = nil then
  begin
    FCancelarBoletoEmitidoCommand := FDBXConnection.CreateCommand;
    FCancelarBoletoEmitidoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarBoletoEmitidoCommand.Text := 'TSMContaReceber.CancelarBoletoEmitido';
    FCancelarBoletoEmitidoCommand.Prepare;
  end;
  FCancelarBoletoEmitidoCommand.Parameters[0].Value.SetWideString(AIdsContaReceber);
  FCancelarBoletoEmitidoCommand.ExecuteUpdate;
end;


constructor TSMContaReceberClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMContaReceberClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMContaReceberClient.Destroy;
begin
  FGerarContaReceberCommand.DisposeOf;
  FRemoverContaReceberCommand.DisposeOf;
  FPodeExcluirCommand.DisposeOf;
  FAtualizarMovimentosContaCorrenteCommand.DisposeOf;
  FRemoverMovimentosContaCorrenteCommand.DisposeOf;
  FGetIdContaReceberPeloIdDaQuitacaoCommand.DisposeOf;
  FGetIdsNovasQuitacoesCommand.DisposeOf;
  FGetTotaisContaReceberCommand.DisposeOf;
  FPessoaPossuiContaReceberEmAbertoCommand.DisposeOf;
  FProcessoPossuiContaReceberEmAbertoCommand.DisposeOf;
  FGetValorJurosCommand.DisposeOf;
  FGetValorMultaCommand.DisposeOf;
  FGetValorAbertoCommand.DisposeOf;
  FAtualizarDadosBoletoContasReceberCommand.DisposeOf;
  FGetQueryVariosContasReceberJSONDatasetsCommand.DisposeOf;
  FCancelarBoletoEmitidoCommand.DisposeOf;
  inherited;
end;

function TSMContaPagarClient.GerarContaPagar(AValue: string): Boolean;
begin
  if FGerarContaPagarCommand = nil then
  begin
    FGerarContaPagarCommand := FDBXConnection.CreateCommand;
    FGerarContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarContaPagarCommand.Text := 'TSMContaPagar.GerarContaPagar';
    FGerarContaPagarCommand.Prepare;
  end;
  FGerarContaPagarCommand.Parameters[0].Value.SetWideString(AValue);
  FGerarContaPagarCommand.ExecuteUpdate;
  Result := FGerarContaPagarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMContaPagarClient.RemoverContaPagar(AValue: Integer): Boolean;
begin
  if FRemoverContaPagarCommand = nil then
  begin
    FRemoverContaPagarCommand := FDBXConnection.CreateCommand;
    FRemoverContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverContaPagarCommand.Text := 'TSMContaPagar.RemoverContaPagar';
    FRemoverContaPagarCommand.Prepare;
  end;
  FRemoverContaPagarCommand.Parameters[0].Value.SetInt32(AValue);
  FRemoverContaPagarCommand.ExecuteUpdate;
  Result := FRemoverContaPagarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMContaPagarClient.PodeRemover(AValue: Integer): Boolean;
begin
  if FPodeRemoverCommand = nil then
  begin
    FPodeRemoverCommand := FDBXConnection.CreateCommand;
    FPodeRemoverCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeRemoverCommand.Text := 'TSMContaPagar.PodeRemover';
    FPodeRemoverCommand.Prepare;
  end;
  FPodeRemoverCommand.Parameters[0].Value.SetInt32(AValue);
  FPodeRemoverCommand.ExecuteUpdate;
  Result := FPodeRemoverCommand.Parameters[1].Value.GetBoolean;
end;

function TSMContaPagarClient.AtualizarMovimentosContaCorrente(AIdContaPagar: Integer; AIdChaveProcesso: Integer; AGerarContraPartida: Boolean): Boolean;
begin
  if FAtualizarMovimentosContaCorrenteCommand = nil then
  begin
    FAtualizarMovimentosContaCorrenteCommand := FDBXConnection.CreateCommand;
    FAtualizarMovimentosContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAtualizarMovimentosContaCorrenteCommand.Text := 'TSMContaPagar.AtualizarMovimentosContaCorrente';
    FAtualizarMovimentosContaCorrenteCommand.Prepare;
  end;
  FAtualizarMovimentosContaCorrenteCommand.Parameters[0].Value.SetInt32(AIdContaPagar);
  FAtualizarMovimentosContaCorrenteCommand.Parameters[1].Value.SetInt32(AIdChaveProcesso);
  FAtualizarMovimentosContaCorrenteCommand.Parameters[2].Value.SetBoolean(AGerarContraPartida);
  FAtualizarMovimentosContaCorrenteCommand.ExecuteUpdate;
  Result := FAtualizarMovimentosContaCorrenteCommand.Parameters[3].Value.GetBoolean;
end;

function TSMContaPagarClient.RemoverMovimentosContaCorrente(AIdContaPagar: Integer; AGerarContraPartida: Boolean): Boolean;
begin
  if FRemoverMovimentosContaCorrenteCommand = nil then
  begin
    FRemoverMovimentosContaCorrenteCommand := FDBXConnection.CreateCommand;
    FRemoverMovimentosContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverMovimentosContaCorrenteCommand.Text := 'TSMContaPagar.RemoverMovimentosContaCorrente';
    FRemoverMovimentosContaCorrenteCommand.Prepare;
  end;
  FRemoverMovimentosContaCorrenteCommand.Parameters[0].Value.SetInt32(AIdContaPagar);
  FRemoverMovimentosContaCorrenteCommand.Parameters[1].Value.SetBoolean(AGerarContraPartida);
  FRemoverMovimentosContaCorrenteCommand.ExecuteUpdate;
  Result := FRemoverMovimentosContaCorrenteCommand.Parameters[2].Value.GetBoolean;
end;

function TSMContaPagarClient.GetIdContaPagarPeloIdDaQuitacao(AIdQuitacao: Integer): Integer;
begin
  if FGetIdContaPagarPeloIdDaQuitacaoCommand = nil then
  begin
    FGetIdContaPagarPeloIdDaQuitacaoCommand := FDBXConnection.CreateCommand;
    FGetIdContaPagarPeloIdDaQuitacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdContaPagarPeloIdDaQuitacaoCommand.Text := 'TSMContaPagar.GetIdContaPagarPeloIdDaQuitacao';
    FGetIdContaPagarPeloIdDaQuitacaoCommand.Prepare;
  end;
  FGetIdContaPagarPeloIdDaQuitacaoCommand.Parameters[0].Value.SetInt32(AIdQuitacao);
  FGetIdContaPagarPeloIdDaQuitacaoCommand.ExecuteUpdate;
  Result := FGetIdContaPagarPeloIdDaQuitacaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMContaPagarClient.GetTotaisContaPagar(AIdsContaPagar: string): TFDJSONDataSets;
begin
  if FGetTotaisContaPagarCommand = nil then
  begin
    FGetTotaisContaPagarCommand := FDBXConnection.CreateCommand;
    FGetTotaisContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotaisContaPagarCommand.Text := 'TSMContaPagar.GetTotaisContaPagar';
    FGetTotaisContaPagarCommand.Prepare;
  end;
  FGetTotaisContaPagarCommand.Parameters[0].Value.SetWideString(AIdsContaPagar);
  FGetTotaisContaPagarCommand.ExecuteUpdate;
  if not FGetTotaisContaPagarCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotaisContaPagarCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotaisContaPagarCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotaisContaPagarCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMContaPagarClient.PessoaPossuiContaPagarEmAberto(AIdPessoa: Integer): Integer;
begin
  if FPessoaPossuiContaPagarEmAbertoCommand = nil then
  begin
    FPessoaPossuiContaPagarEmAbertoCommand := FDBXConnection.CreateCommand;
    FPessoaPossuiContaPagarEmAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPessoaPossuiContaPagarEmAbertoCommand.Text := 'TSMContaPagar.PessoaPossuiContaPagarEmAberto';
    FPessoaPossuiContaPagarEmAbertoCommand.Prepare;
  end;
  FPessoaPossuiContaPagarEmAbertoCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FPessoaPossuiContaPagarEmAbertoCommand.ExecuteUpdate;
  Result := FPessoaPossuiContaPagarEmAbertoCommand.Parameters[1].Value.GetInt32;
end;

function TSMContaPagarClient.ProcessoPossuiContaPagarEmAberto(AIdChaveProcesso: Integer): Integer;
begin
  if FProcessoPossuiContaPagarEmAbertoCommand = nil then
  begin
    FProcessoPossuiContaPagarEmAbertoCommand := FDBXConnection.CreateCommand;
    FProcessoPossuiContaPagarEmAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FProcessoPossuiContaPagarEmAbertoCommand.Text := 'TSMContaPagar.ProcessoPossuiContaPagarEmAberto';
    FProcessoPossuiContaPagarEmAbertoCommand.Prepare;
  end;
  FProcessoPossuiContaPagarEmAbertoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FProcessoPossuiContaPagarEmAbertoCommand.ExecuteUpdate;
  Result := FProcessoPossuiContaPagarEmAbertoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMContaPagarClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMContaPagarClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMContaPagarClient.Destroy;
begin
  FGerarContaPagarCommand.DisposeOf;
  FRemoverContaPagarCommand.DisposeOf;
  FPodeRemoverCommand.DisposeOf;
  FAtualizarMovimentosContaCorrenteCommand.DisposeOf;
  FRemoverMovimentosContaCorrenteCommand.DisposeOf;
  FGetIdContaPagarPeloIdDaQuitacaoCommand.DisposeOf;
  FGetTotaisContaPagarCommand.DisposeOf;
  FPessoaPossuiContaPagarEmAbertoCommand.DisposeOf;
  FProcessoPossuiContaPagarEmAbertoCommand.DisposeOf;
  inherited;
end;

function TSMGeracaoDocumentoClient.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeExcluirCommand = nil then
  begin
    FPodeExcluirCommand := FDBXConnection.CreateCommand;
    FPodeExcluirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeExcluirCommand.Text := 'TSMGeracaoDocumento.PodeExcluir';
    FPodeExcluirCommand.Prepare;
  end;
  FPodeExcluirCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeExcluirCommand.ExecuteUpdate;
  Result := FPodeExcluirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMGeracaoDocumentoClient.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeEstornarCommand = nil then
  begin
    FPodeEstornarCommand := FDBXConnection.CreateCommand;
    FPodeEstornarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeEstornarCommand.Text := 'TSMGeracaoDocumento.PodeEstornar';
    FPodeEstornarCommand.Prepare;
  end;
  FPodeEstornarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeEstornarCommand.ExecuteUpdate;
  Result := FPodeEstornarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMGeracaoDocumentoClient.GerarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: string): Boolean;
begin
  if FGerarDocumentoCommand = nil then
  begin
    FGerarDocumentoCommand := FDBXConnection.CreateCommand;
    FGerarDocumentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarDocumentoCommand.Text := 'TSMGeracaoDocumento.GerarDocumento';
    FGerarDocumentoCommand.Prepare;
  end;
  FGerarDocumentoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGerarDocumentoCommand.Parameters[1].Value.SetWideString(AGeracaoDocumentoProxy);
  FGerarDocumentoCommand.ExecuteUpdate;
  Result := FGerarDocumentoCommand.Parameters[2].Value.GetBoolean;
end;

function TSMGeracaoDocumentoClient.EstornarDocumento(AIdChaveProcesso: Integer; AGeracaoDocumentoProxy: string): Boolean;
begin
  if FEstornarDocumentoCommand = nil then
  begin
    FEstornarDocumentoCommand := FDBXConnection.CreateCommand;
    FEstornarDocumentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstornarDocumentoCommand.Text := 'TSMGeracaoDocumento.EstornarDocumento';
    FEstornarDocumentoCommand.Prepare;
  end;
  FEstornarDocumentoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEstornarDocumentoCommand.Parameters[1].Value.SetWideString(AGeracaoDocumentoProxy);
  FEstornarDocumentoCommand.ExecuteUpdate;
  Result := FEstornarDocumentoCommand.Parameters[2].Value.GetBoolean;
end;

function TSMGeracaoDocumentoClient.GetContasReceberAberto(AIdChaveProcesso: Integer): TFDJSONDataSets;
begin
  if FGetContasReceberAbertoCommand = nil then
  begin
    FGetContasReceberAbertoCommand := FDBXConnection.CreateCommand;
    FGetContasReceberAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasReceberAbertoCommand.Text := 'TSMGeracaoDocumento.GetContasReceberAberto';
    FGetContasReceberAbertoCommand.Prepare;
  end;
  FGetContasReceberAbertoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGetContasReceberAbertoCommand.ExecuteUpdate;
  if not FGetContasReceberAbertoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasReceberAbertoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasReceberAbertoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasReceberAbertoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMGeracaoDocumentoClient.GetContasPagarAberto(AIdChaveProcesso: Integer): TFDJSONDataSets;
begin
  if FGetContasPagarAbertoCommand = nil then
  begin
    FGetContasPagarAbertoCommand := FDBXConnection.CreateCommand;
    FGetContasPagarAbertoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasPagarAbertoCommand.Text := 'TSMGeracaoDocumento.GetContasPagarAberto';
    FGetContasPagarAbertoCommand.Prepare;
  end;
  FGetContasPagarAbertoCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGetContasPagarAbertoCommand.ExecuteUpdate;
  if not FGetContasPagarAbertoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasPagarAbertoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasPagarAbertoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasPagarAbertoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMGeracaoDocumentoClient.Duplicar(AIdGeracaoDocumento: Integer): Integer;
begin
  if FDuplicarCommand = nil then
  begin
    FDuplicarCommand := FDBXConnection.CreateCommand;
    FDuplicarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FDuplicarCommand.Text := 'TSMGeracaoDocumento.Duplicar';
    FDuplicarCommand.Prepare;
  end;
  FDuplicarCommand.Parameters[0].Value.SetInt32(AIdGeracaoDocumento);
  FDuplicarCommand.ExecuteUpdate;
  Result := FDuplicarCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMGeracaoDocumentoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMGeracaoDocumentoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMGeracaoDocumentoClient.Destroy;
begin
  FPodeExcluirCommand.DisposeOf;
  FPodeEstornarCommand.DisposeOf;
  FGerarDocumentoCommand.DisposeOf;
  FEstornarDocumentoCommand.DisposeOf;
  FGetContasReceberAbertoCommand.DisposeOf;
  FGetContasPagarAbertoCommand.DisposeOf;
  FDuplicarCommand.DisposeOf;
  inherited;
end;

function TSMVendaClient.Efetivar(AIdChaveProcesso: Integer; AVendaProxy: string): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMVenda.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarCommand.Parameters[1].Value.SetWideString(AVendaProxy);
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[2].Value.GetBoolean;
end;

function TSMVendaClient.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  if FCancelarCommand = nil then
  begin
    FCancelarCommand := FDBXConnection.CreateCommand;
    FCancelarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarCommand.Text := 'TSMVenda.Cancelar';
    FCancelarCommand.Prepare;
  end;
  FCancelarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FCancelarCommand.ExecuteUpdate;
  Result := FCancelarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMVendaClient.GetId(AIdChaveProcesso: Integer): Integer;
begin
  if FGetIdCommand = nil then
  begin
    FGetIdCommand := FDBXConnection.CreateCommand;
    FGetIdCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdCommand.Text := 'TSMVenda.GetId';
    FGetIdCommand.Prepare;
  end;
  FGetIdCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGetIdCommand.ExecuteUpdate;
  Result := FGetIdCommand.Parameters[1].Value.GetInt32;
end;

function TSMVendaClient.EfetivarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
begin
  if FEfetivarMovimentacaoVendaCondicionalCommand = nil then
  begin
    FEfetivarMovimentacaoVendaCondicionalCommand := FDBXConnection.CreateCommand;
    FEfetivarMovimentacaoVendaCondicionalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarMovimentacaoVendaCondicionalCommand.Text := 'TSMVenda.EfetivarMovimentacaoVendaCondicional';
    FEfetivarMovimentacaoVendaCondicionalCommand.Prepare;
  end;
  FEfetivarMovimentacaoVendaCondicionalCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarMovimentacaoVendaCondicionalCommand.ExecuteUpdate;
  Result := FEfetivarMovimentacaoVendaCondicionalCommand.Parameters[1].Value.GetBoolean;
end;

function TSMVendaClient.CancelarMovimentacaoVendaCondicional(AIdChaveProcesso: Integer): Boolean;
begin
  if FCancelarMovimentacaoVendaCondicionalCommand = nil then
  begin
    FCancelarMovimentacaoVendaCondicionalCommand := FDBXConnection.CreateCommand;
    FCancelarMovimentacaoVendaCondicionalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarMovimentacaoVendaCondicionalCommand.Text := 'TSMVenda.CancelarMovimentacaoVendaCondicional';
    FCancelarMovimentacaoVendaCondicionalCommand.Prepare;
  end;
  FCancelarMovimentacaoVendaCondicionalCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FCancelarMovimentacaoVendaCondicionalCommand.ExecuteUpdate;
  Result := FCancelarMovimentacaoVendaCondicionalCommand.Parameters[1].Value.GetBoolean;
end;

function TSMVendaClient.GetVendaProxy(AIdVenda: Integer): string;
begin
  if FGetVendaProxyCommand = nil then
  begin
    FGetVendaProxyCommand := FDBXConnection.CreateCommand;
    FGetVendaProxyCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetVendaProxyCommand.Text := 'TSMVenda.GetVendaProxy';
    FGetVendaProxyCommand.Prepare;
  end;
  FGetVendaProxyCommand.Parameters[0].Value.SetInt32(AIdVenda);
  FGetVendaProxyCommand.ExecuteUpdate;
  Result := FGetVendaProxyCommand.Parameters[1].Value.GetWideString;
end;

function TSMVendaClient.GetIdNotaFiscalNFCE(AId: Integer): Integer;
begin
  if FGetIdNotaFiscalNFCECommand = nil then
  begin
    FGetIdNotaFiscalNFCECommand := FDBXConnection.CreateCommand;
    FGetIdNotaFiscalNFCECommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdNotaFiscalNFCECommand.Text := 'TSMVenda.GetIdNotaFiscalNFCE';
    FGetIdNotaFiscalNFCECommand.Prepare;
  end;
  FGetIdNotaFiscalNFCECommand.Parameters[0].Value.SetInt32(AId);
  FGetIdNotaFiscalNFCECommand.ExecuteUpdate;
  Result := FGetIdNotaFiscalNFCECommand.Parameters[1].Value.GetInt32;
end;

function TSMVendaClient.GetIdNotaFiscalNFE(AId: Integer): Integer;
begin
  if FGetIdNotaFiscalNFECommand = nil then
  begin
    FGetIdNotaFiscalNFECommand := FDBXConnection.CreateCommand;
    FGetIdNotaFiscalNFECommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdNotaFiscalNFECommand.Text := 'TSMVenda.GetIdNotaFiscalNFE';
    FGetIdNotaFiscalNFECommand.Prepare;
  end;
  FGetIdNotaFiscalNFECommand.Parameters[0].Value.SetInt32(AId);
  FGetIdNotaFiscalNFECommand.ExecuteUpdate;
  Result := FGetIdNotaFiscalNFECommand.Parameters[1].Value.GetInt32;
end;

function TSMVendaClient.GerarDevolucaoEmDinheiroParaCliente(AVendaProxy: TVendaProxy; AValorDevolucao: Double): Boolean;
begin
  if FGerarDevolucaoEmDinheiroParaClienteCommand = nil then
  begin
    FGerarDevolucaoEmDinheiroParaClienteCommand := FDBXConnection.CreateCommand;
    FGerarDevolucaoEmDinheiroParaClienteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarDevolucaoEmDinheiroParaClienteCommand.Text := 'TSMVenda.GerarDevolucaoEmDinheiroParaCliente';
    FGerarDevolucaoEmDinheiroParaClienteCommand.Prepare;
  end;
  if not Assigned(AVendaProxy) then
    FGerarDevolucaoEmDinheiroParaClienteCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarDevolucaoEmDinheiroParaClienteCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarDevolucaoEmDinheiroParaClienteCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AVendaProxy), True);
      if FInstanceOwner then
        AVendaProxy.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarDevolucaoEmDinheiroParaClienteCommand.Parameters[1].Value.SetDouble(AValorDevolucao);
  FGerarDevolucaoEmDinheiroParaClienteCommand.ExecuteUpdate;
  Result := FGerarDevolucaoEmDinheiroParaClienteCommand.Parameters[2].Value.GetBoolean;
end;

function TSMVendaClient.GerarDevolucaoEmCreditoParaCliente(AIdChaveProcesso: Integer; AValorDevolucao: Double): Boolean;
begin
  if FGerarDevolucaoEmCreditoParaClienteCommand = nil then
  begin
    FGerarDevolucaoEmCreditoParaClienteCommand := FDBXConnection.CreateCommand;
    FGerarDevolucaoEmCreditoParaClienteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarDevolucaoEmCreditoParaClienteCommand.Text := 'TSMVenda.GerarDevolucaoEmCreditoParaCliente';
    FGerarDevolucaoEmCreditoParaClienteCommand.Prepare;
  end;
  FGerarDevolucaoEmCreditoParaClienteCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGerarDevolucaoEmCreditoParaClienteCommand.Parameters[1].Value.SetDouble(AValorDevolucao);
  FGerarDevolucaoEmCreditoParaClienteCommand.ExecuteUpdate;
  Result := FGerarDevolucaoEmCreditoParaClienteCommand.Parameters[2].Value.GetBoolean;
end;

function TSMVendaClient.GerarDevolucaoCreditandoNoContasAReceber(AIdChaveProcesso: Integer; AValorDevolucao: Double; AIdsContasReceber: string): Boolean;
begin
  if FGerarDevolucaoCreditandoNoContasAReceberCommand = nil then
  begin
    FGerarDevolucaoCreditandoNoContasAReceberCommand := FDBXConnection.CreateCommand;
    FGerarDevolucaoCreditandoNoContasAReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarDevolucaoCreditandoNoContasAReceberCommand.Text := 'TSMVenda.GerarDevolucaoCreditandoNoContasAReceber';
    FGerarDevolucaoCreditandoNoContasAReceberCommand.Prepare;
  end;
  FGerarDevolucaoCreditandoNoContasAReceberCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGerarDevolucaoCreditandoNoContasAReceberCommand.Parameters[1].Value.SetDouble(AValorDevolucao);
  FGerarDevolucaoCreditandoNoContasAReceberCommand.Parameters[2].Value.SetWideString(AIdsContasReceber);
  FGerarDevolucaoCreditandoNoContasAReceberCommand.ExecuteUpdate;
  Result := FGerarDevolucaoCreditandoNoContasAReceberCommand.Parameters[3].Value.GetBoolean;
end;

function TSMVendaClient.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeEstornarCommand = nil then
  begin
    FPodeEstornarCommand := FDBXConnection.CreateCommand;
    FPodeEstornarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeEstornarCommand.Text := 'TSMVenda.PodeEstornar';
    FPodeEstornarCommand.Prepare;
  end;
  FPodeEstornarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeEstornarCommand.ExecuteUpdate;
  Result := FPodeEstornarCommand.Parameters[1].Value.GetBoolean;
end;

procedure TSMVendaClient.ExcluirVenda(AIdChaveProcesso: Integer);
begin
  if FExcluirVendaCommand = nil then
  begin
    FExcluirVendaCommand := FDBXConnection.CreateCommand;
    FExcluirVendaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExcluirVendaCommand.Text := 'TSMVenda.ExcluirVenda';
    FExcluirVendaCommand.Prepare;
  end;
  FExcluirVendaCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FExcluirVendaCommand.ExecuteUpdate;
end;

function TSMVendaClient.GetTotaisPorTipoVenda(AIdsVenda: string): TFDJSONDataSets;
begin
  if FGetTotaisPorTipoVendaCommand = nil then
  begin
    FGetTotaisPorTipoVendaCommand := FDBXConnection.CreateCommand;
    FGetTotaisPorTipoVendaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotaisPorTipoVendaCommand.Text := 'TSMVenda.GetTotaisPorTipoVenda';
    FGetTotaisPorTipoVendaCommand.Prepare;
  end;
  FGetTotaisPorTipoVendaCommand.Parameters[0].Value.SetWideString(AIdsVenda);
  FGetTotaisPorTipoVendaCommand.ExecuteUpdate;
  if not FGetTotaisPorTipoVendaCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotaisPorTipoVendaCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotaisPorTipoVendaCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotaisPorTipoVendaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMVendaClient.BuscarBloqueiosVenda(AIdVenda: Integer): TFDJSONDataSets;
begin
  if FBuscarBloqueiosVendaCommand = nil then
  begin
    FBuscarBloqueiosVendaCommand := FDBXConnection.CreateCommand;
    FBuscarBloqueiosVendaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarBloqueiosVendaCommand.Text := 'TSMVenda.BuscarBloqueiosVenda';
    FBuscarBloqueiosVendaCommand.Prepare;
  end;
  FBuscarBloqueiosVendaCommand.Parameters[0].Value.SetInt32(AIdVenda);
  FBuscarBloqueiosVendaCommand.ExecuteUpdate;
  if not FBuscarBloqueiosVendaCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FBuscarBloqueiosVendaCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscarBloqueiosVendaCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscarBloqueiosVendaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMVendaClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMVendaClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMVendaClient.Destroy;
begin
  FEfetivarCommand.DisposeOf;
  FCancelarCommand.DisposeOf;
  FGetIdCommand.DisposeOf;
  FEfetivarMovimentacaoVendaCondicionalCommand.DisposeOf;
  FCancelarMovimentacaoVendaCondicionalCommand.DisposeOf;
  FGetVendaProxyCommand.DisposeOf;
  FGetIdNotaFiscalNFCECommand.DisposeOf;
  FGetIdNotaFiscalNFECommand.DisposeOf;
  FGerarDevolucaoEmDinheiroParaClienteCommand.DisposeOf;
  FGerarDevolucaoEmCreditoParaClienteCommand.DisposeOf;
  FGerarDevolucaoCreditandoNoContasAReceberCommand.DisposeOf;
  FPodeEstornarCommand.DisposeOf;
  FExcluirVendaCommand.DisposeOf;
  FGetTotaisPorTipoVendaCommand.DisposeOf;
  FBuscarBloqueiosVendaCommand.DisposeOf;
  inherited;
end;

function TSMRelatorioFRClient.BuscarRelatorio(AIdMaster: Integer; AIdRelatorio: Integer; AWhere: string): TStream;
begin
  if FBuscarRelatorioCommand = nil then
  begin
    FBuscarRelatorioCommand := FDBXConnection.CreateCommand;
    FBuscarRelatorioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarRelatorioCommand.Text := 'TSMRelatorioFR.BuscarRelatorio';
    FBuscarRelatorioCommand.Prepare;
  end;
  FBuscarRelatorioCommand.Parameters[0].Value.SetInt32(AIdMaster);
  FBuscarRelatorioCommand.Parameters[1].Value.SetInt32(AIdRelatorio);
  FBuscarRelatorioCommand.Parameters[2].Value.SetWideString(AWhere);
  FBuscarRelatorioCommand.ExecuteUpdate;
  Result := FBuscarRelatorioCommand.Parameters[3].Value.GetStream(FInstanceOwner);
end;

function TSMRelatorioFRClient.BuscarRelatoriosAssociados(AClasseFormulario: string; AIdUsuario: Integer): TFDJSONDataSets;
begin
  if FBuscarRelatoriosAssociadosCommand = nil then
  begin
    FBuscarRelatoriosAssociadosCommand := FDBXConnection.CreateCommand;
    FBuscarRelatoriosAssociadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarRelatoriosAssociadosCommand.Text := 'TSMRelatorioFR.BuscarRelatoriosAssociados';
    FBuscarRelatoriosAssociadosCommand.Prepare;
  end;
  FBuscarRelatoriosAssociadosCommand.Parameters[0].Value.SetWideString(AClasseFormulario);
  FBuscarRelatoriosAssociadosCommand.Parameters[1].Value.SetInt32(AIdUsuario);
  FBuscarRelatoriosAssociadosCommand.ExecuteUpdate;
  if not FBuscarRelatoriosAssociadosCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FBuscarRelatoriosAssociadosCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscarRelatoriosAssociadosCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscarRelatoriosAssociadosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMRelatorioFRClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMRelatorioFRClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMRelatorioFRClient.Destroy;
begin
  FBuscarRelatorioCommand.DisposeOf;
  FBuscarRelatoriosAssociadosCommand.DisposeOf;
  inherited;
end;

function TSMAjusteEstoqueClient.Efetivar(AIdChaveProcesso: Integer): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMAjusteEstoque.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMAjusteEstoqueClient.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  if FCancelarCommand = nil then
  begin
    FCancelarCommand := FDBXConnection.CreateCommand;
    FCancelarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarCommand.Text := 'TSMAjusteEstoque.Cancelar';
    FCancelarCommand.Prepare;
  end;
  FCancelarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FCancelarCommand.ExecuteUpdate;
  Result := FCancelarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMAjusteEstoqueClient.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  if FReabrirCommand = nil then
  begin
    FReabrirCommand := FDBXConnection.CreateCommand;
    FReabrirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReabrirCommand.Text := 'TSMAjusteEstoque.Reabrir';
    FReabrirCommand.Prepare;
  end;
  FReabrirCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FReabrirCommand.ExecuteUpdate;
  Result := FReabrirCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMAjusteEstoqueClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMAjusteEstoqueClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMAjusteEstoqueClient.Destroy;
begin
  FEfetivarCommand.DisposeOf;
  FCancelarCommand.DisposeOf;
  FReabrirCommand.DisposeOf;
  inherited;
end;


constructor TSMMotivoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMMotivoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMMotivoClient.Destroy;
begin
  inherited;
end;

procedure TSMChequeClient.GerarUtilizacaoCheque(AIdCheque: Integer);
begin
  if FGerarUtilizacaoChequeCommand = nil then
  begin
    FGerarUtilizacaoChequeCommand := FDBXConnection.CreateCommand;
    FGerarUtilizacaoChequeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarUtilizacaoChequeCommand.Text := 'TSMCheque.GerarUtilizacaoCheque';
    FGerarUtilizacaoChequeCommand.Prepare;
  end;
  FGerarUtilizacaoChequeCommand.Parameters[0].Value.SetInt32(AIdCheque);
  FGerarUtilizacaoChequeCommand.ExecuteUpdate;
end;

procedure TSMChequeClient.EstornarUtilizacaoCheque(AIdCheque: Integer);
begin
  if FEstornarUtilizacaoChequeCommand = nil then
  begin
    FEstornarUtilizacaoChequeCommand := FDBXConnection.CreateCommand;
    FEstornarUtilizacaoChequeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstornarUtilizacaoChequeCommand.Text := 'TSMCheque.EstornarUtilizacaoCheque';
    FEstornarUtilizacaoChequeCommand.Prepare;
  end;
  FEstornarUtilizacaoChequeCommand.Parameters[0].Value.SetInt32(AIdCheque);
  FEstornarUtilizacaoChequeCommand.ExecuteUpdate;
end;

function TSMChequeClient.GetCheque(AIdCheque: Integer): string;
begin
  if FGetChequeCommand = nil then
  begin
    FGetChequeCommand := FDBXConnection.CreateCommand;
    FGetChequeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetChequeCommand.Text := 'TSMCheque.GetCheque';
    FGetChequeCommand.Prepare;
  end;
  FGetChequeCommand.Parameters[0].Value.SetInt32(AIdCheque);
  FGetChequeCommand.ExecuteUpdate;
  Result := FGetChequeCommand.Parameters[1].Value.GetWideString;
end;

function TSMChequeClient.ChequeDisponivelParaUtilizacao(AIdCheque: Integer): Boolean;
begin
  if FChequeDisponivelParaUtilizacaoCommand = nil then
  begin
    FChequeDisponivelParaUtilizacaoCommand := FDBXConnection.CreateCommand;
    FChequeDisponivelParaUtilizacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FChequeDisponivelParaUtilizacaoCommand.Text := 'TSMCheque.ChequeDisponivelParaUtilizacao';
    FChequeDisponivelParaUtilizacaoCommand.Prepare;
  end;
  FChequeDisponivelParaUtilizacaoCommand.Parameters[0].Value.SetInt32(AIdCheque);
  FChequeDisponivelParaUtilizacaoCommand.ExecuteUpdate;
  Result := FChequeDisponivelParaUtilizacaoCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMChequeClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMChequeClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMChequeClient.Destroy;
begin
  FGerarUtilizacaoChequeCommand.DisposeOf;
  FEstornarUtilizacaoChequeCommand.DisposeOf;
  FGetChequeCommand.DisposeOf;
  FChequeDisponivelParaUtilizacaoCommand.DisposeOf;
  inherited;
end;

function TSMServicoClient.GetServico(AIdServico: Integer): string;
begin
  if FGetServicoCommand = nil then
  begin
    FGetServicoCommand := FDBXConnection.CreateCommand;
    FGetServicoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetServicoCommand.Text := 'TSMServico.GetServico';
    FGetServicoCommand.Prepare;
  end;
  FGetServicoCommand.Parameters[0].Value.SetInt32(AIdServico);
  FGetServicoCommand.ExecuteUpdate;
  Result := FGetServicoCommand.Parameters[1].Value.GetWideString;
end;

function TSMServicoClient.GetDescricao(AIdServico: Integer): string;
begin
  if FGetDescricaoCommand = nil then
  begin
    FGetDescricaoCommand := FDBXConnection.CreateCommand;
    FGetDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoCommand.Text := 'TSMServico.GetDescricao';
    FGetDescricaoCommand.Prepare;
  end;
  FGetDescricaoCommand.Parameters[0].Value.SetInt32(AIdServico);
  FGetDescricaoCommand.ExecuteUpdate;
  Result := FGetDescricaoCommand.Parameters[1].Value.GetWideString;
end;

function TSMServicoClient.GerarServico(AServico: TServicoProxy): Integer;
begin
  if FGerarServicoCommand = nil then
  begin
    FGerarServicoCommand := FDBXConnection.CreateCommand;
    FGerarServicoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarServicoCommand.Text := 'TSMServico.GerarServico';
    FGerarServicoCommand.Prepare;
  end;
  if not Assigned(AServico) then
    FGerarServicoCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarServicoCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarServicoCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AServico), True);
      if FInstanceOwner then
        AServico.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarServicoCommand.ExecuteUpdate;
  Result := FGerarServicoCommand.Parameters[1].Value.GetInt32;
end;

function TSMServicoClient.ExisteServico(ADescricaoServico: string): Boolean;
begin
  if FExisteServicoCommand = nil then
  begin
    FExisteServicoCommand := FDBXConnection.CreateCommand;
    FExisteServicoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteServicoCommand.Text := 'TSMServico.ExisteServico';
    FExisteServicoCommand.Prepare;
  end;
  FExisteServicoCommand.Parameters[0].Value.SetWideString(ADescricaoServico);
  FExisteServicoCommand.ExecuteUpdate;
  Result := FExisteServicoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMServicoClient.ExisteServicoTerceiro(ADescricaoServico: string): Boolean;
begin
  if FExisteServicoTerceiroCommand = nil then
  begin
    FExisteServicoTerceiroCommand := FDBXConnection.CreateCommand;
    FExisteServicoTerceiroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteServicoTerceiroCommand.Text := 'TSMServico.ExisteServicoTerceiro';
    FExisteServicoTerceiroCommand.Prepare;
  end;
  FExisteServicoTerceiroCommand.Parameters[0].Value.SetWideString(ADescricaoServico);
  FExisteServicoTerceiroCommand.ExecuteUpdate;
  Result := FExisteServicoTerceiroCommand.Parameters[1].Value.GetBoolean;
end;

function TSMServicoClient.GetIdServicoPorDescricao(ADescricaoServico: string): Integer;
begin
  if FGetIdServicoPorDescricaoCommand = nil then
  begin
    FGetIdServicoPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdServicoPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdServicoPorDescricaoCommand.Text := 'TSMServico.GetIdServicoPorDescricao';
    FGetIdServicoPorDescricaoCommand.Prepare;
  end;
  FGetIdServicoPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoServico);
  FGetIdServicoPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdServicoPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMServicoClient.GetIdServicoTerceiroPorDescricao(ADescricaoServico: string): Integer;
begin
  if FGetIdServicoTerceiroPorDescricaoCommand = nil then
  begin
    FGetIdServicoTerceiroPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdServicoTerceiroPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdServicoTerceiroPorDescricaoCommand.Text := 'TSMServico.GetIdServicoTerceiroPorDescricao';
    FGetIdServicoTerceiroPorDescricaoCommand.Prepare;
  end;
  FGetIdServicoTerceiroPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoServico);
  FGetIdServicoTerceiroPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdServicoTerceiroPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMServicoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMServicoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMServicoClient.Destroy;
begin
  FGetServicoCommand.DisposeOf;
  FGetDescricaoCommand.DisposeOf;
  FGerarServicoCommand.DisposeOf;
  FExisteServicoCommand.DisposeOf;
  FExisteServicoTerceiroCommand.DisposeOf;
  FGetIdServicoPorDescricaoCommand.DisposeOf;
  FGetIdServicoTerceiroPorDescricaoCommand.DisposeOf;
  inherited;
end;

function TSMCheckListClient.GetTodosItensAtivosDoChecklist(AIdChecklist: Integer): TFDJSONDataSets;
begin
  if FGetTodosItensAtivosDoChecklistCommand = nil then
  begin
    FGetTodosItensAtivosDoChecklistCommand := FDBXConnection.CreateCommand;
    FGetTodosItensAtivosDoChecklistCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTodosItensAtivosDoChecklistCommand.Text := 'TSMCheckList.GetTodosItensAtivosDoChecklist';
    FGetTodosItensAtivosDoChecklistCommand.Prepare;
  end;
  FGetTodosItensAtivosDoChecklistCommand.Parameters[0].Value.SetInt32(AIdChecklist);
  FGetTodosItensAtivosDoChecklistCommand.ExecuteUpdate;
  if not FGetTodosItensAtivosDoChecklistCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTodosItensAtivosDoChecklistCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTodosItensAtivosDoChecklistCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTodosItensAtivosDoChecklistCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMCheckListClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCheckListClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCheckListClient.Destroy;
begin
  FGetTodosItensAtivosDoChecklistCommand.DisposeOf;
  inherited;
end;

function TSMEquipamentoClient.GetEquipamento(AIdEquipamento: Integer): TEquipamentoProxy;
begin
  if FGetEquipamentoCommand = nil then
  begin
    FGetEquipamentoCommand := FDBXConnection.CreateCommand;
    FGetEquipamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetEquipamentoCommand.Text := 'TSMEquipamento.GetEquipamento';
    FGetEquipamentoCommand.Prepare;
  end;
  FGetEquipamentoCommand.Parameters[0].Value.SetInt32(AIdEquipamento);
  FGetEquipamentoCommand.ExecuteUpdate;
  if not FGetEquipamentoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetEquipamentoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TEquipamentoProxy(FUnMarshal.UnMarshal(FGetEquipamentoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetEquipamentoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMEquipamentoClient.GerarEquipamento(AEquipamento: TEquipamentoProxy): Integer;
begin
  if FGerarEquipamentoCommand = nil then
  begin
    FGerarEquipamentoCommand := FDBXConnection.CreateCommand;
    FGerarEquipamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarEquipamentoCommand.Text := 'TSMEquipamento.GerarEquipamento';
    FGerarEquipamentoCommand.Prepare;
  end;
  if not Assigned(AEquipamento) then
    FGerarEquipamentoCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarEquipamentoCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarEquipamentoCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AEquipamento), True);
      if FInstanceOwner then
        AEquipamento.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarEquipamentoCommand.ExecuteUpdate;
  Result := FGerarEquipamentoCommand.Parameters[1].Value.GetInt32;
end;

function TSMEquipamentoClient.BuscarIdEquipamentoPelaDescricao(ADescricaoEquipamento: string): Integer;
begin
  if FBuscarIdEquipamentoPelaDescricaoCommand = nil then
  begin
    FBuscarIdEquipamentoPelaDescricaoCommand := FDBXConnection.CreateCommand;
    FBuscarIdEquipamentoPelaDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarIdEquipamentoPelaDescricaoCommand.Text := 'TSMEquipamento.BuscarIdEquipamentoPelaDescricao';
    FBuscarIdEquipamentoPelaDescricaoCommand.Prepare;
  end;
  FBuscarIdEquipamentoPelaDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoEquipamento);
  FBuscarIdEquipamentoPelaDescricaoCommand.ExecuteUpdate;
  Result := FBuscarIdEquipamentoPelaDescricaoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMEquipamentoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMEquipamentoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMEquipamentoClient.Destroy;
begin
  FGetEquipamentoCommand.DisposeOf;
  FGerarEquipamentoCommand.DisposeOf;
  FBuscarIdEquipamentoPelaDescricaoCommand.DisposeOf;
  inherited;
end;


constructor TSMSituacaoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMSituacaoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMSituacaoClient.Destroy;
begin
  inherited;
end;

function TSMOrdemServicoClient.PodeExcluir(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeExcluirCommand = nil then
  begin
    FPodeExcluirCommand := FDBXConnection.CreateCommand;
    FPodeExcluirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeExcluirCommand.Text := 'TSMOrdemServico.PodeExcluir';
    FPodeExcluirCommand.Prepare;
  end;
  FPodeExcluirCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeExcluirCommand.ExecuteUpdate;
  Result := FPodeExcluirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMOrdemServicoClient.PodeEstornar(AIdChaveProcesso: Integer): Boolean;
begin
  if FPodeEstornarCommand = nil then
  begin
    FPodeEstornarCommand := FDBXConnection.CreateCommand;
    FPodeEstornarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeEstornarCommand.Text := 'TSMOrdemServico.PodeEstornar';
    FPodeEstornarCommand.Prepare;
  end;
  FPodeEstornarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FPodeEstornarCommand.ExecuteUpdate;
  Result := FPodeEstornarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMOrdemServicoClient.Efetivar(AIdChaveProcesso: Integer; OrdemServicoProxy: string): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMOrdemServico.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarCommand.Parameters[1].Value.SetWideString(OrdemServicoProxy);
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[2].Value.GetBoolean;
end;

function TSMOrdemServicoClient.Cancelar(AIdChaveProcesso: Integer): Boolean;
begin
  if FCancelarCommand = nil then
  begin
    FCancelarCommand := FDBXConnection.CreateCommand;
    FCancelarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCancelarCommand.Text := 'TSMOrdemServico.Cancelar';
    FCancelarCommand.Prepare;
  end;
  FCancelarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FCancelarCommand.ExecuteUpdate;
  Result := FCancelarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMOrdemServicoClient.GetId(AIdChaveProcesso: Integer): Integer;
begin
  if FGetIdCommand = nil then
  begin
    FGetIdCommand := FDBXConnection.CreateCommand;
    FGetIdCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdCommand.Text := 'TSMOrdemServico.GetId';
    FGetIdCommand.Prepare;
  end;
  FGetIdCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FGetIdCommand.ExecuteUpdate;
  Result := FGetIdCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMOrdemServicoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMOrdemServicoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMOrdemServicoClient.Destroy;
begin
  FPodeExcluirCommand.DisposeOf;
  FPodeEstornarCommand.DisposeOf;
  FEfetivarCommand.DisposeOf;
  FCancelarCommand.DisposeOf;
  FGetIdCommand.DisposeOf;
  inherited;
end;

function TSMLotePagamentoClient.RemoverQuitacoesIndividuais(AIdLotePagamento: Integer): string;
begin
  if FRemoverQuitacoesIndividuaisCommand = nil then
  begin
    FRemoverQuitacoesIndividuaisCommand := FDBXConnection.CreateCommand;
    FRemoverQuitacoesIndividuaisCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverQuitacoesIndividuaisCommand.Text := 'TSMLotePagamento.RemoverQuitacoesIndividuais';
    FRemoverQuitacoesIndividuaisCommand.Prepare;
  end;
  FRemoverQuitacoesIndividuaisCommand.Parameters[0].Value.SetInt32(AIdLotePagamento);
  FRemoverQuitacoesIndividuaisCommand.ExecuteUpdate;
  Result := FRemoverQuitacoesIndividuaisCommand.Parameters[1].Value.GetWideString;
end;

function TSMLotePagamentoClient.RemoverMovimentosIndividuais(AIdLotePagamento: Integer): string;
begin
  if FRemoverMovimentosIndividuaisCommand = nil then
  begin
    FRemoverMovimentosIndividuaisCommand := FDBXConnection.CreateCommand;
    FRemoverMovimentosIndividuaisCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverMovimentosIndividuaisCommand.Text := 'TSMLotePagamento.RemoverMovimentosIndividuais';
    FRemoverMovimentosIndividuaisCommand.Prepare;
  end;
  FRemoverMovimentosIndividuaisCommand.Parameters[0].Value.SetInt32(AIdLotePagamento);
  FRemoverMovimentosIndividuaisCommand.ExecuteUpdate;
  Result := FRemoverMovimentosIndividuaisCommand.Parameters[1].Value.GetWideString;
end;

function TSMLotePagamentoClient.GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
begin
  if FGerarLancamentosAgrupadosCommand = nil then
  begin
    FGerarLancamentosAgrupadosCommand := FDBXConnection.CreateCommand;
    FGerarLancamentosAgrupadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarLancamentosAgrupadosCommand.Text := 'TSMLotePagamento.GerarLancamentosAgrupados';
    FGerarLancamentosAgrupadosCommand.Prepare;
  end;
  FGerarLancamentosAgrupadosCommand.Parameters[0].Value.SetInt32(AChaveProcesso);
  FGerarLancamentosAgrupadosCommand.ExecuteUpdate;
  Result := FGerarLancamentosAgrupadosCommand.Parameters[1].Value.GetWideString;
end;

function TSMLotePagamentoClient.RemoverLancamentosAgrupados(AIdLotePagamento: Integer; AGerarContraPartida: Boolean): string;
begin
  if FRemoverLancamentosAgrupadosCommand = nil then
  begin
    FRemoverLancamentosAgrupadosCommand := FDBXConnection.CreateCommand;
    FRemoverLancamentosAgrupadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverLancamentosAgrupadosCommand.Text := 'TSMLotePagamento.RemoverLancamentosAgrupados';
    FRemoverLancamentosAgrupadosCommand.Prepare;
  end;
  FRemoverLancamentosAgrupadosCommand.Parameters[0].Value.SetInt32(AIdLotePagamento);
  FRemoverLancamentosAgrupadosCommand.Parameters[1].Value.SetBoolean(AGerarContraPartida);
  FRemoverLancamentosAgrupadosCommand.ExecuteUpdate;
  Result := FRemoverLancamentosAgrupadosCommand.Parameters[2].Value.GetWideString;
end;

function TSMLotePagamentoClient.GetContasAPagar(AIdsContaPagar: string): TFDJSONDataSets;
begin
  if FGetContasAPagarCommand = nil then
  begin
    FGetContasAPagarCommand := FDBXConnection.CreateCommand;
    FGetContasAPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasAPagarCommand.Text := 'TSMLotePagamento.GetContasAPagar';
    FGetContasAPagarCommand.Prepare;
  end;
  FGetContasAPagarCommand.Parameters[0].Value.SetWideString(AIdsContaPagar);
  FGetContasAPagarCommand.ExecuteUpdate;
  if not FGetContasAPagarCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasAPagarCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasAPagarCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasAPagarCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMLotePagamentoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMLotePagamentoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMLotePagamentoClient.Destroy;
begin
  FRemoverQuitacoesIndividuaisCommand.DisposeOf;
  FRemoverMovimentosIndividuaisCommand.DisposeOf;
  FGerarLancamentosAgrupadosCommand.DisposeOf;
  FRemoverLancamentosAgrupadosCommand.DisposeOf;
  FGetContasAPagarCommand.DisposeOf;
  inherited;
end;

function TSMLoteRecebimentoClient.RemoverQuitacoesIndividuais(AIdLoteRecebimento: Integer): string;
begin
  if FRemoverQuitacoesIndividuaisCommand = nil then
  begin
    FRemoverQuitacoesIndividuaisCommand := FDBXConnection.CreateCommand;
    FRemoverQuitacoesIndividuaisCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverQuitacoesIndividuaisCommand.Text := 'TSMLoteRecebimento.RemoverQuitacoesIndividuais';
    FRemoverQuitacoesIndividuaisCommand.Prepare;
  end;
  FRemoverQuitacoesIndividuaisCommand.Parameters[0].Value.SetInt32(AIdLoteRecebimento);
  FRemoverQuitacoesIndividuaisCommand.ExecuteUpdate;
  Result := FRemoverQuitacoesIndividuaisCommand.Parameters[1].Value.GetWideString;
end;

function TSMLoteRecebimentoClient.GerarLancamentosAgrupados(AChaveProcesso: Integer): string;
begin
  if FGerarLancamentosAgrupadosCommand = nil then
  begin
    FGerarLancamentosAgrupadosCommand := FDBXConnection.CreateCommand;
    FGerarLancamentosAgrupadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarLancamentosAgrupadosCommand.Text := 'TSMLoteRecebimento.GerarLancamentosAgrupados';
    FGerarLancamentosAgrupadosCommand.Prepare;
  end;
  FGerarLancamentosAgrupadosCommand.Parameters[0].Value.SetInt32(AChaveProcesso);
  FGerarLancamentosAgrupadosCommand.ExecuteUpdate;
  Result := FGerarLancamentosAgrupadosCommand.Parameters[1].Value.GetWideString;
end;

function TSMLoteRecebimentoClient.RemoverLancamentosAgrupados(AIdLoteRecebimento: Integer; AGerarContraPartida: Boolean): string;
begin
  if FRemoverLancamentosAgrupadosCommand = nil then
  begin
    FRemoverLancamentosAgrupadosCommand := FDBXConnection.CreateCommand;
    FRemoverLancamentosAgrupadosCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRemoverLancamentosAgrupadosCommand.Text := 'TSMLoteRecebimento.RemoverLancamentosAgrupados';
    FRemoverLancamentosAgrupadosCommand.Prepare;
  end;
  FRemoverLancamentosAgrupadosCommand.Parameters[0].Value.SetInt32(AIdLoteRecebimento);
  FRemoverLancamentosAgrupadosCommand.Parameters[1].Value.SetBoolean(AGerarContraPartida);
  FRemoverLancamentosAgrupadosCommand.ExecuteUpdate;
  Result := FRemoverLancamentosAgrupadosCommand.Parameters[2].Value.GetWideString;
end;

function TSMLoteRecebimentoClient.GetContasAReceber(AIdsContaReceber: string): TFDJSONDataSets;
begin
  if FGetContasAReceberCommand = nil then
  begin
    FGetContasAReceberCommand := FDBXConnection.CreateCommand;
    FGetContasAReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasAReceberCommand.Text := 'TSMLoteRecebimento.GetContasAReceber';
    FGetContasAReceberCommand.Prepare;
  end;
  FGetContasAReceberCommand.Parameters[0].Value.SetWideString(AIdsContaReceber);
  FGetContasAReceberCommand.ExecuteUpdate;
  if not FGetContasAReceberCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasAReceberCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasAReceberCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasAReceberCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;


constructor TSMLoteRecebimentoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMLoteRecebimentoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMLoteRecebimentoClient.Destroy;
begin
  FRemoverQuitacoesIndividuaisCommand.DisposeOf;
  FGerarLancamentosAgrupadosCommand.DisposeOf;
  FRemoverLancamentosAgrupadosCommand.DisposeOf;
  FGetContasAReceberCommand.DisposeOf;
  inherited;
end;


constructor TSMCNAEClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCNAEClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCNAEClient.Destroy;
begin
  inherited;
end;

function TSMCorClient.GetCor(AIdCor: Integer): TCorProxy;
begin
  if FGetCorCommand = nil then
  begin
    FGetCorCommand := FDBXConnection.CreateCommand;
    FGetCorCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCorCommand.Text := 'TSMCor.GetCor';
    FGetCorCommand.Prepare;
  end;
  FGetCorCommand.Parameters[0].Value.SetInt32(AIdCor);
  FGetCorCommand.ExecuteUpdate;
  if not FGetCorCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetCorCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCorProxy(FUnMarshal.UnMarshal(FGetCorCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetCorCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMCorClient.GerarCor(ACor: TCorProxy): Integer;
begin
  if FGerarCorCommand = nil then
  begin
    FGerarCorCommand := FDBXConnection.CreateCommand;
    FGerarCorCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarCorCommand.Text := 'TSMCor.GerarCor';
    FGerarCorCommand.Prepare;
  end;
  if not Assigned(ACor) then
    FGerarCorCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarCorCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarCorCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ACor), True);
      if FInstanceOwner then
        ACor.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarCorCommand.ExecuteUpdate;
  Result := FGerarCorCommand.Parameters[1].Value.GetInt32;
end;

function TSMCorClient.ExisteCor(ADescricaoCor: string): Boolean;
begin
  if FExisteCorCommand = nil then
  begin
    FExisteCorCommand := FDBXConnection.CreateCommand;
    FExisteCorCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteCorCommand.Text := 'TSMCor.ExisteCor';
    FExisteCorCommand.Prepare;
  end;
  FExisteCorCommand.Parameters[0].Value.SetWideString(ADescricaoCor);
  FExisteCorCommand.ExecuteUpdate;
  Result := FExisteCorCommand.Parameters[1].Value.GetBoolean;
end;

function TSMCorClient.GetIdCorPorDescricao(ADescricaoCor: string): Integer;
begin
  if FGetIdCorPorDescricaoCommand = nil then
  begin
    FGetIdCorPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdCorPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdCorPorDescricaoCommand.Text := 'TSMCor.GetIdCorPorDescricao';
    FGetIdCorPorDescricaoCommand.Prepare;
  end;
  FGetIdCorPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoCor);
  FGetIdCorPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdCorPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;

function TSMCorClient.GetDescricaoCor(AIdCor: Integer): string;
begin
  if FGetDescricaoCorCommand = nil then
  begin
    FGetDescricaoCorCommand := FDBXConnection.CreateCommand;
    FGetDescricaoCorCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDescricaoCorCommand.Text := 'TSMCor.GetDescricaoCor';
    FGetDescricaoCorCommand.Prepare;
  end;
  FGetDescricaoCorCommand.Parameters[0].Value.SetInt32(AIdCor);
  FGetDescricaoCorCommand.ExecuteUpdate;
  Result := FGetDescricaoCorCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMCorClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCorClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCorClient.Destroy;
begin
  FGetCorCommand.DisposeOf;
  FGerarCorCommand.DisposeOf;
  FExisteCorCommand.DisposeOf;
  FGetIdCorPorDescricaoCommand.DisposeOf;
  FGetDescricaoCorCommand.DisposeOf;
  inherited;
end;

function TSMOperadoraCartaoClient.GetOperadoraCartao(AIdOperadoraCartao: Integer): string;
begin
  if FGetOperadoraCartaoCommand = nil then
  begin
    FGetOperadoraCartaoCommand := FDBXConnection.CreateCommand;
    FGetOperadoraCartaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetOperadoraCartaoCommand.Text := 'TSMOperadoraCartao.GetOperadoraCartao';
    FGetOperadoraCartaoCommand.Prepare;
  end;
  FGetOperadoraCartaoCommand.Parameters[0].Value.SetInt32(AIdOperadoraCartao);
  FGetOperadoraCartaoCommand.ExecuteUpdate;
  Result := FGetOperadoraCartaoCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMOperadoraCartaoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMOperadoraCartaoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMOperadoraCartaoClient.Destroy;
begin
  FGetOperadoraCartaoCommand.DisposeOf;
  inherited;
end;

function TSMVeiculoClient.GetVeiculo(AIdVeiculo: Integer): string;
begin
  if FGetVeiculoCommand = nil then
  begin
    FGetVeiculoCommand := FDBXConnection.CreateCommand;
    FGetVeiculoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetVeiculoCommand.Text := 'TSMVeiculo.GetVeiculo';
    FGetVeiculoCommand.Prepare;
  end;
  FGetVeiculoCommand.Parameters[0].Value.SetInt32(AIdVeiculo);
  FGetVeiculoCommand.ExecuteUpdate;
  Result := FGetVeiculoCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMVeiculoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMVeiculoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMVeiculoClient.Destroy;
begin
  FGetVeiculoCommand.DisposeOf;
  inherited;
end;

function TSMCreditoPessoaClient.CreditoDisponivel(AIdPessoa: Integer): Double;
begin
  if FCreditoDisponivelCommand = nil then
  begin
    FCreditoDisponivelCommand := FDBXConnection.CreateCommand;
    FCreditoDisponivelCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCreditoDisponivelCommand.Text := 'TSMCreditoPessoa.CreditoDisponivel';
    FCreditoDisponivelCommand.Prepare;
  end;
  FCreditoDisponivelCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FCreditoDisponivelCommand.ExecuteUpdate;
  Result := FCreditoDisponivelCommand.Parameters[1].Value.GetDouble;
end;

function TSMCreditoPessoaClient.RealizarTransacaoCredito(APessoaCredito: string): Boolean;
begin
  if FRealizarTransacaoCreditoCommand = nil then
  begin
    FRealizarTransacaoCreditoCommand := FDBXConnection.CreateCommand;
    FRealizarTransacaoCreditoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRealizarTransacaoCreditoCommand.Text := 'TSMCreditoPessoa.RealizarTransacaoCredito';
    FRealizarTransacaoCreditoCommand.Prepare;
  end;
  FRealizarTransacaoCreditoCommand.Parameters[0].Value.SetWideString(APessoaCredito);
  FRealizarTransacaoCreditoCommand.ExecuteUpdate;
  Result := FRealizarTransacaoCreditoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMCreditoPessoaClient.ExisteMovimentacao(AIdPessoa: Integer): Boolean;
begin
  if FExisteMovimentacaoCommand = nil then
  begin
    FExisteMovimentacaoCommand := FDBXConnection.CreateCommand;
    FExisteMovimentacaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteMovimentacaoCommand.Text := 'TSMCreditoPessoa.ExisteMovimentacao';
    FExisteMovimentacaoCommand.Prepare;
  end;
  FExisteMovimentacaoCommand.Parameters[0].Value.SetInt32(AIdPessoa);
  FExisteMovimentacaoCommand.ExecuteUpdate;
  Result := FExisteMovimentacaoCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMCreditoPessoaClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCreditoPessoaClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCreditoPessoaClient.Destroy;
begin
  FCreditoDisponivelCommand.DisposeOf;
  FRealizarTransacaoCreditoCommand.DisposeOf;
  FExisteMovimentacaoCommand.DisposeOf;
  inherited;
end;

function TSMNCMClient.GetNCM(AIdNCM: Integer): string;
begin
  if FGetNCMCommand = nil then
  begin
    FGetNCMCommand := FDBXConnection.CreateCommand;
    FGetNCMCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetNCMCommand.Text := 'TSMNCM.GetNCM';
    FGetNCMCommand.Prepare;
  end;
  FGetNCMCommand.Parameters[0].Value.SetInt32(AIdNCM);
  FGetNCMCommand.ExecuteUpdate;
  Result := FGetNCMCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMNCMClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMNCMClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMNCMClient.Destroy;
begin
  FGetNCMCommand.DisposeOf;
  inherited;
end;

function TSMCarteiraClient.GetCarteira(AIdCarteira: Integer): string;
begin
  if FGetCarteiraCommand = nil then
  begin
    FGetCarteiraCommand := FDBXConnection.CreateCommand;
    FGetCarteiraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCarteiraCommand.Text := 'TSMCarteira.GetCarteira';
    FGetCarteiraCommand.Prepare;
  end;
  FGetCarteiraCommand.Parameters[0].Value.SetInt32(AIdCarteira);
  FGetCarteiraCommand.ExecuteUpdate;
  Result := FGetCarteiraCommand.Parameters[1].Value.GetWideString;
end;

function TSMCarteiraClient.BuscarProximaSequenciaRemessa(AIdCarteira: Integer): Integer;
begin
  if FBuscarProximaSequenciaRemessaCommand = nil then
  begin
    FBuscarProximaSequenciaRemessaCommand := FDBXConnection.CreateCommand;
    FBuscarProximaSequenciaRemessaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarProximaSequenciaRemessaCommand.Text := 'TSMCarteira.BuscarProximaSequenciaRemessa';
    FBuscarProximaSequenciaRemessaCommand.Prepare;
  end;
  FBuscarProximaSequenciaRemessaCommand.Parameters[0].Value.SetInt32(AIdCarteira);
  FBuscarProximaSequenciaRemessaCommand.ExecuteUpdate;
  Result := FBuscarProximaSequenciaRemessaCommand.Parameters[1].Value.GetInt32;
end;

function TSMCarteiraClient.BuscarProximaSequenciaNossoNumero(AIdCarteira: Integer): Integer;
begin
  if FBuscarProximaSequenciaNossoNumeroCommand = nil then
  begin
    FBuscarProximaSequenciaNossoNumeroCommand := FDBXConnection.CreateCommand;
    FBuscarProximaSequenciaNossoNumeroCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarProximaSequenciaNossoNumeroCommand.Text := 'TSMCarteira.BuscarProximaSequenciaNossoNumero';
    FBuscarProximaSequenciaNossoNumeroCommand.Prepare;
  end;
  FBuscarProximaSequenciaNossoNumeroCommand.Parameters[0].Value.SetInt32(AIdCarteira);
  FBuscarProximaSequenciaNossoNumeroCommand.ExecuteUpdate;
  Result := FBuscarProximaSequenciaNossoNumeroCommand.Parameters[1].Value.GetInt32;
end;

procedure TSMCarteiraClient.IncrementarBoletoNossoNumeroProximo(AIdCarteira: Integer);
begin
  if FIncrementarBoletoNossoNumeroProximoCommand = nil then
  begin
    FIncrementarBoletoNossoNumeroProximoCommand := FDBXConnection.CreateCommand;
    FIncrementarBoletoNossoNumeroProximoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FIncrementarBoletoNossoNumeroProximoCommand.Text := 'TSMCarteira.IncrementarBoletoNossoNumeroProximo';
    FIncrementarBoletoNossoNumeroProximoCommand.Prepare;
  end;
  FIncrementarBoletoNossoNumeroProximoCommand.Parameters[0].Value.SetInt32(AIdCarteira);
  FIncrementarBoletoNossoNumeroProximoCommand.ExecuteUpdate;
end;


constructor TSMCarteiraClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCarteiraClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCarteiraClient.Destroy;
begin
  FGetCarteiraCommand.DisposeOf;
  FBuscarProximaSequenciaRemessaCommand.DisposeOf;
  FBuscarProximaSequenciaNossoNumeroCommand.DisposeOf;
  FIncrementarBoletoNossoNumeroProximoCommand.DisposeOf;
  inherited;
end;

function TSMCSOSNClient.GetCstCsosn(AIdCSOSCN: Integer): string;
begin
  if FGetCstCsosnCommand = nil then
  begin
    FGetCstCsosnCommand := FDBXConnection.CreateCommand;
    FGetCstCsosnCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCstCsosnCommand.Text := 'TSMCSOSN.GetCstCsosn';
    FGetCstCsosnCommand.Prepare;
  end;
  FGetCstCsosnCommand.Parameters[0].Value.SetInt32(AIdCSOSCN);
  FGetCstCsosnCommand.ExecuteUpdate;
  Result := FGetCstCsosnCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMCSOSNClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCSOSNClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCSOSNClient.Destroy;
begin
  FGetCstCsosnCommand.DisposeOf;
  inherited;
end;

function TSMRegimeEspecialClient.GetRegimeEspecial(AIdRegimeEspecial: Integer): string;
begin
  if FGetRegimeEspecialCommand = nil then
  begin
    FGetRegimeEspecialCommand := FDBXConnection.CreateCommand;
    FGetRegimeEspecialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetRegimeEspecialCommand.Text := 'TSMRegimeEspecial.GetRegimeEspecial';
    FGetRegimeEspecialCommand.Prepare;
  end;
  FGetRegimeEspecialCommand.Parameters[0].Value.SetInt32(AIdRegimeEspecial);
  FGetRegimeEspecialCommand.ExecuteUpdate;
  Result := FGetRegimeEspecialCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMRegimeEspecialClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMRegimeEspecialClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMRegimeEspecialClient.Destroy;
begin
  FGetRegimeEspecialCommand.DisposeOf;
  inherited;
end;

function TSMSituacaoEspecialClient.GetSituacaoEspecial(AIdSituacaoEspecial: Integer): string;
begin
  if FGetSituacaoEspecialCommand = nil then
  begin
    FGetSituacaoEspecialCommand := FDBXConnection.CreateCommand;
    FGetSituacaoEspecialCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetSituacaoEspecialCommand.Text := 'TSMSituacaoEspecial.GetSituacaoEspecial';
    FGetSituacaoEspecialCommand.Prepare;
  end;
  FGetSituacaoEspecialCommand.Parameters[0].Value.SetInt32(AIdSituacaoEspecial);
  FGetSituacaoEspecialCommand.ExecuteUpdate;
  Result := FGetSituacaoEspecialCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMSituacaoEspecialClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMSituacaoEspecialClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMSituacaoEspecialClient.Destroy;
begin
  FGetSituacaoEspecialCommand.DisposeOf;
  inherited;
end;

function TSMZoneamentoClient.GetZoneamento(AIdZoneamento: Integer): string;
begin
  if FGetZoneamentoCommand = nil then
  begin
    FGetZoneamentoCommand := FDBXConnection.CreateCommand;
    FGetZoneamentoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetZoneamentoCommand.Text := 'TSMZoneamento.GetZoneamento';
    FGetZoneamentoCommand.Prepare;
  end;
  FGetZoneamentoCommand.Parameters[0].Value.SetInt32(AIdZoneamento);
  FGetZoneamentoCommand.ExecuteUpdate;
  Result := FGetZoneamentoCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMZoneamentoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMZoneamentoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMZoneamentoClient.Destroy;
begin
  FGetZoneamentoCommand.DisposeOf;
  inherited;
end;

function TSMReceitaOticaClient.GetReceitaOtica(AIdReceitaOtica: Integer): string;
begin
  if FGetReceitaOticaCommand = nil then
  begin
    FGetReceitaOticaCommand := FDBXConnection.CreateCommand;
    FGetReceitaOticaCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetReceitaOticaCommand.Text := 'TSMReceitaOtica.GetReceitaOtica';
    FGetReceitaOticaCommand.Prepare;
  end;
  FGetReceitaOticaCommand.Parameters[0].Value.SetInt32(AIdReceitaOtica);
  FGetReceitaOticaCommand.ExecuteUpdate;
  Result := FGetReceitaOticaCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMReceitaOticaClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMReceitaOticaClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMReceitaOticaClient.Destroy;
begin
  FGetReceitaOticaCommand.DisposeOf;
  inherited;
end;


constructor TSMMedicoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMMedicoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMMedicoClient.Destroy;
begin
  inherited;
end;

function TSMRegraImpostoClient.GetRegraImposto(AIdRegraImposto: Integer): string;
begin
  if FGetRegraImpostoCommand = nil then
  begin
    FGetRegraImpostoCommand := FDBXConnection.CreateCommand;
    FGetRegraImpostoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetRegraImpostoCommand.Text := 'TSMRegraImposto.GetRegraImposto';
    FGetRegraImpostoCommand.Prepare;
  end;
  FGetRegraImpostoCommand.Parameters[0].Value.SetInt32(AIdRegraImposto);
  FGetRegraImpostoCommand.ExecuteUpdate;
  Result := FGetRegraImpostoCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMRegraImpostoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMRegraImpostoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMRegraImpostoClient.Destroy;
begin
  FGetRegraImpostoCommand.DisposeOf;
  inherited;
end;

function TSMCSTIPIClient.GetCSTIPI(AIdCSTIPI: Integer): string;
begin
  if FGetCSTIPICommand = nil then
  begin
    FGetCSTIPICommand := FDBXConnection.CreateCommand;
    FGetCSTIPICommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCSTIPICommand.Text := 'TSMCSTIPI.GetCSTIPI';
    FGetCSTIPICommand.Prepare;
  end;
  FGetCSTIPICommand.Parameters[0].Value.SetInt32(AIdCSTIPI);
  FGetCSTIPICommand.ExecuteUpdate;
  Result := FGetCSTIPICommand.Parameters[1].Value.GetWideString;
end;


constructor TSMCSTIPIClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCSTIPIClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCSTIPIClient.Destroy;
begin
  FGetCSTIPICommand.DisposeOf;
  inherited;
end;

function TSMCSTPISCofinsClient.GetCSTPISCofins(AIdCSTPISCofins: Integer): string;
begin
  if FGetCSTPISCofinsCommand = nil then
  begin
    FGetCSTPISCofinsCommand := FDBXConnection.CreateCommand;
    FGetCSTPISCofinsCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetCSTPISCofinsCommand.Text := 'TSMCSTPISCofins.GetCSTPISCofins';
    FGetCSTPISCofinsCommand.Prepare;
  end;
  FGetCSTPISCofinsCommand.Parameters[0].Value.SetInt32(AIdCSTPISCofins);
  FGetCSTPISCofinsCommand.ExecuteUpdate;
  Result := FGetCSTPISCofinsCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMCSTPISCofinsClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCSTPISCofinsClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCSTPISCofinsClient.Destroy;
begin
  FGetCSTPISCofinsCommand.DisposeOf;
  inherited;
end;

function TSMImpostoIPIClient.GetImpostoIPI(AIdImpostoIPI: Integer): string;
begin
  if FGetImpostoIPICommand = nil then
  begin
    FGetImpostoIPICommand := FDBXConnection.CreateCommand;
    FGetImpostoIPICommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetImpostoIPICommand.Text := 'TSMImpostoIPI.GetImpostoIPI';
    FGetImpostoIPICommand.Prepare;
  end;
  FGetImpostoIPICommand.Parameters[0].Value.SetInt32(AIdImpostoIPI);
  FGetImpostoIPICommand.ExecuteUpdate;
  Result := FGetImpostoIPICommand.Parameters[1].Value.GetWideString;
end;


constructor TSMImpostoIPIClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMImpostoIPIClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMImpostoIPIClient.Destroy;
begin
  FGetImpostoIPICommand.DisposeOf;
  inherited;
end;

function TSMImpostoICMSClient.GetImpostoICMS(AIdImpostoICMS: Integer): string;
begin
  if FGetImpostoICMSCommand = nil then
  begin
    FGetImpostoICMSCommand := FDBXConnection.CreateCommand;
    FGetImpostoICMSCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetImpostoICMSCommand.Text := 'TSMImpostoICMS.GetImpostoICMS';
    FGetImpostoICMSCommand.Prepare;
  end;
  FGetImpostoICMSCommand.Parameters[0].Value.SetInt32(AIdImpostoICMS);
  FGetImpostoICMSCommand.ExecuteUpdate;
  Result := FGetImpostoICMSCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMImpostoICMSClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMImpostoICMSClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMImpostoICMSClient.Destroy;
begin
  FGetImpostoICMSCommand.DisposeOf;
  inherited;
end;

function TSMImpostoPisCofinsClient.GetImpostoPisCofins(AIdImpostoPisCofins: Integer): string;
begin
  if FGetImpostoPisCofinsCommand = nil then
  begin
    FGetImpostoPisCofinsCommand := FDBXConnection.CreateCommand;
    FGetImpostoPisCofinsCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetImpostoPisCofinsCommand.Text := 'TSMImpostoPisCofins.GetImpostoPisCofins';
    FGetImpostoPisCofinsCommand.Prepare;
  end;
  FGetImpostoPisCofinsCommand.Parameters[0].Value.SetInt32(AIdImpostoPisCofins);
  FGetImpostoPisCofinsCommand.ExecuteUpdate;
  Result := FGetImpostoPisCofinsCommand.Parameters[1].Value.GetWideString;
end;


constructor TSMImpostoPisCofinsClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMImpostoPisCofinsClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMImpostoPisCofinsClient.Destroy;
begin
  FGetImpostoPisCofinsCommand.DisposeOf;
  inherited;
end;

function TSMConfiguracoesFiscaisClient.GetConfiguracaoFiscal(AIdConfiguracaoFiscal: Integer): string;
begin
  if FGetConfiguracaoFiscalCommand = nil then
  begin
    FGetConfiguracaoFiscalCommand := FDBXConnection.CreateCommand;
    FGetConfiguracaoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetConfiguracaoFiscalCommand.Text := 'TSMConfiguracoesFiscais.GetConfiguracaoFiscal';
    FGetConfiguracaoFiscalCommand.Prepare;
  end;
  FGetConfiguracaoFiscalCommand.Parameters[0].Value.SetInt32(AIdConfiguracaoFiscal);
  FGetConfiguracaoFiscalCommand.ExecuteUpdate;
  Result := FGetConfiguracaoFiscalCommand.Parameters[1].Value.GetWideString;
end;

function TSMConfiguracoesFiscaisClient.DuplicarConfiguracaoFiscal(AIdConfiguracaoFiscalOrigem: Integer): Integer;
begin
  if FDuplicarConfiguracaoFiscalCommand = nil then
  begin
    FDuplicarConfiguracaoFiscalCommand := FDBXConnection.CreateCommand;
    FDuplicarConfiguracaoFiscalCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FDuplicarConfiguracaoFiscalCommand.Text := 'TSMConfiguracoesFiscais.DuplicarConfiguracaoFiscal';
    FDuplicarConfiguracaoFiscalCommand.Prepare;
  end;
  FDuplicarConfiguracaoFiscalCommand.Parameters[0].Value.SetInt32(AIdConfiguracaoFiscalOrigem);
  FDuplicarConfiguracaoFiscalCommand.ExecuteUpdate;
  Result := FDuplicarConfiguracaoFiscalCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMConfiguracoesFiscaisClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMConfiguracoesFiscaisClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMConfiguracoesFiscaisClient.Destroy;
begin
  FGetConfiguracaoFiscalCommand.DisposeOf;
  FDuplicarConfiguracaoFiscalCommand.DisposeOf;
  inherited;
end;

function TSMInventarioClient.GetInventario(AIdInventario: Integer): string;
begin
  if FGetInventarioCommand = nil then
  begin
    FGetInventarioCommand := FDBXConnection.CreateCommand;
    FGetInventarioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetInventarioCommand.Text := 'TSMInventario.GetInventario';
    FGetInventarioCommand.Prepare;
  end;
  FGetInventarioCommand.Parameters[0].Value.SetInt32(AIdInventario);
  FGetInventarioCommand.ExecuteUpdate;
  Result := FGetInventarioCommand.Parameters[1].Value.GetWideString;
end;

function TSMInventarioClient.GerarInventario(AInventario: string): Integer;
begin
  if FGerarInventarioCommand = nil then
  begin
    FGerarInventarioCommand := FDBXConnection.CreateCommand;
    FGerarInventarioCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarInventarioCommand.Text := 'TSMInventario.GerarInventario';
    FGerarInventarioCommand.Prepare;
  end;
  FGerarInventarioCommand.Parameters[0].Value.SetWideString(AInventario);
  FGerarInventarioCommand.ExecuteUpdate;
  Result := FGerarInventarioCommand.Parameters[1].Value.GetInt32;
end;

function TSMInventarioClient.Efetivar(AIdChaveProcesso: Integer): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMInventario.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMInventarioClient.Reabrir(AIdChaveProcesso: Integer): Boolean;
begin
  if FReabrirCommand = nil then
  begin
    FReabrirCommand := FDBXConnection.CreateCommand;
    FReabrirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReabrirCommand.Text := 'TSMInventario.Reabrir';
    FReabrirCommand.Prepare;
  end;
  FReabrirCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FReabrirCommand.ExecuteUpdate;
  Result := FReabrirCommand.Parameters[1].Value.GetBoolean;
end;


constructor TSMInventarioClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMInventarioClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMInventarioClient.Destroy;
begin
  FGetInventarioCommand.DisposeOf;
  FGerarInventarioCommand.DisposeOf;
  FEfetivarCommand.DisposeOf;
  FReabrirCommand.DisposeOf;
  inherited;
end;

function TSMFluxoCaixaResumidoClient.GetContasCorrentes: TFDJSONDataSets;
begin
  if FGetContasCorrentesCommand = nil then
  begin
    FGetContasCorrentesCommand := FDBXConnection.CreateCommand;
    FGetContasCorrentesCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetContasCorrentesCommand.Text := 'TSMFluxoCaixaResumido.GetContasCorrentes';
    FGetContasCorrentesCommand.Prepare;
  end;
  FGetContasCorrentesCommand.ExecuteUpdate;
  if not FGetContasCorrentesCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetContasCorrentesCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContasCorrentesCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContasCorrentesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFluxoCaixaResumidoClient.GetFluxoCaixaResumido(AContasCorrentes: string; AListaCamposPeriodo: string; ATipoPeriodo: string): TFDJSONDataSets;
begin
  if FGetFluxoCaixaResumidoCommand = nil then
  begin
    FGetFluxoCaixaResumidoCommand := FDBXConnection.CreateCommand;
    FGetFluxoCaixaResumidoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetFluxoCaixaResumidoCommand.Text := 'TSMFluxoCaixaResumido.GetFluxoCaixaResumido';
    FGetFluxoCaixaResumidoCommand.Prepare;
  end;
  FGetFluxoCaixaResumidoCommand.Parameters[0].Value.SetWideString(AContasCorrentes);
  FGetFluxoCaixaResumidoCommand.Parameters[1].Value.SetWideString(AListaCamposPeriodo);
  FGetFluxoCaixaResumidoCommand.Parameters[2].Value.SetWideString(ATipoPeriodo);
  FGetFluxoCaixaResumidoCommand.ExecuteUpdate;
  if not FGetFluxoCaixaResumidoCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetFluxoCaixaResumidoCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetFluxoCaixaResumidoCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetFluxoCaixaResumidoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFluxoCaixaResumidoClient.GetTotalPorContaCorrente(AContasCorrentes: string; ADtInicial: string; ADtFinal: string; ATipoPeriodo: string): TFDJSONDataSets;
begin
  if FGetTotalPorContaCorrenteCommand = nil then
  begin
    FGetTotalPorContaCorrenteCommand := FDBXConnection.CreateCommand;
    FGetTotalPorContaCorrenteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalPorContaCorrenteCommand.Text := 'TSMFluxoCaixaResumido.GetTotalPorContaCorrente';
    FGetTotalPorContaCorrenteCommand.Prepare;
  end;
  FGetTotalPorContaCorrenteCommand.Parameters[0].Value.SetWideString(AContasCorrentes);
  FGetTotalPorContaCorrenteCommand.Parameters[1].Value.SetWideString(ADtInicial);
  FGetTotalPorContaCorrenteCommand.Parameters[2].Value.SetWideString(ADtFinal);
  FGetTotalPorContaCorrenteCommand.Parameters[3].Value.SetWideString(ATipoPeriodo);
  FGetTotalPorContaCorrenteCommand.ExecuteUpdate;
  if not FGetTotalPorContaCorrenteCommand.Parameters[4].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTotalPorContaCorrenteCommand.Parameters[4].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTotalPorContaCorrenteCommand.Parameters[4].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTotalPorContaCorrenteCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFluxoCaixaResumidoClient.GetFluxoCaixaResumidoProxy: TFluxoCaixaResumidoProxy;
begin
  if FGetFluxoCaixaResumidoProxyCommand = nil then
  begin
    FGetFluxoCaixaResumidoProxyCommand := FDBXConnection.CreateCommand;
    FGetFluxoCaixaResumidoProxyCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetFluxoCaixaResumidoProxyCommand.Text := 'TSMFluxoCaixaResumido.GetFluxoCaixaResumidoProxy';
    FGetFluxoCaixaResumidoProxyCommand.Prepare;
  end;
  FGetFluxoCaixaResumidoProxyCommand.ExecuteUpdate;
  if not FGetFluxoCaixaResumidoProxyCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetFluxoCaixaResumidoProxyCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFluxoCaixaResumidoProxy(FUnMarshal.UnMarshal(FGetFluxoCaixaResumidoProxyCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetFluxoCaixaResumidoProxyCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMFluxoCaixaResumidoClient.GerarFluxoCaixaResumido(AFluxoCaixaResumido: TFluxoCaixaResumidoProxy): Integer;
begin
  if FGerarFluxoCaixaResumidoCommand = nil then
  begin
    FGerarFluxoCaixaResumidoCommand := FDBXConnection.CreateCommand;
    FGerarFluxoCaixaResumidoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarFluxoCaixaResumidoCommand.Text := 'TSMFluxoCaixaResumido.GerarFluxoCaixaResumido';
    FGerarFluxoCaixaResumidoCommand.Prepare;
  end;
  if not Assigned(AFluxoCaixaResumido) then
    FGerarFluxoCaixaResumidoCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarFluxoCaixaResumidoCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarFluxoCaixaResumidoCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AFluxoCaixaResumido), True);
      if FInstanceOwner then
        AFluxoCaixaResumido.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarFluxoCaixaResumidoCommand.ExecuteUpdate;
  Result := FGerarFluxoCaixaResumidoCommand.Parameters[1].Value.GetInt32;
end;

function TSMFluxoCaixaResumidoClient.GetTotalContaReceber(ADtInicial: string; ADtFinal: string): Double;
begin
  if FGetTotalContaReceberCommand = nil then
  begin
    FGetTotalContaReceberCommand := FDBXConnection.CreateCommand;
    FGetTotalContaReceberCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalContaReceberCommand.Text := 'TSMFluxoCaixaResumido.GetTotalContaReceber';
    FGetTotalContaReceberCommand.Prepare;
  end;
  FGetTotalContaReceberCommand.Parameters[0].Value.SetWideString(ADtInicial);
  FGetTotalContaReceberCommand.Parameters[1].Value.SetWideString(ADtFinal);
  FGetTotalContaReceberCommand.ExecuteUpdate;
  Result := FGetTotalContaReceberCommand.Parameters[2].Value.GetDouble;
end;

function TSMFluxoCaixaResumidoClient.GetTotalContaPagar(ADtInicial: string; ADtFinal: string): Double;
begin
  if FGetTotalContaPagarCommand = nil then
  begin
    FGetTotalContaPagarCommand := FDBXConnection.CreateCommand;
    FGetTotalContaPagarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTotalContaPagarCommand.Text := 'TSMFluxoCaixaResumido.GetTotalContaPagar';
    FGetTotalContaPagarCommand.Prepare;
  end;
  FGetTotalContaPagarCommand.Parameters[0].Value.SetWideString(ADtInicial);
  FGetTotalContaPagarCommand.Parameters[1].Value.SetWideString(ADtFinal);
  FGetTotalContaPagarCommand.ExecuteUpdate;
  Result := FGetTotalContaPagarCommand.Parameters[2].Value.GetDouble;
end;


constructor TSMFluxoCaixaResumidoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMFluxoCaixaResumidoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMFluxoCaixaResumidoClient.Destroy;
begin
  FGetContasCorrentesCommand.DisposeOf;
  FGetFluxoCaixaResumidoCommand.DisposeOf;
  FGetTotalPorContaCorrenteCommand.DisposeOf;
  FGetFluxoCaixaResumidoProxyCommand.DisposeOf;
  FGerarFluxoCaixaResumidoCommand.DisposeOf;
  FGetTotalContaReceberCommand.DisposeOf;
  FGetTotalContaPagarCommand.DisposeOf;
  inherited;
end;

function TSMMovimentacaoChequeClient.Efetivar(AIdChaveProcesso: Integer): Boolean;
begin
  if FEfetivarCommand = nil then
  begin
    FEfetivarCommand := FDBXConnection.CreateCommand;
    FEfetivarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEfetivarCommand.Text := 'TSMMovimentacaoCheque.Efetivar';
    FEfetivarCommand.Prepare;
  end;
  FEfetivarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEfetivarCommand.ExecuteUpdate;
  Result := FEfetivarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMMovimentacaoChequeClient.Estornar(AIdChaveProcesso: Integer): Boolean;
begin
  if FEstornarCommand = nil then
  begin
    FEstornarCommand := FDBXConnection.CreateCommand;
    FEstornarCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEstornarCommand.Text := 'TSMMovimentacaoCheque.Estornar';
    FEstornarCommand.Prepare;
  end;
  FEstornarCommand.Parameters[0].Value.SetInt32(AIdChaveProcesso);
  FEstornarCommand.ExecuteUpdate;
  Result := FEstornarCommand.Parameters[1].Value.GetBoolean;
end;

function TSMMovimentacaoChequeClient.PodeReabrir(AIdMovimentacaoCheque: Integer): Boolean;
begin
  if FPodeReabrirCommand = nil then
  begin
    FPodeReabrirCommand := FDBXConnection.CreateCommand;
    FPodeReabrirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeReabrirCommand.Text := 'TSMMovimentacaoCheque.PodeReabrir';
    FPodeReabrirCommand.Prepare;
  end;
  FPodeReabrirCommand.Parameters[0].Value.SetInt32(AIdMovimentacaoCheque);
  FPodeReabrirCommand.ExecuteUpdate;
  Result := FPodeReabrirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMMovimentacaoChequeClient.PodeExcluir(AIdMovimentacaoCheque: Integer): Boolean;
begin
  if FPodeExcluirCommand = nil then
  begin
    FPodeExcluirCommand := FDBXConnection.CreateCommand;
    FPodeExcluirCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FPodeExcluirCommand.Text := 'TSMMovimentacaoCheque.PodeExcluir';
    FPodeExcluirCommand.Prepare;
  end;
  FPodeExcluirCommand.Parameters[0].Value.SetInt32(AIdMovimentacaoCheque);
  FPodeExcluirCommand.ExecuteUpdate;
  Result := FPodeExcluirCommand.Parameters[1].Value.GetBoolean;
end;

function TSMMovimentacaoChequeClient.BuscarIDPelaChaveProcesso(AIDChaveProcesso: Integer): Integer;
begin
  if FBuscarIDPelaChaveProcessoCommand = nil then
  begin
    FBuscarIDPelaChaveProcessoCommand := FDBXConnection.CreateCommand;
    FBuscarIDPelaChaveProcessoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBuscarIDPelaChaveProcessoCommand.Text := 'TSMMovimentacaoCheque.BuscarIDPelaChaveProcesso';
    FBuscarIDPelaChaveProcessoCommand.Prepare;
  end;
  FBuscarIDPelaChaveProcessoCommand.Parameters[0].Value.SetInt32(AIDChaveProcesso);
  FBuscarIDPelaChaveProcessoCommand.ExecuteUpdate;
  Result := FBuscarIDPelaChaveProcessoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMMovimentacaoChequeClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMMovimentacaoChequeClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMMovimentacaoChequeClient.Destroy;
begin
  FEfetivarCommand.DisposeOf;
  FEstornarCommand.DisposeOf;
  FPodeReabrirCommand.DisposeOf;
  FPodeExcluirCommand.DisposeOf;
  FBuscarIDPelaChaveProcessoCommand.DisposeOf;
  inherited;
end;

function TSMMontadoraClient.GetMontadora(AIdMontadora: Integer): TMontadoraProxy;
begin
  if FGetMontadoraCommand = nil then
  begin
    FGetMontadoraCommand := FDBXConnection.CreateCommand;
    FGetMontadoraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetMontadoraCommand.Text := 'TSMMontadora.GetMontadora';
    FGetMontadoraCommand.Prepare;
  end;
  FGetMontadoraCommand.Parameters[0].Value.SetInt32(AIdMontadora);
  FGetMontadoraCommand.ExecuteUpdate;
  if not FGetMontadoraCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetMontadoraCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TMontadoraProxy(FUnMarshal.UnMarshal(FGetMontadoraCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetMontadoraCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMMontadoraClient.GerarMontadora(AMontadora: TMontadoraProxy): Integer;
begin
  if FGerarMontadoraCommand = nil then
  begin
    FGerarMontadoraCommand := FDBXConnection.CreateCommand;
    FGerarMontadoraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarMontadoraCommand.Text := 'TSMMontadora.GerarMontadora';
    FGerarMontadoraCommand.Prepare;
  end;
  if not Assigned(AMontadora) then
    FGerarMontadoraCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarMontadoraCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarMontadoraCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AMontadora), True);
      if FInstanceOwner then
        AMontadora.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarMontadoraCommand.ExecuteUpdate;
  Result := FGerarMontadoraCommand.Parameters[1].Value.GetInt32;
end;

function TSMMontadoraClient.ExisteMontadora(ADescricaoMontadora: string): Boolean;
begin
  if FExisteMontadoraCommand = nil then
  begin
    FExisteMontadoraCommand := FDBXConnection.CreateCommand;
    FExisteMontadoraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteMontadoraCommand.Text := 'TSMMontadora.ExisteMontadora';
    FExisteMontadoraCommand.Prepare;
  end;
  FExisteMontadoraCommand.Parameters[0].Value.SetWideString(ADescricaoMontadora);
  FExisteMontadoraCommand.ExecuteUpdate;
  Result := FExisteMontadoraCommand.Parameters[1].Value.GetBoolean;
end;

function TSMMontadoraClient.GetIdMontadoraPorDescricao(ADescricaoMontadora: string): Integer;
begin
  if FGetIdMontadoraPorDescricaoCommand = nil then
  begin
    FGetIdMontadoraPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdMontadoraPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdMontadoraPorDescricaoCommand.Text := 'TSMMontadora.GetIdMontadoraPorDescricao';
    FGetIdMontadoraPorDescricaoCommand.Prepare;
  end;
  FGetIdMontadoraPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoMontadora);
  FGetIdMontadoraPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdMontadoraPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMMontadoraClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMMontadoraClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMMontadoraClient.Destroy;
begin
  FGetMontadoraCommand.DisposeOf;
  FGerarMontadoraCommand.DisposeOf;
  FExisteMontadoraCommand.DisposeOf;
  FGetIdMontadoraPorDescricaoCommand.DisposeOf;
  inherited;
end;

function TSMModeloMontadoraClient.GetModeloMontadora(AIdModeloMontadora: Integer): TModeloMontadoraProxy;
begin
  if FGetModeloMontadoraCommand = nil then
  begin
    FGetModeloMontadoraCommand := FDBXConnection.CreateCommand;
    FGetModeloMontadoraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetModeloMontadoraCommand.Text := 'TSMModeloMontadora.GetModeloMontadora';
    FGetModeloMontadoraCommand.Prepare;
  end;
  FGetModeloMontadoraCommand.Parameters[0].Value.SetInt32(AIdModeloMontadora);
  FGetModeloMontadoraCommand.ExecuteUpdate;
  if not FGetModeloMontadoraCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetModeloMontadoraCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TModeloMontadoraProxy(FUnMarshal.UnMarshal(FGetModeloMontadoraCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetModeloMontadoraCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMModeloMontadoraClient.GerarModeloMontadora(AModeloMontadora: TModeloMontadoraProxy): Integer;
begin
  if FGerarModeloMontadoraCommand = nil then
  begin
    FGerarModeloMontadoraCommand := FDBXConnection.CreateCommand;
    FGerarModeloMontadoraCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarModeloMontadoraCommand.Text := 'TSMModeloMontadora.GerarModeloMontadora';
    FGerarModeloMontadoraCommand.Prepare;
  end;
  if not Assigned(AModeloMontadora) then
    FGerarModeloMontadoraCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarModeloMontadoraCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarModeloMontadoraCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(AModeloMontadora), True);
      if FInstanceOwner then
        AModeloMontadora.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarModeloMontadoraCommand.ExecuteUpdate;
  Result := FGerarModeloMontadoraCommand.Parameters[1].Value.GetInt32;
end;

function TSMModeloMontadoraClient.ExisteModelo(ADescricaoModelo: string; ADescricaoMontadora: string): Boolean;
begin
  if FExisteModeloCommand = nil then
  begin
    FExisteModeloCommand := FDBXConnection.CreateCommand;
    FExisteModeloCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteModeloCommand.Text := 'TSMModeloMontadora.ExisteModelo';
    FExisteModeloCommand.Prepare;
  end;
  FExisteModeloCommand.Parameters[0].Value.SetWideString(ADescricaoModelo);
  FExisteModeloCommand.Parameters[1].Value.SetWideString(ADescricaoMontadora);
  FExisteModeloCommand.ExecuteUpdate;
  Result := FExisteModeloCommand.Parameters[2].Value.GetBoolean;
end;


constructor TSMModeloMontadoraClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMModeloMontadoraClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMModeloMontadoraClient.Destroy;
begin
  FGetModeloMontadoraCommand.DisposeOf;
  FGerarModeloMontadoraCommand.DisposeOf;
  FExisteModeloCommand.DisposeOf;
  inherited;
end;


constructor TSMNegociacaoContaReceberClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMNegociacaoContaReceberClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMNegociacaoContaReceberClient.Destroy;
begin
  inherited;
end;

function TSMTamanhoClient.GetTamanho(AIdTamanho: Integer): TTamanhoProxy;
begin
  if FGetTamanhoCommand = nil then
  begin
    FGetTamanhoCommand := FDBXConnection.CreateCommand;
    FGetTamanhoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetTamanhoCommand.Text := 'TSMTamanho.GetTamanho';
    FGetTamanhoCommand.Prepare;
  end;
  FGetTamanhoCommand.Parameters[0].Value.SetInt32(AIdTamanho);
  FGetTamanhoCommand.ExecuteUpdate;
  if not FGetTamanhoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FGetTamanhoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TTamanhoProxy(FUnMarshal.UnMarshal(FGetTamanhoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTamanhoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSMTamanhoClient.GerarTamanho(ATamanho: TTamanhoProxy): Integer;
begin
  if FGerarTamanhoCommand = nil then
  begin
    FGerarTamanhoCommand := FDBXConnection.CreateCommand;
    FGerarTamanhoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarTamanhoCommand.Text := 'TSMTamanho.GerarTamanho';
    FGerarTamanhoCommand.Prepare;
  end;
  if not Assigned(ATamanho) then
    FGerarTamanhoCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarTamanhoCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarTamanhoCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ATamanho), True);
      if FInstanceOwner then
        ATamanho.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarTamanhoCommand.ExecuteUpdate;
  Result := FGerarTamanhoCommand.Parameters[1].Value.GetInt32;
end;

function TSMTamanhoClient.ExisteTamanho(ADescricaoTamanho: string): Boolean;
begin
  if FExisteTamanhoCommand = nil then
  begin
    FExisteTamanhoCommand := FDBXConnection.CreateCommand;
    FExisteTamanhoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExisteTamanhoCommand.Text := 'TSMTamanho.ExisteTamanho';
    FExisteTamanhoCommand.Prepare;
  end;
  FExisteTamanhoCommand.Parameters[0].Value.SetWideString(ADescricaoTamanho);
  FExisteTamanhoCommand.ExecuteUpdate;
  Result := FExisteTamanhoCommand.Parameters[1].Value.GetBoolean;
end;

function TSMTamanhoClient.GetIdTamanhoPorDescricao(ADescricaoTamanho: string): Integer;
begin
  if FGetIdTamanhoPorDescricaoCommand = nil then
  begin
    FGetIdTamanhoPorDescricaoCommand := FDBXConnection.CreateCommand;
    FGetIdTamanhoPorDescricaoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetIdTamanhoPorDescricaoCommand.Text := 'TSMTamanho.GetIdTamanhoPorDescricao';
    FGetIdTamanhoPorDescricaoCommand.Prepare;
  end;
  FGetIdTamanhoPorDescricaoCommand.Parameters[0].Value.SetWideString(ADescricaoTamanho);
  FGetIdTamanhoPorDescricaoCommand.ExecuteUpdate;
  Result := FGetIdTamanhoPorDescricaoCommand.Parameters[1].Value.GetInt32;
end;


constructor TSMTamanhoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMTamanhoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMTamanhoClient.Destroy;
begin
  FGetTamanhoCommand.DisposeOf;
  FGerarTamanhoCommand.DisposeOf;
  FExisteTamanhoCommand.DisposeOf;
  FGetIdTamanhoPorDescricaoCommand.DisposeOf;
  inherited;
end;


constructor TSMGradeProdutoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMGradeProdutoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMGradeProdutoClient.Destroy;
begin
  inherited;
end;

function TSMBackupClient.ValidarConfiguracoesParaBackup: Boolean;
begin
  if FValidarConfiguracoesParaBackupCommand = nil then
  begin
    FValidarConfiguracoesParaBackupCommand := FDBXConnection.CreateCommand;
    FValidarConfiguracoesParaBackupCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FValidarConfiguracoesParaBackupCommand.Text := 'TSMBackup.ValidarConfiguracoesParaBackup';
    FValidarConfiguracoesParaBackupCommand.Prepare;
  end;
  FValidarConfiguracoesParaBackupCommand.ExecuteUpdate;
  Result := FValidarConfiguracoesParaBackupCommand.Parameters[0].Value.GetBoolean;
end;

function TSMBackupClient.RealizarBackup(AIdUsuario: Integer): Boolean;
begin
  if FRealizarBackupCommand = nil then
  begin
    FRealizarBackupCommand := FDBXConnection.CreateCommand;
    FRealizarBackupCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRealizarBackupCommand.Text := 'TSMBackup.RealizarBackup';
    FRealizarBackupCommand.Prepare;
  end;
  FRealizarBackupCommand.Parameters[0].Value.SetInt32(AIdUsuario);
  FRealizarBackupCommand.ExecuteUpdate;
  Result := FRealizarBackupCommand.Parameters[1].Value.GetBoolean;
end;

function TSMBackupClient.BackupDiarioRealizado: Boolean;
begin
  if FBackupDiarioRealizadoCommand = nil then
  begin
    FBackupDiarioRealizadoCommand := FDBXConnection.CreateCommand;
    FBackupDiarioRealizadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FBackupDiarioRealizadoCommand.Text := 'TSMBackup.BackupDiarioRealizado';
    FBackupDiarioRealizadoCommand.Prepare;
  end;
  FBackupDiarioRealizadoCommand.ExecuteUpdate;
  Result := FBackupDiarioRealizadoCommand.Parameters[0].Value.GetBoolean;
end;

function TSMBackupClient.GerarRegistroBackup(ABackup: TBackupProxy): Integer;
begin
  if FGerarRegistroBackupCommand = nil then
  begin
    FGerarRegistroBackupCommand := FDBXConnection.CreateCommand;
    FGerarRegistroBackupCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGerarRegistroBackupCommand.Text := 'TSMBackup.GerarRegistroBackup';
    FGerarRegistroBackupCommand.Prepare;
  end;
  if not Assigned(ABackup) then
    FGerarRegistroBackupCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FGerarRegistroBackupCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FGerarRegistroBackupCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ABackup), True);
      if FInstanceOwner then
        ABackup.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FGerarRegistroBackupCommand.ExecuteUpdate;
  Result := FGerarRegistroBackupCommand.Parameters[1].Value.GetInt32;
end;

function TSMBackupClient.DownloadBackup(out Size: Int64): TStream;
begin
  if FDownloadBackupCommand = nil then
  begin
    FDownloadBackupCommand := FDBXConnection.CreateCommand;
    FDownloadBackupCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FDownloadBackupCommand.Text := 'TSMBackup.DownloadBackup';
    FDownloadBackupCommand.Prepare;
  end;
  FDownloadBackupCommand.ExecuteUpdate;
  Size := FDownloadBackupCommand.Parameters[0].Value.GetInt64;
  Result := FDownloadBackupCommand.Parameters[1].Value.GetStream(FInstanceOwner);
end;


constructor TSMBackupClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMBackupClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMBackupClient.Destroy;
begin
  FValidarConfiguracoesParaBackupCommand.DisposeOf;
  FRealizarBackupCommand.DisposeOf;
  FBackupDiarioRealizadoCommand.DisposeOf;
  FGerarRegistroBackupCommand.DisposeOf;
  FDownloadBackupCommand.DisposeOf;
  inherited;
end;


constructor TSMCFOPClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMCFOPClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMCFOPClient.Destroy;
begin
  inherited;
end;

function TSMBloqueioPersonalizadoClient.UsuarioAutorizado(AIdBloqueioPersonalizado: Integer; AIdUsuario: Integer): Boolean;
begin
  if FUsuarioAutorizadoCommand = nil then
  begin
    FUsuarioAutorizadoCommand := FDBXConnection.CreateCommand;
    FUsuarioAutorizadoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FUsuarioAutorizadoCommand.Text := 'TSMBloqueioPersonalizado.UsuarioAutorizado';
    FUsuarioAutorizadoCommand.Prepare;
  end;
  FUsuarioAutorizadoCommand.Parameters[0].Value.SetInt32(AIdBloqueioPersonalizado);
  FUsuarioAutorizadoCommand.Parameters[1].Value.SetInt32(AIdUsuario);
  FUsuarioAutorizadoCommand.ExecuteUpdate;
  Result := FUsuarioAutorizadoCommand.Parameters[2].Value.GetBoolean;
end;


constructor TSMBloqueioPersonalizadoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMBloqueioPersonalizadoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMBloqueioPersonalizadoClient.Destroy;
begin
  FUsuarioAutorizadoCommand.DisposeOf;
  inherited;
end;


constructor TSMTipoRestricaoClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TSMTipoRestricaoClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TSMTipoRestricaoClient.Destroy;
begin
  inherited;
end;

end.
