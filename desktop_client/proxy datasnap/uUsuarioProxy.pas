unit uUsuarioProxy;

interface

type TUsuarioProxy = class
  id: Integer;
  usuario: String;
  nome: String;
  idUsuarioPerfil: Integer;
  administrador: Boolean;

  idSeguranca: Integer; //Usar para parametrizacao de liberacao de sistema

  const USUARIO_ADMINISTRADOR: String = 'KRATOS';
end;

implementation

end.
