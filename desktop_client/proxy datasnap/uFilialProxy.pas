unit uFilialProxy;

Interface

type TFilialProxy = class
  FId: Integer;
  FNrItem: Integer;
  FFantasia: String;
  FRazaoSocial: String;
  FCnpj: String;
  FIe: String;
  FDtFundacao: String;
  FIdEmpresa: Integer;
  FIdZoneamento: Integer;
  FRegimeTributario: String;
  FBoMatriz: String;
  FIdCidade: Integer;
  FLogradouro: String;
  FNumero: String;
  FComplemento: String;
  FBairro: String;
  FCep: String;
  FTelefone: String;
  FSite: String;
  FEmail: String;
  FTipoEmpresa: String;
  FCrt: Integer;
  FBoContribuinteIcms: String;
  FBoContribuinteIpi: String;
  FIM: String;
  FIdCNAE: Integer;

  //Dados Transientes
  FCodigoIGBECidade: Integer;
  FCodigoIGBEEstado: Integer;
  FCodigoIGBEPais: Integer;
  FNomeCidade: String;
  FUF: String;
  FNomeEstado: String;
  FNomePais: String;

  const REGIME_TRIBUTARIO_SIMPLES = 'SIMPLES';
end;

implementation

end.


