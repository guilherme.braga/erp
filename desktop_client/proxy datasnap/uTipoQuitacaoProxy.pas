unit uTipoQuitacaoProxy;

interface

type TTipoQuitacaoProxy = class
    const
      TIPO_DINHEIRO        = 'DINHEIRO';
      TIPO_CHEQUE_TERCEIRO = 'CHEQUE_TERCEIRO';
      TIPO_CHEQUE_PROPRIO  = 'CHEQUE_PROPRIO';
      TIPO_CREDITO         = 'CREDITO';
      TIPO_DEBITO          = 'DEBITO';
      TIPO_ADIANTAMENTO    = 'ADIANTAMENTO';
      TIPO_SEM_MOVIMENTO   = 'SEM_MOVIMENTO';
  end;

implementation

end.
