unit uRegraImpostoProxy;

Interface

type TRegraImpostoProxy = class
  FId: String;
  FIdNcm: Integer;
  FDescricao: String;
  FExcecaoIpi: Integer;
  FIdNcmEspecializado: Integer;

  const FIELD_ID_NCM = 'REGRA_IMPOSTO.ID_NCM';
end;

implementation

end.


