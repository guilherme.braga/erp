unit uInventarioProxy;

Interface

Uses generics.Collections, SysUtils;

type TInventarioProdutoProxy = class
  FId: Integer;
  FIdInventario: Integer;
  FIdProduto: Integer;
end;

type TInventarioProxy = class
  FId: Integer;
  FDhCadastro: String;
  FTipo: String;
  FIdFilial: Integer;
  FIdPessoaUsuario: Integer;
  FIdChaveProcesso: Integer;
  FDhInicio: String;
  FDhFechamento: String;
  FDhFim: String;
  FObservacao: String;
  FInventarioProduto: TList<TInventarioProdutoProxy>;

  const TIPO_MANUAL = 'MANUAL';
  const TIPO_AUTOMATICO = 'AUTOMATICO';

  const STATUS_ABERTO= 'ABERTO';
  const STATUS_CONFIRMADO ='CONFIRMADO';
  const STATUS_CANCELADO  = 'CANCELADO';

  constructor Create;
  destructor Destroy;
end;

implementation

{ TInventarioProxy }

constructor TInventarioProxy.Create;
begin
  FInventarioProduto := TList<TInventarioProdutoProxy>.Create;
end;

destructor TInventarioProxy.Destroy;
begin
  FreeAndNil(FInventarioProduto);
end;

end.

