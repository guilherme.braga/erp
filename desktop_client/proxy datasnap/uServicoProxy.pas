unit uServicoProxy;

interface

type TServicoProxy = class
  FId: Integer;
  FDescricao: String;
  FVlCusto: Double;
  FPercLucro: Double;
  FVlVenda: Double;
  FDuracao: String;
  FBoServicoTerceiro: String;
  FBoAtivo: String;

  const FIELD_BO_SERVICO_TERCEIRO = 'SERVICO.BO_SERVICO_TERCEIRO';

  constructor Create;
end;

implementation

{ TServicoProxy }

constructor TServicoProxy.Create;
begin
  FVlVenda := 0;
  FVlCusto := 0;
  FBoServicoTerceiro := 'N';
end;

end.
