unit uContaCorrenteProxy;

interface

type TContaCorrenteMovimento = class

  Fid         : Integer;

  FDocumento  : string;
  FDescricao  : string;
  FObservacao : string;

  FDhCadastro    : string;
  FDtEmissao     : string;
  FDtMovimento   : string;
  FDtCompetencia : string;

  FVlMovimento : Double;
  FTpMovimento : string;

  FVlSaldoConciliado : Double;
  FVlSaldoGeral      : Double;

  FIdContaCorrente    : Integer;
  FIdContaAnalise     : Integer;
  FIdCentroResultado  : Integer;

  FBoConciliado      : string;
  FDhConciliado      : string;

  FBoMovimentoOrigem  : string;
  FIdMovimentoOrigem  : Integer;

  FBoMovimentoDestino : string;
  FIdMovimentoDestino : Integer;

  FBoTransferencia        : string;
  FIdContaCorrenteDestino : Integer;

  FIdChaveProcesso : Integer;

  FIdFilial : Integer;
  FIdUsuarioMovimento: Integer;

  FTPDocumentoOrigem  : string;
  FIdDocumentoOrigem  : Integer;
  FParcelamento: String;

  FBoCheque: String;
  FBoChequeTerceiro: String;
  FBoChequeProprio: String;

  FChequeSacado: string;
  FChequeDocFederal: string;
  FChequeBanco: string;
  FChequeDtEmissao: string;
  FChequeDtVencimento: string;
  FChequeAgencia: string;
  FChequeContaCorrente: string;
  FChequeNumero: Integer;

  FBoProcessoAmortizado: String;

  FIdOperadoraCartao: Integer;

  const
    ORIGEM_MANUAL           = 'MANUAL';
    ORIGEM_CONTA_PAGAR      = 'CONTAPAGAR';
    ORIGEM_CONTA_RECEBER    = 'CONTARECEBER';
    ORIGEM_LOTE_PAGAMENTO   = 'LOTE DE PAGAMENTO';
    ORIGEM_LOTE_RECEBIMENTO = 'LOTE DE RECEBIMENTO';
    ORIGEM_VENDA            = 'VENDA';
    ORIGEM_ORDEM_SERVICO    = 'ORDEM DE SERVI�O';

    LISTA_ORIGEM = 'MANUAL;CONTAPAGAR;CONTARECEBER;LOTE DE PAGAMENTO;LOTE DE RECEBIMENTO;VENDA;ORDEM DE SERVI�O';

    TIPO_MOVIMENTO_DEBITO  = 'DEBITO';
    TIPO_MOVIMENTO_CREDITO = 'CREDITO';

    TIPO_DOCUMENTO_CONTAPAGAR   = 'CONTAPAGAR';
    TIPO_DOCUMENTO_CONTARECEBER = 'CONTARECEBER';

  constructor Create;
end;

type TContaCorrenteProxy = class
  const NOME_TABELA = 'CONTA_CORRENTE';
end;

implementation

{ TContaCorrenteMovimento }

constructor TContaCorrenteMovimento.Create;
begin
  FBoConciliado := 'N';
  FBoMovimentoOrigem := 'N';
  FBoMovimentoDestino := 'N';
  FBoTransferencia := 'N';
  FParcelamento := '1/1';
  FBoCheque := 'N';
  FBoChequeTerceiro := 'N';
  FBoChequeProprio := 'N';
  FBoProcessoAmortizado := 'N';
  FVlSaldoConciliado := 0;
  FVlSaldoGeral := 0;
end;

end.
