unit uLotePagamentoProxy;

interface

type TLotePagamentoProxy = class

    const
      STATUS_ABERTO    = 'ABERTO';
      STATUS_EFETIVADO = 'EFETIVADO';
      RETORNO_SUCESSO  = 'RETORNO_SUCESSO';

      CONSTANTE_TODOS = 'TODOS';

      FIELD_DATA_EFETIVACAO      = 'LOTE_PAGAMENTO.DH_EFETIVACAO';
      FIELD_STATUS               = 'LOTE_PAGAMENTO.STATUS';
      FIELD_VALOR_LOTE_PAGAMENTO = 'LOTE_PAGAMENTO.VALOR';
      FIELD_DOCUMENTO            = 'LOTE_PAGAMENTO.DOCUMENTO';

  end;

implementation

end.
