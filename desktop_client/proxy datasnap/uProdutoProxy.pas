unit uProdutoProxy;

interface

Uses SysUtils;

type TProdutoMovimento = class
  FId                : Integer;
  FIdProduto         : Integer;
  FIdFilial          : Integer;
  FIdChaveProcesso   : Integer;
  FTipo              : String;
  FDocumentoOrigem   : String;
  FDtMovimento       : String;
  FQtEstoqueAnterior : Double;
  FQtMovimento       : Double;
  FQtEstoqueAtual    : Double;
  FVlUltimoCusto     : Double;
  FQtEstoqueCondicional: Double;
  FVlUltimoFrete: Double;
  FVlUltimoSeguro: Double;
  FVlUltimoIcmsSt: Double;
  FVlUltimoIpi: Double;
  FVlUltimoDesconto: Double;
  FVlUltimaDespesaAcessoria: Double;
  FVlUltimoCustoImposto: Double;

  FVlCustoOperacional: Double;
  FVlCustoImpostoOperacional: Double;
  FPercUltimoFrete: Double;
  FPercUltimoSeguro: Double;
  FPercUltimoIcmsSt: Double;
  FPercUltimoIpi: Double;
  FPercUltimoDesconto: Double;
  FPercUltimaDespesaAcessoria: Double;
  FPercCustoOperacional: Double;

  FVlCustoMedio      : Double;
  FVlCustoMedioImposto: Double;
  FVlCustoMedioImpostoOperacional: Double;
  FBoProcessoAmortizado: String;

  const TIPO_MOVIMENTO_ENTRADA= 'ENTRADA';
  const TIPO_MOVIMENTO_SAIDA = 'SAIDA';

  constructor Create;
end;

type TProdutoFilialProxy = class
  FId: Integer;
  FQtEstoque: Double;
  FLocalizacao: String;
  FQtEstoqueMinimo: Double;
  FQtEstoqueMaximo: Double;
  FIdProduto: Integer;
  FIdFilial: Integer;
  FQtEstoqueCondicional: Double;
  FVlUltimoCusto: Double;
  FVlUltimoFrete: Double;
  FVlUltimoSeguro: Double;
  FVlUltimoIcmsSt: Double;
  FVlUltimoIpi: Double;
  FVlUltimoDesconto: Double;
  FVlUltimaDespesaAcessoria: Double;
  FVlUltimoCustoImposto: Double;
  FVlCustoOperacional: Double;
  FVlCustoImpostoOperacional: Double;
  FPercUltimoFrete: Double;
  FPercUltimoSeguro: Double;
  FPercUltimoIcmsSt: Double;
  FPercUltimoIpi: Double;
  FPercUltimoDesconto: Double;
  FPercUltimaDespesaAcessoria: Double;
  FPercCustoOperacional: Double;
  FVlCustoMedio: Double;
  FVlCustoMedioImposto: Double;
  FVlCustoMedioImpostoOperacional: Double;

  const FIELD_FILIAL = 'PRODUTO_FILIAL.ID_FILIAL';

  const DOCUMENTO_ORIGEM_NOTA_FISCAL ='NOTA_FISCAL';
  const DOCUMENTO_ORIGEM_VENDA ='VENDA';
  const DOCUMENTO_ORIGEM_AJUSTE_ESTOQUE ='AJUSTE_ESTOQUE';
  const DOCUMENTO_ORIGEM_FORMACAO_PRECO_INICIAL = 'FORMA��O DE PRE�O INICIAL';//Feito pelo cadastro de produto

  const LISTA_ORIGEM = 'NOTA_FISCAL;VENDA;AJUSTE_ESTOQUE;FORMA��O DE PRE�O INICIAL';
end;

type TProdutoProxy = class
  FId: Integer;
  FDescricao: String;
  FQtdeEstoque: Double;
  FUN: String;
  FVlVenda: Double;
  FVlCusto: Double;
  FCodigoBarra: String;
  FBoAtivo: String;
  FIdGrupoProduto: Integer;
  FIdSubGrupoProduto: Integer;
  FIdMarca: Integer;
  FIdCategoria: Integer;
  FIdSubCategoria: Integer;
  FIdLinha: Integer;
  FIdModelo: Integer;
  FIdUnidadeEstoque: Integer;
  FTipo: String;
  FIdCor: Integer;
  FFormaAquisicao: String;
  FOrigemDaMercadoria: Integer;
  FIdNcm: Integer;
  FIdNcmEspecializado: Integer;
  FTipoTributacaoPisCofinsSt: Integer;
  FTipoTributacaoIpi: Integer;
  FExcecaoIpi: Integer;
  FIdRegraImposto: Integer;
  FDhCadastro: String;
  FBoProdutoAgrupador: String;
  FBoProdutoAgrupado: String;
  FIdProdutoAgrupador: Integer;

  FProdutoFilial: TProdutoFilialProxy;

 const FIELD_ATIVO  = 'PRODUTO.BO_ATIVO';
 const FIELD_ID_GRUPO_PRODUTO  = 'PRODUTO.ID_GRUPO_PRODUTO';
 const FIELD_ID_SUB_GRUPO_PRODUTO  = 'PRODUTO.ID_SUB_GRUPO_PRODUTO';
 const FIELD_ID_MARCA  = 'PRODUTO.ID_MARCA';
 const FIELD_ID_CATEGORIA  = 'PRODUTO.ID_CATEGORIA';
 const FIELD_ID_SUB_CATEGORIA  = 'PRODUTO.ID_SUB_CATEGORIA';
 const FIELD_ID_LINHA  = 'PRODUTO.ID_LINHA';
 const FIELD_ID_MODELO  = 'PRODUTO.ID_MODELO';
 const FIELD_ID_UNIDADE_ESTOQUE  = 'PRODUTO.ID_UNIDADE_ESTOQUE';
 const FIELD_TIPO  = 'PRODUTO.TIPO';
 const FIELD_CODIGO_BARRA  = 'PRODUTO.CODIGO_BARRA';
 const FIELD_DESCRICAO  = 'PRODUTO.DESCRICAO';
 const FIELD_ID  = 'PRODUTO.ID';
 const FIELD_FILIAL  = 'PRODUTO.ID_FILIAL';
 const NOME_TABELA = 'PRODUTO';
 const FIELD_ID_TABELA_PRECO = 'ID_TABELA_PRECO';
 const FIELD_BO_PRODUTO_AGRUPADOR = 'PRODUTO.BO_PRODUTO_AGRUPADOR';
 const FIELD_BO_PRODUTO_AGRUPADO = 'PRODUTO.BO_PRODUTO_AGRUPADO';
 const FIELD_ID_NCM = 'PRODUTO.ID_NCM';

 const TIPO_PRODUTO_PRODUTO = 'PRODUTO';
 const TIPO_PRODUTO_SERVICO = 'SERVI�O';
 const TIPO_PRODUTO_OUTROS = 'OUTROS';

 constructor Create;
 destructor Destroy;
end;

type TCategoriaProxy = class
  const NOME_TABELA = 'CATEGORIA';
end;

type TSubCategoriaProxy = class
  const NOME_TABELA = 'SUB_CATEGORIA';
end;

type TLinhaProxy = class
  const NOME_TABELA = 'LINHA';
end;

type TUnidadeEstoqueProxy = class
  const NOME_TABELA = 'UNIDADE_ESTOQUE';
end;

type TModeloProxy = class
  const NOME_TABELA = 'MODELO';
end;

type TMarcaProxy = class
  const NOME_TABELA = 'MARCA';
end;

type TGrupoProdutoProxy = class
  const NOME_TABELA = 'GRUPO_PRODUTO';
end;

type TSubGrupoProdutoProxy = class
  const FIELD_ID_GRUPO_PRODUTO = 'SUB_GRUPO_PRODUTO.ID_GRUPO_PRODUTO';
  const NOME_TABELA = 'SUB_GRUPO_PRODUTO';
end;

type TProdutoFiscalProxy = class
  //Informa��es Gerais
  I_CEAN: String;
  I_CEANTRIB: String;
  I_XPROD: String;
  I_NCM: String;
  I_CFOP: String;
  I_UCOM: String;
  I_UTRIB: String;
  H_NITEM: Integer;
  I_QCOM: Double;
  I_VUNCOM: Double;
  I_VPROD: Double;
  I_QTRIB: Double;
  I_VUNTRIB: Double;
  I_VFRETE: Double;
  I_VSEG: Double;
  I_VDESC: Double;
  I_VOUTRO: Double;
  I_INDTOT: Integer;
  I_XPED: String;
  I_NITEMPED: Integer;

  //Imposto ICMS
  N_ORIG: String;
  N_CST: String;
  N_MODBC: String;
  N_vBC: Double;
  N_pICMS: Double;
  N_vICMS: Double;
  N_MODBCST: String;
  N_pMVAST: Double;
  N_pRedBCST: Double;
  N_vBCST: Double;
  N_pICMSST: Double;
  N_vICMSST: Double;
  N_pRedBC: Double;
  N_motDesICMS: Integer;
  N_vBCSTRet: Double;
  N_vICMSSTRet: Double;
  N_pCredSN: Double;
  N_vCredICMSSN: Double;
  N_VICMSOP: Double;
  N_UFST: String;
  N_PDIF: Double;
  N_VLICMSDIF: Double;
  N_VICMSDESON: Double;
  N_PBCOP: Double;
  N_BCSTRET: Double;
  N_VBCSTDEST: Double;
  N_VICMSSTDEST: Double;

  //Imposto IPI
  O_clEnq: String;
  O_CNPJProd: String;
  O_cSelo: String;
  O_qSelo: Integer;
  O_cEnq: String;
  O_qUnid: Double;
  O_vUnid: Double;
  O_vIPI: Double;
  O_CST: String;
  O_vBC: Double;
  O_pIPI: Double;

  //Imposto PIS
  Q_CST: String;
  Q_VBC: Double;
  Q_PPIS: Double;
  Q_VPIS: Double;
  Q_BCPROD: Double;
  Q_vAliqProd: Double;

  //Imposto COFINS
  S_CST: String;
  S_vBC: Double;
  S_pCOFINS: Double;
  S_vCOFINS: Double;
  S_BCProd: Double;
  S_vAliqProd: Double;
  S_qBCProd: Double;

  //Internos
  ID_PRODUTO: String;
  ID_REGRA_IMPOSTO: Integer;
  ID_REGRA_IMPOSTO_FILTRO: Integer;
  ID_IMPOSTO_IPI: Integer;
  ID_IMPOSTO_PISCofins: Integer;
  ID_IMPOSTO_ICMS: Integer;
end;

implementation

{ TProdutoProxy }

constructor TProdutoProxy.Create;
begin
  FId := 0;
  FDescricao := '';
  FQtdeEstoque := 0;
  FUN := '';
  FVlVenda := 0;
  FVlCusto := 0;
  FCodigoBarra := '';

  FProdutoFilial := TProdutoFilialProxy.Create;
end;

destructor TProdutoProxy.Destroy;
begin
  if Assigned(FProdutoFilial) then
    FreeAndNil(FProdutoFilial);
end;

{ TProdutoMovimento }

constructor TProdutoMovimento.Create;
begin
  FBoProcessoAmortizado := 'N';
end;

end.
