unit uVeiculoProxy;

interface

type TVeiculoProxy = class
  FId: Integer;
  FPlaca: String;
  FAno: Integer;
  FCombustivel: String;
  FChassi: String;
  FRenavam: String;
  FIdModelo: Integer;
  FIdMarca: Integer;
  FIdCor: Integer;
  FIdFilial: Integer;
  FIdPessoa: Integer;
  FBoAtivo: String;

  FDescricaoModelo: String;
  FDescricaoMarca: String;
  FDescricaoCor: String;
  FNomeFilial: String;
  FNomdePessoa: String;
end;

implementation

end.
