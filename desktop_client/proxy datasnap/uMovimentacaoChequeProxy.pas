unit uMovimentacaoChequeProxy;

Interface

type TMovimentacaoChequeProxy = class
  FId: Integer;
  FTipoMovimento: String;
  FMovimentacaoChequecol: String;
  FIdPessoaUsuarioCadastro: Integer;
  FIdPessoaUsuarioEfetivacao: Integer;
  FIdPessoaUsuarioReabertura: Integer;
  FIdContaCorrente: Integer;
  FIdContaCorrenteDestino: Integer;
  FIdFilial: Integer;
  FIdChaveProcesso: Integer;
  FDhCadastro: String;
  FDhFechamento: String;
  FStatus: String;
  FObservacao: String;

  const TIPO_MOVIMENTO_TRANSFERENCIA = 'TRANSFERENCIA';
  const TIPO_MOVIMENTO_DEBITO = 'DEBITO';
  const STATUS_ABERTO = 'ABERTO';
  const STATUS_FECHADO = 'FECHADO';
  const STATUS_CANCELADO = 'CANCELADO';
end;

type TMovimentacaoChequeChequeProxy = class
  FId: Integer;
  FIdMovimentacaoCheque: Integer;
  FIdCheque: Integer;
end;

implementation

end.

