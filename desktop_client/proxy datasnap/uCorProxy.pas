unit uCorProxy;

Interface

type TCorProxy = class
  FId: Integer;
  FDescricao: String;
  FRgb: String;
  FCmyk: String;
  FBoAtivo: String;

  constructor Create;
end;

implementation

{ TCorProxy }

constructor TCorProxy.Create;
begin
  FBoAtivo := 'S';
  FRgb := 'N';
  FCmyk := 'N';
end;

end.


