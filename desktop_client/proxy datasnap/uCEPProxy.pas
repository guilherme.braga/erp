unit uCEPProxy;

interface

type TCEPProxy = class
  FIdCep: Integer;
  FCep: String;
  FBairro: String;
  FIdCidade: Integer;
  FRua: String;
  FCidade: String;
  FIdEstado: Integer;
  FEstado: String;
  FUf: String;
  FPais: String;
  FIdPais: Integer;
end;

implementation

end.

