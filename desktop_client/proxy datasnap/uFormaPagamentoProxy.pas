unit uFormaPagamentoProxy;

interface

type TFormaPagamentoProxy = class
  public
  FId: Integer;
  FDescricao: String;
  FIdTipoQuitacao: Integer;
  FDescricaoTipoQuitacao: String;
  FTipoTipoQuitacao: String;

  const FIELD_ID = 'FORMA_PAGAMENTO.ID';
end;

implementation

end.
