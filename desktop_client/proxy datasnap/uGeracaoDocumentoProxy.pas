unit uGeracaoDocumentoProxy;

interface

type TGeracaoDocumentoProxy = class
    {Transiente}
    FTipoDocumentoParaGeracaoTitulos: String; //Cr�dito ou D�bito
    FGerarContraPartidaContaCorrenteMovimento: Boolean;

    const
      STATUS_ABERTO    = 'ABERTO';
      STATUS_GERADO    = 'GERADO';
      STATUS_CANCELADO = 'CANCELADO';

      TIPO_CONTAPAGAR   = 'CONTAPAGAR';
      TIPO_CONTARECEBER = 'CONTARECEBER';
  end;

implementation

end.
