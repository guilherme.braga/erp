unit uBloqueioPersonalizadoProxy;

Interface

type TBloqueioPersonalizadoProxy = class
  FId: Integer;
  FDescricao: String;
  FSql: String;
  FBoVenda: String;
  FBoNotaFiscal: String;
  FBoOrdemServico: String;
  FBoAtivo: String;

  const STATUS_LIBERADO = 'LIBERADO';
  const STATUS_BLOQUEADO = 'BLOQUEADO';
end;

type TBloqueioPersonalizadoUsuarioLiberacaoProxy = class
  FId: Integer;
  FIdPessoaUsuario: Integer;
  FIdBloqueioPersonalizado: Integer;
end;

implementation

end.


