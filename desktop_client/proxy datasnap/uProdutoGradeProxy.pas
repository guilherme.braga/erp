unit uProdutoGradeProxy;

interface

type TProdutoGradeProxy = class
  FIdProdutoAgrupador: Integer;
  FIdCor: Integer;
  FIdTamanho: Integer;
  FQuantidade: Double;
  FIdProduto: Integer;
  FCodigoBarra: String;
end;

implementation

end.
