unit UFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage, System.Actions, Vcl.ActnList, Vcl.Touch.GestureMgr, clipbrd;

type
  TForm1 = class(TForm)
    AppBar: TPanel;
    CloseButton: TImage;
    ActionList1: TActionList;
    Action1: TAction;
    GestureManager1: TGestureManager;
    MemoScript: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    EdtTableName: TEdit;
    procedure CloseButtonClick(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormGesture(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure AppBarResize;
    procedure AppBarShow(mode: integer);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses uGeradorCodigo;

const
  AppBarHeight = 75;

procedure TForm1.AppBarResize;
begin
  AppBar.SetBounds(0, AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width, AppBarHeight);
end;

procedure TForm1.AppBarShow(mode: integer);
begin
  if mode = -1 then // Toggle
    mode := integer(not AppBar.Visible );

  if mode = 0 then
    AppBar.Visible := False
  else
  begin
    AppBar.Visible := True;
    AppBar.BringToFront;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  MemoScript.Lines.Clear;
  MemoScript.Lines.Add(TGeradorCodigo.GetProxyScript(EdtTableName.Text).Text);
  ClipBoard.AsText := MemoScript.Lines.Text;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  MemoScript.Lines.Clear;
  MemoScript.Lines.Add(TGeradorCodigo.GetServerScript(EdtTableName.Text).Text);
  ClipBoard.AsText := MemoScript.Lines.Text;
end;

procedure TForm1.Action1Execute(Sender: TObject);
begin
  AppBarShow(-1);
end;

procedure TForm1.CloseButtonClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.FormGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
  AppBarShow(0);
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    AppBarShow(-1)
  else
    AppBarShow(0);
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  AppBarResize;
end;

end.
