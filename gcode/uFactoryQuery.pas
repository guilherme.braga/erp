unit uFactoryQuery;

interface

uses uDmConnection, uGBFDQuery, db, SysUtils;

type IFastQuery = Interface
  function Eof: Boolean;
  function Bof: Boolean;
  function IsEmpty: Boolean;
  function RecordCount: Integer;
  function Recno: Integer;
  procedure Primeiro;
  procedure Ultimo;
  procedure Proximo;
  procedure Anterior;
  function GetAsFloat(const AFieldName: String): Double;
  function GetAsString(const AFieldName: String): String;
  function GetAsInteger(const AFieldName: String): Integer;
  function GetAsDateTime(const AFieldName: String): TDateTime;
  function GetAsDate(const AFieldName: String): TDate;
  function SetAsFloat(const AFieldName: String; const AValue: Double): Double;
  function SetAsString(const AFieldName: String; const AValue: String): String;
  function SetAsInteger(const AFieldName: String; const AValue: Integer): Integer;
  function SetAsDateTime(const AFieldName: String; const AValue: TDateTime): TDateTime;
  function SetAsDate(const AFieldName: String; const AValue: TDate): TDate;
  procedure Incluir;
  procedure Alterar;
  procedure Excluir;
  procedure Salvar;
  procedure Cancelar;
  procedure Persistir;
  procedure Desfazer;
  function Localizar(const AFieldName: String; const AValue: Variant): Boolean;
End;

type TFastQuery = class(TgbFDQuery, IFastQuery)
  private
    procedure SetConnection;
  public
    constructor ModoDeAlteracao(const ASQL: String; const AValorParametros: TArray<Variant> = nil); overload;
    constructor ModoDeConsulta(const ASQL: String; const AValorParametros: TArray<Variant> = nil); overload;
    constructor ModoDeInclusao(const ATabela: String); overload;

    function RecordCount: Integer;
    function Recno: Integer;
    function Eof: Boolean;
    function Bof: Boolean;
    function IsEmpty: Boolean;
    procedure Primeiro;
    procedure Ultimo;
    procedure Proximo;
    procedure Anterior;
    function GetAsFloat(const AFieldName: String): Double;
    function GetAsString(const AFieldName: String): String;
    function GetAsInteger(const AFieldName: String): Integer;
    function GetAsDateTime(const AFieldName: String): TDateTime;
    function GetAsDate(const AFieldName: String): TDate;
    function SetAsFloat(const AFieldName: String; const AValue: Double): Double;
    function SetAsString(const AFieldName: String; const AValue: String): String;
    function SetAsInteger(const AFieldName: String; const AValue: Integer): Integer;
    function SetAsDateTime(const AFieldName: String; const AValue: TDateTime): TDateTime;
    function SetAsDate(const AFieldName: String; const AValue: TDate): TDate;
    procedure Incluir;
    procedure Alterar;
    procedure Excluir;
    procedure Salvar;
    procedure Cancelar;
    procedure Persistir;
    procedure Desfazer;
    function Localizar(const AFieldName: String; const AValue: Variant): Boolean;

    class procedure ExecutarScript(const ASQL: String; const AValorParametros: TArray<Variant> = nil);
    class procedure ExecutarScriptIndependente(const ASQL: String; const AValorParametros: TArray<Variant> = nil);
End;

implementation

{ TFactoryQuery }

procedure TFastQuery.Salvar;
begin
  (Self as TgbFDQuery).Salvar;
end;

function TFastQuery.SetAsDate(const AFieldName: String;
  const AValue: TDate): TDate;
begin
  (Self as TgbFDQuery).SetAsDate(AFieldName, AValue);
end;

function TFastQuery.SetAsDateTime(const AFieldName: String;
  const AValue: TDateTime): TDateTime;
begin
  (Self as TgbFDQuery).SetAsDateTime(AFieldName, AValue);
end;

function TFastQuery.SetAsFloat(const AFieldName: String;
  const AValue: Double): Double;
begin
  (Self as TgbFDQuery).SetAsFloat(AFieldName, AValue);
end;

function TFastQuery.SetAsInteger(const AFieldName: String;
  const AValue: Integer): Integer;
begin
  (Self as TgbFDQuery).SetAsInteger(AFieldName, AValue);
end;

function TFastQuery.SetAsString(const AFieldName, AValue: String): String;
begin
  (Self as TgbFDQuery).SetAsString(AFieldName, AValue);
end;

procedure TFastQuery.SetConnection;
begin
  Self.Connection := Dm.Connection;
end;

procedure TFastQuery.Ultimo;
begin
  (Self as TgbFDQuery).Last;
end;

procedure TFastQuery.Alterar;
begin
  (Self as TgbFDQuery).Alterar;
end;

procedure TFastQuery.Anterior;
begin
  Self.Prior;
end;

function TFastQuery.Bof: Boolean;
begin
  result := (Self as TgbFDQuery).Bof;
end;

procedure TFastQuery.Cancelar;
begin
  (Self as TgbFDQuery).Cancelar;
end;

procedure TFastQuery.Desfazer;
begin
  (Self as TgbFDQuery).RollBack;
end;

function TFastQuery.Eof: Boolean;
begin
  result := (Self as TgbFDQuery).Eof;
end;

procedure TFastQuery.Excluir;
begin
  (Self as TgbFDQuery).Excluir;
end;

class procedure TFastQuery.ExecutarScript(const ASQL: String;
  const AValorParametros: TArray<Variant>);
var query: TFastQuery;
begin
  try
    query := TFastQuery.Create(nil);
    query.SetConnection;
    (query as TgbFDQuery).ExecutarScript(ASQL, AValorParametros);
  finally
    FreeAndNil(query);
  end;
end;

class procedure TFastQuery.ExecutarScriptIndependente(const ASQL: String;
  const AValorParametros: TArray<Variant>);
var query: TFastQuery;
begin
  try
    query := TFastQuery.Create(nil);
    query.SetConnection;
    (query as TgbFDQuery).ExecutarScriptIndependente(ASQL, AValorParametros);
  finally
    FreeAndNil(query);
  end;
end;

function TFastQuery.GetAsDate(const AFieldName: String): TDate;
begin
  result := (Self as TgbFDQuery).GetAsDate(AFieldName);
end;

function TFastQuery.GetAsDateTime(const AFieldName: String): TDateTime;
begin
  result := (Self as TgbFDQuery).GetAsDateTime(AFieldName);
end;

function TFastQuery.GetAsFloat(const AFieldName: String): Double;
begin
  result := (Self as TgbFDQuery).GetAsFloat(AFieldName);
end;

function TFastQuery.GetAsInteger(const AFieldName: String): Integer;
begin
  result := (Self as TgbFDQuery).GetAsInteger(AFieldName);
end;

function TFastQuery.GetAsString(const AFieldName: String): String;
begin
  result := (Self as TgbFDQuery).GetAsString(AFieldName);
end;

procedure TFastQuery.Incluir;
begin
  (Self as TgbFDQuery).Incluir;
end;

function TFastQuery.IsEmpty: Boolean;
begin
  result := (Self as TgbFDQuery).IsEmpty;
end;

function TFastQuery.Localizar(const AFieldName: String;
  const AValue: Variant): Boolean;
begin
  result := (Self as TgbFDQuery).Localizar(AFieldName, AValue);
end;

constructor TFastQuery.ModoDeAlteracao(const ASQL: String;
  const AValorParametros: TArray<Variant>);
begin
  inherited Create(Dm.Connection);
  SetConnection;
  (Self as TgbFDQuery).ModoDeAlteracao(ASQL, AValorParametros);
end;

constructor TFastQuery.ModoDeConsulta(const ASQL: String;
  const AValorParametros: TArray<Variant>);
begin
  inherited Create(Dm.Connection);
  SetConnection;
  (Self as TgbFDQuery).ModoDeConsulta(ASQL, AValorParametros);
  (Self as TgbFDQuery).Last;
  (Self as TgbFDQuery).First;
end;

constructor TFastQuery.ModoDeInclusao(const ATabela: String);
begin
  inherited Create(Dm.Connection);
  SetConnection;
  (Self as TgbFDQuery).ModoDeInclusao(ATabela);
end;

procedure TFastQuery.Persistir;
begin
  (Self as TgbFDQuery).Persistir;
end;

procedure TFastQuery.Primeiro;
begin
  (Self as TgbFDQuery).First;
end;

procedure TFastQuery.Proximo;
begin
  Self.Next;
end;

function TFastQuery.Recno: Integer;
begin
  result := Self.Recno;
end;

function TFastQuery.RecordCount: Integer;
begin
  result := Self.RecordCount;
end;

end.
