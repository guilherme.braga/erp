unit uGeradorCodigo;

interface

Uses db, Classes, StrUtils, SysUtils;

type TGeradorCodigo = class
  class function GetProxyScript(ATableName: String): TStringList;
  class function GetServerScript(ATableName: String): TStringList;
  class function PrimeiraLetraMaiuscula(AString: string): string;
  class function GetSMPas(ATableName: String): TStringList;
end;

implementation

{ TGeradorCodigo }

uses uFactoryQuery;

class function TGeradorCodigo.GetServerScript(
  ATableName: String): TStringList;
var
  i: Integer;
  propertyName: String;
  queryEntidade: IFastQuery;
  entidade: String;
  nomeEntidadeReal: String;
  nomeEntidadeProxy: String;
  propertyType: String;
  identacao: String;
begin
  result := TStringList.Create;

  entidade := ATableName;
  entidade := StringReplace(entidade, '_', ' ', [rfReplaceAll]);
  entidade := PrimeiraLetraMaiuscula(entidade);
  entidade := StringReplace(entidade, ' ', '', [rfReplaceAll]);
  nomeEntidadeReal := entidade;
  entidade := 'T'+entidade+'Serv';

  result.Add('unit u'+nomeEntidadeReal+'Serv;');
  result.Add('');
  result.Add('Interface');
  result.Add('');

  nomeEntidadeProxy :=  nomeEntidadeReal+'Proxy';

  result.Add('Uses u'+nomeEntidadeProxy+';');
  result.Add('');

  result.Add('type '+ entidade +' = class');

  identacao := identacao + '  ';

  result.Add(identacao+'class function Get'+nomeEntidadeReal+'(');
  result.Add(identacao+'  const AId'+nomeEntidadeReal+': Integer): T'+nomeEntidadeProxy+';');
  result.Add(identacao+'class function Gerar'+nomeEntidadeReal+'(');
  result.Add(identacao+'  const A'+nomeEntidadeReal+': T'+nomeEntidadeProxy+'): Integer;');

  result.Add('end;');

  result.Add('');

  result.Add('implementation');
  result.Add('');
  result.Add('{ '+ entidade +' }');
  result.Add('');
  result.Add('uses uFactoryQuery;');
  result.Add('');

  {M�TODO DE BUSCA DE ENTIDADE}

  result.Add('class function '+entidade+'.Get'+nomeEntidadeReal+'(');
  result.Add(identacao+'  const AId'+nomeEntidadeReal+': Integer): T'+nomeEntidadeProxy+';');
  result.Add('const');
  result.Add(identacao+'SQL = ''SELECT * FROM '+ATableName+' WHERE id = :id '';');
  result.Add('var');
  result.Add(identacao+' qConsulta: IFastQuery;');
  result.Add('begin');
  result.Add(identacao+'result := T'+nomeEntidadeProxy+'.Create;');
  result.Add('');
  result.Add(identacao+'qConsulta := TFastQuery.ModoDeConsulta(SQL,');
  result.Add(identacao+'  TArray<Variant>.Create(AId'+nomeEntidadeReal+'));');
  result.Add('');

  queryEntidade := TFastQuery.ModoDeInclusao(ATableName);
  with queryEntidade as TFastQuery do
  begin
    for i := 0 to FieldCount - 1 do
    begin
      propertyName := Fields[i].FieldName;
      propertyName := StringReplace(propertyName, '_', ' ', [rfReplaceAll]);
      propertyName := PrimeiraLetraMaiuscula(propertyName);
      propertyName := StringReplace(propertyName, ' ', '', [rfReplaceAll]);
      propertyName := 'F'+propertyName;

      case Fields[i].DataType of
        ftLargeInt:
        begin
          propertyType := 'GetAsInteger';
        end;
        ftInteger:
        begin
          propertyType := 'GetAsInteger';
        end;
        ftSmallint:
        begin
          propertyType := 'GetAsInteger';
        end;
        ftWideString:
        begin
          propertyType := 'GetAsString';
        end;
        ftString:
        begin
          propertyType := 'GetAsString';
        end;
        ftDate:
        begin
          propertyType := 'GetAsString';
        end;
        ftDateTime:
        begin
          propertyType := 'GetAsString';
        end;
        ftBoolean:
        begin
          propertyType := 'GetAsBoolean';
        end;
        ftFloat, ftCurrency, ftBCD, ftFMTBcd:
        begin
          propertyType := 'GetAsFloat';
        end;
      else
        begin
          propertyType := 'GetAsString';
        end;
      end;
      result.Add(identacao+'result.'+propertyName+' := qConsulta.'+propertyType+
        '('+QuotedStr(Fields[i].FieldName)+');');
    end;
  end;

  result.Add('end;');
  result.Add('');

  {M�TODO DE GERA��O DE ENTIDADE}

  result.Add('class function '+entidade+'.Gerar'+nomeEntidadeReal+'(');
  result.Add(identacao+'  const A'+nomeEntidadeReal+': T'+nomeEntidadeProxy+'): Integer;');
  result.Add('var');
  result.Add(identacao+'qEntidade: IFastQuery;');
  result.Add('begin');
  result.Add(identacao+'try');
  identacao := '    ';
  result.Add('');
  result.Add(identacao+'qEntidade := TFastQuery.ModoDeInclusao('+QuotedStr(ATableName)+');');
  result.Add(identacao+'qEntidade.Incluir;');
  result.Add('');

  queryEntidade := TFastQuery.ModoDeInclusao(ATableName);
  with queryEntidade as TFastQuery do
  begin
    for i := 0 to FieldCount - 1 do
    begin
      if Fields[i].FieldName = 'ID' then
        continue;

      propertyName := Fields[i].FieldName;
      propertyName := StringReplace(propertyName, '_', ' ', [rfReplaceAll]);
      propertyName := PrimeiraLetraMaiuscula(propertyName);
      propertyName := StringReplace(propertyName, ' ', '', [rfReplaceAll]);
      propertyName := 'F'+propertyName;

      case Fields[i].DataType of
        ftLargeInt:
        begin
          propertyType := 'qEntidade.SetAsInteger';
        end;
        ftInteger:
        begin
          propertyType := 'qEntidade.SetAsInteger';
        end;
        ftSmallint:
        begin
          propertyType := 'qEntidade.SetAsInteger';
        end;
        ftWideString:
        begin
          propertyType := 'qEntidade.SetAsString';
        end;
        ftString:
        begin
          propertyType := 'qEntidade.SetAsString';
        end;
        ftDate:
        begin
          propertyType := 'qEntidade.SetAsString';
        end;
        ftDateTime:
        begin
          propertyType := 'qEntidade.SetAsString';
        end;
        ftBoolean:
        begin
          propertyType := 'qEntidade.SetAsBoolean';
        end;
        ftFloat, ftCurrency, ftBCD, ftFMTBcd:
        begin
          propertyType := 'qEntidade.SetAsFloat';
        end;
      else
        begin
          propertyType := 'qEntidade.SetAsString';
        end;
      end;

      if Copy(Fields[i].FieldName,1,3) = 'ID_' then
      begin
        result.Add('');
        result.Add(identacao+'if A'+nomeEntidadeReal+'.'+propertyName+' > 0 then');
        result.Add(identacao+'begin');
        result.Add(identacao+'  '+propertyType+'('+QuotedStr(Fields[i].FieldName)+
          ', A'+nomeEntidadeReal+'.'+propertyName+');');
        result.Add(identacao+'end;');
      end
      else
      begin
        result.Add(identacao+propertyType+'('+QuotedStr(Fields[i].FieldName)+
        ', A'+nomeEntidadeReal+'.'+propertyName+');');
      end;

      identacao := '    ';
    end;
    result.Add(identacao+'qEntidade.Salvar;');
    result.Add(identacao+'qEntidade.Persistir;');
    result.Add('');
    result.Add(identacao+'A'+nomeEntidadeReal+'.FID := qEntidade.GetAsInteger('+QuotedStr('ID')+');');
    result.Add(identacao+'result := '+identacao+'A'+nomeEntidadeReal+'.FID;');
  end;
  identacao := '  ';
  result.Add(identacao+'except');
  result.Add(identacao+'  on e : Exception do');
  result.Add(identacao+'  begin');
  result.Add(identacao+'    raise Exception.Create('+QuotedStr('Erro ao gerar '+nomeEntidadeReal+'.')+'+');
  result.Add(identacao+'      '+QuotedStr('Mensagem Original: ')+'+e.message);');
  result.Add(identacao+'  end;');
  result.Add(identacao+'end;');
  result.Add('end;');
  result.Add('');

  result.Add('end.');
end;

class function TGeradorCodigo.GetSMPas(ATableName: String): TStringList;
var
  i: Integer;
  propertyName: String;
  queryEntidade: IFastQuery;
  entidade: String;
  propertyType: String;
  identacao: String;
begin
{  result := TStringList.Create;

  entidade := ATableName;
  entidade := StringReplace(entidade, '_', ' ', [rfReplaceAll]);
  entidade := PrimeiraLetraMaiuscula(entidade);
  entidade := StringReplace(entidade,' ', '', [rfReplaceAll]);
  entidade := 'SM'+entidade;

  result.Add('unit u'+entidade+';');
  result.Add('');
  result.Add('Interface');
  result.Add('');

  result.Add('uses');
  result.Add('  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth,');
  result.Add('  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,');
  result.Add('  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,');
  result.Add('  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,');
  result.Add('  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uGBFDQuery,');
  result.Add('  DataSnap.DSProviderDataModuleAdapter;');

  result.Add('');

  entidade := 'T'+entidade;

  result.Add('type '+ entidade +' = class(TDSServerModule)');

  identacao := identacao + '  ';

  queryEntidade := TFastQuery.ModoDeInclusao(ATableName);
  with queryEntidade as TFastQuery do
  begin
    for i := 0 to FieldCount - 1 do
    begin
      propertyName := Fields[i].FieldName;
      propertyName := StringReplace(propertyName, '_', ' ', [rfReplaceAll]);
      propertyName := PrimeiraLetraMaiuscula(propertyName);
      propertyName := StringReplace(propertyName, ' ', '', [rfReplaceAll]);
      propertyName := 'F'+propertyName;

      case Fields[i].DataType of
        ftLargeInt:
        begin
          propertyType := 'Integer';
        end;
        ftInteger:
        begin
          propertyType := 'Integer';
        end;
        ftSmallint:
        begin
          propertyType := 'Integer';
        end;
        ftWideString:
        begin
          propertyType := 'String';
        end;
        ftString:
        begin
          propertyType := 'String';
        end;
        ftDate:
        begin
          propertyType := 'String';
        end;
        ftDateTime:
        begin
          propertyType := 'String';
        end;
        ftBoolean:
        begin
          propertyType := 'Boolean';
        end;
        ftFloat, ftCurrency, ftBCD, ftFMTBcd:
        begin
          propertyType := 'Double';
        end;
      else
        begin
          propertyType := 'String';
        end;
      end;

      result.Add(identacao+propertyName+': '+propertyType+';');
    end;
  end;
  result.Add('end;');
  result.Add('');
  result.Add('implementation');
  result.Add('');
  result.Add('end.');

type
  TSMMedico = class(TDSServerModule)
    fdqMedico: TgbFDQuery;
    dspMedico: TDataSetProvider;
    fdqMedicoID: TFDAutoIncField;
    fdqMedicoNOME: TStringField;
    fdqMedicoCRM: TIntegerField;
    fdqMedicoNUMERO: TIntegerField;
    fdqMedicoCOMPLEMENTO: TStringField;
    fdqMedicoBAIRRO: TStringField;
    fdqMedicoCEP: TStringField;
    fdqMedicoLOGRADOURO: TStringField;
    fdqMedicoID_CIDADE: TIntegerField;
    fdqMedicoJOIN_DESCRICAO_CIDADE: TStringField;
    fdqMedicoTELEFONE: TStringField;
    fdqMedicoOBSERVACAO: TBlobField;
  private
  public
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{
uses uDmConnection;

{$R *.dfm}
 {
end.

}
end;

class function TGeradorCodigo.GetProxyScript(ATableName: String): TStringList;
var
  i: Integer;
  propertyName: String;
  queryEntidade: IFastQuery;
  entidade: String;
  propertyType: String;
  identacao: String;
begin
  result := TStringList.Create;

  entidade := ATableName;
  entidade := StringReplace(entidade, '_', ' ', [rfReplaceAll]);
  entidade := PrimeiraLetraMaiuscula(entidade);
  entidade := StringReplace(entidade,' ', '', [rfReplaceAll]);
  entidade := entidade+'Proxy';

  result.Add('unit u'+entidade+';');
  result.Add('');
  result.Add('Interface');
  result.Add('');

  entidade := 'T'+entidade;

  result.Add('type '+ entidade +' = class');

  identacao := identacao + '  ';

  queryEntidade := TFastQuery.ModoDeInclusao(ATableName);
  with queryEntidade as TFastQuery do
  begin
    for i := 0 to FieldCount - 1 do
    begin
      propertyName := Fields[i].FieldName;
      propertyName := StringReplace(propertyName, '_', ' ', [rfReplaceAll]);
      propertyName := PrimeiraLetraMaiuscula(propertyName);
      propertyName := StringReplace(propertyName, ' ', '', [rfReplaceAll]);
      propertyName := 'F'+propertyName;

      case Fields[i].DataType of
        ftLargeInt, ftAutoInc, ftInteger, ftSmallint:
        begin
          propertyType := 'Integer';
        end;
        ftWideString, ftString, ftDate, ftDateTime:
        begin
          propertyType := 'String';
        end;
        ftBoolean:
        begin
          propertyType := 'Boolean';
        end;
        ftFloat, ftCurrency, ftBCD, ftFMTBcd:
        begin
          propertyType := 'Double';
        end;
      else
        begin
          propertyType := 'String';
        end;
      end;

      result.Add(identacao+propertyName+': '+propertyType+';');
    end;
  end;
  result.Add('end;');
  result.Add('');
  result.Add('implementation');
  result.Add('');
  result.Add('end.');
end;

class function TGeradorCodigo.PrimeiraLetraMaiuscula(AString: string): string;
var
  i: integer;
  esp: boolean;
begin
  AString := LowerCase(Trim(AString));
  for i := 1 to Length(AString) do
  begin
    if i = 1 then
      AString[i] := UpCase(AString[i])
    else
      begin
        if i <> Length(AString) then
        begin
          esp := (AString[i] = ' ');
          if esp then
            AString[i+1] := UpCase(AString[i+1]);
        end;
      end;
  end;
  Result := AString;
end;

end.
