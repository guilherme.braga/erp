unit uDmConnection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Dialogs, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, Forms, FireDAC.Phys.MySQL,
  FireDAC.FMXUI.Wait, FireDAC.Comp.UI;

type
  TDm = class(TDataModule)
    Connection: TFDConnection;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dm: TDm;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uIniFileServ;

{$R *.dfm}

procedure TDm.DataModuleCreate(Sender: TObject);
begin
  try
    Connection.Connected := false;

    Connection.Params.Clear;
    Connection.Params.Add('Database='+IniSystemFile.databaseERP);
    Connection.Params.Add('User_Name=root');
    Connection.Params.Add('Password=root');
    Connection.Params.Add('port='+IniSystemFile.porta);
    Connection.Params.Add('Server='+IniSystemFile.server);
    Connection.Params.Add('DriverID=MySQL');

    Connection.Connected := true;
  Except
    try
      Connection.Connected := false;

      Connection.Params.Clear;
      Connection.Params.Add('Database='+IniSystemFile.databaseERP);
      Connection.Params.Add('User_Name=root');
      Connection.Params.Add('Password=t23mhcr247');
      Connection.Params.Add('Server='+IniSystemFile.server);
      Connection.Params.Add('DriverID=MySQL');

      Connection.Connected := true;
    except
      on e:Exception do
      begin
        showMessage('Erro ao tentar estabelecer conex�o com o banco de dados: '+e.message);
        Application.Terminate;
      end;
    end;
  end;
end;

end.
