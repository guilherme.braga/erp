unit uIniFileServ;

interface

uses
  Classes, SysUtils, IniFiles, Windows, FMX.Forms;

type TIniSystemFile = record
  server, databaseERP, porta, caminho_gbak, caminho_relatorios: String;
  IndoorPort : Integer;
end;

type TIniOptions = class(TObject)
  private
    function FileName: String;
  public
    constructor Create;
    procedure LoadSettings(Ini: TIniFile);
    procedure SaveSettings(Ini: TIniFile);

    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);

  end;

var
  IniOptions: TIniOptions = nil;
  IniSystemFile: TIniSystemFile;

implementation

procedure TIniOptions.LoadSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin
      IniSystemFile.server := ReadString('MYSQL','server','127.0.0.1');
      IniSystemFile.porta := ReadString('MYSQL','porta','3306');
      IniSystemFile.databaseERP := ReadString('MYSQL','databaseERP','gestao_empresarial');
      IniSystemFile.IndoorPort := ReadInteger('INDOOR', 'IndoorPort', 8081);
    end;
  end;
end;

procedure TIniOptions.SaveSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    with Ini do
    begin
      WriteString('MYSQL','server','127.0.0.1');
      WriteString('MYSQL','porta','3306');
      WriteString('MYSQL','databaseERP','gestao_empresarial');
      WriteString('INDOOR','IndoorPort','8081');
    end;
  end;
end;

constructor TIniOptions.Create;
begin
  inherited Create;
  LoadFromFile(FileName);
end;

function TIniOptions.FileName: String;
begin
  result :=
    ExtractFilePath(ParamStr(0))+
    StringReplace(ExtractFileName(ParamStr(0))+'.ini',
                  ExtractFileExt(ExtractFileName(ParamStr(0))),
                  '', [rfReplaceAll]);
end;

procedure TIniOptions.LoadFromFile(const FileName: string);
var
  Ini: TIniFile;
begin
  if not FileExists(FileName) then
    SaveToFile(FileName);

  Ini := TIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    Ini.Free;
  end;
end;

procedure TIniOptions.SaveToFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    SaveSettings(Ini);
  finally
    Ini.Free;
  end;
end;

initialization
  IniOptions := TIniOptions.Create;

finalization
  IniOptions.Free;

end.

