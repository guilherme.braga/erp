program gCode;



uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  UFrmPrincipal in 'UFrmPrincipal.pas' {Form1},
  uDmConnection in 'uDmConnection.pas' {Dm: TDataModule},
  uFactoryQuery in 'uFactoryQuery.pas',
  uGeradorCodigo in 'uGeradorCodigo.pas',
  uIniFileServ in 'uIniFileServ.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.UseMetropolisUI;
  TStyleManager.TrySetStyle('Metropolis UI Dark');
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Metropolis UI Application';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDm, Dm);
  Application.Run;
end.
