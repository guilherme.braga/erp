unit uMotorCalculoNotaFiscalCofins;

interface

Uses Data.DB, SysUtils, uFilialProxy, uImpostoPISCofinsProxy;

type TMotorCalculoNotaFiscalCofins = class
  private
    FTipoDocumentoFiscal: String;
    FTipoNotaFiscal: String;
    FDatasetNotaFiscal: TDataset;
    FDatasetProduto: TDataset;
    FFilial: TFilialProxy;
    FImpostoCofins: TImpostoCofinsCofinsProxy;

    function CalcularPIS(ACalularImpostoCofins: TEntradaCalculoImpostoCofinsProxy): TSaidaCalculoImpostoCofinsProxy;
  public
    procedure CalcularImpostoCofins;

    constructor Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String; ADatasetNotaFiscal,
        ADatasetProduto: TDataset; AFilialProxy: TFilialProxy; AImpostoCofins: TImpostoCofinsCofinsProxy) overload;
end;

implementation

uses uMathUtils;

constructor TMotorCalculoNotaFiscalCofins.Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String;
  ADatasetNotaFiscal, ADatasetProduto: TDataset; AFilialProxy: TFilialProxy;
  AImpostoCofins: TImpostoCofinsCofinsProxy);
begin
  inherited Create;
  FTipoDocumentoFiscal := ATipoDocumentoFiscal;
  FTipoNotaFiscal := ATipoNotaFiscal;
  FDatasetNotaFiscal := ADatasetNotaFiscal;
  FDatasetProduto := ADatasetProduto;

  FFilial := AFilialProxy;
  FImpostoCofins := AImpostoCofins;
end;

procedure TMotorCalculoNotaFiscalCofins.CalcularImpostoPIS;
var
  calcularImpostoCofinsEnvio: TEntradaCalculoImpostoCofinsProxy;
  calcularImpostoCofinsResposta: TSaidaCalculoImpostoCofinsProxy;
begin
  calcularImpostoCofinsEnvio := TEntradaCalculoImpostoIcmsProxy.Create;

  with calcularImpostoCofinsEnvio do
  begin
    FCRTEmitente := FFilial.FCrt;
    FCST := FDatasetProduto.FieldByName('N_CST').AsString;
    FImpostoIcms := Self.FImpostoIcms;
    FQuantidadeItem := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
    FValorUnitario := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;
    FValorSeguro := FDatasetProduto.FieldByName('I_VSEG').AsFloat;
    FValorOutrasDespesas := FDatasetProduto.FieldByName('I_VOUTRO').AsFloat;
    FValorDesconto := FDatasetProduto.FieldByName('I_VDESC').AsFloat;
  end;

  calcularImpostoCofinsResposta := TSaidaCalculoImpostoIcmsProxy.Create;
  try
    calcularImpostoCofinsResposta := CalcularCofins(calcularImpostoCofinsEnvio);

    FDatasetProduto.FieldByName('N_VBC').AsFloat := calcularImpostoCofinsResposta.FValorBaseCalculoICMSCalculado;
    FDatasetProduto.FieldByName('N_VICMS').AsFloat := calcularImpostoCofinsResposta.FValorICMSCalculado;
    FDatasetProduto.FieldByName('N_PICMS').AsFloat := calcularImpostoCofinsResposta.FPercentualICMSCalculado;
    FDatasetProduto.FieldByName('N_VBCST').AsFloat := calcularImpostoCofinsResposta.FValorBaseCalculoICMSSTCalculado;
    FDatasetProduto.FieldByName('N_VICMSST').AsFloat := calcularImpostoCofinsResposta.FValorICMSSTCalculado;
    FDatasetProduto.FieldByName('N_PICMSST').AsFloat := calcularImpostoCofinsResposta.FPercentualICMSSTCalculado;

    //MUDAR URGENTE VAI DAR PAU QDO NAO TIVER ICMS - ISSO É MUITO PROVISORIO
    FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat := calcularImpostoCofinsResposta.FValorBaseOperacaoCalculado;
    FDatasetProduto.FieldByName('I_VPROD').AsFloat := FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat;
  finally
    FreeAndNil(calcularImpostoCofinsResposta);
  end;
end;

function TMotorCalculoNotaFiscalCofins.CalcularCofins(
  ACalularImpostoCofins: TEntradaCalculoImpostoCofinsProxy): TSaidaCalculoImpostoCofinsProxy;
begin
  result := TSaidaCalculoImpostoCofinsProxy.Create;

  with ACalularImpostoCofins do
  begin
    case StrtoIntDef(ACalularImpostoIcms.FCST, 0) do
      01:
      02:
      begin       
        result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

        result.FPercentualPISCalculado := FImpostoCofins.FPercAliquotaCofins;              
        result.FValorBaseCalculoPISCalculado := result.FValorBaseOperacaoCalculado;              
        result.FValorPISCalculado = ceil(result.FPercentualBaseCalculoPISCalculado * result.FPercentualPISCalculado) / 100;        

        result.FPercentualBaseCalculoPISCalculado := TMathUtils.PercentualSobreValor(
          result.FValorBaseCalculoPISCalculado, result.FValorBaseOperacaoCalculado);

        result.FPercentualPISCalculado := TMathUtils.PercentualSobreValor(
          result.FValorPISCalculado, result.FValorBaseOperacaoCalculado);
      end;      
      03:
      begin        
        {$item->imposto->PIS->CST = $tributacaoPIS->CST;
        $item->imposto->PIS->qBCProd = $produto->qTrib;
        $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS;  COMENTADO POR CAUSA DESSE CAMPO AQUI
        $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
      end;  
      04:
      05:
      06:
      07:
      08:
      09:
      begin
      end;      
      else
      begin
        if ($tributacaoPIS->TipoTributacaoPISCOFINS == 0) then
        begin          
          result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

          result.FPercentualPISCalculado := FImpostoCofins.FPercAliquotaCofins;              
          result.FValorBaseCalculoPISCalculado := result.FValorBaseOperacaoCalculado;              
          result.FValorPISCalculado = ceil(result.FPercentualBaseCalculoPISCalculado * result.FPercentualPISCalculado) / 100;        

          result.FPercentualBaseCalculoPISCalculado := TMathUtils.PercentualSobreValor(
            result.FValorBaseCalculoPISCalculado, result.FValorBaseOperacaoCalculado);

          result.FPercentualPISCalculado := TMathUtils.PercentualSobreValor(
            result.FValorPISCalculado, result.FValorBaseOperacaoCalculado);
        end;    
        else 
        begin          
          {$item->imposto->PIS->qBCProd = $produto->qTrib;
          $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS; COMENTADO POR CAUSA DESSE CAMPO AQUI
          $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
        end;  
      end;                    
    end;    
  end;
end;

end.