unit uMotorCalculoNotaFiscalPIS;

interface

Uses Data.DB, SysUtils, uFilialProxy, uImpostoPISCofinsProxy;

type TMotorCalculoNotaFiscalPIS = class
  private
    FTipoDocumentoFiscal: String;
    FTipoNotaFiscal: String;
    FDatasetNotaFiscal: TDataset;
    FDatasetProduto: TDataset;
    FFilial: TFilialProxy;
    FImpostoPis: TImpostoPisCofinsProxy;

    function CalcularPIS(ACalularImpostoPis: TEntradaCalculoImpostoPisProxy): TSaidaCalculoImpostoPisProxy;
  public
    procedure CalcularImpostoPIS;

    constructor Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String; ADatasetNotaFiscal,
        ADatasetProduto: TDataset; AFilialProxy: TFilialProxy; AImpostoPis: TImpostoPisCofinsProxy) overload;
end;

implementation

uses uMathUtils;

constructor TMotorCalculoNotaFiscalPIS.Create(const ATipoDocumentoFiscal, ATipoNotaFiscal: String;
  ADatasetNotaFiscal, ADatasetProduto: TDataset; AFilialProxy: TFilialProxy;
  AImpostoPIS: TImpostoPisCofinsProxy);
begin
  inherited Create;
  FTipoDocumentoFiscal := ATipoDocumentoFiscal;
  FTipoNotaFiscal := ATipoNotaFiscal;
  FDatasetNotaFiscal := ADatasetNotaFiscal;
  FDatasetProduto := ADatasetProduto;

  FFilial := AFilialProxy;
  FImpostoPis := AImpostoPis;
end;

procedure TMotorCalculoNotaFiscalPIS.CalcularImpostoPIS;
var
  calcularImpostoPISEnvio: TEntradaCalculoImpostoPISProxy;
  calcularImpostoPISResposta: TSaidaCalculoImpostoPISProxy;
begin
  calcularImpostoPISEnvio := TEntradaCalculoImpostoIcmsProxy.Create;

  with calcularImpostoPISEnvio do
  begin
    FCRTEmitente := FFilial.FCrt;
    FCST := FDatasetProduto.FieldByName('N_CST').AsString;
    FImpostoIcms := Self.FImpostoIcms;
    FQuantidadeItem := FDatasetProduto.FieldByName('I_QCOM').AsFloat;
    FValorUnitario := FDatasetProduto.FieldByName('I_VUNCOM').AsFloat;
    FValorSeguro := FDatasetProduto.FieldByName('I_VSEG').AsFloat;
    FValorOutrasDespesas := FDatasetProduto.FieldByName('I_VOUTRO').AsFloat;
    FValorDesconto := FDatasetProduto.FieldByName('I_VDESC').AsFloat;
  end;

  calcularImpostoPISResposta := TSaidaCalculoImpostoIcmsProxy.Create;
  try
    calcularImpostoPISResposta := CalcularICMS(calcularImpostoPISEnvio);

    FDatasetProduto.FieldByName('N_VBC').AsFloat := calcularImpostoPISResposta.FValorBaseCalculoICMSCalculado;
    FDatasetProduto.FieldByName('N_VICMS').AsFloat := calcularImpostoPISResposta.FValorICMSCalculado;
    FDatasetProduto.FieldByName('N_PICMS').AsFloat := calcularImpostoPISResposta.FPercentualICMSCalculado;
    FDatasetProduto.FieldByName('N_VBCST').AsFloat := calcularImpostoPISResposta.FValorBaseCalculoICMSSTCalculado;
    FDatasetProduto.FieldByName('N_VICMSST').AsFloat := calcularImpostoPISResposta.FValorICMSSTCalculado;
    FDatasetProduto.FieldByName('N_PICMSST').AsFloat := calcularImpostoPISResposta.FPercentualICMSSTCalculado;

    //MUDAR URGENTE VAI DAR PAU QDO NAO TIVER ICMS - ISSO É MUITO PROVISORIO
    FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat := calcularImpostoPISResposta.FValorBaseOperacaoCalculado;
    FDatasetProduto.FieldByName('I_VPROD').AsFloat := FDatasetProduto.FieldByName('VL_LIQUIDO').AsFloat;
  finally
    FreeAndNil(calcularImpostoPISResposta);
  end;
end;

function TMotorCalculoNotaFiscalICMS.CalcularPIS(
  ACalularImpostoPis: TEntradaCalculoImpostoPisProxy): TSaidaCalculoImpostoPisProxy;
begin
  result := TSaidaCalculoImpostoPisProxy.Create;

  with ACalularImpostoPis do
  begin
    case StrtoIntDef(ACalularImpostoIcms.FCST, 0) do
      01:
      02:
      begin       
        result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

        result.FPercentualPISCalculado := FImpostoPis.FPercAliquotaPis;              
        result.FValorBaseCalculoPISCalculado := result.FValorBaseOperacaoCalculado;              
        result.FValorPISCalculado = ceil(result.FPercentualBaseCalculoPISCalculado * result.FPercentualPISCalculado) / 100;        

        result.FPercentualBaseCalculoPISCalculado := TMathUtils.PercentualSobreValor(
          result.FValorBaseCalculoPISCalculado, result.FValorBaseOperacaoCalculado);

        result.FPercentualPISCalculado := TMathUtils.PercentualSobreValor(
          result.FValorPISCalculado, result.FValorBaseOperacaoCalculado);
      end;      
      03:
      begin        
        {$item->imposto->PIS->CST = $tributacaoPIS->CST;
        $item->imposto->PIS->qBCProd = $produto->qTrib;
        $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS;  COMENTADO POR CAUSA DESSE CAMPO AQUI
        $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
      end;  
      04:
      05:
      06:
      07:
      08:
      09:
      begin
      end;      
      else
      begin
        if ($tributacaoPIS->TipoTributacaoPISCOFINS == 0) then
        begin          
          result.FValorBaseOperacaoCalculado := (FValorUnitario * FQuantidadeItem) - FValorDesconto;

          result.FPercentualPISCalculado := FImpostoPis.FPercAliquotaPis;              
          result.FValorBaseCalculoPISCalculado := result.FValorBaseOperacaoCalculado;              
          result.FValorPISCalculado = ceil(result.FPercentualBaseCalculoPISCalculado * result.FPercentualPISCalculado) / 100;        

          result.FPercentualBaseCalculoPISCalculado := TMathUtils.PercentualSobreValor(
            result.FValorBaseCalculoPISCalculado, result.FValorBaseOperacaoCalculado);

          result.FPercentualPISCalculado := TMathUtils.PercentualSobreValor(
            result.FValorPISCalculado, result.FValorBaseOperacaoCalculado);
        end;    
        else 
        begin          
          {$item->imposto->PIS->qBCProd = $produto->qTrib;
          $item->imposto->PIS->vAliqProd = $tributacaoPIS->ValorPIS; COMENTADO POR CAUSA DESSE CAMPO AQUI
          $item->imposto->PIS->vPIS = $item->imposto->PIS->qBCProd * $item->imposto->PIS->vAliqProd;}
        end;  
      end;                    
    end;    
  end;
end;

end.