program Obliviate;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uFrmMain in 'forms\uFrmMain.PAS' {MainForm},
  uFastReport in '..\library\uFastReport.pas',
  ConverterGDI2DMP in 'classes\ConverterGDI2DMP.pas',
  ConverterDMP2GDI in 'classes\ConverterDMP2GDI.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
