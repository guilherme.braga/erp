CREATE PROCEDURE `correcao_condicional`()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE vid Int default 0;  
  DECLARE vidvenda Int default 0;
  DECLARE vidchaveprocesso Int default 0;
  DECLARE vidfilial Int default 0;
  DECLARE vidproduto Decimal(24,9) default 0.00;
  DECLARE vnritem Decimal(24,9) default 0.00;
  DECLARE vquantidade Decimal(24,9) default 0.00;
  DECLARE vquantidadeEstoqueAnterior Decimal(24,9) default 0.00;
  DECLARE vvlDesconto Decimal(24,9) default 0.00;
  DECLARE vvlacrescimo Decimal(24,9) default 0.00;
  DECLARE vvlbruto Decimal(24,9) default 0.00;
  DECLARE vvlliquido Decimal(24,9) default 0.00;
  DECLARE vpercdesconto Decimal(24,9) default 0.00;
  DECLARE vpercacrescimo Decimal(24,9) default 0.00;
  DECLARE vdttadastro DATE;            
  
  DECLARE vQT_ESTOQUE_ANTERIOR  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_CUSTO  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_FRETE  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_SEGURO  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_ICMS_ST  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_IPI  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_DESCONTO  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMA_DESPESA_ACESSORIA  Decimal(24,9) default 0.00;
  DECLARE vVL_ULTIMO_CUSTO_IMPOSTO  Decimal(24,9) default 0.00;
  DECLARE vVL_CUSTO_OPERACIONAL  Decimal(24,9) default 0.00;
  DECLARE vVL_CUSTO_IMPOSTO_OPERACIONAL  Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMO_FRETE  Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMO_SEGURO Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMO_ICMS_ST  Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMO_IPI  Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMO_DESCONTO  Decimal(24,9) default 0.00;
  DECLARE vPERC_ULTIMA_DESPESA_ACESSORIA  Decimal(24,9) default 0.00;
  DECLARE vPERC_CUSTO_OPERACIONAL  Decimal(24,9) default 0.00;
  DECLARE vVL_CUSTO_MEDIO  Decimal(24,9) default 0.00;
  DECLARE vVL_CUSTO_MEDIO_IMPOSTO  Decimal(24,9) default 0.00;
  DECLARE vVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL  Decimal(24,9) default 0.00;
  
  DECLARE itens_condicionais CURSOR FOR 
      select venda_item.*, 
             CAST(venda.dh_cadastro AS DATE), 
             venda.id_chave_processo, 
             venda.id_filial 
        from venda_item 
  inner join venda on venda_item.id_venda = venda.id 
       where venda.tipo = 'CONDICIONAL' order by venda_item.id_produto, venda.id;          

#Deletar da produto movimento
DELETE FROM produto_movimento where id_chave_processo in (select id_chave_processo from venda where tipo = 'CONDICIONAL');

#Deletar da reserva de estoque  
DELETE FROM produto_reserva_estoque_movimento;


  OPEN itens_condicionais;
  
  read_loop: LOOP
    FETCH itens_condicionais INTO 
    vid
   ,vidvenda
   ,vidproduto 
   ,vnritem 
   ,vquantidade 
   ,vvlDesconto
   ,vvlacrescimo 
   ,vvlbruto 
   ,vvlliquido 
   ,vpercdesconto 
   ,vpercacrescimo
   ,vdttadastro
   ,vidchaveprocesso
   ,vidfilial;  
         
  select    
  QT_ESTOQUE_ATUAL
  ,VL_ULTIMO_CUSTO
  ,VL_ULTIMO_FRETE
  ,VL_ULTIMO_SEGURO
  ,VL_ULTIMO_ICMS_ST
  ,VL_ULTIMO_IPI
  ,VL_ULTIMO_DESCONTO
  ,VL_ULTIMA_DESPESA_ACESSORIA 
  ,VL_ULTIMO_CUSTO_IMPOSTO 
  ,VL_CUSTO_OPERACIONAL 
  ,VL_CUSTO_IMPOSTO_OPERACIONAL 
  ,PERC_ULTIMO_FRETE 
  ,PERC_ULTIMO_SEGURO
  ,PERC_ULTIMO_ICMS_ST 
  ,PERC_ULTIMO_IPI 
  ,PERC_ULTIMO_DESCONTO 
  ,PERC_ULTIMA_DESPESA_ACESSORIA 
  ,PERC_CUSTO_OPERACIONAL 
  ,VL_CUSTO_MEDIO 
  ,VL_CUSTO_MEDIO_IMPOSTO 
  ,VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL 
INTO
  vQT_ESTOQUE_ANTERIOR
  ,vVL_ULTIMO_CUSTO
  ,vVL_ULTIMO_FRETE
  ,vVL_ULTIMO_SEGURO
  ,vVL_ULTIMO_ICMS_ST
  ,vVL_ULTIMO_IPI
  ,vVL_ULTIMO_DESCONTO
  ,vVL_ULTIMA_DESPESA_ACESSORIA 
  ,vVL_ULTIMO_CUSTO_IMPOSTO 
  ,vVL_CUSTO_OPERACIONAL 
  ,vVL_CUSTO_IMPOSTO_OPERACIONAL 
  ,vPERC_ULTIMO_FRETE 
  ,vPERC_ULTIMO_SEGURO
  ,vPERC_ULTIMO_ICMS_ST 
  ,vPERC_ULTIMO_IPI 
  ,vPERC_ULTIMO_DESCONTO 
  ,vPERC_ULTIMA_DESPESA_ACESSORIA 
  ,vPERC_CUSTO_OPERACIONAL 
  ,vVL_CUSTO_MEDIO 
  ,vVL_CUSTO_MEDIO_IMPOSTO 
  ,vVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL 
FROM produto_movimento where id = (select max(id) from produto_movimento where id_produto = vidproduto);   
   
   
   INSERT INTO 
  `produto_movimento`
(
  `TIPO`,
  `DT_MOVIMENTO`,
  `QT_ESTOQUE_ANTERIOR`,
  `QT_MOVIMENTO`,
  `QT_ESTOQUE_ATUAL`,
  `DOCUMENTO_ORIGEM`,
  `ID_PRODUTO`,
  `ID_FILIAL`,
  `ID_CHAVE_PROCESSO`,
  `VL_ULTIMO_CUSTO`,
  `QT_ESTOQUE_CONDICIONAL`,
  `VL_ULTIMO_FRETE`,
  `VL_ULTIMO_SEGURO`,
  `VL_ULTIMO_ICMS_ST`,
  `VL_ULTIMO_IPI`,
  `VL_ULTIMO_DESCONTO`,
  `VL_ULTIMA_DESPESA_ACESSORIA`,
  `VL_ULTIMO_CUSTO_IMPOSTO`,
  `VL_CUSTO_OPERACIONAL`,
  `VL_CUSTO_IMPOSTO_OPERACIONAL`,
  `PERC_ULTIMO_FRETE`,
  `PERC_ULTIMO_SEGURO`,
  `PERC_ULTIMO_ICMS_ST`,
  `PERC_ULTIMO_IPI`,
  `PERC_ULTIMO_DESCONTO`,
  `PERC_ULTIMA_DESPESA_ACESSORIA`,
  `PERC_CUSTO_OPERACIONAL`,
  `VL_CUSTO_MEDIO`,
  `VL_CUSTO_MEDIO_IMPOSTO`,
  `VL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL`,
  `BO_PROCESSO_AMORTIZADO`
) 
VALUE (
  'SAIDA',
  vdttadastro,
  vQT_ESTOQUE_ANTERIOR,
  vquantidade,
  vQT_ESTOQUE_ANTERIOR-vquantidade,
  'VENDA',
  vidproduto,
  vidfilial,
  vidchaveprocesso,
  vVL_ULTIMO_CUSTO,
  0,
  vVL_ULTIMO_FRETE,
  vVL_ULTIMO_SEGURO,
  vVL_ULTIMO_ICMS_ST,
  vVL_ULTIMO_IPI,
  vVL_ULTIMO_DESCONTO,
  vVL_ULTIMA_DESPESA_ACESSORIA,
  vVL_ULTIMO_CUSTO_IMPOSTO,
  vVL_CUSTO_OPERACIONAL,
  vVL_CUSTO_IMPOSTO_OPERACIONAL,
  vPERC_ULTIMO_FRETE,
  vPERC_ULTIMO_SEGURO,
  vPERC_ULTIMO_ICMS_ST,
  vPERC_ULTIMO_IPI,
  vPERC_ULTIMO_DESCONTO,
  vPERC_ULTIMA_DESPESA_ACESSORIA,
  vPERC_CUSTO_OPERACIONAL,
  vVL_CUSTO_MEDIO,
  vVL_CUSTO_MEDIO_IMPOSTO,
  vVL_CUSTO_MEDIO_IMPOSTO_OPERACIONAL,
  'N'
);
  
select 0 into vquantidadeEstoqueAnterior from filial where id = 1;

SELECT ifnull(QT_ESTOQUE_ATUAL, 0)
 INTO vquantidadeEstoqueAnterior
 FROM produto_reserva_estoque_movimento
 WHERE id = (SELECT MAX(id) from produto_reserva_estoque_movimento 
             where id_produto = vidproduto);
     
   INSERT INTO 
  `produto_reserva_estoque_movimento`
(
  `TIPO`,
  `DT_MOVIMENTO`,
  `QT_ESTOQUE_ANTERIOR`,
  `QT_MOVIMENTO`,
  `QT_ESTOQUE_ATUAL`,
  `DOCUMENTO_ORIGEM`,
  `ID_PRODUTO`,
  `ID_FILIAL`,
  `ID_CHAVE_PROCESSO`,
  `BO_PROCESSO_AMORTIZADO`,
  `TIPO_RESERVA`
) 
VALUE (
   'ENTRADA',
   vdttadastro,
   vquantidadeEstoqueAnterior,
   vquantidade,
   vquantidadeEstoqueAnterior + vquantidade,
   'VENDA',
   vidproduto,
   vidfilial,
   vidchaveprocesso,
  'N',
  'CONDICIONAL'
);  
   
    #Atualizar quantidade condicional da produto filial  
    UPDATE produto_filial SET 
      QT_ESTOQUE_CONDICIONAL = vquantidadeEstoqueAnterior + vquantidade, 
      QT_ESTOQUE = vQT_ESTOQUE_ANTERIOR-vquantidade 
    WHERE ID_PRODUTO = vidproduto;

        
  END LOOP;
    
  CLOSE itens_condicionais;
END;